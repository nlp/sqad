<s>
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
</s>
<s>
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
Logo	logo	k1gNnSc1
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
Datum	datum	k1gNnSc1
zániku	zánik	k1gInSc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Císařsko-královské	císařsko-královský	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
Nástupci	nástupce	k1gMnPc1
</s>
<s>
České	český	k2eAgFnPc1d1
dráhyŽeleznice	dráhyŽeleznice	k1gFnPc1
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Charakteristika	charakteristikon	k1gNnSc2
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
kolejová	kolejový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
železniční	železniční	k2eAgFnSc1d1
infrastruktura	infrastruktura	k1gFnSc1
a	a	k8xC
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
Identifikátory	identifikátor	k1gInPc1
IČO	IČO	kA
</s>
<s>
00407135	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
(	(	kIx(
<g/>
ČSD	ČSD	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
státní	státní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
správou	správa	k1gFnSc7
železniční	železniční	k2eAgFnSc2d1
infrastruktury	infrastruktura	k1gFnSc2
a	a	k8xC
provozováním	provozování	k1gNnSc7
železniční	železniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
celého	celý	k2eAgNnSc2d1
tehdejšího	tehdejší	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
a	a	k8xC
provozovala	provozovat	k5eAaImAgNnP
dopravu	doprava	k1gFnSc4
i	i	k9
na	na	k7c6
několika	několik	k4yIc6
soukromých	soukromý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1927	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
(	(	kIx(
<g/>
mimo	mimo	k7c4
období	období	k1gNnSc4
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
provozovala	provozovat	k5eAaImAgFnS
též	též	k9
síť	síť	k1gFnSc1
státní	státní	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c4
den	den	k1gInSc4
vzniku	vznik	k1gInSc2
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
zanikly	zaniknout	k5eAaPmAgFnP
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
státu	stát	k1gInSc2
nahradily	nahradit	k5eAaPmAgFnP
protektorátní	protektorátní	k2eAgFnPc1d1
Českomoravské	českomoravský	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
a	a	k8xC
Slovenské	slovenský	k2eAgFnPc1d1
železnice	železnice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
byly	být	k5eAaImAgFnP
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
ČSD	ČSD	kA
obnoveny	obnovit	k5eAaPmNgInP
a	a	k8xC
fungovaly	fungovat	k5eAaImAgInP
až	až	k9
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zanikly	zaniknout	k5eAaPmAgFnP
definitivně	definitivně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nástupnickými	nástupnický	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
České	český	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
a	a	k8xC
Železnice	železnice	k1gFnSc1
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
byla	být	k5eAaImAgFnS
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
Mezinárodní	mezinárodní	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jí	jíst	k5eAaImIp3nS
přidělen	přidělen	k2eAgMnSc1d1
UIC	UIC	kA
kód	kód	k1gInSc4
54	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
po	po	k7c6
jejím	její	k3xOp3gNnSc6
rozdělení	rozdělení	k1gNnSc6
používaly	používat	k5eAaImAgFnP
České	český	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
a	a	k8xC
po	po	k7c6
liberalizaci	liberalizace	k1gFnSc6
přístupu	přístup	k1gInSc2
na	na	k7c4
železniční	železniční	k2eAgFnSc4d1
dopravní	dopravní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
je	být	k5eAaImIp3nS
kódem	kód	k1gInSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
;	;	kIx,
Slovenské	slovenský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
náleží	náležet	k5eAaImIp3nS
kód	kód	k1gInSc4
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Období	období	k1gNnSc4
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
počátky	počátek	k1gInPc1
ČSD	ČSD	kA
</s>
<s>
Název	název	k1gInSc1
společnosti	společnost	k1gFnSc2
na	na	k7c6
vlaku	vlak	k1gInSc6
ČSD	ČSD	kA
</s>
<s>
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
skončení	skončení	k1gNnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převzaly	převzít	k5eAaPmAgInP
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc4d1
železniční	železniční	k2eAgFnSc4d1
síť	síť	k1gFnSc4
bývalých	bývalý	k2eAgMnPc2d1
c.	c.	k?
k.	k.	k?
Státních	státní	k2eAgFnPc2d1
drah	draha	k1gFnPc2
(	(	kIx(
<g/>
kkStB	kkStB	k?
<g/>
)	)	kIx)
a	a	k8xC
Maďarských	maďarský	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
drah	draha	k1gFnPc2
(	(	kIx(
<g/>
MÁV	MÁV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
resp.	resp.	kA
jejich	jejich	k3xOp3gNnPc4
části	část	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
na	na	k7c4
území	území	k1gNnSc4
nově	nově	k6eAd1
založeného	založený	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnSc2d1
soukromé	soukromý	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
–	–	k?
Košicko-bohumínská	košicko-bohumínský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
Ústecko-teplická	ústecko-teplický	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
a	a	k8xC
Buštěhradská	buštěhradský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
zůstaly	zůstat	k5eAaPmAgInP
v	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
samostatné	samostatný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mnoha	mnoho	k4c2
státem	stát	k1gInSc7
garantovaných	garantovaný	k2eAgFnPc2d1
lokálních	lokální	k2eAgFnPc2d1
drah	draha	k1gFnPc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
převzaly	převzít	k5eAaPmAgFnP
ČSD	ČSD	kA
od	od	k7c2
kkStB	kkStB	k?
jejich	jejich	k3xOp3gInSc4
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
ředitelem	ředitel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
de	de	k?
iure	iure	k1gInSc1
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Bašta	Bašta	k1gMnSc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
působící	působící	k2eAgInPc1d1
v	v	k7c6
kkStB	kkStB	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
setrval	setrvat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
privátní	privátní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
zákonem	zákon	k1gInSc7
zestátňovány	zestátňován	k2eAgFnPc4d1
a	a	k8xC
začleňovány	začleňován	k2eAgFnPc4d1
do	do	k7c2
sítě	síť	k1gFnSc2
ČSD	ČSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byly	být	k5eAaImAgInP
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
měněny	měněn	k2eAgInPc4d1
názvy	název	k1gInPc4
stanic	stanice	k1gFnPc2
na	na	k7c6
české	český	k2eAgFnSc6d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
německy	německy	k6eAd1
mluvících	mluvící	k2eAgNnPc6d1
územích	území	k1gNnPc6
byly	být	k5eAaImAgFnP
zachovány	zachovat	k5eAaPmNgInP
dvojjazyčné	dvojjazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
existence	existence	k1gFnSc2
samostatného	samostatný	k2eAgInSc2d1
státu	stát	k1gInSc2
bylo	být	k5eAaImAgNnS
zapotřebí	zapotřebí	k6eAd1
vyvinout	vyvinout	k5eAaPmF
velké	velký	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
železniční	železniční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
orientovaná	orientovaný	k2eAgFnSc1d1
na	na	k7c4
Vídeň	Vídeň	k1gFnSc4
a	a	k8xC
Budapešť	Budapešť	k1gFnSc4
přizpůsobila	přizpůsobit	k5eAaPmAgFnS
novým	nový	k2eAgInPc3d1
přepravním	přepravní	k2eAgInPc3d1
požadavkům	požadavek	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
dopravu	doprava	k1gFnSc4
ve	v	k7c6
směru	směr	k1gInSc6
východ	východ	k1gInSc4
–	–	k?
<g/>
západ	západ	k1gInSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
počátku	počátek	k1gInSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pouze	pouze	k6eAd1
náročně	náročně	k6eAd1
trasovaná	trasovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
jednokolejná	jednokolejný	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Košicko-bohumínské	košicko-bohumínský	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
investovat	investovat	k5eAaBmF
do	do	k7c2
modernizace	modernizace	k1gFnSc2
nejdůležitějších	důležitý	k2eAgFnPc2d3
tratí	trať	k1gFnPc2
a	a	k8xC
výstavby	výstavba	k1gFnSc2
nových	nový	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
projektů	projekt	k1gInPc2
byla	být	k5eAaImAgFnS
modernizace	modernizace	k1gFnSc1
<g/>
,	,	kIx,
zdvojkolejnění	zdvojkolejnění	k1gNnSc1
a	a	k8xC
částečná	částečný	k2eAgFnSc1d1
novostavba	novostavba	k1gFnSc1
trati	trať	k1gFnSc2
Praha	Praha	k1gFnSc1
–	–	k?
Brno	Brno	k1gNnSc1
přes	přes	k7c4
Havlíčkův	Havlíčkův	k2eAgInSc4d1
Brod	Brod	k1gInSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
dokončit	dokončit	k5eAaPmF
až	až	k9
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zavádění	zavádění	k1gNnSc1
nových	nový	k2eAgFnPc2d1
trakcí	trakce	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
inovací	inovace	k1gFnPc2
</s>
<s>
Motorový	motorový	k2eAgInSc1d1
vůz	vůz	k1gInSc1
řady	řada	k1gFnSc2
M	M	kA
120.4	120.4	k4
s	s	k7c7
věžovým	věžový	k2eAgNnSc7d1
stanovištěm	stanoviště	k1gNnSc7
strojvedoucího	strojvedoucí	k1gMnSc2
</s>
<s>
Jako	jako	k9
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
evropských	evropský	k2eAgFnPc2d1
železničních	železniční	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
začaly	začít	k5eAaPmAgFnP
ČSD	ČSD	kA
usilovat	usilovat	k5eAaImF
o	o	k7c6
motorizaci	motorizace	k1gFnSc6
provozu	provoz	k1gInSc2
na	na	k7c6
svých	svůj	k3xOyFgFnPc6
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
nasadily	nasadit	k5eAaPmAgFnP
na	na	k7c6
několika	několik	k4yIc6
málo	málo	k6eAd1
zatížených	zatížený	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
motorové	motorový	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
skládaly	skládat	k5eAaImAgFnP
z	z	k7c2
benzolelektrického	benzolelektrický	k2eAgInSc2d1
motorového	motorový	k2eAgInSc2d1
vozu	vůz	k1gInSc2
a	a	k8xC
příslušných	příslušný	k2eAgInPc2d1
přívěsných	přívěsný	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
30	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
motorové	motorový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
nasazovány	nasazován	k2eAgInPc1d1
i	i	k9
na	na	k7c6
místních	místní	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
již	již	k9
v	v	k7c6
polovině	polovina	k1gFnSc6
30	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
podařilo	podařit	k5eAaPmAgNnS
na	na	k7c6
mnoha	mnoho	k4c6
vedlejších	vedlejší	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
prakticky	prakticky	k6eAd1
odstranit	odstranit	k5eAaPmF
parní	parní	k2eAgInSc4d1
provoz	provoz	k1gInSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
zrychlit	zrychlit	k5eAaPmF
a	a	k8xC
zhospodárnit	zhospodárnit	k5eAaPmF
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nP
Praha	Praha	k1gFnSc1
nebyla	být	k5eNaImAgFnS
obtěžována	obtěžovat	k5eAaImNgFnS
kouřem	kouř	k1gInSc7
z	z	k7c2
lokomotiv	lokomotiva	k1gFnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
na	na	k7c6
pražských	pražský	k2eAgFnPc6d1
spojovacích	spojovací	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
a	a	k8xC
nádražích	nádraží	k1gNnPc6
využita	využít	k5eAaPmNgFnS
elektrická	elektrický	k2eAgFnSc1d1
trakce	trakce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
byly	být	k5eAaImAgFnP
nasazeny	nasadit	k5eAaPmNgFnP
akumulátorové	akumulátorový	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
a	a	k8xC
poté	poté	k6eAd1
byly	být	k5eAaImAgInP
v	v	k7c6
letech	let	k1gInPc6
1924	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
tratě	trať	k1gFnSc2
elektrizovány	elektrizovat	k5eAaBmNgFnP
soustavou	soustava	k1gFnSc7
1500	#num#	k4
Vss	Vss	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plány	plán	k1gInPc1
na	na	k7c4
elektrizaci	elektrizace	k1gFnSc4
dalších	další	k2eAgFnPc2d1
drah	draha	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
trati	trať	k1gFnSc2
do	do	k7c2
Plzně	Plzeň	k1gFnSc2
se	se	k3xPyFc4
již	již	k6eAd1
neuskutečnily	uskutečnit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Například	například	k6eAd1
počátkem	počátkem	k7c2
září	září	k1gNnSc2
1931	#num#	k4
měly	mít	k5eAaImAgFnP
ČSD	ČSD	kA
4309	#num#	k4
parních	parní	k2eAgFnPc2d1
a	a	k8xC
33	#num#	k4
elektrických	elektrický	k2eAgFnPc2d1
lokomotiv	lokomotiva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
provozu	provoz	k1gInSc6
169	#num#	k4
motorových	motorový	k2eAgNnPc2d1
hnacích	hnací	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
(	(	kIx(
<g/>
62	#num#	k4
motorových	motorový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
96	#num#	k4
kolejových	kolejový	k2eAgInPc2d1
autobusů	autobus	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
posunovacích	posunovací	k2eAgFnPc2d1
lokomotiv	lokomotiva	k1gFnPc2
<g/>
,	,	kIx,
6	#num#	k4
drezín	drezína	k1gFnPc2
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
novin	novina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ČSD	ČSD	kA
také	také	k9
potřebovaly	potřebovat	k5eAaImAgInP
provést	provést	k5eAaPmF
sjednocení	sjednocení	k1gNnSc2
brzdových	brzdový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
rakouských	rakouský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
prosadila	prosadit	k5eAaPmAgFnS
sací	sací	k2eAgFnSc1d1
brzda	brzda	k1gFnSc1
Hardy	Harda	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
uherské	uherský	k2eAgFnSc6d1
části	část	k1gFnSc6
monarchie	monarchie	k1gFnSc2
byla	být	k5eAaImAgFnS
zaváděna	zaváděn	k2eAgFnSc1d1
samočinná	samočinný	k2eAgFnSc1d1
tlaková	tlakový	k2eAgFnSc1d1
brzda	brzda	k1gFnSc1
Westinghouse	Westinghouse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nákladní	nákladní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
byly	být	k5eAaImAgInP
většinou	většinou	k6eAd1
brzděné	brzděný	k2eAgInPc1d1
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byla	být	k5eAaImAgFnS
tlaková	tlakový	k2eAgFnSc1d1
brzda	brzda	k1gFnSc1
zaváděna	zaváděn	k2eAgFnSc1d1
v	v	k7c6
osobní	osobní	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
v	v	k7c6
letech	let	k1gInPc6
1924	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
nákladních	nákladní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
byly	být	k5eAaImAgFnP
vystrojovány	vystrojovat	k5eAaImNgFnP
tlakovou	tlakový	k2eAgFnSc7d1
brzdou	brzda	k1gFnSc7
Kunze-Knorr	Kunze-Knorra	k1gFnPc2
především	především	k6eAd1
ty	ten	k3xDgInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
zajížděly	zajíždět	k5eAaImAgInP
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
byly	být	k5eAaImAgInP
nákladní	nákladní	k2eAgInPc1d1
vozy	vůz	k1gInPc1
vystrojovány	vystrojován	k2eAgInPc1d1
brzdou	brzda	k1gFnSc7
Božič	Božič	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
října	říjen	k1gInSc2
1931	#num#	k4
bylo	být	k5eAaImAgNnS
touto	tento	k3xDgFnSc7
brzdou	brzda	k1gFnSc7
opatřeno	opatřen	k2eAgNnSc1d1
5790	#num#	k4
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
průběžným	průběžný	k2eAgNnSc7d1
brzdovým	brzdový	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
48220	#num#	k4
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
zavedly	zavést	k5eAaPmAgFnP
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
některé	některý	k3yIgFnPc4
další	další	k2eAgFnPc4d1
evropské	evropský	k2eAgFnPc4d1
železnice	železnice	k1gFnPc4
<g/>
,	,	kIx,
rychlé	rychlý	k2eAgInPc4d1
spoje	spoj	k1gInPc4
vedené	vedený	k2eAgInPc4d1
motorovými	motorový	k2eAgInPc7d1
vozy	vůz	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
ČSD	ČSD	kA
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
dva	dva	k4xCgInPc4
motorové	motorový	k2eAgInPc4d1
vozy	vůz	k1gInPc4
řady	řada	k1gFnSc2
M	M	kA
290.0	290.0	k4
známé	známý	k2eAgInPc1d1
spíše	spíše	k9
pod	pod	k7c7
názvem	název	k1gInSc7
vlaku	vlak	k1gInSc2
Slovenská	slovenský	k2eAgFnSc1d1
strela	strela	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
spojoval	spojovat	k5eAaImAgMnS
Bratislavu	Bratislava	k1gFnSc4
s	s	k7c7
Prahou	Praha	k1gFnSc7
s	s	k7c7
jízdní	jízdní	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
4	#num#	k4
h	h	k?
18	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ČSD	ČSD	kA
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
začaly	začít	k5eAaPmAgFnP
ČSD	ČSD	kA
provozovat	provozovat	k5eAaImF
též	též	k9
státní	státní	k2eAgFnSc4d1
autobusovou	autobusový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
získaly	získat	k5eAaPmAgFnP
převahu	převaha	k1gFnSc4
nad	nad	k7c7
rovněž	rovněž	k9
státní	státní	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
poštovních	poštovní	k2eAgFnPc2d1
autobusových	autobusový	k2eAgFnPc2d1
linek	linka	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vznikala	vznikat	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
měly	mít	k5eAaImAgFnP
ČSD	ČSD	kA
v	v	k7c6
majetku	majetek	k1gInSc6
158	#num#	k4
autobusů	autobus	k1gInPc2
nasazených	nasazený	k2eAgInPc2d1
na	na	k7c6
79	#num#	k4
linkách	linka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1933	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
byla	být	k5eAaImAgFnS
poštovní	poštovní	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
začleněna	začleněn	k2eAgFnSc1d1
do	do	k7c2
ČSD	ČSD	kA
a	a	k8xC
státní	státní	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
sjednocena	sjednocen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dobu	doba	k1gFnSc4
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
provozovaly	provozovat	k5eAaImAgFnP
státní	státní	k2eAgFnPc1d1
linky	linka	k1gFnPc1
Českomoravské	českomoravský	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
a	a	k8xC
Slovenská	slovenský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
válce	válka	k1gFnSc6
přešla	přejít	k5eAaPmAgFnS
síť	síť	k1gFnSc1
opět	opět	k6eAd1
pod	pod	k7c7
ČSD	ČSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
byla	být	k5eAaImAgFnS
veškerá	veškerý	k3xTgFnSc1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
převedena	převést	k5eAaPmNgFnS
pod	pod	k7c4
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgInPc4d1
podniky	podnik	k1gInPc4
ČSAD	ČSAD	kA
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
po	po	k7c6
Mnichovské	mnichovský	k2eAgFnSc6d1
dohodě	dohoda	k1gFnSc6
a	a	k8xC
během	během	k7c2
protektorátu	protektorát	k1gInSc2
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
mnichovské	mnichovský	k2eAgFnSc6d1
dohodě	dohoda	k1gFnSc6
</s>
<s>
Po	po	k7c6
podepsání	podepsání	k1gNnSc6
Mnichovské	mnichovský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgInSc3,k3yIgInSc3,k3yQgInSc3
došlo	dojít	k5eAaPmAgNnS
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1938	#num#	k4
<g/>
,	,	kIx,
vyklidily	vyklidit	k5eAaPmAgFnP
ČSD	ČSD	kA
obsazená	obsazený	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Personál	personál	k1gInSc1
<g/>
,	,	kIx,
vozidla	vozidlo	k1gNnPc1
i	i	k8xC
vybavení	vybavení	k1gNnSc1
stanic	stanice	k1gFnPc2
byly	být	k5eAaImAgFnP
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
převezeny	převézt	k5eAaPmNgFnP
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
byly	být	k5eAaImAgFnP
ČSD	ČSD	kA
smluvně	smluvně	k6eAd1
zavázány	zavázat	k5eAaPmNgFnP
odvezený	odvezený	k2eAgInSc1d1
materiál	materiál	k1gInSc4
předat	předat	k5eAaPmF
Německým	německý	k2eAgFnPc3d1
říšským	říšský	k2eAgFnPc3d1
drahám	draha	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1938	#num#	k4
bylo	být	k5eAaImAgNnS
sjednáno	sjednán	k2eAgNnSc1d1
předání	předání	k1gNnSc1
877	#num#	k4
lokomotiv	lokomotiva	k1gFnPc2
<g/>
,	,	kIx,
136	#num#	k4
motorových	motorový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
158	#num#	k4
přívěsných	přívěsný	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
117	#num#	k4
rychlíkových	rychlíkový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
2160	#num#	k4
osobních	osobní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
a	a	k8xC
23	#num#	k4
500	#num#	k4
nákladních	nákladní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byly	být	k5eAaImAgInP
ČSD	ČSD	kA
povinny	povinen	k2eAgFnPc1d1
nahradit	nahradit	k5eAaPmF
škody	škoda	k1gFnPc4
na	na	k7c6
všech	všecek	k3xTgFnPc6
zničených	zničený	k2eAgFnPc6d1
nebo	nebo	k8xC
poškozených	poškozený	k2eAgNnPc6d1
drážních	drážní	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
rozdělení	rozdělení	k1gNnSc1
ČSD	ČSD	kA
</s>
<s>
Po	po	k7c6
začátku	začátek	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1939	#num#	k4
byly	být	k5eAaImAgInP
postupně	postupně	k6eAd1
odstaveny	odstavit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
motorové	motorový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
,	,	kIx,
protože	protože	k8xS
veškeré	veškerý	k3xTgFnPc1
pohonné	pohonný	k2eAgFnPc1d1
hmoty	hmota	k1gFnPc1
byly	být	k5eAaImAgFnP
využity	využít	k5eAaPmNgFnP
pro	pro	k7c4
vedení	vedení	k1gNnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
týkalo	týkat	k5eAaImAgNnS
i	i	k9
Slovenské	slovenský	k2eAgFnSc2d1
strely	strela	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
provoz	provoz	k1gInSc1
skončil	skončit	k5eAaPmAgInS
s	s	k7c7
odtržením	odtržení	k1gNnSc7
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
obsazením	obsazení	k1gNnSc7
zbytku	zbytek	k1gInSc2
Československa	Československo	k1gNnSc2
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
a	a	k8xC
zřízením	zřízení	k1gNnSc7
Protektorátu	protektorát	k1gInSc2
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
resp.	resp.	kA
vznikem	vznik	k1gInSc7
Slovenského	slovenský	k2eAgInSc2d1
státu	stát	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
rozdělení	rozdělení	k1gNnSc3
ČSD	ČSD	kA
na	na	k7c4
Českomoravské	českomoravský	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
–	–	k?
Protektoratsbahnen	Protektoratsbahnen	k1gInSc1
Böhmen	Böhmen	k1gInSc1
und	und	k?
Mähren	Mährna	k1gFnPc2
(	(	kIx(
<g/>
ČMD-BMB	ČMD-BMB	k1gFnSc1
–	–	k?
německá	německý	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
Böhmisch-Mährische	Böhmisch-Mährisch	k1gFnSc2
Bahn	Bahna	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Slovenské	slovenský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČMD-BMB	ČMD-BMB	k1gFnSc1
byly	být	k5eAaImAgFnP
sice	sice	k8xC
samostatnou	samostatný	k2eAgFnSc4d1
organizací	organizace	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
byly	být	k5eAaImAgFnP
podřízené	podřízený	k2eAgFnPc1d1
říšským	říšský	k2eAgFnPc3d1
drahám	draha	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Obnova	obnova	k1gFnSc1
tratí	trať	k1gFnPc2
a	a	k8xC
vozového	vozový	k2eAgInSc2d1
parku	park	k1gInSc2
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
–	–	k?
byly	být	k5eAaImAgFnP
obnoveny	obnoven	k2eAgFnPc1d1
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
nebyly	být	k5eNaImAgFnP
železnice	železnice	k1gFnPc1
příliš	příliš	k6eAd1
dotčeny	dotknout	k5eAaPmNgInP
válečnými	válečný	k2eAgFnPc7d1
škodami	škoda	k1gFnPc7
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
obnovit	obnovit	k5eAaPmF
provoz	provoz	k1gInSc4
na	na	k7c6
všech	všecek	k3xTgFnPc6
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanice	stanice	k1gFnSc1
byly	být	k5eAaImAgFnP
povinně	povinně	k6eAd1
pojmenovány	pojmenovat	k5eAaPmNgFnP
výlučně	výlučně	k6eAd1
česky	česky	k6eAd1
nebo	nebo	k8xC
slovensky	slovensky	k6eAd1
<g/>
,	,	kIx,
stanice	stanice	k1gFnSc1
jenom	jenom	k9
s	s	k7c7
německými	německý	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
byly	být	k5eAaImAgFnP
úředně	úředně	k6eAd1
přejmenovány	přejmenovat	k5eAaPmNgFnP
v	v	k7c6
letech	let	k1gInPc6
1946	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Československa	Československo	k1gNnSc2
se	se	k3xPyFc4
po	po	k7c6
válce	válka	k1gFnSc6
nacházelo	nacházet	k5eAaImAgNnS
mnoho	mnoho	k4c1
cizích	cizí	k2eAgFnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
německých	německý	k2eAgMnPc2d1
–	–	k?
například	například	k6eAd1
i	i	k9
elektrické	elektrický	k2eAgFnPc4d1
lokomotivy	lokomotiva	k1gFnPc4
evakuované	evakuovaný	k2eAgFnPc4d1
před	před	k7c7
postupující	postupující	k2eAgFnSc7d1
frontou	fronta	k1gFnSc7
ze	z	k7c2
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
vozidel	vozidlo	k1gNnPc2
zařadily	zařadit	k5eAaPmAgFnP
ČSD	ČSD	kA
do	do	k7c2
svého	svůj	k3xOyFgInSc2
parku	park	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc4
zabavil	zabavit	k5eAaPmAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
jako	jako	k8xS,k8xC
válečnou	válečný	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poválečných	poválečný	k2eAgInPc6d1
letech	let	k1gInPc6
pak	pak	k6eAd1
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
výměnám	výměna	k1gFnPc3
vozidel	vozidlo	k1gNnPc2
za	za	k7c4
původní	původní	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
ČSD	ČSD	kA
zavlečená	zavlečený	k2eAgFnSc1d1
do	do	k7c2
ciziny	cizina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
velkým	velký	k2eAgInSc7d1
elánem	elán	k1gInSc7
přikročily	přikročit	k5eAaPmAgFnP
ČSD	ČSD	kA
k	k	k7c3
poválečné	poválečný	k2eAgFnSc3d1
obnově	obnova	k1gFnSc3
vozidlového	vozidlový	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
1945	#num#	k4
opustila	opustit	k5eAaPmAgFnS
první	první	k4xOgFnSc1
nově	nově	k6eAd1
vyrobená	vyrobený	k2eAgFnSc1d1
lokomotiva	lokomotiva	k1gFnSc1
těžce	těžce	k6eAd1
poškozené	poškozený	k2eAgFnSc2d1
tovární	tovární	k2eAgFnSc2d1
haly	hala	k1gFnSc2
plzeňské	plzeňský	k2eAgFnSc2d1
Škodovky	škodovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
dodávaly	dodávat	k5eAaImAgFnP
firmy	firma	k1gFnPc1
Škoda	škoda	k1gFnSc1
a	a	k8xC
ČKD	ČKD	kA
množství	množství	k1gNnSc1
parních	parní	k2eAgFnPc2d1
lokomotiv	lokomotiva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
patřily	patřit	k5eAaImAgInP
k	k	k7c3
nejvýkonnějším	výkonný	k2eAgFnPc3d3
a	a	k8xC
nejmodernějším	moderní	k2eAgFnPc3d3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Elektrifikace	elektrifikace	k1gFnSc1
hlavních	hlavní	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
a	a	k8xC
postupné	postupný	k2eAgNnSc4d1
opouštění	opouštění	k1gNnSc4
parní	parní	k2eAgFnSc2d1
trakce	trakce	k1gFnSc2
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
ve	v	k7c6
stanici	stanice	k1gFnSc6
Jedlová	jedlový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
znovu	znovu	k6eAd1
objevovat	objevovat	k5eAaImF
plány	plán	k1gInPc4
na	na	k7c4
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
elektrizaci	elektrizace	k1gFnSc4
železniční	železniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brzy	brzy	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
projekt	projekt	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1943	#num#	k4
započaly	započnout	k5eAaPmAgFnP
práce	práce	k1gFnPc1
na	na	k7c4
elektrizaci	elektrizace	k1gFnSc4
sklonově	sklonově	k6eAd1
náročného	náročný	k2eAgInSc2d1
úseku	úsek	k1gInSc2
bývalé	bývalý	k2eAgFnSc2d1
Košicko-bohumínské	košicko-bohumínský	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úsek	úsek	k1gInSc1
Poprad-Tary	Poprad-Tara	k1gFnSc2
–	–	k?
Liptovský	liptovský	k2eAgInSc1d1
Mikuláš	mikuláš	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
dubnu	duben	k1gInSc6
1955	#num#	k4
a	a	k8xC
od	od	k7c2
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1956	#num#	k4
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
provoz	provoz	k1gInSc1
v	v	k7c6
celém	celý	k2eAgInSc6d1
úseku	úsek	k1gInSc6
Žilina	Žilina	k1gFnSc1
–	–	k?
Spišská	spišský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
napájení	napájení	k1gNnSc4
byla	být	k5eAaImAgFnS
zvolena	zvolen	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
3000	#num#	k4
Vss	Vss	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
upřednostňována	upřednostňovat	k5eAaImNgFnS
v	v	k7c6
Polsku	Polsko	k1gNnSc6
i	i	k8xC
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrizace	elektrizace	k1gFnSc1
rychle	rychle	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
<g/>
,	,	kIx,
takže	takže	k8xS
počátkem	počátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
již	již	k6eAd1
byl	být	k5eAaImAgInS
možný	možný	k2eAgInSc1d1
elektrický	elektrický	k2eAgInSc1d1
provoz	provoz	k1gInSc1
z	z	k7c2
Mostu	most	k1gInSc2
(	(	kIx(
<g/>
přes	přes	k7c4
Mělník	Mělník	k1gInSc4
a	a	k8xC
Nymburk	Nymburk	k1gInSc4
<g/>
)	)	kIx)
až	až	k9
do	do	k7c2
Košic	Košice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pražských	pražský	k2eAgFnPc6d1
spojkách	spojka	k1gFnPc6
bylo	být	k5eAaImAgNnS
přepnuto	přepnut	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
na	na	k7c4
3	#num#	k4
kVss	kVss	k6eAd1
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
možných	možný	k2eAgFnPc2d1
úspor	úspora	k1gFnPc2
mědi	měď	k1gFnSc2
díky	díky	k7c3
nižším	nízký	k2eAgInPc3d2
proudům	proud	k1gInPc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c4
elektrifikaci	elektrifikace	k1gFnSc4
systémem	systém	k1gInSc7
25	#num#	k4
kV	kV	k?
50	#num#	k4
Hz	Hz	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
především	především	k6eAd1
ve	v	k7c6
Francii	Francie	k1gFnSc6
jevil	jevit	k5eAaImAgInS
jako	jako	k9
velmi	velmi	k6eAd1
perspektivní	perspektivní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
přineslo	přinést	k5eAaPmAgNnS
komplikace	komplikace	k1gFnPc4
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
obě	dva	k4xCgFnPc1
napájecí	napájecí	k2eAgFnPc1d1
soustavy	soustava	k1gFnPc1
(	(	kIx(
<g/>
3	#num#	k4
a	a	k8xC
25	#num#	k4
kV	kV	k?
<g/>
)	)	kIx)
setkaly	setkat	k5eAaPmAgFnP
v	v	k7c6
Kutné	kutný	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
dodány	dodán	k2eAgInPc1d1
první	první	k4xOgFnPc1
dvousystémové	dvousystémový	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
řady	řada	k1gFnSc2
ES	ES	kA
499.0	499.0	k4
zde	zde	k6eAd1
musely	muset	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
vlaky	vlak	k1gInPc1
přepřahat	přepřahat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poválečném	poválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
také	také	k9
pokračovaly	pokračovat	k5eAaImAgFnP
práce	práce	k1gFnPc1
na	na	k7c6
zdvojkolejnění	zdvojkolejnění	k1gNnSc6
mnoha	mnoho	k4c2
důležitých	důležitý	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
<g/>
,	,	kIx,
především	především	k9
pro	pro	k7c4
nákladní	nákladní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nákladní	nákladní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
prioritou	priorita	k1gFnSc7
státní	státní	k2eAgFnSc2d1
dopravní	dopravní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
úsecích	úsek	k1gInPc6
byla	být	k5eAaImAgFnS
dokonce	dokonce	k9
kvůli	kvůli	k7c3
uvolnění	uvolnění	k1gNnSc3
kapacity	kapacita	k1gFnSc2
pro	pro	k7c4
nákladní	nákladní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
osobní	osobní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
zcela	zcela	k6eAd1
zastavena	zastavit	k5eAaPmNgFnS
a	a	k8xC
převzata	převzít	k5eAaPmNgFnS
ČSAD	ČSAD	kA
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
technický	technický	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
drážní	drážní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
zastavil	zastavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
v	v	k7c6
západoevropských	západoevropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byla	být	k5eAaImAgFnS
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
vlaků	vlak	k1gInPc2
zvyšována	zvyšovat	k5eAaImNgFnS
ze	z	k7c2
120	#num#	k4
na	na	k7c4
160	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
u	u	k7c2
ČSD	ČSD	kA
byla	být	k5eAaImAgFnS
na	na	k7c6
zhlavích	zhlaví	k1gNnPc6
železničních	železniční	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
snižována	snižován	k2eAgFnSc1d1
ze	z	k7c2
120	#num#	k4
na	na	k7c4
100	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatečné	dostatečný	k2eNgFnSc2d1
údržby	údržba	k1gFnSc2
výhybek	výhybka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1988	#num#	k4
a	a	k8xC
1989	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
několika	několik	k4yIc6
málo	málo	k6eAd1
úsecích	úsek	k1gInPc6
(	(	kIx(
<g/>
Zaječí	zaječí	k2eAgFnPc4d1
–	–	k?
Šakvice	Šakvice	k1gFnPc4
a	a	k8xC
mezi	mezi	k7c7
Olomoucí	Olomouc	k1gFnSc7
a	a	k8xC
Zábřehem	Zábřeh	k1gInSc7
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
)	)	kIx)
zvýšena	zvýšen	k2eAgFnSc1d1
traťová	traťový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
na	na	k7c4
140	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Novější	nový	k2eAgFnPc1d2
technologie	technologie	k1gFnPc1
jako	jako	k8xS,k8xC
vratné	vratný	k2eAgFnPc1d1
soupravy	souprava	k1gFnPc1
s	s	k7c7
řídicími	řídicí	k2eAgInPc7d1
vozy	vůz	k1gInPc7
nebyly	být	k5eNaImAgFnP
zavedeny	zaveden	k2eAgFnPc1d1
vůbec	vůbec	k9
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
ovšem	ovšem	k9
nedošlo	dojít	k5eNaPmAgNnS
ani	ani	k9
k	k	k7c3
podstatné	podstatný	k2eAgFnSc3d1
redukci	redukce	k1gFnSc3
kolejové	kolejový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
si	se	k3xPyFc3
Československo	Československo	k1gNnSc1
zachovalo	zachovat	k5eAaPmAgNnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejhustších	hustý	k2eAgFnPc2d3
železničních	železniční	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Druhé	druhý	k4xOgNnSc1
rozdělení	rozdělení	k1gNnSc1
ČSD	ČSD	kA
</s>
<s>
Podruhé	podruhé	k6eAd1
a	a	k8xC
tentokrát	tentokrát	k6eAd1
již	již	k6eAd1
definitivně	definitivně	k6eAd1
zanikly	zaniknout	k5eAaPmAgFnP
ČSD	ČSD	kA
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc3
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
připravovaným	připravovaný	k2eAgNnSc7d1
rozdělením	rozdělení	k1gNnSc7
federace	federace	k1gFnSc2
přijalo	přijmout	k5eAaPmAgNnS
Federální	federální	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
zákon	zákon	k1gInSc1
č.	č.	k?
625	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
zániku	zánik	k1gInSc6
státní	státní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
mj.	mj.	kA
určil	určit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
veškeré	veškerý	k3xTgFnPc4
nemovité	movitý	k2eNgFnPc4d1
věci	věc	k1gFnPc4
z	z	k7c2
majetku	majetek	k1gInSc2
ČSD	ČSD	kA
přejdou	přejít	k5eAaPmIp3nP
do	do	k7c2
majetku	majetek	k1gInSc2
toho	ten	k3xDgInSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
Česka	Česko	k1gNnSc2
nebo	nebo	k8xC
Slovenska	Slovensko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jejichž	jejichž	k3xOyRp3gNnSc6
území	území	k1gNnSc6
leží	ležet	k5eAaImIp3nS
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInSc1d1
majetek	majetek	k1gInSc1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
vlastnictví	vlastnictví	k1gNnSc2
Česka	Česko	k1gNnSc2
nebo	nebo	k8xC
Slovenska	Slovensko	k1gNnSc2
v	v	k7c6
poměru	poměr	k1gInSc6
dvě	dva	k4xCgFnPc1
ku	k	k7c3
jedné	jeden	k4xCgFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgInPc1d1
státy	stát	k1gInPc1
pak	pak	k6eAd1
rozdělený	rozdělený	k2eAgInSc4d1
majetek	majetek	k1gInSc4
začlenily	začlenit	k5eAaPmAgFnP
do	do	k7c2
nově	nově	k6eAd1
ustavených	ustavený	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
do	do	k7c2
Českých	český	k2eAgFnPc2d1
drah	draha	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
do	do	k7c2
Železnic	železnice	k1gFnPc2
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Československé	československý	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Z	z	k7c2
technické	technický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
státní	státní	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Grégr	Grégr	k1gMnSc1
<g/>
,	,	kIx,
15.3	15.3	k4
<g/>
.1932	.1932	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
72	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
75	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.zelpage.cz/zpravy/7062#	http://www.zelpage.cz/zpravy/7062#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
<g/>
:	:	kIx,
<g/>
Hnací	hnací	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
ČSD	ČSD	kA
</s>
<s>
Historie	historie	k1gFnSc1
železniční	železniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Československé	československý	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Národní	národní	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
BDŽ	BDŽ	kA
•	•	k?
BR	br	k0
•	•	k?
BČ	BČ	kA
•	•	k?
CFL	CFL	kA
•	•	k?
CFM	CFM	kA
•	•	k?
CFR	CFR	kA
•	•	k?
CP	CP	kA
•	•	k?
ČD	ČD	kA
•	•	k?
DB	db	kA
•	•	k?
DSB	DSB	kA
•	•	k?
ELRON	ELRON	kA
•	•	k?
FS	FS	kA
•	•	k?
HŽ	HŽ	kA
•	•	k?
LDz	LDz	k1gFnSc2
•	•	k?
LG	LG	kA
•	•	k?
MÁV	MÁV	kA
•	•	k?
MŽ	MŽ	kA
•	•	k?
NS	NS	kA
•	•	k?
NSB	NSB	kA
•	•	k?
OSE	osa	k1gFnSc6
•	•	k?
ÖBB	ÖBB	kA
•	•	k?
PKP	PKP	kA
•	•	k?
RENFE	RENFE	kA
•	•	k?
RŽD	RŽD	kA
•	•	k?
SBB	SBB	kA
•	•	k?
SJ	SJ	kA
•	•	k?
SNCB	SNCB	kA
•	•	k?
SNCF	SNCF	kA
•	•	k?
SŽ	SŽ	kA
•	•	k?
UZ	UZ	kA
•	•	k?
VR	vr	k0
•	•	k?
ŽSR	ŽSR	kA
<g/>
,	,	kIx,
ZSSK	ZSSK	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
20010092602	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
154090927	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
|	|	kIx~
Železnice	železnice	k1gFnSc1
</s>
