<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
příslušníků	příslušník	k1gMnPc2	příslušník
československého	československý	k2eAgNnSc2d1	Československé
letectva	letectvo	k1gNnSc2	letectvo
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
britské	britský	k2eAgInPc1d1	britský
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
hrála	hrát	k5eAaImAgFnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
německému	německý	k2eAgInSc3d1	německý
nacismu	nacismus	k1gInSc3	nacismus
<g/>
;	;	kIx,	;
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
smutnější	smutný	k2eAgFnSc1d2	smutnější
je	být	k5eAaImIp3nS	být
osud	osud	k1gInSc4	osud
a	a	k8xC	a
pronásledování	pronásledování	k1gNnSc1	pronásledování
těchto	tento	k3xDgMnPc2	tento
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
komunistickém	komunistický	k2eAgNnSc6d1	komunistické
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
doma	doma	k6eAd1	doma
dostalo	dostat	k5eAaPmAgNnS	dostat
uznání	uznání	k1gNnSc1	uznání
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
československými	československý	k2eAgFnPc7d1	Československá
perutěmi	peruť	k1gFnPc7	peruť
RAF	raf	k0	raf
prošlo	projít	k5eAaPmAgNnS	projít
2500	[number]	k4	2500
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odchod	odchod	k1gInSc1	odchod
do	do	k7c2	do
odboje	odboj	k1gInSc2	odboj
==	==	k?	==
</s>
</p>
<p>
<s>
Odchod	odchod	k1gInSc1	odchod
do	do	k7c2	do
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
nebyl	být	k5eNaImAgInS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
letci	letec	k1gMnPc1	letec
byli	být	k5eAaImAgMnP	být
iritováni	iritovat	k5eAaImNgMnP	iritovat
počátečním	počáteční	k2eAgInSc7d1	počáteční
sklonem	sklon	k1gInSc7	sklon
části	část	k1gFnSc2	část
velení	velení	k1gNnSc2	velení
<g/>
,	,	kIx,	,
pozitivně	pozitivně	k6eAd1	pozitivně
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
nabídku	nabídka	k1gFnSc4	nabídka
maršála	maršál	k1gMnSc4	maršál
Hermanna	Hermann	k1gMnSc4	Hermann
Göringa	Göring	k1gMnSc4	Göring
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
asi	asi	k9	asi
1	[number]	k4	1
500	[number]	k4	500
pilotů	pilot	k1gMnPc2	pilot
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
specialistů	specialista	k1gMnPc2	specialista
do	do	k7c2	do
německého	německý	k2eAgNnSc2d1	německé
letectva	letectvo	k1gNnSc2	letectvo
po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
protektorátu	protektorát	k1gInSc2	protektorát
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
mělo	mít	k5eAaImAgNnS	mít
dokonce	dokonce	k9	dokonce
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
převodům	převod	k1gInPc3	převod
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
posádek	posádka	k1gFnPc2	posádka
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
provedení	provedení	k1gNnSc1	provedení
útěku	útěk	k1gInSc2	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
letců	letec	k1gMnPc2	letec
odešlo	odejít	k5eAaPmAgNnS	odejít
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
polské	polský	k2eAgInPc1d1	polský
úřady	úřad	k1gInPc1	úřad
nejevily	jevit	k5eNaImAgInP	jevit
ochotu	ochota	k1gFnSc4	ochota
případ	případ	k1gInSc1	případ
řešit	řešit	k5eAaImF	řešit
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
vojáků	voják	k1gMnPc2	voják
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
cizinecké	cizinecký	k2eAgFnSc2d1	cizinecká
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
si	se	k3xPyFc3	se
i	i	k8xC	i
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
uvědomovala	uvědomovat	k5eAaImAgFnS	uvědomovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jí	on	k3xPp3gFnSc7	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
od	od	k7c2	od
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
pilotů	pilot	k1gMnPc2	pilot
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosud	dosud	k6eAd1	dosud
neodešla	odejít	k5eNaPmAgFnS	odejít
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
přijata	přijat	k2eAgFnSc1d1	přijata
do	do	k7c2	do
polského	polský	k2eAgNnSc2d1	polské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gNnSc1	jejich
bojové	bojový	k2eAgNnSc1d1	bojové
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
,	,	kIx,	,
rychlá	rychlý	k2eAgFnSc1d1	rychlá
porážka	porážka	k1gFnSc1	porážka
Polska	Polsko	k1gNnSc2	Polsko
znamenala	znamenat	k5eAaImAgFnS	znamenat
brzké	brzký	k2eAgNnSc4d1	brzké
ukončení	ukončení	k1gNnSc4	ukončení
jejich	jejich	k3xOp3gFnSc2	jejich
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
piloti	pilot	k1gMnPc1	pilot
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
polskými	polský	k2eAgMnPc7d1	polský
kolegy	kolega	k1gMnPc7	kolega
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
Střední	střední	k2eAgInSc4d1	střední
východ	východ	k1gInSc4	východ
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
nejdříve	dříve	k6eAd3	dříve
internováni	internován	k2eAgMnPc1d1	internován
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
intervenci	intervence	k1gFnSc6	intervence
československých	československý	k2eAgInPc2d1	československý
orgánů	orgán	k1gInPc2	orgán
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
byli	být	k5eAaImAgMnP	být
přesunováni	přesunovat	k5eAaImNgMnP	přesunovat
k	k	k7c3	k
jednotkám	jednotka	k1gFnPc3	jednotka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Polska	Polska	k1gFnSc1	Polska
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jedinou	jediný	k2eAgFnSc7d1	jediná
cestou	cesta	k1gFnSc7	cesta
pro	pro	k7c4	pro
české	český	k2eAgMnPc4d1	český
emigranty	emigrant	k1gMnPc4	emigrant
na	na	k7c4	na
západ	západ	k1gInSc4	západ
trasa	trasa	k1gFnSc1	trasa
z	z	k7c2	z
Protektorátu	protektorát	k1gInSc2	protektorát
přes	přes	k7c4	přes
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
dopadení	dopadení	k1gNnSc6	dopadení
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vydání	vydání	k1gNnSc1	vydání
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
se	se	k3xPyFc4	se
čeští	český	k2eAgMnPc1d1	český
letci	letec	k1gMnPc1	letec
i	i	k8xC	i
vojáci	voják	k1gMnPc1	voják
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k6eAd1	až
při	při	k7c6	při
překročení	překročení	k1gNnSc6	překročení
hranic	hranice	k1gFnPc2	hranice
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
československý	československý	k2eAgMnSc1d1	československý
konzul	konzul	k1gMnSc1	konzul
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
jejich	jejich	k3xOp3gInSc4	jejich
převoz	převoz	k1gInSc4	převoz
do	do	k7c2	do
Bejrútu	Bejrút	k1gInSc2	Bejrút
a	a	k8xC	a
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Francie	Francie	k1gFnSc2	Francie
1939	[number]	k4	1939
-	-	kIx~	-
1940	[number]	k4	1940
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
leteckých	letecký	k2eAgFnPc2d1	letecká
jednotek	jednotka	k1gFnPc2	jednotka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgInS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
řadou	řada	k1gFnSc7	řada
byrokratických	byrokratický	k2eAgInPc2d1	byrokratický
průtahů	průtah	k1gInPc2	průtah
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc4	několik
čs	čs	kA	čs
<g/>
.	.	kIx.	.
letců	letec	k1gMnPc2	letec
včleněno	včleněn	k2eAgNnSc1d1	včleněno
do	do	k7c2	do
francouzských	francouzský	k2eAgFnPc2d1	francouzská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
samostatných	samostatný	k2eAgFnPc2d1	samostatná
československých	československý	k2eAgFnPc2d1	Československá
stíhacích	stíhací	k2eAgFnPc2d1	stíhací
a	a	k8xC	a
bombardovacích	bombardovací	k2eAgFnPc2d1	bombardovací
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Vpádem	vpád	k1gInSc7	vpád
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
Beneluxu	Benelux	k1gInSc2	Benelux
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
skončilo	skončit	k5eAaPmAgNnS	skončit
období	období	k1gNnSc1	období
tzv.	tzv.	kA	tzv.
podivné	podivný	k2eAgFnSc2d1	podivná
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
vtrhly	vtrhnout	k5eAaPmAgFnP	vtrhnout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
začínala	začínat	k5eAaImAgFnS	začínat
být	být	k5eAaImF	být
vážná	vážný	k2eAgFnSc1d1	vážná
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
leteckých	letecký	k2eAgFnPc2d1	letecká
jednotek	jednotka	k1gFnPc2	jednotka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byla	být	k5eAaImAgFnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
eskadra	eskadra	k1gFnSc1	eskadra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
sloužilo	sloužit	k5eAaImAgNnS	sloužit
18	[number]	k4	18
čs	čs	kA	čs
<g/>
.	.	kIx.	.
letců	letec	k1gMnPc2	letec
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
kapitán	kapitán	k1gMnSc1	kapitán
Alois	Alois	k1gMnSc1	Alois
Vašátko	Vašátko	k1gMnSc1	Vašátko
či	či	k8xC	či
poručík	poručík	k1gMnSc1	poručík
František	František	k1gMnSc1	František
Peřina	Peřina	k1gMnSc1	Peřina
<g/>
.	.	kIx.	.
</s>
<s>
Naši	náš	k3xOp1gMnPc1	náš
letci	letec	k1gMnPc1	letec
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
a	a	k8xC	a
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
elitním	elitní	k2eAgMnPc3d1	elitní
stíhačům	stíhač	k1gMnPc3	stíhač
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
čs	čs	kA	čs
<g/>
.	.	kIx.	.
<g/>
-francouzská	rancouzský	k2eAgFnSc1d1	-francouzský
letecká	letecký	k2eAgFnSc1d1	letecká
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
samostatného	samostatný	k2eAgNnSc2d1	samostatné
československého	československý	k2eAgNnSc2d1	Československé
letectva	letectvo	k1gNnSc2	letectvo
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
rychlá	rychlý	k2eAgFnSc1d1	rychlá
kapitulace	kapitulace	k1gFnSc1	kapitulace
Francie	Francie	k1gFnSc2	Francie
její	její	k3xOp3gFnSc1	její
realizaci	realizace	k1gFnSc4	realizace
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
Povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
zřídit	zřídit	k5eAaPmF	zřídit
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc4d1	stíhací
letku	letka	k1gFnSc4	letka
u	u	k7c2	u
GC	GC	kA	GC
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
kapitána	kapitán	k1gMnSc2	kapitán
Kulhánka	Kulhánek	k1gMnSc2	Kulhánek
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
130	[number]	k4	130
čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhačů	stíhač	k1gMnPc2	stíhač
<g/>
.	.	kIx.	.
</s>
<s>
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
letci	letec	k1gMnPc1	letec
byli	být	k5eAaImAgMnP	být
přesunuti	přesunout	k5eAaPmNgMnP	přesunout
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
bojům	boj	k1gInPc3	boj
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Začlenění	začlenění	k1gNnPc4	začlenění
do	do	k7c2	do
Královského	královský	k2eAgNnSc2d1	královské
letectva	letectvo	k1gNnSc2	letectvo
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
začaly	začít	k5eAaPmAgInP	začít
rychle	rychle	k6eAd1	rychle
vznikat	vznikat	k5eAaImF	vznikat
útvary	útvar	k1gInPc4	útvar
československých	československý	k2eAgMnPc2d1	československý
letců	letec	k1gMnPc2	letec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
před	před	k7c7	před
podepsáním	podepsání	k1gNnSc7	podepsání
příslušné	příslušný	k2eAgFnSc2d1	příslušná
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
útvary	útvar	k1gInPc4	útvar
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
RAF	raf	k0	raf
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
případech	případ	k1gInPc6	případ
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
útvarech	útvar	k1gInPc6	útvar
sloužili	sloužit	k5eAaImAgMnP	sloužit
i	i	k9	i
britští	britský	k2eAgMnPc1d1	britský
příslušníci	příslušník	k1gMnPc1	příslušník
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
českoslovenští	československý	k2eAgMnPc1d1	československý
letci	letec	k1gMnPc1	letec
a	a	k8xC	a
příslušníci	příslušník	k1gMnPc1	příslušník
posádky	posádka	k1gFnSc2	posádka
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
čistě	čistě	k6eAd1	čistě
britských	britský	k2eAgInPc6d1	britský
útvarech	útvar	k1gInPc6	útvar
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
československo-britská	československoritský	k2eAgFnSc1d1	československo-britský
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
branné	branný	k2eAgFnSc6d1	Branná
moci	moc	k1gFnSc6	moc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kromě	kromě	k7c2	kromě
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
československých	československý	k2eAgFnPc2d1	Československá
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
upravovala	upravovat	k5eAaImAgFnS	upravovat
také	také	k9	také
způsob	způsob	k1gInSc4	způsob
jejich	jejich	k3xOp3gNnSc2	jejich
financování	financování	k1gNnSc2	financování
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
Finanční	finanční	k2eAgFnSc1d1	finanční
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Československou	československý	k2eAgFnSc7d1	Československá
prozatímní	prozatímní	k2eAgFnSc7d1	prozatímní
vládou	vláda	k1gFnSc7	vláda
o	o	k7c4	o
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
válečného	válečný	k2eAgInSc2d1	válečný
úvěru	úvěr	k1gInSc2	úvěr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následně	následně	k6eAd1	následně
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
financování	financování	k1gNnSc3	financování
všech	všecek	k3xTgMnPc2	všecek
československých	československý	k2eAgMnPc2d1	československý
příslušníků	příslušník	k1gMnPc2	příslušník
branné	branný	k2eAgFnSc2d1	Branná
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Perutě	peruť	k1gFnSc2	peruť
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
vlády	vláda	k1gFnSc2	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
sestavila	sestavit	k5eAaPmAgFnS	sestavit
tyto	tento	k3xDgInPc4	tento
útvary	útvar	k1gInPc4	útvar
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
310	[number]	k4	310
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
310	[number]	k4	310
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Duxford	Duxford	k1gInSc1	Duxford
</s>
</p>
<p>
<s>
311	[number]	k4	311
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
bombardovací	bombardovací	k2eAgFnSc1d1	bombardovací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
311	[number]	k4	311
Bombing	Bombing	k1gInSc1	Bombing
Squadron	Squadron	k1gInSc4	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Honington	Honington	k1gInSc1	Honington
</s>
</p>
<p>
<s>
312	[number]	k4	312
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
312	[number]	k4	312
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Duxford	Duxford	k1gInSc1	Duxford
</s>
</p>
<p>
<s>
313	[number]	k4	313
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
<g/>
(	(	kIx(	(
<g/>
No	no	k9	no
313	[number]	k4	313
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
CatterickPozději	CatterickPozději	k1gFnSc1	CatterickPozději
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
stávajících	stávající	k2eAgFnPc2d1	stávající
stíhacích	stíhací	k2eAgFnPc2d1	stíhací
perutí	peruť	k1gFnPc2	peruť
(	(	kIx(	(
<g/>
310	[number]	k4	310
<g/>
,	,	kIx,	,
312	[number]	k4	312
a	a	k8xC	a
313	[number]	k4	313
<g/>
)	)	kIx)	)
v	v	k7c6	v
Exeteru	Exeter	k1gInSc6	Exeter
vytvořeno	vytvořen	k2eAgNnSc4d1	vytvořeno
vlastní	vlastní	k2eAgNnSc4d1	vlastní
stíhací	stíhací	k2eAgNnSc4d1	stíhací
křídlo	křídlo	k1gNnSc4	křídlo
(	(	kIx(	(
<g/>
Czechoslovak	Czechoslovak	k1gMnSc1	Czechoslovak
Fighter	fighter	k1gMnSc1	fighter
Wing	Wing	k1gMnSc1	Wing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k6eAd1	mimo
těchto	tento	k3xDgFnPc2	tento
československých	československý	k2eAgFnPc2d1	Československá
perutí	peruť	k1gFnPc2	peruť
sloužili	sloužit	k5eAaImAgMnP	sloužit
českoslovenští	československý	k2eAgMnPc1d1	československý
vojáci	voják	k1gMnPc1	voják
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
britských	britský	k2eAgInPc6d1	britský
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
polské	polský	k2eAgFnSc2d1	polská
peruti	peruť	k1gFnSc2	peruť
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
1	[number]	k4	1
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnPc1d1	používající
většinou	většina	k1gFnSc7	většina
stroje	stroj	k1gInSc2	stroj
Hawker	Hawker	k1gInSc1	Hawker
Hurricane	Hurrican	k1gMnSc5	Hurrican
<g/>
;	;	kIx,	;
v	v	k7c6	v
peruti	peruť	k1gFnSc6	peruť
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
na	na	k7c4	na
30	[number]	k4	30
československých	československý	k2eAgMnPc2d1	československý
letců	letec	k1gMnPc2	letec
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
proslavený	proslavený	k2eAgMnSc1d1	proslavený
Karel	Karel	k1gMnSc1	Karel
Kuttelwascher	Kuttelwaschra	k1gFnPc2	Kuttelwaschra
(	(	kIx(	(
<g/>
18	[number]	k4	18
sestřelů	sestřel	k1gInPc2	sestřel
<g/>
,	,	kIx,	,
5	[number]	k4	5
dalších	další	k2eAgFnPc2d1	další
pravděpodobných	pravděpodobný	k2eAgFnPc2d1	pravděpodobná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
68	[number]	k4	68
<g/>
.	.	kIx.	.
noční	noční	k2eAgFnSc1d1	noční
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
68	[number]	k4	68
Night	Night	k1gMnSc1	Night
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Catterick	Cattericka	k1gFnPc2	Cattericka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
dle	dle	k7c2	dle
čísla	číslo	k1gNnSc2	číslo
britskou	britský	k2eAgFnSc7d1	britská
perutí	peruť	k1gFnSc7	peruť
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
dvou	dva	k4xCgFnPc2	dva
letek	letka	k1gFnPc2	letka
(	(	kIx(	(
<g/>
flight	flight	k1gInSc1	flight
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
však	však	k9	však
československé	československý	k2eAgMnPc4d1	československý
letce	letec	k1gMnPc4	letec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vybrali	vybrat	k5eAaPmAgMnP	vybrat
znak	znak	k1gInSc4	znak
perutě	peruť	k1gFnSc2	peruť
(	(	kIx(	(
<g/>
sovu	sova	k1gFnSc4	sova
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
nočního	noční	k2eAgInSc2d1	noční
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
heslo	heslo	k1gNnSc4	heslo
"	"	kIx"	"
<g/>
Vždy	vždy	k6eAd1	vždy
připraven	připravit	k5eAaPmNgInS	připravit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gFnPc2	jejichž
řad	řada	k1gFnPc2	řada
pocházel	pocházet	k5eAaImAgMnS	pocházet
i	i	k9	i
první	první	k4xOgMnSc1	první
velitel	velitel	k1gMnSc1	velitel
kapitán	kapitán	k1gMnSc1	kapitán
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
;	;	kIx,	;
československá	československý	k2eAgFnSc1d1	Československá
letka	letka	k1gFnSc1	letka
B	B	kA	B
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
</s>
</p>
<p>
<s>
111	[number]	k4	111
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
111	[number]	k4	111
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
působící	působící	k2eAgFnSc1d1	působící
peruť	peruť	k1gFnSc1	peruť
<g/>
,	,	kIx,	,
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
letouny	letoun	k1gInPc4	letoun
Hawker	Hawker	k1gInSc1	Hawker
Hurricane	Hurrican	k1gInSc5	Hurrican
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
peruti	peruť	k1gFnSc6	peruť
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
letci	letec	k1gMnSc3	letec
J.	J.	kA	J.
Mansfeld	Mansfeld	k1gInSc1	Mansfeld
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Kučera	Kučera	k1gMnSc1	Kučera
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
122	[number]	k4	122
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
122	[number]	k4	122
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnSc1d1	působící
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
Hornchurch	Hornchurch	k1gInSc1	Hornchurch
(	(	kIx(	(
<g/>
Essex	Essex	k1gInSc1	Essex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
Spitfiry	Spitfir	k1gInPc7	Spitfir
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
doprovázet	doprovázet	k5eAaImF	doprovázet
britské	britský	k2eAgInPc4d1	britský
a	a	k8xC	a
americké	americký	k2eAgInPc4d1	americký
bombardéry	bombardér	k1gInPc4	bombardér
při	při	k7c6	při
náletech	nálet	k1gInPc6	nálet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známých	známý	k2eAgInPc2d1	známý
letcům	letec	k1gMnPc3	letec
zde	zde	k6eAd1	zde
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k9	i
Otto	Otto	k1gMnSc1	Otto
Smik	Smik	k1gMnSc1	Smik
<g/>
,	,	kIx,	,
T.	T.	kA	T.
Kruml	Kruml	k1gInSc1	Kruml
a	a	k8xC	a
František	František	k1gMnSc1	František
Fajtl	Fajtl	k1gMnSc1	Fajtl
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
velitel	velitel	k1gMnSc1	velitel
perutě	peruť	k1gFnSc2	peruť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
138	[number]	k4	138
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
výsadková	výsadkový	k2eAgFnSc1d1	výsadková
<g/>
)	)	kIx)	)
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
138	[number]	k4	138
Special	Special	k1gMnSc1	Special
duties	duties	k1gMnSc1	duties
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
(	(	kIx(	(
<g/>
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
oddělení	oddělení	k1gNnPc2	oddělení
SOE	SOE	kA	SOE
–	–	k?	–
Special	Special	k1gInSc1	Special
Operations	Operations	k1gInSc1	Operations
Executive	Executiv	k1gInSc5	Executiv
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
Newmarket	Newmarket	k1gInSc4	Newmarket
přeformována	přeformován	k2eAgFnSc1d1	přeformován
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
s	s	k7c7	s
účelem	účel	k1gInSc7	účel
provádět	provádět	k5eAaImF	provádět
záškodnické	záškodnický	k2eAgFnPc4d1	záškodnická
akce	akce	k1gFnPc4	akce
v	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
;	;	kIx,	;
peruť	peruť	k1gFnSc1	peruť
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
posádkou	posádka	k1gFnSc7	posádka
létala	létat	k5eAaImAgNnP	létat
stroji	stroj	k1gInSc3	stroj
Whitley	Whitle	k2eAgFnPc4d1	Whitle
<g/>
,	,	kIx,	,
Lysander	Lysander	k1gInSc1	Lysander
<g/>
,	,	kIx,	,
Halifax	Halifax	k1gInSc1	Halifax
a	a	k8xC	a
Stirling	Stirling	k1gInSc1	Stirling
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
tato	tento	k3xDgFnSc1	tento
peruť	peruť	k1gFnSc1	peruť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provedla	provést	k5eAaPmAgFnS	provést
výsadek	výsadek	k1gInSc4	výsadek
československých	československý	k2eAgMnPc2d1	československý
parašutistů	parašutista	k1gMnPc2	parašutista
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1941	[number]	k4	1941
s	s	k7c7	s
účelem	účel	k1gInSc7	účel
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
222	[number]	k4	222
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
222	[number]	k4	222
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
O.	O.	kA	O.
Smik	Smik	k1gInSc1	Smik
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
československými	československý	k2eAgMnPc7d1	československý
letci	letec	k1gMnPc7	letec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
303	[number]	k4	303
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polská	polský	k2eAgFnSc1d1	polská
<g/>
)	)	kIx)	)
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
303	[number]	k4	303
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřízená	zřízený	k2eAgFnSc1d1	zřízená
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Northolt	Northoltum	k1gNnPc2	Northoltum
1940	[number]	k4	1940
jako	jako	k8xC	jako
polská	polský	k2eAgFnSc1d1	polská
jednotka	jednotka	k1gFnSc1	jednotka
RAF	raf	k0	raf
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Anglii	Anglie	k1gFnSc6	Anglie
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
perutí	peruť	k1gFnSc7	peruť
RAF	raf	k0	raf
<g/>
;	;	kIx,	;
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
československých	československý	k2eAgMnPc2d1	československý
letců	letec	k1gMnPc2	letec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
bojovali	bojovat	k5eAaImAgMnP	bojovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nejznámější	známý	k2eAgMnSc1d3	nejznámější
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
17	[number]	k4	17
jistých	jistý	k2eAgInPc2d1	jistý
sestřelů	sestřel	k1gInPc2	sestřel
německých	německý	k2eAgNnPc2d1	německé
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
pravděpodobného	pravděpodobný	k2eAgMnSc2d1	pravděpodobný
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
spojeneckým	spojenecký	k2eAgInSc7d1	spojenecký
pilotem	pilot	k1gInSc7	pilot
celé	celý	k2eAgFnSc2d1	celá
Bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
255	[number]	k4	255
<g/>
.	.	kIx.	.
noční	noční	k2eAgFnSc1d1	noční
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
255	[number]	k4	255
Night	Night	k1gMnSc1	Night
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
96	[number]	k4	96
<g/>
.	.	kIx.	.
noční	noční	k2eAgFnSc1d1	noční
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
96	[number]	k4	96
Night	Night	k1gMnSc1	Night
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
601	[number]	k4	601
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
601	[number]	k4	601
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k8xC	jako
County	Counta	k1gFnPc1	Counta
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
sloužil	sloužit	k5eAaImAgMnS	sloužit
Jiří	Jiří	k1gMnSc1	Jiří
Maňák	Maňák	k1gMnSc1	Maňák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
198	[number]	k4	198
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
(	(	kIx(	(
<g/>
No	no	k9	no
198	[number]	k4	198
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
)	)	kIx)	)
sloužil	sloužit	k5eAaImAgInS	sloužit
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
např.	např.	kA	např.
Jiří	Jiří	k1gMnSc1	Jiří
Maňák	Maňák	k1gMnSc1	Maňák
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
létala	létat	k5eAaImAgFnS	létat
na	na	k7c6	na
strojích	stroj	k1gInPc6	stroj
Hawker	Hawker	k1gMnSc1	Hawker
Typhoon	Typhoon	k1gMnSc1	Typhoon
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
letci	letec	k1gMnPc1	letec
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
Maintenance	Maintenanec	k1gMnSc2	Maintenanec
unit	unit	k1gMnSc1	unit
a	a	k8xC	a
Ferry	Ferro	k1gNnPc7	Ferro
Pool	Pool	k1gInSc1	Pool
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
přelétávat	přelétávat	k5eAaImF	přelétávat
s	s	k7c7	s
opravenými	opravený	k2eAgNnPc7d1	opravené
letadly	letadlo	k1gNnPc7	letadlo
z	z	k7c2	z
dílen	dílna	k1gFnPc2	dílna
k	k	k7c3	k
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
tam	tam	k6eAd1	tam
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
Šeda	Šeda	k1gMnSc1	Šeda
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Doktor	doktor	k1gMnSc1	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velitelství	velitelství	k1gNnSc6	velitelství
Ferry	Ferro	k1gNnPc7	Ferro
Command	Command	k1gInSc1	Command
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
přelétávat	přelétávat	k5eAaImF	přelétávat
s	s	k7c7	s
letadly	letadlo	k1gNnPc7	letadlo
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
létající	létající	k2eAgInSc1d1	létající
personál	personál	k1gInSc1	personál
10	[number]	k4	10
československých	československý	k2eAgMnPc2d1	československý
pilotů	pilot	k1gMnPc2	pilot
<g/>
,	,	kIx,	,
navigátorů	navigátor	k1gMnPc2	navigátor
a	a	k8xC	a
telegrafistů	telegrafista	k1gMnPc2	telegrafista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
stíhací	stíhací	k2eAgFnPc1d1	stíhací
perutě	peruť	k1gFnPc1	peruť
tak	tak	k9	tak
i	i	k9	i
bombardovací	bombardovací	k2eAgFnSc4d1	bombardovací
peruť	peruť	k1gFnSc4	peruť
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
založení	založení	k1gNnSc6	založení
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyznamenaly	vyznamenat	k5eAaPmAgFnP	vyznamenat
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
bojích	boj	k1gInPc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Posádky	posádka	k1gFnPc1	posádka
byly	být	k5eAaImAgFnP	být
přitom	přitom	k6eAd1	přitom
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
navíc	navíc	k6eAd1	navíc
dalšímu	další	k2eAgNnSc3d1	další
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sestřelení	sestřelení	k1gNnSc2	sestřelení
nad	nad	k7c7	nad
německými	německý	k2eAgNnPc7d1	německé
územími	území	k1gNnPc7	území
a	a	k8xC	a
zajmutí	zajmutí	k1gNnPc2	zajmutí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
letců	letec	k1gMnPc2	letec
nebyli	být	k5eNaImAgMnP	být
podkládáni	podkládán	k2eAgMnPc1d1	podkládán
za	za	k7c4	za
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
příslušníci	příslušník	k1gMnPc1	příslušník
Německem	Německo	k1gNnSc7	Německo
ovládaného	ovládaný	k2eAgInSc2d1	ovládaný
Protektorátu	protektorát	k1gInSc2	protektorát
za	za	k7c4	za
velezrádce	velezrádce	k1gMnSc4	velezrádce
–	–	k?	–
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
pak	pak	k6eAd1	pak
v	v	k7c6	v
zesílené	zesílený	k2eAgFnSc6d1	zesílená
míře	míra	k1gFnSc6	míra
posádek	posádka	k1gFnPc2	posádka
311	[number]	k4	311
<g/>
.	.	kIx.	.
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
perutě	peruť	k1gFnSc2	peruť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podnikala	podnikat	k5eAaImAgFnS	podnikat
dálkové	dálkový	k2eAgInPc4d1	dálkový
nálety	nálet	k1gInPc4	nálet
na	na	k7c4	na
německé	německý	k2eAgNnSc4d1	německé
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
až	až	k9	až
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stíhací	stíhací	k2eAgMnPc1d1	stíhací
letci	letec	k1gMnPc1	letec
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
dalším	další	k2eAgInSc7d1	další
cílem	cíl	k1gInSc7	cíl
agrese	agrese	k1gFnSc2	agrese
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
hledala	hledat	k5eAaImAgFnS	hledat
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgInSc1d1	ochoten
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
českoslovenští	československý	k2eAgMnPc1d1	československý
piloti	pilot	k1gMnPc1	pilot
vítanou	vítaný	k2eAgFnSc7d1	vítaná
posilou	posila	k1gFnSc7	posila
britského	britský	k2eAgNnSc2d1	Britské
Královského	královský	k2eAgNnSc2d1	královské
letectva	letectvo	k1gNnSc2	letectvo
(	(	kIx(	(
<g/>
RAF	raf	k0	raf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
čs	čs	kA	čs
<g/>
.	.	kIx.	.
<g/>
-britská	ritský	k2eAgFnSc1d1	-britský
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
vojenským	vojenský	k2eAgNnSc7d1	vojenské
působením	působení	k1gNnSc7	působení
čs	čs	kA	čs
<g/>
.	.	kIx.	.
pozemních	pozemní	k2eAgFnPc2d1	pozemní
jednotek	jednotka	k1gFnPc2	jednotka
i	i	k8xC	i
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Letci	letec	k1gMnPc1	letec
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
samostatné	samostatný	k2eAgFnPc4d1	samostatná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Dobrovolnické	dobrovolnický	k2eAgFnSc2d1	dobrovolnická
zálohy	záloha	k1gFnSc2	záloha
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
téměř	téměř	k6eAd1	téměř
tisíc	tisíc	k4xCgInSc4	tisíc
čs	čs	kA	čs
<g/>
.	.	kIx.	.
letců	letec	k1gMnPc2	letec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
ihned	ihned	k6eAd1	ihned
postupně	postupně	k6eAd1	postupně
zapojováni	zapojovat	k5eAaImNgMnP	zapojovat
do	do	k7c2	do
výcviku	výcvik	k1gInSc2	výcvik
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
letounech	letoun	k1gInPc6	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
již	již	k6eAd1	již
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
čs	čs	kA	čs
<g/>
.	.	kIx.	.
310	[number]	k4	310
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
zahájila	zahájit	k5eAaPmAgFnS	zahájit
operační	operační	k2eAgFnSc1d1	operační
činnost	činnost	k1gFnSc1	činnost
i	i	k8xC	i
čs	čs	kA	čs
<g/>
.	.	kIx.	.
312	[number]	k4	312
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
čs	čs	kA	čs
<g/>
.	.	kIx.	.
piloti	pilot	k1gMnPc1	pilot
byli	být	k5eAaImAgMnP	být
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
i	i	k9	i
v	v	k7c6	v
britských	britský	k2eAgFnPc6d1	britská
a	a	k8xC	a
polských	polský	k2eAgFnPc6d1	polská
perutích	peruť	k1gFnPc6	peruť
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
stíhačů	stíhač	k1gMnPc2	stíhač
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
byl	být	k5eAaImAgMnS	být
četař	četař	k1gMnSc1	četař
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c4	v
polské	polský	k2eAgInPc4d1	polský
303	[number]	k4	303
<g/>
.	.	kIx.	.
peruti	peruť	k1gFnSc2	peruť
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
září	září	k1gNnSc2	září
1940	[number]	k4	1940
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
17	[number]	k4	17
nepřátelských	přátelský	k2eNgNnPc2d1	nepřátelské
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
sestřelili	sestřelit	k5eAaPmAgMnP	sestřelit
čs	čs	kA	čs
<g/>
.	.	kIx.	.
piloti	pilot	k1gMnPc1	pilot
56	[number]	k4	56
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
letadel	letadlo	k1gNnPc2	letadlo
sestřelili	sestřelit	k5eAaPmAgMnP	sestřelit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
a	a	k8xC	a
6	[number]	k4	6
poškodili	poškodit	k5eAaPmAgMnP	poškodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1942	[number]	k4	1942
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
československá	československý	k2eAgFnSc1d1	Československá
vyšší	vysoký	k2eAgFnSc1d2	vyšší
stíhací	stíhací	k2eAgFnSc1d1	stíhací
letecká	letecký	k2eAgFnSc1d1	letecká
formace	formace	k1gFnSc1	formace
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
wing	wing	k1gMnSc1	wing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
první	první	k4xOgFnSc7	první
významnou	významný	k2eAgFnSc7d1	významná
bojovou	bojový	k2eAgFnSc7d1	bojová
akcí	akce	k1gFnSc7	akce
byla	být	k5eAaImAgFnS	být
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
Dieppské	Dieppský	k2eAgFnSc6d1	Dieppský
výsadkové	výsadkový	k2eAgFnSc6d1	výsadková
operaci	operace	k1gFnSc6	operace
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
skupina	skupina	k1gFnSc1	skupina
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
nově	nově	k6eAd1	nově
zřízeného	zřízený	k2eAgNnSc2d1	zřízené
britského	britský	k2eAgNnSc2d1	Britské
taktického	taktický	k2eAgNnSc2d1	taktické
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
označení	označení	k1gNnPc4	označení
"	"	kIx"	"
<g/>
134	[number]	k4	134
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
čs	čs	kA	čs
<g/>
.	.	kIx.	.
perutě	peruť	k1gFnPc1	peruť
zúčastňovaly	zúčastňovat	k5eAaImAgFnP	zúčastňovat
nepřetržitých	přetržitý	k2eNgFnPc2d1	nepřetržitá
akcí	akce	k1gFnPc2	akce
spojeneckého	spojenecký	k2eAgNnSc2d1	spojenecké
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
předehrou	předehra	k1gFnSc7	předehra
k	k	k7c3	k
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
v	v	k7c4	v
"	"	kIx"	"
<g/>
den	den	k1gInSc4	den
D	D	kA	D
<g/>
"	"	kIx"	"
čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhači	stíhač	k1gMnPc1	stíhač
podporovali	podporovat	k5eAaImAgMnP	podporovat
vylodění	vylodění	k1gNnSc4	vylodění
1	[number]	k4	1
<g/>
.	.	kIx.	.
kanadské	kanadský	k2eAgFnSc2d1	kanadská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
absence	absence	k1gFnSc2	absence
záloh	záloha	k1gFnPc2	záloha
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
odvoláni	odvolat	k5eAaPmNgMnP	odvolat
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byli	být	k5eAaImAgMnP	být
přiděleni	přidělit	k5eAaPmNgMnP	přidělit
k	k	k7c3	k
velitelství	velitelství	k1gNnSc3	velitelství
Protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
přepady	přepad	k1gInPc4	přepad
německých	německý	k2eAgFnPc2d1	německá
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
bojové	bojový	k2eAgFnSc2d1	bojová
techniky	technika	k1gFnSc2	technika
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
bombardovací	bombardovací	k2eAgInPc4d1	bombardovací
svazy	svaz	k1gInPc4	svaz
<g/>
,	,	kIx,	,
zjišťovali	zjišťovat	k5eAaImAgMnP	zjišťovat
a	a	k8xC	a
ničili	ničit	k5eAaImAgMnP	ničit
létající	létající	k2eAgFnPc4d1	létající
střely	střela	k1gFnPc4	střela
V-	V-	k1gFnSc2	V-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
se	se	k3xPyFc4	se
letadla	letadlo	k1gNnSc2	letadlo
310	[number]	k4	310
<g/>
.	.	kIx.	.
a	a	k8xC	a
312	[number]	k4	312
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc2d1	stíhací
perutě	peruť	k1gFnSc2	peruť
účastnily	účastnit	k5eAaImAgFnP	účastnit
ochrany	ochrana	k1gFnPc1	ochrana
výsadkové	výsadkový	k2eAgFnSc2d1	výsadková
operace	operace	k1gFnSc2	operace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Arnhemu	Arnhem	k1gInSc2	Arnhem
a	a	k8xC	a
Eindhovenu	Eindhoven	k2eAgFnSc4d1	Eindhoven
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
velkou	velký	k2eAgFnSc7d1	velká
akcí	akce	k1gFnSc7	akce
čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhacích	stíhací	k2eAgFnPc2d1	stíhací
perutí	peruť	k1gFnPc2	peruť
byla	být	k5eAaImAgFnS	být
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
výsadkové	výsadkový	k2eAgFnPc4d1	výsadková
operace	operace	k1gFnPc4	operace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Weselu	Wesel	k1gInSc2	Wesel
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
přistálo	přistát	k5eAaImAgNnS	přistát
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
letišti	letiště	k1gNnSc6	letiště
54	[number]	k4	54
spitfirů	spitfir	k1gMnPc2	spitfir
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc2d1	stíhací
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
příslušníci	příslušník	k1gMnPc1	příslušník
311	[number]	k4	311
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
perutě	peruť	k1gFnSc2	peruť
byli	být	k5eAaImAgMnP	být
přivítáni	přivítat	k5eAaPmNgMnP	přivítat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bombardovací	bombardovací	k2eAgMnPc1d1	bombardovací
letci	letec	k1gMnPc1	letec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1940	[number]	k4	1940
zahájila	zahájit	k5eAaPmAgFnS	zahájit
činnost	činnost	k1gFnSc1	činnost
čs	čs	kA	čs
<g/>
.	.	kIx.	.
311	[number]	k4	311
<g/>
.	.	kIx.	.
bombardovací	bombardovací	k2eAgFnSc1d1	bombardovací
peruť	peruť	k1gFnSc1	peruť
<g/>
.	.	kIx.	.
</s>
<s>
Prováděla	provádět	k5eAaImAgFnS	provádět
noční	noční	k2eAgInPc4d1	noční
nálety	nálet	k1gInPc4	nálet
na	na	k7c4	na
střediska	středisko	k1gNnPc4	středisko
německého	německý	k2eAgInSc2d1	německý
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1940	[number]	k4	1940
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
prováděla	provádět	k5eAaImAgFnS	provádět
až	až	k9	až
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pro	pro	k7c4	pro
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
záloh	záloha	k1gFnPc2	záloha
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
bombardování	bombardování	k1gNnSc2	bombardování
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
a	a	k8xC	a
pověřena	pověřit	k5eAaPmNgFnS	pověřit
hlídkovou	hlídkový	k2eAgFnSc7d1	hlídková
činností	činnost	k1gFnSc7	činnost
nad	nad	k7c7	nad
Atlantikem	Atlantik	k1gInSc7	Atlantik
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
napadala	napadat	k5eAaPmAgFnS	napadat
a	a	k8xC	a
ničila	ničit	k5eAaImAgFnS	ničit
německé	německý	k2eAgFnPc4d1	německá
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
,	,	kIx,	,
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
doprovod	doprovod	k1gInSc4	doprovod
dopravních	dopravní	k2eAgNnPc2d1	dopravní
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
prosince	prosinec	k1gInSc2	prosinec
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
osádce	osádka	k1gFnSc3	osádka
jednoho	jeden	k4xCgMnSc4	jeden
letadla	letadlo	k1gNnPc1	letadlo
B-24	B-24	k1gFnSc4	B-24
Liberator	Liberator	k1gInSc4	Liberator
podařilo	podařit	k5eAaPmAgNnS	podařit
potopit	potopit	k5eAaPmF	potopit
německou	německý	k2eAgFnSc4d1	německá
loď	loď	k1gFnSc4	loď
Alsterufer	Alsterufra	k1gFnPc2	Alsterufra
s	s	k7c7	s
důležitým	důležitý	k2eAgInSc7d1	důležitý
nákladem	náklad	k1gInSc7	náklad
surovin	surovina	k1gFnPc2	surovina
z	z	k7c2	z
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boje	boj	k1gInPc1	boj
a	a	k8xC	a
ztráty	ztráta	k1gFnPc1	ztráta
==	==	k?	==
</s>
</p>
<p>
<s>
Historiografie	historiografie	k1gFnSc1	historiografie
válek	válka	k1gFnPc2	válka
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
poplatná	poplatný	k2eAgFnSc1d1	poplatná
spektakulárním	spektakulární	k2eAgFnPc3d1	spektakulární
akcím	akce	k1gFnPc3	akce
–	–	k?	–
proto	proto	k8xC	proto
vešly	vejít	k5eAaPmAgFnP	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
zejména	zejména	k9	zejména
jména	jméno	k1gNnPc1	jméno
letců	letec	k1gMnPc2	letec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
sestřelením	sestřelení	k1gNnSc7	sestřelení
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
dlužno	dlužen	k2eAgNnSc1d1	dlužno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
zásluha	zásluha	k1gFnSc1	zásluha
zde	zde	k6eAd1	zde
patří	patřit	k5eAaImIp3nS	patřit
i	i	k8xC	i
letcům	letec	k1gMnPc3	letec
a	a	k8xC	a
dalším	další	k2eAgMnPc3d1	další
příslušníkům	příslušník	k1gMnPc3	příslušník
posádek	posádka	k1gFnPc2	posádka
bombardovací	bombardovací	k2eAgInPc1d1	bombardovací
311	[number]	k4	311
<g/>
.	.	kIx.	.
perutě	peruť	k1gFnSc2	peruť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
tak	tak	k9	tak
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
příslušnících	příslušník	k1gMnPc6	příslušník
létajícího	létající	k2eAgInSc2d1	létající
personálu	personál	k1gInSc2	personál
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
úspěchy	úspěch	k1gInPc4	úspěch
československých	československý	k2eAgMnPc2d1	československý
bojovníků	bojovník	k1gMnPc2	bojovník
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
RAF	raf	k0	raf
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
365	[number]	k4	365
sestřelených	sestřelený	k2eAgFnPc2d1	sestřelená
(	(	kIx(	(
<g/>
či	či	k8xC	či
silně	silně	k6eAd1	silně
poškozených	poškozený	k2eAgFnPc2d1	poškozená
<g/>
)	)	kIx)	)
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
6	[number]	k4	6
sestřelených	sestřelený	k2eAgFnPc2d1	sestřelená
řízených	řízený	k2eAgFnPc2d1	řízená
střel	střela	k1gFnPc2	střela
V	v	k7c4	v
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
nutno	nutno	k6eAd1	nutno
přičíst	přičíst	k5eAaPmF	přičíst
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
<g/>
,	,	kIx,	,
zbrojních	zbrojní	k2eAgInPc2d1	zbrojní
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
<g/>
)	)	kIx)	)
cílem	cíl	k1gInSc7	cíl
bombardování	bombardování	k1gNnSc1	bombardování
311	[number]	k4	311
<g/>
.	.	kIx.	.
bombardovací	bombardovací	k2eAgFnSc2d1	bombardovací
perutě	peruť	k1gFnSc2	peruť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
zde	zde	k6eAd1	zde
figuruje	figurovat	k5eAaImIp3nS	figurovat
531	[number]	k4	531
příslušníků	příslušník	k1gMnPc2	příslušník
československého	československý	k2eAgNnSc2d1	Československé
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
během	během	k7c2	během
války	válka	k1gFnSc2	válka
ztratili	ztratit	k5eAaPmAgMnP	ztratit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Letadla	letadlo	k1gNnPc1	letadlo
čs	čs	kA	čs
<g/>
.	.	kIx.	.
letců	letec	k1gMnPc2	letec
v	v	k7c6	v
RAF	raf	k0	raf
==	==	k?	==
</s>
</p>
<p>
<s>
Stíhací	stíhací	k2eAgInSc1d1	stíhací
<g/>
:	:	kIx,	:
Hawker	Hawker	k1gInSc1	Hawker
Hurricane	Hurrican	k1gMnSc5	Hurrican
<g/>
,	,	kIx,	,
Supermarine	Supermarin	k1gInSc5	Supermarin
Spitfire	Spitfir	k1gMnSc5	Spitfir
</s>
</p>
<p>
<s>
Noční	noční	k2eAgInSc1d1	noční
stíhací	stíhací	k2eAgInSc1d1	stíhací
<g/>
:	:	kIx,	:
Bristol	Bristol	k1gInSc1	Bristol
Beaufighter	Beaufighter	k1gInSc1	Beaufighter
<g/>
,	,	kIx,	,
de	de	k?	de
Havilland	Havilland	k1gInSc1	Havilland
Mosquito	Mosquit	k2eAgNnSc4d1	Mosquito
</s>
</p>
<p>
<s>
Bombardovací	bombardovací	k2eAgInSc1d1	bombardovací
<g/>
:	:	kIx,	:
Vickers	Vickers	k1gInSc1	Vickers
Wellington	Wellington	k1gInSc1	Wellington
<g/>
,	,	kIx,	,
Consolidated	Consolidated	k1gMnSc1	Consolidated
B-24	B-24	k1gMnSc1	B-24
Liberator	Liberator	k1gMnSc1	Liberator
</s>
</p>
<p>
<s>
==	==	k?	==
Postavení	postavení	k1gNnSc1	postavení
příslušníků	příslušník	k1gMnPc2	příslušník
západní	západní	k2eAgFnSc2d1	západní
fronty	fronta	k1gFnSc2	fronta
za	za	k7c2	za
komunismu	komunismus	k1gInSc2	komunismus
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
osvobozeného	osvobozený	k2eAgNnSc2d1	osvobozené
Československa	Československo	k1gNnSc2	Československo
tyto	tento	k3xDgFnPc4	tento
perutě	peruť	k1gFnPc4	peruť
ještě	ještě	k6eAd1	ještě
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
existovaly	existovat	k5eAaImAgInP	existovat
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
však	však	k9	však
byly	být	k5eAaImAgInP	být
jako	jako	k9	jako
perutě	peruť	k1gFnSc2	peruť
RAF	raf	k0	raf
zrušeny	zrušit	k5eAaPmNgInP	zrušit
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
vedení	vedení	k1gNnSc4	vedení
zde	zde	k6eAd1	zde
převzali	převzít	k5eAaPmAgMnP	převzít
mnoho	mnoho	k4c1	mnoho
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
úloh	úloha	k1gFnPc2	úloha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectví	letectví	k1gNnSc2	letectví
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
letců	letec	k1gMnPc2	letec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
až	až	k9	až
koncem	koncem	k7c2	koncem
války	válka	k1gFnSc2	válka
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
odborníky	odborník	k1gMnPc7	odborník
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
velitelé	velitel	k1gMnPc1	velitel
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
náčelníci	náčelník	k1gMnPc1	náčelník
výcvikových	výcvikový	k2eAgNnPc2d1	výcvikové
zařízení	zařízení	k1gNnPc2	zařízení
ap.	ap.	kA	ap.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
1948	[number]	k4	1948
a	a	k8xC	a
zejména	zejména	k9	zejména
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
politických	politický	k2eAgInPc2d1	politický
strojených	strojený	k2eAgInPc2d1	strojený
procesů	proces	k1gInPc2	proces
se	se	k3xPyFc4	se
pozornost	pozornost	k1gFnSc1	pozornost
stranických	stranický	k2eAgMnPc2d1	stranický
a	a	k8xC	a
soudních	soudní	k2eAgMnPc2d1	soudní
činitelů	činitel	k1gMnPc2	činitel
soustředila	soustředit	k5eAaPmAgFnS	soustředit
i	i	k9	i
na	na	k7c4	na
účastníky	účastník	k1gMnPc4	účastník
odboje	odboj	k1gInSc2	odboj
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
na	na	k7c4	na
bývalé	bývalý	k2eAgMnPc4d1	bývalý
příslušníky	příslušník	k1gMnPc4	příslušník
RAF	raf	k0	raf
<g/>
.	.	kIx.	.
</s>
<s>
Propuštění	propuštění	k1gNnSc1	propuštění
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
postih	postih	k1gInSc4	postih
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
vyhození	vyhození	k1gNnSc2	vyhození
z	z	k7c2	z
bytu	byt	k1gInSc2	byt
a	a	k8xC	a
propuštění	propuštění	k1gNnSc4	propuštění
manželky	manželka	k1gFnSc2	manželka
ze	z	k7c2	z
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc2d2	pozdější
perzekuce	perzekuce	k1gFnSc2	perzekuce
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
přijímání	přijímání	k1gNnSc6	přijímání
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
patřily	patřit	k5eAaImAgFnP	patřit
ještě	ještě	k6eAd1	ještě
k	k	k7c3	k
těm	ten	k3xDgInPc3	ten
nejmenším	malý	k2eAgInPc3d3	nejmenší
postihům	postih	k1gInPc3	postih
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
příslušníků	příslušník	k1gMnPc2	příslušník
západního	západní	k2eAgInSc2d1	západní
odboje	odboj	k1gInSc2	odboj
bylo	být	k5eAaImAgNnS	být
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
<g/>
,	,	kIx,	,
vyslýcháno	vyslýchán	k2eAgNnSc1d1	vyslýcháno
(	(	kIx(	(
<g/>
nejčastější	častý	k2eAgNnSc1d3	nejčastější
obvinění	obvinění	k1gNnSc1	obvinění
bylo	být	k5eAaImAgNnS	být
špionáž	špionáž	k1gFnSc4	špionáž
pro	pro	k7c4	pro
západní	západní	k2eAgFnPc4d1	západní
mocnosti	mocnost	k1gFnPc4	mocnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
odsouzeno	odsouzen	k2eAgNnSc1d1	odsouzeno
k	k	k7c3	k
žaláři	žalář	k1gInSc3	žalář
<g/>
;	;	kIx,	;
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
došlo	dojít	k5eAaPmAgNnS	dojít
často	často	k6eAd1	často
k	k	k7c3	k
mučení	mučení	k1gNnSc3	mučení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
zásluhy	zásluha	k1gFnSc2	zásluha
západního	západní	k2eAgInSc2d1	západní
odboje	odboj	k1gInSc2	odboj
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Československa	Československo	k1gNnSc2	Československo
dostaly	dostat	k5eAaPmAgInP	dostat
opět	opět	k6eAd1	opět
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
liknavých	liknavý	k2eAgFnPc6d1	liknavá
reakcích	reakce	k1gFnPc6	reakce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rehabilitaci	rehabilitace	k1gFnSc3	rehabilitace
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
českoslovenští	československý	k2eAgMnPc1d1	československý
stíhači	stíhač	k1gMnPc1	stíhač
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc4	počet
sestřelených	sestřelený	k2eAgInPc2d1	sestřelený
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc6	druhý
počet	počet	k1gInSc1	počet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
sestřelených	sestřelený	k2eAgFnPc2d1	sestřelená
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
počet	počet	k1gInSc4	počet
poškozených	poškozený	k2eAgInPc2d1	poškozený
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchy	úspěch	k1gInPc1	úspěch
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
francouzskému	francouzský	k2eAgInSc3d1	francouzský
systému	systém	k1gInSc3	systém
započítávání	započítávání	k1gNnSc2	započítávání
sestřelů	sestřel	k1gInPc2	sestřel
<g/>
,	,	kIx,	,
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
jsou	být	k5eAaImIp3nP	být
úspěchy	úspěch	k1gInPc1	úspěch
vyjádřené	vyjádřený	k2eAgInPc1d1	vyjádřený
britskou	britský	k2eAgFnSc7d1	britská
normou	norma	k1gFnSc7	norma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
Ldr	Ldr	k1gMnSc1	Ldr
Karel	Karel	k1gMnSc1	Karel
Kuttelwascher	Kuttelwaschra	k1gFnPc2	Kuttelwaschra
<g/>
,	,	kIx,	,
DFC	DFC	kA	DFC
&	&	k?	&
bar	bar	k1gInSc1	bar
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
(	(	kIx(	(
<g/>
18	[number]	k4	18
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sgt	Sgt	k?	Sgt
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
DFM	DFM	kA	DFM
&	&	k?	&
bar	bar	k1gInSc1	bar
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
Cdr	Cdr	k1gMnSc1	Cdr
Alois	Alois	k1gMnSc1	Alois
Vašátko	Vašátko	k1gMnSc1	Vašátko
<g/>
,	,	kIx,	,
DFC	DFC	kA	DFC
<g/>
:	:	kIx,	:
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
6	[number]	k4	6
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
2	[number]	k4	2
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
Ldr	Ldr	k1gMnSc1	Ldr
František	František	k1gMnSc1	František
Peřina	Peřina	k1gMnSc1	Peřina
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
6	[number]	k4	6
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
Ldr	Ldr	k1gMnSc1	Ldr
Otto	Otto	k1gMnSc1	Otto
Smik	Smik	k1gMnSc1	Smik
<g/>
,	,	kIx,	,
DFC	DFC	kA	DFC
<g/>
:	:	kIx,	:
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
+3	+3	k4	+3
střely	střel	k1gInPc7	střel
V	v	k7c4	v
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
Lt	Lt	k1gMnSc1	Lt
Josef	Josef	k1gMnSc1	Josef
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
5	[number]	k4	5
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
2	[number]	k4	2
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
Ldr	Ldr	k1gMnSc1	Ldr
Miroslav	Miroslav	k1gMnSc1	Miroslav
J.	J.	kA	J.
Mansfeld	Mansfeld	k1gMnSc1	Mansfeld
<g/>
,	,	kIx,	,
DSO	DSO	kA	DSO
<g/>
,	,	kIx,	,
DFC	DFC	kA	DFC
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
střely	střela	k1gFnPc4	střela
V	v	k7c4	v
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
O	O	kA	O
Leopold	Leopold	k1gMnSc1	Leopold
Šrom	Šrom	k1gMnSc1	Šrom
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
6	[number]	k4	6
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
Lt	Lt	k1gFnSc1	Lt
Václav	Václav	k1gMnSc1	Václav
Cukr	cukr	k1gInSc1	cukr
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
4	[number]	k4	4
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
Ldr	Ldr	k1gMnSc1	Ldr
Otmar	Otmar	k1gMnSc1	Otmar
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
DFC	DFC	kA	DFC
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
5	[number]	k4	5
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Památníky	památník	k1gInPc4	památník
věnované	věnovaný	k2eAgInPc4d1	věnovaný
československým	československý	k2eAgMnPc3d1	československý
letcům	letec	k1gMnPc3	letec
na	na	k7c6	na
území	území	k1gNnSc6	území
Prahy	Praha	k1gFnSc2	Praha
==	==	k?	==
</s>
</p>
<p>
<s>
Památníky	památník	k1gInPc1	památník
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
československým	československý	k2eAgMnPc3d1	československý
letcům	letec	k1gMnPc3	letec
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RAJLICH	RAJLICH	kA	RAJLICH
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
No	no	k9	no
312	[number]	k4	312
<g/>
th	th	k?	th
(	(	kIx(	(
<g/>
Czechoslovak	Czechoslovak	k1gMnSc1	Czechoslovak
<g/>
)	)	kIx)	)
Fighter	fighter	k1gMnSc1	fighter
Squadron	Squadron	k1gMnSc1	Squadron
<g/>
.	.	kIx.	.
</s>
<s>
HPM	HPM	kA	HPM
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
s.	s.	k?	s.
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
1427	[number]	k4	1427
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
310	[number]	k4	310
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
311	[number]	k4	311
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
bombardovací	bombardovací	k2eAgFnSc1d1	bombardovací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
312	[number]	k4	312
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
313	[number]	k4	313
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
stíhací	stíhací	k2eAgFnSc1d1	stíhací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
smíšená	smíšený	k2eAgFnSc1d1	smíšená
letecká	letecký	k2eAgFnSc1d1	letecká
divize	divize	k1gFnSc1	divize
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
</s>
</p>
<p>
<s>
Dalibor	Dalibor	k1gMnSc1	Dalibor
Brochard	Brochard	k1gMnSc1	Brochard
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Vella	Vella	k1gMnSc1	Vella
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Wiener	Wiener	k1gMnSc1	Wiener
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
Okřídlený	okřídlený	k2eAgInSc4d1	okřídlený
lev	lev	k1gInSc4	lev
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
návratu	návrat	k1gInSc2	návrat
československých	československý	k2eAgFnPc2d1	Československá
perutí	peruť	k1gFnPc2	peruť
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
Československou	československý	k2eAgFnSc7d1	Československá
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
branné	branný	k2eAgFnSc6d1	Branná
moci	moc	k1gFnSc6	moc
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Příloha	příloha	k1gFnSc1	příloha
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jednající	jednající	k2eAgMnSc1d1	jednající
o	o	k7c6	o
československém	československý	k2eAgNnSc6d1	Československé
letectvu	letectvo	k1gNnSc6	letectvo
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
západních	západní	k2eAgMnPc2d1	západní
letců	letec	k1gMnPc2	letec
(	(	kIx(	(
<g/>
MNO	MNO	kA	MNO
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Svaz	svaz	k1gInSc1	svaz
letců	letec	k1gMnPc2	letec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
RAF	raf	k0	raf
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Letecká	letecký	k2eAgFnSc1d1	letecká
galerie	galerie	k1gFnSc1	galerie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Českoslovenští	československý	k2eAgMnPc1d1	československý
letci	letec	k1gMnPc1	letec
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
www.army.cz	www.army.cz	k1gInSc1	www.army.cz
–	–	k?	–
seznam	seznam	k1gInSc1	seznam
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
publikací	publikace	k1gFnPc2	publikace
o	o	k7c6	o
československých	československý	k2eAgInPc6d1	československý
letcích	letek	k1gInPc6	letek
(	(	kIx(	(
<g/>
MNO	MNO	kA	MNO
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Čeští	český	k2eAgMnPc1d1	český
RAFáci	RAFák	k1gMnPc1	RAFák
–	–	k?	–
připomínka	připomínka	k1gFnSc1	připomínka
čs	čs	kA	čs
<g/>
.	.	kIx.	.
válečných	válečný	k2eAgMnPc2d1	válečný
letců	letec	k1gMnPc2	letec
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Hrdiny	Hrdina	k1gMnSc2	Hrdina
z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
vítaly	vítat	k5eAaImAgInP	vítat
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
nestáli	stát	k5eNaImAgMnP	stát
</s>
</p>
<p>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
nedoletěli	doletět	k5eNaPmAgMnP	doletět
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
maps	maps	k1gInSc1	maps
<g/>
.	.	kIx.	.
<g/>
google	google	k1gFnSc1	google
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
–	–	k?	–
interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
mapa	mapa	k1gFnSc1	mapa
míst	místo	k1gNnPc2	místo
sestřelení	sestřelení	k1gNnSc2	sestřelení
nebo	nebo	k8xC	nebo
úmrtí	úmrtí	k1gNnSc2	úmrtí
československých	československý	k2eAgMnPc2d1	československý
letců	letec	k1gMnPc2	letec
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
