<p>
<s>
Psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
uživatel	uživatel	k1gMnSc1	uživatel
píše	psát	k5eAaImIp3nS	psát
na	na	k7c4	na
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
,	,	kIx,	,
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ruční	ruční	k2eAgInSc1d1	ruční
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mechanický	mechanický	k2eAgInSc1d1	mechanický
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
elektrický	elektrický	k2eAgInSc4d1	elektrický
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgInPc1d1	elektrický
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c6	na
elektromechanické	elektromechanický	k2eAgFnSc6d1	elektromechanická
a	a	k8xC	a
elektronické	elektronický	k2eAgFnSc6d1	elektronická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
klávesnice	klávesnice	k1gFnSc2	klávesnice
a	a	k8xC	a
mechanického	mechanický	k2eAgMnSc2d1	mechanický
nebo	nebo	k8xC	nebo
elektricky	elektricky	k6eAd1	elektricky
poháněného	poháněný	k2eAgNnSc2d1	poháněné
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
psaný	psaný	k2eAgInSc4d1	psaný
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
zařízením	zařízení	k1gNnSc7	zařízení
pro	pro	k7c4	pro
opravu	oprava	k1gFnSc4	oprava
psaného	psaný	k2eAgInSc2d1	psaný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
výstupní	výstupní	k2eAgNnSc1d1	výstupní
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytisknout	vytisknout	k5eAaPmF	vytisknout
text	text	k1gInSc4	text
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
typový	typový	k2eAgInSc4d1	typový
koš	koš	k1gInSc4	koš
<g/>
,	,	kIx,	,
kulová	kulový	k2eAgFnSc1d1	kulová
hlavice	hlavice	k1gFnSc1	hlavice
<g/>
,	,	kIx,	,
typové	typový	k2eAgNnSc1d1	typové
kolo	kolo	k1gNnSc1	kolo
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Papír	papír	k1gInSc1	papír
bývá	bývat	k5eAaImIp3nS	bývat
posunován	posunovat	k5eAaImNgInS	posunovat
nejčastěji	často	k6eAd3	často
pomocí	pomocí	k7c2	pomocí
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Horizontální	horizontální	k2eAgInSc1d1	horizontální
posun	posun	k1gInSc1	posun
papíru	papír	k1gInSc2	papír
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
posuvný	posuvný	k2eAgInSc4d1	posuvný
vozík	vozík	k1gInSc4	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
stroje	stroj	k1gInSc2	stroj
se	se	k3xPyFc4	se
posunuje	posunovat	k5eAaImIp3nS	posunovat
válec	válec	k1gInSc1	válec
s	s	k7c7	s
připevněným	připevněný	k2eAgInSc7d1	připevněný
papírem	papír	k1gInSc7	papír
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
úhozu	úhoz	k1gInSc6	úhoz
(	(	kIx(	(
<g/>
vytištění	vytištění	k1gNnSc2	vytištění
jednoho	jeden	k4xCgMnSc2	jeden
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
krok	krok	k1gInSc4	krok
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
psací	psací	k2eAgNnSc1d1	psací
zařízení	zařízení	k1gNnSc1	zařízení
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
krok	krok	k1gInSc4	krok
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
neplatí	platit	k5eNaImIp3nS	platit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
znak	znak	k1gInSc4	znak
umístěný	umístěný	k2eAgInSc4d1	umístěný
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
mrtvé	mrtvý	k2eAgFnSc6d1	mrtvá
klávese	klávesa	k1gFnSc6	klávesa
–	–	k?	–
například	například	k6eAd1	například
́	́	k?	́
<g/>
,	,	kIx,	,
ˇ	ˇ	k?	ˇ
<g/>
,	,	kIx,	,
̈	̈	k?	̈
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Henry	Henry	k1gMnSc1	Henry
Mill	Mill	k1gMnSc1	Mill
patentoval	patentovat	k5eAaBmAgMnS	patentovat
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
řešení	řešení	k1gNnPc1	řešení
se	se	k3xPyFc4	se
však	však	k9	však
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozložení	rozložení	k1gNnSc1	rozložení
kláves	klávesa	k1gFnPc2	klávesa
nebylo	být	k5eNaImAgNnS	být
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc1	jaký
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
významná	významný	k2eAgFnSc1d1	významná
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pánové	pán	k1gMnPc1	pán
Christopher	Christophra	k1gFnPc2	Christophra
Latham	Latham	k1gInSc1	Latham
Sholes	Sholes	k1gMnSc1	Sholes
a	a	k8xC	a
Carlos	Carlos	k1gMnSc1	Carlos
Glidden	Gliddna	k1gFnPc2	Gliddna
prodali	prodat	k5eAaPmAgMnP	prodat
krachující	krachující	k2eAgFnSc3d1	krachující
zbrojovce	zbrojovka	k1gFnSc3	zbrojovka
E.	E.	kA	E.
Remington	remington	k1gInSc1	remington
&	&	k?	&
Sons	Sons	k1gInSc1	Sons
psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nazvali	nazvat	k5eAaPmAgMnP	nazvat
Typewriter	Typewriter	k1gInSc4	Typewriter
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
uměl	umět	k5eAaImAgMnS	umět
psát	psát	k5eAaImF	psát
pouze	pouze	k6eAd1	pouze
verzálky	verzálka	k1gFnSc2	verzálka
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k6eAd1	jen
5000	[number]	k4	5000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
ovšem	ovšem	k9	ovšem
firma	firma	k1gFnSc1	firma
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
verzí	verze	k1gFnSc7	verze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
už	už	k6eAd1	už
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
přeřaďovač	přeřaďovač	k1gInSc4	přeřaďovač
(	(	kIx(	(
<g/>
shift	shift	k1gInSc4	shift
<g/>
)	)	kIx)	)
a	a	k8xC	a
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
psát	psát	k5eAaImF	psát
malá	malý	k2eAgNnPc4d1	malé
i	i	k8xC	i
velká	velký	k2eAgNnPc4d1	velké
písmena	písmeno	k1gNnPc4	písmeno
pomocí	pomocí	k7c2	pomocí
stejných	stejný	k2eAgFnPc2d1	stejná
kláves	klávesa	k1gFnPc2	klávesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
Typewriteru	Typewriter	k1gInSc2	Typewriter
se	s	k7c7	s
C.	C.	kA	C.
L.	L.	kA	L.
Sholes	Sholes	k1gMnSc1	Sholes
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
psala	psát	k5eAaImAgNnP	psát
písmena	písmeno	k1gNnPc1	písmeno
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
typové	typový	k2eAgFnPc1d1	typová
páky	páka	k1gFnPc1	páka
byly	být	k5eAaImAgFnP	být
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
častému	častý	k2eAgNnSc3d1	časté
zasekávání	zasekávání	k1gNnSc3	zasekávání
a	a	k8xC	a
psaní	psaní	k1gNnSc3	psaní
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
velmi	velmi	k6eAd1	velmi
obtížným	obtížný	k2eAgInSc7d1	obtížný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
přišel	přijít	k5eAaPmAgMnS	přijít
vynálezce	vynálezce	k1gMnSc1	vynálezce
Typewriteru	Typewriter	k1gInSc2	Typewriter
se	s	k7c7	s
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
:	:	kIx,	:
typové	typový	k2eAgFnPc4d1	typová
páky	páka	k1gFnPc4	páka
s	s	k7c7	s
nejčastěji	často	k6eAd3	často
používanými	používaný	k2eAgFnPc7d1	používaná
písmeny	písmeno	k1gNnPc7	písmeno
anglické	anglický	k2eAgFnSc2d1	anglická
abecedy	abeceda	k1gFnSc2	abeceda
umístil	umístit	k5eAaPmAgMnS	umístit
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
sebe	se	k3xPyFc2	se
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zvolil	zvolit	k5eAaPmAgMnS	zvolit
takové	takový	k3xDgNnSc4	takový
rozložení	rozložení	k1gNnSc4	rozložení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
pisateli	pisatel	k1gMnSc3	pisatel
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
rychlý	rychlý	k2eAgInSc1d1	rychlý
způsob	způsob	k1gInSc1	způsob
psaní	psaní	k1gNnSc2	psaní
(	(	kIx(	(
<g/>
mohl	moct	k5eAaImAgMnS	moct
mačkat	mačkat	k5eAaImF	mačkat
klávesy	kláves	k1gInPc4	kláves
rychle	rychle	k6eAd1	rychle
za	za	k7c7	za
sebou	se	k3xPyFc7	se
pomocí	pomocí	k7c2	pomocí
všech	všecek	k3xTgNnPc2	všecek
deseti	deset	k4xCc2	deset
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
zpaměti	zpaměti	k6eAd1	zpaměti
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
a	a	k8xC	a
přesnosti	přesnost	k1gFnSc6	přesnost
psaní	psaní	k1gNnSc2	psaní
dalo	dát	k5eAaPmAgNnS	dát
přednost	přednost	k1gFnSc4	přednost
tomuto	tento	k3xDgInSc3	tento
rozložení	rozložení	k1gNnSc1	rozložení
kláves	klávesa	k1gFnPc2	klávesa
před	před	k7c7	před
všemi	všecek	k3xTgFnPc7	všecek
ostatními	ostatní	k2eAgFnPc7d1	ostatní
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc4d1	různý
počty	počet	k1gInPc4	počet
řad	řada	k1gFnPc2	řada
kláves	klávesa	k1gFnPc2	klávesa
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
rozložení	rozložení	k1gNnSc1	rozložení
kláves	klávesa	k1gFnPc2	klávesa
přijato	přijmout	k5eAaPmNgNnS	přijmout
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
odborníků	odborník	k1gMnPc2	odborník
jako	jako	k8xC	jako
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc3	úspěch
Typewriteru	Typewriter	k1gInSc2	Typewriter
II	II	kA	II
převzaly	převzít	k5eAaPmAgFnP	převzít
toto	tento	k3xDgNnSc4	tento
rozložení	rozložení	k1gNnSc4	rozložení
další	další	k2eAgFnSc2d1	další
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gNnSc4	on
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
národy	národ	k1gInPc1	národ
zavedly	zavést	k5eAaPmAgInP	zavést
drobné	drobné	k1gInPc4	drobné
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
klávesnice	klávesnice	k1gFnSc1	klávesnice
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
německých	německý	k2eAgInPc2d1	německý
psacích	psací	k2eAgInPc2d1	psací
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
začaly	začít	k5eAaPmAgInP	začít
dovážet	dovážet	k5eAaImF	dovážet
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
měly	mít	k5eAaImAgFnP	mít
přehozené	přehozený	k2eAgFnPc4d1	přehozená
klávesy	klávesa	k1gFnPc4	klávesa
Z	Z	kA	Z
a	a	k8xC	a
Y	Y	kA	Y
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Z	Z	kA	Z
se	se	k3xPyFc4	se
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
používá	používat	k5eAaImIp3nS	používat
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
než	než	k8xS	než
Y.	Y.	kA	Y.
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
psalo	psát	k5eAaImAgNnS	psát
hlavně	hlavně	k9	hlavně
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
přirozeně	přirozeně	k6eAd1	přirozeně
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
rozložení	rozložení	k1gNnSc1	rozložení
převzato	převzít	k5eAaPmNgNnS	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ergonomického	ergonomický	k2eAgNnSc2d1	ergonomické
hlediska	hledisko	k1gNnSc2	hledisko
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
umístění	umístění	k1gNnSc1	umístění
Z	z	k7c2	z
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
písmenné	písmenný	k2eAgFnSc6d1	písmenná
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
rozložení	rozložení	k1gNnSc1	rozložení
QWERTY	QWERTY	kA	QWERTY
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přehozením	přehození	k1gNnSc7	přehození
Q	Q	kA	Q
a	a	k8xC	a
W	W	kA	W
s	s	k7c7	s
písmeny	písmeno	k1gNnPc7	písmeno
A	A	kA	A
a	a	k8xC	a
Z	Z	kA	Z
se	se	k3xPyFc4	se
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
klávesnici	klávesnice	k1gFnSc3	klávesnice
AZERTY	AZERTY	kA	AZERTY
<g/>
.	.	kIx.	.
</s>
<s>
Podobných	podobný	k2eAgFnPc2d1	podobná
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
úprav	úprava	k1gFnPc2	úprava
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dálnopis	dálnopis	k1gInSc1	dálnopis
</s>
</p>
<p>
<s>
Sázecí	sázecí	k2eAgInSc1d1	sázecí
stroj	stroj	k1gInSc1	stroj
</s>
</p>
<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
tiskárna	tiskárna	k1gFnSc1	tiskárna
</s>
</p>
<p>
<s>
Psaní	psaní	k1gNnSc1	psaní
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
</s>
</p>
<p>
<s>
Normostrana	Normostrana	k1gFnSc1	Normostrana
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
psací	psací	k2eAgInSc4d1	psací
stroj	stroj	k1gInSc4	stroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
psací	psací	k2eAgFnSc1d1	psací
stroj	stroj	k1gInSc4	stroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
