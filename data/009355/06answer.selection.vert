<s>
Psací	psací	k2eAgInSc1d1	psací
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
uživatel	uživatel	k1gMnSc1	uživatel
píše	psát	k5eAaImIp3nS	psát
na	na	k7c4	na
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
,	,	kIx,	,
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
