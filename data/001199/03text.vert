<s>
Lithium	lithium	k1gNnSc1	lithium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Li	li	k9	li
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Lithium	lithium	k1gNnSc4	lithium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
stříbřitě	stříbřitě	k6eAd1	stříbřitě
lesklého	lesklý	k2eAgInSc2d1	lesklý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
lehký	lehký	k2eAgInSc4d1	lehký
a	a	k8xC	a
měkký	měkký	k2eAgInSc4d1	měkký
kov	kov	k1gInSc4	kov
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
měkčí	měkký	k2eAgInSc1d2	měkčí
než	než	k8xS	než
mastek	mastek	k1gInSc1	mastek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
lze	lze	k6eAd1	lze
krájet	krájet	k5eAaImF	krájet
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
vede	vést	k5eAaImIp3nS	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hustotu	hustota	k1gFnSc4	hustota
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
pevných	pevný	k2eAgInPc2d1	pevný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
petrolej	petrolej	k1gInSc1	petrolej
a	a	k8xC	a
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
kovy	kov	k1gInPc7	kov
má	mít	k5eAaImIp3nS	mít
lithium	lithium	k1gNnSc1	lithium
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgFnPc4d1	nízká
teploty	teplota	k1gFnPc4	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plynném	plynný	k2eAgNnSc6d1	plynné
lithiu	lithium	k1gNnSc6	lithium
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vedle	vedle	k7c2	vedle
jednoatomových	jednoatomový	k2eAgFnPc2d1	jednoatomový
částic	částice	k1gFnPc2	částice
i	i	k8xC	i
dvouatomové	dvouatomový	k2eAgFnPc1d1	dvouatomová
molekuly	molekula	k1gFnPc1	molekula
lithia	lithium	k1gNnSc2	lithium
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
lithia	lithium	k1gNnSc2	lithium
mají	mít	k5eAaImIp3nP	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
lithia	lithium	k1gNnSc2	lithium
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
má	mít	k5eAaImIp3nS	mít
temně	temně	k6eAd1	temně
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgNnSc4d1	přírodní
lithium	lithium	k1gNnSc4	lithium
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
cca	cca	kA	cca
7,5	[number]	k4	7,5
<g/>
%	%	kIx~	%
izotopu	izotop	k1gInSc2	izotop
6	[number]	k4	6
<g/>
Li	li	k8xS	li
a	a	k8xC	a
92,5	[number]	k4	92,5
<g/>
%	%	kIx~	%
7	[number]	k4	7
<g/>
Li	li	k8xS	li
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
izotopů	izotop	k1gInPc2	izotop
není	být	k5eNaImIp3nS	být
stálý	stálý	k2eAgInSc1d1	stálý
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
geologických	geologický	k2eAgInPc6d1	geologický
poměrech	poměr	k1gInPc6	poměr
původního	původní	k2eAgInSc2d1	původní
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Lehčí	lehký	k2eAgInSc1d2	lehčí
izotop	izotop	k1gInSc1	izotop
6	[number]	k4	6
<g/>
Li	li	k8xS	li
dobře	dobře	k6eAd1	dobře
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
neutrony	neutron	k1gInPc4	neutron
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tritia	tritium	k1gNnSc2	tritium
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
6	[number]	k4	6
<g/>
Li	li	k9	li
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
těžkého	těžký	k2eAgInSc2d1	těžký
vodíkového	vodíkový	k2eAgInSc2d1	vodíkový
izotopu	izotop	k1gInSc2	izotop
využívána	využíván	k2eAgFnSc1d1	využívána
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
pro	pro	k7c4	pro
civilní	civilní	k2eAgFnPc4d1	civilní
potřeby	potřeba	k1gFnPc4	potřeba
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
lithiumdeuterid	lithiumdeuterid	k1gInSc4	lithiumdeuterid
v	v	k7c6	v
termonukleární	termonukleární	k2eAgFnSc6d1	termonukleární
zbrani	zbraň	k1gFnSc6	zbraň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
lithia	lithium	k1gNnSc2	lithium
v	v	k7c4	v
6	[number]	k4	6
<g/>
LiD	Lido	k1gNnPc2	Lido
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tritium	tritium	k1gNnSc1	tritium
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
následně	následně	k6eAd1	následně
zreaguje	zreagovat	k5eAaBmIp3nS	zreagovat
s	s	k7c7	s
deuteriem	deuterium	k1gNnSc7	deuterium
za	za	k7c4	za
uvolnění	uvolnění	k1gNnSc4	uvolnění
velké	velký	k2eAgFnSc2d1	velká
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Těžší	těžký	k2eAgInSc1d2	těžší
izotop	izotop	k1gInSc1	izotop
7	[number]	k4	7
<g/>
Li	li	k8xS	li
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
účinný	účinný	k2eAgInSc1d1	účinný
průřez	průřez	k1gInSc1	průřez
záchytu	záchyt	k1gInSc2	záchyt
neutronu	neutron	k1gInSc2	neutron
malý	malý	k2eAgMnSc1d1	malý
a	a	k8xC	a
soli	sůl	k1gFnPc1	sůl
7	[number]	k4	7
<g/>
Li	li	k8xS	li
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
inertní	inertní	k2eAgNnSc4d1	inertní
médium	médium	k1gNnSc4	médium
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
technologii	technologie	k1gFnSc6	technologie
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
LiOH	LiOH	k1gFnPc2	LiOH
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
alkalizaci	alkalizace	k1gFnSc3	alkalizace
chladící	chladící	k2eAgFnSc2d1	chladící
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc1	známá
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
taveninové	taveninový	k2eAgFnSc2d1	taveninový
palivové	palivový	k2eAgFnSc2d1	palivová
kompozice	kompozice	k1gFnSc2	kompozice
fluoridů	fluorid	k1gInPc2	fluorid
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
plutonia	plutonium	k1gNnSc2	plutonium
či	či	k8xC	či
nejmoderněji	moderně	k6eAd3	moderně
thoria	thorium	k1gNnSc2	thorium
<g/>
,	,	kIx,	,
ve	v	k7c4	v
kterých	který	k3yQgFnPc2	který
7	[number]	k4	7
<g/>
LiF	LiF	k1gFnPc2	LiF
účinně	účinně	k6eAd1	účinně
snižuje	snižovat	k5eAaImIp3nS	snižovat
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc2	tání
takové	takový	k3xDgFnSc2	takový
směsi	směs	k1gFnSc2	směs
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
ze	z	k7c2	z
systému	systém	k1gInSc2	systém
vychytával	vychytávat	k5eAaImAgMnS	vychytávat
neutrony	neutron	k1gInPc7	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdíl	rozdíl	k1gInSc1	rozdíl
hmotností	hmotnost	k1gFnSc7	hmotnost
obou	dva	k4xCgInPc2	dva
lithiových	lithiový	k2eAgInPc2d1	lithiový
izotopů	izotop	k1gInPc2	izotop
je	být	k5eAaImIp3nS	být
procentně	procentně	k6eAd1	procentně
významný	významný	k2eAgInSc1d1	významný
<g/>
,	,	kIx,	,
obohacování	obohacování	k1gNnSc1	obohacování
lithia	lithium	k1gNnSc2	lithium
vcelku	vcelku	k6eAd1	vcelku
není	být	k5eNaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
metody	metoda	k1gFnPc1	metoda
<g/>
:	:	kIx,	:
využití	využití	k1gNnSc1	využití
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
afinity	afinita	k1gFnSc2	afinita
6	[number]	k4	6
<g/>
Li	li	k8xS	li
a	a	k8xC	a
7	[number]	k4	7
<g/>
Li	li	k8xS	li
ke	k	k7c3	k
rtuti	rtuť	k1gFnSc3	rtuť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
lithný	lithný	k2eAgInSc1d1	lithný
amalgám	amalgám	k1gInSc1	amalgám
v	v	k7c6	v
protiproudu	protiproud	k1gInSc6	protiproud
k	k	k7c3	k
vodnému	vodný	k2eAgInSc3d1	vodný
roztoku	roztok	k1gInSc3	roztok
LiOH	LiOH	k1gFnSc2	LiOH
obohacuje	obohacovat	k5eAaImIp3nS	obohacovat
lehčím	lehký	k2eAgInSc7d2	lehčí
izotopem	izotop	k1gInSc7	izotop
a	a	k8xC	a
vodná	vodný	k2eAgFnSc1d1	vodná
fáze	fáze	k1gFnSc1	fáze
těžším	těžký	k2eAgNnPc3d2	těžší
díky	díky	k7c3	díky
relativně	relativně	k6eAd1	relativně
vysoké	vysoký	k2eAgFnSc3d1	vysoká
tenzi	tenze	k1gFnSc3	tenze
par	para	k1gFnPc2	para
lithia	lithium	k1gNnSc2	lithium
a	a	k8xC	a
nízkému	nízký	k2eAgInSc3d1	nízký
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
lze	lze	k6eAd1	lze
izotopy	izotop	k1gInPc4	izotop
separovat	separovat	k5eAaBmF	separovat
i	i	k9	i
modifikovanou	modifikovaný	k2eAgFnSc7d1	modifikovaná
destilací	destilace	k1gFnSc7	destilace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
těkající	těkající	k2eAgInPc1d1	těkající
páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
obohaceny	obohacen	k2eAgMnPc4d1	obohacen
lehčím	lehký	k2eAgInSc7d2	lehčí
izotopem	izotop	k1gInSc7	izotop
a	a	k8xC	a
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
7	[number]	k4	7
<g/>
Li	li	k8xS	li
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
technologie	technologie	k1gFnPc1	technologie
mají	mít	k5eAaImIp3nP	mít
pochopitelně	pochopitelně	k6eAd1	pochopitelně
původ	původ	k1gInSc4	původ
v	v	k7c6	v
poválečném	poválečný	k2eAgInSc6d1	poválečný
vojenském	vojenský	k2eAgInSc6d1	vojenský
výzkumu	výzkum	k1gInSc6	výzkum
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
připravit	připravit	k5eAaPmF	připravit
6	[number]	k4	6
<g/>
LiD	Lido	k1gNnPc2	Lido
pro	pro	k7c4	pro
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
neutrony	neutron	k1gInPc4	neutron
neabsorbující	absorbující	k2eNgFnSc2d1	absorbující
soli	sůl	k1gFnSc2	sůl
7	[number]	k4	7
<g/>
Li	li	k8xS	li
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
odpadem	odpad	k1gInSc7	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nS	lišit
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
od	od	k7c2	od
vlastností	vlastnost	k1gFnPc2	vlastnost
ostatních	ostatní	k2eAgInPc2d1	ostatní
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
vlastnostem	vlastnost	k1gFnPc3	vlastnost
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
i	i	k8xC	i
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
proto	proto	k8xC	proto
setkáváme	setkávat	k5eAaImIp1nP	setkávat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
slučuje	slučovat	k5eAaImIp3nS	slučovat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
na	na	k7c4	na
nitrid	nitrid	k1gInSc4	nitrid
lithný	lithný	k2eAgInSc4d1	lithný
Li	li	k8xS	li
<g/>
3	[number]	k4	3
<g/>
N.	N.	kA	N.
Ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
lithium	lithium	k1gNnSc1	lithium
nejméně	málo	k6eAd3	málo
reaktivní	reaktivní	k2eAgNnSc1d1	reaktivní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
alkalický	alkalický	k2eAgInSc1d1	alkalický
kov	kov	k1gInSc1	kov
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
za	za	k7c2	za
vyšší	vysoký	k2eAgFnSc2d2	vyšší
teploty	teplota	k1gFnSc2	teplota
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
na	na	k7c4	na
karbid	karbid	k1gInSc4	karbid
Li	li	k9	li
<g/>
2	[number]	k4	2
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
a	a	k8xC	a
křemíkem	křemík	k1gInSc7	křemík
na	na	k7c4	na
silicid	silicid	k1gInSc4	silicid
Li	li	k8xS	li
<g/>
6	[number]	k4	6
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgNnSc4d1	elementární
kovové	kovový	k2eAgNnSc4d1	kovové
lithium	lithium	k1gNnSc4	lithium
lze	lze	k6eAd1	lze
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchovávat	uchovávat	k5eAaImF	uchovávat
např.	např.	kA	např.
překryté	překrytý	k2eAgFnSc2d1	překrytá
vrstvou	vrstva	k1gFnSc7	vrstva
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
jako	jako	k8xC	jako
petrolej	petrolej	k1gInSc1	petrolej
nebo	nebo	k8xC	nebo
nafta	nafta	k1gFnSc1	nafta
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
alkalické	alkalický	k2eAgInPc4d1	alkalický
kovy	kov	k1gInPc4	kov
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
Li	li	k8xS	li
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
lithia	lithium	k1gNnSc2	lithium
barví	barvit	k5eAaImIp3nP	barvit
plamen	plamen	k1gInSc4	plamen
karmínově	karmínově	k6eAd1	karmínově
červeně	červeně	k6eAd1	červeně
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
švédským	švédský	k2eAgMnSc7d1	švédský
chemikem	chemik	k1gMnSc7	chemik
Johannem	Johann	k1gMnSc7	Johann
Arfvedsonem	Arfvedson	k1gMnSc7	Arfvedson
v	v	k7c6	v
aluminosilikátových	aluminosilikátův	k2eAgFnPc6d1	aluminosilikátův
horninách	hornina	k1gFnPc6	hornina
petalitu	petalit	k1gInSc2	petalit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
lithium	lithium	k1gNnSc1	lithium
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
a	a	k8xC	a
objeveno	objeven	k2eAgNnSc1d1	objeveno
i	i	k8xC	i
ve	v	k7c6	v
spodumenu	spodumen	k1gInSc6	spodumen
a	a	k8xC	a
lepidolitu	lepidolit	k1gInSc6	lepidolit
<g/>
.	.	kIx.	.
</s>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
lithia	lithium	k1gNnSc2	lithium
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
již	již	k9	již
objevenými	objevený	k2eAgInPc7d1	objevený
alkalickými	alkalický	k2eAgInPc7d1	alkalický
kovy	kov	k1gInPc7	kov
zpozoroval	zpozorovat	k5eAaPmAgMnS	zpozorovat
již	již	k6eAd1	již
Johann	Johann	k1gMnSc1	Johann
Arfvedson	Arfvedson	k1gMnSc1	Arfvedson
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
lithos	lithos	k1gInSc1	lithos
-	-	kIx~	-
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgNnSc1d1	Červené
zbarvení	zbarvení	k1gNnSc1	zbarvení
plamene	plamen	k1gInSc2	plamen
lithia	lithium	k1gNnSc2	lithium
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
poprvé	poprvé	k6eAd1	poprvé
Leopold	Leopold	k1gMnSc1	Leopold
Gmelin	Gmelin	k1gInSc4	Gmelin
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgNnSc1d1	čisté
lithium	lithium	k1gNnSc1	lithium
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
připraveno	připraven	k2eAgNnSc1d1	připraveno
Robertem	Robert	k1gMnSc7	Robert
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Bunsenem	Bunsen	k1gMnSc7	Bunsen
a	a	k8xC	a
Michaelem	Michael	k1gMnSc7	Michael
Matthiessenem	Matthiessen	k1gMnSc7	Matthiessen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztaveného	roztavený	k2eAgInSc2d1	roztavený
chloridu	chlorid	k1gInSc2	chlorid
lithného	lithný	k2eAgInSc2d1	lithný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
patří	patřit	k5eAaImIp3nS	patřit
lithium	lithium	k1gNnSc1	lithium
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
atomovou	atomový	k2eAgFnSc4d1	atomová
hmotnost	hmotnost	k1gFnSc4	hmotnost
mezi	mezi	k7c4	mezi
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgInPc4d1	vzácný
prvky	prvek	k1gInPc4	prvek
-	-	kIx~	-
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
jeho	jeho	k3xOp3gMnSc3	jeho
atom	atom	k1gInSc1	atom
připadá	připadat	k5eAaPmIp3nS	připadat
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
miliarda	miliarda	k4xCgFnSc1	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
termonukleárních	termonukleární	k2eAgFnPc6d1	termonukleární
reakcích	reakce	k1gFnPc6	reakce
horkých	horký	k2eAgFnPc2d1	horká
hvězd	hvězda	k1gFnPc2	hvězda
vzniká	vznikat	k5eAaImIp3nS	vznikat
totiž	totiž	k9	totiž
jen	jen	k9	jen
přechodně	přechodně	k6eAd1	přechodně
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
zpětně	zpětně	k6eAd1	zpětně
štěpí	štěpit	k5eAaImIp3nS	štěpit
na	na	k7c4	na
lehčí	lehký	k2eAgInPc4d2	lehčí
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
tudíž	tudíž	k8xC	tudíž
nepochází	pocházet	k5eNaImIp3nS	pocházet
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vznikalo	vznikat	k5eAaImAgNnS	vznikat
až	až	k6eAd1	až
jaderným	jaderný	k2eAgInSc7d1	jaderný
rozpadem	rozpad	k1gInSc7	rozpad
těžších	těžký	k2eAgInPc2d2	těžší
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
lithium	lithium	k1gNnSc1	lithium
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
průměrný	průměrný	k2eAgInSc4d1	průměrný
obsah	obsah	k1gInSc4	obsah
lithia	lithium	k1gNnSc2	lithium
0,18	[number]	k4	0,18
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
elektropozitivitě	elektropozitivita	k1gFnSc3	elektropozitivita
se	se	k3xPyFc4	se
Lithium	lithium	k1gNnSc1	lithium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
jako	jako	k8xS	jako
příměs	příměs	k1gFnSc4	příměs
různých	různý	k2eAgFnPc2d1	různá
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
rudy	ruda	k1gFnPc1	ruda
lithia	lithium	k1gNnSc2	lithium
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
%	%	kIx~	%
lithia	lithium	k1gNnSc2	lithium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc4d3	nejznámější
minerály	minerál	k1gInPc4	minerál
obsahující	obsahující	k2eAgNnSc4d1	obsahující
lithium	lithium	k1gNnSc4	lithium
jsou	být	k5eAaImIp3nP	být
aluminosilikáty	aluminosilikát	k1gInPc1	aluminosilikát
lepidolit	lepidolit	k1gInSc1	lepidolit
KLi	KLi	k1gFnSc1	KLi
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
AlSi	AlS	k1gInSc3	AlS
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
spodumen	spodumen	k1gInSc1	spodumen
LiAl	LiAl	k1gInSc1	LiAl
<g/>
[	[	kIx(	[
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
trifylin	trifylin	k2eAgInSc1d1	trifylin
LiFe	LiFe	k1gInSc1	LiFe
<g/>
[	[	kIx(	[
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
petalit	petalit	k1gInSc1	petalit
(	(	kIx(	(
<g/>
Li	li	k9	li
<g/>
,	,	kIx,	,
Na	na	k7c6	na
<g/>
)	)	kIx)	)
<g/>
AlSi	AlS	k1gInSc6	AlS
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
amblygonit	amblygonit	k1gInSc1	amblygonit
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Li	li	k9	li
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Na	na	k7c4	na
<g/>
)	)	kIx)	)
<g/>
Al	ala	k1gFnPc2	ala
<g/>
(	(	kIx(	(
<g/>
PO	po	k7c6	po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
a	a	k8xC	a
cinvaldit	cinvaldit	k1gInSc1	cinvaldit
<g/>
:	:	kIx,	:
KLiFeAl	KLiFeAl	k1gInSc1	KLiFeAl
<g/>
[	[	kIx(	[
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
|	|	kIx~	|
AlSi	AlS	k1gInSc3	AlS
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc4d3	veliký
zásoby	zásoba	k1gFnPc4	zásoba
lithia	lithium	k1gNnSc2	lithium
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
The	The	k1gFnSc2	The
United	United	k1gInSc1	United
States	States	k1gInSc1	States
Geological	Geological	k1gMnSc2	Geological
Survey	Survea	k1gMnSc2	Survea
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Bolívie	Bolívie	k1gFnSc1	Bolívie
5,4	[number]	k4	5,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Chile	Chile	k1gNnSc1	Chile
3,0	[number]	k4	3,0
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
1,1	[number]	k4	1,1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
0,4	[number]	k4	0,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t	t	k?	t
Polovina	polovina	k1gFnSc1	polovina
známých	známý	k2eAgFnPc2d1	známá
zásob	zásoba	k1gFnPc2	zásoba
lithia	lithium	k1gNnSc2	lithium
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Bolívii	Bolívie	k1gFnSc6	Bolívie
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
solných	solný	k2eAgFnPc2d1	solná
pánví	pánev	k1gFnPc2	pánev
-	-	kIx~	-
největší	veliký	k2eAgFnPc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
Salar	salar	k1gInSc1	salar
de	de	k?	de
Uyuni	Uyuň	k1gMnSc3	Uyuň
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
je	být	k5eAaImIp3nS	být
přítomné	přítomný	k1gMnPc4	přítomný
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
organismů	organismus	k1gInPc2	organismus
jen	jen	k9	jen
ve	v	k7c6	v
stopovém	stopový	k2eAgInSc6d1	stopový
(	(	kIx(	(
<g/>
extrémně	extrémně	k6eAd1	extrémně
nízkém	nízký	k2eAgNnSc6d1	nízké
<g/>
)	)	kIx)	)
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tam	tam	k6eAd1	tam
chemicky	chemicky	k6eAd1	chemicky
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jiné	jiný	k2eAgInPc1d1	jiný
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
přítomné	přítomný	k2eAgInPc1d1	přítomný
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
lithium	lithium	k1gNnSc1	lithium
v	v	k7c4	v
koncentraci	koncentrace	k1gFnSc4	koncentrace
pouhých	pouhý	k2eAgInPc2d1	pouhý
cca	cca	kA	cca
70	[number]	k4	70
nmol	nmol	k1gInSc4	nmol
<g/>
/	/	kIx~	/
<g/>
litr	litr	k1gInSc4	litr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
organizmu	organizmus	k1gInSc2	organizmus
nějaký	nějaký	k3yIgInSc4	nějaký
význam	význam	k1gInSc4	význam
<g/>
;	;	kIx,	;
koncentrace	koncentrace	k1gFnSc1	koncentrace
lithia	lithium	k1gNnSc2	lithium
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
maniodepresivity	maniodepresivita	k1gFnSc2	maniodepresivita
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
3-4	[number]	k4	3-4
řády	řád	k1gInPc1	řád
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
přirozená	přirozený	k2eAgFnSc1d1	přirozená
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
experimentálním	experimentální	k2eAgNnSc6d1	experimentální
krmení	krmení	k1gNnSc6	krmení
zvířat	zvíře	k1gNnPc2	zvíře
potravou	potrava	k1gFnSc7	potrava
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
lithia	lithium	k1gNnSc2	lithium
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
některé	některý	k3yIgInPc1	některý
vývojové	vývojový	k2eAgInPc1d1	vývojový
poruchy	poruch	k1gInPc1	poruch
a	a	k8xC	a
snížený	snížený	k2eAgInSc1d1	snížený
věk	věk	k1gInSc1	věk
dožití	dožití	k1gNnSc1	dožití
Lithium	lithium	k1gNnSc1	lithium
také	také	k9	také
v	v	k7c6	v
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
růst	růst	k1gInSc1	růst
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
aktivně	aktivně	k6eAd1	aktivně
zakoncentrovávají	zakoncentrovávat	k5eAaImIp3nP	zakoncentrovávat
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
pletivech	pletivo	k1gNnPc6	pletivo
až	až	k9	až
do	do	k7c2	do
hladiny	hladina	k1gFnSc2	hladina
1	[number]	k4	1
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
spodumenu	spodumen	k1gInSc2	spodumen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
na	na	k7c4	na
1	[number]	k4	1
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
modifikace	modifikace	k1gFnSc2	modifikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
promývá	promývat	k5eAaImIp3nS	promývat
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
při	při	k7c6	při
250	[number]	k4	250
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
z	z	k7c2	z
výluhu	výluh	k1gInSc2	výluh
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
síran	síran	k1gInSc1	síran
lithný	lithný	k2eAgInSc1d1	lithný
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
uhličitanem	uhličitan	k1gInSc7	uhličitan
sodným	sodný	k2eAgInSc7d1	sodný
a	a	k8xC	a
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
nerozpustného	rozpustný	k2eNgInSc2d1	nerozpustný
uhličitanu	uhličitan	k1gInSc2	uhličitan
lithného	lithný	k2eAgInSc2d1	lithný
a	a	k8xC	a
rozpustného	rozpustný	k2eAgInSc2d1	rozpustný
chloridu	chlorid	k1gInSc2	chlorid
lithného	lithný	k2eAgInSc2d1	lithný
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
lithný	lithný	k2eAgInSc1d1	lithný
se	se	k3xPyFc4	se
kompletně	kompletně	k6eAd1	kompletně
převede	převést	k5eAaPmIp3nS	převést
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1	kovové
lithium	lithium	k1gNnSc1	lithium
lze	lze	k6eAd1	lze
průmyslově	průmyslově	k6eAd1	průmyslově
nejsnáze	snadno	k6eAd3	snadno
připravit	připravit	k5eAaPmF	připravit
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztaveného	roztavený	k2eAgInSc2d1	roztavený
chloridu	chlorid	k1gInSc2	chlorid
lithného	lithný	k2eAgInSc2d1	lithný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
čistý	čistý	k2eAgInSc1d1	čistý
chlorid	chlorid	k1gInSc1	chlorid
nejlépe	dobře	k6eAd3	dobře
získatelný	získatelný	k2eAgInSc1d1	získatelný
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
lithia	lithium	k1gNnSc2	lithium
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
i	i	k9	i
snadněji	snadno	k6eAd2	snadno
tavitelnou	tavitelný	k2eAgFnSc4d1	tavitelná
směs	směs	k1gFnSc4	směs
chloridu	chlorid	k1gInSc2	chlorid
lithného	lithný	k2eAgInSc2d1	lithný
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Železná	železný	k2eAgFnSc1d1	železná
katoda	katoda	k1gFnSc1	katoda
2	[number]	k4	2
Li	li	k8xS	li
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
e	e	k0	e
<g/>
-	-	kIx~	-
→	→	k?	→
2	[number]	k4	2
Li	li	k8xS	li
Grafitová	grafitový	k2eAgFnSc1d1	grafitová
anoda	anoda	k1gFnSc1	anoda
2	[number]	k4	2
Cl	Cl	k1gFnPc2	Cl
<g/>
-	-	kIx~	-
→	→	k?	→
Cl	Cl	k1gFnSc7	Cl
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
e	e	k0	e
<g/>
-	-	kIx~	-
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
lze	lze	k6eAd1	lze
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
lithia	lithium	k1gNnSc2	lithium
použít	použít	k5eAaPmF	použít
i	i	k9	i
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
chloridu	chlorid	k1gInSc2	chlorid
lithného	lithný	k2eAgNnSc2d1	lithné
rozpuštěného	rozpuštěný	k2eAgNnSc2d1	rozpuštěné
v	v	k7c6	v
pyridinu	pyridin	k1gInSc6	pyridin
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgNnSc1d1	elementární
lithium	lithium	k1gNnSc1	lithium
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
typech	typ	k1gInPc6	typ
reaktorů	reaktor	k1gInPc2	reaktor
slouží	sloužit	k5eAaImIp3nP	sloužit
roztavené	roztavený	k2eAgNnSc4d1	roztavené
lithium	lithium	k1gNnSc4	lithium
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
lithiové	lithiový	k2eAgFnPc4d1	lithiová
baterie	baterie	k1gFnPc4	baterie
a	a	k8xC	a
akumulátory	akumulátor	k1gInPc4	akumulátor
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
perspektivním	perspektivní	k2eAgInPc3d1	perspektivní
prostředkům	prostředek	k1gInPc3	prostředek
pro	pro	k7c4	pro
dlouhodobější	dlouhodobý	k2eAgNnSc4d2	dlouhodobější
uchování	uchování	k1gNnSc4	uchování
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
stále	stále	k6eAd1	stále
silně	silně	k6eAd1	silně
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Elektrody	elektroda	k1gFnPc1	elektroda
akumulátoru	akumulátor	k1gInSc2	akumulátor
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
na	na	k7c6	na
záporné	záporný	k2eAgFnSc6d1	záporná
elektrodě	elektroda	k1gFnSc6	elektroda
slitinu	slitina	k1gFnSc4	slitina
Li	li	k8xS	li
<g/>
/	/	kIx~	/
<g/>
Si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
na	na	k7c6	na
kladné	kladný	k2eAgFnSc6d1	kladná
elektrodě	elektroda	k1gFnSc6	elektroda
je	být	k5eAaImIp3nS	být
FeSx	FeSx	k1gInSc4	FeSx
a	a	k8xC	a
jako	jako	k9	jako
elektrolyt	elektrolyt	k1gInSc1	elektrolyt
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
roztavený	roztavený	k2eAgInSc1d1	roztavený
LiCl	LiCl	k1gInSc1	LiCl
<g/>
/	/	kIx~	/
<g/>
KCl	KCl	k1gFnSc1	KCl
při	při	k7c6	při
400	[number]	k4	400
°	°	k?	°
<g/>
C.	C.	kA	C.
Tento	tento	k3xDgInSc4	tento
akumulátor	akumulátor	k1gInSc4	akumulátor
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
další	další	k2eAgInPc1d1	další
nové	nový	k2eAgInPc1d1	nový
typy	typ	k1gInPc1	typ
<g/>
.	.	kIx.	.
</s>
<s>
Lithiové	lithiový	k2eAgInPc1d1	lithiový
akumulátory	akumulátor	k1gInPc1	akumulátor
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
v	v	k7c6	v
elektromobilech	elektromobil	k1gInPc6	elektromobil
a	a	k8xC	a
automobilech	automobil	k1gInPc6	automobil
s	s	k7c7	s
hybridními	hybridní	k2eAgInPc7d1	hybridní
motory	motor	k1gInPc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
soli	sůl	k1gFnPc1	sůl
lithia	lithium	k1gNnSc2	lithium
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
průmyslu	průmysl	k1gInSc6	průmysl
jako	jako	k8xS	jako
součásti	součást	k1gFnSc3	součást
uklidňujících	uklidňující	k2eAgInPc2d1	uklidňující
léků	lék	k1gInPc2	lék
tlumících	tlumící	k2eAgInPc2d1	tlumící
afekt	afekt	k1gInSc4	afekt
<g/>
.	.	kIx.	.
</s>
<s>
Lithium	lithium	k1gNnSc1	lithium
je	být	k5eAaImIp3nS	být
přísadou	přísada	k1gFnSc7	přísada
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
speciálních	speciální	k2eAgNnPc2d1	speciální
skel	sklo	k1gNnPc2	sklo
a	a	k8xC	a
keramik	keramika	k1gFnPc2	keramika
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
hvězdářských	hvězdářský	k2eAgInPc2d1	hvězdářský
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
silných	silný	k2eAgFnPc2d1	silná
hygroskopických	hygroskopický	k2eAgFnPc2d1	hygroskopická
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
nízké	nízký	k2eAgFnSc3d1	nízká
relativní	relativní	k2eAgFnSc3d1	relativní
hmotnosti	hmotnost	k1gFnSc3	hmotnost
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
pohlcování	pohlcování	k1gNnSc3	pohlcování
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
z	z	k7c2	z
vydýchaného	vydýchaný	k2eAgInSc2d1	vydýchaný
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
ponorkách	ponorka	k1gFnPc6	ponorka
a	a	k8xC	a
kosmických	kosmický	k2eAgFnPc6d1	kosmická
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc4	slitina
lithia	lithium	k1gNnSc2	lithium
s	s	k7c7	s
hliníkem	hliník	k1gInSc7	hliník
<g/>
,	,	kIx,	,
kadmiem	kadmium	k1gNnSc7	kadmium
<g/>
,	,	kIx,	,
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
manganem	mangan	k1gInSc7	mangan
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
lehké	lehký	k2eAgFnPc1d1	lehká
a	a	k8xC	a
současně	současně	k6eAd1	současně
značně	značně	k6eAd1	značně
mechanicky	mechanicky	k6eAd1	mechanicky
odolné	odolný	k2eAgFnPc1d1	odolná
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
součástí	součást	k1gFnPc2	součást
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
družic	družice	k1gFnPc2	družice
<g/>
,	,	kIx,	,
kosmických	kosmický	k2eAgFnPc2d1	kosmická
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
ložiskových	ložiskový	k2eAgInPc2d1	ložiskový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Slitina	slitina	k1gFnSc1	slitina
lithia	lithium	k1gNnSc2	lithium
s	s	k7c7	s
hořčíkem	hořčík	k1gInSc7	hořčík
a	a	k8xC	a
hliníkem	hliník	k1gInSc7	hliník
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
pancéřové	pancéřový	k2eAgFnPc4d1	pancéřová
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
družic	družice	k1gFnPc2	družice
a	a	k8xC	a
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
složení	složení	k1gNnSc1	složení
14	[number]	k4	14
%	%	kIx~	%
lithia	lithium	k1gNnPc4	lithium
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
85	[number]	k4	85
%	%	kIx~	%
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
lithný	lithný	k2eAgInSc1d1	lithný
a	a	k8xC	a
hydroxid	hydroxid	k1gInSc1	hydroxid
lithný	lithný	k2eAgInSc1d1	lithný
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
práškovitých	práškovitý	k2eAgFnPc2d1	práškovitá
fotografických	fotografický	k2eAgFnPc2d1	fotografická
vývojek	vývojka	k1gFnPc2	vývojka
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
lithný	lithný	k2eAgMnSc1d1	lithný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
vodíku	vodík	k1gInSc2	vodík
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
a	a	k8xC	a
meteorologické	meteorologický	k2eAgInPc4d1	meteorologický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
jako	jako	k8xC	jako
tetrahydridohlinitan	tetrahydridohlinitan	k1gInSc4	tetrahydridohlinitan
lithný	lithný	k2eAgInSc4d1	lithný
LiAlH	LiAlH	k1gFnSc7	LiAlH
<g/>
4	[number]	k4	4
a	a	k8xC	a
organolithná	organolithný	k2eAgNnPc1d1	organolithný
činidla	činidlo	k1gNnPc1	činidlo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
chemii	chemie	k1gFnSc6	chemie
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgNnPc4d1	známé
redukovadla	redukovadlo	k1gNnPc4	redukovadlo
<g/>
.	.	kIx.	.
</s>
<s>
Citronan	citronan	k1gInSc1	citronan
lithný	lithný	k2eAgInSc1d1	lithný
-	-	kIx~	-
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
uhličitan	uhličitan	k1gInSc4	uhličitan
lithný	lithný	k2eAgInSc1d1	lithný
Síran	síran	k1gInSc1	síran
lithný	lithný	k2eAgInSc1d1	lithný
-	-	kIx~	-
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
uhličitan	uhličitan	k1gInSc4	uhličitan
lithný	lithný	k2eAgInSc1d1	lithný
Orotát	Orotát	k1gInSc1	Orotát
lithný	lithný	k2eAgInSc1d1	lithný
-	-	kIx~	-
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
uhličitan	uhličitan	k1gInSc4	uhličitan
lithný	lithný	k2eAgInSc1d1	lithný
Jantaran	Jantaran	k1gInSc1	Jantaran
lithný	lithný	k2eAgInSc1d1	lithný
-	-	kIx~	-
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k8xC	jako
dermatologikum	dermatologikum	k1gNnSc1	dermatologikum
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
seboroické	seboroický	k2eAgFnSc2d1	seboroická
dermatitidy	dermatitida	k1gFnSc2	dermatitida
Stearát	Stearát	k1gInSc1	Stearát
lithný	lithný	k2eAgInSc1d1	lithný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
zahušťovadlo	zahušťovadlo	k1gNnSc1	zahušťovadlo
a	a	k8xC	a
želatinová	želatinový	k2eAgFnSc1d1	želatinová
látka	látka	k1gFnSc1	látka
k	k	k7c3	k
převádění	převádění	k1gNnSc3	převádění
olejů	olej	k1gInPc2	olej
na	na	k7c4	na
plastická	plastický	k2eAgNnPc4d1	plastické
maziva	mazivo	k1gNnPc4	mazivo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
maziva	mazivo	k1gNnPc1	mazivo
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
dobré	dobrý	k2eAgFnPc4d1	dobrá
nízkoteplotní	nízkoteplotní	k2eAgFnPc4d1	nízkoteplotní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
stálost	stálost	k1gFnSc4	stálost
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
teplotách	teplota	k1gFnPc6	teplota
(	(	kIx(	(
<g/>
>	>	kIx)	>
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zahušťovadla	zahušťovadlo	k1gNnPc1	zahušťovadlo
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
z	z	k7c2	z
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
a	a	k8xC	a
přírodních	přírodní	k2eAgInPc2d1	přírodní
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
lithný	lithný	k2eAgInSc1d1	lithný
Li	li	k8xS	li
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
porcelánu	porcelán	k1gInSc2	porcelán
jako	jako	k8xS	jako
tavidlo	tavidlo	k1gNnSc1	tavidlo
ve	v	k7c6	v
smaltech	smalt	k1gInPc6	smalt
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgNnPc2d1	speciální
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
skel	sklo	k1gNnPc2	sklo
a	a	k8xC	a
jako	jako	k9	jako
lék	lék	k1gInSc1	lék
profylakticky	profylakticky	k6eAd1	profylakticky
ke	k	k7c3	k
kupírování	kupírování	k1gNnSc3	kupírování
manické	manický	k2eAgFnSc2d1	manická
fáze	fáze	k1gFnSc2	fáze
bipolární	bipolární	k2eAgFnSc2d1	bipolární
deprese	deprese	k1gFnSc2	deprese
(	(	kIx(	(
<g/>
maniodepresivní	maniodepresivní	k2eAgFnSc2d1	maniodepresivní
psychózy	psychóza	k1gFnSc2	psychóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
uhličitanu	uhličitan	k1gInSc3	uhličitan
lithného	lithný	k2eAgInSc2d1	lithný
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
bauxitu	bauxit	k1gInSc2	bauxit
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
průtok	průtok	k1gInSc4	průtok
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
lithný	lithný	k2eAgInSc1d1	lithný
LiH	LiH	k1gFnSc4	LiH
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
hydridů	hydrid	k1gInPc2	hydrid
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stálý	stálý	k2eAgInSc1d1	stálý
(	(	kIx(	(
<g/>
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
složkou	složka	k1gFnSc7	složka
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
hydrid	hydrid	k1gInSc1	hydrid
lithný	lithný	k2eAgInSc1d1	lithný
velmi	velmi	k6eAd1	velmi
bouřlivě	bouřlivě	k6eAd1	bouřlivě
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
lithný	lithný	k2eAgMnSc1d1	lithný
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
mírně	mírně	k6eAd1	mírně
zahřátého	zahřátý	k2eAgNnSc2d1	zahřáté
lithia	lithium	k1gNnSc2	lithium
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
Oxid	oxid	k1gInSc4	oxid
lithný	lithný	k2eAgInSc4d1	lithný
Li	li	k8xS	li
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
lithia	lithium	k1gNnSc2	lithium
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
značně	značně	k6eAd1	značně
znečištěn	znečistit	k5eAaPmNgInS	znečistit
peroxidem	peroxid	k1gInSc7	peroxid
lithným	lithný	k2eAgInSc7d1	lithný
Li	li	k8xS	li
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
čistého	čistý	k2eAgInSc2d1	čistý
oxidu	oxid	k1gInSc2	oxid
lithného	lithný	k2eAgInSc2d1	lithný
používá	používat	k5eAaImIp3nS	používat
termický	termický	k2eAgInSc1d1	termický
rozklad	rozklad	k1gInSc1	rozklad
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
lithného	lithný	k2eAgInSc2d1	lithný
nebo	nebo	k8xC	nebo
dusičnanu	dusičnan	k1gInSc2	dusičnan
lithného	lithný	k2eAgInSc2d1	lithný
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
lithný	lithný	k2eAgInSc1d1	lithný
LiOH	LiOH	k1gFnSc4	LiOH
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
silně	silně	k6eAd1	silně
zásaditá	zásaditý	k2eAgFnSc1d1	zásaditá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
alkalických	alkalický	k2eAgInPc2d1	alkalický
hydroxidů	hydroxid	k1gInPc2	hydroxid
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
lihu	líh	k1gInSc6	líh
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
hůře	zle	k6eAd2	zle
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
lithného	lithný	k2eAgMnSc4d1	lithný
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
kovového	kovový	k2eAgNnSc2d1	kovové
lithia	lithium	k1gNnSc2	lithium
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
bouřlivá	bouřlivý	k2eAgFnSc1d1	bouřlivá
a	a	k8xC	a
exotermní	exotermní	k2eAgFnSc1d1	exotermní
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
uvedeného	uvedený	k2eAgInSc2d1	uvedený
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
dochází	docházet	k5eAaImIp3nS	docházet
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
reaguje	reagovat	k5eAaBmIp3nS	reagovat
lithium	lithium	k1gNnSc1	lithium
za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
nitridu	nitrid	k1gInSc2	nitrid
lithného	lithný	k2eAgInSc2d1	lithný
Li	li	k9	li
<g/>
3	[number]	k4	3
<g/>
N.	N.	kA	N.
Uvedené	uvedený	k2eAgFnSc2d1	uvedená
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
dusíku	dusík	k1gInSc2	dusík
z	z	k7c2	z
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Nitrid	nitrid	k1gInSc1	nitrid
lithný	lithný	k2eAgInSc1d1	lithný
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
štěpí	štěpit	k5eAaImIp3nP	štěpit
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
lithný	lithný	k2eAgInSc4d1	lithný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
<g/>
,	,	kIx,	,
a	a	k8xC	a
amoniaku	amoniak	k1gInSc2	amoniak
Borohydrid	Borohydrid	k1gInSc4	Borohydrid
lithný	lithný	k2eAgInSc4d1	lithný
LiBH	LiBH	k1gFnSc7	LiBH
<g/>
4	[number]	k4	4
tetrahydridoboritan	tetrahydridoboritan	k1gInSc1	tetrahydridoboritan
lithný	lithný	k2eAgInSc1d1	lithný
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpoužívanějších	používaný	k2eAgFnPc2d3	nejpoužívanější
sloučeninách	sloučenina	k1gFnPc6	sloučenina
lithia	lithium	k1gNnSc2	lithium
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
atomární	atomární	k2eAgNnSc1d1	atomární
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
tak	tak	k9	tak
využití	využití	k1gNnSc1	využití
jako	jako	k8xS	jako
hydrogenační	hydrogenační	k2eAgInSc1d1	hydrogenační
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
účinné	účinný	k2eAgNnSc1d1	účinné
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Lithné	lithný	k2eAgFnPc1d1	lithná
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
však	však	k9	však
solí	sůl	k1gFnPc2	sůl
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
obecně	obecně	k6eAd1	obecně
nejméně	málo	k6eAd3	málo
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
u	u	k7c2	u
lithných	lithný	k2eAgFnPc2d1	lithná
solí	sůl	k1gFnPc2	sůl
tvoří	tvořit	k5eAaImIp3nS	tvořit
chlorečnan	chlorečnan	k1gInSc1	chlorečnan
lithný	lithný	k2eAgInSc1d1	lithný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nejrozpustnější	rozpustný	k2eAgFnSc7d3	rozpustný
anorganickou	anorganický	k2eAgFnSc7d1	anorganická
látkou	látka	k1gFnSc7	látka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
-	-	kIx~	-
313,5	[number]	k4	313,5
g	g	kA	g
ve	v	k7c4	v
100	[number]	k4	100
ml	ml	kA	ml
při	při	k7c6	při
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
však	však	k9	však
lithné	lithný	k2eAgFnPc1d1	lithná
soli	sůl	k1gFnPc1	sůl
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
polárních	polární	k2eAgNnPc6d1	polární
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kapalný	kapalný	k2eAgInSc1d1	kapalný
amoniak	amoniak	k1gInSc1	amoniak
nebo	nebo	k8xC	nebo
líh	líh	k1gInSc1	líh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
lithný	lithný	k2eAgInSc1d1	lithný
LiF	LiF	k1gFnSc4	LiF
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
uhličitanu	uhličitan	k1gInSc2	uhličitan
lithného	lithný	k2eAgInSc2d1	lithný
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgMnSc4d1	lithný
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
fluorovodíkovou	fluorovodíkový	k2eAgFnSc7d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
lithný	lithný	k2eAgInSc4d1	lithný
LiCl	LiCl	k1gInSc4	LiCl
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozplývá	rozplývat	k5eAaImIp3nS	rozplývat
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
lihu	líh	k1gInSc3	líh
i	i	k9	i
směsi	směs	k1gFnPc4	směs
lihu	líh	k1gInSc2	líh
a	a	k8xC	a
etheru	ether	k1gInSc6	ether
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
polárních	polární	k2eAgNnPc6d1	polární
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
uhličitanu	uhličitan	k1gInSc2	uhličitan
lithného	lithný	k2eAgInSc2d1	lithný
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgMnSc4d1	lithný
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
lithný	lithný	k2eAgInSc4d1	lithný
LiBr	LiBr	k1gInSc4	LiBr
i	i	k8xC	i
jodid	jodid	k1gInSc4	jodid
lithný	lithný	k2eAgInSc4d1	lithný
LiI	LiI	k1gFnSc7	LiI
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
<g/>
,	,	kIx,	,
krystalické	krystalický	k2eAgFnSc2d1	krystalická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
silně	silně	k6eAd1	silně
hygroskopické	hygroskopický	k2eAgFnPc1d1	hygroskopická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
s	s	k7c7	s
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
náplně	náplň	k1gFnPc1	náplň
exikátorů	exikátor	k1gMnPc2	exikátor
<g/>
.	.	kIx.	.
</s>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
lithného	lithný	k2eAgInSc2d1	lithný
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
bromovodíkové	bromovodíkový	k2eAgFnSc6d1	bromovodíkový
popř.	popř.	kA	popř.
kyselině	kyselina	k1gFnSc6	kyselina
jodovodíkové	jodovodíkový	k2eAgFnSc6d1	jodovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
lithný	lithný	k2eAgInSc1d1	lithný
LiNO	Lina	k1gFnSc5	Lina
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
rozplývavá	rozplývavý	k2eAgFnSc1d1	rozplývavá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
docela	docela	k6eAd1	docela
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
lithná	lithný	k2eAgFnSc1d1	lithná
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
na	na	k7c4	na
uhličitan	uhličitan	k1gInSc4	uhličitan
lithný	lithný	k2eAgInSc4d1	lithný
nebo	nebo	k8xC	nebo
hydroxid	hydroxid	k1gInSc4	hydroxid
lithný	lithný	k2eAgInSc4d1	lithný
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
lithný	lithný	k2eAgInSc1d1	lithný
Li	li	k9	li
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
uhličitan	uhličitan	k1gInSc1	uhličitan
alkalického	alkalický	k2eAgInSc2d1	alkalický
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
srážením	srážení	k1gNnSc7	srážení
roztoku	roztok	k1gInSc2	roztok
lithné	lithný	k2eAgFnSc2d1	lithná
soli	sůl	k1gFnSc2	sůl
roztokem	roztok	k1gInSc7	roztok
rozpustného	rozpustný	k2eAgInSc2d1	rozpustný
uhličitanu	uhličitan	k1gInSc2	uhličitan
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnPc2	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgInSc2d1	lithný
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
lithný	lithný	k2eAgInSc1d1	lithný
Li	li	k9	li
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
uhličitanu	uhličitan	k1gInSc2	uhličitan
lithného	lithný	k2eAgInSc2d1	lithný
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
lithného	lithný	k2eAgMnSc4d1	lithný
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
uhličitan	uhličitan	k1gInSc1	uhličitan
lithný	lithný	k2eAgInSc1d1	lithný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
lithia	lithium	k1gNnSc2	lithium
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
lithné	lithný	k2eAgFnPc1d1	lithná
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
lithné	lithný	k2eAgInPc4d1	lithný
alkoholáty	alkoholát	k1gInPc4	alkoholát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
lithným	lithný	k2eAgFnPc3d1	lithná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
organické	organický	k2eAgInPc4d1	organický
komplexy	komplex	k1gInPc4	komplex
lithných	lithný	k2eAgFnPc2d1	lithná
sloučenin	sloučenina	k1gFnPc2	sloučenina
tzv.	tzv.	kA	tzv.
crowny	crowna	k1gFnSc2	crowna
a	a	k8xC	a
kryptáty	kryptát	k1gInPc4	kryptát
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
organických	organický	k2eAgFnPc2d1	organická
lithných	lithný	k2eAgFnPc2d1	lithná
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1	kovové
lithium	lithium	k1gNnSc1	lithium
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
a	a	k8xC	a
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
i	i	k9	i
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
<g/>
,	,	kIx,	,
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
petroleji	petrolej	k1gInSc6	petrolej
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
lithiem	lithium	k1gNnSc7	lithium
se	se	k3xPyFc4	se
musejí	muset	k5eAaImIp3nP	muset
používat	používat	k5eAaImF	používat
ochranné	ochranný	k2eAgFnPc4d1	ochranná
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lithium	lithium	k1gNnSc1	lithium
nezreagovalo	zreagovat	k5eNaPmAgNnS	zreagovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kůže	kůže	k1gFnSc2	kůže
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
lithný	lithný	k2eAgInSc4d1	lithný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vdechování	vdechování	k1gNnSc6	vdechování
prachu	prach	k1gInSc2	prach
kovového	kovový	k2eAgNnSc2d1	kovové
lithia	lithium	k1gNnSc2	lithium
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podráždění	podráždění	k1gNnSc3	podráždění
či	či	k8xC	či
až	až	k9	až
k	k	k7c3	k
bolesti	bolest	k1gFnSc3	bolest
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
taky	taky	k6eAd1	taky
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
plicní	plicní	k2eAgInSc4d1	plicní
edém	edém	k1gInSc4	edém
<g/>
.	.	kIx.	.
</s>
