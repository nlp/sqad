<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
indický	indický	k2eAgMnSc1d1
císař	císař	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1902	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1841	#num#	k4
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
<g/>
,	,	kIx,
Windsorský	windsorský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Albert	Albert	k1gMnSc1
ViktorJiří	ViktorJiří	k1gNnSc2
V.	V.	kA
LouiseViktorieMaudAlexander	LouiseViktorieMaudAlexander	k1gMnSc1
John	John	k1gMnSc1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Sachsen-Coburg	Sachsen-Coburg	k1gInSc1
und	und	k?
Gotha	Gotha	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1841	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
a	a	k8xC
britských	britský	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
a	a	k8xC
císař	císař	k1gMnSc1
Indie	Indie	k1gFnSc2
od	od	k7c2
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Sachsen-Coburg	Sachsen-Coburg	k1gInSc1
und	und	k?
Gotha	Gotha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gMnSc7
následníkem	následník	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
V.	V.	kA
posléze	posléze	k6eAd1
přejmenována	přejmenován	k2eAgFnSc1d1
na	na	k7c4
dynastii	dynastie	k1gFnSc4
windsorskou	windsorský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Eduardovské	Eduardovský	k2eAgNnSc1d1
období	období	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
zahrnovalo	zahrnovat	k5eAaImAgNnS
dobu	doba	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
časově	časově	k6eAd1
shodovalo	shodovat	k5eAaImAgNnS
s	s	k7c7
počátkem	počátek	k1gInSc7
nového	nový	k2eAgNnSc2d1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
přineslo	přinést	k5eAaPmAgNnS
významné	významný	k2eAgFnPc4d1
změny	změna	k1gFnPc4
v	v	k7c6
technologiích	technologie	k1gFnPc6
a	a	k8xC
vědě	věda	k1gFnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
vzniku	vznik	k1gInSc2
letectví	letectví	k1gNnSc2
<g/>
,	,	kIx,
nárůstu	nárůst	k1gInSc2
socialismu	socialismus	k1gInSc2
a	a	k8xC
dělnického	dělnický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
sehrál	sehrát	k5eAaPmAgMnS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
rozvoji	rozvoj	k1gInSc6
floty	flota	k1gFnSc2
britského	britský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgFnSc2d1
zdravotní	zdravotní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
a	a	k8xC
reorganizace	reorganizace	k1gFnSc2
britské	britský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
búrské	búrský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporoval	podporovat	k5eAaImAgInS
dobré	dobrý	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
a	a	k8xC
jinými	jiný	k2eAgInPc7d1
evropskými	evropský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
s	s	k7c7
Francií	Francie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgInS
celkově	celkově	k6eAd1
devětkrát	devětkrát	k6eAd1
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
převážně	převážně	k6eAd1
vystupoval	vystupovat	k5eAaImAgMnS
pod	pod	k7c7
titulem	titul	k1gInSc7
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Lancasteru	Lancaster	k1gInSc2
(	(	kIx(
<g/>
Duke	Duke	k1gInSc1
of	of	k?
Lancaster	Lancaster	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Svatební	svatební	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
prince	princ	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Alexandry	Alexandra	k1gFnSc2
Dánské	dánský	k2eAgFnSc2d1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1841	#num#	k4
v	v	k7c6
Buckinghamském	buckinghamský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
jako	jako	k8xS,k8xC
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
Alberta	Albert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pokřtěn	pokřtít	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
Albert	Albert	k1gMnSc1
Edward	Edward	k1gMnSc1
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1842	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Albert	Alberta	k1gFnPc2
bylo	být	k5eAaImAgNnS
po	po	k7c6
jeho	jeho	k3xOp3gMnSc6
otci	otec	k1gMnSc6
a	a	k8xC
Edward	Edward	k1gMnSc1
po	po	k7c6
dědečkovi	dědeček	k1gMnSc3
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
nejstaršímu	starý	k2eAgMnSc3d3
synovi	syn	k1gMnSc3
a	a	k8xC
dědici	dědic	k1gMnSc3
trůnu	trůn	k1gInSc2
(	(	kIx(
<g/>
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
)	)	kIx)
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
krátce	krátce	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc6
udělen	udělen	k2eAgInSc4d1
tradiční	tradiční	k2eAgInSc4d1
titul	titul	k1gInSc4
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
(	(	kIx(
<g/>
Prince	princ	k1gMnSc2
of	of	k?
Wales	Wales	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1863	#num#	k4
se	se	k3xPyFc4
vzdal	vzdát	k5eAaPmAgInS
následnictví	následnictví	k1gNnPc4
pro	pro	k7c4
vévodství	vévodství	k1gNnSc4
Sasko-Kobursko-Gothajské	Sasko-Kobursko-Gothajský	k2eAgNnSc4d1
(	(	kIx(
<g/>
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
)	)	kIx)
ve	v	k7c4
prospěch	prospěch	k1gInSc4
svého	svůj	k3xOyFgMnSc2
mladšího	mladý	k2eAgMnSc2d2
bratra	bratr	k1gMnSc2
Alfréda	Alfréd	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Eduard	Eduard	k1gMnSc1
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
vzdělání	vzdělání	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
by	by	kYmCp3nP
ho	on	k3xPp3gNnSc4
připravilo	připravit	k5eAaPmAgNnS
na	na	k7c6
roli	role	k1gFnSc6
britského	britský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
studijní	studijní	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
nebyly	být	k5eNaImAgInP
příliš	příliš	k6eAd1
dobré	dobrý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduardovy	Eduardův	k2eAgFnPc1d1
kvality	kvalita	k1gFnPc1
spočívaly	spočívat	k5eAaImAgFnP
spíše	spíše	k9
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
šarmu	šarm	k1gInSc6
<g/>
,	,	kIx,
společenskosti	společenskost	k1gFnSc6
a	a	k8xC
taktu	takt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krátkém	krátký	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
v	v	k7c6
Římě	Řím	k1gInSc6
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
studiu	studio	k1gNnSc6
na	na	k7c6
Edinburské	Edinburský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
později	pozdě	k6eAd2
na	na	k7c4
Christ	Christ	k1gInSc4
Church	Churcha	k1gFnPc2
College	Colleg	k1gInSc2
v	v	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1860	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
jako	jako	k9
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
cestu	cesta	k1gFnSc4
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
smysl	smysl	k1gInSc4
pro	pro	k7c4
humor	humor	k1gInSc4
a	a	k8xC
vybrané	vybraný	k2eAgNnSc1d1
společenské	společenský	k2eAgNnSc1d1
chování	chování	k1gNnSc1
podpořilo	podpořit	k5eAaPmAgNnS
skvělý	skvělý	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
cesty	cesta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdekoli	kdekoli	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
Americe	Amerika	k1gFnSc6
objevil	objevit	k5eAaPmAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vítán	vítat	k5eAaImNgInS
nadšenými	nadšený	k2eAgInPc7d1
davy	dav	k1gInPc7
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
návštěva	návštěva	k1gFnSc1
přispěla	přispět	k5eAaPmAgFnS
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
a	a	k8xC
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
k	k	k7c3
posílení	posílení	k1gNnSc3
Eduardovy	Eduardův	k2eAgFnSc2d1
sebedůvěry	sebedůvěra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
svém	svůj	k3xOyFgInSc6
návratu	návrat	k1gInSc6
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
věnovat	věnovat	k5eAaImF,k5eAaPmF
kariéře	kariéra	k1gFnSc3
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k8xC,k8xS
následníkovi	následník	k1gMnSc3
trůnu	trůn	k1gInSc2
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
znemožněno	znemožněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1861	#num#	k4
byl	být	k5eAaImAgInS
vyslán	vyslat	k5eAaPmNgInS
do	do	k7c2
Německa	Německo	k1gNnSc2
zdánlivě	zdánlivě	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
sledoval	sledovat	k5eAaImAgMnS
vojenské	vojenský	k2eAgNnSc4d1
cvičení	cvičení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc1
setkání	setkání	k1gNnSc1
s	s	k7c7
princeznou	princezna	k1gFnSc7
Alexandrou	Alexandra	k1gFnSc7
Dánskou	dánský	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
setkání	setkání	k1gNnSc1
bylo	být	k5eAaImAgNnS
zorganizováno	zorganizovat	k5eAaPmNgNnS
na	na	k7c4
popud	popud	k1gInSc4
jeho	jeho	k3xOp3gMnPc2
rodičů	rodič	k1gMnPc2
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
a	a	k8xC
prince	princ	k1gMnSc2
Alberta	Albert	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
došli	dojít	k5eAaPmAgMnP
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Alexandra	Alexandr	k1gMnSc2
měli	mít	k5eAaImAgMnP
uzavřít	uzavřít	k5eAaPmF
manželství	manželství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
Viktorie	Viktorie	k1gFnSc1
ovdověla	ovdovět	k5eAaPmAgFnS
<g/>
,	,	kIx,
stáhla	stáhnout	k5eAaPmAgFnS
se	se	k3xPyFc4
z	z	k7c2
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
Albertově	Albertův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
zajistila	zajistit	k5eAaPmAgFnS
pro	pro	k7c4
Eduarda	Eduard	k1gMnSc4
cestu	cesta	k1gFnSc4
po	po	k7c6
Středním	střední	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
cesty	cesta	k1gFnSc2
byla	být	k5eAaImAgFnS
návštěva	návštěva	k1gFnSc1
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
Damašku	Damašek	k1gInSc2
<g/>
,	,	kIx,
Bejrútu	Bejrút	k1gInSc2
a	a	k8xC
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sňatek	sňatek	k1gInSc1
Eduarda	Eduard	k1gMnSc2
a	a	k8xC
Alexandry	Alexandra	k1gFnSc2
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1863	#num#	k4
v	v	k7c6
kapli	kaple	k1gFnSc6
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
</s>
<s>
Eduardova	Eduardův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Viktorie	Viktorie	k1gFnSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
neukazovala	ukazovat	k5eNaImAgFnS
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
<g/>
,	,	kIx,
ji	on	k3xPp3gFnSc4
zastupoval	zastupovat	k5eAaImAgMnS
princ	princ	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
různých	různý	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byly	být	k5eAaImAgFnP
otevření	otevření	k1gNnSc4
radnice	radnice	k1gFnSc2
v	v	k7c6
Halifaxu	Halifax	k1gInSc6
<g/>
,	,	kIx,
nábřeží	nábřeží	k1gNnSc4
na	na	k7c4
Temži	Temže	k1gFnSc4
<g/>
,	,	kIx,
Tower	Tower	k1gInSc4
Bridge	Bridge	k1gFnPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
a	a	k8xC
byl	být	k5eAaImAgMnS
tak	tak	k6eAd1
prvním	první	k4xOgMnSc7
monarchou	monarcha	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
prezentoval	prezentovat	k5eAaBmAgMnS
na	na	k7c6
veřejných	veřejný	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	on	k3xPp3gMnPc4
známe	znát	k5eAaImIp1nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
mu	on	k3xPp3gInSc3
jeho	jeho	k3xOp3gMnPc4
matka	matka	k1gFnSc1
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1898	#num#	k4
nedovolila	dovolit	k5eNaPmAgFnS
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
státních	státní	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1875	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
dlouhou	dlouhý	k2eAgFnSc4d1
osmiměsíční	osmiměsíční	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
po	po	k7c6
Indii	Indie	k1gFnSc6
</s>
<s>
Eduard	Eduard	k1gMnSc1
byl	být	k5eAaImAgMnS
patronem	patron	k1gMnSc7
umění	umění	k1gNnSc2
a	a	k8xC
vědy	věda	k1gFnSc2
a	a	k8xC
pomohl	pomoct	k5eAaPmAgMnS
založit	založit	k5eAaPmF
Royal	Royal	k1gMnSc1
College	Colleg	k1gInSc2
of	of	k?
Music	Music	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
propadl	propadnout	k5eAaPmAgInS
hraní	hraní	k1gNnSc4
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
rád	rád	k6eAd1
venkovní	venkovní	k2eAgInPc4d1
sporty	sport	k1gInPc4
a	a	k8xC
byl	být	k5eAaImAgMnS
také	také	k6eAd1
nadšeným	nadšený	k2eAgMnSc7d1
lovcem	lovec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporoval	podporovat	k5eAaImAgMnS
módu	móda	k1gFnSc4
nošení	nošení	k1gNnSc2
tvídového	tvídový	k2eAgNnSc2d1
oblečení	oblečení	k1gNnSc2
a	a	k8xC
zpopularizoval	zpopularizovat	k5eAaPmAgMnS
nošení	nošení	k1gNnSc4
smokingu	smoking	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavedl	zavést	k5eAaPmAgInS
zvyk	zvyk	k1gInSc1
jíst	jíst	k5eAaImF
k	k	k7c3
nedělnímu	nedělní	k2eAgInSc3d1
obědu	oběd	k1gInSc3
hovězí	hovězí	k1gNnSc4
pečeni	pečen	k2eAgMnPc1d1
a	a	k8xC
opečené	opečený	k2eAgFnPc1d1
brambory	brambora	k1gFnPc1
s	s	k7c7
křenovou	křenový	k2eAgFnSc7d1
omáčkou	omáčka	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zůstalo	zůstat	k5eAaPmAgNnS
nejoblíbenějším	oblíbený	k2eAgNnSc7d3
nedělním	nedělní	k2eAgNnSc7d1
jídlem	jídlo	k1gNnSc7
Britů	Brit	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
Když	když	k8xS
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
královna	královna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1901	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
Eduard	Eduard	k1gMnSc1
králem	král	k1gMnSc7
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
císařem	císař	k1gMnSc7
Indie	Indie	k1gFnSc2
a	a	k8xC
také	také	k9
králem	král	k1gMnSc7
britských	britský	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dům	dům	k1gInSc1
svých	svůj	k3xOyFgMnPc2
rodičů	rodič	k1gMnPc2
na	na	k7c6
Isle	Isle	k1gNnSc6
of	of	k?
Wight	Wight	k1gMnSc1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
státu	stát	k1gInSc3
a	a	k8xC
sám	sám	k3xTgMnSc1
žil	žít	k5eAaImAgMnS
v	v	k7c6
Sandringhamu	Sandringham	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
prvního	první	k4xOgMnSc4
dědice	dědic	k1gMnSc4
britské	britský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
bez	bez	k7c2
dluhů	dluh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
finance	finance	k1gFnSc2
spravoval	spravovat	k5eAaImAgMnS
Dighton	Dighton	k1gInSc4
Probyn	Probyn	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
využíval	využívat	k5eAaImAgMnS,k5eAaPmAgMnS
rady	rada	k1gFnPc4
Eduardových	Eduardův	k2eAgMnPc2d1
židovských	židovský	k2eAgMnPc2d1
přátel	přítel	k1gMnPc2
a	a	k8xC
to	ten	k3xDgNnSc4
například	například	k6eAd1
i	i	k9
členů	člen	k1gMnPc2
Rothschildovy	Rothschildův	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
narůstajícího	narůstající	k2eAgInSc2d1
antisemitismu	antisemitismus	k1gInSc2
tak	tak	k6eAd1
musel	muset	k5eAaImAgMnS
Eduard	Eduard	k1gMnSc1
vzdorovat	vzdorovat	k5eAaImF
kritice	kritika	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
otevřeně	otevřeně	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
Židy	Žid	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Alexandra	Alexandr	k1gMnSc4
byli	být	k5eAaImAgMnP
korunováni	korunován	k2eAgMnPc1d1
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
korunovace	korunovace	k1gFnSc1
odehrát	odehrát	k5eAaPmF
již	již	k6eAd1
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Eduard	Eduard	k1gMnSc1
byl	být	k5eAaImAgMnS
postižen	postihnout	k5eAaPmNgMnS
zánětem	zánět	k1gInSc7
slepého	slepý	k2eAgNnSc2d1
střeva	střevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
pokrokům	pokrok	k1gInPc3
v	v	k7c6
anestézii	anestézie	k1gFnSc6
a	a	k8xC
antiseptice	antiseptika	k1gFnSc6
podstoupil	podstoupit	k5eAaPmAgMnS
operaci	operace	k1gFnSc4
<g/>
,	,	kIx,
prováděnou	prováděný	k2eAgFnSc4d1
Frederikem	Frederik	k1gMnSc7
Trevesem	Treves	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
mu	on	k3xPp3gMnSc3
zachránila	zachránit	k5eAaPmAgFnS
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
slepé	slepý	k2eAgNnSc1d1
střevo	střevo	k1gNnSc1
neodstraňovalo	odstraňovat	k5eNaImAgNnS
operativně	operativně	k6eAd1
a	a	k8xC
úmrtnost	úmrtnost	k1gFnSc1
na	na	k7c4
toto	tento	k3xDgNnSc4
onemocnění	onemocnění	k1gNnSc4
byla	být	k5eAaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Treves	Treves	k1gInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
pro	pro	k7c4
radikální	radikální	k2eAgInSc4d1
zákrok	zákrok	k1gInSc4
odstraněním	odstranění	k1gNnSc7
zaníceného	zanícený	k2eAgNnSc2d1
slepého	slepý	k2eAgNnSc2d1
střeva	střevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
již	již	k6eAd1
příští	příští	k2eAgInSc4d1
den	den	k1gInSc4
cítil	cítit	k5eAaImAgMnS
lépe	dobře	k6eAd2
a	a	k8xC
tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
jako	jako	k9
hlavní	hlavní	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
řešení	řešení	k1gNnSc4
zánětu	zánět	k1gInSc2
slepého	slepý	k2eAgNnSc2d1
střeva	střevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Jiří	Jiří	k1gMnSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
vnukové	vnuk	k1gMnPc1
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Jiří	Jiří	k1gMnSc1
</s>
<s>
Eduardovými	Eduardův	k2eAgInPc7d1
hlavními	hlavní	k2eAgInPc7d1
zájmy	zájem	k1gInPc7
byly	být	k5eAaImAgFnP
zahraniční	zahraniční	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
a	a	k8xC
starost	starost	k1gFnSc4
o	o	k7c4
námořní	námořní	k2eAgFnPc4d1
a	a	k8xC
vojenské	vojenský	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uměl	umět	k5eAaImAgMnS
plynně	plynně	k6eAd1
francouzsky	francouzsky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
a	a	k8xC
podnikl	podniknout	k5eAaPmAgMnS
mnoho	mnoho	k4c4
cest	cesta	k1gFnPc2
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
cest	cesta	k1gFnPc2
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1903	#num#	k4
návštěva	návštěva	k1gFnSc1
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
hostem	host	k1gMnSc7
prezidenta	prezident	k1gMnSc2
Loubeta	Loubet	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návštěvě	návštěva	k1gFnSc6
papeže	papež	k1gMnSc2
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
další	další	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
důvěry	důvěra	k1gFnSc2
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
státy	stát	k1gInPc7
<g/>
,	,	kIx,
urovnání	urovnání	k1gNnPc4
sporů	spor	k1gInPc2
o	o	k7c4
kolonie	kolonie	k1gFnPc4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
odvrácení	odvrácení	k1gNnSc6
rizika	riziko	k1gNnSc2
války	válka	k1gFnSc2
mezi	mezi	k7c7
Británií	Británie	k1gFnSc7
a	a	k8xC
Francií	Francie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srdečná	srdečná	k1gFnSc1
dohoda	dohoda	k1gFnSc1
<g/>
,	,	kIx,
odstraňující	odstraňující	k2eAgInPc1d1
rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
koloniálními	koloniální	k2eAgFnPc7d1
mocnostmi	mocnost	k1gFnPc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1904	#num#	k4
<g/>
,	,	kIx,
ukončila	ukončit	k5eAaPmAgFnS
dlouhodobou	dlouhodobý	k2eAgFnSc4d1
rivalitu	rivalita	k1gFnSc4
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
zeměmi	zem	k1gFnPc7
a	a	k8xC
vytvořila	vytvořit	k5eAaPmAgFnS
protiváhu	protiváha	k1gFnSc4
vůči	vůči	k7c3
narůstajícímu	narůstající	k2eAgNnSc3d1
spojenectví	spojenectví	k1gNnSc3
mezi	mezi	k7c7
Německem	Německo	k1gNnSc7
a	a	k8xC
Rakouskem-Uherskem	Rakouskem-Uhersko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1908	#num#	k4
byl	být	k5eAaImAgMnS
Eduard	Eduard	k1gMnSc1
prvním	první	k4xOgNnSc7
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
navštívil	navštívit	k5eAaPmAgMnS
Ruské	ruský	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Politické	politický	k2eAgInPc1d1
spory	spor	k1gInPc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
diskuse	diskuse	k1gFnSc2
o	o	k7c6
reformě	reforma	k1gFnSc6
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
potřeba	potřeba	k1gFnSc1
se	se	k3xPyFc4
projevila	projevit	k5eAaPmAgFnS
po	po	k7c6
druhé	druhý	k4xOgFnSc6
búrské	búrský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporoval	podporovat	k5eAaImAgMnS
reorganizaci	reorganizace	k1gFnSc4
velení	velení	k1gNnSc2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
vytvoření	vytvoření	k1gNnSc4
dobrovolné	dobrovolný	k2eAgFnSc2d1
zálohy	záloha	k1gFnSc2
armády	armáda	k1gFnSc2
a	a	k8xC
vytvoření	vytvoření	k1gNnSc2
expedičních	expediční	k2eAgInPc2d1
sborů	sbor	k1gInPc2
pro	pro	k7c4
podporu	podpora	k1gFnSc4
Francie	Francie	k1gFnSc2
v	v	k7c6
případné	případný	k2eAgFnSc6d1
válce	válka	k1gFnSc6
proti	proti	k7c3
Německu	Německo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reforma	reforma	k1gFnSc1
královského	královský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
byla	být	k5eAaImAgFnS
vyvolána	vyvolat	k5eAaPmNgFnS
i	i	k9
vznikem	vznik	k1gInSc7
nové	nový	k2eAgFnSc2d1
strategické	strategický	k2eAgFnSc2d1
hrozby	hrozba	k1gFnSc2
-	-	kIx~
německého	německý	k2eAgNnSc2d1
válečného	válečný	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
podporoval	podporovat	k5eAaImAgMnS
názory	názor	k1gInPc4
Johna	John	k1gMnSc4
Fishera	Fisher	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
navrhoval	navrhovat	k5eAaImAgInS
efektivní	efektivní	k2eAgFnPc4d1
úspory	úspora	k1gFnPc4
<g/>
,	,	kIx,
zrušení	zrušení	k1gNnSc4
zastaralých	zastaralý	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
strategické	strategický	k2eAgNnSc4d1
přeskupení	přeskupení	k1gNnSc4
loďstva	loďstvo	k1gNnSc2
na	na	k7c4
torpédové	torpédový	k2eAgInPc4d1
čluny	člun	k1gInPc4
pro	pro	k7c4
obranu	obrana	k1gFnSc4
země	zem	k1gFnSc2
podporovanou	podporovaný	k2eAgFnSc4d1
bitevními	bitevní	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
roce	rok	k1gInSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
byl	být	k5eAaImAgMnS
Eduard	Eduard	k1gMnSc1
zapojen	zapojit	k5eAaPmNgMnS
do	do	k7c2
ústavní	ústavní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
způsobené	způsobený	k2eAgNnSc4d1
odmítnutím	odmítnutí	k1gNnSc7
přijetí	přijetí	k1gNnPc2
rozpočtu	rozpočet	k1gInSc2
navrženého	navržený	k2eAgInSc2d1
liberální	liberální	k2eAgInPc4d1
vládou	vláda	k1gFnSc7
vedenou	vedený	k2eAgFnSc7d1
H.	H.	kA
H.	H.	kA
Asquithem	Asquith	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
obsahoval	obsahovat	k5eAaImAgInS
daňová	daňový	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
směřující	směřující	k2eAgNnPc1d1
k	k	k7c3
přerozdělení	přerozdělení	k1gNnSc3
bohatství	bohatství	k1gNnSc2
<g/>
,	,	kIx,
konzervativní	konzervativní	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
ve	v	k7c6
Sněmovně	sněmovna	k1gFnSc6
lordů	lord	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
Asquithovi	Asquith	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
nové	nový	k2eAgMnPc4d1
peery	peer	k1gMnPc4
<g/>
,	,	kIx,
potřebné	potřebný	k2eAgInPc4d1
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
tohoto	tento	k3xDgInSc2
rozpočtu	rozpočet	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
zvítězí	zvítězit	k5eAaPmIp3nS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
všeobecných	všeobecný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
pravidelně	pravidelně	k6eAd1
vykouřil	vykouřit	k5eAaPmAgMnS
dvacet	dvacet	k4xCc4
cigaret	cigareta	k1gFnPc2
a	a	k8xC
dvanáct	dvanáct	k4xCc4
doutníků	doutník	k1gInPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
života	život	k1gInSc2
trpěl	trpět	k5eAaImAgInS
zánětem	zánět	k1gInSc7
průdušek	průduška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1910	#num#	k4
Eduard	Eduard	k1gMnSc1
v	v	k7c6
Biarritzu	Biarritz	k1gInSc6
zkolaboval	zkolabovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
stále	stále	k6eAd1
trpěl	trpět	k5eAaImAgInS
zánětem	zánět	k1gInSc7
průdušek	průduška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
z	z	k7c2
Korfu	Korfu	k1gNnSc2
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
den	den	k1gInSc1
byl	být	k5eAaImAgInS
postižen	postihnout	k5eAaPmNgInS
několika	několik	k4yIc7
zástavami	zástava	k1gFnPc7
srdce	srdce	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
ulehnout	ulehnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navečer	navečer	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
vědomí	vědomí	k1gNnSc4
a	a	k8xC
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
Eduarda	Eduard	k1gMnSc2
VII	VII	kA
<g/>
..	..	k?
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Antonie	Antonie	k1gFnSc2
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XXIV	XXIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuss-Ebersdorf	Reuss-Ebersdorf	k1gInSc1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Ernestina	Ernestin	k2eAgNnPc1d1
Erbašsko-Schönberská	Erbašsko-Schönberský	k2eAgNnPc1d1
</s>
<s>
Albert	Albert	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Augustus	Augustus	k1gInSc1
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
Meklenbursko-Zvěřínský	Meklenbursko-Zvěřínský	k2eAgMnSc5d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Zvěřínská	Meklenbursko-Zvěřínský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Frederik	Frederik	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Augusta	Augusta	k1gMnSc1
Sasko-Gothajská	sasko-gothajský	k2eAgFnSc1d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgMnSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Sasko-Hildburghausenská	Sasko-Hildburghausenský	k2eAgFnSc1d1
</s>
<s>
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Antonie	Antonie	k1gFnSc2
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XXIV	XXIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuss-Ebersdorf	Reuss-Ebersdorf	k1gInSc1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Ernestina	Ernestin	k2eAgNnPc1d1
Erbašsko-Schönberská	Erbašsko-Schönberský	k2eAgNnPc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Edward	Edward	k1gMnSc1
VII	VII	kA
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20030212003	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118528955	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
8269	#num#	k4
6461	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79039821	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500023438	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
265340794	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79039821	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
