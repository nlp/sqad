<s>
Navštívil	navštívit	k5eAaPmAgInS
celkově	celkově	k6eAd1
devětkrát	devětkrát	k6eAd1
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
převážně	převážně	k6eAd1
vystupoval	vystupovat	k5eAaImAgMnS
pod	pod	k7c7
titulem	titul	k1gInSc7
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Lancasteru	Lancaster	k1gInSc2
(	(	kIx(
<g/>
Duke	Duke	k1gInSc1
of	of	k?
Lancaster	Lancaster	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>