<s>
Reynolds	Reynolds	k1gInSc1	Reynolds
"	"	kIx"	"
<g/>
Rey	Rea	k1gFnPc1	Rea
<g/>
"	"	kIx"	"
Koranteng	Koranteng	k1gInSc1	Koranteng
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
televizní	televizní	k2eAgMnSc1d1	televizní
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Ludmila	Ludmila	k1gFnSc1	Ludmila
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
české	český	k2eAgFnPc4d1	Česká
národnosti	národnost	k1gFnPc4	národnost
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Ghany	Ghana	k1gFnSc2	Ghana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Monikou	Monika	k1gFnSc7	Monika
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
Vanessu	Vaness	k1gInSc2	Vaness
(	(	kIx(	(
<g/>
*	*	kIx~	*
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sofii	Sofia	k1gFnSc4	Sofia
(	(	kIx(	(
<g/>
*	*	kIx~	*
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Leontýnu	Leontýna	k1gFnSc4	Leontýna
(	(	kIx(	(
<g/>
*	*	kIx~	*
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
meteorologii	meteorologie	k1gFnSc4	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
působí	působit	k5eAaImIp3nP	působit
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xS	jako
moderátor	moderátor	k1gInSc4	moderátor
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
televize	televize	k1gFnSc2	televize
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
rozešel	rozejít	k5eAaPmAgMnS	rozejít
se	s	k7c7	s
servisní	servisní	k2eAgFnSc7d1	servisní
organizací	organizace	k1gFnSc7	organizace
CME	CME	kA	CME
a	a	k8xC	a
Nova	nova	k1gFnSc1	nova
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
z	z	k7c2	z
Barrandova	Barrandov	k1gInSc2	Barrandov
<g/>
,	,	kIx,	,
moderuje	moderovat	k5eAaBmIp3nS	moderovat
hlavní	hlavní	k2eAgInSc1d1	hlavní
zpravodajský	zpravodajský	k2eAgInSc1d1	zpravodajský
pořad	pořad	k1gInSc1	pořad
této	tento	k3xDgFnSc2	tento
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
Televizní	televizní	k2eAgFnSc2d1	televizní
noviny	novina	k1gFnSc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
CEVRO	CEVRO	kA	CEVRO
Institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
zálibám	záliba	k1gFnPc3	záliba
patří	patřit	k5eAaImIp3nS	patřit
šachy	šach	k1gInPc1	šach
a	a	k8xC	a
létání	létání	k1gNnSc1	létání
na	na	k7c6	na
větroních	větroň	k1gInPc6	větroň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Televizních	televizní	k2eAgFnPc6d1	televizní
novinách	novina	k1gFnPc6	novina
moderuje	moderovat	k5eAaBmIp3nS	moderovat
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
Borhyovou	Borhyová	k1gFnSc7	Borhyová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jejího	její	k3xOp3gNnSc2	její
těhotenství	těhotenství	k1gNnSc2	těhotenství
moderoval	moderovat	k5eAaBmAgMnS	moderovat
s	s	k7c7	s
Michaelou	Michaela	k1gFnSc7	Michaela
Ochotskou	ochotský	k2eAgFnSc7d1	Ochotská
a	a	k8xC	a
Kristinou	Kristina	k1gFnSc7	Kristina
Kloubkovou	Kloubkový	k2eAgFnSc7d1	Kloubková
<g/>
.	.	kIx.	.
</s>
