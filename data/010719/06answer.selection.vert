<s>
Roman	Roman	k1gMnSc1	Roman
Horký	Horký	k1gMnSc1	Horký
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
"	"	kIx"	"
<g/>
Romiš	Romiš	k1gMnSc1	Romiš
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
frontman	frontman	k1gMnSc1	frontman
skupiny	skupina	k1gFnSc2	skupina
Kamelot	kamelot	k1gMnSc1	kamelot
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
