<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Horký	Horký	k1gMnSc1	Horký
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
"	"	kIx"	"
<g/>
Romiš	Romiš	k1gMnSc1	Romiš
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
frontman	frontman	k1gMnSc1	frontman
skupiny	skupina	k1gFnSc2	skupina
Kamelot	kamelot	k1gMnSc1	kamelot
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
hrával	hrávat	k5eAaImAgMnS	hrávat
se	se	k3xPyFc4	se
skupinami	skupina	k1gFnPc7	skupina
Karabina	karabina	k1gFnSc1	karabina
<g/>
,	,	kIx,	,
Wanailon	Wanailon	k1gInSc1	Wanailon
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muzice	muzika	k1gFnSc6	muzika
není	být	k5eNaImIp3nS	být
stylově	stylově	k6eAd1	stylově
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Složil	Složil	k1gMnSc1	Složil
přes	přes	k7c4	přes
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
mnoha	mnoho	k4c2	mnoho
hudebních	hudební	k2eAgNnPc2d1	hudební
ocenění	ocenění	k1gNnPc2	ocenění
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
×	×	k?	×
Kamelot	kamelot	k1gInSc1	kamelot
–	–	k?	–
kapela	kapela	k1gFnSc1	kapela
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
×	×	k?	×
Kamelot	kamelot	k1gInSc1	kamelot
–	–	k?	–
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
nahrál	nahrát	k5eAaPmAgInS	nahrát
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
autorských	autorský	k2eAgNnPc2d1	autorské
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
15	[number]	k4	15
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
oceněno	ocenit	k5eAaPmNgNnS	ocenit
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
<g/>
"	"	kIx"	"
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
Platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Multiplatiová	Multiplatiový	k2eAgFnSc1d1	Multiplatiový
deska	deska	k1gFnSc1	deska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
autorských	autorský	k2eAgNnPc2d1	autorské
alb	album	k1gNnPc2	album
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
500	[number]	k4	500
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
písní	píseň	k1gFnPc2	píseň
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c6	na
multiplatinových	multiplatinový	k2eAgFnPc6d1	multiplatinová
hudebních	hudební	k2eAgFnPc6d1	hudební
kompilacích	kompilace	k1gFnPc6	kompilace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
koně	kůň	k1gMnSc4	kůň
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
mezi	mezi	k7c7	mezi
sty	sto	k4xCgNnPc7	sto
nejpopulárnějšími	populární	k2eAgMnPc7d3	nejpopulárnější
zpěváky	zpěvák	k1gMnPc7	zpěvák
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kandidatura	kandidatura	k1gFnSc1	kandidatura
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
jako	jako	k8xC	jako
nestranický	stranický	k2eNgMnSc1d1	nestranický
kandidát	kandidát	k1gMnSc1	kandidát
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
senátní	senátní	k2eAgFnPc4d1	senátní
volby	volba	k1gFnPc4	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2018	[number]	k4	2018
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
59	[number]	k4	59
–	–	k?	–
Brno-město	Brnoěsta	k1gFnSc5	Brno-města
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
koně	kůň	k1gMnSc4	kůň
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Levnej	Levnej	k?	Levnej
hotel	hotel	k1gInSc1	hotel
<g/>
"	"	kIx"	"
–	–	k?	–
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vedla	vést	k5eAaImAgFnS	vést
oficiální	oficiální	k2eAgFnSc4d1	oficiální
českou	český	k2eAgFnSc4d1	Česká
hitparádu	hitparáda	k1gFnSc4	hitparáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zrození	zrození	k1gNnSc1	zrození
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Čas	čas	k1gInSc1	čas
rozchodů	rozchod	k1gInPc2	rozchod
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Honolulu	Honolulu	k1gNnPc3	Honolulu
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Slib	slib	k1gInSc4	slib
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Země	země	k1gFnSc1	země
antilop	antilopa	k1gFnPc2	antilopa
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pozor	pozor	k1gInSc1	pozor
tunel	tunel	k1gInSc1	tunel
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hardegg	Hardegg	k1gInSc4	Hardegg
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Babí	babí	k2eAgNnSc1d1	babí
léto	léto	k1gNnSc1	léto
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Anděl	Anděla	k1gFnPc2	Anděla
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Paměť	paměť	k1gFnSc1	paměť
slonů	slon	k1gMnPc2	slon
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Když	když	k8xS	když
vlaky	vlak	k1gInPc1	vlak
jedou	jet	k5eAaImIp3nP	jet
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Psáno	psát	k5eAaImNgNnS	psát
na	na	k7c6	na
březové	březový	k2eAgFnSc6d1	Březová
kůře	kůra	k1gFnSc6	kůra
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
koně	kůň	k1gMnSc4	kůň
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Tajný	tajný	k1gMnSc1	tajný
výpravy	výprava	k1gFnSc2	výprava
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Vyznavači	vyznavač	k1gMnSc3	vyznavač
ohňů	oheň	k1gInPc2	oheň
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Duhová	duhový	k2eAgFnSc1d1	Duhová
cesta	cesta	k1gFnSc1	cesta
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ryba	ryba	k1gFnSc1	ryba
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
ráje	ráj	k1gInSc2	ráj
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Vyhaslý	vyhaslý	k2eAgInSc4d1	vyhaslý
oheň	oheň	k1gInSc4	oheň
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Země	zem	k1gFnSc2	zem
antilop	antilopa	k1gFnPc2	antilopa
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Větrné	větrný	k2eAgNnSc4d1	větrné
město	město	k1gNnSc4	město
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Pozdní	pozdní	k2eAgInPc1d1	pozdní
návraty	návrat	k1gInPc1	návrat
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Paměť	paměť	k1gFnSc4	paměť
slonů	slon	k1gMnPc2	slon
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Valerie	Valerie	k1gFnSc2	Valerie
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
svět	svět	k1gInSc4	svět
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Modrá	modrý	k2eAgFnSc1d1	modrá
planeta	planeta	k1gFnSc1	planeta
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
Mořská	mořský	k2eAgFnSc1d1	mořská
sůl	sůl	k1gFnSc1	sůl
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
-	-	kIx~	-
Babí	babí	k2eAgNnSc1d1	babí
léto	léto	k1gNnSc1	léto
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilační	kompilační	k2eAgNnPc4d1	kompilační
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
kamaráda	kamarád	k1gMnSc4	kamarád
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
pláž	pláž	k1gFnSc1	pláž
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
25	[number]	k4	25
–	–	k?	–
Výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
19	[number]	k4	19
ztracených	ztracený	k2eAgFnPc2d1	ztracená
písní	píseň	k1gFnPc2	píseň
Wabiho	Wabi	k1gMnSc2	Wabi
Ryvoly	Ryvola	k1gFnSc2	Ryvola
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Od	od	k7c2	od
A	a	k8xC	a
do	do	k7c2	do
Z	z	k7c2	z
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
Roman	Roman	k1gMnSc1	Roman
Horký	Horký	k1gMnSc1	Horký
a	a	k8xC	a
Kamelot	kamelot	k1gMnSc1	kamelot
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
singel	singel	k1gMnSc1	singel
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Rio	Rio	k1gMnSc1	Rio
<g/>
/	/	kIx~	/
<g/>
Hardegg	Hardegg	k1gMnSc1	Hardegg
singel	singel	k1gMnSc1	singel
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Island	Island	k1gInSc1	Island
singel	singel	k1gInSc1	singel
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Roman	Roman	k1gMnSc1	Roman
Horký	Horký	k1gMnSc1	Horký
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Horký	Horký	k1gMnSc1	Horký
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Kamelotu	kamelot	k1gInSc2	kamelot
</s>
</p>
