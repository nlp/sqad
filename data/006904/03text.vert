<s>
Hana	Hana	k1gFnSc1	Hana
Dýčková	Dýčková	k1gFnSc1	Dýčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
poslankyně	poslankyně	k1gFnSc2	poslankyně
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
profesně	profesně	k6eAd1	profesně
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
instruktorka	instruktorka	k1gFnSc1	instruktorka
učňů	učeň	k1gMnPc2	učeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
zasedla	zasednout	k5eAaPmAgFnS	zasednout
za	za	k7c4	za
KSČ	KSČ	kA	KSČ
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
42	[number]	k4	42
-	-	kIx~	-
Náchod	Náchod	k1gInSc1	Náchod
<g/>
,	,	kIx,	,
Východočeský	východočeský	k2eAgInSc1d1	východočeský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
setrvala	setrvat	k5eAaPmAgFnS	setrvat
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
procesu	proces	k1gInSc2	proces
kooptací	kooptace	k1gFnPc2	kooptace
do	do	k7c2	do
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992-2008	[number]	k4	1992-2008
se	se	k3xPyFc4	se
eviduje	evidovat	k5eAaImIp3nS	evidovat
jako	jako	k9	jako
živnostnice	živnostnice	k1gFnSc1	živnostnice
<g/>
,	,	kIx,	,
bytem	byt	k1gInSc7	byt
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c4	nad
Kněžnou-Lokot	Kněžnou-Lokot	k1gInSc4	Kněžnou-Lokot
<g/>
.	.	kIx.	.
</s>
