<s>
Predikát	predikát	k1gInSc1	predikát
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
takové	takový	k3xDgNnSc4	takový
jazykové	jazykový	k2eAgNnSc4d1	jazykové
sdělení	sdělení	k1gNnSc4	sdělení
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
obsahové	obsahový	k2eAgFnSc6d1	obsahová
stránce	stránka	k1gFnSc6	stránka
smysl	smysl	k1gInSc4	smysl
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
(	(	kIx(	(
<g/>
označuje	označovat	k5eAaImIp3nS	označovat
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
true	tru	k1gFnSc2	tru
nebo	nebo	k8xC	nebo
číslicí	číslice	k1gFnSc7	číslice
1	[number]	k4	1
<g/>
)	)	kIx)	)
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
(	(	kIx(	(
<g/>
označuje	označovat	k5eAaImIp3nS	označovat
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
false	false	k1gFnSc2	false
nebo	nebo	k8xC	nebo
číslicí	číslice	k1gFnSc7	číslice
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
výrok	výrok	k1gInSc1	výrok
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
,	,	kIx,	,
říkáme	říkat	k5eAaImIp1nP	říkat
že	že	k8xS	že
platí	platit	k5eAaImIp3nP	platit
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
nepravdivý	pravdivý	k2eNgMnSc1d1	nepravdivý
-	-	kIx~	-
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
věta	věta	k1gFnSc1	věta
nebo	nebo	k8xC	nebo
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
něco	něco	k3yInSc1	něco
o	o	k7c6	o
subjektu	subjekt	k1gInSc6	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gNnSc7	on
část	část	k1gFnSc4	část
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
výroková	výrokový	k2eAgFnSc1d1	výroková
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
vypovídající	vypovídající	k2eAgFnSc1d1	vypovídající
o	o	k7c6	o
subjektu	subjekt	k1gInSc6	subjekt
nebo	nebo	k8xC	nebo
proměnné	proměnná	k1gFnSc3	proměnná
<g/>
.	.	kIx.	.
</s>
