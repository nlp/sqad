<s>
Chiasognathus	Chiasognathus	k1gInSc1
granti	granť	k1gFnSc2
</s>
<s>
Chiasognathus	Chiasognathus	k1gInSc1
granti	granť	k1gFnSc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
Insecta	Insecta	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
brouci	brouk	k1gMnPc1
(	(	kIx(
<g/>
Coleoptera	Coleopter	k1gMnSc2
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
všežraví	všežravý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Polyphaga	Polyphaga	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
roháčovití	roháčovití	k1gMnPc1
(	(	kIx(
<g/>
Lucanidae	Lucanidae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
Chiasognathus	Chiasognathus	k1gMnSc1
Binomické	binomický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Chiasognathus	Chiasognathus	k1gInSc1
grantiStephens	grantiStephensa	k1gFnPc2
<g/>
,	,	kIx,
1831	#num#	k4
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chiasognathus	Chiasognathus	k1gInSc1
granti	granť	k1gFnSc2
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
brouka	brouk	k1gMnSc4
rodu	rod	k1gInSc2
Chiasognathus	Chiasognathus	k1gInSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
roháčovití	roháčovití	k1gMnPc1
<g/>
,	,	kIx,
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
Argentině	Argentina	k1gFnSc6
a	a	k8xC
Chile	Chile	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
nazýván	nazýván	k2eAgMnSc1d1
Darwinův	Darwinův	k2eAgMnSc1d1
brouk	brouk	k1gMnSc1
po	po	k7c6
anglickém	anglický	k2eAgInSc6d1
přírodovědci	přírodovědec	k1gMnSc3
Charlesu	Charlesu	k?
Darwinovi	Darwin	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
získal	získat	k5eAaPmAgMnS
exempláře	exemplář	k1gInPc4
v	v	k7c4
Chile	Chile	k1gNnSc4
během	během	k7c2
své	svůj	k3xOyFgFnSc2
druhé	druhý	k4xOgFnSc2
výpravy	výprava	k1gFnSc2
na	na	k7c6
lodi	loď	k1gFnSc6
HMS	HMS	kA
Beagle	Beagle	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
dlouhým	dlouhý	k2eAgNnPc3d1
kusadlům	kusadla	k1gNnPc3
brouka	brouk	k1gMnSc2
<g/>
,	,	kIx,
si	se	k3xPyFc3
badatel	badatel	k1gMnSc1
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
stisk	stisk	k1gInSc1
čelistí	čelist	k1gFnPc2
na	na	k7c6
malíčku	malíček	k1gInSc6
nedosahoval	dosahovat	k5eNaImAgInS
takové	takový	k3xDgFnPc4
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
způsobil	způsobit	k5eAaPmAgMnS
bolest	bolest	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Chiasognathus	Chiasognathus	k1gInSc1
granti	granť	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biolib	Biolib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
M.	M.	kA
J.	J.	kA
Paulsen	Paulsna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annotated	Annotated	k1gMnSc1
Checklist	Checklist	k1gMnSc1
of	of	k?
the	the	k?
New	New	k1gFnSc2
World	World	k1gMnSc1
Lucanidae	Lucanida	k1gMnSc2
(	(	kIx(
<g/>
Coleoptera	Coleopter	k1gMnSc2
<g/>
:	:	kIx,
Scarabaeoidea	Scarabaeoidea	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-11-24	2008-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Entomology	entomolog	k1gMnPc4
treasures	treasuresa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natural	Natural	k?
History	Histor	k1gInPc1
Museum	museum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kenneth	Kenneth	k1gInSc1
G.	G.	kA
V.	V.	kA
Smith	Smith	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Darwin	Darwin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
insects	insects	k6eAd1
<g/>
:	:	kIx,
Charles	Charles	k1gMnSc1
Darwin	Darwin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
entomological	entomologicat	k5eAaPmAgInS
notes	notes	k1gInSc1
<g/>
,	,	kIx,
with	with	k1gInSc1
an	an	k?
introduction	introduction	k1gInSc1
and	and	k?
comments	comments	k1gInSc1
by	by	kYmCp3nS
Kenneth	Kenneth	k1gInSc4
G.	G.	kA
V.	V.	kA
Smith	Smith	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
British	British	k1gInSc1
Museum	museum	k1gNnSc1
(	(	kIx(
<g/>
Natural	Natural	k?
History	Histor	k1gInPc4
<g/>
)	)	kIx)
Historical	Historical	k1gMnSc1
Series	Series	k1gMnSc1
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
143	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chiasognathus	Chiasognathus	k1gMnSc1
granti	granť	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Entomologie	entomologie	k1gFnSc1
</s>
