<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
nazýván	nazýván	k2eAgMnSc1d1
Darwinův	Darwinův	k2eAgMnSc1d1
brouk	brouk	k1gMnSc1
po	po	k7c6
anglickém	anglický	k2eAgInSc6d1
přírodovědci	přírodovědec	k1gMnSc3
Charlesu	Charlesu	k?
Darwinovi	Darwin	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
získal	získat	k5eAaPmAgMnS
exempláře	exemplář	k1gInPc4
v	v	k7c4
Chile	Chile	k1gNnSc4
během	během	k7c2
své	svůj	k3xOyFgFnSc2
druhé	druhý	k4xOgFnSc2
výpravy	výprava	k1gFnSc2
na	na	k7c6
lodi	loď	k1gFnSc6
HMS	HMS	kA
Beagle	Beagle	k1gInSc1
<g/>
.	.	kIx.
</s>