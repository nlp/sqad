<s>
Lesbická	lesbický	k2eAgFnSc1d1	lesbická
utopie	utopie	k1gFnSc1	utopie
je	být	k5eAaImIp3nS	být
společenství	společenství	k1gNnSc1	společenství
tvořené	tvořený	k2eAgNnSc1d1	tvořené
pouze	pouze	k6eAd1	pouze
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c6	na
mužích	muž	k1gMnPc6	muž
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
pojetí	pojetí	k1gNnSc1	pojetí
společnosti	společnost	k1gFnSc2	společnost
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
v	v	k7c6	v
legendách	legenda	k1gFnPc6	legenda
o	o	k7c6	o
Amazonkách	Amazonka	k1gFnPc6	Amazonka
<g/>
,	,	kIx,	,
národu	národ	k1gInSc6	národ
ženských	ženský	k2eAgFnPc2d1	ženská
bojovnic	bojovnice	k1gFnPc2	bojovnice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
Amazonky	Amazonka	k1gFnPc4	Amazonka
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Gargareány	Gargareán	k1gMnPc4	Gargareán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
pomilovaly	pomilovat	k5eAaPmAgFnP	pomilovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidské	lidský	k2eAgFnSc2d1	lidská
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
vajíčko	vajíčko	k1gNnSc1	vajíčko
(	(	kIx(	(
<g/>
od	od	k7c2	od
ženy	žena	k1gFnSc2	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spermie	spermie	k1gFnPc4	spermie
(	(	kIx(	(
<g/>
od	od	k7c2	od
muže	muž	k1gMnSc2	muž
<g/>
)	)	kIx)	)
a	a	k8xC	a
děloha	děloha	k1gFnSc1	děloha
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
vhodné	vhodný	k2eAgNnSc1d1	vhodné
prostředí	prostředí	k1gNnSc1	prostředí
s	s	k7c7	s
dostatečnými	dostatečný	k2eAgInPc7d1	dostatečný
zdroji	zdroj	k1gInPc7	zdroj
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
embryo	embryo	k1gNnSc1	embryo
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
v	v	k7c4	v
plod	plod	k1gInSc4	plod
a	a	k8xC	a
růst	růst	k1gInSc4	růst
až	až	k6eAd1	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
mládě	mládě	k1gNnSc4	mládě
myši	myš	k1gFnSc2	myš
od	od	k7c2	od
dvou	dva	k4xCgFnPc2	dva
myších	myší	k2eAgFnPc2d1	myší
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
tohoto	tento	k3xDgInSc2	tento
nebo	nebo	k8xC	nebo
podobného	podobný	k2eAgInSc2d1	podobný
postupu	postup	k1gInSc2	postup
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
dovolit	dovolit	k5eAaPmF	dovolit
dvěma	dva	k4xCgFnPc7	dva
ženám	žena	k1gFnPc3	žena
být	být	k5eAaImF	být
genetickými	genetický	k2eAgFnPc7d1	genetická
rodiči	rodič	k1gMnPc7	rodič
jednoho	jeden	k4xCgNnSc2	jeden
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
objevili	objevit	k5eAaPmAgMnP	objevit
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
oplodnit	oplodnit	k5eAaPmF	oplodnit
lidské	lidský	k2eAgNnSc4d1	lidské
vajíčko	vajíčko	k1gNnSc4	vajíčko
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
genetického	genetický	k2eAgInSc2d1	genetický
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
lidské	lidský	k2eAgFnSc2d1	lidská
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
případně	případně	k6eAd1	případně
umožnit	umožnit	k5eAaPmF	umožnit
dvěma	dva	k4xCgInPc7	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ženám	žena	k1gFnPc3	žena
otěhotnět	otěhotnět	k5eAaPmF	otěhotnět
a	a	k8xC	a
porodit	porodit	k5eAaPmF	porodit
dítě	dítě	k1gNnSc4	dítě
bez	bez	k7c2	bez
mužské	mužský	k2eAgFnSc2d1	mužská
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
získávají	získávat	k5eAaImIp3nP	získávat
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
manželství	manželství	k1gNnSc2	manželství
osob	osoba	k1gFnPc2	osoba
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
registrovaná	registrovaný	k2eAgNnPc4d1	registrované
partnerství	partnerství	k1gNnSc4	partnerství
právní	právní	k2eAgFnSc4d1	právní
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
biotechnologie	biotechnologie	k1gFnSc1	biotechnologie
použita	použít	k5eAaPmNgFnS	použít
i	i	k9	i
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
etické	etický	k2eAgInPc4d1	etický
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
vyvstat	vyvstat	k5eAaPmF	vyvstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lesbian	Lesbian	k1gInSc4	Lesbian
utopia	utopium	k1gNnSc2	utopium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
