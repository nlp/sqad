<s>
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Gdańsk	Gdańsk	k1gInSc1	Gdańsk
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
[	[	kIx(	[
<g/>
gdaɲ	gdaɲ	k?	gdaɲ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
kašubsky	kašubsky	k6eAd1	kašubsky
Gduńsk	Gduńsk	k1gInSc1	Gduńsk
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Danzig	Danzig	k1gInSc1	Danzig
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Gedania	Gedanium	k1gNnSc2	Gedanium
<g/>
,	,	kIx,	,
Gedanum	Gedanum	k1gInSc1	Gedanum
abo	abo	k?	abo
Dantiscum	Dantiscum	k1gInSc1	Dantiscum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
a	a	k8xC	a
největších	veliký	k2eAgNnPc2d3	veliký
polských	polský	k2eAgNnPc2d1	polské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
metropolí	metropol	k1gFnPc2	metropol
Pomořského	pomořský	k2eAgNnSc2d1	Pomořské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
Pomoří	Pomoří	k1gNnSc6	Pomoří
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Visle	Visla	k1gFnSc6	Visla
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
u	u	k7c2	u
Gdaňského	gdaňský	k2eAgInSc2d1	gdaňský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
