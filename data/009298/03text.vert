<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Indonésie	Indonésie	k1gFnSc1	Indonésie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
horního	horní	k2eAgNnSc2d1	horní
červeného	červené	k1gNnSc2	červené
a	a	k8xC	a
dolního	dolní	k2eAgNnSc2d1	dolní
bílého	bílé	k1gNnSc2	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Červeno-bílá	Červenoílý	k2eAgFnSc1d1	Červeno-bílá
vlajka	vlajka	k1gFnSc1	vlajka
vlála	vlát	k5eAaImAgFnS	vlát
už	už	k6eAd1	už
v	v	k7c6	v
letech	let	k1gInPc6	let
1293	[number]	k4	1293
<g/>
–	–	k?	–
<g/>
1475	[number]	k4	1475
v	v	k7c6	v
madžapahitském	madžapahitský	k2eAgNnSc6d1	madžapahitský
císařství	císařství	k1gNnSc6	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
barvy	barva	k1gFnPc1	barva
byly	být	k5eAaImAgFnP	být
podmíněny	podmínit	k5eAaPmNgFnP	podmínit
lehko	lehko	k6eAd1	lehko
dosažitelným	dosažitelný	k2eAgInSc7d1	dosažitelný
materiálem	materiál	k1gInSc7	materiál
–	–	k?	–
bílou	bílý	k2eAgFnSc7d1	bílá
bavlnovou	bavlnový	k2eAgFnSc7d1	bavlnový
látkou	látka	k1gFnSc7	látka
a	a	k8xC	a
červeným	červený	k2eAgNnSc7d1	červené
barvivem	barvivo	k1gNnSc7	barvivo
získávaným	získávaný	k2eAgMnSc7d1	získávaný
z	z	k7c2	z
mořských	mořský	k2eAgMnPc2d1	mořský
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
v	v	k7c6	v
sobě	se	k3xPyFc3	se
taktéž	taktéž	k?	taktéž
párové	párový	k2eAgInPc4d1	párový
pojmy	pojem	k1gInPc4	pojem
nebe	nebe	k1gNnSc4	nebe
–	–	k?	–
zem	zem	k1gFnSc1	zem
<g/>
,	,	kIx,	,
statečnost	statečnost	k1gFnSc1	statečnost
–	–	k?	–
čistota	čistota	k1gFnSc1	čistota
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
–	–	k?	–
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
partyzánů	partyzán	k1gMnPc2	partyzán
bojujících	bojující	k2eAgMnPc2d1	bojující
proti	proti	k7c3	proti
nizozemské	nizozemský	k2eAgFnSc3d1	nizozemská
okupaci	okupace	k1gFnSc3	okupace
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
vlajku	vlajka	k1gFnSc4	vlajka
národní	národní	k2eAgFnSc4d1	národní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgFnPc1d1	podobná
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Indonéská	indonéský	k2eAgFnSc1d1	Indonéská
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgFnPc4d1	stejná
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
rozložení	rozložení	k1gNnSc1	rozložení
jako	jako	k8xS	jako
monacká	monacký	k2eAgFnSc1d1	monacká
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
schválená	schválený	k2eAgFnSc1d1	schválená
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
zavedení	zavedení	k1gNnSc3	zavedení
indonéské	indonéský	k2eAgFnSc2d1	Indonéská
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
Monako	Monako	k1gNnSc1	Monako
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
<g/>
.	.	kIx.	.
</s>
<s>
Monacká	monacký	k2eAgFnSc1d1	monacká
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
ale	ale	k8xC	ale
i	i	k9	i
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Indonésie	Indonésie	k1gFnSc2	Indonésie
</s>
</p>
<p>
<s>
Indonéská	indonéský	k2eAgFnSc1d1	Indonéská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Indonésie	Indonésie	k1gFnSc2	Indonésie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Indonéská	indonéský	k2eAgFnSc1d1	Indonéská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
