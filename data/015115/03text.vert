<s>
Pečora	Pečora	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
řece	řeka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
městě	město	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Pečora	Pečora	k1gFnSc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pečora	Pečora	k1gFnSc1
nákladní	nákladní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
na	na	k7c6
řece	řeka	k1gFnSc6
blízko	blízko	k6eAd1
Narjan-MaruZákladní	Narjan-MaruZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc2
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
1809	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
322	#num#	k4
000	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
4100	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
Ural	Ural	k1gInSc1
62	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
6,12	6,12	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
59	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
54,84	54,84	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Pečorské	Pečorský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
68	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
54	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
0,12	0,12	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
0	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
Komi	Komi	k1gNnSc1
<g/>
,	,	kIx,
Archangelská	archangelský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
–	–	k?
Něnecký	Něnecký	k2eAgInSc4d1
autonomní	autonomní	k2eAgInSc4d1
okruh	okruh	k1gInSc4
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc2
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc2
</s>
<s>
Severní	severní	k2eAgInSc1d1
ledový	ledový	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Barentsovo	Barentsův	k2eAgNnSc1d1
moře	moře	k1gNnSc1
,	,	kIx,
Pečorské	Pečorský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
tok	tok	k1gInSc1
řeky	řeka	k1gFnSc2
s	s	k7c7
přítoky	přítok	k1gInPc7
Usou	Usous	k1gInSc2
a	a	k8xC
Ižmou	Ižmá	k1gFnSc4
na	na	k7c6
mapě	mapa	k1gFnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pečora	Pečora	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
П	П	k?
<g/>
,	,	kIx,
komijsky	komijsky	k6eAd1
П	П	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Komiské	Komiský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
v	v	k7c6
Něneckém	Něnecký	k2eAgInSc6d1
autonomním	autonomní	k2eAgInSc6d1
okruhu	okruh	k1gInSc6
v	v	k7c6
Archangelské	archangelský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
evropské	evropský	k2eAgFnSc2d1
části	část	k1gFnSc2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
1809	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plocha	plocha	k1gFnSc1
povodí	povodí	k1gNnSc2
měří	měřit	k5eAaImIp3nS
322	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pátou	pátá	k1gFnSc4
nejdelší	dlouhý	k2eAgFnSc7d3
evropskou	evropský	k2eAgFnSc7d1
řekou	řeka	k1gFnSc7
a	a	k8xC
současně	současně	k6eAd1
největší	veliký	k2eAgFnSc7d3
evropskou	evropský	k2eAgFnSc7d1
řekou	řeka	k1gFnSc7
patřící	patřící	k2eAgFnSc1d1
k	k	k7c3
úmoří	úmoří	k1gNnSc3
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS
na	na	k7c6
Severním	severní	k2eAgInSc6d1
Uralu	Ural	k1gInSc6
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Komiské	Komiský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
teče	téct	k5eAaImIp3nS
severozápadním	severozápadní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
k	k	k7c3
ústí	ústí	k1gNnSc3
Uňji	Uňj	k1gFnSc2
má	mít	k5eAaImIp3nS
horský	horský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
ústím	ústí	k1gNnSc7
Volostnice	Volostnice	k1gFnSc2
se	se	k3xPyFc4
obrací	obracet	k5eAaImIp3nS
na	na	k7c4
sever	sever	k1gInSc4
a	a	k8xC
teče	téct	k5eAaImIp3nS
přes	přes	k7c4
Pečorskou	Pečorský	k2eAgFnSc4d1
nížinu	nížina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šířka	šířka	k1gFnSc1
doliny	dolina	k1gFnSc2
v	v	k7c6
místech	místo	k1gNnPc6
jezerům	jezero	k1gNnPc3
podobných	podobný	k2eAgFnPc2d1
rozšíření	rozšíření	k1gNnPc2
řeky	řeka	k1gFnSc2
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
10	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
protíná	protínat	k5eAaImIp3nS
základní	základní	k2eAgFnPc4d1
horniny	hornina	k1gFnPc4
<g/>
,	,	kIx,
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
úzkou	úzký	k2eAgFnSc4d1
členitou	členitý	k2eAgFnSc4d1
lesnatou	lesnatý	k2eAgFnSc4d1
dolinu	dolina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
ústím	ústí	k1gNnSc7
Usy	usus	k1gInPc1
se	se	k3xPyFc4
stáčí	stáčet	k5eAaImIp3nP
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
široký	široký	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
velkými	velký	k2eAgFnPc7d1
kličkami	klička	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodnost	vodnost	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
úseku	úsek	k1gInSc6
zvyšuje	zvyšovat	k5eAaImIp3nS
na	na	k7c4
dvojnásobek	dvojnásobek	k1gInSc4
a	a	k8xC
šířka	šířka	k1gFnSc1
koryta	koryto	k1gNnSc2
dosahuje	dosahovat	k5eAaImIp3nS
2	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
řeky	řeka	k1gFnSc2
jsou	být	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
nivní	nivní	k2eAgFnPc4d1
louky	louka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
ústím	ústí	k1gNnSc7
Pižmy	pižmo	k1gNnPc7
se	se	k3xPyFc4
směr	směr	k1gInSc1
toku	tok	k1gInSc2
mění	měnit	k5eAaImIp3nS
opět	opět	k6eAd1
k	k	k7c3
severu	sever	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
úvalu	úval	k1gInSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
průtoků	průtok	k1gInPc2
a	a	k8xC
starých	starý	k2eAgNnPc2d1
ramen	rameno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
130	#num#	k4
km	km	kA
od	od	k7c2
ústí	ústí	k1gNnSc2
se	se	k3xPyFc4
řeka	řeka	k1gFnSc1
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgNnPc4
ramena	rameno	k1gNnPc4
-	-	kIx~
východní	východní	k2eAgFnSc3d1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
Pečora	Pečora	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
západní	západní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Malá	malý	k2eAgFnSc1d1
Pečora	Pečora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
ústí	ústí	k1gNnSc6
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
přibližně	přibližně	k6eAd1
45	#num#	k4
km	km	kA
širokou	široký	k2eAgFnSc4d1
deltu	delta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústí	ústí	k1gNnPc1
do	do	k7c2
Pečorského	Pečorský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
Pečorského	Pečorský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
jihovýchodní	jihovýchodní	k2eAgFnSc7d1
částí	část	k1gFnSc7
Barentsova	Barentsův	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
přítoky	přítok	k1gInPc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
zleva	zleva	k6eAd1
–	–	k?
Severní	severní	k2eAgFnSc1d1
Mylva	Mylva	k1gFnSc1
<g/>
,	,	kIx,
Kožva	Kožvo	k1gNnSc2
<g/>
,	,	kIx,
Lyža	Lyžum	k1gNnSc2
<g/>
,	,	kIx,
Ižma	Ižmum	k1gNnSc2
<g/>
,	,	kIx,
Něrica	Něricum	k1gNnSc2
<g/>
,	,	kIx,
Pižma	pižmo	k1gNnSc2
<g/>
,	,	kIx,
Cilma	Cilmum	k1gNnSc2
<g/>
,	,	kIx,
Sula	sout	k5eAaImAgFnS
</s>
<s>
zprava	zprava	k6eAd1
–	–	k?
Ilyč	Ilyč	k1gMnSc1
<g/>
,	,	kIx,
Ščugor	Ščugor	k1gMnSc1
<g/>
,	,	kIx,
Usa	Usa	k1gMnSc1
<g/>
,	,	kIx,
Laja	Laja	k1gMnSc1
<g/>
,	,	kIx,
Šapkina	Šapkina	k1gMnSc1
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Zdrojem	zdroj	k1gInSc7
vody	voda	k1gFnSc2
jsou	být	k5eAaImIp3nP
sněhové	sněhový	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
v	v	k7c6
ústí	ústí	k1gNnSc6
činí	činit	k5eAaImIp3nS
4100	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Je	být	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc7
nejvodnější	vodný	k2eAgFnSc7d3
evropskou	evropský	k2eAgFnSc7d1
řekou	řeka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zamrzá	zamrzat	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
října	říjen	k1gInSc2
a	a	k8xC
rozmrzá	rozmrzat	k5eAaImIp3nS
od	od	k7c2
horního	horní	k2eAgInSc2d1
toku	tok	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
rozmrzání	rozmrzání	k1gNnSc1
je	být	k5eAaImIp3nS
provázeno	provázet	k5eAaImNgNnS
ucpáváním	ucpávání	k1gNnSc7
koryta	koryto	k1gNnSc2
ledovými	ledový	k2eAgFnPc7d1
krami	kra	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
60	#num#	k4
%	%	kIx~
ročního	roční	k2eAgInSc2d1
průtoku	průtok	k1gInSc2
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
jaro	jaro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšších	vysoký	k2eAgInPc2d3
vodních	vodní	k2eAgInPc2d1
stavů	stav	k1gInPc2
dosahuje	dosahovat	k5eAaImIp3nS
od	od	k7c2
konce	konec	k1gInSc2
dubna	duben	k1gInSc2
až	až	k8xS
začátku	začátek	k1gInSc2
května	květen	k1gInSc2
a	a	k8xC
v	v	k7c6
polovině	polovina	k1gFnSc6
května	květen	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
maxima	maxima	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
od	od	k7c2
poloviny	polovina	k1gFnSc2
července	červenec	k1gInSc2
do	do	k7c2
srpna	srpen	k1gInSc2
je	být	k5eAaImIp3nS
vody	voda	k1gFnPc1
v	v	k7c6
řece	řeka	k1gFnSc6
málo	málo	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
často	často	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
povodním	povodeň	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
jsou	být	k5eAaImIp3nP
způsobeny	způsobit	k5eAaPmNgInP
dešti	dešť	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
hladina	hladina	k1gFnSc1
stoupá	stoupat	k5eAaImIp3nS
a	a	k8xC
v	v	k7c6
zimě	zima	k1gFnSc6
opět	opět	k6eAd1
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příliv	příliv	k1gInSc4
a	a	k8xC
odliv	odliv	k1gInSc4
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
až	až	k9
k	k	k7c3
vesnici	vesnice	k1gFnSc3
Oksino	Oksin	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Pravidelná	pravidelný	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
je	být	k5eAaImIp3nS
provozována	provozovat	k5eAaImNgFnS
do	do	k7c2
města	město	k1gNnSc2
Trojicko-Pečorsk	Trojicko-Pečorsk	k1gInSc1
a	a	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
až	až	k9
do	do	k7c2
Usť-Uňji	Usť-Uňj	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
mohou	moct	k5eAaImIp3nP
plout	plout	k5eAaImF
do	do	k7c2
Narjan-Maru	Narjan-Mar	k1gInSc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
110	#num#	k4
km	km	kA
od	od	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
řece	řeka	k1gFnSc6
se	se	k3xPyFc4
plaví	plavit	k5eAaImIp3nS
dřevo	dřevo	k1gNnSc1
svázané	svázaný	k2eAgNnSc1d1
do	do	k7c2
vorů	vor	k1gInPc2
a	a	k8xC
dopravuje	dopravovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
černé	černá	k1gFnSc2
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
ropné	ropný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
<g/>
,	,	kIx,
obilí	obilí	k1gNnSc1
aj.	aj.	kA
Řeka	Řek	k1gMnSc2
protéká	protékat	k5eAaImIp3nS
městy	město	k1gNnPc7
Usť-Ilyč	Usť-Ilyč	k1gInSc1
<g/>
,	,	kIx,
Trojicko-Pečorsk	Trojicko-Pečorsk	k1gInSc1
<g/>
,	,	kIx,
Vuktyl	Vuktyl	k1gInSc1
<g/>
,	,	kIx,
Pečora	Pečora	k1gFnSc1
nebo	nebo	k8xC
Narjan-Mar	Narjan-Mar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
П	П	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Gunn	Gunn	k1gInSc1
G.	G.	kA
P.	P.	kA
<g/>
,	,	kIx,
Pečora	Pečora	k1gFnSc1
–	–	k?
zlaté	zlatý	k2eAgInPc4d1
břehy	břeh	k1gInPc4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
1972	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Г	Г	k?
Г	Г	k?
П	П	k?
<g/>
,	,	kIx,
П	П	k?
-	-	kIx~
з	з	k?
б	б	k?
<g/>
,	,	kIx,
М	М	k?
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Pitystin	Pitystin	k2eAgMnSc1d1
M.	M.	kA
<g/>
,	,	kIx,
Pečora	Pečora	k1gFnSc1
<g/>
,	,	kIx,
Ekonomicko-geografický	ekonomicko-geografický	k2eAgInSc1d1
popis	popis	k1gInSc1
<g/>
,	,	kIx,
Syktyvkar	Syktyvkar	k1gInSc1
1974	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
П	П	k?
М	М	k?
<g/>
,	,	kIx,
П	П	k?
<g/>
,	,	kIx,
Э	Э	k?
о	о	k?
<g/>
,	,	kIx,
С	С	k?
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Pečora	Pečora	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pečora	Pečor	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
651661	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4235794-9	4235794-9	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
315158288	#num#	k4
</s>
