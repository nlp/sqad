<p>
<s>
Malachit	malachit	k1gInSc1	malachit
je	být	k5eAaImIp3nS	být
uhličitan	uhličitan	k1gInSc4	uhličitan
mědi	měď	k1gFnSc2	měď
s	s	k7c7	s
OH	OH	kA	OH
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Malachit	malachit	k1gInSc1	malachit
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
od	od	k7c2	od
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgInPc1d1	zelený
až	až	k9	až
po	po	k7c4	po
černozelenou	černozelený	k2eAgFnSc4d1	černozelená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
neprůhledný	průhledný	k2eNgMnSc1d1	neprůhledný
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
práškovitých	práškovitý	k2eAgInPc2d1	práškovitý
<g/>
,	,	kIx,	,
zemitých	zemitý	k2eAgInPc2d1	zemitý
<g/>
,	,	kIx,	,
vrstevnatých	vrstevnatý	k2eAgInPc2d1	vrstevnatý
<g/>
,	,	kIx,	,
ledvinitých	ledvinitý	k2eAgInPc2d1	ledvinitý
a	a	k8xC	a
krápníkovitých	krápníkovitý	k2eAgInPc2d1	krápníkovitý
agregátů	agregát	k1gInPc2	agregát
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
také	také	k9	také
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
malých	malý	k2eAgInPc2d1	malý
sloupcovitých	sloupcovitý	k2eAgInPc2d1	sloupcovitý
a	a	k8xC	a
jehlicovitých	jehlicovitý	k2eAgInPc2d1	jehlicovitý
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
výskytů	výskyt	k1gInPc2	výskyt
sulfidů	sulfid	k1gInPc2	sulfid
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
např.	např.	kA	např.
chalkopyrit	chalkopyrit	k1gInSc1	chalkopyrit
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
přeměnou	přeměna	k1gFnSc7	přeměna
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malachit	malachit	k1gInSc1	malachit
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
minerální	minerální	k2eAgInSc1d1	minerální
pigment	pigment	k1gInSc1	pigment
v	v	k7c6	v
zelených	zelený	k2eAgFnPc6d1	zelená
barvách	barva	k1gFnPc6	barva
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
kyseliny	kyselina	k1gFnPc4	kyselina
a	a	k8xC	a
kolísání	kolísání	k1gNnPc4	kolísání
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
akumulací	akumulace	k1gFnSc7	akumulace
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xS	jako
méně	málo	k6eAd2	málo
významná	významný	k2eAgFnSc1d1	významná
ruda	ruda	k1gFnSc1	ruda
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
55	[number]	k4	55
%	%	kIx~	%
až	až	k9	až
57,4	[number]	k4	57,4
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Nacházen	nacházen	k2eAgMnSc1d1	nacházen
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
mědnatých	mědnatý	k2eAgNnPc2d1	mědnatý
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
jako	jako	k8xS	jako
skalní	skalní	k2eAgFnSc1d1	skalní
zeleň	zeleň	k1gFnSc1	zeleň
<g/>
.	.	kIx.	.
<g/>
Název	název	k1gInSc1	název
minerálu	minerál	k1gInSc2	minerál
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
malache	malachat	k5eAaPmIp3nS	malachat
označující	označující	k2eAgInSc4d1	označující
sléz	sléz	k1gInSc4	sléz
a	a	k8xC	a
sytě	sytě	k6eAd1	sytě
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
v	v	k7c6	v
alternačních	alternační	k2eAgFnPc6d1	alternační
a	a	k8xC	a
oxidačních	oxidační	k2eAgFnPc6d1	oxidační
oblastech	oblast	k1gFnPc6	oblast
měďných	měďná	k1gFnPc2	měďná
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	s	k7c7	s
sekundárními	sekundární	k2eAgInPc7d1	sekundární
minerály	minerál	k1gInPc7	minerál
včetně	včetně	k7c2	včetně
azuritu	azurit	k1gInSc2	azurit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniká	vznikat	k5eAaImIp3nS	vznikat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Zjednoduššeně	Zjednoduššeně	k6eAd1	Zjednoduššeně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
malachit	malachit	k1gInSc1	malachit
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvětrávání	zvětrávání	k1gNnSc3	zvětrávání
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
z	z	k7c2	z
jistého	jistý	k2eAgInSc2d1	jistý
pohledu	pohled	k1gInSc2	pohled
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
na	na	k7c6	na
měděných	měděný	k2eAgFnPc6d1	měděná
střechách	střecha	k1gFnPc6	střecha
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
zvětrávání	zvětrávání	k1gNnSc6	zvětrávání
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
povlak	povlak	k1gInSc4	povlak
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
horninách	hornina	k1gFnPc6	hornina
a	a	k8xC	a
minerálech	minerál	k1gInPc6	minerál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
také	také	k9	také
krápníky	krápník	k1gInPc4	krápník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
3,5	[number]	k4	3,5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
krápníkovitý	krápníkovitý	k2eAgInSc4d1	krápníkovitý
<g/>
,	,	kIx,	,
vláknitý	vláknitý	k2eAgInSc4d1	vláknitý
<g/>
,	,	kIx,	,
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
<g/>
,	,	kIx,	,
jehličkovitý	jehličkovitý	k2eAgInSc4d1	jehličkovitý
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
3,9	[number]	k4	3,9
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
201	[number]	k4	201
<g/>
}	}	kIx)	}
a	a	k8xC	a
dobrá	dobrá	k9	dobrá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
–	–	k?	–
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
až	až	k8xS	až
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barvu	barva	k1gFnSc4	barva
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
zelenou	zelená	k1gFnSc4	zelená
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
světle	světle	k6eAd1	světle
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
,	,	kIx,	,
matný	matný	k2eAgInSc1d1	matný
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Cu	Cu	k1gFnSc1	Cu
57,48	[number]	k4	57,48
%	%	kIx~	%
<g/>
,	,	kIx,	,
C	C	kA	C
5,43	[number]	k4	5,43
%	%	kIx~	%
<g/>
,	,	kIx,	,
H	H	kA	H
0,91	[number]	k4	0,91
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
36,18	[number]	k4	36,18
%	%	kIx~	%
<g/>
,	,	kIx,	,
šumí	šumět	k5eAaImIp3nS	šumět
v	v	k7c6	v
HCl	HCl	k1gFnSc6	HCl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Příbram	Příbram	k1gFnSc1	Příbram
</s>
</p>
<p>
<s>
Podkrkonoší	Podkrkonoší	k1gNnSc1	Podkrkonoší
–	–	k?	–
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
permských	permský	k2eAgInPc6d1	permský
sedimentech	sediment	k1gInPc6	sediment
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
nesouvislé	souvislý	k2eNgInPc4d1	nesouvislý
horizonty	horizont	k1gInPc4	horizont
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měděnec	měděnec	k1gInSc1	měděnec
a	a	k8xC	a
vrch	vrch	k1gInSc1	vrch
Mědník	Mědník	k1gInSc1	Mědník
</s>
</p>
<p>
<s>
svět	svět	k1gInSc1	svět
–	–	k?	–
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
Ural	Ural	k1gInSc1	Ural
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
</s>
</p>
<p>
<s>
==	==	k?	==
Parageneze	parageneze	k1gFnSc2	parageneze
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
časté	častý	k2eAgFnSc6d1	častá
asociaci	asociace	k1gFnSc6	asociace
s	s	k7c7	s
azuritem	azurit	k1gInSc7	azurit
<g/>
,	,	kIx,	,
atacamitem	atacamit	k1gInSc7	atacamit
<g/>
,	,	kIx,	,
aurichalcitem	aurichalcit	k1gInSc7	aurichalcit
<g/>
,	,	kIx,	,
brochantitem	brochantit	k1gInSc7	brochantit
<g/>
,	,	kIx,	,
kalcitem	kalcit	k1gInSc7	kalcit
<g/>
,	,	kIx,	,
chalcedonem	chalcedon	k1gInSc7	chalcedon
<g/>
,	,	kIx,	,
chryzokolem	chryzokol	k1gInSc7	chryzokol
<g/>
,	,	kIx,	,
kupritem	kuprit	k1gInSc7	kuprit
<g/>
,	,	kIx,	,
limonitem	limonit	k1gInSc7	limonit
<g/>
,	,	kIx,	,
tenoritem	tenorit	k1gInSc7	tenorit
a	a	k8xC	a
wadem	wadem	k?	wadem
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Malachit	malachit	k1gInSc1	malachit
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c4	na
dekorativní	dekorativní	k2eAgInPc4d1	dekorativní
účely	účel	k1gInPc4	účel
a	a	k8xC	a
jako	jako	k9	jako
šperkařský	šperkařský	k2eAgInSc4d1	šperkařský
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
rozemletý	rozemletý	k2eAgInSc1d1	rozemletý
malachit	malachit	k1gInSc1	malachit
používal	používat	k5eAaImAgInS	používat
i	i	k9	i
jako	jako	k9	jako
líčidlo	líčidlo	k1gNnSc4	líčidlo
<g/>
.	.	kIx.	.
<g/>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
malachit	malachit	k1gInSc1	malachit
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
méně	málo	k6eAd2	málo
výrazná	výrazný	k2eAgFnSc1d1	výrazná
ruda	ruda	k1gFnSc1	ruda
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
odedávna	odedávna	k6eAd1	odedávna
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vyřezávali	vyřezávat	k5eAaImAgMnP	vyřezávat
kameje	kameje	k1gFnSc1	kameje
<g/>
,	,	kIx,	,
amulety	amulet	k1gInPc1	amulet
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
ozdobné	ozdobný	k2eAgInPc1d1	ozdobný
předměty	předmět	k1gInPc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Znali	znát	k5eAaImAgMnP	znát
ho	on	k3xPp3gNnSc4	on
i	i	k9	i
staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Drcený	drcený	k2eAgMnSc1d1	drcený
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
jako	jako	k9	jako
barvivo	barvivo	k1gNnSc4	barvivo
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
líčidlo	líčidlo	k1gNnSc1	líčidlo
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
chránit	chránit	k5eAaImF	chránit
děti	dítě	k1gFnPc4	dítě
před	před	k7c7	před
kouzly	kouzlo	k1gNnPc7	kouzlo
a	a	k8xC	a
čáry	čár	k1gInPc7	čár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
antiky	antika	k1gFnSc2	antika
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
očních	oční	k2eAgFnPc2d1	oční
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
používal	používat	k5eAaImAgInS	používat
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
růstu	růst	k1gInSc2	růst
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
podporovat	podporovat	k5eAaImF	podporovat
léčitele	léčitel	k1gMnPc4	léčitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
malachitem	malachit	k1gInSc7	malachit
pracovali	pracovat	k5eAaImAgMnP	pracovat
a	a	k8xC	a
utišovat	utišovat	k5eAaImF	utišovat
bolest	bolest	k1gFnSc4	bolest
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Malachit	malachit	k1gInSc1	malachit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Malachit	malachit	k1gInSc1	malachit
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Malachit	malachit	k1gInSc1	malachit
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Malachit	malachit	k1gInSc1	malachit
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
