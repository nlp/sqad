<s>
Malachit	malachit	k1gInSc1	malachit
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
od	od	k7c2	od
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgInPc1d1	zelený
až	až	k9	až
po	po	k7c4	po
černozelenou	černozelený	k2eAgFnSc4d1	černozelená
<g/>
.	.	kIx.	.
</s>
