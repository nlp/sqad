<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Nepálu	Nepál	k1gInSc2	Nepál
je	být	k5eAaImIp3nS	být
tvarem	tvar	k1gInSc7	tvar
naprosto	naprosto	k6eAd1	naprosto
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
nestejně	stejně	k6eNd1	stejně
vysokých	vysoký	k2eAgFnPc2d1	vysoká
karmínových	karmínový	k2eAgFnPc2d1	karmínová
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
lemovaných	lemovaný	k2eAgInPc2d1	lemovaný
pravoúhlých	pravoúhlý	k2eAgInPc2d1	pravoúhlý
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
položených	položená	k1gFnPc2	položená
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
přes	přes	k7c4	přes
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stylizovaná	stylizovaný	k2eAgFnSc1d1	stylizovaná
bílá	bílý	k2eAgFnSc1d1	bílá
silueta	silueta	k1gFnSc1	silueta
Měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
osmi	osm	k4xCc7	osm
paprsky	paprsek	k1gInPc7	paprsek
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
pak	pak	k6eAd1	pak
silueta	silueta	k1gFnSc1	silueta
Slunce	slunce	k1gNnSc2	slunce
s	s	k7c7	s
dvanácti	dvanáct	k4xCc2	dvanáct
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
změně	změna	k1gFnSc3	změna
vlajky	vlajka	k1gFnSc2	vlajka
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
vypuštěny	vypuštěn	k2eAgFnPc1d1	vypuštěna
kresby	kresba	k1gFnPc1	kresba
tváří	tvář	k1gFnPc2	tvář
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symbolika	symbolika	k1gFnSc1	symbolika
==	==	k?	==
</s>
</p>
<p>
<s>
Karmínová	karmínový	k2eAgFnSc1d1	karmínová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
nepálskou	nepálský	k2eAgFnSc7d1	nepálská
národní	národní	k2eAgFnSc7d1	národní
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
statečnosti	statečnost	k1gFnSc2	statečnost
Nepálců	Nepálec	k1gMnPc2	Nepálec
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
květiny	květina	k1gFnSc2	květina
Pěnišníku	pěnišník	k1gInSc2	pěnišník
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
lemu	lem	k1gInSc2	lem
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
mír	mír	k1gInSc4	mír
a	a	k8xC	a
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Červená	Červená	k1gFnSc1	Červená
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
barvy	barva	k1gFnPc4	barva
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
chladu	chlad	k1gInSc2	chlad
<g/>
,	,	kIx,	,
kontrastů	kontrast	k1gInPc2	kontrast
nepálského	nepálský	k2eAgNnSc2d1	nepálské
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
<g/>
Dva	dva	k4xCgInPc1	dva
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
představují	představovat	k5eAaImIp3nP	představovat
dvě	dva	k4xCgNnPc1	dva
hlavní	hlavní	k2eAgNnPc1d1	hlavní
nepálská	nepálský	k2eAgNnPc1d1	nepálské
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
:	:	kIx,	:
buddhismus	buddhismus	k1gInSc1	buddhismus
a	a	k8xC	a
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
vlajky	vlajka	k1gFnSc2	vlajka
připomíná	připomínat	k5eAaImIp3nS	připomínat
pohoří	pohoří	k1gNnSc4	pohoří
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
.	.	kIx.	.
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
věčný	věčný	k2eAgInSc4d1	věčný
koloběh	koloběh	k1gInSc4	koloběh
času	čas	k1gInSc2	čas
a	a	k8xC	a
přání	přání	k1gNnSc4	přání
existence	existence	k1gFnSc2	existence
Nepálu	Nepál	k1gInSc2	Nepál
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
obě	dva	k4xCgNnPc4	dva
vesmírná	vesmírný	k2eAgNnPc4d1	vesmírné
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
vlajky	vlajka	k1gFnSc2	vlajka
symbolem	symbol	k1gInSc7	symbol
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc2	slunce
rodiny	rodina	k1gFnSc2	rodina
Ránů	Rán	k1gMnPc2	Rán
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměr	k1gInPc4	rozměr
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Ústavy	ústava	k1gFnSc2	ústava
království	království	k1gNnSc1	království
Nepálu	Nepál	k1gInSc2	Nepál
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1962	[number]	k4	1962
ke	k	k7c3	k
článku	článek	k1gInSc3	článek
č.	č.	k?	č.
5	[number]	k4	5
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgFnPc4d1	uvedena
instrukce	instrukce	k1gFnPc4	instrukce
k	k	k7c3	k
narýsování	narýsování	k1gNnSc3	narýsování
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
konstrukční	konstrukční	k2eAgInPc1d1	konstrukční
detaily	detail	k1gInPc1	detail
byly	být	k5eAaImAgInP	být
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
převzaty	převzít	k5eAaPmNgFnP	převzít
do	do	k7c2	do
nepálské	nepálský	k2eAgFnSc2d1	nepálská
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
prozatímní	prozatímní	k2eAgInPc1d1	prozatímní
ústavy	ústav	k1gInPc1	ústav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postup	postup	k1gInSc1	postup
tvorby	tvorba	k1gFnSc2	tvorba
Národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
Nepálu	Nepál	k1gInSc2	Nepál
</s>
</p>
<p>
<s>
A	a	k9	a
<g/>
)	)	kIx)	)
<g/>
Postup	postup	k1gInSc1	postup
tvorby	tvorba	k1gFnSc2	tvorba
tvaru	tvar	k1gInSc2	tvar
bez	bez	k7c2	bez
modrého	modrý	k2eAgInSc2d1	modrý
lemu	lem	k1gInSc2	lem
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
karmínové	karmínový	k2eAgFnSc2d1	karmínová
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
,	,	kIx,	,
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
úsečku	úsečka	k1gFnSc4	úsečka
AB	AB	kA	AB
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
délky	délka	k1gFnSc2	délka
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
bodu	bod	k1gInSc2	bod
A	a	k8xC	a
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
úsečku	úsečka	k1gFnSc4	úsečka
AC	AC	kA	AC
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
AB	AB	kA	AB
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
AC	AC	kA	AC
se	se	k3xPyFc4	se
rovnala	rovnat	k5eAaImAgFnS	rovnat
AB	AB	kA	AB
plus	plus	k6eAd1	plus
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
AB	AB	kA	AB
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úsečce	úsečka	k1gFnSc6	úsečka
AC	AC	kA	AC
vyznač	vyznačit	k5eAaPmRp2nS	vyznačit
bod	bod	k1gInSc4	bod
D	D	kA	D
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
úsečka	úsečka	k1gFnSc1	úsečka
AD	ad	k7c4	ad
rovnala	rovnat	k5eAaImAgFnS	rovnat
úsečce	úsečka	k1gFnSc6	úsečka
AB	AB	kA	AB
<g/>
.	.	kIx.	.
</s>
<s>
Spoj	spoj	k1gFnSc1	spoj
BD	BD	kA	BD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
úsečce	úsečka	k1gFnSc6	úsečka
BD	BD	kA	BD
vyznačte	vyznačit	k5eAaPmRp2nP	vyznačit
bod	bod	k1gInSc4	bod
E	E	kA	E
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
BE	BE	kA	BE
rovnala	rovnat	k5eAaImAgFnS	rovnat
AB	AB	kA	AB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
Narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
úsečku	úsečka	k1gFnSc4	úsečka
FG	FG	kA	FG
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
k	k	k7c3	k
AB	AB	kA	AB
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
bod	bod	k1gInSc1	bod
E	E	kA	E
náleží	náležet	k5eAaImIp3nS	náležet
FG	FG	kA	FG
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
F	F	kA	F
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
AC	AC	kA	AC
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
FG	FG	kA	FG
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
AB	AB	kA	AB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
Spoj	spoj	k1gInSc1	spoj
CG	cg	kA	cg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
Postup	postup	k1gInSc1	postup
tvorby	tvorba	k1gFnSc2	tvorba
Měsíce	měsíc	k1gInSc2	měsíc
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
úsečce	úsečka	k1gFnSc6	úsečka
AB	AB	kA	AB
vyznačte	vyznačit	k5eAaPmRp2nP	vyznačit
bod	bod	k1gInSc4	bod
H	H	kA	H
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
AH	ah	k0	ah
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
jedné	jeden	k4xCgFnSc6	jeden
čtvrtině	čtvrtina	k1gFnSc3	čtvrtina
úsečky	úsečka	k1gFnSc2	úsečka
AB	AB	kA	AB
a	a	k8xC	a
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
úsečku	úsečka	k1gFnSc4	úsečka
HI	hi	k0	hi
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
k	k	k7c3	k
AC	AC	kA	AC
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protne	protnout	k5eAaPmIp3nS	protnout
úsečku	úsečka	k1gFnSc4	úsečka
CG	cg	kA	cg
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc1	bod
I.	I.	kA	I.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
Bod	bod	k1gInSc1	bod
J	J	kA	J
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
CF	CF	kA	CF
<g/>
.	.	kIx.	.
</s>
<s>
Narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
úsečku	úsečka	k1gFnSc4	úsečka
JK	JK	kA	JK
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
k	k	k7c3	k
AB	AB	kA	AB
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protne	protnout	k5eAaPmIp3nS	protnout
úsečku	úsečka	k1gFnSc4	úsečka
CG	cg	kA	cg
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc1	bod
K.	K.	kA	K.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
Bod	bod	k1gInSc1	bod
L	L	kA	L
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
průniku	průnik	k1gInSc6	průnik
úseček	úsečka	k1gFnPc2	úsečka
JK	JK	kA	JK
a	a	k8xC	a
HI	hi	k0	hi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
Spoj	spoj	k1gInSc1	spoj
JG	JG	kA	JG
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
Bod	bod	k1gInSc1	bod
M	M	kA	M
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
průniku	průnik	k1gInSc6	průnik
úseček	úsečka	k1gFnPc2	úsečka
JG	JG	kA	JG
a	a	k8xC	a
HI	hi	k0	hi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
Vyznačte	vyznačit	k5eAaPmRp2nP	vyznačit
bod	bod	k1gInSc4	bod
N	N	kA	N
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
HI	hi	k0	hi
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
průnik	průnik	k1gInSc1	průnik
kružnice	kružnice	k1gFnSc2	kružnice
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
M	M	kA	M
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
bodu	bod	k1gInSc2	bod
M	M	kA	M
a	a	k8xC	a
úsečky	úsečka	k1gFnPc1	úsečka
BD	BD	kA	BD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
Narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
úsečku	úsečka	k1gFnSc4	úsečka
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
k	k	k7c3	k
AB	AB	kA	AB
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgFnSc1d1	začínající
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
O	O	kA	O
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
AC	AC	kA	AC
a	a	k8xC	a
končící	končící	k2eAgInSc1d1	končící
v	v	k7c6	v
M.	M.	kA	M.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
L	L	kA	L
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
LN	LN	kA	LN
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
půlkružnici	půlkružnice	k1gFnSc4	půlkružnice
ve	v	k7c4	v
spodní	spodní	k2eAgFnPc4d1	spodní
části	část	k1gFnPc4	část
a	a	k8xC	a
nechť	nechť	k9	nechť
body	bod	k1gInPc1	bod
P	P	kA	P
a	a	k8xC	a
Q	Q	kA	Q
leží	ležet	k5eAaImIp3nS	ležet
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
půlkružnice	půlkružnice	k1gFnSc1	půlkružnice
příslušně	příslušně	k6eAd1	příslušně
protíná	protínat	k5eAaImIp3nS	protínat
OM	OM	kA	OM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
M	M	kA	M
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
MQ	MQ	kA	MQ
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
půlkružnici	půlkružnice	k1gFnSc4	půlkružnice
ve	v	k7c4	v
spodní	spodní	k2eAgFnPc4d1	spodní
části	část	k1gFnPc4	část
procházející	procházející	k2eAgFnPc4d1	procházející
P	P	kA	P
a	a	k8xC	a
Q.	Q.	kA	Q.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
N	N	kA	N
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
NM	NM	kA	NM
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
oblouk	oblouk	k1gInSc4	oblouk
protínající	protínající	k2eAgInSc4d1	protínající
PNQ	PNQ	kA	PNQ
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
R	R	kA	R
a	a	k8xC	a
S.	S.	kA	S.
Spoj	spoj	k1gInSc4	spoj
RS	RS	kA	RS
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
T	T	kA	T
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
průniku	průnik	k1gInSc6	průnik
úseček	úsečka	k1gFnPc2	úsečka
RS	RS	kA	RS
a	a	k8xC	a
HI	hi	k0	hi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
T	T	kA	T
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
TS	ts	k0	ts
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
půlkružnici	půlkružnice	k1gFnSc4	půlkružnice
v	v	k7c4	v
horní	horní	k2eAgFnPc4d1	horní
části	část	k1gFnPc4	část
nad	nad	k7c7	nad
PNQ	PNQ	kA	PNQ
protínajíc	protínat	k5eAaImSgFnS	protínat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
T	T	kA	T
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
TM	TM	kA	TM
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
oblouk	oblouk	k1gInSc4	oblouk
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
nad	nad	k7c7	nad
PNQ	PNQ	kA	PNQ
protínajíc	protínat	k5eAaImSgFnS	protínat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
Osm	osm	k4xCc4	osm
stejných	stejný	k2eAgInPc2d1	stejný
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
měsíce	měsíc	k1gInSc2	měsíc
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
uvnitř	uvnitř	k7c2	uvnitř
půlkružnice	půlkružnice	k1gFnSc2	půlkružnice
č.	č.	k?	č.
16	[number]	k4	16
a	a	k8xC	a
vně	vně	k7c2	vně
oblouku	oblouk	k1gInSc2	oblouk
č.	č.	k?	č.
17	[number]	k4	17
toho	ten	k3xDgInSc2	ten
odstavce	odstavec	k1gInSc2	odstavec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
Postup	postup	k1gInSc1	postup
tvorby	tvorba	k1gFnSc2	tvorba
Slunce	slunce	k1gNnSc2	slunce
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
Bod	bod	k1gInSc1	bod
U	U	kA	U
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
AF	AF	kA	AF
<g/>
.	.	kIx.	.
</s>
<s>
Narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
úsečku	úsečka	k1gFnSc4	úsečka
UV	UV	kA	UV
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
k	k	k7c3	k
úsečce	úsečka	k1gFnSc3	úsečka
AB	AB	kA	AB
tak	tak	k8xC	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
procházela	procházet	k5eAaImAgFnS	procházet
úsečkou	úsečka	k1gFnSc7	úsečka
BE	BE	kA	BE
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
V.	V.	kA	V.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
W	W	kA	W
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
průniku	průnik	k1gInSc6	průnik
úseček	úsečka	k1gFnPc2	úsečka
HI	hi	k0	hi
a	a	k8xC	a
UV	UV	kA	UV
<g/>
,	,	kIx,	,
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
MN	MN	kA	MN
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
W	W	kA	W
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
LN	LN	kA	LN
narýsujte	narýsovat	k5eAaPmRp2nP	narýsovat
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
Dvanáct	dvanáct	k4xCc4	dvanáct
stejných	stejný	k2eAgInPc2d1	stejný
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
slunce	slunce	k1gNnSc2	slunce
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
kružnicí	kružnice	k1gFnSc7	kružnice
č.	č.	k?	č.
20	[number]	k4	20
a	a	k8xC	a
č.	č.	k?	č.
21	[number]	k4	21
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vrcholy	vrchol	k1gInPc7	vrchol
dvou	dva	k4xCgMnPc2	dva
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
úsečce	úsečka	k1gFnSc6	úsečka
HI	hi	k0	hi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
Postup	postup	k1gInSc1	postup
tvorby	tvorba	k1gFnSc2	tvorba
lemu	lem	k1gInSc2	lem
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
<g/>
Šířka	šířka	k1gFnSc1	šířka
lemu	lem	k1gInSc2	lem
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
velikost	velikost	k1gFnSc1	velikost
TN	TN	kA	TN
<g/>
.	.	kIx.	.
</s>
<s>
Lem	lem	k1gInSc1	lem
je	být	k5eAaImIp3nS	být
vybarven	vybarvit	k5eAaPmNgInS	vybarvit
tmavě	tmavě	k6eAd1	tmavě
modře	modro	k6eAd1	modro
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proveden	provést	k5eAaPmNgInS	provést
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
obvodu	obvod	k1gInSc6	obvod
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
v	v	k7c6	v
pěti	pět	k4xCc6	pět
úhlech	úhel	k1gInPc6	úhel
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
vnější	vnější	k2eAgInPc1d1	vnější
úhly	úhel	k1gInPc1	úhel
rovnat	rovnat	k5eAaImF	rovnat
vnitřním	vnitřní	k2eAgInPc3d1	vnitřní
úhlům	úhel	k1gInPc3	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
Výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc1d1	uvedený
lem	lem	k1gInSc1	lem
bude	být	k5eAaImBp3nS	být
použit	použít	k5eAaPmNgInS	použít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vlajka	vlajka	k1gFnSc1	vlajka
bude	být	k5eAaImBp3nS	být
pověšena	pověsit	k5eAaPmNgFnS	pověsit
na	na	k7c6	na
lanku	lanko	k1gNnSc6	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zvednuta	zvednout	k5eAaPmNgFnS	zvednout
na	na	k7c6	na
tyči	tyč	k1gFnSc6	tyč
<g/>
,	,	kIx,	,
otvor	otvor	k1gInSc4	otvor
na	na	k7c6	na
lemu	lem	k1gInSc6	lem
strany	strana	k1gFnSc2	strana
AC	AC	kA	AC
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prodloužen	prodloužit	k5eAaPmNgInS	prodloužit
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
<g/>
.	.	kIx.	.
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Úsečky	úsečka	k1gFnPc1	úsečka
HI	hi	k0	hi
<g/>
,	,	kIx,	,
RS	RS	kA	RS
<g/>
,	,	kIx,	,
FE	FE	kA	FE
<g/>
,	,	kIx,	,
ED	ED	kA	ED
<g/>
,	,	kIx,	,
JG	JG	kA	JG
<g/>
,	,	kIx,	,
OQ	OQ	kA	OQ
<g/>
,	,	kIx,	,
JK	JK	kA	JK
a	a	k8xC	a
UV	UV	kA	UV
jsou	být	k5eAaImIp3nP	být
myšlené	myšlený	k2eAgFnPc1d1	myšlená
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
zobrazeny	zobrazit	k5eAaPmNgInP	zobrazit
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
vnější	vnější	k2eAgFnSc2d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
kružnice	kružnice	k1gFnSc2	kružnice
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
oblouky	oblouk	k1gInPc1	oblouk
kromě	kromě	k7c2	kromě
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
není	být	k5eNaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
<g/>
,	,	kIx,	,
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xC	jako
~	~	kIx~	~
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
lemování	lemování	k1gNnSc2	lemování
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
lemem	lem	k1gInSc7	lem
je	být	k5eAaImIp3nS	být
výpočtem	výpočet	k1gInSc7	výpočet
poměr	poměr	k1gInSc4	poměr
1,219	[number]	k4	1,219
<g/>
0	[number]	k4	0
<g/>
1033	[number]	k4	1033
<g/>
...	...	k?	...
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
Nepálská	nepálský	k2eAgFnSc1d1	nepálská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jedinou	jediný	k2eAgFnSc7d1	jediná
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
která	který	k3yRgNnPc4	který
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
letopočtu	letopočet	k1gInSc2	letopočet
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Nepálu	Nepál	k1gInSc2	Nepál
existovalo	existovat	k5eAaImAgNnS	existovat
několik	několik	k4yIc1	několik
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
stát	stát	k5eAaPmF	stát
Liččhaviú	Liččhaviú	k1gFnSc4	Liččhaviú
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zemi	zem	k1gFnSc4	zem
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
a	a	k8xC	a
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
Gurkhové	Gurkhová	k1gFnPc4	Gurkhová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1769	[number]	k4	1769
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
vznik	vznik	k1gInSc1	vznik
Království	království	k1gNnSc2	království
Nepál	Nepál	k1gInSc1	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Tibetu	Tibet	k1gInSc2	Tibet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
musel	muset	k5eAaImAgInS	muset
Nepál	Nepál	k1gInSc1	Nepál
přijmout	přijmout	k5eAaPmF	přijmout
čínskou	čínský	k2eAgFnSc4d1	čínská
suverenitu	suverenita	k1gFnSc4	suverenita
a	a	k8xC	a
platit	platit	k5eAaImF	platit
ji	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
vazalské	vazalský	k2eAgInPc1d1	vazalský
poplatky	poplatek	k1gInPc1	poplatek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1818	[number]	k4	1818
na	na	k7c4	na
Nepál	Nepál	k1gInSc4	Nepál
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
vojsko	vojsko	k1gNnSc1	vojsko
vedené	vedený	k2eAgNnSc1d1	vedené
Britskou	britský	k2eAgFnSc7d1	britská
Východoindickou	východoindický	k2eAgFnSc7d1	Východoindická
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
Sikkim	Sikkim	k1gInSc4	Sikkim
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
Teraje	Teraj	k1gInSc2	Teraj
<g/>
.	.	kIx.	.
</s>
<s>
Nepál	Nepál	k1gInSc1	Nepál
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgInS	být
dobyt	dobyt	k2eAgMnSc1d1	dobyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spravován	spravován	k2eAgMnSc1d1	spravován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Britske	Britsk	k1gFnSc2	Britsk
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
se	se	k3xPyFc4	se
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dynastie	dynastie	k1gFnSc2	dynastie
Ránů	Rán	k1gMnPc2	Rán
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
dědičně	dědičně	k6eAd1	dědičně
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
Šáhů	šáh	k1gMnPc2	šáh
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
červeného	červený	k2eAgInSc2d1	červený
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
Měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Symbolem	symbol	k1gInSc7	symbol
Ránů	Rán	k1gMnPc2	Rán
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
stejného	stejný	k2eAgInSc2d1	stejný
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
Slunce	slunce	k1gNnSc2	slunce
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
Šáhů	šáh	k1gMnPc2	šáh
a	a	k8xC	a
Ránů	Rán	k1gMnPc2	Rán
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
nepálské	nepálský	k2eAgFnSc2d1	nepálská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zavedl	zavést	k5eAaPmAgMnS	zavést
král	král	k1gMnSc1	král
Prithvi	Prithev	k1gFnSc3	Prithev
Bira	Bir	k1gInSc2	Bir
Bikram	Bikram	k1gInSc1	Bikram
Šáh	šáh	k1gMnSc1	šáh
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
tvořily	tvořit	k5eAaImAgInP	tvořit
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
z	z	k7c2	z
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
umístěné	umístěný	k2eAgInPc4d1	umístěný
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgInPc4d1	oddělený
<g/>
,	,	kIx,	,
červené	červený	k2eAgInPc4d1	červený
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
lemované	lemovaný	k2eAgInPc1d1	lemovaný
rovnostranné	rovnostranný	k2eAgInPc1d1	rovnostranný
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
(	(	kIx(	(
<g/>
plameny	plamen	k1gInPc1	plamen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
byl	být	k5eAaImAgInS	být
symbol	symbol	k1gInSc4	symbol
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
bez	bez	k7c2	bez
lidských	lidský	k2eAgFnPc2d1	lidská
tváří	tvář	k1gFnPc2	tvář
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
Spojené	spojený	k2eAgInPc4d1	spojený
království	království	k1gNnPc4	království
uznalo	uznat	k5eAaPmAgNnS	uznat
formálně	formálně	k6eAd1	formálně
nezávislost	nezávislost	k1gFnSc4	nezávislost
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Nepál	Nepál	k1gInSc4	Nepál
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Percevala	Percevala	k1gMnSc2	Percevala
Landona	Landon	k1gMnSc2	Landon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
nepálskými	nepálský	k2eAgFnPc7d1	nepálská
vlajkami	vlajka	k1gFnPc7	vlajka
zobrazena	zobrazit	k5eAaPmNgNnP	zobrazit
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sešitím	sešití	k1gNnSc7	sešití
dvou	dva	k4xCgFnPc2	dva
stejných	stejný	k2eAgFnPc2d1	stejná
červených	červená	k1gFnPc2	červená
plamenů	plamen	k1gInPc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgNnSc6d1	horní
poli	pole	k1gNnSc6	pole
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
symbol	symbol	k1gInSc1	symbol
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
symboly	symbol	k1gInPc1	symbol
byly	být	k5eAaImAgInP	být
s	s	k7c7	s
kresbou	kresba	k1gFnSc7	kresba
lidských	lidský	k2eAgFnPc2d1	lidská
tváří	tvář	k1gFnPc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
zeleně	zeleně	k6eAd1	zeleně
lemována	lemován	k2eAgFnSc1d1	lemována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zdroji	zdroj	k1gInSc6	zdroj
jsou	být	k5eAaImIp3nP	být
nesprávně	správně	k6eNd1	správně
všechny	všechen	k3xTgFnPc1	všechen
vlajky	vlajka	k1gFnPc1	vlajka
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
umístěnými	umístěný	k2eAgInPc7d1	umístěný
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
vysokými	vysoký	k2eAgInPc7d1	vysoký
trojúhelníky	trojúhelník	k1gInPc7	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
byl	být	k5eAaImAgInS	být
symbol	symbol	k1gInSc1	symbol
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
byly	být	k5eAaImAgInP	být
modře	modro	k6eAd1	modro
lemované	lemovaný	k2eAgInPc1d1	lemovaný
<g/>
,	,	kIx,	,
u	u	k7c2	u
žerdi	žerď	k1gFnSc2	žerď
ale	ale	k8xC	ale
lem	lem	k1gInSc1	lem
chyběl	chybět	k5eAaImAgInS	chybět
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
výška	výška	k1gFnSc1	výška
obou	dva	k4xCgInPc2	dva
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
spodní	spodní	k2eAgInSc1d1	spodní
byl	být	k5eAaImAgInS	být
vyšší	vysoký	k2eAgInSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Mírně	mírně	k6eAd1	mírně
upraveny	upraven	k2eAgFnPc1d1	upravena
byly	být	k5eAaImAgFnP	být
i	i	k9	i
symboly	symbol	k1gInPc4	symbol
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
lidskými	lidský	k2eAgFnPc7d1	lidská
tvářemi	tvář	k1gFnPc7	tvář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1962	[number]	k4	1962
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
tzv.	tzv.	kA	tzv.
paňčájatový	paňčájatový	k2eAgInSc1d1	paňčájatový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
demokracie	demokracie	k1gFnSc1	demokracie
bez	bez	k7c2	bez
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
nepálského	nepálský	k2eAgMnSc4d1	nepálský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
nepokoje	nepokoj	k1gInSc2	nepokoj
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
federativní	federativní	k2eAgFnSc7d1	federativní
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
vlajky	vlajka	k1gFnSc2	vlajka
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajka	vlajka	k1gFnSc1	vlajka
království	království	k1gNnSc1	království
Mustang	mustang	k1gMnSc1	mustang
==	==	k?	==
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
nepálskou	nepálský	k2eAgFnSc7d1	nepálská
vládou	vláda	k1gFnSc7	vláda
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
existovalo	existovat	k5eAaImAgNnS	existovat
na	na	k7c6	na
nepálském	nepálský	k2eAgNnSc6d1	nepálské
území	území	k1gNnSc6	území
Království	království	k1gNnSc4	království
Mustang	mustang	k1gMnSc1	mustang
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
též	též	k9	též
Lo	Lo	k1gFnSc7	Lo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tradiční	tradiční	k2eAgNnSc1d1	tradiční
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
formálně	formálně	k6eAd1	formálně
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
však	však	k9	však
pod	pod	k7c7	pod
nepálskou	nepálský	k2eAgFnSc7d1	nepálská
suverenitou	suverenita	k1gFnSc7	suverenita
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
distriktů	distrikt	k1gInPc2	distrikt
zóny	zóna	k1gFnSc2	zóna
Dhavalagiri	Dhavalagir	k1gFnSc2	Dhavalagir
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
království	království	k1gNnSc2	království
tvořil	tvořit	k5eAaImAgInS	tvořit
karmínový	karmínový	k2eAgInSc1d1	karmínový
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
lemovaný	lemovaný	k2eAgInSc4d1	lemovaný
list	list	k1gInSc4	list
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
siluetou	silueta	k1gFnSc7	silueta
slunce	slunce	k1gNnSc2	slunce
se	s	k7c7	s
šestnácti	šestnáct	k4xCc2	šestnáct
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekord	rekord	k1gInSc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Káthmándú	Káthmándú	k1gFnSc2	Káthmándú
sešlo	sejít	k5eAaPmAgNnS	sejít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
35	[number]	k4	35
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pomocí	pomocí	k7c2	pomocí
barevných	barevný	k2eAgFnPc2d1	barevná
karet	kareta	k1gFnPc2	kareta
držených	držený	k2eAgFnPc2d1	držená
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
získali	získat	k5eAaPmAgMnP	získat
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
největší	veliký	k2eAgFnSc2d3	veliký
lidské	lidský	k2eAgFnSc2d1	lidská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Nepálu	Nepál	k1gInSc2	Nepál
</s>
</p>
<p>
<s>
Nepálská	nepálský	k2eAgFnSc1d1	nepálská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nepálská	nepálský	k2eAgFnSc1d1	nepálská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
