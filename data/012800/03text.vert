<p>
<s>
Jako	jako	k8xS	jako
tření	tření	k1gNnSc4	tření
označujeme	označovat	k5eAaImIp1nP	označovat
vznik	vznik	k1gInSc4	vznik
tečné	tečný	k2eAgFnSc2d1	tečná
síly	síla	k1gFnSc2	síla
ve	v	k7c6	v
styčné	styčný	k2eAgFnSc6d1	styčná
ploše	plocha	k1gFnSc6	plocha
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tečná	tečný	k2eAgFnSc1d1	tečná
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
tečnou	tečný	k2eAgFnSc4d1	tečná
složku	složka	k1gFnSc4	složka
sil	síla	k1gFnPc2	síla
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
vždy	vždy	k6eAd1	vždy
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
příp.	příp.	kA	příp.
proti	proti	k7c3	proti
změně	změna	k1gFnSc3	změna
klidového	klidový	k2eAgInSc2d1	klidový
stavu	stav	k1gInSc2	stav
u	u	k7c2	u
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
této	tento	k3xDgFnSc2	tento
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
převážně	převážně	k6eAd1	převážně
v	v	k7c4	v
teplo	teplo	k1gNnSc4	teplo
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
v	v	k7c4	v
přírůstek	přírůstek	k1gInSc4	přírůstek
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
zpravidla	zpravidla	k6eAd1	zpravidla
zvýšením	zvýšení	k1gNnSc7	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
silou	síla	k1gFnSc7	síla
disipativní	disipativní	k2eAgFnSc7d1	disipativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Tření	tření	k1gNnPc1	tření
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
skupenství	skupenství	k1gNnPc2	skupenství
stýkajících	stýkající	k2eAgNnPc2d1	stýkající
se	se	k3xPyFc4	se
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
sil	síla	k1gFnPc2	síla
ve	v	k7c6	v
styčné	styčný	k2eAgFnSc6d1	styčná
ploše	plocha	k1gFnSc6	plocha
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
pevnými	pevný	k2eAgInPc7d1	pevný
tělesy	těleso	k1gNnPc7	těleso
nazýváme	nazývat	k5eAaImIp1nP	nazývat
smykovým	smykový	k2eAgNnSc7d1	smykové
třením	tření	k1gNnSc7	tření
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
pouze	pouze	k6eAd1	pouze
třením	tření	k1gNnSc7	tření
<g/>
,	,	kIx,	,
nehrozí	hrozit	k5eNaImIp3nS	hrozit
<g/>
-li	i	k?	-li
záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tření	tření	k1gNnSc1	tření
povrchu	povrch	k1gInSc2	povrch
pevných	pevný	k2eAgNnPc2d1	pevné
těles	těleso	k1gNnPc2	těleso
s	s	k7c7	s
kapalinami	kapalina	k1gFnPc7	kapalina
nebo	nebo	k8xC	nebo
plyny	plyn	k1gInPc7	plyn
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
odpor	odpor	k1gInSc4	odpor
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Tření	tření	k1gNnSc1	tření
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
či	či	k8xC	či
vrstvami	vrstva	k1gFnPc7	vrstva
tekutin	tekutina	k1gFnPc2	tekutina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
třením	tření	k1gNnSc7	tření
(	(	kIx(	(
<g/>
či	či	k8xC	či
přeneseně	přeneseně	k6eAd1	přeneseně
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
projevu	projev	k1gInSc2	projev
vazkostí	vazkost	k1gFnPc2	vazkost
či	či	k8xC	či
viskozitou	viskozita	k1gFnSc7	viskozita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
správně	správně	k6eAd1	správně
pojmy	pojem	k1gInPc1	pojem
vyhrazené	vyhrazený	k2eAgInPc1d1	vyhrazený
pro	pro	k7c4	pro
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
veličinu	veličina	k1gFnSc4	veličina
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
odporové	odporový	k2eAgFnPc4d1	odporová
síly	síla	k1gFnPc4	síla
při	při	k7c6	při
styku	styk	k1gInSc6	styk
dvou	dva	k4xCgNnPc2	dva
pevných	pevný	k2eAgNnPc2d1	pevné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
tření	tření	k1gNnSc4	tření
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
zcela	zcela	k6eAd1	zcela
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
věnovány	věnovat	k5eAaPmNgInP	věnovat
samostatné	samostatný	k2eAgInPc1d1	samostatný
články	článek	k1gInPc1	článek
<g/>
.	.	kIx.	.
</s>
<s>
Samostatný	samostatný	k2eAgInSc1d1	samostatný
článek	článek	k1gInSc1	článek
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
také	také	k6eAd1	také
valivému	valivý	k2eAgInSc3d1	valivý
odporu	odpor	k1gInSc3	odpor
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nevhodně	vhodně	k6eNd1	vhodně
nazývanému	nazývaný	k2eAgMnSc3d1	nazývaný
valivé	valivý	k2eAgNnSc1d1	valivé
tření	tření	k1gNnSc3	tření
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jev	jev	k1gInSc4	jev
odlišné	odlišný	k2eAgFnSc2d1	odlišná
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
podstaty	podstata	k1gFnSc2	podstata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
(	(	kIx(	(
<g/>
též	též	k9	též
suchého	suchý	k2eAgNnSc2d1	suché
tření	tření	k1gNnSc2	tření
či	či	k8xC	či
Coulombova	Coulombův	k2eAgNnSc2d1	Coulombovo
tření	tření	k1gNnSc2	tření
<g/>
)	)	kIx)	)
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
tření	tření	k1gNnSc1	tření
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
–	–	k?	–
statické	statický	k2eAgNnSc4d1	statické
tření	tření	k1gNnSc4	tření
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
index	index	k1gInSc1	index
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tření	tření	k1gNnSc1	tření
za	za	k7c2	za
pohybu	pohyb	k1gInSc2	pohyb
–	–	k?	–
kinematické	kinematický	k2eAgNnSc1d1	kinematické
tření	tření	k1gNnSc1	tření
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
tření	tření	k1gNnSc1	tření
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
20	[number]	k4	20
÷	÷	k?	÷
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
tření	tření	k1gNnSc2	tření
==	==	k?	==
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
třecí	třecí	k2eAgFnSc2d1	třecí
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
rovinnými	rovinný	k2eAgInPc7d1	rovinný
povrchy	povrch	k1gInPc7	povrch
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
empirickými	empirický	k2eAgNnPc7d1	empirické
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
zvanými	zvaný	k2eAgNnPc7d1	zvané
Amontonsovy	Amontonsův	k2eAgInPc1d1	Amontonsův
zákony	zákon	k1gInPc1	zákon
tření	tření	k1gNnSc2	tření
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
třecí	třecí	k2eAgFnSc2d1	třecí
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
kinematického	kinematický	k2eAgNnSc2d1	kinematické
tření	tření	k1gNnSc2	tření
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
kolmé	kolmý	k2eAgFnSc2d1	kolmá
tlakové	tlakový	k2eAgFnSc2d1	tlaková
síle	síla	k1gFnSc3	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Amontonsův	Amontonsův	k2eAgInSc1d1	Amontonsův
zákon	zákon	k1gInSc1	zákon
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
zapsat	zapsat	k5eAaPmF	zapsat
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
definičním	definiční	k2eAgInSc7d1	definiční
vztahem	vztah	k1gInSc7	vztah
pro	pro	k7c4	pro
činitel	činitel	k1gInSc4	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
označovaný	označovaný	k2eAgInSc1d1	označovaný
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Závislost	závislost	k1gFnSc1	závislost
přímé	přímý	k2eAgFnSc2d1	přímá
úměrnosti	úměrnost	k1gFnSc2	úměrnost
platí	platit	k5eAaImIp3nS	platit
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc7d1	dobrá
přesností	přesnost	k1gFnSc7	přesnost
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
rozsah	rozsah	k1gInSc4	rozsah
kolmé	kolmý	k2eAgFnSc2d1	kolmá
tlakové	tlakový	k2eAgFnSc2d1	tlaková
síly	síla	k1gFnSc2	síla
i	i	k8xC	i
velký	velký	k2eAgInSc4d1	velký
rozsah	rozsah	k1gInSc4	rozsah
rychlostí	rychlost	k1gFnSc7	rychlost
posuvného	posuvný	k2eAgInSc2d1	posuvný
pohybu	pohyb	k1gInSc2	pohyb
po	po	k7c6	po
rovině	rovina	k1gFnSc6	rovina
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
kombinace	kombinace	k1gFnPc4	kombinace
stýkajících	stýkající	k2eAgInPc2d1	stýkající
se	se	k3xPyFc4	se
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
mezní	mezní	k2eAgFnSc1d1	mezní
síla	síla	k1gFnSc1	síla
statického	statický	k2eAgNnSc2d1	statické
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
bránit	bránit	k5eAaImF	bránit
uvedení	uvedení	k1gNnSc4	uvedení
tělesa	těleso	k1gNnSc2	těleso
z	z	k7c2	z
klidu	klid	k1gInSc2	klid
do	do	k7c2	do
posuvného	posuvný	k2eAgInSc2d1	posuvný
pohybu	pohyb	k1gInSc2	pohyb
po	po	k7c6	po
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
kolmé	kolmý	k2eAgFnSc3d1	kolmá
tlakové	tlakový	k2eAgFnSc3d1	tlaková
síle	síla	k1gFnSc3	síla
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
f0	f0	k4	f0
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
činitel	činitel	k1gMnSc1	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
tímto	tento	k3xDgInSc7	tento
vztahem	vztah	k1gInSc7	vztah
definovaný	definovaný	k2eAgMnSc1d1	definovaný
<g/>
.	.	kIx.	.
<g/>
Velikost	velikost	k1gFnSc1	velikost
tření	tření	k1gNnSc2	tření
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
velkosti	velkost	k1gFnSc6	velkost
stykové	stykový	k2eAgFnSc2d1	styková
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
stykový	stykový	k2eAgInSc1d1	stykový
tlak	tlak	k1gInSc1	tlak
nepřestoupí	přestoupit	k5eNaPmIp3nS	přestoupit
pevnostní	pevnostní	k2eAgFnPc4d1	pevnostní
meze	mez	k1gFnPc4	mez
materiálů	materiál	k1gInPc2	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgNnSc7d1	další
empirickým	empirický	k2eAgNnSc7d1	empirické
pravidlem	pravidlo	k1gNnSc7	pravidlo
pro	pro	k7c4	pro
tření	tření	k1gNnSc4	tření
je	být	k5eAaImIp3nS	být
tz	tz	k?	tz
<g/>
.	.	kIx.	.
</s>
<s>
Coulombův	Coulombův	k2eAgInSc1d1	Coulombův
zákon	zákon	k1gInSc1	zákon
tření	tření	k1gNnSc2	tření
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Tření	tření	k1gNnSc1	tření
za	za	k7c2	za
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
velikost	velikost	k1gFnSc1	velikost
třecí	třecí	k2eAgFnSc2d1	třecí
síly	síla	k1gFnSc2	síla
u	u	k7c2	u
kinematického	kinematický	k2eAgNnSc2d1	kinematické
tření	tření	k1gNnSc2	tření
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
.	.	kIx.	.
<g/>
Smykové	smykový	k2eAgNnSc1d1	smykové
tření	tření	k1gNnSc1	tření
splňující	splňující	k2eAgNnSc1d1	splňující
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
se	se	k3xPyFc4	se
v	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
praxi	praxe	k1gFnSc6	praxe
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
"	"	kIx"	"
<g/>
suché	suchý	k2eAgNnSc1d1	suché
<g/>
"	"	kIx"	"
tření	tření	k1gNnSc1	tření
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zákon	zákon	k1gInSc1	zákon
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
dvou	dva	k4xCgFnPc2	dva
těles	těleso	k1gNnPc2	těleso
promazaných	promazaný	k2eAgInPc2d1	promazaný
tekutým	tekutý	k2eAgNnSc7d1	tekuté
mazivem	mazivo	k1gNnSc7	mazivo
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
povrchy	povrch	k1gInPc4	povrch
nedokonalé	dokonalý	k2eNgFnSc2d1	nedokonalá
tuhosti	tuhost	k1gFnSc2	tuhost
<g/>
,	,	kIx,	,
měnící	měnící	k2eAgFnPc1d1	měnící
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
svou	svůj	k3xOyFgFnSc4	svůj
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
mikrostrukturu	mikrostruktura	k1gFnSc4	mikrostruktura
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
své	svůj	k3xOyFgFnPc4	svůj
třecí	třecí	k2eAgFnPc4d1	třecí
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Těleso	těleso	k1gNnSc1	těleso
na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
rovině	rovina	k1gFnSc6	rovina
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
nepůsobí	působit	k5eNaImIp3nS	působit
žádná	žádný	k3yNgFnSc1	žádný
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
pouze	pouze	k6eAd1	pouze
síly	síla	k1gFnSc2	síla
svislé	svislý	k2eAgFnSc2d1	svislá
<g/>
,	,	kIx,	,
kolmé	kolmý	k2eAgFnSc2d1	kolmá
na	na	k7c4	na
stykovou	stykový	k2eAgFnSc4d1	styková
plochu	plocha	k1gFnSc4	plocha
(	(	kIx(	(
<g/>
normálové	normálový	k2eAgFnPc1d1	normálová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tíha	tíha	k1gFnSc1	tíha
tělesa	těleso	k1gNnSc2	těleso
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
nepůsobí	působit	k5eNaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
začne	začít	k5eAaPmIp3nS	začít
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působit	působit	k5eAaImF	působit
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
také	také	k9	také
působit	působit	k5eAaImF	působit
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
hodnoty	hodnota	k1gFnPc4	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
činitel	činitel	k1gInSc1	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc4	těleso
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rovnoměrnému	rovnoměrný	k2eAgInSc3d1	rovnoměrný
pohybu	pohyb	k1gInSc3	pohyb
pak	pak	k6eAd1	pak
postačí	postačit	k5eAaPmIp3nS	postačit
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
</s>
</p>
<p>
<s>
Třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
vodorovnou	vodorovný	k2eAgFnSc4d1	vodorovná
složku	složka	k1gFnSc4	složka
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Výslednicí	výslednice	k1gFnSc7	výslednice
složek	složka	k1gFnPc2	složka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	on	k3xPp3gFnPc4	on
reakce	reakce	k1gFnPc4	reakce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
normály	normála	k1gFnSc2	normála
odkloněná	odkloněný	k2eAgFnSc1d1	odkloněná
o	o	k7c4	o
úhel	úhel	k1gInSc4	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
arctg	arctg	k1gMnSc1	arctg
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
phi	phi	k?	phi
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
arctg	arctg	k1gInSc4	arctg
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
třecí	třecí	k2eAgInSc1d1	třecí
úhel	úhel	k1gInSc1	úhel
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Rovnovážný	rovnovážný	k2eAgInSc1d1	rovnovážný
vektorový	vektorový	k2eAgInSc1d1	vektorový
obrazec	obrazec	k1gInSc1	obrazec
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nS	tvořit
síly	síla	k1gFnPc4	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Znázornění	znázornění	k1gNnSc1	znázornění
tření	tření	k1gNnSc2	tření
pomocí	pomocí	k7c2	pomocí
třecího	třecí	k2eAgInSc2d1	třecí
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
odkloněním	odklonění	k1gNnSc7	odklonění
normálové	normálový	k2eAgFnSc2d1	normálová
reakce	reakce	k1gFnSc2	reakce
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
úhel	úhel	k1gInSc4	úhel
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
pohybu	pohyb	k1gInSc2	pohyb
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
síle	síla	k1gFnSc3	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
těleso	těleso	k1gNnSc4	těleso
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
grafického	grafický	k2eAgNnSc2d1	grafické
řešení	řešení	k1gNnSc2	řešení
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tření	tření	k1gNnSc1	tření
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
<g/>
.	.	kIx.	.
<g/>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
působiště	působiště	k1gNnSc2	působiště
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
posune	posunout	k5eAaPmIp3nS	posunout
od	od	k7c2	od
stykové	stykový	k2eAgFnSc2d1	styková
roviny	rovina	k1gFnSc2	rovina
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
posune	posunout	k5eAaPmIp3nS	posunout
se	se	k3xPyFc4	se
působiště	působiště	k1gNnSc2	působiště
reakce	reakce	k1gFnSc2	reakce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
směrem	směr	k1gInSc7	směr
dopředu	dopředu	k6eAd1	dopředu
(	(	kIx(	(
<g/>
nositelky	nositelka	k1gFnPc1	nositelka
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
stykové	stykový	k2eAgFnSc2d1	styková
plochy	plocha	k1gFnSc2	plocha
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
hodnoty	hodnota	k1gFnPc4	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h_	h_	k?	h_
<g/>
{	{	kIx(	{
<g/>
MAX	Max	k1gMnSc1	Max
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
nezačne	začít	k5eNaPmIp3nS	začít
posouvat	posouvat	k5eAaImF	posouvat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
překlápět	překlápět	k5eAaImF	překlápět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
působiště	působiště	k1gNnSc1	působiště
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
bod	bod	k1gInSc1	bod
R	R	kA	R
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
mimo	mimo	k7c4	mimo
stykovou	stykový	k2eAgFnSc4d1	styková
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rovnovážný	rovnovážný	k2eAgInSc4d1	rovnovážný
vektorový	vektorový	k2eAgInSc4d1	vektorový
obrazec	obrazec	k1gInSc4	obrazec
nemá	mít	k5eNaImIp3nS	mít
poloha	poloha	k1gFnSc1	poloha
působiště	působiště	k1gNnSc2	působiště
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vliv	vliv	k1gInSc1	vliv
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc1	velikost
sil	síla	k1gFnPc2	síla
zůstane	zůstat	k5eAaPmIp3nS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
směru	směr	k1gInSc6	směr
tj.	tj.	kA	tj.
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
normálová	normálový	k2eAgFnSc1d1	normálová
složka	složka	k1gFnSc1	složka
přičte	přičíst	k5eAaPmIp3nS	přičíst
ke	k	k7c3	k
složce	složka	k1gFnSc3	složka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Činitel	činitel	k1gInSc4	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
==	==	k?	==
</s>
</p>
<p>
<s>
Činitel	činitel	k1gInSc1	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
poměr	poměr	k1gInSc4	poměr
třecí	třecí	k2eAgFnSc2d1	třecí
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
kolmé	kolmý	k2eAgFnPc1d1	kolmá
tlakové	tlakový	k2eAgFnPc1d1	tlaková
síly	síla	k1gFnPc1	síla
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
při	při	k7c6	při
smykovém	smykový	k2eAgNnSc6d1	smykové
tření	tření	k1gNnSc6	tření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
činitele	činitel	k1gInSc2	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
dvojici	dvojice	k1gFnSc4	dvojice
látek	látka	k1gFnPc2	látka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
drsnosti	drsnost	k1gFnSc3	drsnost
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
smykové	smykový	k2eAgNnSc1d1	smykové
tření	tření	k1gNnSc1	tření
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
platí	platit	k5eAaImIp3nS	platit
Amontonsovy	Amontonsův	k2eAgInPc4d1	Amontonsův
zákony	zákon	k1gInPc4	zákon
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hodnoty	hodnota	k1gFnPc1	hodnota
činitele	činitel	k1gInSc2	činitel
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
nuly	nula	k1gFnSc2	nula
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgNnSc4	žádný
tření	tření	k1gNnSc4	tření
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
několikanásobně	několikanásobně	k6eAd1	několikanásobně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
1	[number]	k4	1
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
kolmý	kolmý	k2eAgInSc1d1	kolmý
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
např.	např.	kA	např.
silikonová	silikonový	k2eAgFnSc1d1	silikonová
pryž	pryž	k1gFnSc1	pryž
<g/>
,	,	kIx,	,
využívaná	využívaný	k2eAgFnSc1d1	využívaná
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
a	a	k8xC	a
experimentálně	experimentálně	k6eAd1	experimentálně
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
malém	malý	k2eAgInSc6d1	malý
rozsahu	rozsah	k1gInSc6	rozsah
kolmého	kolmý	k2eAgInSc2d1	kolmý
tlaku	tlak	k1gInSc2	tlak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
činitel	činitel	k1gInSc4	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
dokonce	dokonce	k9	dokonce
záporný	záporný	k2eAgInSc1d1	záporný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Značení	značení	k1gNnSc2	značení
===	===	k?	===
</s>
</p>
<p>
<s>
Značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
1	[number]	k4	1
(	(	kIx(	(
<g/>
bezrozměrová	bezrozměrový	k2eAgFnSc1d1	bezrozměrová
veličina	veličina	k1gFnSc1	veličina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zjišťování	zjišťování	k1gNnSc1	zjišťování
<g/>
:	:	kIx,	:
experimentálně	experimentálně	k6eAd1	experimentálně
a	a	k8xC	a
výpočtem	výpočet	k1gInSc7	výpočet
f	f	k?	f
=	=	kIx~	=
Ft	Ft	k1gMnSc1	Ft
/	/	kIx~	/
Fn	Fn	k1gMnSc1	Fn
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ft	Ft	k1gFnSc1	Ft
je	být	k5eAaImIp3nS	být
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
Fn	Fn	k1gFnSc1	Fn
je	být	k5eAaImIp3nS	být
kolmá	kolmý	k2eAgFnSc1d1	kolmá
tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Příklady	příklad	k1gInPc1	příklad
hodnot	hodnota	k1gFnPc2	hodnota
činitele	činitel	k1gInSc2	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Klidové	klidový	k2eAgNnSc1d1	klidové
tření	tření	k1gNnSc1	tření
==	==	k?	==
</s>
</p>
<p>
<s>
Klidové	klidový	k2eAgNnSc1d1	klidové
tření	tření	k1gNnSc1	tření
(	(	kIx(	(
<g/>
statické	statický	k2eAgNnSc1d1	statické
tření	tření	k1gNnSc1	tření
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tření	tření	k1gNnSc1	tření
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgNnSc1d1	vznikající
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
sobě	se	k3xPyFc3	se
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgInSc4d1	speciální
případ	případ	k1gInSc4	případ
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klidová	klidový	k2eAgFnSc1d1	klidová
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
Ft	Ft	k1gMnPc2	Ft
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
stejnou	stejný	k2eAgFnSc4d1	stejná
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tečná	tečný	k2eAgFnSc1d1	tečná
složka	složka	k1gFnSc1	složka
výslednice	výslednice	k1gFnSc2	výslednice
sil	síla	k1gFnPc2	síla
snažících	snažící	k2eAgFnPc2d1	snažící
se	se	k3xPyFc4	se
uvést	uvést	k5eAaPmF	uvést
stýkající	stýkající	k2eAgFnSc1d1	stýkající
se	se	k3xPyFc4	se
tělesa	těleso	k1gNnPc1	těleso
do	do	k7c2	do
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
opačné	opačný	k2eAgFnSc3d1	opačná
orientaci	orientace	k1gFnSc3	orientace
je	on	k3xPp3gFnPc4	on
udržuje	udržovat	k5eAaImIp3nS	udržovat
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepřesáhne	přesáhnout	k5eNaPmIp3nS	přesáhnout
maximální	maximální	k2eAgFnSc4d1	maximální
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnPc4	který
ke	k	k7c3	k
vzájemnému	vzájemný	k2eAgInSc3d1	vzájemný
pohybu	pohyb	k1gInSc3	pohyb
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
maximální	maximální	k2eAgFnSc4d1	maximální
klidovou	klidový	k2eAgFnSc4d1	klidová
třecí	třecí	k2eAgFnSc4d1	třecí
sílu	síla	k1gFnSc4	síla
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc1	vztah
obdobný	obdobný	k2eAgInSc1d1	obdobný
jako	jako	k9	jako
pro	pro	k7c4	pro
sílu	síla	k1gFnSc4	síla
kinematického	kinematický	k2eAgNnSc2d1	kinematické
tření	tření	k1gNnSc2	tření
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
f0	f0	k4	f0
je	být	k5eAaImIp3nS	být
činitel	činitel	k1gInSc1	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
<g/>
,	,	kIx,	,
Fn	Fn	k1gFnSc1	Fn
je	být	k5eAaImIp3nS	být
kolmá	kolmý	k2eAgFnSc1d1	kolmá
tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tíha	tíha	k1gFnSc1	tíha
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klidové	klidový	k2eAgNnSc1d1	klidové
tření	tření	k1gNnSc1	tření
bývá	bývat	k5eAaImIp3nS	bývat
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
smykové	smykový	k2eAgNnSc4d1	smykové
tření	tření	k1gNnSc4	tření
mezi	mezi	k7c7	mezi
stejnými	stejný	k2eAgNnPc7d1	stejné
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Činitel	činitel	k1gInSc4	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
===	===	k?	===
</s>
</p>
<p>
<s>
Činitel	činitel	k1gInSc1	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
poměr	poměr	k1gInSc4	poměr
třecí	třecí	k2eAgFnSc2d1	třecí
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
kolmé	kolmý	k2eAgFnPc1d1	kolmá
tlakové	tlakový	k2eAgFnPc1d1	tlaková
síly	síla	k1gFnPc1	síla
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
při	při	k7c6	při
klidovém	klidový	k2eAgNnSc6d1	klidové
tření	tření	k1gNnSc6	tření
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hodnoty	hodnota	k1gFnPc1	hodnota
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
dvojici	dvojice	k1gFnSc4	dvojice
látek	látka	k1gFnPc2	látka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgInPc7	který
má	mít	k5eAaImIp3nS	mít
tření	tření	k1gNnPc4	tření
vznikat	vznikat	k5eAaImF	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Činitel	činitel	k1gInSc1	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
menší	malý	k2eAgNnPc1d2	menší
než	než	k8xS	než
činitel	činitel	k1gMnSc1	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
pro	pro	k7c4	pro
stejná	stejný	k2eAgNnPc4d1	stejné
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
materiálovou	materiálový	k2eAgFnSc4d1	materiálová
kombinaci	kombinace	k1gFnSc4	kombinace
teflon	teflon	k1gInSc1	teflon
–	–	k?	–
teflon	teflon	k1gInSc1	teflon
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
materiálových	materiálový	k2eAgFnPc2d1	materiálová
kombinací	kombinace	k1gFnPc2	kombinace
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
několik	několik	k4yIc4	několik
procent	procento	k1gNnPc2	procento
až	až	k8xS	až
desítek	desítka	k1gFnPc2	desítka
procent	procento	k1gNnPc2	procento
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
dané	daný	k2eAgFnPc4d1	daná
kombinace	kombinace	k1gFnPc4	kombinace
materiálů	materiál	k1gInPc2	materiál
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
je	být	k5eAaImIp3nS	být
činitel	činitel	k1gInSc1	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
vyšší	vysoký	k2eAgNnSc4d2	vyšší
právě	právě	k9	právě
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
i	i	k8xC	i
činitel	činitel	k1gMnSc1	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
<g/>
;	;	kIx,	;
obecně	obecně	k6eAd1	obecně
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
materiálové	materiálový	k2eAgFnPc4d1	materiálová
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
)	)	kIx)	)
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Značení	značení	k1gNnSc2	značení
====	====	k?	====
</s>
</p>
<p>
<s>
Značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
1	[number]	k4	1
(	(	kIx(	(
<g/>
bezrozměrová	bezrozměrový	k2eAgFnSc1d1	bezrozměrová
veličina	veličina	k1gFnSc1	veličina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zjišťování	zjišťování	k1gNnSc1	zjišťování
<g/>
:	:	kIx,	:
experimentálně	experimentálně	k6eAd1	experimentálně
a	a	k8xC	a
výpočtem	výpočet	k1gInSc7	výpočet
f0	f0	k4	f0
=	=	kIx~	=
Ft0	Ft0	k1gFnSc1	Ft0
/	/	kIx~	/
Fn	Fn	k1gFnSc1	Fn
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ft0	Ft0	k1gFnSc1	Ft0
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
třecí	třecí	k2eAgFnSc1d1	třecí
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
Fn	Fn	k1gFnSc1	Fn
je	být	k5eAaImIp3nS	být
kolmá	kolmý	k2eAgFnSc1d1	kolmá
tlaková	tlakový	k2eAgFnSc1d1	tlaková
síla	síla	k1gFnSc1	síla
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Příklad	příklad	k1gInSc1	příklad
hodnot	hodnota	k1gFnPc2	hodnota
činitele	činitel	k1gInSc2	činitel
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Stick-Slip	Stick-Slip	k1gInSc1	Stick-Slip
efekt	efekt	k1gInSc1	efekt
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
posouvající	posouvající	k2eAgFnSc1d1	posouvající
síla	síla	k1gFnSc1	síla
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nS	působit
přes	přes	k7c4	přes
pružný	pružný	k2eAgInSc4d1	pružný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
síla	síla	k1gFnSc1	síla
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
plynule	plynule	k6eAd1	plynule
narůstá	narůstat	k5eAaImIp3nS	narůstat
od	od	k7c2	od
nuly	nula	k1gFnSc2	nula
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
klidového	klidový	k2eAgNnSc2d1	klidové
tření	tření	k1gNnSc2	tření
posune	posunout	k5eAaPmIp3nS	posunout
těleso	těleso	k1gNnSc4	těleso
tak	tak	k9	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
umožní	umožnit	k5eAaPmIp3nS	umožnit
nahromaděná	nahromaděný	k2eAgFnSc1d1	nahromaděná
energie	energie	k1gFnSc1	energie
pružného	pružný	k2eAgInSc2d1	pružný
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
zastaví	zastavit	k5eAaPmIp3nS	zastavit
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
opět	opět	k6eAd1	opět
působit	působit	k5eAaImF	působit
klidové	klidový	k2eAgNnSc4d1	klidové
tření	tření	k1gNnSc4	tření
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
neustále	neustále	k6eAd1	neustále
cyklicky	cyklicky	k6eAd1	cyklicky
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
trhavý	trhavý	k2eAgInSc1d1	trhavý
pohyb	pohyb	k1gInSc1	pohyb
posouvaného	posouvaný	k2eAgNnSc2d1	posouvané
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Stick-Slip	Stick-Slip	k1gInSc1	Stick-Slip
efekt	efekt	k1gInSc1	efekt
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgInSc1d1	anglický
výraz	výraz	k1gInSc1	výraz
Stick-Slip	Stick-Slip	k1gInSc1	Stick-Slip
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Drží-Klouže	Drží-Klouž	k1gFnPc1	Drží-Klouž
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
působí	působit	k5eAaImIp3nS	působit
například	například	k6eAd1	například
trhavý	trhavý	k2eAgInSc1d1	trhavý
pohyb	pohyb	k1gInSc1	pohyb
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
a	a	k8xC	a
pneumatických	pneumatický	k2eAgInPc2d1	pneumatický
válců	válec	k1gInPc2	válec
zejména	zejména	k9	zejména
za	za	k7c2	za
nižšího	nízký	k2eAgInSc2d2	nižší
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vrzání	vrzání	k1gNnSc1	vrzání
dveřních	dveřní	k2eAgInPc2d1	dveřní
pantů	pant	k1gInPc2	pant
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
však	však	k9	však
založeno	založit	k5eAaPmNgNnS	založit
i	i	k8xC	i
například	například	k6eAd1	například
hraní	hranit	k5eAaImIp3nS	hranit
na	na	k7c4	na
smyčcové	smyčcový	k2eAgInPc4d1	smyčcový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
i	i	k8xC	i
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
skleněné	skleněný	k2eAgFnPc4d1	skleněná
skleničky	sklenička	k1gFnPc4	sklenička
třením	tření	k1gNnSc7	tření
prstu	prst	k1gInSc2	prst
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
okraj	okraj	k1gInSc4	okraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
tření	tření	k1gNnSc1	tření
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
částmi	část	k1gFnPc7	část
tělesa	těleso	k1gNnSc2	těleso
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
(	(	kIx(	(
<g/>
a	a	k8xC	a
u	u	k7c2	u
reálných	reálný	k2eAgNnPc2d1	reálné
těles	těleso	k1gNnPc2	těleso
také	také	k9	také
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
)	)	kIx)	)
ke	k	k7c3	k
tření	tření	k1gNnSc3	tření
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tření	tření	k1gNnSc1	tření
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
uvnitř	uvnitř	k7c2	uvnitř
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
tření	tření	k1gNnSc1	tření
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
např.	např.	kA	např.
při	při	k7c6	při
proudění	proudění	k1gNnSc6	proudění
reálných	reálný	k2eAgFnPc2d1	reálná
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
charakterizující	charakterizující	k2eAgFnSc1d1	charakterizující
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
tření	tření	k1gNnSc4	tření
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
viskozita	viskozita	k1gFnSc1	viskozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
tření	tření	k1gNnSc1	tření
</s>
</p>
<p>
<s>
Valivý	valivý	k2eAgInSc4d1	valivý
odpor	odpor	k1gInSc4	odpor
</s>
</p>
<p>
<s>
Vláknové	vláknový	k2eAgNnSc1d1	vláknové
tření	tření	k1gNnSc1	tření
</s>
</p>
<p>
<s>
Čepové	Čepové	k2eAgNnSc1d1	Čepové
tření	tření	k1gNnSc1	tření
</s>
</p>
<p>
<s>
Odpor	odpor	k1gInSc1	odpor
vzduchu	vzduch	k1gInSc2	vzduch
</s>
</p>
<p>
<s>
Adheze	adheze	k1gFnSc1	adheze
</s>
</p>
<p>
<s>
Skluz	skluz	k1gInSc1	skluz
(	(	kIx(	(
<g/>
kolejová	kolejový	k2eAgFnSc1d1	kolejová
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tření	tření	k1gNnSc2	tření
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tření	tření	k1gNnSc2	tření
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
