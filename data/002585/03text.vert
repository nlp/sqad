<s>
Tristan	Tristan	k1gInSc1	Tristan
Tzara	Tzar	k1gMnSc2	Tzar
[	[	kIx(	[
<g/>
Cara	car	k1gMnSc2	car
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Samuel	Samuel	k1gMnSc1	Samuel
Rosenstock	Rosenstock	k1gMnSc1	Rosenstock
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Moineș	Moineș	k1gFnSc6	Moineș
<g/>
,	,	kIx,	,
Bacău	Bacăum	k1gNnSc6	Bacăum
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
rumunsko-židovského	rumunsko-židovský	k2eAgInSc2d1	rumunsko-židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
dadaismu	dadaismus	k1gInSc2	dadaismus
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
již	již	k6eAd1	již
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
umělecký	umělecký	k2eAgInSc4d1	umělecký
pseudonym	pseudonym	k1gInSc4	pseudonym
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
založily	založit	k5eAaPmAgFnP	založit
v	v	k7c6	v
kabaretu	kabaret	k1gInSc6	kabaret
Voltaire	Voltair	k1gInSc5	Voltair
hnutí	hnutí	k1gNnPc1	hnutí
dada	dada	k1gNnSc1	dada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
kubistické	kubistický	k2eAgMnPc4d1	kubistický
a	a	k8xC	a
futuristické	futuristický	k2eAgMnPc4d1	futuristický
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Louisem	Louis	k1gMnSc7	Louis
Aragonem	Aragon	k1gMnSc7	Aragon
<g/>
,	,	kIx,	,
André	André	k1gMnSc7	André
Bretonem	Breton	k1gMnSc7	Breton
a	a	k8xC	a
Phillipe	Phillip	k1gInSc5	Phillip
Soupaultem	Soupault	k1gInSc7	Soupault
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
skončila	skončit	k5eAaPmAgFnS	skončit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Tzara	Tzara	k1gMnSc1	Tzara
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vznikající	vznikající	k2eAgInSc4d1	vznikající
surrealismus	surrealismus	k1gInSc4	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
však	však	k9	však
začíná	začínat	k5eAaImIp3nS	začínat
publikovat	publikovat	k5eAaBmF	publikovat
i	i	k9	i
v	v	k7c6	v
surrealistických	surrealistický	k2eAgInPc6d1	surrealistický
časopisech	časopis	k1gInPc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
dům	dům	k1gInSc4	dům
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Adolfa	Adolf	k1gMnSc2	Adolf
Loose	Loos	k1gMnSc2	Loos
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
realizací	realizace	k1gFnPc2	realizace
tohoto	tento	k3xDgMnSc2	tento
architekta	architekt	k1gMnSc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
levicovými	levicový	k2eAgMnPc7d1	levicový
politiky	politik	k1gMnPc7	politik
<g/>
,	,	kIx,	,
angažoval	angažovat	k5eAaBmAgMnS	angažovat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
a	a	k8xC	a
nacismu	nacismus	k1gInSc3	nacismus
a	a	k8xC	a
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
republikánů	republikán	k1gMnPc2	republikán
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k9	i
literární	literární	k2eAgMnSc1d1	literární
a	a	k8xC	a
výtvarné	výtvarný	k2eAgFnSc3d1	výtvarná
kritice	kritika	k1gFnSc3	kritika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1946	[number]	k4	1946
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
také	také	k9	také
svou	svůj	k3xOyFgFnSc7	svůj
aférou	aféra	k1gFnSc7	aféra
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
herečkou	herečka	k1gFnSc7	herečka
Natašou	Nataša	k1gFnSc7	Nataša
Gollovou	Gollová	k1gFnSc7	Gollová
<g/>
.	.	kIx.	.
</s>
<s>
Položil	položit	k5eAaPmAgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
dadaistické	dadaistický	k2eAgFnSc2d1	dadaistická
literatury	literatura	k1gFnSc2	literatura
dílem	díl	k1gInSc7	díl
La	la	k1gNnSc4	la
premiè	premiè	k?	premiè
aventure	aventur	k1gMnSc5	aventur
céleste	célest	k1gInSc5	célest
de	de	k?	de
Monsieur	Monsieur	k1gMnSc1	Monsieur
Antipyrine	Antipyrin	k1gInSc5	Antipyrin
(	(	kIx(	(
<g/>
První	první	k4xOgNnSc1	první
nebeské	nebeský	k2eAgNnSc1d1	nebeské
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
pana	pan	k1gMnSc2	pan
Antipyrine	Antipyrin	k1gInSc5	Antipyrin
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
Vingt-cinq	Vingtinq	k1gFnSc2	Vingt-cinq
poè	poè	k?	poè
(	(	kIx(	(
<g/>
Dvacet	dvacet	k4xCc1	dvacet
pět	pět	k4xCc1	pět
básní	báseň	k1gFnPc2	báseň
<g/>
)	)	kIx)	)
1920	[number]	k4	1920
Cinéma	Cinémum	k1gNnSc2	Cinémum
calendrier	calendrira	k1gFnPc2	calendrira
du	du	k?	du
cœ	cœ	k?	cœ
abstrait	abstrait	k2eAgInSc4d1	abstrait
maisons	maisons	k1gInSc4	maisons
(	(	kIx(	(
<g/>
Kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
biograf	biograf	k1gInSc4	biograf
abstraktního	abstraktní	k2eAgNnSc2d1	abstraktní
srdce	srdce	k1gNnSc2	srdce
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
Le	Le	k1gMnSc1	Le
Cœ	Cœ	k1gMnSc1	Cœ
à	à	k?	à
gaz	gaz	k?	gaz
(	(	kIx(	(
<g/>
Srdce	srdce	k1gNnSc1	srdce
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
<g/>
)	)	kIx)	)
1924	[number]	k4	1924
Sept	septum	k1gNnPc2	septum
manifestes	manifestes	k1gMnSc1	manifestes
Dada	dada	k1gNnSc2	dada
(	(	kIx(	(
<g/>
Sedm	sedm	k4xCc1	sedm
manifestů	manifest	k1gInPc2	manifest
Dada	dada	k1gNnPc2	dada
<g/>
)	)	kIx)	)
1923	[number]	k4	1923
De	De	k?	De
nos	nos	k1gInSc1	nos
oiseaux	oiseaux	k1gInSc1	oiseaux
(	(	kIx(	(
<g/>
O	o	k7c6	o
našich	náš	k3xOp1gMnPc6	náš
ptácích	pták	k1gMnPc6	pták
<g/>
)	)	kIx)	)
1931	[number]	k4	1931
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gInSc5	Homm
approximatif	approximatif	k1gMnSc1	approximatif
(	(	kIx(	(
<g/>
Přibližný	přibližný	k2eAgMnSc1d1	přibližný
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
1932	[number]	k4	1932
<g />
.	.	kIx.	.
</s>
<s>
Où	Où	k?	Où
boivent	boivent	k1gInSc1	boivent
les	les	k1gInSc1	les
loups	loups	k1gInSc4	loups
(	(	kIx(	(
<g/>
Kde	kde	k6eAd1	kde
pijí	pít	k5eAaImIp3nP	pít
vlci	vlk	k1gMnPc1	vlk
<g/>
)	)	kIx)	)
1945	[number]	k4	1945
Une	Une	k1gMnPc1	Une
route	route	k5eAaPmIp2nP	route
seul	seul	k1gInSc4	seul
soleil	soleil	k1gInSc1	soleil
(	(	kIx(	(
<g/>
Slunná	slunný	k2eAgFnSc1d1	slunná
silnice	silnice	k1gFnSc1	silnice
samá	samý	k3xTgFnSc1	samý
radost	radost	k1gFnSc1	radost
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
Terre	Terr	k1gInSc5	Terr
sur	sur	k?	sur
Terre	Terr	k1gInSc5	Terr
(	(	kIx(	(
<g/>
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
1950	[number]	k4	1950
Parler	Parler	k1gMnSc1	Parler
seul	seul	k1gMnSc1	seul
(	(	kIx(	(
<g/>
Mluvit	mluvit	k5eAaImF	mluvit
sám	sám	k3xTgInSc1	sám
<g/>
)	)	kIx)	)
1965	[number]	k4	1965
Les	les	k1gInSc1	les
Premiers	Premiers	k1gInSc4	Premiers
poè	poè	k?	poè
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
-	-	kIx~	-
francouzský	francouzský	k2eAgInSc1d1	francouzský
překlad	překlad	k1gInSc1	překlad
knihy	kniha	k1gFnSc2	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
a	a	k8xC	a
několik	několik	k4yIc4	několik
dosud	dosud	k6eAd1	dosud
nepublikovaných	publikovaný	k2eNgFnPc2d1	nepublikovaná
básní	báseň	k1gFnPc2	báseň
Dialektika	dialektika	k1gFnSc1	dialektika
poezie	poezie	k1gFnSc1	poezie
Le	Le	k1gFnSc1	Le
Surréalisme	Surréalismus	k1gInSc5	Surréalismus
et	et	k?	et
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
aprè	aprè	k?	aprè
(	(	kIx(	(
<g/>
Surrealismus	surrealismus	k1gInSc1	surrealismus
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
poválečná	poválečný	k2eAgFnSc1d1	poválečná
<g/>
)	)	kIx)	)
1924	[number]	k4	1924
Mouchoir	Mouchoir	k1gMnSc1	Mouchoir
de	de	k?	de
nuages	nuages	k1gMnSc1	nuages
(	(	kIx(	(
<g/>
Šáteček	šáteček	k1gInSc1	šáteček
z	z	k7c2	z
mlh	mlha	k1gFnPc2	mlha
<g/>
)	)	kIx)	)
Oeuvres	Oeuvres	k1gMnSc1	Oeuvres
completes	completes	k1gMnSc1	completes
(	(	kIx(	(
<g/>
Souborné	souborný	k2eAgFnSc2d1	souborná
<g />
.	.	kIx.	.
</s>
<s>
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
1975-1991	[number]	k4	1975-1991
Šáteček	šáteček	k1gInSc1	šáteček
z	z	k7c2	z
mlh	mlha	k1gFnPc2	mlha
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
Hlídky	hlídka	k1gFnPc1	hlídka
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Řezáč	Řezáč	k1gMnSc1	Řezáč
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
bibliofilie	bibliofilie	k1gFnSc1	bibliofilie
Slunná	slunný	k2eAgFnSc1d1	slunná
silnice	silnice	k1gFnSc1	silnice
samá	samý	k3xTgFnSc1	samý
radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Adolf	Adolf	k1gMnSc1	Adolf
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svaz	svaz	k1gInSc1	svaz
přátel	přítel	k1gMnPc2	přítel
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
TZARA	TZARA	kA	TZARA
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc1	Tristan
<g/>
.	.	kIx.	.
</s>
<s>
Daroval	darovat	k5eAaPmAgMnS	darovat
jsem	být	k5eAaImIp1nS	být
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
bílému	bílý	k2eAgInSc3d1	bílý
kameni	kámen	k1gInSc3	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Lorenc	Lorenc	k1gMnSc1	Lorenc
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Lorenc	Lorenc	k1gMnSc1	Lorenc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Concordia	Concordium	k1gNnPc4	Concordium
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
312	[number]	k4	312
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85997	[number]	k4	85997
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tzarovy	Tzarův	k2eAgFnPc1d1	Tzarův
básně	báseň	k1gFnPc1	báseň
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
i	i	k9	i
ve	v	k7c6	v
sborníku	sborník	k1gInSc6	sborník
Čest	čest	k1gFnSc1	čest
básníků	básník	k1gMnPc2	básník
:	:	kIx,	:
poesie	poesie	k1gFnSc1	poesie
francouzského	francouzský	k2eAgInSc2d1	francouzský
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
Bartušek	Bartušek	k1gMnSc1	Bartušek
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
:	:	kIx,	:
Fr.	Fr.	k1gFnSc6	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
