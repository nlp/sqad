<p>
<s>
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc3	Rarak
je	být	k5eAaImIp3nS	být
boční	boční	k2eAgInSc4d1	boční
kráter	kráter	k1gInSc4	kráter
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neaktivní	aktivní	k2eNgFnSc2d1	neaktivní
sopky	sopka	k1gFnSc2	sopka
Maunga	Maung	k1gMnSc2	Maung
Terevaka	Terevak	k1gMnSc2	Terevak
na	na	k7c6	na
Velikonočním	velikonoční	k2eAgInSc6d1	velikonoční
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
svah	svah	k1gInSc1	svah
kráteru	kráter	k1gInSc2	kráter
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc6	Rarak
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
šestého	šestý	k4xOgNnSc2	šestý
do	do	k7c2	do
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc4	století
lomem	lom	k1gInSc7	lom
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
původní	původní	k2eAgNnSc1d1	původní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
ostrova	ostrov	k1gInSc2	ostrov
tesalo	tesat	k5eAaImAgNnS	tesat
monolitické	monolitický	k2eAgFnPc4d1	monolitická
sochy	socha	k1gFnPc4	socha
Moai	Moai	k1gNnSc2	Moai
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
a	a	k8xC	a
geologie	geologie	k1gFnSc2	geologie
==	==	k?	==
</s>
</p>
<p>
<s>
Kráter	kráter	k1gInSc1	kráter
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc6	Rarak
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
ostrova	ostrov	k1gInSc2	ostrov
asi	asi	k9	asi
12	[number]	k4	12
km	km	kA	km
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Hanga	Hang	k1gMnSc2	Hang
Roa	Roa	k1gMnSc2	Roa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc3	Rarak
je	být	k5eAaImIp3nS	být
boční	boční	k2eAgInSc4d1	boční
kráter	kráter	k1gInSc4	kráter
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
sopky	sopka	k1gFnSc2	sopka
Maunga	Maung	k1gMnSc2	Maung
Terevaka	Terevak	k1gMnSc2	Terevak
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
9	[number]	k4	9
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
vrcholu	vrchol	k1gInSc2	vrchol
(	(	kIx(	(
<g/>
507	[number]	k4	507
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sopka	sopka	k1gFnSc1	sopka
Terevaka	Terevak	k1gMnSc2	Terevak
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
;	;	kIx,	;
její	její	k3xOp3gNnSc1	její
stáří	stáří	k1gNnSc1	stáří
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
300	[number]	k4	300
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vulkány	vulkán	k1gInPc1	vulkán
Rano	Rana	k1gFnSc5	Rana
Kau	Kau	k1gFnPc1	Kau
a	a	k8xC	a
Poike	Poik	k1gInPc1	Poik
původně	původně	k6eAd1	původně
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
oddělené	oddělený	k2eAgInPc4d1	oddělený
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
vulkanický	vulkanický	k2eAgInSc1d1	vulkanický
proces	proces	k1gInSc1	proces
vzniku	vznik	k1gInSc2	vznik
sopky	sopka	k1gFnSc2	sopka
Maunga	Maung	k1gMnSc2	Maung
Terevaka	Terevak	k1gMnSc2	Terevak
spojil	spojit	k5eAaPmAgMnS	spojit
území	území	k1gNnPc1	území
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
sopek	sopka	k1gFnPc2	sopka
a	a	k8xC	a
zformovali	zformovat	k5eAaPmAgMnP	zformovat
dnešní	dnešní	k2eAgInSc4d1	dnešní
tvar	tvar	k1gInSc4	tvar
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
vrstvy	vrstva	k1gFnPc1	vrstva
kráteru	kráter	k1gInSc2	kráter
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc6	Rarak
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
tufem	tuf	k1gInSc7	tuf
-	-	kIx~	-
zhutněným	zhutněný	k2eAgInSc7d1	zhutněný
sopečným	sopečný	k2eAgInSc7d1	sopečný
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kráter	kráter	k1gInSc1	kráter
je	být	k5eAaImIp3nS	být
tvarově	tvarově	k6eAd1	tvarově
výrazně	výrazně	k6eAd1	výrazně
nesymetrický	symetrický	k2eNgInSc1d1	nesymetrický
<g/>
.	.	kIx.	.
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgFnPc1d1	jihovýchodní
stěny	stěna	k1gFnPc1	stěna
kráteru	kráter	k1gInSc2	kráter
jsou	být	k5eAaImIp3nP	být
příkré	příkrý	k2eAgInPc1d1	příkrý
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
znemožňující	znemožňující	k2eAgInSc1d1	znemožňující
turistický	turistický	k2eAgInSc1d1	turistický
přístup	přístup	k1gInSc1	přístup
na	na	k7c4	na
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
část	část	k1gFnSc1	část
kráteru	kráter	k1gInSc2	kráter
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
svažitá	svažitý	k2eAgFnSc1d1	svažitá
a	a	k8xC	a
umožňující	umožňující	k2eAgInSc1d1	umožňující
přístup	přístup	k1gInSc1	přístup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kráterové	kráterový	k2eAgNnSc1d1	kráterové
jezero	jezero	k1gNnSc1	jezero
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
Rano	Rana	k1gFnSc5	Rana
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
kráterové	kráterový	k2eAgNnSc1d1	kráterové
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc1	tři
kráterová	kráterový	k2eAgNnPc1d1	kráterové
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgInPc7d1	jediný
přírodními	přírodní	k2eAgInPc7d1	přírodní
rezervoáry	rezervoár	k1gInPc7	rezervoár
srážkové	srážkový	k2eAgFnSc2d1	srážková
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gInSc6	Rarak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Rano	Rana	k1gFnSc5	Rana
Kau	Kau	k1gMnSc1	Kau
a	a	k8xC	a
Rano	Rana	k1gFnSc5	Rana
Aroi	Aro	k1gInSc3	Aro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc3	Rarak
má	mít	k5eAaImIp3nS	mít
plochu	plocha	k1gFnSc4	plocha
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
hladiny	hladina	k1gFnSc2	hladina
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
přibližně	přibližně	k6eAd1	přibližně
350	[number]	k4	350
m.	m.	k?	m.
Objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
206	[number]	k4	206
000	[number]	k4	000
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
3	[number]	k4	3
<g/>
m.	m.	k?	m.
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
ph	ph	kA	ph
<g/>
=	=	kIx~	=
<g/>
7,15	[number]	k4	7,15
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Břehy	břeh	k1gInPc1	břeh
kráterového	kráterový	k2eAgNnSc2d1	kráterové
jezera	jezero	k1gNnSc2	jezero
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gInSc3	Rarak
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgInPc1d1	porostlý
rákosím	rákosí	k1gNnSc7	rákosí
totora	totor	k1gMnSc2	totor
Polygonum	Polygonum	k1gInSc1	Polygonum
a	a	k8xC	a
Scirpus	Scirpus	k1gMnSc1	Scirpus
californicus	californicus	k1gMnSc1	californicus
<g/>
.	.	kIx.	.
</s>
<s>
Vegetace	vegetace	k1gFnSc1	vegetace
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
asi	asi	k9	asi
45	[number]	k4	45
<g/>
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
hladiny	hladina	k1gFnSc2	hladina
zejména	zejména	k9	zejména
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
třetině	třetina	k1gFnSc6	třetina
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
plovoucími	plovoucí	k2eAgInPc7d1	plovoucí
ostrůvky	ostrůvek	k1gInPc7	ostrůvek
endemického	endemický	k2eAgInSc2d1	endemický
mechu	mech	k1gInSc2	mech
Campylopus	Campylopus	k1gMnSc1	Campylopus
turficola	turficola	k1gFnSc1	turficola
<g/>
.	.	kIx.	.
</s>
<s>
Vodovodní	vodovodní	k2eAgInSc1d1	vodovodní
rozvod	rozvod	k1gInSc1	rozvod
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
usedlosti	usedlost	k1gFnPc4	usedlost
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
napajedlem	napajedlo	k1gNnSc7	napajedlo
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
oblast	oblast	k1gFnSc1	oblast
==	==	k?	==
</s>
</p>
<p>
<s>
Kráter	kráter	k1gInSc1	kráter
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc6	Rarak
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Rapa	rap	k1gMnSc2	rap
Nui	Nui	k1gMnSc2	Nui
<g/>
,	,	kIx,	,
zřízeného	zřízený	k2eAgInSc2d1	zřízený
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
chilským	chilský	k2eAgNnSc7d1	Chilské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
a	a	k8xC	a
kolonizaci	kolonizace	k1gFnSc4	kolonizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gInSc2	Rarak
nežijí	žít	k5eNaImIp3nP	žít
žádní	žádný	k3yNgMnPc1	žádný
vodní	vodní	k2eAgMnPc1d1	vodní
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zpěvného	zpěvný	k2eAgNnSc2d1	zpěvné
ptactva	ptactvo	k1gNnSc2	ptactvo
dnes	dnes	k6eAd1	dnes
obývá	obývat	k5eAaImIp3nS	obývat
oblast	oblast	k1gFnSc4	oblast
kolem	kolem	k7c2	kolem
jezera	jezero	k1gNnSc2	jezero
množství	množství	k1gNnSc2	množství
dijuk	dijuk	k1gMnSc1	dijuk
obecných	obecná	k1gFnPc2	obecná
Diuca	Diuca	k1gMnSc1	Diuca
diuca	diuca	k1gMnSc1	diuca
a	a	k8xC	a
vrabců	vrabec	k1gMnPc2	vrabec
Passer	Passer	k1gMnSc1	Passer
domesticus	domesticus	k1gMnSc1	domesticus
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
Milvago	Milvago	k6eAd1	Milvago
chimango	chimango	k6eAd1	chimango
a	a	k8xC	a
Nothoprocta	Nothoprocta	k1gFnSc1	Nothoprocta
perdicaria	perdicarium	k1gNnSc2	perdicarium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vážky	vážka	k1gFnPc4	vážka
Neuroptera	Neuropter	k1gMnSc2	Neuropter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flóra	Flóra	k1gFnSc1	Flóra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
endemicky	endemicky	k6eAd1	endemicky
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
křivonožka	křivonožka	k1gMnSc1	křivonožka
Campylopus	Campylopus	k1gMnSc1	Campylopus
turficola	turficola	k1gFnSc1	turficola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tufový	tufový	k2eAgInSc1d1	tufový
lom	lom	k1gInSc1	lom
a	a	k8xC	a
sochy	socha	k1gFnPc1	socha
Moai	Moa	k1gFnSc2	Moa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nedokončené	dokončený	k2eNgFnPc1d1	nedokončená
sochy	socha	k1gFnPc1	socha
===	===	k?	===
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
kráteru	kráter	k1gInSc2	kráter
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gInSc2	Rarak
byla	být	k5eAaImAgFnS	být
lomem	lom	k1gInSc7	lom
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc4	který
domorodci	domorodec	k1gMnPc1	domorodec
tesali	tesat	k5eAaImAgMnP	tesat
sochy	socha	k1gFnPc4	socha
Moai	Moai	k1gNnSc2	Moai
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
soch	socha	k1gFnPc2	socha
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
lomu	lom	k1gInSc6	lom
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc3	Rarak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lomu	lom	k1gInSc6	lom
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
nedokončené	dokončený	k2eNgFnPc1d1	nedokončená
sochy	socha	k1gFnPc1	socha
i	i	k8xC	i
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
výskytu	výskyt	k1gInSc2	výskyt
xenolitu	xenolit	k1gInSc2	xenolit
v	v	k7c6	v
opracovávaném	opracovávaný	k2eAgInSc6d1	opracovávaný
kusu	kus	k1gInSc6	kus
tufu	tuf	k1gInSc2	tuf
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
byly	být	k5eAaImAgFnP	být
sochy	socha	k1gFnPc1	socha
tesány	tesán	k2eAgFnPc1d1	tesána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lomu	lom	k1gInSc6	lom
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
množství	množství	k1gNnSc1	množství
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
opotřebených	opotřebený	k2eAgFnPc2d1	opotřebená
tesáním	tesání	k1gNnSc7	tesání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vztyčené	vztyčený	k2eAgFnSc2d1	vztyčená
Moai	Moa	k1gFnSc2	Moa
===	===	k?	===
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
lom	lom	k1gInSc4	lom
<g/>
,	,	kIx,	,
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
svahu	svah	k1gInSc6	svah
kráteru	kráter	k1gInSc2	kráter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
kráteru	kráter	k1gInSc2	kráter
mezi	mezi	k7c7	mezi
hřbetem	hřbet	k1gInSc7	hřbet
a	a	k8xC	a
kráterovým	kráterový	k2eAgNnSc7d1	kráterové
jezerem	jezero	k1gNnSc7	jezero
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
soch	socha	k1gFnPc2	socha
Moai	Moa	k1gFnSc2	Moa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
ramena	rameno	k1gNnPc4	rameno
nebo	nebo	k8xC	nebo
po	po	k7c4	po
bradu	brada	k1gFnSc4	brada
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
soch	socha	k1gFnPc2	socha
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
pukao	pukao	k6eAd1	pukao
a	a	k8xC	a
nebyly	být	k5eNaImAgFnP	být
svrženy	svrhnout	k5eAaPmNgFnP	svrhnout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ostrovní	ostrovní	k2eAgFnSc2d1	ostrovní
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mají	mít	k5eAaImIp3nP	mít
špičatou	špičatý	k2eAgFnSc4d1	špičatá
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
úmysl	úmysl	k1gInSc4	úmysl
vztyčit	vztyčit	k5eAaPmF	vztyčit
je	on	k3xPp3gNnSc4	on
na	na	k7c6	na
ahu	ahu	k?	ahu
plošinách	plošina	k1gFnPc6	plošina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
kráteru	kráter	k1gInSc2	kráter
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc6	Rarak
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
soch	socha	k1gFnPc2	socha
Moai	Moai	k1gNnSc2	Moai
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tukuturi	Tukuturi	k1gNnPc3	Tukuturi
===	===	k?	===
</s>
</p>
<p>
<s>
Zcela	zcela	k6eAd1	zcela
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
Moai	Moa	k1gFnSc3	Moa
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Rano	Rana	k1gFnSc5	Rana
Rarako	Rarako	k1gNnSc1	Rarako
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Tukuturi	Tukutur	k1gFnSc2	Tukutur
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
socha	socha	k1gFnSc1	socha
s	s	k7c7	s
vousy	vous	k1gInPc7	vous
a	a	k8xC	a
v	v	k7c6	v
klečící	klečící	k2eAgFnSc6d1	klečící
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
sochami	socha	k1gFnPc7	socha
Moai	Moai	k1gNnSc2	Moai
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tukuturi	Tukuturi	k6eAd1	Tukuturi
je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
z	z	k7c2	z
červené	červený	k2eAgFnSc2d1	červená
sopečné	sopečný	k2eAgFnSc2d1	sopečná
strusky	struska	k1gFnSc2	struska
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
Puna	Pun	k1gMnSc4	Pun
Pau	Pau	k1gMnSc4	Pau
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
tedy	tedy	k9	tedy
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
u	u	k7c2	u
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gMnSc5	Rarak
<g/>
,	,	kIx,	,
jen	jen	k9	jen
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
dopravena	dopravit	k5eAaPmNgFnS	dopravit
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
má	mít	k5eAaImIp3nS	mít
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
kultu	kult	k1gInSc3	kult
Ptačího	ptačí	k2eAgMnSc2d1	ptačí
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
tedy	tedy	k9	tedy
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
posledním	poslední	k2eAgMnPc3d1	poslední
sochám	sochat	k5eAaImIp1nS	sochat
Moai	Moa	k1gFnPc4	Moa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Tukuturi	Tukuturi	k1gNnPc1	Tukuturi
byla	být	k5eAaImAgNnP	být
vykopána	vykopat	k5eAaPmNgNnP	vykopat
během	během	k7c2	během
expedicí	expedice	k1gFnPc2	expedice
Thora	Thor	k1gMnSc2	Thor
Heyerdahla	Heyerdahla	k1gFnSc2	Heyerdahla
konané	konaný	k2eAgFnSc2d1	konaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
56	[number]	k4	56
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
