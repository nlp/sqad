<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1572	[number]	k4	1572
si	se	k3xPyFc3	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
král	král	k1gMnSc1	král
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Markétu	Markéta	k1gFnSc4	Markéta
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
Margot	Margot	k1gInSc4	Margot
<g/>
.	.	kIx.	.
</s>
