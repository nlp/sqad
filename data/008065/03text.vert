<s>
Manufaktura	manufaktura	k1gFnSc1	manufaktura
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
manu	manout	k5eAaImIp1nS	manout
facere	facrat	k5eAaPmIp3nS	facrat
pracovat	pracovat	k5eAaImF	pracovat
rukama	ruka	k1gFnPc7	ruka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
dílna	dílna	k1gFnSc1	dílna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
práce	práce	k1gFnSc1	práce
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
dělá	dělat	k5eAaImIp3nS	dělat
jen	jen	k9	jen
dílčí	dílčí	k2eAgInPc4d1	dílčí
úkony	úkon	k1gInPc4	úkon
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
významného	významný	k2eAgNnSc2d1	významné
využití	využití	k1gNnSc2	využití
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Manufaktury	manufaktura	k1gFnPc1	manufaktura
představují	představovat	k5eAaImIp3nP	představovat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
podobu	podoba	k1gFnSc4	podoba
kapitalistického	kapitalistický	k2eAgNnSc2d1	kapitalistické
hospodaření	hospodaření	k1gNnSc2	hospodaření
a	a	k8xC	a
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
mnoho	mnoho	k4c1	mnoho
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
koncentrace	koncentrace	k1gFnSc2	koncentrace
individuálního	individuální	k2eAgInSc2d1	individuální
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
masa	masa	k1gFnSc1	masa
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
trh	trh	k1gInSc4	trh
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Předpokladem	předpoklad	k1gInSc7	předpoklad
vzniku	vznik	k1gInSc2	vznik
manufaktury	manufaktura	k1gFnSc2	manufaktura
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
určitém	určitý	k2eAgInSc6d1	určitý
druhu	druh	k1gInSc6	druh
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
nahromadění	nahromadění	k1gNnSc2	nahromadění
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
její	její	k3xOp3gNnSc4	její
založení	založení	k1gNnSc4	založení
umožnil	umožnit	k5eAaPmAgMnS	umožnit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
manufaktury	manufaktura	k1gFnPc1	manufaktura
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
a	a	k8xC	a
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
především	především	k9	především
sukno	sukno	k1gNnSc4	sukno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
soukenické	soukenický	k2eAgFnPc1d1	Soukenická
manufaktury	manufaktura	k1gFnPc1	manufaktura
hojně	hojně	k6eAd1	hojně
zakládány	zakládat	k5eAaImNgFnP	zakládat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Manufaktury	manufaktura	k1gFnPc1	manufaktura
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
předstupněm	předstupeň	k1gInSc7	předstupeň
strojové	strojový	k2eAgFnPc1d1	strojová
tovární	tovární	k2eAgFnPc1d1	tovární
velkovýroby	velkovýroba	k1gFnPc1	velkovýroba
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
Dělba	dělba	k1gFnSc1	dělba
práce	práce	k1gFnSc1	práce
umožnila	umožnit	k5eAaPmAgFnS	umožnit
snížení	snížení	k1gNnSc4	snížení
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
nižší	nízký	k2eAgFnSc4d2	nižší
cenu	cena	k1gFnSc4	cena
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1	nevýhoda
Výrobek	výrobek	k1gInSc1	výrobek
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
"	"	kIx"	"
<g/>
vizitkou	vizitka	k1gFnSc7	vizitka
<g/>
"	"	kIx"	"
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
někdy	někdy	k6eAd1	někdy
byl	být	k5eAaImAgInS	být
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
manufaktur	manufaktura	k1gFnPc2	manufaktura
<g/>
:	:	kIx,	:
Rozptýlené	rozptýlený	k2eAgFnSc2d1	rozptýlená
manufaktury	manufaktura	k1gFnSc2	manufaktura
Byly	být	k5eAaImAgInP	být
přechodným	přechodný	k2eAgInSc7d1	přechodný
jevem	jev	k1gInSc7	jev
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
buď	buď	k8xC	buď
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přeměnily	přeměnit	k5eAaPmAgFnP	přeměnit
v	v	k7c4	v
manufaktury	manufaktura	k1gFnPc4	manufaktura
soustředěné	soustředěný	k2eAgFnPc4d1	soustředěná
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
manufaktur	manufaktura	k1gFnPc2	manufaktura
rozptýlených	rozptýlený	k2eAgInPc2d1	rozptýlený
byl	být	k5eAaImAgInS	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Dělníci	dělník	k1gMnPc1	dělník
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
dílnách	dílna	k1gFnPc6	dílna
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc4	dům
obešel	obejít	k5eAaPmAgInS	obejít
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
rozdal	rozdat	k5eAaPmAgMnS	rozdat
surovinu	surovina	k1gFnSc4	surovina
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgMnS	vybrat
hotové	hotový	k2eAgNnSc4d1	hotové
zboží	zboží	k1gNnSc4	zboží
a	a	k8xC	a
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
mzdu	mzda	k1gFnSc4	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Předstupněm	předstupeň	k1gInSc7	předstupeň
rozptýlené	rozptýlený	k2eAgFnSc2d1	rozptýlená
manufaktury	manufaktura	k1gFnSc2	manufaktura
byl	být	k5eAaImAgInS	být
nákladnický	nákladnický	k2eAgInSc1d1	nákladnický
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Soustředěné	soustředěný	k2eAgFnPc1d1	soustředěná
manufaktury	manufaktura	k1gFnPc1	manufaktura
Byly	být	k5eAaImAgFnP	být
velké	velký	k2eAgFnPc1d1	velká
dílny	dílna	k1gFnPc1	dílna
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
byla	být	k5eAaImAgFnS	být
soustředěna	soustředěn	k2eAgFnSc1d1	soustředěna
veškerá	veškerý	k3xTgFnSc1	veškerý
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
KLÍMA	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
<g/>
.	.	kIx.	.
</s>
<s>
Manufakturní	manufakturní	k2eAgNnSc1d1	manufakturní
období	období	k1gNnSc1	období
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
523	[number]	k4	523
s.	s.	k?	s.
MRÁZEK	Mrázek	k1gMnSc1	Mrázek
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
od	od	k7c2	od
manufaktury	manufaktura	k1gFnSc2	manufaktura
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
490	[number]	k4	490
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
manufaktura	manufaktura	k1gFnSc1	manufaktura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
manufaktura	manufaktura	k1gFnSc1	manufaktura
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Litvínovská	litvínovský	k2eAgFnSc1d1	Litvínovská
hraběcí	hraběcí	k2eAgFnSc1d1	hraběcí
manufaktura	manufaktura	k1gFnSc1	manufaktura
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
sukna	sukno	k1gNnSc2	sukno
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
Jana	Jan	k1gMnSc2	Jan
Josefa	Josef	k1gMnSc2	Josef
Valdštejna	Valdštejn	k1gMnSc2	Valdštejn
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1728	[number]	k4	1728
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
digitalizovaná	digitalizovaný	k2eAgFnSc1d1	digitalizovaná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
služby	služba	k1gFnSc2	služba
eBooks	eBooks	k6eAd1	eBooks
on	on	k3xPp3gInSc1	on
Demand	Demand	k1gInSc1	Demand
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
technické	technický	k2eAgFnSc6d1	technická
knihovně	knihovna	k1gFnSc6	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
