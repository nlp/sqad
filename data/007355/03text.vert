<s>
Halové	halový	k2eAgInPc1d1	halový
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
halo	halo	k1gNnSc4	halo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
optické	optický	k2eAgInPc4d1	optický
úkazy	úkaz	k1gInPc4	úkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
odrazem	odraz	k1gInSc7	odraz
či	či	k8xC	či
průchodem	průchod	k1gInSc7	průchod
slunečních	sluneční	k2eAgInPc2d1	sluneční
respektive	respektive	k9	respektive
měsíčních	měsíční	k2eAgInPc2d1	měsíční
paprsků	paprsek	k1gInPc2	paprsek
drobnými	drobný	k2eAgInPc7d1	drobný
ledovými	ledový	k2eAgInPc7d1	ledový
krystaly	krystal	k1gInPc7	krystal
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
těchto	tento	k3xDgInPc2	tento
úkazů	úkaz	k1gInPc2	úkaz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
krystalky	krystalka	k1gFnPc1	krystalka
ledu	led	k1gInSc2	led
měly	mít	k5eAaImAgFnP	mít
tvar	tvar	k1gInSc4	tvar
šestiboké	šestiboký	k2eAgFnSc2d1	šestiboká
destičky	destička	k1gFnSc2	destička
nebo	nebo	k8xC	nebo
hranolku	hranolek	k1gInSc2	hranolek
<g/>
.	.	kIx.	.
</s>
<s>
Halové	halový	k2eAgInPc1d1	halový
jevy	jev	k1gInPc1	jev
nejčastěji	často	k6eAd3	často
spatříme	spatřit	k5eAaPmIp1nP	spatřit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
oblohu	obloha	k1gFnSc4	obloha
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
oblaka	oblaka	k1gNnPc4	oblaka
typu	typ	k1gInSc2	typ
cirrostratus	cirrostratus	k1gInSc1	cirrostratus
<g/>
.	.	kIx.	.
</s>
<s>
Halové	halový	k2eAgInPc1d1	halový
jevy	jev	k1gInPc1	jev
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
již	již	k6eAd1	již
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
výklad	výklad	k1gInSc1	výklad
těchto	tento	k3xDgInPc2	tento
jevů	jev	k1gInPc2	jev
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
René	René	k1gFnSc2	René
Descarta	Descart	k1gMnSc2	Descart
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
první	první	k4xOgFnSc4	první
soubornou	souborný	k2eAgFnSc4d1	souborná
teorii	teorie	k1gFnSc4	teorie
sepsal	sepsat	k5eAaPmAgMnS	sepsat
další	další	k2eAgMnSc1d1	další
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Edme	Edmus	k1gMnSc5	Edmus
Mariotte	Mariott	k1gMnSc5	Mariott
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
chladných	chladný	k2eAgFnPc2d1	chladná
zimních	zimní	k2eAgFnPc2d1	zimní
nocí	noc	k1gFnPc2	noc
nebo	nebo	k8xC	nebo
dnů	den	k1gInPc2	den
mohou	moct	k5eAaImIp3nP	moct
halové	halový	k2eAgInPc1d1	halový
jevy	jev	k1gInPc1	jev
vzniknout	vzniknout	k5eAaPmF	vzniknout
také	také	k6eAd1	také
na	na	k7c6	na
krystalcích	krystalek	k1gInPc6	krystalek
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
vrstvě	vrstva	k1gFnSc6	vrstva
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vlhkost	vlhkost	k1gFnSc1	vlhkost
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
přechlazené	přechlazený	k2eAgFnPc1d1	přechlazená
kapky	kapka	k1gFnPc1	kapka
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
krystalizačních	krystalizační	k2eAgNnPc2d1	krystalizační
jader	jádro	k1gNnPc2	jádro
transformovat	transformovat	k5eAaBmF	transformovat
v	v	k7c4	v
ledové	ledový	k2eAgInPc4d1	ledový
krystaly	krystal	k1gInPc4	krystal
o	o	k7c6	o
tvaru	tvar	k1gInSc6	tvar
šestibokého	šestiboký	k2eAgInSc2d1	šestiboký
sloupku	sloupek	k1gInSc2	sloupek
nebo	nebo	k8xC	nebo
destičky	destička	k1gFnSc2	destička
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
tak	tak	k6eAd1	tak
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
světla	světlo	k1gNnSc2	světlo
halové	halový	k2eAgInPc1d1	halový
jevy	jev	k1gInPc1	jev
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
°	°	k?	°
halo	halo	k1gNnSc4	halo
9	[number]	k4	9
<g/>
°	°	k?	°
halo	halo	k1gNnSc4	halo
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Van	van	k1gInSc4	van
Buijsenovo	Buijsenův	k2eAgNnSc4d1	Buijsenův
halo	halo	k1gNnSc4	halo
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
°	°	k?	°
halo	halo	k1gNnSc4	halo
18	[number]	k4	18
<g/>
°	°	k?	°
halo	halo	k1gNnSc1	halo
(	(	kIx(	(
<g/>
Rankinovo	Rankinův	k2eAgNnSc1d1	Rankinův
halo	halo	k1gNnSc1	halo
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
°	°	k?	°
halo	halo	k1gNnSc1	halo
(	(	kIx(	(
<g/>
Burneyovo	Burneyův	k2eAgNnSc1d1	Burneyův
halo	halo	k1gNnSc1	halo
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
°	°	k?	°
halo	halo	k1gNnSc1	halo
(	(	kIx(	(
<g/>
malé	malý	k2eAgNnSc1d1	malé
halo	halo	k1gNnSc1	halo
<g/>
)	)	kIx)	)
–	–	k?	–
nejobvyklejší	obvyklý	k2eAgInSc4d3	nejobvyklejší
23	[number]	k4	23
<g/>
°	°	k?	°
halo	halo	k1gNnSc1	halo
(	(	kIx(	(
<g/>
Barkowovo	Barkowovo	k1gNnSc1	Barkowovo
<g />
.	.	kIx.	.
</s>
<s>
halo	halo	k1gNnSc1	halo
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
°	°	k?	°
halo	halo	k1gNnSc1	halo
(	(	kIx(	(
<g/>
Dutheilovo	Dutheilův	k2eAgNnSc1d1	Dutheilův
halo	halo	k1gNnSc1	halo
<g/>
)	)	kIx)	)
35	[number]	k4	35
<g/>
°	°	k?	°
halo	halo	k1gNnSc1	halo
(	(	kIx(	(
<g/>
Feuilleeovo	Feuilleeův	k2eAgNnSc1d1	Feuilleeův
halo	halo	k1gNnSc1	halo
<g/>
)	)	kIx)	)
46	[number]	k4	46
<g/>
°	°	k?	°
halo	halo	k1gNnSc1	halo
(	(	kIx(	(
<g/>
velké	velký	k2eAgNnSc1d1	velké
halo	halo	k1gNnSc1	halo
<g/>
)	)	kIx)	)
spojené	spojený	k2eAgInPc1d1	spojený
dotykové	dotykový	k2eAgInPc1d1	dotykový
oblouky	oblouk	k1gInPc1	oblouk
Duha	duha	k1gFnSc1	duha
Lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
Odraz	odraz	k1gInSc4	odraz
světla	světlo	k1gNnSc2	světlo
Disperze	disperze	k1gFnSc2	disperze
světla	světlo	k1gNnSc2	světlo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Halové	halový	k2eAgInPc1d1	halový
jevy	jev	k1gInPc1	jev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
halo	halo	k1gNnSc4	halo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Úvod	úvod	k1gInSc1	úvod
k	k	k7c3	k
halovým	halový	k2eAgInPc3d1	halový
jevům	jev	k1gInPc3	jev
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Astro	astra	k1gFnSc5	astra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Diamantový	diamantový	k2eAgInSc1d1	diamantový
prach	prach	k1gInSc1	prach
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
atoptics	atoptics	k1gInSc1	atoptics
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
</s>
