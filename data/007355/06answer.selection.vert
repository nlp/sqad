<s>
Halové	halový	k2eAgInPc1d1	halový
jevy	jev	k1gInPc1	jev
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
již	již	k6eAd1	již
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
výklad	výklad	k1gInSc1	výklad
těchto	tento	k3xDgInPc2	tento
jevů	jev	k1gInPc2	jev
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
René	René	k1gFnSc2	René
Descarta	Descart	k1gMnSc2	Descart
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
první	první	k4xOgFnSc4	první
soubornou	souborný	k2eAgFnSc4d1	souborná
teorii	teorie	k1gFnSc4	teorie
sepsal	sepsat	k5eAaPmAgMnS	sepsat
další	další	k2eAgMnSc1d1	další
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Edme	Edmus	k1gMnSc5	Edmus
Mariotte	Mariott	k1gMnSc5	Mariott
<g/>
.	.	kIx.	.
</s>
