<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
Spinozu	Spinoz	k1gInSc6	Spinoz
odvolávali	odvolávat	k5eAaImAgMnP	odvolávat
například	například	k6eAd1	například
Ludwig	Ludwig	k1gMnSc1	Ludwig
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
Spinozou	Spinoza	k1gFnSc7	Spinoza
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
spise	spis	k1gInSc6	spis
<g/>
,	,	kIx,	,
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
"	"	kIx"	"
<g/>
Tractatus	Tractatus	k1gMnSc1	Tractatus
logico-philosophicus	logicohilosophicus	k1gMnSc1	logico-philosophicus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
myslitelé	myslitel	k1gMnPc1	myslitel
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
marxismem	marxismus	k1gInSc7	marxismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Louis	Louis	k1gMnSc1	Louis
Althusser	Althusser	k1gMnSc1	Althusser
<g/>
,	,	kIx,	,
Gilles	Gilles	k1gMnSc1	Gilles
Deleuze	Deleuze	k1gFnSc2	Deleuze
nebo	nebo	k8xC	nebo
Slavoj	Slavoj	k1gMnSc1	Slavoj
Žižek	Žižek	k1gMnSc1	Žižek
<g/>
.	.	kIx.	.
</s>
