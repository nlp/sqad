<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
první	první	k4xOgInSc1	první
spis	spis	k1gInSc1	spis
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Wittgensteina	Wittgenstein	k1gMnSc2	Wittgenstein
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
Spinozou	Spinoza	k1gFnSc7	Spinoza
<g/>
?	?	kIx.	?
</s>
