<s>
Baruch	Baruch	k1gInSc1	Baruch
Spinoza	Spinoza	k1gFnSc1	Spinoza
(	(	kIx(	(
<g/>
či	či	k8xC	či
Benedikt	Benedikt	k1gMnSc1	Benedikt
Spinoza	Spinoza	k1gFnSc1	Spinoza
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Benedictus	Benedictus	k1gInSc1	Benedictus
<g/>
;	;	kIx,	;
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1632	[number]	k4	1632
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1677	[number]	k4	1677
<g/>
,	,	kIx,	,
Haag	Haag	k1gInSc1	Haag
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
holandský	holandský	k2eAgMnSc1d1	holandský
filozof	filozof	k1gMnSc1	filozof
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mj.	mj.	kA	mj.
kriticky	kriticky	k6eAd1	kriticky
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
Descartovu	Descartův	k2eAgFnSc4d1	Descartova
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
substancích	substance	k1gFnPc6	substance
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
současníky	současník	k1gMnPc7	současník
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejdůslednějších	důsledný	k2eAgMnPc2d3	nejdůslednější
racionalistů	racionalist	k1gMnPc2	racionalist
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
kritických	kritický	k2eAgMnPc2d1	kritický
čtenářů	čtenář	k1gMnPc2	čtenář
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
hlavně	hlavně	k9	hlavně
jeho	jeho	k3xOp3gFnSc1	jeho
politická	politický	k2eAgFnSc1d1	politická
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
překonání	překonání	k1gNnSc1	překonání
kartézského	kartézský	k2eAgInSc2d1	kartézský
dualismu	dualismus	k1gInSc2	dualismus
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Bento	Bento	k1gNnSc1	Bento
(	(	kIx(	(
<g/>
Benedikt	Benedikt	k1gMnSc1	Benedikt
<g/>
)	)	kIx)	)
resp.	resp.	kA	resp.
Baruch	Baruch	k1gMnSc1	Baruch
de	de	k?	de
Spinoza	Spinoza	k1gFnSc1	Spinoza
(	(	kIx(	(
<g/>
také	také	k9	také
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Espinoza	Espinoza	k1gFnSc1	Espinoza
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
zámožné	zámožný	k2eAgFnSc6d1	zámožná
sefardské	sefardský	k2eAgFnSc6d1	sefardská
obchodnické	obchodnický	k2eAgFnSc6d1	obchodnická
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
Holandska	Holandsko	k1gNnSc2	Holandsko
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
nižší	nízký	k2eAgFnSc4d2	nižší
talmudskou	talmudský	k2eAgFnSc4d1	talmudská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgMnS	být
mimořádně	mimořádně	k6eAd1	mimořádně
nadaný	nadaný	k2eAgMnSc1d1	nadaný
<g/>
,	,	kIx,	,
do	do	k7c2	do
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
obvykle	obvykle	k6eAd1	obvykle
přípravou	příprava	k1gFnSc7	příprava
k	k	k7c3	k
povolání	povolání	k1gNnSc3	povolání
rabína	rabín	k1gMnSc2	rabín
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
nezapsal	zapsat	k5eNaPmAgMnS	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
převzal	převzít	k5eAaPmAgInS	převzít
jeho	jeho	k3xOp3gFnSc4	jeho
zadluženou	zadlužený	k2eAgFnSc4d1	zadlužená
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
však	však	k9	však
později	pozdě	k6eAd2	pozdě
předal	předat	k5eAaPmAgInS	předat
svému	svůj	k3xOyFgMnSc3	svůj
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratru	bratr	k1gMnSc3	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1650	[number]	k4	1650
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
protestantskými	protestantský	k2eAgInPc7d1	protestantský
menonity	menonit	k1gInPc7	menonit
a	a	k8xC	a
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
latinskou	latinský	k2eAgFnSc4d1	Latinská
školu	škola	k1gFnSc4	škola
bývalého	bývalý	k2eAgMnSc2d1	bývalý
jezuity	jezuita	k1gMnSc2	jezuita
Van	van	k1gInSc4	van
den	den	k1gInSc1	den
Endena	Endena	k1gFnSc1	Endena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Druhou	druhý	k4xOgFnSc7	druhý
scholastikou	scholastika	k1gFnSc7	scholastika
a	a	k8xC	a
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
R.	R.	kA	R.
Descarta	Descart	k1gMnSc4	Descart
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
udělalo	udělat	k5eAaPmAgNnS	udělat
velmi	velmi	k6eAd1	velmi
hluboký	hluboký	k2eAgInSc4d1	hluboký
dojem	dojem	k1gInSc4	dojem
<g/>
;	;	kIx,	;
Maimonidovo	Maimonidův	k2eAgNnSc4d1	Maimonidův
dílo	dílo	k1gNnSc4	dílo
znal	znát	k5eAaImAgInS	znát
patrně	patrně	k6eAd1	patrně
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
racionalisty	racionalist	k1gMnPc7	racionalist
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přijeli	přijet	k5eAaPmAgMnP	přijet
do	do	k7c2	do
Holandska	Holandsko	k1gNnSc2	Holandsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1656	[number]	k4	1656
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
těžkého	těžký	k2eAgInSc2d1	těžký
názorového	názorový	k2eAgInSc2d1	názorový
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
amsterodamskou	amsterodamský	k2eAgFnSc7d1	Amsterodamská
židovskou	židovský	k2eAgFnSc7d1	židovská
obcí	obec	k1gFnSc7	obec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
slavnostně	slavnostně	k6eAd1	slavnostně
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
a	a	k8xC	a
zakázala	zakázat	k5eAaPmAgFnS	zakázat
svým	svůj	k3xOyFgMnPc3	svůj
členům	člen	k1gMnPc3	člen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
stýkali	stýkat	k5eAaImAgMnP	stýkat
<g/>
.	.	kIx.	.
</s>
<s>
Spinoza	Spinoza	k1gFnSc1	Spinoza
žil	žíla	k1gFnPc2	žíla
snad	snad	k9	snad
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
nebo	nebo	k8xC	nebo
v	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
Rijnsburgu	Rijnsburg	k1gInSc6	Rijnsburg
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
svoji	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
pak	pak	k6eAd1	pak
užíval	užívat	k5eAaImAgInS	užívat
jen	jen	k9	jen
latinskou	latinský	k2eAgFnSc4d1	Latinská
formu	forma	k1gFnSc4	forma
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
Benedikt	benedikt	k1gInSc1	benedikt
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
Bento	Bent	k2eAgNnSc1d1	Bento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1658	[number]	k4	1658
<g/>
-	-	kIx~	-
<g/>
1659	[number]	k4	1659
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1660	[number]	k4	1660
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
latinském	latinský	k2eAgInSc6d1	latinský
"	"	kIx"	"
<g/>
Traktátu	traktát	k1gInSc6	traktát
o	o	k7c6	o
nápravě	náprava	k1gFnSc6	náprava
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc3	pojednání
o	o	k7c6	o
Bohu	bůh	k1gMnSc6	bůh
<g/>
,	,	kIx,	,
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
štěstí	štěstí	k1gNnSc6	štěstí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Živil	živit	k5eAaImAgMnS	živit
se	s	k7c7	s
broušením	broušení	k1gNnSc7	broušení
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
snad	snad	k9	snad
i	i	k8xC	i
výrobou	výroba	k1gFnSc7	výroba
mikroskopů	mikroskop	k1gInPc2	mikroskop
a	a	k8xC	a
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
získala	získat	k5eAaPmAgFnS	získat
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gInPc1	jeho
filosofické	filosofický	k2eAgInPc1d1	filosofický
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgMnS	začít
dopisovat	dopisovat	k5eAaImF	dopisovat
s	s	k7c7	s
anglickými	anglický	k2eAgMnPc7d1	anglický
i	i	k8xC	i
německými	německý	k2eAgMnPc7d1	německý
učenci	učenec	k1gMnPc7	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
latinské	latinský	k2eAgNnSc4d1	latinské
shrnutí	shrnutí	k1gNnSc4	shrnutí
Descartových	Descartův	k2eAgNnPc2d1	Descartovo
"	"	kIx"	"
<g/>
Principů	princip	k1gInPc2	princip
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Haagu	Haag	k1gInSc2	Haag
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
obdržel	obdržet	k5eAaPmAgMnS	obdržet
pozvání	pozvání	k1gNnSc4	pozvání
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
<g/>
,	,	kIx,	,
formulované	formulovaný	k2eAgInPc1d1	formulovaný
ovšem	ovšem	k9	ovšem
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemohl	moct	k5eNaImAgMnS	moct
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
i	i	k9	i
pozvání	pozvání	k1gNnSc4	pozvání
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
anonymně	anonymně	k6eAd1	anonymně
"	"	kIx"	"
<g/>
Teologicko-politický	teologickoolitický	k2eAgInSc1d1	teologicko-politický
traktát	traktát	k1gInSc1	traktát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sice	sice	k8xC	sice
uznával	uznávat	k5eAaImAgInS	uznávat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
vladaře	vladař	k1gMnSc2	vladař
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
polemice	polemika	k1gFnSc6	polemika
s	s	k7c7	s
Hobbesem	Hobbes	k1gInSc7	Hobbes
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
panovníkům	panovník	k1gMnPc3	panovník
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
vlastním	vlastní	k2eAgInSc6d1	vlastní
zájmu	zájem	k1gInSc6	zájem
mírnost	mírnost	k1gFnSc4	mírnost
a	a	k8xC	a
názorovou	názorový	k2eAgFnSc4d1	názorová
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1672	[number]	k4	1672
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
liberální	liberální	k2eAgMnSc1d1	liberální
regent	regent	k1gMnSc1	regent
De	De	k?	De
Witt	Witt	k1gMnSc1	Witt
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1674	[number]	k4	1674
byl	být	k5eAaImAgInS	být
Traktát	traktát	k1gInSc1	traktát
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Spinozu	Spinoz	k1gInSc3	Spinoz
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Henry	henry	k1gInSc1	henry
Oldenburg	Oldenburg	k1gInSc1	Oldenburg
<g/>
,	,	kIx,	,
Leibniz	Leibniz	k1gInSc1	Leibniz
a	a	k8xC	a
von	von	k1gInSc1	von
Tschirnhaus	Tschirnhaus	k1gInSc1	Tschirnhaus
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
Traktát	traktát	k1gInSc1	traktát
udělal	udělat	k5eAaPmAgInS	udělat
velký	velký	k2eAgInSc4d1	velký
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Leibniz	Leibniz	k1gMnSc1	Leibniz
jeho	on	k3xPp3gInSc4	on
systém	systém	k1gInSc4	systém
výslovně	výslovně	k6eAd1	výslovně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1677	[number]	k4	1677
Spinoza	Spinoz	k1gMnSc2	Spinoz
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
na	na	k7c4	na
tuberkulosu	tuberkulosa	k1gFnSc4	tuberkulosa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
snad	snad	k9	snad
souvisela	souviset	k5eAaImAgFnS	souviset
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
brusičským	brusičský	k2eAgNnSc7d1	brusičský
povoláním	povolání	k1gNnSc7	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
Spinoza	Spinoz	k1gMnSc4	Spinoz
žil	žít	k5eAaImAgMnS	žít
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
osaměle	osaměle	k6eAd1	osaměle
<g/>
,	,	kIx,	,
v	v	k7c6	v
příkladné	příkladný	k2eAgFnSc6d1	příkladná
skromnosti	skromnost	k1gFnSc6	skromnost
a	a	k8xC	a
poctivosti	poctivost	k1gFnSc6	poctivost
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
velkou	velký	k2eAgFnSc4d1	velká
občanskou	občanský	k2eAgFnSc4d1	občanská
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jeho	jeho	k3xOp3gInSc6	jeho
okolí	okolí	k1gNnSc1	okolí
většinou	většinou	k6eAd1	většinou
vidělo	vidět	k5eAaImAgNnS	vidět
ateistu	ateista	k1gMnSc4	ateista
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
filosofie	filosofie	k1gFnSc1	filosofie
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
Boha	bůh	k1gMnSc2	bůh
nemyslitelná	myslitelný	k2eNgFnSc1d1	nemyslitelná
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
zůstal	zůstat	k5eAaPmAgMnS	zůstat
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
konfese	konfese	k1gFnSc2	konfese
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
židovskou	židovský	k2eAgFnSc4d1	židovská
identitu	identita	k1gFnSc4	identita
nepopíral	popírat	k5eNaImAgMnS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
myšlení	myšlení	k1gNnSc1	myšlení
pak	pak	k6eAd1	pak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
kabaly	kabala	k1gFnSc2	kabala
a	a	k8xC	a
tradiční	tradiční	k2eAgFnSc2d1	tradiční
židovské	židovský	k2eAgFnSc2d1	židovská
etiky	etika	k1gFnSc2	etika
<g/>
.	.	kIx.	.
</s>
<s>
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
za	za	k7c2	za
Boha	bůh	k1gMnSc2	bůh
nepokládal	pokládat	k5eNaImAgMnS	pokládat
<g/>
,	,	kIx,	,
oceňoval	oceňovat	k5eAaImAgMnS	oceňovat
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
jako	jako	k8xC	jako
vzor	vzor	k1gInSc1	vzor
lidské	lidský	k2eAgFnSc2d1	lidská
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
vydali	vydat	k5eAaPmAgMnP	vydat
přátelé	přítel	k1gMnPc1	přítel
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Opera	opera	k1gFnSc1	opera
posthuma	posthuma	k1gFnSc1	posthuma
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
Posmrtné	posmrtný	k2eAgInPc4d1	posmrtný
spisy	spis	k1gInPc4	spis
<g/>
]	]	kIx)	]
jeho	jeho	k3xOp3gNnSc1	jeho
dosud	dosud	k6eAd1	dosud
nevydané	vydaný	k2eNgNnSc1d1	nevydané
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Etiku	etik	k1gMnSc6	etik
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Politický	politický	k2eAgInSc1d1	politický
traktát	traktát	k1gInSc1	traktát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
nápravě	náprava	k1gFnSc6	náprava
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hebrejskou	hebrejský	k2eAgFnSc4d1	hebrejská
gramatiku	gramatika	k1gFnSc4	gramatika
-	-	kIx~	-
vesměs	vesměs	k6eAd1	vesměs
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
-	-	kIx~	-
a	a	k8xC	a
Spinozovy	Spinozův	k2eAgInPc1d1	Spinozův
dopisy	dopis	k1gInPc1	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
dílo	dílo	k1gNnSc4	dílo
Barucha	Barucha	k1gFnSc1	Barucha
Spinozy	Spinoz	k1gInPc4	Spinoz
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c4	v
"	"	kIx"	"
<g/>
Opera	opera	k1gFnSc1	opera
posthuma	posthuma	k1gFnSc1	posthuma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
Index	index	k1gInSc4	index
zařazeny	zařadit	k5eAaPmNgInP	zařadit
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1690	[number]	k4	1690
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Teologicko-politický	teologickoolitický	k2eAgInSc1d1	teologicko-politický
traktát	traktát	k1gInSc1	traktát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Spinoza	Spinoz	k1gMnSc4	Spinoz
vydal	vydat	k5eAaPmAgMnS	vydat
anonymně	anonymně	k6eAd1	anonymně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Index	index	k1gInSc4	index
zařazen	zařazen	k2eAgInSc4d1	zařazen
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1679	[number]	k4	1679
<g/>
.	.	kIx.	.
</s>
<s>
Spinoza	Spinoza	k1gFnSc1	Spinoza
byl	být	k5eAaImAgMnS	být
krajní	krajní	k2eAgMnSc1d1	krajní
racionalista	racionalista	k1gMnSc1	racionalista
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
forma	forma	k1gFnSc1	forma
většiny	většina	k1gFnSc2	většina
jeho	jeho	k3xOp3gInPc2	jeho
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
inspirovaných	inspirovaný	k2eAgNnPc2d1	inspirované
postupem	postupem	k7c2	postupem
Eukleidovy	Eukleidův	k2eAgFnSc2d1	Eukleidova
Geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
vždy	vždy	k6eAd1	vždy
definicemi	definice	k1gFnPc7	definice
a	a	k8xC	a
axiomy	axiom	k1gInPc7	axiom
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
věty	věta	k1gFnPc1	věta
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
důsledky	důsledek	k1gInPc4	důsledek
a	a	k8xC	a
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Spinoza	Spinoza	k1gFnSc1	Spinoza
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Descarta	Descart	k1gMnSc2	Descart
<g/>
,	,	kIx,	,
dovedl	dovést	k5eAaPmAgInS	dovést
však	však	k9	však
jeho	jeho	k3xOp3gInSc1	jeho
systém	systém	k1gInSc1	systém
do	do	k7c2	do
důsledků	důsledek	k1gInPc2	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
nekonečnost	nekonečnost	k1gFnSc1	nekonečnost
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
něhož	jenž	k3xRgNnSc2	jenž
nemůže	moct	k5eNaImIp3nS	moct
už	už	k6eAd1	už
nic	nic	k3yNnSc4	nic
samostatného	samostatný	k2eAgMnSc4d1	samostatný
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Descarta	Descart	k1gMnSc2	Descart
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgMnSc4	jenž
Bůh	bůh	k1gMnSc1	bůh
jako	jako	k8xS	jako
stvořitel	stvořitel	k1gMnSc1	stvořitel
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
ho	on	k3xPp3gMnSc4	on
Spinoza	Spinoz	k1gMnSc4	Spinoz
zahrnout	zahrnout	k5eAaPmF	zahrnout
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
nějž	jenž	k3xRgMnSc4	jenž
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
s	s	k7c7	s
Descartem	Descart	k1gMnSc7	Descart
rozuměl	rozumět	k5eAaImAgMnS	rozumět
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc6	svůj
existenci	existence	k1gFnSc6	existence
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
co	co	k9	co
na	na	k7c6	na
ničem	nic	k3yNnSc6	nic
jiném	jiný	k2eAgNnSc6d1	jiné
nezávisí	záviset	k5eNaImIp3nS	záviset
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
věčná	věčný	k2eAgFnSc1d1	věčná
a	a	k8xC	a
božská	božský	k2eAgFnSc1d1	božská
<g/>
.	.	kIx.	.
</s>
<s>
Substance	substance	k1gFnSc1	substance
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nepodmíněný	podmíněný	k2eNgInSc1d1	nepodmíněný
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
nemůže	moct	k5eNaImIp3nS	moct
nebýt	být	k5eNaImF	být
a	a	k8xC	a
co	co	k3yRnSc1	co
"	"	kIx"	"
<g/>
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
základu	základ	k1gInSc6	základ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nějakou	nějaký	k3yIgFnSc4	nějaký
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
jednotlivou	jednotlivý	k2eAgFnSc4d1	jednotlivá
neboli	neboli	k8xC	neboli
konečnou	konečný	k2eAgFnSc7d1	konečná
věcí	věc	k1gFnSc7	věc
(	(	kIx(	(
<g/>
omnis	omnis	k1gFnSc1	omnis
determinatio	determinatio	k1gMnSc1	determinatio
negatio	negatio	k1gMnSc1	negatio
est	est	k?	est
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsoucna	jsoucno	k1gNnPc1	jsoucno
neboli	neboli	k8xC	neboli
věci	věc	k1gFnPc1	věc
jsou	být	k5eAaImIp3nP	být
jen	jen	k6eAd1	jen
mody	modus	k1gInPc4	modus
jediné	jediný	k2eAgFnSc2d1	jediná
substance	substance	k1gFnSc2	substance
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
neměnná	měnný	k2eNgNnPc1d1	neměnné
<g/>
,	,	kIx,	,
nedělitelná	dělitelný	k2eNgNnPc1d1	nedělitelné
<g/>
,	,	kIx,	,
nekonečná	konečný	k2eNgNnPc1d1	nekonečné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
substance	substance	k1gFnSc1	substance
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
zdůvodnění	zdůvodnění	k1gNnSc4	zdůvodnění
mimo	mimo	k7c4	mimo
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
pokud	pokud	k8xS	pokud
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zdůvodněna	zdůvodnit	k5eAaPmNgFnS	zdůvodnit
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
sama	sám	k3xTgFnSc1	sám
faktem	fakt	k1gInSc7	fakt
své	svůj	k3xOyFgFnSc2	svůj
jednosti	jednost	k1gFnSc2	jednost
a	a	k8xC	a
nutnosti	nutnost	k1gFnSc2	nutnost
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nemůže	moct	k5eNaImIp3nS	moct
nebýt	být	k5eNaImF	být
<g/>
)	)	kIx)	)
své	svůj	k3xOyFgNnSc4	svůj
zdůvodnění	zdůvodnění	k1gNnSc4	zdůvodnění
neboli	neboli	k8xC	neboli
je	být	k5eAaImIp3nS	být
causa	causa	k1gFnSc1	causa
sui	sui	k?	sui
(	(	kIx(	(
<g/>
příčinou	příčina	k1gFnSc7	příčina
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
Spinoza	Spinoza	k1gFnSc1	Spinoza
volí	volit	k5eAaImIp3nS	volit
označení	označení	k1gNnSc4	označení
Deus	Deusa	k1gFnPc2	Deusa
-	-	kIx~	-
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
předpokladem	předpoklad	k1gInSc7	předpoklad
a	a	k8xC	a
základem	základ	k1gInSc7	základ
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
jest	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
čemkoliv	cokoliv	k3yInSc6	cokoliv
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Substance	substance	k1gFnSc1	substance
neznamená	znamenat	k5eNaImIp3nS	znamenat
žádnou	žádný	k3yNgFnSc4	žádný
látku	látka	k1gFnSc4	látka
ani	ani	k8xC	ani
poznatelnou	poznatelný	k2eAgFnSc4d1	poznatelná
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
především	především	k9	především
myšlenkovou	myšlenkový	k2eAgFnSc4d1	myšlenková
kategorii	kategorie	k1gFnSc4	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečná	konečný	k2eNgFnSc1d1	nekonečná
substance	substance	k1gFnSc1	substance
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
atributů	atribut	k1gInPc2	atribut
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
může	moct	k5eAaImIp3nS	moct
lidský	lidský	k2eAgInSc4d1	lidský
rozum	rozum	k1gInSc4	rozum
poznávat	poznávat	k5eAaImF	poznávat
jen	jen	k6eAd1	jen
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
ducha	duch	k1gMnSc4	duch
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
myslící	myslící	k2eAgFnSc1d1	myslící
věc	věc	k1gFnSc1	věc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
res	res	k?	res
cogitans	cogitans	k1gInSc1	cogitans
<g/>
)	)	kIx)	)
a	a	k8xC	a
látku	látka	k1gFnSc4	látka
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
rozlehlou	rozlehlý	k2eAgFnSc4d1	rozlehlá
věc	věc	k1gFnSc4	věc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
res	res	k?	res
extensa	extensa	k1gFnSc1	extensa
<g/>
)	)	kIx)	)
-	-	kIx~	-
Descartovy	Descartův	k2eAgFnSc2d1	Descartova
substance	substance	k1gFnSc2	substance
<g/>
.	.	kIx.	.
</s>
<s>
Rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
je	být	k5eAaImIp3nS	být
identická	identický	k2eAgFnSc1d1	identická
s	s	k7c7	s
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
duch	duch	k1gMnSc1	duch
představuje	představovat	k5eAaImIp3nS	představovat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
aktivitu	aktivita	k1gFnSc4	aktivita
mysli	mysl	k1gFnSc2	mysl
od	od	k7c2	od
smyslového	smyslový	k2eAgNnSc2d1	smyslové
vnímání	vnímání	k1gNnSc2	vnímání
přes	přes	k7c4	přes
imaginaci	imaginace	k1gFnSc4	imaginace
<g/>
,	,	kIx,	,
afekty	afekt	k1gInPc4	afekt
až	až	k9	až
po	po	k7c4	po
ideje	idea	k1gFnPc4	idea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
atributy	atribut	k1gInPc1	atribut
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
všechny	všechen	k3xTgFnPc4	všechen
poznatelné	poznatelný	k2eAgFnPc4d1	poznatelná
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
Vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
v	v	k7c6	v
Bohu	bůh	k1gMnSc6	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
Spinoza	Spinoza	k1gFnSc1	Spinoza
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
"	"	kIx"	"
<g/>
natura	natura	k1gFnSc1	natura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
nasci	nasce	k1gFnSc4	nasce
<g/>
,	,	kIx,	,
rodit	rodit	k5eAaImF	rodit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nelze	lze	k6eNd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
moderním	moderní	k2eAgNnSc7d1	moderní
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
příroda	příroda	k1gFnSc1	příroda
<g/>
"	"	kIx"	"
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
spíše	spíše	k9	spíše
něco	něco	k3yInSc1	něco
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
povaha	povaha	k1gFnSc1	povaha
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
podstata	podstata	k1gFnSc1	podstata
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
všechno	všechen	k3xTgNnSc1	všechen
zahrnuto	zahrnut	k2eAgNnSc1d1	zahrnuto
v	v	k7c6	v
jediném	jediné	k1gNnSc6	jediné
nekonečném	konečný	k2eNgNnSc6d1	nekonečné
<g/>
,	,	kIx,	,
věčném	věčné	k1gNnSc6	věčné
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
nutném	nutný	k2eAgInSc6d1	nutný
celku	celek	k1gInSc6	celek
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Bohem	bůh	k1gMnSc7	bůh
jako	jako	k8xS	jako
stvořitelem	stvořitel	k1gMnSc7	stvořitel
a	a	k8xC	a
stvořeným	stvořený	k2eAgInSc7d1	stvořený
světem	svět	k1gInSc7	svět
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
zásadní	zásadní	k2eAgInSc1d1	zásadní
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Spinoza	Spinoza	k1gFnSc1	Spinoza
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
rozlišením	rozlišení	k1gNnSc7	rozlišení
mezi	mezi	k7c4	mezi
natura	natur	k1gMnSc4	natur
naturans	naturans	k6eAd1	naturans
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgFnSc1d1	tvořící
podstata	podstata	k1gFnSc1	podstata
<g/>
)	)	kIx)	)
a	a	k8xC	a
natura	natura	k1gFnSc1	natura
naturata	naturata	k1gFnSc1	naturata
(	(	kIx(	(
<g/>
stvořená	stvořený	k2eAgFnSc1d1	stvořená
<g/>
,	,	kIx,	,
zrozená	zrozený	k2eAgFnSc1d1	zrozená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Spinozův	Spinozův	k2eAgInSc1d1	Spinozův
metafyzický	metafyzický	k2eAgInSc1d1	metafyzický
systém	systém	k1gInSc1	systém
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
panteismu	panteismus	k1gInSc2	panteismus
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
užívají	užívat	k5eAaImIp3nP	užívat
název	název	k1gInSc4	název
panenteismus	panenteismus	k1gInSc1	panenteismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
pan-en-theó	pannheó	k?	pan-en-theó
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
Bohu	bůh	k1gMnSc6	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Spinoza	Spinoz	k1gMnSc4	Spinoz
výslovně	výslovně	k6eAd1	výslovně
upíral	upírat	k5eAaImAgMnS	upírat
Bohu	bůh	k1gMnSc3	bůh
osobní	osobní	k2eAgFnSc4d1	osobní
a	a	k8xC	a
lidské	lidský	k2eAgInPc4d1	lidský
rysy	rys	k1gInPc4	rys
nebo	nebo	k8xC	nebo
činnosti	činnost	k1gFnPc4	činnost
(	(	kIx(	(
<g/>
vidění	vidění	k1gNnSc4	vidění
<g/>
,	,	kIx,	,
chtění	chtění	k1gNnSc4	chtění
<g/>
,	,	kIx,	,
city	cit	k1gInPc7	cit
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
-	-	kIx~	-
jak	jak	k6eAd1	jak
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
-	-	kIx~	-
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
bránit	bránit	k5eAaImF	bránit
naivním	naivní	k2eAgFnPc3d1	naivní
lidským	lidský	k2eAgFnPc3d1	lidská
představám	představa	k1gFnPc3	představa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
Boha	bůh	k1gMnSc4	bůh
představují	představovat	k5eAaImIp3nP	představovat
jako	jako	k8xS	jako
člověka	člověk	k1gMnSc2	člověk
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc1d1	různá
interpretace	interpretace	k1gFnPc1	interpretace
Spinozovy	Spinozův	k2eAgFnSc2d1	Spinozova
metafyziky	metafyzika	k1gFnSc2	metafyzika
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Tvrdý	Tvrdý	k1gMnSc1	Tvrdý
i	i	k8xC	i
profesor	profesor	k1gMnSc1	profesor
Drtina	drtina	k1gFnSc1	drtina
považovali	považovat	k5eAaImAgMnP	považovat
Spinozu	Spinoz	k1gInSc2	Spinoz
za	za	k7c4	za
klasického	klasický	k2eAgMnSc4d1	klasický
představitele	představitel	k1gMnSc4	představitel
panteismu	panteismus	k1gInSc2	panteismus
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
profesor	profesor	k1gMnSc1	profesor
Rast	Rast	k?	Rast
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Spinoza	Spinoza	k1gFnSc1	Spinoza
učí	učit	k5eAaImIp3nS	učit
jednoznačnému	jednoznačný	k2eAgInSc3d1	jednoznačný
panteismu	panteismus	k1gInSc3	panteismus
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
natura	natura	k1gFnSc1	natura
naturans	naturansa	k1gFnPc2	naturansa
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
příroda	příroda	k1gFnSc1	příroda
tvořící	tvořící	k2eAgFnSc1d1	tvořící
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
mody	modus	k1gInPc1	modus
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
natura	natura	k1gFnSc1	natura
naturata	naturata	k1gFnSc1	naturata
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
příroda	příroda	k1gFnSc1	příroda
vytvářená	vytvářený	k2eAgFnSc1d1	vytvářená
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
základě	základ	k1gInSc6	základ
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dle	dle	k7c2	dle
profesora	profesor	k1gMnSc2	profesor
Röda	Rödus	k1gMnSc2	Rödus
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
Příroda	příroda	k1gFnSc1	příroda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
Spinozy	Spinoz	k1gInPc1	Spinoz
synonyma	synonymum	k1gNnSc2	synonymum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ovšem	ovšem	k9	ovšem
"	"	kIx"	"
<g/>
přírodou	příroda	k1gFnSc7	příroda
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
jen	jen	k6eAd1	jen
materiální	materiální	k2eAgFnSc4d1	materiální
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečnost	skutečnost	k1gFnSc1	skutečnost
obecně	obecně	k6eAd1	obecně
včetně	včetně	k7c2	včetně
jejího	její	k3xOp3gInSc2	její
duchovního	duchovní	k2eAgInSc2d1	duchovní
aspektu	aspekt	k1gInSc2	aspekt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Spinozových	Spinozův	k2eAgInPc2d1	Spinozův
předpokladů	předpoklad	k1gInPc2	předpoklad
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nelze	lze	k6eNd1	lze
vážně	vážně	k6eAd1	vážně
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Bůh	bůh	k1gMnSc1	bůh
stvořil	stvořit	k5eAaPmAgMnS	stvořit
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bohu	bůh	k1gMnSc6	bůh
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
ve	v	k7c6	v
všem	všecek	k3xTgInSc6	všecek
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
vzniku	vznik	k1gInSc2	vznik
věcí	věc	k1gFnPc2	věc
statický	statický	k2eAgInSc4d1	statický
panteismus	panteismus	k1gInSc4	panteismus
Spinozův	Spinozův	k2eAgInSc4d1	Spinozův
jednoduše	jednoduše	k6eAd1	jednoduše
pomíjí	pomíjet	k5eAaImIp3nS	pomíjet
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Muller	Muller	k1gMnSc1	Muller
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Spinozův	Spinozův	k2eAgMnSc1d1	Spinozův
Bůh	bůh	k1gMnSc1	bůh
venkoncem	venkoncem	k?	venkoncem
není	být	k5eNaImIp3nS	být
transcendentní	transcendentní	k2eAgFnSc1d1	transcendentní
a	a	k8xC	a
"	"	kIx"	"
<g/>
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
příroda	příroda	k1gFnSc1	příroda
sama	sám	k3xTgFnSc1	sám
<g/>
..	..	k?	..
<g/>
/	/	kIx~	/
<g/>
...	...	k?	...
<g/>
/	/	kIx~	/
Jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
příroda	příroda	k1gFnSc1	příroda
neboli	neboli	k8xC	neboli
Bůh	bůh	k1gMnSc1	bůh
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pochopena	pochopit	k5eAaPmNgFnS	pochopit
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
Bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
natura	natura	k1gFnSc1	natura
naturans	naturansa	k1gFnPc2	naturansa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
jako	jako	k9	jako
nekonečný	konečný	k2eNgInSc1d1	nekonečný
řád	řád	k1gInSc1	řád
konečných	konečný	k2eAgFnPc2d1	konečná
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
jako	jako	k9	jako
"	"	kIx"	"
<g/>
natura	natura	k1gFnSc1	natura
naturata	naturata	k1gFnSc1	naturata
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spinozově	Spinozův	k2eAgNnSc6d1	Spinozovo
myšlení	myšlení	k1gNnSc6	myšlení
neexistuje	existovat	k5eNaImIp3nS	existovat
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
osobního	osobní	k2eAgMnSc4d1	osobní
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
náboženské	náboženský	k2eAgNnSc4d1	náboženské
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
o	o	k7c6	o
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
duše	duše	k1gFnSc2	duše
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
iluzi	iluze	k1gFnSc4	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
různých	různý	k2eAgFnPc2d1	různá
interpretací	interpretace	k1gFnPc2	interpretace
Spinozových	Spinozův	k2eAgInPc2d1	Spinozův
názorů	názor	k1gInPc2	názor
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
profesor	profesor	k1gMnSc1	profesor
Kenny	Kenna	k1gFnSc2	Kenna
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Klíčem	klíč	k1gInSc7	klíč
ke	k	k7c3	k
Spinozově	Spinozův	k2eAgFnSc3d1	Spinozova
filozofii	filozofie	k1gFnSc3	filozofie
je	být	k5eAaImIp3nS	být
monismus	monismus	k1gInSc1	monismus
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
jediná	jediný	k2eAgFnSc1d1	jediná
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
božská	božský	k2eAgFnSc1d1	božská
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
identická	identický	k2eAgFnSc1d1	identická
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
:	:	kIx,	:
Deus	Deus	k1gInSc1	Deus
sive	siv	k1gMnSc2	siv
Natura	Natur	k1gMnSc2	Natur
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
čili	čili	k8xC	čili
příroda	příroda	k1gFnSc1	příroda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
ztotožnění	ztotožnění	k1gNnSc1	ztotožnění
Boha	bůh	k1gMnSc2	bůh
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
lze	lze	k6eAd1	lze
pojímat	pojímat	k5eAaImF	pojímat
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
chápe	chápat	k5eAaImIp3nS	chápat
Boha	bůh	k1gMnSc4	bůh
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
systému	systém	k1gInSc6	systém
jako	jako	k8xS	jako
prostě	prostě	k6eAd1	prostě
kódovaný	kódovaný	k2eAgInSc4d1	kódovaný
způsob	způsob	k1gInSc4	způsob
odkazující	odkazující	k2eAgInSc4d1	odkazující
k	k	k7c3	k
řádu	řád	k1gInSc3	řád
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
přirozeném	přirozený	k2eAgInSc6d1	přirozený
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
Spinoza	Spinoza	k1gFnSc1	Spinoza
jevit	jevit	k5eAaImF	jevit
jako	jako	k9	jako
nefalšovaný	falšovaný	k2eNgMnSc1d1	nefalšovaný
ateista	ateista	k1gMnSc1	ateista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Materialistické	materialistický	k2eAgNnSc1d1	materialistické
pojetí	pojetí	k1gNnSc1	pojetí
Spinozova	Spinozův	k2eAgInSc2d1	Spinozův
monismu	monismus	k1gInSc2	monismus
zastávali	zastávat	k5eAaImAgMnP	zastávat
marxističtí	marxistický	k2eAgMnPc1d1	marxistický
badatelé	badatel	k1gMnPc1	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Nábožensky	nábožensky	k6eAd1	nábožensky
založení	založený	k2eAgMnPc1d1	založený
myslitelé	myslitel	k1gMnPc1	myslitel
Spinozův	Spinozův	k2eAgInSc4d1	Spinozův
monismus	monismus	k1gInSc4	monismus
a	a	k8xC	a
panteismus	panteismus	k1gInSc4	panteismus
většinou	většinou	k6eAd1	většinou
odmítali	odmítat	k5eAaImAgMnP	odmítat
<g/>
;	;	kIx,	;
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
ke	k	k7c3	k
kritikům	kritik	k1gMnPc3	kritik
Spinozovy	Spinozův	k2eAgFnSc2d1	Spinozova
metafyziky	metafyzika	k1gFnSc2	metafyzika
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
katolický	katolický	k2eAgMnSc1d1	katolický
učenec	učenec	k1gMnSc1	učenec
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
Emanuel	Emanuel	k1gMnSc1	Emanuel
Rádl	Rádl	k1gMnSc1	Rádl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
i	i	k9	i
pochybnost	pochybnost	k1gFnSc4	pochybnost
o	o	k7c6	o
Spinozově	Spinozův	k2eAgFnSc6d1	Spinozova
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
nějakého	nějaký	k3yIgMnSc4	nějaký
Boha	bůh	k1gMnSc4	bůh
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Víra	víra	k1gFnSc1	víra
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
naprosto	naprosto	k6eAd1	naprosto
scházela	scházet	k5eAaImAgFnS	scházet
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
lidi	člověk	k1gMnPc4	člověk
i	i	k8xC	i
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
;	;	kIx,	;
že	že	k8xS	že
užíval	užívat	k5eAaImAgInS	užívat
často	často	k6eAd1	často
slova	slovo	k1gNnPc1	slovo
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
<s>
Motivem	motiv	k1gInSc7	motiv
Spinozova	Spinozův	k2eAgNnSc2d1	Spinozovo
filosofování	filosofování	k1gNnSc2	filosofování
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
praktické	praktický	k2eAgFnPc1d1	praktická
otázky	otázka	k1gFnPc1	otázka
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
svoboda	svoboda	k1gFnSc1	svoboda
mysli	mysl	k1gFnSc2	mysl
a	a	k8xC	a
antiutopistické	antiutopistický	k2eAgNnSc1d1	antiutopistický
uspořádání	uspořádání	k1gNnSc1	uspořádání
společnosti	společnost	k1gFnSc2	společnost
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
plném	plný	k2eAgInSc6d1	plný
občanských	občanský	k2eAgFnPc2d1	občanská
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
zvlášť	zvlášť	k6eAd1	zvlášť
naléhavé	naléhavý	k2eAgFnPc4d1	naléhavá
<g/>
.	.	kIx.	.
</s>
<s>
Spinoza	Spinoza	k1gFnSc1	Spinoza
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
Hobbese	Hobbesa	k1gFnSc6	Hobbesa
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
konceptu	koncept	k1gInSc2	koncept
společenské	společenský	k2eAgFnSc2d1	společenská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lidé	člověk	k1gMnPc1	člověk
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vymanili	vymanit	k5eAaPmAgMnP	vymanit
z	z	k7c2	z
přirozeného	přirozený	k2eAgInSc2d1	přirozený
stavu	stav	k1gInSc2	stav
pouhé	pouhý	k2eAgFnSc2d1	pouhá
snahy	snaha	k1gFnSc2	snaha
zachovat	zachovat	k5eAaPmF	zachovat
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
existenci	existence	k1gFnSc4	existence
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgMnSc1	každý
svému	svůj	k3xOyFgInSc3	svůj
druhu	druh	k1gInSc3	druh
zákonitě	zákonitě	k6eAd1	zákonitě
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
důsledný	důsledný	k2eAgMnSc1d1	důsledný
racionalista	racionalista	k1gMnSc1	racionalista
byl	být	k5eAaImAgMnS	být
Spinoza	Spinoz	k1gMnSc4	Spinoz
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
(	(	kIx(	(
<g/>
přírodními	přírodní	k2eAgFnPc7d1	přírodní
<g/>
)	)	kIx)	)
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
že	že	k8xS	že
mezi	mezi	k7c7	mezi
zákony	zákon	k1gInPc7	zákon
hmotného	hmotný	k2eAgInSc2d1	hmotný
a	a	k8xC	a
duchovního	duchovní	k2eAgInSc2d1	duchovní
či	či	k8xC	či
mravního	mravní	k2eAgInSc2d1	mravní
světa	svět	k1gInSc2	svět
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
žádný	žádný	k3yNgInSc4	žádný
rozpor	rozpor	k1gInSc4	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pojem	pojem	k1gInSc1	pojem
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
tím	ten	k3xDgNnSc7	ten
nabývá	nabývat	k5eAaImIp3nS	nabývat
významu	význam	k1gInSc3	význam
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
přirozeném	přirozený	k2eAgInSc6d1	přirozený
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc4	všechen
myslitelné	myslitelný	k2eAgNnSc4d1	myslitelné
povoleno	povolen	k2eAgNnSc4d1	povoleno
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
lidský	lidský	k2eAgInSc1d1	lidský
život	život	k1gInSc1	život
a	a	k8xC	a
mezilidské	mezilidský	k2eAgInPc1d1	mezilidský
vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
určovány	určovat	k5eAaImNgInP	určovat
různými	různý	k2eAgFnPc7d1	různá
nutnostmi	nutnost	k1gFnPc7	nutnost
a	a	k8xC	a
moudrý	moudrý	k2eAgMnSc1d1	moudrý
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dopracovat	dopracovat	k5eAaPmF	dopracovat
ke	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
a	a	k8xC	a
aktivnímu	aktivní	k2eAgInSc3d1	aktivní
životu	život	k1gInSc3	život
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
uzná	uznat	k5eAaPmIp3nS	uznat
a	a	k8xC	a
přijme	přijmout	k5eAaPmIp3nS	přijmout
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
ovládat	ovládat	k5eAaImF	ovládat
své	svůj	k3xOyFgFnPc4	svůj
vášně	vášeň	k1gFnPc4	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Spinozova	Spinozův	k2eAgFnSc1d1	Spinozova
etika	etika	k1gFnSc1	etika
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
etice	etika	k1gFnSc3	etika
stoické	stoický	k2eAgInPc4d1	stoický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
však	však	k9	však
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
rozumnost	rozumnost	k1gFnSc1	rozumnost
každého	každý	k3xTgMnSc2	každý
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
výslovně	výslovně	k6eAd1	výslovně
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
sledování	sledování	k1gNnSc4	sledování
vlastních	vlastní	k2eAgInPc2d1	vlastní
zájmů	zájem	k1gInPc2	zájem
<g/>
:	:	kIx,	:
rozumný	rozumný	k2eAgMnSc1d1	rozumný
člověk	člověk	k1gMnSc1	člověk
sám	sám	k3xTgMnSc1	sám
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sobectví	sobectví	k1gNnSc1	sobectví
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
druhých	druhý	k4xOgMnPc2	druhý
nikdy	nikdy	k6eAd1	nikdy
neobejde	obejde	k6eNd1	obejde
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Spinozu	Spinoz	k1gInSc2	Spinoz
základem	základ	k1gInSc7	základ
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Harmonické	harmonický	k2eAgNnSc1d1	harmonické
spolužití	spolužití	k1gNnSc1	spolužití
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
nashromáždění	nashromáždění	k1gNnSc2	nashromáždění
moci	moc	k1gFnSc2	moc
každého	každý	k3xTgMnSc4	každý
člena	člen	k1gMnSc4	člen
společnosti	společnost	k1gFnSc2	společnost
do	do	k7c2	do
moci	moc	k1gFnSc2	moc
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
přenesena	přenést	k5eAaPmNgFnS	přenést
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
nejúčinnějšího	účinný	k2eAgNnSc2d3	nejúčinnější
prosazování	prosazování	k1gNnSc2	prosazování
řádu	řád	k1gInSc2	řád
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
stability	stabilita	k1gFnSc2	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytným	zbytný	k2eNgInSc7d1	zbytný
rozumovým	rozumový	k2eAgInSc7d1	rozumový
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
maximální	maximální	k2eAgNnSc4d1	maximální
zachování	zachování	k1gNnSc4	zachování
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
cíle	cíl	k1gInPc1	cíl
jako	jako	k8xC	jako
prospěšnost	prospěšnost	k1gFnSc1	prospěšnost
a	a	k8xC	a
minimální	minimální	k2eAgInSc1d1	minimální
zásah	zásah	k1gInSc1	zásah
státu	stát	k1gInSc2	stát
do	do	k7c2	do
života	život	k1gInSc2	život
občana	občan	k1gMnSc2	občan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
svoboda	svoboda	k1gFnSc1	svoboda
mysli	mysl	k1gFnSc2	mysl
a	a	k8xC	a
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druh	druh	k1gInSc4	druh
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
Spinoza	Spinoza	k1gFnSc1	Spinoza
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
demokracii	demokracie	k1gFnSc3	demokracie
před	před	k7c7	před
aristokracií	aristokracie	k1gFnSc7	aristokracie
a	a	k8xC	a
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rozhodování	rozhodování	k1gNnSc1	rozhodování
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
chráněno	chránit	k5eAaImNgNnS	chránit
před	před	k7c7	před
osobními	osobní	k2eAgInPc7d1	osobní
výstřelky	výstřelek	k1gInPc7	výstřelek
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
racionálnější	racionální	k2eAgInSc4d2	racionálnější
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
ovšem	ovšem	k9	ovšem
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
jiným	jiný	k2eAgFnPc3d1	jiná
restrikcím	restrikce	k1gFnPc3	restrikce
<g/>
,	,	kIx,	,
než	než	k8xS	než
vlastním	vlastní	k2eAgInPc3d1	vlastní
vydaným	vydaný	k2eAgInPc3d1	vydaný
zákonům	zákon	k1gInPc3	zákon
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jiné	jiný	k2eAgFnSc3d1	jiná
vyšší	vysoký	k2eAgFnSc3d2	vyšší
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mezním	mezní	k2eAgInSc6d1	mezní
případě	případ	k1gInSc6	případ
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
moc	moc	k1gFnSc4	moc
lidu	lid	k1gInSc2	lid
převrat	převrat	k1gInSc1	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
nebo	nebo	k8xC	nebo
vláda	vláda	k1gFnSc1	vláda
má	mít	k5eAaImIp3nS	mít
tudíž	tudíž	k8xC	tudíž
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
plnou	plný	k2eAgFnSc4d1	plná
autoritu	autorita	k1gFnSc4	autorita
nad	nad	k7c7	nad
svými	svůj	k3xOyFgMnPc7	svůj
poddanými	poddaný	k1gMnPc7	poddaný
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gInSc4	on
musí	muset	k5eAaImIp3nP	muset
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
poslouchat	poslouchat	k5eAaImF	poslouchat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nenařizuje	nařizovat	k5eNaImIp3nS	nařizovat
něco	něco	k3yInSc4	něco
vyloženě	vyloženě	k6eAd1	vyloženě
zvrhlého	zvrhlý	k2eAgNnSc2d1	zvrhlé
jako	jako	k8xC	jako
zabít	zabít	k5eAaPmF	zabít
vlastní	vlastní	k2eAgMnPc4d1	vlastní
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
panovníka	panovník	k1gMnSc2	panovník
samého	samý	k3xTgNnSc2	samý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
svým	svůj	k3xOyFgMnPc3	svůj
poddaným	poddaný	k1gMnPc3	poddaný
nechal	nechat	k5eAaPmAgMnS	nechat
co	co	k9	co
největší	veliký	k2eAgFnSc4d3	veliký
svobodu	svoboda	k1gFnSc4	svoboda
názoru	názor	k1gInSc2	názor
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
a	a	k8xC	a
nevytvářel	vytvářet	k5eNaImAgMnS	vytvářet
tak	tak	k6eAd1	tak
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
zbytečné	zbytečný	k2eAgInPc4d1	zbytečný
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
Spinozovy	Spinozův	k2eAgInPc1d1	Spinozův
etické	etický	k2eAgInPc1d1	etický
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jistém	jistý	k2eAgNnSc6d1	jisté
napětí	napětí	k1gNnSc6	napětí
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
determinismem	determinismus	k1gInSc7	determinismus
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
politickou	politický	k2eAgFnSc4d1	politická
filosofii	filosofie	k1gFnSc4	filosofie
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
pevné	pevný	k2eAgNnSc4d1	pevné
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
právních	právní	k2eAgFnPc2d1	právní
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
Spinozův	Spinozův	k2eAgInSc1d1	Spinozův
vliv	vliv	k1gInSc1	vliv
omezil	omezit	k5eAaPmAgInS	omezit
na	na	k7c4	na
malý	malý	k2eAgInSc4d1	malý
okruh	okruh	k1gInSc4	okruh
znalců	znalec	k1gMnPc2	znalec
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
například	například	k6eAd1	například
Leibnizovu	Leibnizův	k2eAgFnSc4d1	Leibnizova
"	"	kIx"	"
<g/>
Monadologii	Monadologie	k1gFnSc4	Monadologie
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Leibniz	Leibniz	k1gInSc1	Leibniz
ale	ale	k8xC	ale
také	také	k9	také
napsal	napsat	k5eAaBmAgMnS	napsat
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
Spinozovy	Spinozův	k2eAgFnSc2d1	Spinozova
etiky	etika	k1gFnSc2	etika
<g/>
.	.	kIx.	.
</s>
<s>
Osvícenec	osvícenec	k1gMnSc1	osvícenec
Pierre	Pierr	k1gInSc5	Pierr
Bayle	Bayle	k1gNnPc6	Bayle
považoval	považovat	k5eAaImAgMnS	považovat
Spinozovu	Spinozův	k2eAgFnSc4d1	Spinozova
filosofii	filosofie	k1gFnSc4	filosofie
za	za	k7c7	za
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
nejabsurdnější	absurdní	k2eAgNnSc1d3	nejabsurdnější
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
vymyslet	vymyslet	k5eAaPmF	vymyslet
<g/>
"	"	kIx"	"
a	a	k8xC	a
David	David	k1gMnSc1	David
Hume	Hume	k1gNnSc4	Hume
za	za	k7c4	za
"	"	kIx"	"
<g/>
ohavnou	ohavný	k2eAgFnSc4d1	ohavná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
lidské	lidský	k2eAgNnSc1d1	lidské
já	já	k3xPp1nSc1	já
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
podoba	podoba	k1gFnSc1	podoba
ontologického	ontologický	k2eAgInSc2d1	ontologický
důkazu	důkaz	k1gInSc2	důkaz
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Kant	Kant	k1gMnSc1	Kant
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
právě	právě	k9	právě
ze	z	k7c2	z
Spinozy	Spinoz	k1gInPc5	Spinoz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
vlnu	vlna	k1gFnSc4	vlna
širšího	široký	k2eAgInSc2d2	širší
zájmu	zájem	k1gInSc2	zájem
o	o	k7c6	o
Spinozu	Spinoz	k1gInSc6	Spinoz
ale	ale	k8xC	ale
zahájil	zahájit	k5eAaPmAgMnS	zahájit
teprve	teprve	k6eAd1	teprve
Friedrich	Friedrich	k1gMnSc1	Friedrich
Heinrich	Heinrich	k1gMnSc1	Heinrich
Jacobi	Jacobe	k1gFnSc4	Jacobe
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ho	on	k3xPp3gMnSc4	on
obvinil	obvinit	k5eAaPmAgMnS	obvinit
z	z	k7c2	z
panteismu	panteismus	k1gInSc2	panteismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
se	se	k3xPyFc4	se
se	s	k7c7	s
Spinozou	Spinoza	k1gFnSc7	Spinoza
seznámil	seznámit	k5eAaPmAgMnS	seznámit
například	například	k6eAd1	například
Lessing	Lessing	k1gInSc4	Lessing
<g/>
,	,	kIx,	,
Herder	Herder	k1gInSc4	Herder
a	a	k8xC	a
Goethe	Goeth	k1gMnPc4	Goeth
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jím	on	k3xPp3gMnSc7	on
byli	být	k5eAaImAgMnP	být
nadšeni	nadšen	k2eAgMnPc1d1	nadšen
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Fichte	Ficht	k1gInSc5	Ficht
<g/>
,	,	kIx,	,
Schleiermacher	Schleiermachra	k1gFnPc2	Schleiermachra
nebo	nebo	k8xC	nebo
Schelling	Schelling	k1gInSc1	Schelling
<g/>
.	.	kIx.	.
</s>
<s>
Hegel	Hegel	k1gMnSc1	Hegel
dokonce	dokonce	k9	dokonce
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spinoza	Spinoza	k1gFnSc1	Spinoza
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
možný	možný	k2eAgInSc4d1	možný
počátek	počátek	k1gInSc4	počátek
filosofického	filosofický	k2eAgNnSc2d1	filosofické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
nadšeně	nadšeně	k6eAd1	nadšeně
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
píše	psát	k5eAaImIp3nS	psát
Feuerbach	Feuerbach	k1gMnSc1	Feuerbach
a	a	k8xC	a
Nietzsche	Nietzsch	k1gMnSc4	Nietzsch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
Spinozu	Spinoz	k1gInSc6	Spinoz
odvolávali	odvolávat	k5eAaImAgMnP	odvolávat
například	například	k6eAd1	například
Ludwig	Ludwig	k1gMnSc1	Ludwig
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
Spinozou	Spinoza	k1gFnSc7	Spinoza
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
spise	spis	k1gInSc6	spis
<g/>
,	,	kIx,	,
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
"	"	kIx"	"
<g/>
Tractatus	Tractatus	k1gMnSc1	Tractatus
logico-philosophicus	logicohilosophicus	k1gMnSc1	logico-philosophicus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
myslitelé	myslitel	k1gMnPc1	myslitel
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
marxismem	marxismus	k1gInSc7	marxismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Louis	Louis	k1gMnSc1	Louis
Althusser	Althusser	k1gMnSc1	Althusser
<g/>
,	,	kIx,	,
Gilles	Gilles	k1gMnSc1	Gilles
Deleuze	Deleuze	k1gFnSc2	Deleuze
nebo	nebo	k8xC	nebo
Slavoj	Slavoj	k1gMnSc1	Slavoj
Žižek	Žižek	k1gMnSc1	Žižek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
politickou	politický	k2eAgFnSc4d1	politická
filosofii	filosofie	k1gFnSc4	filosofie
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
navázal	navázat	k5eAaPmAgMnS	navázat
Pavel	Pavel	k1gMnSc1	Pavel
Barša	Barša	k1gMnSc1	Barša
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
polemicky	polemicky	k6eAd1	polemicky
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Spinozovi	Spinoz	k1gMnSc3	Spinoz
postavil	postavit	k5eAaPmAgMnS	postavit
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Lévinas	Lévinas	k1gMnSc1	Lévinas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
viděl	vidět	k5eAaImAgInS	vidět
prototyp	prototyp	k1gInSc1	prototyp
totalizujícího	totalizující	k2eAgNnSc2d1	totalizující
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
myšlení	myšlení	k1gNnSc1	myšlení
Hegelovo	Hegelův	k2eAgNnSc1d1	Hegelovo
-	-	kIx~	-
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
Jiné	jiný	k2eAgNnSc1d1	jiné
Stejnému	stejné	k1gNnSc3	stejné
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
aspoň	aspoň	k9	aspoň
v	v	k7c6	v
myšlení	myšlení	k1gNnSc6	myšlení
"	"	kIx"	"
<g/>
uchopit	uchopit	k5eAaPmF	uchopit
<g/>
"	"	kIx"	"
celek	celek	k1gInSc4	celek
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzdáleně	vzdáleně	k6eAd1	vzdáleně
připravuje	připravovat	k5eAaImIp3nS	připravovat
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s>
politické	politický	k2eAgFnSc2d1	politická
totality	totalita	k1gFnSc2	totalita
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Krátké	Krátké	k2eAgNnSc1d1	Krátké
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
Bohu	bůh	k1gMnSc6	bůh
<g/>
,	,	kIx,	,
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
štěstí	štěstí	k1gNnSc6	štěstí
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tractatus	Tractatus	k1gMnSc1	Tractatus
de	de	k?	de
Deo	Deo	k1gMnSc1	Deo
et	et	k?	et
homine	homin	k1gMnSc5	homin
eiusque	eiusquus	k1gMnSc5	eiusquus
felicitate	felicitat	k1gMnSc5	felicitat
<g/>
,	,	kIx,	,
kolem	kolem	k6eAd1	kolem
1660	[number]	k4	1660
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
nápravě	náprava	k1gFnSc6	náprava
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tractatus	Tractatus	k1gMnSc1	Tractatus
de	de	k?	de
Intellectus	Intellectus	k1gMnSc1	Intellectus
Emendatione	Emendation	k1gInSc5	Emendation
1662	[number]	k4	1662
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Project	Project	k2eAgInSc1d1	Project
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
<g/>
;	;	kIx,	;
Pdf	Pdf	k1gFnSc1	Pdf
Version	Version	k1gInSc1	Version
"	"	kIx"	"
<g/>
Principy	princip	k1gInPc1	princip
Descartovské	Descartovský	k2eAgFnSc2d1	Descartovský
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Principia	principium	k1gNnPc1	principium
philosophiae	philosophia	k1gInSc2	philosophia
cartesianae	cartesiana	k1gInSc2	cartesiana
1663	[number]	k4	1663
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gallica	Gallica	k1gFnSc1	Gallica
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Teologicko-politický	teologickoolitický	k2eAgInSc1d1	teologicko-politický
traktát	traktát	k1gInSc1	traktát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tractatus	Tractatus	k1gMnSc1	Tractatus
Theologico-Politicus	Theologico-Politicus	k1gMnSc1	Theologico-Politicus
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgInS	vyjít
anonymně	anonymně	k6eAd1	anonymně
1670	[number]	k4	1670
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Project	Project	k2eAgInSc1d1	Project
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
<g/>
:	:	kIx,	:
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
;	;	kIx,	;
<g/>
Part	part	k1gInSc1	part
2	[number]	k4	2
<g/>
;	;	kIx,	;
<g/>
Part	part	k1gInSc1	part
3	[number]	k4	3
<g/>
;	;	kIx,	;
<g/>
Part	part	k1gInSc1	part
4	[number]	k4	4
<g/>
;	;	kIx,	;
Pdf	Pdf	k1gFnSc1	Pdf
Version	Version	k1gInSc1	Version
"	"	kIx"	"
<g/>
Kompendium	kompendium	k1gNnSc1	kompendium
gramatiky	gramatika	k1gFnSc2	gramatika
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Compendium	Compendium	k1gNnSc1	Compendium
grammatices	grammaticesa	k1gFnPc2	grammaticesa
linguae	linguaat	k5eAaPmIp3nS	linguaat
hebraeae	hebraeae	k6eAd1	hebraeae
1670	[number]	k4	1670
<g/>
-	-	kIx~	-
<g/>
1675	[number]	k4	1675
<g/>
,	,	kIx,	,
nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pdf	Pdf	k?	Pdf
Version	Version	k1gInSc1	Version
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Politický	politický	k2eAgInSc1d1	politický
traktát	traktát	k1gInSc1	traktát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tractatus	Tractatus	k1gInSc1	Tractatus
Politicus	Politicus	k1gInSc1	Politicus
1675	[number]	k4	1675
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
<g/>
)	)	kIx)	)
Pdf	Pdf	k1gFnSc1	Pdf
Version	Version	k1gInSc1	Version
"	"	kIx"	"
<g/>
Etika	etika	k1gFnSc1	etika
vyložená	vyložený	k2eAgFnSc1d1	vyložená
na	na	k7c4	na
geometrický	geometrický	k2eAgInSc4d1	geometrický
způsob	způsob	k1gInSc4	způsob
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ethica	Ethica	k1gFnSc1	Ethica
Ordine	Ordin	k1gInSc5	Ordin
Geometrico	Geometrico	k6eAd1	Geometrico
Demonstrata	Demonstrata	k1gFnSc1	Demonstrata
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Project	Project	k1gMnSc1	Project
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
překlad	překlad	k1gInSc1	překlad
Jonathana	Jonathan	k1gMnSc2	Jonathan
Bennetta	Bennett	k1gMnSc2	Bennett
<g/>
.	.	kIx.	.
;	;	kIx,	;
Pdf	Pdf	k1gFnSc1	Pdf
version	version	k1gInSc1	version
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Listy	list	k1gInPc1	list
<g/>
"	"	kIx"	"
Pdf	Pdf	k1gFnSc1	Pdf
Version	Version	k1gInSc1	Version
<g/>
.	.	kIx.	.
</s>
