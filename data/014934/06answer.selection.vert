<s desamb="1">
Pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
životě	život	k1gInSc6
Evy	Eva	k1gFnSc2
Perónové	Perónová	k1gFnSc2
<g/>
,	,	kIx,
manželky	manželka	k1gFnSc2
argentinského	argentinský	k2eAgMnSc2d1
diktátora	diktátor	k1gMnSc2
Juana	Juan	k1gMnSc2
Peróna	Perón	k1gMnSc2
<g/>
,	,	kIx,
od	od	k7c2
samých	samý	k3xTgInPc2
začátků	začátek	k1gInPc2
přes	přes	k7c4
dobu	doba	k1gFnSc4
její	její	k3xOp3gFnSc2
největší	veliký	k2eAgFnSc2d3
slávy	sláva	k1gFnSc2
až	až	k9
do	do	k7c2
její	její	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>