<s>
Evita	Evita	k1gFnSc1
(	(	kIx(
<g/>
muzikál	muzikál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Evita	Evita	k1gFnSc1
je	být	k5eAaImIp3nS
muzikál	muzikál	k1gInSc4
z	z	k7c2
pera	pero	k1gNnSc2
Andrew	Andrew	k1gFnSc2
Lloyd	Lloyd	k1gMnSc1
Webbera	Webbera	k1gFnSc1
a	a	k8xC
Tima	Tim	k2eAgFnSc1d1
Rice	Rice	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
životě	život	k1gInSc6
Evy	Eva	k1gFnSc2
Perónové	Perónová	k1gFnSc2
<g/>
,	,	kIx,
manželky	manželka	k1gFnSc2
argentinského	argentinský	k2eAgMnSc2d1
diktátora	diktátor	k1gMnSc2
Juana	Juan	k1gMnSc2
Peróna	Perón	k1gMnSc2
<g/>
,	,	kIx,
od	od	k7c2
samých	samý	k3xTgInPc2
začátků	začátek	k1gInPc2
přes	přes	k7c4
dobu	doba	k1gFnSc4
její	její	k3xOp3gFnSc2
největší	veliký	k2eAgFnSc2d3
slávy	sláva	k1gFnSc2
až	až	k9
do	do	k7c2
její	její	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
velkém	velký	k2eAgInSc6d1
úspěchu	úspěch	k1gInSc6
muzikálu	muzikál	k1gInSc2
Jesus	Jesus	k1gMnSc1
Christ	Christ	k1gMnSc1
Superstar	superstar	k1gFnSc2
se	se	k3xPyFc4
autorská	autorský	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
napsat	napsat	k5eAaBmF,k5eAaPmF
další	další	k2eAgInSc4d1
muzikál	muzikál	k1gInSc4
<g/>
,	,	kIx,
Evita	Evita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
kvůli	kvůli	k7c3
strachu	strach	k1gInSc3
z	z	k7c2
neúspěchu	neúspěch	k1gInSc2
se	se	k3xPyFc4
nejdříve	dříve	k6eAd3
rozhodli	rozhodnout	k5eAaPmAgMnP
vydat	vydat	k5eAaPmF
nahrávku	nahrávka	k1gFnSc4
kompletního	kompletní	k2eAgInSc2d1
muzikálu	muzikál	k1gInSc2
s	s	k7c7
Julií	Julie	k1gFnSc7
Covington	Covington	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CD	CD	kA
zaznamenalo	zaznamenat	k5eAaPmAgNnS
úspěch	úspěch	k1gInSc4
a	a	k8xC
proto	proto	k8xC
již	již	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1978	#num#	k4
měla	mít	k5eAaImAgFnS
Evita	Evita	k1gFnSc1
premiéru	premiéra	k1gFnSc4
v	v	k7c6
londýnském	londýnský	k2eAgNnSc6d1
West	West	k2eAgInSc1d1
Endu	Enda	k1gFnSc4
v	v	k7c6
Princ	princa	k1gFnPc2
Edward	Edward	k1gMnSc1
theatre	theatr	k1gInSc5
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
s	s	k7c7
Elaine	elain	k1gInSc5
Paige	Paige	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
následovala	následovat	k5eAaImAgFnS
Broadway	Broadwaa	k1gFnPc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
a	a	k8xC
spousta	spousta	k1gFnSc1
dalších	další	k2eAgNnPc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
se	se	k3xPyFc4
Evita	Evita	k1gFnSc1
dočkala	dočkat	k5eAaPmAgFnS
filmové	filmový	k2eAgFnPc4d1
verze	verze	k1gFnPc4
s	s	k7c7
Madonnou	Madonný	k2eAgFnSc7d1
a	a	k8xC
Antoniem	Antonio	k1gMnSc7
Banderasem	Banderas	k1gMnSc7
a	a	k8xC
byla	být	k5eAaImAgFnS
čtyřikrát	čtyřikrát	k6eAd1
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
ČR	ČR	kA
–	–	k?
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
Divadlo	divadlo	k1gNnSc1
Spirála	spirála	k1gFnSc1
1998	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
MdB	MdB	k1gFnSc1
2009	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
(	(	kIx(
<g/>
Divadlo	divadlo	k1gNnSc1
Josefa	Josef	k1gMnSc2
Kajetána	Kajetán	k1gMnSc4
Tyla	Tyl	k1gMnSc4
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
(	(	kIx(
<g/>
NDM	NDM	kA
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Jiřího	Jiří	k1gMnSc2
Myrona	Myron	k1gMnSc2
2014	#num#	k4
–	–	k?
stále	stále	k6eAd1
v	v	k7c6
repertoáru	repertoár	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Páté	pátý	k4xOgNnSc1
uvedení	uvedení	k1gNnPc2
se	se	k3xPyFc4
chystá	chystat	k5eAaImIp3nS
na	na	k7c4
únor	únor	k1gInSc4
2015	#num#	k4
v	v	k7c6
pražském	pražský	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
Studio	studio	k1gNnSc1
DVA	dva	k4xCgMnPc1
s	s	k7c7
Monikou	Monika	k1gFnSc7
Absolonovou	Absolonův	k2eAgFnSc7d1
v	v	k7c6
alternaci	alternace	k1gFnSc6
s	s	k7c7
Radkou	Radka	k1gFnSc7
Fišarovou	Fišarův	k2eAgFnSc7d1
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autoři	autor	k1gMnPc1
</s>
<s>
Hudba	hudba	k1gFnSc1
<g/>
:	:	kIx,
Andrew	Andrew	k1gMnSc1
Lloyd	Lloyd	k1gMnSc1
Webber	Webber	k1gMnSc1
</s>
<s>
Text	text	k1gInSc1
<g/>
:	:	kIx,
Tim	Tim	k?
Rice	Rice	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
písní	píseň	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějství	dějství	k1gNnSc1
</s>
<s>
Biograf	biograf	k1gInSc1
v	v	k7c4
Buenos	Buenos	k1gInSc4
Aires	Aires	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1952	#num#	k4
(	(	kIx(
<g/>
A	a	k8xC
Cinema	Cinema	k1gFnSc1
in	in	k?
Buenos	Buenos	k1gInSc1
Aires	Aires	k1gInSc1
26	#num#	k4
July	Jula	k1gFnSc2
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Film	film	k1gInSc1
v	v	k7c6
biografu	biograf	k1gInSc6
přeruší	přerušit	k5eAaPmIp3nS
tragická	tragický	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
zemřela	zemřít	k5eAaPmAgFnS
duchovní	duchovní	k2eAgFnPc4d1
vůdkyně	vůdkyně	k1gFnPc4
národa	národ	k1gInSc2
<g/>
,	,	kIx,
modla	modla	k1gFnSc1
a	a	k8xC
nejmocnější	mocný	k2eAgFnSc1d3
žena	žena	k1gFnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Perón	perón	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
</s>
<s>
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
ve	v	k7c6
masách	masa	k1gFnPc6
loučí	loučit	k5eAaImIp3nP
s	s	k7c7
svou	svůj	k3xOyFgFnSc7
milovanou	milovaný	k2eAgFnSc7d1
Evitou	Evita	k1gFnSc7
-	-	kIx~
Requiem	Requius	k1gMnSc7
za	za	k7c4
Evitu	Evita	k1gFnSc4
(	(	kIx(
<g/>
Requiem	Requium	k1gNnSc7
for	forum	k1gNnPc2
Evita	Evita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
-lid	-lid	k1gInSc1
Argentiny	Argentina	k1gFnSc2
</s>
<s>
Evitin	Evitin	k1gMnSc1
odpůrce	odpůrce	k1gMnSc1
Che	che	k0
se	se	k3xPyFc4
vysmívá	vysmívat	k5eAaImIp3nS
Argentině	Argentina	k1gFnSc3
za	za	k7c4
podlý	podlý	k2eAgInSc4d1
cirkus	cirkus	k1gInSc4
kolem	kolem	k7c2
Evitiny	Evitin	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
-	-	kIx~
Ó	Ó	kA
nač	nač	k6eAd1
ten	ten	k3xDgInSc1
cirkus	cirkus	k1gInSc1
(	(	kIx(
<g/>
Oh	oh	k0
What	What	k1gInSc4
a	a	k8xC
Cirkus	cirkus	k1gInSc4
<g/>
)	)	kIx)
<g/>
-	-	kIx~
Che	che	k0
a	a	k8xC
Eva	Eva	k1gFnSc1
</s>
<s>
Jsme	být	k5eAaImIp1nP
úplně	úplně	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
domovině	domovina	k1gFnSc6
Evy	Eva	k1gFnSc2
Duarte	Duart	k1gInSc5
vystupuje	vystupovat	k5eAaImIp3nS
zpěvák	zpěvák	k1gMnSc1
tanga	tango	k1gNnSc2
Magaldi	Magald	k1gMnPc1
<g/>
,	,	kIx,
její	její	k3xOp3gInPc4
milenec	milenec	k1gMnSc1
-	-	kIx~
Lásko	láska	k1gFnSc5
má	mít	k5eAaImIp3nS
<g/>
,	,	kIx,
tónem	tón	k1gInSc7
v	v	k7c6
záři	zář	k1gFnSc6
hvězd	hvězda	k1gFnPc2
(	(	kIx(
<g/>
On	on	k3xPp3gInSc1
This	This	k1gInSc1
Night	Night	k2eAgInSc1d1
of	of	k?
a	a	k8xC
Thousand	Thousand	k1gInSc1
Stars	Starsa	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
Magaldi	Magald	k1gMnPc1
</s>
<s>
Evita	Evita	k1gFnSc1
chce	chtít	k5eAaImIp3nS
odjet	odjet	k5eAaPmF
s	s	k7c7
Magaldim	Magaldi	k1gNnSc7
do	do	k7c2
Buenos	Buenosa	k1gFnPc2
Aires	Aires	k1gInSc1
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
jí	jíst	k5eAaImIp3nS
varuje	varovat	k5eAaImIp3nS
před	před	k7c7
vírem	vír	k1gInSc7
velkoměsta	velkoměsto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evina	Evin	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
naléhá	naléhat	k5eAaBmIp3nS,k5eAaImIp3nS
a	a	k8xC
Evita	Evita	k1gFnSc1
nakonec	nakonec	k6eAd1
odjíždí	odjíždět	k5eAaImIp3nS
-	-	kIx~
Eva	Eva	k1gFnSc1
a	a	k8xC
Magaldi	Magald	k1gMnPc1
<g/>
,	,	kIx,
Bulváry	bulvár	k1gInPc1
jsou	být	k5eAaImIp3nP
plné	plný	k2eAgFnPc4d1
špíny	špína	k1gFnPc4
(	(	kIx(
<g/>
Eva	Eva	k1gFnSc1
and	and	k?
Magaldi	Magald	k1gMnPc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
Beware	Bewar	k1gMnSc5
of	of	k?
the	the	k?
City	city	k1gNnSc1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
Magaldi	Magald	k1gMnPc1
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
Evina	Evin	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
</s>
<s>
Evita	Evita	k1gFnSc1
vítá	vítat	k5eAaImIp3nS
milované	milovaný	k1gMnPc4
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
-	-	kIx~
Buenos	Buenos	k1gMnSc1
Aires-Eva	Aires-Eva	k1gFnSc1
a	a	k8xC
sbor	sbor	k1gInSc1
</s>
<s>
Evita	Evita	k1gMnSc1
střídá	střídat	k5eAaImIp3nS
milence	milenec	k1gMnSc4
<g/>
,	,	kIx,
díky	díky	k7c3
nim	on	k3xPp3gFnPc3
se	se	k3xPyFc4
tak	tak	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
novinách	novina	k1gFnPc6
<g/>
,	,	kIx,
televizi	televize	k1gFnSc3
a	a	k8xC
rádiu	rádius	k1gInSc3
-	-	kIx~
Díky	díky	k7c3
a	a	k8xC
sbohem	sbohem	k0
(	(	kIx(
<g/>
Goodnight	Goodnight	k2eAgMnSc1d1
and	and	k?
Thank	Thank	k1gMnSc1
You	You	k1gMnSc1
<g/>
)	)	kIx)
-	-	kIx~
Che	che	k0
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
Magaldi	Magald	k1gMnPc1
<g/>
,	,	kIx,
Evini	Evin	k2eAgMnPc1d1
milenci	milenec	k1gMnPc1
</s>
<s>
V	v	k7c6
Argentině	Argentina	k1gFnSc6
se	se	k3xPyFc4
chytají	chytat	k5eAaImIp3nP
volby	volba	k1gFnPc1
<g/>
,	,	kIx,
žhavý	žhavý	k2eAgMnSc1d1
kandidát	kandidát	k1gMnSc1
je	být	k5eAaImIp3nS
Juan	Juan	k1gMnSc1
Perón	perón	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
podpoří	podpořit	k5eAaPmIp3nS
i	i	k9
Eva	Eva	k1gFnSc1
-	-	kIx~
Cesta	cesta	k1gFnSc1
možného	možný	k2eAgInSc2d1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Art	Art	k1gFnSc2
of	of	k?
the	the	k?
Possible	Possible	k1gFnSc1
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gFnSc1
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
<g/>
,	,	kIx,
vojáci	voják	k1gMnPc1
</s>
<s>
Skočíme	skočit	k5eAaPmIp1nP
v	v	k7c6
čase	čas	k1gInSc6
<g/>
,	,	kIx,
proběhla	proběhnout	k5eAaPmAgFnS
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
politického	politický	k2eAgNnSc2d1
popředí	popředí	k1gNnSc2
a	a	k8xC
z	z	k7c2
Evy	Eva	k1gFnSc2
je	být	k5eAaImIp3nS
sice	sice	k8xC
špatná	špatný	k2eAgFnSc1d1
<g/>
,	,	kIx,
zato	zato	k6eAd1
hodně	hodně	k6eAd1
slavná	slavný	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
San	San	k1gMnSc1
Juan	Juan	k1gMnSc1
zasáhne	zasáhnout	k5eAaPmIp3nS
silné	silný	k2eAgNnSc4d1
zemětřesení	zemětřesení	k1gNnSc4
-	-	kIx~
Ta	ten	k3xDgFnSc1
dáma	dáma	k1gFnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
má	mít	k5eAaImIp3nS
(	(	kIx(
<g/>
The	The	k1gFnSc1
Lady	lady	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Got	Got	k1gFnSc7
Potencial	Potencial	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Che	-Che	k1gInSc1
<g/>
,	,	kIx,
sbor	sbor	k1gInSc1
</s>
<s>
Na	na	k7c6
benefičním	benefiční	k2eAgInSc6d1
koncertě	koncert	k1gInSc6
obětem	oběť	k1gFnPc3
zemětřesení	zemětřesení	k1gNnSc1
se	se	k3xPyFc4
setká	setkat	k5eAaPmIp3nS
Eva	Eva	k1gFnSc1
s	s	k7c7
Magaldim	Magaldi	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
s	s	k7c7
Perónem	perón	k1gInSc7
-	-	kIx~
:	:	kIx,
<g/>
Charitativní	charitativní	k2eAgInSc1d1
koncert	koncert	k1gInSc1
(	(	kIx(
<g/>
Charity	charita	k1gFnSc2
Concert	Concert	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Perón	-Perón	k1gMnSc1
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
Magaldi	Magald	k1gMnPc1
</s>
<s>
Eva	Eva	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
Perónovi	Perón	k1gMnSc3
spojenectví	spojenectví	k1gNnSc4
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
být	být	k5eAaImF
užitečná	užitečný	k2eAgFnSc1d1
-	-	kIx~
Vím	vědět	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dnes	dnes	k6eAd1
mohu	moct	k5eAaImIp1nS
hodit	hodit	k5eAaPmF,k5eAaImF
vám	vy	k3xPp2nPc3
(	(	kIx(
<g/>
I	i	k9
<g/>
'	'	kIx"
<g/>
d	d	k?
Be	Be	k1gMnPc1
Suprisingly	Suprisingla	k1gFnSc2
Good	Good	k1gInSc1
for	forum	k1gNnPc2
You	You	k1gFnSc2
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gFnSc1
a	a	k8xC
Perón	perón	k1gInSc1
</s>
<s>
Eva	Eva	k1gFnSc1
vyhání	vyhánět	k5eAaImIp3nS
mladinkou	mladinký	k2eAgFnSc4d1
Perónovu	Perónův	k2eAgFnSc4d1
milenku	milenka	k1gFnSc4
-	-	kIx~
Tak	tak	k6eAd1
nazdar	nazdar	k0
a	a	k8xC
spíš	spát	k5eAaImIp2nS
hned	hned	k6eAd1
říkám	říkat	k5eAaImIp1nS
ti	ten	k3xDgMnPc1
sbohem	sbohem	k0
(	(	kIx(
<g/>
Hello	Hello	k1gNnSc1
and	and	k?
Goodbye	Goodby	k1gInSc2
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k6eAd1
</s>
<s>
Milenka	Milenka	k1gFnSc1
zpívá	zpívat	k5eAaImIp3nS
o	o	k7c6
zklamání	zklamání	k1gNnSc6
a	a	k8xC
zradě	zrada	k1gFnSc6
-	-	kIx~
Další	další	k2eAgInSc1d1
kufr	kufr	k1gInSc1
stojí	stát	k5eAaImIp3nS
u	u	k7c2
dveří	dveře	k1gFnPc2
(	(	kIx(
<g/>
Another	Anothra	k1gFnPc2
Suitcase	Suitcasa	k1gFnSc6
in	in	k?
Another	Anothra	k1gFnPc2
Hall	Hall	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Perónova	-Perónův	k2eAgFnSc1d1
milenka	milenka	k1gFnSc1
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
sbor	sbor	k1gInSc1
</s>
<s>
Aristokraté	aristokrat	k1gMnPc1
ani	ani	k8xC
vojáci	voják	k1gMnPc1
nemůžou	nemůžou	k?
vystát	vystát	k5eAaPmF
Evu	Eva	k1gFnSc4
<g/>
,	,	kIx,
dívku	dívka	k1gFnSc4
z	z	k7c2
chudého	chudý	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
-	-	kIx~
Co	co	k9
se	se	k3xPyFc4
z	z	k7c2
tý	tý	k0
mrchy	mrcha	k1gFnSc2
vyklube	vyklubat	k5eAaPmIp3nS
<g/>
/	/	kIx~
<g/>
Perónův	Perónův	k2eAgInSc1d1
poslední	poslední	k2eAgInSc1d1
výstřelek	výstřelek	k1gInSc1
(	(	kIx(
<g/>
Peron	peron	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Latest	Latest	k1gInSc1
Flame	Flam	k1gInSc5
<g/>
)	)	kIx)
<g/>
-Eva	-Evus	k1gMnSc2
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
aristokraté	aristokrat	k1gMnPc1
</s>
<s>
Perón	perón	k1gInSc1
je	být	k5eAaImIp3nS
vržen	vrhnout	k5eAaPmNgInS
do	do	k7c2
vězení	vězení	k1gNnSc2
<g/>
,	,	kIx,
vysvobodí	vysvobodit	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
síla	síla	k1gFnSc1
lidu	lid	k1gInSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Evou	Eva	k1gFnSc7
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
prezidentem	prezident	k1gMnSc7
a	a	k8xC
žení	ženit	k5eAaImIp3nP
se	se	k3xPyFc4
s	s	k7c7
Evou	Eva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
nová	nový	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
.	.	kIx.
-	-	kIx~
Hleď	hledět	k5eAaImRp2nS
v	v	k7c6
dál	daleko	k6eAd2
Argentino	Argentina	k1gFnSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
A	a	k8xC
New	New	k1gFnSc1
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gFnSc1
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
<g/>
,	,	kIx,
lid	lid	k1gInSc1
Argentiny	Argentina	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějství	dějství	k1gNnSc1
</s>
<s>
Lid	lid	k1gInSc1
volá	volat	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc4
nového	nový	k2eAgMnSc4d1
prezidenta	prezident	k1gMnSc4
Juana	Juan	k1gMnSc4
Peróna	Perón	k1gMnSc4
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
představí	představit	k5eAaPmIp3nS
první	první	k4xOgFnSc4
dámu	dáma	k1gFnSc4
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Evu	Eva	k1gFnSc4
Perón	perón	k1gInSc1
-	-	kIx~
Na	na	k7c6
balkóně	balkón	k1gInSc6
vily	vila	k1gFnSc2
Casa	Casa	k1gFnSc1
Rosada	Rosada	k1gFnSc1
(	(	kIx(
<g/>
On	on	k3xPp3gMnSc1
the	the	k?
Balcony	Balcona	k1gFnSc2
of	of	k?
the	the	k?
Casa	Cas	k2eAgFnSc1d1
Rosada	Rosada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
-Che	-Che	k1gInSc1
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
<g/>
,	,	kIx,
lid	lid	k1gInSc1
</s>
<s>
Evita	Evita	k1gFnSc1
přísahá	přísahat	k5eAaImIp3nS
lidu	lid	k1gInSc3
–	–	k?
vždy	vždy	k6eAd1
bude	být	k5eAaImBp3nS
s	s	k7c7
ním	on	k3xPp3gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
opakuje	opakovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
-	-	kIx~
Utiš	utišit	k5eAaPmRp2nS
svůj	svůj	k3xOyFgInSc4
pláč	pláč	k1gInSc4
<g/>
,	,	kIx,
Argentino	Argentina	k1gFnSc5
(	(	kIx(
<g/>
Don	dona	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
Cry	Cry	k1gFnSc1
for	forum	k1gNnPc2
Me	Me	k1gFnSc2
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gFnSc1
</s>
<s>
Che	che	k0
varuje	varovat	k5eAaImIp3nS
mladou	mladá	k1gFnSc4
Evita	Evito	k1gNnSc2
před	před	k7c7
pádem	pád	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
může	moct	k5eAaImIp3nS
nastat	nastat	k5eAaPmF
z	z	k7c2
nebeských	nebeský	k2eAgFnPc2d1
výšin	výšina	k1gFnPc2
-	-	kIx~
Tvůj	tvůj	k3xOp2gInSc1
Olymp	Olymp	k1gInSc1
(	(	kIx(
<g/>
High	High	k1gInSc1
Flying	Flying	k1gInSc1
Adored	Adored	k1gInSc1
<g/>
)	)	kIx)
<g/>
-Che	-Che	k1gFnSc1
a	a	k8xC
Eva	Eva	k1gFnSc1
</s>
<s>
Evitu	Evita	k1gFnSc4
štve	štvát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
ve	v	k7c6
světě	svět	k1gInSc6
neví	vědět	k5eNaImIp3nS
o	o	k7c6
ní	on	k3xPp3gFnSc6
a	a	k8xC
Perónovi	Perón	k1gMnSc3
<g/>
,	,	kIx,
míří	mířit	k5eAaImIp3nS
do	do	k7c2
Evropy	Evropa	k1gFnSc2
-	-	kIx~
Modla	modla	k1gFnSc1
(	(	kIx(
<g/>
Rainbow	Rainbow	k1gMnSc1
High	High	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gMnSc1
<g/>
,	,	kIx,
kadeřníci	kadeřník	k1gMnPc1
<g/>
,	,	kIx,
módní	módní	k2eAgMnPc1d1
návrháři	návrhář	k1gMnPc1
<g/>
,	,	kIx,
vizážisté	vizážista	k1gMnPc1
</s>
<s>
Evita	Evita	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
navštíví	navštívit	k5eAaPmIp3nS
mnoho	mnoho	k4c4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
Itálii	Itálie	k1gFnSc6
jí	on	k3xPp3gFnSc7
odmítnou	odmítnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
milují	milovat	k5eAaImIp3nP
<g/>
,	,	kIx,
ve	v	k7c6
Francii	Francie	k1gFnSc6
však	však	k9
náhle	náhle	k6eAd1
Evita	Evita	k1gFnSc1
kolabuje	kolabovat	k5eAaImIp3nS
<g/>
…	…	k?
Duhová	duhový	k2eAgFnSc1d1
Jízda	jízda	k1gFnSc1
(	(	kIx(
<g/>
Rainbow	Rainbow	k1gMnSc1
Tour	Tour	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Perón	-Perón	k1gMnSc1
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
důstojníci	důstojník	k1gMnPc1
</s>
<s>
Evita	Evita	k1gFnSc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nechce	chtít	k5eNaImIp3nS
dostat	dostat	k5eAaPmF
mezi	mezi	k7c4
argentinské	argentinský	k2eAgMnPc4d1
aristokraty	aristokrat	k1gMnPc4
-	-	kIx~
Herečka	hereček	k1gMnSc4
netouží	toužit	k5eNaImIp3nS
nudné	nudný	k2eAgFnPc4d1
role	role	k1gFnPc4
hrát	hrát	k5eAaImF
(	(	kIx(
<g/>
The	The	k1gFnSc1
Actress	Actressa	k1gFnPc2
Hasn	Hasna	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
Learned	Learned	k1gMnSc1
the	the	k?
Lines	Lines	k1gMnSc1
<g/>
/	/	kIx~
<g/>
You	You	k1gMnSc1
<g/>
'	'	kIx"
<g/>
d	d	k?
Like	Like	k1gFnSc1
to	ten	k3xDgNnSc4
Hear	Hear	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gMnSc1
a	a	k8xC
Che	che	k0
</s>
<s>
Evita	Evita	k1gFnSc1
si	se	k3xPyFc3
založila	založit	k5eAaPmAgFnS
nadaci	nadace	k1gFnSc4
<g/>
,	,	kIx,
charita	charita	k1gFnSc1
ale	ale	k8xC
není	být	k5eNaImIp3nS
úplně	úplně	k6eAd1
tak	tak	k6eAd1
čistá	čistý	k2eAgFnSc1d1
záležitost	záležitost	k1gFnSc1
<g/>
…	…	k?
-	-	kIx~
Prachy	prach	k1gInPc1
se	se	k3xPyFc4
točí	točit	k5eAaImIp3nP
(	(	kIx(
<g/>
And	Anda	k1gFnPc2
the	the	k?
Money	Monea	k1gMnSc2
Kept	Kept	k2eAgInSc4d1
Rolling	Rolling	k1gInSc4
In	In	k1gFnSc2
<g/>
/	/	kIx~
<g/>
and	and	k?
Out	Out	k1gFnSc1
<g/>
/	/	kIx~
<g/>
)	)	kIx)
<g/>
-Che	-Che	k6eAd1
a	a	k8xC
dělníci	dělník	k1gMnPc1
</s>
<s>
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
modlí	modlit	k5eAaImIp3nP
k	k	k7c3
Evitě	Evita	k1gFnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
nesouhlasí	souhlasit	k5eNaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zbit	zbít	k5eAaPmNgMnS
policí	police	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evita	Evit	k1gInSc2
opět	opět	k6eAd1
kolabuje	kolabovat	k5eAaImIp3nS
<g/>
.	.	kIx.
-	-	kIx~
Santa	Santa	k1gMnSc1
Evita-děti	Evita-děti	k1gMnSc1
<g/>
,	,	kIx,
dělníci	dělník	k1gMnPc1
<g/>
,	,	kIx,
Che	che	k0
</s>
<s>
Evita	Evita	k1gFnSc1
a	a	k8xC
Che	che	k0
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
setkávají	setkávat	k5eAaImIp3nP
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
v	v	k7c6
kómatu	kóma	k1gNnSc6
<g/>
,	,	kIx,
navzájem	navzájem	k6eAd1
si	se	k3xPyFc3
vyčítají	vyčítat	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
nedostatky	nedostatek	k1gInPc4
<g/>
.	.	kIx.
-	-	kIx~
Valčík	valčík	k1gInSc1
pro	pro	k7c4
Evu	Eva	k1gFnSc4
a	a	k8xC
Che	che	k0
(	(	kIx(
<g/>
Waltz	waltz	k1gInSc1
for	forum	k1gNnPc2
Eva	Eva	k1gFnSc1
and	and	k?
Che	che	k0
<g/>
)	)	kIx)
<g/>
-Eva	-Ev	k1gInSc2
a	a	k8xC
Che	che	k0
</s>
<s>
Perón	perón	k1gInSc1
před	před	k7c7
vládou	vláda	k1gFnSc7
obhajuje	obhajovat	k5eAaImIp3nS
Evitiny	Evitin	k2eAgInPc4d1
činy	čin	k1gInPc4
a	a	k8xC
přiznává	přiznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jí	on	k3xPp3gFnSc3
ubývá	ubývat	k5eAaImIp3nS
sil	síla	k1gFnPc2
-	-	kIx~
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
démant	démant	k1gInSc1
<g/>
(	(	kIx(
<g/>
She	She	k1gMnSc1
Is	Is	k1gMnSc1
a	a	k8xC
Diamond	Diamond	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Perón	-Perón	k1gMnSc1
</s>
<s>
Evita	Evita	k1gFnSc1
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
viceprezidentkou	viceprezidentka	k1gFnSc7
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
to	ten	k3xDgNnSc1
odmítá	odmítat	k5eAaImIp3nS
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
má	mít	k5eAaImIp3nS
přeci	přeci	k?
rakovinu	rakovina	k1gFnSc4
a	a	k8xC
umírá	umírat	k5eAaImIp3nS
-	-	kIx~
Karty	karta	k1gFnPc1
Leží	ležet	k5eAaImIp3nP
(	(	kIx(
<g/>
Dice	Dic	k1gInPc1
Are	ar	k1gInSc5
Rolling	Rolling	k1gInSc1
<g/>
)	)	kIx)
<g/>
-Perón	-Perón	k1gInSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
lid	lid	k1gInSc1
</s>
<s>
Evita	Evita	k1gMnSc1
si	se	k3xPyFc3
promítá	promítat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
s	s	k7c7
Juanem	Juan	k1gMnSc7
a	a	k8xC
ptá	ptat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
jsi	být	k5eAaImIp2nS
mě	já	k3xPp1nSc4
rád	rád	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
Měl	mít	k5eAaImAgMnS
jsi	být	k5eAaImIp2nS
mě	já	k3xPp1nSc4
rád	rád	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
You	You	k1gMnSc1
Must	Must	k1gMnSc1
Love	lov	k1gInSc5
Me	Me	k1gMnPc5
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gFnSc1
<g/>
*	*	kIx~
</s>
<s>
Che	che	k0
donutí	donutit	k5eAaPmIp3nS
Evu	Eva	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
veřejně	veřejně	k6eAd1
v	v	k7c6
rádiu	rádius	k1gInSc6
přiznala	přiznat	k5eAaPmAgFnS
prohru	prohra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
ale	ale	k8xC
naopak	naopak	k6eAd1
Argentině	Argentina	k1gFnSc3
slibuje	slibovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
je	být	k5eAaImIp3nS
Argentinou	Argentina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navždy	navždy	k6eAd1
-	-	kIx~
Evin	Evin	k2eAgInSc1d1
poslední	poslední	k2eAgInSc1d1
projev	projev	k1gInSc1
(	(	kIx(
<g/>
Eva	Eva	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Final	Final	k1gMnSc1
Broadcast	Broadcast	k1gMnSc1
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gMnSc1
</s>
<s>
Evita	Evita	k1gMnSc1
si	se	k3xPyFc3
přemítá	přemítat	k5eAaImIp3nS
střípky	střípek	k1gInPc4
z	z	k7c2
života	život	k1gInSc2
-	-	kIx~
Fragmenty	fragment	k1gInPc1
(	(	kIx(
<g/>
Montage	Montage	k1gFnSc1
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gFnSc1
<g/>
,	,	kIx,
Che	che	k0
<g/>
,	,	kIx,
Magaldi	Magald	k1gMnPc1
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
<g/>
,	,	kIx,
sbor	sbor	k1gInSc1
</s>
<s>
Evitina	Evitin	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
o	o	k7c6
jejím	její	k3xOp3gInSc6
životě	život	k1gInSc6
před	před	k7c7
i	i	k9
po	po	k7c6
smrti	smrt	k1gFnSc6
<g/>
…	…	k?
-	-	kIx~
Žalm	žalm	k1gInSc1
(	(	kIx(
<g/>
Lament	Lament	k?
<g/>
)	)	kIx)
<g/>
-Eva	-Eva	k1gMnSc1
<g/>
,	,	kIx,
balzamovači	balzamovač	k1gMnPc1
</s>
<s>
Che	che	k0
vypráví	vyprávět	k5eAaImIp3nS
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
…	…	k?
</s>
<s>
*	*	kIx~
<g/>
Tato	tento	k3xDgFnSc1
píseň	píseň	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
před	před	k7c7
She	She	k1gFnSc7
<g/>
'	'	kIx"
<g/>
s	s	k7c7
a	a	k8xC
Diamond	Diamonda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
scénosled	scénosled	k1gInSc1
je	být	k5eAaImIp3nS
čerpán	čerpat	k5eAaImNgInS
z	z	k7c2
programu	program	k1gInSc2
DJKT	DJKT	kA
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3
produkce	produkce	k1gFnSc1
</s>
<s>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
West	West	k2eAgInSc4d1
End-Princ	End-Princ	k1gInSc4
Edward	Edward	k1gMnSc1
theatre	theatr	k1gInSc5
</s>
<s>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
Broadway-Broadway	Broadway-Broadwaa	k1gFnSc2
theatre	theatr	k1gInSc5
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
West	West	k1gInSc1
End-Adelphi	End-Adelph	k1gFnSc2
theatre	theatr	k1gInSc5
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Broadway-Marquis	Broadway-Marquis	k1gFnSc1
theatre	theatr	k1gMnSc5
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Evita	Evit	k1gInSc2
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Velkofilm	velkofilm	k1gInSc1
od	od	k7c2
Alana	Alan	k1gMnSc2
Parkera	Parker	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
kin	kino	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
s	s	k7c7
Madonnou	Madonný	k2eAgFnSc7d1
a	a	k8xC
Antonio	Antonio	k1gMnSc1
Banderasem	Banderas	k1gMnSc7
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tuto	tento	k3xDgFnSc4
verzi	verze	k1gFnSc4
byl	být	k5eAaImAgMnS
připsán	připsán	k2eAgMnSc1d1
megahit	megahit	k1gMnSc1
You	You	k1gMnSc1
Must	Must	k1gMnSc1
Love	lov	k1gInSc5
Me	Me	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
později	pozdě	k6eAd2
získal	získat	k5eAaPmAgMnS
Oscara	Oscara	k1gFnSc1
a	a	k8xC
obrovský	obrovský	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Produkce	produkce	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Spirála	spirála	k1gFnSc1
(	(	kIx(
<g/>
331	#num#	k4
repríz	repríza	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
překladu	překlad	k1gInSc6
Jiřího	Jiří	k1gMnSc2
Bryana	Bryan	k1gMnSc2
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
Brno	Brno	k1gNnSc4
<g/>
,	,	kIx,
Městské	městský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
27	#num#	k4
repríz	repríza	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
překladu	překlad	k1gInSc6
Michaela	Michaela	k1gFnSc1
Prostějovského	prostějovský	k2eAgInSc2d1
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Josefa	Josef	k1gMnSc2
Kajetána	Kajetán	k1gMnSc2
Tyla	Tyl	k1gMnSc2
(	(	kIx(
<g/>
40	#num#	k4
repríz	repríza	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
překladu	překlad	k1gInSc6
Michaela	Michaela	k1gFnSc1
Prostějovského	prostějovský	k2eAgInSc2d1
</s>
<s>
2014	#num#	k4
<g/>
–	–	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
moravskoslezské	moravskoslezský	k2eAgFnSc2d1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
repríz	repríza	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
překladu	překlad	k1gInSc6
Michaela	Michaela	k1gFnSc1
Prostějovského	prostějovský	k2eAgInSc2d1
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Divadlo	divadlo	k1gNnSc1
Studio	studio	k1gNnSc1
DVA	dva	k4xCgInPc4
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
repríz	repríza	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
překladu	překlad	k1gInSc6
Michaela	Michaela	k1gFnSc1
Prostějovského	prostějovský	k2eAgInSc2d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2019	#num#	k4
<g/>
–	–	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
Jihočeské	jihočeský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
repríz	repríza	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
překladu	překlad	k1gInSc6
Michaela	Michaela	k1gFnSc1
Prostějovského	prostějovský	k2eAgInSc2d1
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1
</s>
<s>
1976	#num#	k4
kompletní	kompletní	k2eAgFnSc1d1
studiová	studiový	k2eAgFnSc1d1
nahrávka	nahrávka	k1gFnSc1
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
verze	verze	k1gFnSc1
</s>
<s>
1978	#num#	k4
highlights	highlights	k1gInSc1
z	z	k7c2
londýnské	londýnský	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
</s>
<s>
1979	#num#	k4
komplet	komplet	k1gInSc4
z	z	k7c2
Broadwaye-finální	Broadwaye-finální	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
1998	#num#	k4
česká	český	k2eAgFnSc1d1
nahrávka	nahrávka	k1gFnSc1
z	z	k7c2
divadla	divadlo	k1gNnSc2
Spirála	spirála	k1gFnSc1
</s>
<s>
2006	#num#	k4
highlights	highlights	k1gInSc1
z	z	k7c2
nové	nový	k2eAgFnSc2d1
londýnské	londýnský	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
</s>
<s>
2012	#num#	k4
kompletní	kompletní	k2eAgFnSc1d1
nahrávka	nahrávka	k1gFnSc1
z	z	k7c2
nového	nový	k2eAgNnSc2d1
Broadwayského	broadwayský	k2eAgNnSc2d1
nastudování	nastudování	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
http://www.studiodva.cz/evita-2/evita/	http://www.studiodva.cz/evita-2/evita/	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Prostějovský	prostějovský	k2eAgMnSc1d1
–	–	k?
Muzikál	muzikál	k1gInSc1
Express	express	k1gInSc1
ISBN	ISBN	kA
978-80-86907-49-9	978-80-86907-49-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Evita	Evita	k1gFnSc1
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
Městského	městský	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
Brno	Brno	k1gNnSc1
</s>
<s>
Evita	Evita	k1gFnSc1
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
DJKT	DJKT	kA
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Evita	Evita	k1gFnSc1
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
Studia	studio	k1gNnSc2
Dva	dva	k4xCgMnPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
nového	nový	k2eAgNnSc2d1
nastudování	nastudování	k1gNnSc2
na	na	k7c6
Broadwayi	Broadway	k1gFnSc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Andrew	Andrew	k1gFnSc2
Lloyd	Lloyda	k1gFnPc2
Webbera	Webbero	k1gNnSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Tima	Timus	k1gMnSc2
Rice	Ric	k1gMnSc2
</s>
<s>
České	český	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Evita	Evit	k1gMnSc2
Hudba	hudba	k1gFnSc1
Andrew	Andrew	k1gMnSc1
Lloyd	Lloyd	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
Texty	text	k1gInPc1
Tim	Tim	k?
Rice	Ric	k1gMnSc4
Písně	píseň	k1gFnSc2
</s>
<s>
Oh	oh	k0
What	What	k1gMnSc1
a	a	k8xC
Circus	Circus	k1gMnSc1
</s>
<s>
Another	Anothra	k1gFnPc2
Suitcase	Suitcasa	k1gFnSc6
in	in	k?
Another	Anothra	k1gFnPc2
Hall	Hall	k1gMnSc1
</s>
<s>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Cry	Cry	k1gFnSc1
for	forum	k1gNnPc2
Me	Me	k1gFnSc2
Argentina	Argentina	k1gFnSc1
</s>
<s>
You	You	k?
Must	Must	k1gInSc1
Love	lov	k1gInSc5
Me	Me	k1gFnSc3
Verze	verze	k1gFnSc1
</s>
<s>
Evita	Evita	k1gFnSc1
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Evita	Evita	k1gFnSc1
(	(	kIx(
<g/>
muzikál	muzikál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Evita	Evita	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Evita	Evita	k1gFnSc1
(	(	kIx(
<g/>
soundtrack	soundtrack	k1gInSc1
<g/>
))	))	k?
Historické	historický	k2eAgInPc1d1
postavy	postav	k1gInPc1
</s>
<s>
Eva	Eva	k1gFnSc1
Perón	perón	k1gInSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Perón	perón	k1gInSc4
</s>
<s>
Agustín	Agustín	k1gInSc4
Magaldi	Magald	k1gMnPc1
Související	související	k2eAgMnSc1d1
</s>
<s>
Op	op	k1gMnSc1
zoek	zoek	k1gMnSc1
naar	naar	k1gMnSc1
Evita	Evita	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
