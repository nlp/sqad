<p>
<s>
Zbirožský	zbirožský	k2eAgInSc1d1	zbirožský
potok	potok	k1gInSc1	potok
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rokycany	Rokycany	k1gInPc1	Rokycany
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravobřežním	pravobřežní	k2eAgInSc7d1	pravobřežní
přítokem	přítok	k1gInSc7	přítok
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
lesích	les	k1gInPc6	les
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
Mýto	mýto	k1gNnSc1	mýto
u	u	k7c2	u
Sirské	Sirský	k2eAgFnSc2d1	Sirský
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
593	[number]	k4	593
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Panský	panský	k2eAgInSc1d1	panský
les	les	k1gInSc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
východně	východně	k6eAd1	východně
<g/>
,	,	kIx,	,
vlní	vlnit	k5eAaImIp3nS	vlnit
se	se	k3xPyFc4	se
a	a	k8xC	a
stáčí	stáčet	k5eAaImIp3nS	stáčet
se	se	k3xPyFc4	se
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
k	k	k7c3	k
dálnici	dálnice	k1gFnSc3	dálnice
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Podteče	podtéct	k5eAaPmIp3nS	podtéct
pod	pod	k7c7	pod
dálnicí	dálnice	k1gFnSc7	dálnice
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
východně	východně	k6eAd1	východně
až	až	k9	až
severovýchodně	severovýchodně	k6eAd1	severovýchodně
poblíž	poblíž	k7c2	poblíž
lokality	lokalita	k1gFnSc2	lokalita
Kařízek	Kařízek	k1gInSc1	Kařízek
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Hořejšího	Hořejšího	k2eAgFnPc2d1	Hořejšího
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Dolejšího	Dolejšího	k2eAgInSc2d1	Dolejšího
kařezského	kařezský	k2eAgInSc2d1	kařezský
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
vsí	ves	k1gFnSc7	ves
Kařez	Kařeza	k1gFnPc2	Kařeza
a	a	k8xC	a
otáčí	otáčet	k5eAaImIp3nS	otáčet
se	se	k3xPyFc4	se
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
protéká	protékat	k5eAaImIp3nS	protékat
Zbirohem	Zbiroh	k1gInSc7	Zbiroh
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zahloubeným	zahloubený	k2eAgNnSc7d1	zahloubené
údolím	údolí	k1gNnSc7	údolí
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Křivoklátsko	Křivoklátsko	k1gNnSc1	Křivoklátsko
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Čilá	čilý	k2eAgFnSc1d1	čilá
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
Skryjský	Skryjský	k2eAgInSc4d1	Skryjský
vodopád	vodopád	k1gInSc4	vodopád
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
Skryjskými	Skryjský	k2eAgNnPc7d1	Skryjské
jezírky	jezírko	k1gNnPc7	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
Skryje	skrýt	k5eAaPmIp3nS	skrýt
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Rakovník	Rakovník	k1gInSc1	Rakovník
<g/>
)	)	kIx)	)
a	a	k8xC	a
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
Čilá	čilý	k2eAgFnSc1d1	čilá
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
