<p>
<s>
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Min	min	kA	min
výslovnost	výslovnost	k1gFnSc1	výslovnost
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1890	[number]	k4	1890
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Nghe	Nghe	k1gNnPc2	Nghe
An	An	k1gMnSc1	An
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Indočíny	Indočína	k1gFnSc2	Indočína
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1969	[number]	k4	1969
Hanoj	Hanoj	k1gFnSc4	Hanoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Nguyễ	Nguyễ	k1gMnSc1	Nguyễ
Sinh	Sinh	k1gMnSc1	Sinh
Cung	Cung	k1gInSc4	Cung
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
Ho	on	k3xPp3gInSc2	on
Či	či	k8xC	či
Min	min	kA	min
(	(	kIx(	(
<g/>
vietnamsky	vietnamsky	k6eAd1	vietnamsky
psáno	psán	k2eAgNnSc1d1	psáno
Hồ	Hồ	k1gFnSc4	Hồ
Chí	chí	k1gNnSc2	chí
Minh	Minha	k1gFnPc2	Minha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
přibližně	přibližně	k6eAd1	přibližně
"	"	kIx"	"
<g/>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
osvícený	osvícený	k2eAgMnSc1d1	osvícený
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vietnamský	vietnamský	k2eAgMnSc1d1	vietnamský
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
boje	boj	k1gInSc2	boj
za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
osvobození	osvobození	k1gNnSc4	osvobození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Hoà	Hoà	k1gMnSc2	Hoà
Trù	Trù	k1gMnSc2	Trù
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Vietnamu	Vietnam	k1gInSc2	Vietnam
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
konfuciánského	konfuciánský	k2eAgMnSc2d1	konfuciánský
učence	učenec	k1gMnSc2	učenec
a	a	k8xC	a
císařského	císařský	k2eAgMnSc2d1	císařský
soudce	soudce	k1gMnSc2	soudce
Nguyen	Nguyno	k1gNnPc2	Nguyno
Sin	sino	k1gNnPc2	sino
Hua	Hua	k1gMnPc3	Hua
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
rodného	rodný	k2eAgInSc2d1	rodný
domu	dům	k1gInSc2	dům
muzeum	muzeum	k1gNnSc1	muzeum
Kim	Kim	k1gMnSc1	Kim
Liê	Liê	k1gMnSc1	Liê
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgMnPc4	dva
bratry	bratr	k1gMnPc4	bratr
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
úřednice	úřednice	k1gFnSc1	úřednice
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Min	min	kA	min
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
francouzské	francouzský	k2eAgNnSc4d1	francouzské
lyceum	lyceum	k1gNnSc4	lyceum
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Hue	Hue	k1gFnSc2	Hue
a	a	k8xC	a
pak	pak	k6eAd1	pak
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
vietnamštinu	vietnamština	k1gFnSc4	vietnamština
v	v	k7c4	v
Phan	Phan	k1gNnSc4	Phan
Thiế	Thiế	k1gFnSc2	Thiế
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
parníku	parník	k1gInSc6	parník
Amirale	Amiral	k1gMnSc5	Amiral
de	de	k?	de
Latouche-Treville	Latouche-Treville	k1gNnSc2	Latouche-Treville
připlul	připlout	k5eAaPmAgInS	připlout
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1911	[number]	k4	1911
do	do	k7c2	do
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
hlásil	hlásit	k5eAaImAgMnS	hlásit
do	do	k7c2	do
koloniální	koloniální	k2eAgFnSc2d1	koloniální
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
jako	jako	k8xC	jako
kuchařský	kuchařský	k2eAgMnSc1d1	kuchařský
pomocník	pomocník	k1gMnSc1	pomocník
a	a	k8xC	a
procestoval	procestovat	k5eAaPmAgMnS	procestovat
několik	několik	k4yIc4	několik
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Kominternu	Kominterna	k1gFnSc4	Kominterna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc6	rok
1941	[number]	k4	1941
se	se	k3xPyFc4	se
po	po	k7c6	po
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vedl	vést	k5eAaImAgMnS	vést
osvobozenecké	osvobozenecký	k2eAgNnSc4d1	osvobozenecké
levicové	levicový	k2eAgNnSc4d1	levicové
hnutí	hnutí	k1gNnSc4	hnutí
Viet	Vieta	k1gFnPc2	Vieta
Minh	Minha	k1gFnPc2	Minha
proti	proti	k7c3	proti
Vichistické	vichistický	k2eAgFnSc3d1	vichistická
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
japonským	japonský	k2eAgFnPc3d1	japonská
okupačním	okupační	k2eAgFnPc3d1	okupační
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Srpnové	srpnový	k2eAgFnSc6d1	srpnová
revoluci	revoluce	k1gFnSc6	revoluce
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Min	min	kA	min
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1945	[number]	k4	1945
vietnamskou	vietnamský	k2eAgFnSc4d1	vietnamská
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
doživotním	doživotní	k2eAgMnSc7d1	doživotní
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
zároveň	zároveň	k6eAd1	zároveň
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Indočíně	Indočína	k1gFnSc6	Indočína
vedl	vést	k5eAaImAgMnS	vést
Ligu	liga	k1gFnSc4	liga
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Vietnamu	Vietnam	k1gInSc2	Vietnam
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
francouzské	francouzský	k2eAgFnSc3d1	francouzská
koloniální	koloniální	k2eAgFnSc3d1	koloniální
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Severního	severní	k2eAgInSc2d1	severní
Vietnamu	Vietnam	k1gInSc2	Vietnam
během	během	k7c2	během
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
proti	proti	k7c3	proti
Američanům	Američan	k1gMnPc3	Američan
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
básníkem	básník	k1gMnSc7	básník
<g/>
.	.	kIx.	.
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
plynně	plynně	k6eAd1	plynně
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
kantonsky	kantonsky	k6eAd1	kantonsky
a	a	k8xC	a
mandarínsky	mandarínsky	k6eAd1	mandarínsky
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
domku	domek	k1gInSc6	domek
zahradníka	zahradník	k1gMnSc2	zahradník
vedle	vedle	k7c2	vedle
bývalého	bývalý	k2eAgInSc2d1	bývalý
paláce	palác	k1gInSc2	palác
francouzského	francouzský	k2eAgMnSc4d1	francouzský
guvernéra	guvernér	k1gMnSc4	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1969	[number]	k4	1969
na	na	k7c4	na
srdeční	srdeční	k2eAgNnSc4d1	srdeční
selhání	selhání	k1gNnSc4	selhání
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
hanojském	hanojský	k2eAgInSc6d1	hanojský
domě	dům	k1gInSc6	dům
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
79	[number]	k4	79
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nabalzamované	nabalzamovaný	k2eAgNnSc1d1	nabalzamované
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
mauzoleu	mauzoleum	k1gNnSc6	mauzoleum
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Ba	ba	k9	ba
Đì	Đì	k1gMnSc1	Đì
<g/>
'	'	kIx"	'
v	v	k7c6	v
Hanoji	Hanoj	k1gFnSc6	Hanoj
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
zpopelnění	zpopelnění	k1gNnSc4	zpopelnění
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1978	[number]	k4	1978
bylo	být	k5eAaImAgNnS	být
bývalé	bývalý	k2eAgNnSc1d1	bývalé
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
Saigon	Saigon	k1gInSc1	Saigon
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Ho	on	k3xPp3gInSc4	on
Či	či	k8xC	či
Minovo	Minův	k2eAgNnSc1d1	Minovo
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Návštěva	návštěva	k1gFnSc1	návštěva
Československa	Československo	k1gNnSc2	Československo
==	==	k?	==
</s>
</p>
<p>
<s>
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Min	min	kA	min
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1957	[number]	k4	1957
přicestoval	přicestovat	k5eAaPmAgMnS	přicestovat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Antonínem	Antonín	k1gMnSc7	Antonín
Zápotockým	Zápotocký	k1gMnSc7	Zápotocký
<g/>
,	,	kIx,	,
Antonínem	Antonín	k1gMnSc7	Antonín
Novotným	Novotný	k1gMnSc7	Novotný
<g/>
,	,	kIx,	,
položil	položit	k5eAaPmAgMnS	položit
věnec	věnec	k1gInSc4	věnec
na	na	k7c4	na
hrob	hrob	k1gInSc4	hrob
neznámého	známý	k2eNgMnSc2d1	neznámý
vojína	vojín	k1gMnSc2	vojín
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
památníku	památník	k1gInSc6	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČKD	ČKD	kA	ČKD
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Lidice	Lidice	k1gInPc4	Lidice
a	a	k8xC	a
jednotné	jednotný	k2eAgNnSc4d1	jednotné
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
družstvo	družstvo	k1gNnSc4	družstvo
v	v	k7c6	v
Horních	horní	k2eAgFnPc6d1	horní
Salibách	Saliba	k1gFnPc6	Saliba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
výročí	výročí	k1gNnSc2	výročí
60	[number]	k4	60
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Ho	on	k3xPp3gNnSc2	on
Či	či	k9wB	či
Minovy	Minovy	k?	Minovy
návštěvy	návštěva	k1gFnPc1	návštěva
odhalena	odhalen	k2eAgFnSc1d1	odhalena
v	v	k7c6	v
Horních	horní	k2eAgFnPc6d1	horní
Salibách	Saliba	k1gFnPc6	Saliba
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
navštívil	navštívit	k5eAaPmAgInS	navštívit
taktéž	taktéž	k?	taktéž
pionýrský	pionýrský	k2eAgInSc1d1	pionýrský
tábor	tábor	k1gInSc1	tábor
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Nové	Nové	k2eAgNnSc1d1	Nové
Strašecí	Strašecí	k1gNnSc1	Strašecí
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
vietnamskými	vietnamský	k2eAgFnPc7d1	vietnamská
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
Chrastavě	chrastavě	k6eAd1	chrastavě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
poslal	poslat	k5eAaPmAgInS	poslat
Vietnam	Vietnam	k1gInSc1	Vietnam
do	do	k7c2	do
Chrastavy	Chrastavy	k?	Chrastavy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
dětí	dítě	k1gFnPc2	dítě
na	na	k7c4	na
několikaletý	několikaletý	k2eAgInSc4d1	několikaletý
pobyt	pobyt	k1gInSc4	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Vietnamské	vietnamský	k2eAgFnPc1d1	vietnamská
děti	dítě	k1gFnPc1	dítě
byly	být	k5eAaImAgFnP	být
ubytovány	ubytovat	k5eAaPmNgFnP	ubytovat
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
výchovného	výchovný	k2eAgInSc2d1	výchovný
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
chodily	chodit	k5eAaImAgFnP	chodit
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
místními	místní	k2eAgFnPc7d1	místní
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
školou	škola	k1gFnSc7	škola
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
ambasáda	ambasáda	k1gFnSc1	ambasáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
plánovala	plánovat	k5eAaImAgFnS	plánovat
zhotovení	zhotovení	k1gNnSc4	zhotovení
pomníku	pomník	k1gInSc2	pomník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
pobyt	pobyt	k1gInSc4	pobyt
vietnamských	vietnamský	k2eAgFnPc2d1	vietnamská
dětí	dítě	k1gFnPc2	dítě
připomínal	připomínat	k5eAaImAgInS	připomínat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DUIKER	DUIKER	kA	DUIKER
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
<g/>
.	.	kIx.	.
</s>
<s>
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Min	min	kA	min
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
583	[number]	k4	583
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7341	[number]	k4	7341
<g/>
-	-	kIx~	-
<g/>
166	[number]	k4	166
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Minova	Minův	k2eAgFnSc1d1	Minova
stezka	stezka	k1gFnSc1	stezka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ho	on	k3xPp3gNnSc2	on
Či	či	k8xC	či
Min	mina	k1gFnPc2	mina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
