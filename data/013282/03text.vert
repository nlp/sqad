<p>
<s>
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
a	a	k8xC	a
tma	tma	k1gFnSc1	tma
je	být	k5eAaImIp3nS	být
tragická	tragický	k2eAgFnSc1d1	tragická
novela	novela	k1gFnSc1	novela
Jana	Jan	k1gMnSc2	Jan
Otčenáška	Otčenášek	k1gMnSc2	Otčenášek
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
okupace	okupace	k1gFnSc2	okupace
Československa	Československo	k1gNnSc2	Československo
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
období	období	k1gNnSc6	období
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
tma	tma	k6eAd1	tma
v	v	k7c6	v
názvu	název	k1gInSc6	název
má	mít	k5eAaImIp3nS	mít
právě	právě	k6eAd1	právě
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
dusné	dusný	k2eAgNnSc4d1	dusné
prostředí	prostředí	k1gNnSc4	prostředí
stanného	stanný	k2eAgNnSc2d1	stanné
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
a	a	k8xC	a
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
byl	být	k5eAaImAgInS	být
Jiřím	Jiří	k1gMnSc6	Jiří
Weissem	Weiss	k1gMnSc7	Weiss
natočen	natočit	k5eAaBmNgInS	natočit
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
televizní	televizní	k2eAgFnSc1d1	televizní
adaptace	adaptace	k1gFnSc1	adaptace
pod	pod	k7c7	pod
režijní	režijní	k2eAgFnSc7d1	režijní
taktovkou	taktovka	k1gFnSc7	taktovka
Karla	Karla	k1gFnSc1	Karla
Smyczka	Smyczka	k1gFnSc1	Smyczka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jenž	k3xRgInSc2	jenž
pohledu	pohled	k1gInSc2	pohled
příběh	příběh	k1gInSc1	příběh
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
sledujeme	sledovat	k5eAaImIp1nP	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
maturitu	maturita	k1gFnSc4	maturita
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
mysl	mysl	k1gFnSc1	mysl
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
děvčatech	děvče	k1gNnPc6	děvče
a	a	k8xC	a
nastávající	nastávající	k1gFnSc1	nastávající
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
procházek	procházka	k1gFnPc2	procházka
potká	potkat	k5eAaPmIp3nS	potkat
ve	v	k7c6	v
večerním	večerní	k2eAgInSc6d1	večerní
parku	park	k1gInSc6	park
mladou	mladý	k2eAgFnSc4d1	mladá
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
čímsi	cosi	k3yInSc7	cosi
rozrušena	rozrušit	k5eAaPmNgFnS	rozrušit
–	–	k?	–
opatrně	opatrně	k6eAd1	opatrně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
začne	začít	k5eAaPmIp3nS	začít
konverzaci	konverzace	k1gFnSc4	konverzace
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
židovka	židovka	k1gFnSc1	židovka
jménem	jméno	k1gNnSc7	jméno
Ester	Ester	k1gFnSc2	Ester
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nedostavila	dostavit	k5eNaPmAgFnS	dostavit
do	do	k7c2	do
transportu	transport	k1gInSc2	transport
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
se	se	k3xPyFc4	se
s	s	k7c7	s
náhlou	náhlý	k2eAgFnSc7d1	náhlá
rozhodností	rozhodnost	k1gFnSc7	rozhodnost
odhodlává	odhodlávat	k5eAaImIp3nS	odhodlávat
ukrýt	ukrýt	k5eAaPmF	ukrýt
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
pokoji	pokoj	k1gInSc6	pokoj
vedle	vedle	k7c2	vedle
krejčovské	krejčovský	k2eAgFnSc2d1	krejčovská
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
republika	republika	k1gFnSc1	republika
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
zastupující	zastupující	k2eAgFnSc4d1	zastupující
říšského	říšský	k2eAgMnSc2d1	říšský
protektora	protektor	k1gMnSc2	protektor
Heydricha	Heydrich	k1gMnSc2	Heydrich
–	–	k?	–
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
nařízení	nařízení	k1gNnSc4	nařízení
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgInPc4	který
patří	patřit	k5eAaImIp3nS	patřit
zákaz	zákaz	k1gInSc1	zákaz
nočního	noční	k2eAgNnSc2d1	noční
vycházení	vycházení	k1gNnSc2	vycházení
a	a	k8xC	a
kruté	krutý	k2eAgInPc4d1	krutý
tresty	trest	k1gInPc4	trest
pro	pro	k7c4	pro
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přechovávají	přechovávat	k5eAaImIp3nP	přechovávat
nepřátele	nepřítel	k1gMnPc4	nepřítel
třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
svého	své	k1gNnSc2	své
skutku	skutek	k1gInSc2	skutek
přesto	přesto	k8xC	přesto
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
litovat	litovat	k5eAaImF	litovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
myslí	myslet	k5eAaImIp3nS	myslet
na	na	k7c4	na
Ester	Ester	k1gFnSc4	Ester
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
políbí	políbit	k5eAaPmIp3nP	políbit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
knihy	kniha	k1gFnSc2	kniha
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
mezi	mezi	k7c7	mezi
mladou	mladý	k2eAgFnSc7d1	mladá
láskou	láska	k1gFnSc7	láska
Pavla	Pavel	k1gMnSc2	Pavel
a	a	k8xC	a
Ester	Ester	k1gFnSc2	Ester
a	a	k8xC	a
teroru	teror	k1gInSc2	teror
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
<g/>
.	.	kIx.	.
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
vláda	vláda	k1gFnSc1	vláda
popravuje	popravovat	k5eAaImIp3nS	popravovat
desítky	desítka	k1gFnPc4	desítka
lidí	člověk	k1gMnPc2	člověk
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
a	a	k8xC	a
plakáty	plakát	k1gInPc4	plakát
s	s	k7c7	s
jejich	jejich	k3xOp3gNnPc7	jejich
jmény	jméno	k1gNnPc7	jméno
vylepuje	vylepovat	k5eAaImIp3nS	vylepovat
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
dva	dva	k4xCgMnPc1	dva
milenci	milenec	k1gMnPc1	milenec
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
místnosti	místnost	k1gFnSc6	místnost
a	a	k8xC	a
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
si	se	k3xPyFc3	se
však	však	k9	však
je	být	k5eAaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
rizika	riziko	k1gNnSc2	riziko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
a	a	k8xC	a
o	o	k7c6	o
celé	celý	k2eAgFnSc6d1	celá
situaci	situace	k1gFnSc6	situace
mimo	mimo	k7c4	mimo
jejich	jejich	k3xOp3gFnSc4	jejich
místnost	místnost	k1gFnSc4	místnost
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
i	i	k9	i
Ester	ester	k1gInSc1	ester
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
dokonce	dokonce	k9	dokonce
plánuje	plánovat	k5eAaImIp3nS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Pavla	Pavla	k1gFnSc1	Pavla
neuvrhla	uvrhnout	k5eNaPmAgFnS	uvrhnout
do	do	k7c2	do
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ukrývala	ukrývat	k5eAaImAgFnS	ukrývat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bydlel	bydlet	k5eAaImAgMnS	bydlet
muž	muž	k1gMnSc1	muž
zvaný	zvaný	k2eAgInSc1d1	zvaný
Rejsek	rejsek	k1gInSc1	rejsek
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
místní	místní	k2eAgMnSc1d1	místní
kolaborant	kolaborant	k1gMnSc1	kolaborant
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
všeho	všecek	k3xTgNnSc2	všecek
strkal	strkat	k5eAaImAgInS	strkat
nos	nos	k1gInSc1	nos
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
si	se	k3xPyFc3	se
nechat	nechat	k5eAaPmF	nechat
do	do	k7c2	do
krejčovny	krejčovna	k1gFnSc2	krejčovna
ušít	ušít	k5eAaPmF	ušít
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
divné	divný	k2eAgFnSc3d1	divná
řeči	řeč	k1gFnSc3	řeč
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
obvykle	obvykle	k6eAd1	obvykle
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
Pavlův	Pavlův	k2eAgMnSc1d1	Pavlův
otec	otec	k1gMnSc1	otec
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pavel	Pavel	k1gMnSc1	Pavel
v	v	k7c6	v
pokojíku	pokojík	k1gInSc6	pokojík
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
dívku	dívka	k1gFnSc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
večer	večer	k1gInSc1	večer
<g/>
,	,	kIx,	,
když	když	k8xS	když
odcházel	odcházet	k5eAaImAgMnS	odcházet
Pavel	Pavel	k1gMnSc1	Pavel
za	za	k7c4	za
Ester	Ester	k1gFnSc4	Ester
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
předsíni	předsíň	k1gFnSc6	předsíň
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
mu	on	k3xPp3gMnSc3	on
balíček	balíček	k1gInSc1	balíček
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
jen	jen	k9	jen
vylekaně	vylekaně	k6eAd1	vylekaně
opakoval	opakovat	k5eAaImAgMnS	opakovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hlavně	hlavně	k9	hlavně
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nesmí	smět	k5eNaImIp3nS	smět
dozvědět	dozvědět	k5eAaPmF	dozvědět
maminka	maminka	k1gFnSc1	maminka
<g/>
,	,	kIx,	,
víš	vědět	k5eAaImIp2nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemocná	mocný	k2eNgFnSc1d1	mocný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Potom	potom	k6eAd1	potom
Pavel	Pavel	k1gMnSc1	Pavel
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
večer	večer	k1gInSc1	večer
mu	on	k3xPp3gMnSc3	on
Ester	Ester	k1gFnSc1	Ester
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
chtěla	chtít	k5eAaImAgFnS	chtít
mít	mít	k5eAaImF	mít
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Pomilovali	pomilovat	k5eAaPmAgMnP	pomilovat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
jindy	jindy	k6eAd1	jindy
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
ho	on	k3xPp3gMnSc4	on
probudily	probudit	k5eAaPmAgFnP	probudit
rány	rána	k1gFnPc1	rána
z	z	k7c2	z
děla	dělo	k1gNnSc2	dělo
a	a	k8xC	a
kulometu	kulomet	k1gInSc2	kulomet
<g/>
.	.	kIx.	.
</s>
<s>
Zděsil	zděsit	k5eAaPmAgMnS	zděsit
se	se	k3xPyFc4	se
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
hned	hned	k6eAd1	hned
utíkat	utíkat	k5eAaImF	utíkat
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
domácí	domácí	k2eAgInSc4d1	domácí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
klíče	klíč	k1gInPc4	klíč
od	od	k7c2	od
vrat	vrata	k1gNnPc2	vrata
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
nepustil	pustit	k5eNaPmAgMnS	pustit
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
rozednívalo	rozednívat	k5eAaImAgNnS	rozednívat
<g/>
,	,	kIx,	,
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
před	před	k7c7	před
pavlačovým	pavlačový	k2eAgInSc7d1	pavlačový
domem	dům	k1gInSc7	dům
fašistické	fašistický	k2eAgNnSc4d1	fašistické
auto	auto	k1gNnSc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Rejsek	rejsek	k1gInSc1	rejsek
začal	začít	k5eAaPmAgInS	začít
lítat	lítat	k5eAaImF	lítat
a	a	k8xC	a
křičel	křičet	k5eAaImAgMnS	křičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tady	tady	k6eAd1	tady
to	ten	k3xDgNnSc4	ten
máte	mít	k5eAaImIp2nP	mít
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
jste	být	k5eAaImIp2nP	být
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
nás	my	k3xPp1nPc4	my
všechny	všechen	k3xTgInPc1	všechen
popraví	popravit	k5eAaPmIp3nP	popravit
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
nechci	chtít	k5eNaImIp1nS	chtít
umřít	umřít	k5eAaPmF	umřít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Lidé	člověk	k1gMnPc1	člověk
nevěděli	vědět	k5eNaImAgMnP	vědět
o	o	k7c6	o
čem	co	k3yQnSc6	co
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c4	o
Ester	Ester	k1gFnSc4	Ester
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
dobývat	dobývat	k5eAaImF	dobývat
do	do	k7c2	do
pokojíku	pokojík	k1gInSc2	pokojík
<g/>
.	.	kIx.	.
</s>
<s>
Ester	Ester	k1gFnSc1	Ester
stála	stát	k5eAaImAgFnS	stát
uprostřed	uprostřed	k7c2	uprostřed
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
tušíc	tušit	k5eAaImSgNnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
konec	konec	k1gInSc1	konec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
utekla	utéct	k5eAaPmAgFnS	utéct
spojovacími	spojovací	k2eAgFnPc7d1	spojovací
dveřmi	dveře	k1gFnPc7	dveře
do	do	k7c2	do
krejčovny	krejčovna	k1gFnSc2	krejčovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
obeznámený	obeznámený	k2eAgMnSc1d1	obeznámený
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
úkrytu	úkryt	k1gInSc6	úkryt
schoval	schovat	k5eAaPmAgMnS	schovat
pod	pod	k7c4	pod
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
Rejska	rejsek	k1gMnSc4	rejsek
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Ester	Ester	k1gFnSc1	Ester
ale	ale	k9	ale
zazmatkovala	zazmatkovat	k5eAaPmAgFnS	zazmatkovat
a	a	k8xC	a
utekla	utéct	k5eAaPmAgFnS	utéct
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Stihla	stihnout	k5eAaPmAgFnS	stihnout
zaběhnout	zaběhnout	k5eAaPmF	zaběhnout
za	za	k7c4	za
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
průchodem	průchod	k1gInSc7	průchod
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
už	už	k6eAd1	už
zpozorována	zpozorován	k2eAgFnSc1d1	zpozorována
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Viděla	vidět	k5eAaImAgFnS	vidět
park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
viděla	vidět	k5eAaImAgFnS	vidět
rodný	rodný	k2eAgInSc4d1	rodný
domek	domek	k1gInSc4	domek
<g/>
,	,	kIx,	,
zahrádku	zahrádka	k1gFnSc4	zahrádka
<g/>
,	,	kIx,	,
tátu	táta	k1gMnSc4	táta
s	s	k7c7	s
mámou	máma	k1gFnSc7	máma
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
probleskovalo	probleskovat	k5eAaImAgNnS	probleskovat
skrze	skrze	k?	skrze
všeobjímající	všeobjímající	k2eAgFnSc4d1	všeobjímající
tmu	tma	k1gFnSc4	tma
–	–	k?	–
pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
vojáci	voják	k1gMnPc1	voják
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nedozvěděl	dozvědět	k5eNaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
co	co	k8xS	co
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
událo	udát	k5eAaPmAgNnS	udát
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
okolnosti	okolnost	k1gFnPc1	okolnost
byly	být	k5eAaImAgFnP	být
jasné	jasný	k2eAgFnPc1d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
osaměle	osaměle	k6eAd1	osaměle
trčí	trčet	k5eAaImIp3nS	trčet
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pokoji	pokoj	k1gInSc6	pokoj
a	a	k8xC	a
tone	tonout	k5eAaImIp3nS	tonout
v	v	k7c6	v
pochmurných	pochmurný	k2eAgFnPc6d1	pochmurná
myšlenkách	myšlenka	k1gFnPc6	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
ho	on	k3xPp3gNnSc4	on
vytrhne	vytrhnout	k5eAaPmIp3nS	vytrhnout
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Esteřin	Esteřin	k2eAgInSc4d1	Esteřin
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
Ty	ty	k3xPp2nSc1	ty
musíš	muset	k5eAaImIp2nS	muset
žít	žít	k5eAaImF	žít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Kniha	kniha	k1gFnSc1	kniha
končí	končit	k5eAaImIp3nS	končit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
odhodlá	odhodlat	k5eAaPmIp3nS	odhodlat
jít	jít	k5eAaImF	jít
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobná	podobný	k2eAgNnPc4d1	podobné
díla	dílo	k1gNnPc4	dílo
české	český	k2eAgFnSc2d1	Česká
poválečné	poválečný	k2eAgFnSc2d1	poválečná
literatury	literatura	k1gFnSc2	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Modlitba	modlitba	k1gFnSc1	modlitba
pro	pro	k7c4	pro
Kateřinu	Kateřina	k1gFnSc4	Kateřina
Horovitzovou	Horovitzová	k1gFnSc4	Horovitzová
od	od	k7c2	od
Arnošta	Arnošt	k1gMnSc2	Arnošt
Lustiga	Lustig	k1gMnSc2	Lustig
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
židovské	židovský	k2eAgFnSc2d1	židovská
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
chvíli	chvíle	k1gFnSc4	chvíle
odhodlala	odhodlat	k5eAaPmAgFnS	odhodlat
postavit	postavit	k5eAaPmF	postavit
nacistům	nacista	k1gMnPc3	nacista
</s>
</p>
<p>
<s>
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
od	od	k7c2	od
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
nacismu	nacismus	k1gInSc2	nacismus
mění	měnit	k5eAaImIp3nS	měnit
relativně	relativně	k6eAd1	relativně
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c4	v
zrůdu	zrůda	k1gFnSc4	zrůda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
zavraždit	zavraždit	k5eAaPmF	zavraždit
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Otčenášek	Otčenášek	k1gMnSc1	Otčenášek
</s>
</p>
<p>
<s>
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
a	a	k8xC	a
tma	tma	k1gFnSc1	tma
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
a	a	k8xC	a
tma	tma	k1gFnSc1	tma
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgFnSc1d1	televizní
adaptace	adaptace	k1gFnSc1	adaptace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
a	a	k8xC	a
tma	tma	k1gFnSc1	tma
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
Jana	Jan	k1gMnSc2	Jan
Franka	Frank	k1gMnSc2	Frank
Fischera	Fischer	k1gMnSc2	Fischer
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
o	o	k7c6	o
prologu	prolog	k1gInSc6	prolog
<g/>
,	,	kIx,	,
2	[number]	k4	2
dílech	díl	k1gInPc6	díl
a	a	k8xC	a
epilogu	epilog	k1gInSc6	epilog
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
skladatele	skladatel	k1gMnSc2	skladatel
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
Jana	Jan	k1gMnSc2	Jan
Otčenáška	Otčenášek	k1gMnSc2	Otčenášek
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
14	[number]	k4	14
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
-	-	kIx~	-
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
