<s>
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
a	a	k8xC	a
tma	tma	k1gFnSc1	tma
je	být	k5eAaImIp3nS	být
tragická	tragický	k2eAgFnSc1d1	tragická
novela	novela	k1gFnSc1	novela
Jana	Jan	k1gMnSc2	Jan
Otčenáška	Otčenášek	k1gMnSc2	Otčenášek
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
okupace	okupace	k1gFnSc2	okupace
Československa	Československo	k1gNnSc2	Československo
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
období	období	k1gNnSc6	období
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
<g/>
.	.	kIx.	.
</s>
