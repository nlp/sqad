<s>
Michala	Michala	k1gFnSc1
Kvapilová	Kvapilová	k1gFnSc1
</s>
<s>
Michala	Michala	k1gFnSc1
Kvapilová	Kvapilová	k1gFnSc1
</s>
<s>
Michala	Michala	k1gFnSc1
KvapilováOsobní	KvapilováOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1990	#num#	k4
(	(	kIx(
<g/>
31	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
182	#num#	k4
cm	cm	kA
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Michala	Michala	k1gFnSc1
Kvapilová	Kvapilová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1990	#num#	k4
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
volejbalistka	volejbalistka	k1gFnSc1
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
reprezentantka	reprezentantka	k1gFnSc1
v	v	k7c6
šestkovém	šestkový	k2eAgInSc6d1
i	i	k8xC
plážovém	plážový	k2eAgInSc6d1
volejbalu	volejbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
<g/>
:	:	kIx,
vicemistryně	vicemistryně	k1gFnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
plážovém	plážový	k2eAgInSc6d1
volejbalu	volejbal	k1gInSc6
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
mistryně	mistryně	k1gFnSc1
ČR	ČR	kA
v	v	k7c6
plážovém	plážový	k2eAgInSc6d1
volejbalu	volejbal	k1gInSc6
v	v	k7c4
2019	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
šestkovém	šestkový	k2eAgInSc6d1
volejbalu	volejbal	k1gInSc6
–	–	k?
nejvíce	hodně	k6eAd3,k6eAd1
bodující	bodující	k2eAgFnSc1d1
hráčka	hráčka	k1gFnSc1
v	v	k7c6
bundeslize	bundesliga	k1gFnSc6
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
bodující	bodující	k2eAgFnSc1d1
hráčka	hráčka	k1gFnSc1
české	český	k2eAgFnSc2d1
extraligy	extraliga	k1gFnSc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
×	×	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
extralize	extraliga	k1gFnSc6
žen	žena	k1gFnPc2
ČR	ČR	kA
za	za	k7c7
PVK	PVK	kA
Olymp	Olymp	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
dcerou	dcera	k1gFnSc7
volejbalové	volejbalový	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Jan	Jan	k1gMnSc1
Kvapil	Kvapil	k1gMnSc1
byl	být	k5eAaImAgMnS
od	od	k7c2
1986	#num#	k4
do	do	k7c2
1994	#num#	k4
extraligový	extraligový	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Dukly	Dukla	k1gFnSc2
Liberec	Liberec	k1gInSc1
a	a	k8xC
Spolchemie	Spolchemie	k1gFnSc1
Ústí	ústí	k1gNnSc2
n.	n.	k?
L.	L.	kA
<g/>
,	,	kIx,
reprezentant	reprezentant	k1gMnSc1
ČSSR	ČSSR	kA
resp.	resp.	kA
ČSFR	ČSFR	kA
<g/>
,	,	kIx,
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
hráč	hráč	k1gMnSc1
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
belgické	belgický	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Kvapilová	Kvapilová	k1gFnSc1
(	(	kIx(
<g/>
roz	roz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ducháčková	Ducháčková	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
hráčka	hráčka	k1gFnSc1
české	český	k2eAgFnSc2d1
1	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
za	za	k7c4
Lokomotivu	lokomotiva	k1gFnSc4
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Technickou	technický	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
Liberec	Liberec	k1gInSc1
a	a	k8xC
Efektu	efekt	k1gInSc2
Liberec	Liberec	k1gInSc4
<g/>
,	,	kIx,
hrála	hrát	k5eAaImAgFnS
i	i	k9
za	za	k7c4
belgický	belgický	k2eAgInSc4d1
celek	celek	k1gInSc4
VTC	VTC	kA
Tournai	Tournai	k1gNnSc1
(	(	kIx(
<g/>
vyhlášena	vyhlášen	k2eAgFnSc1d1
nejužitečnější	užitečný	k2eAgFnSc7d3
hráčkou	hráčka	k1gFnSc7
Belgického	belgický	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c6
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
dětství	dětství	k1gNnSc2
se	se	k3xPyFc4
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
sportu	sport	k1gInSc3
<g/>
,	,	kIx,
např.	např.	kA
gymnastice	gymnastika	k1gFnSc6
<g/>
,	,	kIx,
atletice	atletika	k1gFnSc6
<g/>
,	,	kIx,
plavání	plavání	k1gNnSc6
<g/>
,	,	kIx,
krasobruslení	krasobruslení	k1gNnSc6
a	a	k8xC
dalším	další	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
lze	lze	k6eAd1
hledat	hledat	k5eAaImF
základy	základ	k1gInPc4
její	její	k3xOp3gFnSc2
dobré	dobrý	k2eAgFnSc2d1
koordinace	koordinace	k1gFnSc2
a	a	k8xC
fyzické	fyzický	k2eAgFnSc2d1
připravenosti	připravenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
volejbalem	volejbal	k1gInSc7
soutěžně	soutěžně	k6eAd1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
12	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
SK	Sk	kA
Ještědská	ještědský	k2eAgFnSc1d1
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
hrála	hrát	k5eAaImAgFnS
jako	jako	k9
kadetka	kadetka	k1gFnSc1
za	za	k7c4
VK	VK	kA
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
juniorka	juniorka	k1gFnSc1
za	za	k7c7
PVK	PVK	kA
Olymp	Olymp	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
souběžně	souběžně	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
i	i	k9
extraligu	extraliga	k1gFnSc4
žen	žena	k1gFnPc2
za	za	k7c7
PVK	PVK	kA
Olymp	Olymp	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
gymnázia	gymnázium	k1gNnSc2
získala	získat	k5eAaPmAgFnS
stipendium	stipendium	k1gNnSc4
na	na	k7c6
americké	americký	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
VCU	VCU	kA
Richmond	Richmond	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
v	v	k7c6
soutěži	soutěž	k1gFnSc6
NCAA	NCAA	kA
dostala	dostat	k5eAaPmAgFnS
ocenění	ocenění	k1gNnSc4
Rookie	Rookie	k1gFnSc2
of	of	k?
the	the	k?
Year	Year	k1gInSc1
a	a	k8xC
ocenění	ocenění	k1gNnSc1
1	#num#	k4
<g/>
st	st	kA
Team	team	k1gInSc1
konference	konference	k1gFnSc1
CAA	CAA	kA
–	–	k?
nejlepší	dobrý	k2eAgFnSc1d3
smečařka	smečařka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
Čech	Čechy	k1gFnPc2
a	a	k8xC
hrála	hrát	k5eAaImAgFnS
extraligu	extraliga	k1gFnSc4
žen	žena	k1gFnPc2
postupně	postupně	k6eAd1
za	za	k7c4
TJ	tj	kA
Tatran	Tatran	k1gInSc4
Střešovice	Střešovice	k1gFnSc2
a	a	k8xC
PVK	PVK	kA
Olymp	Olymp	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
×	×	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
bodující	bodující	k2eAgFnSc1d1
hráčka	hráčka	k1gFnSc1
ročníku	ročník	k1gInSc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
německé	německý	k2eAgFnSc6d1
volejbalové	volejbalový	k2eAgFnSc6d1
bundeslize	bundesliga	k1gFnSc6
hrála	hrát	k5eAaImAgFnS
v	v	k7c6
SC	SC	kA
Potsdam	Potsdam	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
bodující	bodující	k2eAgFnSc1d1
hráčka	hráčka	k1gFnSc1
ročníku	ročník	k1gInSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
za	za	k7c4
USC	USC	kA
Münster	Münster	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
soutěži	soutěž	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ČR	ČR	kA
reprezentovala	reprezentovat	k5eAaImAgFnS
v	v	k7c6
šestkovém	šestkový	k2eAgInSc6d1
volejbalu	volejbal	k1gInSc6
na	na	k7c4
ME	ME	kA
kadetek	kadetek	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
MS	MS	kA
juniorek	juniorka	k1gFnPc2
a	a	k8xC
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vždy	vždy	k6eAd1
současně	současně	k6eAd1
s	s	k7c7
šestkovým	šestkový	k2eAgInSc7d1
volejbalem	volejbal	k1gInSc7
se	se	k3xPyFc4
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
i	i	k8xC
plážovému	plážový	k2eAgInSc3d1
volejbalu	volejbal	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
ČR	ČR	kA
reprezentovala	reprezentovat	k5eAaImAgFnS
na	na	k7c6
světových	světový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
(	(	kIx(
<g/>
MS	MS	kA
U19	U19	k1gFnSc2
v	v	k7c6
Haagu	Haag	k1gInSc6
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
s	s	k7c7
Barborou	Barbora	k1gFnSc7
Hermannovou	Hermannová	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
pak	pak	k6eAd1
na	na	k7c6
mnoha	mnoho	k4c6
zahraničních	zahraniční	k2eAgInPc6d1
i	i	k8xC
domácích	domácí	k2eAgInPc6d1
turnajích	turnaj	k1gInPc6
světového	světový	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
hrála	hrát	k5eAaImAgFnS
s	s	k7c7
Kristýnou	Kristýna	k1gFnSc7
Kolocovou-Hoidarovou	Kolocovou-Hoidarová	k1gFnSc7
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
dvojice	dvojice	k1gFnSc1
Maki	Mak	k1gFnSc2
&	&	k?
Kiki	Kik	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
bylo	být	k5eAaImAgNnS
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
ME	ME	kA
v	v	k7c6
Jurmale	Jurmala	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
MS	MS	kA
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
účast	účast	k1gFnSc1
ve	v	k7c4
World	World	k1gInSc4
Tour	Tour	k1gInSc1
Finals	Finals	k1gInSc1
2017	#num#	k4
v	v	k7c6
Hamburku	Hamburk	k1gInSc6
(	(	kIx(
<g/>
nejlepších	dobrý	k2eAgFnPc2d3
12	#num#	k4
týmů	tým	k1gInPc2
z	z	k7c2
žebříčku	žebříček	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
hraje	hrát	k5eAaImIp3nS
s	s	k7c7
Michaelou	Michaela	k1gFnSc7
Kubíčkovou	Kubíčková	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
úspěchy	úspěch	k1gInPc4
této	tento	k3xDgFnSc2
dvojice	dvojice	k1gFnSc2
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k7c2
MČR	MČR	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
třetí	třetí	k4xOgFnSc6
za	za	k7c2
sebou	se	k3xPyFc7
mistryně	mistryně	k1gFnPc4
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
ME	ME	kA
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
turnaji	turnaj	k1gInSc6
WT	WT	kA
<g/>
**	**	k?
v	v	k7c4
Qoidong	Qoidong	k1gInSc4
v	v	k7c6
Číně	Čína	k1gFnSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
bojují	bojovat	k5eAaImIp3nP
o	o	k7c6
nominaci	nominace	k1gFnSc6
na	na	k7c4
OH	OH	kA
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.frekvence1.cz/clanky/zpravy/plazove-volejbalistky-kolocova-s-kvapilovou-budou-hrat-o-evropsky-titul.shtml	https://www.frekvence1.cz/clanky/zpravy/plazove-volejbalistky-kolocova-s-kvapilovou-budou-hrat-o-evropsky-titul.shtml	k1gInSc1
<g/>
↑	↑	k?
KVAPILOVÁ	Kvapilová	k1gFnSc1
Michala	Michala	k1gFnSc1
<g/>
.	.	kIx.
www.cvf.cz	www.cvf.cz	k1gInSc1
<g/>
cvs	cvs	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
