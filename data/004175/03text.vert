<s>
Petr	Petr	k1gMnSc1	Petr
Zelenka	Zelenka	k1gMnSc1	Zelenka
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1967	[number]	k4	1967
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
scenáristů	scenárista	k1gMnPc2	scenárista
Otto	Otto	k1gMnSc1	Otto
Zelenky	zelenka	k1gFnSc2	zelenka
a	a	k8xC	a
Bohumily	Bohumila	k1gFnSc2	Bohumila
Zelenkové	Zelenkové	k2eAgFnSc2d1	Zelenkové
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
choreografky	choreografka	k1gFnSc2	choreografka
Kláry	Klára	k1gFnSc2	Klára
Lidové	lidový	k2eAgFnSc2d1	lidová
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sám	sám	k3xTgMnSc1	sám
režíroval	režírovat	k5eAaImAgInS	režírovat
v	v	k7c6	v
Dejvickém	dejvický	k2eAgNnSc6d1	Dejvické
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
Cenu	cena	k1gFnSc4	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nastudována	nastudovat	k5eAaBmNgFnS	nastudovat
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
scénách	scéna	k1gFnPc6	scéna
(	(	kIx(	(
<g/>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Nitra	Nitra	k1gFnSc1	Nitra
<g/>
,	,	kIx,	,
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
,	,	kIx,	,
Tübingen	Tübingen	k1gInSc1	Tübingen
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
českých	český	k2eAgNnPc2d1	české
divadel	divadlo	k1gNnPc2	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaBmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
úspěch	úspěch	k1gInSc4	úspěch
původní	původní	k2eAgFnSc2d1	původní
divadelní	divadelní	k2eAgFnSc2d1	divadelní
inscenace	inscenace	k1gFnSc2	inscenace
nezopakoval	zopakovat	k5eNaPmAgMnS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
Zelenkovou	Zelenkův	k2eAgFnSc7d1	Zelenkova
hrou	hra	k1gFnSc7	hra
je	být	k5eAaImIp3nS	být
Teremin	Teremin	k1gInSc1	Teremin
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
životním	životní	k2eAgInSc7d1	životní
příběhem	příběh	k1gInSc7	příběh
Lva	Lev	k1gMnSc2	Lev
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Těrmena	Těrmen	k2eAgFnSc1d1	Těrmen
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvedený	uvedený	k2eAgInSc1d1	uvedený
v	v	k7c6	v
autorově	autorův	k2eAgFnSc6d1	autorova
režii	režie	k1gFnSc6	režie
v	v	k7c6	v
Dejvickém	dejvický	k2eAgNnSc6d1	Dejvické
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
hru	hra	k1gFnSc4	hra
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
Sergej	Sergej	k1gMnSc1	Sergej
Fedotov	Fedotov	k1gInSc4	Fedotov
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
U	u	k7c2	u
mostu	most	k1gInSc2	most
v	v	k7c6	v
Permi	Per	k1gFnPc7	Per
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejnovější	nový	k2eAgFnSc7d3	nejnovější
hrou	hra	k1gFnSc7	hra
uvedenou	uvedený	k2eAgFnSc7d1	uvedená
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
jsou	být	k5eAaImIp3nP	být
Ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Režíroval	režírovat	k5eAaImAgMnS	režírovat
také	také	k9	také
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Terapie	terapie	k1gFnSc2	terapie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Marthou	Martha	k1gFnSc7	Martha
Issovou	Issová	k1gFnSc7	Issová
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Mádlem	mádlo	k1gNnSc7	mádlo
přípravy	příprava	k1gFnSc2	příprava
klipu	klip	k1gInSc2	klip
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
"	"	kIx"	"
<g/>
Přemluv	přemluva	k1gFnPc2	přemluva
bábu	bába	k1gFnSc4	bába
a	a	k8xC	a
dědu	děda	k1gMnSc4	děda
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
nevolí	volit	k5eNaImIp3nS	volit
levici	levice	k1gFnSc4	levice
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
který	který	k3yQgMnSc1	který
zvedl	zvednout	k5eAaPmAgMnS	zvednout
vlnu	vlna	k1gFnSc4	vlna
vášní	vášeň	k1gFnPc2	vášeň
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tématem	téma	k1gNnSc7	téma
polemiky	polemika	k1gFnSc2	polemika
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
osobních	osobní	k2eAgInPc2d1	osobní
blogů	blog	k1gInPc2	blog
i	i	k8xC	i
internetových	internetový	k2eAgNnPc2d1	internetové
periodik	periodikum	k1gNnPc2	periodikum
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
"	"	kIx"	"
<g/>
věcné	věcný	k2eAgInPc4d1	věcný
argumenty	argument	k1gInPc4	argument
míchá	míchat	k5eAaImIp3nS	míchat
s	s	k7c7	s
úšklebky	úšklebek	k1gInPc7	úšklebek
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
balancují	balancovat	k5eAaImIp3nP	balancovat
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
vulgárního	vulgární	k2eAgNnSc2d1	vulgární
zesměšňování	zesměšňování	k1gNnSc2	zesměšňování
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
starých	starý	k2eAgMnPc2d1	starý
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
volebním	volební	k2eAgInSc7d1	volební
klipem	klip	k1gInSc7	klip
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Sarah	Sarah	k1gFnSc1	Sarah
Silverman	Silverman	k1gMnSc1	Silverman
vyzývala	vyzývat	k5eAaImAgFnS	vyzývat
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
svou	svůj	k3xOyFgFnSc4	svůj
inspiraci	inspirace	k1gFnSc4	inspirace
přiznali	přiznat	k5eAaPmAgMnP	přiznat
až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
premiéru	premiéra	k1gFnSc4	premiéra
opera	opera	k1gFnSc1	opera
Aleše	Aleš	k1gMnSc2	Aleš
Březiny	Březina	k1gFnSc2	Březina
Toufar	Toufar	k1gMnSc1	Toufar
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Petr	Petr	k1gMnSc1	Petr
Zelenka	Zelenka	k1gMnSc1	Zelenka
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
jeho	jeho	k3xOp3gInSc1	jeho
film	film	k1gInSc1	film
Ztraceni	ztracen	k2eAgMnPc1d1	ztracen
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
o	o	k7c6	o
papouškovi	papoušek	k1gMnSc6	papoušek
francouzského	francouzský	k2eAgMnSc2d1	francouzský
premiéra	premiér	k1gMnSc2	premiér
Édouarda	Édouard	k1gMnSc2	Édouard
Daladiera	Daladier	k1gMnSc2	Daladier
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
statí	stať	k1gFnSc7	stať
Mnichovský	mnichovský	k2eAgInSc1d1	mnichovský
komplex	komplex	k1gInSc1	komplex
historika	historik	k1gMnSc2	historik
Jana	Jan	k1gMnSc2	Jan
Tesaře	Tesař	k1gMnSc2	Tesař
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
Teremin	Teremin	k1gInSc1	Teremin
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
Herci	herc	k1gInSc6	herc
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Michaela	Michael	k1gMnSc2	Michael
Frayna	Frayn	k1gMnSc2	Frayn
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
Dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
Divadlo	divadlo	k1gNnSc4	divadlo
Ta	ten	k3xDgFnSc1	ten
Fantastika	fantastika	k1gFnSc1	fantastika
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
obnovená	obnovený	k2eAgFnSc1d1	obnovená
premiéra	premiéra	k1gFnSc1	premiéra
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Palace	Palace	k1gFnSc2	Palace
Ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
měla	mít	k5eAaImAgFnS	mít
světovou	světový	k2eAgFnSc4d1	světová
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
scéna	scéna	k1gFnSc1	scéna
<g/>
)	)	kIx)	)
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
Dabing	dabing	k1gInSc1	dabing
Street	Street	k1gInSc4	Street
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Dejvické	dejvický	k2eAgNnSc1d1	Dejvické
divadlo	divadlo	k1gNnSc1	divadlo
Job	Job	k1gMnSc1	Job
Interviews	Interviews	k1gInSc1	Interviews
<g/>
,	,	kIx,	,
Jihočeské	jihočeský	k2eAgNnSc1d1	Jihočeské
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
květen	květen	k1gInSc1	květen
2014	[number]	k4	2014
Arthur	Arthura	k1gFnPc2	Arthura
Conan	Conana	k1gFnPc2	Conana
Doyle	Doyle	k1gFnSc2	Doyle
<g/>
:	:	kIx,	:
Pes	pes	k1gMnSc1	pes
baskervillský	baskervillský	k2eAgMnSc1d1	baskervillský
<g/>
,	,	kIx,	,
dramatizace	dramatizace	k1gFnSc1	dramatizace
Petr	Petr	k1gMnSc1	Petr
Zelenka	Zelenka	k1gMnSc1	Zelenka
a	a	k8xC	a
Olga	Olga	k1gFnSc1	Olga
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
,	,	kIx,	,
Otáčivé	otáčivý	k2eAgNnSc4d1	otáčivé
hlediště	hlediště	k1gNnSc4	hlediště
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
2015	[number]	k4	2015
Maria	Maria	k1gFnSc1	Maria
Goos	Goosa	k1gFnPc2	Goosa
<g/>
:	:	kIx,	:
Fuk	fuk	k6eAd1	fuk
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
pod	pod	k7c7	pod
Palmovkou	Palmovka	k1gFnSc7	Palmovka
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
</s>
<s>
1996	[number]	k4	1996
–	–	k?	–
Mňága	Mňága	k1gFnSc1	Mňága
-	-	kIx~	-
Happy	Happ	k1gInPc1	Happ
End	End	k1gFnSc1	End
</s>
<s>
1997	[number]	k4	1997
–	–	k?	–
Knoflíkáři	knoflíkář	k1gMnSc3	knoflíkář
–	–	k?	–
cena	cena	k1gFnSc1	cena
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gInSc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
scénář	scénář	k1gInSc1	scénář
</s>
<s>
2000	[number]	k4	2000
–	–	k?	–
Samotáři	samotář	k1gMnSc3	samotář
</s>
<s>
2002	[number]	k4	2002
–	–	k?	–
Rok	rok	k1gInSc1	rok
ďábla	ďábel	k1gMnSc2	ďábel
–	–	k?	–
cena	cena	k1gFnSc1	cena
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gInSc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
a	a	k8xC	a
režii	režie	k1gFnSc4	režie
</s>
<s>
2005	[number]	k4	2005
–	–	k?	–
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
</s>
<s>
2008	[number]	k4	2008
–	–	k?	–
Karamazovi	Karamaz	k1gMnSc3	Karamaz
–	–	k?	–
cena	cena	k1gFnSc1	cena
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gInSc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
a	a	k8xC	a
režii	režie	k1gFnSc4	režie
</s>
<s>
2015	[number]	k4	2015
–	–	k?	–
Ztraceni	ztracen	k2eAgMnPc1d1	ztracen
v	v	k7c6	v
Mnichově	mnichův	k2eAgInSc6d1	mnichův
–	–	k?	–
cena	cena	k1gFnSc1	cena
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gInSc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
2017	[number]	k4	2017
–	–	k?	–
Dabing	dabing	k1gInSc1	dabing
Street	Street	k1gInSc1	Street
(	(	kIx(	(
<g/>
chystaný	chystaný	k2eAgMnSc1d1	chystaný
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
<g/>
)	)	kIx)	)
ZELENKA	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
JUNGMANNOVÁ	JUNGMANNOVÁ	kA	JUNGMANNOVÁ
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejná	obyčejný	k2eAgNnPc1d1	obyčejné
šílenství	šílenství	k1gNnPc1	šílenství
:	:	kIx,	:
Divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7470	[number]	k4	7470
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
56	[number]	k4	56
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnSc2	hra
<g/>
:	:	kIx,	:
Příběhy	příběh	k1gInPc1	příběh
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
šílenství	šílenství	k1gNnSc2	šílenství
Teremin	Teremin	k1gInSc1	Teremin
Odjezdy	odjezd	k1gInPc1	odjezd
vlaků	vlak	k1gInPc2	vlak
Očištění	očištění	k1gNnSc1	očištění
Ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
druhy	druh	k1gInPc1	druh
Dabing	dabing	k1gInSc4	dabing
Street	Street	k1gInSc1	Street
</s>
