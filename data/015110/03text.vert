<s>
Zimní	zimní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Luďka	Luděk	k1gMnSc2
Čajky	čajka	k1gFnSc2
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Luďka	Luděk	k1gMnSc2
Čajky	čajka	k1gFnSc2
<g/>
„	„	k?
<g/>
Čajkárna	Čajkárna	k1gFnSc1
<g/>
“	“	k?
Poloha	poloha	k1gFnSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
2,96	2,96	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
35,83	35,83	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Otevření	otevření	k1gNnSc2
</s>
<s>
1957	#num#	k4
Přestavění	přestavění	k1gNnSc1
</s>
<s>
60	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Vlastník	vlastník	k1gMnSc1
</s>
<s>
PSG	PSG	kA
Berani	beran	k1gMnPc1
Zlín	Zlín	k1gInSc1
Povrch	povrch	k1gInSc1
</s>
<s>
proměnlivý	proměnlivý	k2eAgMnSc1d1
Bývalé	bývalý	k2eAgInPc4d1
názvy	název	k1gInPc4
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Gottwaldov	Gottwaldov	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Události	událost	k1gFnPc1
</s>
<s>
•	•	k?
MS	MS	kA
v	v	k7c6
házené	házená	k1gFnSc6
mužů	muž	k1gMnPc2
1964	#num#	k4
•	•	k?
MS	MS	kA
v	v	k7c6
házené	házená	k1gFnSc6
žen	žena	k1gFnPc2
1978	#num#	k4
•	•	k?
MS	MS	kA
v	v	k7c6
házené	házená	k1gFnSc6
mužů	muž	k1gMnPc2
1990	#num#	k4
•	•	k?
MS	MS	kA
v	v	k7c4
led	led	k1gInSc4
<g/>
.	.	kIx.
hokeji	hokej	k1gInSc6
žen	žena	k1gFnPc2
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
2012	#num#	k4
•	•	k?
MS	MS	kA
v	v	k7c4
led	led	k1gInSc4
<g/>
.	.	kIx.
hokeji	hokej	k1gInSc6
žen	žena	k1gFnPc2
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
2017	#num#	k4
Týmy	tým	k1gInPc7
</s>
<s>
PSG	PSG	kA
Zlín	Zlín	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
–	–	k?
)	)	kIx)
Kapacita	kapacita	k1gFnSc1
</s>
<s>
7	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
4	#num#	k4
525	#num#	k4
sedících	sedící	k2eAgInPc2d1
<g/>
)	)	kIx)
Rozměry	rozměra	k1gFnPc1
</s>
<s>
60	#num#	k4
<g/>
×	×	k?
<g/>
28	#num#	k4
m	m	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Luďka	Luděk	k1gMnSc2
Čajky	čajka	k1gFnSc2
je	být	k5eAaImIp3nS
sportovní	sportovní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
zde	zde	k6eAd1
odehrává	odehrávat	k5eAaImIp3nS
klub	klub	k1gInSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
PSG	PSG	kA
Berani	beran	k1gMnPc1
Zlín	Zlín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stadion	stadion	k1gInSc1
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
provozu	provoz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
a	a	k8xC
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
zastřešen	zastřešen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenován	pojmenován	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
po	po	k7c6
zlínském	zlínský	k2eAgMnSc6d1
hráči	hráč	k1gMnSc6
Luďkovi	Luděk	k1gMnSc6
Čajkovi	Čajek	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
tragicky	tragicky	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnSc2
při	při	k7c6
utkání	utkání	k1gNnSc6
v	v	k7c6
Košicích	Košice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Parametry	parametr	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
kapacita	kapacita	k1gFnSc1
<g/>
:	:	kIx,
7	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
(	(	kIx(
<g/>
4	#num#	k4
525	#num#	k4
sedících	sedící	k2eAgMnPc2d1
<g/>
,	,	kIx,
2	#num#	k4
475	#num#	k4
na	na	k7c6
stání	stání	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
rozměr	rozměr	k1gInSc1
kluziště	kluziště	k1gNnSc2
<g/>
:	:	kIx,
60	#num#	k4
<g/>
×	×	k?
<g/>
28	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
"	"	kIx"
<g/>
Zimní	zimní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Luďka	Luděk	k1gMnSc2
Čajky	čajka	k1gFnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hokej	hokej	k1gInSc1
<g/>
.	.	kIx.
<g/>
zlin	zlin	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
PSG	PSG	kA
Berani	beran	k1gMnPc5
Zlín	Zlín	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zimní	zimní	k2eAgInSc4d1
stadion	stadion	k1gInSc4
Luďka	Luděk	k1gMnSc2
Čajky	čajka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
Stadion	stadion	k1gInSc1
Luďka	Luděk	k1gMnSc2
Čajky	čajka	k1gFnSc2
Zlín	Zlín	k1gInSc1
-	-	kIx~
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PSG	PSG	kA
Beraní	beranit	k5eAaImIp3nS
Zlín	Zlín	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Extraliga	extraliga	k1gFnSc1
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
–	–	k?
Extraligové	extraligový	k2eAgInPc1d1
kluby	klub	k1gInPc1
Sezóna	sezóna	k1gFnSc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
<g/>
:	:	kIx,
Mistr	mistr	k1gMnSc1
ČR	ČR	kA
–	–	k?
HC	HC	kA
Oceláři	ocelář	k1gMnSc3
Třinec	Třinec	k1gInSc4
•	•	k?
Pohár	pohár	k1gInSc1
prezidenta	prezident	k1gMnSc2
–	–	k?
HC	HC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
Kluby	klub	k1gInPc1
2021	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
-	-	kIx~
15	#num#	k4
týmů	tým	k1gInPc2
</s>
<s>
HC	HC	kA
Kometa	kometa	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Mountfield	Mountfieldo	k1gNnPc2
HK	HK	kA
•	•	k?
HC	HC	kA
Energie	energie	k1gFnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
•	•	k?
Madeta	Madet	k1gMnSc4
Motor	motor	k1gInSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Bílí	bílit	k5eAaImIp3nP
Tygři	tygr	k1gMnPc1
Liberec	Liberec	k1gInSc4
•	•	k?
HC	HC	kA
Verva	verva	k1gFnSc1
Litvínov	Litvínov	k1gInSc1
•	•	k?
BK	BK	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
HC	HC	kA
Olomouc	Olomouc	k1gFnSc1
•	•	k?
HC	HC	kA
Dynamo	dynamo	k1gNnSc4
Pardubice	Pardubice	k1gInPc1
•	•	k?
HC	HC	kA
Škoda	škoda	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
•	•	k?
HC	HC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
HC	HC	kA
Oceláři	ocelář	k1gMnSc3
Třinec	Třinec	k1gInSc4
•	•	k?
HC	HC	kA
Vítkovice	Vítkovice	k1gInPc4
Ridera	Ridera	k1gFnSc1
•	•	k?
PSG	PSG	kA
Berani	beran	k1gMnPc1
Zlín	Zlín	k1gInSc1
•	•	k?
Rytíři	Rytíř	k1gMnPc7
Kladno	Kladno	k1gNnSc1
Stadiony	stadion	k1gInPc7
</s>
<s>
Brno	Brno	k1gNnSc4
•	•	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
•	•	k?
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Liberec	Liberec	k1gInSc1
•	•	k?
Litvínov	Litvínov	k1gInSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Pardubice	Pardubice	k1gInPc1
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
Třinec	Třinec	k1gInSc4
•	•	k?
Vítkovice	Vítkovice	k1gInPc4
•	•	k?
Zlín	Zlín	k1gInSc1
Sezóny	sezóna	k1gFnSc2
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
•	•	k?
2021	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
Utkání	utkání	k1gNnSc2
hvězd	hvězda	k1gFnPc2
české	český	k2eAgFnSc2d1
extraligy	extraliga	k1gFnSc2
</s>
<s>
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
All	All	k1gMnSc2
Star	Star	kA
Cup	cup	k1gInSc1
</s>
<s>
2018	#num#	k4
Bývalí	bývalý	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
HC	HC	kA
Vajgar	Vajgar	k1gMnSc1
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Slezan	Slezan	k1gMnSc1
Opava	Opava	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AZ	AZ	kA
Heimstaden	Heimstaden	k2eAgInSc1d1
Havířov	Havířov	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VHK	VHK	kA
ROBE	ROBE	k?
Vsetín	Vsetín	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Slovan	Slovan	k1gInSc4
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aqotec	Aqotec	k1gMnSc1
Orli	orel	k1gMnPc1
Znojmo	Znojmo	k1gNnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Piráti	pirát	k1gMnPc1
Chomutov	Chomutov	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Seznamy	seznam	k1gInPc1
utkání	utkání	k1gNnSc2
v	v	k7c6
play	play	k0
off	off	k?
</s>
<s>
Současné	současný	k2eAgInPc1d1
kluby	klub	k1gInPc1
–	–	k?
HC	HC	kA
Kometa	kometa	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Mountfield	Mountfieldo	k1gNnPc2
HK	HK	kA
•	•	k?
HC	HC	kA
Energie	energie	k1gFnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
•	•	k?
Rytíři	Rytíř	k1gMnPc7
Kladno	Kladno	k1gNnSc1
•	•	k?
Bílí	bílý	k2eAgMnPc1d1
Tygři	tygr	k1gMnPc1
Liberec	Liberec	k1gInSc1
•	•	k?
HC	HC	kA
Verva	verva	k1gFnSc1
Litvínov	Litvínov	k1gInSc1
•	•	k?
BK	BK	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
HC	HC	kA
Olomouc	Olomouc	k1gFnSc1
•	•	k?
HC	HC	kA
Dynamo	dynamo	k1gNnSc4
Pardubice	Pardubice	k1gInPc1
•	•	k?
HC	HC	kA
Škoda	škoda	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
•	•	k?
HC	HC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
HC	HC	kA
Oceláři	ocelář	k1gMnSc3
Třinec	Třinec	k1gInSc4
•	•	k?
HC	HC	kA
Vítkovice	Vítkovice	k1gInPc4
Ridera	Ridera	k1gFnSc1
•	•	k?
PSG	PSG	kA
Berani	beran	k1gMnPc1
Zlín	Zlín	k1gInSc1
•	•	k?
ČEZ	ČEZ	kA
Motor	motor	k1gInSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
Bývalé	bývalý	k2eAgInPc1d1
kluby	klub	k1gInPc1
–	–	k?
Piráti	pirát	k1gMnPc1
Chomutov	Chomutov	k1gInSc1
•	•	k?
HC	HC	kA
Dukla	Dukla	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
•	•	k?
HC	HC	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
VHK	VHK	kA
ROBE	ROBE	k?
Vsetín	Vsetín	k1gInSc1
•	•	k?
Orli	orel	k1gMnPc1
Znojmo	Znojmo	k1gNnSc4
Trofeje	trofej	k1gInSc2
</s>
<s>
Mistr	mistr	k1gMnSc1
ČR	ČR	kA
•	•	k?
Pohár	pohár	k1gInSc1
prezidenta	prezident	k1gMnSc2
•	•	k?
Hokejista	hokejista	k1gMnSc1
sezony	sezona	k1gFnSc2
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
utkání	utkání	k1gNnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
obránce	obránce	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
brankář	brankář	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
trenér	trenér	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
nováček	nováček	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
rozhodčí	rozhodčí	k1gMnSc1
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
helma	helma	k1gFnSc1
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
helma	helma	k1gFnSc1
•	•	k?
Nejvytíženější	vytížený	k2eAgMnSc1d3
brankář	brankář	k1gMnSc1
na	na	k7c6
ledě	led	k1gInSc6
•	•	k?
Nejnižší	nízký	k2eAgInSc1d3
průměr	průměr	k1gInSc1
inkasovaných	inkasovaný	k2eAgFnPc2d1
branek	branka	k1gFnPc2
Statistiky	statistika	k1gFnSc2
(	(	kIx(
<g/>
základní	základní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Klubové	klubový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
ELH	ELH	kA
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezoně	sezona	k1gFnSc6
•	•	k?
Nejproduktivnější	produktivní	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnPc1d3
nahrávači	nahrávač	k1gMnPc1
•	•	k?
Nejtrestanější	trestaný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgFnSc1d3
statistika	statistika	k1gFnSc1
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
•	•	k?
Nejvyšší	vysoký	k2eAgFnSc1d3
úspěšnost	úspěšnost	k1gFnSc1
zákroků	zákrok	k1gInPc2
•	•	k?
Nejproduktivnější	produktivní	k2eAgMnSc1d3
obránce	obránce	k1gMnSc1
•	•	k?
Nejproduktivnější	produktivní	k2eAgMnSc1d3
junior	junior	k1gMnSc1
české	český	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
extraligy	extraliga	k1gFnSc2
•	•	k?
Vítězný	vítězný	k2eAgInSc1d1
gól	gól	k1gInSc1
•	•	k?
Nejvíce	nejvíce	k6eAd1,k6eAd3
minut	minuta	k1gFnPc2
bez	bez	k7c2
gólu	gól	k1gInSc2
Statistiky	statistika	k1gFnSc2
(	(	kIx(
<g/>
play	play	k0
off	off	k?
<g/>
)	)	kIx)
</s>
<s>
Nejproduktivnější	produktivní	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
(	(	kIx(
<g/>
play	play	k0
off	off	k?
<g/>
)	)	kIx)
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
play-off	play-off	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnPc1d3
nahrávači	nahrávač	k1gMnPc1
play-off	play-off	k1gMnSc1
•	•	k?
Nejtrestanější	trestaný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
play-off	play-off	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgFnSc1d3
statistika	statistika	k1gFnSc1
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
play-off	play-off	k1gInSc1
•	•	k?
Nejvyšší	vysoký	k2eAgFnSc4d3
úspěšnost	úspěšnost	k1gFnSc4
zákroků	zákrok	k1gInPc2
play-off	play-off	k1gInSc4
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
play-off	play-off	k1gMnSc1
Zrušené	zrušený	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
Mistr	mistr	k1gMnSc1
ČR	ČR	kA
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
veterán	veterán	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgMnSc1d3
cizinec	cizinec	k1gMnSc1
•	•	k?
Nejlepší	dobrý	k2eAgFnSc1d3
pětka	pětka	k1gFnSc1
•	•	k?
Nejužitečnější	užitečný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
•	•	k?
Hokejová	hokejový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
•	•	k?
Cena	cena	k1gFnSc1
Bóži	Bóž	k1gFnSc2
Modrého	modré	k1gNnSc2
•	•	k?
Sympaťák	sympaťák	k1gMnSc1
•	•	k?
Nejslušnější	slušný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
hokejka	hokejka	k1gFnSc1
•	•	k?
Klub	klub	k1gInSc4
hokejových	hokejový	k2eAgMnPc2d1
střelců	střelec	k1gMnPc2
•	•	k?
Síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
českého	český	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
•	•	k?
Radegast	Radegast	k1gMnSc1
index	index	k1gInSc1
•	•	k?
Sedmá	sedmý	k4xOgNnPc1
utkání	utkání	k1gNnPc1
v	v	k7c6
play	play	k0
off	off	k?
české	český	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
extraligy	extraliga	k1gFnSc2
Předchůdkyně	předchůdkyně	k1gFnSc2
</s>
<s>
Československá	československý	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Tabulka	tabulka	k1gFnSc1
všech	všecek	k3xTgInPc2
extraligových	extraligový	k2eAgInPc2d1
klubů	klub	k1gInPc2
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
