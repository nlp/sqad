<s>
Barvy	barva	k1gFnSc2	barva
estonské	estonský	k2eAgFnPc1d1	Estonská
vlajky	vlajka	k1gFnPc1	vlajka
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
<g/>
)	)	kIx)	)
a	a	k8xC	a
sníh	sníh	k1gInSc1	sníh
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
byly	být	k5eAaImAgFnP	být
vykládány	vykládat	k5eAaImNgInP	vykládat
i	i	k9	i
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
