<s>
Vrchotice	Vrchotice	k1gFnSc1
(	(	kIx(
<g/>
tvrz	tvrz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tvrz	tvrz	k1gFnSc1
Vrchotice	Vrchotice	k1gFnSc1
Tvrz	tvrz	k1gFnSc1
VrchoticeÚčel	VrchoticeÚčel	k1gFnSc1
stavby	stavba	k1gFnSc2
</s>
<s>
středověká	středověký	k2eAgFnSc1d1
tvrz	tvrz	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Vrchotice	Vrchotice	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
39,73	39,73	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
35,99	35,99	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
36246	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
230	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vrchotice	Vrchotice	k1gFnPc1
jsou	být	k5eAaImIp3nP
renesanční	renesanční	k2eAgFnSc1d1
tvrz	tvrz	k1gFnSc1
ve	v	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
<g/>
,	,	kIx,
části	část	k1gFnSc6
města	město	k1gNnSc2
Sedlec-Prčice	Sedlec-Prčice	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Příbram	Příbram	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1965	#num#	k4
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnPc1
zmínky	zmínka	k1gFnPc1
o	o	k7c6
Vrchoticích	Vrchotice	k1gFnPc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1370	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
majetku	majetek	k1gInSc6
rodu	rod	k1gInSc2
Vrchotických	Vrchotický	k2eAgInPc2d1
z	z	k7c2
Vrchotic	Vrchotice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
tvrzi	tvrz	k1gFnSc6
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1457	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
jejím	její	k3xOp3gMnSc7
majitelem	majitel	k1gMnSc7
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
předek	předek	k1gMnSc1
vladyckého	vladycký	k2eAgInSc2d1
rodu	rod	k1gInSc2
Vrchotických	Vrchotický	k2eAgFnPc2d1
z	z	k7c2
Loutkova	Loutkův	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1596	#num#	k4
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
majetku	majetek	k1gInSc2
Bechyňů	Bechyně	k1gMnPc2
z	z	k7c2
Lažan	Lažana	k1gFnPc2
a	a	k8xC
okolo	okolo	k7c2
roku	rok	k1gInSc2
1603	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Lažanští	Lažanský	k2eAgMnPc1d1
z	z	k7c2
Bukové	bukový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
gotická	gotický	k2eAgFnSc1d1
tvrz	tvrz	k1gFnSc1
přestavěna	přestavět	k5eAaPmNgFnS
na	na	k7c4
renesanční	renesanční	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1613	#num#	k4
tvrz	tvrz	k1gFnSc1
koupil	koupit	k5eAaPmAgMnS
Maxmilián	Maxmilián	k1gMnSc1
Velemyský	Velemyský	k1gMnSc1
z	z	k7c2
Velemyšlevsi	Velemyšlevs	k1gMnSc3
a	a	k8xC
připojil	připojit	k5eAaPmAgInS
ji	on	k3xPp3gFnSc4
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
mitrovickému	mitrovický	k2eAgInSc3d1
statku	statek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevyužitá	využitý	k2eNgFnSc1d1
tvrz	tvrz	k1gFnSc1
chátrala	chátrat	k5eAaImAgFnS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1629	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
pramenech	pramen	k1gInPc6
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
pustá	pustý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1660	#num#	k4
vesnici	vesnice	k1gFnSc6
získal	získat	k5eAaPmAgMnS
Vilém	Vilém	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Mitrovský	Mitrovský	k2eAgMnSc1d1
z	z	k7c2
Nemyšle	Nemyšle	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ji	on	k3xPp3gFnSc4
spravoval	spravovat	k5eAaImAgMnS
z	z	k7c2
jetřichovického	jetřichovický	k2eAgInSc2d1
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1700	#num#	k4
byla	být	k5eAaImAgFnS
část	část	k1gFnSc1
tvrze	tvrz	k1gFnSc2
zbořena	zbořen	k2eAgFnSc1d1
a	a	k8xC
její	její	k3xOp3gNnSc4
místo	místo	k1gNnSc4
zaujal	zaujmout	k5eAaPmAgInS
poplužní	poplužní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochovaná	dochovaný	k2eAgFnSc1d1
část	část	k1gFnSc1
budovy	budova	k1gFnSc2
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
ubytování	ubytování	k1gNnSc3
zaměstnanců	zaměstnanec	k1gMnPc2
a	a	k8xC
její	její	k3xOp3gInPc4
sklepy	sklep	k1gInPc4
jako	jako	k8xC,k8xS
skladiště	skladiště	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
budova	budova	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
potřebám	potřeba	k1gFnPc3
místního	místní	k2eAgNnSc2d1
jednotného	jednotný	k2eAgNnSc2d1
zemědělského	zemědělský	k2eAgNnSc2d1
družstva	družstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
byla	být	k5eAaImAgFnS
rozsáhle	rozsáhle	k6eAd1
renovována	renovován	k2eAgFnSc1d1
a	a	k8xC
dnes	dnes	k6eAd1
zde	zde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
firma	firma	k1gFnSc1
FOREST-FISH	FOREST-FISH	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Interiéry	interiér	k1gInPc1
tvrze	tvrz	k1gFnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
nepřístupné	přístupný	k2eNgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Renesanční	renesanční	k2eAgFnSc1d1
tvrz	tvrz	k1gFnSc1
Vrchotice	Vrchotice	k1gFnSc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
hospodářského	hospodářský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
poblíž	poblíž	k7c2
vesnice	vesnice	k1gFnSc2
Vrchotice	Vrchotice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
jednopatrová	jednopatrový	k2eAgFnSc1d1
s	s	k7c7
obdélným	obdélný	k2eAgInSc7d1
půdorysem	půdorys	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těsné	těsný	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
hlavní	hlavní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
masivní	masivní	k2eAgFnSc1d1
hranolová	hranolový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
kryta	krýt	k5eAaImNgFnS
nízkou	nízký	k2eAgFnSc7d1
stanovou	stanový	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
ze	z	k7c2
šindelů	šindel	k1gInPc2
<g/>
,	,	kIx,
zakončenou	zakončený	k2eAgFnSc7d1
nízkou	nízký	k2eAgFnSc7d1
vížkou	vížka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věž	věž	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
prvním	první	k4xOgInSc6
podlaží	podlaží	k1gNnSc6
jehlanovitý	jehlanovitý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
druhém	druhý	k4xOgInSc6
podlaží	podlaží	k1gNnSc6
je	být	k5eAaImIp3nS
subtilnější	subtilní	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
a	a	k8xC
věž	věž	k1gFnSc4
spojuje	spojovat	k5eAaImIp3nS
vestavba	vestavba	k1gFnSc1
schodiště	schodiště	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Západní	západní	k2eAgFnSc1d1
a	a	k8xC
severní	severní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
dvora	dvůr	k1gInSc2
je	být	k5eAaImIp3nS
ohraničena	ohraničen	k2eAgFnSc1d1
dvoukřídlým	dvoukřídlí	k1gMnPc3
chlévem	chlév	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednu	jeden	k4xCgFnSc4
z	z	k7c2
částí	část	k1gFnPc2
dvora	dvůr	k1gInSc2
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
stodola	stodola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budovy	budova	k1gFnPc1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
ohradní	ohradní	k2eAgFnSc7d1
zdí	zeď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdivo	zdivo	k1gNnSc1
stavby	stavba	k1gFnPc1
je	být	k5eAaImIp3nS
lomové	lomový	k2eAgNnSc1d1
a	a	k8xC
cihlové	cihlový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
brána	brána	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mezi	mezi	k7c7
chlévem	chlév	k1gInSc7
a	a	k8xC
tvrzí	tvrz	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Tvrz	tvrz	k1gFnSc1
s	s	k7c7
hospodářským	hospodářský	k2eAgInSc7d1
dvorem	dvůr	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
a	a	k8xC
tvrze	tvrz	k1gFnPc1
Království	království	k1gNnSc2
českého	český	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kouřimsko	Kouřimsko	k1gNnSc1
<g/>
,	,	kIx,
Vltavsko	Vltavsko	k1gNnSc1
a	a	k8xC
jihozápadní	jihozápadní	k2eAgNnSc1d1
Boleslavsko	Boleslavsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Šolc	Šolc	k1gMnSc1
a	a	k8xC
Šimáček	Šimáček	k1gMnSc1
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
.	.	kIx.
348	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Tvrze	tvrz	k1gFnSc2
okolo	okolo	k7c2
Mitrovic	Mitrovice	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
263	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
a	a	k8xC
tvrze	tvrz	k1gFnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Miloslav	Miloslava	k1gFnPc2
Bělohlávek	bělohlávka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Vrchotice	Vrchotice	k1gFnSc1
–	–	k?
tvrz	tvrz	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
393	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FABIAN	Fabian	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchotice	Vrchotice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hrady-zriceniny	hrady-zricenina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tvrz	tvrz	k1gFnSc1
Vrchotice	Vrchotice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Česka	Česko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vrchotice	Vrchotice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
stredovek	stredovka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vrchotice	Vrchotice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
