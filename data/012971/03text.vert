<p>
<s>
Serie	serie	k1gFnSc1	serie
A	a	k9	a
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Serie	serie	k1gFnSc1	serie
A	a	k8xC	a
TIM	TIM	k?	TIM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
italská	italský	k2eAgFnSc1d1	italská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
sponzorem	sponzor	k1gMnSc7	sponzor
soutěže	soutěž	k1gFnSc2	soutěž
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc4	společnost
Telecom	Telecom	k1gInSc1	Telecom
Italia	Italium	k1gNnSc2	Italium
Mobile	mobile	k1gNnSc2	mobile
(	(	kIx(	(
<g/>
TIM	TIM	k?	TIM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
a	a	k8xC	a
plynule	plynule	k6eAd1	plynule
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
předchozí	předchozí	k2eAgFnPc4d1	předchozí
italské	italský	k2eAgFnPc4d1	italská
celonárodní	celonárodní	k2eAgFnPc4d1	celonárodní
soutěže	soutěž	k1gFnPc4	soutěž
(	(	kIx(	(
<g/>
hrané	hraný	k2eAgFnPc1d1	hraná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
vítězové	vítěz	k1gMnPc1	vítěz
mohou	moct	k5eAaImIp3nP	moct
počítat	počítat	k5eAaImF	počítat
tituly	titul	k1gInPc1	titul
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
získané	získaný	k2eAgFnPc1d1	získaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	a	k9	a
hraje	hrát	k5eAaImIp3nS	hrát
20	[number]	k4	20
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
3	[number]	k4	3
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
do	do	k7c2	do
Serie	serie	k1gFnSc2	serie
B	B	kA	B
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
tým	tým	k1gInSc1	tým
získá	získat	k5eAaPmIp3nS	získat
italský	italský	k2eAgInSc4d1	italský
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
Scudetto	Scudetto	k1gNnSc4	Scudetto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
ligových	ligový	k2eAgInPc2d1	ligový
titulů	titul	k1gInPc2	titul
má	mít	k5eAaImIp3nS	mít
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
32	[number]	k4	32
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
odebrané	odebraný	k2eAgMnPc4d1	odebraný
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
celkem	celkem	k6eAd1	celkem
20	[number]	k4	20
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
první	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
postupují	postupovat	k5eAaImIp3nP	postupovat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
tým	tým	k1gInSc1	tým
pak	pak	k6eAd1	pak
postupuje	postupovat	k5eAaImIp3nS	postupovat
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
předkola	předkolo	k1gNnSc2	předkolo
(	(	kIx(	(
<g/>
play-off	playff	k1gInSc1	play-off
<g/>
)	)	kIx)	)
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
a	a	k8xC	a
pátý	pátý	k4xOgInSc1	pátý
tým	tým	k1gInSc1	tým
postupují	postupovat	k5eAaImIp3nP	postupovat
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
předkola	předkolo	k1gNnSc2	předkolo
respektive	respektive	k9	respektive
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkola	předkolo	k1gNnSc2	předkolo
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
místo	místo	k1gNnSc1	místo
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
předkole	předkolo	k1gNnSc6	předkolo
Evropské	evropský	k2eAgFnSc2d1	Evropská
lize	liga	k1gFnSc6	liga
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
vítěz	vítěz	k1gMnSc1	vítěz
Coppa	Copp	k1gMnSc2	Copp
Italia	Italium	k1gNnSc2	Italium
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
finalistů	finalista	k1gMnPc2	finalista
do	do	k7c2	do
pohárů	pohár	k1gInPc2	pohár
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
připadnout	připadnout	k5eAaPmF	připadnout
šestému	šestý	k4xOgInSc3	šestý
týmu	tým	k1gInSc3	tým
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
vítěz	vítěz	k1gMnSc1	vítěz
Coppy	Coppa	k1gFnSc2	Coppa
Italia	Italium	k1gNnSc2	Italium
se	se	k3xPyFc4	se
kvalifikuje	kvalifikovat	k5eAaBmIp3nS	kvalifikovat
do	do	k7c2	do
pohárů	pohár	k1gInPc2	pohár
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
jde	jít	k5eAaImIp3nS	jít
finalista	finalista	k1gMnSc1	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zařadí	zařadit	k5eAaPmIp3nS	zařadit
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
předkola	předkolo	k1gNnSc2	předkolo
a	a	k8xC	a
pátý	pátý	k4xOgInSc1	pátý
tým	tým	k1gInSc1	tým
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
ho	on	k3xPp3gMnSc4	on
nahradí	nahradit	k5eAaPmIp3nS	nahradit
v	v	k7c6	v
předkole	předkolo	k1gNnSc6	předkolo
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
nejhorší	zlý	k2eAgInPc1d3	Nejhorší
týmy	tým	k1gInPc1	tým
soutěže	soutěž	k1gFnSc2	soutěž
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
do	do	k7c2	do
Serie	serie	k1gFnSc2	serie
B	B	kA	B
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
soutěže	soutěž	k1gFnSc2	soutěž
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
právo	právo	k1gNnSc1	právo
startovat	startovat	k5eAaBmF	startovat
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
superpoháru	superpohár	k1gInSc6	superpohár
zvaném	zvaný	k2eAgInSc6d1	zvaný
Supercoppa	Supercopp	k1gMnSc4	Supercopp
italiana	italiana	k1gFnSc1	italiana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
vítěz	vítěz	k1gMnSc1	vítěz
Coppa	Copp	k1gMnSc2	Copp
Italia	Italius	k1gMnSc2	Italius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
držiteli	držitel	k1gMnSc6	držitel
titulu	titul	k1gInSc2	titul
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
korupční	korupční	k2eAgFnSc2d1	korupční
aféry	aféra	k1gFnSc2	aféra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
a	a	k8xC	a
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Serie	serie	k1gFnSc2	serie
B	B	kA	B
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
Juventus	Juventus	k1gInSc4	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
tak	tak	k9	tak
oficiálně	oficiálně	k6eAd1	oficiálně
připsal	připsat	k5eAaPmAgInS	připsat
již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
titul	titul	k1gInSc1	titul
obhájil	obhájit	k5eAaPmAgInS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Vicemistrem	vicemistr	k1gMnSc7	vicemistr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
SSC	SSC	kA	SSC
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Dominance	dominance	k1gFnSc1	dominance
Juventusu	Juventus	k1gInSc2	Juventus
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získal	získat	k5eAaPmAgMnS	získat
již	již	k6eAd1	již
třetí	třetí	k4xOgInSc4	třetí
titul	titul	k1gInSc4	titul
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
třicátý	třicátý	k4xOgMnSc1	třicátý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc7	počátek
fotbalu	fotbal	k1gInSc2	fotbal
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
===	===	k?	===
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přinesli	přinést	k5eAaPmAgMnP	přinést
fotbal	fotbal	k1gInSc4	fotbal
i	i	k9	i
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
oficiálně	oficiálně	k6eAd1	oficiálně
založeným	založený	k2eAgInSc7d1	založený
sportovním	sportovní	k2eAgInSc7d1	sportovní
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
hrajícím	hrající	k2eAgFnPc3d1	hrající
z	z	k7c2	z
části	část	k1gFnSc2	část
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
janovský	janovský	k2eAgInSc1d1	janovský
klub	klub	k1gInSc1	klub
Genoa	Geno	k1gInSc2	Geno
CFC	CFC	kA	CFC
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
zde	zde	k6eAd1	zde
expanza	expanza	k1gFnSc1	expanza
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
velmi	velmi	k6eAd1	velmi
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
soutěž	soutěž	k1gFnSc1	soutěž
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
přistěhovalci	přistěhovalec	k1gMnSc3	přistěhovalec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Italská	italský	k2eAgFnSc1d1	italská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FIGC	FIGC	kA	FIGC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc4	sídlo
si	se	k3xPyFc3	se
vybrala	vybrat	k5eAaPmAgFnS	vybrat
Turín	Turín	k1gInSc4	Turín
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
členy	člen	k1gInPc7	člen
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
4	[number]	k4	4
severoitalské	severoitalský	k2eAgInPc1d1	severoitalský
kluby	klub	k1gInPc1	klub
–	–	k?	–
Genoa	Geno	k1gInSc2	Geno
CFC	CFC	kA	CFC
<g/>
,	,	kIx,	,
FBC	FBC	kA	FBC
Torinese	Torinese	k1gFnSc1	Torinese
<g/>
,	,	kIx,	,
Internazionale	Internazionale	k1gFnSc1	Internazionale
Torino	Torino	k1gNnSc1	Torino
a	a	k8xC	a
Reale	Real	k1gInSc6	Real
Società	Società	k1gMnSc2	Società
Ginnastica	Ginnastic	k1gInSc2	Ginnastic
Torino	Torino	k1gNnSc4	Torino
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
čtyři	čtyři	k4xCgInPc1	čtyři
týmy	tým	k1gInPc1	tým
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
účastníky	účastník	k1gMnPc7	účastník
prvního	první	k4xOgNnSc2	první
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
Italského	italský	k2eAgNnSc2d1	italské
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
mistrovství	mistrovství	k1gNnSc4	mistrovství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
vítězem	vítěz	k1gMnSc7	vítěz
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
držitelem	držitel	k1gMnSc7	držitel
prvního	první	k4xOgInSc2	první
oficiálního	oficiální	k2eAgInSc2d1	oficiální
titulu	titul	k1gInSc2	titul
pro	pro	k7c4	pro
mistra	mistr	k1gMnSc4	mistr
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Genoa	Genoa	k1gMnSc1	Genoa
CFC	CFC	kA	CFC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dvou	dva	k4xCgInPc6	dva
ročnících	ročník	k1gInPc6	ročník
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgFnP	přidat
i	i	k9	i
další	další	k2eAgInPc4d1	další
celky	celek	k1gInPc4	celek
jako	jako	k8xC	jako
UC	UC	kA	UC
Sampdoria	Sampdorium	k1gNnSc2	Sampdorium
<g/>
,	,	kIx,	,
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
nebo	nebo	k8xC	nebo
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Genoa	Genoa	k1gMnSc1	Genoa
CFC	CFC	kA	CFC
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
šest	šest	k4xCc1	šest
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
úvodních	úvodní	k2eAgInPc2d1	úvodní
ročníků	ročník	k1gInPc2	ročník
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
nadvládu	nadvláda	k1gFnSc4	nadvláda
dokázal	dokázat	k5eAaPmAgInS	dokázat
narušit	narušit	k5eAaPmF	narušit
jen	jen	k9	jen
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
a	a	k8xC	a
ukončit	ukončit	k5eAaPmF	ukončit
ji	on	k3xPp3gFnSc4	on
musel	muset	k5eAaImAgInS	muset
až	až	k8xS	až
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
také	také	k9	také
Italská	italský	k2eAgFnSc1d1	italská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FIGC	FIGC	kA	FIGC
<g/>
)	)	kIx)	)
stala	stát	k5eAaPmAgFnS	stát
právoplatným	právoplatný	k2eAgInSc7d1	právoplatný
členem	člen	k1gInSc7	člen
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
neplánované	plánovaný	k2eNgFnSc6d1	neplánovaná
pauze	pauza	k1gFnSc6	pauza
zapříčiněné	zapříčiněný	k2eAgFnSc6d1	zapříčiněná
I.	I.	kA	I.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
obliba	obliba	k1gFnSc1	obliba
fotbalu	fotbal	k1gInSc2	fotbal
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
ještě	ještě	k6eAd1	ještě
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následek	k1gInSc7	následek
bylo	být	k5eAaImAgNnS	být
částečné	částečný	k2eAgNnSc1d1	částečné
oddělení	oddělení	k1gNnSc1	oddělení
soutěže	soutěž	k1gFnSc2	soutěž
pod	pod	k7c7	pod
CCI	CCI	kA	CCI
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
svaz	svaz	k1gInSc1	svaz
sdružující	sdružující	k2eAgFnSc1d1	sdružující
menší	malý	k2eAgFnSc1d2	menší
kluby	klub	k1gInPc7	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1921	[number]	k4	1921
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
tak	tak	k6eAd1	tak
máme	mít	k5eAaImIp1nP	mít
dvě	dva	k4xCgFnPc1	dva
rovnocenné	rovnocenný	k2eAgFnPc1d1	rovnocenná
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
FC	FC	kA	FC
Pro	pro	k7c4	pro
Vercelli	Vercelle	k1gFnSc4	Vercelle
1892	[number]	k4	1892
(	(	kIx(	(
<g/>
FIGC	FIGC	kA	FIGC
<g/>
)	)	kIx)	)
a	a	k8xC	a
USD	USD	kA	USD
Novese	Novese	k1gFnSc1	Novese
(	(	kIx(	(
<g/>
CCI	CCI	kA	CCI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc4d1	následující
sezonu	sezona	k1gFnSc4	sezona
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
exces	exces	k1gInSc1	exces
ukončen	ukončit	k5eAaPmNgInS	ukončit
a	a	k8xC	a
svazy	svaz	k1gInPc7	svaz
i	i	k8xC	i
jejich	jejich	k3xOp3gFnPc4	jejich
soutěže	soutěž	k1gFnPc4	soutěž
sloučeny	sloučen	k2eAgFnPc4d1	sloučena
pod	pod	k7c7	pod
FIGC	FIGC	kA	FIGC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1923	[number]	k4	1923
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
získal	získat	k5eAaPmAgMnS	získat
svůj	svůj	k3xOyFgMnSc1	svůj
9	[number]	k4	9
<g/>
.	.	kIx.	.
titul	titul	k1gInSc1	titul
Genoa	Geno	k1gInSc2	Geno
CFC	CFC	kA	CFC
a	a	k8xC	a
stanovil	stanovit	k5eAaPmAgInS	stanovit
rekord	rekord	k1gInSc4	rekord
platný	platný	k2eAgInSc4d1	platný
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
1926	[number]	k4	1926
<g/>
/	/	kIx~	/
<g/>
27	[number]	k4	27
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc2	několik
vážným	vážný	k2eAgInPc3d1	vážný
sportovním	sportovní	k2eAgInPc3d1	sportovní
přestupkům	přestupek	k1gInPc3	přestupek
týmu	tým	k1gInSc2	tým
Torino	Torino	k1gNnSc4	Torino
FC	FC	kA	FC
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Juventus	Juventus	k1gInSc4	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
odštěpil	odštěpit	k5eAaPmAgMnS	odštěpit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
trest	trest	k1gInSc4	trest
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
odebrán	odebrán	k2eAgInSc4d1	odebrán
jeho	on	k3xPp3gInSc4	on
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
italského	italský	k2eAgMnSc2d1	italský
mistra	mistr	k1gMnSc2	mistr
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
poprvé	poprvé	k6eAd1	poprvé
neobsazeno	obsazen	k2eNgNnSc1d1	neobsazeno
<g/>
.	.	kIx.	.
<g/>
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
éra	éra	k1gFnSc1	éra
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
Serie	serie	k1gFnSc2	serie
A	a	k9	a
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
dominancí	dominance	k1gFnSc7	dominance
dvou	dva	k4xCgInPc2	dva
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
dohromady	dohromady	k6eAd1	dohromady
získali	získat	k5eAaPmAgMnP	získat
16	[number]	k4	16
titulů	titul	k1gInPc2	titul
–	–	k?	–
Genoa	Genoa	k1gFnSc1	Genoa
CFC	CFC	kA	CFC
a	a	k8xC	a
FC	FC	kA	FC
Pro	pro	k7c4	pro
Vercelli	Vercelle	k1gFnSc4	Vercelle
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
nezískal	získat	k5eNaPmAgInS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
mistrovské	mistrovský	k2eAgInPc4d1	mistrovský
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
týmy	tým	k1gInPc4	tým
však	však	k9	však
titul	titul	k1gInSc4	titul
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	A	kA	A
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
-	-	kIx~	-
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
doposud	doposud	k6eAd1	doposud
nezískali	získat	k5eNaPmAgMnP	získat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
Serie	serie	k1gFnSc2	serie
A	A	kA	A
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
-	-	kIx~	-
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
činovníci	činovník	k1gMnPc1	činovník
FIGC	FIGC	kA	FIGC
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
skoncují	skoncovat	k5eAaPmIp3nP	skoncovat
s	s	k7c7	s
nejasnou	jasný	k2eNgFnSc7d1	nejasná
strukturou	struktura	k1gFnSc7	struktura
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
soutěží	soutěž	k1gFnPc2	soutěž
na	na	k7c6	na
území	území	k1gNnSc6	území
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
sjednotí	sjednotit	k5eAaPmIp3nS	sjednotit
ho	on	k3xPp3gInSc4	on
na	na	k7c4	na
klasický	klasický	k2eAgInSc4d1	klasický
ligový	ligový	k2eAgInSc4d1	ligový
formát	formát	k1gInSc4	formát
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
soutěže	soutěž	k1gFnSc2	soutěž
Divisione	Division	k1gInSc5	Division
Nazionale	Nazionale	k1gFnPc2	Nazionale
1928-1929	[number]	k4	1928-1929
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
18	[number]	k4	18
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
(	(	kIx(	(
<g/>
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
9	[number]	k4	9
<g/>
)	)	kIx)	)
týmů	tým	k1gInPc2	tým
do	do	k7c2	do
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Serie	serie	k1gFnSc2	serie
A.	A.	kA	A.
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
soutěž	soutěž	k1gFnSc1	soutěž
zůstane	zůstat	k5eAaPmIp3nS	zůstat
nadále	nadále	k6eAd1	nadále
otevřená	otevřený	k2eAgFnSc1d1	otevřená
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
nejhorší	zlý	k2eAgInPc1d3	Nejhorší
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
ročníku	ročník	k1gInSc2	ročník
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
do	do	k7c2	do
Serie	serie	k1gFnSc2	serie
B	B	kA	B
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
formát	formát	k1gInSc4	formát
nejvíce	hodně	k6eAd3	hodně
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
týmům	tým	k1gInPc3	tým
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
ze	z	k7c2	z
sousedících	sousedící	k2eAgFnPc2d1	sousedící
provincíí	provincí	k1gFnPc2	provincí
Piemont	Piemonta	k1gFnPc2	Piemonta
a	a	k8xC	a
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
nejslavnější	slavný	k2eAgInPc1d3	nejslavnější
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
provincie	provincie	k1gFnPc1	provincie
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
(	(	kIx(	(
<g/>
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Torino	Torino	k1gNnSc1	Torino
FC	FC	kA	FC
<g/>
,	,	kIx,	,
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
a	a	k8xC	a
FC	FC	kA	FC
Internazionale	Internazionale	k1gFnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
získali	získat	k5eAaPmAgMnP	získat
56	[number]	k4	56
titulů	titul	k1gInPc2	titul
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
70	[number]	k4	70
ročníků	ročník	k1gInPc2	ročník
Serie	serie	k1gFnSc1	serie
A	a	k9	a
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
zde	zde	k6eAd1	zde
naprostou	naprostý	k2eAgFnSc4d1	naprostá
dominanci	dominance	k1gFnSc4	dominance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
mohl	moct	k5eAaImAgMnS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
5	[number]	k4	5
ročnících	ročník	k1gInPc6	ročník
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
(	(	kIx(	(
<g/>
Quinquennio	Quinquennio	k1gNnSc1	Quinquennio
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
oro	oro	k?	oro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Serie	serie	k1gFnSc2	serie
A	a	k9	a
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
meziválečného	meziválečný	k2eAgNnSc2d1	meziválečné
období	období	k1gNnSc2	období
změnil	změnit	k5eAaPmAgInS	změnit
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc2	Internazionale
Milano	Milana	k1gFnSc5	Milana
název	název	k1gInSc4	název
na	na	k7c4	na
Associazione	Associazion	k1gInSc5	Associazion
Sportiva	Sportiva	k1gFnSc1	Sportiva
Ambrosiana	Ambrosian	k1gMnSc2	Ambrosian
(	(	kIx(	(
<g/>
sloučením	sloučení	k1gNnSc7	sloučení
s	s	k7c7	s
Unione	union	k1gInSc5	union
Sportiva	Sportivum	k1gNnPc4	Sportivum
Milanese	Milanese	k1gFnSc2	Milanese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
získal	získat	k5eAaPmAgInS	získat
3	[number]	k4	3
mistrovské	mistrovský	k2eAgInPc4d1	mistrovský
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
a	a	k8xC	a
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
přejmenování	přejmenování	k1gNnSc3	přejmenování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
zachovány	zachovat	k5eAaPmNgInP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
kluby	klub	k1gInPc1	klub
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
silnou	silný	k2eAgFnSc4d1	silná
konkurenci	konkurence	k1gFnSc4	konkurence
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Bologna	Bologna	k1gFnSc1	Bologna
FC	FC	kA	FC
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
4	[number]	k4	4
<g/>
×	×	k?	×
titul	titul	k1gInSc4	titul
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
ve	v	k7c6	v
slavném	slavný	k2eAgMnSc6d1	slavný
Mitropa	Mitropa	k1gFnSc1	Mitropa
Cupu	cup	k1gInSc2	cup
(	(	kIx(	(
<g/>
Středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
poháru	pohár	k1gInSc6	pohár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
Evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
-	-	kIx~	-
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
tentokrát	tentokrát	k6eAd1	tentokrát
nastává	nastávat	k5eAaImIp3nS	nastávat
dynastie	dynastie	k1gFnSc1	dynastie
druhého	druhý	k4xOgMnSc2	druhý
z	z	k7c2	z
turínských	turínský	k2eAgMnPc2d1	turínský
klubů	klub	k1gInPc2	klub
-	-	kIx~	-
Torino	Torino	k1gNnSc1	Torino
FC	FC	kA	FC
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
první	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
poválečné	poválečný	k2eAgInPc4d1	poválečný
ročníky	ročník	k1gInPc4	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
jeho	jeho	k3xOp3gFnSc2	jeho
éry	éra	k1gFnSc2	éra
opět	opět	k6eAd1	opět
stál	stát	k5eAaImAgMnS	stát
největší	veliký	k2eAgMnSc1d3	veliký
rival	rival	k1gMnSc1	rival
Juventus	Juventus	k1gInSc4	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
blížil	blížit	k5eAaImAgInS	blížit
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
tabulky	tabulka	k1gFnSc2	tabulka
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
chvíle	chvíle	k1gFnSc1	chvíle
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
tým	tým	k1gInSc4	tým
Genoa	Geno	k1gInSc2	Geno
CFC	CFC	kA	CFC
a	a	k8xC	a
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1957	[number]	k4	1957
<g/>
/	/	kIx~	/
<g/>
58	[number]	k4	58
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
jubilejního	jubilejní	k2eAgMnSc2d1	jubilejní
10	[number]	k4	10
<g/>
.	.	kIx.	.
titulu	titul	k1gInSc2	titul
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
prestižní	prestižní	k2eAgFnSc1d1	prestižní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
nosit	nosit	k5eAaImF	nosit
na	na	k7c6	na
dresu	dres	k1gInSc6	dres
nášivku	nášivek	k1gInSc2	nášivek
Scudetto	Scudett	k2eAgNnSc1d1	Scudetto
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
mistra	mistr	k1gMnSc4	mistr
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prestižní	prestižní	k2eAgNnSc1d1	prestižní
právo	právo	k1gNnSc4	právo
našít	našít	k5eAaPmF	našít
si	se	k3xPyFc3	se
nad	nad	k7c4	nad
logo	logo	k1gNnSc4	logo
hvězdu	hvězda	k1gFnSc4	hvězda
(	(	kIx(	(
<g/>
Stella	Stella	k1gFnSc1	Stella
<g/>
)	)	kIx)	)
za	za	k7c4	za
10	[number]	k4	10
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
Juventus	Juventus	k1gInSc4	Juventus
FC	FC	kA	FC
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
vyvoleným	vyvolený	k1gMnSc7	vyvolený
a	a	k8xC	a
především	především	k9	především
vhodným	vhodný	k2eAgInSc7d1	vhodný
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pro	pro	k7c4	pro
nápad	nápad	k1gInSc4	nápad
politika	politika	k1gFnSc1	politika
Umberto	Umberta	k1gFnSc5	Umberta
Agnelli	Agnelle	k1gFnSc3	Agnelle
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
myšlenka	myšlenka	k1gFnSc1	myšlenka
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
mnoho	mnoho	k4c4	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
založeny	založen	k2eAgInPc4d1	založen
Evropské	evropský	k2eAgInPc4d1	evropský
poháry	pohár	k1gInPc4	pohár
<g/>
,	,	kIx,	,
považujeme	považovat	k5eAaImIp1nP	považovat
Italské	italský	k2eAgInPc1d1	italský
kluby	klub	k1gInPc1	klub
jako	jako	k8xC	jako
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1962	[number]	k4	1962
<g/>
/	/	kIx~	/
<g/>
63	[number]	k4	63
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
triumfy	triumf	k1gInPc7	triumf
ho	on	k3xPp3gInSc4	on
následoval	následovat	k5eAaImAgInS	následovat
FC	FC	kA	FC
Internazionale	Internazionale	k1gFnSc2	Internazionale
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1960	[number]	k4	1960
<g/>
/	/	kIx~	/
<g/>
61	[number]	k4	61
zase	zase	k9	zase
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
AS	as	k1gNnSc1	as
Roma	Rom	k1gMnSc2	Rom
Veletržní	veletržní	k2eAgInSc4d1	veletržní
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
úvodní	úvodní	k2eAgInSc4d1	úvodní
ročník	ročník	k1gInSc4	ročník
Poháru	pohár	k1gInSc2	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
ACF	ACF	kA	ACF
Fiorentina	Fiorentina	k1gFnSc1	Fiorentina
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
stylem	styl	k1gInSc7	styl
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
celá	celý	k2eAgFnSc1d1	celá
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Italské	italský	k2eAgInPc4d1	italský
kluby	klub	k1gInPc4	klub
získali	získat	k5eAaPmAgMnP	získat
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
velkých	velký	k2eAgFnPc2d1	velká
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
trofejí	trofej	k1gFnPc2	trofej
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Interkontinentálního	interkontinentální	k2eAgInSc2d1	interkontinentální
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
a	a	k8xC	a
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	a	k9	a
nejznatelnější	znatelný	k2eAgInSc1d3	nejznatelnější
odskok	odskok	k1gInSc1	odskok
v	v	k7c6	v
dominanci	dominance	k1gFnSc6	dominance
velké	velký	k2eAgFnSc2d1	velká
trojky	trojka	k1gFnSc2	trojka
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
FC	FC	kA	FC
Internazionale	Internazionale	k1gFnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k9	pak
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
a	a	k8xC	a
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
kluby	klub	k1gInPc1	klub
jsou	být	k5eAaImIp3nP	být
doposavad	doposavad	k6eAd1	doposavad
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
hvězdu	hvězda	k1gFnSc4	hvězda
za	za	k7c4	za
10	[number]	k4	10
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Juventusu	Juventus	k1gInSc6	Juventus
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
nejprve	nejprve	k6eAd1	nejprve
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
týmu	tým	k1gInSc2	tým
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
i	i	k9	i
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
faktům	fakt	k1gInPc3	fakt
jsou	být	k5eAaImIp3nP	být
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
označována	označován	k2eAgNnPc4d1	označováno
jako	jako	k8xC	jako
La	la	k1gNnPc4	la
Grande	grand	k1gMnSc5	grand
Inter	Intero	k1gNnPc2	Intero
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
nadvláda	nadvláda	k1gFnSc1	nadvláda
Interu	Intera	k1gFnSc4	Intera
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
i	i	k8xC	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
k	k	k7c3	k
životu	život	k1gInSc3	život
probudil	probudit	k5eAaPmAgMnS	probudit
gigant	gigant	k1gMnSc1	gigant
jménem	jméno	k1gNnSc7	jméno
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc4	počátek
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
povadlého	povadlý	k2eAgNnSc2d1	povadlé
AC	AC	kA	AC
přichází	přicházet	k5eAaImIp3nS	přicházet
pozdější	pozdní	k2eAgFnSc1d2	pozdější
politická	politický	k2eAgFnSc1d1	politická
superstar	superstar	k1gFnSc1	superstar
Silvio	Silvio	k1gMnSc1	Silvio
Berlusconi	Berluscon	k1gMnPc1	Berluscon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
klubu	klub	k1gInSc2	klub
získal	získat	k5eAaPmAgMnS	získat
obdiv	obdiv	k1gInSc4	obdiv
i	i	k8xC	i
cenné	cenný	k2eAgFnPc4d1	cenná
trofeje	trofej	k1gFnPc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgFnPc2d1	následující
sezon	sezona	k1gFnPc2	sezona
najdeme	najít	k5eAaPmIp1nP	najít
milánský	milánský	k2eAgInSc4d1	milánský
klub	klub	k1gInSc4	klub
pětkrát	pětkrát	k6eAd1	pětkrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
PMEZ	PMEZ	kA	PMEZ
a	a	k8xC	a
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
hned	hned	k6eAd1	hned
3	[number]	k4	3
<g/>
×	×	k?	×
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
lize	liga	k1gFnSc6	liga
získává	získávat	k5eAaImIp3nS	získávat
dalších	další	k2eAgInPc2d1	další
6	[number]	k4	6
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
na	na	k7c4	na
kótu	kóta	k1gFnSc4	kóta
16	[number]	k4	16
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Zdatně	zdatně	k6eAd1	zdatně
mu	on	k3xPp3gMnSc3	on
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
i	i	k9	i
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
nejvýznamnějších	významný	k2eAgFnPc6d3	nejvýznamnější
evropských	evropský	k2eAgFnPc6d1	Evropská
soutěžích	soutěž	k1gFnPc6	soutěž
(	(	kIx(	(
<g/>
Pohár	pohár	k1gInSc4	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Pohár	pohár	k1gInSc4	pohár
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
Pohár	pohár	k1gInSc1	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
jediným	jediný	k2eAgInSc7d1	jediný
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
6	[number]	k4	6
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
,	,	kIx,	,
jakých	jaký	k3yRgFnPc2	jaký
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
evropský	evropský	k2eAgInSc1d1	evropský
klub	klub	k1gInSc1	klub
účastnit	účastnit	k5eAaImF	účastnit
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
Superpohár	superpohár	k1gInSc1	superpohár
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
Pohár	pohár	k1gInSc4	pohár
Intertoto	Intertota	k1gFnSc5	Intertota
a	a	k8xC	a
Interkontinentální	interkontinentální	k2eAgInSc4d1	interkontinentální
pohár	pohár	k1gInSc4	pohár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dosažených	dosažený	k2eAgInPc2d1	dosažený
úspěchů	úspěch	k1gInPc2	úspěch
turínského	turínský	k2eAgInSc2d1	turínský
klubu	klub	k1gInSc2	klub
musíme	muset	k5eAaImIp1nP	muset
zmínit	zmínit	k5eAaPmF	zmínit
i	i	k9	i
zisk	zisk	k1gInSc4	zisk
druhé	druhý	k4xOgFnSc2	druhý
zlaté	zlatý	k2eAgFnSc2d1	zlatá
hvězdy	hvězda	k1gFnSc2	hvězda
Stella	Stella	k1gFnSc1	Stella
za	za	k7c4	za
zisk	zisk	k1gInSc4	zisk
20	[number]	k4	20
<g/>
.	.	kIx.	.
mistrovského	mistrovský	k2eAgInSc2d1	mistrovský
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Neopomenutelná	opomenutelný	k2eNgFnSc1d1	neopomenutelná
je	být	k5eAaImIp3nS	být
i	i	k9	i
naprostá	naprostý	k2eAgFnSc1d1	naprostá
dominance	dominance	k1gFnSc1	dominance
italských	italský	k2eAgInPc2d1	italský
klubů	klub	k1gInPc2	klub
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
nejvýznamnější	významný	k2eAgFnSc6d3	nejvýznamnější
soutěži	soutěž	k1gFnSc6	soutěž
UEFA	UEFA	kA	UEFA
-	-	kIx~	-
Poháru	pohár	k1gInSc6	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
Itálie	Itálie	k1gFnSc1	Itálie
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
14	[number]	k4	14
finálových	finálový	k2eAgInPc2d1	finálový
účastí	účastí	k1gNnSc2	účastí
svých	svůj	k3xOyFgNnPc2	svůj
mužstev	mužstvo	k1gNnPc2	mužstvo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
hned	hned	k6eAd1	hned
8	[number]	k4	8
vítězných	vítězný	k2eAgFnPc2d1	vítězná
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k6eAd1	nic
podobného	podobný	k2eAgInSc2d1	podobný
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
fotbalu	fotbal	k1gInSc2	fotbal
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
historie	historie	k1gFnSc1	historie
poznamenaná	poznamenaný	k2eAgFnSc1d1	poznamenaná
korupční	korupční	k2eAgFnSc7d1	korupční
aférou	aféra	k1gFnSc7	aféra
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
Serie	serie	k1gFnSc1	serie
se	s	k7c7	s
známkou	známka	k1gFnSc7	známka
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
evropských	evropský	k2eAgFnPc2d1	Evropská
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vždy	vždy	k6eAd1	vždy
dokazovalo	dokazovat	k5eAaImAgNnS	dokazovat
i	i	k9	i
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vydala	vydat	k5eAaPmAgFnS	vydat
FIFA	FIFA	kA	FIFA
žebříček	žebříček	k1gInSc4	žebříček
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
klubů	klub	k1gInPc2	klub
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
měla	mít	k5eAaImAgFnS	mít
zástupce	zástupce	k1gMnPc4	zástupce
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
(	(	kIx(	(
<g/>
Juventus	Juventus	k1gInSc4	Juventus
FC	FC	kA	FC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
děleném	dělený	k2eAgInSc6d1	dělený
devátém	devátý	k4xOgNnSc6	devátý
(	(	kIx(	(
<g/>
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
i	i	k8xC	i
děleném	dělený	k2eAgMnSc6d1	dělený
dvanáctém	dvanáctý	k4xOgInSc6	dvanáctý
(	(	kIx(	(
<g/>
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
<g/>
)	)	kIx)	)
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Žebříček	žebříček	k1gInSc1	žebříček
fotbalových	fotbalový	k2eAgMnPc2d1	fotbalový
historiků	historik	k1gMnPc2	historik
IFFHS	IFFHS	kA	IFFHS
posuzoval	posuzovat	k5eAaImAgMnS	posuzovat
kontinenty	kontinent	k1gInPc4	kontinent
zvlášť	zvlášť	k6eAd1	zvlášť
a	a	k8xC	a
možná	možná	k9	možná
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
italské	italský	k2eAgInPc1d1	italský
kluby	klub	k1gInPc1	klub
umístily	umístit	k5eAaPmAgInP	umístit
ještě	ještě	k9	ještě
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
evropských	evropský	k2eAgInPc2d1	evropský
klubů	klub	k1gInPc2	klub
za	za	k7c4	za
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Juventus	Juventus	k1gInSc1	Juventus
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
Internazionale	Internazionale	k1gFnSc6	Internazionale
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
italských	italský	k2eAgInPc2d1	italský
klubů	klub	k1gInPc2	klub
ty	ten	k3xDgInPc1	ten
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	a	k9	a
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
dočasně	dočasně	k6eAd1	dočasně
usadili	usadit	k5eAaPmAgMnP	usadit
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
římské	římský	k2eAgMnPc4d1	římský
kluby	klub	k1gInPc4	klub
SS	SS	kA	SS
Lazio	Lazio	k1gNnSc4	Lazio
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
AS	as	k9	as
Roma	Rom	k1gMnSc4	Rom
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
panuje	panovat	k5eAaImIp3nS	panovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
velká	velký	k2eAgFnSc1d1	velká
trojka	trojka	k1gFnSc1	trojka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
pošesté	pošesté	k4xO	pošesté
vítězí	vítězit	k5eAaImIp3nS	vítězit
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ročníku	ročník	k1gInSc2	ročník
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
poráží	porážet	k5eAaImIp3nS	porážet
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Nedvědem	Nedvěd	k1gMnSc7	Nedvěd
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
trest	trest	k1gInSc4	trest
však	však	k9	však
nemohl	moct	k5eNaImAgMnS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
utkaly	utkat	k5eAaPmAgInP	utkat
dva	dva	k4xCgInPc1	dva
italské	italský	k2eAgInPc1d1	italský
kluby	klub	k1gInPc1	klub
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
setkání	setkání	k1gNnSc1	setkání
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
Juve	Juve	k1gFnSc4	Juve
již	již	k9	již
pátou	pátý	k4xOgFnSc4	pátý
finálovou	finálový	k2eAgFnSc4d1	finálová
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
Si	se	k3xPyFc3	se
spravil	spravit	k5eAaPmAgMnS	spravit
chuť	chuť	k1gFnSc4	chuť
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vítězí	vítězit	k5eAaImIp3nS	vítězit
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Korupční	korupční	k2eAgFnSc1d1	korupční
aféra	aféra	k1gFnSc1	aféra
2006	[number]	k4	2006
====	====	k?	====
</s>
</p>
<p>
<s>
Rozvíjející	rozvíjející	k2eAgFnSc4d1	rozvíjející
se	se	k3xPyFc4	se
hegemonii	hegemonie	k1gFnSc4	hegemonie
Juventusu	Juventus	k1gInSc2	Juventus
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
narušil	narušit	k5eAaPmAgMnS	narušit
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pro	pro	k7c4	pro
následující	následující	k2eAgInPc4d1	následující
dva	dva	k4xCgInPc4	dva
ročníky	ročník	k1gInPc4	ročník
znovu	znovu	k6eAd1	znovu
převzal	převzít	k5eAaPmAgInS	převzít
mistrovskou	mistrovský	k2eAgFnSc4d1	mistrovská
"	"	kIx"	"
<g/>
taktovku	taktovka	k1gFnSc4	taktovka
<g/>
"	"	kIx"	"
Juventus	Juventus	k1gInSc1	Juventus
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
radovat	radovat	k5eAaImF	radovat
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
začala	začít	k5eAaPmAgFnS	začít
italská	italský	k2eAgFnSc1d1	italská
Policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
telefonních	telefonní	k2eAgInPc2d1	telefonní
odposlechů	odposlech	k1gInPc2	odposlech
<g/>
,	,	kIx,	,
rozplétat	rozplétat	k5eAaImF	rozplétat
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
korupčních	korupční	k2eAgInPc2d1	korupční
skandálů	skandál	k1gInPc2	skandál
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Vážně	vážně	k6eAd1	vážně
bylo	být	k5eAaImAgNnS	být
zapleteno	zaplést	k5eAaPmNgNnS	zaplést
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
klubů	klub	k1gInPc2	klub
ze	z	k7c2	z
Serie	serie	k1gFnSc2	serie
A	a	k9	a
i	i	k9	i
Serie	serie	k1gFnSc1	serie
B.	B.	kA	B.
Obviněni	obviněn	k2eAgMnPc1d1	obviněn
byli	být	k5eAaImAgMnP	být
Juventus	Juventus	k1gInSc4	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
ACF	ACF	kA	ACF
Fiorentina	Fiorentina	k1gFnSc1	Fiorentina
<g/>
,	,	kIx,	,
SS	SS	kA	SS
Lazio	Lazio	k1gNnSc1	Lazio
<g/>
,	,	kIx,	,
Reggina	Reggina	k1gFnSc1	Reggina
Calcio	Calcio	k1gNnSc1	Calcio
a	a	k8xC	a
Atletico	Atletico	k1gNnSc1	Atletico
Arezzo	Arezza	k1gFnSc5	Arezza
(	(	kIx(	(
<g/>
Serie	serie	k1gFnSc2	serie
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
obětí	oběť	k1gFnSc7	oběť
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
svazu	svaz	k1gInSc2	svaz
FIGC	FIGC	kA	FIGC
Franco	Franco	k1gMnSc1	Franco
Carraro	Carrara	k1gFnSc5	Carrara
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byly	být	k5eAaImAgFnP	být
vyneseny	vynesen	k2eAgInPc4d1	vynesen
tresty	trest	k1gInPc4	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
trest	trest	k1gInSc4	trest
dostal	dostat	k5eAaPmAgInS	dostat
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Mistru	mistr	k1gMnSc3	mistr
Serie	serie	k1gFnSc2	serie
A	a	k8xC	a
ze	z	k7c2	z
sezon	sezona	k1gFnPc2	sezona
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
byl	být	k5eAaImAgMnS	být
odebrán	odebrán	k2eAgMnSc1d1	odebrán
prvně	prvně	k?	prvně
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
titul	titul	k1gInSc1	titul
a	a	k8xC	a
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
sezony	sezona	k1gFnSc2	sezona
odečteno	odečten	k2eAgNnSc1d1	odečteno
všech	všecek	k3xTgInPc2	všecek
91	[number]	k4	91
získaných	získaný	k2eAgInPc2d1	získaný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
klesl	klesnout	k5eAaPmAgMnS	klesnout
na	na	k7c4	na
poslední	poslední	k2eAgNnSc4d1	poslední
místo	místo	k1gNnSc4	místo
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
do	do	k7c2	do
Serie	serie	k1gFnSc2	serie
B.	B.	kA	B.
Navíc	navíc	k6eAd1	navíc
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
znemožněno	znemožnit	k5eAaPmNgNnS	znemožnit
startovat	startovat	k5eAaBmF	startovat
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
a	a	k8xC	a
odečteno	odečíst	k5eAaPmNgNnS	odečíst
30	[number]	k4	30
bodů	bod	k1gInPc2	bod
pro	pro	k7c4	pro
ročník	ročník	k1gInSc4	ročník
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
startoval	startovat	k5eAaBmAgInS	startovat
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
B.	B.	kA	B.
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
44	[number]	k4	44
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
mimo	mimo	k7c4	mimo
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
a	a	k8xC	a
pro	pro	k7c4	pro
ročník	ročník	k1gInSc4	ročník
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
15	[number]	k4	15
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Ostatním	ostatní	k2eAgInPc3d1	ostatní
týmům	tým	k1gInPc3	tým
byly	být	k5eAaImAgFnP	být
odečteny	odečten	k2eAgInPc4d1	odečten
body	bod	k1gInPc4	bod
v	v	k7c6	v
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
sezoně	sezona	k1gFnSc6	sezona
a	a	k8xC	a
znemožněn	znemožněn	k2eAgInSc4d1	znemožněn
start	start	k1gInSc4	start
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
situace	situace	k1gFnSc2	situace
vytěžil	vytěžit	k5eAaPmAgInS	vytěžit
nejvíce	nejvíce	k6eAd1	nejvíce
jediný	jediný	k2eAgInSc1d1	jediný
nezapletený	zapletený	k2eNgInSc1d1	zapletený
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
trojky	trojka	k1gFnSc2	trojka
-	-	kIx~	-
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
mu	on	k3xPp3gMnSc3	on
připadl	připadnout	k5eAaPmAgMnS	připadnout
titul	titul	k1gInSc4	titul
ze	z	k7c2	z
sezony	sezona	k1gFnSc2	sezona
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
původně	původně	k6eAd1	původně
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
hlavně	hlavně	k9	hlavně
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
největší	veliký	k2eAgMnPc4d3	veliký
soupeře	soupeř	k1gMnPc4	soupeř
pro	pro	k7c4	pro
následující	následující	k2eAgInPc4d1	následující
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
naskytnutou	naskytnutý	k2eAgFnSc4d1	naskytnutý
možnost	možnost	k1gFnSc4	možnost
využil	využít	k5eAaPmAgMnS	využít
znamenitě	znamenitě	k6eAd1	znamenitě
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
třetí	třetí	k4xOgInSc1	třetí
klub	klub	k1gInSc1	klub
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
dokázal	dokázat	k5eAaPmAgInS	dokázat
5	[number]	k4	5
<g/>
×	×	k?	×
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zvítězit	zvítězit	k5eAaPmF	zvítězit
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	a	k8xC	a
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
převzal	převzít	k5eAaPmAgInS	převzít
i	i	k9	i
pozici	pozice	k1gFnSc4	pozice
nejsilnějšího	silný	k2eAgInSc2d3	nejsilnější
italského	italský	k2eAgInSc2d1	italský
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Juventus	Juventus	k1gInSc1	Juventus
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vrátil	vrátit	k5eAaPmAgMnS	vrátit
hned	hned	k6eAd1	hned
po	po	k7c6	po
první	první	k4xOgFnSc6	první
sezoně	sezona	k1gFnSc6	sezona
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
B	B	kA	B
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
94	[number]	k4	94
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
85	[number]	k4	85
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
u	u	k7c2	u
odvolací	odvolací	k2eAgFnSc2d1	odvolací
komise	komise	k1gFnSc2	komise
podařilo	podařit	k5eAaPmAgNnS	podařit
snížit	snížit	k5eAaPmF	snížit
trest	trest	k1gInSc4	trest
ze	z	k7c2	z
30	[number]	k4	30
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
9	[number]	k4	9
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
Serii	serie	k1gFnSc6	serie
A	a	k9	a
opouštěl	opouštět	k5eAaImAgMnS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
Interu	Inter	k1gInSc2	Inter
gradovala	gradovat	k5eAaImAgFnS	gradovat
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
José	José	k1gNnSc2	José
Mourinha	Mourinha	k1gFnSc1	Mourinha
popáté	popáté	k4xO	popáté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
získal	získat	k5eAaPmAgMnS	získat
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
18	[number]	k4	18
se	se	k3xPyFc4	se
posunul	posunout	k5eAaPmAgInS	posunout
před	před	k7c7	před
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
a	a	k8xC	a
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
ještě	ještě	k6eAd1	ještě
získal	získat	k5eAaPmAgMnS	získat
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
Mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
klubů	klub	k1gInPc2	klub
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
AC	AC	kA	AC
Milán	Milán	k1gInSc1	Milán
a	a	k8xC	a
dotáhl	dotáhnout	k5eAaPmAgMnS	dotáhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
dělené	dělený	k2eAgNnSc4d1	dělené
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
FC	FC	kA	FC
Internazionale	Internazionale	k1gFnSc7	Internazionale
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
návratu	návrat	k1gInSc2	návrat
ze	z	k7c2	z
Serie	serie	k1gFnSc2	serie
B	B	kA	B
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
radovat	radovat	k5eAaImF	radovat
i	i	k9	i
Juventus	Juventus	k1gInSc4	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
získal	získat	k5eAaPmAgMnS	získat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
28	[number]	k4	28
<g/>
.	.	kIx.	.
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
neohrožená	ohrožený	k2eNgFnSc1d1	neohrožená
dominance	dominance	k1gFnSc1	dominance
Juventusu	Juventus	k1gInSc2	Juventus
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k9	i
díky	díky	k7c3	díky
krizi	krize	k1gFnSc4	krize
obou	dva	k4xCgInPc2	dva
milánských	milánský	k2eAgInPc2d1	milánský
klubů	klub	k1gInPc2	klub
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obhájit	obhájit	k5eAaPmF	obhájit
titul	titul	k1gInSc4	titul
i	i	k9	i
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
již	již	k9	již
31	[number]	k4	31
titulů	titul	k1gInPc2	titul
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
navíc	navíc	k6eAd1	navíc
dokázal	dokázat	k5eAaPmAgInS	dokázat
Juventus	Juventus	k1gInSc1	Juventus
postoupit	postoupit	k5eAaPmF	postoupit
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakonec	nakonec	k6eAd1	nakonec
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
týmu	tým	k1gInSc2	tým
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
2005	[number]	k4	2005
-	-	kIx~	-
titul	titul	k1gInSc1	titul
odebrán	odebrán	k2eAgInSc1d1	odebrán
(	(	kIx(	(
<g/>
korupční	korupční	k2eAgFnSc1d1	korupční
aféra	aféra	k1gFnSc1	aféra
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
-	-	kIx~	-
titul	titul	k1gInSc1	titul
odebrán	odebrán	k2eAgInSc1d1	odebrán
(	(	kIx(	(
<g/>
korupční	korupční	k2eAgFnSc1d1	korupční
aféra	aféra	k1gFnSc1	aféra
v	v	k7c6	v
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Torino	Torin	k2eAgNnSc4d1	Torino
FC	FC	kA	FC
1927	[number]	k4	1927
-	-	kIx~	-
titul	titul	k1gInSc1	titul
odebrán	odebrat	k5eAaPmNgInS	odebrat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
sportovních	sportovní	k2eAgInPc2d1	sportovní
přestupků	přestupek	k1gInPc2	přestupek
</s>
</p>
<p>
<s>
==	==	k?	==
Vítězové	vítěz	k1gMnPc1	vítěz
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
ročnících	ročník	k1gInPc6	ročník
==	==	k?	==
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
sv	sv	kA	sv
Soutěž	soutěž	k1gFnSc1	soutěž
nebyla	být	k5eNaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
kvůli	kvůli	k7c3	kvůli
začátku	začátek	k1gInSc2	začátek
První	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
dodatečně	dodatečně	k6eAd1	dodatečně
udělil	udělit	k5eAaPmAgInS	udělit
titul	titul	k1gInSc1	titul
týmu	tým	k1gInSc2	tým
Genoa	Genoum	k1gNnSc2	Genoum
CFC	CFC	kA	CFC
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
7	[number]	k4	7
<g/>
.	.	kIx.	.
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
místa	místo	k1gNnPc1	místo
dodatečně	dodatečně	k6eAd1	dodatečně
zůstala	zůstat	k5eAaPmAgNnP	zůstat
neobsazena	obsadit	k5eNaPmNgNnP	obsadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
AMB	ambo	k1gNnPc2	ambo
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1928	[number]	k4	1928
byl	být	k5eAaImAgMnS	být
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
sloučen	sloučen	k2eAgInSc1d1	sloučen
s	s	k7c7	s
Unione	union	k1gInSc5	union
Sportiva	Sportiva	k1gFnSc1	Sportiva
Milanese	Milanese	k1gFnSc1	Milanese
a	a	k8xC	a
klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c6	na
Associazione	Associazion	k1gInSc5	Associazion
Sportiva	Sportivum	k1gNnSc2	Sportivum
Ambrosiana	Ambrosiana	k1gFnSc1	Ambrosiana
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělen	rozdělen	k2eAgMnSc1d1	rozdělen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Internazionale	Internazionale	k1gFnSc2	Internazionale
<g/>
.	.	kIx.	.
</s>
<s>
Unione	union	k1gInSc5	union
Sportiva	Sportiva	k1gFnSc1	Sportiva
rozdělením	rozdělení	k1gNnSc7	rozdělení
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
úspěchy	úspěch	k1gInPc1	úspěch
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
3	[number]	k4	3
titulů	titul	k1gInPc2	titul
<g/>
)	)	kIx)	)
i	i	k9	i
historie	historie	k1gFnSc1	historie
zápasů	zápas	k1gInPc2	zápas
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
Internazionale	Internazionale	k1gFnSc4	Internazionale
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
sportovních	sportovní	k2eAgInPc2d1	sportovní
přestupků	přestupek	k1gInPc2	přestupek
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
odebrán	odebrán	k2eAgInSc1d1	odebrán
týmu	tým	k1gInSc2	tým
Torino	Torino	k1gNnSc4	Torino
FC	FC	kA	FC
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ho	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
premiérově	premiérově	k6eAd1	premiérově
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
Titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
odebrán	odebrat	k5eAaPmNgInS	odebrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
Titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
odebrány	odebrán	k2eAgInPc4d1	odebrán
všechny	všechen	k3xTgInPc4	všechen
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
obsadil	obsadit	k5eAaPmAgMnS	obsadit
poslední	poslední	k2eAgNnSc4d1	poslední
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
posunul	posunout	k5eAaPmAgMnS	posunout
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
a	a	k8xC	a
Juventus	Juventus	k1gInSc4	Juventus
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
do	do	k7c2	do
Serie	serie	k1gFnSc2	serie
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Historická	historický	k2eAgFnSc1d1	historická
tabulka	tabulka	k1gFnSc1	tabulka
střelců	střelec	k1gMnPc2	střelec
Serie	serie	k1gFnSc2	serie
A	A	kA	A
==	==	k?	==
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
psaný	psaný	k2eAgMnSc1d1	psaný
kurzívou	kurzíva	k1gFnSc7	kurzíva
je	on	k3xPp3gFnPc4	on
aktivní	aktivní	k2eAgFnPc4d1	aktivní
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
psaný	psaný	k2eAgMnSc1d1	psaný
tučně	tučně	k6eAd1	tučně
a	a	k8xC	a
kurzívou	kurzíva	k1gFnSc7	kurzíva
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
Serii	serie	k1gFnSc6	serie
A	a	k9	a
</s>
</p>
<p>
<s>
V	v	k7c6	v
kolonce	kolonka	k1gFnSc6	kolonka
klub	klub	k1gInSc1	klub
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
daný	daný	k2eAgMnSc1d1	daný
hráč	hráč	k1gMnSc1	hráč
odehrál	odehrát	k5eAaPmAgMnS	odehrát
nejvíce	nejvíce	k6eAd1	nejvíce
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
<g/>
Nejvíce	hodně	k6eAd3	hodně
branek	branka	k1gFnPc2	branka
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2007	[number]	k4	2007
-	-	kIx~	-
2008	[number]	k4	2008
Alessandro	Alessandra	k1gFnSc5	Alessandra
Del	Del	k1gMnSc3	Del
Piero	Piero	k1gNnSc4	Piero
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
21	[number]	k4	21
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2008	[number]	k4	2008
-	-	kIx~	-
2009	[number]	k4	2009
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
nejvíce	nejvíce	k6eAd1	nejvíce
branek	branka	k1gFnPc2	branka
Zlatan	Zlatan	k1gInSc1	Zlatan
Ibrahimovič	Ibrahimovič	k1gInSc4	Ibrahimovič
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgInS	trefit
25	[number]	k4	25
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Di	Di	k1gMnSc1	Di
Natale	Natal	k1gInSc5	Natal
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
titulem	titul	k1gInSc7	titul
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
Serie	serie	k1gFnSc2	serie
A	A	kA	A
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2009-2010	[number]	k4	2009-2010
s	s	k7c7	s
28	[number]	k4	28
brankami	branka	k1gFnPc7	branka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2011	[number]	k4	2011
-	-	kIx~	-
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
ligy	liga	k1gFnSc2	liga
s	s	k7c7	s
28	[number]	k4	28
góly	gól	k1gInPc4	gól
Zlatan	Zlatan	k1gInSc1	Zlatan
Ibrahimović	Ibrahimović	k1gMnPc2	Ibrahimović
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2012	[number]	k4	2012
-	-	kIx~	-
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
Edinson	Edinsona	k1gFnPc2	Edinsona
Cavani	Cavaň	k1gFnSc3	Cavaň
z	z	k7c2	z
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
29	[number]	k4	29
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Coppa	Coppa	k1gFnSc1	Coppa
Italia	Italia	k1gFnSc1	Italia
</s>
</p>
<p>
<s>
Supercoppa	Supercoppa	k1gFnSc1	Supercoppa
italiana	italiana	k1gFnSc1	italiana
</s>
</p>
<p>
<s>
Serie	serie	k1gFnSc1	serie
B	B	kA	B
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Serie	serie	k1gFnSc2	serie
A	a	k9	a
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
ze	z	k7c2	z
Serie	serie	k1gFnSc2	serie
A	a	k9	a
</s>
</p>
