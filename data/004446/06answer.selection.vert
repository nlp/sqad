<s>
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
čp.	čp.	k?	čp.
16	[number]	k4	16
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kaňkově	Kaňkův	k2eAgInSc6d1	Kaňkův
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
advokátovi	advokát	k1gMnSc3	advokát
JUDr.	JUDr.	kA	JUDr.
Janu	Jan	k1gMnSc3	Jan
Kaňkovi	Kaňka	k1gMnSc3	Kaňka
odkázala	odkázat	k5eAaPmAgFnS	odkázat
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
nadaci	nadace	k1gFnSc4	nadace
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
advokáty	advokát	k1gMnPc4	advokát
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc4	jejich
vdovy	vdova	k1gFnPc4	vdova
a	a	k8xC	a
sirotky	sirotka	k1gFnPc4	sirotka
<g/>
.	.	kIx.	.
</s>
