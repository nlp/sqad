<s>
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
ČAK	čak	k0	čak
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stavovským	stavovský	k2eAgNnSc7d1	Stavovské
sdružením	sdružení	k1gNnSc7	sdružení
advokátů	advokát	k1gMnPc2	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
sídlo	sídlo	k1gNnSc1	sídlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pobočku	pobočka	k1gFnSc4	pobočka
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
advokáty	advokát	k1gMnPc4	advokát
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
právnické	právnický	k2eAgNnSc4d1	právnické
profesní	profesní	k2eAgNnSc4d1	profesní
sdružení	sdružení	k1gNnSc4	sdružení
samosprávy	samospráva	k1gFnSc2	samospráva
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
vykonávat	vykonávat	k5eAaImF	vykonávat
samosprávu	samospráva	k1gFnSc4	samospráva
advokacie	advokacie	k1gFnSc2	advokacie
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
chránit	chránit	k5eAaImF	chránit
a	a	k8xC	a
garantovat	garantovat	k5eAaBmF	garantovat
kvalitu	kvalita	k1gFnSc4	kvalita
právních	právní	k2eAgFnPc2d1	právní
služeb	služba	k1gFnPc2	služba
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
advokáty	advokát	k1gMnPc7	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Komora	komora	k1gFnSc1	komora
je	být	k5eAaImIp3nS	být
veřejnoprávní	veřejnoprávní	k2eAgFnSc7d1	veřejnoprávní
korporací	korporace	k1gFnSc7	korporace
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
součástí	součást	k1gFnSc7	součást
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
právnická	právnický	k2eAgFnSc1d1	právnická
osoba	osoba	k1gFnSc1	osoba
veřejného	veřejný	k2eAgNnSc2d1	veřejné
práva	právo	k1gNnSc2	právo
je	být	k5eAaImIp3nS	být
zřizována	zřizovat	k5eAaImNgFnS	zřizovat
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
nezapisuje	zapisovat	k5eNaImIp3nS	zapisovat
se	se	k3xPyFc4	se
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
pobočka	pobočka	k1gFnSc1	pobočka
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
působnost	působnost	k1gFnSc4	působnost
komory	komora	k1gFnSc2	komora
pro	pro	k7c4	pro
evropské	evropský	k2eAgMnPc4d1	evropský
advokáty	advokát	k1gMnPc4	advokát
a	a	k8xC	a
pro	pro	k7c4	pro
advokáty	advokát	k1gMnPc4	advokát
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
koncipienty	koncipient	k1gMnPc4	koncipient
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
vrchního	vrchní	k2eAgInSc2d1	vrchní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
čp.	čp.	k?	čp.
16	[number]	k4	16
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kaňkově	Kaňkův	k2eAgInSc6d1	Kaňkův
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
advokátovi	advokát	k1gMnSc3	advokát
JUDr.	JUDr.	kA	JUDr.
Janu	Jan	k1gMnSc3	Jan
Kaňkovi	Kaňka	k1gMnSc3	Kaňka
odkázala	odkázat	k5eAaPmAgFnS	odkázat
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
nadaci	nadace	k1gFnSc4	nadace
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
advokáty	advokát	k1gMnPc4	advokát
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc4	jejich
vdovy	vdova	k1gFnPc4	vdova
a	a	k8xC	a
sirotky	sirotka	k1gFnPc4	sirotka
<g/>
.	.	kIx.	.
</s>
<s>
Sídlila	sídlit	k5eAaImAgFnS	sídlit
zda	zda	k8xS	zda
pražská	pražský	k2eAgFnSc1d1	Pražská
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
advokátních	advokátní	k2eAgFnPc2d1	advokátní
poraden	poradna	k1gFnPc2	poradna
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
ČAK	čak	k0	čak
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
pobočka	pobočka	k1gFnSc1	pobočka
pak	pak	k6eAd1	pak
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Kleinově	Kleinův	k2eAgInSc6d1	Kleinův
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
organizace	organizace	k1gFnSc1	organizace
České	český	k2eAgFnSc2d1	Česká
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
č.	č.	k?	č.
85	[number]	k4	85
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
advokacii	advokacie	k1gFnSc6	advokacie
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gInPc7	její
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
sněm	sněm	k1gInSc1	sněm
–	–	k?	–
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
orgán	orgán	k1gInSc1	orgán
komory	komora	k1gFnSc2	komora
představenstvo	představenstvo	k1gNnSc1	představenstvo
–	–	k?	–
výkonný	výkonný	k2eAgMnSc1d1	výkonný
orgán	orgán	k1gMnSc1	orgán
komory	komora	k1gFnSc2	komora
o	o	k7c6	o
11	[number]	k4	11
členech	člen	k1gInPc6	člen
předseda	předseda	k1gMnSc1	předseda
komory	komora	k1gFnSc2	komora
–	–	k?	–
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
komoru	komora	k1gFnSc4	komora
navenek	navenek	k6eAd1	navenek
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
rada	rada	k1gFnSc1	rada
–	–	k?	–
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
orgán	orgán	k1gInSc4	orgán
komory	komora	k1gFnSc2	komora
o	o	k7c6	o
54	[number]	k4	54
členech	člen	k1gInPc6	člen
kárná	kárný	k2eAgFnSc1d1	kárná
komise	komise	k1gFnSc1	komise
–	–	k?	–
provádí	provádět	k5eAaImIp3nS	provádět
u	u	k7c2	u
advokátů	advokát	k1gMnPc2	advokát
kárné	kárný	k2eAgNnSc1d1	kárné
řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
83	[number]	k4	83
členů	člen	k1gInPc2	člen
odvolací	odvolací	k2eAgFnSc1d1	odvolací
kárná	kárný	k2eAgFnSc1d1	kárná
komise	komise	k1gFnSc1	komise
–	–	k?	–
odvolací	odvolací	k2eAgFnPc4d1	odvolací
instance	instance	k1gFnPc4	instance
v	v	k7c6	v
kárném	kárný	k2eAgNnSc6d1	kárné
řízení	řízení	k1gNnSc6	řízení
o	o	k7c6	o
11	[number]	k4	11
členech	člen	k1gInPc6	člen
zkušební	zkušební	k2eAgFnSc2d1	zkušební
komise	komise	k1gFnSc2	komise
–	–	k?	–
provádí	provádět	k5eAaImIp3nS	provádět
advokátní	advokátní	k2eAgFnPc4d1	advokátní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
zkoušky	zkouška	k1gFnPc4	zkouška
způsobilosti	způsobilost	k1gFnSc2	způsobilost
a	a	k8xC	a
uznávací	uznávací	k2eAgFnSc2d1	uznávací
zkoušky	zkouška	k1gFnSc2	zkouška
Funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
orgánech	orgán	k1gInPc6	orgán
komory	komora	k1gFnPc1	komora
jsou	být	k5eAaImIp3nP	být
čestné	čestný	k2eAgFnPc1d1	čestná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
advokacie	advokacie	k1gFnSc2	advokacie
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
Ius	Ius	k1gFnSc6	Ius
regale	regale	k6eAd1	regale
montanorum	montanorum	k1gInSc4	montanorum
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
diferenciaci	diferenciace	k1gFnSc3	diferenciace
na	na	k7c4	na
řečníky	řečník	k1gMnPc4	řečník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
zároveň	zároveň	k6eAd1	zároveň
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
klientem	klient	k1gMnSc7	klient
<g/>
,	,	kIx,	,
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
poručníky	poručník	k1gMnPc7	poručník
pře	pře	k1gFnSc2	pře
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gInSc4	on
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc4d1	podobné
postavení	postavení	k1gNnSc4	postavení
mají	mít	k5eAaImIp3nP	mít
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
lavic	lavice	k1gFnPc2	lavice
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
ale	ale	k9	ale
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
pomoc	pomoc	k1gFnSc4	pomoc
jen	jen	k9	jen
co	co	k3yInSc1	co
týče	týkat	k5eAaImIp3nS	týkat
procesního	procesní	k2eAgNnSc2d1	procesní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnSc1d1	právní
regulace	regulace	k1gFnSc1	regulace
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
Koldínových	Koldínův	k2eAgNnPc6d1	Koldínův
Právech	právo	k1gNnPc6	právo
městských	městský	k2eAgNnPc6d1	Městské
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
advokátní	advokátní	k2eAgInSc4d1	advokátní
řád	řád	k1gInSc4	řád
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
usnesení	usnesení	k1gNnSc4	usnesení
generálního	generální	k2eAgInSc2d1	generální
sněmu	sněm	k1gInSc2	sněm
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1615	[number]	k4	1615
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
Obnoveným	obnovený	k2eAgNnSc7d1	obnovené
zřízením	zřízení	k1gNnSc7	zřízení
zemským	zemský	k2eAgNnSc7d1	zemské
se	se	k3xPyFc4	se
výkonu	výkon	k1gInSc3	výkon
advokacie	advokacie	k1gFnSc2	advokacie
dostává	dostávat	k5eAaImIp3nS	dostávat
pevnějšího	pevný	k2eAgInSc2d2	pevnější
rámce	rámec	k1gInSc2	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
prokurátory	prokurátor	k1gMnPc4	prokurátor
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
významu	význam	k1gInSc6	význam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
u	u	k7c2	u
soudního	soudní	k2eAgNnSc2d1	soudní
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
advokáty	advokát	k1gMnPc4	advokát
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
oprávněni	oprávněn	k2eAgMnPc1d1	oprávněn
pouze	pouze	k6eAd1	pouze
sepisovat	sepisovat	k5eAaImF	sepisovat
podání	podání	k1gNnSc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
platí	platit	k5eAaImIp3nS	platit
nová	nový	k2eAgFnSc1d1	nová
úprava	úprava	k1gFnSc1	úprava
<g/>
,	,	kIx,	,
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
Josefínském	josefínský	k2eAgInSc6d1	josefínský
soudním	soudní	k2eAgInSc6d1	soudní
řádu	řád	k1gInSc6	řád
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
advokacie	advokacie	k1gFnSc2	advokacie
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k9	jako
nepominutelná	pominutelný	k2eNgFnSc1d1	nepominutelná
podmínka	podmínka	k1gFnSc1	podmínka
předepsán	předepsán	k2eAgInSc1d1	předepsán
doktorát	doktorát	k1gInSc4	doktorát
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
advokátní	advokátní	k2eAgFnSc1d1	advokátní
zkouška	zkouška	k1gFnSc1	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
tehdy	tehdy	k6eAd1	tehdy
zaveden	zavést	k5eAaPmNgInS	zavést
numerus	numerus	k1gInSc1	numerus
clausus	clausus	k1gInSc1	clausus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
striktní	striktní	k2eAgFnPc1d1	striktní
kvóty	kvóta	k1gFnPc1	kvóta
na	na	k7c4	na
počet	počet	k1gInSc4	počet
advokátů	advokát	k1gMnPc2	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
instituce	instituce	k1gFnSc1	instituce
pro	pro	k7c4	pro
advokáty	advokát	k1gMnPc4	advokát
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
však	však	k9	však
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1	novodobá
úprava	úprava	k1gFnSc1	úprava
advokacie	advokacie	k1gFnSc2	advokacie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k9	až
přijetím	přijetí	k1gNnSc7	přijetí
provizorního	provizorní	k2eAgInSc2d1	provizorní
advokátního	advokátní	k2eAgInSc2d1	advokátní
řádu	řád	k1gInSc2	řád
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ustavil	ustavit	k5eAaPmAgMnS	ustavit
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
definitivního	definitivní	k2eAgInSc2d1	definitivní
advokátního	advokátní	k2eAgInSc2d1	advokátní
řádu	řád	k1gInSc2	řád
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
řádu	řád	k1gInSc2	řád
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
každý	každý	k3xTgMnSc1	každý
advokát	advokát	k1gMnSc1	advokát
členem	člen	k1gMnSc7	člen
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
však	však	k9	však
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
každý	každý	k3xTgInSc1	každý
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
zákonných	zákonný	k2eAgFnPc2d1	zákonná
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
domovské	domovský	k2eAgNnSc1d1	domovské
právo	právo	k1gNnSc1	právo
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
svéprávnost	svéprávnost	k1gFnSc4	svéprávnost
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
letá	letý	k2eAgFnSc1d1	letá
právní	právní	k2eAgFnSc1d1	právní
praxe	praxe	k1gFnSc1	praxe
<g/>
,	,	kIx,	,
advokátní	advokátní	k2eAgFnSc1d1	advokátní
zkouška	zkouška	k1gFnSc1	zkouška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Numerus	Numerus	k1gInSc1	Numerus
clausus	clausus	k1gInSc1	clausus
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
advokátní	advokátní	k2eAgInSc1d1	advokátní
kárný	kárný	k2eAgInSc1d1	kárný
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
zřízena	zřízen	k2eAgFnSc1d1	zřízena
disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
instanci	instance	k1gFnSc6	instance
pak	pak	k6eAd1	pak
kárné	kárný	k2eAgInPc4d1	kárný
prohřešky	prohřešek	k1gInPc4	prohřešek
posuzoval	posuzovat	k5eAaImAgInS	posuzovat
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
jsou	být	k5eAaImIp3nP	být
odměny	odměna	k1gFnPc1	odměna
advokátů	advokát	k1gMnPc2	advokát
jen	jen	k6eAd1	jen
smluvní	smluvní	k2eAgFnPc1d1	smluvní
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
vydán	vydán	k2eAgInSc1d1	vydán
advokátní	advokátní	k2eAgInSc1d1	advokátní
tarif	tarif	k1gInSc1	tarif
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
3	[number]	k4	3
tarifní	tarifní	k2eAgFnSc2d1	tarifní
třídy	třída	k1gFnSc2	třída
podle	podle	k7c2	podle
působnosti	působnost	k1gFnSc2	působnost
toho	ten	k3xDgNnSc2	ten
kterého	který	k3yRgMnSc4	který
advokáta	advokát	k1gMnSc4	advokát
(	(	kIx(	(
<g/>
I.	I.	kA	I.
třída	třída	k1gFnSc1	třída
Vídeň	Vídeň	k1gFnSc1	Vídeň
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
ostatní	ostatní	k2eAgNnPc1d1	ostatní
místa	místo	k1gNnPc1	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1904	[number]	k4	1904
je	být	k5eAaImIp3nS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
advokátům	advokát	k1gMnPc3	advokát
nosit	nosit	k5eAaImF	nosit
talár	talár	k1gInSc4	talár
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
zvaný	zvaný	k2eAgMnSc1d1	zvaný
jako	jako	k8xS	jako
roucho	roucho	k1gNnSc1	roucho
úřední	úřední	k2eAgNnSc1d1	úřední
<g/>
.	.	kIx.	.
</s>
<s>
Advokátní	advokátní	k2eAgFnPc1d1	advokátní
komory	komora	k1gFnPc1	komora
byly	být	k5eAaImAgFnP	být
zřizovány	zřizován	k2eAgInPc1d1	zřizován
v	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
a	a	k8xC	a
s	s	k7c7	s
územní	územní	k2eAgFnSc7d1	územní
působností	působnost	k1gFnSc7	působnost
totožnou	totožný	k2eAgFnSc4d1	totožná
s	s	k7c7	s
vrchními	vrchní	k2eAgInPc7d1	vrchní
zemskými	zemský	k2eAgInPc7d1	zemský
soudy	soud	k1gInPc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
působila	působit	k5eAaImAgFnS	působit
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
původně	původně	k6eAd1	původně
jen	jen	k9	jen
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tam	tam	k6eAd1	tam
sídlil	sídlit	k5eAaImAgInS	sídlit
vrchní	vrchní	k2eAgInSc1d1	vrchní
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
společný	společný	k2eAgInSc1d1	společný
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
je	být	k5eAaImIp3nS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
i	i	k8xC	i
slezská	slezský	k2eAgFnSc1d1	Slezská
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
delegátů	delegát	k1gMnPc2	delegát
rakouských	rakouský	k2eAgFnPc2d1	rakouská
advokátních	advokátní	k2eAgFnPc2d1	advokátní
komor	komora	k1gFnPc2	komora
ustavena	ustavit	k5eAaPmNgFnS	ustavit
rakouská	rakouský	k2eAgFnSc1d1	rakouská
Stálá	stálý	k2eAgFnSc1d1	stálá
delegace	delegace	k1gFnSc1	delegace
advokátních	advokátní	k2eAgFnPc2d1	advokátní
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Komory	komora	k1gFnPc1	komora
jednaly	jednat	k5eAaImAgFnP	jednat
buď	buď	k8xC	buď
ve	v	k7c6	v
shromáždění	shromáždění	k1gNnSc6	shromáždění
všech	všecek	k3xTgInPc2	všecek
členů	člen	k1gInPc2	člen
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svého	svůj	k3xOyFgInSc2	svůj
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
členů	člen	k1gMnPc2	člen
tajně	tajně	k6eAd1	tajně
volilo	volit	k5eAaImAgNnS	volit
nadpoloviční	nadpoloviční	k2eAgFnSc7d1	nadpoloviční
většinou	většina	k1gFnSc7	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgNnPc2	tři
let	let	k1gInSc4	let
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc4	jeho
náměstka	náměstek	k1gMnSc4	náměstek
a	a	k8xC	a
členy	člen	k1gInPc4	člen
výboru	výbor	k1gInSc2	výbor
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgNnPc2	tři
let	let	k1gInSc1	let
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc4	člen
a	a	k8xC	a
náhradníky	náhradník	k1gMnPc4	náhradník
disciplinární	disciplinární	k2eAgFnSc2d1	disciplinární
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
volilo	volit	k5eAaImAgNnS	volit
zkušební	zkušební	k2eAgFnSc4d1	zkušební
komisaře	komisař	k1gMnSc2	komisař
pro	pro	k7c4	pro
advokátní	advokátní	k2eAgFnPc4d1	advokátní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
členů	člen	k1gMnPc2	člen
komory	komora	k1gFnSc2	komora
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
jednacím	jednací	k2eAgInSc6d1	jednací
řádu	řád	k1gInSc6	řád
komory	komora	k1gFnSc2	komora
i	i	k8xC	i
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
členů	člen	k1gMnPc2	člen
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
rozpočtu	rozpočet	k1gInSc2	rozpočet
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Jednací	jednací	k2eAgInPc1d1	jednací
řády	řád	k1gInPc1	řád
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
schváleny	schválit	k5eAaPmNgInP	schválit
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
se	se	k3xPyFc4	se
usnášel	usnášet	k5eAaImAgInS	usnášet
nadpoloviční	nadpoloviční	k2eAgFnSc7d1	nadpoloviční
většinou	většina	k1gFnSc7	většina
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
alespoň	alespoň	k9	alespoň
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vedl	vést	k5eAaImAgMnS	vést
seznam	seznam	k1gInSc4	seznam
advokátů	advokát	k1gMnPc2	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
či	či	k8xC	či
jeho	jeho	k3xOp3gMnSc1	jeho
náměstek	náměstek	k1gMnSc1	náměstek
předsedali	předsedat	k5eAaImAgMnP	předsedat
komoře	komora	k1gFnSc3	komora
i	i	k8xC	i
výboru	výbor	k1gInSc3	výbor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
předsedající	předsedající	k1gMnSc1	předsedající
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rovnosti	rovnost	k1gFnSc2	rovnost
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
rada	rada	k1gFnSc1	rada
měla	mít	k5eAaImAgFnS	mít
disciplinárním	disciplinární	k2eAgInSc7d1	disciplinární
statutem	statut	k1gInSc7	statut
stanoven	stanoven	k2eAgInSc1d1	stanoven
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
včetně	včetně	k7c2	včetně
prezidenta	prezident	k1gMnSc2	prezident
na	na	k7c4	na
7	[number]	k4	7
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
advokátů	advokát	k1gMnPc2	advokát
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
9	[number]	k4	9
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
advokátů	advokát	k1gMnPc2	advokát
bylo	být	k5eAaImAgNnS	být
50	[number]	k4	50
–	–	k?	–
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
15	[number]	k4	15
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
advokátů	advokát	k1gMnPc2	advokát
bylo	být	k5eAaImAgNnS	být
101	[number]	k4	101
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
volili	volit	k5eAaImAgMnP	volit
4	[number]	k4	4
náhradníci	náhradník	k1gMnPc1	náhradník
a	a	k8xC	a
6	[number]	k4	6
náhradníků	náhradník	k1gMnPc2	náhradník
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
Disciplinární	disciplinární	k2eAgFnSc2d1	disciplinární
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Advokátní	advokátní	k2eAgFnSc6d1	advokátní
komoře	komora	k1gFnSc6	komora
k	k	k7c3	k
národnímu	národní	k2eAgInSc3d1	národní
kompromisu	kompromis	k1gInSc3	kompromis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ukončil	ukončit	k5eAaPmAgInS	ukončit
německou	německý	k2eAgFnSc4d1	německá
převahu	převaha	k1gFnSc4	převaha
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
prezidentem	prezident	k1gMnSc7	prezident
komory	komora	k1gFnSc2	komora
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
Němec	Němec	k1gMnSc1	Němec
bude	být	k5eAaImBp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
disciplinární	disciplinární	k2eAgFnSc2d1	disciplinární
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
náměstkové	náměstek	k1gMnPc1	náměstek
byli	být	k5eAaImAgMnP	být
opačné	opačný	k2eAgFnPc4d1	opačná
národnosti	národnost	k1gFnPc4	národnost
než	než	k8xS	než
předsedové	předseda	k1gMnPc1	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byl	být	k5eAaImAgInS	být
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
schválen	schválen	k2eAgInSc4d1	schválen
jednací	jednací	k2eAgInSc4d1	jednací
řád	řád	k1gInSc4	řád
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
paritu	parita	k1gFnSc4	parita
zastoupení	zastoupení	k1gNnSc2	zastoupení
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
advokátů	advokát	k1gMnPc2	advokát
v	v	k7c6	v
orgánech	orgán	k1gInPc6	orgán
pražské	pražský	k2eAgFnSc2d1	Pražská
Advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
byl	být	k5eAaImAgInS	být
národní	národní	k2eAgInSc1d1	národní
kompromis	kompromis	k1gInSc1	kompromis
dosažen	dosažen	k2eAgInSc1d1	dosažen
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
Advokátní	advokátní	k2eAgFnSc6d1	advokátní
komoře	komora	k1gFnSc6	komora
jednacím	jednací	k2eAgInSc7d1	jednací
řádem	řád	k1gInSc7	řád
přijatým	přijatý	k2eAgInSc7d1	přijatý
na	na	k7c6	na
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
střídání	střídání	k1gNnSc4	střídání
německy	německy	k6eAd1	německy
a	a	k8xC	a
česky	česky	k6eAd1	česky
hovořících	hovořící	k2eAgMnPc2d1	hovořící
advokátů	advokát	k1gMnPc2	advokát
v	v	k7c6	v
postu	post	k1gInSc6	post
prezidenta	prezident	k1gMnSc2	prezident
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Předsedy	předseda	k1gMnPc7	předseda
opavské	opavský	k2eAgFnSc2d1	Opavská
Advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
byli	být	k5eAaImAgMnP	být
vždy	vždy	k6eAd1	vždy
německy	německy	k6eAd1	německy
hovořící	hovořící	k2eAgMnPc1d1	hovořící
advokáti	advokát	k1gMnPc1	advokát
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
značné	značný	k2eAgFnSc3d1	značná
početní	početní	k2eAgFnSc3d1	početní
převaze	převaha	k1gFnSc3	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
dochází	docházet	k5eAaImIp3nS	docházet
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
k	k	k7c3	k
uzákonění	uzákonění	k1gNnSc3	uzákonění
povinnosti	povinnost	k1gFnSc2	povinnost
složit	složit	k5eAaPmF	složit
slib	slib	k1gInSc4	slib
věrnosti	věrnost	k1gFnSc3	věrnost
republice	republika	k1gFnSc3	republika
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1922	[number]	k4	1922
je	být	k5eAaImIp3nS	být
advokacie	advokacie	k1gFnSc1	advokacie
konečně	konečně	k6eAd1	konečně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
i	i	k9	i
ženám	žena	k1gFnPc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
advokátkou	advokátka	k1gFnSc7	advokátka
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Hilda	Hilda	k1gFnSc1	Hilda
Wíchová	Wíchová	k1gFnSc1	Wíchová
Mocová	Mocová	k1gFnSc1	Mocová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1923	[number]	k4	1923
je	být	k5eAaImIp3nS	být
vydán	vydat	k5eAaPmNgInS	vydat
i	i	k9	i
nový	nový	k2eAgInSc1d1	nový
advokátní	advokátní	k2eAgInSc1d1	advokátní
tarif	tarif	k1gInSc1	tarif
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
nového	nový	k2eAgInSc2d1	nový
advokátního	advokátní	k2eAgInSc2d1	advokátní
řádu	řád	k1gInSc2	řád
však	však	k9	však
vychází	vycházet	k5eAaImIp3nS	vycházet
naprázdno	naprázdno	k6eAd1	naprázdno
a	a	k8xC	a
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
platí	platit	k5eAaImIp3nS	platit
původní	původní	k2eAgInSc1d1	původní
advokátní	advokátní	k2eAgInSc1d1	advokátní
řád	řád	k1gInSc1	řád
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
státu	stát	k1gInSc2	stát
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
i	i	k9	i
čtyři	čtyři	k4xCgFnPc4	čtyři
bývalé	bývalý	k2eAgFnPc4d1	bývalá
uherské	uherský	k2eAgFnPc4d1	uherská
advokátní	advokátní	k2eAgFnPc4d1	advokátní
komory	komora	k1gFnPc4	komora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
slučují	slučovat	k5eAaImIp3nP	slučovat
v	v	k7c6	v
jednu	jeden	k4xCgFnSc4	jeden
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Turčianském	Turčianský	k2eAgInSc6d1	Turčianský
sv.	sv.	kA	sv.
Martině	Martin	k1gInSc6	Martin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
společná	společný	k2eAgFnSc1d1	společná
reprezentace	reprezentace	k1gFnSc1	reprezentace
československých	československý	k2eAgFnPc2d1	Československá
advokátních	advokátní	k2eAgFnPc2d1	advokátní
komor	komora	k1gFnPc2	komora
Stálá	stálý	k2eAgFnSc1d1	stálá
delegace	delegace	k1gFnSc1	delegace
advokátních	advokátní	k2eAgFnPc2d1	advokátní
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
zákonný	zákonný	k2eAgInSc4d1	zákonný
podklad	podklad	k1gInSc4	podklad
a	a	k8xC	a
veřejnomocenský	veřejnomocenský	k2eAgInSc4d1	veřejnomocenský
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Paritní	paritní	k2eAgInSc1d1	paritní
princip	princip	k1gInSc1	princip
při	při	k7c6	při
obsazování	obsazování	k1gNnSc6	obsazování
funkcionářů	funkcionář	k1gMnPc2	funkcionář
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Advokátní	advokátní	k2eAgFnSc6d1	advokátní
komoře	komora	k1gFnSc6	komora
dosažený	dosažený	k2eAgInSc1d1	dosažený
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgMnS	být
zrušením	zrušení	k1gNnSc7	zrušení
jednacího	jednací	k2eAgInSc2d1	jednací
řádu	řád	k1gInSc2	řád
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
opuštěn	opustit	k5eAaPmNgInS	opustit
a	a	k8xC	a
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
kompromisu	kompromis	k1gInSc3	kompromis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Češi	Čech	k1gMnPc1	Čech
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc4	prezident
(	(	kIx(	(
<g/>
významným	významný	k2eAgMnSc7d1	významný
byl	být	k5eAaImAgMnS	být
JUDr.	JUDr.	kA	JUDr.
Alois	Alois	k1gMnSc1	Alois
Stompfe	Stompf	k1gMnSc5	Stompf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
náměstka	náměstek	k1gMnSc4	náměstek
prezidenta	prezident	k1gMnSc4	prezident
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
16	[number]	k4	16
členů	člen	k1gInPc2	člen
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
Němcům	Němec	k1gMnPc3	Němec
připadne	připadnout	k5eAaPmIp3nS	připadnout
post	post	k1gInSc4	post
jednoho	jeden	k4xCgMnSc2	jeden
náměstka	náměstek	k1gMnSc2	náměstek
prezidenta	prezident	k1gMnSc2	prezident
komory	komora	k1gFnSc2	komora
a	a	k8xC	a
11	[number]	k4	11
členů	člen	k1gInPc2	člen
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
paritního	paritní	k2eAgInSc2d1	paritní
principu	princip	k1gInSc2	princip
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
Advokátní	advokátní	k2eAgFnSc6d1	advokátní
komoře	komora	k1gFnSc6	komora
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
růstu	růst	k1gInSc2	růst
počtu	počet	k1gInSc2	počet
advokátů	advokát	k1gMnPc2	advokát
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
složení	složení	k1gNnSc2	složení
disciplinární	disciplinární	k2eAgFnSc2d1	disciplinární
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
10	[number]	k4	10
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
4	[number]	k4	4
náhradníky	náhradník	k1gMnPc7	náhradník
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
advokátů	advokát	k1gMnPc2	advokát
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
nejvýše	nejvýše	k6eAd1	nejvýše
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
6	[number]	k4	6
náhradníků	náhradník	k1gMnPc2	náhradník
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
advokátů	advokát	k1gMnPc2	advokát
201	[number]	k4	201
až	až	k9	až
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
22	[number]	k4	22
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
6	[number]	k4	6
náhradníků	náhradník	k1gMnPc2	náhradník
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
advokátů	advokát	k1gMnPc2	advokát
1001	[number]	k4	1001
až	až	k9	až
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
32	[number]	k4	32
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
10	[number]	k4	10
náhradníků	náhradník	k1gMnPc2	náhradník
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
advokátů	advokát	k1gMnPc2	advokát
2001	[number]	k4	2001
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
opavské	opavský	k2eAgFnSc2d1	Opavská
Advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgNnPc1d1	zbylé
nezabraná	zabraný	k2eNgNnPc1d1	zabraný
území	území	k1gNnPc1	území
spravuje	spravovat	k5eAaImIp3nS	spravovat
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
omezen	omezit	k5eAaPmNgInS	omezit
volný	volný	k2eAgInSc1d1	volný
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
advokacie	advokacie	k1gFnSc2	advokacie
novou	nový	k2eAgFnSc7d1	nová
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
povolen	povolen	k2eAgInSc1d1	povolen
zápis	zápis	k1gInSc1	zápis
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
advokátů	advokát	k1gMnPc2	advokát
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
tomu	ten	k3xDgNnSc3	ten
nebrání	bránit	k5eNaImIp3nS	bránit
veřejný	veřejný	k2eAgInSc1d1	veřejný
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyloučení	vyloučení	k1gNnSc3	vyloučení
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
advokacie	advokacie	k1gFnSc2	advokacie
a	a	k8xC	a
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
možnosti	možnost	k1gFnSc2	možnost
se	se	k3xPyFc4	se
nechat	nechat	k5eAaPmF	nechat
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
židovský	židovský	k2eAgMnSc1d1	židovský
právní	právní	k2eAgMnSc1d1	právní
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tehdejším	tehdejší	k2eAgInPc3d1	tehdejší
poměrům	poměr	k1gInPc3	poměr
šlo	jít	k5eAaImAgNnS	jít
ale	ale	k9	ale
jen	jen	k9	jen
o	o	k7c4	o
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
advokáti	advokát	k1gMnPc1	advokát
navíc	navíc	k6eAd1	navíc
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
z	z	k7c2	z
advokacie	advokacie	k1gFnSc2	advokacie
vyloučeni	vyloučen	k2eAgMnPc1d1	vyloučen
pro	pro	k7c4	pro
politickou	politický	k2eAgFnSc4d1	politická
nespolehlivost	nespolehlivost	k1gFnSc4	nespolehlivost
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
území	území	k1gNnSc6	území
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
začínají	začínat	k5eAaImIp3nP	začínat
působit	působit	k5eAaImF	působit
němečtí	německý	k2eAgMnPc1d1	německý
advokáti	advokát	k1gMnPc1	advokát
(	(	kIx(	(
<g/>
Rechtsanwalt	Rechtsanwalt	k1gMnSc1	Rechtsanwalt
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedle	vedle	k7c2	vedle
dvou	dva	k4xCgFnPc2	dva
českých	český	k2eAgFnPc2d1	Česká
advokátních	advokátní	k2eAgFnPc2d1	advokátní
komor	komora	k1gFnPc2	komora
souběžně	souběžně	k6eAd1	souběžně
působí	působit	k5eAaImIp3nS	působit
německá	německý	k2eAgFnSc1d1	německá
Říšská	říšský	k2eAgFnSc1d1	říšská
komora	komora	k1gFnSc1	komora
právních	právní	k2eAgMnPc2d1	právní
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
1945	[number]	k4	1945
nebyla	být	k5eNaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
Moravskoslezské	moravskoslezský	k2eAgFnSc6d1	Moravskoslezská
zemi	zem	k1gFnSc6	zem
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
odsunu	odsun	k1gInSc3	odsun
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
německé	německý	k2eAgFnPc4d1	německá
advokátní	advokátní	k2eAgFnPc4d1	advokátní
kanceláře	kancelář	k1gFnPc4	kancelář
uzavřeny	uzavřen	k2eAgFnPc4d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
je	být	k5eAaImIp3nS	být
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
vydán	vydán	k2eAgInSc1d1	vydán
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
advokacii	advokacie	k1gFnSc6	advokacie
<g/>
,	,	kIx,	,
nahrazující	nahrazující	k2eAgInSc1d1	nahrazující
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
advokátní	advokátní	k2eAgInSc4d1	advokátní
řád	řád	k1gInSc4	řád
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1949	[number]	k4	1949
zrušil	zrušit	k5eAaPmAgInS	zrušit
obě	dva	k4xCgFnPc4	dva
samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
advokátní	advokátní	k2eAgFnPc4d1	advokátní
komory	komora	k1gFnPc4	komora
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
ustavil	ustavit	k5eAaPmAgInS	ustavit
krajská	krajský	k2eAgNnPc4d1	krajské
sdružení	sdružení	k1gNnPc4	sdružení
advokátů	advokát	k1gMnPc2	advokát
a	a	k8xC	a
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
sdružení	sdružení	k1gNnSc1	sdružení
advokátů	advokát	k1gMnPc2	advokát
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dohlíželo	dohlížet	k5eAaImAgNnS	dohlížet
na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
krajských	krajský	k2eAgNnPc2d1	krajské
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
byla	být	k5eAaImAgFnS	být
právnickými	právnický	k2eAgFnPc7d1	právnická
osobami	osoba	k1gFnPc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Orgány	orgán	k1gInPc1	orgán
krajských	krajský	k2eAgNnPc2d1	krajské
sdružení	sdružení	k1gNnPc2	sdružení
byla	být	k5eAaImAgFnS	být
členská	členský	k2eAgFnSc1d1	členská
schůze	schůze	k1gFnSc1	schůze
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
ústředního	ústřední	k2eAgNnSc2d1	ústřední
sdružení	sdružení	k1gNnSc2	sdružení
byly	být	k5eAaImAgFnP	být
schůze	schůze	k1gFnPc1	schůze
delegátů	delegát	k1gMnPc2	delegát
krajských	krajský	k2eAgNnPc2d1	krajské
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
,	,	kIx,	,
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Organizační	organizační	k2eAgInPc1d1	organizační
řády	řád	k1gInPc1	řád
Ústředního	ústřední	k2eAgMnSc2d1	ústřední
i	i	k8xC	i
krajských	krajský	k2eAgNnPc2d1	krajské
sdružení	sdružení	k1gNnPc2	sdružení
vydával	vydávat	k5eAaImAgMnS	vydávat
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
nařízením	nařízení	k1gNnSc7	nařízení
<g/>
,	,	kIx,	,
jednací	jednací	k2eAgInPc4d1	jednací
řády	řád	k1gInPc4	řád
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
sdružení	sdružení	k1gNnSc1	sdružení
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
schválením	schválení	k1gNnSc7	schválení
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
politická	politický	k2eAgFnSc1d1	politická
podmínka	podmínka	k1gFnSc1	podmínka
výkonu	výkon	k1gInSc2	výkon
advokacie	advokacie	k1gFnSc2	advokacie
–	–	k?	–
oddanost	oddanost	k1gFnSc4	oddanost
lidově	lidově	k6eAd1	lidově
demokratickému	demokratický	k2eAgNnSc3d1	demokratické
zřízení	zřízení	k1gNnSc3	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Nejden	Nejdna	k1gFnPc2	Nejdna
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
celkově	celkově	k6eAd1	celkově
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
advokátů	advokát	k1gMnPc2	advokát
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ale	ale	k8xC	ale
vznikají	vznikat	k5eAaImIp3nP	vznikat
právní	právní	k2eAgMnSc1d1	právní
poradci	poradce	k1gMnPc1	poradce
při	při	k7c6	při
odborech	odbor	k1gInPc6	odbor
a	a	k8xC	a
státních	státní	k2eAgInPc6d1	státní
podnicích	podnik	k1gInPc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
vydán	vydán	k2eAgInSc1d1	vydán
nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
advokacii	advokacie	k1gFnSc6	advokacie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sdružení	sdružení	k1gNnSc4	sdružení
advokátů	advokát	k1gMnPc2	advokát
ruší	rušit	k5eAaImIp3nS	rušit
a	a	k8xC	a
místo	místo	k7c2	místo
nich	on	k3xPp3gFnPc2	on
zavádí	zavádět	k5eAaImIp3nS	zavádět
tzv.	tzv.	kA	tzv.
advokátní	advokátní	k2eAgFnSc2d1	advokátní
poradny	poradna	k1gFnSc2	poradna
bez	bez	k7c2	bez
právní	právní	k2eAgFnSc2d1	právní
subjektivity	subjektivita	k1gFnSc2	subjektivita
a	a	k8xC	a
jim	on	k3xPp3gFnPc3	on
nadřízené	nadřízený	k2eAgNnSc1d1	nadřízené
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
advokátních	advokátní	k2eAgFnPc2d1	advokátní
poraden	poradna	k1gFnPc2	poradna
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Advokáten	Advokáten	k2eAgMnSc1d1	Advokáten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
tehdy	tehdy	k6eAd1	tehdy
stát	stát	k5eAaPmF	stát
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
státně	státně	k6eAd1	státně
spolehlivým	spolehlivý	k2eAgMnSc7d1	spolehlivý
československým	československý	k2eAgMnSc7d1	československý
občanem	občan	k1gMnSc7	občan
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
právní	právní	k2eAgNnSc1d1	právní
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
letou	letý	k2eAgFnSc4d1	letá
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
odbornou	odborný	k2eAgFnSc4d1	odborná
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
nicméně	nicméně	k8xC	nicméně
může	moct	k5eAaImIp3nS	moct
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
podmínek	podmínka	k1gFnPc2	podmínka
prominout	prominout	k5eAaPmF	prominout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
opětovně	opětovně	k6eAd1	opětovně
zaveden	zavést	k5eAaPmNgInS	zavést
advokátní	advokátní	k2eAgInSc1d1	advokátní
tarif	tarif	k1gInSc1	tarif
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
příjmy	příjem	k1gInPc1	příjem
povinně	povinně	k6eAd1	povinně
odevzdávaly	odevzdávat	k5eAaImAgInP	odevzdávat
na	na	k7c4	na
krajské	krajský	k2eAgNnSc4d1	krajské
sdružení	sdružení	k1gNnSc4	sdružení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	on	k3xPp3gMnPc4	on
přerozdělovalo	přerozdělovat	k5eAaImAgNnS	přerozdělovat
<g/>
.	.	kIx.	.
</s>
<s>
Nestabilní	stabilní	k2eNgInSc1d1	nestabilní
vývoj	vývoj	k1gInSc1	vývoj
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
advokacie	advokacie	k1gFnSc2	advokacie
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
institucionálního	institucionální	k2eAgNnSc2d1	institucionální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
vydán	vydat	k5eAaPmNgInS	vydat
nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
advokacii	advokacie	k1gFnSc6	advokacie
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
opětovně	opětovně	k6eAd1	opětovně
zavádí	zavádět	k5eAaImIp3nS	zavádět
krajská	krajský	k2eAgNnPc4d1	krajské
sdružení	sdružení	k1gNnPc4	sdružení
advokátů	advokát	k1gMnPc2	advokát
a	a	k8xC	a
jim	on	k3xPp3gMnPc3	on
nadřízené	nadřízený	k2eAgNnSc1d1	nadřízené
Ústředí	ústředí	k1gNnSc1	ústředí
československé	československý	k2eAgFnSc2d1	Československá
advokacie	advokacie	k1gFnSc2	advokacie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
pak	pak	k6eAd1	pak
působí	působit	k5eAaImIp3nS	působit
jeho	jeho	k3xOp3gInSc1	jeho
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
advokátních	advokátní	k2eAgFnPc2d1	advokátní
poraden	poradna	k1gFnPc2	poradna
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
nadále	nadále	k6eAd1	nadále
fungují	fungovat	k5eAaImIp3nP	fungovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
okresní	okresní	k2eAgFnSc6d1	okresní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
československé	československý	k2eAgFnSc2d1	Československá
federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
zase	zase	k9	zase
zrušen	zrušen	k2eAgInSc4d1	zrušen
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vydány	vydat	k5eAaPmNgInP	vydat
zákony	zákon	k1gInPc1	zákon
republikové	republikový	k2eAgInPc1d1	republikový
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
úprava	úprava	k1gFnSc1	úprava
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
prakticky	prakticky	k6eAd1	prakticky
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xS	jako
zrušený	zrušený	k2eAgInSc1d1	zrušený
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Logicky	logicky	k6eAd1	logicky
pouze	pouze	k6eAd1	pouze
vzniká	vznikat	k5eAaImIp3nS	vznikat
Ústředí	ústředí	k1gNnSc4	ústředí
české	český	k2eAgFnSc2d1	Česká
advokacie	advokacie	k1gFnSc2	advokacie
a	a	k8xC	a
Ústředí	ústředí	k1gNnSc2	ústředí
slovenské	slovenský	k2eAgFnSc2d1	slovenská
advokacie	advokacie	k1gFnSc2	advokacie
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
povinná	povinný	k2eAgFnSc1d1	povinná
praxe	praxe	k1gFnSc1	praxe
pro	pro	k7c4	pro
adepty	adept	k1gMnPc4	adept
advokacie	advokacie	k1gFnSc2	advokacie
na	na	k7c4	na
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ústředí	ústředí	k1gNnSc2	ústředí
advokacie	advokacie	k1gFnSc2	advokacie
je	být	k5eAaImIp3nS	být
také	také	k9	také
zavedena	zaveden	k2eAgFnSc1d1	zavedena
revizní	revizní	k2eAgFnSc1d1	revizní
a	a	k8xC	a
kárná	kárný	k2eAgFnSc1d1	kárná
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
došlo	dojít	k5eAaPmAgNnS	dojít
díky	díky	k7c3	díky
novému	nový	k2eAgInSc3d1	nový
zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
advokacii	advokacie	k1gFnSc6	advokacie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
již	již	k6eAd1	již
má	mít	k5eAaImIp3nS	mít
působnost	působnost	k1gFnSc4	působnost
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
komora	komora	k1gFnSc1	komora
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
obnovena	obnoven	k2eAgFnSc1d1	obnovena
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
její	její	k3xOp3gFnSc1	její
brněnská	brněnský	k2eAgFnSc1d1	brněnská
pobočka	pobočka	k1gFnSc1	pobočka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
advokát	advokát	k1gMnSc1	advokát
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
.	.	kIx.	.
</s>
<s>
Advokacie	advokacie	k1gFnSc1	advokacie
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
svobodné	svobodný	k2eAgNnSc4d1	svobodné
povolání	povolání	k1gNnSc4	povolání
se	s	k7c7	s
zajištěnou	zajištěný	k2eAgFnSc7d1	zajištěná
samosprávou	samospráva	k1gFnSc7	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
komerční	komerční	k2eAgMnPc1d1	komerční
právníci	právník	k1gMnPc1	právník
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
Komora	komora	k1gFnSc1	komora
komerčních	komerční	k2eAgMnPc2d1	komerční
právníků	právník	k1gMnPc2	právník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
advokacii	advokacie	k1gFnSc6	advokacie
stávají	stávat	k5eAaImIp3nP	stávat
klasičtí	klasický	k2eAgMnPc1d1	klasický
advokáti	advokát	k1gMnPc1	advokát
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnSc1	sdružení
v	v	k7c6	v
ČAK	čak	k0	čak
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Karel	Karel	k1gMnSc1	Karel
Čermák	Čermák	k1gMnSc1	Čermák
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Martin	Martin	k1gMnSc1	Martin
Šolc	Šolc	k1gMnSc1	Šolc
<g />
.	.	kIx.	.
</s>
<s>
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Milan	Milan	k1gMnSc1	Milan
Skalník	Skalník	k1gMnSc1	Skalník
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
prof.	prof.	kA	prof.
JUDr.	JUDr.	kA	JUDr.
Luboš	Luboš	k1gMnSc1	Luboš
Tichý	tichý	k2eAgMnSc1d1	tichý
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Karel	Karel	k1gMnSc1	Karel
Čermák	Čermák	k1gMnSc1	Čermák
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
PhDr.	PhDr.	kA	PhDr.
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Jirousek	Jirousek	k1gMnSc1	Jirousek
od	od	k7c2	od
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Martin	Martin	k1gInSc1	Martin
Vychopeň	Vychopeň	k1gFnSc4	Vychopeň
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
bývá	bývat	k5eAaImIp3nS	bývat
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
laxnost	laxnost	k1gFnSc4	laxnost
v	v	k7c6	v
trestání	trestání	k1gNnSc6	trestání
prohřešků	prohřešek	k1gInPc2	prohřešek
svých	svůj	k3xOyFgMnPc2	svůj
členů-advokátů	členůdvokát	k1gMnPc2	členů-advokát
<g/>
.	.	kIx.	.
</s>
<s>
Nevyloučila	vyloučit	k5eNaPmAgFnS	vyloučit
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
řad	řada	k1gFnPc2	řada
např.	např.	kA	např.
advokáta	advokát	k1gMnSc2	advokát
Kolju	Kolju	k1gMnSc2	Kolju
Kubíčka	Kubíček	k1gMnSc2	Kubíček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
pravomocně	pravomocně	k6eAd1	pravomocně
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
vydírání	vydírání	k1gNnSc4	vydírání
svědka	svědek	k1gMnSc4	svědek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
advokáty	advokát	k1gMnPc4	advokát
Lenku	Lenka	k1gFnSc4	Lenka
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
Hnilicovy	Hnilicův	k2eAgMnPc4d1	Hnilicův
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
šikanovali	šikanovat	k5eAaImAgMnP	šikanovat
nájemníky	nájemník	k1gMnPc4	nájemník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
veřejný	veřejný	k2eAgMnSc1d1	veřejný
ochránce	ochránce	k1gMnSc1	ochránce
práv	práv	k2eAgMnSc1d1	práv
Pavel	Pavel	k1gMnSc1	Pavel
Varvařovský	Varvařovský	k1gMnSc1	Varvařovský
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
advokátní	advokátní	k2eAgFnSc4d1	advokátní
komoru	komora	k1gFnSc4	komora
k	k	k7c3	k
zásahu	zásah	k1gInSc3	zásah
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Hnilicová	Hnilicová	k1gFnSc1	Hnilicová
nebyla	být	k5eNaImAgFnS	být
z	z	k7c2	z
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
ani	ani	k8xC	ani
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
pravomocně	pravomocně	k6eAd1	pravomocně
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
za	za	k7c4	za
vydírání	vydírání	k1gNnSc4	vydírání
<g/>
.	.	kIx.	.
</s>
<s>
Komora	komora	k1gFnSc1	komora
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Hnilicová	Hnilicová	k1gFnSc1	Hnilicová
"	"	kIx"	"
<g/>
dopustila	dopustit	k5eAaPmAgFnS	dopustit
méně	málo	k6eAd2	málo
závažného	závažný	k2eAgInSc2d1	závažný
skutku	skutek	k1gInSc2	skutek
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
vedla	vést	k5eAaImAgFnS	vést
řádný	řádný	k2eAgInSc4d1	řádný
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zástupce	zástupce	k1gMnSc1	zástupce
ombudsmanky	ombudsmanka	k1gFnSc2	ombudsmanka
Stanislav	Stanislav	k1gMnSc1	Stanislav
Křeček	křeček	k1gMnSc1	křeček
považuje	považovat	k5eAaImIp3nS	považovat
takové	takový	k3xDgNnSc4	takový
jednání	jednání	k1gNnSc4	jednání
komory	komora	k1gFnSc2	komora
za	za	k7c4	za
nepochopitelné	pochopitelný	k2eNgNnSc4d1	nepochopitelné
<g/>
,	,	kIx,	,
ilustrující	ilustrující	k2eAgNnSc4d1	ilustrující
"	"	kIx"	"
<g/>
naprostou	naprostý	k2eAgFnSc4d1	naprostá
neschopnost	neschopnost	k1gFnSc4	neschopnost
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
...	...	k?	...
právo	právo	k1gNnSc4	právo
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Koudelka	Koudelka	k1gMnSc1	Koudelka
<g/>
:	:	kIx,	:
Samospráva	samospráva	k1gFnSc1	samospráva
<g/>
,	,	kIx,	,
Linde	Lind	k1gMnSc5	Lind
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
399	[number]	k4	399
s.	s.	k?	s.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7201-665-5	[number]	k4	978-80-7201-665-5
Evžen	Evžen	k1gMnSc1	Evžen
Tarabrin	Tarabrin	k1gInSc1	Tarabrin
<g/>
:	:	kIx,	:
Nástin	nástin	k1gInSc1	nástin
vývoje	vývoj	k1gInSc2	vývoj
advokacie	advokacie	k1gFnSc2	advokacie
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
Stanislav	Stanislav	k1gMnSc1	Stanislav
Balík	Balík	k1gMnSc1	Balík
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
advokacie	advokacie	k1gFnSc2	advokacie
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
Linde	Lind	k1gMnSc5	Lind
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7035-427-8	[number]	k4	978-80-7035-427-8
Advokát	advokát	k1gMnSc1	advokát
Advokátní	advokátní	k2eAgMnSc1d1	advokátní
koncipient	koncipient	k1gMnSc1	koncipient
Bulletin	bulletin	k1gInSc4	bulletin
advokacie	advokacie	k1gFnSc2	advokacie
Notářská	notářský	k2eAgFnSc1d1	notářská
komora	komora	k1gFnSc1	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Notářská	notářský	k2eAgFnSc1d1	notářská
komora	komora	k1gFnSc1	komora
Exekutorská	exekutorský	k2eAgFnSc1d1	Exekutorská
komora	komora	k1gFnSc1	komora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
</s>
