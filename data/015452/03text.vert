<s>
Geesthacht	Geesthacht	k1gMnSc1
</s>
<s>
Geesthacht	Geesthacht	k2eAgInSc1d1
Geesthacht	Geesthacht	k1gInSc1
Radnice	radnice	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
53	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
27	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
SEČ	SEČ	kA
<g/>
/	/	kIx~
<g/>
SELČ	SELČ	kA
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc4
spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k6eAd1
okres	okres	k1gInSc1
</s>
<s>
Vévodství	vévodství	k1gNnSc1
Lauenburské	Lauenburský	k2eAgFnSc2d1
</s>
<s>
Geesthacht	Geesthacht	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
okresu	okres	k1gInSc2
Vévodství	vévodství	k1gNnSc2
Lauenburské	Lauenburský	k2eAgFnSc2d1
</s>
<s>
Geesthacht	Geesthacht	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
33,19	33,19	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
29	#num#	k4
363	#num#	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
884,7	884,7	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Volker	Volker	k1gMnSc1
Manow	Manow	k1gMnSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.geesthacht.de	www.geesthacht.de	k6eAd1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Markt	Markt	k1gInSc1
1521502	#num#	k4
Geesthacht	Geesthachtum	k1gNnPc2
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
04152	#num#	k4
PSČ	PSČ	kA
</s>
<s>
21502	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
RZ	RZ	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Geesthacht	Geesthacht	k1gMnSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
ve	v	k7c6
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Šlesvicku-Holštýnsku	Šlesvicku-Holštýnsko	k1gNnSc6
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Labe	Labe	k1gNnSc2
blízko	blízko	k7c2
Hamburku	Hamburk	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
největším	veliký	k2eAgNnSc7d3
městem	město	k1gNnSc7
v	v	k7c6
okrese	okres	k1gInSc6
Vévodství	vévodství	k1gNnSc1
Lauenburské	Lauenburský	k2eAgFnPc1d1
(	(	kIx(
<g/>
Kreis	Kreis	k1gInSc1
Herzogtum	Herzogtum	k1gNnSc1
Lauenburg	Lauenburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
historickému	historický	k2eAgNnSc3d1
knížectví	knížectví	k1gNnSc3
Sasko-Lauenbursko	Sasko-Lauenbursko	k1gNnSc1
však	však	k9
nepatří	patřit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1937	#num#	k4
patřil	patřit	k5eAaImAgInS
Geesthacht	Geesthacht	k1gInSc1
jako	jako	k8xS,k8xC
exkláva	exkláva	k1gFnSc1
k	k	k7c3
hanzovnímu	hanzovní	k2eAgNnSc3d1
městu	město	k1gNnSc3
Hamburk	Hamburk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
získal	získat	k5eAaPmAgInS
Hamburk	Hamburk	k1gInSc1
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
„	„	k?
<g/>
o	o	k7c6
Velkém	velký	k2eAgInSc6d1
Hamburku	Hamburk	k1gInSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
územních	územní	k2eAgFnPc6d1
úpravách	úprava	k1gFnPc6
<g/>
“	“	k?
některé	některý	k3yIgFnSc2
sousední	sousední	k2eAgFnSc2d1
obce	obec	k1gFnSc2
a	a	k8xC
města	město	k1gNnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
Bergedorf	Bergedorf	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
ztratil	ztratit	k5eAaPmAgMnS
některé	některý	k3yIgFnPc4
exklávy	exkláva	k1gFnPc4
jako	jako	k8xS,k8xC
např.	např.	kA
Geesthacht	Geesthacht	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
připojeno	připojit	k5eAaPmNgNnS
k	k	k7c3
tehdy	tehdy	k6eAd1
pruskému	pruský	k2eAgNnSc3d1
Šlesvicku-Holštýnsku	Šlesvicku-Holštýnsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
na	na	k7c6
okraji	okraj	k1gInSc6
Hamburku	Hamburk	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
dálnicí	dálnice	k1gFnSc7
A	a	k9
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Hachede	Hached	k1gMnSc5
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1216	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
změnilo	změnit	k5eAaPmAgNnS
Labe	Labe	k1gNnSc4
svůj	svůj	k3xOyFgInSc4
tok	tok	k1gInSc1
a	a	k8xC
tím	ten	k3xDgNnSc7
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
vesnice	vesnice	k1gFnSc1
Hachede	Hached	k1gMnSc5
spolu	spolu	k6eAd1
s	s	k7c7
jejím	její	k3xOp3gInSc7
kostelem	kostel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
zaniklé	zaniklý	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
však	však	k9
žije	žít	k5eAaImIp3nS
dál	daleko	k6eAd2
v	v	k7c6
názvech	název	k1gInPc6
města	město	k1gNnSc2
Geesthacht	Geesthachta	k1gFnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Geest-Hachede	Geest-Hached	k1gMnSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
obce	obec	k1gFnPc1
Marschacht	Marschachta	k1gFnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Marsch-Hachede	Marsch-Hached	k1gMnSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
v	v	k7c6
Dolním	dolní	k2eAgNnSc6d1
Sasku	Sasko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
leží	ležet	k5eAaImIp3nS
naproti	naproti	k7c3
Geesthachtu	Geesthacht	k1gInSc3
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Labe	Labe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolnoněmecké	dolnoněmecký	k2eAgFnPc4d1
slovo	slovo	k1gNnSc1
Geest	Geest	k1gInSc1
označuje	označovat	k5eAaImIp3nS
výše	vysoce	k6eAd2
položený	položený	k2eAgInSc4d1
vlnitý	vlnitý	k2eAgInSc4d1
nebo	nebo	k8xC
kopcovitý	kopcovitý	k2eAgInSc4d1
kraj	kraj	k1gInSc4
hlavně	hlavně	k9
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Nizozemí	Nizozemí	k1gNnSc6
a	a	k8xC
Dánsku	Dánsko	k1gNnSc6
s	s	k7c7
neúrodnou	úrodný	k2eNgFnSc7d1
a	a	k8xC
většinou	většina	k1gFnSc7
písčitou	písčitý	k2eAgFnSc7d1
půdou	půda	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
vznikl	vzniknout	k5eAaPmAgInS
hlavně	hlavně	k9
nánosem	nános	k1gInSc7
z	z	k7c2
doby	doba	k1gFnSc2
ledové	ledový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
označuje	označovat	k5eAaImIp3nS
slovo	slovo	k1gNnSc1
Marsch	Marscha	k1gFnPc2
úrodný	úrodný	k2eAgInSc4d1
náplav	náplav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Geesthacht	Geesthacht	k1gInSc1
je	být	k5eAaImIp3nS
nové	nový	k2eAgNnSc4d1
Hachede	Hached	k1gMnSc5
na	na	k7c6
vyšším	vysoký	k2eAgInSc6d2
břehu	břeh	k1gInSc6
Labe	Labe	k1gNnSc2
<g/>
,	,	kIx,
takzvaném	takzvaný	k2eAgNnSc6d1
Geesthang	Geesthang	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1420	#num#	k4
patří	patřit	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
vesnice	vesnice	k1gFnSc1
hanzovním	hanzovní	k2eAgInSc7d1
městům	město	k1gNnPc3
Hamburk	Hamburk	k1gInSc1
a	a	k8xC
Lübeck	Lübeck	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společná	společný	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
obou	dva	k4xCgNnPc2
hanzovních	hanzovní	k2eAgNnPc2d1
měst	město	k1gNnPc2
trvala	trvat	k5eAaImAgFnS
do	do	k7c2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
patřil	patřit	k5eAaImAgInS
Geesthacht	Geesthacht	k1gInSc1
jako	jako	k8xC,k8xS
exkláva	exkláva	k1gFnSc1
k	k	k7c3
hamburskému	hamburský	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
má	mít	k5eAaImIp3nS
Geesthacht	Geesthacht	k1gInSc4
městská	městský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
patřil	patřit	k5eAaImAgMnS
však	však	k9
ještě	ještě	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
k	k	k7c3
hamburskému	hamburský	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Politika	politika	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
radě	rada	k1gFnSc6
města	město	k1gNnSc2
(	(	kIx(
<g/>
Ratsversammlung	Ratsversammlung	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
po	po	k7c6
volbách	volba	k1gFnPc6
z	z	k7c2
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
zastoupeno	zastoupit	k5eAaPmNgNnS
pět	pět	k4xCc4
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Křesťanští	křesťanský	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
(	(	kIx(
<g/>
CDU	CDU	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
12	#num#	k4
poslanců	poslanec	k1gMnPc2
(	(	kIx(
<g/>
35,8	35,8	k4
%	%	kIx~
voličů	volič	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
(	(	kIx(
<g/>
SPD	SPD	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
10	#num#	k4
poslanců	poslanec	k1gMnPc2
(	(	kIx(
<g/>
30,7	30,7	k4
%	%	kIx~
voličů	volič	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Zelení	Zelený	k1gMnPc1
(	(	kIx(
<g/>
GRÜNE	GRÜNE	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
5	#num#	k4
poslanců	poslanec	k1gMnPc2
(	(	kIx(
<g/>
15,2	15,2	k4
%	%	kIx~
voličů	volič	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Liberálové	liberál	k1gMnPc1
(	(	kIx(
<g/>
FDP	FDP	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
4	#num#	k4
poslanci	poslanec	k1gMnPc1
(	(	kIx(
<g/>
12,1	12,1	k4
%	%	kIx~
voličů	volič	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Levice	levice	k1gFnSc1
(	(	kIx(
<g/>
LINKE	LINKE	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2	#num#	k4
poslanci	poslanec	k1gMnPc1
(	(	kIx(
<g/>
6,2	6,2	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Město	město	k1gNnSc1
Geesthacht	Geesthachta	k1gFnPc2
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
bez	bez	k7c2
starosty	starosta	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
náhle	náhle	k6eAd1
dosavadní	dosavadní	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Ingo	Ingo	k1gMnSc1
Fokken	Fokken	k2eAgMnSc1d1
(	(	kIx(
<g/>
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fokken	Fokken	k1gInSc1
byl	být	k5eAaImAgInS
starostou	starosta	k1gMnSc7
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
byl	být	k5eAaImAgMnS
znovu	znovu	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datum	datum	k1gNnSc1
voleb	volba	k1gFnPc2
na	na	k7c4
nového	nový	k2eAgMnSc4d1
starostu	starosta	k1gMnSc4
zatím	zatím	k6eAd1
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
severního	severní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
patří	patřit	k5eAaImIp3nS
převážně	převážně	k6eAd1
k	k	k7c3
evangelické	evangelický	k2eAgFnSc3d1
luterské	luterský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
církev	církev	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
Geesthachtu	Geesthacht	k1gInSc6
dva	dva	k4xCgInPc4
kostely	kostel	k1gInPc4
<g/>
:	:	kIx,
St.	st.	kA
Salvatoris	Salvatoris	k1gFnSc1
(	(	kIx(
<g/>
sv.	sv.	kA
Vykupitele	vykupitel	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
St.	st.	kA
Petri	Petri	k1gNnSc4
(	(	kIx(
<g/>
sv.	sv.	kA
Petra	Petr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
existuje	existovat	k5eAaImIp3nS
v	v	k7c6
obci	obec	k1gFnSc6
katolický	katolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Sv.	sv.	kA
Barbory	Barbora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Geesthacht	Geesthachta	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4019635-5	4019635-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82036441	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
124353823	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82036441	#num#	k4
</s>
