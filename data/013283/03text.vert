<p>
<s>
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
od	od	k7c2	od
spisovatele	spisovatel	k1gMnSc2	spisovatel
Ladislava	Ladislav	k1gMnSc2	Ladislav
Fukse	Fuks	k1gMnSc2	Fuks
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
druhé	druhý	k4xOgFnSc6	druhý
vlně	vlna	k1gFnSc3	vlna
literatury	literatura	k1gFnSc2	literatura
ovlivněné	ovlivněný	k2eAgFnSc2d1	ovlivněná
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
první	první	k4xOgFnSc2	první
etapy	etapa	k1gFnSc2	etapa
autorovy	autorův	k2eAgFnSc2d1	autorova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
také	také	k9	také
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
Jurajem	Juraj	k1gMnSc7	Juraj
Herzem	Herz	k1gMnSc7	Herz
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc2	první
etapy	etapa	k1gFnSc2	etapa
Fuksovy	Fuksův	k2eAgFnSc2d1	Fuksova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
romány	román	k1gInPc4	román
Pan	Pan	k1gMnSc1	Pan
Theodor	Theodor	k1gMnSc1	Theodor
Mundstock	Mundstock	k1gMnSc1	Mundstock
<g/>
,	,	kIx,	,
Variace	variace	k1gFnSc1	variace
pro	pro	k7c4	pro
temnou	temný	k2eAgFnSc4d1	temná
strunu	struna	k1gFnSc4	struna
či	či	k8xC	či
Myši	myš	k1gFnPc4	myš
Natalie	Natalie	k1gFnSc2	Natalie
Mooshabrové	Mooshabrový	k2eAgFnPc1d1	Mooshabrový
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
i	i	k9	i
sbírka	sbírka	k1gFnSc1	sbírka
šesti	šest	k4xCc2	šest
povídek	povídka	k1gFnPc2	povídka
Mí	můj	k3xOp1gMnPc1	můj
černovlasí	černovlasý	k2eAgMnPc1d1	černovlasý
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
velmi	velmi	k6eAd1	velmi
blízce	blízce	k6eAd1	blízce
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
témat	téma	k1gNnPc2	téma
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
problematiky	problematika	k1gFnSc2	problematika
židovství	židovství	k1gNnSc2	židovství
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Fuks	Fuks	k1gMnSc1	Fuks
knihu	kniha	k1gFnSc4	kniha
uvedl	uvést	k5eAaPmAgMnS	uvést
citátem	citát	k1gInSc7	citát
od	od	k7c2	od
italského	italský	k2eAgMnSc2d1	italský
básníka	básník	k1gMnSc2	básník
Giovanni	Giovaneň	k1gFnSc6	Giovaneň
Papiniho	Papini	k1gMnSc4	Papini
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Největší	veliký	k2eAgFnSc7d3	veliký
lstí	lest	k1gFnSc7	lest
ďábla	ďábel	k1gMnSc2	ďábel
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Autor	autor	k1gMnSc1	autor
staví	stavit	k5eAaBmIp3nS	stavit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
krematoria	krematorium	k1gNnSc2	krematorium
a	a	k8xC	a
vojáka	voják	k1gMnSc4	voják
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Karla	Karel	k1gMnSc2	Karel
Kopfrkingla	Kopfrkingl	k1gMnSc2	Kopfrkingl
před	před	k7c4	před
hrůzu	hrůza	k1gFnSc4	hrůza
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
rozklad	rozklad	k1gInSc4	rozklad
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	dít	k5eAaImRp2nS	dít
knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Kopfrkingl	Kopfrkingl	k1gMnSc1	Kopfrkingl
<g/>
,	,	kIx,	,
spořádaný	spořádaný	k2eAgMnSc1d1	spořádaný
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
pražského	pražský	k2eAgNnSc2d1	Pražské
krematoria	krematorium	k1gNnSc2	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
jemného	jemný	k2eAgMnSc4d1	jemný
<g/>
,	,	kIx,	,
milého	milý	k2eAgMnSc4d1	milý
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
abstinenta	abstinent	k1gMnSc4	abstinent
a	a	k8xC	a
nekuřáka	nekuřák	k1gMnSc4	nekuřák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
skutečnosti	skutečnost	k1gFnPc4	skutečnost
neustále	neustále	k6eAd1	neustále
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
knihy	kniha	k1gFnSc2	kniha
vidíme	vidět	k5eAaImIp1nP	vidět
nacismem	nacismus	k1gInSc7	nacismus
rozložený	rozložený	k2eAgInSc4d1	rozložený
charakter	charakter	k1gInSc4	charakter
vraždící	vraždící	k2eAgInSc4d1	vraždící
vlastní	vlastní	k2eAgFnSc4d1	vlastní
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Karel	Karel	k1gMnSc1	Karel
nám	my	k3xPp1nPc3	my
připomene	připomenout	k5eAaPmIp3nS	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
hodný	hodný	k2eAgMnSc1d1	hodný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
abstinent	abstinent	k1gMnSc1	abstinent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
zřízence	zřízenec	k1gMnSc2	zřízenec
krematoria	krematorium	k1gNnSc2	krematorium
bere	brát	k5eAaImIp3nS	brát
jako	jako	k9	jako
životní	životní	k2eAgNnSc4d1	životní
poslání	poslání	k1gNnSc4	poslání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kremací	kremace	k1gFnSc7	kremace
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
duším	dušit	k5eAaImIp1nS	dušit
nalézt	nalézt	k5eAaPmF	nalézt
nové	nový	k2eAgNnSc4d1	nové
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
názory	názor	k1gInPc4	názor
čerpá	čerpat	k5eAaImIp3nS	čerpat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
žluté	žlutý	k2eAgFnSc2d1	žlutá
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
krásná	krásný	k2eAgFnSc1d1	krásná
a	a	k8xC	a
vznešená	vznešený	k2eAgFnSc1d1	vznešená
jistota	jistota	k1gFnSc1	jistota
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
se	se	k3xPyFc4	se
také	také	k9	také
za	za	k7c4	za
romantika	romantik	k1gMnSc4	romantik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
i	i	k9	i
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
rodině	rodina	k1gFnSc6	rodina
–	–	k?	–
svojí	svůj	k3xOyFgFnSc3	svůj
manželce	manželka	k1gFnSc3	manželka
Marii	Maria	k1gFnSc3	Maria
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Lakmé	Lakmý	k2eAgNnSc1d1	Lakmé
a	a	k8xC	a
přeje	přát	k5eAaImIp3nS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ona	onen	k3xDgFnSc1	onen
jemu	on	k3xPp3gMnSc3	on
říkala	říkat	k5eAaImAgFnS	říkat
Romane	Roman	k1gMnSc5	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Kočku	kočka	k1gFnSc4	kočka
adresuje	adresovat	k5eAaBmIp3nS	adresovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nebeská	nebeský	k2eAgFnSc1d1	nebeská
<g/>
,	,	kIx,	,
čarokrásná	čarokrásný	k2eAgFnSc1d1	čarokrásná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
poloviční	poloviční	k2eAgFnSc1d1	poloviční
Židovka	Židovka	k1gFnSc1	Židovka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc4d2	starší
dceru	dcera	k1gFnSc4	dcera
Zinu	Zina	k1gFnSc4	Zina
a	a	k8xC	a
mladšího	mladý	k2eAgMnSc4d2	mladší
syna	syn	k1gMnSc4	syn
Milivoje	Milivoj	k1gInSc2	Milivoj
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kapku	kapka	k1gFnSc4	kapka
německé	německý	k2eAgFnSc2d1	německá
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
ohleduplný	ohleduplný	k2eAgMnSc1d1	ohleduplný
<g/>
,	,	kIx,	,
jemný	jemný	k2eAgMnSc1d1	jemný
člověk	člověk	k1gMnSc1	člověk
opovrhující	opovrhující	k2eAgFnSc4d1	opovrhující
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Židy	Žid	k1gMnPc7	Žid
–	–	k?	–
např.	např.	kA	např.
doktor	doktor	k1gMnSc1	doktor
Bettelheim	Bettelheim	k1gMnSc1	Bettelheim
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Zakládá	zakládat	k5eAaImIp3nS	zakládat
si	se	k3xPyFc3	se
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
drobnostech	drobnost	k1gFnPc6	drobnost
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
je	být	k5eAaImIp3nS	být
jeho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
jízdní	jízdní	k2eAgInSc4d1	jízdní
řád	řád	k1gInSc4	řád
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
–	–	k?	–
kremační	kremační	k2eAgInSc4d1	kremační
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
vážnou	vážný	k2eAgFnSc4d1	vážná
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
také	také	k9	také
polemizuje	polemizovat	k5eAaImIp3nS	polemizovat
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
celou	celý	k2eAgFnSc4d1	celá
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
poklidného	poklidný	k2eAgInSc2d1	poklidný
života	život	k1gInSc2	život
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
a	a	k8xC	a
především	především	k6eAd1	především
Karlův	Karlův	k2eAgMnSc1d1	Karlův
přítel	přítel	k1gMnSc1	přítel
Willi	Wille	k1gFnSc4	Wille
Reinke	Reink	k1gFnSc2	Reink
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
SdP	SdP	k1gMnSc1	SdP
<g/>
.	.	kIx.	.
</s>
<s>
Reinke	Reinke	k1gFnSc1	Reinke
začne	začít	k5eAaPmIp3nS	začít
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
Karlových	Karlův	k2eAgInPc2d1	Karlův
politických	politický	k2eAgInPc2d1	politický
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
otevřeně	otevřeně	k6eAd1	otevřeně
podporuje	podporovat	k5eAaImIp3nS	podporovat
němectví	němectví	k1gNnSc4	němectví
<g/>
,	,	kIx,	,
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
Karla	Karel	k1gMnSc4	Karel
do	do	k7c2	do
SdP	SdP	k1gFnSc2	SdP
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
členové	člen	k1gMnPc1	člen
jej	on	k3xPp3gMnSc4	on
poté	poté	k6eAd1	poté
využívají	využívat	k5eAaPmIp3nP	využívat
ke	k	k7c3	k
špionáži	špionáž	k1gFnSc3	špionáž
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
pořád	pořád	k6eAd1	pořád
snaží	snažit	k5eAaImIp3nS	snažit
jednat	jednat	k5eAaImF	jednat
"	"	kIx"	"
<g/>
správně	správně	k6eAd1	správně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
začne	začít	k5eAaPmIp3nS	začít
postupně	postupně	k6eAd1	postupně
věřit	věřit	k5eAaImF	věřit
myšlence	myšlenka	k1gFnSc3	myšlenka
Vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zavede	zavést	k5eAaPmIp3nS	zavést
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
spasí	spasit	k5eAaPmIp3nS	spasit
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
činy	čin	k1gInPc7	čin
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
Karlova	Karlův	k2eAgFnSc1d1	Karlova
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
ředitelem	ředitel	k1gMnSc7	ředitel
pražského	pražský	k2eAgNnSc2d1	Pražské
krematoria	krematorium	k1gNnSc2	krematorium
a	a	k8xC	a
zastáncem	zastánce	k1gMnSc7	zastánce
proněmeckých	proněmecký	k2eAgFnPc2d1	proněmecká
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
společnosti	společnost	k1gFnSc6	společnost
nebude	být	k5eNaImBp3nS	být
místo	místo	k1gNnSc1	místo
–	–	k?	–
a	a	k8xC	a
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
koluje	kolovat	k5eAaImIp3nS	kolovat
židovská	židovský	k2eAgFnSc1d1	židovská
krev	krev	k1gFnSc1	krev
–	–	k?	–
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	s	k7c7	s
"	"	kIx"	"
<g/>
ušetřit	ušetřit	k5eAaPmF	ušetřit
rodinu	rodina	k1gFnSc4	rodina
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oběsí	oběsit	k5eAaPmIp3nS	oběsit
Lakmé	Lakmý	k2eAgNnSc1d1	Lakmé
v	v	k7c6	v
koupelně	koupelna	k1gFnSc6	koupelna
<g/>
,	,	kIx,	,
Miliho	Mili	k1gMnSc2	Mili
zabije	zabít	k5eAaPmIp3nS	zabít
v	v	k7c6	v
krematoriu	krematorium	k1gNnSc6	krematorium
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
Zina	Zina	k1gFnSc1	Zina
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kopfrkingl	Kopfrkingl	k1gMnSc1	Kopfrkingl
pak	pak	k6eAd1	pak
sedí	sedit	k5eAaImIp3nS	sedit
doma	doma	k6eAd1	doma
a	a	k8xC	a
přemítá	přemítat	k5eAaImIp3nS	přemítat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
spasí	spasit	k5eAaPmIp3nS	spasit
všechny	všechen	k3xTgInPc4	všechen
a	a	k8xC	a
nastolí	nastolit	k5eAaPmIp3nS	nastolit
nový	nový	k2eAgInSc4d1	nový
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
má	můj	k3xOp1gFnSc1	můj
strana	strana	k1gFnSc1	strana
novou	nový	k2eAgFnSc4d1	nová
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
budou	být	k5eAaImBp3nP	být
pece	pec	k1gFnPc1	pec
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgFnPc1d2	lepší
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
jich	on	k3xPp3gFnPc2	on
mohl	moct	k5eAaImAgMnS	moct
spasit	spasit	k5eAaPmF	spasit
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hned	hned	k6eAd1	hned
vzápětí	vzápětí	k6eAd1	vzápětí
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
vidění	vidění	k1gNnSc4	vidění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
přišel	přijít	k5eAaPmAgInS	přijít
mnich	mnich	k1gInSc4	mnich
až	až	k9	až
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
novým	nový	k2eAgMnSc7d1	nový
dalajlámou	dalajláma	k1gMnSc7	dalajláma
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jej	on	k3xPp3gInSc4	on
odvezou	odvézt	k5eAaPmIp3nP	odvézt
bílým	bílý	k2eAgNnSc7d1	bílé
autem	auto	k1gNnSc7	auto
pro	pro	k7c4	pro
anděly	anděl	k1gMnPc4	anděl
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
příběh	příběh	k1gInSc1	příběh
končí	končit	k5eAaImIp3nS	končit
kratičkým	kratičký	k2eAgInSc7d1	kratičký
epilogem	epilog	k1gInSc7	epilog
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
z	z	k7c2	z
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
lapen	lapit	k5eAaPmNgInS	lapit
v	v	k7c6	v
iluzi	iluze	k1gFnSc6	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgMnPc4	všechen
spasil	spasit	k5eAaPmAgMnS	spasit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Motivy	motiv	k1gInPc1	motiv
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
napsal	napsat	k5eAaPmAgMnS	napsat
Fuks	Fuks	k1gMnSc1	Fuks
perfektně	perfektně	k6eAd1	perfektně
propracované	propracovaný	k2eAgNnSc4d1	propracované
psychologické	psychologický	k2eAgNnSc4d1	psychologické
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
nejen	nejen	k6eAd1	nejen
jeho	jeho	k3xOp3gInSc1	jeho
osobní	osobní	k2eAgInSc1d1	osobní
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
židovské	židovský	k2eAgFnSc3d1	židovská
komunitě	komunita	k1gFnSc3	komunita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
psychické	psychický	k2eAgInPc4d1	psychický
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dělají	dělat	k5eAaImIp3nP	dělat
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k9	ještě
o	o	k7c4	o
stupeň	stupeň	k1gInSc4	stupeň
děsivější	děsivý	k2eAgFnSc1d2	děsivější
a	a	k8xC	a
zajímavější	zajímavý	k2eAgFnSc1d2	zajímavější
<g/>
.	.	kIx.	.
</s>
<s>
Vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
bezmoc	bezmoc	k1gFnSc4	bezmoc
a	a	k8xC	a
dusivou	dusivý	k2eAgFnSc4d1	dusivá
atmosféru	atmosféra	k1gFnSc4	atmosféra
předválečných	předválečný	k2eAgNnPc2d1	předválečné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
postavě	postava	k1gFnSc6	postava
Karla	Karel	k1gMnSc2	Karel
Kopfrkingla	Kopfrkingl	k1gMnSc2	Kopfrkingl
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pramenící	pramenící	k2eAgNnSc1d1	pramenící
z	z	k7c2	z
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
snadné	snadný	k2eAgNnSc1d1	snadné
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
relativně	relativně	k6eAd1	relativně
neškodného	škodný	k2eNgMnSc2d1	neškodný
člověka	člověk	k1gMnSc2	člověk
pomocí	pomocí	k7c2	pomocí
propagandy	propaganda	k1gFnSc2	propaganda
a	a	k8xC	a
chytrých	chytrý	k2eAgFnPc2d1	chytrá
frází	fráze	k1gFnPc2	fráze
vychovat	vychovat	k5eAaPmF	vychovat
vraždící	vraždící	k2eAgNnPc4d1	vraždící
monstrum	monstrum	k1gNnSc4	monstrum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vyššího	vysoký	k2eAgNnSc2d2	vyšší
dobra	dobro	k1gNnSc2	dobro
schopno	schopen	k2eAgNnSc1d1	schopno
"	"	kIx"	"
<g/>
spasit	spasit	k5eAaPmF	spasit
<g/>
"	"	kIx"	"
vlastní	vlastní	k2eAgFnSc4d1	vlastní
rodinu	rodina	k1gFnSc4	rodina
–	–	k?	–
a	a	k8xC	a
klidně	klidně	k6eAd1	klidně
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
fuksovská	fuksovský	k2eAgFnSc1d1	fuksovský
grotesknost	grotesknost	k1gFnSc1	grotesknost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přechází	přecházet	k5eAaImIp3nS	přecházet
až	až	k9	až
ve	v	k7c4	v
formu	forma	k1gFnSc4	forma
groteskního	groteskní	k2eAgInSc2d1	groteskní
hororu	horor	k1gInSc2	horor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Spalovači	spalovač	k1gMnSc6	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
dovedeno	dovést	k5eAaPmNgNnS	dovést
až	až	k9	až
do	do	k7c2	do
krajnosti	krajnost	k1gFnSc2	krajnost
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
tak	tak	k6eAd1	tak
grotesku	groteska	k1gFnSc4	groteska
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
šílenství	šílenství	k1gNnSc4	šílenství
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mnohdy	mnohdy	k6eAd1	mnohdy
překonalo	překonat	k5eAaPmAgNnS	překonat
hrůzy	hrůza	k1gFnPc4	hrůza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
popisovány	popisován	k2eAgInPc1d1	popisován
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
realitou	realita	k1gFnSc7	realita
a	a	k8xC	a
hororem	horor	k1gInSc7	horor
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
navození	navození	k1gNnSc3	navození
hororové	hororový	k2eAgFnSc2d1	hororová
atmosféry	atmosféra	k1gFnSc2	atmosféra
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
další	další	k2eAgInPc4d1	další
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
celou	celý	k2eAgFnSc7d1	celá
knihou	kniha	k1gFnSc7	kniha
prolínají	prolínat	k5eAaImIp3nP	prolínat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Kopfrkinglovo	Kopfrkinglův	k2eAgNnSc1d1	Kopfrkinglův
zalíbení	zalíbení	k1gNnSc1	zalíbení
ve	v	k7c6	v
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
projevuje	projevovat	k5eAaImIp3nS	projevovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
objevují	objevovat	k5eAaImIp3nP	objevovat
(	(	kIx(	(
<g/>
manželský	manželský	k2eAgInSc1d1	manželský
pár	pár	k1gInSc1	pár
<g/>
,	,	kIx,	,
dívka	dívka	k1gFnSc1	dívka
v	v	k7c6	v
černých	černý	k2eAgInPc6d1	černý
šatech	šat	k1gInPc6	šat
<g/>
,	,	kIx,	,
starší	starší	k1gMnPc4	starší
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
brýlemi	brýle	k1gFnPc7	brýle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
striktně	striktně	k6eAd1	striktně
spisovný	spisovný	k2eAgMnSc1d1	spisovný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
promluvách	promluva	k1gFnPc6	promluva
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
i	i	k8xC	i
jazyk	jazyk	k1gMnSc1	jazyk
svou	svůj	k3xOyFgFnSc7	svůj
groteskností	grotesknost	k1gFnSc7	grotesknost
působí	působit	k5eAaImIp3nS	působit
uměle	uměle	k6eAd1	uměle
a	a	k8xC	a
nepřirozeně	přirozeně	k6eNd1	přirozeně
<g/>
.	.	kIx.	.
</s>
<s>
Připomínají	připomínat	k5eAaImIp3nP	připomínat
tak	tak	k9	tak
voskové	voskový	k2eAgFnPc1d1	vosková
figuríny	figurína	k1gFnPc1	figurína
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
navozují	navozovat	k5eAaImIp3nP	navozovat
atmosféru	atmosféra	k1gFnSc4	atmosféra
hororu	horor	k1gInSc2	horor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
časté	častý	k2eAgNnSc1d1	časté
opakování	opakování	k1gNnSc1	opakování
variací	variace	k1gFnPc2	variace
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
té	ten	k3xDgFnSc2	ten
samé	samý	k3xTgFnSc2	samý
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opakování	opakování	k1gNnSc1	opakování
navozuje	navozovat	k5eAaImIp3nS	navozovat
dojem	dojem	k1gInSc4	dojem
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
počtem	počet	k1gInSc7	počet
herců	herec	k1gMnPc2	herec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
rolích	role	k1gFnPc6	role
na	na	k7c4	na
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgFnSc6d1	stejná
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobná	podobný	k2eAgNnPc1d1	podobné
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Mefisto	Mefisto	k1gMnSc1	Mefisto
–	–	k?	–
román	román	k1gInSc1	román
Klause	Klaus	k1gMnSc2	Klaus
Manna	Mann	k1gMnSc2	Mann
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
může	moct	k5eAaImIp3nS	moct
čtenář	čtenář	k1gMnSc1	čtenář
sledovat	sledovat	k5eAaImF	sledovat
také	také	k9	také
přerod	přerod	k1gInSc4	přerod
složité	složitý	k2eAgFnSc2d1	složitá
osobnosti	osobnost	k1gFnSc2	osobnost
komunisty	komunista	k1gMnSc2	komunista
a	a	k8xC	a
talentovaného	talentovaný	k2eAgMnSc2d1	talentovaný
provinčního	provinční	k2eAgMnSc2d1	provinční
herce	herec	k1gMnSc2	herec
v	v	k7c4	v
nacistu	nacista	k1gMnSc4	nacista
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
(	(	kIx(	(
<g/>
Slovník	slovník	k1gInSc1	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
–	–	k?	–
dílo	dílo	k1gNnSc1	dílo
online	onlinout	k5eAaPmIp3nS	onlinout
k	k	k7c3	k
přečtení	přečtení	k1gNnSc3	přečtení
</s>
</p>
