<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
formou	forma	k1gFnSc7	forma
kvartetu	kvartet	k1gInSc2	kvartet
je	být	k5eAaImIp3nS	být
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
skladba	skladba	k1gFnSc1	skladba
napsaná	napsaný	k2eAgFnSc1d1	napsaná
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
