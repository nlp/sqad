<p>
<s>
Krabi	krab	k1gMnPc1	krab
(	(	kIx(	(
<g/>
Brachyura	Brachyura	k1gFnSc1	Brachyura
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
polyfyletická	polyfyletický	k2eAgFnSc1d1	polyfyletická
skupina	skupina	k1gFnSc1	skupina
vodních	vodní	k2eAgMnPc2d1	vodní
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
taxonomických	taxonomický	k2eAgInPc6d1	taxonomický
systémech	systém	k1gInPc6	systém
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jako	jako	k8xC	jako
infrařád	infrařáda	k1gFnPc2	infrařáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krabi	krab	k1gMnPc1	krab
mají	mít	k5eAaImIp3nP	mít
5	[number]	k4	5
párů	pár	k1gInPc2	pár
kráčivých	kráčivý	k2eAgFnPc2d1	kráčivá
nohou	noha	k1gFnPc2	noha
(	(	kIx(	(
<g/>
pereiopody	pereiopoda	k1gFnPc1	pereiopoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
pár	pár	k1gInSc1	pár
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
klepety	klepeto	k1gNnPc7	klepeto
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
široce	široko	k6eAd1	široko
oválný	oválný	k2eAgInSc4d1	oválný
plochý	plochý	k2eAgInSc4d1	plochý
krunýř	krunýř	k1gInSc4	krunýř
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
mohutná	mohutný	k2eAgNnPc4d1	mohutné
klepeta	klepeto	k1gNnPc4	klepeto
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
klepet	klepeto	k1gNnPc2	klepeto
bývá	bývat	k5eAaImIp3nS	bývat
dominantní	dominantní	k2eAgNnSc1d1	dominantní
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
orientované	orientovaný	k2eAgFnPc1d1	orientovaná
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
redukovaný	redukovaný	k2eAgInSc4d1	redukovaný
zadeček	zadeček	k1gInSc4	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
přimknut	přimknout	k5eAaPmNgInS	přimknout
na	na	k7c4	na
spodní	spodní	k2eAgFnSc4d1	spodní
plochu	plocha	k1gFnSc4	plocha
hlavohrudi	hlavohruď	k1gFnSc2	hlavohruď
<g/>
,	,	kIx,	,
na	na	k7c4	na
plastron	plastron	k1gInSc4	plastron
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
zadeček	zadeček	k1gInSc4	zadeček
subtilní	subtilní	k2eAgInSc4d1	subtilní
a	a	k8xC	a
špičatý	špičatý	k2eAgInSc4d1	špičatý
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
široký	široký	k2eAgInSc4d1	široký
a	a	k8xC	a
tupě	tupě	k6eAd1	tupě
zakončený	zakončený	k2eAgInSc1d1	zakončený
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgInSc1d3	veliký
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
krab	krab	k1gInSc1	krab
je	být	k5eAaImIp3nS	být
velekrab	velekrab	k1gInSc1	velekrab
japonský	japonský	k2eAgInSc1d1	japonský
(	(	kIx(	(
<g/>
Macrocheira	Macrocheira	k1gFnSc1	Macrocheira
kaempferi	kaempfer	k1gFnSc2	kaempfer
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
nohou	noha	k1gFnSc7	noha
až	až	k9	až
3,7	[number]	k4	3,7
m.	m.	k?	m.
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejčastěji	často	k6eAd3	často
chovaný	chovaný	k2eAgInSc1d1	chovaný
krab	krab	k1gInSc1	krab
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
krab	krab	k1gMnSc1	krab
suchozemský	suchozemský	k2eAgMnSc1d1	suchozemský
(	(	kIx(	(
<g/>
Cardisoma	Cardisoma	k1gNnSc1	Cardisoma
armatum	armatum	k1gNnSc1	armatum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
krab	krab	k1gMnSc1	krab
harlekýn	harlekýn	k1gMnSc1	harlekýn
nebo	nebo	k8xC	nebo
krab	krab	k1gMnSc1	krab
tříbarvý	tříbarvý	k2eAgMnSc1d1	tříbarvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Krabi	krab	k1gMnPc1	krab
jsou	být	k5eAaImIp3nP	být
rozšířeni	rozšířit	k5eAaPmNgMnP	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
místy	místy	k6eAd1	místy
invazní	invazní	k2eAgInSc1d1	invazní
druh	druh	k1gInSc1	druh
krab	krab	k1gInSc1	krab
říční	říční	k2eAgInSc1d1	říční
(	(	kIx(	(
<g/>
Eriocheir	Eriocheir	k1gInSc1	Eriocheir
sinensis	sinensis	k1gFnSc2	sinensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Krabi	krab	k1gMnPc1	krab
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
loveni	lovit	k5eAaImNgMnP	lovit
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
klepet	klepeto	k1gNnPc2	klepeto
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
odloví	odlovit	k5eAaPmIp3nS	odlovit
asi	asi	k9	asi
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
tun	tuna	k1gFnPc2	tuna
krabů	krab	k1gMnPc2	krab
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
země	země	k1gFnSc1	země
v	v	k7c6	v
Jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
sekce	sekce	k1gFnSc1	sekce
Dromiacea	Dromiace	k1gInSc2	Dromiace
De	De	k?	De
Haan	Haan	k1gMnSc1	Haan
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Homolodromioidea	Homolodromioidea	k1gMnSc1	Homolodromioidea
Alcock	Alcock	k1gMnSc1	Alcock
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
</s>
</p>
<p>
<s>
Homoloidea	Homoloidea	k1gMnSc1	Homoloidea
De	De	k?	De
Haan	Haan	k1gMnSc1	Haan
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
</s>
</p>
<p>
<s>
sekce	sekce	k1gFnSc1	sekce
Raninoida	Raninoida	k1gFnSc1	Raninoida
De	De	k?	De
Haan	Haana	k1gFnPc2	Haana
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
</s>
</p>
<p>
<s>
sekce	sekce	k1gFnSc1	sekce
Cyclodorippoida	Cyclodorippoida	k1gFnSc1	Cyclodorippoida
Ortmann	Ortmann	k1gInSc1	Ortmann
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
</s>
</p>
<p>
<s>
sekce	sekce	k1gFnSc1	sekce
Eubrachyura	Eubrachyur	k1gMnSc2	Eubrachyur
de	de	k?	de
Saint	Saint	k1gMnSc1	Saint
Laurent	Laurent	k1gMnSc1	Laurent
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
podsekce	podsekce	k1gFnSc1	podsekce
Heterotremata	Heterotrema	k1gNnPc4	Heterotrema
Guinot	Guinota	k1gFnPc2	Guinota
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Aethroidea	Aethroide	k2eAgFnSc1d1	Aethroide
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
</s>
</p>
<p>
<s>
Bellioidea	Bellioide	k2eAgFnSc1d1	Bellioide
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
1852	[number]	k4	1852
</s>
</p>
<p>
<s>
Bythograeoidea	Bythograeoidea	k1gFnSc1	Bythograeoidea
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Calappoidea	Calappoidea	k1gMnSc1	Calappoidea
De	De	k?	De
Haan	Haan	k1gMnSc1	Haan
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
</s>
</p>
<p>
<s>
Cancroidea	Cancroide	k2eAgFnSc1d1	Cancroide
Latreille	Latreille	k1gFnSc1	Latreille
<g/>
,	,	kIx,	,
1802	[number]	k4	1802
</s>
</p>
<p>
<s>
Carpilioidea	Carpilioidea	k1gMnSc1	Carpilioidea
Ortmann	Ortmann	k1gMnSc1	Ortmann
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Cheiragonoidea	Cheiragonoidea	k1gMnSc1	Cheiragonoidea
Ortmann	Ortmann	k1gMnSc1	Ortmann
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Dairoidea	Dairoidea	k1gMnSc1	Dairoidea
Serè	Serè	k1gMnSc1	Serè
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
Dorippoidea	Dorippoidea	k1gFnSc1	Dorippoidea
MacLeay	MacLeaa	k1gFnSc2	MacLeaa
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
</s>
</p>
<p>
<s>
Eriphioidea	Eriphioidea	k1gFnSc1	Eriphioidea
MacLeay	MacLeaa	k1gFnSc2	MacLeaa
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
</s>
</p>
<p>
<s>
Gecarcinucoidea	Gecarcinucoidea	k1gMnSc1	Gecarcinucoidea
Rathbun	Rathbun	k1gMnSc1	Rathbun
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
</s>
</p>
<p>
<s>
Goneplacoidea	Goneplacoidea	k1gFnSc1	Goneplacoidea
MacLeay	MacLeaa	k1gFnSc2	MacLeaa
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
</s>
</p>
<p>
<s>
Hexapodoidea	Hexapodoidea	k1gFnSc1	Hexapodoidea
Miers	Miersa	k1gFnPc2	Miersa
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
</s>
</p>
<p>
<s>
Leucosioidea	Leucosioide	k2eAgFnSc1d1	Leucosioide
Samouelle	Samouelle	k1gFnSc1	Samouelle
<g/>
,	,	kIx,	,
1819	[number]	k4	1819
</s>
</p>
<p>
<s>
Majoidea	Majoide	k2eAgFnSc1d1	Majoide
Samouelle	Samouelle	k1gFnSc1	Samouelle
<g/>
,	,	kIx,	,
1819	[number]	k4	1819
</s>
</p>
<p>
<s>
Orithyioidea	Orithyioide	k2eAgFnSc1d1	Orithyioide
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
1852	[number]	k4	1852
</s>
</p>
<p>
<s>
Palicoidea	Palicoidea	k1gMnSc1	Palicoidea
Bouvier	Bouvier	k1gMnSc1	Bouvier
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
</s>
</p>
<p>
<s>
Parthenopoidea	Parthenopoidea	k1gFnSc1	Parthenopoidea
MacLeay	MacLeaa	k1gFnSc2	MacLeaa
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
</s>
</p>
<p>
<s>
Pilumnoidea	Pilumnoide	k2eAgFnSc1d1	Pilumnoide
Samouelle	Samouelle	k1gFnSc1	Samouelle
<g/>
,	,	kIx,	,
1819	[number]	k4	1819
</s>
</p>
<p>
<s>
Portunoidea	Portunoide	k2eAgFnSc1d1	Portunoide
Rafinesque	Rafinesque	k1gFnSc1	Rafinesque
<g/>
,	,	kIx,	,
1815	[number]	k4	1815
</s>
</p>
<p>
<s>
Potamoidea	Potamoidea	k1gMnSc1	Potamoidea
Ortmann	Ortmann	k1gMnSc1	Ortmann
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
</s>
</p>
<p>
<s>
Pseudothelphusoidea	Pseudothelphusoidea	k1gMnSc1	Pseudothelphusoidea
Ortmann	Ortmann	k1gMnSc1	Ortmann
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Pseudozioidea	Pseudozioidea	k1gMnSc1	Pseudozioidea
Alcock	Alcock	k1gMnSc1	Alcock
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
</s>
</p>
<p>
<s>
Retroplumoidea	Retroplumoidea	k1gMnSc1	Retroplumoidea
Gill	Gill	k1gMnSc1	Gill
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Trapezioidea	Trapezioidea	k1gFnSc1	Trapezioidea
Miers	Miersa	k1gFnPc2	Miersa
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
</s>
</p>
<p>
<s>
Trichodactyloidea	Trichodactyloidea	k6eAd1	Trichodactyloidea
H.	H.	kA	H.
Milne-Edwards	Milne-Edwardsa	k1gFnPc2	Milne-Edwardsa
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
</s>
</p>
<p>
<s>
Xanthoidea	Xanthoidea	k1gFnSc1	Xanthoidea
MacLeay	MacLeaa	k1gFnSc2	MacLeaa
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
</s>
</p>
<p>
<s>
podsekce	podsekce	k1gFnSc1	podsekce
Thoracotremata	Thoracotrema	k1gNnPc4	Thoracotrema
Guinot	Guinota	k1gFnPc2	Guinota
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Cryptochiroidea	Cryptochiroidea	k1gMnSc1	Cryptochiroidea
Paul	Paul	k1gMnSc1	Paul
<g/>
'	'	kIx"	'
<g/>
son	son	k1gInSc1	son
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
</s>
</p>
<p>
<s>
Grapsoidea	Grapsoidea	k1gFnSc1	Grapsoidea
MacLeay	MacLeaa	k1gFnSc2	MacLeaa
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
</s>
</p>
<p>
<s>
Ocypodoidea	Ocypodoide	k2eAgFnSc1d1	Ocypodoide
Rafinesque	Rafinesque	k1gFnSc1	Rafinesque
<g/>
,	,	kIx,	,
1815	[number]	k4	1815
</s>
</p>
<p>
<s>
Pinnotheroidea	Pinnotheroidea	k1gMnSc1	Pinnotheroidea
De	De	k?	De
Haan	Haan	k1gMnSc1	Haan
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krabi	krab	k1gMnPc1	krab
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
krab	krab	k1gInSc1	krab
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
