<s>
Sultanát	sultanát	k1gInSc1	sultanát
Brunej	Brunej	k1gFnSc2	Brunej
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
Brunej	Brunej	k1gFnSc1	Brunej
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
bohatých	bohatý	k2eAgFnPc2d1	bohatá
ropných	ropný	k2eAgFnPc2d1	ropná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Borneo	Borneo	k1gNnSc1	Borneo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jihočínského	jihočínský	k2eAgNnSc2d1	Jihočínské
moře	moře	k1gNnSc2	moře
sousedí	sousedit	k5eAaImIp3nS	sousedit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Malajsií	Malajsie	k1gFnSc7	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Brunej	Brunat	k5eAaPmRp2nS	Brunat
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
Sdružení	sdružení	k1gNnSc4	sdružení
národů	národ	k1gInPc2	národ
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
ASEAN	ASEAN	kA	ASEAN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sultánova	sultánův	k2eAgNnSc2d1	sultánovo
nařízení	nařízení	k1gNnSc2	nařízení
se	se	k3xPyFc4	se
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
nemusejí	muset	k5eNaImIp3nP	muset
platit	platit	k5eAaImF	platit
daně	daň	k1gFnPc1	daň
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
mají	mít	k5eAaImIp3nP	mít
zdarma	zdarma	k6eAd1	zdarma
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Zásoby	zásoba	k1gFnPc1	zásoba
ropy	ropa	k1gFnSc2	ropa
podle	podle	k7c2	podle
CIA	CIA	kA	CIA
Factbook	Factbook	k1gInSc4	Factbook
2011	[number]	k4	2011
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
110	[number]	k4	110
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
barelů	barel	k1gInPc2	barel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
při	při	k7c6	při
současné	současný	k2eAgFnSc6d1	současná
těžbě	těžba	k1gFnSc6	těžba
vystačí	vystačit	k5eAaBmIp3nS	vystačit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
těžba	těžba	k1gFnSc1	těžba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
výroba	výroba	k1gFnSc1	výroba
(	(	kIx(	(
<g/>
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
stavu	stav	k1gInSc6	stav
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
krychlových	krychlový	k2eAgInPc2d1	krychlový
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
pouze	pouze	k6eAd1	pouze
tří	tři	k4xCgFnPc2	tři
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
a	a	k8xC	a
Palau	Palaus	k1gInSc2	Palaus
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
nulový	nulový	k2eAgInSc4d1	nulový
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
včetně	včetně	k7c2	včetně
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
státní	státní	k2eAgInSc1d1	státní
dluh	dluh	k1gInSc1	dluh
začal	začít	k5eAaPmAgInS	začít
pomalu	pomalu	k6eAd1	pomalu
narůstat	narůstat	k5eAaImF	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
výše	výše	k1gFnPc4	výše
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
vystoupala	vystoupat	k5eAaPmAgFnS	vystoupat
na	na	k7c4	na
2,46	[number]	k4	2,46
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc4	parlament
tvořila	tvořit	k5eAaImAgFnS	tvořit
dvacetičlenná	dvacetičlenný	k2eAgFnSc1d1	dvacetičlenná
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Brunej	Brunej	k1gFnSc4	Brunej
součástí	součást	k1gFnPc2	součást
sultanátů	sultanát	k1gInPc2	sultanát
Sabah	Sabaha	k1gFnPc2	Sabaha
<g/>
,	,	kIx,	,
Sarawak	Sarawak	k1gInSc1	Sarawak
a	a	k8xC	a
spodní	spodní	k2eAgFnPc1d1	spodní
části	část	k1gFnPc1	část
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Brunej	Brunej	k1gFnPc2	Brunej
kolbištěm	kolbiště	k1gNnSc7	kolbiště
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
terčem	terč	k1gInSc7	terč
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
pirátů	pirát	k1gMnPc2	pirát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
sultán	sultán	k1gMnSc1	sultán
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
se	se	k3xPyFc4	se
Brunej	Brunej	k1gFnSc1	Brunej
stala	stát	k5eAaPmAgFnS	stát
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
osídlovací	osídlovací	k2eAgInSc1d1	osídlovací
systém	systém	k1gInSc1	systém
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Britští	britský	k2eAgMnPc1d1	britský
občané	občan	k1gMnPc1	občan
mohli	moct	k5eAaImAgMnP	moct
zemi	zem	k1gFnSc4	zem
osídlovat	osídlovat	k5eAaImF	osídlovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Dodatkové	dodatkový	k2eAgFnSc2d1	dodatková
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Zakládali	zakládat	k5eAaImAgMnP	zakládat
zde	zde	k6eAd1	zde
farmy	farma	k1gFnSc2	farma
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc2d1	obchodní
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
zavedli	zavést	k5eAaPmAgMnP	zavést
intenzivní	intenzivní	k2eAgNnSc4d1	intenzivní
pěstování	pěstování	k1gNnSc4	pěstování
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
tropického	tropický	k2eAgNnSc2d1	tropické
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
až	až	k9	až
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
okupována	okupovat	k5eAaBmNgFnS	okupovat
japonskou	japonský	k2eAgFnSc7d1	japonská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
sjednocena	sjednocen	k2eAgFnSc1d1	sjednocena
země	země	k1gFnSc1	země
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
existenci	existence	k1gFnSc4	existence
místní	místní	k2eAgFnSc2d1	místní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
úplné	úplný	k2eAgMnPc4d1	úplný
nezávislosti	nezávislost	k1gFnSc2	nezávislost
jak	jak	k8xC	jak
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
tak	tak	k6eAd1	tak
i	i	k9	i
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
sultán	sultán	k1gMnSc1	sultán
Haji	haji	k0	haji
Sir	sir	k1gMnSc1	sir
Muda	Mudum	k1gNnSc2	Mudum
Omar	Omar	k1gMnSc1	Omar
Ali	Ali	k1gMnSc1	Ali
Saifuddienna	Saifuddienna	k1gFnSc1	Saifuddienna
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
sultána	sultán	k1gMnSc2	sultán
Pengiran	Pengirana	k1gFnPc2	Pengirana
Muda	Mudus	k1gMnSc2	Mudus
Mahkota	Mahkot	k1gMnSc2	Mahkot
Hassanal	Hassanal	k1gMnSc1	Hassanal
Bolkiah	Bolkiah	k1gMnSc1	Bolkiah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
přijal	přijmout	k5eAaPmAgMnS	přijmout
sultán	sultán	k1gMnSc1	sultán
také	také	k9	také
funkci	funkce	k1gFnSc4	funkce
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Panuje	panovat	k5eAaImIp3nS	panovat
zde	zde	k6eAd1	zde
rovníkové	rovníkový	k2eAgNnSc4d1	rovníkové
klima	klima	k1gNnSc4	klima
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
velkými	velký	k2eAgFnPc7d1	velká
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
23	[number]	k4	23
až	až	k8xS	až
32	[number]	k4	32
stupni	stupeň	k1gInPc7	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgInPc1d1	průměrný
srážkové	srážkový	k2eAgInPc1d1	srážkový
úhrny	úhrn	k1gInPc1	úhrn
zde	zde	k6eAd1	zde
představují	představovat	k5eAaImIp3nP	představovat
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
2500	[number]	k4	2500
mm	mm	kA	mm
a	a	k8xC	a
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
až	až	k9	až
7500	[number]	k4	7500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Brunej	Brunat	k5eAaImRp2nS	Brunat
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
distriktů	distrikt	k1gInPc2	distrikt
<g/>
:	:	kIx,	:
Tutong	Tutong	k1gInSc1	Tutong
Brunei	Brune	k1gFnSc2	Brune
<g/>
/	/	kIx~	/
<g/>
Muara	Muar	k1gMnSc2	Muar
Belait	Belait	k2eAgInSc1d1	Belait
Temburong	Temburong	k1gInSc1	Temburong
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
sultanátu	sultanát	k1gInSc2	sultanát
je	být	k5eAaImIp3nS	být
Bandar	Bandar	k1gMnSc1	Bandar
Seri	Seri	k1gNnSc4	Seri
Begawan	Begawany	k1gInPc2	Begawany
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
16	[number]	k4	16
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Pověstný	pověstný	k2eAgInSc1d1	pověstný
vodní	vodní	k2eAgInSc1d1	vodní
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Kampong	Kampong	k1gMnSc1	Kampong
Ayer	Ayer	k1gMnSc1	Ayer
<g/>
)	)	kIx)	)
Bruneje	Brunej	k1gInSc2	Brunej
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
náboženstvím	náboženství	k1gNnSc7	náboženství
sultanátu	sultanát	k1gInSc2	sultanát
Brunei	Brune	k1gFnSc2	Brune
Darussalam	Darussalam	k1gInSc1	Darussalam
je	být	k5eAaImIp3nS	být
Islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
jeho	jeho	k3xOp3gFnSc1	jeho
sunnitská	sunnitský	k2eAgFnSc1d1	sunnitská
větev	větev	k1gFnSc1	větev
šáfiovského	šáfiovský	k2eAgInSc2d1	šáfiovský
mazhabu	mazhab	k1gInSc2	mazhab
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
většinových	většinový	k2eAgMnPc2d1	většinový
brunejských	brunejský	k2eAgMnPc2d1	brunejský
Malajců	Malajec	k1gMnPc2	Malajec
a	a	k8xC	a
etnických	etnický	k2eAgMnPc2d1	etnický
brunejských	brunejský	k2eAgMnPc2d1	brunejský
Číňanů	Číňan	k1gMnPc2	Číňan
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ukotven	ukotvit	k5eAaPmNgInS	ukotvit
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
islámské	islámský	k2eAgFnSc2d1	islámská
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
sultanátu	sultanát	k1gInSc6	sultanát
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
sultánská	sultánský	k2eAgFnSc1d1	sultánská
výsost	výsost	k1gFnSc1	výsost
a	a	k8xC	a
Yang	Yang	k1gMnSc1	Yang
Di-Pertuan	Di-Pertuan	k1gMnSc1	Di-Pertuan
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
zdejší	zdejší	k2eAgFnSc6d1	zdejší
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgNnSc1d1	náboženské
složení	složení	k1gNnSc1	složení
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
islám	islám	k1gInSc1	islám
67	[number]	k4	67
%	%	kIx~	%
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
13	[number]	k4	13
%	%	kIx~	%
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc2	křesťanství
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
víry	vír	k1gInPc1	vír
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
