<p>
<s>
Brooke	Brooke	k1gFnSc1	Brooke
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
díky	díky	k7c3	díky
roli	role	k1gFnSc3	role
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Chirurgové	chirurg	k1gMnPc5	chirurg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
postavu	postava	k1gFnSc4	postava
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Eriky	Erika	k1gFnSc2	Erika
Hahn	Hahn	k1gInSc1	Hahn
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
roli	role	k1gFnSc3	role
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Martinové	Martinová	k1gFnSc2	Martinová
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Mlčení	mlčení	k1gNnSc4	mlčení
jehňátek	jehňátko	k1gNnPc2	jehňátko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
jako	jako	k8xS	jako
dcera	dcera	k1gFnSc1	dcera
vydavatele	vydavatel	k1gMnSc2	vydavatel
Gena	Genus	k1gMnSc2	Genus
Smithe	Smith	k1gMnSc2	Smith
a	a	k8xC	a
novinářky	novinářka	k1gFnSc2	novinářka
Lois	Loisa	k1gFnPc2	Loisa
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Robertem	Robert	k1gMnSc7	Robert
Redfordem	Redford	k1gInSc7	Redford
a	a	k8xC	a
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
herci	herec	k1gMnPc7	herec
a	a	k8xC	a
režiséry	režisér	k1gMnPc7	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
Tappan	Tappan	k1gMnSc1	Tappan
Zee	Zee	k1gMnSc1	Zee
High	High	k1gMnSc1	High
School	School	k1gInSc4	School
v	v	k7c6	v
Orangetown	Orangetown	k1gMnSc1	Orangetown
<g/>
/	/	kIx~	/
Blauvelt	Blauvelt	k1gMnSc1	Blauvelt
<g/>
,	,	kIx,	,
NY	NY	kA	NY
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
American	American	k1gInSc4	American
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Dramatic	Dramatice	k1gFnPc2	Dramatice
Arts	Artsa	k1gFnPc2	Artsa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
po	po	k7c4	po
první	první	k4xOgInPc4	první
roce	rok	k1gInSc6	rok
studia	studio	k1gNnSc2	studio
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Bydlela	bydlet	k5eAaImAgFnS	bydlet
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Jeffem	Jeff	k1gMnSc7	Jeff
Buckleyem	Buckley	k1gMnSc7	Buckley
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
nahrál	nahrát	k5eAaBmAgMnS	nahrát
svou	svůj	k3xOyFgFnSc4	svůj
nejznámější	známý	k2eAgFnSc4d3	nejznámější
píseň	píseň	k1gFnSc4	píseň
a	a	k8xC	a
album	album	k1gNnSc4	album
Grace	Grace	k1gFnSc2	Grace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Smith	Smitha	k1gFnPc2	Smitha
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Modernisté	modernista	k1gMnPc1	modernista
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mlčení	mlčení	k1gNnSc1	mlčení
jehňátek	jehňátko	k1gNnPc2	jehňátko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Night	Night	k1gMnSc1	Night
We	We	k1gMnSc1	We
Never	Never	k1gMnSc1	Never
Met	Mety	k1gFnPc2	Mety
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
Báječný	báječný	k2eAgMnSc1d1	báječný
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vanya	Vanya	k1gMnSc1	Vanya
on	on	k3xPp3gMnSc1	on
42	[number]	k4	42
<g/>
<g />
.	.	kIx.	.
</s>
<s>
nd	nd	k?	nd
Street	Street	k1gInSc1	Street
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Last	Last	k2eAgInSc1d1	Last
Summer	Summer	k1gInSc1	Summer
in	in	k?	in
the	the	k?	the
Hamptons	Hamptons	k1gInSc1	Hamptons
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Můj	můj	k3xOp1gInSc1	můj
nejmilejší	milý	k2eAgInSc1d3	nejmilejší
bar	bar	k1gInSc1	bar
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc1	Kansas
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Broken	Brokna	k1gFnPc2	Brokna
Giant	Giant	k1gMnSc1	Giant
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Remembering	Remembering	k1gInSc1	Remembering
Sex	sex	k1gInSc1	sex
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Náhodné	náhodný	k2eAgNnSc1d1	náhodné
setkání	setkání	k1gNnSc1	setkání
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eventual	Eventual	k1gMnSc1	Eventual
Wife	Wif	k1gFnSc2	Wif
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zabít	zabít	k5eAaPmF	zabít
nebo	nebo	k8xC	nebo
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
:	:	kIx,	:
Hráči	hráč	k1gMnPc1	hráč
-	-	kIx~	-
série	série	k1gFnSc1	série
sedmá	sedmý	k4xOgFnSc1	sedmý
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
For	forum	k1gNnPc2	forum
Earth	Earth	k1gMnSc1	Earth
Below	Below	k1gMnSc1	Below
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
spojka	spojka	k1gFnSc1	spojka
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Melinda	Melinda	k1gFnSc1	Melinda
a	a	k8xC	a
Melinda	Melinda	k1gFnSc1	Melinda
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andělé	anděl	k1gMnPc1	anděl
s	s	k7c7	s
ocelovým	ocelový	k2eAgInSc7d1	ocelový
hlasem	hlas	k1gInSc7	hlas
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shooting	Shooting	k1gInSc1	Shooting
Vegetarians	Vegetarians	k1gInSc1	Vegetarians
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zná	znát	k5eAaImIp3nS	znát
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
svý	svý	k?	svý
boty	bota	k1gFnPc1	bota
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jmenovec	jmenovec	k1gMnSc1	jmenovec
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
Smith	Smith	k1gMnSc1	Smith
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
Drzá	drzý	k2eAgFnSc1d1	drzá
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kate	kat	k1gInSc5	kat
Switzer	Switzero	k1gNnPc2	Switzero
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Odpočívej	odpočívat	k5eAaImRp2nS	odpočívat
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
učitelů	učitel	k1gMnPc2	učitel
umění	umění	k1gNnSc2	umění
Claire	Clair	k1gInSc5	Clair
Fisher	Fishra	k1gFnPc2	Fishra
a	a	k8xC	a
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Tráva	tráva	k1gFnSc1	tráva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
exmanželku	exmanželka	k1gFnSc4	exmanželka
Petera	Peter	k1gMnSc2	Peter
Scottsona	Scottson	k1gMnSc2	Scottson
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
manžela	manžel	k1gMnSc2	manžel
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
seriálu	seriál	k1gInSc2	seriál
Nancy	Nancy	k1gFnSc2	Nancy
Botwin	Botwina	k1gFnPc2	Botwina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brooke	Brooke	k1gFnSc1	Brooke
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
pilotním	pilotní	k2eAgNnSc6d1	pilotní
díle	dílo	k1gNnSc6	dílo
seriálu	seriál	k1gInSc6	seriál
Dirty	Dirta	k1gFnSc2	Dirta
Sexy	sex	k1gInPc1	sex
Money	Monea	k1gFnSc2	Monea
Andreu	Andrea	k1gFnSc4	Andrea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
dílech	dílo	k1gNnPc6	dílo
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
herečkou	herečka	k1gFnSc7	herečka
Sheryl	Sheryl	k1gInSc1	Sheryl
Lee	Lea	k1gFnSc6	Lea
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ale	ale	k9	ale
přišla	přijít	k5eAaPmAgFnS	přijít
její	její	k3xOp3gFnSc1	její
průlomová	průlomový	k2eAgFnSc1d1	průlomová
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Erica	Eric	k2eAgFnSc1d1	Erica
Hahnová	Hahnová	k1gFnSc1	Hahnová
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
totiž	totiž	k9	totiž
do	do	k7c2	do
seriálu	seriál	k1gInSc2	seriál
Chirurgové	chirurg	k1gMnPc5	chirurg
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zde	zde	k6eAd1	zde
odehrála	odehrát	k5eAaPmAgFnS	odehrát
23	[number]	k4	23
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
<g/>
Získala	získat	k5eAaPmAgFnS	získat
také	také	k9	také
role	role	k1gFnSc1	role
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
Zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
pořádek	pořádek	k1gInSc1	pořádek
<g/>
:	:	kIx,	:
Zločinné	zločinný	k2eAgInPc1d1	zločinný
úmysly	úmysl	k1gInPc1	úmysl
<g/>
,	,	kIx,	,
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
pořádek	pořádek	k1gInSc1	pořádek
<g/>
,	,	kIx,	,
Myšlenky	myšlenka	k1gFnPc1	myšlenka
zločince	zločinec	k1gMnSc2	zločinec
a	a	k8xC	a
The	The	k1gMnSc1	The
Hunger	Hunger	k1gMnSc1	Hunger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Chirurgové	chirurg	k1gMnPc5	chirurg
====	====	k?	====
</s>
</p>
<p>
<s>
Smith	Smith	k1gMnSc1	Smith
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
sezóně	sezóna	k1gFnSc6	sezóna
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
číslo	číslo	k1gNnSc1	číslo
5	[number]	k4	5
"	"	kIx"	"
<g/>
Haunt	Haunt	k1gInSc1	Haunt
You	You	k1gMnSc2	You
Every	Evera	k1gMnSc2	Evera
Day	Day	k1gMnSc2	Day
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
získala	získat	k5eAaPmAgFnS	získat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hahnová	Hahnová	k1gFnSc1	Hahnová
místo	místo	k7c2	místo
šéfky	šéfka	k1gFnSc2	šéfka
kardiologického	kardiologický	k2eAgNnSc2d1	kardiologické
oddělení	oddělení	k1gNnSc2	oddělení
nemocnice	nemocnice	k1gFnSc2	nemocnice
Seattle	Seattle	k1gFnSc2	Seattle
Grace	Grace	k1gFnSc2	Grace
<g/>
.	.	kIx.	.
</s>
<s>
Brooke	Brooke	k1gFnSc1	Brooke
Smith	Smitha	k1gFnPc2	Smitha
patřila	patřit	k5eAaImAgFnS	patřit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
sezóně	sezóna	k1gFnSc6	sezóna
byla	být	k5eAaImAgFnS	být
vyhozena	vyhozen	k2eAgFnSc1d1	vyhozena
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
sdělila	sdělit	k5eAaPmAgFnS	sdělit
magazínu	magazín	k1gInSc2	magazín
Entertainment	Entertainment	k1gInSc4	Entertainment
Weekly	Weekl	k1gInPc7	Weekl
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
byla	být	k5eAaImAgFnS	být
vyškrtnuta	vyškrtnout	k5eAaPmNgFnS	vyškrtnout
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gInSc4	Smith
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
opravdu	opravdu	k6eAd1	opravdu
velmi	velmi	k6eAd1	velmi
šokující	šokující	k2eAgMnSc1d1	šokující
<g/>
"	"	kIx"	"
a	a	k8xC	a
nadnesla	nadnést	k5eAaPmAgFnS	nadnést
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
vyškrtnutí	vyškrtnutí	k1gNnSc1	vyškrtnutí
postavy	postava	k1gFnSc2	postava
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
nepříjemné	příjemný	k2eNgInPc4d1	nepříjemný
pocity	pocit	k1gInPc4	pocit
kvůli	kvůli	k7c3	kvůli
lesbickému	lesbický	k2eAgInSc3d1	lesbický
vztahu	vztah	k1gInSc3	vztah
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vedla	vést	k5eAaImAgFnS	vést
s	s	k7c7	s
postavou	postava	k1gFnSc7	postava
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Calliope	Calliop	k1gInSc5	Calliop
Torres	Torres	k1gMnSc1	Torres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Smith	Smith	k1gInSc4	Smith
promlouvala	promlouvat	k5eAaImAgFnS	promlouvat
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
debatě	debata	k1gFnSc6	debata
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
Denveru	Denver	k1gInSc6	Denver
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
kandidáta	kandidát	k1gMnSc2	kandidát
Ralpha	Ralph	k1gMnSc2	Ralph
Nadera	nadrat	k5eAaBmSgInS	nadrat
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
účastníkem	účastník	k1gMnSc7	účastník
newyorské	newyorský	k2eAgFnSc2d1	newyorská
hardcore	hardcor	k1gInSc5	hardcor
punkové	punkový	k2eAgFnSc2d1	punková
scény	scéna	k1gFnPc1	scéna
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
její	její	k3xOp3gNnPc4	její
fotografie	fotografia	k1gFnPc4	fotografia
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
spolku	spolek	k1gInSc2	spolek
"	"	kIx"	"
<g/>
Street	Street	k1gInSc1	Street
Boners	Boners	k1gInSc1	Boners
and	and	k?	and
TV	TV	kA	TV
Carnage	Carnag	k1gFnSc2	Carnag
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Smith	Smith	k1gMnSc1	Smith
se	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
ruského	ruský	k2eAgMnSc2d1	ruský
kameramana	kameraman	k1gMnSc2	kameraman
Steva	Steve	k1gMnSc2	Steve
Lubenskeho	Lubenske	k1gMnSc2	Lubenske
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
vychovávají	vychovávat	k5eAaImIp3nP	vychovávat
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
Fanny	Fanna	k1gFnPc4	Fanna
Grace	Grace	k1gFnSc2	Grace
Lubensky	Lubensky	k1gFnSc4	Lubensky
<g/>
,	,	kIx,	,
narozenou	narozený	k2eAgFnSc4d1	narozená
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
dívku	dívka	k1gFnSc4	dívka
z	z	k7c2	z
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
,	,	kIx,	,
Lucy	Luca	k1gFnSc2	Luca
Dinknesh	Dinknesha	k1gFnPc2	Dinknesha
Lubensky	Lubensky	k1gMnSc1	Lubensky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Upper	Uppra	k1gFnPc2	Uppra
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
a	a	k8xC	a
v	v	k7c6	v
Hollywoode	Hollywood	k1gInSc5	Hollywood
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Brooke	Brooke	k1gFnSc1	Brooke
Smith	Smitha	k1gFnPc2	Smitha
na	na	k7c4	na
IMDb	IMDb	k1gInSc4	IMDb
</s>
</p>
