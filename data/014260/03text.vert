<s>
Abstrakce	abstrakce	k1gFnSc1
</s>
<s>
Vytváření	vytváření	k1gNnSc1
mentálních	mentální	k2eAgInPc2d1
modelů	model	k1gInPc2
je	být	k5eAaImIp3nS
první	první	k4xOgInSc4
předpoklad	předpoklad	k1gInSc4
k	k	k7c3
racionálnímu	racionální	k2eAgNnSc3d1
myšlení	myšlení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
abs-trahere	abs-trahrat	k5eAaPmIp3nS
<g/>
,	,	kIx,
odtáhnout	odtáhnout	k5eAaPmF
<g/>
,	,	kIx,
odvléci	odvléct	k5eAaPmF
<g/>
,	,	kIx,
oddělit	oddělit	k5eAaPmF
<g/>
)	)	kIx)
ve	v	k7c4
filozofii	filozofie	k1gFnSc4
označuje	označovat	k5eAaImIp3nS
buď	buď	k8xC
důležitý	důležitý	k2eAgInSc1d1
moment	moment	k1gInSc1
procesu	proces	k1gInSc2
poznání	poznání	k1gNnSc2
při	při	k7c6
přechodu	přechod	k1gInSc6
od	od	k7c2
smyslového	smyslový	k2eAgNnSc2d1
k	k	k7c3
racionálnímu	racionální	k2eAgNnSc3d1
poznání	poznání	k1gNnSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
jako	jako	k9
hotový	hotový	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
tohoto	tento	k3xDgInSc2
procesu	proces	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
abstrakce	abstrakce	k1gFnSc2
spočívá	spočívat	k5eAaImIp3nS
obecně	obecně	k6eAd1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
analytických	analytický	k2eAgInPc2d1
aktů	akt	k1gInPc2
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
je	být	k5eAaImIp3nS
zpracováván	zpracováván	k2eAgInSc1d1
konkrétní	konkrétní	k2eAgInSc4d1
smyslový	smyslový	k2eAgInSc4d1
materiál	materiál	k1gInSc4
a	a	k8xC
při	při	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
odhlíží	odhlížet	k5eAaImIp3nP
od	od	k7c2
určitých	určitý	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
vlastností	vlastnost	k1gFnPc2
a	a	k8xC
vztahů	vztah	k1gInPc2
daného	daný	k2eAgInSc2d1
předmětu	předmět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
,	,	kIx,
vlastnosti	vlastnost	k1gFnPc1
a	a	k8xC
vztahy	vztah	k1gInPc1
jsou	být	k5eAaImIp3nP
naopak	naopak	k6eAd1
vyčleňovány	vyčleňován	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
podstatné	podstatný	k2eAgFnPc1d1
a	a	k8xC
současně	současně	k6eAd1
nabývají	nabývat	k5eAaImIp3nP
variabilní	variabilní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
prostřednictvím	prostřednictvím	k7c2
znaků	znak	k1gInPc2
nepodstatných	podstatný	k2eNgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Výsledkem	výsledek	k1gInSc7
procesu	proces	k1gInSc2
abstrakce	abstrakce	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
těsně	těsně	k6eAd1
spjat	spjat	k2eAgMnSc1d1
se	s	k7c7
zobecněním	zobecnění	k1gNnSc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pojmy	pojem	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
odrážejí	odrážet	k5eAaImIp3nP
podstatu	podstata	k1gFnSc4
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonymem	antonymum	k1gNnSc7
abstraktního	abstraktní	k2eAgMnSc2d1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
konkrétní	konkrétní	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
v	v	k7c6
teorii	teorie	k1gFnSc6
poznání	poznání	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Teorie	teorie	k1gFnSc1
poznání	poznání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Problém	problém	k1gInSc1
abstrakce	abstrakce	k1gFnSc2
patří	patřit	k5eAaImIp3nS
k	k	k7c3
základním	základní	k2eAgFnPc3d1
a	a	k8xC
nejobtížnějším	obtížný	k2eAgFnPc3d3
otázkám	otázka	k1gFnPc3
teorie	teorie	k1gFnSc2
poznání	poznání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Platón	Platón	k1gMnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc7
upozornil	upozornit	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
poznání	poznání	k1gNnSc6
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
nějakou	nějaký	k3yIgFnSc4
ideu	idea	k1gFnSc4
jako	jako	k8xS,k8xC
pojem	pojem	k1gInSc4
oddělit	oddělit	k5eAaPmF
od	od	k7c2
všech	všecek	k3xTgMnPc2
ostatních	ostatní	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aristotelés	Aristotelés	k1gInSc1
se	se	k3xPyFc4
abstrakcí	abstrakce	k1gFnSc7
zabýval	zabývat	k5eAaImAgMnS
podrobněji	podrobně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoumal	zkoumat	k5eAaImAgMnS
otázku	otázka	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
myšlení	myšlení	k1gNnSc1
dospívá	dospívat	k5eAaImIp3nS
od	od	k7c2
smyslově	smyslově	k6eAd1
daného	daný	k2eAgNnSc2d1
<g/>
,	,	kIx,
jednotlivého	jednotlivý	k2eAgNnSc2d1
k	k	k7c3
obecnému	obecný	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
myšlení	myšlení	k1gNnSc1
může	moct	k5eAaImIp3nS
poznat	poznat	k5eAaPmF
jen	jen	k9
obecné	obecný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formu	forma	k1gFnSc4
pouze	pouze	k6eAd1
prostřednictvím	prostřednictvím	k7c2
obrazových	obrazový	k2eAgFnPc2d1
představ	představa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
ve	v	k7c6
vnímání	vnímání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
myšlení	myšlení	k1gNnSc1
odděluje	oddělovat	k5eAaImIp3nS
určité	určitý	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
od	od	k7c2
konkrétních	konkrétní	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matematik	matematika	k1gFnPc2
například	například	k6eAd1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
úvahách	úvaha	k1gFnPc6
odhlíží	odhlížet	k5eAaImIp3nS
od	od	k7c2
všeho	všecek	k3xTgNnSc2
smyslového	smyslový	k2eAgNnSc2d1
<g/>
,	,	kIx,
ponechává	ponechávat	k5eAaImIp3nS
jen	jen	k9
kvantitu	kvantita	k1gFnSc4
a	a	k8xC
přemýšlí	přemýšlet	k5eAaImIp3nS
jen	jen	k9
o	o	k7c6
ní	on	k3xPp3gFnSc6
samotné	samotný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
tyto	tento	k3xDgInPc4
důležité	důležitý	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
však	však	k8xC
Aristotelés	Aristotelés	k1gInSc1
neuměl	umět	k5eNaImAgInS
vysvětlit	vysvětlit	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
vznikne	vzniknout	k5eAaPmIp3nS
abstraktní	abstraktní	k2eAgNnSc4d1
poznání	poznání	k1gNnSc4
ze	z	k7c2
smyslového	smyslový	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
Aristotela	Aristoteles	k1gMnSc4
navazuje	navazovat	k5eAaImIp3nS
v	v	k7c6
problému	problém	k1gInSc6
Abstrakce	abstrakce	k1gFnSc2
Tomáš	Tomáš	k1gMnSc1
Akvinský	Akvinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
teorii	teorie	k1gFnSc4
abstrakce	abstrakce	k1gFnSc1
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
prvky	prvek	k1gInPc7
platonismu	platonismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidský	lidský	k2eAgInSc1d1
rozum	rozum	k1gInSc1
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
účasti	účast	k1gFnSc3
na	na	k7c6
světle	světlo	k1gNnSc6
božího	boží	k2eAgInSc2d1
rozumu	rozum	k1gInSc2
prosvětluje	prosvětlovat	k5eAaImIp3nS
obrazné	obrazný	k2eAgFnPc4d1
představy	představa	k1gFnPc4
a	a	k8xC
abstrahuje	abstrahovat	k5eAaBmIp3nS
z	z	k7c2
nich	on	k3xPp3gMnPc2
formy	forma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
formy	forma	k1gFnPc1
existují	existovat	k5eAaImIp3nP
původně	původně	k6eAd1
v	v	k7c6
božím	boží	k2eAgInSc6d1
rozumu	rozum	k1gInSc6
před	před	k7c7
věcmi	věc	k1gFnPc7
<g/>
,	,	kIx,
současně	současně	k6eAd1
ve	v	k7c6
věcech	věc	k1gFnPc6
a	a	k8xC
poté	poté	k6eAd1
jako	jako	k9
obecné	obecný	k2eAgInPc1d1
pojmy	pojem	k1gInPc1
v	v	k7c6
lidském	lidský	k2eAgInSc6d1
rozumu	rozum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
dalšímu	další	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
abstrakcí	abstrakce	k1gFnSc7
<g/>
,	,	kIx,
přispěli	přispět	k5eAaPmAgMnP
empirističtí	empiristický	k2eAgMnPc1d1
filozofové	filozof	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Locke	Locke	k1gFnSc4
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
abstrakce	abstrakce	k1gFnSc1
je	být	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
rozumu	rozum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
této	tento	k3xDgFnSc2
činnosti	činnost	k1gFnSc2
vznikají	vznikat	k5eAaImIp3nP
obecné	obecný	k2eAgFnPc1d1
ideje	idea	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jejich	jejich	k3xOp3gNnSc3
označení	označení	k1gNnSc3
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
přidělena	přidělen	k2eAgFnSc1d1
obecná	obecná	k1gFnSc1
slova	slovo	k1gNnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
tím	ten	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
oddělena	oddělit	k5eAaPmNgNnP
od	od	k7c2
podmínek	podmínka	k1gFnPc2
času	čas	k1gInSc2
<g/>
,	,	kIx,
místa	místo	k1gNnSc2
a	a	k8xC
od	od	k7c2
jiných	jiný	k2eAgFnPc2d1
idejí	idea	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
stávají	stávat	k5eAaImIp3nP
tyto	tento	k3xDgFnPc1
ideje	idea	k1gFnPc1
obecnými	obecný	k2eAgFnPc7d1
reprezentanty	reprezentant	k1gMnPc4
všech	všecek	k3xTgFnPc2
věcí	věc	k1gFnPc2
toho	ten	k3xDgNnSc2
samého	samý	k3xTgInSc2
druhu	druh	k1gInSc2
a	a	k8xC
lze	lze	k6eAd1
tedy	tedy	k9
věci	věc	k1gFnPc4
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
různých	různý	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
obecnost	obecnost	k1gFnSc1
<g/>
,	,	kIx,
dosahována	dosahován	k2eAgFnSc1d1
abstrakcí	abstrakce	k1gFnSc7
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
rozumem	rozum	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
nepřísluší	příslušet	k5eNaImIp3nS
věcem	věc	k1gFnPc3
<g/>
,	,	kIx,
protože	protože	k8xS
ty	ten	k3xDgMnPc4
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
nazýváno	nazývat	k5eAaImNgNnS
podstatou	podstata	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jen	jen	k9
člověkem	člověk	k1gMnSc7
vytvořenou	vytvořený	k2eAgFnSc7d1
abstraktní	abstraktní	k2eAgFnSc7d1
idejí	idea	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lockovo	Lockův	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
prototypem	prototyp	k1gInSc7
pro	pro	k7c4
empiristické	empiristický	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
abstrakce	abstrakce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
prostřednictvím	prostřednictvím	k7c2
George	Georg	k1gMnSc2
Berkeleye	Berkeley	k1gMnSc2
a	a	k8xC
Davida	David	k1gMnSc2
Huma	Humus	k1gMnSc2
tradována	tradovat	k5eAaImNgFnS
v	v	k7c6
novopozitivismu	novopozitivismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
je	být	k5eAaImIp3nS
logická	logický	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
odhlíží	odhlížet	k5eAaImIp3nS
od	od	k7c2
určitých	určitý	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
vlastností	vlastnost	k1gFnPc2
a	a	k8xC
vztahů	vztah	k1gInPc2
daného	daný	k2eAgInSc2d1
předmětu	předmět	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jiné	jiný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
naopak	naopak	k6eAd1
vyčleňovány	vyčleňován	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
podstatné	podstatný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
generalizující	generalizující	k2eAgFnSc1d1
abstrakce	abstrakce	k1gFnSc1
–	–	k?
odděluje	oddělovat	k5eAaImIp3nS
nepodstatné	podstatný	k2eNgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
věci	věc	k1gFnSc2
<g/>
,	,	kIx,
vztahů	vztah	k1gInPc2
apod.	apod.	kA
vyčleňuje	vyčleňovat	k5eAaImIp3nS
podstatné	podstatný	k2eAgNnSc1d1
</s>
<s>
izolující	izolující	k2eAgFnSc1d1
abstrakce	abstrakce	k1gFnSc1
–	–	k?
vyčleňuje	vyčleňovat	k5eAaImIp3nS
určité	určitý	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
vztahy	vztah	k1gInPc1
atd.	atd.	kA
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
souvislostí	souvislost	k1gFnPc2
a	a	k8xC
propůjčuje	propůjčovat	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
samostatnou	samostatný	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
</s>
<s>
idealizující	idealizující	k2eAgFnSc1d1
abstrakce	abstrakce	k1gFnSc1
–	–	k?
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
pojmy	pojem	k1gInPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nejdříve	dříve	k6eAd3
konstruují	konstruovat	k5eAaImIp3nP
ideální	ideální	k2eAgInPc4d1
objekty	objekt	k1gInPc4
<g/>
,	,	kIx,
jimž	jenž	k3xRgInPc3
tyto	tento	k3xDgInPc1
pojmy	pojem	k1gInPc1
odpovídají	odpovídat	k5eAaImIp3nP
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Abstraktní	abstraktní	k2eAgFnSc1d1
malba	malba	k1gFnSc1
</s>
<s>
Abstraktní	abstraktní	k2eAgNnSc1d1
umění	umění	k1gNnSc1
je	být	k5eAaImIp3nS
styl	styl	k1gInSc4
moderního	moderní	k2eAgNnSc2d1
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
postrádající	postrádající	k2eAgFnSc2d1
konkrétní	konkrétní	k2eAgFnSc2d1
figury	figura	k1gFnSc2
<g/>
,	,	kIx,
objekty	objekt	k1gInPc4
a	a	k8xC
detaily	detail	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Známí	známý	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
abstraktního	abstraktní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
František	František	k1gMnSc1
Kupka	Kupka	k1gMnSc1
</s>
<s>
Gerhard	Gerhard	k1gMnSc1
Richter	Richter	k1gMnSc1
</s>
<s>
Bernet	Bernet	k1gMnSc1
Newman	Newman	k1gMnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Rothko	Rothko	k1gNnSc1
</s>
<s>
Pablo	Pabla	k1gFnSc5
Picasso	Picassa	k1gFnSc5
</s>
<s>
František	František	k1gMnSc1
Foltýn	Foltýn	k1gMnSc1
</s>
<s>
Piet	pieta	k1gFnPc2
Mondrian	Mondriany	k1gInPc2
</s>
<s>
Abstrakce	abstrakce	k1gFnPc4
tvořící	tvořící	k2eAgInSc4d1
exaktní	exaktní	k2eAgInSc4d1
svět	svět	k1gInSc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Exaktní	exaktní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Speciální	speciální	k2eAgFnSc7d1
částí	část	k1gFnSc7
abstraktního	abstraktní	k2eAgInSc2d1
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
exaktní	exaktní	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exaktní	exaktní	k2eAgInSc4d1
jako	jako	k8xS,k8xC
odborný	odborný	k2eAgInSc4d1
termín	termín	k1gInSc4
znamená	znamenat	k5eAaImIp3nS
absolutně	absolutně	k6eAd1
přesný	přesný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemůže	moct	k5eNaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
vztahovat	vztahovat	k5eAaImF
na	na	k7c4
reálný	reálný	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
na	na	k7c4
abstraktní	abstraktní	k2eAgFnPc4d1
konstrukce	konstrukce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jakékoli	jakýkoli	k3yIgNnSc4
měření	měření	k1gNnSc4
entit	entita	k1gFnPc2
reálného	reálný	k2eAgInSc2d1
světa	svět	k1gInSc2
vždy	vždy	k6eAd1
poskytuje	poskytovat	k5eAaImIp3nS
výsledky	výsledek	k1gInPc4
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
<g/>
,	,	kIx,
i	i	k8xC
nejpřesnější	přesný	k2eAgFnSc1d3
výrobní	výrobní	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
vytvořit	vytvořit	k5eAaPmF
výrobky	výrobek	k1gInPc4
pouze	pouze	k6eAd1
v	v	k7c6
jistých	jistý	k2eAgFnPc6d1
nenulových	nulový	k2eNgFnPc6d1
tolerancích	tolerance	k1gFnPc6
<g/>
,	,	kIx,
nikoli	nikoli	k9
absolutně	absolutně	k6eAd1
přesné	přesný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
reálným	reálný	k2eAgInSc7d1
světem	svět	k1gInSc7
nejsme	být	k5eNaImIp1nP
schopni	schopen	k2eAgMnPc1d1
pracovat	pracovat	k5eAaImF
(	(	kIx(
<g/>
přetvářet	přetvářet	k5eAaImF
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
ani	ani	k8xC
měřit	měřit	k5eAaImF
<g/>
)	)	kIx)
exaktně	exaktně	k6eAd1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmíněné	zmíněný	k2eAgFnPc1d1
abstraktní	abstraktní	k2eAgFnPc1d1
konstrukce	konstrukce	k1gFnPc1
jsou	být	k5eAaImIp3nP
exaktní	exaktní	k2eAgFnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
exaktně	exaktně	k6eAd1
vytyčeny	vytyčen	k2eAgInPc1d1
(	(	kIx(
<g/>
tj.	tj.	kA
s	s	k7c7
nulovou	nulový	k2eAgFnSc7d1
vnitřní	vnitřní	k2eAgFnSc7d1
vágností	vágnost	k1gFnSc7
,	,	kIx,
jejich	jejich	k3xOp3gInSc2
významu	význam	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
seznámený	seznámený	k2eAgMnSc1d1
odborník	odborník	k1gMnSc1
naprosto	naprosto	k6eAd1
přesně	přesně	k6eAd1
(	(	kIx(
<g/>
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgFnPc2
pochyb	pochyba	k1gFnPc2
<g/>
)	)	kIx)
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
znamenají	znamenat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
exaktnost	exaktnost	k1gFnSc1
oněch	onen	k3xDgFnPc2
abstraktních	abstraktní	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
(	(	kIx(
<g/>
říkejme	říkat	k5eAaImRp1nP
jim	on	k3xPp3gMnPc3
exaktní	exaktní	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
)	)	kIx)
tedy	tedy	k9
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
přesné	přesný	k2eAgFnSc6d1
a	a	k8xC
neomylné	omylný	k2eNgFnSc6d1
vazbě	vazba	k1gFnSc6
lidské	lidský	k2eAgFnSc2d1
psýchy	psýcha	k1gFnSc2
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
významem	význam	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
exaktního	exaktní	k2eAgInSc2d1
světa	svět	k1gInSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Matematika	matematika	k1gFnSc1
</s>
<s>
Exaktní	exaktní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
</s>
<s>
Exaktní	exaktní	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Exaktní	exaktní	k2eAgInPc1d1
stroje	stroj	k1gInPc1
–	–	k?
Turingův	Turingův	k2eAgInSc4d1
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
Chomského	Chomský	k2eAgNnSc2d1
formální	formální	k2eAgFnPc4d1
gramatiky	gramatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
v	v	k7c4
antropologii	antropologie	k1gFnSc4
</s>
<s>
Na	na	k7c4
abstrakci	abstrakce	k1gFnSc4
a	a	k8xC
abstraktní	abstraktní	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
pohlížet	pohlížet	k5eAaImF
jako	jako	k9
na	na	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
liší	lišit	k5eAaImIp3nS
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
organismů	organismus	k1gInPc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
;	;	kIx,
abstraktní	abstraktní	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
samotné	samotný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
znaků	znak	k1gInPc2
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
v	v	k7c6
informatice	informatika	k1gFnSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
abstrakce	abstrakce	k1gFnSc1
(	(	kIx(
<g/>
softwarové	softwarový	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
v	v	k7c6
informatice	informatika	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
přiblížení	přiblížení	k1gNnSc1
způsobu	způsob	k1gInSc2
práce	práce	k1gFnSc2
s	s	k7c7
daty	datum	k1gNnPc7
nebo	nebo	k8xC
technickými	technický	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
počítače	počítač	k1gInSc2
způsobu	způsob	k1gInSc2
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
by	by	kYmCp3nS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
pracoval	pracovat	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
programování	programování	k1gNnSc6
je	být	k5eAaImIp3nS
abstrakce	abstrakce	k1gFnSc1
zastoupena	zastoupit	k5eAaPmNgFnS
programovacími	programovací	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
nahradily	nahradit	k5eAaPmAgFnP
strojový	strojový	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
unixových	unixový	k2eAgInPc6d1
systémech	systém	k1gInPc6
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
ze	z	k7c2
základních	základní	k2eAgNnPc2d1
paradigmat	paradigma	k1gNnPc2
abstrakce	abstrakce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
zpřístupnit	zpřístupnit	k5eAaPmF
většinu	většina	k1gFnSc4
systémových	systémový	k2eAgMnPc2d1
prostředků	prostředek	k1gInPc2
jako	jako	k8xS,k8xC
běžný	běžný	k2eAgInSc4d1
soubor	soubor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abstrakcí	abstrakce	k1gFnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
se	s	k7c7
soubory	soubor	k1gInPc7
je	být	k5eAaImIp3nS
vytvoření	vytvoření	k1gNnSc3
jednotného	jednotný	k2eAgNnSc2d1
API	API	kA
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
volání	volání	k1gNnSc2
funkcí	funkce	k1gFnPc2
(	(	kIx(
<g/>
open	open	k1gMnSc1
<g/>
,	,	kIx,
read	read	k1gMnSc1
<g/>
,	,	kIx,
write	write	k5eAaPmIp2nP
<g/>
,	,	kIx,
close	close	k6eAd1
a	a	k8xC
dalších	další	k2eAgInPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
pracují	pracovat	k5eAaImIp3nP
se	s	k7c7
souborovým	souborový	k2eAgInSc7d1
deskriptorem	deskriptor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Modely	model	k1gInPc1
a	a	k8xC
systémy	systém	k1gInPc1
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Abstrakt	abstrakt	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Abstraktní	abstraktní	k2eAgInSc1d1
model	model	k1gInSc1
<g/>
,	,	kIx,
Abstraktní	abstraktní	k2eAgInSc1d1
datový	datový	k2eAgInSc1d1
typ	typ	k1gInSc1
a	a	k8xC
Abstraktní	abstraktní	k2eAgFnSc1d1
datová	datový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
abstrakce	abstrakce	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Abstraktní	abstraktní	k2eAgNnSc1d1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4141162-6	4141162-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85000258	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85000258	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
</s>
