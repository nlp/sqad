<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
superkontinent	superkontinent	k1gMnSc1	superkontinent
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozpadl	rozpadnout	k5eAaPmAgMnS	rozpadnout
na	na	k7c4	na
dnešní	dnešní	k2eAgInPc4d1	dnešní
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
?	?	kIx.	?
</s>
