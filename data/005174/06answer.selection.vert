<s>
Pangea	Pangea	k1gFnSc1	Pangea
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
všechny	všechen	k3xTgFnPc4	všechen
země	zem	k1gFnPc4	zem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
superkontinent	superkontinent	k1gInSc4	superkontinent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
formoval	formovat	k5eAaImAgInS	formovat
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
300	[number]	k4	300
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
paleozoiku	paleozoikum	k1gNnSc6	paleozoikum
a	a	k8xC	a
mezozoiku	mezozoikum	k1gNnSc6	mezozoikum
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
200	[number]	k4	200
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
důsledkem	důsledek	k1gInSc7	důsledek
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c4	na
menší	malý	k2eAgInPc4d2	menší
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
.	.	kIx.	.
</s>
