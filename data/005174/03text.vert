<s>
Pangea	Pangea	k1gFnSc1	Pangea
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
všechny	všechen	k3xTgFnPc4	všechen
země	zem	k1gFnPc4	zem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
superkontinent	superkontinent	k1gInSc4	superkontinent
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
formoval	formovat	k5eAaImAgInS	formovat
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
300	[number]	k4	300
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
paleozoiku	paleozoikum	k1gNnSc6	paleozoikum
a	a	k8xC	a
mezozoiku	mezozoikum	k1gNnSc6	mezozoikum
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
200	[number]	k4	200
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
důsledkem	důsledek	k1gInSc7	důsledek
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c4	na
menší	malý	k2eAgInPc4d2	menší
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
kontinenty	kontinent	k1gInPc1	kontinent
poprvé	poprvé	k6eAd1	poprvé
spojily	spojit	k5eAaPmAgInP	spojit
v	v	k7c4	v
Pangeu	Pangea	k1gFnSc4	Pangea
<g/>
,	,	kIx,	,
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
existují	existovat	k5eAaImIp3nP	existovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Ural	Ural	k1gInSc1	Ural
nebo	nebo	k8xC	nebo
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
</s>
<s>
Oceán	oceán	k1gInSc1	oceán
obklopující	obklopující	k2eAgFnSc4d1	obklopující
Pangeu	Pangea	k1gFnSc4	Pangea
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Panthalassa	Panthalassa	k1gFnSc1	Panthalassa
<g/>
.	.	kIx.	.
</s>
<s>
Pangea	Pangea	k1gFnSc1	Pangea
měla	mít	k5eAaImAgFnS	mít
tvar	tvar	k1gInSc4	tvar
písmene	písmeno	k1gNnSc2	písmeno
C	C	kA	C
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
její	její	k3xOp3gFnSc3	její
obrovské	obrovský	k2eAgFnSc3d1	obrovská
rozloze	rozloha	k1gFnSc3	rozloha
nedocházelo	docházet	k5eNaImAgNnS	docházet
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
k	k	k7c3	k
dešťovým	dešťový	k2eAgFnPc3d1	dešťová
srážkám	srážka	k1gFnPc3	srážka
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
bylo	být	k5eAaImAgNnS	být
extrémně	extrémně	k6eAd1	extrémně
suché	suchý	k2eAgNnSc1d1	suché
<g/>
.	.	kIx.	.
</s>
<s>
Jednolitost	jednolitost	k1gFnSc1	jednolitost
území	území	k1gNnSc2	území
umožnila	umožnit	k5eAaPmAgFnS	umožnit
suchozemské	suchozemský	k2eAgFnSc2d1	suchozemská
zvěři	zvěř	k1gFnSc3	zvěř
migrovat	migrovat	k5eAaImF	migrovat
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
k	k	k7c3	k
severnímu	severní	k2eAgNnSc3d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
pod	pod	k7c7	pod
původním	původní	k2eAgNnSc7d1	původní
umístěním	umístění	k1gNnSc7	umístění
Pangey	Pangea	k1gFnSc2	Pangea
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
rozpálen	rozpálit	k5eAaImNgInS	rozpálit
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
země	zem	k1gFnSc2	zem
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyvyšovat	vyvyšovat	k5eAaImF	vyvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Afrika	Afrika	k1gFnSc1	Afrika
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Pangea	Pangea	k1gFnSc1	Pangea
nebyla	být	k5eNaImAgFnS	být
první	první	k4xOgInSc4	první
superkontinent	superkontinent	k1gInSc4	superkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pannotia	Pannotia	k1gFnSc1	Pannotia
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
před	před	k7c7	před
600	[number]	k4	600
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
a	a	k8xC	a
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
se	se	k3xPyFc4	se
před	před	k7c7	před
550	[number]	k4	550
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Rodinie	Rodinie	k1gFnSc1	Rodinie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zformovala	zformovat	k5eAaPmAgFnS	zformovat
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
1	[number]	k4	1
100	[number]	k4	100
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
a	a	k8xC	a
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
před	před	k7c7	před
750	[number]	k4	750
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pangeu	Pangea	k1gFnSc4	Pangea
se	se	k3xPyFc4	se
coby	coby	k?	coby
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
superkontinentů	superkontinent	k1gMnPc2	superkontinent
podařilo	podařit	k5eAaPmAgNnS	podařit
geologům	geolog	k1gMnPc3	geolog
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jury	jura	k1gFnSc2	jura
a	a	k8xC	a
křídy	křída	k1gFnSc2	křída
se	se	k3xPyFc4	se
Pangea	Pangea	k1gFnSc1	Pangea
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Gondwanu	Gondwan	k1gInSc2	Gondwan
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
Laurasii	Laurasie	k1gFnSc4	Laurasie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
kontinenty	kontinent	k1gInPc4	kontinent
oddělil	oddělit	k5eAaPmAgInS	oddělit
oceán	oceán	k1gInSc1	oceán
Tethys	Tethys	k1gInSc1	Tethys
<g/>
.	.	kIx.	.
</s>
<s>
Protoatlantik	Protoatlantik	k1gMnSc1	Protoatlantik
se	se	k3xPyFc4	se
formoval	formovat	k5eAaImAgMnS	formovat
již	již	k6eAd1	již
v	v	k7c6	v
juře	jura	k1gFnSc6	jura
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Laurasie	Laurasie	k1gFnSc1	Laurasie
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neexistovala	existovat	k5eNaImAgFnS	existovat
a	a	k8xC	a
totéž	týž	k3xTgNnSc4	týž
platí	platit	k5eAaImIp3nS	platit
o	o	k7c6	o
Gondwaně	Gondwan	k1gInSc6	Gondwan
<g/>
.	.	kIx.	.
tektonická	tektonický	k2eAgFnSc1d1	tektonická
deska	deska	k1gFnSc1	deska
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pangea	Pangea	k1gFnSc1	Pangea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Přehled	přehled	k1gInSc4	přehled
USGS	USGS	kA	USGS
</s>
