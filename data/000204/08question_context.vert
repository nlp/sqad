<s>
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Woolridge	Woolridg	k1gInSc2	Woolridg
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1985	[number]	k4	1985
Lake	Lak	k1gInSc2	Lak
Placid	Placid	k1gInSc1	Placid
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgInSc7d1	umělecký
pseudonymem	pseudonym	k1gInSc7	pseudonym
jako	jako	k8xC	jako
Lana	lano	k1gNnSc2	lano
Del	Del	k1gFnSc2	Del
Rey	Rea	k1gFnSc2	Rea
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
umělecký	umělecký	k2eAgInSc1d1	umělecký
pseudonym	pseudonym	k1gInSc1	pseudonym
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kombinací	kombinace	k1gFnPc2	kombinace
jména	jméno	k1gNnSc2	jméno
hollywoodské	hollywoodský	k2eAgFnSc2d1	hollywoodská
herečky	herečka	k1gFnSc2	herečka
Lany	lano	k1gNnPc7	lano
Turner	turner	k1gMnSc1	turner
a	a	k8xC	a
auta	auto	k1gNnPc1	auto
Ford	ford	k1gInSc1	ford
Del	Del	k1gMnSc4	Del
Rey	Rea	k1gFnSc2	Rea
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
gangsterskou	gangsterský	k2eAgFnSc4d1	gangsterská
Nancy	Nancy	k1gFnSc4	Nancy
Sinatru	Sinatr	k1gInSc2	Sinatr
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
doménového	doménový	k2eAgMnSc2d1	doménový
investora	investor	k1gMnSc2	investor
a	a	k8xC	a
milionáře	milionář	k1gMnSc2	milionář
Roberta	Robert	k1gMnSc2	Robert
Granta	Grant	k1gMnSc2	Grant
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
Pat	pata	k1gFnPc2	pata
Grant	grant	k1gInSc1	grant
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
14	[number]	k4	14
let	léto	k1gNnPc2	léto
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c6	v
Lake	Lake	k1gFnSc6	Lake
Placid	Placid	k1gInSc4	Placid
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
sourozenci	sourozenec	k1gMnPc7	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
začínající	začínající	k2eAgFnSc3d1	začínající
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
poslána	poslán	k2eAgFnSc1d1	poslána
do	do	k7c2	do
internátní	internátní	k2eAgFnSc2d1	internátní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Connecticutu	Connecticut	k1gInSc6	Connecticut
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
období	období	k1gNnSc3	období
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgNnP	vyjádřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
GQ	GQ	kA	GQ
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
jsem	být	k5eAaImIp1nS	být
velký	velký	k2eAgMnSc1d1	velký
pijan	pijan	k1gMnSc1	pijan
<g/>
,	,	kIx,	,
pila	pila	k1gFnSc1	pila
jsem	být	k5eAaImIp1nS	být
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k8xC	i
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Myslela	myslet	k5eAaImAgFnS	myslet
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
celý	celý	k2eAgInSc1d1	celý
zatraceně	zatraceně	k6eAd1	zatraceně
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
byl	být	k5eAaImAgInS	být
moje	můj	k3xOp1gFnSc1	můj
první	první	k4xOgFnSc1	první
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomhle	tenhle	k3xDgInSc6	tenhle
období	období	k1gNnSc6	období
jsem	být	k5eAaImIp1nS	být
psala	psát	k5eAaImAgFnS	psát
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Born	Born	k1gNnSc1	Born
to	ten	k3xDgNnSc1	ten
Die	Die	k1gFnPc1	Die
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
se	se	k3xPyFc4	se
zbavila	zbavit	k5eAaPmAgFnS	zbavit
závislosti	závislost	k1gFnPc4	závislost
<g/>
,	,	kIx,	,
šla	jít	k5eAaImAgFnS	jít
studovat	studovat	k5eAaImF	studovat
newyorskou	newyorský	k2eAgFnSc4d1	newyorská
Fordham	Fordham	k1gInSc4	Fordham
University	universita	k1gFnPc1	universita
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
obor	obor	k1gInSc1	obor
filozofie	filozofie	k1gFnSc2	filozofie
-	-	kIx~	-
metafyzika	metafyzika	k1gFnSc1	metafyzika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
mluvila	mluvit	k5eAaImAgFnS	mluvit
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Reflex	reflex	k1gInSc4	reflex
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Filozofie	filozofie	k1gFnSc1	filozofie
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
promlouval	promlouvat	k5eAaImAgMnS	promlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
jsem	být	k5eAaImIp1nS	být
ráda	rád	k2eAgFnSc1d1	ráda
i	i	k9	i
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tohle	tenhle	k3xDgNnSc1	tenhle
mě	já	k3xPp1nSc4	já
opravdu	opravdu	k6eAd1	opravdu
hodně	hodně	k6eAd1	hodně
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Narazila	narazit	k5eAaPmAgFnS	narazit
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
řešili	řešit	k5eAaImAgMnP	řešit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsme	být	k5eAaImIp1nP	být
tady	tady	k6eAd1	tady
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsme	být	k5eAaImIp1nP	být
sem	sem	k6eAd1	sem
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vnímáme	vnímat	k5eAaImIp1nP	vnímat
jako	jako	k8xC	jako
realitu	realita	k1gFnSc4	realita
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
své	své	k1gNnSc4	své
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
tyhle	tenhle	k3xDgFnPc4	tenhle
otázky	otázka	k1gFnPc4	otázka
kladla	klást	k5eAaImAgFnS	klást
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
mi	já	k3xPp1nSc3	já
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
jí	on	k3xPp3gFnSc7	on
bylo	být	k5eAaImAgNnS	být
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
přívěsu	přívěs	k1gInSc2	přívěs
za	za	k7c4	za
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
takový	takový	k3xDgInSc1	takový
styl	styl	k1gInSc1	styl
života	život	k1gInSc2	život
líbil	líbit	k5eAaImAgInS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
let	let	k1gInSc1	let
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
rehabilitačním	rehabilitační	k2eAgNnSc6d1	rehabilitační
centru	centrum	k1gNnSc6	centrum
pro	pro	k7c4	pro
alkoholiky	alkoholik	k1gMnPc4	alkoholik
a	a	k8xC	a
drogově	drogově	k6eAd1	drogově
závislé	závislý	k2eAgNnSc1d1	závislé
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
má	mít	k5eAaImIp3nS	mít
vytetováno	vytetován	k2eAgNnSc1d1	vytetováno
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
M	M	kA	M
<g/>
"	"	kIx"	"
odkazované	odkazovaný	k2eAgFnPc1d1	odkazovaná
její	její	k3xOp3gFnSc3	její
babičce	babička	k1gFnSc3	babička
Madeleine	Madeleine	k1gFnSc2	Madeleine
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
rukách	ruka	k1gFnPc6	ruka
má	mít	k5eAaImIp3nS	mít
vytetované	vytetovaný	k2eAgNnSc1d1	vytetované
"	"	kIx"	"
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Trust	trust	k1gInSc1	trust
no	no	k9	no
one	one	k?	one
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prsteníčku	prsteníček	k1gInSc6	prsteníček
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
má	mít	k5eAaImIp3nS	mít
frázi	fráze	k1gFnSc4	fráze
"	"	kIx"	"
<g/>
Die	Die	k1gMnSc1	Die
young	young	k1gMnSc1	young
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
tetování	tetování	k1gNnSc1	tetování
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
-	-	kIx~	-
jména	jméno	k1gNnSc2	jméno
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Whitman	Whitman	k1gMnSc1	Whitman
a	a	k8xC	a
Nabokov	Nabokov	k1gInSc1	Nabokov
-	-	kIx~	-
a	a	k8xC	a
na	na	k7c6	na
hrudníku	hrudník	k1gInSc6	hrudník
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
Nina	Nina	k1gFnSc1	Nina
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Billie	Billie	k1gFnSc1	Billie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Popírá	popírat	k5eAaImIp3nS	popírat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
plastické	plastický	k2eAgFnPc4d1	plastická
operace	operace	k1gFnPc4	operace
včetně	včetně	k7c2	včetně
botoxu	botox	k1gInSc2	botox
ve	v	k7c6	v
rtech	ret	k1gInPc6	ret
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
dva	dva	k4xCgInPc4	dva
apartmány	apartmán	k1gInPc1	apartmán
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
frontmanem	frontman	k1gInSc7	frontman
skupiny	skupina	k1gFnSc2	skupina
Kasidy	Kasida	k1gFnSc2	Kasida
Barrie-James	Barrie-James	k1gMnSc1	Barrie-James
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neillem	Neill	k1gMnSc7	Neill
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
oficiálně	oficiálně	k6eAd1	oficiálně
skončil	skončit	k5eAaPmAgInS	skončit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc4	lano
už	už	k6eAd1	už
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
italským	italský	k2eAgMnSc7d1	italský
modním	modní	k1gMnSc7	modní
fotografem	fotograf	k1gMnSc7	fotograf
Francescem	Francesce	k1gMnSc7	Francesce
Carrozzinim	Carrozzinima	k1gFnPc2	Carrozzinima
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
strýc	strýc	k1gMnSc1	strýc
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
napsat	napsat	k5eAaBmF	napsat
milion	milion	k4xCgInSc4	milion
písniček	písnička	k1gFnPc2	písnička
jen	jen	k6eAd1	jen
se	s	k7c7	s
šesti	šest	k4xCc7	šest
akordy	akord	k1gInPc7	akord
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
nočních	noční	k2eAgInPc6d1	noční
klubech	klub	k1gInPc6	klub
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
pseudonymy	pseudonym	k1gInPc7	pseudonym
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Jump	Jump	k1gInSc1	Jump
Rope	Rope	k1gNnSc1	Rope
Queen	Queen	k1gInSc1	Queen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lizzy	Lizz	k1gInPc4	Lizz
Grant	grant	k1gInSc1	grant
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Phenomena	Phenomen	k2eAgFnSc1d1	Phenomena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přiznala	přiznat	k5eAaPmAgFnS	přiznat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměla	mít	k5eNaImAgFnS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
pokračovat	pokračovat	k5eAaImF	pokračovat
a	a	k8xC	a
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
kariérou	kariéra	k1gFnSc7	kariéra
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
to	ten	k3xDgNnSc4	ten
nebrala	brát	k5eNaImAgFnS	brát
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
album	album	k1gNnSc1	album
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
písněmi	píseň	k1gFnPc7	píseň
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Woolridge	Woolridg	k1gInSc2	Woolridg
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nese	nést	k5eAaImIp3nS	nést
dva	dva	k4xCgInPc4	dva
názvy	název	k1gInPc4	název
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
Me	Me	k1gMnSc1	Me
Stable	Stable	k1gMnSc1	Stable
<g/>
;	;	kIx,	;
Young	Young	k1gMnSc1	Young
Like	Lik	k1gFnSc2	Lik
Me	Me	k1gMnSc1	Me
-	-	kIx~	-
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
názvy	název	k1gInPc7	název
písní	píseň	k1gFnPc2	píseň
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
neznámé	známý	k2eNgFnPc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
nahrála	nahrát	k5eAaPmAgFnS	nahrát
Sirens	Sirens	k1gInSc4	Sirens
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
May	May	k1gMnSc1	May
Jailer	Jailer	k1gMnSc1	Jailer
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
5	[number]	k4	5
Point	pointa	k1gFnPc2	pointa
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vydala	vydat	k5eAaPmAgFnS	vydat
svoje	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
EP	EP	kA	EP
Kill	Kill	k1gInSc1	Kill
Kill	Kill	k1gInSc1	Kill
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
album	album	k1gNnSc4	album
jako	jako	k8xS	jako
Lana	lano	k1gNnPc4	lano
Del	Del	k1gFnPc2	Del
Ray	Ray	k1gFnSc1	Ray
a.k.a.	a.k.a.	k?	a.k.a.
Lizzy	Lizza	k1gFnSc2	Lizza
Grant	grant	k1gInSc1	grant
<g/>
.	.	kIx.	.
</s>
