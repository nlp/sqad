<p>
<s>
Obec	obec	k1gFnSc1	obec
Vážany	Vážana	k1gFnSc2	Vážana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Blansko	Blansko	k1gNnSc4	Blansko
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
231	[number]	k4	231
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1167	[number]	k4	1167
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
vladycké	vladycký	k2eAgFnSc2d1	vladycká
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Vážan	Vážana	k1gFnPc2	Vážana
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
staletí	staletí	k1gNnSc1	staletí
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
několik	několik	k4yIc1	několik
majitelů	majitel	k1gMnPc2	majitel
<g/>
,	,	kIx,	,
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
Vážany	Vážana	k1gFnPc1	Vážana
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
šebetovského	šebetovský	k2eAgNnSc2d1	šebetovský
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1647	[number]	k4	1647
</s>
</p>
<p>
<s>
Rovinné	rovinný	k2eAgNnSc1d1	rovinné
neopevněné	opevněný	k2eNgNnSc1d1	neopevněné
sídliště	sídliště	k1gNnSc1	sídliště
Na	na	k7c4	na
Vejštici	Vejštice	k1gFnSc4	Vejštice
<g/>
,	,	kIx,	,
archeologické	archeologický	k2eAgNnSc4d1	Archeologické
naleziště	naleziště	k1gNnSc4	naleziště
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Knínice	Knínice	k1gFnSc1	Knínice
u	u	k7c2	u
Boskovic	Boskovice	k1gInPc2	Boskovice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vážany	Vážana	k1gFnSc2	Vážana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Vážany	Vážana	k1gFnSc2	Vážana
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
