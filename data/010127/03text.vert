<p>
<s>
Morena	Moren	k2eAgFnSc1d1	Morena
Silva	Silva	k1gFnSc1	Silva
de	de	k?	de
Vaz	vaz	k1gInSc1	vaz
Setta	Setta	k1gMnSc1	Setta
Baccarin	Baccarin	k1gInSc1	Baccarin
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
brazilsko-americká	brazilskomerický	k2eAgFnSc1d1	brazilsko-americký
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
brazilské	brazilský	k2eAgFnSc2d1	brazilská
herečky	herečka	k1gFnSc2	herečka
Vera	Verus	k1gMnSc2	Verus
Setty	Setta	k1gMnSc2	Setta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gFnPc3	její
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
rolím	role	k1gFnPc3	role
patří	patřit	k5eAaImIp3nS	patřit
Inara	Inara	k1gMnSc1	Inara
Serra	Serra	k1gMnSc1	Serra
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Firefly	Firefly	k1gFnSc2	Firefly
<g/>
,	,	kIx,	,
Adria	Adria	k1gFnSc1	Adria
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
V	V	kA	V
<g/>
,	,	kIx,	,
Jessica	Jessic	k2eAgFnSc1d1	Jessica
Brodyová	Brodyová	k1gFnSc1	Brodyová
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
Leslie	Leslie	k1gFnSc2	Leslie
Thompkinsová	Thompkinsová	k1gFnSc1	Thompkinsová
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Gotham	Gotham	k1gInSc1	Gotham
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
do	do	k7c2	do
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Village	k1gFnSc4	Village
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
Fiorella	Fiorell	k1gMnSc2	Fiorell
LaGuardiy	LaGuardia	k1gMnSc2	LaGuardia
(	(	kIx(	(
<g/>
Fiorello	Fiorello	k1gNnSc1	Fiorello
H.	H.	kA	H.
LaGuardia	LaGuardium	k1gNnSc2	LaGuardium
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Music	Music	k1gMnSc1	Music
&	&	k?	&
Art	Art	k1gMnSc1	Art
and	and	k?	and
Performing	Performing	k1gInSc1	Performing
Arts	Arts	k1gInSc1	Arts
<g/>
)	)	kIx)	)
a	a	k8xC	a
divadelní	divadelní	k2eAgInSc4d1	divadelní
obor	obor	k1gInSc4	obor
na	na	k7c6	na
Juilliardově	Juilliardův	k2eAgFnSc6d1	Juilliardův
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
roli	role	k1gFnSc4	role
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
módy	móda	k1gFnSc2	móda
Parfém	parfém	k1gInSc1	parfém
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
role	role	k1gFnPc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Way	Way	k1gMnSc2	Way
Off	Off	k1gMnSc2	Off
Broadway	Broadwaa	k1gMnSc2	Broadwaa
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
pak	pak	k6eAd1	pak
získala	získat	k5eAaPmAgFnS	získat
roli	role	k1gFnSc4	role
ve	v	k7c6	v
sci-fi	scii	k1gNnSc6	sci-fi
seriálu	seriál	k1gInSc2	seriál
Firefly	Firefly	k1gFnSc2	Firefly
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
navazujícím	navazující	k2eAgInSc6d1	navazující
filmu	film	k1gInSc6	film
Serenity	Serenita	k1gFnSc2	Serenita
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
roli	role	k1gFnSc4	role
společnice	společnice	k1gFnSc2	společnice
(	(	kIx(	(
<g/>
luxusní	luxusní	k2eAgFnSc2d1	luxusní
prostitutky	prostitutka	k1gFnSc2	prostitutka
<g/>
)	)	kIx)	)
Inary	Inara	k1gFnSc2	Inara
Serry	Serra	k1gFnSc2	Serra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
desáté	desátý	k4xOgFnSc6	desátý
sérii	série	k1gFnSc6	série
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
navazujícím	navazující	k2eAgInSc6d1	navazující
filmu	film	k1gInSc6	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Archa	archa	k1gFnSc1	archa
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
hrála	hrát	k5eAaImAgFnS	hrát
Adriu	Adrius	k1gMnSc3	Adrius
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Valy	Vala	k1gMnSc2	Vala
Mal	málit	k5eAaImRp2nS	málit
Doran	Doran	k1gInSc4	Doran
a	a	k8xC	a
představitelku	představitelka	k1gFnSc4	představitelka
Orici	Orice	k1gFnSc4	Orice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
dostala	dostat	k5eAaPmAgFnS	dostat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
ABC	ABC	kA	ABC
V	V	kA	V
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
hrála	hrát	k5eAaImAgFnS	hrát
Annu	Anna	k1gFnSc4	Anna
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnPc4	vůdce
Návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
jako	jako	k9	jako
manželka	manželka	k1gFnSc1	manželka
amerického	americký	k2eAgMnSc2d1	americký
válečného	válečný	k2eAgMnSc2d1	válečný
veterána	veterán	k1gMnSc2	veterán
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2015	[number]	k4	2015
a	a	k8xC	a
2019	[number]	k4	2019
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Gotham	Gotham	k1gInSc1	Gotham
jako	jako	k8xS	jako
doktorka	doktorka	k1gFnSc1	doktorka
Leslie	Leslie	k1gFnSc1	Leslie
Thompkinsová	Thompkinsová	k1gFnSc1	Thompkinsová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
dabuje	dabovat	k5eAaBmIp3nS	dabovat
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
Gideon	Gideona	k1gFnPc2	Gideona
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
The	The	k1gMnSc1	The
Flash	Flash	k1gMnSc1	Flash
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Deadpool	Deadpoola	k1gFnPc2	Deadpoola
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
si	se	k3xPyFc3	se
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
i	i	k8xC	i
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
filmu	film	k1gInSc6	film
Deadpool	Deadpoola	k1gFnPc2	Deadpoola
2	[number]	k4	2
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
amerického	americký	k2eAgMnSc2d1	americký
filmového	filmový	k2eAgMnSc2d1	filmový
producenta	producent	k1gMnSc2	producent
a	a	k8xC	a
režiséra	režisér	k1gMnSc2	režisér
Austina	Austin	k2eAgNnSc2d1	Austin
Chicka	Chicko	k1gNnSc2	Chicko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Julius	Julius	k1gMnSc1	Julius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
zažádala	zažádat	k5eAaPmAgFnS	zažádat
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Benem	Ben	k1gInSc7	Ben
McKenziem	McKenzius	k1gMnSc7	McKenzius
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
potkala	potkat	k5eAaPmAgFnS	potkat
na	na	k7c4	na
natáčení	natáčení	k1gNnSc4	natáčení
seriálu	seriál	k1gInSc2	seriál
Gotham	Gotham	k1gInSc4	Gotham
<g/>
,	,	kIx,	,
čeká	čekat	k5eAaImIp3nS	čekat
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Frances	Frances	k1gMnSc1	Frances
Laitz	Laitz	k1gMnSc1	Laitz
Setta	Setta	k1gMnSc1	Setta
Schenkkan	Schenkkana	k1gFnPc2	Schenkkana
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
s	s	k7c7	s
McKenziem	McKenzium	k1gNnSc7	McKenzium
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Videohry	videohra	k1gFnSc2	videohra
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Morena	Moreno	k1gNnSc2	Moreno
Baccarin	Baccarin	k1gInSc4	Baccarin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Morena	Morena	k1gFnSc1	Morena
Baccarin	Baccarin	k1gInSc4	Baccarin
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Morena	Morena	k1gFnSc1	Morena
Baccarin	Baccarin	k1gInSc1	Baccarin
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
