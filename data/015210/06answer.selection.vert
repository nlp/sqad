<s desamb="1">
Byl	být	k5eAaImAgMnS
třetím	třetí	k4xOgMnSc7
panovníkem	panovník	k1gMnSc7
Hannoverské	hannoverský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
dvou	dva	k4xCgMnPc2
předchůdců	předchůdce	k1gMnPc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
Británii	Británie	k1gFnSc6
a	a	k8xC
angličtina	angličtina	k1gFnSc1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gInSc7
rodným	rodný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
.	.	kIx.
</s>