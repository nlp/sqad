<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
král	král	k1gMnSc1
Království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
král	král	k1gMnSc1
Hannoverska	Hannoversek	k1gMnSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1760	#num#	k4
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1820	#num#	k4
(	(	kIx(
<g/>
59	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
96	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1761	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1738	#num#	k4
</s>
<s>
Londýn	Londýn	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1820	#num#	k4
</s>
<s>
Windsorský	windsorský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
von	von	k1gInSc1
Mecklenburg-Strelitz	Mecklenburg-Strelitz	k1gMnSc1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
JiříFrederikVilémŠarlotaEduard	JiříFrederikVilémŠarlotaEduard	k1gMnSc1
AugustAugustaAlžbětaErnestAugust	AugustAugustaAlžbětaErnestAugust	k1gMnSc1
FrederikAdolfMarieSofieOctaviusAlfrédAmélie	FrederikAdolfMarieSofieOctaviusAlfrédAmélie	k1gFnSc1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Welfové	Welf	k1gMnPc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Hannoverská	hannoverský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Frederik	Frederik	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Sasko-Gothajská	sasko-gothajský	k2eAgNnPc1d1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1738	#num#	k4
Londýn	Londýn	k1gInSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1820	#num#	k4
Windsorský	windsorský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
panovník	panovník	k1gMnSc1
Království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
od	od	k7c2
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1760	#num#	k4
do	do	k7c2
spojení	spojení	k1gNnSc4
obou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1801	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
vévodou	vévoda	k1gMnSc7
brunšvicko-lüneburským	brunšvicko-lüneburský	k2eAgMnSc7d1
a	a	k8xC
kurfiřtem	kurfiřt	k1gMnSc7
hannoverským	hannoverský	k2eAgMnSc7d1
do	do	k7c2
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1814	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
hannoverské	hannoverský	k2eAgNnSc1d1
kurfiřtství	kurfiřtství	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
královstvím	království	k1gNnSc7
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgInS
králem	král	k1gMnSc7
hannoverským	hannoverský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
třetím	třetí	k4xOgMnSc7
panovníkem	panovník	k1gMnSc7
Hannoverské	hannoverský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
dvou	dva	k4xCgMnPc2
předchůdců	předchůdce	k1gMnPc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
Británii	Británie	k1gFnSc6
a	a	k8xC
angličtina	angličtina	k1gFnSc1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gInSc7
rodným	rodný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svůj	svůj	k3xOyFgInSc4
dlouhý	dlouhý	k2eAgInSc4d1
život	život	k1gInSc4
nikdy	nikdy	k6eAd1
nenavštívil	navštívit	k5eNaPmAgInS
Hannover	Hannover	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
bylo	být	k5eAaImAgNnS
poznamenáno	poznamenat	k5eAaPmNgNnS
sérií	série	k1gFnSc7
vojenských	vojenský	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
,	,	kIx,
do	do	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
Británie	Británie	k1gFnSc1
zapojila	zapojit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátečním	počáteční	k2eAgNnSc6d1
období	období	k1gNnSc6
porazila	porazit	k5eAaPmAgFnS
Británie	Británie	k1gFnSc1
Francii	Francie	k1gFnSc4
v	v	k7c6
sedmileté	sedmiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
dominantní	dominantní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
Indii	Indie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
amerických	americký	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
Británie	Británie	k1gFnSc1
ztratila	ztratit	k5eAaPmAgFnS
po	po	k7c6
americké	americký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
vznik	vznik	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
válečných	válečný	k2eAgInPc2d1
střetů	střet	k1gInPc2
s	s	k7c7
napoleonskou	napoleonský	k2eAgFnSc7d1
Francií	Francie	k1gFnSc7
skončila	skončit	k5eAaPmAgFnS
Napoleonovou	Napoleonův	k2eAgFnSc7d1
definitivní	definitivní	k2eAgFnSc7d1
porážkou	porážka	k1gFnSc7
roku	rok	k1gInSc2
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
trpěl	trpět	k5eAaImAgMnS
občasnými	občasný	k2eAgFnPc7d1
a	a	k8xC
později	pozdě	k6eAd2
trvalými	trvalý	k2eAgInPc7d1
záchvaty	záchvat	k1gInPc7
duševní	duševní	k2eAgFnSc2d1
choroby	choroba	k1gFnSc2
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
označované	označovaný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
šílenství	šílenství	k1gNnSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
projevy	projev	k1gInPc4
porfyrie	porfyrie	k1gFnSc2
<g/>
,	,	kIx,
těžké	těžký	k2eAgFnSc2d1
metabolické	metabolický	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
Peter	Peter	k1gMnSc1
Gerrard	Gerrard	k1gMnSc1
z	z	k7c2
britského	britský	k2eAgInSc2d1
neurologického	urologický	k2eNgInSc2d1,k2eAgInSc2d1
institutu	institut	k1gInSc2
UCL	UCL	kA
spíše	spíše	k9
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
maniodepresivní	maniodepresivní	k2eAgFnSc4d1
psychózu	psychóza	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
posledním	poslední	k2eAgInSc6d1
záchvatu	záchvat	k1gInSc6
roku	rok	k1gInSc2
1810	#num#	k4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
regentská	regentský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
vládl	vládnout	k5eAaImAgInS
jako	jako	k9
princ	princ	k1gMnSc1
regent	regent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
pak	pak	k6eAd1
vládl	vládnout	k5eAaImAgMnS
jako	jako	k9
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1738	#num#	k4
v	v	k7c4
Norfolk	Norfolk	k1gInSc4
House	house	k1gNnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
jako	jako	k8xS,k8xC
vnuk	vnuk	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Frederika	Frederik	k1gMnSc2
<g/>
,	,	kIx,
prince	princ	k1gMnSc2
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Augusty	Augusta	k1gMnSc2
Sasko-Gothajské	sasko-gothajský	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
studoval	studovat	k5eAaImAgMnS
systematicky	systematicky	k6eAd1
přírodní	přírodní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uměl	umět	k5eAaImAgMnS
psát	psát	k5eAaImF
a	a	k8xC
číst	číst	k5eAaImF
anglicky	anglicky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
a	a	k8xC
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
studoval	studovat	k5eAaImAgMnS
astronomii	astronomie	k1gFnSc4
<g/>
,	,	kIx,
matematiku	matematika	k1gFnSc4
<g/>
,	,	kIx,
francouzštinu	francouzština	k1gFnSc4
<g/>
,	,	kIx,
latinu	latina	k1gFnSc4
<g/>
,	,	kIx,
dějepis	dějepis	k1gInSc1
<g/>
,	,	kIx,
zeměpis	zeměpis	k1gInSc1
<g/>
,	,	kIx,
obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
zemědělství	zemědělství	k1gNnSc1
a	a	k8xC
ústavní	ústavní	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Frederika	Frederik	k1gMnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zákonitým	zákonitý	k2eAgMnSc7d1
dědicem	dědic	k1gMnSc7
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
princem	princ	k1gMnSc7
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1761	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Šarlotou	Šarlota	k1gFnSc7
von	von	k1gInSc4
Mecklenburg-Strelitz	Mecklenburg-Strelitza	k1gFnPc2
a	a	k8xC
o	o	k7c4
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
později	pozdě	k6eAd2
byli	být	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
korunováni	korunován	k2eAgMnPc1d1
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
spolu	spolu	k6eAd1
patnáct	patnáct	k4xCc4
potomků	potomek	k1gMnPc2
<g/>
,	,	kIx,
devět	devět	k4xCc1
synů	syn	k1gMnPc2
a	a	k8xC
šest	šest	k4xCc4
dcer	dcera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1762	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
August	August	k1gMnSc1
(	(	kIx(
<g/>
1763	#num#	k4
<g/>
–	–	k?
<g/>
1827	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
a	a	k8xC
Albany	Albana	k1gFnSc2
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1765	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
(	(	kIx(
<g/>
1766	#num#	k4
<g/>
–	–	k?
<g/>
1828	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1797	#num#	k4
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1754	#num#	k4
<g/>
–	–	k?
<g/>
1816	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
(	(	kIx(
<g/>
1767	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
a	a	k8xC
Strathearnu	Strathearn	k1gInSc2
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
Augusta	Augusta	k1gMnSc1
Žofie	Žofie	k1gFnSc2
(	(	kIx(
<g/>
1768	#num#	k4
<g/>
–	–	k?
<g/>
1840	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
1770	#num#	k4
<g/>
–	–	k?
<g/>
1840	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1818	#num#	k4
lankrabě	lankrabě	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
von	von	k1gInSc1
Hessen-Homburg	Hessen-Homburg	k1gInSc1
(	(	kIx(
<g/>
1769	#num#	k4
<g/>
–	–	k?
<g/>
1829	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ernst	Ernst	k1gMnSc1
August	August	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1771	#num#	k4
<g/>
–	–	k?
<g/>
1851	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
král	král	k1gMnSc1
Hannoveru	Hannover	k1gInSc2
</s>
<s>
August	August	k1gMnSc1
Frederik	Frederik	k1gMnSc1
(	(	kIx(
<g/>
1773	#num#	k4
<g/>
–	–	k?
<g/>
1843	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
ze	z	k7c2
Sussexu	Sussex	k1gInSc2
</s>
<s>
Adolf	Adolf	k1gMnSc1
Frederik	Frederik	k1gMnSc1
(	(	kIx(
<g/>
1774	#num#	k4
<g/>
–	–	k?
<g/>
1850	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Marie	Marie	k1gFnSc1
(	(	kIx(
<g/>
1776	#num#	k4
<g/>
–	–	k?
<g/>
1857	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1816	#num#	k4
Vilém	Vilém	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Gloucester	Gloucester	k1gMnSc1
a	a	k8xC
Edinburgh	Edinburgh	k1gMnSc1
(	(	kIx(
<g/>
1776	#num#	k4
<g/>
–	–	k?
<g/>
1834	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Žofie	Žofie	k1gFnSc1
(	(	kIx(
<g/>
1777	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oktavius	Oktavius	k1gInSc1
(	(	kIx(
<g/>
1779	#num#	k4
<g/>
–	–	k?
<g/>
1783	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alfred	Alfred	k1gMnSc1
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1782	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Amálie	Amálie	k1gFnSc1
(	(	kIx(
<g/>
1783	#num#	k4
<g/>
–	–	k?
<g/>
1810	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Královna	královna	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
s	s	k7c7
prvními	první	k4xOgFnPc7
dvěma	dva	k4xCgFnPc7
dětmi	dítě	k1gFnPc7
(	(	kIx(
<g/>
1765	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1762	#num#	k4
koupil	koupit	k5eAaPmAgMnS
pro	pro	k7c4
rodinné	rodinný	k2eAgInPc4d1
účely	účel	k1gInPc4
Buckingham	Buckingham	k1gInSc1
House	house	k1gNnSc1
(	(	kIx(
<g/>
nynější	nynější	k2eAgInSc1d1
Buckinghamský	buckinghamský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
St	St	kA
James	James	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Palace	Palace	k1gFnPc1
byl	být	k5eAaImAgInS
používán	používat	k5eAaImNgInS
pouze	pouze	k6eAd1
pro	pro	k7c4
oficiální	oficiální	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Počáteční	počáteční	k2eAgNnSc1d1
období	období	k1gNnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Počáteční	počáteční	k2eAgNnSc1d1
období	období	k1gNnSc1
vlády	vláda	k1gFnSc2
bylo	být	k5eAaImAgNnS
poznamenáno	poznamenat	k5eAaPmNgNnS
politickou	politický	k2eAgFnSc7d1
nestabilitou	nestabilita	k1gFnSc7
způsobenou	způsobený	k2eAgFnSc7d1
rozdílnými	rozdílný	k2eAgInPc7d1
názory	názor	k1gInPc7
na	na	k7c4
sedmiletou	sedmiletý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
byl	být	k5eAaImAgMnS
Whigy	whig	k1gMnPc4
obviňován	obviňován	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
straní	stranit	k5eAaImIp3nS
Toryům	tory	k1gMnPc3
a	a	k8xC
je	být	k5eAaImIp3nS
autoritativní	autoritativní	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1762	#num#	k4
byl	být	k5eAaImAgMnS
úřadující	úřadující	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Pelham-Holles	Pelham-Holles	k1gMnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
Whigů	whig	k1gMnPc2
nahrazen	nahradit	k5eAaPmNgInS
lordem	lord	k1gMnSc7
Buttem	Butt	k1gMnSc7
ze	z	k7c2
strany	strana	k1gFnSc2
Toryů	tory	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
podpisu	podpis	k1gInSc6
mírové	mírový	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ukončila	ukončit	k5eAaPmAgFnS
sedmiletou	sedmiletý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
lord	lord	k1gMnSc1
Bute	Bute	k1gNnSc2
odstoupil	odstoupit	k5eAaPmAgMnS
a	a	k8xC
k	k	k7c3
moci	moc	k1gFnSc3
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
Whigové	whig	k1gMnPc1
vedení	vedení	k1gNnSc2
Georgem	Georg	k1gMnSc7
Grenvillem	Grenvill	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Královská	královský	k2eAgFnSc1d1
proklamace	proklamace	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1763	#num#	k4
stanovila	stanovit	k5eAaPmAgFnS
západní	západní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
pro	pro	k7c4
expanzi	expanze	k1gFnSc4
amerických	americký	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
dokument	dokument	k1gInSc1
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
s	s	k7c7
úmyslem	úmysl	k1gInSc7
přinutit	přinutit	k5eAaPmF
osadníky	osadník	k1gMnPc4
jednat	jednat	k5eAaImF
s	s	k7c7
rodilými	rodilý	k2eAgMnPc7d1
Američany	Američan	k1gMnPc7
a	a	k8xC
zmenšit	zmenšit	k5eAaPmF
náklady	náklad	k1gInPc4
na	na	k7c4
pohraniční	pohraniční	k2eAgInPc4d1
konflikty	konflikt	k1gInPc4
vznikající	vznikající	k2eAgInPc4d1
ze	z	k7c2
sporů	spor	k1gInPc2
o	o	k7c4
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proklamační	proklamační	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
se	se	k3xPyFc4
netýkala	týkat	k5eNaImAgFnS
většiny	většina	k1gFnSc2
farmářů	farmář	k1gMnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
využita	využít	k5eAaPmNgFnS
menšinou	menšina	k1gFnSc7
k	k	k7c3
vyvolání	vyvolání	k1gNnSc3
sporu	spor	k1gInSc2
mezi	mezi	k7c7
kolonisty	kolonista	k1gMnPc7
a	a	k8xC
britskou	britský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
američtí	americký	k2eAgMnPc1d1
kolonisté	kolonista	k1gMnPc1
všeobecně	všeobecně	k6eAd1
nepodléhali	podléhat	k5eNaImAgMnP
britským	britský	k2eAgFnPc3d1
daním	daň	k1gFnPc3
<g/>
,	,	kIx,
začalo	začít	k5eAaPmAgNnS
být	být	k5eAaImF
nákladné	nákladný	k2eAgNnSc1d1
financovat	financovat	k5eAaBmF
obranu	obrana	k1gFnSc4
kolonistů	kolonista	k1gMnPc2
proti	proti	k7c3
vzpourám	vzpoura	k1gFnPc3
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
nebo	nebo	k8xC
útoku	útok	k1gInSc2
Francouzů	Francouz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1765	#num#	k4
představil	představit	k5eAaPmAgInS
Greenwille	Greenwille	k1gFnSc2
kolkový	kolkový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
uvalil	uvalit	k5eAaPmAgInS
daň	daň	k1gFnSc4
na	na	k7c4
každý	každý	k3xTgInSc4
dokument	dokument	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
opatřen	opatřit	k5eAaPmNgInS
kolkem	kolek	k1gInSc7
v	v	k7c6
britských	britský	k2eAgFnPc6d1
koloniích	kolonie	k1gFnPc6
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
noviny	novina	k1gFnPc1
byly	být	k5eAaImAgFnP
tištěny	tištěn	k2eAgFnPc1d1
na	na	k7c4
papír	papír	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
touto	tento	k3xDgFnSc7
daní	daň	k1gFnSc7
zatížen	zatížit	k5eAaPmNgMnS
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
noviny	novina	k1gFnSc2
hlavní	hlavní	k2eAgFnSc7d1
hlásnou	hlásný	k2eAgFnSc7d1
troubou	trouba	k1gFnSc7
odporu	odpor	k1gInSc2
proti	proti	k7c3
této	tento	k3xDgFnSc3
dani	daň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
byl	být	k5eAaImAgMnS
nespokojen	spokojen	k2eNgMnSc1d1
s	s	k7c7
Grenvilleho	Grenvilleha	k1gFnSc5
úmyslem	úmysl	k1gInSc7
omezit	omezit	k5eAaPmF
královy	králův	k2eAgFnPc4d1
výsady	výsada	k1gFnPc4
<g/>
,	,	kIx,
odvolal	odvolat	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
a	a	k8xC
sestavením	sestavení	k1gNnSc7
vlády	vláda	k1gFnSc2
pověřil	pověřit	k5eAaPmAgMnS
Charlese	Charles	k1gMnSc4
Watson-Wentwortha	Watson-Wentworth	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Wentworth	Wentworth	k1gInSc1
s	s	k7c7
podporou	podpora	k1gFnSc7
Williama	William	k1gMnSc2
Pitta	Pitt	k1gMnSc2
a	a	k8xC
krále	král	k1gMnSc2
odvolal	odvolat	k5eAaPmAgInS
nepopulární	populární	k2eNgInSc1d1
kolkový	kolkový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
byla	být	k5eAaImAgFnS
slabá	slabý	k2eAgFnSc1d1
a	a	k8xC
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
vládou	vláda	k1gFnSc7
Pittovou	Pittový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daně	daň	k1gFnPc1
uvalené	uvalený	k2eAgFnPc1d1
na	na	k7c4
kolonie	kolonie	k1gFnPc4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
daně	daň	k1gFnSc2
na	na	k7c4
čaj	čaj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1773	#num#	k4
byla	být	k5eAaImAgFnS
loď	loď	k1gFnSc1
s	s	k7c7
nákladem	náklad	k1gInSc7
čaje	čaj	k1gInSc2
v	v	k7c6
Bostonském	bostonský	k2eAgInSc6d1
přístavu	přístav	k1gInSc6
přepadena	přepadnout	k5eAaPmNgFnS
kolonisty	kolonista	k1gMnPc7
a	a	k8xC
čaj	čaj	k1gInSc1
byl	být	k5eAaImAgInS
shozen	shodit	k5eAaPmNgInS
do	do	k7c2
moře	moře	k1gNnSc2
–	–	k?
tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
Bostonské	bostonský	k2eAgNnSc1d1
pití	pití	k1gNnSc1
čaje	čaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejné	veřejný	k2eAgInPc1d1
mínění	mínění	k1gNnSc4
v	v	k7c6
Británii	Británie	k1gFnSc6
se	se	k3xPyFc4
obrátilo	obrátit	k5eAaPmAgNnS
proti	proti	k7c3
kolonistům	kolonista	k1gMnPc3
a	a	k8xC
tato	tento	k3xDgFnSc1
akce	akce	k1gFnSc1
byla	být	k5eAaImAgFnS
označena	označit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
kriminální	kriminální	k2eAgInSc4d1
čin	čin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frederik	Frederik	k1gMnSc1
North	North	k1gMnSc1
s	s	k7c7
podporou	podpora	k1gFnSc7
parlamentu	parlament	k1gInSc2
vydal	vydat	k5eAaPmAgMnS
opatření	opatření	k1gNnSc4
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yIgMnPc3,k3yQgMnPc3
byl	být	k5eAaImAgInS
uzavřen	uzavřít	k5eAaPmNgInS
přístav	přístav	k1gInSc1
v	v	k7c6
Bostonu	Boston	k1gInSc6
a	a	k8xC
ústava	ústava	k1gFnSc1
Massachusetts	Massachusetts	k1gNnSc2
byla	být	k5eAaImAgFnS
upravena	upravit	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
členové	člen	k1gMnPc1
jeho	jeho	k3xOp3gFnSc2
horní	horní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
byli	být	k5eAaImAgMnP
jmenováni	jmenovat	k5eAaImNgMnP,k5eAaBmNgMnP
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
místo	místo	k7c2
volby	volba	k1gFnSc2
členy	člen	k1gMnPc4
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
</s>
<s>
Americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
vypukla	vypuknout	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
1775	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vojenskému	vojenský	k2eAgInSc3d1
střetu	střet	k1gInSc3
mezi	mezi	k7c7
britskou	britský	k2eAgFnSc7d1
pravidelnou	pravidelný	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
a	a	k8xC
kolonistickými	kolonistický	k2eAgMnPc7d1
ozbrojenci	ozbrojenec	k1gMnPc7
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
války	válka	k1gFnSc2
v	v	k7c6
červenci	červenec	k1gInSc6
1776	#num#	k4
kolonie	kolonie	k1gFnSc2
vyhlásily	vyhlásit	k5eAaPmAgInP
nezávislost	nezávislost	k1gFnSc4
na	na	k7c4
Británii	Británie	k1gFnSc4
a	a	k8xC
uvedly	uvést	k5eAaPmAgInP
výhrady	výhrada	k1gFnPc4
vůči	vůči	k7c3
králi	král	k1gMnSc3
<g/>
,	,	kIx,
zákonům	zákon	k1gInPc3
a	a	k8xC
britským	britský	k2eAgMnPc3d1
obyvatelům	obyvatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
New	New	k1gFnSc4
York	York	k1gInSc4
roku	rok	k1gInSc2
1776	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
strategický	strategický	k2eAgInSc4d1
plán	plán	k1gInSc4
na	na	k7c4
invazi	invaze	k1gFnSc4
z	z	k7c2
Kanady	Kanada	k1gFnSc2
selhal	selhat	k5eAaPmAgMnS
po	po	k7c4
kapitulaci	kapitulace	k1gFnSc4
vrchního	vrchní	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
britského	britský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
Johna	John	k1gMnSc2
Burgoyneho	Burgoyne	k1gMnSc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Saratogy	Saratoga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
je	být	k5eAaImIp3nS
často	často	k6eAd1
obviňován	obviňován	k2eAgMnSc1d1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
chtěl	chtít	k5eAaImAgMnS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
válce	válka	k1gFnSc6
proti	proti	k7c3
americkým	americký	k2eAgFnPc3d1
koloniím	kolonie	k1gFnPc3
navzdory	navzdory	k7c3
názorům	názor	k1gInPc3
svých	svůj	k3xOyFgMnPc2
ministrů	ministr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
současní	současný	k2eAgMnPc1d1
historici	historik	k1gMnPc1
hodnotí	hodnotit	k5eAaImIp3nP
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
postoj	postoj	k1gInSc4
komplexněji	komplexně	k6eAd2
v	v	k7c6
širších	široký	k2eAgFnPc6d2
souvislostech	souvislost	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
žádný	žádný	k3yNgMnSc1
panovník	panovník	k1gMnSc1
nehodlá	hodlat	k5eNaImIp3nS
ochotně	ochotně	k6eAd1
pozbýt	pozbýt	k5eAaPmF
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
území	území	k1gNnSc2
a	a	k8xC
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gNnSc1
chování	chování	k1gNnSc1
bylo	být	k5eAaImAgNnS
daleko	daleko	k6eAd1
méně	málo	k6eAd2
nemilosrdné	milosrdný	k2eNgNnSc1d1
<g/>
,	,	kIx,
než	než	k8xS
postoje	postoj	k1gInPc1
jiných	jiný	k2eAgMnPc2d1
monarchů	monarcha	k1gMnPc2
jeho	jeho	k3xOp3gFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Saratogy	Saratoga	k1gFnSc2
byly	být	k5eAaImAgInP
parlament	parlament	k1gInSc4
i	i	k8xC
veřejné	veřejný	k2eAgNnSc4d1
mínění	mínění	k1gNnSc4
nakloněny	nakloněn	k2eAgFnPc1d1
pokračování	pokračování	k1gNnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
v	v	k7c6
Americe	Amerika	k1gFnSc6
navrhl	navrhnout	k5eAaPmAgMnS
North	North	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vláda	vláda	k1gFnSc1
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
Pittových	Pittův	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
ho	on	k3xPp3gMnSc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
schopnějšího	schopný	k2eAgMnSc4d2
<g/>
,	,	kIx,
ale	ale	k8xC
Jiří	Jiří	k1gMnSc1
to	ten	k3xDgNnSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1778	#num#	k4
podepsala	podepsat	k5eAaPmAgFnS
Francie	Francie	k1gFnSc1
dohodu	dohoda	k1gFnSc4
o	o	k7c4
přátelství	přátelství	k1gNnSc4
s	s	k7c7
nezávislými	závislý	k2eNgFnPc7d1
americkými	americký	k2eAgFnPc7d1
koloniemi	kolonie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Británie	Británie	k1gFnSc1
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
ve	v	k7c6
válce	válka	k1gFnSc6
jak	jak	k6eAd1
s	s	k7c7
Francií	Francie	k1gFnSc7
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
se	s	k7c7
Španělskem	Španělsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lord	lord	k1gMnSc1
North	North	k1gMnSc1
znovu	znovu	k6eAd1
navrhl	navrhnout	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
rezignaci	rezignace	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Jiří	Jiří	k1gMnSc1
ji	on	k3xPp3gFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpor	odpor	k1gInSc1
proti	proti	k7c3
vzrůstajícím	vzrůstající	k2eAgInPc3d1
nákladům	náklad	k1gInPc3
narůstal	narůstat	k5eAaImAgInS
a	a	k8xC
vyvrcholil	vyvrcholit	k5eAaPmAgInS
pouličními	pouliční	k2eAgInPc7d1
nepokoji	nepokoj	k1gInPc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1781	#num#	k4
dorazila	dorazit	k5eAaPmAgFnS
do	do	k7c2
Londýna	Londýn	k1gInSc2
zpráva	zpráva	k1gFnSc1
o	o	k7c6
kapitulaci	kapitulace	k1gFnSc6
Yorktownu	Yorktown	k1gInSc2
a	a	k8xC
lord	lord	k1gMnSc1
North	North	k1gMnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gFnSc1
parlamentní	parlamentní	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
poklesla	poklesnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
rezignoval	rezignovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
akceptoval	akceptovat	k5eAaBmAgMnS
ztrátu	ztráta	k1gFnSc4
území	území	k1gNnSc3
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
souhlasil	souhlasit	k5eAaImAgMnS
se	s	k7c7
zahájením	zahájení	k1gNnSc7
mírových	mírový	k2eAgNnPc2d1
jednání	jednání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírová	mírový	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
uznávající	uznávající	k2eAgFnSc4d1
nezávislost	nezávislost	k1gFnSc4
amerických	americký	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
a	a	k8xC
návrat	návrat	k1gInSc1
Floridy	Florida	k1gFnSc2
Španělsku	Španělsko	k1gNnSc3
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
roku	rok	k1gInSc2
1783	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
William	William	k6eAd1
Pitt	Pitt	k2eAgInSc1d1
</s>
<s>
Pro	pro	k7c4
Jiřího	Jiří	k1gMnSc4
bylo	být	k5eAaImAgNnS
Pittovo	Pittův	k2eAgNnSc1d1
jmenování	jmenování	k1gNnSc1
velkým	velký	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokázal	dokázat	k5eAaPmAgMnS
prosadit	prosadit	k5eAaPmF
předsedu	předseda	k1gMnSc4
vlády	vláda	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
vlastního	vlastní	k2eAgNnSc2d1
přesvědčení	přesvědčení	k1gNnSc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
názor	názor	k1gInSc4
většiny	většina	k1gFnSc2
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
Pitt	Pitt	k1gInSc1
v	v	k7c6
úřadu	úřad	k1gInSc6
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
podporoval	podporovat	k5eAaImAgMnS
mnohé	mnohé	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
záměry	záměra	k1gFnSc2
a	a	k8xC
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
nových	nový	k2eAgMnPc2d1
peerů	peer	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
Pittovi	Pittův	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
zajistil	zajistit	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
tak	tak	k6eAd1
podporu	podpora	k1gFnSc4
ve	v	k7c6
Sněmovně	sněmovna	k1gFnSc6
lordů	lord	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
Pitt	Pitt	k1gInSc1
předsedou	předseda	k1gMnSc7
vlády	vláda	k1gFnSc2
a	a	k8xC
krátce	krátce	k6eAd1
poté	poté	k6eAd1
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
v	v	k7c6
Británii	Británie	k1gFnSc6
velmi	velmi	k6eAd1
populární	populární	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
Jiřího	Jiří	k1gMnSc4
zhoršil	zhoršit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Procházel	procházet	k5eAaImAgMnS
záchvaty	záchvat	k1gInPc7
duševní	duševní	k2eAgFnSc2d1
choroby	choroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
krátký	krátký	k2eAgInSc4d1
záchvat	záchvat	k1gInSc4
ho	on	k3xPp3gInSc4
zasáhl	zasáhnout	k5eAaPmAgInS
roku	rok	k1gInSc2
1765	#num#	k4
a	a	k8xC
druhý	druhý	k4xOgMnSc1
<g/>
,	,	kIx,
tentokráte	tentokráte	k?
delší	dlouhý	k2eAgFnSc4d2
<g/>
,	,	kIx,
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1788	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
konci	konec	k1gInSc6
období	období	k1gNnSc2
zasedání	zasedání	k1gNnSc2
parlamentu	parlament	k1gInSc2
se	se	k3xPyFc4
odejel	odejet	k5eAaPmAgMnS
léčit	léčit	k5eAaImF
do	do	k7c2
lázní	lázeň	k1gFnPc2
v	v	k7c6
Cheltenhamu	Cheltenham	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pitt	Pitt	k1gMnSc1
navrhoval	navrhovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
Jiřího	Jiří	k1gMnSc4
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
jmenován	jmenován	k2eAgMnSc1d1
regentem	regens	k1gMnSc7
a	a	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
překvapení	překvapení	k1gNnSc3
Fox	fox	k1gInSc1
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
právoplatným	právoplatný	k2eAgMnSc7d1
následníkem	následník	k1gMnSc7
s	s	k7c7
plnou	plný	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
parlamentu	parlament	k1gInSc6
vyřešily	vyřešit	k5eAaPmAgInP
spory	spor	k1gInPc1
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
návrh	návrh	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
realizován	realizovat	k5eAaBmNgInS
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
uzdravil	uzdravit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
a	a	k8xC
napoleonské	napoleonský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
Tři	tři	k4xCgMnPc1
nejmladší	mladý	k2eAgMnPc1d3
Jiřího	Jiří	k1gMnSc2
dcery	dcera	k1gFnSc2
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
byla	být	k5eAaImAgFnS
svržena	svržen	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
<g/>
,	,	kIx,
polekala	polekat	k5eAaPmAgFnS
mnoho	mnoho	k4c4
britských	britský	k2eAgMnPc2d1
velkostatkářů	velkostatkář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
vyhlásila	vyhlásit	k5eAaPmAgFnS
roku	rok	k1gInSc2
1793	#num#	k4
válku	válek	k1gInSc2
Velké	velký	k2eAgFnSc3d1
Británii	Británie	k1gFnSc3
a	a	k8xC
Jiří	Jiří	k1gMnSc1
povolil	povolit	k5eAaPmAgMnS
Pittovi	Pitt	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zvýšil	zvýšit	k5eAaPmAgMnS
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
koalice	koalice	k1gFnSc1
proti	proti	k7c3
Francii	Francie	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
Rakousko	Rakousko	k1gNnSc4
<g/>
,	,	kIx,
Prusko	Prusko	k1gNnSc4
a	a	k8xC
Španělsko	Španělsko	k1gNnSc4
<g/>
,	,	kIx,
ztroskotala	ztroskotat	k5eAaPmAgFnS
roku	rok	k1gInSc2
1795	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
Prusko	Prusko	k1gNnSc1
a	a	k8xC
Španělsko	Španělsko	k1gNnSc1
uzavřely	uzavřít	k5eAaPmAgInP
s	s	k7c7
Francií	Francie	k1gFnSc7
vlastní	vlastní	k2eAgFnSc2d1
mírové	mírový	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
sestávající	sestávající	k2eAgMnSc1d1
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
poražena	poražen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
stála	stát	k5eAaImAgFnS
mimo	mimo	k7c4
hlavní	hlavní	k2eAgInSc4d1
válečný	válečný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jiřího	Jiří	k1gMnSc2
portrét	portrét	k1gInSc1
z	z	k7c2
pozdního	pozdní	k2eAgNnSc2d1
období	období	k1gNnSc2
</s>
<s>
Krátký	krátký	k2eAgInSc1d1
mír	mír	k1gInSc1
dovolil	dovolit	k5eAaPmAgInS
Pittovi	Pitt	k1gMnSc3
soustředit	soustředit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
Irsko	Irsko	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
vzpoura	vzpoura	k1gFnSc1
a	a	k8xC
roku	rok	k1gInSc2
1798	#num#	k4
pokus	pokus	k1gInSc1
o	o	k7c4
vylodění	vylodění	k1gNnPc4
Francouzů	Francouz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
Irský	irský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
přijaly	přijmout	k5eAaPmAgInP
roku	rok	k1gInSc2
1800	#num#	k4
zákon	zákon	k1gInSc1
o	o	k7c4
sjednocení	sjednocení	k1gNnSc4
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1801	#num#	k4
spojilo	spojit	k5eAaPmAgNnS
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irsko	Irsko	k1gNnSc1
do	do	k7c2
jednoho	jeden	k4xCgInSc2
státu	stát	k1gInSc2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pitt	Pitt	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
odstranit	odstranit	k5eAaPmF
některá	některý	k3yIgNnPc1
právní	právní	k2eAgNnPc1d1
omezení	omezení	k1gNnPc1
vůči	vůči	k7c3
katolíkům	katolík	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
setkal	setkat	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
odporem	odpor	k1gInSc7
u	u	k7c2
krále	král	k1gMnSc2
i	i	k9
u	u	k7c2
britské	britský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
neúspěchu	neúspěch	k1gInSc6
chtěl	chtít	k5eAaImAgMnS
Pitt	Pitt	k1gMnSc1
rezignovat	rezignovat	k5eAaBmF
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c4
tutéž	týž	k3xTgFnSc4
dobu	doba	k1gFnSc4
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
zasažen	zasáhnout	k5eAaPmNgMnS
obnoveným	obnovený	k2eAgInSc7d1
záchvatem	záchvat	k1gInSc7
duševní	duševní	k2eAgFnSc2d1
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1801	#num#	k4
byl	být	k5eAaImAgInS
Pitt	Pitt	k1gInSc1
ve	v	k7c6
funkci	funkce	k1gFnSc6
formálně	formálně	k6eAd1
nahrazen	nahradit	k5eAaPmNgMnS
mluvčím	mluvčí	k1gMnSc7
dolní	dolní	k2eAgFnSc2d1
komory	komora	k1gFnSc2
parlamentu	parlament	k1gInSc2
Henrym	Henry	k1gMnSc7
Addingtonem	Addington	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Addington	Addington	k1gInSc1
zavrhl	zavrhnout	k5eAaPmAgInS
emancipaci	emancipace	k1gFnSc3
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
zrušil	zrušit	k5eAaPmAgMnS
daň	daň	k1gFnSc4
z	z	k7c2
příjmu	příjem	k1gInSc2
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
odzbrojení	odzbrojení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
1801	#num#	k4
uzavřel	uzavřít	k5eAaPmAgInS
s	s	k7c7
Francií	Francie	k1gFnSc7
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
považoval	považovat	k5eAaImAgMnS
mír	mír	k1gInSc4
s	s	k7c7
Francií	Francie	k1gFnSc7
za	za	k7c4
pouhý	pouhý	k2eAgInSc4d1
experiment	experiment	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1803	#num#	k4
se	se	k3xPyFc4
válečný	válečný	k2eAgInSc1d1
stav	stav	k1gInSc1
znovu	znovu	k6eAd1
obnovil	obnovit	k5eAaPmAgInS
a	a	k8xC
veřejné	veřejný	k2eAgNnSc1d1
mínění	mínění	k1gNnSc1
přinutilo	přinutit	k5eAaPmAgNnS
Addingtona	Addington	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
předal	předat	k5eAaPmAgMnS
vedení	vedení	k1gNnSc4
válečných	válečný	k2eAgFnPc2d1
operací	operace	k1gFnPc2
Pittovi	Pittův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc1
invaze	invaze	k1gFnSc2
Napoleona	Napoleon	k1gMnSc2
do	do	k7c2
Británie	Británie	k1gFnSc2
se	se	k3xPyFc4
ukazovala	ukazovat	k5eAaImAgFnS
být	být	k5eAaImF
reálnou	reálný	k2eAgFnSc4d1
a	a	k8xC
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
dobrovolníků	dobrovolník	k1gMnPc2
se	se	k3xPyFc4
hlásilo	hlásit	k5eAaImAgNnS
do	do	k7c2
sborů	sbor	k1gInPc2
na	na	k7c4
obranu	obrana	k1gFnSc4
země	zem	k1gFnSc2
proti	proti	k7c3
Francouzům	Francouz	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc1
vpádu	vpád	k1gInSc2
Francouzů	Francouz	k1gMnPc2
byla	být	k5eAaImAgFnS
odvrácena	odvrátit	k5eAaPmNgFnS
po	po	k7c6
slavném	slavný	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
britského	britský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
vedeného	vedený	k2eAgNnSc2d1
Horatiem	Horatius	k1gMnSc7
Nelsonem	Nelson	k1gMnSc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Trafalgaru	Trafalgar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1804	#num#	k4
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
znovu	znovu	k6eAd1
postižen	postihnout	k5eAaPmNgInS
záchvatem	záchvat	k1gInSc7
nemoci	nemoc	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
uzdravil	uzdravit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Addington	Addington	k1gInSc4
rezignoval	rezignovat	k5eAaBmAgMnS
a	a	k8xC
k	k	k7c3
moci	moc	k1gFnSc3
se	se	k3xPyFc4
znovu	znovu	k6eAd1
dostal	dostat	k5eAaPmAgMnS
Pitt	Pitt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
soustředil	soustředit	k5eAaPmAgMnS
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
koalice	koalice	k1gFnSc2
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
,	,	kIx,
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
Švédskem	Švédsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
třetí	třetí	k4xOgFnSc1
koalice	koalice	k1gFnSc1
dopadla	dopadnout	k5eAaPmAgFnS
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
obě	dva	k4xCgFnPc1
předchozí	předchozí	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neúspěch	neúspěch	k1gInSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
podepsal	podepsat	k5eAaPmAgMnS
na	na	k7c6
Pittově	Pittův	k2eAgNnSc6d1
zdraví	zdraví	k1gNnSc6
a	a	k8xC
ten	ten	k3xDgMnSc1
roku	rok	k1gInSc2
1806	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
lord	lord	k1gMnSc1
Greenvile	Greenvila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
na	na	k7c4
podporu	podpora	k1gFnSc4
najímání	najímání	k1gNnPc2
vojáků	voják	k1gMnPc2
vyhlásila	vyhlásit	k5eAaPmAgFnS
plán	plán	k1gInSc4
povolit	povolit	k5eAaPmF
katolíkům	katolík	k1gMnPc3
zastávat	zastávat	k5eAaImF
v	v	k7c6
armádě	armáda	k1gFnSc6
jakoukoli	jakýkoli	k3yIgFnSc4
důstojnickou	důstojnický	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
jim	on	k3xPp3gMnPc3
ale	ale	k9
nařídil	nařídit	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tento	tento	k3xDgInSc4
záměr	záměr	k1gInSc4
odvolali	odvolat	k5eAaPmAgMnP
a	a	k8xC
vyhlásili	vyhlásit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
takové	takový	k3xDgNnSc1
nařízení	nařízení	k1gNnSc1
nebude	být	k5eNaImBp3nS
přijato	přijmout	k5eAaPmNgNnS
ani	ani	k9
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
plán	plán	k1gInSc1
odvolala	odvolat	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
odmítla	odmítnout	k5eAaPmAgFnS
se	se	k3xPyFc4
zavázat	zavázat	k5eAaPmF
nepřijmout	přijmout	k5eNaPmF
takové	takový	k3xDgNnSc4
opatření	opatření	k1gNnSc4
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
ministry	ministr	k1gMnPc4
odvolal	odvolat	k5eAaPmAgMnS
a	a	k8xC
premiérem	premiér	k1gMnSc7
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
vévodu	vévoda	k1gMnSc4
z	z	k7c2
Portlandu	Portland	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejvlivnějším	vlivný	k2eAgMnSc7d3
člověkem	člověk	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
Spencer	Spencer	k1gMnSc1
Perceval	Perceval	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
byl	být	k5eAaImAgInS
rozpuštěn	rozpustit	k5eAaPmNgInS
a	a	k8xC
následné	následný	k2eAgFnPc1d1
volby	volba	k1gFnPc1
vytvořily	vytvořit	k5eAaPmAgFnP
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
parlamentu	parlament	k1gInSc2
pohodlnou	pohodlný	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
vzdorujícím	vzdorující	k2eAgMnSc7d1
ministrům	ministr	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
již	již	k9
pak	pak	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
své	svůj	k3xOyFgFnSc2
vlády	vláda	k1gFnSc2
neprovedl	provést	k5eNaPmAgInS
významnější	významný	k2eAgFnSc4d2
politickou	politický	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Roku	rok	k1gInSc2
1810	#num#	k4
byl	být	k5eAaImAgMnS
postižený	postižený	k1gMnSc1
revmatismem	revmatismus	k1gInSc7
a	a	k8xC
vážně	vážně	k6eAd1
onemocněl	onemocnět	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
stav	stav	k1gInSc1
se	se	k3xPyFc4
zhoršil	zhoršit	k5eAaPmAgInS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
zemřela	zemřít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
nejmladší	mladý	k2eAgFnSc1d3
a	a	k8xC
nejoblíbenější	oblíbený	k2eAgFnSc1d3
dcera	dcera	k1gFnSc1
Amélie	Amélie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1811	#num#	k4
souhlasil	souhlasit	k5eAaImAgInS
s	s	k7c7
vyhlášením	vyhlášení	k1gNnSc7
regentství	regentství	k1gNnSc2
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
regentem	regens	k1gMnSc7
do	do	k7c2
konce	konec	k1gInSc2
Jiřího	Jiří	k1gMnSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1811	#num#	k4
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
trvale	trvale	k6eAd1
ve	v	k7c6
stavu	stav	k1gInSc6
šílenství	šílenství	k1gNnSc2
a	a	k8xC
žil	žít	k5eAaImAgMnS
v	v	k7c6
odloučení	odloučení	k1gNnSc6
na	na	k7c6
hradu	hrad	k1gInSc6
Windsor	Windsor	k1gInSc1
až	až	k8xS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Perceval	Percevat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1812	#num#	k4
zavražděn	zavraždit	k5eAaPmNgMnS
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
britský	britský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
potkal	potkat	k5eAaPmAgInS
takový	takový	k3xDgInSc1
osud	osud	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Robert	Robert	k1gMnSc1
Banks	Banksa	k1gFnPc2
Jenkinson	Jenkinson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
vedl	vést	k5eAaImAgMnS
vládu	vláda	k1gFnSc4
v	v	k7c6
době	doba	k1gFnSc6
porážky	porážka	k1gFnSc2
Napoleona	Napoleon	k1gMnSc2
(	(	kIx(
<g/>
1813	#num#	k4
<g/>
,	,	kIx,
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následný	následný	k2eAgInSc1d1
Vídeňský	vídeňský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
(	(	kIx(
<g/>
1814	#num#	k4
<g/>
-	-	kIx~
<g/>
1815	#num#	k4
<g/>
)	)	kIx)
přinesl	přinést	k5eAaPmAgInS
velké	velký	k2eAgInPc4d1
územní	územní	k2eAgInPc4d1
zisky	zisk	k1gInPc4
pro	pro	k7c4
Hannover	Hannover	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
z	z	k7c2
kurfiřtství	kurfiřtství	k1gNnSc2
rozrostl	rozrůst	k5eAaPmAgInS
na	na	k7c6
království	království	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Jiřího	Jiří	k1gMnSc2
stav	stav	k1gInSc1
se	se	k3xPyFc4
stále	stále	k6eAd1
zhoršoval	zhoršovat	k5eAaImAgMnS
<g/>
,	,	kIx,
oslepl	oslepnout	k5eAaPmAgMnS
a	a	k8xC
ohluchl	ohluchnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
nedověděl	dovědět	k5eNaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hannoverským	hannoverský	k2eAgMnSc7d1
králem	král	k1gMnSc7
a	a	k8xC
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1818	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
období	období	k1gNnSc1
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgMnSc1d1
ani	ani	k8xC
chodit	chodit	k5eAaImF
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1820	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
také	také	k9
v	v	k7c6
kapli	kaple	k1gFnSc6
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
pohřben	pohřben	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gNnSc4
nástupci	nástupce	k1gMnPc1
byli	být	k5eAaImAgMnP
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
Jiří	Jiří	k1gMnSc2
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
však	však	k9
oba	dva	k4xCgMnPc1
zemřeli	zemřít	k5eAaPmAgMnP
bez	bez	k7c2
legitimních	legitimní	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnPc2
nástupkyní	nástupkyně	k1gFnPc2
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
vnučka	vnučka	k1gFnSc1
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
čtvrtého	čtvrtý	k4xOgNnSc2
syna	syn	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc7d1
příslušnicí	příslušnice	k1gFnSc7
hannoverské	hannoverský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
na	na	k7c6
britském	britský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
-	-	kIx~
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
August	August	k1gMnSc1
Brunšvicko-Lüneburský	Brunšvicko-Lüneburský	k2eAgMnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
</s>
<s>
Žofie	Žofie	k1gFnSc1
Hannoverská	hannoverský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Brunšvicko-Lüneburský	Brunšvicko-Lüneburský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Dorotea	Dorote	k1gInSc2
z	z	k7c2
Celle	Celle	k1gFnSc2
</s>
<s>
Éléonore	Éléonor	k1gMnSc5
Desmier	Desmier	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Olbreuse	Olbreuse	k1gFnSc1
</s>
<s>
Frederik	Frederik	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Braniborsko-Ansbašský	Braniborsko-Ansbašský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Braniborsko-Ansbašský	Braniborsko-Ansbašský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Markéta	Markéta	k1gFnSc1
Oettingenská	Oettingenský	k2eAgFnSc1d1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
z	z	k7c2
Ansbachu	Ansbach	k1gInSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Jiří	Jiří	k1gMnSc2
I.	I.	kA
Sasko-Eisenašský	Sasko-Eisenašský	k2eAgInSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Sasko-Eisenašská	Sasko-Eisenašský	k2eAgFnSc1d1
</s>
<s>
Johana	Johana	k1gFnSc1
Saynsko-Wittgensteinská	Saynsko-Wittgensteinský	k2eAgFnSc1d1
</s>
<s>
'	'	kIx"
<g/>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
'	'	kIx"
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Gothajský	sasko-gothajský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc4d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Sasko-Altenburská	Sasko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
August	August	k1gMnSc1
Sasko-Weissenfelský	Sasko-Weissenfelský	k2eAgMnSc1d1
</s>
<s>
Magdaléna	Magdaléna	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
Meklenbursko-Schwerinská	Meklenbursko-Schwerinský	k2eAgFnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Sasko-Gothajská	sasko-gothajský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anhaltsko-Zerbstský	Anhaltsko-Zerbstský	k2eAgInSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Anhaltsko-Zerbstský	Anhaltsko-Zerbstský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Augusta	August	k1gMnSc2
Holštýnsko-Gottorpská	Holštýnsko-Gottorpský	k2eAgNnPc5d1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Augusta	August	k1gMnSc2
Anhaltsko-Zerbstská	Anhaltsko-Zerbstský	k2eAgNnPc1d1
</s>
<s>
August	August	k1gMnSc1
Sasko-Weissenfelský	Sasko-Weissenfelský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
Meklenbursko-Schwerinská	Meklenbursko-Schwerinský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
George	Georg	k1gMnSc2
III	III	kA
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
http://www.thepeerage.com/p10078.htm#i100777	http://www.thepeerage.com/p10078.htm#i100777	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700822	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118716913	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0898	#num#	k4
6255	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79061402	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500009904	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
49264990	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79061402	#num#	k4
</s>
