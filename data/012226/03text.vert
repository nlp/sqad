<p>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
Mars	Mars	k1gMnSc1	Mars
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Peter	Peter	k1gMnSc1	Peter
Gene	gen	k1gInSc5	gen
Hernandez	Hernandez	k1gInSc4	Hernandez
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1985	[number]	k4	1985
Waikiki	Waikiki	k1gNnPc2	Waikiki
<g/>
,	,	kIx,	,
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Waikiki	Waikik	k1gFnSc2	Waikik
(	(	kIx(	(
<g/>
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jak	jak	k8xS	jak
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
hity	hit	k1gInPc4	hit
jako	jako	k8xC	jako
Just	just	k6eAd1	just
the	the	k?	the
Way	Way	k1gMnSc1	Way
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
nebo	nebo	k8xC	nebo
Grenade	Grenad	k1gInSc5	Grenad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
featuringy	featuring	k1gInPc4	featuring
na	na	k7c6	na
písních	píseň	k1gFnPc6	píseň
"	"	kIx"	"
<g/>
Nothin	Nothin	k1gInSc4	Nothin
<g/>
'	'	kIx"	'
on	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
od	od	k7c2	od
rappera	rappero	k1gNnSc2	rappero
B.o.B	B.o.B	k1gFnSc2	B.o.B
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Billionaire	Billionair	k1gInSc5	Billionair
<g/>
"	"	kIx"	"
od	od	k7c2	od
Travieho	Travie	k1gMnSc2	Travie
McCoye	McCoy	k1gMnSc2	McCoy
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
producent	producent	k1gMnSc1	producent
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
hitů	hit	k1gInPc2	hit
"	"	kIx"	"
<g/>
Right	Right	k2eAgInSc1d1	Right
Round	round	k1gInSc1	round
<g/>
"	"	kIx"	"
od	od	k7c2	od
Flo	Flo	k1gFnSc2	Flo
Ridy	Rida	k1gFnSc2	Rida
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Wavin	Wavin	k1gInSc1	Wavin
<g/>
'	'	kIx"	'
Flag	flag	k1gInSc1	flag
<g/>
"	"	kIx"	"
od	od	k7c2	od
K	K	kA	K
<g/>
'	'	kIx"	'
<g/>
naan	naan	k1gMnSc1	naan
a	a	k8xC	a
"	"	kIx"	"
<g/>
Fuck	Fuck	k1gMnSc1	Fuck
You	You	k1gMnSc1	You
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
od	od	k7c2	od
CeeLo	CeeLo	k6eAd1	CeeLo
Green	Greno	k1gNnPc2	Greno
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgMnS	vydat
svoje	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Doo-Wops	Doo-Wops	k1gInSc1	Doo-Wops
&	&	k?	&
Hooligans	Hooligans	k1gInSc1	Hooligans
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Začátky	začátek	k1gInPc1	začátek
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
Peterovi	Peter	k1gMnSc3	Peter
a	a	k8xC	a
Bernadette	Bernadett	k1gInSc5	Bernadett
"	"	kIx"	"
<g/>
Bernie	Bernie	k1gFnSc2	Bernie
<g/>
"	"	kIx"	"
Hernandezovým	Hernandezová	k1gFnPc3	Hernandezová
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
předci	předek	k1gMnPc1	předek
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Portorika	Portorico	k1gNnSc2	Portorico
a	a	k8xC	a
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
dětí	dítě	k1gFnPc2	dítě
této	tento	k3xDgFnSc2	tento
hudební	hudební	k2eAgFnSc2d1	hudební
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
muzikant	muzikant	k1gMnSc1	muzikant
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
tanečnice	tanečnice	k1gFnPc4	tanečnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
mu	on	k3xPp3gMnSc3	on
jeho	on	k3xPp3gInSc4	on
otec	otec	k1gMnSc1	otec
říkat	říkat	k5eAaImF	říkat
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
amerického	americký	k2eAgMnSc2d1	americký
wrestlingového	wrestlingový	k2eAgMnSc2d1	wrestlingový
zápasníka	zápasník	k1gMnSc2	zápasník
Bruna	Bruno	k1gMnSc2	Bruno
Sammartina	Sammartin	k1gMnSc2	Sammartin
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
buclaté	buclatý	k2eAgFnPc4d1	buclatá
tváře	tvář	k1gFnPc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gInSc7	jeho
vzorem	vzor	k1gInSc7	vzor
takoví	takový	k3xDgMnPc1	takový
zpěváci	zpěvák	k1gMnPc1	zpěvák
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
nebo	nebo	k8xC	nebo
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
MidWekk	MidWekk	k1gInSc1	MidWekk
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Malý	malý	k2eAgMnSc1d1	malý
Elvis	Elvis	k1gMnSc1	Elvis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
President	president	k1gMnSc1	president
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
jako	jako	k9	jako
sedmnáctiletý	sedmnáctiletý	k2eAgMnSc1d1	sedmnáctiletý
mladík	mladík	k1gMnSc1	mladík
do	do	k7c2	do
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaImF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
hudební	hudební	k2eAgFnSc3d1	hudební
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
zpíval	zpívat	k5eAaImAgMnS	zpívat
v	v	k7c4	v
Motown	Motown	k1gNnSc4	Motown
Recods	Recodsa	k1gFnPc2	Recodsa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k9	ale
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
nevedlo	vést	k5eNaImAgNnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tyto	tento	k3xDgFnPc1	tento
zkušenosti	zkušenost	k1gFnPc1	zkušenost
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
být	být	k5eAaImF	být
prospěšné	prospěšný	k2eAgFnPc1d1	prospěšná
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
skladatelem	skladatel	k1gMnSc7	skladatel
a	a	k8xC	a
producentem	producent	k1gMnSc7	producent
Philipem	Philip	k1gMnSc7	Philip
Lawrencem	Lawrenec	k1gMnSc7	Lawrenec
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
spolu	spolu	k6eAd1	spolu
a	a	k8xC	a
s	s	k7c7	s
Arim	Ari	k1gNnSc7	Ari
Levinem	Levin	k1gInSc7	Levin
psali	psát	k5eAaImAgMnP	psát
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
produkční	produkční	k2eAgInSc4d1	produkční
tým	tým	k1gInSc4	tým
The	The	k1gFnSc2	The
Smeezingtons	Smeezingtonsa	k1gFnPc2	Smeezingtonsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
jej	on	k3xPp3gInSc2	on
Lawrence	Lawrenec	k1gInSc2	Lawrenec
představil	představit	k5eAaPmAgInS	představit
jeho	jeho	k3xOp3gMnSc3	jeho
budoucímu	budoucí	k2eAgMnSc3d1	budoucí
manažerovi	manažer	k1gMnSc3	manažer
z	z	k7c2	z
Atlantic	Atlantice	k1gFnPc2	Atlantice
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
Aaronu	Aaron	k1gMnSc6	Aaron
Bay-Schuckovi	Bay-Schucek	k1gMnSc6	Bay-Schucek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ho	on	k3xPp3gNnSc4	on
slyšel	slyšet	k5eAaImAgInS	slyšet
zpívat	zpívat	k5eAaImF	zpívat
pár	pár	k4xCyI	pár
písniček	písnička	k1gFnPc2	písnička
s	s	k7c7	s
kytarou	kytara	k1gFnSc7	kytara
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
okamžitě	okamžitě	k6eAd1	okamžitě
podepsat	podepsat	k5eAaPmF	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
trvalo	trvat	k5eAaImAgNnS	trvat
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
Bay-Schuck	Bay-Schuck	k1gMnSc1	Bay-Schuck
najal	najmout	k5eAaPmAgMnS	najmout
a	a	k8xC	a
The	The	k1gMnSc1	The
Smeezingtons	Smeezingtons	k1gInSc4	Smeezingtons
k	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
a	a	k8xC	a
produkování	produkování	k1gNnSc3	produkování
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
již	již	k6eAd1	již
pro	pro	k7c4	pro
Atlantic	Atlantice	k1gFnPc2	Atlantice
Records	Records	k1gInSc4	Records
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Bay-Schucka	Bay-Schucko	k1gNnSc2	Bay-Schucko
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
HitQuarters	HitQuarters	k1gInSc4	HitQuarters
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gInSc1	jeho
konečný	konečný	k2eAgInSc1d1	konečný
cíl	cíl	k1gInSc1	cíl
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
sólový	sólový	k2eAgMnSc1d1	sólový
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
psát	psát	k5eAaImF	psát
a	a	k8xC	a
produkovat	produkovat	k5eAaImF	produkovat
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
jeho	on	k3xPp3gNnSc2	on
psaní	psaní	k1gNnSc2	psaní
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
objevit	objevit	k5eAaPmF	objevit
sama	sám	k3xTgMnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
sólový	sólový	k2eAgMnSc1d1	sólový
umělec	umělec	k1gMnSc1	umělec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
prvního	první	k4xOgInSc2	první
úspěchu	úspěch	k1gInSc2	úspěch
jako	jako	k9	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
písní	píseň	k1gFnPc2	píseň
od	od	k7c2	od
B.o.B	B.o.B	k1gFnSc2	B.o.B
"	"	kIx"	"
<g/>
Nothin	Nothin	k1gMnSc1	Nothin
<g/>
'	'	kIx"	'
on	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
a	a	k8xC	a
Travieho	Travie	k1gMnSc2	Travie
McCoye	McCoy	k1gMnSc2	McCoy
"	"	kIx"	"
<g/>
Billionaire	Billionair	k1gInSc5	Billionair
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
top	topit	k5eAaImRp2nS	topit
ten	ten	k3xDgInSc4	ten
nemála	nemála	k1gFnSc1	nemála
světových	světový	k2eAgFnPc2d1	světová
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Mars	Mars	k1gInSc1	Mars
vydává	vydávat	k5eAaPmIp3nS	vydávat
svůj	svůj	k3xOyFgInSc4	svůj
EP	EP	kA	EP
debut	debut	k1gInSc4	debut
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
It	It	k1gFnSc7	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Better	Better	k1gMnSc1	Better
If	If	k1gMnSc7	If
You	You	k1gMnSc1	You
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Understand	Understand	k1gInSc1	Understand
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
vzešla	vzejít	k5eAaPmAgFnS	vzejít
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Other	Other	k1gMnSc1	Other
Side	Side	k1gInSc1	Side
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
hostovali	hostovat	k5eAaImAgMnP	hostovat
zpěváci	zpěvák	k1gMnPc1	zpěvák
CeeLo	CeeLo	k6eAd1	CeeLo
Green	Green	k1gInSc4	Green
a	a	k8xC	a
B.o.B.	B.o.B.	k1gFnSc4	B.o.B.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
Mars	Mars	k1gInSc1	Mars
vydává	vydávat	k5eAaPmIp3nS	vydávat
své	svůj	k3xOyFgFnSc3	svůj
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Doo-Wops	Doo-Wopsa	k1gFnPc2	Doo-Wopsa
&	&	k?	&
Hooligans	Hooligansa	k1gFnPc2	Hooligansa
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
the	the	k?	the
Way	Way	k1gMnSc1	Way
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
<g/>
"	"	kIx"	"
vyšel	vyjít	k5eAaPmAgInS	vyjít
již	již	k6eAd1	již
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
top	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Grenade	Grenad	k1gInSc5	Grenad
<g/>
"	"	kIx"	"
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
také	také	k9	také
významný	významný	k2eAgInSc1d1	významný
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
55	[number]	k4	55
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
dočkala	dočkat	k5eAaPmAgFnS	dočkat
se	se	k3xPyFc4	se
i	i	k9	i
velice	velice	k6eAd1	velice
pozitivní	pozitivní	k2eAgMnPc4d1	pozitivní
kritiky	kritik	k1gMnPc4	kritik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
Mars	Mars	k1gMnSc1	Mars
zatčen	zatknout	k5eAaPmNgMnS	zatknout
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
za	za	k7c2	za
držení	držení	k1gNnSc2	držení
kokainu	kokain	k1gInSc2	kokain
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nepříjemná	příjemný	k2eNgFnSc1d1	nepříjemná
událost	událost	k1gFnSc1	událost
ho	on	k3xPp3gNnSc4	on
stála	stát	k5eAaImAgFnS	stát
kauci	kauce	k1gFnSc4	kauce
2	[number]	k4	2
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
200	[number]	k4	200
hodin	hodina	k1gFnPc2	hodina
veřejně	veřejně	k6eAd1	veřejně
prospěšných	prospěšný	k2eAgFnPc2d1	prospěšná
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
příslibem	příslib	k1gInSc7	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejméně	málo	k6eAd3	málo
rok	rok	k1gInSc1	rok
nespáchá	spáchat	k5eNaPmIp3nS	spáchat
podobný	podobný	k2eAgInSc1d1	podobný
zločin	zločin	k1gInSc1	zločin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Marse	Mars	k1gInSc5	Mars
ve	v	k7c4	v
znamení	znamení	k1gNnSc4	znamení
úspěchů	úspěch	k1gInPc2	úspěch
–	–	k?	–
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
celkových	celkový	k2eAgFnPc2d1	celková
šesti	šest	k4xCc2	šest
nominací	nominace	k1gFnPc2	nominace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
vypuštění	vypuštění	k1gNnSc2	vypuštění
nové	nový	k2eAgFnSc2d1	nová
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
It	It	k1gMnSc1	It
Will	Will	k1gMnSc1	Will
Rain	Rain	k1gMnSc1	Rain
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Twilight	Twilighta	k1gFnPc2	Twilighta
sága	sága	k1gFnSc1	sága
<g/>
:	:	kIx,	:
Rozbřesk	rozbřesk	k1gInSc1	rozbřesk
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
54	[number]	k4	54
<g/>
.	.	kIx.	.
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
nominován	nominovat	k5eAaBmNgMnS	nominovat
hned	hned	k6eAd1	hned
v	v	k7c6	v
několika	několik	k4yIc6	několik
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
,	,	kIx,	,
sošku	soška	k1gFnSc4	soška
si	se	k3xPyFc3	se
ale	ale	k9	ale
neodnesl	odnést	k5eNaPmAgMnS	odnést
ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgFnPc4	všechen
převzala	převzít	k5eAaPmAgFnS	převzít
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Adele	Adele	k1gFnSc1	Adele
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
očekávaného	očekávaný	k2eAgNnSc2d1	očekávané
alba	album	k1gNnSc2	album
Unorthodox	Unorthodox	k1gInSc1	Unorthodox
Jukebox	Jukebox	k1gInSc1	Jukebox
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
Locked	Locked	k1gInSc1	Locked
Out	Out	k1gFnSc2	Out
of	of	k?	of
Heaven	Heavna	k1gFnPc2	Heavna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
v	v	k7c6	v
USA	USA	kA	USA
album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
přesahujícím	přesahující	k2eAgInSc7d1	přesahující
187	[number]	k4	187
000	[number]	k4	000
a	a	k8xC	a
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
stalo	stát	k5eAaPmAgNnS	stát
nejrychleji	rychle	k6eAd3	rychle
prodávaným	prodávaný	k2eAgNnSc7d1	prodávané
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dočkal	dočkat	k5eAaPmAgInS	dočkat
nominace	nominace	k1gFnSc2	nominace
v	v	k7c6	v
již	již	k6eAd1	již
55	[number]	k4	55
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
časopis	časopis	k1gInSc1	časopis
Billboard	billboard	k1gInSc1	billboard
Bruna	Bruno	k1gMnSc2	Bruno
Marse	Mars	k1gInSc5	Mars
umělcem	umělec	k1gMnSc7	umělec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2013	[number]	k4	2013
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
světové	světový	k2eAgInPc4d1	světový
tour	tour	k1gInSc4	tour
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
USA	USA	kA	USA
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Moonshine	Moonshin	k1gInSc5	Moonshin
Jungle	Jungle	k1gNnSc3	Jungle
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Přiveze	přivézt	k5eAaPmIp3nS	přivézt
s	s	k7c7	s
sebou	se	k3xPyFc7	se
i	i	k8xC	i
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Ellie	Ellie	k1gFnSc2	Ellie
Goulding	Goulding	k1gInSc1	Goulding
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
navštíví	navštívit	k5eAaPmIp3nS	navštívit
i	i	k9	i
Pražskou	pražský	k2eAgFnSc4d1	Pražská
O2	O2	k1gFnSc4	O2
arénu	aréna	k1gFnSc4	aréna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
singly	singl	k1gInPc1	singl
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
the	the	k?	the
Way	Way	k1gMnSc1	Way
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
"	"	kIx"	"
<g/>
Grenade	Grenad	k1gInSc5	Grenad
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Lazy	Lazy	k?	Lazy
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
"	"	kIx"	"
<g/>
Marry	Marra	k1gMnSc2	Marra
You	You	k1gMnSc2	You
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
"	"	kIx"	"
<g/>
It	It	k1gMnSc1	It
Will	Will	k1gMnSc1	Will
Rain	Rain	k1gMnSc1	Rain
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
"	"	kIx"	"
<g/>
Count	Count	k1gInSc4	Count
on	on	k3xPp3gMnSc1	on
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
"	"	kIx"	"
<g/>
Locked	Locked	k1gMnSc1	Locked
Out	Out	k1gMnSc1	Out
of	of	k?	of
Heaven	Heavno	k1gNnPc2	Heavno
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
"	"	kIx"	"
<g/>
When	When	k1gMnSc1	When
I	i	k8xC	i
Was	Was	k1gMnSc1	Was
Your	Your	k1gMnSc1	Your
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
"	"	kIx"	"
<g/>
Treasure	Treasur	k1gMnSc5	Treasur
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
"	"	kIx"	"
<g/>
Uptown	Uptown	k1gInSc1	Uptown
Funk	funk	k1gInSc1	funk
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
"	"	kIx"	"
<g/>
24	[number]	k4	24
<g/>
k	k	k7c3	k
Magic	Magic	k1gMnSc1	Magic
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
"	"	kIx"	"
<g/>
That	That	k1gMnSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
What	Whata	k1gFnPc2	Whata
I	I	kA	I
Like	Lik	k1gFnSc2	Lik
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bruno	Bruno	k1gMnSc1	Bruno
Mars	Mars	k1gMnSc1	Mars
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
