<p>
<s>
Sodomka	Sodomka	k1gFnSc1	Sodomka
PRK	prk	k1gInSc1	prk
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
model	model	k1gInSc4	model
autobusového	autobusový	k2eAgInSc2d1	autobusový
přívěsu	přívěs	k1gInSc2	přívěs
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kuse	kus	k1gInSc6	kus
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
firma	firma	k1gFnSc1	firma
Sodomka	Sodomka	k1gFnSc1	Sodomka
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc1d2	pozdější
Karosa	karosa	k1gFnSc1	karosa
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
Mýtě	mýto	k1gNnSc6	mýto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Sodomka	Sodomka	k1gFnSc1	Sodomka
PRK	prk	k1gInSc1	prk
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
dvounápravový	dvounápravový	k2eAgInSc1d1	dvounápravový
autobusový	autobusový	k2eAgInSc1d1	autobusový
vlečný	vlečný	k2eAgInSc1d1	vlečný
vůz	vůz	k1gInSc1	vůz
(	(	kIx(	(
<g/>
přívěs	přívěs	k1gInSc1	přívěs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odvozen	odvozen	k2eAgMnSc1d1	odvozen
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
většího	veliký	k2eAgInSc2d2	veliký
a	a	k8xC	a
kapacitnějšího	kapacitný	k2eAgInSc2d2	kapacitný
modelu	model	k1gInSc2	model
PRK	prk	k1gInSc1	prk
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
PRK	prk	k1gInSc1	prk
4	[number]	k4	4
měl	mít	k5eAaImAgInS	mít
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
samonosnou	samonosný	k2eAgFnSc4d1	samonosná
karoserii	karoserie	k1gFnSc4	karoserie
<g/>
,	,	kIx,	,
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
oplechovanou	oplechovaný	k2eAgFnSc4d1	oplechovaná
<g/>
,	,	kIx,	,
s	s	k7c7	s
ocelovými	ocelový	k2eAgFnPc7d1	ocelová
výztuhami	výztuha	k1gFnPc7	výztuha
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
i	i	k8xC	i
přední	přední	k2eAgFnSc1d1	přední
náprava	náprava	k1gFnSc1	náprava
měly	mít	k5eAaImAgFnP	mít
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
montáž	montáž	k1gFnSc4	montáž
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgNnPc1	čtyři
kola	kolo	k1gNnPc1	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnSc1d1	přední
byla	být	k5eAaImAgFnS	být
řiditelná	řiditelný	k2eAgFnSc1d1	řiditelná
<g/>
.	.	kIx.	.
</s>
<s>
Sedačky	sedačka	k1gFnPc1	sedačka
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
byly	být	k5eAaImAgFnP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
podélně	podélně	k6eAd1	podélně
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
čelech	čelo	k1gNnPc6	čelo
vozu	vůz	k1gInSc2	vůz
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
vždy	vždy	k6eAd1	vždy
jedna	jeden	k4xCgFnSc1	jeden
sedačka	sedačka	k1gFnSc1	sedačka
(	(	kIx(	(
<g/>
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
po	po	k7c6	po
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
v	v	k7c6	v
předním	přední	k2eAgNnSc6d1	přední
čele	čelo	k1gNnSc6	čelo
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
jízdy	jízda	k1gFnSc2	jízda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
a	a	k8xC	a
nástup	nástup	k1gInSc1	nástup
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgInP	určit
jedny	jeden	k4xCgFnPc1	jeden
<g/>
,	,	kIx,	,
ručně	ručně	k6eAd1	ručně
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bouchací	bouchací	k2eAgFnSc1d1	bouchací
(	(	kIx(	(
<g/>
na	na	k7c4	na
kliku	klika	k1gFnSc4	klika
<g/>
)	)	kIx)	)
dveře	dveře	k1gFnPc1	dveře
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
bočnici	bočnice	k1gFnSc6	bočnice
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
uprostřed	uprostřed	k7c2	uprostřed
délky	délka	k1gFnSc2	délka
vozu	vůz	k1gInSc2	vůz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
4770	[number]	k4	4770
mm	mm	kA	mm
</s>
</p>
<p>
<s>
Šířka	šířka	k1gFnSc1	šířka
<g/>
:	:	kIx,	:
?	?	kIx.	?
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
?	?	kIx.	?
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
prázdného	prázdný	k2eAgInSc2d1	prázdný
vozu	vůz	k1gInSc2	vůz
<g/>
:	:	kIx,	:
?	?	kIx.	?
</s>
</p>
<p>
<s>
Místa	místo	k1gNnSc2	místo
celkem	celek	k1gInSc7	celek
<g/>
:	:	kIx,	:
?	?	kIx.	?
</s>
</p>
<p>
<s>
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
:	:	kIx,	:
18	[number]	k4	18
</s>
</p>
<p>
<s>
ke	k	k7c3	k
stání	stání	k1gNnSc3	stání
<g/>
:	:	kIx,	:
?	?	kIx.	?
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
==	==	k?	==
</s>
</p>
<p>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
výroby	výroba	k1gFnSc2	výroba
přívěsu	přívěs	k1gInSc2	přívěs
PRK	prk	k1gInSc1	prk
4	[number]	k4	4
(	(	kIx(	(
<g/>
význam	význam	k1gInSc1	význam
zkratky	zkratka	k1gFnSc2	zkratka
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
číslice	číslice	k1gFnSc1	číslice
označuje	označovat	k5eAaImIp3nS	označovat
počet	počet	k1gInSc1	počet
kol	kolo	k1gNnPc2	kolo
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
přichystáno	přichystat	k5eAaPmNgNnS	přichystat
na	na	k7c4	na
jaro	jaro	k1gNnSc4	jaro
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
událostem	událost	k1gFnPc3	událost
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
ale	ale	k8xC	ale
k	k	k7c3	k
sériové	sériový	k2eAgFnSc3d1	sériová
produkci	produkce	k1gFnSc3	produkce
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
(	(	kIx(	(
<g/>
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
PRK	prk	k1gInSc4	prk
4	[number]	k4	4
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
odvozený	odvozený	k2eAgInSc1d1	odvozený
model	model	k1gInSc1	model
DM	dm	kA	dm
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
prototyp	prototyp	k1gInSc1	prototyp
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vozidlo	vozidlo	k1gNnSc1	vozidlo
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
Českomoravským	českomoravský	k2eAgFnPc3d1	Českomoravská
protektorátním	protektorátní	k2eAgFnPc3d1	protektorátní
drahám	draha	k1gFnPc3	draha
(	(	kIx(	(
<g/>
ČMD-BMB	ČMD-BMB	k1gFnSc1	ČMD-BMB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jej	on	k3xPp3gInSc4	on
provozovalo	provozovat	k5eAaImAgNnS	provozovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
a	a	k8xC	a
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
Mýta	mýto	k1gNnSc2	mýto
s	s	k7c7	s
autobusem	autobus	k1gInSc7	autobus
Praga	Prag	k1gMnSc2	Prag
RN	RN	kA	RN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HARÁK	HARÁK	kA	HARÁK
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
československých	československý	k2eAgInPc2d1	československý
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
trolejbusů	trolejbus	k1gInPc2	trolejbus
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Corona	Corona	k1gFnSc1	Corona
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86116	[number]	k4	86116
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
