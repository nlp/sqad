<s>
Zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
-	-	kIx~	-
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
rekonstruuje	rekonstruovat	k5eAaBmIp3nS	rekonstruovat
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
srovnáváním	srovnávání	k1gNnSc7	srovnávání
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
(	(	kIx(	(
<g/>
i	i	k8xC	i
neslovanských	slovanský	k2eNgInPc2d1	neslovanský
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
-	-	kIx~	-
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
všechny	všechen	k3xTgInPc1	všechen
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
už	už	k6eAd1	už
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
vývojových	vývojový	k2eAgFnPc2d1	vývojová
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
vycházejí	vycházet	k5eAaImIp3nP	vycházet
<g/>
:	:	kIx,	:
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
jihoslovanské	jihoslovanský	k2eAgFnSc6d1	Jihoslovanská
<g/>
.	.	kIx.	.
</s>
