<p>
<s>
Genocida	genocida	k1gFnSc1	genocida
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
genocidium	genocidium	k1gNnSc1	genocidium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zločin	zločin	k1gInSc1	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
definovaný	definovaný	k2eAgInSc1d1	definovaný
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
trestním	trestní	k2eAgNnSc7d1	trestní
právem	právo	k1gNnSc7	právo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
úmyslné	úmyslný	k2eAgNnSc1d1	úmyslné
a	a	k8xC	a
systematické	systematický	k2eAgNnSc1d1	systematické
zničení	zničení	k1gNnSc1	zničení
<g/>
,	,	kIx,	,
celé	celá	k1gFnPc1	celá
nebo	nebo	k8xC	nebo
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
etnické	etnický	k2eAgFnPc1d1	etnická
<g/>
,	,	kIx,	,
rasové	rasový	k2eAgFnPc1d1	rasová
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgFnPc1d1	náboženská
nebo	nebo	k8xC	nebo
národnostní	národnostní	k2eAgFnPc1d1	národnostní
skupiny	skupina	k1gFnPc1	skupina
<g/>
"	"	kIx"	"
ačkoliv	ačkoliv	k8xS	ačkoliv
co	co	k3yRnSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
část	část	k1gFnSc1	část
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
debaty	debata	k1gFnSc2	debata
právníků	právník	k1gMnPc2	právník
<g/>
.	.	kIx.	.
</s>
<s>
Genocidu	genocida	k1gFnSc4	genocida
definuje	definovat	k5eAaBmIp3nS	definovat
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c4	o
zabránění	zabránění	k1gNnSc4	zabránění
a	a	k8xC	a
trestání	trestání	k1gNnSc4	trestání
zločinu	zločin	k1gInSc2	zločin
genocidia	genocidium	k1gNnSc2	genocidium
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
genocida	genocida	k1gFnSc1	genocida
<g/>
"	"	kIx"	"
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Raphael	Raphael	k1gMnSc1	Raphael
Lemkin	Lemkin	k1gMnSc1	Lemkin
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
polsko-židovského	polsko-židovský	k2eAgInSc2d1	polsko-židovský
původu	původ	k1gInSc2	původ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
kořenu	kořen	k1gInSc2	kořen
slova	slovo	k1gNnSc2	slovo
génos	génosa	k1gFnPc2	génosa
(	(	kIx(	(
<g/>
γ	γ	k?	γ
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
narození	narození	k1gNnSc1	narození
<g/>
,	,	kIx,	,
rasa	rasa	k1gFnSc1	rasa
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
a	a	k8xC	a
latinské	latinský	k2eAgFnSc2d1	Latinská
přípony	přípona	k1gFnSc2	přípona
-	-	kIx~	-
<g/>
cidium	cidium	k1gNnSc4	cidium
(	(	kIx(	(
<g/>
zabít	zabít	k5eAaPmF	zabít
<g/>
)	)	kIx)	)
skrze	skrze	k?	skrze
francouzské	francouzský	k2eAgFnPc4d1	francouzská
-	-	kIx~	-
<g/>
cide	cid	k1gFnPc4	cid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
Lemkin	Lemkin	k1gInSc1	Lemkin
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
"	"	kIx"	"
<g/>
zločin	zločin	k1gInSc1	zločin
barbarství	barbarství	k1gNnSc1	barbarství
<g/>
"	"	kIx"	"
právní	právní	k2eAgFnSc6d1	právní
radě	rada	k1gFnSc6	rada
Společnosti	společnost	k1gFnPc1	společnost
národů	národ	k1gInPc2	národ
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
zákona	zákon	k1gInSc2	zákon
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bude	být	k5eAaImBp3nS	být
později	pozdě	k6eAd2	pozdě
nazýváno	nazývat	k5eAaImNgNnS	nazývat
genocidou	genocida	k1gFnSc7	genocida
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
návrhu	návrh	k1gInSc2	návrh
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poprvé	poprvé	k6eAd1	poprvé
slyšel	slyšet	k5eAaImAgMnS	slyšet
o	o	k7c6	o
osmanském	osmanský	k2eAgNnSc6d1	osmanské
masovém	masový	k2eAgNnSc6d1	masové
vraždění	vraždění	k1gNnSc6	vraždění
(	(	kIx(	(
<g/>
arménská	arménský	k2eAgFnSc1d1	arménská
genocida	genocida	k1gFnSc1	genocida
<g/>
)	)	kIx)	)
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
anti-asyrské	antisyrský	k2eAgFnSc6d1	anti-asyrský
perzekuci	perzekuce	k1gFnSc6	perzekuce
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
přivodila	přivodit	k5eAaBmAgFnS	přivodit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
polské	polský	k2eAgFnSc2d1	polská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
politiku	politika	k1gFnSc4	politika
usmíření	usmíření	k1gNnSc3	usmíření
s	s	k7c7	s
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
Carnegie	Carnegie	k1gFnSc1	Carnegie
Endowment	Endowment	k1gInSc1	Endowment
for	forum	k1gNnPc2	forum
International	International	k1gFnSc2	International
Peace	Peaec	k1gInSc2	Peaec
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Lemkinovu	Lemkinův	k2eAgFnSc4d1	Lemkinův
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
práci	práce	k1gFnSc4	práce
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Axis	Axisa	k1gFnPc2	Axisa
Rule	rula	k1gFnSc3	rula
in	in	k?	in
Occupied	Occupied	k1gInSc1	Occupied
Europe	Europ	k1gInSc5	Europ
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
právní	právní	k2eAgFnSc4d1	právní
analýzu	analýza	k1gFnSc4	analýza
německé	německý	k2eAgFnSc2d1	německá
nadvlády	nadvláda	k1gFnSc2	nadvláda
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
okupovaných	okupovaný	k2eAgInPc2d1	okupovaný
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
definice	definice	k1gFnSc2	definice
pojmu	pojem	k1gInSc2	pojem
genocidy	genocida	k1gFnSc2	genocida
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zničení	zničení	k1gNnSc1	zničení
národa	národ	k1gInSc2	národ
nebo	nebo	k8xC	nebo
etnické	etnický	k2eAgFnSc2d1	etnická
skupiny	skupina	k1gFnSc2	skupina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Lemkinova	Lemkinův	k2eAgFnSc1d1	Lemkinův
idea	idea	k1gFnSc1	idea
genocidy	genocida	k1gFnSc2	genocida
jako	jako	k8xS	jako
zločinu	zločin	k1gInSc2	zločin
proti	proti	k7c3	proti
mezinárodnímu	mezinárodní	k2eAgNnSc3d1	mezinárodní
právu	právo	k1gNnSc3	právo
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
komunitou	komunita	k1gFnSc7	komunita
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
právních	právní	k2eAgInPc2d1	právní
podkladů	podklad	k1gInPc2	podklad
norimberského	norimberský	k2eAgInSc2d1	norimberský
procesu	proces	k1gInSc2	proces
(	(	kIx(	(
<g/>
obvinění	obvinění	k1gNnSc2	obvinění
specifikovaná	specifikovaný	k2eAgFnSc1d1	specifikovaná
3.	[number]	k4	3.
bodem	bod	k1gInSc7	bod
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
,	,	kIx,	,
že	že	k8xS	že
obvinění	obviněný	k1gMnPc1	obviněný
"	"	kIx"	"
<g/>
spáchali	spáchat	k5eAaPmAgMnP	spáchat
úmyslnou	úmyslný	k2eAgFnSc4d1	úmyslná
a	a	k8xC	a
systematickou	systematický	k2eAgFnSc4d1	systematická
genocidu	genocida	k1gFnSc4	genocida
–	–	k?	–
zejména	zejména	k9	zejména
vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
rasových	rasový	k2eAgFnPc2d1	rasová
a	a	k8xC	a
národnostních	národnostní	k2eAgFnPc2d1	národnostní
skupin	skupina	k1gFnPc2	skupina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lemkin	Lemkin	k1gMnSc1	Lemkin
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
návrh	návrh	k1gInSc4	návrh
úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
genocidě	genocida	k1gFnSc6	genocida
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
resoluce	resoluce	k1gFnSc2	resoluce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
USA	USA	kA	USA
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc1	návrh
resoluce	resoluce	k1gFnSc2	resoluce
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
Valnému	valný	k2eAgInSc3d1	valný
shromáždění	shromáždění	k1gNnSc2	shromáždění
ke	k	k7c3	k
zvážení	zvážení	k1gNnSc3	zvážení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
Lemkin	Lemkin	k1gMnSc1	Lemkin
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Genocida	genocida	k1gFnSc1	genocida
jako	jako	k8xC	jako
zločin	zločin	k1gInSc1	zločin
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
právo	právo	k1gNnSc1	právo
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Následkem	následkem	k7c2	následkem
holokaustu	holokaust	k1gInSc2	holokaust
Lemkin	Lemkin	k1gMnSc1	Lemkin
úspěšně	úspěšně	k6eAd1	úspěšně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
přijetí	přijetí	k1gNnSc4	přijetí
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
zákonů	zákon	k1gInPc2	zákon
definujících	definující	k2eAgFnPc2d1	definující
a	a	k8xC	a
zakazujících	zakazující	k2eAgFnPc2d1	zakazující
genocidu	genocida	k1gFnSc4	genocida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
první	první	k4xOgFnSc2	první
zasedání	zasedání	k1gNnPc2	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
přijalo	přijmout	k5eAaPmAgNnS	přijmout
resoluci	resoluce	k1gFnSc4	resoluce
96	[number]	k4	96
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
genocida	genocida	k1gFnSc1	genocida
je	být	k5eAaImIp3nS	být
zločinem	zločin	k1gInSc7	zločin
podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezajistilo	zajistit	k5eNaPmAgNnS	zajistit
právní	právní	k2eAgFnSc4d1	právní
definici	definice	k1gFnSc4	definice
zločinu	zločin	k1gInSc2	zločin
genocidy	genocida	k1gFnSc2	genocida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c4	o
zabránění	zabránění	k1gNnSc4	zabránění
a	a	k8xC	a
trestání	trestání	k1gNnSc4	trestání
zločinu	zločin	k1gInSc2	zločin
genocidia	genocidium	k1gNnSc2	genocidium
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poprvé	poprvé	k6eAd1	poprvé
stanovila	stanovit	k5eAaPmAgFnS	stanovit
právní	právní	k2eAgFnSc4d1	právní
definici	definice	k1gFnSc4	definice
genocidy	genocida	k1gFnSc2	genocida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Definice	definice	k1gFnSc2	definice
podle	podle	k7c2	podle
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c4	o
zabránění	zabránění	k1gNnSc4	zabránění
a	a	k8xC	a
trestání	trestání	k1gNnSc4	trestání
zločinu	zločin	k1gInSc2	zločin
genocidia	genocidium	k1gNnSc2	genocidium
OSN	OSN	kA	OSN
====	====	k?	====
</s>
</p>
<p>
<s>
Čl.	čl.	kA	čl.
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
Úmluvě	úmluva	k1gFnSc6	úmluva
se	se	k3xPyFc4	se
genocidou	genocida	k1gFnSc7	genocida
rozumí	rozumět	k5eAaImIp3nS	rozumět
kterýkoli	kterýkoli	k3yIgInSc4	kterýkoli
z	z	k7c2	z
níže	nízce	k6eAd2	nízce
uvedených	uvedený	k2eAgInPc2d1	uvedený
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
zničit	zničit	k5eAaPmF	zničit
úplně	úplně	k6eAd1	úplně
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
některou	některý	k3yIgFnSc4	některý
národní	národní	k2eAgFnSc4d1	národní
<g/>
,	,	kIx,	,
etnickou	etnický	k2eAgFnSc4d1	etnická
<g/>
,	,	kIx,	,
rasovou	rasový	k2eAgFnSc4d1	rasová
nebo	nebo	k8xC	nebo
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
skupinu	skupina	k1gFnSc4	skupina
jako	jako	k9	jako
takovou	takový	k3xDgFnSc4	takový
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
<g/>
)	)	kIx)	)
usmrcení	usmrcení	k1gNnSc1	usmrcení
příslušníků	příslušník	k1gMnPc2	příslušník
takové	takový	k3xDgFnSc2	takový
skupiny	skupina	k1gFnSc2	skupina
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
b	b	k?	b
<g/>
)	)	kIx)	)
způsobení	způsobení	k1gNnSc1	způsobení
těžkých	těžký	k2eAgNnPc2d1	těžké
tělesných	tělesný	k2eAgNnPc2d1	tělesné
ublížení	ublížení	k1gNnPc2	ublížení
nebo	nebo	k8xC	nebo
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
členům	člen	k1gMnPc3	člen
takové	takový	k3xDgFnSc2	takový
skupiny	skupina	k1gFnSc2	skupina
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
c	c	k0	c
<g/>
)	)	kIx)	)
úmyslné	úmyslný	k2eAgNnSc1d1	úmyslné
uvedení	uvedení	k1gNnSc1	uvedení
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
skupiny	skupina	k1gFnSc2	skupina
do	do	k7c2	do
takových	takový	k3xDgFnPc2	takový
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
přivodit	přivodit	k5eAaPmF	přivodit
její	její	k3xOp3gNnSc4	její
úplné	úplný	k2eAgNnSc4d1	úplné
nebo	nebo	k8xC	nebo
částečné	částečný	k2eAgNnSc4d1	částečné
fyzické	fyzický	k2eAgNnSc4d1	fyzické
zničení	zničení	k1gNnSc4	zničení
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
d	d	k?	d
<g/>
)	)	kIx)	)
opatření	opatření	k1gNnSc4	opatření
směřující	směřující	k2eAgNnSc4d1	směřující
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
skupině	skupina	k1gFnSc6	skupina
bránilo	bránit	k5eAaImAgNnS	bránit
rození	rození	k1gNnSc1	rození
dětí	dítě	k1gFnPc2	dítě
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
e	e	k0	e
<g/>
)	)	kIx)	)
násilné	násilný	k2eAgNnSc4d1	násilné
převádění	převádění	k1gNnSc4	převádění
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
skupiny	skupina	k1gFnSc2	skupina
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgNnSc1d1	české
právo	právo	k1gNnSc1	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
č	č	k0	č
<g/>
.	.	kIx.	.
40/2009	[number]	k4	40/2009
Sb.	sb.	kA	sb.
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
genocidia	genocidium	k1gNnSc2	genocidium
a	a	k8xC	a
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
popírání	popírání	k1gNnSc2	popírání
či	či	k8xC	či
schvalování	schvalování	k1gNnSc2	schvalování
genocidia	genocidium	k1gNnSc2	genocidium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Genocidy	genocida	k1gFnSc2	genocida
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Masové	masový	k2eAgNnSc1d1	masové
zabíjení	zabíjení	k1gNnSc1	zabíjení
poražených	poražený	k2eAgMnPc2d1	poražený
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
vypalování	vypalování	k1gNnSc4	vypalování
a	a	k8xC	a
ničení	ničení	k1gNnSc4	ničení
celých	celý	k2eAgNnPc2d1	celé
měst	město	k1gNnPc2	město
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
zabíjením	zabíjení	k1gNnSc7	zabíjení
tamních	tamní	k2eAgMnPc2d1	tamní
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
již	již	k6eAd1	již
do	do	k7c2	do
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Asyřané	Asyřan	k1gMnPc1	Asyřan
proslului	proslului	k1gNnPc2	proslului
ničením	ničení	k1gNnSc7	ničení
dobytých	dobytý	k2eAgNnPc2d1	dobyté
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
bývali	bývat	k5eAaImAgMnP	bývat
pro	pro	k7c4	pro
výstrahu	výstraha	k1gFnSc4	výstraha
umučeni	umučit	k5eAaPmNgMnP	umučit
nebo	nebo	k8xC	nebo
deportování	deportování	k1gNnPc1	deportování
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
král	král	k1gMnSc1	král
Sinacherib	Sinacherib	k1gMnSc1	Sinacherib
roku	rok	k1gInSc2	rok
689	[number]	k4	689
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
takto	takto	k6eAd1	takto
naložil	naložit	k5eAaPmAgMnS	naložit
s	s	k7c7	s
Babylónem	Babylón	k1gInSc7	Babylón
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
krutý	krutý	k2eAgInSc1d1	krutý
byl	být	k5eAaImAgInS	být
i	i	k9	i
král	král	k1gMnSc1	král
Aššurnasirpal	Aššurnasirpal	k1gMnSc2	Aššurnasirpal
II	II	kA	II
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
babylonský	babylonský	k2eAgMnSc1d1	babylonský
vládce	vládce	k1gMnSc1	vládce
Nabukadnesar	Nabukadnesar	k1gMnSc1	Nabukadnesar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
strůjce	strůjce	k1gMnSc1	strůjce
babylonského	babylonský	k2eAgNnSc2d1	babylonské
zajetí	zajetí	k1gNnSc2	zajetí
Izraelitů	izraelita	k1gMnPc2	izraelita
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
krutost	krutost	k1gFnSc1	krutost
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nijak	nijak	k6eAd1	nijak
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
chovali	chovat	k5eAaImAgMnP	chovat
na	na	k7c6	na
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
i	i	k9	i
Chetité	Chetita	k1gMnPc1	Chetita
<g/>
,	,	kIx,	,
Mittannci	Mittannek	k1gMnPc1	Mittannek
nebo	nebo	k8xC	nebo
Izraelité	izraelita	k1gMnPc1	izraelita
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Jozue	Jozue	k1gMnSc1	Jozue
popisuje	popisovat	k5eAaImIp3nS	popisovat
nelítostné	lítostný	k2eNgNnSc4d1	nelítostné
vyvražďování	vyvražďování	k1gNnSc4	vyvražďování
dobytých	dobytý	k2eAgNnPc2d1	dobyté
měst	město	k1gNnPc2	město
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
král	král	k1gMnSc1	král
Saul	Saul	k1gMnSc1	Saul
s	s	k7c7	s
poźehnáním	poźehnání	k1gNnSc7	poźehnání
proroka	prorok	k1gMnSc2	prorok
Samuela	Samuel	k1gMnSc2	Samuel
vyhubil	vyhubit	k5eAaPmAgInS	vyhubit
kmen	kmen	k1gInSc1	kmen
loupeživých	loupeživý	k2eAgMnPc2d1	loupeživý
nájezdníků	nájezdník	k1gMnPc2	nájezdník
Amalekitů	Amalekit	k1gInPc2	Amalekit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
antiky	antika	k1gFnSc2	antika
k	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
masakrům	masakr	k1gInPc3	masakr
docházelo	docházet	k5eAaImAgNnS	docházet
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
výjimek	výjimka	k1gFnPc2	výjimka
bylo	být	k5eAaImAgNnS	být
masové	masový	k2eAgNnSc1d1	masové
pronásledování	pronásledování	k1gNnSc1	pronásledování
Židů	Žid	k1gMnPc2	Žid
Římany	Říman	k1gMnPc4	Říman
po	po	k7c6	po
porázce	porázka	k1gFnSc6	porázka
bar	bar	k1gInSc1	bar
Kochbova	Kochbův	k2eAgNnSc2d1	Kochbovo
povstání	povstání	k1gNnSc2	povstání
roku	rok	k1gInSc2	rok
135	[number]	k4	135
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
měl	mít	k5eAaImAgInS	mít
charakter	charakter	k1gInSc4	charakter
genocidy	genocida	k1gFnSc2	genocida
postup	postup	k1gInSc4	postup
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
proti	proti	k7c3	proti
albigenským	albigenský	k2eAgFnPc3d1	albigenský
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1209.	[number]	k4	1209.
<g/>
1244	[number]	k4	1244
<g/>
,	,	kIx,	,
provázená	provázený	k2eAgFnSc1d1	provázená
nemilosrdnými	milosrdný	k2eNgInPc7d1	nemilosrdný
masakry	masakr	k1gInPc7	masakr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojska	vojsko	k1gNnSc2	vojsko
mongolského	mongolský	k2eAgMnSc2d1	mongolský
dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
Čingischána	Čingischán	k1gMnSc2	Čingischán
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc2	jeho
islamizovaného	islamizovaný	k2eAgMnSc2d1	islamizovaný
pokračovatele	pokračovatel	k1gMnSc2	pokračovatel
Tamerlána	Tamerlán	k2eAgFnSc1d1	Tamerlána
se	se	k3xPyFc4	se
dopustila	dopustit	k5eAaPmAgFnS	dopustit
mnoha	mnoho	k4c2	mnoho
genocidních	genocidní	k2eAgInPc2d1	genocidní
masakrů	masakr	k1gInPc2	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Tamerlán	Tamerlán	k1gInSc1	Tamerlán
vyvraždil	vyvraždit	k5eAaPmAgInS	vyvraždit
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgMnPc4	všechen
křesťany	křesťan	k1gMnPc4	křesťan
z	z	k7c2	z
Asyrské	asyrský	k2eAgFnSc2d1	Asyrská
církve	církev	k1gFnSc2	církev
Východu	východ	k1gInSc2	východ
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zlikvidoval	zlikvidovat	k5eAaPmAgMnS	zlikvidovat
starobylé	starobylý	k2eAgFnPc4d1	starobylá
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
komunity	komunita	k1gFnPc4	komunita
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
milosti	milost	k1gFnSc2	milost
zabíjel	zabíjet	k5eAaImAgInS	zabíjet
i	i	k9	i
všechny	všechen	k3xTgMnPc4	všechen
šiítské	šiítský	k2eAgMnPc4d1	šiítský
muslimy	muslim	k1gMnPc4	muslim
<g/>
,	,	kIx,	,
židy	žid	k1gMnPc4	žid
a	a	k8xC	a
pohany	pohan	k1gMnPc4	pohan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dobýváním	dobývání	k1gNnSc7	dobývání
Ameriky	Amerika	k1gFnSc2	Amerika
někdy	někdy	k6eAd1	někdy
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
genocidě	genocida	k1gFnSc6	genocida
amerických	americký	k2eAgMnPc2d1	americký
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
celé	celý	k2eAgNnSc1d1	celé
období	období	k1gNnSc1	období
kolonizace	kolonizace	k1gFnSc2	kolonizace
Ameriky	Amerika	k1gFnSc2	Amerika
měla	mít	k5eAaImAgFnS	mít
charakter	charakter	k1gInSc4	charakter
genocidy	genocida	k1gFnSc2	genocida
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
však	však	k9	však
k	k	k7c3	k
sérii	série	k1gFnSc3	série
dílčích	dílčí	k2eAgInPc2d1	dílčí
genocidních	genocidní	k2eAgInPc2d1	genocidní
aktů	akt	k1gInPc2	akt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bylo	být	k5eAaImAgNnS	být
vyhubení	vyhubení	k1gNnSc1	vyhubení
Tainů	Tain	k1gMnPc2	Tain
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1462-1550	[number]	k4	1462-1550
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
slz	slza	k1gFnPc2	slza
Čerokíjů	Čerokíj	k1gInPc2	Čerokíj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1838-39	[number]	k4	1838-39
<g/>
,	,	kIx,	,
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pochod	pochod	k1gInSc1	pochod
Navahů	Navah	k1gMnPc2	Navah
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
vybíjení	vybíjení	k1gNnSc1	vybíjení
Mapučů	Mapuč	k1gInPc2	Mapuč
a	a	k8xC	a
Tehuelčéů	Tehuelčé	k1gInPc2	Tehuelčé
Argentinci	Argentinec	k1gMnPc1	Argentinec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1870-1884	[number]	k4	1870-1884
<g/>
,	,	kIx,	,
vyhubení	vyhubení	k1gNnSc1	vyhubení
kmene	kmen	k1gInSc2	kmen
Selknam	Selknam	k1gInSc1	Selknam
(	(	kIx(	(
<g/>
Ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
z	z	k7c2	z
Ohńové	Ohńové	k2eAgFnSc2d1	Ohńové
země	zem	k1gFnSc2	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1890.	[number]	k4	1890.
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Aféra	aféra	k1gFnSc1	aféra
Putumayo	Putumayo	k6eAd1	Putumayo
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1892-1909	[number]	k4	1892-1909
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
příapdy	příapd	k1gInPc1	příapd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
lze	lze	k6eAd1	lze
připomenout	připomenout	k5eAaPmF	připomenout
genocidu	genocida	k1gFnSc4	genocida
Džungarů	Džungar	k1gInPc2	Džungar
Číňany	Číňan	k1gMnPc4	Číňan
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1755-1758	[number]	k4	1755-1758
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
genocidy	genocida	k1gFnSc2	genocida
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
postup	postup	k1gInSc1	postup
Britů	Brit	k1gMnPc2	Brit
proti	proti	k7c3	proti
původním	původní	k2eAgMnPc3d1	původní
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1824-1838	[number]	k4	1824-1838
<g/>
,	,	kIx,	,
barbarské	barbarský	k2eAgFnPc4d1	barbarská
metody	metoda	k1gFnPc4	metoda
belgické	belgický	k2eAgFnSc2d1	belgická
koloniální	koloniální	k2eAgFnSc2d1	koloniální
správy	správa	k1gFnSc2	správa
ve	v	k7c6	v
Svobodném	svobodný	k2eAgInSc6d1	svobodný
státu	stát	k1gInSc6	stát
Kongo	Kongo	k1gNnSc4	Kongo
v	v	k7c6	v
letch	let	k1gFnPc6	let
1885-1904	[number]	k4	1885-1904
a	a	k8xC	a
postup	postup	k1gInSc1	postup
německých	německý	k2eAgMnPc2d1	německý
kolonizátorů	kolonizátor	k1gMnPc2	kolonizátor
proti	proti	k7c3	proti
kmenům	kmen	k1gInPc3	kmen
Namaqua	Namaqu	k1gInSc2	Namaqu
a	a	k8xC	a
Herero	Herero	k1gNnSc1	Herero
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Namibii	Namibie	k1gFnSc6	Namibie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1904-1907	[number]	k4	1904-1907
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genocidního	genocidní	k2eAgNnSc2d1	genocidní
vraždění	vraždění	k1gNnSc2	vraždění
se	se	k3xPyFc4	se
nedopouštěli	dopouštět	k5eNaImAgMnP	dopouštět
jen	jen	k9	jen
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyhubení	vyhubení	k1gNnSc1	vyhubení
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Chathamských	Chathamský	k2eAgInPc2d1	Chathamský
ostrovů	ostrov	k1gInPc2	ostrov
novozélanskými	novozélanský	k2eAgInPc7d1	novozélanský
Maory	Maor	k1gInPc7	Maor
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19.	[number]	k4	19.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
případy	případ	k1gInPc4	případ
genocidy	genocida	k1gFnSc2	genocida
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
genocida	genocida	k1gFnSc1	genocida
Arménů	Armén	k1gMnPc2	Armén
Turky	Turek	k1gMnPc4	Turek
(	(	kIx(	(
<g/>
1915-1917	[number]	k4	1915-1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
genocidy	genocida	k1gFnSc2	genocida
páchané	páchaný	k2eAgFnSc2d1	páchaná
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
vlády	vláda	k1gFnSc2	vláda
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
hladomoru	hladomor	k1gInSc2	hladomor
<g/>
,	,	kIx,	,
genocidy	genocida	k1gFnSc2	genocida
Čečenců	Čečenec	k1gMnPc2	Čečenec
a	a	k8xC	a
Ingušů	Inguš	k1gMnPc2	Inguš
<g/>
,	,	kIx,	,
Tatarů	Tatar	k1gMnPc2	Tatar
nebo	nebo	k8xC	nebo
pronásledování	pronásledování	k1gNnPc2	pronásledování
Kalmyků	Kalmyk	k1gMnPc2	Kalmyk
<g/>
.	.	kIx.	.
</s>
<s>
Jakýmsi	jakýsi	k3yIgInSc7	jakýsi
vzorem	vzor	k1gInSc7	vzor
moderní	moderní	k2eAgFnSc2d1	moderní
genocidy	genocida	k1gFnSc2	genocida
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nacistický	nacistický	k2eAgInSc1d1	nacistický
holokaust	holokaust	k1gInSc1	holokaust
<g/>
,	,	kIx,	,
genocida	genocida	k1gFnSc1	genocida
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
vraždění	vraždění	k1gNnSc4	vraždění
sovětských	sovětský	k2eAgFnPc2d1	sovětská
válečných	válečná	k1gFnPc2	válečná
zajatců	zajatec	k1gMnPc2	zajatec
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
masakry	masakr	k1gInPc1	masakr
páchané	páchaný	k2eAgFnSc2d1	páchaná
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c4	v
okupovaných	okupovaný	k2eAgNnPc2d1	okupované
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
<g/>
)	)	kIx)	)
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejmodernějších	moderní	k2eAgFnPc2d3	nejmodernější
dějin	dějiny	k1gFnPc2	dějiny
jsou	být	k5eAaImIp3nP	být
počtem	počet	k1gInSc7	počet
obětí	oběť	k1gFnPc2	oběť
i	i	k9	i
brutalitou	brutalita	k1gFnSc7	brutalita
zvlášť	zvlášť	k6eAd1	zvlášť
neblaze	blaze	k6eNd1	blaze
proslulé	proslulý	k2eAgFnPc1d1	proslulá
kambodžská	kambodžský	k2eAgFnSc1d1	kambodžská
genocida	genocida	k1gFnSc1	genocida
<g/>
,	,	kIx,	,
rwandská	rwandský	k2eAgFnSc1d1	Rwandská
genocida	genocida	k1gFnSc1	genocida
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
<g/>
"	"	kIx"	"
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
mezi	mezi	k7c7	mezi
Srby	Srb	k1gMnPc7	Srb
<g/>
,	,	kIx,	,
Bosňáky	Bosňáky	k?	Bosňáky
a	a	k8xC	a
Chorvaty	Chorvat	k1gMnPc7	Chorvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OSN	OSN	kA	OSN
vyhodnotila	vyhodnotit	k5eAaPmAgFnS	vyhodnotit
útoky	útok	k1gInPc7	útok
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
na	na	k7c4	na
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
menšinu	menšina	k1gFnSc4	menšina
jezídů	jezíd	k1gInPc2	jezíd
jako	jako	k8xC	jako
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
genocidu	genocida	k1gFnSc4	genocida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
v	v	k7c6	v
severoiráckého	severoirácký	k2eAgInSc2d1	severoirácký
Sindžáru	Sindžár	k1gInSc2	Sindžár
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
až	až	k9	až
5000	[number]	k4	5000
jezídů	jezíd	k1gMnPc2	jezíd
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc1	tisíc
jezídských	jezídský	k2eAgFnPc2d1	jezídská
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
byly	být	k5eAaImAgFnP	být
znásilněny	znásilněn	k2eAgFnPc1d1	znásilněna
a	a	k8xC	a
odvlečeny	odvléct	k5eAaPmNgFnP	odvléct
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Genocide	Genocid	k1gInSc5	Genocid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LEMKIN	LEMKIN	kA	LEMKIN
<g/>
,	,	kIx,	,
Raphael	Raphael	k1gMnSc1	Raphael
<g/>
.	.	kIx.	.
</s>
<s>
Axis	Axis	k6eAd1	Axis
Rule	rula	k1gFnSc3	rula
in	in	k?	in
Occupied	Occupied	k1gInSc1	Occupied
Europe	Europ	k1gInSc5	Europ
<g/>
:	:	kIx,	:
Laws	Laws	k1gInSc1	Laws
of	of	k?	of
Occupation	Occupation	k1gInSc1	Occupation
–	–	k?	–
Analysis	Analysis	k1gInSc1	Analysis
of	of	k?	of
Government	Government	k1gInSc1	Government
–	–	k?	–
Proposals	Proposals	k1gInSc1	Proposals
for	forum	k1gNnPc2	forum
Redress	Redressa	k1gFnPc2	Redressa
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C	D.C	k1gFnSc1	D.C
<g/>
:	:	kIx,	:
Carnegie	Carnegie	k1gFnSc1	Carnegie
Endowment	Endowment	k1gInSc4	Endowment
for	forum	k1gNnPc2	forum
International	International	k1gMnSc2	International
Peace	Peace	k1gMnSc2	Peace
<g/>
,	,	kIx,	,
1944.	[number]	k4	1944.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ANDREOPOULOS	ANDREOPOULOS	kA	ANDREOPOULOS
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
J.	J.	kA	J.
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Genocide	Genocid	k1gMnSc5	Genocid
<g/>
:	:	kIx,	:
Conceptual	Conceptual	k1gInSc1	Conceptual
and	and	k?	and
Historical	Historical	k1gFnSc2	Historical
Dimensions	Dimensionsa	k1gFnPc2	Dimensionsa
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
.	.	kIx.	.
<g/>
l	l	kA	l
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0812232496.	[number]	k4	0812232496.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CHALK	CHALK	kA	CHALK
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Jonassohn	Jonassohn	k1gMnSc1	Jonassohn
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
History	Histor	k1gInPc1	Histor
and	and	k?	and
Sociology	sociolog	k1gMnPc4	sociolog
of	of	k?	of
Genocide	Genocid	k1gInSc5	Genocid
<g/>
:	:	kIx,	:
Analyses	Analyses	k1gInSc1	Analyses
and	and	k?	and
Case	Case	k1gInSc1	Case
Studies	Studies	k1gInSc1	Studies
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
.	.	kIx.	.
<g/>
l	l	kA	l
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0300044461.	[number]	k4	0300044461.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KELLY	KELLY	kA	KELLY
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
J.	J.	kA	J.
Nowhere	Nowher	k1gInSc5	Nowher
to	ten	k3xDgNnSc4	ten
Hide	Hide	k1gFnSc1	Hide
<g/>
:	:	kIx,	:
Defeat	Defeat	k1gInSc1	Defeat
of	of	k?	of
the	the	k?	the
Sovereign	sovereign	k1gInSc1	sovereign
Immunity	Immunit	k2eAgInPc1d1	Immunit
Defense	defense	k1gFnPc4	defense
for	forum	k1gNnPc2	forum
Crimes	Crimesa	k1gFnPc2	Crimesa
of	of	k?	of
Genocide	Genocid	k1gInSc5	Genocid
&	&	k?	&
the	the	k?	the
Trials	Trialsa	k1gFnPc2	Trialsa
of	of	k?	of
Slobodan	Slobodan	k1gMnSc1	Slobodan
Milosevic	Milosevic	k1gMnSc1	Milosevic
and	and	k?	and
Saddam	Saddam	k1gInSc1	Saddam
Hussein	Hussein	k1gInSc1	Hussein
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
.	.	kIx.	.
<g/>
l	l	kA	l
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Peter	Peter	k1gMnSc1	Peter
Lang	Lang	k1gMnSc1	Lang
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0820478350.	[number]	k4	0820478350.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Genocidy	genocida	k1gFnPc1	genocida
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
</s>
</p>
<p>
<s>
Definice	definice	k1gFnSc1	definice
genocidy	genocida	k1gFnSc2	genocida
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
genocida	genocida	k1gFnSc1	genocida
</s>
</p>
<p>
<s>
Autogenocida	Autogenocida	k1gFnSc1	Autogenocida
</s>
</p>
