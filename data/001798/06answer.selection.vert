<s>
RISC	RISC	kA	RISC
označuje	označovat	k5eAaImIp3nS	označovat
procesory	procesor	k1gInPc4	procesor
s	s	k7c7	s
redukovanou	redukovaný	k2eAgFnSc7d1	redukovaná
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
návrh	návrh	k1gInSc1	návrh
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
optimalizovanou	optimalizovaný	k2eAgFnSc4d1	optimalizovaná
sadu	sada	k1gFnSc4	sada
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
instrukcí	instrukce	k1gFnPc2	instrukce
ostatních	ostatní	k2eAgFnPc2d1	ostatní
architektur	architektura	k1gFnPc2	architektura
<g/>
.	.	kIx.	.
</s>
