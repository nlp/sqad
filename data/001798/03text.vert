<s>
RISC	RISC	kA	RISC
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Reduced	Reduced	k1gInSc1	Reduced
Instruction	Instruction	k1gInSc1	Instruction
Set	set	k1gInSc1	set
Computing	Computing	k1gInSc1	Computing
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
risk	risk	k1gInSc1	risk
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
architektur	architektura	k1gFnPc2	architektura
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
RISC	RISC	kA	RISC
označuje	označovat	k5eAaImIp3nS	označovat
procesory	procesor	k1gInPc4	procesor
s	s	k7c7	s
redukovanou	redukovaný	k2eAgFnSc7d1	redukovaná
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
návrh	návrh	k1gInSc1	návrh
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
optimalizovanou	optimalizovaný	k2eAgFnSc4d1	optimalizovaná
sadu	sada	k1gFnSc4	sada
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
instrukcí	instrukce	k1gFnPc2	instrukce
ostatních	ostatní	k2eAgFnPc2d1	ostatní
architektur	architektura	k1gFnPc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
definice	definice	k1gFnSc1	definice
označení	označení	k1gNnSc2	označení
RISC	RISC	kA	RISC
není	být	k5eNaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
často	často	k6eAd1	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
popisnější	popisní	k2eAgInSc1d2	popisní
název	název	k1gInSc1	název
architektura	architektura	k1gFnSc1	architektura
load-store	loadtor	k1gMnSc5	load-stor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lépe	dobře	k6eAd2	dobře
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
instrukcí	instrukce	k1gFnPc2	instrukce
RISC	RISC	kA	RISC
procesoru	procesor	k1gInSc2	procesor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
paradoxně	paradoxně	k6eAd1	paradoxně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
architektur	architektura	k1gFnPc2	architektura
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c4	mezi
zástupce	zástupce	k1gMnPc4	zástupce
RISC	RISC	kA	RISC
procesorů	procesor	k1gInPc2	procesor
patří	patřit	k5eAaImIp3nP	patřit
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
MIPS	MIPS	kA	MIPS
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
Am	Am	k1gFnSc1	Am
<g/>
29000	[number]	k4	29000
<g/>
,	,	kIx,	,
ARC	ARC	kA	ARC
<g/>
,	,	kIx,	,
Atmel	Atmel	k1gInSc1	Atmel
AVR	AVR	kA	AVR
<g/>
,	,	kIx,	,
PA-RISC	PA-RISC	k1gFnSc1	PA-RISC
<g/>
,	,	kIx,	,
POWER	POWER	kA	POWER
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
PowerPC	PowerPC	k1gFnSc2	PowerPC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SuperH	SuperH	k1gMnSc1	SuperH
<g/>
,	,	kIx,	,
SPARC	SPARC	kA	SPARC
<g/>
,	,	kIx,	,
DEC	DEC	kA	DEC
Alpha	Alpha	k1gFnSc1	Alpha
<g/>
.	.	kIx.	.
</s>
<s>
Protichůdnou	protichůdný	k2eAgFnSc7d1	protichůdná
architekturou	architektura	k1gFnSc7	architektura
jsou	být	k5eAaImIp3nP	být
CISC	CISC	kA	CISC
procesory	procesor	k1gInPc1	procesor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Complex	Complex	k1gInSc1	Complex
Instruction	Instruction	k1gInSc1	Instruction
Set	set	k1gInSc1	set
Computers	Computers	k1gInSc4	Computers
<g/>
,	,	kIx,	,
s	s	k7c7	s
komplexní	komplexní	k2eAgFnSc7d1	komplexní
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
alternativou	alternativa	k1gFnSc7	alternativa
jsou	být	k5eAaImIp3nP	být
VLIW	VLIW	kA	VLIW
(	(	kIx(	(
<g/>
Very	Vera	k1gMnSc2	Vera
Large	Large	k1gFnSc1	Large
Instruction	Instruction	k1gInSc1	Instruction
Word	Word	kA	Word
<g/>
)	)	kIx)	)
a	a	k8xC	a
EPIC	EPIC	kA	EPIC
(	(	kIx(	(
<g/>
Explicitly	Explicitly	k1gMnPc3	Explicitly
parallel	parallet	k5eAaPmAgInS	parallet
instruction	instruction	k1gInSc1	instruction
computing	computing	k1gInSc1	computing
<g/>
)	)	kIx)	)
procesory	procesor	k1gInPc1	procesor
<g/>
.	.	kIx.	.
</s>
<s>
VLIW	VLIW	kA	VLIW
a	a	k8xC	a
EPIC	EPIC	kA	EPIC
procesory	procesor	k1gInPc1	procesor
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
vývojem	vývoj	k1gInSc7	vývoj
z	z	k7c2	z
RISC	RISC	kA	RISC
<g/>
.	.	kIx.	.
</s>
<s>
VLIW	VLIW	kA	VLIW
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
ve	v	k7c6	v
starších	starší	k1gMnPc6	starší
GPU	GPU	kA	GPU
firmy	firma	k1gFnSc2	firma
AMD	AMD	kA	AMD
<g/>
.	.	kIx.	.
</s>
<s>
Architekturu	architektura	k1gFnSc4	architektura
EPIC	EPIC	kA	EPIC
používají	používat	k5eAaImIp3nP	používat
například	například	k6eAd1	například
procesory	procesor	k1gInPc1	procesor
Itanium	Itanium	k1gNnSc1	Itanium
a	a	k8xC	a
Itanium	Itanium	k1gNnSc1	Itanium
2	[number]	k4	2
<g/>
.	.	kIx.	.
procesor	procesor	k1gInSc1	procesor
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
po	po	k7c6	po
sběrnici	sběrnice	k1gFnSc6	sběrnice
redukované	redukovaný	k2eAgFnSc2d1	redukovaná
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
typy	typ	k1gInPc1	typ
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
práce	práce	k1gFnSc2	práce
uvnitř	uvnitř	k7c2	uvnitř
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
operace	operace	k1gFnSc1	operace
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
řídící	řídící	k2eAgFnSc1d1	řídící
instrukce	instrukce	k1gFnSc1	instrukce
<g/>
)	)	kIx)	)
délka	délka	k1gFnSc1	délka
provádění	provádění	k1gNnSc2	provádění
jedné	jeden	k4xCgFnSc2	jeden
instrukce	instrukce	k1gFnSc2	instrukce
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
cyklus	cyklus	k1gInSc4	cyklus
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
délka	délka	k1gFnSc1	délka
v	v	k7c6	v
bitech	bit	k1gInPc6	bit
všech	všecek	k3xTgFnPc2	všecek
instrukcí	instrukce	k1gFnPc2	instrukce
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
)	)	kIx)	)
mikroinstrukce	mikroinstrukce	k1gFnSc1	mikroinstrukce
jsou	být	k5eAaImIp3nP	být
hardwarově	hardwarově	k6eAd1	hardwarově
implementovány	implementován	k2eAgFnPc1d1	implementována
na	na	k7c6	na
procesoru	procesor	k1gInSc6	procesor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
zvýšena	zvýšen	k2eAgFnSc1d1	zvýšena
rychlost	rychlost	k1gFnSc1	rychlost
jejich	jejich	k3xOp3gNnSc2	jejich
provádění	provádění	k1gNnSc2	provádění
registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
víceúčelové	víceúčelový	k2eAgFnPc1d1	víceúčelová
(	(	kIx(	(
<g/>
nezáleží	záležet	k5eNaImIp3nS	záležet
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
instrukce	instrukce	k1gFnSc1	instrukce
využije	využít	k5eAaPmIp3nS	využít
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
návrh	návrh	k1gInSc4	návrh
překladačů	překladač	k1gInPc2	překladač
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
využívají	využívat	k5eAaImIp3nP	využívat
řetězení	řetězení	k1gNnSc4	řetězení
instrukcí	instrukce	k1gFnPc2	instrukce
(	(	kIx(	(
<g/>
pipelining	pipelining	k1gInSc1	pipelining
<g/>
)	)	kIx)	)
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
vědci	vědec	k1gMnPc1	vědec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Seymour	Seymour	k1gMnSc1	Seymour
Cray	Craa	k1gFnSc2	Craa
<g/>
)	)	kIx)	)
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
programů	program	k1gInPc2	program
prováděných	prováděný	k2eAgInPc2d1	prováděný
na	na	k7c6	na
tehdejších	tehdejší	k2eAgInPc6d1	tehdejší
počítačích	počítač	k1gInPc6	počítač
využívala	využívat	k5eAaPmAgFnS	využívat
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
jen	jen	k9	jen
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dostupných	dostupný	k2eAgFnPc2d1	dostupná
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bylo	být	k5eAaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgInPc1d1	tehdejší
překladače	překladač	k1gInPc1	překladač
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
efektivněji	efektivně	k6eAd2	efektivně
využít	využít	k5eAaPmF	využít
všech	všecek	k3xTgFnPc2	všecek
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
složitý	složitý	k2eAgInSc4d1	složitý
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
zpomaloval	zpomalovat	k5eAaImAgMnS	zpomalovat
provádění	provádění	k1gNnSc4	provádění
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Započalo	započnout	k5eAaPmAgNnS	započnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
RISC	RISC	kA	RISC
procesorů	procesor	k1gInPc2	procesor
s	s	k7c7	s
redukovanou	redukovaný	k2eAgFnSc7d1	redukovaná
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
využívat	využívat	k5eAaPmF	využívat
mikroprocesorové	mikroprocesorový	k2eAgInPc4d1	mikroprocesorový
ortogonality	ortogonalit	k1gInPc4	ortogonalit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
operaci	operace	k1gFnSc4	operace
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
procesoru	procesor	k1gInSc6	procesor
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
instrukce	instrukce	k1gFnSc1	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
RISCových	RISCův	k2eAgInPc2d1	RISCův
strojů	stroj	k1gInPc2	stroj
byl	být	k5eAaImAgInS	být
Superpočítač	superpočítač	k1gInSc1	superpočítač
CDC	CDC	kA	CDC
6600	[number]	k4	6600
navržený	navržený	k2eAgInSc4d1	navržený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
CPU	CPU	kA	CPU
měla	mít	k5eAaImAgFnS	mít
74	[number]	k4	74
operačních	operační	k2eAgInPc2d1	operační
kódů	kód	k1gInPc2	kód
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
různých	různý	k2eAgFnPc2d1	různá
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
400	[number]	k4	400
u	u	k7c2	u
8086	[number]	k4	8086
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
současné	současný	k2eAgMnPc4d1	současný
zástupce	zástupce	k1gMnPc4	zástupce
plně	plně	k6eAd1	plně
ortogonálních	ortogonální	k2eAgInPc2d1	ortogonální
procesorů	procesor	k1gInPc2	procesor
patří	patřit	k5eAaImIp3nP	patřit
procesory	procesor	k1gInPc1	procesor
ARMv	ARMv	k1gInSc1	ARMv
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
RISC	RISC	kA	RISC
procesory	procesor	k1gInPc1	procesor
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInPc1	jejich
výrobní	výrobní	k2eAgInPc1d1	výrobní
náklady	náklad	k1gInPc1	náklad
nižší	nízký	k2eAgInPc1d2	nižší
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
,	,	kIx,	,
než	než	k8xS	než
procesory	procesor	k1gInPc1	procesor
CISC	CISC	kA	CISC
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
produkce	produkce	k1gFnSc1	produkce
procesorů	procesor	k1gInPc2	procesor
CISC	CISC	kA	CISC
eskalovala	eskalovat	k5eAaImAgFnS	eskalovat
k	k	k7c3	k
technickým	technický	k2eAgInPc3d1	technický
limitům	limit	k1gInPc3	limit
(	(	kIx(	(
<g/>
zvyšování	zvyšování	k1gNnSc3	zvyšování
frekvence	frekvence	k1gFnSc2	frekvence
a	a	k8xC	a
zvětšování	zvětšování	k1gNnSc1	zvětšování
čipu	čip	k1gInSc2	čip
versus	versus	k7c1	versus
konečná	konečný	k2eAgFnSc1d1	konečná
rychlost	rychlost	k1gFnSc4	rychlost
šíření	šíření	k1gNnSc2	šíření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
implementace	implementace	k1gFnSc1	implementace
mnoha	mnoho	k4c6	mnoho
komplexních	komplexní	k2eAgFnPc6d1	komplexní
(	(	kIx(	(
<g/>
a	a	k8xC	a
méně	málo	k6eAd2	málo
využívaných	využívaný	k2eAgFnPc2d1	využívaná
<g/>
)	)	kIx)	)
CISCových	CISCův	k2eAgFnPc2d1	CISCův
instrukcí	instrukce	k1gFnPc2	instrukce
pomocí	pomocí	k7c2	pomocí
interních	interní	k2eAgFnPc2d1	interní
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
mikroinstrukcí	mikroinstrukce	k1gFnPc2	mikroinstrukce
<g/>
.	.	kIx.	.
</s>
<s>
CISC	CISC	kA	CISC
procesory	procesor	k1gInPc1	procesor
se	se	k3xPyFc4	se
tak	tak	k9	tak
interně	interně	k6eAd1	interně
staly	stát	k5eAaPmAgFnP	stát
RISC	RISC	kA	RISC
procesory	procesor	k1gInPc1	procesor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohly	moct	k5eAaImAgInP	moct
pomocí	pomocí	k7c2	pomocí
mikrokódu	mikrokód	k1gInSc2	mikrokód
interpretovat	interpretovat	k5eAaBmF	interpretovat
složité	složitý	k2eAgInPc4d1	složitý
a	a	k8xC	a
komplexní	komplexní	k2eAgFnPc4d1	komplexní
CISC	CISC	kA	CISC
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
výrobce	výrobce	k1gMnPc4	výrobce
procesorů	procesor	k1gInPc2	procesor
RISC	RISC	kA	RISC
patří	patřit	k5eAaImIp3nS	patřit
IBM	IBM	kA	IBM
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řada	řada	k1gFnSc1	řada
PowerPC	PowerPC	k1gFnSc1	PowerPC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Intel	Intel	kA	Intel
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gInPc2	jeho
procesorů	procesor	k1gInPc2	procesor
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
CISC	CISC	kA	CISC
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
post-RISC	post-RISC	k?	post-RISC
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řada	řada	k1gFnSc1	řada
Sparc	Sparc	k1gFnSc1	Sparc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
32	[number]	k4	32
<g/>
bitových	bitový	k2eAgInPc6d1	bitový
RISC	RISC	kA	RISC
procesorech	procesor	k1gInPc6	procesor
zabírají	zabírat	k5eAaImIp3nP	zabírat
75	[number]	k4	75
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
procesory	procesor	k1gInPc7	procesor
ARM	ARM	kA	ARM
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
počítačového	počítačový	k2eAgInSc2d1	počítačový
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
programovalo	programovat	k5eAaImAgNnS	programovat
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
assembleru	assembler	k1gInSc6	assembler
nebo	nebo	k8xC	nebo
strojovém	strojový	k2eAgInSc6d1	strojový
kódu	kód	k1gInSc6	kód
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgFnP	vyznačovat
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
instrukcemi	instrukce	k1gFnPc7	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Návrháři	návrhář	k1gMnPc1	návrhář
CPU	CPU	kA	CPU
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
instrukce	instrukce	k1gFnPc4	instrukce
dělaly	dělat	k5eAaImAgFnP	dělat
tolik	tolik	k6eAd1	tolik
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
je	být	k5eAaImIp3nS	být
proveditelné	proveditelný	k2eAgNnSc1d1	proveditelné
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
vyšších	vysoký	k2eAgInPc2d2	vyšší
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
začali	začít	k5eAaPmAgMnP	začít
programátoři	programátor	k1gMnPc1	programátor
vytvářet	vytvářet	k5eAaImF	vytvářet
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přímo	přímo	k6eAd1	přímo
použili	použít	k5eAaPmAgMnP	použít
některé	některý	k3yIgInPc4	některý
centrální	centrální	k2eAgInPc4d1	centrální
mechanismy	mechanismus	k1gInPc4	mechanismus
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
obecným	obecný	k2eAgInSc7d1	obecný
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zajistit	zajistit	k5eAaPmF	zajistit
všechny	všechen	k3xTgInPc4	všechen
možné	možný	k2eAgInPc4d1	možný
adresovací	adresovací	k2eAgInPc4d1	adresovací
módy	mód	k1gInPc4	mód
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
instrukci	instrukce	k1gFnSc4	instrukce
tzv.	tzv.	kA	tzv.
ortogonalita	ortogonalita	k1gFnSc1	ortogonalita
k	k	k7c3	k
usnadnění	usnadnění	k1gNnSc3	usnadnění
implementace	implementace	k1gFnSc2	implementace
kompilátoru	kompilátor	k1gInSc2	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
aritmetické	aritmetický	k2eAgFnPc1d1	aritmetická
operace	operace	k1gFnPc1	operace
stejné	stejný	k2eAgInPc4d1	stejný
výsledky	výsledek	k1gInPc4	výsledek
jako	jako	k8xS	jako
operandy	operand	k1gInPc4	operand
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
registrů	registr	k1gInPc2	registr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
design	design	k1gInSc4	design
hardwaru	hardware	k1gInSc2	hardware
vyspělejší	vyspělý	k2eAgFnSc3d2	vyspělejší
než	než	k8xS	než
design	design	k1gInSc1	design
kompilátoru	kompilátor	k1gInSc2	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
hardwaru	hardware	k1gInSc2	hardware
nebo	nebo	k8xC	nebo
mikrokódu	mikrokód	k1gInSc2	mikrokód
než	než	k8xS	než
pamětí	paměť	k1gFnPc2	paměť
omezené	omezený	k2eAgInPc1d1	omezený
kompilátory	kompilátor	k1gInPc1	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
filozofie	filozofie	k1gFnSc1	filozofie
designu	design	k1gInSc2	design
byla	být	k5eAaImAgFnS	být
zpětně	zpětně	k6eAd1	zpětně
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
jako	jako	k8xC	jako
komplexní	komplexní	k2eAgFnSc1d1	komplexní
instrukční	instrukční	k2eAgFnSc1d1	instrukční
výpočetní	výpočetní	k2eAgFnSc1d1	výpočetní
sada	sada	k1gFnSc1	sada
(	(	kIx(	(
<g/>
complex	complex	k1gInSc1	complex
instruction	instruction	k1gInSc1	instruction
set	set	k1gInSc1	set
computing	computing	k1gInSc1	computing
-	-	kIx~	-
CISC	CISC	kA	CISC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
RISCová	RISCový	k2eAgFnSc1d1	RISCový
filozofie	filozofie	k1gFnSc1	filozofie
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
<g/>
.	.	kIx.	.
</s>
<s>
CPU	CPU	kA	CPU
měli	mít	k5eAaImAgMnP	mít
relativně	relativně	k6eAd1	relativně
málo	málo	k4c4	málo
registrů	registr	k1gInPc2	registr
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
Více	hodně	k6eAd2	hodně
registrů	registr	k1gInPc2	registr
znamená	znamenat	k5eAaImIp3nS	znamenat
také	také	k9	také
časově	časově	k6eAd1	časově
náročnější	náročný	k2eAgNnSc1d2	náročnější
ukládání	ukládání	k1gNnSc1	ukládání
a	a	k8xC	a
obnovení	obnovení	k1gNnSc1	obnovení
jejich	jejich	k3xOp3gInSc2	jejich
obsahu	obsah	k1gInSc2	obsah
ze	z	k7c2	z
zásobníku	zásobník	k1gInSc2	zásobník
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
registrů	registr	k1gInPc2	registr
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
instrukčních	instrukční	k2eAgInPc2d1	instrukční
bitů	bit	k1gInPc2	bit
jako	jako	k8xS	jako
registr	registr	k1gInSc1	registr
specifikací	specifikace	k1gFnPc2	specifikace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zpřehledňuje	zpřehledňovat	k5eAaImIp3nS	zpřehledňovat
kód	kód	k1gInSc1	kód
CPU	CPU	kA	CPU
registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
dražší	drahý	k2eAgNnPc4d2	dražší
než	než	k8xS	než
externí	externí	k2eAgNnPc4d1	externí
paměťová	paměťový	k2eAgNnPc4d1	paměťové
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
velké	velká	k1gFnPc4	velká
registrové	registrový	k2eAgFnSc2d1	registrová
sady	sada	k1gFnSc2	sada
byly	být	k5eAaImAgInP	být
značně	značně	k6eAd1	značně
těžkopádné	těžkopádný	k2eAgInPc1d1	těžkopádný
s	s	k7c7	s
omezenými	omezený	k2eAgInPc7d1	omezený
obvody	obvod	k1gInPc7	obvod
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
nebo	nebo	k8xC	nebo
čipovou	čipový	k2eAgFnSc7d1	čipová
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
byla	být	k5eAaImAgFnS	být
omezená	omezený	k2eAgFnSc1d1	omezená
hlavní	hlavní	k2eAgFnSc1d1	hlavní
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
kilobytů	kilobyt	k1gInPc2	kilobyt
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
uchovávat	uchovávat	k5eAaImF	uchovávat
informace	informace	k1gFnPc4	informace
ve	v	k7c6	v
složitých	složitý	k2eAgInPc6d1	složitý
počítačových	počítačový	k2eAgInPc6d1	počítačový
programech	program	k1gInPc6	program
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zbytečně	zbytečně	k6eAd1	zbytečně
neblokovat	blokovat	k5eNaImF	blokovat
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
více	hodně	k6eAd2	hodně
kódují	kódovat	k5eAaBmIp3nP	kódovat
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
proměnné	proměnná	k1gFnPc4	proměnná
délky	délka	k1gFnSc2	délka
instrukce	instrukce	k1gFnSc2	instrukce
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
načítají	načítat	k5eAaPmIp3nP	načítat
data	datum	k1gNnPc4	datum
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
jako	jako	k9	jako
počítají	počítat	k5eAaImIp3nP	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
problémy	problém	k1gInPc1	problém
měly	mít	k5eAaImAgInP	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
prioritu	priorita	k1gFnSc4	priorita
než	než	k8xS	než
snadné	snadný	k2eAgNnSc4d1	snadné
dekódování	dekódování	k1gNnSc4	dekódování
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc2d1	hlavní
paměti	paměť	k1gFnSc2	paměť
byly	být	k5eAaImAgInP	být
poměrně	poměrně	k6eAd1	poměrně
pomalé	pomalý	k2eAgNnSc1d1	pomalé
(	(	kIx(	(
<g/>
běžný	běžný	k2eAgInSc4d1	běžný
typ	typ	k1gInSc4	typ
paměti	paměť	k1gFnSc2	paměť
byla	být	k5eAaImAgFnS	být
paměť	paměť	k1gFnSc1	paměť
s	s	k7c7	s
feritovým	feritový	k2eAgNnSc7d1	feritové
jádrem	jádro	k1gNnSc7	jádro
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
jedna	jeden	k4xCgFnSc1	jeden
snížit	snížit	k5eAaPmF	snížit
frekvenci	frekvence	k1gFnSc4	frekvence
CPU	CPU	kA	CPU
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
prostředkům	prostředek	k1gInPc3	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
počítače	počítač	k1gInPc1	počítač
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
limitujícími	limitující	k2eAgInPc7d1	limitující
faktory	faktor	k1gInPc7	faktor
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgFnSc2d1	hlavní
paměti	paměť	k1gFnSc2	paměť
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
CPU	CPU	kA	CPU
pomalé	pomalý	k2eAgFnSc2d1	pomalá
a	a	k8xC	a
rychlé	rychlý	k2eAgFnSc2d1	rychlá
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
cache	cache	k1gInSc1	cache
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgInPc1d1	omezený
svojí	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
částečně	částečně	k6eAd1	částečně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
dokážou	dokázat	k5eAaPmIp3nP	dokázat
vysoce	vysoce	k6eAd1	vysoce
zakódované	zakódovaný	k2eAgFnPc4d1	zakódovaná
instrukční	instrukční	k2eAgFnPc4d1	instrukční
sady	sada	k1gFnPc4	sada
být	být	k5eAaImF	být
stejně	stejně	k6eAd1	stejně
užitečné	užitečný	k2eAgNnSc1d1	užitečné
jako	jako	k8xS	jako
RISC	RISC	kA	RISC
procesory	procesor	k1gInPc1	procesor
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
počítačích	počítač	k1gInPc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
dnes	dnes	k6eAd1	dnes
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
CDC	CDC	kA	CDC
6600	[number]	k4	6600
supercomputer	supercomputra	k1gFnPc2	supercomputra
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgInSc1d1	navržený
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
samotný	samotný	k2eAgInSc1d1	samotný
pojem	pojem	k1gInSc1	pojem
RISC	RISC	kA	RISC
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
CDC	CDC	kA	CDC
6600	[number]	k4	6600
využíval	využívat	k5eAaImAgInS	využívat
load-store	loadtor	k1gMnSc5	load-stor
architekturu	architektura	k1gFnSc4	architektura
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
dvěma	dva	k4xCgInPc7	dva
adresními	adresní	k2eAgInPc7d1	adresní
módy	mód	k1gInPc7	mód
(	(	kIx(	(
<g/>
registr-registr	registregistr	k1gInSc1	registr-registr
a	a	k8xC	a
registr-konstanta	registronstant	k1gMnSc4	registr-konstant
<g/>
)	)	kIx)	)
a	a	k8xC	a
74	[number]	k4	74
operačních	operační	k2eAgInPc2d1	operační
kódů	kód	k1gInPc2	kód
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
Intel	Intel	kA	Intel
8086	[number]	k4	8086
jich	on	k3xPp3gInPc2	on
měl	mít	k5eAaImAgMnS	mít
400	[number]	k4	400
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
CDC	CDC	kA	CDC
6600	[number]	k4	6600
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
jedenáct	jedenáct	k4xCc1	jedenáct
zřetězených	zřetězený	k2eAgFnPc2d1	zřetězená
funkčních	funkční	k2eAgFnPc2d1	funkční
jednotek	jednotka	k1gFnPc2	jednotka
pro	pro	k7c4	pro
aritmetické	aritmetický	k2eAgInPc4d1	aritmetický
a	a	k8xC	a
logické	logický	k2eAgInPc4d1	logický
výpočty	výpočet	k1gInPc4	výpočet
<g/>
,	,	kIx,	,
pět	pět	k4xCc1	pět
načítacích	načítací	k2eAgFnPc2d1	načítací
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
ukládací	ukládací	k2eAgFnPc4d1	ukládací
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Paměť	paměť	k1gFnSc1	paměť
CDC	CDC	kA	CDC
6600	[number]	k4	6600
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
násobné	násobný	k2eAgInPc4d1	násobný
paměťové	paměťový	k2eAgInPc4d1	paměťový
banky	bank	k1gInPc4	bank
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgFnPc1	všechen
load-store	loadtor	k1gMnSc5	load-stor
jednotky	jednotka	k1gFnPc4	jednotka
mohly	moct	k5eAaImAgFnP	moct
pracovat	pracovat	k5eAaImF	pracovat
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
hodinový	hodinový	k2eAgInSc1d1	hodinový
takt	takt	k1gInSc1	takt
cyklu	cyklus	k1gInSc2	cyklus
<g/>
/	/	kIx~	/
<g/>
instrukce	instrukce	k1gFnSc1	instrukce
byl	být	k5eAaImAgInS	být
desetkrát	desetkrát	k6eAd1	desetkrát
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
rané	raný	k2eAgFnPc1d1	raná
load-store	loadtor	k1gMnSc5	load-stor
zařízen	zařídit	k5eAaPmNgInS	zařídit
byl	být	k5eAaImAgInS	být
minipočítač	minipočítač	k1gInSc1	minipočítač
Data	datum	k1gNnSc2	datum
General	General	k1gFnSc2	General
Nova	novum	k1gNnSc2	novum
navržený	navržený	k2eAgInSc1d1	navržený
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vývojářem	vývojář	k1gMnSc7	vývojář
Edson	Edsona	k1gFnPc2	Edsona
de	de	k?	de
Castro	Castro	k1gNnSc4	Castro
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
téměř	téměř	k6eAd1	téměř
čistou	čistý	k2eAgFnSc4d1	čistá
RISCovou	RISCový	k2eAgFnSc4d1	RISCový
instrukční	instrukční	k2eAgFnSc4d1	instrukční
sadu	sada	k1gFnSc4	sada
<g/>
,	,	kIx,	,
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
podobnou	podobný	k2eAgFnSc4d1	podobná
dnešním	dnešní	k2eAgInPc3d1	dnešní
procesorům	procesor	k1gInPc3	procesor
ARM	ARM	kA	ARM
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
pokus	pokus	k1gInSc1	pokus
vyrobit	vyrobit	k5eAaPmF	vyrobit
čipově	čipově	k6eAd1	čipově
založený	založený	k2eAgInSc4d1	založený
RISC	RISC	kA	RISC
procesor	procesor	k1gInSc1	procesor
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc4	projekt
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
projekt	projekt	k1gInSc1	projekt
probíhal	probíhat	k5eAaImAgInS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
rodině	rodina	k1gFnSc3	rodina
procesorů	procesor	k1gInPc2	procesor
IBM	IBM	kA	IBM
801	[number]	k4	801
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
široce	široko	k6eAd1	široko
užita	užít	k5eAaPmNgFnS	užít
v	v	k7c4	v
hardware	hardware	k1gInSc4	hardware
IBM	IBM	kA	IBM
<g/>
.	.	kIx.	.
</s>
<s>
IBM	IBM	kA	IBM
801	[number]	k4	801
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
produkován	produkovat	k5eAaImNgInS	produkovat
v	v	k7c6	v
jednočipové	jednočipový	k2eAgFnSc6d1	jednočipová
podobě	podoba	k1gFnSc6	podoba
jako	jako	k8xS	jako
ROMP	ROMP	kA	ROMP
(	(	kIx(	(
<g/>
Research	Research	k1gMnSc1	Research
OPD	OPD	kA	OPD
<g/>
[	[	kIx(	[
<g/>
Office	Office	kA	Office
Products	Products	k1gInSc1	Products
Division	Division	k1gInSc1	Division
<g/>
]	]	kIx)	]
Micro	Micro	k1gNnSc1	Micro
Processor	Processora	k1gFnPc2	Processora
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k6eAd1	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
procesor	procesor	k1gInSc1	procesor
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
mini	mini	k2eAgFnPc4d1	mini
<g/>
"	"	kIx"	"
úlohy	úloha	k1gFnPc4	úloha
a	a	k8xC	a
když	když	k8xS	když
IBM	IBM	kA	IBM
vydalo	vydat	k5eAaPmAgNnS	vydat
IBM	IBM	kA	IBM
RT-PC	RT-PC	k1gMnPc2	RT-PC
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
návrhu	návrh	k1gInSc6	návrh
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
výkon	výkon	k1gInSc1	výkon
nebyl	být	k5eNaImAgInS	být
přijatelný	přijatelný	k2eAgInSc1d1	přijatelný
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
IBM	IBM	kA	IBM
801	[number]	k4	801
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
několik	několik	k4yIc4	několik
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
projektů	projekt	k1gInPc2	projekt
včetně	včetně	k7c2	včetně
nových	nový	k2eAgFnPc2d1	nová
u	u	k7c2	u
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
IBM	IBM	kA	IBM
POWER	POWER	kA	POWER
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prokázali	prokázat	k5eAaPmAgMnP	prokázat
výzkumní	výzkumní	k2eAgMnPc1d1	výzkumní
pracovníci	pracovník	k1gMnPc1	pracovník
IBM	IBM	kA	IBM
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
John	John	k1gMnSc1	John
Cocke	Cock	k1gFnSc2	Cock
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
kombinací	kombinace	k1gFnPc2	kombinace
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
ortogonálních	ortogonální	k2eAgInPc2d1	ortogonální
adresních	adresní	k2eAgInPc2d1	adresní
módů	mód	k1gInPc2	mód
a	a	k8xC	a
instrukcí	instrukce	k1gFnPc2	instrukce
nebyla	být	k5eNaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
programů	program	k1gInPc2	program
generovaných	generovaný	k2eAgInPc2d1	generovaný
kompilátory	kompilátor	k1gInPc7	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
napsat	napsat	k5eAaPmF	napsat
kompilátor	kompilátor	k1gInSc4	kompilátor
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
schopností	schopnost	k1gFnSc7	schopnost
využívat	využívat	k5eAaImF	využívat
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
klasické	klasický	k2eAgFnSc2d1	klasická
CPU	CPU	kA	CPU
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
implementovaných	implementovaný	k2eAgFnPc6d1	implementovaná
architekturách	architektura	k1gFnPc6	architektura
byly	být	k5eAaImAgFnP	být
složité	složitý	k2eAgFnPc1d1	složitá
operace	operace	k1gFnPc1	operace
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
než	než	k8xS	než
posloupnost	posloupnost	k1gFnSc1	posloupnost
jednodušších	jednoduchý	k2eAgFnPc2d2	jednodušší
operací	operace	k1gFnPc2	operace
vykonávající	vykonávající	k2eAgFnSc4d1	vykonávající
stejnou	stejný	k2eAgFnSc4d1	stejná
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
části	část	k1gFnSc2	část
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgInPc1d1	mnohý
návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
dělány	dělat	k5eAaImNgInP	dělat
ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
<g/>
,	,	kIx,	,
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
času	čas	k1gInSc2	čas
na	na	k7c4	na
optimalizaci	optimalizace	k1gFnSc4	optimalizace
či	či	k8xC	či
doladění	doladění	k1gNnSc4	doladění
každé	každý	k3xTgFnSc2	každý
instrukce	instrukce	k1gFnSc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
však	však	k9	však
doladěny	doladit	k5eAaPmNgFnP	doladit
pouze	pouze	k6eAd1	pouze
nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgFnPc4d1	používaná
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
neblaze	blaze	k6eNd1	blaze
známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
byla	být	k5eAaImAgFnS	být
instrukce	instrukce	k1gFnSc1	instrukce
VAX	VAX	kA	VAX
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
INDEX	index	k1gInSc1	index
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
paměť	paměť	k1gFnSc1	paměť
byla	být	k5eAaImAgFnS	být
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
než	než	k8xS	než
mnohé	mnohé	k1gNnSc1	mnohé
CPU	CPU	kA	CPU
<g/>
.	.	kIx.	.
</s>
<s>
Nástupem	nástup	k1gInSc7	nástup
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
pamětí	paměť	k1gFnPc2	paměť
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
snížil	snížit	k5eAaPmAgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
stále	stále	k6eAd1	stále
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
registrů	registr	k1gInPc2	registr
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vyšší	vysoký	k2eAgFnSc4d2	vyšší
operační	operační	k2eAgFnSc4d1	operační
frekvenci	frekvence	k1gFnSc4	frekvence
CPU	CPU	kA	CPU
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
registry	registr	k1gInPc1	registr
by	by	kYmCp3nP	by
vyžadovaly	vyžadovat	k5eAaImAgInP	vyžadovat
další	další	k2eAgInSc4d1	další
čip	čip	k1gInSc4	čip
nebo	nebo	k8xC	nebo
board	board	k1gInSc4	board
areas	areasa	k1gFnPc2	areasa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nP	by
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
reálné	reálný	k2eAgNnSc1d1	reálné
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
snížení	snížení	k1gNnSc1	snížení
logické	logický	k2eAgFnSc2d1	logická
složitosti	složitost	k1gFnSc2	složitost
CPU	CPU	kA	CPU
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgInSc4	jeden
impulz	impulz	k1gInSc4	impulz
jak	jak	k8xC	jak
RISC	RISC	kA	RISC
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
návrhy	návrh	k1gInPc1	návrh
přišly	přijít	k5eAaPmAgFnP	přijít
z	z	k7c2	z
praktických	praktický	k2eAgNnPc2d1	praktické
měření	měření	k1gNnPc2	měření
do	do	k7c2	do
světa	svět	k1gInSc2	svět
reálných	reálný	k2eAgInPc2d1	reálný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Andrew	Andrew	k?	Andrew
Tanenbaum	Tanenbaum	k1gInSc1	Tanenbaum
shrnul	shrnout	k5eAaPmAgInS	shrnout
mnoho	mnoho	k4c4	mnoho
ukázek	ukázka	k1gFnPc2	ukázka
<g/>
,	,	kIx,	,
že	že	k8xS	že
procesory	procesor	k1gInPc1	procesor
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
bezprostřední	bezprostřední	k2eAgInPc4d1	bezprostřední
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
98	[number]	k4	98
<g/>
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
konstant	konstanta	k1gFnPc2	konstanta
v	v	k7c6	v
programu	program	k1gInSc6	program
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vešlo	vejít	k5eAaPmAgNnS	vejít
do	do	k7c2	do
13	[number]	k4	13
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
ale	ale	k8xC	ale
mnoho	mnoho	k6eAd1	mnoho
CPU	CPU	kA	CPU
vyhrazovalo	vyhrazovat	k5eAaImAgNnS	vyhrazovat
16	[number]	k4	16
nebo	nebo	k8xC	nebo
32	[number]	k4	32
bitů	bit	k1gInPc2	bit
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
uložení	uložení	k1gNnSc4	uložení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
počtu	počet	k1gInSc2	počet
přístupů	přístup	k1gInPc2	přístup
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
proměnná	proměnný	k2eAgFnSc1d1	proměnná
délka	délka	k1gFnSc1	délka
konstant	konstanta	k1gFnPc2	konstanta
sama	sám	k3xTgFnSc1	sám
uložit	uložit	k5eAaPmF	uložit
do	do	k7c2	do
nepoužívaných	používaný	k2eNgFnPc2d1	nepoužívaná
částí	část	k1gFnPc2	část
instrukčního	instrukční	k2eAgNnSc2d1	instrukční
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
okamžitě	okamžitě	k6eAd1	okamžitě
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gMnPc4	on
procesor	procesor	k1gInSc1	procesor
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
adresování	adresování	k1gNnSc4	adresování
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
designu	design	k1gInSc6	design
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
malé	malý	k2eAgInPc4d1	malý
operační	operační	k2eAgInSc4d1	operační
kódy	kód	k1gInPc4	kód
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ponechal	ponechat	k5eAaPmAgInS	ponechat
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
rozumně	rozumně	k6eAd1	rozumně
velkou	velký	k2eAgFnSc4d1	velká
konstantu	konstanta	k1gFnSc4	konstanta
v	v	k7c6	v
32	[number]	k4	32
bitovém	bitový	k2eAgNnSc6d1	bitové
instrukčním	instrukční	k2eAgNnSc6d1	instrukční
slově	slovo	k1gNnSc6	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
CPU	CPU	kA	CPU
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
disponovaly	disponovat	k5eAaBmAgInP	disponovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezenou	omezený	k2eAgFnSc7d1	omezená
instrukční	instrukční	k2eAgFnSc7d1	instrukční
sadou	sada	k1gFnSc7	sada
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
návrhy	návrh	k1gInPc1	návrh
dost	dost	k6eAd1	dost
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
klasických	klasický	k2eAgInPc2d1	klasický
návrhů	návrh	k1gInPc2	návrh
RICS	RICS	kA	RICS
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
jiný	jiný	k2eAgInSc4d1	jiný
název	název	k1gInSc4	název
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
MISC	MISC	kA	MISC
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Minimální	minimální	k2eAgFnSc1d1	minimální
instrukční	instrukční	k2eAgFnSc1d1	instrukční
sada	sada	k1gFnSc1	sada
počítače	počítač	k1gInSc2	počítač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
též	též	k6eAd1	též
známy	znám	k2eAgInPc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Transport	transport	k1gInSc1	transport
triggered	triggered	k1gInSc1	triggered
architecture	architectur	k1gMnSc5	architectur
(	(	kIx(	(
<g/>
TTA	TTA	kA	TTA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
veškerou	veškerý	k3xTgFnSc4	veškerý
snahu	snaha	k1gFnSc4	snaha
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
architektura	architektura	k1gFnSc1	architektura
RISC	RISC	kA	RISC
ovládnout	ovládnout	k5eAaPmF	ovládnout
desktopové	desktopový	k2eAgInPc4d1	desktopový
počítače	počítač	k1gInPc4	počítač
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
servery	server	k1gInPc4	server
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dominoval	dominovat	k5eAaImAgInS	dominovat
Intel	Intel	kA	Intel
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
platformou	platforma	k1gFnSc7	platforma
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
důvody	důvod	k1gInPc1	důvod
<g/>
:	:	kIx,	:
Opravdu	opravdu	k6eAd1	opravdu
velmi	velmi	k6eAd1	velmi
velká	velký	k2eAgFnSc1d1	velká
základna	základna	k1gFnSc1	základna
proprietárních	proprietární	k2eAgFnPc2d1	proprietární
počítačových	počítačový	k2eAgFnPc2d1	počítačová
aplikací	aplikace	k1gFnPc2	aplikace
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
pro	pro	k7c4	pro
platformu	platforma	k1gFnSc4	platforma
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
platformy	platforma	k1gFnSc2	platforma
zkompilována	zkompilován	k2eAgFnSc1d1	zkompilován
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
RISC	RISC	kA	RISC
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
takovou	takový	k3xDgFnSc4	takový
podobnou	podobný	k2eAgFnSc4d1	podobná
architekturu	architektura	k1gFnSc4	architektura
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
uživatelé	uživatel	k1gMnPc1	uživatel
zamčeni	zamčen	k2eAgMnPc1d1	zamčen
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
RISC	RISC	kA	RISC
schopen	schopen	k2eAgMnSc1d1	schopen
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
levně	levně	k6eAd1	levně
škálovat	škálovat	k5eAaPmF	škálovat
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
Intel	Intel	kA	Intel
využil	využít	k5eAaPmAgMnS	využít
své	své	k1gNnSc4	své
stabilní	stabilní	k2eAgFnSc2d1	stabilní
velké	velký	k2eAgFnSc2d1	velká
základny	základna	k1gFnSc2	základna
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
začal	začít	k5eAaPmAgMnS	začít
utrácet	utrácet	k5eAaImF	utrácet
za	za	k7c4	za
vývoj	vývoj	k1gInSc4	vývoj
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
utrácet	utrácet	k5eAaImF	utrácet
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
a	a	k8xC	a
častěji	často	k6eAd2	často
než	než	k8xS	než
jakýkoliv	jakýkoliv	k3yIgMnSc1	jakýkoliv
výrobce	výrobce	k1gMnSc1	výrobce
RISC	RISC	kA	RISC
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc4	týž
nelze	lze	k6eNd1	lze
říci	říct	k5eAaPmF	říct
o	o	k7c6	o
menších	malý	k2eAgFnPc6d2	menší
společnostech	společnost	k1gFnPc6	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Cyrix	Cyrix	kA	Cyrix
nebo	nebo	k8xC	nebo
NexGen	NexGna	k1gFnPc2	NexGna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
myšlenky	myšlenka	k1gFnSc2	myšlenka
využít	využít	k5eAaPmF	využít
užšího	úzký	k2eAgInSc2d2	užší
pipeliningu	pipelining	k1gInSc2	pipelining
i	i	k9	i
na	na	k7c4	na
x	x	k?	x
<g/>
86	[number]	k4	86
architektuře	architektura	k1gFnSc3	architektura
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
486	[number]	k4	486
nebo	nebo	k8xC	nebo
Pentiu	Pentius	k1gMnSc3	Pentius
<g/>
.	.	kIx.	.
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Cyrix	Cyrix	kA	Cyrix
udělala	udělat	k5eAaPmAgFnS	udělat
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
dalo	dát	k5eAaPmAgNnS	dát
hovořit	hovořit	k5eAaImF	hovořit
jako	jako	k9	jako
o	o	k7c4	o
vyspělé	vyspělý	k2eAgFnPc4d1	vyspělá
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
využila	využít	k5eAaPmAgFnS	využít
superskalární	superskalární	k2eAgNnSc4d1	superskalární
spekulativní	spekulativní	k2eAgNnSc4d1	spekulativní
vykonávání	vykonávání	k1gNnSc4	vykonávání
kódu	kód	k1gInSc2	kód
pomocí	pomocí	k7c2	pomocí
přejmenování	přejmenování	k1gNnSc2	přejmenování
registrů	registr	k1gInPc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Nx	Nx	k1gFnSc4	Nx
<g/>
586	[number]	k4	586
a	a	k8xC	a
AMD	AMD	kA	AMD
K5	K5	k1gFnPc1	K5
udělaly	udělat	k5eAaPmAgFnP	udělat
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřímo	přímo	k6eNd1	přímo
pomocí	pomocí	k7c2	pomocí
ukládání	ukládání	k1gNnSc2	ukládání
dynamického	dynamický	k2eAgInSc2d1	dynamický
mikrokódu	mikrokód	k1gInSc2	mikrokód
do	do	k7c2	do
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
částečným	částečný	k2eAgNnSc7d1	částečné
nezávislým	závislý	k2eNgNnSc7d1	nezávislé
plánováním	plánování	k1gNnSc7	plánování
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
čipy	čip	k1gInPc4	čip
využívající	využívající	k2eAgFnSc4d1	využívající
tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
byly	být	k5eAaImAgInP	být
NexGen	NexGen	k1gInSc4	NexGen
Nx	Nx	k1gFnPc2	Nx
<g/>
586	[number]	k4	586
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
K5	K5	k1gFnSc4	K5
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
procesory	procesor	k1gInPc1	procesor
byly	být	k5eAaImAgInP	být
více	hodně	k6eAd2	hodně
výkonnější	výkonný	k2eAgInPc1d2	výkonnější
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
procesory	procesor	k1gInPc1	procesor
Intel	Intel	kA	Intel
P	P	kA	P
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
K	k	k7c3	k
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
K	k	k7c3	k
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pentium	Pentium	kA	Pentium
4	[number]	k4	4
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
dynamickém	dynamický	k2eAgNnSc6d1	dynamické
ukládání	ukládání	k1gNnSc6	ukládání
do	do	k7c2	do
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
včetně	včetně	k7c2	včetně
implementace	implementace	k1gFnSc2	implementace
základních	základní	k2eAgInPc2d1	základní
plánovací	plánovací	k2eAgMnSc1d1	plánovací
technik	technik	k1gMnSc1	technik
<g/>
.	.	kIx.	.
</s>
<s>
Prováděly	provádět	k5eAaImAgFnP	provádět
i	i	k9	i
samotné	samotný	k2eAgFnPc4d1	samotná
volně	volně	k6eAd1	volně
spojené	spojený	k2eAgFnPc4d1	spojená
superskalární	superskalární	k2eAgFnPc4d1	superskalární
(	(	kIx(	(
<g/>
spekulativní	spekulativní	k2eAgFnPc4d1	spekulativní
<g/>
)	)	kIx)	)
sekvence	sekvence	k1gFnPc4	sekvence
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
generovaly	generovat	k5eAaImAgFnP	generovat
z	z	k7c2	z
několika	několik	k4yIc2	několik
paralelních	paralelní	k2eAgFnPc2d1	paralelní
x	x	k?	x
<g/>
86	[number]	k4	86
dekódovací	dekódovací	k2eAgInSc4d1	dekódovací
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgInP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
RISC	RISC	kA	RISC
a	a	k8xC	a
CISC	CISC	kA	CISC
poměrně	poměrně	k6eAd1	poměrně
značné	značný	k2eAgNnSc4d1	značné
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
nerozeznání	nerozeznání	k1gNnSc3	nerozeznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
RISC	RISC	kA	RISC
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
PA-RISC	PA-RISC	k?	PA-RISC
Advanced	Advanced	k1gInSc1	Advanced
RISC	RISC	kA	RISC
Machine	Machin	k1gInSc5	Machin
CISC	CISC	kA	CISC
</s>
