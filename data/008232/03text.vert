<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
silniční	silniční	k2eAgMnSc1d1	silniční
profesionální	profesionální	k2eAgMnSc1d1	profesionální
cyklista	cyklista	k1gMnSc1	cyklista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
MS	MS	kA	MS
<g/>
)	)	kIx)	)
v	v	k7c6	v
silničním	silniční	k2eAgInSc6d1	silniční
závodě	závod	k1gInSc6	závod
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
jezdil	jezdit	k5eAaImAgMnS	jezdit
za	za	k7c2	za
ProTour	ProToura	k1gFnPc2	ProToura
stáj	stáj	k1gFnSc1	stáj
Cannondale	Cannondala	k1gFnSc3	Cannondala
(	(	kIx(	(
<g/>
Liquigas	Liquigas	k1gInSc1	Liquigas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
závodníkem	závodník	k1gMnSc7	závodník
WorldTour	WorldTour	k1gMnSc1	WorldTour
stáje	stáj	k1gFnSc2	stáj
Tinkoff-Saxo	Tinkoff-Saxo	k1gMnSc1	Tinkoff-Saxo
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
závodí	závodit	k5eAaImIp3nS	závodit
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Bora	Borum	k1gNnSc2	Borum
Hansgrohe	Hansgroh	k1gFnSc2	Hansgroh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
cyklistickou	cyklistický	k2eAgFnSc4d1	cyklistická
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
jako	jako	k9	jako
cyklokrosař	cyklokrosař	k1gMnSc1	cyklokrosař
a	a	k8xC	a
jezdec	jezdec	k1gMnSc1	jezdec
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
juniorským	juniorský	k2eAgMnSc7d1	juniorský
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
třikrát	třikrát	k6eAd1	třikrát
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
etapě	etapa	k1gFnSc6	etapa
a	a	k8xC	a
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
zelený	zelený	k2eAgInSc4d1	zelený
trikot	trikot	k1gInSc4	trikot
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
náskokem	náskok	k1gInSc7	náskok
na	na	k7c4	na
druhého	druhý	k4xOgMnSc4	druhý
Greipela	Greipel	k1gMnSc4	Greipel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
poprvé	poprvé	k6eAd1	poprvé
silniční	silniční	k2eAgInSc1d1	silniční
závod	závod	k1gInSc1	závod
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Richmondu	Richmond	k1gInSc6	Richmond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
2016	[number]	k4	2016
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
dal	dát	k5eAaPmAgMnS	dát
před	před	k7c7	před
silničním	silniční	k2eAgInSc7d1	silniční
závodem	závod	k1gInSc7	závod
přednost	přednost	k1gFnSc1	přednost
disciplíně	disciplína	k1gFnSc6	disciplína
cross-country	crossountr	k1gInPc4	cross-countr
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
35	[number]	k4	35
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
incident	incident	k1gInSc4	incident
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Cavendishem	Cavendish	k1gInSc7	Cavendish
z	z	k7c2	z
Tour	Toura	k1gFnPc2	Toura
de	de	k?	de
France	Franc	k1gMnSc2	Franc
2017	[number]	k4	2017
diskvalifikován	diskvalifikován	k2eAgInSc1d1	diskvalifikován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
plynně	plynně	k6eAd1	plynně
italsky	italsky	k6eAd1	italsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
vítězných	vítězný	k2eAgFnPc2d1	vítězná
etap	etapa	k1gFnPc2	etapa
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc4d1	významný
úspěchy	úspěch	k1gInPc4	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
juniorů	junior	k1gMnPc2	junior
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
na	na	k7c6	na
MS	MS	kA	MS
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
cyklokrosu	cyklokros	k1gInSc6	cyklokros
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
na	na	k7c6	na
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
–	–	k?	–
<g/>
Roubaix	Roubaix	k1gInSc4	Roubaix
juniorů	junior	k1gMnPc2	junior
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
na	na	k7c6	na
GP	GP	kA	GP
Kooperativa	kooperativa	k1gFnSc1	kooperativa
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
na	na	k7c6	na
Dookola	Dookola	k1gFnSc1	Dookola
Mazowsza	Mazowsz	k1gMnSc4	Mazowsz
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
bodovací	bodovací	k2eAgFnSc2d1	bodovací
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mladý	mladý	k2eAgMnSc1d1	mladý
jezdec	jezdec	k1gMnSc1	jezdec
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
2	[number]	k4	2
etap	etapa	k1gFnPc2	etapa
<g/>
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
na	na	k7c6	na
Paříž-Nice	Paříž-Nika	k1gFnSc6	Paříž-Nika
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
bodovací	bodovací	k2eAgFnSc2d1	bodovací
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
etapy	etapa	k1gFnSc2	etapa
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
na	na	k7c4	na
Kolem	kolem	k6eAd1	kolem
Romandie	Romandie	k1gFnPc4	Romandie
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
1	[number]	k4	1
<g/>
.	.	kIx.	.
etapy	etapa	k1gFnSc2	etapa
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
na	na	k7c4	na
Kolem	kolem	k6eAd1	kolem
Kalifornie	Kalifornie	k1gFnPc4	Kalifornie
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
bodovací	bodovací	k2eAgFnSc2d1	bodovací
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mladý	mladý	k2eAgMnSc1d1	mladý
jezdec	jezdec	k1gMnSc1	jezdec
</s>
</p>
<p>
<s>
vítěz	vítěz	k1gMnSc1	vítěz
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
etapy	etapa	k1gFnSc2	etapa
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
na	na	k7c4	na
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
International	International	k1gMnSc1	International
Championship	Championship	k1gMnSc1	Championship
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
Cycliste	Cyclist	k1gMnSc5	Cyclist
de	de	k?	de
Montréal	Montréal	k1gInSc4	Montréal
<g/>
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
Kolem	kolem	k7c2	kolem
Sardínie	Sardínie	k1gFnSc2	Sardínie
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
Kolem	kolem	k7c2	kolem
Polska	Polsko	k1gNnSc2	Polsko
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
Vuelta	Vuelta	k1gFnSc1	Vuelta
a	a	k8xC	a
Españ	Españ	k1gFnSc1	Españ
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
GP	GP	kA	GP
Industria	Industrium	k1gNnSc2	Industrium
&	&	k?	&
Commercio	Commercio	k1gMnSc1	Commercio
di	di	k?	di
Prato	Prat	k2eAgNnSc1d1	Prato
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
International	International	k1gFnSc2	International
Championship	Championship	k1gInSc1	Championship
<g/>
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
Ománu	Omán	k1gInSc2	Omán
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
na	na	k7c6	na
Tirreno	Tirren	k2eAgNnSc4d1	Tirreno
<g/>
–	–	k?	–
<g/>
Adriatico	Adriatico	k6eAd1	Adriatico
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
na	na	k7c4	na
Tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
v	v	k7c6	v
Panne	Pann	k1gInSc5	Pann
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Suisse	Suisse	k1gFnSc2	Suisse
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
soutěž	soutěž	k1gFnSc1	soutěž
kombativity	kombativita	k1gFnSc2	kombativita
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Gent	Gent	k2eAgMnSc1d1	Gent
<g/>
–	–	k?	–
<g/>
Wevelgem	Wevelg	k1gInSc7	Wevelg
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Amstel	Amstel	k1gInSc1	Amstel
Gold	Golda	k1gFnPc2	Golda
Race	Rac	k1gInSc2	Rac
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
-	-	kIx~	-
San	San	k1gMnSc1	San
Remo	Remo	k1gMnSc1	Remo
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Flander	Flandry	k1gInPc2	Flandry
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
UCI	UCI	kA	UCI
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Brabantský	Brabantský	k2eAgInSc1d1	Brabantský
šíp	šíp	k1gInSc1	šíp
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Cycliste	Cyclist	k1gMnSc5	Cyclist
Montréal	Montréal	k1gInSc4	Montréal
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Gent	Gent	k2eAgMnSc1d1	Gent
<g/>
–	–	k?	–
<g/>
Wevelgem	Wevelg	k1gInSc7	Wevelg
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
-	-	kIx~	-
San	San	k1gMnSc1	San
Remo	Remo	k1gMnSc1	Remo
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Flander	Flandry	k1gInPc2	Flandry
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
UCI	UCI	kA	UCI
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
Vuelta	Vuelta	k1gFnSc1	Vuelta
a	a	k8xC	a
Españ	Españ	k1gFnSc1	Españ
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
závodu	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
<g/>
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Flander	Flandry	k1gInPc2	Flandry
</s>
</p>
<p>
<s>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
celkový	celkový	k2eAgMnSc1d1	celkový
největší	veliký	k2eAgMnSc1d3	veliký
bojovník	bojovník	k1gMnSc1	bojovník
ročníku	ročník	k1gInSc2	ročník
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
etapě	etapa	k1gFnSc6	etapa
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
<g/>
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
-	-	kIx~	-
Roubaix	Roubaix	k1gInSc1	Roubaix
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
silniční	silniční	k2eAgInSc1d1	silniční
závod	závod	k1gInSc1	závod
</s>
</p>
<p>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
3	[number]	k4	3
etap	etapa	k1gFnPc2	etapa
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
bodovací	bodovací	k2eAgFnSc6d1	bodovací
soutěži	soutěž	k1gFnSc6	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagany	k1gInPc2	Sagany
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
stáje	stáj	k1gFnSc2	stáj
Liquigas	Liquigasa	k1gFnPc2	Liquigasa
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
–	–	k?	–
talent	talent	k1gInSc1	talent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c2	za
století	století	k1gNnSc2	století
</s>
</p>
