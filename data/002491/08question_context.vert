<s>
Samuel	Samuel	k1gMnSc1	Samuel
Barclay	Barclaa	k1gFnSc2	Barclaa
Beckett	Beckett	k1gMnSc1	Beckett
[	[	kIx(	[
<g/>
Bekit	Bekit	k1gMnSc1	Bekit
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
irský	irský	k2eAgMnSc1d1	irský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
absurdního	absurdní	k2eAgNnSc2d1	absurdní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
Foxrocku	Foxrock	k1gInSc6	Foxrock
nedaleko	nedaleko	k7c2	nedaleko
Dublinu	Dublin	k1gInSc2	Dublin
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc6d1	situovaná
protestantské	protestantský	k2eAgFnSc6d1	protestantská
rodině	rodina	k1gFnSc6	rodina
zeměměřiče	zeměměřič	k1gMnSc2	zeměměřič
Williama	William	k1gMnSc2	William
Becketta	Beckett	k1gMnSc2	Beckett
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
sestry	sestra	k1gFnSc2	sestra
Mary	Mary	k1gFnSc2	Mary
Roeové	Roeová	k1gFnSc2	Roeová
<g/>
.	.	kIx.	.
</s>
