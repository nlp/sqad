<p>
<s>
Peřejník	Peřejník	k1gInSc1	Peřejník
je	být	k5eAaImIp3nS	být
přepážka	přepážka	k1gFnSc1	přepážka
montovaná	montovaný	k2eAgFnSc1d1	montovaná
do	do	k7c2	do
nádrží	nádrž	k1gFnPc2	nádrž
s	s	k7c7	s
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
omezování	omezování	k1gNnSc3	omezování
a	a	k8xC	a
zpomalování	zpomalování	k1gNnSc3	zpomalování
jejího	její	k3xOp3gNnSc2	její
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
desky	deska	k1gFnSc2	deska
s	s	k7c7	s
několika	několik	k4yIc2	několik
otvory	otvor	k1gInPc7	otvor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Peřejník	Peřejník	k1gInSc1	Peřejník
mění	měnit	k5eAaImIp3nS	měnit
proudění	proudění	k1gNnSc1	proudění
kapaliny	kapalina	k1gFnSc2	kapalina
z	z	k7c2	z
laminárního	laminární	k2eAgMnSc2d1	laminární
na	na	k7c4	na
turbulentní	turbulentní	k2eAgInSc4d1	turbulentní
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
proudící	proudící	k2eAgFnPc1d1	proudící
kapalina	kapalina	k1gFnSc1	kapalina
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
rychleji	rychle	k6eAd2	rychle
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
pohyb	pohyb	k1gInSc4	pohyb
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
cisteren	cisterna	k1gFnPc2	cisterna
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
velkých	velký	k2eAgInPc2d1	velký
objemů	objem	k1gInPc2	objem
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
setrvačnost	setrvačnost	k1gFnSc1	setrvačnost
nekontrolovaně	kontrolovaně	k6eNd1	kontrolovaně
proudící	proudící	k2eAgFnSc2d1	proudící
kapaliny	kapalina	k1gFnSc2	kapalina
mohla	moct	k5eAaImAgFnS	moct
velmi	velmi	k6eAd1	velmi
nepříznivě	příznivě	k6eNd1	příznivě
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
jízdní	jízdní	k2eAgFnPc4d1	jízdní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
případem	případ	k1gInSc7	případ
použití	použití	k1gNnSc2	použití
jsou	být	k5eAaImIp3nP	být
automobilové	automobilový	k2eAgFnPc1d1	automobilová
cisterny	cisterna	k1gFnPc1	cisterna
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgInPc1d1	železniční
cisternové	cisternový	k2eAgInPc1d1	cisternový
vozy	vůz	k1gInPc1	vůz
<g/>
,	,	kIx,	,
či	či	k8xC	či
lokomotivní	lokomotivní	k2eAgInSc1d1	lokomotivní
tendr	tendr	k1gInSc1	tendr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Peřejník	Peřejník	k1gMnSc1	Peřejník
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
součástí	součást	k1gFnSc7	součást
konstrukce	konstrukce	k1gFnSc2	konstrukce
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
zpevnění	zpevnění	k1gNnSc4	zpevnění
jejího	její	k3xOp3gInSc2	její
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
o	o	k7c6	o
nesnímatelných	snímatelný	k2eNgFnPc6d1	nesnímatelná
cisternách	cisterna	k1gFnPc6	cisterna
(	(	kIx(	(
<g/>
cisternových	cisternový	k2eAgNnPc6d1	cisternové
vozidlech	vozidlo	k1gNnPc6	vozidlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snímatelných	snímatelný	k2eAgFnPc6d1	snímatelná
cisternách	cisterna	k1gFnPc6	cisterna
a	a	k8xC	a
bateriových	bateriový	k2eAgNnPc6d1	bateriové
vozidlech	vozidlo	k1gNnPc6	vozidlo
<g/>
,	,	kIx,	,
dodatek	dodatek	k1gInSc1	dodatek
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
ADR	ADR	kA	ADR
</s>
</p>
