<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
přepážka	přepážka	k1gFnSc1	přepážka
montovaná	montovaný	k2eAgFnSc1d1	montovaná
do	do	k7c2	do
nádrží	nádrž	k1gFnPc2	nádrž
s	s	k7c7	s
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
omezování	omezování	k1gNnSc3	omezování
a	a	k8xC	a
zpomalování	zpomalování	k1gNnSc3	zpomalování
jejího	její	k3xOp3gNnSc2	její
proudění	proudění	k1gNnSc2	proudění
<g/>
?	?	kIx.	?
</s>
