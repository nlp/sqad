<p>
<s>
David	David	k1gMnSc1	David
Vaněček	Vaněček	k1gMnSc1	Vaněček
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnSc1d1	hrající
za	za	k7c4	za
klub	klub	k1gInSc4	klub
FK	FK	kA	FK
Tachov	Tachov	k1gInSc1	Tachov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
začínal	začínat	k5eAaImAgMnS	začínat
ve	v	k7c6	v
Viktorii	Viktoria	k1gFnSc6	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prošel	projít	k5eAaPmAgInS	projít
všemi	všecek	k3xTgFnPc7	všecek
mládežnickými	mládežnický	k2eAgFnPc7d1	mládežnická
kategoriemi	kategorie	k1gFnPc7	kategorie
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
A-týmu	Aýmo	k1gNnSc6	A-týmo
Plzně	Plzeň	k1gFnSc2	Plzeň
se	se	k3xPyFc4	se
však	však	k9	však
natrvalo	natrvalo	k6eAd1	natrvalo
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
a	a	k8xC	a
po	po	k7c6	po
ročním	roční	k2eAgNnSc6d1	roční
hostování	hostování	k1gNnSc6	hostování
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
do	do	k7c2	do
FK	FK	kA	FK
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Pohár	pohár	k1gInSc4	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
po	po	k7c6	po
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Baníku	Baník	k1gInSc2	Baník
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
propracoval	propracovat	k5eAaPmAgInS	propracovat
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
týmu	tým	k1gInSc6	tým
sešel	sejít	k5eAaPmAgMnS	sejít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mladším	mladý	k2eAgMnSc7d2	mladší
bratrancem	bratranec	k1gMnSc7	bratranec
Davidem	David	k1gMnSc7	David
Vaněčkem	Vaněček	k1gMnSc7	Vaněček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Sokolově	Sokolov	k1gInSc6	Sokolov
hostoval	hostovat	k5eAaImAgInS	hostovat
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
transfermarkt	transfermarkt	k1gInSc4	transfermarkt
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
</s>
</p>
