<s>
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
bos	bos	k1gMnSc1	bos
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jheronimus	Jheronimus	k1gInSc1	Jheronimus
Anthonissoen	Anthonissoen	k1gInSc1	Anthonissoen
van	vana	k1gFnPc2	vana
Aken	Akena	k1gFnPc2	Akena
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1450	[number]	k4	1450
–	–	k?	–
cca	cca	kA	cca
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1516	[number]	k4	1516
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
přední	přední	k2eAgMnSc1d1	přední
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
malíř	malíř	k1gMnSc1	malíř
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tvořil	tvořit	k5eAaImAgMnS	tvořit
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
nizozemské	nizozemský	k2eAgFnSc6d1	nizozemská
renesanci	renesance	k1gFnSc6	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
prací	práce	k1gFnPc2	práce
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
hříchy	hřích	k1gInPc4	hřích
a	a	k8xC	a
morální	morální	k2eAgInSc4d1	morální
úpadek	úpadek	k1gInSc4	úpadek
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Bosch	Bosch	kA	Bosch
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
zobrazoval	zobrazovat	k5eAaImAgMnS	zobrazovat
člověka	člověk	k1gMnSc2	člověk
z	z	k7c2	z
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
stránky	stránka	k1gFnSc2	stránka
racionálně	racionálně	k6eAd1	racionálně
pojaté	pojatý	k2eAgFnSc2d1	pojatá
jako	jako	k8xS	jako
démony	démon	k1gMnPc4	démon
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
komplexně	komplexně	k6eAd1	komplexně
pojatá	pojatý	k2eAgNnPc1d1	pojaté
<g/>
,	,	kIx,	,
originální	originální	k2eAgNnPc1d1	originální
<g/>
,	,	kIx,	,
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
hluboce	hluboko	k6eAd1	hluboko
promyšlenou	promyšlený	k2eAgFnSc7d1	promyšlená
symbolikou	symbolika	k1gFnSc7	symbolika
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
ikonografie	ikonografie	k1gFnSc2	ikonografie
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
dokonce	dokonce	k9	dokonce
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
kacířská	kacířský	k2eAgNnPc4d1	kacířské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přišel	přijít	k5eAaPmAgMnS	přijít
německý	německý	k2eAgMnSc1d1	německý
historik	historik	k1gMnSc1	historik
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Fraenger	Fraenger	k1gMnSc1	Fraenger
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bosch	Bosch	kA	Bosch
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
heretické	heretický	k2eAgFnSc2d1	heretická
skupiny	skupina	k1gFnSc2	skupina
Bratři	bratr	k1gMnPc1	bratr
volného	volný	k2eAgMnSc2d1	volný
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
dějepis	dějepis	k1gInSc1	dějepis
umění	umění	k1gNnSc2	umění
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
tezi	teze	k1gFnSc4	teze
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Boschovo	Boschův	k2eAgNnSc1d1	Boschovo
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
také	také	k9	také
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
inspiračních	inspirační	k2eAgInPc2d1	inspirační
zdrojů	zdroj	k1gInPc2	zdroj
pro	pro	k7c4	pro
surrealismus	surrealismus	k1gInSc4	surrealismus
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
jako	jako	k9	jako
Jheronimus	Jheronimus	k1gInSc1	Jheronimus
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Jeroen	Jeroen	k2eAgInSc1d1	Jeroen
<g/>
)	)	kIx)	)
van	van	k1gInSc1	van
Aken	Akena	k1gFnPc2	Akena
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
Cách	Cáchy	k1gFnPc2	Cáchy
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Aken	Aken	k1gNnSc1	Aken
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
ovšem	ovšem	k9	ovšem
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
jako	jako	k9	jako
Jheronimus	Jheronimus	k1gMnSc1	Jheronimus
Bosch	Bosch	kA	Bosch
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
svého	svůj	k3xOyFgNnSc2	svůj
narození	narození	k1gNnSc2	narození
'	'	kIx"	'
<g/>
s-Hertogenbosch	s-Hertogenbosch	k1gInSc1	s-Hertogenbosch
(	(	kIx(	(
<g/>
také	také	k9	také
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
Den	den	k1gInSc4	den
Bosch	Bosch	kA	Bosch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Boschově	Boschův	k2eAgInSc6d1	Boschův
životě	život	k1gInSc6	život
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgInPc1d1	jediný
písemné	písemný	k2eAgInPc1d1	písemný
doklady	doklad	k1gInPc1	doklad
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
městě	město	k1gNnSc6	město
'	'	kIx"	'
<g/>
s-Hertogenbosch	s-Hertogenbosch	k1gMnSc1	s-Hertogenbosch
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
účetních	účetní	k2eAgFnPc6d1	účetní
knihách	kniha	k1gFnPc6	kniha
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgFnPc4	žádný
informace	informace	k1gFnPc4	informace
proto	proto	k8xC	proto
neexistují	existovat	k5eNaImIp3nP	existovat
ani	ani	k8xC	ani
o	o	k7c6	o
charakteru	charakter	k1gInSc6	charakter
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
nelze	lze	k6eNd1	lze
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
kresba	kresba	k1gFnSc1	kresba
tužkou	tužka	k1gFnSc7	tužka
a	a	k8xC	a
hlinkou	hlinka	k1gFnSc7	hlinka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
autoportrét	autoportrét	k1gInSc4	autoportrét
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
originál	originál	k1gInSc4	originál
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
kopii	kopie	k1gFnSc4	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
originál	originál	k1gInSc1	originál
kresby	kresba	k1gFnSc2	kresba
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
Boschovou	Boschův	k2eAgFnSc7d1	Boschova
smrtí	smrt	k1gFnSc7	smrt
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
malíř	malíř	k1gMnSc1	malíř
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1450	[number]	k4	1450
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc1	portrét
totiž	totiž	k9	totiž
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
umělce	umělec	k1gMnSc4	umělec
v	v	k7c6	v
pokročilém	pokročilý	k2eAgInSc6d1	pokročilý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
šedesáti	šedesát	k4xCc6	šedesát
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bosch	Bosch	kA	Bosch
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
strávil	strávit	k5eAaPmAgMnS	strávit
poblíž	poblíž	k6eAd1	poblíž
'	'	kIx"	'
<g/>
s-Hertogenbosche	s-Hertogenbosche	k1gInSc4	s-Hertogenbosche
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
provincie	provincie	k1gFnSc2	provincie
Brabantsko	Brabantsko	k1gNnSc1	Brabantsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
Jan	Jan	k1gMnSc1	Jan
van	vana	k1gFnPc2	vana
Aken	Aken	k1gMnSc1	Aken
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1454	[number]	k4	1454
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1430	[number]	k4	1430
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
pět	pět	k4xCc4	pět
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejméně	málo	k6eAd3	málo
čtyři	čtyři	k4xCgMnPc1	čtyři
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
také	také	k9	také
malíři	malíř	k1gMnPc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
velice	velice	k6eAd1	velice
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boschův	Boschův	k2eAgMnSc1d1	Boschův
otec	otec	k1gMnSc1	otec
nebo	nebo	k8xC	nebo
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
strýců	strýc	k1gMnPc2	strýc
ho	on	k3xPp3gMnSc4	on
vyučil	vyučit	k5eAaPmAgMnS	vyučit
malbě	malba	k1gFnSc3	malba
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jejich	jejich	k3xOp3gFnPc1	jejich
práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Boschův	Boschův	k2eAgMnSc1d1	Boschův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Anthonius	Anthonius	k1gMnSc1	Anthonius
van	vana	k1gFnPc2	vana
Aken	Aken	k1gMnSc1	Aken
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
poradcem	poradce	k1gMnSc7	poradce
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
záznamech	záznam	k1gInPc6	záznam
roku	rok	k1gInSc2	rok
1474	[number]	k4	1474
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
vedle	vedle	k7c2	vedle
svých	svůj	k3xOyFgMnPc2	svůj
dvou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
jménem	jméno	k1gNnSc7	jméno
Goossen	Goossen	k2eAgInSc1d1	Goossen
byl	být	k5eAaImAgInS	být
také	také	k9	také
malířem	malíř	k1gMnSc7	malíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
'	'	kIx"	'
<g/>
s-Hertogenbosch	s-Hertogenbosch	k1gInSc1	s-Hertogenbosch
prosperujícím	prosperující	k2eAgNnSc7d1	prosperující
centrem	centrum	k1gNnSc7	centrum
Brabantska	Brabantsko	k1gNnSc2	Brabantsko
(	(	kIx(	(
<g/>
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1463	[number]	k4	1463
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
městě	město	k1gNnSc6	město
ohromným	ohromný	k2eAgInSc7d1	ohromný
požárem	požár	k1gInSc7	požár
zničeno	zničit	k5eAaPmNgNnS	zničit
přibližně	přibližně	k6eAd1	přibližně
4000	[number]	k4	4000
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
byl	být	k5eAaImAgMnS	být
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
)	)	kIx)	)
tehdy	tehdy	k6eAd1	tehdy
třináctiletý	třináctiletý	k2eAgInSc1d1	třináctiletý
Bosch	Bosch	kA	Bosch
svědkem	svědek	k1gMnSc7	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
malířem	malíř	k1gMnSc7	malíř
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
již	již	k6eAd1	již
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
a	a	k8xC	a
často	často	k6eAd1	často
přijímal	přijímat	k5eAaImAgMnS	přijímat
také	také	k9	také
návštěvy	návštěva	k1gFnPc4	návštěva
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1480	[number]	k4	1480
<g/>
–	–	k?	–
<g/>
1481	[number]	k4	1481
si	se	k3xPyFc3	se
Bosch	Bosch	kA	Bosch
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
Aleyt	Aleyt	k1gInSc4	Aleyt
Goyaerts	Goyaerts	k1gInSc1	Goyaerts
van	van	k1gInSc1	van
den	den	k1gInSc1	den
Meerven	Meerven	k2eAgInSc1d1	Meerven
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1453	[number]	k4	1453
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
městečka	městečko	k1gNnSc2	městečko
Oirschot	Oirschota	k1gFnPc2	Oirschota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
zdědila	zdědit	k5eAaPmAgFnS	zdědit
dům	dům	k1gInSc4	dům
a	a	k8xC	a
pozemky	pozemek	k1gInPc4	pozemek
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
zámožné	zámožný	k2eAgFnSc6d1	zámožná
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
Boschovo	Boschův	k2eAgNnSc1d1	Boschovo
jméno	jméno	k1gNnSc1	jméno
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
registrech	registr	k1gInPc6	registr
členů	člen	k1gMnPc2	člen
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1486	[number]	k4	1486
nebo	nebo	k8xC	nebo
1487	[number]	k4	1487
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
něho	on	k3xPp3gNnSc2	on
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
v	v	k7c6	v
'	'	kIx"	'
<g/>
s-Hertogenboschi	s-Hertogenbosch	k1gFnPc4	s-Hertogenbosch
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1318	[number]	k4	1318
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
severním	severní	k2eAgNnSc6d1	severní
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
členy	člen	k1gInPc7	člen
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
řádoví	řádový	k2eAgMnPc1d1	řádový
kněží	kněz	k1gMnPc1	kněz
i	i	k9	i
světské	světský	k2eAgFnPc4d1	světská
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
městech	město	k1gNnPc6	město
měla	mít	k5eAaImAgFnS	mít
bratrstva	bratrstvo	k1gNnPc1	bratrstvo
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
náboženském	náboženský	k2eAgMnSc6d1	náboženský
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc6d1	kulturní
životě	život	k1gInSc6	život
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
rodiny	rodina	k1gFnSc2	rodina
van	van	k1gInSc1	van
Akenů	Aken	k1gMnPc2	Aken
byli	být	k5eAaImAgMnP	být
většinou	většina	k1gFnSc7	většina
členy	člen	k1gMnPc7	člen
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
a	a	k8xC	a
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
umělecké	umělecký	k2eAgFnPc4d1	umělecká
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1488	[number]	k4	1488
předsedal	předsedat	k5eAaImAgMnS	předsedat
Bosch	Bosch	kA	Bosch
výročnímu	výroční	k2eAgInSc3d1	výroční
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
labutímu	labutí	k2eAgInSc3d1	labutí
banketu	banket	k1gInSc3	banket
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1504	[number]	k4	1504
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
výroční	výroční	k2eAgFnSc4d1	výroční
hostinu	hostina	k1gFnSc4	hostina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc4	on
burgundský	burgundský	k2eAgMnSc1d1	burgundský
vévoda	vévoda	k1gMnSc1	vévoda
Filip	Filip	k1gMnSc1	Filip
Sličný	sličný	k2eAgMnSc1d1	sličný
objednal	objednat	k5eAaPmAgInS	objednat
obraz	obraz	k1gInSc1	obraz
Posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vyobrazeno	vyobrazen	k2eAgNnSc4d1	vyobrazeno
peklo	peklo	k1gNnSc4	peklo
i	i	k8xC	i
ráj	ráj	k1gInSc4	ráj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písemnostech	písemnost	k1gFnPc6	písemnost
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
se	se	k3xPyFc4	se
Bosch	Bosch	kA	Bosch
objevuje	objevovat	k5eAaImIp3nS	objevovat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1480	[number]	k4	1480
nebo	nebo	k8xC	nebo
1481	[number]	k4	1481
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgMnS	být
jeho	jeho	k3xOp3gInSc7	jeho
členem	člen	k1gInSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
dostával	dostávat	k5eAaImAgMnS	dostávat
od	od	k7c2	od
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
různé	různý	k2eAgFnPc4d1	různá
zakázky	zakázka	k1gFnPc4	zakázka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
roku	rok	k1gInSc2	rok
1493	[number]	k4	1493
návrh	návrh	k1gInSc4	návrh
vitráže	vitráž	k1gFnSc2	vitráž
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
kaple	kaple	k1gFnSc2	kaple
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
pečovalo	pečovat	k5eAaImAgNnS	pečovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1511	[number]	k4	1511
navrhoval	navrhovat	k5eAaImAgInS	navrhovat
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
svícen	svícen	k2eAgMnSc1d1	svícen
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
mizivým	mizivý	k2eAgFnPc3d1	mizivá
odměnám	odměna	k1gFnPc3	odměna
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
za	za	k7c4	za
tyto	tento	k3xDgFnPc4	tento
práce	práce	k1gFnPc4	práce
obdržel	obdržet	k5eAaPmAgMnS	obdržet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jeho	jeho	k3xOp3gInPc4	jeho
dary	dar	k1gInPc4	dar
bratrstvu	bratrstvo	k1gNnSc6	bratrstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápisech	zápis	k1gInPc6	zápis
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenán	k2eAgNnSc4d1	zaznamenáno
Boschovo	Boschův	k2eAgNnSc4d1	Boschovo
úmrtí	úmrtí	k1gNnSc4	úmrtí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
<g/>
.	.	kIx.	.
</s>
<s>
Zádušní	zádušní	k2eAgFnSc1d1	zádušní
mše	mše	k1gFnSc1	mše
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
'	'	kIx"	'
<g/>
s-Hertogenboschi	s-Hertogenbosch	k1gFnSc6	s-Hertogenbosch
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Bosch	Bosch	kA	Bosch
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
rané	raný	k2eAgFnSc3d1	raná
tvorbě	tvorba	k1gFnSc3	tvorba
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
tvorbou	tvorba	k1gFnSc7	tvorba
iluminace	iluminace	k1gFnPc1	iluminace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
k	k	k7c3	k
pokračovatelům	pokračovatel	k1gMnPc3	pokračovatel
doznívající	doznívající	k2eAgFnSc2d1	doznívající
gotiky	gotika	k1gFnSc2	gotika
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
středověké	středověký	k2eAgFnSc2d1	středověká
knižní	knižní	k2eAgFnSc2d1	knižní
malby	malba	k1gFnSc2	malba
–	–	k?	–
jak	jak	k8xS	jak
patrno	patrn	k2eAgNnSc1d1	patrno
z	z	k7c2	z
detailů	detail	k1gInPc2	detail
maleb	malba	k1gFnPc2	malba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
někdy	někdy	k6eAd1	někdy
miniatury	miniatura	k1gFnPc4	miniatura
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
narativností	narativnost	k1gFnPc2	narativnost
a	a	k8xC	a
moralizujícími	moralizující	k2eAgNnPc7d1	moralizující
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
zápas	zápas	k1gInSc4	zápas
dobra	dobro	k1gNnSc2	dobro
se	s	k7c7	s
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
Bosch	Bosch	kA	Bosch
vyobrazuje	vyobrazovat	k5eAaImIp3nS	vyobrazovat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
fantazií	fantazie	k1gFnSc7	fantazie
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
lidských	lidský	k2eAgNnPc2d1	lidské
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
démony	démon	k1gMnPc4	démon
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
komplexně	komplexně	k6eAd1	komplexně
pojatá	pojatý	k2eAgNnPc1d1	pojaté
<g/>
,	,	kIx,	,
originální	originální	k2eAgNnPc1d1	originální
<g/>
,	,	kIx,	,
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
hluboce	hluboko	k6eAd1	hluboko
promyšlenou	promyšlený	k2eAgFnSc7d1	promyšlená
symbolikou	symbolika	k1gFnSc7	symbolika
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
ikonografie	ikonografie	k1gFnSc2	ikonografie
<g/>
.	.	kIx.	.
</s>
<s>
Bosch	Bosch	kA	Bosch
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
několik	několik	k4yIc4	několik
triptychů	triptych	k1gInPc2	triptych
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Zahrada	zahrada	k1gFnSc1	zahrada
pozemských	pozemský	k2eAgFnPc2d1	pozemská
rozkoší	rozkoš	k1gFnPc2	rozkoš
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
malba	malba	k1gFnSc1	malba
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
ráj	ráj	k1gInSc4	ráj
s	s	k7c7	s
Adamem	Adam	k1gMnSc7	Adam
a	a	k8xC	a
Evou	Eva	k1gFnSc7	Eva
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
podivuhodnými	podivuhodný	k2eAgNnPc7d1	podivuhodné
zvířaty	zvíře	k1gNnPc7	zvíře
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
pozemské	pozemský	k2eAgFnPc1d1	pozemská
rozkoše	rozkoš	k1gFnPc1	rozkoš
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
nahých	nahý	k2eAgFnPc2d1	nahá
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
s	s	k7c7	s
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
děsivými	děsivý	k2eAgMnPc7d1	děsivý
ptáky	pták	k1gMnPc7	pták
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
a	a	k8xC	a
fantazijním	fantazijní	k2eAgNnSc7d1	fantazijní
zobrazením	zobrazení	k1gNnSc7	zobrazení
hudebního	hudební	k2eAgNnSc2d1	hudební
pekla	peklo	k1gNnSc2	peklo
trestajícího	trestající	k2eAgInSc2d1	trestající
hříchy	hřích	k1gInPc4	hřích
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
panelu	panel	k1gInSc6	panel
triptychu	triptych	k1gInSc2	triptych
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zavření	zavření	k1gNnSc6	zavření
postranních	postranní	k2eAgInPc2d1	postranní
panelů	panel	k1gInPc2	panel
může	moct	k5eAaImIp3nS	moct
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
vidět	vidět	k5eAaImF	vidět
Boha	bůh	k1gMnSc4	bůh
tvořícího	tvořící	k2eAgMnSc4d1	tvořící
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
malířově	malířův	k2eAgFnSc6d1	malířova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgInS	získat
řadu	řada	k1gFnSc4	řada
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
Pradu	Prado	k1gNnSc6	Prado
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
nechybí	chybit	k5eNaPmIp3nS	chybit
ani	ani	k9	ani
Zahrada	zahrada	k1gFnSc1	zahrada
pozemských	pozemský	k2eAgFnPc2d1	pozemská
rozkoší	rozkoš	k1gFnPc2	rozkoš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
stoletích	století	k1gNnPc6	století
se	se	k3xPyFc4	se
často	často	k6eAd1	často
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boschova	Boschův	k2eAgNnSc2d1	Boschovo
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
středověkou	středověký	k2eAgFnSc7d1	středověká
herezí	hereze	k1gFnSc7	hereze
a	a	k8xC	a
tajemnými	tajemný	k2eAgFnPc7d1	tajemná
okultními	okultní	k2eAgFnPc7d1	okultní
praktikami	praktika	k1gFnPc7	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
soudí	soudit	k5eAaImIp3nP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
jenom	jenom	k9	jenom
jako	jako	k8xC	jako
provokace	provokace	k1gFnSc1	provokace
a	a	k8xC	a
zábava	zábava	k1gFnSc1	zábava
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
groteska	groteska	k1gFnSc1	groteska
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
renesanci	renesance	k1gFnSc6	renesance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
známých	známý	k2eAgInPc6d1	známý
záznamech	záznam	k1gInPc6	záznam
Boschových	Boschová	k1gFnPc6	Boschová
děl	dělo	k1gNnPc2	dělo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1560	[number]	k4	1560
<g/>
,	,	kIx,	,
Felipe	Felip	k1gInSc5	Felip
de	de	k?	de
Guevara	Guevar	k1gMnSc2	Guevar
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bosch	Bosch	kA	Bosch
byl	být	k5eAaImAgInS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
skoro	skoro	k6eAd1	skoro
za	za	k7c7	za
"	"	kIx"	"
<g/>
vynálezce	vynálezce	k1gMnSc1	vynálezce
nestvůr	nestvůra	k1gFnPc2	nestvůra
a	a	k8xC	a
přízraků	přízrak	k1gInPc2	přízrak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
díla	dílo	k1gNnSc2	dílo
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
duševním	duševní	k2eAgInSc6d1	duševní
světě	svět	k1gInSc6	svět
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Bosch	Bosch	kA	Bosch
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
konfrontoval	konfrontovat	k5eAaBmAgInS	konfrontovat
svůj	svůj	k3xOyFgInSc4	svůj
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
badatelé	badatel	k1gMnPc1	badatel
začali	začít	k5eAaPmAgMnP	začít
vidět	vidět	k5eAaImF	vidět
Boschovy	Boschův	k2eAgFnPc4d1	Boschova
vize	vize	k1gFnPc4	vize
jako	jako	k8xC	jako
méně	málo	k6eAd2	málo
fantastické	fantastický	k2eAgNnSc4d1	fantastické
a	a	k8xC	a
akceptovali	akceptovat	k5eAaBmAgMnP	akceptovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
reflektují	reflektovat	k5eAaImIp3nP	reflektovat
ortodoxní	ortodoxní	k2eAgFnSc4d1	ortodoxní
víru	víra	k1gFnSc4	víra
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zobrazení	zobrazení	k1gNnSc1	zobrazení
hříšného	hříšný	k2eAgNnSc2d1	hříšné
lidstva	lidstvo	k1gNnSc2	lidstvo
odráží	odrážet	k5eAaImIp3nS	odrážet
pojetí	pojetí	k1gNnSc1	pojetí
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
z	z	k7c2	z
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
výkladové	výkladový	k2eAgFnSc2d1	výkladová
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
kázání	kázání	k1gNnSc2	kázání
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c4	v
interpretaci	interpretace	k1gFnSc4	interpretace
díla	dílo	k1gNnSc2	dílo
sehráli	sehrát	k5eAaPmAgMnP	sehrát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
také	také	k9	také
surrealisté	surrealista	k1gMnPc1	surrealista
<g/>
,	,	kIx,	,
především	především	k9	především
potom	potom	k6eAd1	potom
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
a	a	k8xC	a
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
Boschovi	Bosch	k1gMnSc6	Bosch
také	také	k9	také
nalezli	nalézt	k5eAaBmAgMnP	nalézt
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc4d1	důležité
východisko	východisko	k1gNnSc4	východisko
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
číslo	číslo	k1gNnSc1	číslo
vytvořených	vytvořený	k2eAgNnPc2d1	vytvořené
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Bosch	Bosch	kA	Bosch
podepsal	podepsat	k5eAaPmAgMnS	podepsat
pouze	pouze	k6eAd1	pouze
sedm	sedm	k4xCc4	sedm
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechna	všechen	k3xTgNnPc1	všechen
díla	dílo	k1gNnPc1	dílo
jemu	on	k3xPp3gMnSc3	on
někdy	někdy	k6eAd1	někdy
připisovaná	připisovaný	k2eAgFnSc1d1	připisovaná
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
jeho	jeho	k3xOp3gFnPc6	jeho
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
variací	variace	k1gFnPc2	variace
na	na	k7c4	na
jeho	jeho	k3xOp3gNnPc4	jeho
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
bezpočet	bezpočet	k1gInSc1	bezpočet
svých	svůj	k3xOyFgMnPc2	svůj
následovníků	následovník	k1gMnPc2	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
léta	léto	k1gNnPc4	léto
mu	on	k3xPp3gMnSc3	on
badatelé	badatel	k1gMnPc1	badatel
připisovali	připisovat	k5eAaImAgMnP	připisovat
další	další	k2eAgNnPc4d1	další
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
,	,	kIx,	,
s	s	k7c7	s
definitivní	definitivní	k2eAgFnSc7d1	definitivní
jistotou	jistota	k1gFnSc7	jistota
to	ten	k3xDgNnSc1	ten
však	však	k9	však
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
25	[number]	k4	25
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
pozemských	pozemský	k2eAgFnPc2d1	pozemská
rozkoší	rozkoš	k1gFnPc2	rozkoš
Loď	loď	k1gFnSc1	loď
bláznů	blázen	k1gMnPc2	blázen
Fůra	fůra	k1gFnSc1	fůra
sena	sena	k1gFnSc1	sena
Pokušení	pokušení	k1gNnSc1	pokušení
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
Klanění	klanění	k1gNnSc2	klanění
tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
Sedm	sedm	k4xCc1	sedm
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
Nesení	nesení	k1gNnSc2	nesení
kříže	kříž	k1gInSc2	kříž
Seznam	seznam	k1gInSc1	seznam
maleb	malba	k1gFnPc2	malba
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
Kresby	kresba	k1gFnSc2	kresba
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Darmoděj	darmoděj	k1gMnSc1	darmoděj
flétnu	flétna	k1gFnSc4	flétna
od	od	k7c2	od
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
<g/>
.	.	kIx.	.
</s>
<s>
Rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
Progres	progres	k1gInSc1	progres
2	[number]	k4	2
nahrála	nahrát	k5eAaBmAgFnS	nahrát
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
Dialog	dialog	k1gInSc4	dialog
s	s	k7c7	s
vesmírem	vesmír	k1gInSc7	vesmír
skladbu	skladba	k1gFnSc4	skladba
Planeta	planeta	k1gFnSc1	planeta
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gFnSc2	Bosch
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Gothic-rocková	Gothicockový	k2eAgFnSc1d1	Gothic-rockový
skupina	skupina	k1gFnSc1	skupina
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
jméno	jméno	k1gNnSc4	jméno
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Fatherland	Fatherlanda	k1gFnPc2	Fatherlanda
<g/>
.	.	kIx.	.
</s>
<s>
Písničkář	písničkář	k1gMnSc1	písničkář
Vladimír	Vladimír	k1gMnSc1	Vladimír
Merta	Merta	k1gMnSc1	Merta
složil	složit	k5eAaPmAgMnS	složit
písničku	písnička	k1gFnSc4	písnička
Pomeranče	pomeranč	k1gInSc2	pomeranč
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Chernobyl	Chernobyl	k1gInSc1	Chernobyl
Generation	Generation	k1gInSc1	Generation
má	mít	k5eAaImIp3nS	mít
píseň	píseň	k1gFnSc4	píseň
Noc	noc	k1gFnSc4	noc
Hieronyma	Hieronymus	k1gMnSc2	Hieronymus
Bosche	Bosch	k1gMnSc2	Bosch
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Theun	Theun	k1gMnSc1	Theun
de	de	k?	de
Vries	Vries	k1gMnSc1	Vries
použil	použít	k5eAaPmAgMnS	použít
Hieronyma	Hieronymus	k1gMnSc4	Hieronymus
Bosche	Bosch	k1gMnSc4	Bosch
jako	jako	k9	jako
předobraz	předobraz	k1gInSc4	předobraz
pro	pro	k7c4	pro
hlavního	hlavní	k2eAgMnSc4d1	hlavní
hrdinu	hrdina	k1gMnSc4	hrdina
svého	svůj	k3xOyFgInSc2	svůj
románu	román	k1gInSc2	román
Skřeti	skřet	k1gMnPc1	skřet
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
natočila	natočit	k5eAaBmAgFnS	natočit
seriál	seriál	k1gInSc4	seriál
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
rituální	rituální	k2eAgFnSc2d1	rituální
vraždy	vražda	k1gFnSc2	vražda
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
Boschových	Boschův	k2eAgInPc2d1	Boschův
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
