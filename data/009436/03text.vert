<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
transplantační	transplantační	k2eAgInSc1d1	transplantační
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
schválený	schválený	k2eAgInSc1d1	schválený
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1984	[number]	k4	1984
a	a	k8xC	a
novelizován	novelizovat	k5eAaBmNgMnS	novelizovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
prodej	prodej	k1gInSc1	prodej
lidských	lidský	k2eAgInPc2d1	lidský
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
zřízení	zřízení	k1gNnSc4	zřízení
pracovní	pracovní	k2eAgFnSc2d1	pracovní
skupiny	skupina	k1gFnSc2	skupina
pro	pro	k7c4	pro
transplantaci	transplantace	k1gFnSc4	transplantace
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
americkému	americký	k2eAgNnSc3d1	americké
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
(	(	kIx(	(
<g/>
Department	department	k1gInSc1	department
of	of	k?	of
Health	Health	k1gInSc1	Health
and	and	k?	and
Human	Human	k1gInSc1	Human
Services	Services	k1gInSc1	Services
<g/>
)	)	kIx)	)
udělovat	udělovat	k5eAaImF	udělovat
granty	grant	k1gInPc4	grant
na	na	k7c4	na
plánování	plánování	k1gNnSc4	plánování
<g/>
,	,	kIx,	,
zakládání	zakládání	k1gNnSc4	zakládání
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
organizací	organizace	k1gFnPc2	organizace
obchodujících	obchodující	k2eAgFnPc2d1	obchodující
s	s	k7c7	s
orgány	orgán	k1gInPc7	orgán
(	(	kIx(	(
<g/>
Organ	organon	k1gNnPc2	organon
Procurement	Procurement	k1gMnSc1	Procurement
Organizations	Organizations	k1gInSc1	Organizations
-	-	kIx~	-
OPO	OPO	kA	OPO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
zákonem	zákon	k1gInSc7	zákon
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
Organ	organon	k1gNnPc2	organon
Procurement	Procurement	k1gInSc1	Procurement
and	and	k?	and
Transplantation	Transplantation	k1gInSc1	Transplantation
Network	network	k1gInSc1	network
a	a	k8xC	a
Scientific	Scientific	k1gMnSc1	Scientific
Registry	registrum	k1gNnPc7	registrum
of	of	k?	of
Transplant	Transplant	k1gMnSc1	Transplant
Recipients	Recipients	k1gInSc1	Recipients
<g/>
.	.	kIx.	.
</s>
<s>
Podpořili	podpořit	k5eAaPmAgMnP	podpořit
ho	on	k3xPp3gMnSc4	on
např.	např.	kA	např.
demokratický	demokratický	k2eAgMnSc1d1	demokratický
reprezentant	reprezentant	k1gMnSc1	reprezentant
Al	ala	k1gFnPc2	ala
Gore	Gore	k1gInSc1	Gore
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
Orrin	Orrin	k1gMnSc1	Orrin
Hatch	Hatch	k1gMnSc1	Hatch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
National	National	k1gFnSc2	National
Organ	organon	k1gNnPc2	organon
Transplant	Transplant	k1gMnSc1	Transplant
Act	Act	k1gFnSc2	Act
of	of	k?	of
1984	[number]	k4	1984
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
