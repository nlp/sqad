<s>
Národní	národní	k2eAgInSc1d1	národní
transplantační	transplantační	k2eAgInSc1d1	transplantační
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
schválený	schválený	k2eAgInSc1d1	schválený
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1984	[number]	k4	1984
a	a	k8xC	a
novelizován	novelizovat	k5eAaBmNgMnS	novelizovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
prodej	prodej	k1gInSc1	prodej
lidských	lidský	k2eAgInPc2d1	lidský
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
zřízení	zřízení	k1gNnSc4	zřízení
pracovní	pracovní	k2eAgFnSc2d1	pracovní
skupiny	skupina	k1gFnSc2	skupina
pro	pro	k7c4	pro
transplantaci	transplantace	k1gFnSc4	transplantace
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
