<p>
<s>
Hard	Hard	k6eAd1	Hard
Stuff	Stuff	k1gInSc4	Stuff
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
superskupina	superskupina	k1gFnSc1	superskupina
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Du	Du	k?	Du
Cann	Cann	k1gMnSc1	Cann
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Hammond	Hammond	k1gInSc1	Hammond
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
Atomic	Atomice	k1gInPc2	Atomice
Rooster	Roostrum	k1gNnPc2	Roostrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Harry	Harra	k1gMnSc2	Harra
Shaw	Shaw	k1gMnSc2	Shaw
(	(	kIx(	(
<g/>
Curiosity	Curiosita	k1gFnPc1	Curiosita
Shoppe	Shopp	k1gInSc5	Shopp
<g/>
)	)	kIx)	)
a	a	k8xC	a
John	John	k1gMnSc1	John
Gustafson	Gustafson	k1gMnSc1	Gustafson
(	(	kIx(	(
<g/>
Quatermass	Quatermass	k1gInSc1	Quatermass
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Bulletproof	Bulletproof	k1gInSc1	Bulletproof
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bolex	Bolex	k1gInSc1	Bolex
Dementia	Dementium	k1gNnSc2	Dementium
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
