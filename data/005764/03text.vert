<s>
Jeffery	Jeffer	k1gInPc1	Jeffer
Deaver	Deavra	k1gFnPc2	Deavra
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1950	[number]	k4	1950
v	v	k7c4	v
Glen	Glen	k1gNnSc4	Glen
Ellyn	Ellyna	k1gFnPc2	Ellyna
v	v	k7c6	v
Illinois	Illinois	k1gFnPc6	Illinois
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
autor	autor	k1gMnSc1	autor
detektivních	detektivní	k2eAgInPc2d1	detektivní
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
překládají	překládat	k5eAaImIp3nP	překládat
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Pernicová	Pernicová	k1gFnSc1	Pernicová
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
Suderová	Suderová	k1gFnSc1	Suderová
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Švejda	Švejda	k1gMnSc1	Švejda
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1950	[number]	k4	1950
v	v	k7c4	v
Glen	Glen	k1gNnSc4	Glen
Ellyn	Ellyna	k1gFnPc2	Ellyna
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
reklamní	reklamní	k2eAgMnSc1d1	reklamní
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
hospodyňka	hospodyňka	k1gFnSc1	hospodyňka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
taky	taky	k6eAd1	taky
sestru	sestra	k1gFnSc4	sestra
Julie	Julie	k1gFnSc1	Julie
Reece	Reece	k1gFnSc1	Reece
Deaverovou	Deaverová	k1gFnSc7	Deaverová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
píše	psát	k5eAaImIp3nS	psát
romány	román	k1gInPc4	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
jedenáct	jedenáct	k4xCc1	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
folkový	folkový	k2eAgMnSc1d1	folkový
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgMnS	zpívat
po	po	k7c6	po
klubech	klub	k1gInPc6	klub
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
a	a	k8xC	a
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
na	na	k7c4	na
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
komerční	komerční	k2eAgMnSc1d1	komerční
právník	právník	k1gMnSc1	právník
na	na	k7c4	na
Wall	Wall	k1gInSc4	Wall
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
třiceti	třicet	k4xCc2	třicet
dvou	dva	k4xCgInPc2	dva
románů	román	k1gInPc2	román
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
sbírek	sbírka	k1gFnPc2	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgInS	proslout
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
knihami	kniha	k1gFnPc7	kniha
s	s	k7c7	s
ochrnutým	ochrnutý	k2eAgMnSc7d1	ochrnutý
kriminalistou	kriminalista	k1gMnSc7	kriminalista
Lincolnem	Lincoln	k1gMnSc7	Lincoln
Rhymem	Rhym	k1gMnSc7	Rhym
<g/>
.	.	kIx.	.
</s>
<s>
Sběratel	sběratel	k1gMnSc1	sběratel
kostí	kost	k1gFnPc2	kost
Tanečník	tanečník	k1gMnSc1	tanečník
Prázdné	prázdný	k2eAgNnSc4d1	prázdné
křeslo	křeslo	k1gNnSc4	křeslo
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Empty	Empt	k1gInPc1	Empt
Chair	Chair	k1gInSc1	Chair
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělko	k1gNnSc2	Kobělko
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
opice	opice	k1gFnSc1	opice
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Stone	ston	k1gInSc5	ston
Monkey	Monke	k2eAgInPc4d1	Monke
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Iluze	iluze	k1gFnSc1	iluze
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Wanished	Wanished	k1gMnSc1	Wanished
Man	Man	k1gMnSc1	Man
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Dvanáctá	dvanáctý	k4xOgFnSc1	dvanáctý
karta	karta	k1gFnSc1	karta
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Twelfth	Twelfth	k1gMnSc1	Twelfth
Card	Card	k1gMnSc1	Card
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Hodinář	hodinář	k1gMnSc1	hodinář
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cold	Cold	k1gMnSc1	Cold
Moon	Moon	k1gMnSc1	Moon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Rozbité	rozbitý	k2eAgNnSc4d1	rozbité
okno	okno	k1gNnSc4	okno
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Broken	Broken	k1gInSc1	Broken
Window	Window	k1gFnSc1	Window
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Hořící	hořící	k2eAgInSc4d1	hořící
drát	drát	k1gInSc4	drát
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Burning	Burning	k1gInSc1	Burning
Wire	Wire	k1gInSc1	Wire
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Pokoj	pokoj	k1gInSc4	pokoj
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Kill	Kill	k1gMnSc1	Kill
Room	Room	k1gMnSc1	Room
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Sběratel	sběratel	k1gMnSc1	sběratel
kůží	kůže	k1gFnPc2	kůže
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Skin	skin	k1gMnSc1	skin
Collector	Collector	k1gMnSc1	Collector
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Ocelový	ocelový	k2eAgInSc4d1	ocelový
polibek	polibek	k1gInSc4	polibek
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Steel	Steel	k1gInSc1	Steel
Kiss	Kiss	k1gInSc1	Kiss
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Dalibor	Dalibor	k1gMnSc1	Dalibor
Míček	míček	k1gInSc4	míček
Poslední	poslední	k2eAgFnSc1d1	poslední
hodina	hodina	k1gFnSc1	hodina
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Burial	Burial	k1gMnSc1	Burial
Hour	Hour	k1gMnSc1	Hour
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělko	k1gNnSc2	Kobělko
Spící	spící	k2eAgFnSc1d1	spící
panna	panna	k1gFnSc1	panna
Kříže	kříž	k1gInSc2	kříž
u	u	k7c2	u
cesty	cesta	k1gFnSc2	cesta
Tvůj	tvůj	k1gMnSc1	tvůj
stín	stín	k1gInSc1	stín
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
XO	XO	kA	XO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Zapadákov	Zapadákov	k1gInSc1	Zapadákov
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Solitude	Solitud	k1gMnSc5	Solitud
Creek	Creko	k1gNnPc2	Creko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Dalibor	Dalibor	k1gMnSc1	Dalibor
Míček	Míček	k1gMnSc1	Míček
Příští	příští	k2eAgFnSc2d1	příští
oběti	oběť	k1gFnSc2	oběť
Panoptikum	panoptikum	k1gNnSc1	panoptikum
Vrahem	vrah	k1gMnSc7	vrah
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
More	mor	k1gInSc5	mor
Twisted	Twisted	k1gMnSc1	Twisted
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
a	a	k8xC	a
Zuzana	Zuzana	k1gFnSc1	Zuzana
Pernicová	Pernicová	k1gFnSc1	Pernicová
Tak	tak	k9	tak
trochu	trochu	k6eAd1	trochu
šílení	šílení	k1gNnSc4	šílení
Milenka	Milenka	k1gFnSc1	Milenka
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Lekce	lekce	k1gFnSc2	lekce
smrti	smrt	k1gFnSc2	smrt
Neklid	neklid	k1gInSc1	neklid
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Praying	Praying	k1gInSc1	Praying
For	forum	k1gNnPc2	forum
Sleep	Sleep	k1gInSc1	Sleep
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Domino	domino	k1gNnSc1	domino
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kobělka	Kobělka	k1gMnSc1	Kobělka
Dívčí	dívčí	k2eAgInSc4d1	dívčí
hrob	hrob	k1gInSc4	hrob
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
slza	slza	k1gFnSc1	slza
Katedrála	katedrála	k1gFnSc1	katedrála
hrůzy	hrůza	k1gFnSc2	hrůza
Modrá	modrý	k2eAgFnSc1d1	modrá
sféra	sféra	k1gFnSc1	sféra
Zahrada	zahrada	k1gFnSc1	zahrada
bestií	bestie	k1gFnPc2	bestie
Lovec	lovec	k1gMnSc1	lovec
Nula	nula	k1gFnSc1	nula
stupňů	stupeň	k1gInPc2	stupeň
volnosti	volnost	k1gFnSc2	volnost
(	(	kIx(	(
<g/>
James	James	k1gInSc1	James
Bond	bond	k1gInSc1	bond
<g/>
)	)	kIx)	)
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
Smrt	smrt	k1gFnSc4	smrt
pornohvězdy	pornohvězda	k1gFnSc2	pornohvězda
Žhavé	žhavý	k2eAgFnSc2d1	žhavá
zprávy	zpráva	k1gFnSc2	zpráva
Mělké	mělký	k2eAgInPc4d1	mělký
hroby	hrob	k1gInPc4	hrob
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
řeka	řeka	k1gFnSc1	řeka
Pekelná	pekelný	k2eAgFnSc1d1	pekelná
kuchyně	kuchyně	k1gFnSc1	kuchyně
</s>
