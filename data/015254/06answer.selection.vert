<s>
Chronologický	chronologický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Sia-Šang-Čou	Sia-Šang-Čou	k1gFnSc1
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
pchin-jinem	pchin-jin	k1gInSc7
Xì	Xì	k1gInSc2
Shā	Shā	k1gInSc4
Zhō	Zhō	k1gMnSc3
Duà	Duà	k1gMnSc3
Gō	Gō	k1gInSc4
<g/>
,	,	kIx,
znaky	znak	k1gInPc4
夏	夏	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čínský	čínský	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vnést	vnést	k5eAaPmF
pořádek	pořádek	k1gInSc4
do	do	k7c2
chronologie	chronologie	k1gFnSc2
nejstarších	starý	k2eAgFnPc2d3
čínských	čínský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
totiž	totiž	k9
období	období	k1gNnSc1
dynastií	dynastie	k1gFnPc2
Sia	Sia	k1gFnPc2
<g/>
,	,	kIx,
Šang	Šanga	k1gFnPc2
a	a	k8xC
Čou	Čou	k1gFnPc2
<g/>
.	.	kIx.
</s>