<s>
Chronologický	chronologický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Sia-Šang-Čou	Sia-Šang-Čá	k1gFnSc4
</s>
<s>
Chronologický	chronologický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Sia-Šang-Čou	Sia-Šang-Čou	k1gFnSc1
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
pchin-jinem	pchin-jin	k1gInSc7
Xì	Xì	k1gInSc2
Shā	Shā	k1gInSc4
Zhō	Zhō	k1gMnSc3
Duà	Duà	k1gMnSc3
Gō	Gō	k1gInSc4
<g/>
,	,	kIx,
znaky	znak	k1gInPc4
夏	夏	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čínský	čínský	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vnést	vnést	k5eAaPmF
pořádek	pořádek	k1gInSc4
do	do	k7c2
chronologie	chronologie	k1gFnSc2
nejstarších	starý	k2eAgFnPc2d3
čínských	čínský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
totiž	totiž	k9
období	období	k1gNnSc1
dynastií	dynastie	k1gFnPc2
Sia	Sia	k1gFnPc2
<g/>
,	,	kIx,
Šang	Šanga	k1gFnPc2
a	a	k8xC
Čou	Čou	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
na	na	k7c6
projektu	projekt	k1gInSc6
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
roku	rok	k1gInSc3
1996	#num#	k4
<g/>
,	,	kIx,
předběžná	předběžný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
výsledcích	výsledek	k1gInPc6
byla	být	k5eAaImAgFnS
publikována	publikován	k2eAgFnSc1d1
v	v	k7c6
listopadu	listopad	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Projekt	projekt	k1gInSc1
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3
všeobecně	všeobecně	k6eAd1
přijímaným	přijímaný	k2eAgNnSc7d1
datem	datum	k1gNnSc7
čínské	čínský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
je	být	k5eAaImIp3nS
vyhnání	vyhnání	k1gNnSc1
čouského	čouský	k1gMnSc2
krále	král	k1gMnSc2
Li	li	k8xS
a	a	k8xC
nastolení	nastolení	k1gNnSc4
regentské	regentský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
roku	rok	k1gInSc2
841	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chronologický	chronologický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Sia-Šang-Čou	Sia-Šang-Ča	k1gMnSc7
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
stanovit	stanovit	k5eAaPmF
posloupnost	posloupnost	k1gFnSc4
dynastií	dynastie	k1gFnPc2
a	a	k8xC
králů	král	k1gMnPc2
pro	pro	k7c4
starší	starý	k2eAgFnPc4d2
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
datování	datování	k1gNnSc1
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
diskuzí	diskuze	k1gFnPc2
a	a	k8xC
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
projektu	projekt	k1gInSc6
se	se	k3xPyFc4
spojilo	spojit	k5eAaPmAgNnS
na	na	k7c4
200	#num#	k4
čínských	čínský	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
a	a	k8xC
archeologů	archeolog	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
největší	veliký	k2eAgFnSc7d3
historickou	historický	k2eAgFnSc7d1
akcí	akce	k1gFnSc7
od	od	k7c2
sestavení	sestavení	k1gNnSc2
knižního	knižní	k2eAgInSc2d1
souboru	soubor	k1gInSc2
S	s	k7c7
<g/>
'	'	kIx"
<g/>
-kchu	-kcha	k1gMnSc4
čchüan-šu	čchüan-sat	k5eAaPmIp1nS
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Koncipován	koncipovat	k5eAaBmNgMnS
byl	být	k5eAaImAgMnS
jako	jako	k9
interdisciplinární	interdisciplinární	k2eAgFnPc4d1
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
využity	využít	k5eAaPmNgInP
jak	jak	k8xS,k8xC
písemné	písemný	k2eAgInPc1d1
prameny	pramen	k1gInPc1
(	(	kIx(
<g/>
Kniha	kniha	k1gFnSc1
obřadů	obřad	k1gInPc2
<g/>
,	,	kIx,
Bambusové	bambusový	k2eAgInPc1d1
anály	anály	k1gInPc1
<g/>
,	,	kIx,
Zápisky	zápiska	k1gFnPc1
historika	historik	k1gMnSc2
a	a	k8xC
další	další	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
archeologické	archeologický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
a	a	k8xC
data	datum	k1gNnPc4
o	o	k7c6
významných	významný	k2eAgInPc6d1
astronomických	astronomický	k2eAgInPc6d1
jevech	jev	k1gInPc6
(	(	kIx(
<g/>
zatměních	zatmění	k1gNnPc6
slunce	slunce	k1gNnSc2
a	a	k8xC
měsíce	měsíc	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2000	#num#	k4
byly	být	k5eAaImAgFnP
zveřejněny	zveřejnit	k5eAaPmNgInP
předběžné	předběžný	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
chronologie	chronologie	k1gFnSc1
čínských	čínský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
Čínské	čínský	k2eAgFnSc6d1
lidové	lidový	k2eAgFnSc6d1
republice	republika	k1gFnSc6
oficiálně	oficiálně	k6eAd1
přijata	přijmout	k5eAaPmNgFnS
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
základem	základ	k1gInSc7
školní	školní	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Nejstarší	starý	k2eAgNnPc1d3
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
přibližná	přibližný	k2eAgNnPc1d1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Všechny	všechen	k3xTgFnPc1
čtyři	čtyři	k4xCgFnPc1
fáze	fáze	k1gFnPc1
kultury	kultura	k1gFnSc2
Er	Er	k1gMnPc2
<g/>
-li	-li	k?
<g/>
-tchou	-tcha	k1gFnSc7
byly	být	k5eAaImAgInP
přiřazeny	přiřadit	k5eAaPmNgInP
dynastii	dynastie	k1gFnSc3
Sia	Sia	k1gFnSc1
<g/>
,	,	kIx,
počínající	počínající	k2eAgFnSc1d1
cca	cca	kA
2070	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tradiční	tradiční	k2eAgNnSc1d1
datum	datum	k1gNnSc1
založené	založený	k2eAgNnSc1d1
na	na	k7c6
výpočtech	výpočet	k1gInPc6
Liou	Lious	k1gInSc2
Sina	sino	k1gNnSc2
je	být	k5eAaImIp3nS
2205	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Založení	založení	k1gNnSc1
dynastie	dynastie	k1gFnSc2
Šang	Šanga	k1gFnPc2
bylo	být	k5eAaImAgNnS
stanoveno	stanovit	k5eAaPmNgNnS
jako	jako	k9
současné	současný	k2eAgFnSc2d1
s	s	k7c7
výstavbou	výstavba	k1gFnSc7
opevněného	opevněný	k2eAgNnSc2d1
města	město	k1gNnSc2
v	v	k7c4
Jen-š	Jen-š	k1gInSc4
<g/>
'	'	kIx"
a	a	k8xC
určeno	určit	k5eAaPmNgNnS
na	na	k7c4
cca	cca	kA
1600	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tradiční	tradiční	k2eAgNnSc1d1
datum	datum	k1gNnSc1
je	být	k5eAaImIp3nS
1766	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
;	;	kIx,
Cambridge	Cambridge	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
Ancient	Ancient	k1gInSc1
China	China	k1gFnSc1
udává	udávat	k5eAaImIp3nS
cca	cca	kA
1570	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Přemístění	přemístění	k1gNnSc1
šangského	šangský	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
do	do	k7c2
Jin	Jin	k1gFnSc2
za	za	k7c2
vlády	vláda	k1gFnSc2
Pchan	Pchan	k1gInSc1
Kenga	Keng	k1gMnSc2
bylo	být	k5eAaImAgNnS
určeno	určit	k5eAaPmNgNnS
na	na	k7c4
cca	cca	kA
1300	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Počínajíc	počínajíc	k7c7
nástupem	nástup	k1gInSc7
šangského	šangský	k1gMnSc2
krále	král	k1gMnSc2
Wu	Wu	k1gMnSc2
Tinga	Ting	k1gMnSc2
byla	být	k5eAaImAgFnS
data	datum	k1gNnSc2
vlády	vláda	k1gFnSc2
šangských	šangský	k2eAgMnPc2d1
a	a	k8xC
čouských	čouský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
stanovena	stanovit	k5eAaPmNgFnS
přesně	přesně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Srovnání	srovnání	k1gNnSc4
výsledků	výsledek	k1gInPc2
projektu	projekt	k1gInSc2
Sia-Šang-Čou	Sia-Šang-Čá	k1gFnSc4
<g/>
,	,	kIx,
dat	datum	k1gNnPc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
Ancient	Ancient	k1gInSc1
China	China	k1gFnSc1
a	a	k8xC
tradičních	tradiční	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Král	Král	k1gMnSc1
</s>
<s>
Počátek	počátek	k1gInSc1
vlády	vláda	k1gFnSc2
(	(	kIx(
<g/>
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Projekt	projekt	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cambridge	Cambridge	k1gFnSc1
History	Histor	k1gInPc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tradiční	tradiční	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šang	Šang	k1gMnSc1
</s>
<s>
Wu	Wu	k?
Ting	Ting	k1gInSc1
<g/>
1250	#num#	k4
<g/>
před	před	k7c7
11981324	#num#	k4
</s>
<s>
Cu	Cu	k?
Keng	Keng	k1gInSc1
<g/>
1191	#num#	k4
<g/>
po	po	k7c4
11881265	#num#	k4
</s>
<s>
Cu	Cu	k?
Ťia	Ťia	k1gFnSc1
<g/>
–	–	k?
<g/>
cca	cca	kA
11771258	#num#	k4
</s>
<s>
Lin	Lina	k1gFnPc2
Sin	sino	k1gNnPc2
<g/>
–	–	k?
<g/>
cca	cca	kA
11571225	#num#	k4
</s>
<s>
Kchang	Kchang	k1gMnSc1
Ting	Ting	k1gMnSc1
<g/>
–	–	k?
<g/>
cca	cca	kA
11481219	#num#	k4
</s>
<s>
Wu	Wu	k?
I	i	k9
<g/>
1147	#num#	k4
<g/>
cca	cca	kA
11311198	#num#	k4
</s>
<s>
Wen	Wen	k?
Ting	Ting	k1gMnSc1
<g/>
1112	#num#	k4
<g/>
cca	cca	kA
11161194	#num#	k4
</s>
<s>
Ti	ten	k3xDgMnPc1
I110111051191	I110111051191	k1gMnPc1
</s>
<s>
Ti	ten	k3xDgMnPc1
Sin	sin	kA
<g/>
107510861154	#num#	k4
</s>
<s>
Čou	Čou	k?
</s>
<s>
Wu-wang	Wu-wang	k1gInSc1
<g/>
104610451121	#num#	k4
</s>
<s>
Čcheng-wang	Čcheng-wang	k1gInSc1
<g/>
104210421115	#num#	k4
</s>
<s>
Kchang-wang	Kchang-wang	k1gInSc1
<g/>
102010051078	#num#	k4
</s>
<s>
Čao-wang	Čao-wang	k1gInSc1
<g/>
9959771052	#num#	k4
</s>
<s>
Mu-wang	Mu-wang	k1gInSc1
<g/>
9769561101	#num#	k4
</s>
<s>
Kung-wang	Kung-wang	k1gInSc1
<g/>
922917946	#num#	k4
</s>
<s>
I-wang	I-wang	k1gInSc1
<g/>
899899934	#num#	k4
</s>
<s>
Siao-wang	Siao-wang	k1gInSc1
<g/>
891872	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
905	#num#	k4
</s>
<s>
I-wang	I-wang	k1gInSc1
<g/>
885865894	#num#	k4
</s>
<s>
Li-wang	Li-wang	k1gInSc1
<g/>
877857878	#num#	k4
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Mimo	mimo	k7c4
Čínu	Čína	k1gFnSc4
byl	být	k5eAaImAgInS
projekt	projekt	k1gInSc1
podroben	podrobit	k5eAaPmNgInS
kritice	kritika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritizovány	kritizován	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
jak	jak	k8xC,k8xS
východiska	východisko	k1gNnPc1
čínských	čínský	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
i	i	k9
jejich	jejich	k3xOp3gInPc4
závěry	závěr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritizována	kritizován	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
představa	představa	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Čína	Čína	k1gFnSc1
má	mít	k5eAaImIp3nS
nepřetržitou	přetržitý	k2eNgFnSc4d1
historii	historie	k1gFnSc4
pět	pět	k4xCc4
tisíc	tisíc	k4xCgInSc4
let	léto	k1gNnPc2
dlouhou	dlouhý	k2eAgFnSc4d1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
státy	stát	k1gInPc4
Sia	Sia	k1gFnPc2
<g/>
,	,	kIx,
Šang	Šanga	k1gFnPc2
a	a	k8xC
Čou	Čou	k1gFnPc2
zaujímaly	zaujímat	k5eAaImAgFnP
ústřední	ústřední	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jsou	být	k5eAaImIp3nP
ignorovány	ignorovat	k5eAaImNgInP
ostatní	ostatní	k2eAgInPc1d1
národy	národ	k1gInPc1
<g/>
,	,	kIx,
kmeny	kmen	k1gInPc1
a	a	k8xC
státy	stát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
neméně	málo	k6eNd2
rozvinuté	rozvinutý	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tisku	tisk	k1gInSc6
bylo	být	k5eAaImAgNnS
prosazování	prosazování	k1gNnSc1
„	„	k?
<g/>
pěti	pět	k4xCc3
tisíc	tisíc	k4xCgInSc4
let	let	k1gInSc4
čínské	čínský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
“	“	k?
hodnoceno	hodnotit	k5eAaImNgNnS
spíše	spíše	k9
jako	jako	k9
projev	projev	k1gInSc4
čínského	čínský	k2eAgInSc2d1
nacionalismu	nacionalismus	k1gInSc2
<g/>
,	,	kIx,
než	než	k8xS
vědecky	vědecky	k6eAd1
podložený	podložený	k2eAgInSc1d1
fakt	fakt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výhrady	výhrada	k1gFnPc1
byly	být	k5eAaImAgFnP
vzneseny	vznesen	k2eAgFnPc1d1
též	též	k9
ke	k	k7c3
konkrétním	konkrétní	k2eAgInPc3d1
bodům	bod	k1gInPc3
<g/>
:	:	kIx,
</s>
<s>
Archeologické	archeologický	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
mezi	mezi	k7c7
Sia	Sia	k1gFnSc7
a	a	k8xC
Šang	Šang	k1gMnSc1
i	i	k9
mezi	mezi	k7c4
Šang	Šang	k1gInSc4
a	a	k8xC
Čou	Čou	k1gFnSc4
nejsou	být	k5eNaImIp3nP
jasně	jasně	k6eAd1
stanoveny	stanovit	k5eAaPmNgInP
</s>
<s>
Pro	pro	k7c4
účely	účel	k1gInPc4
projektu	projekt	k1gInSc2
byl	být	k5eAaImAgMnS
u	u	k7c2
dat	datum	k1gNnPc2
získaných	získaný	k2eAgNnPc2d1
pomocí	pomocí	k7c2
radiokarbonové	radiokarbonový	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
datování	datování	k1gNnSc2
použit	použít	k5eAaPmNgInS
koeficient	koeficient	k1gInSc1
spolehlivosti	spolehlivost	k1gFnSc2
68	#num#	k4
%	%	kIx~
místo	místo	k7c2
běžného	běžný	k2eAgMnSc2d1
95	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získaná	získaný	k2eAgNnPc1d1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
méně	málo	k6eAd2
spolehlivá	spolehlivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výpočty	výpočet	k1gInPc1
pro	pro	k7c4
zatmění	zatmění	k1gNnSc4
slunce	slunce	k1gNnSc1
roku	rok	k1gInSc2
899	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
jsou	být	k5eAaImIp3nP
nesprávné	správný	k2eNgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Datování	datování	k1gNnSc1
pomocí	pomocí	k7c2
astronomických	astronomický	k2eAgInPc2d1
jevů	jev	k1gInPc2
jsou	být	k5eAaImIp3nP
založená	založený	k2eAgFnSc1d1
na	na	k7c6
sporných	sporný	k2eAgFnPc6d1
interpretacích	interpretace	k1gFnPc6
starověkých	starověký	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autoři	autor	k1gMnPc1
kronik	kronika	k1gFnPc2
a	a	k8xC
záznamů	záznam	k1gInPc2
vzdálených	vzdálený	k2eAgFnPc2d1
od	od	k7c2
popisovaného	popisovaný	k2eAgNnSc2d1
období	období	k1gNnSc2
chybují	chybovat	k5eAaImIp3nP
při	při	k7c6
používání	používání	k1gNnSc6
starých	starý	k2eAgInPc2d1
kalendářů	kalendář	k1gInPc2
což	což	k9
vede	vést	k5eAaImIp3nS
k	k	k7c3
nesprávné	správný	k2eNgFnSc3d1
interpretaci	interpretace	k1gFnSc3
jejich	jejich	k3xOp3gFnPc2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Xia	Xia	k1gFnSc2
<g/>
–	–	k?
<g/>
Shang	Shang	k1gInSc1
<g/>
–	–	k?
<g/>
Zhou	Zhous	k1gInSc2
Chronology	chronolog	k1gMnPc4
Project	Project	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
ERIK	Erika	k1gFnPc2
<g/>
,	,	kIx,
Eckholm	Eckholma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
China	China	k1gFnSc1
<g/>
,	,	kIx,
Ancient	Ancient	k1gInSc1
History	Histor	k1gMnPc4
Kindles	Kindles	k1gMnSc1
Modern	Modern	k1gMnSc1
Doubts	Doubts	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnPc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHAUGHNESSY	SHAUGHNESSY	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
L.	L.	kA
Chronologies	Chronologies	k1gMnSc1
of	of	k?
Ancient	Ancient	k1gInSc1
China	China	k1gFnSc1
<g/>
:	:	kIx,
A	a	k9
Critique	Critique	k1gNnSc6
of	of	k?
the	the	k?
‘	‘	k?
<g/>
Xia-Shang-Zhou	Xia-Shang-Zha	k1gMnSc7
Chronology	chronolog	k1gMnPc4
Project	Project	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
HO	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
Clara	Clara	k1gFnSc1
Wing-chung	Wing-chung	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Windows	Windows	kA
on	on	k3xPp3gMnSc1
the	the	k?
Chinese	Chinese	k1gFnSc1
World	World	k1gMnSc1
<g/>
:	:	kIx,
Reflections	Reflections	k1gInSc1
by	by	kYmCp3nS
Five	Five	k1gInSc4
Historians	Historiansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lanham	Lanham	k1gInSc1
<g/>
,	,	kIx,
Md	Md	k1gFnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Lexington	Lexington	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
ealc	ealc	k1gInSc1
<g/>
.	.	kIx.
<g/>
uchicago	uchicago	k1gNnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
<g/>
/	/	kIx~
<g/>
documents	documents	k1gInSc1
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
Chronologies	Chronologies	k1gInSc1
of	of	k?
Ancient	Ancient	k1gInSc1
China-	China-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
pdf	pdf	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
夏	夏	k?
<g/>
1996	#num#	k4
<g/>
—	—	k?
<g/>
2000	#num#	k4
<g/>
年	年	k?
<g/>
·	·	k?
<g/>
简	简	k?
[	[	kIx(
<g/>
Zpráva	zpráva	k1gFnSc1
chronologického	chronologický	k2eAgInSc2d1
projektu	projekt	k1gInSc2
Sia-Šang-Čou	Sia-Šang-Čá	k1gFnSc4
za	za	k7c4
roky	rok	k1gInPc4
1996	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LI	li	k9
<g/>
,	,	kIx,
Xueqin	Xueqin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc7
Xia-Shang-Zhou	Xia-Shang-Zha	k1gMnSc7
Chronology	chronolog	k1gMnPc4
Project	Projecta	k1gFnPc2
<g/>
:	:	kIx,
Methodology	Methodolog	k1gMnPc4
and	and	k?
results	results	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
East	East	k1gMnSc1
Asian	Asian	k1gMnSc1
Archaeology	Archaeolog	k1gMnPc7
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
321	#num#	k4
<g/>
–	–	k?
<g/>
333	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.116	10.116	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
156852302322454585	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHAUGHNESSY	SHAUGHNESSY	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
L.	L.	kA
Language	language	k1gFnPc1
and	and	k?
Writing	Writing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
LOEWE	LOEWE	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
;	;	kIx,
SHAUGHNESSY	SHAUGHNESSY	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
L.	L.	kA
The	The	k1gMnSc1
Cambridge	Cambridge	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
Ancient	Ancient	k1gInSc1
China	China	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
47030	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
THEOBALD	THEOBALD	kA
<g/>
,	,	kIx,
Ulrich	Ulrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chinaknowledge	Chinaknowledge	k1gInSc1
-	-	kIx~
a	a	k8xC
universal	universat	k5eAaPmAgMnS,k5eAaImAgMnS
guide	guide	k6eAd1
for	forum	k1gNnPc2
China	China	k1gFnSc1
studies	studies	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Chinese	Chinese	k1gFnSc2
History	Histor	k1gInPc1
-	-	kIx~
Shang	Shang	k1gInSc1
Dynasty	dynasta	k1gMnSc2
商	商	k?
(	(	kIx(
<g/>
17	#num#	k4
<g/>
th	th	k?
to	ten	k3xDgNnSc4
11	#num#	k4
<g/>
th	th	k?
cent	cent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BC	BC	kA
<g/>
)	)	kIx)
kings	kings	k1gInSc1
<g/>
,	,	kIx,
rulers	rulers	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Theobald	Theobald	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Theobald	Theobald	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Chinese	Chinese	k1gFnSc2
History	Histor	k1gInPc1
-	-	kIx~
Zhou	Zhous	k1gInSc2
Dynasty	dynasta	k1gMnSc2
周	周	k?
(	(	kIx(
<g/>
11	#num#	k4
<g/>
th	th	k?
<g/>
.	.	kIx.
cent	cent	k1gInSc1
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
221	#num#	k4
BC	BC	kA
<g/>
)	)	kIx)
kings	kings	k1gInSc1
and	and	k?
rulers	rulers	k1gInSc1
<g/>
.	.	kIx.
↑	↑	k?
LEE	Lea	k1gFnSc6
<g/>
,	,	kIx,
Yun	Yun	k1gMnSc1
Kuen	Kuen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Building	Building	k1gInSc1
the	the	k?
chronology	chronolog	k1gMnPc4
of	of	k?
early	earl	k1gMnPc4
Chinese	Chinese	k1gFnSc2
history	histor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asian	Asian	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
<g/>
:	:	kIx,
the	the	k?
Journal	Journal	k1gFnSc1
of	of	k?
Archaeology	Archaeolog	k1gMnPc4
for	forum	k1gNnPc2
Asia	Asium	k1gNnSc2
and	and	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
41	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KEENAN	KEENAN	kA
<g/>
,	,	kIx,
Douglas	Douglas	k1gMnSc1
J.	J.	kA
Defence	Defence	k1gFnPc1
of	of	k?
planetary	planetara	k1gFnSc2
conjunctions	conjunctions	k6eAd1
for	forum	k1gNnPc2
early	earl	k1gMnPc4
Chinese	Chinese	k1gFnPc1
chronology	chronolog	k1gMnPc4
is	is	k?
unmerited	unmerited	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Astronomical	Astronomical	k1gFnSc2
History	Histor	k1gInPc4
and	and	k?
Heritage	Heritag	k1gFnSc2
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
–	–	k?
<g/>
147	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
KEENAN	KEENAN	kA
<g/>
,	,	kIx,
Douglas	Douglas	k1gMnSc1
J.	J.	kA
Astro-historiographic	Astro-historiographic	k1gMnSc1
chronologies	chronologies	k1gMnSc1
of	of	k?
early	earl	k1gMnPc4
China	China	k1gFnSc1
are	ar	k1gInSc5
unfounded	unfounded	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
East	East	k2eAgInSc1d1
Asian	Asian	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
23	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
61	#num#	k4
<g/>
–	–	k?
<g/>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
STEPHENSON	STEPHENSON	kA
<g/>
,	,	kIx,
F.	F.	kA
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnPc2
reliable	reliable	k6eAd1
are	ar	k1gInSc5
archaic	archaice	k1gInPc2
records	records	k1gInSc4
of	of	k?
large	large	k1gInSc1
solar	solar	k1gMnSc1
eclipses	eclipses	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Astronomy	astronom	k1gMnPc7
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
39	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
229	#num#	k4
<g/>
–	–	k?
<g/>
250	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
