<s desamb="1">
Vévodou	vévoda	k1gMnSc7
byl	být	k5eAaImAgInS
po	po	k7c6
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
nejstarší	starý	k2eAgMnSc1d3
bratr	bratr	k1gMnSc1
Jan	Jan	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
o	o	k7c4
veškeré	veškerý	k3xTgInPc4
tituly	titul	k1gInPc4
přišel	přijít	k5eAaPmAgInS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
odmítal	odmítat	k5eAaImAgMnS
podrobit	podrobit	k5eAaPmF
císaři	císař	k1gMnSc3
Ferdinandovi	Ferdinand	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
Štýrskému	štýrský	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
