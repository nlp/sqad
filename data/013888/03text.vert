<s>
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
</s>
<s>
Vévoda	vévoda	k1gMnSc1
Sasko-výmarský	sasko-výmarský	k2eAgMnSc1d1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1598	#num#	k4
</s>
<s>
Altenburg	Altenburg	k1gMnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1662	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Výmar	Výmar	k1gInSc1
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Dorotea	Dorote	k1gInSc2
Anhaltsko-Desavská	Anhaltsko-Desavský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
ArnoštAdolf	ArnoštAdolf	k1gMnSc1
VilémJan	VilémJan	k1gMnSc1
JiříVilemína	JiříVilemína	k1gFnSc1
EleonoraBernardDorota	EleonoraBernardDorota	k1gFnSc1
Marie	Marie	k1gFnSc1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Wettinové	Wettin	k1gMnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Výmarský	sasko-výmarský	k2eAgInSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Marie	Marie	k1gFnSc1
Anhaltská	Anhaltský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1598	#num#	k4
<g/>
,	,	kIx,
Altenburg	Altenburg	k1gInSc1
—	—	k?
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1662	#num#	k4
<g/>
,	,	kIx,
Výmar	Výmar	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
vévoda	vévoda	k1gMnSc1
Sasko-výmarský	sasko-výmarský	k2eAgMnSc1d1
a	a	k8xC
po	po	k7c6
smrti	smrt	k1gFnSc6
bratra	bratr	k1gMnSc2
Albrechta	Albrecht	k1gMnSc2
od	od	k7c2
roku	rok	k1gInSc2
1644	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
vládce	vládce	k1gMnSc1
Sasko-eisenašský	Sasko-eisenašský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
rodiči	rodič	k1gMnPc7
byli	být	k5eAaImAgMnP
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko	Sasko	k1gNnSc1
Výmarský	výmarský	k2eAgInSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Dorota	Dorota	k1gFnSc1
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1598	#num#	k4
v	v	k7c6
Altenburgu	Altenburg	k1gInSc6
jako	jako	k9
pátý	pátý	k4xOgMnSc1
potomek	potomek	k1gMnSc1
vévody	vévoda	k1gMnSc2
Jana	Jan	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Doroty	Dorota	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
několik	několik	k4yIc4
sourozenců	sourozenec	k1gMnPc2
<g/>
:	:	kIx,
bratry	bratr	k1gMnPc7
Jana	Jan	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
<g/>
,	,	kIx,
Fridricha	Fridrich	k1gMnSc2
<g/>
,	,	kIx,
Albrechta	Albrecht	k1gMnSc2
<g/>
,	,	kIx,
Jana	Jan	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
<g/>
,	,	kIx,
Arnošta	Arnošt	k1gMnSc2
a	a	k8xC
Bernarda	Bernard	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Jeně	Jena	k1gFnSc6
a	a	k8xC
později	pozdě	k6eAd2
doprovázel	doprovázet	k5eAaImAgMnS
své	svůj	k3xOyFgMnPc4
bratry	bratr	k1gMnPc4
i	i	k9
při	při	k7c6
studiích	studio	k1gNnPc6
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1613	#num#	k4
bratři	bratr	k1gMnPc1
navštívili	navštívit	k5eAaPmAgMnP
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc4
i	i	k8xC
Velkou	velký	k2eAgFnSc4d1
Británii	Británie	k1gFnSc4
a	a	k8xC
roku	rok	k1gInSc2
1614	#num#	k4
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Sasko-výmarského	sasko-výmarský	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1617	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
šlechtici	šlechtic	k1gMnPc7
založil	založit	k5eAaPmAgMnS
jazykovou	jazykový	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
Fruchtbringende	Fruchtbringend	k1gInSc5
Gesellschaft	Gesellschaft	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
na	na	k7c6
pohřbu	pohřeb	k1gInSc6
matky	matka	k1gFnSc2
Doroty	Dorota	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
až	až	k9
jako	jako	k9
pátý	pátý	k4xOgInSc4
(	(	kIx(
<g/>
třetí	třetí	k4xOgFnSc4
přeživší	přeživší	k2eAgFnSc4d1
<g/>
)	)	kIx)
potomek	potomek	k1gMnSc1
svých	svůj	k3xOyFgMnPc2
rodičů	rodič	k1gMnPc2
a	a	k8xC
ani	ani	k8xC
nepředpokládalo	předpokládat	k5eNaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
někdy	někdy	k6eAd1
vévodství	vévodství	k1gNnSc4
mohl	moct	k5eAaImAgMnS
vládnout	vládnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vévodou	vévoda	k1gMnSc7
byl	být	k5eAaImAgInS
po	po	k7c6
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
nejstarší	starý	k2eAgMnSc1d3
bratr	bratr	k1gMnSc1
Jan	Jan	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
o	o	k7c4
veškeré	veškerý	k3xTgInPc4
tituly	titul	k1gInPc4
přišel	přijít	k5eAaPmAgInS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
odmítal	odmítat	k5eAaImAgMnS
podrobit	podrobit	k5eAaPmF
císaři	císař	k1gMnSc3
Ferdinandovi	Ferdinand	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
Štýrskému	štýrský	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vilém	Vilém	k1gMnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgMnS
vládcem	vládce	k1gMnSc7
vévodství	vévodství	k1gNnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
samotný	samotný	k2eAgInSc1d1
titul	titul	k1gInSc1
stále	stále	k6eAd1
náležel	náležet	k5eAaImAgInS
bratrovi	bratr	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1626	#num#	k4
získal	získat	k5eAaPmAgInS
titul	titul	k1gInSc1
vévody	vévoda	k1gMnSc2
Sasko-výmarského	sasko-výmarský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
Vilém	Vilém	k1gMnSc1
stal	stát	k5eAaPmAgMnS
držitelem	držitel	k1gMnSc7
Řádu	řád	k1gInSc2
stability	stabilita	k1gFnSc2
(	(	kIx(
<g/>
Orden	Orden	k1gInSc1
der	drát	k5eAaImRp2nS
Beständigkeit	Beständigkeit	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1622	#num#	k4
až	až	k8xS
1623	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
vlastenecký	vlastenecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Deutscher	Deutschra	k1gFnPc2
Friedbund	Friedbund	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
hlásal	hlásat	k5eAaImAgMnS
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
<g/>
,	,	kIx,
i	i	k9
náboženskou	náboženský	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štědré	štědrý	k2eAgFnSc2d1
finanční	finanční	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
se	se	k3xPyFc4
Vilémovi	Vilém	k1gMnSc3
dostalo	dostat	k5eAaPmAgNnS
například	například	k6eAd1
od	od	k7c2
strýce	strýc	k1gMnSc2
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Ludvíka	Ludvík	k1gMnSc2
I.	I.	kA
Anhaltsko-Köthenského	Anhaltsko-Köthenský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
mnoho	mnoho	k4c1
jiných	jiný	k2eAgMnPc2d1
vévodů	vévoda	k1gMnPc2
a	a	k8xC
knížat	kníže	k1gMnPc2wR
<g/>
,	,	kIx,
i	i	k8xC
Vilém	Vilém	k1gMnSc1
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
zapojit	zapojit	k5eAaPmF
do	do	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
–	–	k?
sloužil	sloužit	k5eAaImAgMnS
pod	pod	k7c7
Petrem	Petr	k1gMnSc7
Arnoštem	Arnošt	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mansfeldem	Mansfeldo	k1gNnSc7
a	a	k8xC
Jiřím	Jiří	k1gMnSc7
Fridrichem	Fridrich	k1gMnSc7
Bádensko-Durlašským	Bádensko-Durlašský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
později	pozdě	k6eAd2
začal	začít	k5eAaPmAgMnS
sloužit	sloužit	k5eAaImF
pod	pod	k7c7
Kristiánem	Kristián	k1gMnSc7
Brunšvickým	brunšvický	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
přerozdělování	přerozdělování	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1614	#num#	k4
Vilém	Vilém	k1gMnSc1
udržel	udržet	k5eAaPmAgMnS
Výmar	Výmar	k1gInSc4
i	i	k9
Jenu	jen	k1gInSc2
pohromadě	pohromadě	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
moci	moct	k5eAaImF
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
navíc	navíc	k6eAd1
získal	získat	k5eAaPmAgMnS
Eisenach	Eisenach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1644	#num#	k4
ale	ale	k8xC
Albrecht	Albrecht	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
Vilémovi	Vilém	k1gMnSc3
tak	tak	k8xS,k8xC
připadl	připadnout	k5eAaPmAgInS
i	i	k9
Eisenach	Eisenach	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
odvozováno	odvozován	k2eAgNnSc1d1
i	i	k8xC
Sasko-eisenašské	Sasko-eisenašský	k2eAgNnSc1d1
<g/>
,	,	kIx,
resp.	resp.	kA
Sasko-Výmarsko-eisenašské	Sasko-Výmarsko-eisenašský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vilémova	Vilémův	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
poměrně	poměrně	k6eAd1
dobré	dobrý	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
mohl	moct	k5eAaImAgMnS
především	především	k6eAd1
švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
Gustav	Gustav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
ale	ale	k8xC
Gustav	Gustav	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
Axel	Axel	k1gMnSc1
Oxenstierna	Oxenstierna	k1gFnSc1
dohnal	dohnat	k5eAaPmAgMnS
Viléma	Vilém	k1gMnSc4
k	k	k7c3
přistoupení	přistoupení	k1gNnSc3
na	na	k7c4
Pražský	pražský	k2eAgInSc4d1
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
v	v	k7c6
lednu	leden	k1gInSc6
1650	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
Vilémův	Vilémův	k2eAgMnSc1d1
strýc	strýc	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Anhaltsko-Köthenský	Anhaltsko-Köthenský	k2eAgMnSc1d1
<g/>
,	,	kIx,
shodli	shodnout	k5eAaPmAgMnP,k5eAaBmAgMnP
se	se	k3xPyFc4
členové	člen	k1gMnPc1
spolku	spolek	k1gInSc2
Deutscher	Deutschra	k1gFnPc2
Friedbund	Friedbund	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Vilém	Vilém	k1gMnSc1
stane	stanout	k5eAaPmIp3nS
Ludvíkovým	Ludvíkův	k2eAgMnSc7d1
nástupcem	nástupce	k1gMnSc7
a	a	k8xC
tedy	tedy	k9
i	i	k9
hlavou	hlava	k1gFnSc7
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1651	#num#	k4
a	a	k8xC
Vilém	Vilém	k1gMnSc1
si	se	k3xPyFc3
svoji	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4
držel	držet	k5eAaImAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
zemřel	zemřít	k5eAaPmAgMnS
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1662	#num#	k4
ve	v	k7c6
Výmaru	Výmar	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
čtyřiašedesát	čtyřiašedesát	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
v	v	k7c6
roli	role	k1gFnSc6
vévody	vévoda	k1gMnSc2
Sasko-výmarského	sasko-výmarský	k2eAgMnSc2d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Jan	Jan	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vládl	vládnout	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1683	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládci	vládce	k1gMnPc1
Sasko-eisenašského	Sasko-eisenašský	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
se	se	k3xPyFc4
po	po	k7c6
smrti	smrt	k1gFnSc6
správce	správce	k1gMnSc2
Viléma	Vilém	k1gMnSc2
stali	stát	k5eAaPmAgMnP
(	(	kIx(
<g/>
společně	společně	k6eAd1
<g/>
)	)	kIx)
Adolf	Adolf	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Sasko-Eisenašský	Sasko-Eisenašský	k2eAgMnSc1d1
a	a	k8xC
Jan	Jan	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
potomci	potomek	k1gMnPc1
</s>
<s>
Vilémova	Vilémův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
Eleonora	Eleonora	k1gFnSc1
Dorota	Dorota	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1625	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
Vilémovi	Vilémův	k2eAgMnPc1d1
27	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Eleonorou	Eleonora	k1gFnSc7
Dorotou	Dorota	k1gFnSc7
Anhaltsko-Desavskou	Anhaltsko-Desavský	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc4
nevlastní	vlastní	k2eNgFnSc1d1
sestřenicí	sestřenice	k1gFnSc7
(	(	kIx(
<g/>
byla	být	k5eAaImAgFnS
dcerou	dcera	k1gFnSc7
Jana	Jan	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vzešel	vzejít	k5eAaPmAgMnS
z	z	k7c2
prvního	první	k4xOgNnSc2
manželství	manželství	k1gNnSc2
Jáchyma	Jáchym	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
Anhaltského	Anhaltský	k2eAgMnSc2d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Vilémova	Vilémův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
Dorota	Dorota	k1gFnSc1
Marie	Marie	k1gFnSc1
vzešla	vzejít	k5eAaPmAgFnS
až	až	k9
z	z	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
druhého	druhý	k4xOgNnSc2
manželství	manželství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pár	pár	k1gInSc1
měl	mít	k5eAaImAgInS
devět	devět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
šest	šest	k4xCc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
dcery	dcera	k1gFnPc1
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
synové	syn	k1gMnPc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
dožili	dožít	k5eAaPmAgMnP
dospělosti	dospělost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vilém	Vilém	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
/	/	kIx~
<g/>
†	†	k?
1626	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
dětství	dětství	k1gNnSc6
</s>
<s>
Jan	Jan	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
(	(	kIx(
<g/>
1627	#num#	k4
<g/>
–	–	k?
<g/>
1683	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
Kristýna	Kristýna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Šlesvicko-Holštýnsko-Sonderburská	Šlesvicko-Holštýnsko-Sonderburský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Vilém	Vilém	k1gMnSc1
(	(	kIx(
<g/>
1630	#num#	k4
<g/>
–	–	k?
<g/>
1639	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
dětství	dětství	k1gNnSc6
</s>
<s>
Adolf	Adolf	k1gMnSc1
Vilém	Vilém	k1gMnSc1
(	(	kIx(
<g/>
1632	#num#	k4
<g/>
–	–	k?
<g/>
1668	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
Marie	Marie	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Jiří	Jiří	k1gMnSc1
(	(	kIx(
<g/>
1634	#num#	k4
<g/>
–	–	k?
<g/>
1686	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
Johannetta	Johannett	k1gInSc2
ze	z	k7c2
Sayn-Wittgensteinu	Sayn-Wittgenstein	k1gInSc2
</s>
<s>
Vilemína	Vilemína	k1gFnSc1
Eleonora	Eleonora	k1gFnSc1
(	(	kIx(
<g/>
1636	#num#	k4
<g/>
–	–	k?
<g/>
1653	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemřela	zemřít	k5eAaPmAgFnS
bezdětná	bezdětný	k2eAgFnSc1d1
</s>
<s>
Bernard	Bernard	k1gMnSc1
(	(	kIx(
<g/>
1638	#num#	k4
<g/>
–	–	k?
<g/>
1678	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
Marie	Marie	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
de	de	k?
la	la	k1gNnSc7
Trémoille	Trémoille	k1gFnSc2
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
(	(	kIx(
<g/>
1640	#num#	k4
<g/>
–	–	k?
<g/>
1656	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
</s>
<s>
Dorota	Dorota	k1gFnSc1
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
1641	#num#	k4
<g/>
–	–	k?
<g/>
1675	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
Mořic	Mořic	k1gMnSc1
Sasko-Zeitzský	Sasko-Zeitzský	k2eAgMnSc1d1
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Předkové	předek	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
Falcko-Simmernský	Falcko-Simmernský	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
</s>
<s>
Sibyla	Sibyla	k1gFnSc1
Klévská	Klévský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Výmarský	sasko-výmarský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcký	falcký	k2eAgInSc1d1
</s>
<s>
Dorota	Dorota	k1gFnSc1
Zuzana	Zuzana	k1gFnSc1
ze	z	k7c2
Simmernu	Simmerna	k1gFnSc4
</s>
<s>
Marie	Marie	k1gFnSc1
Braniborsko-Kulmbašská	Braniborsko-Kulmbašský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
V.	V.	kA
Anhaltsko-Zerbstský	Anhaltsko-Zerbstský	k2eAgMnSc5d1
</s>
<s>
Jáchym	Jáchym	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
Anhaltský	Anhaltský	k2eAgMnSc1d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Braniborská	braniborský	k2eAgFnSc1d1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Marie	Marie	k1gFnSc1
Anhaltská	Anhaltský	k2eAgFnSc1d1
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
Braniborsko-Ansbašská	Braniborsko-Ansbašský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
William	William	k1gInSc1
<g/>
,	,	kIx,
Duke	Duke	k1gFnSc1
of	of	k?
Saxe-Weimar	Saxe-Weimara	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vilém	Vilém	k1gMnSc1
Sasko-Výmarský	sasko-výmarský	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
102104387	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
0468	#num#	k4
8747	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
12686414	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
</s>
