<p>
<s>
Vodojemy	vodojem	k1gInPc1	vodojem
Žlutý	žlutý	k2eAgInSc1d1	žlutý
kopec	kopec	k1gInSc4	kopec
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
Tvrdého	tvrdé	k1gNnSc2	tvrdé
a	a	k8xC	a
Tomešovou	Tomešová	k1gFnSc4	Tomešová
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
úbočí	úbočí	k1gNnSc6	úbočí
Žlutého	žlutý	k2eAgInSc2d1	žlutý
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
vodojemy	vodojem	k1gInPc1	vodojem
byly	být	k5eAaImAgInP	být
vystavěny	vystavět	k5eAaPmNgInP	vystavět
postupně	postupně	k6eAd1	postupně
mezi	mezi	k7c7	mezi
60	[number]	k4	60
<g/>
.	.	kIx.	.
lety	let	k1gInPc7	let
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
druhým	druhý	k4xOgNnSc7	druhý
desetiletím	desetiletí	k1gNnSc7	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
byly	být	k5eAaImAgFnP	být
prohlášeny	prohlásit	k5eAaPmNgFnP	prohlásit
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
projekt	projekt	k1gInSc1	projekt
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
vodovodu	vodovod	k1gInSc2	vodovod
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
anglického	anglický	k2eAgMnSc2d1	anglický
stavitele	stavitel	k1gMnSc2	stavitel
Thomase	Thomas	k1gMnSc2	Thomas
Docwryho	Docwry	k1gMnSc2	Docwry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1872	[number]	k4	1872
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
úpravna	úpravna	k1gFnSc1	úpravna
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
Pisárkách	Pisárka	k1gFnPc6	Pisárka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odebírala	odebírat	k5eAaImAgFnS	odebírat
surovou	surový	k2eAgFnSc4d1	surová
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
nad	nad	k7c7	nad
jezem	jez	k1gInSc7	jez
v	v	k7c6	v
Kamenném	kamenný	k2eAgInSc6d1	kamenný
mlýně	mlýn	k1gInSc6	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Upravená	upravený	k2eAgFnSc1d1	upravená
byla	být	k5eAaImAgFnS	být
čerpána	čerpat	k5eAaImNgFnS	čerpat
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
tlakových	tlakový	k2eAgNnPc2d1	tlakové
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nižší	nízký	k2eAgFnSc4d2	nižší
tlakové	tlakový	k2eAgNnSc1d1	tlakové
pásmo	pásmo	k1gNnSc1	pásmo
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Žlutém	žlutý	k2eAgInSc6d1	žlutý
kopci	kopec	k1gInSc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1872	[number]	k4	1872
byl	být	k5eAaImAgMnS	být
postaven	postavit	k5eAaPmNgMnS	postavit
první	první	k4xOgInPc4	první
ze	z	k7c2	z
zdejších	zdejší	k2eAgInPc2d1	zdejší
vodojemů	vodojem	k1gInPc2	vodojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
jednou	jednou	k6eAd1	jednou
podzemní	podzemní	k2eAgFnSc7d1	podzemní
nádrží	nádrž	k1gFnSc7	nádrž
o	o	k7c6	o
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
rozměrech	rozměr	k1gInPc6	rozměr
45	[number]	k4	45
<g/>
×	×	k?	×
<g/>
45	[number]	k4	45
m	m	kA	m
a	a	k8xC	a
hloubce	hloubka	k1gFnSc6	hloubka
6	[number]	k4	6
m.	m.	k?	m.
Nádrž	nádrž	k1gFnSc1	nádrž
je	být	k5eAaImIp3nS	být
vyzděna	vyzdít	k5eAaPmNgFnS	vyzdít
pálenými	pálený	k2eAgFnPc7d1	pálená
červenými	červený	k2eAgFnPc7d1	červená
cihlami	cihla	k1gFnPc7	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
vodojem	vodojem	k1gInSc1	vodojem
tvoří	tvořit	k5eAaImIp3nS	tvořit
obdobná	obdobný	k2eAgFnSc1d1	obdobná
nádrž	nádrž	k1gFnSc1	nádrž
(	(	kIx(	(
<g/>
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
45	[number]	k4	45
<g/>
×	×	k?	×
<g/>
70	[number]	k4	70
m	m	kA	m
<g/>
)	)	kIx)	)
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
vodojem	vodojem	k1gInSc1	vodojem
byl	být	k5eAaImAgInS	být
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
desetiletí	desetiletí	k1gNnSc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
betonovými	betonový	k2eAgFnPc7d1	betonová
nádržemi	nádrž	k1gFnPc7	nádrž
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
<g/>
×	×	k?	×
<g/>
35	[number]	k4	35
m	m	kA	m
a	a	k8xC	a
30	[number]	k4	30
<g/>
×	×	k?	×
<g/>
45	[number]	k4	45
m	m	kA	m
a	a	k8xC	a
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
přes	přes	k7c4	přes
5	[number]	k4	5
m.	m.	k?	m.
Mezi	mezi	k7c7	mezi
prvním	první	k4xOgInSc7	první
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
vodojemem	vodojem	k1gInSc7	vodojem
stojí	stát	k5eAaImIp3nS	stát
bývalý	bývalý	k2eAgInSc1d1	bývalý
technický	technický	k2eAgInSc1d1	technický
domek	domek	k1gInSc1	domek
<g/>
.	.	kIx.	.
</s>
<s>
Vodojemy	vodojem	k1gInPc1	vodojem
na	na	k7c6	na
Žlutém	žlutý	k2eAgInSc6d1	žlutý
kopci	kopec	k1gInSc6	kopec
byly	být	k5eAaImAgFnP	být
odstaveny	odstavit	k5eAaPmNgFnP	odstavit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
ukončení	ukončení	k1gNnSc2	ukončení
provozu	provoz	k1gInSc6	provoz
byla	být	k5eAaImAgFnS	být
nízká	nízký	k2eAgFnSc1d1	nízká
poloha	poloha	k1gFnSc1	poloha
vodojemů	vodojem	k1gInPc2	vodojem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodojemy	vodojem	k1gInPc4	vodojem
Žlutý	žlutý	k2eAgInSc4d1	žlutý
kopec	kopec	k1gInSc4	kopec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vodojemy	vodojem	k1gInPc1	vodojem
při	při	k7c6	při
ulici	ulice	k1gFnSc6	ulice
Tvrdého	tvrdé	k1gNnSc2	tvrdé
</s>
</p>
