<s>
Hartmut	Hartmut	k1gMnSc1	Hartmut
Losch	Losch	k1gMnSc1	Losch
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Angermünde	Angermünd	k1gMnSc5	Angermünd
<g/>
,	,	kIx,	,
Braniborsko	Braniborsko	k1gNnSc1	Braniborsko
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Neu	Neu	k1gFnSc1	Neu
Fahrland	Fahrlanda	k1gFnPc2	Fahrlanda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
východoněmecký	východoněmecký	k2eAgMnSc1d1	východoněmecký
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
diskem	disk	k1gInSc7	disk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
získal	získat	k5eAaPmAgMnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohrál	prohrát	k5eAaPmAgInS	prohrát
jen	jen	k9	jen
s	s	k7c7	s
Detlefem	Detlef	k1gMnSc7	Detlef
Thorithem	Thorith	k1gInSc7	Thorith
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
mistrem	mistr	k1gMnSc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
když	když	k8xS	když
disk	disk	k1gInSc4	disk
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
61,82	[number]	k4	61,82
m.	m.	k?	m.
Stříbro	stříbro	k1gNnSc4	stříbro
získal	získat	k5eAaPmAgMnS	získat
Švéd	Švéd	k1gMnSc1	Švéd
Ricky	Ricka	k1gFnSc2	Ricka
Bruch	bruch	k1gInSc1	bruch
a	a	k8xC	a
bronz	bronz	k1gInSc1	bronz
Lothar	Lothar	k1gMnSc1	Lothar
Milde	Mild	k1gInSc5	Mild
z	z	k7c2	z
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
1971	[number]	k4	1971
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
skončil	skončit	k5eAaPmAgMnS	skončit
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
na	na	k7c6	na
pátém	pátý	k4xOgInSc6	pátý
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
60,86	[number]	k4	60,86
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k1gNnSc1	México
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
obsadil	obsadit	k5eAaPmAgMnS	obsadit
výkonem	výkon	k1gInSc7	výkon
62,12	[number]	k4	62,12
metru	metro	k1gNnSc6	metro
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
zde	zde	k6eAd1	zde
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
československý	československý	k2eAgMnSc1d1	československý
diskař	diskař	k1gMnSc1	diskař
Ludvík	Ludvík	k1gMnSc1	Ludvík
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poslal	poslat	k5eAaPmAgMnS	poslat
disk	disk	k1gInSc4	disk
o	o	k7c4	o
80	[number]	k4	80
centimetrů	centimetr	k1gInPc2	centimetr
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
1964	[number]	k4	1964
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
neprošel	projít	k5eNaPmAgMnS	projít
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
sports-reference	sportseferenka	k1gFnSc6	sports-referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
