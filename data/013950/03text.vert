<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
</s>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
Svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Nepomucký	Nepomucký	k1gMnSc1
u	u	k7c2
Bílého	bílý	k2eAgInSc2d1
mostu	most	k1gInSc2
ve	v	k7c6
Špindlerově	Špindlerův	k2eAgInSc6d1
Mlýně	mlýn	k1gInSc6
</s>
<s>
znak	znak	k1gInSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
město	město	k1gNnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0525	CZ0525	k4
579742	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Vrchlabí	Vrchlabí	k1gNnSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Trutnov	Trutnov	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
525	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Královéhradecký	královéhradecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
34	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
34	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
098	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
76,9	76,9	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
718	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
543	#num#	k4
51	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
4	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
4	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
15	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
městského	městský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
MěÚ	MěÚ	k?
Špindlerův	Špindlerův	k2eAgInSc4d1
Mlýn	mlýn	k1gInSc4
Svatopetrská	svatopetrský	k2eAgFnSc1d1
173	#num#	k4
543	#num#	k4
51	#num#	k4
Špindlerův	Špindlerův	k2eAgInSc4d1
Mlýn	mlýn	k1gInSc4
podatelna@mestospindleruvmlyn.cz	podatelna@mestospindleruvmlyn.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Bc.	Bc.	k?
Vladimír	Vladimír	k1gMnSc1
Staruch	starucha	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.mestospindleruvmlyn.cz	www.mestospindleruvmlyn.cz	k1gInSc1
</s>
<s>
Úřední	úřední	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.spindleruvmlyn.org	www.spindleruvmlyn.org	k1gInSc1
</s>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Spindlermühle	Spindlermühle	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
a	a	k8xC
zároveň	zároveň	k6eAd1
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3
horské	horský	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
1	#num#	k4
100	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
7690,91	7690,91	k4
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
končí	končit	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
295	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
navazuje	navazovat	k5eAaImIp3nS
horská	horský	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
na	na	k7c4
Špindlerovu	Špindlerův	k2eAgFnSc4d1
boudu	bouda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
V	v	k7c6
němčině	němčina	k1gFnSc6
se	se	k3xPyFc4
město	město	k1gNnSc1
dříve	dříve	k6eAd2
nazývalo	nazývat	k5eAaImAgNnS
Spindlermühle	Spindlermühle	k1gFnSc4
podle	podle	k7c2
rodiny	rodina	k1gFnSc2
Spindlerových	Spindlerových	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vlastnila	vlastnit	k5eAaImAgFnS
v	v	k7c6
obci	obec	k1gFnSc6
mlýn	mlýn	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Předtím	předtím	k6eAd1
se	se	k3xPyFc4
však	však	k9
obec	obec	k1gFnSc1
vždy	vždy	k6eAd1
jmenovala	jmenovat	k5eAaBmAgFnS,k5eAaImAgFnS
Svatý	svatý	k2eAgMnSc1d1
Petr	Petr	k1gMnSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
část	část	k1gFnSc1
města	město	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
v	v	k7c6
žádosti	žádost	k1gFnSc6
o	o	k7c4
povolení	povolení	k1gNnSc4
stavby	stavba	k1gFnSc2
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
zaslané	zaslaný	k2eAgFnPc4d1
císaři	císař	k1gMnPc7
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
u	u	k7c2
podpisů	podpis	k1gInPc2
rovněž	rovněž	k9
uvedeno	uveden	k2eAgNnSc1d1
–	–	k?
sepsáno	sepsán	k2eAgNnSc1d1
ve	v	k7c6
Špindlerově	Špindlerův	k2eAgInSc6d1
mlýně	mlýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádost	žádost	k1gFnSc1
totiž	totiž	k9
občané	občan	k1gMnPc1
sepisovali	sepisovat	k5eAaImAgMnP
právě	právě	k6eAd1
v	v	k7c6
tomto	tento	k3xDgInSc6
mlýně	mlýn	k1gInSc6
Špindlerů	Špindler	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
jelikož	jelikož	k8xS
jsou	být	k5eAaImIp3nP
podobné	podobný	k2eAgInPc1d1
názvy	název	k1gInPc1
obcí	obec	k1gFnPc2
v	v	k7c6
německy	německy	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
časté	častý	k2eAgNnSc1d1
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
omylu	omyl	k1gInSc3
a	a	k8xC
zpět	zpět	k6eAd1
se	se	k3xPyFc4
vrátilo	vrátit	k5eAaPmAgNnS
povolení	povolení	k1gNnSc1
ke	k	k7c3
stavbě	stavba	k1gFnSc3
kostela	kostel	k1gInSc2
v	v	k7c6
obci	obec	k1gFnSc6
Špindlerův	Špindlerův	k2eAgInSc4d1
Mlýn	mlýn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občané	občan	k1gMnPc1
raději	rád	k6eAd2
obec	obec	k1gFnSc4
přejmenovali	přejmenovat	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
stavět	stavět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaslání	zaslání	k1gNnSc1
nové	nový	k2eAgFnSc2d1
žádosti	žádost	k1gFnSc2
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
brána	brát	k5eAaImNgFnS
jako	jako	k8xS,k8xC
urážka	urážka	k1gFnSc1
neomylného	omylný	k2eNgInSc2d1
úřadu	úřad	k1gInSc2
a	a	k8xC
kostel	kostel	k1gInSc4
by	by	kYmCp3nS
tu	tu	k6eAd1
už	už	k6eAd1
určitě	určitě	k6eAd1
nestál	stát	k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
stabilním	stabilní	k2eAgInSc6d1
katastru	katastr	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1842	#num#	k4
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
Špindlerův	Špindlerův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
jako	jako	k8xC,k8xS
Břetenský	Břetenský	k2eAgInSc1d1
mlejn	mlejn	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
je	být	k5eAaImIp3nS
běžně	běžně	k6eAd1
nazýván	nazývat	k5eAaImNgInS
Vřetenový	vřetenový	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
naivním	naivní	k2eAgInSc7d1
překladem	překlad	k1gInSc7
německého	německý	k2eAgMnSc2d1
Spindelmühle	Spindelmühle	k1gMnSc2
(	(	kIx(
<g/>
Spindel	Spindlo	k1gNnPc2
–	–	k?
vřeteno	vřeteno	k1gNnSc1
<g/>
,	,	kIx,
Mühle	Mühle	k1gInSc1
–	–	k?
mlýn	mlýn	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3
dochovaný	dochovaný	k2eAgInSc1d1
záznam	záznam	k1gInSc1
o	o	k7c6
Špindlerově	Špindlerův	k2eAgInSc6d1
mlýně	mlýn	k1gInSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
počátku	počátek	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
několik	několik	k4yIc1
chalup	chalupa	k1gFnPc2
sloužilo	sloužit	k5eAaImAgNnS
jako	jako	k8xS,k8xC
útočiště	útočiště	k1gNnSc1
horníkům	horník	k1gMnPc3
pracujícím	pracující	k2eAgNnPc3d1
v	v	k7c6
nedalekých	daleký	k2eNgFnPc6d1
hutích	huť	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
zpracovávaly	zpracovávat	k5eAaImAgFnP
železnou	železný	k2eAgFnSc4d1
rudu	ruda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
lety	let	k1gInPc4
1516	#num#	k4
<g/>
–	–	k?
<g/>
1521	#num#	k4
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
povolil	povolit	k5eAaPmAgMnS
horníkům	horník	k1gMnPc3
lhůty	lhůta	k1gFnSc2
k	k	k7c3
placení	placení	k1gNnSc3
desátků	desátek	k1gInPc2
a	a	k8xC
úlevy	úleva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžba	těžba	k1gFnSc1
rud	ruda	k1gFnPc2
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
mědi	měď	k1gFnSc2
a	a	k8xC
arsenu	arsen	k1gInSc2
přilákala	přilákat	k5eAaPmAgFnS
Kryštofa	Kryštof	k1gMnSc4
z	z	k7c2
Gendorfu	Gendorf	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
posléze	posléze	k6eAd1
získal	získat	k5eAaPmAgInS
vrchlabské	vrchlabský	k2eAgNnSc4d1
panství	panství	k1gNnSc4
a	a	k8xC
výrazně	výrazně	k6eAd1
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
Vrchlabí	Vrchlabí	k1gNnSc1
roku	rok	k1gInSc2
1533	#num#	k4
povýšeno	povýšit	k5eAaPmNgNnS
na	na	k7c4
horní	horní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1621	#num#	k4
<g/>
,	,	kIx,
dokonce	dokonce	k9
putoval	putovat	k5eAaImAgInS
do	do	k7c2
Prahy	Praha	k1gFnSc2
ze	z	k7c2
Svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
náklad	náklad	k1gInSc4
mědi	měď	k1gFnSc2
a	a	k8xC
stříbra	stříbro	k1gNnSc2
cca	cca	kA
za	za	k7c4
10	#num#	k4
000	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
těžbu	těžba	k1gFnSc4
přerušila	přerušit	k5eAaPmAgFnS
třicetiletá	třicetiletý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
doly	dol	k1gInPc1
změnily	změnit	k5eAaPmAgInP
majitele	majitel	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
snahy	snaha	k1gFnPc1
těžbu	těžba	k1gFnSc4
obnovit	obnovit	k5eAaPmF
se	se	k3xPyFc4
ukázaly	ukázat	k5eAaPmAgInP
marné	marný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
o	o	k7c4
znovuobnovení	znovuobnovení	k1gNnSc4
těžby	těžba	k1gFnSc2
proběhly	proběhnout	k5eAaPmAgInP
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
ve	v	k7c6
Špindlerově	Špindlerův	k2eAgInSc6d1
Mlýně	mlýn	k1gInSc6
začal	začít	k5eAaPmAgInS
rozvíjet	rozvíjet	k5eAaImF
cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
proměně	proměna	k1gFnSc3
několika	několik	k4yIc2
opuštěných	opuštěný	k2eAgFnPc2d1
chalup	chalupa	k1gFnPc2
na	na	k7c4
horské	horský	k2eAgNnSc4d1
letovisko	letovisko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
dostupnosti	dostupnost	k1gFnSc2
celé	celý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
přispělo	přispět	k5eAaPmAgNnS
zbudování	zbudování	k1gNnSc1
silnice	silnice	k1gFnSc2
z	z	k7c2
Vrchlabí	Vrchlabí	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1888	#num#	k4
zde	zde	k6eAd1
začaly	začít	k5eAaPmAgFnP
fungovat	fungovat	k5eAaImF
lázně	lázeň	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
Špindlerova	Špindlerův	k2eAgInSc2d1
Mlýna	mlýn	k1gInSc2
celkem	celkem	k6eAd1
18	#num#	k4
hotelů	hotel	k1gInPc2
a	a	k8xC
restaurací	restaurace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
(	(	kIx(
<g/>
pohostinství	pohostinství	k1gNnSc2
<g/>
)	)	kIx)
tak	tak	k9
pomalu	pomalu	k6eAd1
začal	začít	k5eAaPmAgInS
nahrazovat	nahrazovat	k5eAaImF
pastevní	pastevní	k2eAgMnPc4d1
(	(	kIx(
<g/>
tedy	tedy	k9
budní	budnět	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
)	)	kIx)
i	i	k8xC
těžební	těžební	k2eAgNnSc1d1
hospodářství	hospodářství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
populární	populární	k2eAgMnSc1d1
trávit	trávit	k5eAaImF
čas	čas	k1gInSc4
především	především	k9
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
začaly	začít	k5eAaPmAgFnP
provozovat	provozovat	k5eAaImF
i	i	k9
zimní	zimní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
sedačková	sedačkový	k2eAgFnSc1d1
lanovka	lanovka	k1gFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
zprovozněna	zprovoznit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
<g/>
,	,	kIx,
vedla	vést	k5eAaImAgFnS
ze	z	k7c2
Svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
na	na	k7c4
Pláň	pláň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
43	#num#	k4
hotelů	hotel	k1gInPc2
<g/>
,	,	kIx,
134	#num#	k4
penzionů	penzion	k1gInPc2
a	a	k8xC
23	#num#	k4
horských	horský	k2eAgFnPc2d1
bud	bouda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středisko	středisko	k1gNnSc1
navštěvují	navštěvovat	k5eAaImIp3nP
především	především	k9
čeští	český	k2eAgMnPc1d1
turisté	turist	k1gMnPc1
<g/>
,	,	kIx,
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
sem	sem	k6eAd1
jezdí	jezdit	k5eAaImIp3nP
také	také	k9
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
Poláci	Polák	k1gMnPc1
či	či	k8xC
Nizozemci	Nizozemec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
pastevní	pastevní	k2eAgNnPc4d1
hospodářství	hospodářství	k1gNnPc4
a	a	k8xC
celkově	celkově	k6eAd1
zemědělství	zemědělství	k1gNnSc1
přestalo	přestat	k5eAaPmAgNnS
na	na	k7c4
úkor	úkor	k1gInSc4
turistickému	turistický	k2eAgInSc3d1
ruchu	ruch	k1gInSc3
provozovat	provozovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sporty	sport	k1gInPc1
</s>
<s>
Zima	zima	k1gFnSc1
</s>
<s>
Město	město	k1gNnSc1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
k	k	k7c3
nejznámějším	známý	k2eAgNnPc3d3
a	a	k8xC
nejnavštěvovanějším	navštěvovaný	k2eAgNnPc3d3
střediskům	středisko	k1gNnPc3
zimních	zimní	k2eAgInPc2d1
sportů	sport	k1gInPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lůžková	lůžkový	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
10	#num#	k4
tisíc	tisíc	k4xCgInPc2
lůžek	lůžko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
má	mít	k5eAaImIp3nS
pro	pro	k7c4
zimní	zimní	k2eAgInPc4d1
sporty	sport	k1gInPc4
ideální	ideální	k2eAgFnSc2d1
podmínky	podmínka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lyžařské	lyžařský	k2eAgInPc1d1
a	a	k8xC
běžecké	běžecký	k2eAgFnPc1d1
tratě	trať	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
provozu	provoz	k1gInSc6
až	až	k9
5	#num#	k4
měsíců	měsíc	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionálně	profesionálně	k6eAd1
upravované	upravovaný	k2eAgFnPc4d1
sjezdové	sjezdový	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
různého	různý	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
obtížnosti	obtížnost	k1gFnSc2
dosahují	dosahovat	k5eAaImIp3nP
celkové	celkový	k2eAgFnPc1d1
délky	délka	k1gFnPc1
25	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
14	#num#	k4
lanovek	lanovka	k1gFnPc2
a	a	k8xC
vleků	vlek	k1gInPc2
zajišťuje	zajišťovat	k5eAaImIp3nS
přepravní	přepravní	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
17	#num#	k4
500	#num#	k4
lyžařů	lyžař	k1gMnPc2
za	za	k7c4
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jedním	jeden	k4xCgInSc7
skipasem	skipas	k1gInSc7
lze	lze	k6eAd1
lyžovat	lyžovat	k5eAaImF
na	na	k7c6
sjezdovkách	sjezdovka	k1gFnPc6
v	v	k7c6
5	#num#	k4
lyžařských	lyžařský	k2eAgInPc6d1
areálech	areál	k1gInPc6
(	(	kIx(
<g/>
Stoh	stoh	k1gInSc1
<g/>
,	,	kIx,
Medvědín	Medvědín	k1gInSc1
<g/>
,	,	kIx,
Hromovka	Hromovka	k1gFnSc1
<g/>
,	,	kIx,
Horní	horní	k2eAgFnPc1d1
Mísečky	Mísečky	k1gFnPc1
a	a	k8xC
Labská	labský	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
sjezdovek	sjezdovka	k1gFnPc2
tu	tu	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
nižší	nízký	k2eAgFnSc2d2
sněhové	sněhový	k2eAgFnSc2d1
pokrývky	pokrývka	k1gFnSc2
uměle	uměle	k6eAd1
zasněžují	zasněžovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Léto	léto	k1gNnSc1
</s>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
nabízí	nabízet	k5eAaImIp3nS
mnoho	mnoho	k4c4
atrakcí	atrakce	k1gFnPc2
i	i	k9
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letních	letní	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
ideální	ideální	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
pro	pro	k7c4
pěší	pěší	k2eAgFnSc4d1
turistiku	turistika	k1gFnSc4
a	a	k8xC
horská	horský	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Špindlerově	Špindlerův	k2eAgInSc6d1
Mlýně	mlýn	k1gInSc6
se	se	k3xPyFc4
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
nachází	nacházet	k5eAaImIp3nS
i	i	k9
aquapark	aquapark	k1gInSc1
<g/>
,	,	kIx,
bobová	bobový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
či	či	k8xC
lanové	lanový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
křižovatkou	křižovatka	k1gFnSc7
turistických	turistický	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
samotném	samotný	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
začíná	začínat	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
turistická	turistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
<g/>
,	,	kIx,
Harrachovská	harrachovský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
vede	vést	k5eAaImIp3nS
na	na	k7c4
Labskou	labský	k2eAgFnSc4d1
boudu	bouda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolí	okolí	k1gNnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
navštívit	navštívit	k5eAaPmF
turistické	turistický	k2eAgFnPc4d1
chaty	chata	k1gFnPc4
Špindlerovu	Špindlerův	k2eAgFnSc4d1
boudu	bouda	k1gFnSc4
<g/>
,	,	kIx,
ruinu	ruina	k1gFnSc4
Petrovy	Petrův	k2eAgFnSc2d1
boudy	bouda	k1gFnSc2
a	a	k8xC
další	další	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolí	okolí	k1gNnPc1
Špindlerova	Špindlerův	k2eAgInSc2d1
Mlýna	mlýn	k1gInSc2
lze	lze	k6eAd1
projít	projít	k5eAaPmF
po	po	k7c6
turistických	turistický	k2eAgFnPc6d1
trasách	trasa	k1gFnPc6
přes	přes	k7c4
Horní	horní	k2eAgFnPc4d1
Mísečky	Mísečky	k1gFnPc4
<g/>
,	,	kIx,
pramen	pramen	k1gInSc4
Labe	Labe	k1gNnSc4
<g/>
,	,	kIx,
Sněžné	sněžný	k2eAgFnPc4d1
jámy	jáma	k1gFnPc4
a	a	k8xC
Labskou	labský	k2eAgFnSc4d1
boudu	bouda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
vede	vést	k5eAaImIp3nS
turistická	turistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
na	na	k7c4
Výrovku	výrovka	k1gFnSc4
nebo	nebo	k8xC
na	na	k7c4
Friesovy	Friesův	k2eAgFnPc4d1
boudy	bouda	k1gFnPc4
a	a	k8xC
chalupu	chalupa	k1gFnSc4
Na	na	k7c6
Rozcestí	rozcestí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
Špindlerova	Špindlerův	k2eAgInSc2d1
Mlýna	mlýn	k1gInSc2
vede	vést	k5eAaImIp3nS
vcelku	vcelku	k6eAd1
přímá	přímý	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
na	na	k7c6
Sněžku	sněžek	k1gInSc6
<g/>
;	;	kIx,
měří	měřit	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
10	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
575	#num#	k4
–	–	k?
<g/>
1555	#num#	k4
m	m	kA
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
obce	obec	k1gFnSc2
je	být	k5eAaImIp3nS
Luční	luční	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
při	při	k7c6
soutoku	soutok	k1gInSc6
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc2
a	a	k8xC
Svatopetrského	svatopetrský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
nazývaného	nazývaný	k2eAgInSc2d1
též	též	k9
Dolský	dolský	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Bílý	bílý	k2eAgInSc1d1
most	most	k1gInSc1
</s>
<s>
kostel	kostel	k1gInSc1
sv.	sv.	kA
Petra	Petra	k1gFnSc1
a	a	k8xC
Pavla	Pavla	k1gFnSc1
z	z	k7c2
let	léto	k1gNnPc2
1802	#num#	k4
<g/>
–	–	k?
<g/>
1807	#num#	k4
<g/>
;	;	kIx,
do	do	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
zvonu	zvon	k1gInSc2
prý	prý	k9
bylo	být	k5eAaImAgNnS
přidáno	přidán	k2eAgNnSc4d1
stříbro	stříbro	k1gNnSc4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bílý	bílý	k2eAgInSc1d1
most	most	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Labská	labský	k2eAgFnSc1d1
z	z	k7c2
let	léto	k1gNnPc2
1910	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
</s>
<s>
Zvonička	zvonička	k1gFnSc1
na	na	k7c6
Labské	labský	k2eAgFnSc6d1
z	z	k7c2
roku	rok	k1gInSc2
1828	#num#	k4
</s>
<s>
Části	část	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Město	město	k1gNnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
části	část	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
odpovídají	odpovídat	k5eAaImIp3nP
katastrům	katastr	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
ty	ten	k3xDgMnPc4
dále	daleko	k6eAd2
na	na	k7c4
základní	základní	k2eAgFnPc4d1
sídelní	sídelní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
část	část	k1gFnSc4
obcekatastrzákladní	obcekatastrzákladný	k2eAgMnPc1d1
sídelní	sídelní	k2eAgMnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
MlýnŠpindlerův	MlýnŠpindlerův	k2eAgInSc1d1
MlýnBrádlerovy	MlýnBrádlerův	k2eAgFnPc1d1
boudy	bouda	k1gFnPc1
<g/>
,	,	kIx,
Davidovy	Davidův	k2eAgFnPc1d1
Boudy	bouda	k1gFnPc1
<g/>
,	,	kIx,
Jelení	jelení	k2eAgFnPc1d1
boudy	bouda	k1gFnPc1
<g/>
,	,	kIx,
Medvědí	medvědí	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
<g/>
,	,	kIx,
Petrovka	Petrovka	k1gFnSc1
<g/>
,	,	kIx,
Svatý	svatý	k2eAgMnSc1d1
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Špindlerovka	Špindlerovka	k1gFnSc1
<g/>
,	,	kIx,
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
Tabulové	tabulový	k2eAgFnPc1d1
Boudy	bouda	k1gFnPc1
</s>
<s>
BedřichovBedřichov	BedřichovBedřichov	k1gInSc1
v	v	k7c4
KrkonošíchBedřichov	KrkonošíchBedřichov	k1gInSc4
<g/>
,	,	kIx,
Martinovka	Martinovka	k1gFnSc1
</s>
<s>
LabskáLabskáLabská	LabskáLabskáLabský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Čerstvá	čerstvý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
</s>
<s>
Přední	přední	k2eAgFnPc1d1
LabskáPřední	LabskáPřední	k2eAgFnPc1d1
LabskáKlínové	LabskáKlínový	k2eAgFnPc1d1
boudy	bouda	k1gFnPc1
<g/>
,	,	kIx,
Přední	přední	k2eAgFnSc1d1
Labská	labský	k2eAgFnSc1d1
</s>
<s>
Filmotéka	filmotéka	k1gFnSc1
</s>
<s>
zimní	zimní	k2eAgFnSc1d1
Výrovka	výrovka	k1gFnSc1
</s>
<s>
Vrchní	vrchní	k1gMnSc1
<g/>
,	,	kIx,
prchni	prchnout	k5eAaPmRp2nS
<g/>
!	!	kIx.
</s>
<s>
Homolka	homolka	k1gFnSc1
a	a	k8xC
Tobolka	tobolka	k1gFnSc1
</s>
<s>
Jak	jak	k6eAd1
vytrhnout	vytrhnout	k5eAaPmF
velrybě	velryba	k1gFnSc3
stoličku	stolička	k1gFnSc4
</s>
<s>
Snowboarďáci	Snowboarďák	k1gMnPc1
</s>
<s>
Padesátka	padesátka	k1gFnSc1
</s>
<s>
Rozpaky	rozpak	k1gInPc1
kuchaře	kuchař	k1gMnSc2
Svatopluka	Svatopluk	k1gMnSc2
</s>
<s>
Špindl	Špindnout	k5eAaPmAgMnS
</s>
<s>
Špindl	Špindnout	k5eAaPmAgMnS
2	#num#	k4
</s>
<s>
Poslední	poslední	k2eAgInSc1d1
závod	závod	k1gInSc1
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Anna	Anna	k1gFnSc1
K	k	k7c3
</s>
<s>
Eva	Eva	k1gFnSc1
Samková	Samková	k1gFnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Podgórzyn	Podgórzyn	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Alanya	Alanya	k1gFnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
SPITZEROVÁ	SPITZEROVÁ	kA
<g/>
,	,	kIx,
Barbora	Barbora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvnost	návštěvnost	k1gFnSc1
střediska	středisko	k1gNnSc2
Špindlerův	Špindlerův	k2eAgInSc4d1
Mlýn	mlýn	k1gInSc4
v	v	k7c6
letním	letní	k2eAgNnSc6d1
a	a	k8xC
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihlava	Jihlava	k1gFnSc1
<g/>
:	:	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PALÁTKOVÁ	PALÁTKOVÁ	kA
<g/>
,	,	kIx,
Dáša	Dáša	k1gFnSc1
<g/>
;	;	kIx,
RICHTER	Richter	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špindlerův	Špindlerův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
1793	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krkonoše	Krkonoše	k1gFnPc1
<g/>
,	,	kIx,
Jizerské	jizerský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
Krkonošského	krkonošský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
36	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
18	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Z	z	k7c2
našich	náš	k3xOp1gInPc2
časopisů	časopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
.	.	kIx.
1930	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠMILAUER	ŠMILAUER	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
toponomastiky	toponomastika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Budní	Budný	k1gMnPc1
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bradlerovy	Bradlerův	k2eAgFnPc4d1
boudy	bouda	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Toulavá	toulavý	k2eAgFnSc1d1
kamera	kamera	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
68	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7316	#num#	k4
<g/>
-	-	kIx~
<g/>
228	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
↑	↑	k?
Základní	základní	k2eAgFnPc4d1
sídelní	sídelní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
-	-	kIx~
Obec	obec	k1gFnSc4
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ve	v	k7c6
Špindlerově	Špindlerův	k2eAgInSc6d1
Mlýně	mlýn	k1gInSc6
</s>
<s>
Tatry	Tatra	k1gFnPc1
mountain	mountaina	k1gFnPc2
resorts	resortsa	k1gFnPc2
-	-	kIx~
Skiareál	Skiareál	k1gMnSc1
Špindl	Špindl	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Špindlerův	Špindlerův	k2eAgInSc4d1
Mlýn	mlýn	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Města	město	k1gNnSc2
Špindlerův	Špindlerův	k2eAgInSc4d1
Mlýn	mlýn	k1gInSc4
</s>
<s>
Obsáhlé	obsáhlý	k2eAgFnPc1d1
německé	německý	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
o	o	k7c6
historii	historie	k1gFnSc6
i	i	k8xC
současnosti	současnost	k1gFnSc6
města	město	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
města	město	k1gNnSc2
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
BedřichovLabskáPřední	BedřichovLabskáPřední	k2eAgInSc1d1
LabskáŠpindlerův	LabskáŠpindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
Ostatní	ostatní	k2eAgInSc1d1
</s>
<s>
Brádlerovy	Brádlerův	k2eAgFnPc1d1
boudyČerstvá	boudyČerstvá	k1gFnSc1
VodaKlínové	VodaKlínový	k2eAgFnPc4d1
BoudyMartinova	BoudyMartinův	k2eAgNnPc4d1
boudaMedvědí	boudaMedvědí	k1gNnPc4
boudaŠpindlerova	boudaŠpindlerův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
</s>
<s>
Města	město	k1gNnPc1
<g/>
,	,	kIx,
městyse	městys	k1gInPc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Trutnov	Trutnov	k1gInSc1
</s>
<s>
Batňovice	Batňovice	k1gFnSc1
•	•	k?
Bernartice	Bernartice	k1gFnSc2
•	•	k?
Bílá	bílý	k2eAgFnSc1d1
Třemešná	Třemešná	k1gFnSc1
•	•	k?
Bílé	bílý	k2eAgMnPc4d1
Poličany	Poličan	k1gMnPc4
•	•	k?
Borovnice	borovnice	k1gFnSc1
•	•	k?
Borovnička	Borovnička	k1gFnSc1
•	•	k?
Čermná	Čermný	k2eAgFnSc1d1
•	•	k?
Černý	černý	k2eAgInSc1d1
Důl	důl	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Branná	branný	k2eAgFnSc1d1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Brusnice	brusnice	k1gFnSc1
•	•	k?
Dolní	dolní	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Kalná	Kalná	k1gFnSc1
•	•	k?
Dolní	dolní	k2eAgInSc4d1
Lánov	Lánov	k1gInSc4
•	•	k?
Dolní	dolní	k2eAgFnSc2d1
Olešnice	Olešnice	k1gFnSc2
•	•	k?
Doubravice	Doubravice	k1gFnSc2
•	•	k?
Dubenec	Dubenec	k1gInSc1
•	•	k?
Dvůr	Dvůr	k1gInSc1
Králové	Králová	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Hajnice	hajnice	k1gFnSc2
•	•	k?
Havlovice	Havlovice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
Brusnice	brusnice	k1gFnSc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Kalná	Kalná	k1gFnSc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Maršov	Maršov	k1gInSc4
•	•	k?
Horní	horní	k2eAgFnSc2d1
Olešnice	Olešnice	k1gFnSc2
•	•	k?
Hostinné	hostinný	k2eAgNnSc1d1
•	•	k?
Hřibojedy	Hřibojeda	k1gMnSc2
•	•	k?
Chotěvice	Chotěvice	k1gFnSc2
•	•	k?
Choustníkovo	Choustníkův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Chvaleč	Chvaleč	k1gMnSc1
•	•	k?
Janské	janský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
•	•	k?
Jívka	jívka	k1gFnSc1
•	•	k?
Klášterská	klášterský	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Kocbeře	Kocbera	k1gFnSc3
•	•	k?
Kohoutov	Kohoutov	k1gInSc1
•	•	k?
Královec	Královec	k1gInSc1
•	•	k?
Kuks	Kuks	k1gInSc1
•	•	k?
Kunčice	Kunčice	k1gFnPc4
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Lampertice	Lampertice	k1gFnSc2
•	•	k?
Lánov	Lánov	k1gInSc1
•	•	k?
Lanžov	Lanžov	k1gInSc1
•	•	k?
Libňatov	Libňatov	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Libotov	Libotov	k1gInSc1
•	•	k?
Litíč	Litíč	k1gFnSc1
•	•	k?
Malá	malý	k2eAgFnSc1d1
Úpa	Úpa	k1gFnSc1
•	•	k?
Malé	Malé	k2eAgFnPc4d1
Svatoňovice	Svatoňovice	k1gFnPc4
•	•	k?
Maršov	Maršov	k1gInSc1
u	u	k7c2
Úpice	Úpice	k1gFnSc2
•	•	k?
Mladé	mladý	k2eAgInPc1d1
Buky	buk	k1gInPc1
•	•	k?
Mostek	mostek	k1gInSc1
•	•	k?
Nemojov	Nemojov	k1gInSc1
•	•	k?
Pec	Pec	k1gFnSc1
pod	pod	k7c7
Sněžkou	Sněžka	k1gFnSc7
•	•	k?
Pilníkov	Pilníkov	k1gInSc4
•	•	k?
Prosečné	Prosečný	k2eAgFnSc2d1
•	•	k?
Radvanice	Radvanice	k1gFnSc2
•	•	k?
Rtyně	Rtyeň	k1gFnSc2
v	v	k7c6
Podkrkonoší	Podkrkonoší	k1gNnSc6
•	•	k?
Rudník	rudník	k1gMnSc1
•	•	k?
Stanovice	Stanovice	k1gFnSc2
•	•	k?
Staré	Staré	k2eAgInPc1d1
Buky	buk	k1gInPc1
•	•	k?
Strážné	strážná	k1gFnSc2
•	•	k?
Suchovršice	Suchovršice	k1gFnSc2
•	•	k?
Svoboda	Svoboda	k1gMnSc1
nad	nad	k7c7
Úpou	Úpa	k1gFnSc7
•	•	k?
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
•	•	k?
Trotina	Trotina	k1gFnSc1
•	•	k?
Trutnov	Trutnov	k1gInSc1
•	•	k?
Třebihošť	Třebihošť	k1gFnSc2
•	•	k?
Úpice	Úpice	k1gFnSc2
•	•	k?
Velké	velký	k2eAgFnPc1d1
Svatoňovice	Svatoňovice	k1gFnPc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Vřešťov	Vřešťov	k1gInSc1
•	•	k?
Vilantice	Vilantice	k1gFnSc2
•	•	k?
Vítězná	vítězný	k2eAgFnSc1d1
•	•	k?
Vlčice	vlčice	k1gFnSc1
•	•	k?
Vlčkovice	Vlčkovice	k1gFnSc2
v	v	k7c6
Podkrkonoší	Podkrkonoší	k1gNnSc6
•	•	k?
Vrchlabí	Vrchlabí	k1gNnPc2
•	•	k?
Zábřezí-Řečice	Zábřezí-Řečice	k1gFnSc2
•	•	k?
Zdobín	Zdobín	k1gMnSc1
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
Olešnice	Olešnice	k1gFnSc1
•	•	k?
Žacléř	Žacléř	k1gInSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
131545	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4322784-3	4322784-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
98044135	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
139734072	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
98044135	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
