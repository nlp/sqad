<s>
Špindlerův	Špindlerův	k2eAgInSc1d1
Mlýn	mlýn	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Spindlermühle	Spindlermühle	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
a	a	k8xC
zároveň	zároveň	k6eAd1
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3
horské	horský	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
1	#num#	k4
100	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>