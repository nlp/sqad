<s>
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
CR	cr	k0
Vasco	Vasca	k1gMnSc5
da	da	k?
Gama	gama	k1gNnSc1
(	(	kIx(
<g/>
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnSc1
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnPc2
<g/>
,	,	kIx,
portugalský	portugalský	k2eAgMnSc1d1
objevitel	objevitel	k1gMnSc1
a	a	k8xC
mořeplavec	mořeplavec	k1gMnSc1
(	(	kIx(
<g/>
1524	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
1469	#num#	k4
<g/>
Sines	Sines	k1gInSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1524	#num#	k4
<g/>
Kóčin	Kóčina	k1gFnPc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
malárie	malárie	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
kostel	kostel	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
(	(	kIx(
<g/>
1524	#num#	k4
<g/>
–	–	k?
<g/>
1539	#num#	k4
<g/>
)	)	kIx)
<g/>
Klášter	klášter	k1gInSc1
svatého	svatý	k1gMnSc2
JeronýmaKostel	JeronýmaKostel	k1gInSc4
svaté	svatý	k2eAgFnSc2d1
Engrácie	Engrácie	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
objevitel	objevitel	k1gMnSc1
<g/>
,	,	kIx,
cestovatel	cestovatel	k1gMnSc1
a	a	k8xC
mořeplavec	mořeplavec	k1gMnSc1
Děti	dítě	k1gFnPc4
</s>
<s>
Estê	Estê	k1gMnSc1
da	da	k?
GamaCristóvã	GamaCristóvã	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Estê	Estê	k6eAd1
da	da	k?
Gama	gama	k1gNnSc2
a	a	k8xC
Izabel	Izabela	k1gFnPc2
Sodre	Sodr	k1gInSc5
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Paulo	Paula	k1gFnSc5
da	da	k?
Gama	gama	k1gNnSc6
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
Viceroy	Viceroa	k1gFnPc1
of	of	k?
Portuguese	Portuguese	k1gFnSc2
India	indium	k1gNnSc2
(	(	kIx(
<g/>
září	září	k1gNnSc2
1524	#num#	k4
–	–	k?
prosinec	prosinec	k1gInSc1
1524	#num#	k4
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
hrob	hrob	k1gInSc1
Vasca	Vascum	k1gNnSc2
de	de	k?
Gamy	game	k1gInPc1
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Františka	František	k1gMnSc2
v	v	k7c6
Kóčinu	Kóčin	k1gInSc6
</s>
<s>
Vasco	Vasco	k6eAd1
da	da	k?
Gama	gama	k1gNnPc1
[	[	kIx(
<g/>
vašku	vašku	k6eAd1
da	da	k?
gama	gama	k1gNnSc1
<g/>
]	]	kIx)
(	(	kIx(
<g/>
1469	#num#	k4
Sines	Sines	k1gInSc1
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1524	#num#	k4
Kóčin	Kóčina	k1gFnPc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
portugalských	portugalský	k2eAgMnPc2d1
mořeplavců	mořeplavec	k1gMnPc2
a	a	k8xC
objevitelů	objevitel	k1gMnPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1519	#num#	k4
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Vidigueiry	Vidigueira	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgInPc7
třemi	tři	k4xCgFnPc7
cestami	cesta	k1gFnPc7
do	do	k7c2
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
1497	#num#	k4
<g/>
–	–	k?
<g/>
1499	#num#	k4
<g/>
,	,	kIx,
1502	#num#	k4
<g/>
–	–	k?
<g/>
1503	#num#	k4
<g/>
,	,	kIx,
1524	#num#	k4
<g/>
)	)	kIx)
položil	položit	k5eAaPmAgMnS
základy	základ	k1gInPc4
pozdější	pozdní	k2eAgFnSc2d2
portugalské	portugalský	k2eAgFnSc2d1
koloniální	koloniální	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vasco	Vasco	k1gNnSc1
Da	Da	k1gMnPc2
Gama	gama	k1gNnSc2
byl	být	k5eAaImAgInS
třetí	třetí	k4xOgFnSc4
z	z	k7c2
pěti	pět	k4xCc2
synů	syn	k1gMnPc2
venkovského	venkovský	k2eAgMnSc2d1
šlechtice	šlechtic	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
působil	působit	k5eAaImAgMnS
jako	jako	k8xS,k8xC
guvernér	guvernér	k1gMnSc1
přístavního	přístavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Sines	Sinesa	k1gFnPc2
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Portugalska	Portugalsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jeho	jeho	k3xOp3gNnSc6
mládí	mládí	k1gNnSc6
se	se	k3xPyFc4
mnoho	mnoho	k4c1
zpráv	zpráva	k1gFnPc2
nedochovalo	dochovat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1480	#num#	k4
po	po	k7c6
vzoru	vzor	k1gInSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
Řádu	řád	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
od	od	k7c2
meče	meč	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
velmistrem	velmistr	k1gMnSc7
byl	být	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1481	#num#	k4
portugalský	portugalský	k2eAgMnSc1d1
král	král	k1gMnSc1
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
..	..	k?
</s>
<s>
Koncem	koncem	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
po	po	k7c6
pádu	pád	k1gInSc6
Konstantinopole	Konstantinopol	k1gInSc2
(	(	kIx(
<g/>
1453	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
obchod	obchod	k1gInSc1
s	s	k7c7
cizokrajným	cizokrajný	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
především	především	k9
s	s	k7c7
kořením	koření	k1gNnSc7
<g/>
,	,	kIx,
monopolizován	monopolizovat	k5eAaBmNgInS
arabskými	arabský	k2eAgMnPc7d1
a	a	k8xC
osmanskými	osmanský	k2eAgMnPc7d1
panovníky	panovník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portugalský	portugalský	k2eAgMnSc1d1
král	král	k1gMnSc1
Manuel	Manuel	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
získat	získat	k5eAaPmF
přístup	přístup	k1gInSc1
do	do	k7c2
Asie	Asie	k1gFnSc2
obeplutím	obeplutí	k1gNnSc7
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
cestou	cesta	k1gFnSc7
byl	být	k5eAaImAgMnS
pověřen	pověřit	k5eAaPmNgMnS
posléze	posléze	k6eAd1
Vasco	Vasco	k6eAd1
da	da	k?
Gama	gama	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
první	první	k4xOgFnSc2
úspěšné	úspěšný	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1499	#num#	k4
mu	on	k3xPp3gMnSc3
král	král	k1gMnSc1
za	za	k7c4
odměnu	odměna	k1gFnSc4
udělil	udělit	k5eAaPmAgInS
dědičný	dědičný	k2eAgInSc1d1
šlechtický	šlechtický	k2eAgInSc1d1
titul	titul	k1gInSc1
s	s	k7c7
doživotní	doživotní	k2eAgFnSc7d1
rentou	renta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidělil	přidělit	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
také	také	k9
správu	správa	k1gFnSc4
nad	nad	k7c7
městem	město	k1gNnSc7
Sines	Sinesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
však	však	k8xC
patřilo	patřit	k5eAaImAgNnS
Řádu	řád	k1gInSc3
svatého	svatý	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
velmistr	velmistr	k1gMnSc1
nechtěl	chtít	k5eNaImAgMnS
rozhodnutí	rozhodnutí	k1gNnSc4
krále	král	k1gMnSc2
respektovat	respektovat	k5eAaImF
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
Vasco	Vasco	k6eAd1
da	da	k?
Gama	gama	k1gNnSc2
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
členem	člen	k1gMnSc7
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
po	po	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
trvajícím	trvající	k2eAgNnSc6d1
marném	marný	k2eAgNnSc6d1
úsilí	úsilí	k1gNnSc6
o	o	k7c4
získání	získání	k1gNnSc4
Sines	Sinesa	k1gFnPc2
nakonec	nakonec	k6eAd1
z	z	k7c2
řádu	řád	k1gInSc2
vystoupil	vystoupit	k5eAaPmAgInS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1507	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Kristova	Kristův	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1502	#num#	k4
byl	být	k5eAaImAgInS
Vasco	Vasco	k1gNnSc4
da	da	k?
Gama	gama	k1gNnSc2
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
admirálem	admirál	k1gMnSc7
arabských	arabský	k2eAgNnPc2d1
<g/>
,	,	kIx,
perských	perský	k2eAgNnPc2d1
a	a	k8xC
indických	indický	k2eAgNnPc2d1
moří	moře	k1gNnPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
celého	celý	k2eAgInSc2d1
orientu	orient	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1501	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Catarinou	Catarina	k1gFnSc7
de	de	k?
Ataide	Ataid	k1gMnSc5
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
prominentního	prominentní	k2eAgMnSc2d1
šlechtice	šlechtic	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
spolu	spolu	k6eAd1
šest	šest	k4xCc4
synů	syn	k1gMnPc2
a	a	k8xC
jednu	jeden	k4xCgFnSc4
dceru	dcera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cesta	cesta	k1gFnSc1
do	do	k7c2
Indie	Indie	k1gFnSc2
roku	rok	k1gInSc2
1502	#num#	k4
byla	být	k5eAaImAgFnS
provázena	provázet	k5eAaImNgFnS
několika	několik	k4yIc7
incidenty	incident	k1gInPc4
vůči	vůči	k7c3
domorodému	domorodý	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
a	a	k8xC
tím	ten	k3xDgNnSc7
utrpěla	utrpět	k5eAaPmAgFnS
da	da	k?
Gamova	Gamův	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Manuel	Manuel	k1gMnSc1
I.	I.	kA
ho	on	k3xPp3gMnSc4
při	při	k7c6
jmenování	jmenování	k1gNnSc6
guvernéra	guvernér	k1gMnSc4
a	a	k8xC
místokrále	místokrál	k1gMnSc4
Indie	Indie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1505	#num#	k4
opomenul	opomenout	k5eAaPmAgInS
a	a	k8xC
titul	titul	k1gInSc4
získal	získat	k5eAaPmAgMnS
Francisco	Francisco	k1gMnSc1
de	de	k?
Almeida	Almeida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Následujících	následující	k2eAgInPc2d1
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
žil	žít	k5eAaImAgMnS
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc2
klidným	klidný	k2eAgInSc7d1
životem	život	k1gInSc7
<g/>
,	,	kIx,
stranou	strana	k1gFnSc7
pozornosti	pozornost	k1gFnSc2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marně	marně	k6eAd1
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
královu	králův	k2eAgFnSc4d1
přízeň	přízeň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
změnila	změnit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1518	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Fernã	Fernã	k1gMnSc1
de	de	k?
Magalhã	Magalhã	k1gMnSc1
po	po	k7c6
roztržce	roztržka	k1gFnSc6
s	s	k7c7
králem	král	k1gMnSc7
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnPc2
vyhrožoval	vyhrožovat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
bude	být	k5eAaImBp3nS
následovat	následovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nechtěl	chtít	k5eNaImAgMnS
ztratit	ztratit	k5eAaPmF
svého	svůj	k3xOyFgMnSc4
admirála	admirál	k1gMnSc4
východních	východní	k2eAgNnPc2d1
moří	moře	k1gNnPc2
<g/>
,	,	kIx,
mu	on	k3xPp3gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
1519	#num#	k4
udělil	udělit	k5eAaPmAgInS
titul	titul	k1gInSc1
hraběte	hrabě	k1gMnSc2
z	z	k7c2
Vidigueiry	Vidigueira	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1524	#num#	k4
byl	být	k5eAaImAgInS
novým	nový	k2eAgMnSc7d1
králem	král	k1gMnSc7
Janem	Jan	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
do	do	k7c2
funkce	funkce	k1gFnSc2
místokrále	místokrál	k1gMnSc2
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
zajistil	zajistit	k5eAaPmAgMnS
dobré	dobrý	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
syny	syn	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
po	po	k7c6
příjezdu	příjezd	k1gInSc6
do	do	k7c2
Indie	Indie	k1gFnSc2
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnPc2
onemocněl	onemocnět	k5eAaPmAgInS
malárií	malárie	k1gFnSc7
a	a	k8xC
na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
roku	rok	k1gInSc2
1524	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
kostele	kostel	k1gInSc6
svatého	svatý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
v	v	k7c6
Kóčinu	Kóčin	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1539	#num#	k4
byly	být	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
převezeny	převezen	k2eAgInPc1d1
do	do	k7c2
Portugalska	Portugalsko	k1gNnSc2
(	(	kIx(
<g/>
Vidigueira	Vidigueiro	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1880	#num#	k4
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
hrob	hrob	k1gInSc1
v	v	k7c6
klášteře	klášter	k1gInSc6
jeronymitů	jeronymit	k1gInPc2
v	v	k7c6
lisabonské	lisabonský	k2eAgFnSc3d1
čtvrti	čtvrt	k1gFnSc3
Belém	Belé	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc2
tam	tam	k6eAd1
odpočívá	odpočívat	k5eAaImIp3nS
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
králů	král	k1gMnPc2
Manuela	Manuel	k1gMnSc2
I.	I.	kA
a	a	k8xC
Jana	Jana	k1gFnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Cesty	cesta	k1gFnPc4
</s>
<s>
Objevitelská	objevitelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1497	#num#	k4
<g/>
–	–	k?
<g/>
1499	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
Indie	Indie	k1gFnSc2
1497	#num#	k4
<g/>
–	–	k?
<g/>
1499	#num#	k4
</s>
<s>
Vasco	Vasco	k6eAd1
da	da	k?
Gama	gama	k1gNnSc2
opustil	opustit	k5eAaPmAgInS
Lisabon	Lisabon	k1gInSc1
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1497	#num#	k4
se	s	k7c7
čtyřmi	čtyři	k4xCgFnPc7
plachetnicemi	plachetnice	k1gFnPc7
<g/>
:	:	kIx,
dvě	dva	k4xCgFnPc1
silné	silný	k2eAgFnPc1d1
expediční	expediční	k2eAgFnPc1d1
o	o	k7c6
výtlaku	výtlak	k1gInSc6
100	#num#	k4
až	až	k9
200	#num#	k4
tun	tuna	k1gFnPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
rychlá	rychlý	k2eAgFnSc1d1
karavela	karavela	k1gFnSc1
a	a	k8xC
zásobovací	zásobovací	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expediční	expediční	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
Sā	Sā	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
velel	velet	k5eAaImAgMnS
Sám	sám	k3xTgMnSc1
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
byl	být	k5eAaImAgMnS
velitelem	velitel	k1gMnSc7
jeho	jeho	k3xOp3gFnSc2
bratr	bratr	k1gMnSc1
Paulo	Paula	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádky	posádka	k1gFnPc1
tvořilo	tvořit	k5eAaImAgNnS
dohromady	dohromady	k6eAd1
asi	asi	k9
170	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
zpět	zpět	k6eAd1
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
vrátila	vrátit	k5eAaPmAgFnS
asi	asi	k9
třetina	třetina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc1
zastávka	zastávka	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
Madeiře	Madeira	k1gFnSc6
a	a	k8xC
potom	potom	k6eAd1
pokračovali	pokračovat	k5eAaImAgMnP
ke	k	k7c3
Kapverdským	kapverdský	k2eAgInPc3d1
ostrovům	ostrov	k1gInPc3
a	a	k8xC
odtud	odtud	k6eAd1
jihovýchodním	jihovýchodní	k2eAgInSc7d1
směrem	směr	k1gInSc7
k	k	k7c3
jižnímu	jižní	k2eAgNnSc3d1
pobřeží	pobřeží	k1gNnSc3
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
obepluli	obeplout	k5eAaPmAgMnP
mys	mys	k1gInSc4
Dobré	dobrý	k2eAgFnSc2d1
naděje	naděje	k1gFnSc2
a	a	k8xC
pokračovali	pokračovat	k5eAaImAgMnP
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jednom	jeden	k4xCgInSc6
setkání	setkání	k1gNnSc6
s	s	k7c7
Hotentoty	Hotentot	k1gMnPc7
byl	být	k5eAaImAgMnS
da	da	k?
Gama	gama	k1gNnSc6
zraněn	zranit	k5eAaPmNgMnS
oštěpem	oštěp	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mosselském	Mosselský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
byla	být	k5eAaImAgFnS
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
příkaz	příkaz	k1gInSc4
spálena	spálen	k2eAgFnSc1d1
zásobovací	zásobovací	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
již	již	k6eAd1
splnila	splnit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
účel	účel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosáhli	dosáhnout	k5eAaPmAgMnP
ostrova	ostrov	k1gInSc2
Santa	Sant	k1gInSc2
Cruz	Cruza	k1gFnPc2
v	v	k7c6
zálivu	záliv	k1gInSc6
Algoa	Algo	k1gInSc2
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
k	k	k7c3
ústí	ústí	k1gNnSc3
řeky	řeka	k1gFnSc2
Rio	Rio	k1gFnSc2
do	do	k7c2
Infante	infant	k1gMnSc5
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Velká	velký	k2eAgFnSc1d1
rybí	rybí	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
vrátit	vrátit	k5eAaPmF
Bartolomeu	Bartolomea	k1gFnSc4
Dias	Diasa	k1gFnPc2
při	při	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
do	do	k7c2
Indie	Indie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1487	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc6
zastávkách	zastávka	k1gFnPc6
na	na	k7c6
východoafrickém	východoafrický	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jim	on	k3xPp3gMnPc3
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
většinou	většinou	k6eAd1
projevovali	projevovat	k5eAaImAgMnP
nepřátelství	nepřátelství	k1gNnSc4
<g/>
,	,	kIx,
dopluli	doplout	k5eAaPmAgMnP
do	do	k7c2
Malindi	Malind	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc4
tam	tam	k6eAd1
doplnil	doplnit	k5eAaPmAgMnS
zásoby	zásoba	k1gFnPc4
a	a	k8xC
pro	pro	k7c4
další	další	k2eAgFnSc4d1
plavbu	plavba	k1gFnSc4
získal	získat	k5eAaPmAgMnS
zkušeného	zkušený	k2eAgMnSc4d1
lodivoda	lodivod	k1gMnSc4
<g/>
,	,	kIx,
proslulého	proslulý	k2eAgMnSc2d1
arabského	arabský	k2eAgMnSc2d1
mořeplavce	mořeplavec	k1gMnSc2
a	a	k8xC
navigátora	navigátor	k1gMnSc2
Ahmada	Ahmada	k1gFnSc1
ibn	ibn	k?
Mádžida	Mádžida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jeho	jeho	k3xOp3gNnSc7
přispěním	přispění	k1gNnSc7
dosáhl	dosáhnout	k5eAaPmAgInS
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1498	#num#	k4
indického	indický	k2eAgInSc2d1
subkontinentu	subkontinent	k1gInSc2
u	u	k7c2
města	město	k1gNnSc2
Kóžikkót	Kóžikkóta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
jednání	jednání	k1gNnSc6
s	s	k7c7
místním	místní	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
si	se	k3xPyFc3
Portugalci	Portugalec	k1gMnPc1
odvážejí	odvážet	k5eAaImIp3nP
zpět	zpět	k6eAd1
příslib	příslib	k1gInSc4
vzájemného	vzájemný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tříměsíční	tříměsíční	k2eAgFnSc6d1
strastiplné	strastiplný	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
Indickým	indický	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
se	s	k7c7
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1499	#num#	k4
dostali	dostat	k5eAaPmAgMnP
k	k	k7c3
pobřeží	pobřeží	k1gNnSc3
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
posádky	posádka	k1gFnSc2
zbylo	zbýt	k5eAaPmAgNnS
na	na	k7c6
každé	každý	k3xTgFnSc6
lodi	loď	k1gFnSc6
po	po	k7c6
sedmi	sedm	k4xCc2
či	či	k8xC
osmi	osm	k4xCc2
zdravých	zdravý	k2eAgFnPc2d1
mužích	muž	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Da	Da	k1gMnPc3
Gama	gama	k1gNnSc2
nechal	nechat	k5eAaPmAgMnS
spálit	spálit	k5eAaPmF
jednu	jeden	k4xCgFnSc4
loď	loď	k1gFnSc4
a	a	k8xC
posádku	posádka	k1gFnSc4
rozdělil	rozdělit	k5eAaPmAgInS
na	na	k7c4
zbývající	zbývající	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
lodi	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Atlantském	atlantský	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
se	se	k3xPyFc4
lodě	loď	k1gFnPc1
v	v	k7c6
bouři	bouř	k1gFnSc6
vzájemně	vzájemně	k6eAd1
ztratily	ztratit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
doplula	doplout	k5eAaPmAgFnS
do	do	k7c2
Portugalska	Portugalsko	k1gNnSc2
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1499	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnPc2
plul	plout	k5eAaImAgMnS
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
nemocným	nemocný	k1gMnSc7
bratrem	bratr	k1gMnSc7
Paulem	Paul	k1gMnSc7
přes	přes	k7c4
Kapverdské	kapverdský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
na	na	k7c4
Azory	Azory	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
Paulo	Paula	k1gFnSc5
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domů	dům	k1gInPc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
až	až	k9
začátkem	začátkem	k7c2
září	září	k1gNnSc2
roku	rok	k1gInSc2
1499	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobyvatelská	dobyvatelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1502	#num#	k4
<g/>
–	–	k?
<g/>
1503	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Druhou	druhý	k4xOgFnSc4
výpravu	výprava	k1gFnSc4
do	do	k7c2
Indie	Indie	k1gFnSc2
uskutečnil	uskutečnit	k5eAaPmAgInS
da	da	k?
Gama	gama	k1gNnSc6
roku	rok	k1gInSc2
1502	#num#	k4
již	již	k6eAd1
jako	jako	k8xS,k8xC
admirál	admirál	k1gMnSc1
<g/>
;	;	kIx,
velel	velet	k5eAaImAgInS
deseti	deset	k4xCc3
válečným	válečný	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
úlohou	úloha	k1gFnSc7
bylo	být	k5eAaImAgNnS
dosažení	dosažení	k1gNnSc1
a	a	k8xC
upevnění	upevnění	k1gNnSc1
portugalské	portugalský	k2eAgFnSc2d1
hegemonie	hegemonie	k1gFnSc2
v	v	k7c6
Arabském	arabský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
a	a	k8xC
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
námořní	námořní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
porazil	porazit	k5eAaPmAgMnS
da	da	k?
Gama	gama	k1gNnPc1
jednu	jeden	k4xCgFnSc4
arabskou	arabský	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
s	s	k7c7
použitím	použití	k1gNnSc7
násilí	násilí	k1gNnSc2
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
Cannanore	Cannanor	k1gInSc5
<g/>
)	)	kIx)
dosáhl	dosáhnout	k5eAaPmAgInS
v	v	k7c6
mnoha	mnoho	k4c6
městech	město	k1gNnPc6
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Indie	Indie	k1gFnSc2
uznání	uznání	k1gNnSc4
Portugalska	Portugalsko	k1gNnSc2
jako	jako	k8xS,k8xC
nadřízené	nadřízený	k2eAgFnSc2d1
mocnosti	mocnost	k1gFnSc2
<g/>
;	;	kIx,
položil	položit	k5eAaPmAgMnS
tak	tak	k6eAd1
základní	základní	k2eAgInSc4d1
kámen	kámen	k1gInSc4
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
budoucí	budoucí	k2eAgFnSc2d1
Portugalské	portugalský	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
cesty	cesta	k1gFnSc2
bylo	být	k5eAaImAgNnS
založení	založení	k1gNnSc4
obchodních	obchodní	k2eAgNnPc2d1
středisek	středisko	k1gNnPc2
v	v	k7c6
Kóžikkótu	Kóžikkót	k1gInSc6
a	a	k8xC
Kananuru	Kananur	k1gInSc6
(	(	kIx(
<g/>
Kannur	Kannur	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Místokrál	místokrál	k1gMnSc1
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
1524	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Třetí	třetí	k4xOgFnSc4
cestu	cesta	k1gFnSc4
podnikl	podniknout	k5eAaPmAgMnS
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc4
roku	rok	k1gInSc2
1524	#num#	k4
jako	jako	k8xC,k8xS
nově	nově	k6eAd1
jmenovaný	jmenovaný	k2eAgMnSc1d1
místokrál	místokrál	k1gMnSc1
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
asi	asi	k9
po	po	k7c6
čtyřměsíčním	čtyřměsíční	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
však	však	k9
v	v	k7c6
Kóčinu	Kóčin	k1gInSc6
na	na	k7c6
Malabarském	Malabarský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Jeho	jeho	k3xOp3gInPc4
činy	čin	k1gInPc4
zvěčnil	zvěčnit	k5eAaPmAgInS
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1572	#num#	k4
<g/>
)	)	kIx)
slavný	slavný	k2eAgMnSc1d1
portugalský	portugalský	k2eAgMnSc1d1
básník	básník	k1gMnSc1
Luís	Luísa	k1gFnPc2
Vaz	vaz	k1gInSc4
de	de	k?
Camõ	Camõ	k1gInSc1
v	v	k7c6
hrdinském	hrdinský	k2eAgInSc6d1
eposu	epos	k1gInSc6
„	„	k?
<g/>
Lusovci	Lusovec	k1gMnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
televizní	televizní	k2eAgFnSc6d1
anketě	anketa	k1gFnSc6
Naši	náš	k3xOp1gMnPc1
velcí	velký	k2eAgMnPc1d1
Portugalci	Portugalec	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
na	na	k7c6
desátém	desátý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnSc1
</s>
<s>
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnSc1
</s>
<s>
Pomník	pomník	k1gInSc1
v	v	k7c6
rodném	rodný	k2eAgNnSc6d1
městě	město	k1gNnSc6
Sines	Sinesa	k1gFnPc2
</s>
<s>
Hrob	hrob	k1gInSc1
v	v	k7c6
Klášteře	klášter	k1gInSc6
sv.	sv.	kA
Jeronýma	Jeroným	k1gMnSc2
v	v	k7c6
Lisabonu	Lisabon	k1gInSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Vasco	Vasco	k6eAd1
da	da	k?
Gama	gama	k1gNnPc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.1	.1	k4
2	#num#	k4
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc4
<g/>
:	:	kIx,
Do	do	k7c2
Indie	Indie	k1gFnSc2
kolem	kolem	k7c2
Afriky	Afrika	k1gFnSc2
aneb	aneb	k?
Jak	jak	k8xC,k8xS
Portugalci	Portugalec	k1gMnPc1
objevovali	objevovat	k5eAaImAgMnP
svět	svět	k1gInSc4
-	-	kIx~
Lidé	člověk	k1gMnPc1
a	a	k8xC
Země	zem	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lideazeme	Lideazeme	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
HOUDEK	Houdek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemožitelé	přemožitel	k1gMnPc1
času	čas	k1gInSc2
sv.	sv.	kA
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Vasco	Vasco	k1gNnSc1
da	da	k?
Gama	gama	k1gNnPc2
<g/>
,	,	kIx,
s.	s.	k?
178	#num#	k4
<g/>
-	-	kIx~
<g/>
182	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
Lusiads	Lusiads	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1800-1882	1800-1882	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Gama	gama	k1gNnSc2
<g/>
,	,	kIx,
dom	dom	k?
Vasco	Vasco	k1gMnSc1
da	da	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
9	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
888	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KLÍMA	Klíma	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
:	:	kIx,
Zámořské	zámořský	k2eAgInPc4d1
objevy	objev	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc4
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1
</s>
<s>
Věk	věk	k1gInSc1
zámořských	zámořský	k2eAgInPc2d1
objevů	objev	k1gInPc2
</s>
<s>
Zámořské	zámořský	k2eAgInPc4d1
objevy	objev	k1gInPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vasco	Vasco	k6eAd1
da	da	k?
Gama	gama	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Vasco	Vasco	k1gNnSc4
da	da	k?
Gama	gama	k1gNnSc2
</s>
<s>
Éra	éra	k1gFnSc1
Vasca	Vasc	k1gInSc2
da	da	k?
Gamu	game	k1gInSc2
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Portugalští	portugalský	k2eAgMnPc1d1
mořeplavci	mořeplavec	k1gMnPc1
a	a	k8xC
cestovatelé	cestovatel	k1gMnPc1
</s>
<s>
Afonso	Afonsa	k1gFnSc5
de	de	k?
Albuquerque	Albuquerqu	k1gFnSc2
•	•	k?
Jean	Jean	k1gMnSc1
Alphonse	Alphonse	k1gFnSc2
•	•	k?
Fernã	Fernã	k1gMnSc1
Pires	Pires	k1gMnSc1
de	de	k?
Andrade	Andrad	k1gInSc5
•	•	k?
António	António	k1gNnSc4
de	de	k?
Abreu	Abreus	k1gInSc2
•	•	k?
Afonso	Afonsa	k1gFnSc5
Gonçalves	Gonçalves	k1gMnSc1
Baldaia	Baldaium	k1gNnSc2
•	•	k?
Gaspar	Gaspar	k1gMnSc1
Boccaro	Boccara	k1gFnSc5
•	•	k?
Goncalvo	Goncalvo	k1gNnSc1
Velho	Vel	k1gMnSc2
Cabral	Cabral	k1gMnSc2
•	•	k?
Pedro	Pedro	k1gNnSc1
Álvares	Álvares	k1gMnSc1
Cabral	Cabral	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Rodriguez	Rodriguez	k1gMnSc1
Cabrillo	Cabrillo	k1gNnSc1
•	•	k?
Alviso	Alvisa	k1gFnSc5
Cadamosto	Cadamosta	k1gMnSc5
•	•	k?
Diogo	Diogo	k1gMnSc1
Cã	Cã	k1gNnSc1
•	•	k?
Hermenegildo	Hermenegildo	k1gNnSc1
Cardos	Cardos	k1gMnSc1
de	de	k?
Brito	Brito	k1gNnSc4
Capelo	Capela	k1gFnSc5
•	•	k?
Joã	Joã	k1gMnSc1
de	de	k?
Castro	Castro	k1gNnSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Gaspar	Gaspar	k1gMnSc1
Corte-Real	Corte-Real	k1gMnSc1
•	•	k?
Joã	Joã	k6eAd1
Vaz	vaz	k1gInSc1
Corte-Real	Corte-Real	k1gInSc1
•	•	k?
Pedro	Pedro	k1gNnSc1
da	da	k?
Covilhã	Covilhã	k1gMnSc1
•	•	k?
Tristã	Tristã	k1gMnSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
•	•	k?
Bartolomeu	Bartolomeus	k1gInSc2
Dias	Dias	k1gInSc1
•	•	k?
Dinis	Dinis	k1gInSc1
Dias	Dias	k1gInSc1
•	•	k?
Diogo	Diogo	k6eAd1
Dias	Dias	k1gInSc1
•	•	k?
Pê	Pê	k1gNnSc1
Dias	Dias	k1gInSc1
•	•	k?
Gil	Gil	k1gFnSc2
Eanes	Eanesa	k1gFnPc2
•	•	k?
Pê	Pê	k1gNnSc1
Escobar	Escobar	k1gMnSc1
•	•	k?
Antonio	Antonio	k1gMnSc1
Fernandes	Fernandes	k1gMnSc1
•	•	k?
Dinis	Dinis	k1gInSc1
Fernandes	Fernandes	k1gMnSc1
•	•	k?
Vasco	Vasco	k1gMnSc1
da	da	k?
Gama	gama	k1gNnSc2
•	•	k?
Diogo	Diogo	k1gMnSc1
Gomes	Gomes	k1gMnSc1
•	•	k?
Estê	Estê	k1gMnSc1
Gomes	Gomes	k1gMnSc1
•	•	k?
Antã	Antã	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Gonçalves	Gonçalves	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Mořeplavec	mořeplavec	k1gMnSc1
•	•	k?
Lançarote	Lançarot	k1gInSc5
de	de	k?
Freitas	Freitas	k1gMnSc1
•	•	k?
Manuel	Manuel	k1gMnSc1
Felix	Felix	k1gMnSc1
Lima	limo	k1gNnSc2
•	•	k?
Fernã	Fernã	k1gMnSc1
de	de	k?
Magalhã	Magalhã	k1gInSc1
•	•	k?
Pedro	Pedro	k1gNnSc1
Mascarenhas	Mascarenhas	k1gMnSc1
•	•	k?
Jorge	Jorg	k1gFnSc2
de	de	k?
Meneses	Meneses	k1gMnSc1
•	•	k?
Antã	Antã	k1gMnSc1
de	de	k?
Miranda	Miranda	k1gFnSc1
•	•	k?
Joã	Joã	k6eAd1
da	da	k?
Nova	nova	k1gFnSc1
•	•	k?
Bartolomeu	Bartolomeus	k1gInSc2
Perestrelo	Perestrela	k1gFnSc5
•	•	k?
Pedro	Pedro	k1gNnSc1
Fernandes	Fernandes	k1gMnSc1
de	de	k?
Queirós	Queirós	k1gInSc1
•	•	k?
Diogo	Diogo	k1gMnSc1
Rodrigues	Rodrigues	k1gMnSc1
•	•	k?
Joã	Joã	k1gMnSc1
de	de	k?
Santarém	Santarý	k2eAgInSc6d1
•	•	k?
Ruy	Ruy	k1gFnSc1
de	de	k?
Sequeira	Sequeira	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Alexandre	Alexandr	k1gInSc5
de	de	k?
Serpa	Serp	k1gMnSc2
Pinto	pinta	k1gFnSc5
•	•	k?
Fernã	Fernã	k1gMnSc1
Mendes	Mendes	k1gMnSc1
Pinto	pinta	k1gFnSc5
•	•	k?
Francisco	Francisco	k1gMnSc1
Serrã	Serrã	k1gMnSc1
•	•	k?
Antã	Antã	k1gMnSc1
Francisco	Francisco	k1gMnSc1
Silva	Silva	k1gFnSc1
Porto	porto	k1gNnSc1
•	•	k?
Pedro	Pedro	k1gNnSc1
de	de	k?
Sintra	Sintr	k1gMnSc2
•	•	k?
Juan	Juan	k1gMnSc1
Diaz	Diaz	k1gMnSc1
de	de	k?
Solís	Solís	k1gInSc1
•	•	k?
Gabriel	Gabriel	k1gMnSc1
Soares	Soares	k1gMnSc1
de	de	k?
Souza	Souza	k1gFnSc1
•	•	k?
Fernã	Fernã	k1gMnSc1
Suares	Suares	k1gMnSc1
•	•	k?
Pedro	Pedro	k1gNnSc4
Teixeira	Teixeir	k1gInSc2
•	•	k?
Tristã	Tristã	k6eAd1
Vaz	vaz	k1gInSc1
Teixeira	Teixeir	k1gInSc2
•	•	k?
Luis	Luisa	k1gFnPc2
Váez	Váez	k1gMnSc1
de	de	k?
Torres	Torres	k1gMnSc1
•	•	k?
Nuňo	Nuňo	k1gMnSc1
Tristã	Tristã	k1gMnSc1
•	•	k?
Joã	Joã	k1gMnSc1
Gonçalves	Gonçalves	k1gMnSc1
Zarco	Zarco	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700584	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118537431	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8346	#num#	k4
2817	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80019653	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
106966471	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80019653	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Portugalsko	Portugalsko	k1gNnSc1
|	|	kIx~
Kolonialismus	kolonialismus	k1gInSc1
</s>
