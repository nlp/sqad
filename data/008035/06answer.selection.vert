<s>
Hippokrates	Hippokrates	k1gMnSc1	Hippokrates
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
představy	představa	k1gFnSc2	představa
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
tělesných	tělesný	k2eAgFnPc6d1	tělesná
šťávách	šťáva	k1gFnPc6	šťáva
(	(	kIx(	(
<g/>
humorech	humor	k1gInPc6	humor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
poměr	poměr	k1gInSc1	poměr
určuje	určovat	k5eAaImIp3nS	určovat
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
