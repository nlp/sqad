<s>
Temperament	temperament	k1gInSc1	temperament
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
temperare	temperar	k1gMnSc5	temperar
=	=	kIx~	=
mísit	mísit	k5eAaImF	mísit
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
souhrn	souhrn	k1gInSc4	souhrn
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
nebo	nebo	k8xC	nebo
vrozených	vrozený	k2eAgInPc2d1	vrozený
rysů	rys	k1gInPc2	rys
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
trvale	trvale	k6eAd1	trvale
projevují	projevovat	k5eAaImIp3nP	projevovat
způsobem	způsob	k1gInSc7	způsob
reagování	reagování	k1gNnSc2	reagování
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
prožívání	prožívání	k1gNnSc2	prožívání
<g/>
.	.	kIx.	.
</s>
<s>
Temperament	temperament	k1gInSc1	temperament
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgMnSc1d1	spjat
se	s	k7c7	s
vzrušivostí	vzrušivost	k1gFnSc7	vzrušivost
–	–	k?	–
tj.	tj.	kA	tj.
mírou	míra	k1gFnSc7	míra
odpovědi	odpověď	k1gFnSc2	odpověď
určitého	určitý	k2eAgMnSc2d1	určitý
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
podněty	podnět	k1gInPc4	podnět
-	-	kIx~	-
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
tendenci	tendence	k1gFnSc4	tendence
měnit	měnit	k5eAaImF	měnit
nálady	nálada	k1gFnPc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
starou	starý	k2eAgFnSc4d1	stará
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
každou	každý	k3xTgFnSc4	každý
osobu	osoba	k1gFnSc4	osoba
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
určitou	určitý	k2eAgFnSc4d1	určitá
směs	směs	k1gFnSc4	směs
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
tělesných	tělesný	k2eAgFnPc2d1	tělesná
šťáv	šťáva	k1gFnPc2	šťáva
čili	čili	k8xC	čili
humorů	humor	k1gInPc2	humor
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nS	lišit
nejen	nejen	k6eAd1	nejen
obsahem	obsah	k1gInSc7	obsah
svého	svůj	k3xOyFgInSc2	svůj
duševního	duševní	k2eAgInSc2d1	duševní
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vnímáním	vnímání	k1gNnSc7	vnímání
<g/>
,	,	kIx,	,
pamatováním	pamatování	k1gNnSc7	pamatování
<g/>
,	,	kIx,	,
myšlením	myšlení	k1gNnSc7	myšlení
<g/>
,	,	kIx,	,
zájmy	zájem	k1gInPc4	zájem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
formou	forma	k1gFnSc7	forma
-	-	kIx~	-
reakcemi	reakce	k1gFnPc7	reakce
na	na	k7c4	na
podněty	podnět	k1gInPc4	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Temperament	temperament	k1gInSc1	temperament
tedy	tedy	k9	tedy
určuje	určovat	k5eAaImIp3nS	určovat
dynamiku	dynamika	k1gFnSc4	dynamika
celého	celý	k2eAgNnSc2d1	celé
prožívání	prožívání	k1gNnSc2	prožívání
a	a	k8xC	a
chování	chování	k1gNnSc2	chování
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Dynamika	dynamika	k1gFnSc1	dynamika
prožívání	prožívání	k1gNnSc2	prožívání
je	být	k5eAaImIp3nS	být
tempo	tempo	k1gNnSc4	tempo
průběhu	průběh	k1gInSc2	průběh
a	a	k8xC	a
střídání	střídání	k1gNnSc2	střídání
psychických	psychický	k2eAgInPc2d1	psychický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
činností	činnost	k1gFnPc2	činnost
a	a	k8xC	a
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Prožívání	prožívání	k1gNnPc1	prožívání
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
<g/>
:	:	kIx,	:
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
intenzitou	intenzita	k1gFnSc7	intenzita
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
reakcí	reakce	k1gFnPc2	reakce
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
podněty	podnět	k1gInPc4	podnět
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
prožíváním	prožívání	k1gNnSc7	prožívání
<g/>
)	)	kIx)	)
projevem	projev	k1gInSc7	projev
prožívaného	prožívaný	k2eAgMnSc2d1	prožívaný
navenek	navenek	k6eAd1	navenek
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
chováním	chování	k1gNnSc7	chování
<g/>
)	)	kIx)	)
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
odedávna	odedávna	k6eAd1	odedávna
všímali	všímat	k5eAaImAgMnP	všímat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
podněty	podnět	k1gInPc4	podnět
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
charakteristicky	charakteristicky	k6eAd1	charakteristicky
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikaly	vznikat	k5eAaImAgFnP	vznikat
snahy	snaha	k1gFnPc1	snaha
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
čtyři	čtyři	k4xCgFnPc4	čtyři
slavné	slavný	k2eAgFnPc4d1	slavná
typologie	typologie	k1gFnPc4	typologie
<g/>
.	.	kIx.	.
</s>
<s>
Hippokrates	Hippokrates	k1gMnSc1	Hippokrates
(	(	kIx(	(
<g/>
asi	asi	k9	asi
460	[number]	k4	460
-	-	kIx~	-
370	[number]	k4	370
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
lékař	lékař	k1gMnSc1	lékař
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
pokládaný	pokládaný	k2eAgInSc1d1	pokládaný
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
spisy	spis	k1gInPc1	spis
Corpus	corpus	k1gNnSc2	corpus
Hippocraticum	Hippocraticum	k1gInSc1	Hippocraticum
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
středověkého	středověký	k2eAgNnSc2d1	středověké
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
autorství	autorství	k1gNnSc2	autorství
lékařské	lékařský	k2eAgFnSc2d1	lékařská
přísahy	přísaha	k1gFnSc2	přísaha
–	–	k?	–
tzv.	tzv.	kA	tzv.
Hippokratova	Hippokratův	k2eAgFnSc1d1	Hippokratova
přísaha	přísaha	k1gFnSc1	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Hippokrates	Hippokrates	k1gMnSc1	Hippokrates
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
představy	představa	k1gFnSc2	představa
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
tělesných	tělesný	k2eAgFnPc6d1	tělesná
šťávách	šťáva	k1gFnPc6	šťáva
(	(	kIx(	(
<g/>
humorech	humor	k1gInPc6	humor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
poměr	poměr	k1gInSc1	poměr
určuje	určovat	k5eAaImIp3nS	určovat
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
temperament	temperament	k1gInSc4	temperament
na	na	k7c4	na
4	[number]	k4	4
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
rozvíjeny	rozvíjet	k5eAaImNgFnP	rozvíjet
pozdějšími	pozdní	k2eAgMnPc7d2	pozdější
badateli	badatel	k1gMnPc7	badatel
(	(	kIx(	(
<g/>
Galénos	Galénos	k1gMnSc1	Galénos
<g/>
,	,	kIx,	,
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
Eysenck	Eysenck	k1gMnSc1	Eysenck
<g/>
,	,	kIx,	,
Pavlov	Pavlov	k1gInSc1	Pavlov
<g/>
,	,	kIx,	,
Kretschmer	Kretschmer	k1gInSc1	Kretschmer
<g/>
,	,	kIx,	,
Scheldon	Scheldon	k1gInSc1	Scheldon
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
sangvinik	sangvinik	k1gMnSc1	sangvinik
(	(	kIx(	(
<g/>
krev	krev	k1gFnSc1	krev
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
především	především	k9	především
přiměřenou	přiměřený	k2eAgFnSc7d1	přiměřená
reaktivitou	reaktivita	k1gFnSc7	reaktivita
<g/>
;	;	kIx,	;
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
slabé	slabý	k2eAgInPc4d1	slabý
podněty	podnět	k1gInPc4	podnět
reaguje	reagovat	k5eAaBmIp3nS	reagovat
slabě	slabě	k6eAd1	slabě
<g/>
,	,	kIx,	,
na	na	k7c4	na
silné	silný	k2eAgNnSc4d1	silné
silně	silně	k6eAd1	silně
<g/>
;	;	kIx,	;
dominuje	dominovat	k5eAaImIp3nS	dominovat
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
reakce	reakce	k1gFnSc1	reakce
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
slaměný	slaměný	k2eAgInSc1d1	slaměný
oheň	oheň	k1gInSc1	oheň
<g/>
"	"	kIx"	"
tj.	tj.	kA	tj.
rychlé	rychlý	k2eAgNnSc1d1	rychlé
doznívání	doznívání	k1gNnSc1	doznívání
zážitku	zážitek	k1gInSc2	zážitek
a	a	k8xC	a
rychlé	rychlý	k2eAgFnSc2d1	rychlá
změny	změna	k1gFnSc2	změna
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
přizpůsobivý	přizpůsobivý	k2eAgMnSc1d1	přizpůsobivý
<g/>
,	,	kIx,	,
emočně	emočně	k6eAd1	emočně
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poněkud	poněkud	k6eAd1	poněkud
nestálý	stálý	k2eNgInSc4d1	nestálý
a	a	k8xC	a
lehkovážný	lehkovážný	k2eAgInSc4d1	lehkovážný
<g/>
,	,	kIx,	,
vesele	vesele	k6eAd1	vesele
laděný	laděný	k2eAgInSc1d1	laděný
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
optimistický	optimistický	k2eAgInSc4d1	optimistický
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
prožitky	prožitek	k1gInPc1	prožitek
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
mělké	mělký	k2eAgFnPc1d1	mělká
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k9	i
jeho	jeho	k3xOp3gInPc1	jeho
city	cit	k1gInPc1	cit
-	-	kIx~	-
emočně	emočně	k6eAd1	emočně
stabilní	stabilní	k2eAgMnSc1d1	stabilní
extrovert	extrovert	k1gMnSc1	extrovert
flegmatik	flegmatik	k1gMnSc1	flegmatik
(	(	kIx(	(
<g/>
sliz	sliz	k1gInSc1	sliz
<g/>
;	;	kIx,	;
hlen	hlen	k1gInSc1	hlen
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
emočně	emočně	k6eAd1	emočně
celkem	celkem	k6eAd1	celkem
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
,	,	kIx,	,
navenek	navenek	k6eAd1	navenek
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
lhostejný	lhostejný	k2eAgInSc1d1	lhostejný
<g/>
,	,	kIx,	,
vzrušují	vzrušovat	k5eAaImIp3nP	vzrušovat
ho	on	k3xPp3gMnSc4	on
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgInPc1d1	silný
podněty	podnět	k1gInPc1	podnět
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
stálost	stálost	k1gFnSc4	stálost
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
spokojenost	spokojenost	k1gFnSc1	spokojenost
<g/>
,	,	kIx,	,
klid	klid	k1gInSc1	klid
až	až	k6eAd1	až
a	a	k8xC	a
chladnokrevnost	chladnokrevnost	k1gFnSc4	chladnokrevnost
či	či	k8xC	či
apatii	apatie	k1gFnSc4	apatie
<g/>
;	;	kIx,	;
hlubší	hluboký	k2eAgInPc4d2	hlubší
vztahy	vztah	k1gInPc4	vztah
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
k	k	k7c3	k
vybraným	vybraný	k2eAgFnPc3d1	vybraná
osobám	osoba	k1gFnPc3	osoba
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
pasivní	pasivní	k2eAgInSc1d1	pasivní
a	a	k8xC	a
bez	bez	k7c2	bez
velkých	velký	k2eAgFnPc2d1	velká
životních	životní	k2eAgFnPc2d1	životní
ambicí	ambice	k1gFnPc2	ambice
a	a	k8xC	a
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k6eAd1	rád
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
pohybově	pohybově	k6eAd1	pohybově
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
úsporný	úsporný	k2eAgInSc1d1	úsporný
-	-	kIx~	-
emočně	emočně	k6eAd1	emočně
stabilní	stabilní	k2eAgMnSc1d1	stabilní
introvert	introvert	k1gMnSc1	introvert
melancholik	melancholik	k1gMnSc1	melancholik
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
černá	černý	k2eAgFnSc1d1	černá
žluč	žluč	k1gFnSc1	žluč
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	s	k7c7	s
hlubokými	hluboký	k2eAgInPc7d1	hluboký
prožitky	prožitek	k1gInPc7	prožitek
a	a	k8xC	a
spíše	spíše	k9	spíše
smutným	smutný	k2eAgNnSc7d1	smutné
laděním	ladění	k1gNnSc7	ladění
<g/>
,	,	kIx,	,
pesimismem	pesimismus	k1gInSc7	pesimismus
a	a	k8xC	a
strachem	strach	k1gInSc7	strach
z	z	k7c2	z
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
;	;	kIx,	;
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
klid	klid	k1gInSc4	klid
a	a	k8xC	a
nesnáší	snášet	k5eNaImIp3nP	snášet
vypjaté	vypjatý	k2eAgFnPc4d1	vypjatá
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
vzruchy	vzruch	k1gInPc4	vzruch
<g/>
,	,	kIx,	,
hlučnost	hlučnost	k1gFnSc4	hlučnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
city	cit	k1gInPc1	cit
jsou	být	k5eAaImIp3nP	být
trvalé	trvalý	k2eAgInPc1d1	trvalý
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
intenzita	intenzita	k1gFnSc1	intenzita
se	se	k3xPyFc4	se
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
navenek	navenek	k6eAd1	navenek
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
spíše	spíše	k9	spíše
vnitřně	vnitřně	k6eAd1	vnitřně
<g/>
;	;	kIx,	;
obtížně	obtížně	k6eAd1	obtížně
navazuje	navazovat	k5eAaImIp3nS	navazovat
kontakty	kontakt	k1gInPc4	kontakt
<g/>
,	,	kIx,	,
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
naváže	navázat	k5eAaPmIp3nS	navázat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
trvalé	trvalý	k2eAgFnPc1d1	trvalá
a	a	k8xC	a
hluboké	hluboký	k2eAgFnPc1d1	hluboká
-	-	kIx~	-
emočně	emočně	k6eAd1	emočně
labilní	labilní	k2eAgMnSc1d1	labilní
introvert	introvert	k1gMnSc1	introvert
<g/>
.	.	kIx.	.
cholerik	cholerik	k1gMnSc1	cholerik
(	(	kIx(	(
<g/>
žluč	žluč	k1gFnSc1	žluč
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
vzrušivý	vzrušivý	k2eAgInSc1d1	vzrušivý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g />
.	.	kIx.	.
</s>
<s>
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
výbuchům	výbuch	k1gInPc3	výbuch
hněvu	hněv	k1gInSc2	hněv
a	a	k8xC	a
k	k	k7c3	k
agresi	agrese	k1gFnSc3	agrese
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
se	se	k3xPyFc4	se
ovládá	ovládat	k5eAaImIp3nS	ovládat
a	a	k8xC	a
reaguje	reagovat	k5eAaBmIp3nS	reagovat
často	často	k6eAd1	často
impulzivně	impulzivně	k6eAd1	impulzivně
<g/>
,	,	kIx,	,
nerozvážně	rozvážně	k6eNd1	rozvážně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
sklon	sklon	k1gInSc1	sklon
"	"	kIx"	"
<g/>
prorážet	prorážet	k5eAaImF	prorážet
hlavou	hlavý	k2eAgFnSc4d1	hlavá
zeď	zeď	k1gFnSc4	zeď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
netrpělivý	trpělivý	k2eNgInSc1d1	netrpělivý
<g/>
,	,	kIx,	,
panovačný	panovačný	k2eAgInSc1d1	panovačný
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
často	často	k6eAd1	často
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
ústupky	ústupek	k1gInPc7	ústupek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
egocentrický	egocentrický	k2eAgMnSc1d1	egocentrický
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
soužití	soužití	k1gNnSc1	soužití
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
;	;	kIx,	;
emočně	emočně	k6eAd1	emočně
je	být	k5eAaImIp3nS	být
labilní	labilní	k2eAgNnSc1d1	labilní
<g/>
,	,	kIx,	,
city	cit	k1gInPc1	cit
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
vyvolány	vyvolat	k5eAaPmNgFnP	vyvolat
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
navenek	navenek	k6eAd1	navenek
reaguje	reagovat	k5eAaBmIp3nS	reagovat
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bez	bez	k7c2	bez
zábran	zábrana	k1gFnPc2	zábrana
-	-	kIx~	-
emočně	emočně	k6eAd1	emočně
labilní	labilní	k2eAgMnSc1d1	labilní
extrovert	extrovert	k1gMnSc1	extrovert
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
"	"	kIx"	"
<g/>
směsice	směsice	k1gFnSc1	směsice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
<g />
.	.	kIx.	.
</s>
<s>
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
například	například	k6eAd1	například
flegmatik	flegmatik	k1gMnSc1	flegmatik
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
situace	situace	k1gFnPc1	situace
z	z	k7c2	z
vás	vy	k3xPp2nPc2	vy
třeba	třeba	k6eAd1	třeba
dočasně	dočasně	k6eAd1	dočasně
udělají	udělat	k5eAaPmIp3nP	udělat
cholerika	cholerik	k1gMnSc4	cholerik
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
C.	C.	kA	C.
G.	G.	kA	G.
Jung	Jung	k1gMnSc1	Jung
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
dělení	dělení	k1gNnSc2	dělení
temperamentu	temperament	k1gInSc2	temperament
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
okolnímu	okolní	k2eAgInSc3d1	okolní
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
introvert	introvert	k1gMnSc1	introvert
člověk	člověk	k1gMnSc1	člověk
žijící	žijící	k2eAgMnSc1d1	žijící
především	především	k9	především
svým	svůj	k3xOyFgInSc7	svůj
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
velmi	velmi	k6eAd1	velmi
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
přemýšlivý	přemýšlivý	k2eAgMnSc1d1	přemýšlivý
<g/>
,	,	kIx,	,
empatický	empatický	k2eAgMnSc1d1	empatický
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
,	,	kIx,	,
rozvážný	rozvážný	k2eAgMnSc1d1	rozvážný
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jednání	jednání	k1gNnSc6	jednání
a	a	k8xC	a
opatrný	opatrný	k2eAgInSc1d1	opatrný
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
citech	cit	k1gInPc6	cit
<g/>
,	,	kIx,	,
sklony	sklon	k1gInPc7	sklon
k	k	k7c3	k
mlčenlivosti	mlčenlivost	k1gFnSc3	mlčenlivost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neexistence	neexistence	k1gFnSc2	neexistence
relevantního	relevantní	k2eAgNnSc2d1	relevantní
tématu	téma	k1gNnSc2	téma
<g/>
,	,	kIx,	,
preferuje	preferovat	k5eAaImIp3nS	preferovat
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
komunikaci	komunikace	k1gFnSc4	komunikace
(	(	kIx(	(
<g/>
e-maily	eail	k1gInPc1	e-mail
<g/>
,	,	kIx,	,
dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
SMS	SMS	kA	SMS
<g/>
)	)	kIx)	)
před	před	k7c7	před
hovorem	hovor	k1gInSc7	hovor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
preferuje	preferovat	k5eAaImIp3nS	preferovat
menší	malý	k2eAgFnPc4d2	menší
skupinky	skupinka	k1gFnPc4	skupinka
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
5-10	[number]	k4	5-10
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
introvertních	introvertní	k2eAgMnPc2d1	introvertní
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
extrovertně	extrovertně	k6eAd1	extrovertně
<g/>
.	.	kIx.	.
extrovert	extrovert	k1gMnSc1	extrovert
člověk	člověk	k1gMnSc1	člověk
žijící	žijící	k2eAgMnSc1d1	žijící
navenek	navenek	k6eAd1	navenek
–	–	k?	–
povrchní	povrchní	k2eAgNnSc4d1	povrchní
a	a	k8xC	a
spontánní	spontánní	k2eAgNnSc4d1	spontánní
⇒	⇒	k?	⇒
snadno	snadno	k6eAd1	snadno
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zahajovat	zahajovat	k5eAaImF	zahajovat
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
i	i	k9	i
hovor	hovor	k1gInSc4	hovor
"	"	kIx"	"
<g/>
o	o	k7c6	o
ničem	nic	k3yNnSc6	nic
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
rozhodně	rozhodně	k6eAd1	rozhodně
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
až	až	k9	až
zbrkle	zbrkle	k6eAd1	zbrkle
<g/>
,	,	kIx,	,
po	po	k7c6	po
citové	citový	k2eAgFnSc6d1	citová
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
otevřený	otevřený	k2eAgInSc1d1	otevřený
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc7	jeho
city	cit	k1gInPc7	cit
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
povrchní	povrchní	k2eAgInSc1d1	povrchní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
spokojenosti	spokojenost	k1gFnSc3	spokojenost
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
být	být	k5eAaImF	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
dění	dění	k1gNnSc2	dění
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
i	i	k9	i
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
akcích	akce	k1gFnPc6	akce
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
mýtem	mýtus	k1gInSc7	mýtus
je	být	k5eAaImIp3nS	být
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
introvertní	introvertní	k2eAgFnSc1d1	introvertní
osobnost	osobnost	k1gFnSc1	osobnost
je	být	k5eAaImIp3nS	být
nevšímavá	všímavý	k2eNgFnSc1d1	nevšímavá
vůči	vůči	k7c3	vůči
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
extravertní	extravertní	k2eAgFnSc1d1	extravertní
velmi	velmi	k6eAd1	velmi
všímavá	všímavý	k2eAgFnSc1d1	všímavá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
obráceně	obráceně	k6eAd1	obráceně
–	–	k?	–
extrovertní	extrovertní	k2eAgFnSc1d1	extrovertní
osobnost	osobnost	k1gFnSc1	osobnost
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
schopna	schopen	k2eAgFnSc1d1	schopna
vjemy	vjem	k1gInPc4	vjem
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
vnímat	vnímat	k5eAaImF	vnímat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
intenzivně	intenzivně	k6eAd1	intenzivně
<g/>
...	...	k?	...
Důvodem	důvod	k1gInSc7	důvod
vytrvalého	vytrvalý	k2eAgNnSc2d1	vytrvalé
přežívání	přežívání	k1gNnSc2	přežívání
tohoto	tento	k3xDgInSc2	tento
mýtu	mýtus	k1gInSc2	mýtus
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
introvertní	introvertní	k2eAgFnSc4d1	introvertní
osobnost	osobnost	k1gFnSc4	osobnost
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
všímá	všímat	k5eAaImIp3nS	všímat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechává	nechávat	k5eAaImIp3nS	nechávat
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
vysoká	vysoký	k2eAgFnSc1d1	vysoká
kvalita	kvalita	k1gFnSc1	kvalita
vjemů	vjem	k1gInPc2	vjem
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
přetížením	přetížení	k1gNnSc7	přetížení
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
myšlenkového	myšlenkový	k2eAgInSc2d1	myšlenkový
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
zmatenosti	zmatenost	k1gFnSc2	zmatenost
<g/>
,	,	kIx,	,
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
introverze	introverze	k1gFnSc2	introverze
též	též	k9	též
často	často	k6eAd1	často
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
narůstajícím	narůstající	k2eAgInSc7d1	narůstající
intelektem	intelekt	k1gInSc7	intelekt
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
introvertů	introvert	k1gMnPc2	introvert
a	a	k8xC	a
extrovertů	extrovert	k1gMnPc2	extrovert
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
Jungovu	Jungův	k2eAgFnSc4d1	Jungova
teorii	teorie	k1gFnSc4	teorie
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
osobnostní	osobnostní	k2eAgInSc1d1	osobnostní
dotazník	dotazník	k1gInSc1	dotazník
MBTI	MBTI	kA	MBTI
(	(	kIx(	(
<g/>
Meyrs-Briggs	Meyrs-Briggs	k1gInSc1	Meyrs-Briggs
Type	typ	k1gInSc5	typ
Indicator	Indicator	k1gInSc1	Indicator
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
autorkami	autorka	k1gFnPc7	autorka
jsou	být	k5eAaImIp3nP	být
Američanky	Američanka	k1gFnPc1	Američanka
Katharine	Katharin	k1gInSc5	Katharin
Cook	Cook	k1gMnSc1	Cook
Briggs	Briggs	k1gInSc1	Briggs
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
Isabel	Isabela	k1gFnPc2	Isabela
Briggs	Briggsa	k1gFnPc2	Briggsa
Meyers	Meyers	k1gInSc1	Meyers
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
MBTI	MBTI	kA	MBTI
David	David	k1gMnSc1	David
Kiersey	Kiersea	k1gFnSc2	Kiersea
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
typologii	typologie	k1gFnSc4	typologie
osobností	osobnost	k1gFnPc2	osobnost
dle	dle	k7c2	dle
dominantních	dominantní	k2eAgFnPc2d1	dominantní
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
temperamentů	temperament	k1gInPc2	temperament
<g/>
.	.	kIx.	.
</s>
<s>
Temperament	temperament	k1gInSc1	temperament
zde	zde	k6eAd1	zde
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
ústřední	ústřední	k2eAgFnSc7d1	ústřední
<g/>
,	,	kIx,	,
dominantní	dominantní	k2eAgFnSc7d1	dominantní
potřebou	potřeba	k1gFnSc7	potřeba
člověka	člověk	k1gMnSc2	člověk
<g/>
:	:	kIx,	:
Hráči	hráč	k1gMnPc1	hráč
hledající	hledající	k2eAgMnPc1d1	hledající
rozruch	rozrucha	k1gFnPc2	rozrucha
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
40	[number]	k4	40
%	%	kIx~	%
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
)	)	kIx)	)
Strážci	strážce	k1gMnPc1	strážce
hledající	hledající	k2eAgNnPc4d1	hledající
bezpečí	bezpečí	k1gNnPc4	bezpečí
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
40	[number]	k4	40
%	%	kIx~	%
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
)	)	kIx)	)
Racionálové	Racionálový	k2eAgFnSc2d1	Racionálový
hledající	hledající	k2eAgFnSc2d1	hledající
znalosti	znalost	k1gFnSc2	znalost
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
)	)	kIx)	)
Idealisté	idealista	k1gMnPc1	idealista
hledající	hledající	k2eAgFnSc4d1	hledající
identitu	identita	k1gFnSc4	identita
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
)	)	kIx)	)
Vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
konstitucí	konstituce	k1gFnSc7	konstituce
a	a	k8xC	a
temperamentem	temperament	k1gInSc7	temperament
<g/>
,	,	kIx,	,
navazujíce	navazovat	k5eAaImSgFnP	navazovat
na	na	k7c4	na
Hippokratovu	Hippokratův	k2eAgFnSc4d1	Hippokratova
konstituční	konstituční	k2eAgFnSc4d1	konstituční
typologii	typologie	k1gFnSc4	typologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
určitých	určitý	k2eAgInPc2d1	určitý
rysů	rys	k1gInPc2	rys
těla	tělo	k1gNnSc2	tělo
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
typologii	typologie	k1gFnSc4	typologie
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
osobnosti	osobnost	k1gFnPc1	osobnost
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
<g/>
:	:	kIx,	:
pyknik	pyknik	k1gMnSc1	pyknik
–	–	k?	–
cyklotymní	cyklotymnět	k5eAaPmIp3nS	cyklotymnět
typ	typ	k1gInSc1	typ
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
menší	malý	k2eAgFnSc1d2	menší
zakulacená	zakulacený	k2eAgFnSc1d1	zakulacená
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
slabé	slabý	k2eAgNnSc1d1	slabé
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
,	,	kIx,	,
kulatá	kulatý	k2eAgFnSc1d1	kulatá
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
vyklenuté	vyklenutý	k2eAgNnSc1d1	vyklenuté
břicho	břicho	k1gNnSc1	břicho
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgFnPc1d1	psychická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
střídá	střídat	k5eAaImIp3nS	střídat
nálady	nálada	k1gFnPc4	nálada
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc4d1	otevřený
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgInSc4d1	společenský
<g/>
,	,	kIx,	,
realistický	realistický	k2eAgInSc4d1	realistický
<g/>
,	,	kIx,	,
ohrožen	ohrožen	k2eAgInSc4d1	ohrožen
maniodepresivními	maniodepresivní	k2eAgInPc7d1	maniodepresivní
duševními	duševní	k2eAgNnPc7d1	duševní
onemocněními	onemocnění	k1gNnPc7	onemocnění
(	(	kIx(	(
<g/>
střídání	střídání	k1gNnSc1	střídání
nálad	nálada	k1gFnPc2	nálada
bez	bez	k7c2	bez
vnější	vnější	k2eAgFnSc2d1	vnější
příčiny	příčina	k1gFnSc2	příčina
–	–	k?	–
nálady	nálada	k1gFnSc2	nálada
jsou	být	k5eAaImIp3nP	být
nepřiměřeně	přiměřeně	k6eNd1	přiměřeně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
nedají	dát	k5eNaPmIp3nP	dát
se	se	k3xPyFc4	se
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vůlí	vůle	k1gFnSc7	vůle
<g/>
)	)	kIx)	)
astenik	astenik	k1gMnSc1	astenik
(	(	kIx(	(
<g/>
leptosom	leptosom	k1gInSc1	leptosom
<g/>
)	)	kIx)	)
–	–	k?	–
schizotymní	schizotymnět	k5eAaPmIp3nS	schizotymnět
typ	typ	k1gInSc1	typ
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
<g/>
,	,	kIx,	,
úzká	úzký	k2eAgNnPc4d1	úzké
ramena	rameno	k1gNnPc4	rameno
<g/>
,	,	kIx,	,
slabé	slabý	k2eAgNnSc1d1	slabé
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
,	,	kIx,	,
ostrý	ostrý	k2eAgInSc4d1	ostrý
profil	profil	k1gInSc4	profil
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgFnPc4d1	psychická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
<g/>
,	,	kIx,	,
jednostranně	jednostranně	k6eAd1	jednostranně
zaměřený	zaměřený	k2eAgMnSc1d1	zaměřený
<g/>
,	,	kIx,	,
idealista	idealista	k1gMnSc1	idealista
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
přizpůsobivý	přizpůsobivý	k2eAgInSc1d1	přizpůsobivý
<g/>
,	,	kIx,	,
ohrožen	ohrožen	k2eAgInSc1d1	ohrožen
schizofrenií	schizofrenie	k1gFnSc7	schizofrenie
(	(	kIx(	(
<g/>
bludné	bludný	k2eAgFnSc2d1	bludná
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
,	,	kIx,	,
rozpad	rozpad	k1gInSc1	rozpad
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
)	)	kIx)	)
atletik	atletika	k1gFnPc2	atletika
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ixotym	ixotym	k1gInSc1	ixotym
<g/>
)	)	kIx)	)
–	–	k?	–
viskózní	viskózní	k2eAgInSc1d1	viskózní
typ	typ	k1gInSc1	typ
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
silně	silně	k6eAd1	silně
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
kostra	kostra	k1gFnSc1	kostra
<g/>
,	,	kIx,	,
výrazné	výrazný	k2eAgNnSc1d1	výrazné
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc4d1	široký
hrudník	hrudník	k1gInSc4	hrudník
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnPc4d2	menší
lebky	lebka	k1gFnPc4	lebka
s	s	k7c7	s
protáhlým	protáhlý	k2eAgInSc7d1	protáhlý
obličejem	obličej	k1gInSc7	obličej
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgFnPc4d1	psychická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
klidný	klidný	k2eAgInSc4d1	klidný
<g/>
,	,	kIx,	,
přizpůsobivý	přizpůsobivý	k2eAgInSc4d1	přizpůsobivý
<g/>
,	,	kIx,	,
těžkopádný	těžkopádný	k2eAgInSc4d1	těžkopádný
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Kretschmera	Kretschmero	k1gNnSc2	Kretschmero
má	mít	k5eAaImIp3nS	mít
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
epilepsii	epilepsie	k1gFnSc3	epilepsie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
typologie	typologie	k1gFnSc1	typologie
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
zpochybněna	zpochybnit	k5eAaPmNgFnS	zpochybnit
<g/>
.	.	kIx.	.
</s>
