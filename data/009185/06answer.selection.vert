<s>
Někdy	někdy	k6eAd1	někdy
modlitební	modlitební	k2eAgFnPc1d1	modlitební
místnosti	místnost	k1gFnPc1	místnost
plní	plnit	k5eAaImIp3nP	plnit
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
studovny	studovna	k1gFnSc2	studovna
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zimní	zimní	k2eAgFnSc2d1	zimní
modlitebny	modlitebna	k1gFnSc2	modlitebna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
stavěny	stavit	k5eAaImNgInP	stavit
uvnitř	uvnitř	k6eAd1	uvnitř
domů	domů	k6eAd1	domů
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
menším	malý	k2eAgInSc6d2	menší
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
vytápění	vytápění	k1gNnSc1	vytápění
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
sále	sál	k1gInSc6	sál
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
<g/>
.	.	kIx.	.
</s>
