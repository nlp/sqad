<s>
Asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
vykonaná	vykonaný	k2eAgFnSc1d1	vykonaná
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
jiné	jiný	k2eAgFnSc2d1	jiná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Asistence	asistence	k1gFnSc1	asistence
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgFnPc4d1	různá
podoby	podoba	k1gFnPc4	podoba
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
míní	mínit	k5eAaImIp3nS	mínit
obstarání	obstarání	k1gNnSc1	obstarání
a	a	k8xC	a
příprava	příprava	k1gFnSc1	příprava
jedu	jed	k1gInSc2	jed
či	či	k8xC	či
jiného	jiný	k2eAgNnSc2d1	jiné
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
zdravotnický	zdravotnický	k2eAgInSc4d1	zdravotnický
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
úkony	úkon	k1gInPc4	úkon
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
asistovanou	asistovaný	k2eAgFnSc7d1	asistovaná
sebevraždou	sebevražda	k1gFnSc7	sebevražda
a	a	k8xC	a
eutanázií	eutanázie	k1gFnSc7	eutanázie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
eutanázii	eutanázie	k1gFnSc6	eutanázie
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
akt	akt	k1gInSc4	akt
usmrcení	usmrcení	k1gNnSc2	usmrcení
napomáhající	napomáhající	k2eAgFnSc1d1	napomáhající
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
asistované	asistovaný	k2eAgFnSc3d1	asistovaná
sebevraždě	sebevražda	k1gFnSc3	sebevražda
sebevrah	sebevrah	k1gMnSc1	sebevrah
sám	sám	k3xTgMnSc1	sám
-	-	kIx~	-
asistující	asistující	k2eAgMnSc1d1	asistující
např.	např.	kA	např.
připraví	připravit	k5eAaPmIp3nS	připravit
sebevrahovi	sebevrah	k1gMnSc3	sebevrah
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
nápoj	nápoj	k1gInSc4	nápoj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
sebevraždě	sebevražda	k1gFnSc3	sebevražda
přítomen	přítomno	k1gNnPc2	přítomno
<g/>
,	,	kIx,	,
zajistí	zajistit	k5eAaPmIp3nS	zajistit
prostory	prostor	k1gInPc4	prostor
a	a	k8xC	a
postará	postarat	k5eAaPmIp3nS	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
nápoj	nápoj	k1gInSc4	nápoj
ale	ale	k8xC	ale
vypije	vypít	k5eAaPmIp3nS	vypít
sebevrah	sebevrah	k1gMnSc1	sebevrah
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
eutanázie	eutanázie	k1gFnSc1	eutanázie
ilegální	ilegální	k2eAgFnSc1d1	ilegální
<g/>
,	,	kIx,	,
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
ji	on	k3xPp3gFnSc4	on
například	například	k6eAd1	například
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
kliniky	klinika	k1gFnPc1	klinika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
asistovanou	asistovaný	k2eAgFnSc4d1	asistovaná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Dignitas	Dignitas	k1gInSc1	Dignitas
<g/>
,	,	kIx,	,
SterbeHilfeDeutschland	SterbeHilfeDeutschland	k1gInSc1	SterbeHilfeDeutschland
či	či	k8xC	či
Final	Final	k1gInSc1	Final
EXIT	exit	k1gInSc1	exit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
asistujícího	asistující	k2eAgMnSc4d1	asistující
trestná	trestný	k2eAgFnSc1d1	trestná
jako	jako	k8xC	jako
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
sebevraždě	sebevražda	k1gFnSc6	sebevražda
podle	podle	k7c2	podle
§	§	k?	§
144	[number]	k4	144
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
(	(	kIx(	(
<g/>
sazba	sazba	k1gFnSc1	sazba
podle	podle	k7c2	podle
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
pokusu	pokus	k1gInSc2	pokus
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
však	však	k9	však
trestná	trestný	k2eAgFnSc1d1	trestná
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Usmrcení	usmrcení	k1gNnSc1	usmrcení
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
(	(	kIx(	(
<g/>
eutanázie	eutanázie	k1gFnSc1	eutanázie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
trestá	trestat	k5eAaImIp3nS	trestat
jako	jako	k9	jako
vražda	vražda	k1gFnSc1	vražda
podle	podle	k7c2	podle
§	§	k?	§
140	[number]	k4	140
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
sazbou	sazba	k1gFnSc7	sazba
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obecné	obecný	k2eAgFnSc3d1	obecná
dekriminalizaci	dekriminalizace	k1gFnSc3	dekriminalizace
asistence	asistence	k1gFnSc2	asistence
při	při	k7c6	při
sebevraždě	sebevražda	k1gFnSc6	sebevražda
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
článku	článek	k1gInSc2	článek
115	[number]	k4	115
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
přijatého	přijatý	k2eAgInSc2d1	přijatý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabádání	nabádání	k1gNnSc1	nabádání
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
a	a	k8xC	a
asistence	asistence	k1gFnSc2	asistence
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
jsou	být	k5eAaImIp3nP	být
trestnými	trestný	k2eAgMnPc7d1	trestný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zištných	zištný	k2eAgInPc2d1	zištný
motivů	motiv	k1gInPc2	motiv
takto	takto	k6eAd1	takto
konající	konající	k2eAgFnSc2d1	konající
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
takové	takový	k3xDgNnSc1	takový
jednání	jednání	k1gNnSc1	jednání
je	být	k5eAaImIp3nS	být
penalizováno	penalizovat	k5eAaImNgNnS	penalizovat
odnětím	odnětí	k1gNnSc7	odnětí
svobody	svoboda	k1gFnSc2	svoboda
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
peněžitým	peněžitý	k2eAgInSc7d1	peněžitý
trestem	trest	k1gInSc7	trest
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
pomoc	pomoc	k1gFnSc1	pomoc
při	při	k7c6	při
sebevraždě	sebevražda	k1gFnSc6	sebevražda
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
legální	legální	k2eAgFnSc1d1	legální
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
motivována	motivovat	k5eAaBmNgFnS	motivovat
osobním	osobní	k2eAgInSc7d1	osobní
prospěchem	prospěch	k1gInSc7	prospěch
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
dotčeného	dotčený	k2eAgMnSc2d1	dotčený
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
osoby	osoba	k1gFnSc2	osoba
asistující	asistující	k2eAgFnSc2d1	asistující
při	při	k7c6	při
sebevraždě	sebevražda	k1gFnSc6	sebevražda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
některými	některý	k3yIgInPc7	některý
dalšími	další	k2eAgInPc7d1	další
předpisy	předpis	k1gInPc7	předpis
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
lékařů	lékař	k1gMnPc2	lékař
normami	norma	k1gFnPc7	norma
upravujícími	upravující	k2eAgFnPc7d1	upravující
profesní	profesní	k2eAgMnPc1d1	profesní
etiku	etika	k1gFnSc4	etika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
nebrání	bránit	k5eNaImIp3nS	bránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lékaři	lékař	k1gMnPc1	lékař
dodali	dodat	k5eAaPmAgMnP	dodat
látku	látka	k1gFnSc4	látka
způsobující	způsobující	k2eAgFnSc4d1	způsobující
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
metodou	metoda	k1gFnSc7	metoda
provedení	provedení	k1gNnSc2	provedení
asistované	asistovaný	k2eAgFnSc2d1	asistovaná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
je	být	k5eAaImIp3nS	být
požití	požití	k1gNnSc1	požití
rozpuštěného	rozpuštěný	k2eAgNnSc2d1	rozpuštěné
pentobarbitalu	pentobarbital	k1gMnSc6	pentobarbital
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Curych	Curych	k1gInSc1	Curych
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
plebiscit	plebiscit	k1gInSc4	plebiscit
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
voliči	volič	k1gMnPc1	volič
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
návrzích	návrh	k1gInPc6	návrh
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
asistované	asistovaný	k2eAgFnSc2d1	asistovaná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgMnSc6	první
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
úplný	úplný	k2eAgInSc4d1	úplný
zákaz	zákaz	k1gInSc4	zákaz
asistované	asistovaný	k2eAgFnSc2d1	asistovaná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Curych	Curych	k1gInSc1	Curych
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
o	o	k7c4	o
zákaz	zákaz	k1gInSc4	zákaz
poskytování	poskytování	k1gNnSc4	poskytování
pomoci	pomoc	k1gFnSc2	pomoc
se	s	k7c7	s
sebevraždou	sebevražda	k1gFnSc7	sebevražda
cizím	cizí	k2eAgMnPc3d1	cizí
státním	státní	k2eAgMnPc3d1	státní
příslušníkům	příslušník	k1gMnPc3	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
návrhy	návrh	k1gInPc1	návrh
voliči	volič	k1gMnPc7	volič
drtivě	drtivě	k6eAd1	drtivě
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
většinou	většinou	k6eAd1	většinou
85	[number]	k4	85
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
většinou	většina	k1gFnSc7	většina
78	[number]	k4	78
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
Německo	Německo	k1gNnSc1	Německo
Nizozemí	Nizozemí	k1gNnSc4	Nizozemí
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
4	[number]	k4	4
státy	stát	k1gInPc7	stát
USA	USA	kA	USA
-	-	kIx~	-
Oregon	Oregon	k1gMnSc1	Oregon
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
Vermont	Vermont	k1gInSc1	Vermont
a	a	k8xC	a
Montana	Montana	k1gFnSc1	Montana
</s>
