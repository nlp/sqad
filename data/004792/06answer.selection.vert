<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
SARS	SARS	kA	SARS
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
časový	časový	k2eAgInSc1d1	časový
interval	interval	k1gInSc1	interval
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uplyne	uplynout	k5eAaPmIp3nS	uplynout
od	od	k7c2	od
nákazy	nákaza	k1gFnSc2	nákaza
virem	vir	k1gInSc7	vir
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
projevům	projev	k1gInPc3	projev
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
až	až	k9	až
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
