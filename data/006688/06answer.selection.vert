<s>
Mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
na	na	k7c4	na
lehkou	lehký	k2eAgFnSc4d1	lehká
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těžkou	těžký	k2eAgFnSc7d1	těžká
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
(	(	kIx(	(
<g/>
IQ	iq	kA	iq
pod	pod	k7c7	pod
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
nejnovější	nový	k2eAgFnSc1d3	nejnovější
klasifikace	klasifikace	k1gFnSc1	klasifikace
Americké	americký	k2eAgFnSc2d1	americká
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
společnosti	společnost	k1gFnSc2	společnost
DSM-V	DSM-V	k1gFnSc2	DSM-V
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
intelektuální	intelektuální	k2eAgFnSc1d1	intelektuální
neschopnost	neschopnost	k1gFnSc1	neschopnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
intellectual	intellectual	k1gInSc1	intellectual
disability	disabilita	k1gFnSc2	disabilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
