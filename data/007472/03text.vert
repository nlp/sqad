<s>
Orel	Orel	k1gMnSc1	Orel
mořský	mořský	k2eAgMnSc1d1	mořský
(	(	kIx(	(
<g/>
Haliaeetus	Haliaeetus	k1gMnSc1	Haliaeetus
albicilla	albicilla	k1gMnSc1	albicilla
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
dravce	dravec	k1gMnSc2	dravec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jestřábovitých	jestřábovitý	k2eAgMnPc2d1	jestřábovitý
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
žijícím	žijící	k2eAgMnPc3d1	žijící
orlům	orel	k1gMnPc3	orel
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vod	voda	k1gFnPc2	voda
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
převážně	převážně	k6eAd1	převážně
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
Evropy	Evropa	k1gFnSc2	Evropa
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
lidského	lidský	k2eAgNnSc2d1	lidské
pronásledování	pronásledování	k1gNnSc2	pronásledování
zcela	zcela	k6eAd1	zcela
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
,	,	kIx,	,
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc4	jeho
početnost	početnost	k1gFnSc4	početnost
opět	opět	k6eAd1	opět
stoupat	stoupat	k5eAaImF	stoupat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
obsadil	obsadit	k5eAaPmAgMnS	obsadit
řadu	řada	k1gFnSc4	řada
původních	původní	k2eAgNnPc2d1	původní
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Monotypický	monotypický	k2eAgInSc1d1	monotypický
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
populace	populace	k1gFnSc1	populace
orlů	orel	k1gMnPc2	orel
mořských	mořský	k2eAgMnPc2d1	mořský
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
druhu	druh	k1gInSc2	druh
i	i	k8xC	i
největších	veliký	k2eAgInPc2d3	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
některými	některý	k3yIgFnPc7	některý
autory	autor	k1gMnPc7	autor
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
H.	H.	kA	H.
a.	a.	k?	a.
groencaldicus	groencaldicus	k1gInSc1	groencaldicus
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
mořský	mořský	k2eAgMnSc1d1	mořský
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
evropský	evropský	k2eAgMnSc1d1	evropský
orel	orel	k1gMnSc1	orel
a	a	k8xC	a
největší	veliký	k2eAgMnSc1d3	veliký
dravec	dravec	k1gMnSc1	dravec
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
76	[number]	k4	76
<g/>
–	–	k?	–
<g/>
92	[number]	k4	92
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
190-240	[number]	k4	190-240
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
u	u	k7c2	u
samce	samec	k1gInSc2	samec
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
4	[number]	k4	4
kg	kg	kA	kg
<g/>
,	,	kIx,	,
u	u	k7c2	u
samice	samice	k1gFnSc2	samice
7	[number]	k4	7
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
tmavohnědé	tmavohnědý	k2eAgNnSc1d1	tmavohnědé
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ocas	ocas	k1gInSc1	ocas
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
jiné	jiný	k2eAgFnSc6d1	jiná
světlejší	světlý	k2eAgFnSc6d2	světlejší
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
letu	let	k1gInSc2	let
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
pozná	poznat	k5eAaPmIp3nS	poznat
podle	podle	k7c2	podle
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
vzhledu	vzhled	k1gInSc2	vzhled
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
širokých	široký	k2eAgNnPc2d1	široké
křídel	křídlo	k1gNnPc2	křídlo
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
ručními	ruční	k2eAgFnPc7d1	ruční
letkami	letka	k1gFnPc7	letka
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
klínovitý	klínovitý	k2eAgInSc1d1	klínovitý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
zbarvení	zbarvení	k1gNnSc4	zbarvení
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Přechází	přecházet	k5eAaImIp3nS	přecházet
od	od	k7c2	od
hnědé	hnědý	k2eAgFnSc2d1	hnědá
mladých	mladý	k2eAgMnPc2d1	mladý
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
hnědobíle	hnědobíle	k6eAd1	hnědobíle
mramorovanou	mramorovaný	k2eAgFnSc4d1	mramorovaná
k	k	k7c3	k
bílé	bílý	k2eAgFnSc3d1	bílá
barvě	barva	k1gFnSc3	barva
s	s	k7c7	s
hnědými	hnědý	k2eAgInPc7d1	hnědý
okraji	okraj	k1gInPc7	okraj
rýdovacích	rýdovací	k2eAgNnPc2d1	rýdovací
per	pero	k1gNnPc2	pero
u	u	k7c2	u
pohlavně	pohlavně	k6eAd1	pohlavně
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
postupně	postupně	k6eAd1	postupně
světlá	světlat	k5eAaImIp3nS	světlat
starším	starý	k2eAgMnPc3d2	starší
ptákům	pták	k1gMnPc3	pták
nejen	nejen	k6eAd1	nejen
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
hruď	hruď	k1gFnSc1	hruď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
původní	původní	k2eAgFnSc1d1	původní
hnědá	hnědý	k2eAgFnSc1d1	hnědá
(	(	kIx(	(
<g/>
bíle	bíle	k6eAd1	bíle
kropenatá	kropenatý	k2eAgFnSc1d1	kropenatá
<g/>
)	)	kIx)	)
barva	barva	k1gFnSc1	barva
přejít	přejít	k5eAaPmF	přejít
až	až	k9	až
do	do	k7c2	do
hnědožluté	hnědožlutý	k2eAgFnSc2d1	hnědožlutá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
hnědé	hnědý	k2eAgFnSc2d1	hnědá
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
jasně	jasně	k6eAd1	jasně
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Neopeřené	opeřený	k2eNgFnPc1d1	neopeřená
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nS	lišit
pouze	pouze	k6eAd1	pouze
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Hlasem	hlasem	k6eAd1	hlasem
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
slyšitelné	slyšitelný	k2eAgFnPc1d1	slyšitelná
"	"	kIx"	"
<g/>
kjikjikjiklieklikklik	kjikjikjiklieklikklik	k1gMnSc1	kjikjikjiklieklikklik
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgInSc4d2	vyšší
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
mořský	mořský	k2eAgMnSc1d1	mořský
je	být	k5eAaImIp3nS	být
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
mimo	mimo	k7c4	mimo
tropické	tropický	k2eAgFnPc4d1	tropická
oblasti	oblast	k1gFnPc4	oblast
(	(	kIx(	(
<g/>
palearktický	palearktický	k2eAgInSc1d1	palearktický
typ	typ	k1gInSc1	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
výskytu	výskyt	k1gInSc2	výskyt
lesotundra	lesotundr	k1gMnSc2	lesotundr
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
hranice	hranice	k1gFnSc2	hranice
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
Balkánu	Balkán	k1gInSc2	Balkán
přes	přes	k7c4	přes
Turecko	Turecko	k1gNnSc4	Turecko
k	k	k7c3	k
jižnímu	jižní	k2eAgNnSc3d1	jižní
pobřeží	pobřeží	k1gNnSc3	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
okrajové	okrajový	k2eAgFnSc6d1	okrajová
oblasti	oblast	k1gFnSc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
i	i	k8xC	i
některé	některý	k3yIgInPc1	některý
středomořské	středomořský	k2eAgInPc1d1	středomořský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Kamčatka	Kamčatka	k1gFnSc1	Kamčatka
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
podél	podél	k7c2	podél
severovýchodního	severovýchodní	k2eAgNnSc2d1	severovýchodní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
stálí	stálý	k2eAgMnPc1d1	stálý
a	a	k8xC	a
zimu	zima	k1gFnSc4	zima
tráví	trávit	k5eAaImIp3nP	trávit
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
nejsevernějších	severní	k2eAgFnPc2d3	nejsevernější
populací	populace	k1gFnPc2	populace
však	však	k9	však
táhnou	táhnout	k5eAaImIp3nP	táhnout
do	do	k7c2	do
jižnějších	jižní	k2eAgFnPc2d2	jižnější
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
nezamrzající	zamrzající	k2eNgFnPc1d1	nezamrzající
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
zaletují	zaletovat	k5eAaImIp3nP	zaletovat
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
nebo	nebo	k8xC	nebo
severní	severní	k2eAgFnSc2d1	severní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
potulní	potulný	k2eAgMnPc1d1	potulný
až	až	k9	až
tažní	tažní	k2eAgMnPc1d1	tažní
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
podnikat	podnikat	k5eAaImF	podnikat
přesuny	přesun	k1gInPc4	přesun
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
sever	sever	k1gInSc4	sever
(	(	kIx(	(
<g/>
z	z	k7c2	z
Balkánu	Balkán	k1gInSc2	Balkán
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
mořský	mořský	k2eAgMnSc1d1	mořský
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
výrazně	výrazně	k6eAd1	výrazně
sníženy	snížit	k5eAaPmNgInP	snížit
jeho	jeho	k3xOp3gInPc1	jeho
početní	početní	k2eAgInPc1d1	početní
stavy	stav	k1gInPc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vymizel	vymizet	k5eAaPmAgMnS	vymizet
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
naposledy	naposledy	k6eAd1	naposledy
zahnízdil	zahnízdit	k5eAaPmAgInS	zahnízdit
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
původních	původní	k2eAgFnPc2d1	původní
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
opět	opět	k6eAd1	opět
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
i	i	k8xC	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
5.000	[number]	k4	5.000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
na	na	k7c4	na
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
poměrně	poměrně	k6eAd1	poměrně
běžně	běžně	k6eAd1	běžně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
jako	jako	k9	jako
hnízdič	hnízdič	k1gMnSc1	hnízdič
postupně	postupně	k6eAd1	postupně
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
poslední	poslední	k2eAgNnPc1d1	poslední
hnízdiště	hnízdiště	k1gNnPc1	hnízdiště
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
desetiletí	desetiletí	k1gNnPc1	desetiletí
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevoval	objevovat	k5eAaImAgInS	objevovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
největším	veliký	k2eAgNnSc7d3	veliký
zimovištěm	zimoviště	k1gNnSc7	zimoviště
bylo	být	k5eAaImAgNnS	být
pravidelně	pravidelně	k6eAd1	pravidelně
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
také	také	k9	také
díky	díky	k7c3	díky
příhodným	příhodný	k2eAgFnPc3d1	příhodná
podmínkám	podmínka	k1gFnPc3	podmínka
k	k	k7c3	k
hnízdění	hnízdění	k1gNnSc3	hnízdění
a	a	k8xC	a
dostatečné	dostatečný	k2eAgFnSc3d1	dostatečná
potravní	potravní	k2eAgFnSc3d1	potravní
nabídce	nabídka	k1gFnSc3	nabídka
stalo	stát	k5eAaPmAgNnS	stát
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgFnS	být
zacílena	zacílen	k2eAgFnSc1d1	zacílena
snaha	snaha	k1gFnSc1	snaha
ornitologů	ornitolog	k1gMnPc2	ornitolog
a	a	k8xC	a
orgánů	orgán	k1gMnPc2	orgán
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
původních	původní	k2eAgNnPc2d1	původní
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
přikrmování	přikrmování	k1gNnSc1	přikrmování
zimujících	zimující	k2eAgMnPc2d1	zimující
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
postavení	postavení	k1gNnSc4	postavení
několika	několik	k4yIc2	několik
umělých	umělý	k2eAgNnPc2d1	umělé
hnízd	hnízdo	k1gNnPc2	hnízdo
a	a	k8xC	a
vypouštění	vypouštění	k1gNnSc2	vypouštění
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
odchovaných	odchovaný	k2eAgMnPc2d1	odchovaný
mladých	mladý	k2eAgMnPc2d1	mladý
orlů	orel	k1gMnPc2	orel
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1978-89	[number]	k4	1978-89
bylo	být	k5eAaImAgNnS	být
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
zahnízdění	zahnízdění	k1gNnSc1	zahnízdění
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
pár	pár	k4xCyI	pár
tvořili	tvořit	k5eAaImAgMnP	tvořit
jedinci	jedinec	k1gMnPc1	jedinec
vypuštění	vypuštění	k1gNnPc2	vypuštění
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
mláďata	mládě	k1gNnPc1	mládě
byla	být	k5eAaImAgNnP	být
úspěšně	úspěšně	k6eAd1	úspěšně
vyvedena	vyvést	k5eAaPmNgNnP	vyvést
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
zahnízdění	zahnízdění	k1gNnSc3	zahnízdění
došlo	dojít	k5eAaPmAgNnS	dojít
nejméně	málo	k6eAd3	málo
na	na	k7c6	na
2	[number]	k4	2
lokalitách	lokalita	k1gFnPc6	lokalita
již	již	k6eAd1	již
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
divoké	divoký	k2eAgFnSc2d1	divoká
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jen	jen	k9	jen
zimující	zimující	k2eAgFnSc2d1	zimující
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
zahnízdění	zahnízdění	k1gNnSc6	zahnízdění
zaznamenán	zaznamenán	k2eAgMnSc1d1	zaznamenán
i	i	k8xC	i
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
i	i	k8xC	i
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
zdejší	zdejší	k2eAgFnSc6d1	zdejší
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
,	,	kIx,	,
první	první	k4xOgNnPc4	první
mláďata	mládě	k1gNnPc4	mládě
zde	zde	k6eAd1	zde
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vyvedena	vyvést	k5eAaPmNgFnS	vyvést
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
orel	orel	k1gMnSc1	orel
mořský	mořský	k2eAgMnSc1d1	mořský
jako	jako	k8xC	jako
hnízdící	hnízdící	k2eAgInSc1d1	hnízdící
druh	druh	k1gInSc1	druh
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
do	do	k7c2	do
Českobudějovicka	Českobudějovicko	k1gNnSc2	Českobudějovicko
<g/>
,	,	kIx,	,
Jindřichohradecka	Jindřichohradecko	k1gNnSc2	Jindřichohradecko
a	a	k8xC	a
Českolipska	Českolipsko	k1gNnSc2	Českolipsko
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985-89	[number]	k4	1985-89
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
8-10	[number]	k4	8-10
párů	pár	k1gInPc2	pár
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
na	na	k7c4	na
10-15	[number]	k4	10-15
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
17	[number]	k4	17
hnízdění	hnízdění	k1gNnPc2	hnízdění
a	a	k8xC	a
u	u	k7c2	u
dalších	další	k2eAgMnPc2d1	další
4-8	[number]	k4	4-8
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
možné	možný	k2eAgMnPc4d1	možný
až	až	k9	až
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
velikost	velikost	k1gFnSc1	velikost
hnízdící	hnízdící	k2eAgFnSc2d1	hnízdící
populace	populace	k1gFnSc2	populace
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
25-30	[number]	k4	25-30
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
na	na	k7c4	na
100	[number]	k4	100
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Stoupající	stoupající	k2eAgFnSc4d1	stoupající
tendenci	tendence	k1gFnSc4	tendence
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
počet	počet	k1gInSc1	počet
zimujících	zimující	k2eAgMnPc2d1	zimující
ptáků	pták	k1gMnPc2	pták
<g/>
;	;	kIx,	;
největšími	veliký	k2eAgMnPc7d3	veliký
zimovišti	zimoviště	k1gNnSc6	zimoviště
jsou	být	k5eAaImIp3nP	být
Třeboňsko	Třeboňsko	k1gNnSc4	Třeboňsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pravidelně	pravidelně	k6eAd1	pravidelně
zimuje	zimovat	k5eAaImIp3nS	zimovat
40-50	[number]	k4	40-50
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
Novomlýnské	novomlýnský	k2eAgFnPc1d1	Novomlýnská
nádrže	nádrž	k1gFnPc1	nádrž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
až	až	k9	až
60	[number]	k4	60
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
počtu	počet	k1gInSc6	počet
zimuje	zimovat	k5eAaImIp3nS	zimovat
i	i	k9	i
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
orla	orel	k1gMnSc2	orel
mořského	mořský	k2eAgMnSc2d1	mořský
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
ptáky	pták	k1gMnPc7	pták
a	a	k8xC	a
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
složení	složení	k1gNnSc1	složení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
;	;	kIx,	;
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
v	v	k7c6	v
ní	on	k3xPp3gFnSc2	on
převažují	převažovat	k5eAaImIp3nP	převažovat
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
93	[number]	k4	93
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
90,7	[number]	k4	90,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
části	část	k1gFnSc6	část
Laponska	Laponsko	k1gNnSc2	Laponsko
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
40,7	[number]	k4	40,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
bývají	bývat	k5eAaImIp3nP	bývat
nejčastěji	často	k6eAd3	často
chytány	chytán	k2eAgInPc1d1	chytán
exempláře	exemplář	k1gInPc1	exemplář
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
10-15	[number]	k4	10-15
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1-2	[number]	k4	1-2
kg	kg	kA	kg
(	(	kIx(	(
<g/>
štika	štika	k1gFnSc1	štika
<g/>
,	,	kIx,	,
okoun	okoun	k1gMnSc1	okoun
<g/>
,	,	kIx,	,
kapr	kapr	k1gMnSc1	kapr
<g/>
,	,	kIx,	,
plotice	plotice	k1gFnSc1	plotice
<g/>
,	,	kIx,	,
cejn	cejn	k1gMnSc1	cejn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ale	ale	k9	ale
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
kořist	kořist	k1gFnSc4	kořist
těžkou	těžký	k2eAgFnSc4d1	těžká
5-6	[number]	k4	5-6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převládají	převládat	k5eAaImIp3nP	převládat
vodní	vodní	k2eAgMnPc1d1	vodní
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
lysky	lyska	k1gFnPc1	lyska
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc1	kachna
<g/>
,	,	kIx,	,
racci	racek	k1gMnPc1	racek
či	či	k8xC	či
potápky	potápka	k1gFnPc1	potápka
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
po	po	k7c6	po
větších	veliký	k2eAgMnPc6d2	veliký
savcích	savec	k1gMnPc6	savec
(	(	kIx(	(
<g/>
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
<g/>
)	)	kIx)	)
pocházejí	pocházet	k5eAaImIp3nP	pocházet
nepochybně	pochybně	k6eNd1	pochybně
z	z	k7c2	z
padlých	padlý	k1gMnPc2	padlý
nebo	nebo	k8xC	nebo
silně	silně	k6eAd1	silně
oslabených	oslabený	k2eAgInPc2d1	oslabený
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
přímý	přímý	k2eAgInSc1d1	přímý
útok	útok	k1gInSc1	útok
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnSc1d1	denní
spotřeba	spotřeba	k1gFnSc1	spotřeba
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
700	[number]	k4	700
g	g	kA	g
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
500	[number]	k4	500
g	g	kA	g
čisté	čistý	k2eAgFnSc2d1	čistá
svaloviny	svalovina	k1gFnSc2	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
za	za	k7c2	za
letu	let	k1gInSc2	let
nebo	nebo	k8xC	nebo
vsedě	vsedě	k6eAd1	vsedě
z	z	k7c2	z
vyvýšeného	vyvýšený	k2eAgNnSc2d1	vyvýšené
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
zcela	zcela	k6eAd1	zcela
zdravou	zdravý	k2eAgFnSc4d1	zdravá
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
uhynulé	uhynulý	k2eAgInPc4d1	uhynulý
nebo	nebo	k8xC	nebo
oslabené	oslabený	k2eAgInPc4d1	oslabený
kusy	kus	k1gInPc4	kus
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
sanitární	sanitární	k2eAgFnSc4d1	sanitární
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
občasný	občasný	k2eAgInSc1d1	občasný
tzv.	tzv.	kA	tzv.
kleptoparasitismus	kleptoparasitismus	k1gInSc1	kleptoparasitismus
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
zatopených	zatopený	k2eAgFnPc2d1	zatopená
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
v	v	k7c6	v
jezernatých	jezernatý	k2eAgFnPc6d1	jezernatá
<g/>
,	,	kIx,	,
rybničnatých	rybničnatý	k2eAgFnPc6d1	rybničnatá
a	a	k8xC	a
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zásnubní	zásnubní	k2eAgInPc4d1	zásnubní
lety	let	k1gInPc4	let
za	za	k7c2	za
mírného	mírný	k2eAgNnSc2d1	mírné
počasí	počasí	k1gNnSc2	počasí
probíhají	probíhat	k5eAaImIp3nP	probíhat
již	již	k9	již
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
během	během	k7c2	během
ledna	leden	k1gInSc2	leden
až	až	k8xS	až
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
při	při	k7c6	při
nich	on	k3xPp3gInPc6	on
vystoupají	vystoupat	k5eAaPmIp3nP	vystoupat
za	za	k7c2	za
hlasitého	hlasitý	k2eAgNnSc2d1	hlasité
volání	volání	k1gNnSc2	volání
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
samec	samec	k1gMnSc1	samec
spouští	spouštět	k5eAaImIp3nS	spouštět
střemhlav	střemhlav	k6eAd1	střemhlav
na	na	k7c4	na
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
převrátí	převrátit	k5eAaPmIp3nS	převrátit
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
a	a	k8xC	a
proti	proti	k7c3	proti
samci	samec	k1gMnSc3	samec
nastaví	nastavět	k5eAaBmIp3nS	nastavět
pařáty	pařát	k1gInPc7	pařát
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
stavba	stavba	k1gFnSc1	stavba
ze	z	k7c2	z
silných	silný	k2eAgFnPc2d1	silná
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
vystlaná	vystlaný	k2eAgFnSc1d1	vystlaná
tenkými	tenký	k2eAgFnPc7d1	tenká
větvičkami	větvička	k1gFnPc7	větvička
<g/>
,	,	kIx,	,
trávou	tráva	k1gFnSc7	tráva
nebo	nebo	k8xC	nebo
drny	drn	k1gInPc1	drn
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
skalách	skála	k1gFnPc6	skála
<g/>
,	,	kIx,	,
v	v	k7c6	v
rákosinách	rákosina	k1gFnPc6	rákosina
a	a	k8xC	a
v	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
i	i	k8xC	i
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
borovicích	borovice	k1gFnPc6	borovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
stavbě	stavba	k1gFnSc6	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
2,5	[number]	k4	2,5
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Párem	pár	k1gInSc7	pár
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
příhodných	příhodný	k2eAgFnPc2d1	příhodná
podmínek	podmínka	k1gFnPc2	podmínka
využíváno	využívat	k5eAaPmNgNnS	využívat
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
letech	let	k1gInPc6	let
střídat	střídat	k5eAaImF	střídat
více	hodně	k6eAd2	hodně
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
hnízda	hnízdo	k1gNnSc2	hnízdo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
160	[number]	k4	160
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
nového	nový	k2eAgNnSc2d1	nové
hnízda	hnízdo	k1gNnSc2	hnízdo
60	[number]	k4	60
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
však	však	k9	však
hnízdo	hnízdo	k1gNnSc1	hnízdo
trochu	trochu	k6eAd1	trochu
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
výšky	výška	k1gFnSc2	výška
i	i	k9	i
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
náhradní	náhradní	k2eAgFnSc4d1	náhradní
snůšku	snůška	k1gFnSc4	snůška
klade	klást	k5eAaImIp3nS	klást
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
brzkého	brzký	k2eAgNnSc2d1	brzké
zničení	zničení	k1gNnSc2	zničení
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
začíná	začínat	k5eAaImIp3nS	začínat
hnízdit	hnízdit	k5eAaImF	hnízdit
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
klade	klást	k5eAaImIp3nS	klást
1-2	[number]	k4	1-2
bílá	bílý	k2eAgNnPc1d1	bílé
vejce	vejce	k1gNnPc1	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
74,7	[number]	k4	74,7
x	x	k?	x
57,9	[number]	k4	57,9
mm	mm	kA	mm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
141-145	[number]	k4	141-145
g.	g.	k?	g.
Snášena	snášen	k2eAgFnSc1d1	snášena
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
2-5	[number]	k4	2-5
dnů	den	k1gInPc2	den
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
inkubaci	inkubace	k1gFnSc6	inkubace
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
cca	cca	kA	cca
38	[number]	k4	38
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
zpočátku	zpočátku	k6eAd1	zpočátku
krmí	krmit	k5eAaImIp3nP	krmit
jen	jen	k9	jen
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
trhá	trhat	k5eAaImIp3nS	trhat
veškerou	veškerý	k3xTgFnSc4	veškerý
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
po	po	k7c6	po
5-6	[number]	k4	5-6
týdnech	týden	k1gInPc6	týden
si	se	k3xPyFc3	se
s	s	k7c7	s
kořistí	kořist	k1gFnSc7	kořist
již	již	k6eAd1	již
poradí	poradit	k5eAaPmIp3nS	poradit
sama	sám	k3xTgMnSc4	sám
a	a	k8xC	a
předkládá	předkládat	k5eAaImIp3nS	předkládat
jim	on	k3xPp3gMnPc3	on
jí	jíst	k5eAaImIp3nS	jíst
i	i	k9	i
samec	samec	k1gMnSc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
krmení	krmení	k1gNnSc2	krmení
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
70-75	[number]	k4	70-75
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
zprvu	zprvu	k6eAd1	zprvu
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
poblíž	poblíž	k6eAd1	poblíž
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gMnPc4	on
krmí	krmit	k5eAaImIp3nP	krmit
ještě	ještě	k9	ještě
aspoň	aspoň	k9	aspoň
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
zaznamenaný	zaznamenaný	k2eAgInSc4d1	zaznamenaný
věk	věk	k1gInSc4	věk
34	[number]	k4	34
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
nemají	mít	k5eNaImIp3nP	mít
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
žádného	žádný	k3yNgMnSc2	žádný
přirozeného	přirozený	k2eAgMnSc2d1	přirozený
predátora	predátor	k1gMnSc2	predátor
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
lidská	lidský	k2eAgFnSc1d1	lidská
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časných	časný	k2eAgFnPc6d1	časná
fázích	fáze	k1gFnPc6	fáze
hnízdění	hnízdění	k1gNnSc2	hnízdění
jsou	být	k5eAaImIp3nP	být
citliví	citlivý	k2eAgMnPc1d1	citlivý
na	na	k7c4	na
vyrušování	vyrušování	k1gNnSc4	vyrušování
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
přítomnost	přítomnost	k1gFnSc1	přítomnost
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hnízda	hnízdo	k1gNnSc2	hnízdo
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
opuštění	opuštění	k1gNnSc3	opuštění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
dřeva	dřevo	k1gNnSc2	dřevo
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
závažnější	závažný	k2eAgInSc1d2	závažnější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
ilegální	ilegální	k2eAgNnSc1d1	ilegální
pokládání	pokládání	k1gNnSc1	pokládání
otrávených	otrávený	k2eAgFnPc2d1	otrávená
návnad	návnada	k1gFnPc2	návnada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
velmi	velmi	k6eAd1	velmi
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
doplatilo	doplatit	k5eAaPmAgNnS	doplatit
9	[number]	k4	9
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
desátý	desátý	k4xOgMnSc1	desátý
byl	být	k5eAaImAgMnS	být
postřelen	postřelen	k2eAgMnSc1d1	postřelen
<g/>
.	.	kIx.	.
</s>
<s>
Jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
používán	používat	k5eAaImNgInS	používat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
insekticid	insekticid	k1gInSc4	insekticid
karbofuran	karbofurana	k1gFnPc2	karbofurana
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
jakékoli	jakýkoli	k3yIgNnSc1	jakýkoli
pokládání	pokládání	k1gNnSc1	pokládání
otrávených	otrávený	k2eAgFnPc2d1	otrávená
návnad	návnada	k1gFnPc2	návnada
nelegální	legální	k2eNgInSc1d1	nelegální
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
let	let	k1gInSc4	let
způsobit	způsobit	k5eAaPmF	způsobit
vymizení	vymizení	k1gNnSc4	vymizení
orlů	orel	k1gMnPc2	orel
mořských	mořský	k2eAgMnPc2d1	mořský
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990-2000	[number]	k4	1990-2000
zkoumáno	zkoumán	k2eAgNnSc1d1	zkoumáno
120	[number]	k4	120
kadavérů	kadavér	k1gMnPc2	kadavér
orlů	orel	k1gMnPc2	orel
mořských	mořský	k2eAgInPc2d1	mořský
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nejčastějšími	častý	k2eAgFnPc7d3	nejčastější
příčinami	příčina	k1gFnPc7	příčina
úhynu	úhyn	k1gInSc2	úhyn
srážka	srážka	k1gFnSc1	srážka
s	s	k7c7	s
vlaky	vlak	k1gInPc7	vlak
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otrava	otrava	k1gFnSc1	otrava
olovem	olovo	k1gNnSc7	olovo
(	(	kIx(	(
<g/>
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
infekční	infekční	k2eAgNnPc1d1	infekční
onemocnění	onemocnění	k1gNnPc1	onemocnění
(	(	kIx(	(
<g/>
11	[number]	k4	11
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
příčinám	příčina	k1gFnPc3	příčina
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
například	například	k6eAd1	například
zásah	zásah	k1gInSc4	zásah
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
(	(	kIx(	(
<g/>
9	[number]	k4	9
%	%	kIx~	%
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zranění	zranění	k1gNnSc1	zranění
způsobené	způsobený	k2eAgNnSc1d1	způsobené
během	běh	k1gInSc7	běh
vnitrodruhových	vnitrodruhový	k2eAgInPc2d1	vnitrodruhový
konfliktů	konflikt	k1gInPc2	konflikt
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
chlorovanými	chlorovaný	k2eAgInPc7d1	chlorovaný
uhlovodíky	uhlovodík	k1gInPc7	uhlovodík
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
u	u	k7c2	u
3	[number]	k4	3
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
otrava	otrava	k1gFnSc1	otrava
karbofuranem	karbofuran	k1gInSc7	karbofuran
u	u	k7c2	u
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
kriticky	kriticky	k6eAd1	kriticky
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
též	též	k9	též
v	v	k7c6	v
Příloze	příloha	k1gFnSc6	příloha
I.	I.	kA	I.
Směrnice	směrnice	k1gFnSc2	směrnice
o	o	k7c6	o
ptácích	pták	k1gMnPc6	pták
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
-	-	kIx~	-
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Orel	Orel	k1gMnSc1	Orel
mořský	mořský	k2eAgMnSc1d1	mořský
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
heraldickým	heraldický	k2eAgNnSc7d1	heraldické
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
i	i	k8xC	i
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
