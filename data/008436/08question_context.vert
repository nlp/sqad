<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
výrazná	výrazný	k2eAgFnSc1d1	výrazná
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
maximum	maximum	k1gNnSc1	maximum
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
velice	velice	k6eAd1	velice
citlivé	citlivý	k2eAgInPc1d1	citlivý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
žlutá	žlutat	k5eAaImIp3nS	žlutat
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
upoutání	upoutání	k1gNnSc4	upoutání
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
varování	varování	k1gNnSc1	varování
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
:	:	kIx,	:
</s>

