<s>
Josef	Josef	k1gMnSc1
Osvald	Osvald	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thun-Hohenstein	Thun-Hohenstein	k1gInSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Osvald	Osvald	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1
Erb	erb	k1gInSc4
rodu	rod	k1gInSc2
Thun-Hohensteinů	Thun-Hohenstein	k1gMnPc2
</s>
<s>
Poslanec	poslanec	k1gMnSc1
českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1909	#num#	k4
–	–	k?
1913	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1878	#num#	k4
Klösterle	Klösterle	k1gNnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1942	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Klösterle	Klösterle	k1gInSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Josef	Josef	k1gMnSc1
Osvald	Osvald	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thun-Hohenstein	Thun-Hohensteina	k1gFnPc2
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Právnická	právnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Německé	německý	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Osvald	Osvald	k1gMnSc1
hrabě	hrabě	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1
(	(	kIx(
<g/>
Josef	Josef	k1gMnSc1
Osvald	Osvald	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matyáš	Matyáš	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Thun-Hohenstein-Salm-Reifferscheidt	Thun-Hohenstein-Salm-Reifferscheidt	k1gMnSc1
/	/	kIx~
Joseph	Joseph	k1gMnSc1
Oswald	Oswald	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matthias	Matthias	k1gMnSc1
Reichsgraf	Reichsgraf	k1gMnSc1
von	von	k1gInSc4
Thun	Thun	k1gMnSc1
und	und	k?
Hohenstein-Salm-Reifferscheidt	Hohenstein-Salm-Reifferscheidt	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1878	#num#	k4
<g/>
,	,	kIx,
Klášterec	Klášterec	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1942	#num#	k4
<g/>
,	,	kIx,
Klášterec	Klášterec	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
a	a	k8xC
velkostatkář	velkostatkář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Zámek	zámek	k1gInSc1
Klášterec	Klášterec	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
ze	z	k7c2
starého	starý	k2eAgInSc2d1
šlechtického	šlechtický	k2eAgInSc2d1
rodu	rod	k1gInSc2
Thun-Hohensteinů	Thun-Hohenstein	k1gMnPc2
<g/>
,	,	kIx,
patřil	patřit	k5eAaImAgInS
ke	k	k7c3
klášterecké	klášterecký	k2eAgFnSc3d1
rodové	rodový	k2eAgFnSc3d1
větvi	větev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Klášterci	Klášterec	k1gInSc6
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
jako	jako	k8xS,k8xC
nejstarší	starý	k2eAgMnSc1d3
ze	z	k7c2
tří	tři	k4xCgMnPc2
synů	syn	k1gMnPc2
Josefa	Josef	k1gMnSc2
Osvalda	Osvald	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thun-Hohensteina	Thun-Hohensteino	k1gNnPc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
Kristina	Kristina	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
–	–	k?
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
rodině	rodina	k1gFnSc3
Valdštejnů	Valdštejn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
Theresianum	Theresianum	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
maturitě	maturita	k1gFnSc6
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
studoval	studovat	k5eAaImAgMnS
práva	právo	k1gNnSc2
na	na	k7c6
Karlově	Karlův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
tiulu	tiula	k1gMnSc4
doktora	doktor	k1gMnSc4
práv	práv	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
armádě	armáda	k1gFnSc6
a	a	k8xC
dosáhl	dosáhnout	k5eAaPmAgInS
hodnosti	hodnost	k1gFnSc3
nadporučíka	nadporučík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1909	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
byl	být	k5eAaImAgMnS
poslancem	poslanec	k1gMnSc7
českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
a	a	k8xC
po	po	k7c6
otci	otec	k1gMnSc6
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
získal	získat	k5eAaPmAgMnS
dědičné	dědičný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
panské	panský	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
otcem	otec	k1gMnSc7
užíval	užívat	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1897	#num#	k4
alianční	alianční	k2eAgFnSc1d1
příjmení	příjmení	k1gNnSc4
Thun-Hohenstein-Salm-Reifferscheidt	Thun-Hohenstein-Salm-Reifferscheidt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
zdědil	zdědit	k5eAaPmAgInS
několik	několik	k4yIc4
velkostatků	velkostatek	k1gInPc2
v	v	k7c6
severních	severní	k2eAgFnPc6d1
a	a	k8xC
východních	východní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
a	a	k8xC
na	na	k7c6
Vysočině	vysočina	k1gFnSc6
(	(	kIx(
<g/>
Klášterec	Klášterec	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
,	,	kIx,
Žehušice	Žehušice	k1gFnSc1
<g/>
,	,	kIx,
Benešov	Benešov	k1gInSc1
nad	nad	k7c7
Ploučnicí	Ploučnice	k1gFnSc7
<g/>
,	,	kIx,
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
,	,	kIx,
Hanšpach	Hanšpach	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hned	hned	k6eAd1
po	po	k7c6
převzetí	převzetí	k1gNnSc6
rodového	rodový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
přistoupil	přistoupit	k5eAaPmAgInS
k	k	k7c3
rozprodeji	rozprodej	k1gInSc3
zadlužených	zadlužený	k2eAgFnPc2d1
částí	část	k1gFnPc2
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
prodal	prodat	k5eAaPmAgInS
Žehušice	Žehušice	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
inkasoval	inkasovat	k5eAaBmAgInS
od	od	k7c2
židovského	židovský	k2eAgMnSc2d1
průmyslníka	průmyslník	k1gMnSc2
Richarda	Richard	k1gMnSc2
Morawetze	Morawetz	k1gInSc6
dva	dva	k4xCgInPc1
a	a	k8xC
půl	půl	k1xP
miliónu	milión	k4xCgInSc2
korun	koruna	k1gFnPc2
za	za	k7c4
velkostatek	velkostatek	k1gInSc4
Světlá	světlat	k5eAaImIp3nS
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
prodal	prodat	k5eAaPmAgInS
rodový	rodový	k2eAgInSc1d1
palác	palác	k1gInSc1
v	v	k7c6
Thunovské	Thunovský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
vládě	vláda	k1gFnSc6
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zde	zde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
britské	britský	k2eAgNnSc4d1
velvyslanectví	velvyslanectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
provedení	provedení	k1gNnSc6
pozemkové	pozemkový	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
na	na	k7c6
velkostatku	velkostatek	k1gInSc6
Lipová	lipový	k2eAgFnSc1d1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Hanšpach	Hanšpach	k1gInSc1
<g/>
)	)	kIx)
prodal	prodat	k5eAaPmAgInS
i	i	k9
zámek	zámek	k1gInSc1
v	v	k7c4
Lipové	lipový	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pozemkové	pozemkový	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
prodal	prodat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
i	i	k9
zámek	zámek	k1gInSc1
v	v	k7c6
Benešově	Benešov	k1gInSc6
nad	nad	k7c7
Ploučnicí	Ploučnice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodaření	hospodaření	k1gNnSc1
na	na	k7c6
značně	značně	k6eAd1
zmenšeném	zmenšený	k2eAgInSc6d1
majetku	majetek	k1gInSc6
vykazovalo	vykazovat	k5eAaImAgNnS
stále	stále	k6eAd1
pasivní	pasivní	k2eAgFnSc4d1
bilanci	bilance	k1gFnSc4
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
kvůli	kvůli	k7c3
nutným	nutný	k2eAgFnPc3d1
investicím	investice	k1gFnPc3
do	do	k7c2
rodinné	rodinný	k2eAgFnSc2d1
porcelánky	porcelánka	k1gFnSc2
Thun	Thun	k1gMnSc1
v	v	k7c6
Klášterci	Klášterec	k1gInSc6
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Osvald	Osvald	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
svobodný	svobodný	k2eAgMnSc1d1
a	a	k8xC
bezdětný	bezdětný	k2eAgMnSc1d1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Klášterci	Klášterec	k1gInSc6
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dědicem	dědic	k1gMnSc7
majetku	majetek	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
adoptovaný	adoptovaný	k2eAgMnSc1d1
synovec	synovec	k1gMnSc1
Matyáš	Matyáš	k1gMnSc1
Osvald	Osvald	k1gMnSc1
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
byly	být	k5eAaImAgInP
velkostatky	velkostatek	k1gInPc1
zestátněny	zestátněn	k2eAgInPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
Benešových	Benešových	k2eAgInPc2d1
dekretů	dekret	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HOŘEJŠ	HOŘEJŠ	kA
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
:	:	kIx,
Arizace	arizace	k1gFnSc1
pozemkového	pozemkový	k2eAgInSc2d1
majetku	majetek	k1gInSc2
židovských	židovský	k2eAgFnPc2d1
elit	elita	k1gFnPc2
na	na	k7c6
příkladu	příklad	k1gInSc6
rodiny	rodina	k1gFnSc2
Morawethů	Moraweth	k1gMnPc2
in	in	k?
<g/>
:	:	kIx,
Šlechticův	šlechticův	k2eAgMnSc1d1
Žid	Žid	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žid	Žid	k1gMnSc1
šlechticem	šlechtic	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovské	židovský	k2eAgFnPc4d1
elity	elita	k1gFnPc4
a	a	k8xC
židovská	židovský	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
v	v	k7c6
novověku	novověk	k1gInSc6
a	a	k8xC
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
;	;	kIx,
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
;	;	kIx,
s.	s.	k?
186	#num#	k4
<g/>
–	–	k?
<g/>
187	#num#	k4
ISBN	ISBN	kA
978-80-7464-730-7	978-80-7464-730-7	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KURTANIČ	KURTANIČ	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
A.	A.	kA
<g/>
:	:	kIx,
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klášterecká	Klášterecký	k2eAgFnSc1d1
větev	větev	k1gFnSc1
<g/>
;	;	kIx,
Klášterec	Klášterec	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
,	,	kIx,
2009	#num#	k4
73	#num#	k4
stran	strana	k1gFnPc2
ISBN	ISBN	kA
978-80-254-4262-3	978-80-254-4262-3	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Osvald	Osvald	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thun-Hohenstein	Thun-Hohenstein	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
rakouského	rakouský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
klášterecké	klášterecký	k2eAgFnSc2d1
linie	linie	k1gFnSc2
Thun-Hohensteinů	Thun-Hohenstein	k1gMnPc2
</s>
