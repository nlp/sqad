<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
Theresianum	Theresianum	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
maturitě	maturita	k1gFnSc6
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
studoval	studovat	k5eAaImAgMnS
práva	právo	k1gNnSc2
na	na	k7c6
Karlově	Karlův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
tiulu	tiula	k1gMnSc4
doktora	doktor	k1gMnSc4
práv	práv	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>