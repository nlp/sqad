<s>
Ženatý	ženatý	k2eAgMnSc1d1	ženatý
se	s	k7c7	s
závazky	závazek	k1gInPc7	závazek
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Married	Married	k1gInSc1	Married
<g/>
...	...	k?	...
with	with	k1gInSc1	with
Children	Childrna	k1gFnPc2	Childrna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
sitcom	sitcom	k1gInSc1	sitcom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xC	jako
satirická	satirický	k2eAgFnSc1d1	satirická
parodie	parodie	k1gFnSc1	parodie
rodinných	rodinný	k2eAgInPc2d1	rodinný
seriálů	seriál	k1gInPc2	seriál
typu	typ	k1gInSc2	typ
Krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
(	(	kIx(	(
<g/>
Step	step	k1gInSc1	step
by	by	kYmCp3nS	by
Step	step	k1gInSc1	step
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Plný	plný	k2eAgInSc1d1	plný
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Full	Full	k1gInSc1	Full
House	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
či	či	k8xC	či
Cosby	Cosba	k1gFnPc4	Cosba
Show	show	k1gFnSc2	show
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
jako	jako	k9	jako
sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
závist	závist	k1gFnSc4	závist
<g/>
,	,	kIx,	,
prohry	prohra	k1gFnPc4	prohra
a	a	k8xC	a
neúspěchy	neúspěch	k1gInPc4	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
epizoda	epizoda	k1gFnSc1	epizoda
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Komediální	komediální	k2eAgInSc1d1	komediální
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
dysfunkční	dysfunkční	k2eAgFnSc6d1	dysfunkční
rodině	rodina	k1gFnSc6	rodina
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
televizním	televizní	k2eAgInSc7d1	televizní
seriálem	seriál	k1gInSc7	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
běžel	běžet	k5eAaImAgInS	běžet
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
na	na	k7c4	na
Fox	fox	k1gInSc4	fox
Network	network	k1gInSc2	network
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
11	[number]	k4	11
sezón	sezóna	k1gFnPc2	sezóna
a	a	k8xC	a
262	[number]	k4	262
epizod	epizoda	k1gFnPc2	epizoda
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
druhý	druhý	k4xOgInSc4	druhý
nejdéle	dlouho	k6eAd3	dlouho
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
sitcom	sitcom	k1gInSc4	sitcom
na	na	k7c4	na
FOX	fox	k1gInSc4	fox
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
prvním	první	k4xOgMnSc6	první
jsou	být	k5eAaImIp3nP	být
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
píseň	píseň	k1gFnSc1	píseň
Love	lov	k1gInSc5	lov
and	and	k?	and
Marriage	Marriag	k1gMnSc4	Marriag
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatr	k1gMnSc2	Sinatr
<g/>
.	.	kIx.	.
</s>
<s>
Sitcom	Sitcom	k1gInSc1	Sitcom
sleduje	sledovat	k5eAaImIp3nS	sledovat
příhody	příhoda	k1gFnPc4	příhoda
muže	muž	k1gMnSc2	muž
jménem	jméno	k1gNnSc7	jméno
Al	ala	k1gFnPc2	ala
Bunda	bunda	k1gFnSc1	bunda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Al	ala	k1gFnPc2	ala
Bundy	bunda	k1gFnSc2	bunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
slavného	slavný	k2eAgMnSc4d1	slavný
hráče	hráč	k1gMnSc4	hráč
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
(	(	kIx(	(
<g/>
udělal	udělat	k5eAaPmAgMnS	udělat
4	[number]	k4	4
touchdowny	touchdown	k1gInPc7	touchdown
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
zápasu	zápas	k1gInSc2	zápas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
chudého	chudý	k2eAgMnSc2d1	chudý
prodavače	prodavač	k1gMnSc2	prodavač
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Peggy	Pegga	k1gFnSc2	Pegga
nosí	nosit	k5eAaImIp3nS	nosit
šaty	šat	k1gInPc4	šat
a	a	k8xC	a
účes	účes	k1gInSc1	účes
ze	z	k7c2	z
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
chůzi	chůze	k1gFnSc4	chůze
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
postrádá	postrádat	k5eAaImIp3nS	postrádat
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
Kelly	Kella	k1gFnSc2	Kella
je	být	k5eAaImIp3nS	být
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
<g/>
,	,	kIx,	,
promiskuitní	promiskuitní	k2eAgFnSc1d1	promiskuitní
a	a	k8xC	a
hloupá	hloupý	k2eAgFnSc1d1	hloupá
blondýnka	blondýnka	k1gFnSc1	blondýnka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
Bud	bouda	k1gFnPc2	bouda
je	být	k5eAaImIp3nS	být
chytrý	chytrý	k2eAgInSc1d1	chytrý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neoblíbený	oblíbený	k2eNgInSc1d1	neoblíbený
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
"	"	kIx"	"
<g/>
sbalit	sbalit	k5eAaPmF	sbalit
<g/>
"	"	kIx"	"
všechny	všechen	k3xTgFnPc1	všechen
dívky	dívka	k1gFnPc1	dívka
kolem	kolem	k6eAd1	kolem
<g/>
.	.	kIx.	.
</s>
<s>
Bundovi	Bundův	k2eAgMnPc1d1	Bundův
mají	mít	k5eAaImIp3nP	mít
snobské	snobský	k2eAgMnPc4d1	snobský
sousedy	soused	k1gMnPc4	soused
<g/>
,	,	kIx,	,
Steva	Steve	k1gMnSc2	Steve
a	a	k8xC	a
Marcy	Marca	k1gMnSc2	Marca
Rhodesovy	Rhodesův	k2eAgFnSc2d1	Rhodesův
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Marcy	Marca	k1gFnSc2	Marca
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
druhého	druhý	k4xOgMnSc2	druhý
manžela	manžel	k1gMnSc2	manžel
Jeffersona	Jefferson	k1gMnSc2	Jefferson
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Arcyho	Arcy	k1gMnSc2	Arcy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
epizod	epizoda	k1gFnPc2	epizoda
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
Ala	ala	k1gFnSc1	ala
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
neschopnost	neschopnost	k1gFnSc4	neschopnost
a	a	k8xC	a
neštěstí	neštěstí	k1gNnSc4	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
své	svůj	k3xOyFgInPc4	svůj
mnoholeté	mnoholetý	k2eAgInPc4d1	mnoholetý
působení	působení	k1gNnSc6	působení
tento	tento	k3xDgInSc1	tento
seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
nominován	nominovat	k5eAaBmNgInS	nominovat
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
ceny	cena	k1gFnPc4	cena
<g/>
...	...	k?	...
Casting	Casting	k1gInSc1	Casting
Society	societa	k1gFnSc2	societa
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
:	:	kIx,	:
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Casting	Casting	k1gInSc1	Casting
for	forum	k1gNnPc2	forum
TV	TV	kA	TV
-	-	kIx~	-
Episodic	Episodic	k1gMnSc1	Episodic
Comedy	Comeda	k1gMnSc2	Comeda
(	(	kIx(	(
<g/>
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
Emmy	Emma	k1gFnPc1	Emma
Awards	Awards	k1gInSc1	Awards
<g/>
:	:	kIx,	:
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Outstanding	Outstanding	k1gInSc1	Outstanding
Lighting	Lighting	k1gInSc1	Lighting
Direction	Direction	k1gInSc4	Direction
(	(	kIx(	(
<g/>
Electronic	Electronice	k1gFnPc2	Electronice
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
"	"	kIx"	"
<g/>
But	But	k1gMnSc1	But
I	i	k8xC	i
Didn	Didn	k1gMnSc1	Didn
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Shoot	Shoot	k1gInSc4	Shoot
the	the	k?	the
Deputy	Deput	k1gInPc4	Deput
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
Outstanding	Outstanding	k1gInSc1	Outstanding
Lighting	Lighting	k1gInSc1	Lighting
Direction	Direction	k1gInSc4	Direction
(	(	kIx(	(
<g/>
Electronic	Electronice	k1gFnPc2	Electronice
<g/>
)	)	kIx)	)
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Comedy	Comeda	k1gMnSc2	Comeda
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
"	"	kIx"	"
<g/>
Girls	girl	k1gFnPc2	girl
Just	just	k6eAd1	just
Wanna	Wann	k1gInSc2	Wann
Have	Hav	k1gMnSc2	Hav
Fun	Fun	k1gMnSc2	Fun
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
Outstanding	Outstanding	k1gInSc1	Outstanding
Editing	Editing	k1gInSc1	Editing
-	-	kIx~	-
Multi-Camera	Multi-Camera	k1gFnSc1	Multi-Camera
Production	Production	k1gInSc1	Production
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
"	"	kIx"	"
<g/>
Requiem	Requium	k1gNnSc7	Requium
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Dead	Deada	k1gFnPc2	Deada
Barber	Barbra	k1gFnPc2	Barbra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Outstanding	Outstanding	k1gInSc4	Outstanding
Costuming	Costuming	k1gInSc4	Costuming
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
"	"	kIx"	"
<g/>
Raingirl	Raingirl	k1gInSc1	Raingirl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
nominated	nominated	k1gInSc1	nominated
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Outstanding	Outstanding	k1gInSc1	Outstanding
Editing	Editing	k1gInSc1	Editing
-	-	kIx~	-
Multi-Camera	Multi-Camera	k1gFnSc1	Multi-Camera
Production	Production	k1gInSc1	Production
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
"	"	kIx"	"
<g/>
Who	Who	k1gFnSc1	Who
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Stop	stop	k1gInSc1	stop
the	the	k?	the
Rain	Rain	k1gInSc1	Rain
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Outstanding	Outstanding	k1gInSc4	Outstanding
Costuming	Costuming	k1gInSc4	Costuming
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
"	"	kIx"	"
<g/>
Married	Married	k1gInSc1	Married
<g/>
...	...	k?	...
with	with	k1gInSc1	with
Aliens	Aliens	k1gInSc1	Aliens
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Outstanding	Outstanding	k1gInSc4	Outstanding
Costuming	Costuming	k1gInSc4	Costuming
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
for	forum	k1gNnPc2	forum
"	"	kIx"	"
<g/>
Take	Take	k1gInSc1	Take
My	my	k3xPp1nPc1	my
Wife	Wifus	k1gMnSc5	Wifus
<g/>
,	,	kIx,	,
Please	Pleas	k1gMnSc5	Pleas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
Zlaté	zlatý	k2eAgInPc4d1	zlatý
glóbusy	glóbus	k1gInPc4	glóbus
<g/>
:	:	kIx,	:
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Actress	Actress	k1gInSc1	Actress
-	-	kIx~	-
Musical	musical	k1gInSc1	musical
or	or	k?	or
Comedy	Comeda	k1gMnSc2	Comeda
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
Katey	Katea	k1gFnSc2	Katea
Sagal	Sagal	k1gMnSc1	Sagal
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
"	"	kIx"	"
<g/>
Peggy	Pegga	k1gFnSc2	Pegga
Bundy	bunda	k1gFnSc2	bunda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominována	nominován	k2eAgFnSc1d1	nominována
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Series	Series	k1gInSc1	Series
-	-	kIx~	-
Musical	musical	k1gInSc1	musical
or	or	k?	or
Comedy	Comeda	k1gMnSc2	Comeda
(	(	kIx(	(
<g/>
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Actor	Actor	k1gInSc1	Actor
-	-	kIx~	-
Musical	musical	k1gInSc1	musical
or	or	k?	or
Comedy	Comeda	k1gMnSc2	Comeda
Series	Series	k1gMnSc1	Series
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neill	Neill	k1gInSc4	Neill
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
"	"	kIx"	"
<g/>
Al	ala	k1gFnPc2	ala
<g />
.	.	kIx.	.
</s>
<s>
Bunda	bunda	k1gFnSc1	bunda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Actress	Actress	k1gInSc1	Actress
-	-	kIx~	-
Musical	musical	k1gInSc1	musical
or	or	k?	or
Comedy	Comeda	k1gMnSc2	Comeda
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
Sagalová	Sagalová	k1gFnSc1	Sagalová
<g/>
,	,	kIx,	,
nominována	nominován	k2eAgFnSc1d1	nominována
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Actor	Actor	k1gInSc1	Actor
-	-	kIx~	-
Musical	musical	k1gInSc1	musical
or	or	k?	or
Comedy	Comeda	k1gMnSc2	Comeda
Series	Series	k1gMnSc1	Series
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neill	Neill	k1gMnSc1	Neill
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Actress	Actress	k1gInSc1	Actress
-	-	kIx~	-
Musical	musical	k1gInSc1	musical
or	or	k?	or
Comedy	Comeda	k1gMnSc2	Comeda
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
Sagalová	Sagalová	k1gFnSc1	Sagalová
<g/>
,	,	kIx,	,
nominována	nominován	k2eAgFnSc1d1	nominována
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
Best	Best	k2eAgInSc1d1	Best
Actress	Actress	k1gInSc1	Actress
-	-	kIx~	-
Musical	musical	k1gInSc1	musical
or	or	k?	or
Comedy	Comeda	k1gMnSc2	Comeda
Series	Seriesa	k1gFnPc2	Seriesa
(	(	kIx(	(
<g/>
Sagalová	Sagalová	k1gFnSc1	Sagalová
<g/>
,	,	kIx,	,
nominována	nominován	k2eAgFnSc1d1	nominována
<g/>
)	)	kIx)	)
Married	Married	k1gMnSc1	Married
with	with	k1gMnSc1	with
Hormones	Hormones	k1gMnSc1	Hormones
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
erotická	erotický	k2eAgFnSc1d1	erotická
parodie	parodie	k1gFnSc1	parodie
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
tři	tři	k4xCgFnPc1	tři
MwC	MwC	k1gFnPc1	MwC
epizody	epizoda	k1gFnSc2	epizoda
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
míněny	míněn	k2eAgInPc1d1	míněn
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
spin-offy	spinff	k1gInPc1	spin-off
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Heap	Heap	k1gInSc1	Heap
–	–	k?	–
téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
především	především	k9	především
jak	jak	k6eAd1	jak
rychle	rychle	k6eAd1	rychle
zbohatnout	zbohatnout	k5eAaPmF	zbohatnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
dílu	díl	k1gInSc6	díl
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
zahrál	zahrát	k5eAaPmAgMnS	zahrát
herec	herec	k1gMnSc1	herec
známý	známý	k1gMnSc1	známý
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
Matt	Matt	k2eAgInSc1d1	Matt
LeBlanc	LeBlanc	k1gInSc1	LeBlanc
<g/>
.	.	kIx.	.
</s>
<s>
Radio	radio	k1gNnSc1	radio
Free	Fre	k1gInSc2	Fre
Trumaine	Trumain	k1gInSc5	Trumain
–	–	k?	–
...	...	k?	...
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
času	čas	k1gInSc6	čas
Buda	Budum	k1gNnSc2	Budum
Bundy	bunda	k1gFnSc2	bunda
tráveným	trávený	k2eAgInPc3d1	trávený
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
rádiu	rádius	k1gInSc6	rádius
se	se	k3xPyFc4	se
Steve	Steve	k1gMnSc1	Steve
Rhoadesem	Rhoades	k1gInSc7	Rhoades
jako	jako	k8xC	jako
antagonistickým	antagonistický	k2eAgNnSc7d1	antagonistické
Deanem	Deano	k1gNnSc7	Deano
<g/>
.	.	kIx.	.
</s>
<s>
Enemies	Enemies	k1gInSc1	Enemies
–	–	k?	–
klon	klon	k1gInSc4	klon
Přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Založeno	založen	k2eAgNnSc1d1	založeno
na	na	k7c6	na
sociálním	sociální	k2eAgInSc6d1	sociální
kruhu	kruh	k1gInSc6	kruh
Kelly	Kella	k1gFnSc2	Kella
Bundové	bundový	k2eAgFnSc2d1	Bundová
<g/>
.	.	kIx.	.
</s>
<s>
Rodinní	rodinní	k2eAgMnPc1d1	rodinní
příslušníci	příslušník	k1gMnPc1	příslušník
skutečných	skutečný	k2eAgInPc2d1	skutečný
herců	herc	k1gInPc2	herc
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
seriálu	seriál	k1gInSc6	seriál
také	také	k6eAd1	také
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
<g/>
:	:	kIx,	:
Ed	Ed	k1gMnSc1	Ed
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neillova	Neillův	k2eAgFnSc1d1	Neillova
manželka	manželka	k1gFnSc1	manželka
Catherine	Catherin	k1gInSc5	Catherin
Rusoff	Rusoff	k1gInSc4	Rusoff
(	(	kIx(	(
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
cameo	cameo	k1gNnSc1	cameo
role	role	k1gFnSc2	role
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Faustinův	Faustinův	k2eAgMnSc1d1	Faustinův
bratr	bratr	k1gMnSc1	bratr
Michael	Michaela	k1gFnPc2	Michaela
Faustino	Faustin	k2eAgNnSc1d1	Faustino
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Christina	Christin	k2eAgFnSc1d1	Christina
Applegatové	Applegatový	k2eAgNnSc4d1	Applegatový
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Katey	Katea	k1gFnSc2	Katea
Sagalové	Sagalová	k1gFnSc2	Sagalová
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dvě	dva	k4xCgFnPc1	dva
tehdy	tehdy	k6eAd1	tehdy
skutečné	skutečný	k2eAgFnSc2d1	skutečná
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
Davida	David	k1gMnSc2	David
Faustina	Faustin	k1gMnSc2	Faustin
(	(	kIx(	(
<g/>
Juliet	Juliet	k1gMnSc1	Juliet
Tablak	Tablak	k1gMnSc1	Tablak
a	a	k8xC	a
Elaine	elain	k1gInSc5	elain
Hendrix	Hendrix	k1gInSc1	Hendrix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alovy	Alov	k1gInPc1	Alov
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hondo	Honda	k1gMnSc5	Honda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Shane	Shan	k1gInSc5	Shan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Telefonní	telefonní	k2eAgNnSc1d1	telefonní
číslo	číslo	k1gNnSc1	číslo
Bundyových	Bundyová	k1gFnPc2	Bundyová
je	být	k5eAaImIp3nS	být
555-2878	[number]	k4	555-2878
Peggyina	Peggyin	k2eAgFnSc1d1	Peggyin
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
relace	relace	k1gFnSc1	relace
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
Oprah	Opraha	k1gFnPc2	Opraha
a	a	k8xC	a
Donahue	Donahue	k1gInSc1	Donahue
také	také	k9	také
"	"	kIx"	"
<g/>
Psycho	psycha	k1gFnSc5	psycha
Mom	Mom	k1gFnSc3	Mom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Alova	Alova	k6eAd1	Alova
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
písnička	písnička	k1gFnSc1	písnička
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Duke	Duke	k1gNnSc1	Duke
of	of	k?	of
Earl	earl	k1gMnSc1	earl
<g/>
"	"	kIx"	"
od	od	k7c2	od
soulového	soulový	k2eAgMnSc2d1	soulový
zpěváka	zpěvák	k1gMnSc2	zpěvák
Gene	gen	k1gInSc5	gen
Chandlera	Chandler	k1gMnSc2	Chandler
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
botami	bota	k1gFnPc7	bota
kde	kde	k6eAd1	kde
Al	ala	k1gFnPc2	ala
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Gary	Gara	k1gFnPc1	Gara
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Shoes	Shoes	k1gInSc1	Shoes
and	and	k?	and
Accesories	Accesories	k1gInSc1	Accesories
for	forum	k1gNnPc2	forum
Today	Todaa	k1gFnSc2	Todaa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Woman	Womana	k1gFnPc2	Womana
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Garyho	Gary	k1gMnSc4	Gary
boty	bota	k1gFnSc2	bota
a	a	k8xC	a
příslušenství	příslušenství	k1gNnSc2	příslušenství
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgFnPc4d1	dnešní
ženy	žena	k1gFnPc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Peggyiny	Peggyin	k2eAgFnPc1d1	Peggyin
narozeniny	narozeniny	k1gFnPc1	narozeniny
jsou	být	k5eAaImIp3nP	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
I	i	k9	i
když	když	k8xS	když
nikdy	nikdy	k6eAd1	nikdy
nebylo	být	k5eNaImAgNnS	být
vyřknuto	vyřknout	k5eAaPmNgNnS	vyřknout
datum	datum	k1gNnSc1	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
Al	ala	k1gFnPc2	ala
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
znamením	znamení	k1gNnSc7	znamení
raka	rak	k1gMnSc2	rak
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
Married	Married	k1gInSc1	Married
<g/>
...	...	k?	...
with	with	k1gInSc1	with
Children	Childrna	k1gFnPc2	Childrna
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
seriálu	seriál	k1gInSc2	seriál
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
Bundyologie	Bundyologie	k1gFnSc1	Bundyologie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
ze	z	k7c2	z
Sony	Sony	kA	Sony
TV	TV	kA	TV
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Britská	britský	k2eAgFnSc1d1	britská
oficiální	oficiální	k2eAgFnSc1d1	oficiální
ztránka	ztránka	k1gFnSc1	ztránka
ze	z	k7c2	z
Sony	Sony	kA	Sony
TV	TV	kA	TV
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Married	Married	k1gInSc1	Married
with	witha	k1gFnPc2	witha
Children	Childrna	k1gFnPc2	Childrna
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Script	Script	k1gInSc1	Script
několika	několik	k4yIc2	několik
epizod	epizoda	k1gFnPc2	epizoda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bundyologie	Bundyologie	k1gFnSc1	Bundyologie
<g/>
:	:	kIx,	:
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
historie	historie	k1gFnSc1	historie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
