<s>
Behaimův	Behaimův	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
</s>
<s>
Behaimův	Behaimův	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
Behaimův	Behaimův	k2eAgInSc4d1
glóbus	glóbus	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Behaimův	Behaimův	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgInSc1d3
dochovaný	dochovaný	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Martin	Martin	k1gMnSc1
Behaim	Behaim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glóbus	glóbus	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
na	na	k7c4
žádost	žádost	k1gFnSc4
městské	městský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Norimberka	Norimberk	k1gInSc2
v	v	k7c6
letech	léto	k1gNnPc6
1492	#num#	k4
<g/>
-	-	kIx~
<g/>
1493	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověření	pověření	k1gNnSc1
lze	lze	k6eAd1
najít	najít	k5eAaPmF
v	v	k7c6
historických	historický	k2eAgInPc6d1
dokumentech	dokument	k1gInPc6
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
i	i	k9
ze	z	k7c2
samého	samý	k3xTgInSc2
glóbu	glóbus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pólu	pólo	k1gNnSc6
glóbu	glóbus	k1gInSc2
je	být	k5eAaImIp3nS
napsáno	napsat	k5eAaPmNgNnS,k5eAaBmNgNnS
věnování	věnování	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hlavně	hlavně	k9
v	v	k7c6
Německu	Německo	k1gNnSc6
je	být	k5eAaImIp3nS
glóbus	glóbus	k1gInSc1
označován	označovat	k5eAaImNgInS
jako	jako	k9
„	„	k?
<g/>
zemské	zemský	k2eAgNnSc1d1
jablko	jablko	k1gNnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
Erdapfel	Erdapfel	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glóbus	glóbus	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
posledních	poslední	k2eAgNnPc2d1
kartografických	kartografický	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
představují	představovat	k5eAaImIp3nP
svět	svět	k1gInSc4
před	před	k7c7
objevením	objevení	k1gNnSc7
Ameriky	Amerika	k1gFnSc2
Kryštofem	Kryštof	k1gMnSc7
Kolumbem	Kolumbus	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1492	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glóbus	glóbus	k1gInSc1
má	mít	k5eAaImIp3nS
průměru	průměr	k1gInSc2
54	#num#	k4
cm	cm	kA
a	a	k8xC
je	být	k5eAaImIp3nS
zhotoven	zhotovit	k5eAaPmNgInS
z	z	k7c2
papíru	papír	k1gInSc2
a	a	k8xC
sádry	sádra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
glóbu	glóbus	k1gInSc6
jsou	být	k5eAaImIp3nP
zachyceny	zachycen	k2eAgInPc1d1
tři	tři	k4xCgInPc1
tehdy	tehdy	k6eAd1
známé	známý	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc1
a	a	k8xC
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
množství	množství	k1gNnSc1
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
hypotetických	hypotetický	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1492	#num#	k4
byl	být	k5eAaImAgMnS
Martin	Martin	k1gMnSc1
Behaim	Behaim	k1gMnSc1
pověřen	pověřen	k2eAgMnSc1d1
vytvořit	vytvořit	k5eAaPmF
glóbus	glóbus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
odrážel	odrážet	k5eAaImAgInS
tehdejší	tehdejší	k2eAgInSc1d1
známý	známý	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
důvodech	důvod	k1gInPc6
vzniku	vznik	k1gInSc2
glóbu	glóbus	k1gInSc2
se	se	k3xPyFc4
stále	stále	k6eAd1
spekuluje	spekulovat	k5eAaImIp3nS
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
glóbus	glóbus	k1gInSc1
motivací	motivace	k1gFnPc2
pro	pro	k7c4
námořní	námořní	k2eAgFnPc4d1
expedice	expedice	k1gFnPc4
k	k	k7c3
hledání	hledání	k1gNnSc3
námořních	námořní	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
do	do	k7c2
Indie	Indie	k1gFnSc2
nebo	nebo	k8xC
rozvoj	rozvoj	k1gInSc4
námořního	námořní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgNnSc3
by	by	kYmCp3nS
odpovídalo	odpovídat	k5eAaImAgNnS
i	i	k9
znázornění	znázornění	k1gNnSc1
míst	místo	k1gNnPc2
původu	původ	k1gInSc2
různých	různý	k2eAgFnPc2d1
komodit	komodita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předlohou	předloha	k1gFnSc7
pro	pro	k7c4
Behaima	Behaim	k1gMnSc4
bylo	být	k5eAaImAgNnS
dílo	dílo	k1gNnSc1
Claudia	Claudia	k1gFnSc1
Ptolemaia	Ptolemaios	k1gMnSc2
<g/>
,	,	kIx,
Ptolemaiova	Ptolemaiův	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
vliv	vliv	k1gInSc4
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgMnSc1d1
na	na	k7c6
rozměrech	rozměr	k1gInPc6
kontinentů	kontinent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
současné	současný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
byly	být	k5eAaImAgInP
využity	využit	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
Marca	Marc	k2eAgFnSc1d1
Pola	pola	k1gFnSc1
a	a	k8xC
zprávy	zpráva	k1gFnSc2
Jehana	Jehan	k1gMnSc2
de	de	k?
Mandevilla	Mandevill	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Spolupracovníci	spolupracovník	k1gMnPc1
</s>
<s>
Behaim	Behaim	k1gMnSc1
nepracoval	pracovat	k5eNaImAgMnS
na	na	k7c6
celém	celý	k2eAgInSc6d1
glóbu	glóbus	k1gInSc6
sám	sám	k3xTgInSc4
přizval	přizvat	k5eAaPmAgMnS
si	se	k3xPyFc3
několik	několik	k4yIc4
spolupracovníků	spolupracovník	k1gMnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hans	Hans	k1gMnSc1
Glockengießer	Glockengießer	k1gMnSc1
-	-	kIx~
zkušený	zkušený	k2eAgMnSc1d1
zvonař	zvonař	k1gMnSc1
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgMnS
šablonu	šablona	k1gFnSc4
formy	forma	k1gFnSc2
glóbu	glóbus	k1gInSc2
</s>
<s>
Hans	Hans	k1gMnSc1
Stork	Stork	k1gInSc4
</s>
<s>
Ruprecht	Ruprecht	k2eAgInSc1d1
Kalberger	Kalberger	k1gInSc1
-	-	kIx~
vytvořil	vytvořit	k5eAaPmAgInS
kouli	koule	k1gFnSc4
glóbu	glóbus	k1gInSc2
</s>
<s>
Georg	Georg	k1gMnSc1
Glockendon	Glockendon	k1gMnSc1
-	-	kIx~
malíř	malíř	k1gMnSc1
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
malby	malba	k1gFnSc2
na	na	k7c6
glóbu	glóbus	k1gInSc6
</s>
<s>
Peter	Peter	k1gMnSc1
Gage	Gage	k1gNnSc2
Hart	Hart	k1gMnSc1
</s>
<s>
Postup	postup	k1gInSc1
výroby	výroba	k1gFnSc2
glóbu	glóbus	k1gInSc2
</s>
<s>
Projekce	projekce	k1gFnSc1
Behaimovo	Behaimův	k2eAgNnSc1d1
glóbusu	glóbus	k1gInSc6
</s>
<s>
Behaimův	Behaimův	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
</s>
<s>
Hans	Hans	k1gMnSc1
Glockengießer	Glockengießer	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
šablonu	šablona	k1gFnSc4
glóbu	glóbus	k1gInSc2
pro	pro	k7c4
zachování	zachování	k1gNnSc4
stejného	stejný	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
naplněna	naplnit	k5eAaPmNgFnS
jílem	jíl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jílovou	jílový	k2eAgFnSc4d1
kouli	koule	k1gFnSc4
potáhl	potáhnout	k5eAaPmAgInS
Ruprecht	Ruprecht	k2eAgInSc1d1
Kalberger	Kalberger	k1gInSc1
čtyřmi	čtyři	k4xCgFnPc7
vrstvami	vrstva	k1gFnPc7
plátna	plátno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
upevnil	upevnit	k5eAaPmAgInS
pomocí	pomocí	k7c2
šití	šití	k1gNnSc2
a	a	k8xC
lepidla	lepidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
byl	být	k5eAaImAgInS
papír	papír	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
kresby	kresba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zaschnutí	zaschnutí	k1gNnSc6
byla	být	k5eAaImAgFnS
koule	koule	k1gFnSc1
rozříznuta	rozříznout	k5eAaPmNgFnS
na	na	k7c6
severní	severní	k2eAgFnSc6d1
a	a	k8xC
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
v	v	k7c6
rovině	rovina	k1gFnSc6
rovníku	rovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jíl	jíl	k1gInSc1
z	z	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
části	část	k1gFnSc2
byl	být	k5eAaImAgInS
odstraněn	odstranit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
větší	veliký	k2eAgFnSc4d2
stabilitu	stabilita	k1gFnSc4
byly	být	k5eAaImAgInP
do	do	k7c2
těla	tělo	k1gNnSc2
glóbu	glóbus	k1gInSc2
vloženy	vložen	k2eAgFnPc4d1
dřevěné	dřevěný	k2eAgFnPc4d1
obruče	obruč	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
severní	severní	k2eAgInSc4d1
a	a	k8xC
jižní	jižní	k2eAgInSc4d1
pól	pól	k1gInSc4
byla	být	k5eAaImAgFnS
vsazena	vsazen	k2eAgFnSc1d1
osa	osa	k1gFnSc1
rotace	rotace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glóbus	glóbus	k1gInSc1
byl	být	k5eAaImAgInS
osazen	osadit	k5eAaPmNgInS
dvěma	dva	k4xCgInPc7
železnými	železný	k2eAgInPc7d1
kroužky	kroužek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
sloužily	sloužit	k5eAaImAgInP
jako	jako	k8xS,k8xC
poledník	poledník	k1gInSc1
a	a	k8xC
obzor	obzor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byla	být	k5eAaImAgFnS
na	na	k7c4
poslední	poslední	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
papíru	papír	k1gInSc2
nanesena	nanesen	k2eAgFnSc1d1
kresba	kresba	k1gFnSc1
kontinentů	kontinent	k1gInPc2
a	a	k8xC
moří	moře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
George	George	k1gInSc4
Glockendon	Glockendona	k1gFnPc2
a	a	k8xC
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
„	„	k?
<g/>
Erdepfel	Erdepfel	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
zemské	zemský	k2eAgNnSc1d1
jablko	jablko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kresba	kresba	k1gFnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
znázornění	znázornění	k1gNnSc4
poledníků	poledník	k1gInPc2
ani	ani	k8xC
rovnoběžek	rovnoběžka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1510	#num#	k4
byl	být	k5eAaImAgInS
glóbus	glóbus	k1gInSc1
opatřen	opatřit	k5eAaPmNgInS
dnešním	dnešní	k2eAgInSc7d1
kovovým	kovový	k2eAgInSc7d1
rámem	rám	k1gInSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgMnSc1
Behaim	Behaim	k1gMnSc1
pak	pak	k6eAd1
vytvořil	vytvořit	k5eAaPmAgMnS
popisky	popiska	k1gFnSc2
k	k	k7c3
různým	různý	k2eAgNnPc3d1
místům	místo	k1gNnPc3
a	a	k8xC
stručné	stručný	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
hospodářsky	hospodářsky	k6eAd1
významných	významný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nápisy	nápis	k1gInPc4
podél	podél	k7c2
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
pro	pro	k7c4
tyto	tento	k3xDgInPc4
texty	text	k1gInPc4
čerpal	čerpat	k5eAaImAgMnS
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
účasti	účast	k1gFnSc2
na	na	k7c6
plavbách	plavba	k1gFnPc6
Diogo	Diogo	k6eAd1
Coa	Coa	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1484	#num#	k4
<g/>
-	-	kIx~
<g/>
1485	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přesnost	přesnost	k1gFnSc1
glóbu	glóbus	k1gInSc2
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1
přesnost	přesnost	k1gFnSc1
glóbu	glóbus	k1gInSc2
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
použitých	použitý	k2eAgInPc6d1
zdrojích	zdroj	k1gInPc6
a	a	k8xC
liší	lišit	k5eAaImIp3nP
se	se	k3xPyFc4
podle	podle	k7c2
kontinentů	kontinent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpočet	výpočet	k1gInSc1
obvodu	obvod	k1gInSc2
Země	zem	k1gFnSc2
byl	být	k5eAaImAgInS
proveden	provést	k5eAaPmNgInS
podle	podle	k7c2
Ptolemaia	Ptolemaios	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nebyl	být	k5eNaImAgInS
přesný	přesný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
značnému	značný	k2eAgNnSc3d1
zkreslení	zkreslení	k1gNnSc3
některých	některý	k3yIgNnPc2
moří	moře	k1gNnPc2
a	a	k8xC
kontinentů	kontinent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
bylo	být	k5eAaImAgNnS
příliš	příliš	k6eAd1
dlouhé	dlouhý	k2eAgNnSc1d1
a	a	k8xC
Evropa	Evropa	k1gFnSc1
s	s	k7c7
Asií	Asie	k1gFnSc7
příliš	příliš	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgMnSc4
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
na	na	k7c4
glóbus	glóbus	k1gInSc4
začlenit	začlenit	k5eAaPmF
později	pozdě	k6eAd2
objevenou	objevený	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostor	prostor	k1gInSc1
v	v	k7c6
Atlantském	atlantský	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
tento	tento	k3xDgInSc4
kontinent	kontinent	k1gInSc4
nacházet	nacházet	k5eAaImF
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
příliš	příliš	k6eAd1
malý	malý	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
jsou	být	k5eAaImIp3nP
i	i	k9
v	v	k7c6
podrobnosti	podrobnost	k1gFnSc6
zpracování	zpracování	k1gNnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
kontinentů	kontinent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
severní	severní	k2eAgNnSc4d1
a	a	k8xC
západní	západní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
Afriky	Afrika	k1gFnSc2
jsou	být	k5eAaImIp3nP
zpracovány	zpracovat	k5eAaPmNgFnP
velmi	velmi	k6eAd1
přesně	přesně	k6eAd1
a	a	k8xC
detailně	detailně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
části	část	k1gFnPc4
Afriky	Afrika	k1gFnSc2
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
zpracovány	zpracovat	k5eAaPmNgInP
povrchně	povrchně	k6eAd1
a	a	k8xC
nepřesně	přesně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
Indie	Indie	k1gFnSc1
je	být	k5eAaImIp3nS
rozeznatelná	rozeznatelný	k2eAgFnSc1d1
již	již	k6eAd1
s	s	k7c7
problémy	problém	k1gInPc7
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
množství	množství	k1gNnSc4
spíše	spíše	k9
mytických	mytický	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
umístění	umístění	k1gNnSc2
</s>
<s>
Glóbus	glóbus	k1gInSc1
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgInS
na	na	k7c6
radnici	radnice	k1gFnSc6
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
až	až	k9
do	do	k7c2
počátku	počátek	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
předán	předat	k5eAaPmNgInS
rodině	rodina	k1gFnSc3
Behaima	Behaimum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1823	#num#	k4
byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgInS
zapomenut	zapomenout	k5eAaPmNgInS,k5eAaImNgInS
v	v	k7c4
podkroví	podkroví	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
domu	dům	k1gInSc2
<g/>
,	,	kIx,
následně	následně	k6eAd1
byl	být	k5eAaImAgInS
zapůjčen	zapůjčit	k5eAaPmNgInS
do	do	k7c2
německého	německý	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
ve	v	k7c6
snaze	snaha	k1gFnSc6
zabránit	zabránit	k5eAaPmF
odvozu	odvoz	k1gInSc3
glóbusu	glóbus	k1gInSc2
do	do	k7c2
USA	USA	kA
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
koupen	koupit	k5eAaPmNgMnS
kancléřem	kancléř	k1gMnSc7
Adolfa	Adolf	k1gMnSc2
Hitlera	Hitler	k1gMnSc2
a	a	k8xC
předán	předat	k5eAaPmNgInS
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Norimberku	Norimberk	k1gInSc6
je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
dodnes	dodnes	k6eAd1
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c4
Germanisches	Germanisches	k1gInSc4
Nationalmuseum	Nationalmuseum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1847	#num#	k4
a	a	k8xC
1908	#num#	k4
byly	být	k5eAaImAgFnP
vyrobeny	vyrobit	k5eAaPmNgFnP
dvě	dva	k4xCgFnPc1
kopie	kopie	k1gFnPc1
glóbu	glóbus	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
uloženy	uložit	k5eAaPmNgInP
v	v	k7c6
Paříži	Paříž	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Zájem	zájem	k1gInSc1
vědců	vědec	k1gMnPc2
</s>
<s>
Behaimův	Behaimův	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
je	být	k5eAaImIp3nS
středem	středem	k7c2
zájmů	zájem	k1gInPc2
vědců	vědec	k1gMnPc2
i	i	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
byl	být	k5eAaImAgInS
glóbus	glóbus	k1gInSc1
detailně	detailně	k6eAd1
zkoumán	zkoumat	k5eAaImNgInS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
rentgenu	rentgen	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
cílem	cíl	k1gInSc7
určit	určit	k5eAaPmF
materiál	materiál	k1gInSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
je	být	k5eAaImIp3nS
glóbus	glóbus	k1gInSc1
vytvořen	vytvořit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
rozvojem	rozvoj	k1gInSc7
technologií	technologie	k1gFnPc2
pro	pro	k7c4
digitalizaci	digitalizace	k1gFnSc4
byla	být	k5eAaImAgFnS
snaha	snaha	k1gFnSc1
o	o	k7c6
digitalizaci	digitalizace	k1gFnSc6
glóbu	glóbus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
pokus	pokus	k1gInSc1
byl	být	k5eAaImAgInS
proveden	provést	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
Technickou	technický	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
digitalizace	digitalizace	k1gFnSc1
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
Germánským	germánský	k2eAgNnSc7d1
národním	národní	k2eAgNnSc7d1
muzeem	muzeum	k1gNnSc7
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Johannes	Johannes	k1gInSc1
Willers	Willers	k1gInSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
des	des	k1gNnPc1
Behaim-Globus	Behaim-Globus	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Focus	Focus	k1gMnSc1
Behaim	Behaima	k1gFnPc2
Globus	globus	k1gInSc1
<g/>
,	,	kIx,
Teil	Teil	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
Aufsätze	Aufsätze	k1gFnSc1
<g/>
,	,	kIx,
Nürnberg	Nürnberg	k1gInSc1
19921	#num#	k4
2	#num#	k4
Renate	Renat	k1gInSc5
Hilsenbeck	Hilsenbeck	k1gInSc4
<g/>
:	:	kIx,
''	''	k?
<g/>
Mittelalterliche	Mittelalterlichus	k1gMnSc5
Weltkunde	Weltkund	k1gMnSc5
und	und	k?
Behaim-Globus	Behaim-Globus	k1gInSc1
<g/>
''	''	k?
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
''	''	k?
<g/>
Focus	Focus	k1gInSc1
Behaim	Behaim	k1gInSc1
Globus	globus	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
''	''	k?
<g/>
,	,	kIx,
Teil	Teil	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
Aufsätze	Aufsätze	k1gFnSc1
<g/>
,	,	kIx,
Nürnberg	Nürnberg	k1gInSc1
1992	#num#	k4
<g/>
↑	↑	k?
http://nuernberg.bayern-online.de/die-stadt/wissenswertes/der-behaim-globus/	http://nuernberg.bayern-online.de/die-stadt/wissenswertes/der-behaim-globus/	k?
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
cartographic-images	cartographic-images	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Lionel	Lionel	k1gInSc1
Dorffner	Dorffnra	k1gFnPc2
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
digitale	digital	k1gMnSc5
Behaim-Globus	Behaim-Globus	k1gMnSc1
-	-	kIx~
Visualisierung	Visualisierung	k1gMnSc1
und	und	k?
Vermessung	Vermessung	k1gMnSc1
des	des	k1gNnSc2
historisch	historisch	k1gMnSc1
wertvollen	wertvollen	k2eAgInSc1d1
Originals	Originals	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Cartographica	Cartographic	k2eAgFnSc1d1
Helvetica	Helvetica	k1gFnSc1
14	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
↑	↑	k?
Digitalizace	digitalizace	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machinepodle	Machinepodle	k1gFnSc2
německé	německý	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
Archivováno	archivován	k2eAgNnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Digitální	digitální	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
v	v	k7c6
geoinformatice	geoinformatika	k1gFnSc6
a	a	k8xC
kartografii	kartografie	k1gFnSc6
<g/>
:	:	kIx,
sborník	sborník	k1gInSc1
referátů	referát	k1gInPc2
:	:	kIx,
Praha	Praha	k1gFnSc1
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Editor	editor	k1gMnSc1
Bohuslav	Bohuslav	k1gMnSc1
Veverka	Veverka	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Růžena	Růžena	k1gFnSc1
Zimová	Zimová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4896	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://gkinfo.fsv.cvut.cz/2011/sbornik2011.pdf	http://gkinfo.fsv.cvut.cz/2011/sbornik2011.pdf	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
J.	J.	kA
Bräunlein	Bräunlein	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Behaim	Behaim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legende	Legend	k1gInSc5
und	und	k?
Wirklichkeit	Wirklichkeit	k2eAgMnSc1d1
eines	eines	k1gMnSc1
berühmten	berühmten	k2eAgInSc4d1
Nürnbergers	Nürnbergers	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bamberg	Bamberg	k1gInSc1
<g/>
:	:	kIx,
Bayerische	Bayerische	k1gInSc1
Verlags-Anstalt	Verlags-Anstalta	k1gFnPc2
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
253	#num#	k4
S.	S.	kA
In	In	k1gFnPc2
<g/>
:	:	kIx,
Zeitschrift	Zeitschrift	k1gInSc1
für	für	k?
historische	historischat	k5eAaPmIp3nS
Forschung	Forschung	k1gInSc1
<g/>
,	,	kIx,
Band	band	k1gInSc1
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bernd	Bernd	k1gInSc1
Hering	Hering	k1gInSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Herstellungstechnik	Herstellungstechnik	k1gMnSc1
des	des	k1gNnSc1
Behaim-Globus	Behaim-Globus	k1gMnSc1
<g/>
:	:	kIx,
Neue	Neue	k1gFnSc1
Ergebnisse	Ergebnisse	k1gFnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Focus	Focus	k1gMnSc1
Behaim	Behaima	k1gFnPc2
Globus	globus	k1gInSc1
<g/>
,	,	kIx,
Teil	Teil	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
Aufsätze	Aufsätze	k1gFnSc1
<g/>
,	,	kIx,
Nürnberg	Nürnberg	k1gInSc1
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Johannes	Johannes	k1gMnSc1
Willers	Willersa	k1gFnPc2
<g/>
:	:	kIx,
Die	Die	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
des	des	k1gNnPc1
Behaim-Globus	Behaim-Globus	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Focus	Focus	k1gMnSc1
Behaim	Behaima	k1gFnPc2
Globus	globus	k1gInSc1
<g/>
,	,	kIx,
Teil	Teil	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
Aufsätze	Aufsätze	k1gFnSc1
<g/>
,	,	kIx,
Nürnberg	Nürnberg	k1gInSc1
1992	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Behaimův	Behaimův	k2eAgInSc4d1
glóbus	glóbus	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://leccos.com/index.php/clanky/globus	http://leccos.com/index.php/clanky/globus	k1gMnSc1
</s>
<s>
https://web.archive.org/web/20140220030155/http://www.inuru.com/index.php/planeta/mezniky-vedy/29-martin-behaim-globus-vynalez	https://web.archive.org/web/20140220030155/http://www.inuru.com/index.php/planeta/mezniky-vedy/29-martin-behaim-globus-vynalez	k1gMnSc1
</s>
<s>
https://web.archive.org/web/20140220033244/http://www.hedvabnastezka.cz/expedicni-kamera/7741-martin-behaim-martin-z-cech/	https://web.archive.org/web/20140220033244/http://www.hedvabnastezka.cz/expedicni-kamera/7741-martin-behaim-martin-z-cech/	k4
</s>
