<p>
<s>
Kornamusa	Kornamus	k1gMnSc4	Kornamus
je	být	k5eAaImIp3nS	být
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
dechový	dechový	k2eAgInSc1d1	dechový
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cylindrickým	cylindrický	k2eAgInSc7d1	cylindrický
vývrtem	vývrt	k1gInSc7	vývrt
a	a	k8xC	a
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
plátkem	plátek	k1gInSc7	plátek
(	(	kIx(	(
<g/>
strojkem	strojek	k1gInSc7	strojek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
ve	v	k7c6	v
vzdušnici	vzdušnice	k1gFnSc6	vzdušnice
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
dud	dudy	k1gFnPc2	dudy
<g/>
.	.	kIx.	.
</s>
<s>
Dobu	doba	k1gFnSc4	doba
vzniku	vznik	k1gInSc6	vznik
kornamusy	kornamus	k1gInPc1	kornamus
přesně	přesně	k6eAd1	přesně
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
,	,	kIx,	,
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc2d3	veliký
obliby	obliba	k1gFnSc2	obliba
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
nebyl	být	k5eNaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
jediný	jediný	k2eAgInSc1d1	jediný
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
exemplář	exemplář	k1gInSc1	exemplář
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
volné	volný	k2eAgFnSc2d1	volná
repliky	replika	k1gFnSc2	replika
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
podle	podle	k7c2	podle
popisů	popis	k1gInPc2	popis
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
organologických	organologický	k2eAgInPc6d1	organologický
pramenech	pramen	k1gInPc6	pramen
-	-	kIx~	-
obsáhlejší	obsáhlý	k2eAgFnSc4d2	obsáhlejší
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
nástroji	nástroj	k1gInSc6	nástroj
lze	lze	k6eAd1	lze
například	například	k6eAd1	například
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Syntagma	syntagma	k1gNnSc1	syntagma
Musicum	Musicum	k1gInSc4	Musicum
Michaela	Michael	k1gMnSc2	Michael
Praetoria	Praetorium	k1gNnSc2	Praetorium
(	(	kIx(	(
<g/>
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kornamusa	Kornamus	k1gMnSc4	Kornamus
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
tiché	tichý	k2eAgInPc4d1	tichý
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
barvou	barva	k1gFnSc7	barva
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
krumhornu	krumhorn	k1gInSc2	krumhorn
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
vydechuje	vydechovat	k5eAaImIp3nS	vydechovat
do	do	k7c2	do
štěrbiny	štěrbina	k1gFnSc2	štěrbina
vzdušnice	vzdušnice	k1gFnSc2	vzdušnice
<g/>
,	,	kIx,	,
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
tlak	tlak	k1gInSc1	tlak
ve	v	k7c6	v
vzdušnici	vzdušnice	k1gFnSc6	vzdušnice
rozechvěje	rozechvět	k5eAaPmIp3nS	rozechvět
strojek	strojek	k1gInSc4	strojek
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
potom	potom	k6eAd1	potom
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
je	být	k5eAaImIp3nS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
sedmi	sedm	k4xCc7	sedm
prstovými	prstový	k2eAgFnPc7d1	prstová
dírkami	dírka	k1gFnPc7	dírka
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
palcovou	palcový	k2eAgFnSc4d1	palcová
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgInSc4d1	spodní
konec	konec	k1gInSc4	konec
nástroje	nástroj	k1gInSc2	nástroj
bývá	bývat	k5eAaImIp3nS	bývat
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
jako	jako	k8xC	jako
rodina	rodina	k1gFnSc1	rodina
čtyř	čtyři	k4xCgFnPc2	čtyři
velikostí	velikost	k1gFnPc2	velikost
SATB	SATB	kA	SATB
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
tonový	tonový	k2eAgInSc1d1	tonový
rozsah	rozsah	k1gInSc1	rozsah
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
-	-	kIx~	-
nona	nona	k1gFnSc1	nona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Baines	Baines	k1gInSc1	Baines
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc1	Anthon
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Woodwind	Woodwind	k1gInSc1	Woodwind
Instruments	Instruments	kA	Instruments
and	and	k?	and
Their	Their	k1gInSc1	Their
History	Histor	k1gInPc1	Histor
<g/>
.	.	kIx.	.
</s>
<s>
Dover	Dover	k1gInSc1	Dover
Publications	Publications	k1gInSc1	Publications
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-486-26885-3	[number]	k4	0-486-26885-3
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Číp	Číp	k1gMnSc1	Číp
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
–	–	k?	–
výrobce	výrobce	k1gMnPc4	výrobce
kopií	kopie	k1gFnSc7	kopie
historických	historický	k2eAgInPc2d1	historický
nástrojů	nástroj	k1gInPc2	nástroj
</s>
</p>
<p>
<s>
Musica	Music	k2eAgFnSc1d1	Musica
Antiqua	Antiqu	k2eAgFnSc1d1	Antiqua
Cornamuse	Cornamuse	k1gFnSc1	Cornamuse
Page	Pag	k1gFnSc2	Pag
</s>
</p>
<p>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
Moeck	Moeck	k1gMnSc1	Moeck
<g/>
:	:	kIx,	:
Zur	Zur	k1gMnSc5	Zur
Geschichte	Geschicht	k1gMnSc5	Geschicht
von	von	k1gInSc1	von
Krummhorn	Krummhorn	k1gMnSc1	Krummhorn
und	und	k?	und
Cornamuse	Cornamuse	k1gFnSc2	Cornamuse
</s>
</p>
