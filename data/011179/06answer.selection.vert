<s>
Kornamusa	Kornamus	k1gMnSc4	Kornamus
je	být	k5eAaImIp3nS	být
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
dechový	dechový	k2eAgInSc1d1	dechový
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
cylindrickým	cylindrický	k2eAgInSc7d1	cylindrický
vývrtem	vývrt	k1gInSc7	vývrt
a	a	k8xC	a
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
plátkem	plátek	k1gInSc7	plátek
(	(	kIx(	(
<g/>
strojkem	strojek	k1gInSc7	strojek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
ve	v	k7c6	v
vzdušnici	vzdušnice	k1gFnSc6	vzdušnice
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
dud	dudy	k1gFnPc2	dudy
<g/>
.	.	kIx.	.
</s>
