<s>
Americium	americium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Am	Am	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sedmým	sedmý	k4xOgInSc7
členem	člen	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
třetím	třetí	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
především	především	k9
z	z	k7c2
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>