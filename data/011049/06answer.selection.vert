<s>
Dillíský	dillíský	k2eAgInSc1d1	dillíský
sultanát	sultanát	k1gInSc1	sultanát
(	(	kIx(	(
<g/>
persky	persky	k6eAd1	persky
د	د	k?	د
س	س	k?	س
<g/>
,	,	kIx,	,
urdsky	urdsky	k6eAd1	urdsky
د	د	k?	د
س	س	k?	س
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
vládly	vládnout	k5eAaImAgFnP	vládnout
muslimské	muslimský	k2eAgFnPc4d1	muslimská
dynastie	dynastie	k1gFnPc4	dynastie
<g/>
.	.	kIx.	.
</s>
