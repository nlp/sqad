<p>
<s>
Dillíský	dillíský	k2eAgInSc1d1	dillíský
sultanát	sultanát	k1gInSc1	sultanát
(	(	kIx(	(
<g/>
persky	persky	k6eAd1	persky
د	د	k?	د
س	س	k?	س
<g/>
,	,	kIx,	,
urdsky	urdsky	k6eAd1	urdsky
د	د	k?	د
س	س	k?	س
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vládly	vládnout	k5eAaImAgFnP	vládnout
muslimské	muslimský	k2eAgFnPc4d1	muslimská
dynastie	dynastie	k1gFnPc4	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
datace	datace	k1gFnSc1	datace
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1206	[number]	k4	1206
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
tureckých	turecký	k2eAgMnPc2d1	turecký
a	a	k8xC	a
paštunských	paštunský	k2eAgFnPc2d1	paštunský
dynastií	dynastie	k1gFnPc2	dynastie
totiž	totiž	k9	totiž
vládlo	vládnout	k5eAaImAgNnS	vládnout
z	z	k7c2	z
Dillí	Dillí	k1gNnSc2	Dillí
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
dynastii	dynastie	k1gFnSc6	dynastie
Muizzí	Muizze	k1gFnPc2	Muizze
(	(	kIx(	(
<g/>
1206	[number]	k4	1206
<g/>
-	-	kIx~	-
<g/>
1290	[number]	k4	1290
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chaldží	Chaldží	k1gNnSc1	Chaldží
(	(	kIx(	(
<g/>
1290	[number]	k4	1290
<g/>
-	-	kIx~	-
<g/>
1320	[number]	k4	1320
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tughlakovců	Tughlakovec	k1gInPc2	Tughlakovec
(	(	kIx(	(
<g/>
1320	[number]	k4	1320
<g/>
-	-	kIx~	-
<g/>
1413	[number]	k4	1413
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sajjdovců	Sajjdovec	k1gInPc2	Sajjdovec
(	(	kIx(	(
<g/>
1414	[number]	k4	1414
<g/>
-	-	kIx~	-
<g/>
1451	[number]	k4	1451
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lódíů	Lódí	k1gMnPc2	Lódí
(	(	kIx(	(
<g/>
Paštunové	Paštun	k1gMnPc1	Paštun
<g/>
,	,	kIx,	,
1451	[number]	k4	1451
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
byl	být	k5eAaImAgInS	být
Dillský	Dillský	k2eAgInSc1d1	Dillský
sultanát	sultanát	k1gInSc1	sultanát
začleněn	začleněn	k2eAgInSc1d1	začleněn
do	do	k7c2	do
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozmáhající	rozmáhající	k2eAgFnPc1d1	rozmáhající
Mughalské	Mughalský	k2eAgFnPc1d1	Mughalská
říše	říš	k1gFnPc1	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
Dillíského	dillíský	k2eAgInSc2d1	dillíský
sultanátu	sultanát	k1gInSc2	sultanát
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Indoganžské	Indoganžský	k2eAgFnSc2d1	Indoganžská
nížiny	nížina	k1gFnSc2	nížina
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
Muhammad	Muhammad	k1gInSc4	Muhammad
z	z	k7c2	z
Ghóru	Ghór	k1gInSc2	Ghór
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
podrobit	podrobit	k5eAaPmF	podrobit
si	se	k3xPyFc3	se
Ghaznu	Ghazna	k1gFnSc4	Ghazna
<g/>
,	,	kIx,	,
Multán	Multán	k1gMnSc1	Multán
<g/>
,	,	kIx,	,
Láhaur	Láhaur	k1gInSc1	Láhaur
<g/>
,	,	kIx,	,
Sind	Sind	k1gInSc1	Sind
i	i	k8xC	i
Dillí	Dillí	k1gNnSc1	Dillí
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
Kutbuddín	Kutbuddín	k1gMnSc1	Kutbuddín
Ajbak	Ajbak	k1gMnSc1	Ajbak
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Dillíským	dillíský	k2eAgMnSc7d1	dillíský
sultánem	sultán	k1gMnSc7	sultán
a	a	k8xC	a
položil	položit	k5eAaPmAgInS	položit
tak	tak	k9	tak
základ	základ	k1gInSc1	základ
první	první	k4xOgInSc1	první
<g/>
,	,	kIx,	,
mamlúcké	mamlúcký	k2eAgFnSc6d1	mamlúcká
dynastii	dynastie	k1gFnSc4	dynastie
Dilliského	Dilliský	k2eAgInSc2d1	Dilliský
sultanátu	sultanát	k1gInSc2	sultanát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
otrocká	otrocký	k2eAgFnSc1d1	otrocká
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Dillískému	dillíský	k2eAgInSc3d1	dillíský
sultanátu	sultanát	k1gInSc3	sultanát
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
od	od	k7c2	od
Chajbarského	Chajbarský	k2eAgInSc2d1	Chajbarský
průsmyku	průsmyk	k1gInSc2	průsmyk
po	po	k7c4	po
Bengálsko	Bengálsko	k1gNnSc4	Bengálsko
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Muizzí	Muizze	k1gFnPc2	Muizze
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1290	[number]	k4	1290
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
dillískou	dillíský	k2eAgFnSc7d1	dillíský
dynastií	dynastie	k1gFnSc7	dynastie
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Chaldžíovci	Chaldžíovec	k1gMnPc1	Chaldžíovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Muhammada	Muhammada	k1gFnSc1	Muhammada
z	z	k7c2	z
Ghóru	Ghór	k1gInSc2	Ghór
stali	stát	k5eAaPmAgMnP	stát
vládci	vládce	k1gMnPc1	vládce
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
iniciovat	iniciovat	k5eAaBmF	iniciovat
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
a	a	k8xC	a
eliminovat	eliminovat	k5eAaBmF	eliminovat
všechny	všechen	k3xTgInPc4	všechen
mamlúky	mamlúk	k1gInPc4	mamlúk
<g/>
.	.	kIx.	.
</s>
<s>
Chaldžíovci	Chaldžíovec	k1gMnPc1	Chaldžíovec
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Gudžarát	Gudžarát	k1gInSc4	Gudžarát
i	i	k8xC	i
Málvu	Málva	k1gFnSc4	Málva
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
rozšířit	rozšířit	k5eAaPmF	rozšířit
sultanát	sultanát	k1gInSc4	sultanát
i	i	k9	i
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Indického	indický	k2eAgInSc2d1	indický
subkontinentu	subkontinent	k1gInSc2	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc4	jejich
výboje	výboj	k1gInPc4	výboj
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
zastavil	zastavit	k5eAaPmAgMnS	zastavit
až	až	k9	až
Vidžajanagar	Vidžajanagar	k1gMnSc1	Vidžajanagar
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
i	i	k8xC	i
dalším	další	k2eAgFnPc3d1	další
dynastiím	dynastie	k1gFnPc3	dynastie
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vybudovat	vybudovat	k5eAaPmF	vybudovat
silnou	silný	k2eAgFnSc4d1	silná
říši	říše	k1gFnSc4	říše
se	s	k7c7	s
stabilní	stabilní	k2eAgFnSc7d1	stabilní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
sítí	síť	k1gFnSc7	síť
obchodních	obchodní	k2eAgFnPc2d1	obchodní
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
zabírala	zabírat	k5eAaImAgFnS	zabírat
takřka	takřka	k6eAd1	takřka
celý	celý	k2eAgInSc4d1	celý
Indický	indický	k2eAgInSc4d1	indický
subkontinent	subkontinent	k1gInSc4	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
Dillíského	dillíský	k2eAgInSc2d1	dillíský
sultanátu	sultanát	k1gInSc2	sultanát
přivodil	přivodit	k5eAaBmAgMnS	přivodit
až	až	k6eAd1	až
Bábur	Bábur	k1gMnSc1	Bábur
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
vládce	vládce	k1gMnSc1	vládce
Mughalů	Mughal	k1gInPc2	Mughal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
dillíských	dillíský	k2eAgMnPc2d1	dillíský
sultánů	sultán	k1gMnPc2	sultán
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mamlúcká	mamlúcký	k2eAgFnSc1d1	mamlúcká
dynastie	dynastie	k1gFnSc1	dynastie
(	(	kIx(	(
<g/>
1206	[number]	k4	1206
<g/>
-	-	kIx~	-
<g/>
1290	[number]	k4	1290
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Kutbuddín	Kutbuddín	k1gMnSc1	Kutbuddín
Ajbak	Ajbak	k1gMnSc1	Ajbak
(	(	kIx(	(
<g/>
1206	[number]	k4	1206
-	-	kIx~	-
1210	[number]	k4	1210
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aram	Aram	k1gMnSc1	Aram
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1210	[number]	k4	1210
-	-	kIx~	-
1211	[number]	k4	1211
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šamsuddín	Šamsuddín	k1gMnSc1	Šamsuddín
Iltutmiš	Iltutmiš	k1gMnSc1	Iltutmiš
(	(	kIx(	(
<g/>
1211	[number]	k4	1211
-	-	kIx~	-
1236	[number]	k4	1236
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ruknuddín	Ruknuddín	k1gMnSc1	Ruknuddín
Fírúz	Fírúz	k1gMnSc1	Fírúz
(	(	kIx(	(
<g/>
1236	[number]	k4	1236
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Razia	Razia	k1gFnSc1	Razia
ad-Dín	ad-Dína	k1gFnPc2	ad-Dína
(	(	kIx(	(
<g/>
1236	[number]	k4	1236
-	-	kIx~	-
1240	[number]	k4	1240
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Muiz	Muiz	k1gInSc1	Muiz
ud	ud	k?	ud
din	din	k1gInSc1	din
Bahram	Bahram	k1gInSc1	Bahram
(	(	kIx(	(
<g/>
1240	[number]	k4	1240
-	-	kIx~	-
1242	[number]	k4	1242
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aláuddín	Aláuddín	k1gMnSc1	Aláuddín
Masúd	Masúd	k1gMnSc1	Masúd
(	(	kIx(	(
<g/>
1242	[number]	k4	1242
-	-	kIx~	-
1246	[number]	k4	1246
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Násiruddín	Násiruddín	k1gMnSc1	Násiruddín
Mahmúd	Mahmúd	k1gMnSc1	Mahmúd
(	(	kIx(	(
<g/>
1246	[number]	k4	1246
-	-	kIx~	-
1266	[number]	k4	1266
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ghijásuddín	Ghijásuddín	k1gMnSc1	Ghijásuddín
Balban	Balban	k1gMnSc1	Balban
(	(	kIx(	(
<g/>
1266	[number]	k4	1266
-	-	kIx~	-
1286	[number]	k4	1286
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Muizuddín	Muizuddín	k1gInSc1	Muizuddín
Kajkubád	Kajkubáda	k1gFnPc2	Kajkubáda
(	(	kIx(	(
<g/>
1286	[number]	k4	1286
-	-	kIx~	-
1290	[number]	k4	1290
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kajúmars	Kajúmars	k1gInSc1	Kajúmars
(	(	kIx(	(
<g/>
1290	[number]	k4	1290
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dynastie	dynastie	k1gFnSc1	dynastie
Chaldží	Chaldž	k1gFnPc2	Chaldž
(	(	kIx(	(
<g/>
1290	[number]	k4	1290
<g/>
-	-	kIx~	-
<g/>
1320	[number]	k4	1320
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Džaláluddín	Džaláluddín	k1gMnSc1	Džaláluddín
Fírúz	Fírúz	k1gMnSc1	Fírúz
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1290	[number]	k4	1290
-	-	kIx~	-
1294	[number]	k4	1294
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aláuddín	Aláuddín	k1gMnSc1	Aláuddín
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1294	[number]	k4	1294
-	-	kIx~	-
1316	[number]	k4	1316
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kutbuddín	Kutbuddín	k1gMnSc1	Kutbuddín
Mubárak	Mubárak	k1gMnSc1	Mubárak
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1316	[number]	k4	1316
-	-	kIx~	-
1321	[number]	k4	1321
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dynastie	dynastie	k1gFnSc1	dynastie
Tughlakovců	Tughlakovec	k1gInPc2	Tughlakovec
(	(	kIx(	(
<g/>
1320	[number]	k4	1320
<g/>
-	-	kIx~	-
<g/>
1413	[number]	k4	1413
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Ghijásuddín	Ghijásuddín	k1gMnSc1	Ghijásuddín
Tughlak	Tughlak	k1gMnSc1	Tughlak
Šáh	šáh	k1gMnSc1	šáh
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1321	[number]	k4	1321
-	-	kIx~	-
1325	[number]	k4	1325
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ghijásuddín	Ghijásuddín	k1gInSc1	Ghijásuddín
Muhammad	Muhammad	k1gInSc1	Muhammad
Šáh	šáh	k1gMnSc1	šáh
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1325	[number]	k4	1325
-	-	kIx~	-
1351	[number]	k4	1351
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mahmúd	Mahmúd	k1gInSc1	Mahmúd
ibn	ibn	k?	ibn
Muhammad	Muhammad	k1gInSc1	Muhammad
(	(	kIx(	(
March	March	k1gInSc1	March
1351	[number]	k4	1351
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Firúz	Firúz	k1gMnSc1	Firúz
Šáh	šáh	k1gMnSc1	šáh
Tughlak	Tughlak	k1gMnSc1	Tughlak
(	(	kIx(	(
<g/>
1351	[number]	k4	1351
-	-	kIx~	-
1388	[number]	k4	1388
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ghijásuddín	Ghijásuddín	k1gMnSc1	Ghijásuddín
Tughlak	Tughlak	k1gMnSc1	Tughlak
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1388	[number]	k4	1388
-	-	kIx~	-
1389	[number]	k4	1389
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Abú	abú	k1gMnSc1	abú
Bakr	Bakr	k1gMnSc1	Bakr
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1389	[number]	k4	1389
-	-	kIx~	-
1390	[number]	k4	1390
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Násiruddín	Násiruddín	k1gInSc1	Násiruddín
Muhammad	Muhammad	k1gInSc1	Muhammad
Šáh	šáh	k1gMnSc1	šáh
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1390	[number]	k4	1390
-	-	kIx~	-
1393	[number]	k4	1393
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sikandar	Sikandar	k1gMnSc1	Sikandar
Šáh	šáh	k1gMnSc1	šáh
I.	I.	kA	I.
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
-	-	kIx~	-
duben	duben	k1gInSc1	duben
1393	[number]	k4	1393
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mahmúd	Mahmúd	k1gMnSc1	Mahmúd
Násiruddín	Násiruddín	k1gMnSc1	Násiruddín
(	(	kIx(	(
<g/>
Sultán	sultán	k1gMnSc1	sultán
Mahmúd	Mahmúd	k1gMnSc1	Mahmúd
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1393	[number]	k4	1393
-	-	kIx~	-
1394	[number]	k4	1394
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nusrat	Nusrat	k1gMnSc1	Nusrat
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1394	[number]	k4	1394
-	-	kIx~	-
1398	[number]	k4	1398
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dynastie	dynastie	k1gFnSc1	dynastie
Sajjdovců	Sajjdovec	k1gInPc2	Sajjdovec
(	(	kIx(	(
<g/>
1414	[number]	k4	1414
<g/>
-	-	kIx~	-
<g/>
1451	[number]	k4	1451
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Chizr	Chizr	k1gMnSc1	Chizr
Chán	chán	k1gMnSc1	chán
(	(	kIx(	(
<g/>
1414	[number]	k4	1414
-	-	kIx~	-
1421	[number]	k4	1421
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mubárak	Mubárak	k1gMnSc1	Mubárak
Šáh	šáh	k1gMnSc1	šáh
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1421	[number]	k4	1421
-	-	kIx~	-
1435	[number]	k4	1435
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Muhammad	Muhammad	k6eAd1	Muhammad
Šáh	šáh	k1gMnSc1	šáh
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1435	[number]	k4	1435
-	-	kIx~	-
1445	[number]	k4	1445
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aláuddín	Aláuddín	k1gMnSc1	Aláuddín
Álam	Álam	k1gMnSc1	Álam
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1445	[number]	k4	1445
-	-	kIx~	-
1451	[number]	k4	1451
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dynastie	dynastie	k1gFnSc1	dynastie
Lódí	Lódí	k1gMnSc1	Lódí
(	(	kIx(	(
<g/>
1451	[number]	k4	1451
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Bahlúl	Bahlúl	k1gMnSc1	Bahlúl
Chán	chán	k1gMnSc1	chán
Lódí	Lódí	k1gMnSc1	Lódí
(	(	kIx(	(
<g/>
1451	[number]	k4	1451
<g/>
-	-	kIx~	-
<g/>
1489	[number]	k4	1489
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sikandar	Sikandar	k1gMnSc1	Sikandar
lódí	lódí	k1gMnSc1	lódí
(	(	kIx(	(
<g/>
1489	[number]	k4	1489
<g/>
-	-	kIx~	-
<g/>
1517	[number]	k4	1517
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ibráhím	Ibráhet	k5eAaImIp1nS	Ibráhet
Lódí	Lódí	k1gFnSc1	Lódí
(	(	kIx(	(
<g/>
1517	[number]	k4	1517
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
1526	[number]	k4	1526
<g/>
-	-	kIx~	-
<g/>
1540	[number]	k4	1540
-	-	kIx~	-
dynastie	dynastie	k1gFnSc1	dynastie
Mughalů	Mughal	k1gInPc2	Mughal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dynastie	dynastie	k1gFnSc1	dynastie
Súrú	Súrú	k1gMnSc1	Súrú
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
<g/>
-	-	kIx~	-
<g/>
1555	[number]	k4	1555
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Šér	Šér	k?	Šér
Šáh	šáh	k1gMnSc1	šáh
Súrí	Súrí	k1gMnSc1	Súrí
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
-	-	kIx~	-
1545	[number]	k4	1545
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Islám	islám	k1gInSc1	islám
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
-	-	kIx~	-
1553	[number]	k4	1553
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Muhammad	Muhammad	k6eAd1	Muhammad
Ádil	Ádil	k1gMnSc1	Ádil
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1553	[number]	k4	1553
-	-	kIx~	-
1554	[number]	k4	1554
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Firúz	Firúz	k1gMnSc1	Firúz
Dillíský	dillíský	k2eAgMnSc1d1	dillíský
(	(	kIx(	(
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1554	[number]	k4	1554
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ibráhim	Ibráhim	k1gMnSc1	Ibráhim
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1554	[number]	k4	1554
-	-	kIx~	-
1554	[number]	k4	1554
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sikandar	Sikandar	k1gMnSc1	Sikandar
Šáh	šáh	k1gMnSc1	šáh
(	(	kIx(	(
<g/>
1554	[number]	k4	1554
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
-	-	kIx~	-
1555	[number]	k4	1555
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Delhi	Delh	k1gFnSc2	Delh
Sultanate	Sultanat	k1gInSc5	Sultanat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Fernand	Fernand	k1gInSc1	Fernand
Braudel	Braudlo	k1gNnPc2	Braudlo
The	The	k1gFnSc2	The
Perspective	Perspectiv	k1gInSc5	Perspectiv
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
of	of	k?	of
Civilization	Civilization	k1gInSc1	Civilization
and	and	k?	and
Capitalism	Capitalism	k1gInSc1	Capitalism
(	(	kIx(	(
<g/>
Harper	Harper	k1gInSc1	Harper
&	&	k?	&
Row	Row	k1gFnSc2	Row
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Jackson	Jackson	k1gMnSc1	Jackson
The	The	k1gMnSc1	The
Delhi	Delh	k1gMnSc5	Delh
Sultanate	Sultanat	k1gMnSc5	Sultanat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Political	Political	k1gFnSc1	Political
and	and	k?	and
Military	Militara	k1gFnSc2	Militara
History	Histor	k1gInPc1	Histor
(	(	kIx(	(
<g/>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Majumdar	Majumdar	k1gMnSc1	Majumdar
<g/>
,	,	kIx,	,
R.	R.	kA	R.
C.	C.	kA	C.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
and	and	k?	and
Culture	Cultur	k1gMnSc5	Cultur
of	of	k?	of
the	the	k?	the
Indian	Indiana	k1gFnPc2	Indiana
People	People	k1gFnSc1	People
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
VI	VI	kA	VI
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Delhi	Delh	k1gMnSc5	Delh
Sultanate	Sultanat	k1gMnSc5	Sultanat
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Bombay	Bombaa	k1gFnPc1	Bombaa
<g/>
)	)	kIx)	)
1960	[number]	k4	1960
<g/>
;	;	kIx,	;
Volume	volum	k1gInSc5	volum
VII	VII	kA	VII
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Mughal	Mughal	k1gMnSc1	Mughal
Empire	empir	k1gInSc5	empir
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Bombay	Bombaa	k1gFnPc1	Bombaa
<g/>
)	)	kIx)	)
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nizami	Niza	k1gFnPc7	Niza
<g/>
,	,	kIx,	,
Khaliq	Khaliq	k1gFnSc4	Khaliq
Ahmad	Ahmad	k1gInSc4	Ahmad
Some	Som	k1gInSc2	Som
Aspects	Aspects	k1gInSc1	Aspects
of	of	k?	of
Religion	religion	k1gInSc1	religion
and	and	k?	and
Politics	Politics	k1gInSc1	Politics
in	in	k?	in
India	indium	k1gNnSc2	indium
in	in	k?	in
the	the	k?	the
Thirteenth	Thirteenth	k1gInSc4	Thirteenth
Century	Centura	k1gFnSc2	Centura
(	(	kIx(	(
<g/>
Delhi	Delh	k1gFnSc2	Delh
<g/>
)	)	kIx)	)
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
Revised	Revised	k1gInSc1	Revised
Edition	Edition	k1gInSc1	Edition
Delhi	Delh	k1gFnSc2	Delh
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Memoir	Memoir	k1gInSc1	Memoir
of	of	k?	of
the	the	k?	the
Emperor	Emperor	k1gMnSc1	Emperor
Timur	Timur	k1gMnSc1	Timur
(	(	kIx(	(
<g/>
Malfuzat-i	Malfuzat	k1gNnSc1	Malfuzat-i
Timuri	Timur	k1gFnSc2	Timur
<g/>
)	)	kIx)	)
Timur	Timur	k1gMnSc1	Timur
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
memoirs	memoirs	k1gInSc1	memoirs
on	on	k3xPp3gMnSc1	on
his	his	k1gNnSc7	his
invasion	invasion	k1gInSc1	invasion
of	of	k?	of
India	indium	k1gNnSc2	indium
<g/>
.	.	kIx.	.
</s>
<s>
Compiled	Compiled	k1gInSc1	Compiled
in	in	k?	in
the	the	k?	the
book	book	k1gInSc1	book
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
India	indium	k1gNnSc2	indium
<g/>
,	,	kIx,	,
as	as	k9	as
Told	Told	k1gInSc4	Told
by	by	kYmCp3nS	by
Its	Its	k1gFnSc1	Its
Own	Own	k1gFnSc2	Own
Historians	Historiansa	k1gFnPc2	Historiansa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Muhammadan	Muhammadan	k1gInSc1	Muhammadan
Period	perioda	k1gFnPc2	perioda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Sir	sir	k1gMnSc1	sir
H.	H.	kA	H.
M.	M.	kA	M.
Elliot	Elliot	k1gInSc4	Elliot
<g/>
,	,	kIx,	,
Edited	Edited	k1gInSc4	Edited
by	by	kYmCp3nS	by
John	John	k1gMnSc1	John
Dowson	Dowson	k1gMnSc1	Dowson
<g/>
;	;	kIx,	;
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Trubner	Trubner	k1gMnSc1	Trubner
Company	Compana	k1gFnSc2	Compana
<g/>
;	;	kIx,	;
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1877	[number]	k4	1877
</s>
</p>
<p>
<s>
Dietmar	Dietmar	k1gMnSc1	Dietmar
Rothermund	Rothermund	k1gMnSc1	Rothermund
<g/>
,	,	kIx,	,
Geschichte	Geschicht	k1gInSc5	Geschicht
Indiens	Indiens	k1gInSc4	Indiens
Vom	Vom	k1gFnSc3	Vom
Mittelalter	Mittelalter	k1gInSc1	Mittelalter
bis	bis	k?	bis
zur	zur	k?	zur
Gegenwart	Gegenwart	k1gInSc1	Gegenwart
<g/>
,	,	kIx,	,
C.H.	C.H.	k1gMnSc1	C.H.
Beck	Beck	k1gMnSc1	Beck
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elliot	Elliot	k1gMnSc1	Elliot
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
H.	H.	kA	H.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Edited	Edited	k1gInSc4	Edited
by	by	kYmCp3nS	by
Dowson	Dowson	k1gMnSc1	Dowson
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
India	indium	k1gNnSc2	indium
<g/>
,	,	kIx,	,
as	as	k9	as
Told	Told	k1gInSc4	Told
by	by	kYmCp3nS	by
Its	Its	k1gFnSc1	Its
Own	Own	k1gFnSc2	Own
Historians	Historiansa	k1gFnPc2	Historiansa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Muhammadan	Muhammadan	k1gInSc1	Muhammadan
Period	perioda	k1gFnPc2	perioda
<g/>
;	;	kIx,	;
published	published	k1gInSc4	published
by	by	kYmCp3nS	by
London	London	k1gMnSc1	London
Trubner	Trubner	k1gMnSc1	Trubner
Company	Compana	k1gFnSc2	Compana
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Online	Onlin	k1gMnSc5	Onlin
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
India	indium	k1gNnSc2	indium
<g/>
,	,	kIx,	,
as	as	k9	as
Told	Told	k1gInSc4	Told
by	by	kYmCp3nS	by
Its	Its	k1gFnSc1	Its
Own	Own	k1gFnSc2	Own
Historians	Historiansa	k1gFnPc2	Historiansa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Muhammadan	Muhammadan	k1gInSc1	Muhammadan
Period	perioda	k1gFnPc2	perioda
<g/>
;	;	kIx,	;
by	by	k9	by
Sir	sir	k1gMnSc1	sir
H.	H.	kA	H.
M.	M.	kA	M.
Elliot	Elliot	k1gMnSc1	Elliot
<g/>
;	;	kIx,	;
Edited	Edited	k1gMnSc1	Edited
by	by	kYmCp3nS	by
John	John	k1gMnSc1	John
Dowson	Dowson	k1gMnSc1	Dowson
<g/>
;	;	kIx,	;
London	London	k1gMnSc1	London
Trubner	Trubner	k1gMnSc1	Trubner
Company	Compana	k1gFnSc2	Compana
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1877	[number]	k4	1877
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dillíský	dillíský	k2eAgInSc4d1	dillíský
sultanát	sultanát	k1gInSc4	sultanát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Dillíský	dillíský	k2eAgInSc1d1	dillíský
sultanát	sultanát	k1gInSc1	sultanát
</s>
</p>
