<p>
<s>
Luis	Luisa	k1gFnPc2	Luisa
Federico	Federico	k1gMnSc1	Federico
Leloir	Leloir	k1gMnSc1	Leloir
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1906	[number]	k4	1906
Paříž	Paříž	k1gFnSc1	Paříž
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1987	[number]	k4	1987
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
argentinský	argentinský	k2eAgMnSc1d1	argentinský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Oceněn	oceněn	k2eAgInSc1d1	oceněn
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
objev	objev	k1gInSc4	objev
aktivačních	aktivační	k2eAgInPc2d1	aktivační
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
biosyntéze	biosyntéza	k1gFnSc6	biosyntéza
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
lékařství	lékařství	k1gNnSc4	lékařství
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
prováděl	provádět	k5eAaImAgMnS	provádět
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
nedostatečně	dostatečně	k6eNd1	dostatečně
financované	financovaný	k2eAgFnPc1d1	financovaná
soukromé	soukromý	k2eAgFnPc1d1	soukromá
výzkumné	výzkumný	k2eAgFnPc1d1	výzkumná
laboratoře	laboratoř	k1gFnPc1	laboratoř
Fundación	Fundación	k1gInSc1	Fundación
Instituto	Institut	k2eAgNnSc1d1	Instituto
Campomar	Campomara	k1gFnPc2	Campomara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Luis	Luisa	k1gFnPc2	Luisa
Federico	Federico	k6eAd1	Federico
Leloir	Leloir	k1gInSc4	Leloir
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Luis	Luisa	k1gFnPc2	Luisa
Federico	Federico	k1gMnSc1	Federico
Leloir	Leloir	k1gMnSc1	Leloir
na	na	k7c6	na
Historii	historie	k1gFnSc6	historie
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
