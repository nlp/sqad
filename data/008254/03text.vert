<p>
<s>
Guangzhou	Guangzha	k1gFnSc7	Guangzha
International	International	k1gFnSc2	International
Finance	finance	k1gFnSc2	finance
Center	centrum	k1gNnPc2	centrum
je	být	k5eAaImIp3nS	být
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
v	v	k7c6	v
městě	město	k1gNnSc6	město
Kanton	Kanton	k1gInSc1	Kanton
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
438	[number]	k4	438
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
103	[number]	k4	103
poschodí	poschodí	k1gNnPc2	poschodí
<g/>
.	.	kIx.	.
</s>
<s>
Dokončený	dokončený	k2eAgMnSc1d1	dokončený
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
třináctá	třináctý	k4xOgFnSc1	třináctý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
světa	svět	k1gInSc2	svět
</s>
</p>
