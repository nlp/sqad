<s>
Dante	Dante	k1gMnSc1
Alighieri	Alighieri		k1gMnSc1
</s>
<s>
Dante	Dante	k1gMnSc1
Alighieri	Alighier	k1gFnSc2
Giotto	Giotto	k1gNnSc1
di	di	k?
Bondone	Bondon	k1gMnSc5
<g/>
,	,	kIx,
Dante	Dante	k1gMnSc5
Alighieri	Alighier	k1gMnSc5
(	(	kIx(
<g/>
kolem	kolem	k7c2
1320	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
květen	květen	k1gInSc1
1265	#num#	k4
Florencie	Florencie	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1321	#num#	k4
Ravenna	Ravenen	k2eAgFnSc1d1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
malárie	malárie	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Dante	Dante	k1gMnSc1
Alighieri	Alighier	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
tomb	tomb	k1gInSc1
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Období	období	k1gNnSc2
</s>
<s>
1291	#num#	k4
<g/>
–	–	k?
<g/>
1320	#num#	k4
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Božská	božská	k1gFnSc1
komedieNový	komedieNový	k2eAgInSc1d1
život	život	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Gemma	Gemma	k1gFnSc1
Donati	Donat	k2eAgMnPc1d1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Alighiero	Alighiero	k1gNnSc1
di	di	k?
Bellincione	Bellincion	k1gInSc5
a	a	k8xC
Bella	Bella	k1gMnSc1
degli	degle	k1gFnSc4
Abati	Abať	k1gFnSc2
Vliv	vliv	k1gInSc1
na	na	k7c6
</s>
<s>
Jan	Jan	k1gMnSc1
Amos	Amos	k1gMnSc1
KomenskýJohn	KomenskýJohn	k1gMnSc1
Milton	Milton	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Plné	plný	k2eAgInPc4d1
texty	text	k1gInPc4
děl	dělo	k1gNnPc2
na	na	k7c6
Projektu	projekt	k1gInSc6
Gutenberg	Gutenberg	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sandro	Sandra	k1gFnSc5
Botticelli	Botticell	k1gFnSc5
<g/>
:	:	kIx,
Dante	Dante	k1gMnSc5
Alighieri	Alighier	k1gMnSc5
</s>
<s>
Dante	Dante	k1gMnSc1
Alighieri	Alighieri	k1gNnSc2
(	(	kIx(
<g/>
Galeria	Galerium	k1gNnSc2
degli	degl	k1gMnPc1
Uffizie	Uffizie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Dante	Dante	k1gMnSc1
Alighieri	Alighier	k1gFnSc2
(	(	kIx(
<g/>
patrně	patrně	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
polovina	polovina	k1gFnSc1
května	květen	k1gInSc2
1265	#num#	k4
Florencie	Florencie	k1gFnSc2
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1321	#num#	k4
Ravenna	Ravenno	k1gNnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
italských	italský	k2eAgMnPc2d1
básníků	básník	k1gMnPc2
<g/>
,	,	kIx,
významně	významně	k6eAd1
však	však	k9
přispěl	přispět	k5eAaPmAgInS
také	také	k9
k	k	k7c3
vývoji	vývoj	k1gInSc3
jazykovědy	jazykověda	k1gFnSc2
a	a	k8xC
italského	italský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
k	k	k7c3
vývoji	vývoj	k1gInSc3
politické	politický	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc7
největším	veliký	k2eAgNnSc7d3
dílem	dílo	k1gNnSc7
je	být	k5eAaImIp3nS
Božská	božský	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
(	(	kIx(
<g/>
původním	původní	k2eAgInSc7d1
názvem	název	k1gInSc7
jen	jen	k6eAd1
Comedia	Comedium	k1gNnSc2
–	–	k?
přívlastek	přívlastek	k1gInSc1
„	„	k?
<g/>
božská	božská	k1gFnSc1
<g/>
“	“	k?
přidal	přidat	k5eAaPmAgMnS
později	pozdě	k6eAd2
Boccaccio	Boccaccio	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
dějiny	dějiny	k1gFnPc4
politické	politický	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
má	mít	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
latinský	latinský	k2eAgInSc1d1
spis	spis	k1gInSc1
De	De	k?
monarchia	monarchia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dante	Dante	k1gMnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
považován	považován	k2eAgMnSc1d1
za	za	k7c4
otce	otec	k1gMnSc4
spisovné	spisovný	k2eAgFnSc2d1
italštiny	italština	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
il	il	k?
padre	padr	k1gInSc5
della	delt	k5eAaBmAgFnS,k5eAaImAgFnS,k5eAaPmAgFnS
lingua	lingu	k2eAgMnSc4d1
italiana	italian	k1gMnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
svými	svůj	k3xOyFgMnPc7
díly	dílo	k1gNnPc7
prosadil	prosadit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
přístup	přístup	k1gInSc4
teoreticky	teoreticky	k6eAd1
zdůvodnil	zdůvodnit	k5eAaPmAgMnS
v	v	k7c6
díle	díl	k1gInSc6
De	De	k?
vulgari	vulgar	k1gFnSc2
eloquentia	eloquentium	k1gNnSc2
(	(	kIx(
<g/>
O	o	k7c6
řeči	řeč	k1gFnSc6
lidové	lidový	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
předchůdce	předchůdce	k1gMnSc4
renesance	renesance	k1gFnSc2
a	a	k8xC
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
představitelů	představitel	k1gMnPc2
světové	světový	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
politika	politika	k1gFnSc1
vyhnance	vyhnanec	k1gMnSc2
</s>
<s>
Mládí	mládí	k1gNnSc1
a	a	k8xC
vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
Dante	Dante	k1gMnSc1
navštěvoval	navštěvovat	k5eAaImAgMnS
nejdříve	dříve	k6eAd3
pravděpodobně	pravděpodobně	k6eAd1
jednu	jeden	k4xCgFnSc4
z	z	k7c2
řádových	řádový	k2eAgFnPc2d1
škol	škola	k1gFnPc2
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
(	(	kIx(
<g/>
dominikánskou	dominikánský	k2eAgFnSc7d1
u	u	k7c2
Santa	Santo	k1gNnSc2
Maria	Mario	k1gMnSc2
Novella	Novell	k1gMnSc2
nebo	nebo	k8xC
františkánskou	františkánský	k2eAgFnSc7d1
u	u	k7c2
Santa	Santo	k1gNnSc2
Croce	Croce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1285	#num#	k4
<g/>
–	–	k?
<g/>
1287	#num#	k4
pobýval	pobývat	k5eAaImAgMnS
na	na	k7c6
tehdy	tehdy	k6eAd1
nejslavnější	slavný	k2eAgFnSc6d3
a	a	k8xC
nejstarobylejší	starobylý	k2eAgFnSc6d3
evropské	evropský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Bologni	Bologna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neskládal	skládat	k5eNaImAgMnS
zde	zde	k6eAd1
však	však	k9
žádné	žádný	k3yNgFnPc4
zkoušky	zkouška	k1gFnPc4
<g/>
,	,	kIx,
nezískal	získat	k5eNaPmAgMnS
žádný	žádný	k3yNgInSc4
univerzitní	univerzitní	k2eAgInSc4d1
gradus	gradus	k1gInSc4
a	a	k8xC
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
příliš	příliš	k6eAd1
nenavštěvoval	navštěvovat	k5eNaImAgMnS
univerzitní	univerzitní	k2eAgFnPc4d1
přednášky	přednáška	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tehdejší	tehdejší	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
důkladné	důkladný	k2eAgNnSc4d1
filozofické	filozofický	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
získal	získat	k5eAaPmAgInS
později	pozdě	k6eAd2
až	až	k9
jako	jako	k9
čtyřicetiletý	čtyřicetiletý	k2eAgMnSc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
již	již	k6eAd1
jako	jako	k8xS,k8xC
vyhnanec	vyhnanec	k1gMnSc1
pobýval	pobývat	k5eAaImAgMnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
jeho	jeho	k3xOp3gNnSc4
studium	studium	k1gNnSc4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
nejisté	jistý	k2eNgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
možná	možná	k9
i	i	k9
v	v	k7c6
Padově	Padova	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgInS
značného	značný	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
zabýval	zabývat	k5eAaImAgInS
se	s	k7c7
studiem	studio	k1gNnSc7
antického	antický	k2eAgMnSc2d1
i	i	k8xC
soudobého	soudobý	k2eAgNnSc2d1
italského	italský	k2eAgNnSc2d1
a	a	k8xC
provensálského	provensálský	k2eAgNnSc2d1
básnictví	básnictví	k1gNnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
antická	antický	k2eAgFnSc1d1
poezie	poezie	k1gFnSc1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc4
vzorem	vzor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Gemmou	Gemma	k1gFnSc7
z	z	k7c2
významného	významný	k2eAgInSc2d1
ghibellinského	ghibellinský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Donati	Donat	k2eAgMnPc1d1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
byl	být	k5eAaImAgMnS
zasnouben	zasnoubit	k5eAaPmNgMnS
již	již	k6eAd1
ve	v	k7c6
věku	věk	k1gInSc6
jedenácti	jedenáct	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gemma	Gemma	k1gFnSc1
mu	on	k3xPp3gMnSc3
porodila	porodit	k5eAaPmAgFnS
minimálně	minimálně	k6eAd1
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dante	Dante	k1gMnSc1
v	v	k7c6
politice	politika	k1gFnSc6
</s>
<s>
Dante	Dante	k1gMnSc1
se	se	k3xPyFc4
nejpozději	pozdě	k6eAd3
roku	rok	k1gInSc2
1295	#num#	k4
aktivně	aktivně	k6eAd1
zapojil	zapojit	k5eAaPmAgInS
do	do	k7c2
politického	politický	k2eAgInSc2d1
života	život	k1gInSc2
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
<g/>
,	,	kIx,
formálně	formálně	k6eAd1
byl	být	k5eAaImAgInS
členem	člen	k1gInSc7
cechu	cech	k1gInSc2
lékařů	lékař	k1gMnPc2
a	a	k8xC
lékárníků	lékárník	k1gMnPc2
(	(	kIx(
<g/>
politiky	politika	k1gFnSc2
se	se	k3xPyFc4
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
mohli	moct	k5eAaImAgMnP
účastnit	účastnit	k5eAaImF
pouze	pouze	k6eAd1
členové	člen	k1gMnPc1
cechů	cech	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgFnPc2
narážek	narážka	k1gFnPc2
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
dílech	díl	k1gInPc6
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1289	#num#	k4
zúčastnil	zúčastnit	k5eAaPmAgInS
bitvy	bitva	k1gFnSc2
u	u	k7c2
Campaldina	Campaldina	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
byli	být	k5eAaImAgMnP
poraženi	poražen	k2eAgMnPc1d1
ghibellinové	ghibellin	k1gMnPc1
(	(	kIx(
<g/>
příslušníci	příslušník	k1gMnPc1
šlechtické	šlechtický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stál	stát	k5eAaImAgMnS
na	na	k7c6
straně	strana	k1gFnSc6
cerchiovců	cerchiovec	k1gInPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
označovaných	označovaný	k2eAgNnPc2d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
bílí	bílit	k5eAaImIp3nS
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
bílí	bílý	k2eAgMnPc1d1
guelfové	guelf	k1gMnPc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílí	bílit	k5eAaImIp3nP
byli	být	k5eAaImAgMnP
protivníky	protivník	k1gMnPc7
strany	strana	k1gFnSc2
Donatiů	Donati	k1gMnPc2
–	–	k?
„	„	k?
<g/>
černých	černý	k2eAgMnPc2d1
guelfů	guelf	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Rozhodující	rozhodující	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
</s>
<s>
Roku	rok	k1gInSc2
1300	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
priorem	prior	k1gMnSc7
(	(	kIx(
<g/>
nejvyšším	vysoký	k2eAgMnSc7d3
představitelem	představitel	k1gMnSc7
<g/>
)	)	kIx)
městského	městský	k2eAgInSc2d1
státu	stát	k1gInSc2
Florencie	Florencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnSc1
vykonávala	vykonávat	k5eAaImAgFnS
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
,	,	kIx,
zastával	zastávat	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
Dante	Dante	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rozhodovalo	rozhodovat	k5eAaImAgNnS
o	o	k7c6
dalším	další	k2eAgInSc6d1
osudu	osud	k1gInSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
probíhajícím	probíhající	k2eAgInSc6d1
sporu	spor	k1gInSc6
s	s	k7c7
papežem	papež	k1gMnSc7
Bonifácem	Bonifác	k1gMnSc7
VIII	VIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
chtěl	chtít	k5eAaImAgMnS
získat	získat	k5eAaPmF
Toskánsko	Toskánsko	k1gNnSc4
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
příbuzné	příbuzný	k1gMnPc4
<g/>
,	,	kIx,
zaujal	zaujmout	k5eAaPmAgInS
protipapežské	protipapežský	k2eAgNnSc4d1
stanovisko	stanovisko	k1gNnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
pozdějších	pozdní	k2eAgFnPc2d2
zpráv	zpráva	k1gFnPc2
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
postaven	postavit	k5eAaPmNgInS
před	před	k7c4
těžké	těžký	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
odjet	odjet	k5eAaPmF
z	z	k7c2
Florencie	Florencie	k1gFnSc2
a	a	k8xC
zúčastnit	zúčastnit	k5eAaPmF
se	se	k3xPyFc4
vyjednávání	vyjednávání	k1gNnSc2
s	s	k7c7
papežem	papež	k1gMnSc7
Bonifácem	Bonifác	k1gMnSc7
VIII	VIII	kA
<g/>
.	.	kIx.
nebo	nebo	k8xC
raději	rád	k6eAd2
zůstat	zůstat	k5eAaPmF
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
té	ten	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
údajně	údajně	k6eAd1
řekl	říct	k5eAaPmAgMnS
legendární	legendární	k2eAgInSc4d1
výrok	výrok	k1gInSc4
„	„	k?
<g/>
Půjdu	jít	k5eAaImIp1nS
<g/>
-li	-li	k?
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
zůstane	zůstat	k5eAaPmIp3nS
<g/>
,	,	kIx,
zůstanu	zůstat	k5eAaPmIp1nS
<g/>
-li	-li	k?
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
půjde	jít	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Vyhnanství	vyhnanství	k1gNnSc1
</s>
<s>
Dante	Dante	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
zúčastnit	zúčastnit	k5eAaPmF
se	se	k3xPyFc4
poselstva	poselstvo	k1gNnPc1
<g/>
,	,	kIx,
během	během	k7c2
jeho	jeho	k3xOp3gFnSc2
nepřítomnosti	nepřítomnost	k1gFnSc2
však	však	k9
koncem	koncem	k7c2
roku	rok	k1gInSc2
1301	#num#	k4
černí	černit	k5eAaImIp3nP
získali	získat	k5eAaPmAgMnP
navrch	navrch	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dante	Dante	k1gMnSc1
byl	být	k5eAaImAgMnS
počátkem	počátkem	k7c2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
v	v	k7c6
nepřítomnosti	nepřítomnost	k1gFnSc6
k	k	k7c3
přehnaně	přehnaně	k6eAd1
vysoké	vysoký	k2eAgFnSc3d1
pokutě	pokuta	k1gFnSc3
(	(	kIx(
<g/>
její	její	k3xOp3gNnSc1
nezaplacení	nezaplacení	k1gNnSc1
znamenalo	znamenat	k5eAaImAgNnS
smrt	smrt	k1gFnSc4
na	na	k7c4
hranici	hranice	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
vyhoštěn	vyhostit	k5eAaPmNgMnS
na	na	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
z	z	k7c2
Florencie	Florencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
doufal	doufat	k5eAaImAgMnS
ve	v	k7c4
zvrat	zvrat	k1gInSc4
poměrů	poměr	k1gInPc2
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
<g/>
,	,	kIx,
zvlášť	zvlášť	k6eAd1
po	po	k7c6
brzké	brzký	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
papeže	papež	k1gMnSc2
Bonifáce	Bonifác	k1gMnSc2
VIII	VIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
pokoušel	pokoušet	k5eAaImAgMnS
se	se	k3xPyFc4
organizovat	organizovat	k5eAaBmF
vnější	vnější	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
změny	změna	k1gFnPc1
poměrů	poměr	k1gInPc2
se	se	k3xPyFc4
nedočkal	dočkat	k5eNaPmAgInS
a	a	k8xC
musel	muset	k5eAaImAgInS
zbytek	zbytek	k1gInSc1
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
strávit	strávit	k5eAaPmF
ve	v	k7c6
vyhnanství	vyhnanství	k1gNnSc6
na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
Itálie	Itálie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1
léta	léto	k1gNnPc1
života	život	k1gInSc2
a	a	k8xC
smrt	smrt	k1gFnSc4
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1
léta	léto	k1gNnPc1
svého	svůj	k1gMnSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dokončoval	dokončovat	k5eAaImAgMnS
Božskou	božský	k2eAgFnSc4d1
komedii	komedie	k1gFnSc4
<g/>
,	,	kIx,
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
Ravenně	Ravenně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tady	tady	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
žili	žít	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
již	již	k6eAd1
mezitím	mezitím	k6eAd1
usadili	usadit	k5eAaPmAgMnP
ve	v	k7c6
Veroně	Verona	k1gFnSc6
a	a	k8xC
vybudovali	vybudovat	k5eAaPmAgMnP
si	se	k3xPyFc3
tu	ten	k3xDgFnSc4
existenci	existence	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
dcera	dcera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
ho	on	k3xPp3gMnSc4
také	také	k6eAd1
ošetřovali	ošetřovat	k5eAaImAgMnP
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
dnech	den	k1gInPc6
života	život	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
nakazil	nakazit	k5eAaPmAgMnS
malárií	malárie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Ravenně	Ravenně	k1gFnSc6
byl	být	k5eAaImAgMnS
také	také	k6eAd1
pohřben	pohřbít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
Dantově	Dantův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
dal	dát	k5eAaPmAgMnS
kardinál	kardinál	k1gMnSc1
Bertrando	Bertranda	k1gFnSc5
del	del	k?
Poggetto	Poggetto	k1gNnSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
papežským	papežský	k2eAgInSc7d1
legátem	legát	k1gInSc7
v	v	k7c6
Lombardii	Lombardie	k1gFnSc6
<g/>
,	,	kIx,
spálit	spálit	k5eAaPmF
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
spis	spis	k1gInSc4
De	De	k?
monarchia	monarchium	k1gNnSc2
jako	jako	k8xC,k8xS
dílo	dílo	k1gNnSc4
kacířské	kacířský	k2eAgInPc4d1
a	a	k8xC
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
autorovy	autorův	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
byly	být	k5eAaImAgFnP
vyzdviženy	vyzdvihnout	k5eAaPmNgFnP
z	z	k7c2
hrobu	hrob	k1gInSc2
a	a	k8xC
rozhozeny	rozhozen	k2eAgFnPc1d1
do	do	k7c2
větru	vítr	k1gInSc2
<g/>
;	;	kIx,
k	k	k7c3
tomu	ten	k3xDgNnSc3
nakonec	nakonec	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Náhrobní	náhrobní	k2eAgInSc1d1
pomník	pomník	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
zřízen	zřídit	k5eAaPmNgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1483	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1780	#num#	k4
byla	být	k5eAaImAgFnS
nad	nad	k7c7
hrobem	hrob	k1gInSc7
postavena	postaven	k2eAgFnSc1d1
kupolovitá	kupolovitý	k2eAgFnSc1d1
kaplička	kaplička	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Dante	Dante	k1gMnSc1
a	a	k8xC
Vergilius	Vergilius	k1gMnSc1
v	v	k7c6
Pekle	peklo	k1gNnSc6
<g/>
,	,	kIx,
výjev	výjev	k1gInSc4
z	z	k7c2
Božské	božský	k2eAgFnSc2d1
komedie	komedie	k1gFnSc2
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
od	od	k7c2
Gustava	Gustav	k1gMnSc2
Doré	Dorá	k1gFnSc2
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
ovlivnila	ovlivnit	k5eAaPmAgFnS
platonická	platonický	k2eAgFnSc1d1
a	a	k8xC
idealizovaná	idealizovaný	k2eAgFnSc1d1
láska	láska	k1gFnSc1
k	k	k7c3
dívce	dívka	k1gFnSc3
jménem	jméno	k1gNnSc7
Bice	bika	k1gFnSc3
de	de	k?
Falco	Falco	k1gNnSc4
Portinari	Portinar	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
dílech	dílo	k1gNnPc6
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
Beatrice	Beatrice	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
její	její	k3xOp3gFnSc1
předčasná	předčasný	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledal	hledat	k5eAaImAgInS
ideál	ideál	k1gInSc1
lidské	lidský	k2eAgFnSc2d1
a	a	k8xC
božské	božský	k2eAgFnSc2d1
dokonalosti	dokonalost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ranější	raný	k2eAgNnPc1d2
díla	dílo	k1gNnPc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
život	život	k1gInSc1
(	(	kIx(
<g/>
La	la	k1gNnPc4
vita	vit	k2eAgNnPc4d1
nuova	nuovo	k1gNnPc4
<g/>
)	)	kIx)
–	–	k?
1293	#num#	k4
<g/>
,	,	kIx,
vrcholné	vrcholný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
stilnovismu	stilnovismus	k1gInSc2
<g/>
,	,	kIx,
sborník	sborník	k1gInSc1
básní	báseň	k1gFnPc2
za	za	k7c4
řadu	řada	k1gFnSc4
let	léto	k1gNnPc2
spojen	spojit	k5eAaPmNgInS
prozaickým	prozaický	k2eAgInSc7d1
komentářem	komentář	k1gInSc7
</s>
<s>
De	De	k?
vulgari	vulgari	k6eAd1
eloquentia	eloquentium	k1gNnSc2
–	–	k?
1304	#num#	k4
<g/>
,	,	kIx,
česky	česky	k6eAd1
přeloženo	přeložen	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
O	o	k7c6
rodném	rodný	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
O	o	k7c6
řeči	řeč	k1gFnSc6
lidové	lidový	k2eAgNnSc1d1
<g/>
,	,	kIx,
nedokončený	dokončený	k2eNgInSc1d1
traktát	traktát	k1gInSc1
psaný	psaný	k2eAgInSc1d1
latinsky	latinsky	k6eAd1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hostina	hostina	k1gFnSc1
(	(	kIx(
<g/>
Il	Il	k1gMnSc1
convivio	convivio	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
1307	#num#	k4
<g/>
,	,	kIx,
psáno	psát	k5eAaImNgNnS
italsky	italsky	k6eAd1
<g/>
,	,	kIx,
nedokončeno	dokončit	k5eNaPmNgNnS
<g/>
,	,	kIx,
3	#num#	k4
části	část	k1gFnPc1
<g/>
,	,	kIx,
vědecko-filosofický	vědecko-filosofický	k2eAgInSc1d1
traktát	traktát	k1gInSc1
<g/>
,	,	kIx,
výraz	výraz	k1gInSc1
středověkého	středověký	k2eAgInSc2d1
encyklopedismu	encyklopedismus	k1gInSc2
</s>
<s>
De	De	k?
monarchia	monarchia	k1gFnSc1
–	–	k?
1310	#num#	k4
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
traktát	traktát	k1gInSc1
<g/>
,	,	kIx,
psáno	psán	k2eAgNnSc1d1
v	v	k7c6
latině	latina	k1gFnSc6
<g/>
,	,	kIx,
období	období	k1gNnSc6
počátku	počátek	k1gInSc2
vlády	vláda	k1gFnSc2
Jindřicha	Jindřich	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
<g/>
,	,	kIx,
3	#num#	k4
knihy	kniha	k1gFnPc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
překládáno	překládat	k5eAaImNgNnS
jako	jako	k9
O	o	k7c6
jediné	jediný	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
někdy	někdy	k6eAd1
i	i	k9
jako	jako	k9
O	o	k7c6
světovládě	světovláda	k1gFnSc6
či	či	k8xC
jinak	jinak	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
zařadila	zařadit	k5eAaPmAgFnS
tento	tento	k3xDgInSc4
spis	spis	k1gInSc4
na	na	k7c4
Index	index	k1gInSc4
zakázaných	zakázaný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Uveden	uveden	k2eAgInSc1d1
je	být	k5eAaImIp3nS
ještě	ještě	k9
ve	v	k7c6
vydání	vydání	k1gNnSc6
Indexu	index	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1892	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
vydání	vydání	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
a	a	k8xC
ve	v	k7c6
vydáních	vydání	k1gNnPc6
pozdějších	pozdní	k2eAgNnPc6d2
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
dílo	dílo	k1gNnSc1
již	již	k6eAd1
nenachází	nacházet	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Božská	božský	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
a	a	k8xC
pozdější	pozdní	k2eAgNnPc1d2
díla	dílo	k1gNnPc1
</s>
<s>
Božská	božská	k1gFnSc1
komedie	komedie	k1gFnSc2
(	(	kIx(
<g/>
Comedia	Comedium	k1gNnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
La	la	k1gNnSc1
divina	divin	k2eAgNnSc2d1
commedia	commedium	k1gNnSc2
<g/>
)	)	kIx)
–	–	k?
cca	cca	kA
1306	#num#	k4
<g/>
–	–	k?
<g/>
1320	#num#	k4
<g/>
,	,	kIx,
části	část	k1gFnSc6
Peklo	peklo	k1gNnSc1
(	(	kIx(
<g/>
Inferno	inferno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Očistec	očistec	k1gInSc1
(	(	kIx(
<g/>
Purgatorio	Purgatorio	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Ráj	ráj	k1gInSc1
(	(	kIx(
<g/>
Paradiso	Paradisa	k1gFnSc5
<g/>
)	)	kIx)
<g/>
Nejvýznamnější	významný	k2eAgNnSc1d3
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
suma	suma	k1gFnSc1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
didakticko-alegorická	didakticko-alegorický	k2eAgFnSc1d1
báseň	báseň	k1gFnSc1
<g/>
/	/	kIx~
<g/>
epos	epos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc4
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
psal	psát	k5eAaImAgMnS
Dante	Dante	k1gMnSc1
pravděpodobně	pravděpodobně	k6eAd1
kolem	kolem	k7c2
20	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
alegorickým	alegorický	k2eAgNnSc7d1
zobrazením	zobrazení	k1gNnSc7
lidstva	lidstvo	k1gNnSc2
a	a	k8xC
vesmíru	vesmír	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
plná	plný	k2eAgFnSc1d1
symbolismu	symbolismus	k1gInSc2
<g/>
,	,	kIx,
filozofických	filozofický	k2eAgFnPc2d1
a	a	k8xC
historických	historický	k2eAgFnPc2d1
narážek	narážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
básnickým	básnický	k2eAgNnSc7d1
vyjádřením	vyjádření	k1gNnSc7
obrazu	obraz	k1gInSc2
křesťanského	křesťanský	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
vytvořili	vytvořit	k5eAaPmAgMnP
nejvýznamnější	významný	k2eAgMnPc1d3
scholastičtí	scholastický	k2eAgMnPc1d1
myslitelé	myslitel	k1gMnPc1
Albert	Albert	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
a	a	k8xC
Tomáš	Tomáš	k1gMnSc1
Akvinský	Akvinský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
napsána	napsat	k5eAaBmNgFnS,k5eAaPmNgFnS
italským	italský	k2eAgNnSc7d1
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
„	„	k?
<g/>
lidovým	lidový	k2eAgMnSc7d1
<g/>
“	“	k?
<g/>
)	)	kIx)
jazykem	jazyk	k1gInSc7
a	a	k8xC
výrazně	výrazně	k6eAd1
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
spisovnému	spisovný	k2eAgNnSc3d1
ustálení	ustálení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Eclogues	Eclogues	k1gInSc1
–	–	k?
1319	#num#	k4
</s>
<s>
Quaestio	Quaestio	k6eAd1
de	de	k?
situ	siíst	k5eAaPmIp1nS
aque	aque	k1gFnSc1
et	et	k?
terre	terr	k1gInSc5
–	–	k?
1320	#num#	k4
</s>
<s>
V	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Letopočet	letopočet	k1gInSc1
v	v	k7c6
závorce	závorka	k1gFnSc6
udává	udávat	k5eAaImIp3nS
první	první	k4xOgNnSc4
vydání	vydání	k1gNnSc4
překladu	překlad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Básně	báseň	k1gFnPc1
lyrické	lyrický	k2eAgFnPc1d1
<g/>
,	,	kIx,
přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Vrchlický	Vrchlický	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
1891	#num#	k4
</s>
<s>
Božská	božský	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
Mikeš	Mikeš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
2009	#num#	k4
</s>
<s>
Přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Vrchlický	Vrchlický	k1gMnSc1
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
–	–	k?
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Tribun	tribun	k1gMnSc1
EU	EU	kA
2009	#num#	k4
</s>
<s>
Přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Vrátný	vrátný	k1gMnSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O.	O.	kA
F.	F.	kA
Babler	Babler	k1gMnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
1989	#num#	k4
</s>
<s>
De	De	k?
vulgari	vulgari	k1gNnSc1
eloquentia	eloquentia	k1gFnSc1
=	=	kIx~
O	o	k7c6
rodném	rodný	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Oikúmené	Oikúmený	k2eAgNnSc1d1
2004	#num#	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Vladislav	Vladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČS	čs	kA
1969	#num#	k4
</s>
<s>
Přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Vrchlický	Vrchlický	k1gMnSc1
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
jediné	jediný	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gInSc1
1942	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
např.	např.	kA
„	„	k?
<g/>
Já	já	k3xPp1nSc1
jezdce	jezdec	k1gMnSc2
vídal	vídat	k5eAaImAgMnS
už	už	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
polem	polem	k6eAd1
hnali	hnát	k5eAaImAgMnP
<g/>
,	,	kIx,
chystali	chystat	k5eAaImAgMnP
útok	útok	k1gInSc4
<g/>
,	,	kIx,
řadili	řadit	k5eAaImAgMnP
se	se	k3xPyFc4
v	v	k7c4
šiky	šik	k1gInPc4
<g/>
,	,	kIx,
však	však	k9
někdy	někdy	k6eAd1
také	také	k9
na	na	k7c4
útěk	útěk	k1gInSc4
se	se	k3xPyFc4
dali	dát	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Božská	božský	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Otto	Otto	k1gMnSc1
František	František	k1gMnSc1
Babler	Babler	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MONTANELLI	MONTANELLI	kA
<g/>
,	,	kIx,
Indro	Indra	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dante	Dante	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
doba	doba	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
Orig	Orig	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Dante	Dante	k1gMnSc1
e	e	k0
il	il	k?
suo	suo	k?
secolo	secola	k1gFnSc5
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložila	přeložit	k5eAaPmAgFnS
Alena	Alena	k1gFnSc1
Hartmanová	Hartmanová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
260	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
234	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
↑	↑	k?
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
:	:	kIx,
illustrovaná	illustrovaný	k2eAgFnSc1d1
encyklopaedie	encyklopaedie	k1gFnSc1
obecných	obecný	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1893	#num#	k4
<g/>
.	.	kIx.
957	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
34	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
MONTANELLI	MONTANELLI	kA
<g/>
,	,	kIx,
Indro	Indra	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dante	Dante	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
doba	doba	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
Orig	Orig	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Dante	Dante	k1gMnSc1
e	e	k0
il	il	k?
suo	suo	k?
secolo	secola	k1gFnSc5
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložila	přeložit	k5eAaPmAgFnS
Alena	Alena	k1gFnSc1
Hartmanová	Hartmanová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
260	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
234	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
ŠPIČKA	Špička	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alighieri	Alighieri	k1gNnSc1
<g/>
,	,	kIx,
Dante	Dante	k1gMnSc1
De	De	k?
vulgari	vulgar	k1gFnSc2
eloquentia	eloquentium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
rodném	rodný	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iLiteratura	iLiteratura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2006-04-26	2006-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TUMPACH	TUMPACH	k?
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
PODLAHA	podlaha	k1gFnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
slovník	slovník	k1gInSc1
bohovědný	bohovědný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
církevní	církevní	k2eAgInPc4d1
řády	řád	k1gInPc4
–	–	k?
Ezzo	Ezzo	k6eAd1
(	(	kIx(
<g/>
sešity	sešit	k1gInPc7
42	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cyrillo-Methodějská	Cyrillo-Methodějský	k2eAgFnSc1d1
knihtiskárna	knihtiskárna	k1gFnSc1
a	a	k8xC
nakladatelství	nakladatelství	k1gNnSc1
V.	V.	kA
Kotrba	kotrba	k1gFnSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
960	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Dante	Dante	k1gMnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
404	#num#	k4
<g/>
–	–	k?
<g/>
408	#num#	k4
<g/>
;	;	kIx,
viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
406	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Index	index	k1gInSc1
librorum	librorum	k1gNnSc1
prohibitorum	prohibitorum	k1gInSc1
sanctissimi	sanctissi	k1gFnPc7
Domini	Domin	k1gMnPc1
Nostri	Nostr	k1gMnPc1
Leonis	Leonis	k1gFnSc2
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pont	Pont	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
jussu	juss	k1gInSc2
editus	editus	k1gInSc1
<g/>
:	:	kIx,
Editio	Editio	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taurnensis	Taurnensis	k1gFnSc1
cum	cum	k?
appendice	appendice	k1gFnSc1
usque	usquat	k5eAaPmIp3nS
ad	ad	k7c4
1892	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taurini	Taurin	k1gMnPc1
:	:	kIx,
Typ	typ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pontificia	Pontificia	k1gFnSc1
et	et	k?
Archiepiscopalis	Archiepiscopalis	k1gFnSc1
Eq	Eq	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petrus	Petrus	k1gInSc1
Marietti	Marietť	k1gFnSc2
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
.	.	kIx.
444	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
De	De	k?
vulgari	vulgari	k1gNnSc1
eloquentia	eloquentia	k1gFnSc1
<g/>
,	,	kIx,
1577	#num#	k4
</s>
<s>
Indro	Indra	k1gMnSc5
Montanelli	Montanell	k1gMnSc5
<g/>
,	,	kIx,
Dante	Dante	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
doba	doba	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
1981	#num#	k4
</s>
<s>
POKORNÝ	Pokorný	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dante	Dante	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
.	.	kIx.
118	#num#	k4
s.	s.	k?
</s>
<s>
Ströbinger	Ströbinger	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
:	:	kIx,
Tajemství	tajemství	k1gNnSc1
básníkova	básníkův	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Lidová	lidový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čtení	čtení	k1gNnSc1
o	o	k7c6
Dantovi	Dante	k1gMnSc6
Alighierim	Alighierim	k1gInSc4
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
M.	M.	kA
Pokorný	Pokorný	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Institut	institut	k1gInSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
tematický	tematický	k2eAgInSc1d1
blok	blok	k1gInSc1
v	v	k7c6
revue	revue	k1gFnPc6
Souvislosti	souvislost	k1gFnSc2
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
italských	italský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
</s>
<s>
Giovanni	Giovann	k1gMnPc1
Boccaccio	Boccaccio	k6eAd1
</s>
<s>
Gustave	Gustav	k1gMnSc5
Doré	Dorý	k2eAgNnSc4d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dante	Dante	k1gMnSc1
Alighieri	Alighier	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autor	autor	k1gMnSc1
Dante	Dante	k1gMnSc1
Alighieri	Alighiere	k1gFnSc4
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Dante	Dante	k1gMnSc1
Alighieri	Alighier	k1gFnSc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Dante	Dante	k1gMnSc1
Alighieri	Alighiere	k1gFnSc4
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Sborník	sborník	k1gInSc1
„	„	k?
<g/>
Humanismus	humanismus	k1gInSc1
v	v	k7c6
období	období	k1gNnSc6
renesance	renesance	k1gFnSc2
a	a	k8xC
reformace	reformace	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
Stať	stať	k1gFnSc1
„	„	k?
<g/>
Božský	božský	k2eAgMnSc1d1
Dante	Dante	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
39	#num#	k4
<g/>
,	,	kIx,
PDF	PDF	kA
</s>
<s>
Heslo	heslo	k1gNnSc4
v	v	k7c4
Stanford	Stanford	k1gInSc4
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosopha	k1gMnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
seznamu	seznam	k1gInSc2
literatury	literatura	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
(	(	kIx(
<g/>
lat.	lat.	k?
<g/>
)	)	kIx)
</s>
<s>
DANTE	Dante	k1gMnSc1
ALIGHIERI	ALIGHIERI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Božská	božská	k1gFnSc1
komedie	komedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
Peklo	peklo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Vrchlický	Vrchlický	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
.	.	kIx.
206	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990001625	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118523708	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2144	#num#	k4
6210	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78095495	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500265888	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
97105654	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78095495	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
