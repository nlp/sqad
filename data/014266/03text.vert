<s>
Confederación	Confederación	k1gMnSc1
Nacional	Nacional	k1gMnSc1
del	del	k?
Trabajo	Trabajo	k1gMnSc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Confederación	Confederación	k1gMnSc1
Nacional	Nacional	k1gMnSc1
del	del	k?
Trabajo	Trabajo	k1gMnSc1
(	(	kIx(
<g/>
šp	šp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
zkr.	zkr.	kA
CNT	CNT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
španělské	španělský	k2eAgNnSc1d1
anarchosyndikalistické	anarchosyndikalistický	k2eAgNnSc1d1
revoluční	revoluční	k2eAgNnSc1d1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
založené	založený	k2eAgNnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNT	CNT	kA
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
federace	federace	k1gFnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
pracujících	pracující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS
anarchisty	anarchista	k1gMnPc4
<g/>
,	,	kIx,
anarchokomunisty	anarchokomunista	k1gMnPc4
a	a	k8xC
libertariánské	libertariánský	k2eAgMnPc4d1
socialisty	socialist	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vzniku	vznik	k1gInSc2
Republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
bylo	být	k5eAaImAgNnS
jednou	jednou	k6eAd1
z	z	k7c2
hlavní	hlavní	k2eAgFnSc2d1
politických	politický	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
páteří	páteř	k1gFnPc2
španělské	španělský	k2eAgFnSc2d1
radikální	radikální	k2eAgFnSc2d1
levice	levice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvořilo	tvořit	k5eAaImAgNnS
protiváhu	protiváha	k1gFnSc4
proti	proti	k7c3
jinému	jiný	k2eAgInSc3d1
odborovému	odborový	k2eAgInSc3d1
svazu	svaz	k1gInSc3
-	-	kIx~
UGT	UGT	kA
(	(	kIx(
<g/>
Unión	Unión	k1gMnSc1
General	General	k1gMnSc1
de	de	k?
Trabajadores	Trabajadores	k1gMnSc1
<g/>
,	,	kIx,
šp	šp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecná	všeobecný	k2eAgFnSc1d1
strana	strana	k1gFnSc1
pracujících	pracující	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
a	a	k8xC
umírněných	umírněný	k2eAgMnPc2d1
komunistů	komunista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
nedocházelo	docházet	k5eNaImAgNnS
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
k	k	k7c3
vážnějším	vážní	k2eAgInPc3d2
sporům	spor	k1gInPc3
a	a	k8xC
většinou	většinou	k6eAd1
spolu	spolu	k6eAd1
CNT	CNT	kA
i	i	k8xC
UGT	UGT	kA
aktivně	aktivně	k6eAd1
spolupracovaly	spolupracovat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
CNT	CNT	kA
spolu	spolu	k6eAd1
s	s	k7c7
anarchistickou	anarchistický	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
FAI	FAI	kA
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
úzké	úzký	k2eAgFnSc3d1
provázaností	provázanost	k1gFnPc2
obou	dva	k4xCgFnPc2
organizací	organizace	k1gFnPc2
mluví	mluvit	k5eAaImIp3nS
o	o	k7c4
organizaci	organizace	k1gFnSc4
CNT-FAI	CNT-FAI	k1gFnSc2
<g/>
)	)	kIx)
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
Lidové	lidový	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
vytvořilo	vytvořit	k5eAaPmAgNnS
CNT	CNT	kA
spolu	spolu	k6eAd1
s	s	k7c7
FAI	FAI	kA
anarchistické	anarchistický	k2eAgFnPc4d1
milice	milice	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
bojovaly	bojovat	k5eAaImAgFnP
proti	proti	k7c3
frankistům	frankista	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc7
velitelem	velitel	k1gMnSc7
byl	být	k5eAaImAgMnS
člen	člen	k1gMnSc1
CNT	CNT	kA
<g/>
,	,	kIx,
levicový	levicový	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Buenaventura	Buenaventura	k1gFnSc1
Durruti	Durruť	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
pro	pro	k7c4
republikány	republikán	k1gMnPc4
legendou	legenda	k1gFnSc7
Španělské	španělský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
jako	jako	k8xC,k8xS
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
ubránil	ubránit	k5eAaPmAgInS
Madrid	Madrid	k1gInSc1
před	před	k7c7
frankisty	frankista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
roku	rok	k1gInSc2
1939	#num#	k4
frankisté	frankista	k1gMnPc1
válku	válka	k1gFnSc4
vyhráli	vyhrát	k5eAaPmAgMnP
a	a	k8xC
CNT	CNT	kA
byla	být	k5eAaImAgFnS
zakázána	zakázat	k5eAaPmNgFnS
a	a	k8xC
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
ilegality	ilegalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc4
členové	člen	k1gMnPc1
buď	buď	k8xC
emigrovali	emigrovat	k5eAaBmAgMnP
nebo	nebo	k8xC
byli	být	k5eAaImAgMnP
frankistickým	frankistický	k2eAgInSc7d1
režimem	režim	k1gInSc7
pozatýkáni	pozatýkán	k2eAgMnPc1d1
nebo	nebo	k8xC
zabiti	zabit	k2eAgMnPc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Navzdory	navzdory	k6eAd1
desetiletím	desetiletí	k1gNnSc7
fungování	fungování	k1gNnSc2
v	v	k7c6
ilegalitě	ilegalita	k1gFnSc6
dnes	dnes	k6eAd1
CNT	CNT	kA
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
činnosti	činnost	k1gFnSc6
na	na	k7c6
principech	princip	k1gInPc6
samosprávy	samospráva	k1gFnSc2
pracujících	pracující	k1gMnPc2
<g/>
,	,	kIx,
federalismu	federalismus	k1gInSc2
a	a	k8xC
vzájemné	vzájemný	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
A	a	k9
las	laso	k1gNnPc2
barricadas	barricadasa	k1gFnPc2
</s>
<s>
Na	na	k7c4
barikádyHymna	barikádyHymn	k1gMnSc4
</s>
<s>
CNT	CNT	kA
Slova	slovo	k1gNnPc4
</s>
<s>
Valeriano	Valeriana	k1gFnSc5
Orobón	Orobón	k1gMnSc1
Fernández	Fernández	k1gMnSc1
<g/>
,	,	kIx,
1936	#num#	k4
Hudba	hudba	k1gFnSc1
</s>
<s>
Wacław	Wacław	k?
Święcicki	Święcicki	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
listenlist	listenlist	k1gInSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k1gInSc1
<g/>
/	/	kIx~
<g/>
thumb	thumb	k1gInSc1
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
94	#num#	k4
<g/>
/	/	kIx~
<g/>
Gnome-speakernotes	Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
px-Gnome-speakernotes	px-Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
padding-left	padding-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
min-height	min-height	k1gInSc1
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
background-position	background-position	k1gInSc4
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
left	left	k1gInSc1
<g/>
;	;	kIx,
<g/>
background-repeat	background-repeat	k1gInSc1
<g/>
:	:	kIx,
<g/>
no-repeat	no-repeat	k5eAaImF,k5eAaPmF,k5eAaBmF
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
list-style-type	list-style-typ	k1gInSc5
<g/>
:	:	kIx,
<g/>
none	none	k1gNnPc6
<g/>
;	;	kIx,
<g/>
list-style-image	list-style-image	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
margin	margin	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
<g/>
{	{	kIx(
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.5	0.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
li	li	k8xS
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
91	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
A	a	k9
las	laso	k1gNnPc2
barricadas	barricadasa	k1gFnPc2
</s>
<s>
Sborový	sborový	k2eAgInSc1d1
zpěv	zpěv	k1gInSc1
</s>
<s>
Problémy	problém	k1gInPc1
s	s	k7c7
přehráváním	přehrávání	k1gNnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nápověda	nápověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Významní	významný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
CNT	CNT	kA
</s>
<s>
Diego	Diego	k1gMnSc1
Abad	Abad	k1gMnSc1
de	de	k?
Santillán	Santillán	k1gInSc1
</s>
<s>
Francisco	Francisca	k1gFnSc5
Ascaso	Ascasa	k1gFnSc5
</s>
<s>
Buenaventura	Buenaventura	k1gFnSc1
Durruti	Durruť	k1gFnSc2
</s>
<s>
Carles	Carles	k1gInSc1
Fontseré	Fontserý	k2eAgFnSc2d1
</s>
<s>
Salvador	Salvador	k1gMnSc1
Seguí	Seguí	k1gMnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
García	García	k1gMnSc1
Oliver	Oliver	k1gMnSc1
</s>
<s>
Anselmo	Anselma	k1gFnSc5
Lorenzo	Lorenza	k1gFnSc5
</s>
<s>
Horacio	Horacio	k1gMnSc1
Martínez	Martínez	k1gMnSc1
Prieto	Prieto	k1gNnSc4
</s>
<s>
Federica	Federica	k6eAd1
Montseny	Montsen	k2eAgFnPc1d1
</s>
<s>
Andreu	Andrea	k1gFnSc4
Nin	Nina	k1gFnPc2
</s>
<s>
Valeriano	Valeriana	k1gFnSc5
Orobón	Orobón	k1gInSc4
Fernández	Fernández	k1gInSc1
</s>
<s>
Josep	Josep	k1gMnSc1
Peiró	Peiró	k1gMnSc1
Olives	Olives	k1gMnSc1
</s>
<s>
Joan	Joan	k1gMnSc1
Peiró	Peiró	k1gMnSc1
</s>
<s>
Ángel	Ángel	k1gMnSc1
Pestañ	Pestañ	k1gMnSc1
</s>
<s>
Isaac	Isaac	k6eAd1
Puente	Puent	k1gMnSc5
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
regionů	region	k1gInPc2
CNT	CNT	kA
</s>
<s>
Mapa	mapa	k1gFnSc1
regionů	region	k1gInPc2
podle	podle	k7c2
CNT	CNT	kA
<g/>
.	.	kIx.
</s>
<s>
Andalucía	Andalucía	k6eAd1
</s>
<s>
Almería	Almería	k6eAd1
</s>
<s>
Cádiz	Cádiz	k1gInSc1
</s>
<s>
Córdoba	Córdoba	k1gFnSc1
</s>
<s>
Granada	Granada	k1gFnSc1
</s>
<s>
Huelva	Huelva	k6eAd1
</s>
<s>
Jaén	Jaén	k1gMnSc1
</s>
<s>
Málaga	Málaga	k1gFnSc1
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
</s>
<s>
Aragón	Aragón	k1gMnSc1
-	-	kIx~
Rioja	Rioja	k1gMnSc1
</s>
<s>
Huesca	Huesca	k6eAd1
</s>
<s>
La	la	k1gNnSc1
Rioja	Rioj	k1gInSc2
</s>
<s>
Teruel	Teruel	k1gMnSc1
</s>
<s>
Zaragoza	Zaragoza	k1gFnSc1
</s>
<s>
Asturias	Asturias	k1gMnSc1
-	-	kIx~
León	León	k1gMnSc1
</s>
<s>
Asturie	Asturie	k1gFnSc1
</s>
<s>
León	León	k1gMnSc1
</s>
<s>
Canarias	Canarias	k1gMnSc1
</s>
<s>
Las	laso	k1gNnPc2
Palmas	Palmasa	k1gFnPc2
</s>
<s>
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
de	de	k?
Tenerife	Tenerif	k1gInSc5
</s>
<s>
Cataluñ	Cataluñ	k1gFnSc1
-	-	kIx~
Baleares	Baleares	k1gInSc1
</s>
<s>
Baleáry	Baleáry	k1gFnPc1
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
Girona	Girona	k1gFnSc1
</s>
<s>
Lérida	Lérida	k1gFnSc1
</s>
<s>
Tarragona	Tarragona	k1gFnSc1
</s>
<s>
Centro	Centro	k6eAd1
</s>
<s>
Ávila	Ávila	k6eAd1
</s>
<s>
Ciudad	Ciudad	k6eAd1
Real	Real	k1gInSc1
</s>
<s>
Cuenca	Cuenca	k6eAd1
</s>
<s>
Guadalajara	Guadalajara	k1gFnSc1
</s>
<s>
Madrid	Madrid	k1gInSc1
</s>
<s>
Palencia	Palencia	k1gFnSc1
</s>
<s>
Salamanca	Salamanca	k6eAd1
</s>
<s>
Segovia	Segovia	k1gFnSc1
</s>
<s>
Soria	Soria	k1gFnSc1
</s>
<s>
Toledo	Toledo	k1gNnSc1
</s>
<s>
Valladolid	Valladolid	k1gInSc1
</s>
<s>
Zamora	Zamora	k1gFnSc1
</s>
<s>
Extremadura	Extremadura	k1gFnSc1
</s>
<s>
Badajoz	Badajoz	k1gMnSc1
</s>
<s>
Cáceres	Cáceres	k1gMnSc1
</s>
<s>
Galicia	Galicia	k1gFnSc1
</s>
<s>
La	la	k1gNnSc1
Coruñ	Coruñ	k1gInSc2
</s>
<s>
Lugo	Lugo	k6eAd1
</s>
<s>
Orense	Orense	k6eAd1
</s>
<s>
Pontevedra	Pontevedra	k1gFnSc1
</s>
<s>
Levante	Levant	k1gMnSc5
</s>
<s>
Albacete	Albacat	k5eAaImIp2nP,k5eAaBmIp2nP,k5eAaPmIp2nP
</s>
<s>
Alicante	Alicant	k1gMnSc5
</s>
<s>
Castellón	Castellón	k1gMnSc1
</s>
<s>
Valencia	Valencia	k1gFnSc1
</s>
<s>
Murcia	Murcia	k1gFnSc1
</s>
<s>
Murcijský	Murcijský	k2eAgInSc1d1
region	region	k1gInSc1
</s>
<s>
Cartagena	Cartagena	k1gFnSc1
</s>
<s>
La	la	k1gNnSc1
Unión	Unión	k1gMnSc1
</s>
<s>
Mar	Mar	k?
Menor	Menor	k1gInSc1
</s>
<s>
Mazarrón	Mazarrón	k1gMnSc1
</s>
<s>
Lorca	Lorca	k6eAd1
</s>
<s>
Yecla	Yecla	k6eAd1
</s>
<s>
Jumilla	Jumilla	k6eAd1
</s>
<s>
Pilar	Pilar	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Horadada	Horadada	k1gFnSc1
</s>
<s>
Norte	Norte	k5eAaPmIp2nP
</s>
<s>
Álava	Álava	k6eAd1
</s>
<s>
Burgos	Burgos	k1gMnSc1
</s>
<s>
Cantabria	Cantabrium	k1gNnPc1
</s>
<s>
Guipúzcoa	Guipúzcoa	k6eAd1
</s>
<s>
Navarra	Navarra	k1gFnSc1
</s>
<s>
Vizcaya	Vizcaya	k6eAd1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Snímek	snímek	k1gInSc1
z	z	k7c2
Kongresu	kongres	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
bylo	být	k5eAaImAgNnS
CNT	CNT	kA
založeno	založit	k5eAaPmNgNnS
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
členů	člen	k1gMnPc2
CNT	CNT	kA
</s>
<s>
Kancelář	kancelář	k1gFnSc1
CNT	CNT	kA
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
</s>
<s>
Lístek	lístek	k1gInSc1
z	z	k7c2
divadla	divadlo	k1gNnSc2
spravovaného	spravovaný	k2eAgInSc2d1
CNT	CNT	kA
</s>
<s>
Svátek	svátek	k1gInSc4
práce	práce	k1gFnSc2
<g/>
:	:	kIx,
Demonstrace	demonstrace	k1gFnSc2
CNT	CNT	kA
v	v	k7c6
Bilbau	Bilbaus	k1gInSc6
</s>
<s>
Vlajka	vlajka	k1gFnSc1
se	s	k7c7
znakem	znak	k1gInSc7
CNT	CNT	kA
</s>
<s>
Struktura	struktura	k1gFnSc1
CNT	CNT	kA
</s>
<s>
Milicionářky	milicionářka	k1gFnPc1
CNT-FAI	CNT-FAI	k1gFnSc2
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Confederación	Confederación	k1gMnSc1
Nacional	Nacional	k1gFnSc4
del	del	k?
Trabajo	Trabajo	k6eAd1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Confederación	Confederación	k1gInSc4
Nacional	Nacional	k1gFnSc2
del	del	k?
Trabajo	Trabajo	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Anselmo	Anselma	k1gFnSc5
Lorenzo	Lorenza	k1gFnSc5
na	na	k7c6
španělské	španělský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2008444460	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1044145-1	1044145-1	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2171	#num#	k4
4406	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80146702	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
141520586	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80146702	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
