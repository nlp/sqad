<s desamb="1">
Národní	národní	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
zkr.	zkr.	kA
CNT	CNT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
španělské	španělský	k2eAgNnSc1d1
anarchosyndikalistické	anarchosyndikalistický	k2eAgNnSc1d1
revoluční	revoluční	k2eAgNnSc1d1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
založené	založený	k2eAgNnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
.	.	kIx.
</s>