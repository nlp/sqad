<p>
<s>
Korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
(	(	kIx(	(
<g/>
slangově	slangově	k6eAd1	slangově
nerez	nerez	k2eAgFnSc1d1	nerez
<g/>
,	,	kIx,	,
nerezová	rezový	k2eNgFnSc1d1	nerezová
ocel	ocel	k1gFnSc4	ocel
či	či	k8xC	či
nerezavějící	rezavějící	k2eNgFnSc4d1	nerezavějící
ocel	ocel	k1gFnSc4	ocel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysocelegovaná	vysocelegovaný	k2eAgFnSc1d1	vysocelegovaný
ocel	ocel	k1gFnSc1	ocel
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
chemické	chemický	k2eAgFnSc3d1	chemická
i	i	k8xC	i
elektrochemické	elektrochemický	k2eAgFnSc3d1	elektrochemická
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korozní	korozní	k2eAgFnSc1d1	korozní
odolnost	odolnost	k1gFnSc1	odolnost
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
schopnosti	schopnost	k1gFnPc4	schopnost
tzv.	tzv.	kA	tzv.
pasivace	pasivace	k1gFnSc1	pasivace
povrchu	povrch	k1gInSc2	povrch
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pasivita	pasivita	k1gFnSc1	pasivita
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
vůči	vůči	k7c3	vůči
celkové	celkový	k2eAgFnSc3d1	celková
korozi	koroze	k1gFnSc3	koroze
dosažena	dosažen	k2eAgFnSc1d1	dosažena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
prostředích	prostředí	k1gNnPc6	prostředí
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
specifických	specifický	k2eAgNnPc6d1	specifické
prostředích	prostředí	k1gNnPc6	prostředí
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
různých	různý	k2eAgInPc2d1	různý
lokálních	lokální	k2eAgInPc2d1	lokální
druhů	druh	k1gInPc2	druh
koroze	koroze	k1gFnSc1	koroze
–	–	k?	–
štěrbinovou	štěrbinový	k2eAgFnSc7d1	štěrbinová
<g/>
,	,	kIx,	,
bodovou	bodový	k2eAgFnSc7d1	bodová
<g/>
,	,	kIx,	,
mezikrystalovou	mezikrystalův	k2eAgFnSc7d1	mezikrystalův
a	a	k8xC	a
korozním	korozní	k2eAgNnSc7d1	korozní
praskáním	praskání	k1gNnSc7	praskání
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
chrómu	chróm	k1gInSc2	chróm
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
korozní	korozní	k2eAgFnSc4d1	korozní
odolnost	odolnost	k1gFnSc4	odolnost
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
typ	typ	k1gInSc4	typ
koroze	koroze	k1gFnSc2	koroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korozivzdorné	korozivzdorný	k2eAgFnPc4d1	korozivzdorná
oceli	ocel	k1gFnPc4	ocel
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
feritické	feritický	k2eAgFnSc2d1	feritická
<g/>
,	,	kIx,	,
martenzitické	martenzitický	k2eAgFnSc2d1	martenzitická
<g/>
,	,	kIx,	,
austenitické	austenitický	k2eAgFnSc2d1	austenitická
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
přechodové	přechodový	k2eAgFnSc2d1	přechodová
skupiny	skupina	k1gFnSc2	skupina
feriticko-austenitické	feritickoustenitický	k2eAgFnSc2d1	feriticko-austenitický
<g/>
,	,	kIx,	,
martenziticko-austenitické	martenzitickoustenitický	k2eAgFnSc2d1	martenziticko-austenitický
a	a	k8xC	a
poloferitické	poloferitický	k2eAgFnSc2d1	poloferitický
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
korozivzdorné	korozivzdorný	k2eAgFnPc1d1	korozivzdorná
oceli	ocel	k1gFnPc1	ocel
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
legur	legura	k1gFnPc2	legura
<g/>
,	,	kIx,	,
12	[number]	k4	12
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
nebo	nebo	k8xC	nebo
do	do	k7c2	do
24	[number]	k4	24
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
slitinu	slitina	k1gFnSc4	slitina
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
železem	železo	k1gNnSc7	železo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ocel	ocel	k1gFnSc1	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
<g/>
,	,	kIx,	,
od	od	k7c2	od
chemického	chemický	k2eAgInSc2d1	chemický
a	a	k8xC	a
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
i	i	k9	i
jako	jako	k9	jako
architektonický	architektonický	k2eAgInSc4d1	architektonický
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
lodí	loď	k1gFnSc7	loď
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Problematiku	problematika	k1gFnSc4	problematika
koroze	koroze	k1gFnSc2	koroze
znali	znát	k5eAaImAgMnP	znát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
–	–	k?	–
od	od	k7c2	od
Platona	Plato	k1gMnSc2	Plato
<g/>
,	,	kIx,	,
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
–	–	k?	–
od	od	k7c2	od
Plinia	Plinium	k1gNnSc2	Plinium
a	a	k8xC	a
Vitruvia	Vitruvium	k1gNnSc2	Vitruvium
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
Dillí	Dillí	k1gNnSc6	Dillí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zachován	zachovat	k5eAaPmNgInS	zachovat
železný	železný	k2eAgInSc1d1	železný
pilíř	pilíř	k1gInSc1	pilíř
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
korozi	koroze	k1gFnSc6	koroze
kovaných	kovaný	k2eAgFnPc2d1	kovaná
železných	železný	k2eAgFnPc2d1	železná
částí	část	k1gFnPc2	část
vodních	vodní	k2eAgFnPc2d1	vodní
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
až	až	k9	až
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Koroze	koroze	k1gFnSc1	koroze
litinových	litinový	k2eAgInPc2d1	litinový
vodovodů	vodovod	k1gInPc2	vodovod
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
až	až	k9	až
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnPc4	století
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
litiny	litina	k1gFnSc2	litina
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
pokovování	pokovování	k1gNnSc2	pokovování
cínem	cín	k1gInSc7	cín
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
už	už	k6eAd1	už
asi	asi	k9	asi
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mědí	mědit	k5eAaImIp3nS	mědit
(	(	kIx(	(
<g/>
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
problematikou	problematika	k1gFnSc7	problematika
koroze	koroze	k1gFnSc2	koroze
zabývali	zabývat	k5eAaImAgMnP	zabývat
Stahl	Stahl	k1gMnSc1	Stahl
<g/>
,	,	kIx,	,
Lavoisier	Lavoisier	k1gMnSc1	Lavoisier
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
Hall	Hall	k1gMnSc1	Hall
popisuje	popisovat	k5eAaImIp3nS	popisovat
podstatu	podstata	k1gFnSc4	podstata
koroze	koroze	k1gFnSc2	koroze
jako	jako	k8xS	jako
oxidaci	oxidace	k1gFnSc4	oxidace
železa	železo	k1gNnSc2	železo
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
popsána	popsán	k2eAgFnSc1d1	popsána
koroze	koroze	k1gFnSc1	koroze
jako	jako	k8xS	jako
elektrochemický	elektrochemický	k2eAgInSc1d1	elektrochemický
děj	děj	k1gInSc1	děj
<g/>
.	.	kIx.	.
<g/>
Korozní	korozní	k2eAgFnSc1d1	korozní
odolnost	odolnost	k1gFnSc1	odolnost
slitiny	slitina	k1gFnSc2	slitina
železa	železo	k1gNnSc2	železo
s	s	k7c7	s
chrómem	chróm	k1gInSc7	chróm
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
Berthierem	Berthier	k1gInSc7	Berthier
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
v	v	k7c6	v
lučavce	lučavka	k1gFnSc6	lučavka
královské	královský	k2eAgFnSc6d1	královská
<g/>
,	,	kIx,	,
Malletem	Mallet	k1gInSc7	Mallet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
v	v	k7c6	v
korozním	korozní	k2eAgNnSc6d1	korozní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
Frémyem	Frémyem	k1gInSc4	Frémyem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
koncentrovaných	koncentrovaný	k2eAgFnPc2d1	koncentrovaná
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
<g/>
.	.	kIx.	.
<g/>
Výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
s	s	k7c7	s
chrómem	chróm	k1gInSc7	chróm
a	a	k8xC	a
niklem	nikl	k1gInSc7	nikl
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozeběhl	rozeběhnout	k5eAaPmAgMnS	rozeběhnout
až	až	k9	až
během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
je	být	k5eAaImIp3nS	být
také	také	k9	také
práce	práce	k1gFnSc1	práce
Philipa	Philip	k1gMnSc2	Philip
Monnartza	Monnartz	k1gMnSc2	Monnartz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
vlastnosti	vlastnost	k1gFnSc3	vlastnost
slitin	slitina	k1gFnPc2	slitina
železa	železo	k1gNnSc2	železo
s	s	k7c7	s
chrómem	chróm	k1gInSc7	chróm
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
práci	práce	k1gFnSc6	práce
naznačil	naznačit	k5eAaPmAgMnS	naznačit
dvě	dva	k4xCgFnPc4	dva
nejnižší	nízký	k2eAgFnPc4d3	nejnižší
meze	mez	k1gFnPc4	mez
obsahu	obsah	k1gInSc2	obsah
chrómu	chróm	k1gInSc2	chróm
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
12,5	[number]	k4	12,5
%	%	kIx~	%
pro	pro	k7c4	pro
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
kyselině	kyselina	k1gFnSc3	kyselina
dusičné	dusičný	k2eAgInPc4d1	dusičný
na	na	k7c4	na
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
14	[number]	k4	14
%	%	kIx~	%
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
varu	var	k1gInSc6	var
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kladenské	kladenský	k2eAgFnSc6d1	kladenská
Poldině	Poldina	k1gFnSc6	Poldina
huti	huť	k1gFnSc2	huť
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
ocel	ocel	k1gFnSc1	ocel
pro	pro	k7c4	pro
hlavně	hlavně	k6eAd1	hlavně
loveckých	lovecký	k2eAgFnPc2d1	lovecká
pušek	puška	k1gFnPc2	puška
s	s	k7c7	s
10	[number]	k4	10
<g/>
%	%	kIx~	%
obsahem	obsah	k1gInSc7	obsah
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
<g/>
Harry	Harra	k1gFnPc4	Harra
Brearley	Brearle	k2eAgFnPc4d1	Brearle
z	z	k7c2	z
sheffieldských	sheffieldský	k2eAgFnPc2d1	sheffieldská
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hledal	hledat	k5eAaImAgInS	hledat
korozivzdorné	korozivzdorný	k2eAgFnPc4d1	korozivzdorná
slitiny	slitina	k1gFnPc4	slitina
pro	pro	k7c4	pro
sudy	sud	k1gInPc4	sud
se	s	k7c7	s
střelným	střelný	k2eAgInSc7d1	střelný
prachem	prach	k1gInSc7	prach
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
a	a	k8xC	a
následně	následně	k6eAd1	následně
zavedl	zavést	k5eAaPmAgInS	zavést
výrobu	výroba	k1gFnSc4	výroba
martenzitické	martenzitický	k2eAgFnSc2d1	martenzitická
korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
0,24	[number]	k4	0,24
%	%	kIx~	%
<g/>
,	,	kIx,	,
chrómu	chróm	k1gInSc2	chróm
12,8	[number]	k4	12,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Benno	Benno	k6eAd1	Benno
Strauss	Strauss	k1gInSc1	Strauss
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
Maurer	Maurra	k1gFnPc2	Maurra
patentovali	patentovat	k5eAaBmAgMnP	patentovat
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Krupp	Krupp	k1gMnSc1	Krupp
austenitickou	austenitický	k2eAgFnSc4d1	austenitická
korozivzdornou	korozivzdorný	k2eAgFnSc4d1	korozivzdorná
ocel	ocel	k1gFnSc4	ocel
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Strauss	Straussa	k1gFnPc2	Straussa
a	a	k8xC	a
Maurer	Maurra	k1gFnPc2	Maurra
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
ocel	ocel	k1gFnSc4	ocel
s	s	k7c7	s
20	[number]	k4	20
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
7	[number]	k4	7
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
0,25	[number]	k4	0,25
%	%	kIx~	%
uhlíku	uhlík	k1gInSc2	uhlík
pro	pro	k7c4	pro
výrobnu	výrobna	k1gFnSc4	výrobna
čpavku	čpavek	k1gInSc2	čpavek
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc1	první
duplexní	duplexní	k2eAgFnSc1d1	duplexní
korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
pro	pro	k7c4	pro
papírenský	papírenský	k2eAgInSc4d1	papírenský
průmysl	průmysl	k1gInSc4	průmysl
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
byly	být	k5eAaImAgInP	být
pochopeny	pochopit	k5eAaPmNgInP	pochopit
metalurgické	metalurgický	k2eAgInPc1d1	metalurgický
procesy	proces	k1gInPc1	proces
a	a	k8xC	a
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
standardizovány	standardizován	k2eAgFnPc4d1	standardizována
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
skokem	skok	k1gInSc7	skok
bylo	být	k5eAaImAgNnS	být
kyslíko-argonové	kyslíkorgonový	k2eAgNnSc1d1	kyslíko-argonový
oduhličení	oduhličení	k1gNnSc1	oduhličení
ocelí	ocel	k1gFnPc2	ocel
na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
výrobu	výroba	k1gFnSc4	výroba
nízkouhlíkových	nízkouhlíkový	k2eAgFnPc2d1	nízkouhlíková
ocelí	ocel	k1gFnPc2	ocel
s	s	k7c7	s
regulovaným	regulovaný	k2eAgNnSc7d1	regulované
množstvím	množství	k1gNnSc7	množství
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
ocelí	ocel	k1gFnPc2	ocel
a	a	k8xC	a
super-	super-	k?	super-
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
superferitických	superferitický	k2eAgFnPc2d1	superferitický
s	s	k7c7	s
vynikající	vynikající	k2eAgFnSc7d1	vynikající
korozní	korozní	k2eAgFnSc7d1	korozní
odolností	odolnost	k1gFnSc7	odolnost
nebo	nebo	k8xC	nebo
superduplexních	superduplexní	k2eAgFnPc2d1	superduplexní
ocelí	ocel	k1gFnPc2	ocel
s	s	k7c7	s
výbornou	výborný	k2eAgFnSc7d1	výborná
korozivzdorností	korozivzdornost	k1gFnSc7	korozivzdornost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dobrou	dobrý	k2eAgFnSc7d1	dobrá
svařitelností	svařitelnost	k1gFnSc7	svařitelnost
<g/>
.	.	kIx.	.
<g/>
Korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
mj.	mj.	kA	mj.
i	i	k9	i
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
průmyslu	průmysl	k1gInSc6	průmysl
již	již	k9	již
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
motorech	motor	k1gInPc6	motor
letadel	letadlo	k1gNnPc2	letadlo
Sopwith	Sopwith	k1gInSc1	Sopwith
Triplane	Triplan	k1gMnSc5	Triplan
<g/>
,	,	kIx,	,
Sopwith	Sopwith	k1gMnSc1	Sopwith
Camel	Camel	k1gMnSc1	Camel
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koroze	koroze	k1gFnSc2	koroze
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
ocel	ocel	k1gFnSc1	ocel
nazývá	nazývat	k5eAaImIp3nS	nazývat
korozivzdornou	korozivzdorný	k2eAgFnSc4d1	korozivzdorná
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
nerezovou	rezový	k2eNgFnSc7d1	nerezová
neznamená	znamenat	k5eNaImIp3nS	znamenat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
korozi	koroze	k1gFnSc4	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
působení	působení	k1gNnSc2	působení
atmosférického	atmosférický	k2eAgNnSc2d1	atmosférické
či	či	k8xC	či
vodního	vodní	k2eAgNnSc2d1	vodní
prostředí	prostředí	k1gNnSc2	prostředí
se	se	k3xPyFc4	se
s	s	k7c7	s
korozí	koroze	k1gFnSc7	koroze
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
zábradlí	zábradlí	k1gNnSc2	zábradlí
<g/>
,	,	kIx,	,
příborů	příbor	k1gInPc2	příbor
apod.	apod.	kA	apod.
Při	při	k7c6	při
působení	působení	k1gNnSc6	působení
agresivního	agresivní	k2eAgNnSc2d1	agresivní
prostředí	prostředí	k1gNnSc2	prostředí
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
korozí	koroze	k1gFnSc7	koroze
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhy	druh	k1gInPc1	druh
koroze	koroze	k1gFnSc1	koroze
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Chemická	chemický	k2eAgFnSc1d1	chemická
====	====	k?	====
</s>
</p>
<p>
<s>
K	k	k7c3	k
chemické	chemický	k2eAgFnSc3d1	chemická
korozi	koroze	k1gFnSc3	koroze
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
styku	styk	k1gInSc6	styk
tuhých	tuhý	k2eAgFnPc2d1	tuhá
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
plynnými	plynný	k2eAgFnPc7d1	plynná
jako	jako	k8xC	jako
oxidačně-redukční	oxidačněedukční	k2eAgFnPc1d1	oxidačně-redukční
reakce	reakce	k1gFnPc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rychlost	rychlost	k1gFnSc1	rychlost
dominantně	dominantně	k6eAd1	dominantně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Korozní	korozní	k2eAgNnPc4d1	korozní
napadení	napadení	k1gNnPc4	napadení
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Mezikrystalová	Mezikrystalový	k2eAgFnSc1d1	Mezikrystalový
koroze	koroze	k1gFnSc1	koroze
===	===	k?	===
</s>
</p>
<p>
<s>
Korozivzdorné	korozivzdorný	k2eAgFnPc1d1	korozivzdorná
oceli	ocel	k1gFnPc1	ocel
mají	mít	k5eAaImIp3nP	mít
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
citlivost	citlivost	k1gFnSc4	citlivost
k	k	k7c3	k
mezikrystalové	mezikrystal	k1gMnPc1	mezikrystal
korozi	koroze	k1gFnSc4	koroze
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vylučování	vylučování	k1gNnSc4	vylučování
karbidů	karbid	k1gInPc2	karbid
chrómu	chróm	k1gInSc2	chróm
(	(	kIx(	(
<g/>
např.	např.	kA	např.
M	M	kA	M
<g/>
23	[number]	k4	23
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
7	[number]	k4	7
<g/>
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
zrn	zrno	k1gNnPc2	zrno
odebíráním	odebírání	k1gNnSc7	odebírání
chrómu	chróm	k1gInSc2	chróm
z	z	k7c2	z
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
oblastí	oblast	k1gFnPc2	oblast
až	až	k9	až
klesne	klesnout	k5eAaPmIp3nS	klesnout
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
pod	pod	k7c4	pod
minimální	minimální	k2eAgFnSc4d1	minimální
mez	mez	k1gFnSc4	mez
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yRgFnSc7	který
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
koroze	koroze	k1gFnSc2	koroze
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
<g/>
,	,	kIx,	,
octové	octový	k2eAgFnSc2d1	octová
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgFnSc2d1	mléčná
nebo	nebo	k8xC	nebo
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
či	či	k8xC	či
chlorovaných	chlorovaný	k2eAgNnPc2d1	chlorované
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
precipitaci	precipitace	k1gFnSc3	precipitace
karbidů	karbid	k1gInPc2	karbid
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
tepelném	tepelný	k2eAgNnSc6d1	tepelné
zpracování	zpracování	k1gNnSc6	zpracování
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
400	[number]	k4	400
až	až	k9	až
800	[number]	k4	800
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zcitlivění	zcitlivění	k1gNnSc1	zcitlivění
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Homogenní	homogenní	k2eAgInSc1d1	homogenní
tuhý	tuhý	k2eAgInSc1d1	tuhý
roztok	roztok	k1gInSc1	roztok
a	a	k8xC	a
tedy	tedy	k9	tedy
zpětné	zpětný	k2eAgNnSc4d1	zpětné
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
karbidů	karbid	k1gInPc2	karbid
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
rozpouštěcím	rozpouštěcí	k2eAgNnPc3d1	rozpouštěcí
žíháním	žíhání	k1gNnPc3	žíhání
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1000	[number]	k4	1000
až	až	k9	až
1150	[number]	k4	1150
°	°	k?	°
<g/>
C.	C.	kA	C.
Pro	pro	k7c4	pro
znecitlivění	znecitlivění	k1gNnSc4	znecitlivění
ocelí	ocel	k1gFnPc2	ocel
k	k	k7c3	k
mezikrystalové	mezikrystal	k1gMnPc1	mezikrystal
korozi	koroze	k1gFnSc3	koroze
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
titanu	titan	k1gInSc2	titan
nebo	nebo	k8xC	nebo
niobu	niob	k1gInSc2	niob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přednostně	přednostně	k6eAd1	přednostně
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
karbidy	karbid	k1gInPc7	karbid
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
snižuje	snižovat	k5eAaImIp3nS	snižovat
obsah	obsah	k1gInSc4	obsah
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
vznik	vznik	k1gInSc4	vznik
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
karbidů	karbid	k1gInPc2	karbid
chrómu	chróm	k1gInSc2	chróm
<g/>
.	.	kIx.	.
</s>
<s>
Oceli	ocel	k1gFnPc1	ocel
legované	legovaný	k2eAgFnPc1d1	legovaná
titanem	titan	k1gInSc7	titan
nebo	nebo	k8xC	nebo
niobem	niob	k1gInSc7	niob
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývají	nazývat	k5eAaImIp3nP	nazývat
stabilizované	stabilizovaný	k2eAgInPc1d1	stabilizovaný
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
i	i	k9	i
ke	k	k7c3	k
svařování	svařování	k1gNnSc3	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
ocelí	ocel	k1gFnPc2	ocel
vystavených	vystavený	k2eAgFnPc2d1	vystavená
vyšším	vysoký	k2eAgNnSc7d2	vyšší
teplotám	teplota	k1gFnPc3	teplota
vůči	vůči	k7c3	vůči
mezikrystalové	mezikrystal	k1gMnPc1	mezikrystal
korozi	koroze	k1gFnSc4	koroze
lze	lze	k6eAd1	lze
též	též	k9	též
zvýšit	zvýšit	k5eAaPmF	zvýšit
snížením	snížení	k1gNnSc7	snížení
obsahu	obsah	k1gInSc2	obsah
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
závislost	závislost	k1gFnSc4	závislost
popisují	popisovat	k5eAaImIp3nP	popisovat
tzv.	tzv.	kA	tzv.
Rolasonovy	Rolasonův	k2eAgFnPc4d1	Rolasonův
křivky	křivka	k1gFnPc4	křivka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bodová	bodový	k2eAgFnSc1d1	bodová
koroze	koroze	k1gFnSc1	koroze
===	===	k?	===
</s>
</p>
<p>
<s>
Bodová	bodový	k2eAgFnSc1d1	bodová
koroze	koroze	k1gFnSc1	koroze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
pitting	pitting	k1gInSc1	pitting
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
lokálním	lokální	k2eAgNnSc6d1	lokální
porušení	porušení	k1gNnSc6	porušení
pasivní	pasivní	k2eAgFnSc2d1	pasivní
vrstvy	vrstva	k1gFnSc2	vrstva
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
nehomogennostmi	nehomogennost	k1gFnPc7	nehomogennost
a	a	k8xC	a
vadami	vada	k1gFnPc7	vada
jakými	jaký	k3yRgNnPc7	jaký
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
nekovové	kovový	k2eNgInPc1d1	nekovový
vměstky	vměstek	k1gInPc4	vměstek
MnS	MnS	k1gMnPc2	MnS
<g/>
,	,	kIx,	,
FeS	fes	k1gNnPc2	fes
a	a	k8xC	a
CaS	CaS	k1gFnPc2	CaS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drobných	drobný	k2eAgInPc6d1	drobný
ale	ale	k8xC	ale
hlubokých	hluboký	k2eAgInPc6d1	hluboký
důlcích	důlek	k1gInPc6	důlek
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
koncentrace	koncentrace	k1gFnSc1	koncentrace
agresivních	agresivní	k2eAgInPc2d1	agresivní
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dále	daleko	k6eAd2	daleko
prohlubují	prohlubovat	k5eAaImIp3nP	prohlubovat
korozní	korozní	k2eAgInSc4d1	korozní
důlek	důlek	k1gInSc4	důlek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
odolnosti	odolnost	k1gFnSc2	odolnost
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
obsah	obsah	k1gInSc1	obsah
chrómu	chróm	k1gInSc2	chróm
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
molybden	molybden	k1gInSc1	molybden
či	či	k8xC	či
dusík	dusík	k1gInSc1	dusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
bodové	bodový	k2eAgFnSc3d1	bodová
(	(	kIx(	(
<g/>
pittingové	pittingový	k2eAgFnSc3d1	pittingový
<g/>
)	)	kIx)	)
korozi	koroze	k1gFnSc3	koroze
lze	lze	k6eAd1	lze
empiricky	empiricky	k6eAd1	empiricky
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
následujícím	následující	k2eAgInSc7d1	následující
indexem	index	k1gInSc7	index
PRE	PRE	kA	PRE
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
austenitické	austenitický	k2eAgFnPc4d1	austenitická
oceli	ocel	k1gFnPc4	ocel
jako	jako	k8xC	jako
PRE	PRE	kA	PRE
=	=	kIx~	=
Cr	cr	k0	cr
+	+	kIx~	+
3,3	[number]	k4	3,3
<g/>
×	×	k?	×
<g/>
Mo	Mo	k1gFnSc1	Mo
+	+	kIx~	+
16	[number]	k4	16
<g/>
×	×	k?	×
<g/>
N	N	kA	N
[	[	kIx(	[
<g/>
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
austeniticko-feritické	austenitickoeritický	k2eAgFnPc4d1	austeniticko-feritický
oceli	ocel	k1gFnPc4	ocel
jako	jako	k8xC	jako
PRE	PRE	kA	PRE
=	=	kIx~	=
Cr	cr	k0	cr
+	+	kIx~	+
3,3	[number]	k4	3,3
<g/>
×	×	k?	×
<g/>
Mo	Mo	k1gFnSc1	Mo
+	+	kIx~	+
30	[number]	k4	30
<g/>
×	×	k?	×
<g/>
N	N	kA	N
[	[	kIx(	[
<g/>
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Štěrbinová	štěrbinový	k2eAgFnSc1d1	štěrbinová
koroze	koroze	k1gFnSc1	koroze
===	===	k?	===
</s>
</p>
<p>
<s>
Štěrbinová	štěrbinový	k2eAgFnSc1d1	štěrbinová
koroze	koroze	k1gFnSc1	koroze
má	mít	k5eAaImIp3nS	mít
podobný	podobný	k2eAgInSc4d1	podobný
charakter	charakter	k1gInSc4	charakter
jako	jako	k8xS	jako
bodová	bodový	k2eAgFnSc1d1	bodová
koroze	koroze	k1gFnSc1	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
vznik	vznik	k1gInSc4	vznik
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
kvalita	kvalita	k1gFnSc1	kvalita
pasivní	pasivní	k2eAgFnSc2d1	pasivní
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
geometricky	geometricky	k6eAd1	geometricky
nevhodný	vhodný	k2eNgInSc4d1	nevhodný
tvar	tvar	k1gInSc4	tvar
např.	např.	kA	např.
ve	v	k7c6	v
styčné	styčný	k2eAgFnSc6d1	styčná
spáře	spára	k1gFnSc6	spára
šroubového	šroubový	k2eAgInSc2d1	šroubový
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koroze	koroze	k1gFnPc1	koroze
za	za	k7c4	za
napětí	napětí	k1gNnSc4	napětí
===	===	k?	===
</s>
</p>
<p>
<s>
Koroze	koroze	k1gFnSc1	koroze
za	za	k7c4	za
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
korozní	korozní	k2eAgNnSc4d1	korozní
praskání	praskání	k1gNnSc4	praskání
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
v	v	k7c6	v
málo	málo	k6eAd1	málo
agresivním	agresivní	k2eAgNnSc6d1	agresivní
prostředí	prostředí	k1gNnSc6	prostředí
obsahující	obsahující	k2eAgInPc4d1	obsahující
chloridy	chlorid	k1gInPc4	chlorid
a	a	k8xC	a
hydroxidy	hydroxid	k1gInPc4	hydroxid
a	a	k8xC	a
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
mechanického	mechanický	k2eAgNnSc2d1	mechanické
namáhání	namáhání	k1gNnSc2	namáhání
(	(	kIx(	(
<g/>
napětí	napětí	k1gNnSc2	napětí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
austenitické	austenitický	k2eAgFnPc4d1	austenitická
a	a	k8xC	a
duplexní	duplexní	k2eAgFnPc4d1	duplexní
oceli	ocel	k1gFnPc4	ocel
je	být	k5eAaImIp3nS	být
agresivní	agresivní	k2eAgNnSc1d1	agresivní
prostředí	prostředí	k1gNnSc1	prostředí
tvořené	tvořený	k2eAgFnSc2d1	tvořená
např.	např.	kA	např.
MgCl	MgCl	k1gInSc4	MgCl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
BaCl	BaCl	k1gInSc1	BaCl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
NaCl	NaCl	k1gInSc1	NaCl
–	–	k?	–
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
NaOH	NaOH	k1gFnSc1	NaOH
-	-	kIx~	-
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
feritické	feritický	k2eAgInPc4d1	feritický
pak	pak	k6eAd1	pak
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
Cl	Cl	k1gFnSc2	Cl
<g/>
,	,	kIx,	,
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mechanického	mechanický	k2eAgNnSc2d1	mechanické
namáhání	namáhání	k1gNnSc2	namáhání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
původcem	původce	k1gMnSc7	původce
napětí	napětí	k1gNnSc4	napětí
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
reziduální	reziduální	k2eAgNnSc1d1	reziduální
napětí	napětí	k1gNnSc1	napětí
způsobené	způsobený	k2eAgNnSc1d1	způsobené
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
válcováním	válcování	k1gNnSc7	válcování
nebo	nebo	k8xC	nebo
svařováním	svařování	k1gNnSc7	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
rizika	riziko	k1gNnSc2	riziko
korozního	korozní	k2eAgNnSc2d1	korozní
praskání	praskání	k1gNnSc2	praskání
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
snížit	snížit	k5eAaPmF	snížit
reziduální	reziduální	k2eAgNnSc4d1	reziduální
pnutí	pnutí	k1gNnSc4	pnutí
<g/>
,	,	kIx,	,
snížit	snížit	k5eAaPmF	snížit
agresivitu	agresivita	k1gFnSc4	agresivita
prostředí	prostředí	k1gNnSc2	prostředí
případně	případně	k6eAd1	případně
zvýšit	zvýšit	k5eAaPmF	zvýšit
obsah	obsah	k1gInSc4	obsah
molybdenu	molybden	k1gInSc2	molybden
na	na	k7c4	na
6	[number]	k4	6
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galvanická	galvanický	k2eAgFnSc1d1	Galvanická
(	(	kIx(	(
<g/>
bimetalická	bimetalický	k2eAgFnSc1d1	bimetalická
<g/>
)	)	kIx)	)
koroze	koroze	k1gFnSc1	koroze
===	===	k?	===
</s>
</p>
<p>
<s>
Galvanická	galvanický	k2eAgFnSc1d1	Galvanická
koroze	koroze	k1gFnSc1	koroze
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
dvou	dva	k4xCgFnPc2	dva
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
kovů	kov	k1gInPc2	kov
vodivým	vodivý	k2eAgInSc7d1	vodivý
elektrolytem	elektrolyt	k1gInSc7	elektrolyt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kondenzací	kondenzace	k1gFnPc2	kondenzace
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kov	kov	k1gInSc1	kov
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
korozní	korozní	k2eAgFnSc7d1	korozní
ušlechtilostí	ušlechtilost	k1gFnSc7	ušlechtilost
(	(	kIx(	(
<g/>
anoda	anoda	k1gFnSc1	anoda
<g/>
)	)	kIx)	)
koroduje	korodovat	k5eAaImIp3nS	korodovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
zabráněno	zabráněn	k2eAgNnSc1d1	zabráněno
vodivému	vodivý	k2eAgNnSc3d1	vodivé
propojení	propojení	k1gNnSc3	propojení
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
šroubové	šroubový	k2eAgInPc1d1	šroubový
spoje	spoj	k1gInPc1	spoj
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
spojují	spojovat	k5eAaImIp3nP	spojovat
části	část	k1gFnPc4	část
z	z	k7c2	z
korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
šrouby	šroub	k1gInPc4	šroub
z	z	k7c2	z
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
spoje	spoj	k1gInPc4	spoj
z	z	k7c2	z
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
oceli	ocel	k1gFnSc2	ocel
budou	být	k5eAaImBp3nP	být
korodovat	korodovat	k5eAaImF	korodovat
masivněji	masivně	k6eAd2	masivně
než	než	k8xS	než
u	u	k7c2	u
spoje	spoj	k1gInSc2	spoj
celého	celý	k2eAgInSc2d1	celý
z	z	k7c2	z
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
je	být	k5eAaImIp3nS	být
vysocelegovaná	vysocelegovaný	k2eAgFnSc1d1	vysocelegovaný
ocel	ocel	k1gFnSc1	ocel
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
chemické	chemický	k2eAgFnSc3d1	chemická
i	i	k8xC	i
elektrochemické	elektrochemický	k2eAgFnSc3d1	elektrochemická
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korozní	korozní	k2eAgFnSc1d1	korozní
odolnost	odolnost	k1gFnSc1	odolnost
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
schopnosti	schopnost	k1gFnPc4	schopnost
tzv.	tzv.	kA	tzv.
pasivace	pasivace	k1gFnSc1	pasivace
povrchu	povrch	k1gInSc2	povrch
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
oxidů	oxid	k1gInPc2	oxid
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
mm	mm	kA	mm
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
mm	mm	kA	mm
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pasivity	pasivita	k1gFnPc4	pasivita
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
již	již	k6eAd1	již
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
nad	nad	k7c7	nad
13	[number]	k4	13
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
resp.	resp.	kA	resp.
nad	nad	k7c7	nad
12	[number]	k4	12
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
resp.	resp.	kA	resp.
nad	nad	k7c7	nad
10,5	[number]	k4	10,5
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
v	v	k7c6	v
tuhém	tuhý	k2eAgInSc6d1	tuhý
roztoku	roztok	k1gInSc6	roztok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
vrstva	vrstva	k1gFnSc1	vrstva
oxidu	oxid	k1gInSc2	oxid
chromitého	chromitý	k2eAgInSc2d1	chromitý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neplatí	platit	k5eNaImIp3nS	platit
pro	pro	k7c4	pro
vysocekorozivní	vysocekorozivní	k2eAgNnSc4d1	vysocekorozivní
prostředí	prostředí	k1gNnSc4	prostředí
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
nebo	nebo	k8xC	nebo
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pasivita	pasivita	k1gFnSc1	pasivita
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
vůči	vůči	k7c3	vůči
celkové	celkový	k2eAgFnSc3d1	celková
korozi	koroze	k1gFnSc3	koroze
dosažena	dosažen	k2eAgFnSc1d1	dosažena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
prostředích	prostředí	k1gNnPc6	prostředí
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
specifických	specifický	k2eAgNnPc6d1	specifické
prostředích	prostředí	k1gNnPc6	prostředí
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
různých	různý	k2eAgInPc2d1	různý
lokálních	lokální	k2eAgInPc2d1	lokální
druhů	druh	k1gInPc2	druh
koroze	koroze	k1gFnSc1	koroze
–	–	k?	–
štěrbinovou	štěrbinový	k2eAgFnSc7d1	štěrbinová
<g/>
,	,	kIx,	,
bodovou	bodový	k2eAgFnSc7d1	bodová
<g/>
,	,	kIx,	,
mezikrystalovou	mezikrystalův	k2eAgFnSc7d1	mezikrystalův
a	a	k8xC	a
korozním	korozní	k2eAgNnSc7d1	korozní
praskáním	praskání	k1gNnSc7	praskání
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
chrómu	chróm	k1gInSc2	chróm
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
korozní	korozní	k2eAgFnSc4d1	korozní
odolnost	odolnost	k1gFnSc4	odolnost
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
typ	typ	k1gInSc4	typ
koroze	koroze	k1gFnSc1	koroze
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nikl	nikl	k1gInSc1	nikl
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
Korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
feritické	feritický	k2eAgFnSc2d1	feritická
<g/>
,	,	kIx,	,
martenzitické	martenzitický	k2eAgFnSc2d1	martenzitická
<g/>
,	,	kIx,	,
austenitické	austenitický	k2eAgFnSc2d1	austenitická
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
přechodové	přechodový	k2eAgFnSc2d1	přechodová
skupiny	skupina	k1gFnSc2	skupina
feriticko-austenitické	feritickoustenitický	k2eAgFnSc2d1	feriticko-austenitický
<g/>
,	,	kIx,	,
martenziticko-austenitické	martenzitickoustenitický	k2eAgFnSc2d1	martenziticko-austenitický
a	a	k8xC	a
poloferitické	poloferitický	k2eAgFnSc2d1	poloferitický
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
korozivzdorné	korozivzdorný	k2eAgFnPc1d1	korozivzdorná
oceli	ocel	k1gFnPc1	ocel
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
legur	legura	k1gFnPc2	legura
<g/>
,	,	kIx,	,
12	[number]	k4	12
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
nebo	nebo	k8xC	nebo
do	do	k7c2	do
24	[number]	k4	24
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
slitinu	slitina	k1gFnSc4	slitina
uhlíku	uhlík	k1gInSc2	uhlík
s	s	k7c7	s
železem	železo	k1gNnSc7	železo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ocel	ocel	k1gFnSc1	ocel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
odhadu	odhad	k1gInSc2	odhad
struktury	struktura	k1gFnSc2	struktura
korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
chróm-niklové	chrómiklový	k2eAgFnSc2d1	chróm-niklový
oceli	ocel	k1gFnSc2	ocel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
Shaefflerův	Shaefflerův	k2eAgInSc1d1	Shaefflerův
a	a	k8xC	a
De	De	k?	De
Longův	Longův	k2eAgInSc4d1	Longův
diagram	diagram	k1gInSc4	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
binární	binární	k2eAgInSc1d1	binární
diagram	diagram	k1gInSc1	diagram
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
vlivy	vliv	k1gInPc4	vliv
feritotvorných	feritotvorný	k2eAgInPc2d1	feritotvorný
prvků	prvek	k1gInPc2	prvek
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
%	%	kIx~	%
<g/>
Creq	Creq	k1gFnPc2	Creq
a	a	k8xC	a
austenitotvorných	austenitotvorný	k2eAgInPc2d1	austenitotvorný
prvků	prvek	k1gInPc2	prvek
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
%	%	kIx~	%
<g/>
Nieq	Nieq	k1gFnSc1	Nieq
na	na	k7c4	na
výslednou	výsledný	k2eAgFnSc4d1	výsledná
strukturu	struktura	k1gFnSc4	struktura
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
%	%	kIx~	%
<g/>
Creq	Creq	k1gFnPc1	Creq
a	a	k8xC	a
%	%	kIx~	%
<g/>
Nieq	Nieq	k1gFnPc1	Nieq
jsou	být	k5eAaImIp3nP	být
počítány	počítat	k5eAaImNgFnP	počítat
podle	podle	k7c2	podle
vztahů	vztah	k1gInPc2	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
%	%	kIx~	%
<g/>
Creq	Creq	k1gMnPc5	Creq
=	=	kIx~	=
Cr	cr	k0	cr
+	+	kIx~	+
Mo	Mo	k1gMnSc6	Mo
+	+	kIx~	+
1,5	[number]	k4	1,5
Si	se	k3xPyFc3	se
+	+	kIx~	+
0,5	[number]	k4	0,5
Nb	Nb	k1gFnPc2	Nb
[	[	kIx(	[
<g/>
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
%	%	kIx~	%
<g/>
Nieq	Nieq	k1gMnSc2	Nieq
=	=	kIx~	=
Ni	on	k3xPp3gFnSc4	on
+	+	kIx~	+
30	[number]	k4	30
C	C	kA	C
+	+	kIx~	+
0.5	[number]	k4	0.5
Mn	Mn	k1gFnPc2	Mn
[	[	kIx(	[
<g/>
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
resp.	resp.	kA	resp.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
%	%	kIx~	%
<g/>
Creq	Creq	k1gMnPc5	Creq
=	=	kIx~	=
Cr	cr	k0	cr
+	+	kIx~	+
2	[number]	k4	2
Mo	Mo	k1gFnPc2	Mo
+	+	kIx~	+
1,5	[number]	k4	1,5
Si	se	k3xPyFc3	se
+	+	kIx~	+
0,5	[number]	k4	0,5
Nb	Nb	k1gFnPc2	Nb
+	+	kIx~	+
0,5	[number]	k4	0,5
Ti	ten	k3xDgMnPc1	ten
[	[	kIx(	[
<g/>
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
%	%	kIx~	%
<g/>
Nieq	Nieq	k1gMnSc2	Nieq
=	=	kIx~	=
Ni	on	k3xPp3gFnSc4	on
+	+	kIx~	+
30	[number]	k4	30
C	C	kA	C
+	+	kIx~	+
30	[number]	k4	30
N	N	kA	N
+	+	kIx~	+
0.5	[number]	k4	0.5
Mn	Mn	k1gFnPc2	Mn
[	[	kIx(	[
<g/>
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
výpočty	výpočet	k1gInPc4	výpočet
ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
chrómu	chróm	k1gInSc2	chróm
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
Schaefflerova	Schaefflerův	k2eAgInSc2d1	Schaefflerův
diagramu	diagram	k1gInSc2	diagram
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc2	množství
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
použitých	použitý	k2eAgFnPc2d1	použitá
legur	legura	k1gFnPc2	legura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
upravený	upravený	k2eAgInSc1d1	upravený
Shaefflerův	Shaefflerův	k2eAgInSc1d1	Shaefflerův
diagram	diagram	k1gInSc1	diagram
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
WRC	WRC	kA	WRC
-	-	kIx~	-
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gMnPc7	druh
korozivzdorných	korozivzdorný	k2eAgFnPc2d1	korozivzdorná
ocelí	ocel	k1gFnPc2	ocel
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Austenitická	austenitický	k2eAgFnSc1d1	austenitická
korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
===	===	k?	===
</s>
</p>
<p>
<s>
Austenitické	austenitický	k2eAgFnPc1d1	austenitická
korozivzdorné	korozivzdorný	k2eAgFnPc1d1	korozivzdorná
oceli	ocel	k1gFnPc1	ocel
obvykle	obvykle	k6eAd1	obvykle
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,10	[number]	k4	0,10
%	%	kIx~	%
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
%	%	kIx~	%
molybdenu	molybden	k1gInSc2	molybden
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
niob	niob	k1gInSc1	niob
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
nebo	nebo	k8xC	nebo
křemík	křemík	k1gInSc1	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Austenitické	austenitický	k2eAgFnPc1d1	austenitická
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
podle	podle	k7c2	podle
austenitické	austenitický	k2eAgFnSc2d1	austenitická
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
i	i	k9	i
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Austenitickou	austenitický	k2eAgFnSc4d1	austenitická
strukturu	struktura	k1gFnSc4	struktura
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dostatek	dostatek	k1gInSc4	dostatek
tzv.	tzv.	kA	tzv.
austenitotvorných	austenitotvorný	k2eAgInPc2d1	austenitotvorný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nikl	nikl	k1gInSc4	nikl
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc4	mangan
<g/>
,	,	kIx,	,
uhlík	uhlík	k1gInSc4	uhlík
a	a	k8xC	a
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
<g/>
Mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
mez	mez	k1gFnSc4	mez
kluzu	kluz	k1gInSc2	kluz
230	[number]	k4	230
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
MPa	MPa	k1gFnPc2	MPa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vysokou	vysoký	k2eAgFnSc4d1	vysoká
houževnatost	houževnatost	k1gFnSc4	houževnatost
až	až	k9	až
240	[number]	k4	240
J.	J.	kA	J.
<g/>
cm	cm	kA	cm
<g/>
2	[number]	k4	2
při	při	k7c6	při
-196	-196	k4	-196
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
tažnost	tažnost	k1gFnSc1	tažnost
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
%	%	kIx~	%
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yRgFnSc3	který
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
ke	k	k7c3	k
tváření	tváření	k1gNnSc3	tváření
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
meze	mez	k1gFnPc4	mez
kluzu	kluz	k1gInSc2	kluz
510	[number]	k4	510
<g/>
–	–	k?	–
<g/>
960	[number]	k4	960
MPa	MPa	k1gFnSc6	MPa
avšak	avšak	k8xC	avšak
při	při	k7c6	při
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
snížení	snížení	k1gNnSc6	snížení
tažnosti	tažnost	k1gFnSc2	tažnost
na	na	k7c4	na
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Jsou	být	k5eAaImIp3nP	být
nemagnetické	magnetický	k2eNgFnPc1d1	nemagnetická
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ale	ale	k8xC	ale
zbytkový	zbytkový	k2eAgInSc4d1	zbytkový
obsah	obsah	k1gInSc4	obsah
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
feritu	ferit	k1gInSc2	ferit
δ	δ	k?	δ
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
slabý	slabý	k2eAgInSc4d1	slabý
feromagnetismus	feromagnetismus	k1gInSc4	feromagnetismus
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
korozivzdornosti	korozivzdornost	k1gFnSc2	korozivzdornost
odolávají	odolávat	k5eAaImIp3nP	odolávat
celkové	celkový	k2eAgFnSc3d1	celková
korozi	koroze	k1gFnSc3	koroze
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
molybdenu	molybden	k1gInSc2	molybden
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
křemíku	křemík	k1gInSc2	křemík
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
mezikrystalové	mezikrystal	k1gMnPc1	mezikrystal
korozi	koroze	k1gFnSc3	koroze
stabilizací	stabilizace	k1gFnPc2	stabilizace
titanem	titan	k1gInSc7	titan
případně	případně	k6eAd1	případně
niobem	niob	k1gInSc7	niob
<g/>
,	,	kIx,	,
bodové	bodový	k2eAgInPc4d1	bodový
a	a	k8xC	a
štěrbinové	štěrbinový	k2eAgInPc4d1	štěrbinový
korozi	koroze	k1gFnSc4	koroze
molybdenem	molybden	k1gInSc7	molybden
<g/>
,	,	kIx,	,
křemíkem	křemík	k1gInSc7	křemík
a	a	k8xC	a
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
koroznímu	korozní	k2eAgNnSc3d1	korozní
praskání	praskání	k1gNnSc3	praskání
omezením	omezení	k1gNnSc7	omezení
obsahu	obsah	k1gInSc2	obsah
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
arzénu	arzén	k1gInSc2	arzén
<g/>
,	,	kIx,	,
antimonu	antimon	k1gInSc2	antimon
popř.	popř.	kA	popř.
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
náchylné	náchylný	k2eAgFnPc1d1	náchylná
ke	k	k7c3	k
korozi	koroze	k1gFnSc3	koroze
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
<g/>
Austenitických	austenitický	k2eAgFnPc2d1	austenitická
ocelí	ocel	k1gFnPc2	ocel
se	se	k3xPyFc4	se
však	však	k9	však
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
desítky	desítka	k1gFnPc1	desítka
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
třech	tři	k4xCgFnPc6	tři
základních	základní	k2eAgFnPc6d1	základní
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
chróm-niklové	chrómikl	k1gMnPc1	chróm-nikl
oceli	ocel	k1gFnSc2	ocel
s	s	k7c7	s
0,01	[number]	k4	0,01
<g/>
–	–	k?	–
<g/>
0,15	[number]	k4	0,15
%	%	kIx~	%
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
s	s	k7c7	s
případnými	případný	k2eAgFnPc7d1	případná
dalšími	další	k2eAgFnPc7d1	další
legurami	legura	k1gFnPc7	legura
-	-	kIx~	-
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
a	a	k8xC	a
stabilizované	stabilizovaný	k2eAgFnPc1d1	stabilizovaná
titanem	titan	k1gInSc7	titan
a	a	k8xC	a
niobem	niob	k1gInSc7	niob
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
chróm-mangan-niklové	chrómanganikl	k1gMnPc1	chróm-mangan-nikl
s	s	k7c7	s
0,02	[number]	k4	0,02
<g/>
–	–	k?	–
<g/>
0,15	[number]	k4	0,15
%	%	kIx~	%
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
s	s	k7c7	s
případnými	případný	k2eAgFnPc7d1	případná
dalšími	další	k2eAgFnPc7d1	další
legurami	legura	k1gFnPc7	legura
-	-	kIx~	-
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
a	a	k8xC	a
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
stabilizované	stabilizovaný	k2eAgFnSc2d1	stabilizovaná
titanem	titan	k1gInSc7	titan
a	a	k8xC	a
niobem	niob	k1gInSc7	niob
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
chróm-manganové	chrómangan	k1gMnPc1	chróm-mangan
s	s	k7c7	s
0,02	[number]	k4	0,02
<g/>
–	–	k?	–
<g/>
0,08	[number]	k4	0,08
%	%	kIx~	%
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
s	s	k7c7	s
případnými	případný	k2eAgFnPc7d1	případná
dalšími	další	k2eAgFnPc7d1	další
legurami	legura	k1gFnPc7	legura
-	-	kIx~	-
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
a	a	k8xC	a
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
stabilizované	stabilizovaný	k2eAgFnSc2d1	stabilizovaná
titanem	titan	k1gInSc7	titan
a	a	k8xC	a
niobem	niob	k1gInSc7	niob
<g/>
.	.	kIx.	.
<g/>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
oceli	ocel	k1gFnPc4	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
0,08	[number]	k4	0,08
%	%	kIx~	%
<g/>
,	,	kIx,	,
18	[number]	k4	18
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
a	a	k8xC	a
8	[number]	k4	8
%	%	kIx~	%
resp.	resp.	kA	resp.
10	[number]	k4	10
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oceli	ocel	k1gFnPc1	ocel
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označují	označovat	k5eAaImIp3nP	označovat
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
resp.	resp.	kA	resp.
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobrá	dobrý	k2eAgFnSc1d1	dobrá
korozní	korozní	k2eAgFnSc1d1	korozní
odolnost	odolnost	k1gFnSc1	odolnost
vůči	vůči	k7c3	vůči
roztokům	roztok	k1gInPc3	roztok
solí	solit	k5eAaImIp3nS	solit
<g/>
,	,	kIx,	,
masu	maso	k1gNnSc6	maso
a	a	k8xC	a
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
vhodně	vhodně	k6eAd1	vhodně
čištěny	čištěn	k2eAgInPc1d1	čištěn
(	(	kIx(	(
<g/>
např.	např.	kA	např.
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
mechanické	mechanický	k2eAgNnSc1d1	mechanické
drhnutí	drhnutí	k1gNnSc1	drhnutí
<g/>
,	,	kIx,	,
detergenty	detergent	k1gInPc1	detergent
<g/>
,	,	kIx,	,
alkalické	alkalický	k2eAgInPc1d1	alkalický
roztoky	roztok	k1gInPc1	roztok
<g/>
,	,	kIx,	,
organická	organický	k2eAgNnPc1d1	organické
rozpouštědla	rozpouštědlo	k1gNnPc1	rozpouštědlo
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Martenzitická	martenzitický	k2eAgFnSc1d1	martenzitická
korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
===	===	k?	===
</s>
</p>
<p>
<s>
Martenzitická	martenzitický	k2eAgFnSc1d1	martenzitická
korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
chrómu	chróm	k1gInSc2	chróm
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
18	[number]	k4	18
%	%	kIx~	%
a	a	k8xC	a
až	až	k9	až
1,5	[number]	k4	1,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
1,2	[number]	k4	1,2
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
)	)	kIx)	)
uhlíku	uhlík	k1gInSc2	uhlík
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgNnPc4d1	schopno
zakalení	zakalení	k1gNnPc4	zakalení
z	z	k7c2	z
austenitizační	austenitizační	k2eAgFnSc2d1	austenitizační
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nad	nad	k7c7	nad
linií	linie	k1gFnSc7	linie
GSE	GSE	kA	GSE
v	v	k7c6	v
binárním	binární	k2eAgInSc6d1	binární
diagramu	diagram	k1gInSc6	diagram
železo-uhlík	železohlík	k1gInSc1	železo-uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Chróm	chróm	k1gInSc1	chróm
jako	jako	k8xS	jako
feritotvorný	feritotvorný	k2eAgInSc1d1	feritotvorný
prvek	prvek	k1gInSc1	prvek
také	také	k9	také
snižuje	snižovat	k5eAaImIp3nS	snižovat
kritickou	kritický	k2eAgFnSc4d1	kritická
rychlost	rychlost	k1gFnSc4	rychlost
ochlazování	ochlazování	k1gNnSc3	ochlazování
při	při	k7c6	při
anizotropickém	anizotropický	k2eAgInSc6d1	anizotropický
rozpadu	rozpad	k1gInSc6	rozpad
austenitu	austenit	k1gInSc2	austenit
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zakalení	zakalení	k1gNnSc3	zakalení
i	i	k8xC	i
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Oceli	ocel	k1gFnPc1	ocel
lze	lze	k6eAd1	lze
po	po	k7c6	po
kalení	kalení	k1gNnSc6	kalení
i	i	k9	i
vyžíhat	vyžíhat	k5eAaBmF	vyžíhat
mezi	mezi	k7c7	mezi
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
750	[number]	k4	750
°	°	k?	°
<g/>
C	C	kA	C
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
feritické	feritický	k2eAgFnSc2d1	feritická
struktury	struktura	k1gFnSc2	struktura
s	s	k7c7	s
karbidy	karbid	k1gInPc7	karbid
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
požadována	požadován	k2eAgFnSc1d1	požadována
houževnatost	houževnatost	k1gFnSc1	houževnatost
nebo	nebo	k8xC	nebo
tažnost	tažnost	k1gFnSc1	tažnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
holicích	holicí	k2eAgFnPc2d1	holicí
čepelek	čepelka	k1gFnPc2	čepelka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zušlechťováním	zušlechťování	k1gNnSc7	zušlechťování
(	(	kIx(	(
<g/>
kalením	kalení	k1gNnSc7	kalení
a	a	k8xC	a
popouštěním	popouštění	k1gNnSc7	popouštění
<g/>
)	)	kIx)	)
až	až	k9	až
2	[number]	k4	2
000	[number]	k4	000
MPa	MPa	k1gMnPc2	MPa
pevnosti	pevnost	k1gFnSc2	pevnost
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
<g/>
.	.	kIx.	.
</s>
<s>
Oceli	ocel	k1gFnPc1	ocel
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
precipitačně	precipitačně	k6eAd1	precipitačně
vytvrzené	vytvrzený	k2eAgNnSc1d1	vytvrzené
a	a	k8xC	a
zpevněné	zpevněný	k2eAgNnSc1d1	zpevněné
legováním	legování	k1gNnSc7	legování
mědí	měď	k1gFnPc2	měď
<g/>
,	,	kIx,	,
titanem	titan	k1gInSc7	titan
<g/>
,	,	kIx,	,
niobem	niob	k1gInSc7	niob
<g/>
,	,	kIx,	,
hliníkem	hliník	k1gInSc7	hliník
nebo	nebo	k8xC	nebo
molybdenem	molybden	k1gInSc7	molybden
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
uhlíku	uhlík	k1gInSc2	uhlík
do	do	k7c2	do
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Martenzitické	martenzitický	k2eAgFnPc1d1	martenzitická
oceli	ocel	k1gFnPc1	ocel
jsou	být	k5eAaImIp3nP	být
feromagnetické	feromagnetický	k2eAgFnPc1d1	feromagnetická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
používané	používaný	k2eAgNnSc1d1	používané
martenzitické	martenzitický	k2eAgNnSc1d1	martenzitický
korozivzdorné	korozivzdorný	k2eAgNnSc1d1	korozivzdorné
ocelí	ocelit	k5eAaImIp3nS	ocelit
–	–	k?	–
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dobré	dobrý	k2eAgFnPc4d1	dobrá
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
korozní	korozní	k2eAgFnSc4d1	korozní
odolnost	odolnost	k1gFnSc4	odolnost
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
uhlíku	uhlík	k1gInSc2	uhlík
do	do	k7c2	do
0,15	[number]	k4	0,15
%	%	kIx~	%
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
chrómu	chróm	k1gInSc2	chróm
11,5	[number]	k4	11,5
až	až	k9	až
13,5	[number]	k4	13,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nožířské	nožířský	k2eAgInPc4d1	nožířský
účely	účel	k1gInPc4	účel
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgFnPc4d1	používána
oceli	ocel	k1gFnPc4	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
od	od	k7c2	od
0,2	[number]	k4	0,2
do	do	k7c2	do
0,4	[number]	k4	0,4
%	%	kIx~	%
a	a	k8xC	a
chrómu	chróm	k1gInSc2	chróm
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
14,5	[number]	k4	14,5
%	%	kIx~	%
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
tažnosti	tažnost	k1gFnSc2	tažnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
tvrdosti	tvrdost	k1gFnSc2	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Oceli	ocel	k1gFnPc1	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
od	od	k7c2	od
0,6	[number]	k4	0,6
do	do	k7c2	do
1,5	[number]	k4	1,5
%	%	kIx~	%
a	a	k8xC	a
chrómu	chróm	k1gInSc2	chróm
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
18	[number]	k4	18
%	%	kIx~	%
sice	sice	k8xC	sice
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
tvrdosti	tvrdost	k1gFnPc4	tvrdost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
nižší	nízký	k2eAgFnSc4d2	nižší
korozivzdornost	korozivzdornost	k1gFnSc4	korozivzdornost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ocelí	ocel	k1gFnPc2	ocel
chrómniklových	chrómniklový	k2eAgFnPc2d1	chrómniklová
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
niklu	nikl	k1gInSc2	nikl
nad	nad	k7c7	nad
2	[number]	k4	2
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
zvýšen	zvýšit	k5eAaPmNgInS	zvýšit
obsah	obsah	k1gInSc1	obsah
chrómu	chróm	k1gInSc2	chróm
na	na	k7c4	na
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
%	%	kIx~	%
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
obsahu	obsah	k1gInSc6	obsah
uhlíku	uhlík	k1gInSc2	uhlík
anebo	anebo	k8xC	anebo
dalších	další	k2eAgInPc2d1	další
feritotvorných	feritotvorný	k2eAgInPc2d1	feritotvorný
prvků	prvek	k1gInPc2	prvek
např.	např.	kA	např.
molybdenu	molybden	k1gInSc2	molybden
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgNnSc7	takový
složením	složení	k1gNnSc7	složení
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
materiál	materiál	k1gInSc4	materiál
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
pevnosti	pevnost	k1gFnSc2	pevnost
ale	ale	k8xC	ale
i	i	k9	i
tažnosti	tažnost	k1gFnPc4	tažnost
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Oceli	ocel	k1gFnPc1	ocel
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
–	–	k?	–
do	do	k7c2	do
0,05	[number]	k4	0,05
%	%	kIx~	%
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
lze	lze	k6eAd1	lze
popouštět	popouštět	k5eAaImF	popouštět
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tak	tak	k6eAd1	tak
martenziticko-austenitickou	martenzitickoustenitický	k2eAgFnSc4d1	martenziticko-austenitický
strukturu	struktura	k1gFnSc4	struktura
s	s	k7c7	s
až	až	k6eAd1	až
30	[number]	k4	30
%	%	kIx~	%
austenitu	austenit	k1gInSc2	austenit
a	a	k8xC	a
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
svařitelností	svařitelnost	k1gFnSc7	svařitelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Feritická	feritický	k2eAgFnSc1d1	feritická
korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
===	===	k?	===
</s>
</p>
<p>
<s>
Feritické	feritický	k2eAgFnPc1d1	feritická
oceli	ocel	k1gFnPc1	ocel
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
strukturou	struktura	k1gFnSc7	struktura
feritu	ferit	k1gInSc2	ferit
α	α	k?	α
i	i	k8xC	i
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
běžného	běžný	k2eAgNnSc2d1	běžné
tepelného	tepelný	k2eAgNnSc2d1	tepelné
zpracování	zpracování	k1gNnSc2	zpracování
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
750	[number]	k4	750
do	do	k7c2	do
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nedosáhnou	dosáhnout	k5eNaPmIp3nP	dosáhnout
přeměny	přeměna	k1gFnPc4	přeměna
na	na	k7c4	na
austenit	austenit	k1gInSc4	austenit
a	a	k8xC	a
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgNnSc4d1	schopné
zakalení	zakalení	k1gNnSc4	zakalení
na	na	k7c4	na
martenzitickou	martenzitický	k2eAgFnSc4d1	martenzitická
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Takového	takový	k3xDgNnSc2	takový
chování	chování	k1gNnSc2	chování
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
chrómu	chróm	k1gInSc2	chróm
od	od	k7c2	od
17	[number]	k4	17
do	do	k7c2	do
26	[number]	k4	26
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
uhlíku	uhlík	k1gInSc2	uhlík
ve	v	k7c6	v
feritu	ferit	k1gInSc6	ferit
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
austenitu	austenit	k1gInSc6	austenit
<g/>
,	,	kIx,	,
uhlík	uhlík	k1gInSc1	uhlík
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
přesycené	přesycený	k2eAgFnSc6d1	přesycená
martenzitické	martenzitický	k2eAgFnSc6d1	martenzitická
formě	forma	k1gFnSc6	forma
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
vylučován	vylučovat	k5eAaImNgInS	vylučovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
karbidů	karbid	k1gInPc2	karbid
chrómu	chróm	k1gInSc2	chróm
typu	typ	k1gInSc2	typ
M	M	kA	M
<g/>
23	[number]	k4	23
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
chróm	chróm	k1gInSc1	chróm
navíc	navíc	k6eAd1	navíc
tvoří	tvořit	k5eAaImIp3nS	tvořit
i	i	k9	i
nitridy	nitrid	k1gInPc4	nitrid
Cr	cr	k0	cr
<g/>
2	[number]	k4	2
<g/>
N.	N.	kA	N.
</s>
</p>
<p>
<s>
Feritické	feritický	k2eAgFnSc2d1	feritická
korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
oceli	ocel	k1gFnPc4	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
chrómu	chróm	k1gInSc2	chróm
od	od	k7c2	od
11,5	[number]	k4	11,5
do	do	k7c2	do
13,5	[number]	k4	13,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
uhlíkem	uhlík	k1gInSc7	uhlík
do	do	k7c2	do
0,08	[number]	k4	0,08
%	%	kIx~	%
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
legovány	legován	k2eAgInPc1d1	legován
feritotvornými	feritotvorný	k2eAgInPc7d1	feritotvorný
prvky	prvek	k1gInPc7	prvek
např.	např.	kA	např.
hliníkem	hliník	k1gInSc7	hliník
<g/>
,	,	kIx,	,
titanem	titan	k1gInSc7	titan
nebo	nebo	k8xC	nebo
niobem	niob	k1gInSc7	niob
mají	mít	k5eAaImIp3nP	mít
potlačenou	potlačený	k2eAgFnSc4d1	potlačená
austenitickou	austenitický	k2eAgFnSc4d1	austenitická
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
martenzitickou	martenzitický	k2eAgFnSc4d1	martenzitická
přeměnu	přeměna	k1gFnSc4	přeměna
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
oceli	ocel	k1gFnPc4	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
chrómu	chróm	k1gInSc2	chróm
do	do	k7c2	do
16	[number]	k4	16
do	do	k7c2	do
18	[number]	k4	18
%	%	kIx~	%
<g/>
,	,	kIx,	,
uhlíkem	uhlík	k1gInSc7	uhlík
do	do	k7c2	do
0,1	[number]	k4	0,1
%	%	kIx~	%
se	se	k3xPyFc4	se
stabilizací	stabilizace	k1gFnPc2	stabilizace
titanem	titan	k1gInSc7	titan
nebo	nebo	k8xC	nebo
niobem	niob	k1gInSc7	niob
a	a	k8xC	a
legované	legovaný	k2eAgFnPc4d1	legovaná
molybdenem	molybden	k1gInSc7	molybden
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
oceli	ocel	k1gFnPc4	ocel
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
chrómu	chróm	k1gInSc2	chróm
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
uhlíkem	uhlík	k1gInSc7	uhlík
od	od	k7c2	od
0,002	[number]	k4	0,002
do	do	k7c2	do
0,2	[number]	k4	0,2
%	%	kIx~	%
mají	mít	k5eAaImIp3nP	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
odolností	odolnost	k1gFnPc2	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
legovány	legován	k2eAgFnPc1d1	legován
0,5	[number]	k4	0,5
až	až	k9	až
4	[number]	k4	4
%	%	kIx~	%
molybdenu	molybden	k1gInSc2	molybden
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
nízké	nízký	k2eAgFnPc1d1	nízká
přechodové	přechodový	k2eAgFnPc1d1	přechodová
teploty	teplota	k1gFnPc1	teplota
až	až	k9	až
při	při	k7c6	při
-40	-40	k4	-40
°	°	k?	°
<g/>
C.	C.	kA	C.
<g/>
Značnou	značný	k2eAgFnSc7d1	značná
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
feritických	feritický	k2eAgFnPc2d1	feritická
ocelí	ocel	k1gFnPc2	ocel
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc1	riziko
<g />
.	.	kIx.	.
</s>
<s>
jejich	jejich	k3xOp3gNnSc1	jejich
zkřehnutí	zkřehnutí	k1gNnSc1	zkřehnutí
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
cca	cca	kA	cca
nad	nad	k7c7	nad
1000	[number]	k4	1000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
uhlíku	uhlík	k1gInSc2	uhlík
z	z	k7c2	z
karbidů	karbid	k1gInPc2	karbid
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
austenitu	austenit	k1gInSc2	austenit
a	a	k8xC	a
po	po	k7c4	po
ochlazení	ochlazení	k1gNnSc4	ochlazení
martenzitická	martenzitický	k2eAgFnSc1d1	martenzitická
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
fáze	fáze	k1gFnSc2	fáze
σ	σ	k?	σ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
chrómu	chróm	k1gInSc2	chróm
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
působení	působení	k1gNnSc4	působení
teplot	teplota	k1gFnPc2	teplota
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zkřehnutím	zkřehnutí	k1gNnSc7	zkřehnutí
475	[number]	k4	475
°	°	k?	°
<g/>
C	C	kA	C
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
houževnatosti	houževnatost	k1gFnSc2	houževnatost
a	a	k8xC	a
tažnosti	tažnost	k1gFnSc2	tažnost
materiálu	materiál	k1gInSc2	materiál
při	při	k7c6	při
teplotním	teplotní	k2eAgNnSc6d1	teplotní
působení	působení	k1gNnSc6	působení
od	od	k7c2	od
350	[number]	k4	350
do	do	k7c2	do
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
od	od	k7c2	od
450	[number]	k4	450
do	do	k7c2	do
525	[number]	k4	525
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
ohřevu	ohřev	k1gInSc6	ohřev
ocelí	ocel	k1gFnPc2	ocel
nad	nad	k7c7	nad
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
austenitu	austenit	k1gInSc3	austenit
a	a	k8xC	a
následně	následně	k6eAd1	následně
při	při	k7c6	při
ochlazování	ochlazování	k1gNnSc6	ochlazování
částečně	částečně	k6eAd1	částečně
martenzitické	martenzitický	k2eAgFnSc2d1	martenzitická
struktury	struktura	k1gFnSc2	struktura
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
chrómu	chróm	k1gInSc2	chróm
od	od	k7c2	od
13	[number]	k4	13
do	do	k7c2	do
16	[number]	k4	16
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
oceli	ocel	k1gFnPc1	ocel
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
nazývají	nazývat	k5eAaImIp3nP	nazývat
poloferitické	poloferitický	k2eAgInPc1d1	poloferitický
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
do	do	k7c2	do
slabých	slabý	k2eAgFnPc2d1	slabá
korozivních	korozivní	k2eAgFnPc2d1	korozivní
prostředí	prostředí	k1gNnSc3	prostředí
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnPc1	ovoce
<g/>
,	,	kIx,	,
suché	suchý	k2eAgFnPc1d1	suchá
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
nápoje	nápoj	k1gInPc1	nápoj
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
svařování	svařování	k1gNnSc4	svařování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
desky	deska	k1gFnPc1	deska
pracovních	pracovní	k2eAgInPc2d1	pracovní
stolů	stol	k1gInPc2	stol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Austeniticko-feritická	austenitickoeritický	k2eAgFnSc1d1	austeniticko-feritický
korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
===	===	k?	===
</s>
</p>
<p>
<s>
Austeniticko-feritické	austenitickoeritický	k2eAgFnPc1d1	austeniticko-feritický
korozivzdorné	korozivzdorný	k2eAgFnPc1d1	korozivzdorná
oceli	ocel	k1gFnPc1	ocel
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
struktuře	struktura	k1gFnSc6	struktura
podíl	podíl	k1gInSc1	podíl
jak	jak	k8xC	jak
fáze	fáze	k1gFnSc1	fáze
austenitu	austenit	k1gInSc2	austenit
tak	tak	k9	tak
i	i	k9	i
feritu	ferit	k1gInSc2	ferit
δ	δ	k?	δ
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podílu	podíl	k1gInSc3	podíl
feritu	ferit	k1gInSc2	ferit
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
přítomnosti	přítomnost	k1gFnSc6	přítomnost
feritotvorných	feritotvorný	k2eAgInPc2d1	feritotvorný
a	a	k8xC	a
austenitotvorných	austenitotvorný	k2eAgInPc2d1	austenitotvorný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
<g/>
Dvoufázová	dvoufázový	k2eAgFnSc1d1	dvoufázová
směs	směs	k1gFnSc1	směs
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
jemnější	jemný	k2eAgNnSc4d2	jemnější
zrno	zrno	k1gNnSc4	zrno
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
fázích	fáze	k1gFnPc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořením	vytvoření	k1gNnSc7	vytvoření
takové	takový	k3xDgFnSc2	takový
dvoufázové	dvoufázový	k2eAgFnSc2d1	dvoufázová
oceli	ocel	k1gFnSc2	ocel
–	–	k?	–
často	často	k6eAd1	často
také	také	k9	také
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
duplexní	duplexní	k2eAgFnPc1d1	duplexní
ocel	ocel	k1gFnSc1	ocel
–	–	k?	–
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vyšší	vysoký	k2eAgFnSc1d2	vyšší
mezi	mezi	k7c7	mezi
kluzu	kluz	k1gInSc2	kluz
zhruba	zhruba	k6eAd1	zhruba
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
MPa	MPa	k1gFnPc2	MPa
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgFnPc4d1	dobrá
svařitelnosti	svařitelnost	k1gFnPc4	svařitelnost
<g/>
,	,	kIx,	,
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
houževnatosti	houževnatost	k1gFnPc4	houževnatost
(	(	kIx(	(
<g/>
horší	zlý	k2eAgNnPc4d2	horší
než	než	k8xS	než
u	u	k7c2	u
austenitických	austenitický	k2eAgFnPc2d1	austenitická
a	a	k8xC	a
lepší	dobrý	k2eAgInPc1d2	lepší
než	než	k8xS	než
u	u	k7c2	u
feritických	feritický	k2eAgFnPc2d1	feritická
ocelí	ocel	k1gFnPc2	ocel
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
odolnosti	odolnost	k1gFnSc2	odolnost
nejenom	nejenom	k6eAd1	nejenom
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podílu	podíl	k1gInSc6	podíl
feritu	ferit	k1gInSc2	ferit
mezi	mezi	k7c7	mezi
40	[number]	k4	40
a	a	k8xC	a
50	[number]	k4	50
%	%	kIx~	%
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
optimální	optimální	k2eAgFnPc4d1	optimální
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jak	jak	k8xC	jak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pevnosti	pevnost	k1gFnSc2	pevnost
tak	tak	k8xC	tak
i	i	k9	i
houževnatosti	houževnatost	k1gFnSc2	houževnatost
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
legování	legování	k1gNnSc6	legování
molybdenem	molybden	k1gInSc7	molybden
<g/>
,	,	kIx,	,
mědí	mědit	k5eAaImIp3nS	mědit
eventuálně	eventuálně	k6eAd1	eventuálně
dusíkem	dusík	k1gInSc7	dusík
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vyšší	vysoký	k2eAgFnPc4d2	vyšší
korozní	korozní	k2eAgFnPc4d1	korozní
odolnosti	odolnost	k1gFnPc4	odolnost
proti	proti	k7c3	proti
mezikrystalové	mezikrystal	k1gMnPc1	mezikrystal
<g/>
,	,	kIx,	,
bodové	bodový	k2eAgFnSc3d1	bodová
a	a	k8xC	a
štěrbinové	štěrbinový	k2eAgFnSc3d1	štěrbinová
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
oceli	ocel	k1gFnSc3	ocel
tak	tak	k6eAd1	tak
lez	lézt	k5eAaImRp2nS	lézt
v	v	k7c6	v
prostředí	prostředí	k1gNnPc4	prostředí
<g />
.	.	kIx.	.
</s>
<s>
kyseliny	kyselina	k1gFnPc1	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
i	i	k9	i
v	v	k7c6	v
chloridech	chlorid	k1gInPc6	chlorid
<g/>
.	.	kIx.	.
<g/>
Typické	typický	k2eAgNnSc4d1	typické
složení	složení	k1gNnSc4	složení
80	[number]	k4	80
%	%	kIx~	%
austeniticko-feritických	austenitickoeritický	k2eAgFnPc2d1	austeniticko-feritický
ocelí	ocel	k1gFnPc2	ocel
je	být	k5eAaImIp3nS	být
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
chrómu	chróm	k1gInSc2	chróm
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
<g/>
–	–	k?	–
<g/>
6,5	[number]	k4	6,5
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
3,5	[number]	k4	3,5
hm	hm	k?	hm
<g />
.	.	kIx.	.
</s>
<s>
<g/>
%	%	kIx~	%
molybdenu	molybden	k1gInSc6	molybden
<g/>
.	.	kIx.	.
<g/>
Omezením	omezení	k1gNnSc7	omezení
duplexních	duplexní	k2eAgFnPc2d1	duplexní
ocelí	ocel	k1gFnPc2	ocel
je	být	k5eAaImIp3nS	být
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
použití	použití	k1gNnSc1	použití
při	při	k7c6	při
zvýšených	zvýšený	k2eAgFnPc6d1	zvýšená
teplotách	teplota	k1gFnPc6	teplota
mezi	mezi	k7c7	mezi
700	[number]	k4	700
a	a	k8xC	a
1000	[number]	k4	1000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vytvrzování	vytvrzování	k1gNnSc3	vytvrzování
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
křehnutí	křehnutí	k1gNnSc1	křehnutí
vylučováním	vylučování	k1gNnSc7	vylučování
tzv.	tzv.	kA	tzv.
fáze	fáze	k1gFnSc2	fáze
sigma	sigma	k1gNnSc2	sigma
a	a	k8xC	a
stárnutím	stárnutí	k1gNnSc7	stárnutí
(	(	kIx(	(
<g/>
vytvrzováním	vytvrzování	k1gNnSc7	vytvrzování
<g/>
,	,	kIx,	,
křehnutím	křehnutí	k1gNnSc7	křehnutí
<g/>
)	)	kIx)	)
při	při	k7c6	při
475	[number]	k4	475
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
teplot	teplota	k1gFnPc2	teplota
350	[number]	k4	350
až	až	k9	až
550	[number]	k4	550
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Stainless	Stainlessa	k1gFnPc2	Stainlessa
steel	stelo	k1gNnPc2	stelo
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČÍHAL	Číhal	k1gMnSc1	Číhal
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
.	.	kIx.	.
437	[number]	k4	437
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
671	[number]	k4	671
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
reference	reference	k1gFnSc1	reference
viz	vidět	k5eAaImRp2nS	vidět
Číhal	Číhal	k1gMnSc1	Číhal
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
FOLDYNA	FOLDYNA	kA	FOLDYNA
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
HENNHOFER	HENNHOFER	kA	HENNHOFER
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
OLŠAROVÁ	OLŠAROVÁ	kA	OLŠAROVÁ
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
,	,	kIx,	,
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
<g/>
;	;	kIx,	;
Koukal	Koukal	k1gMnSc1	Koukal
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
Kristofory	Kristofora	k1gFnPc1	Kristofora
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
;	;	kIx,	;
Ochodek	Ochodek	k1gInSc1	Ochodek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
Pilous	pilous	k1gMnSc1	pilous
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
Purmenský	Purmenský	k2eAgMnSc1d1	Purmenský
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
Drahomír	Drahomír	k1gMnSc1	Drahomír
<g/>
;	;	kIx,	;
Veselko	Veselka	k1gMnSc5	Veselka
<g/>
,	,	kIx,	,
Július	Július	k1gMnSc1	Július
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
svařitelnost	svařitelnost	k1gFnSc4	svařitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Recenzent	recenzent	k1gMnSc1	recenzent
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Koukal	Koukal	k1gMnSc1	Koukal
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
svářečská	svářečský	k2eAgFnSc1d1	svářečská
společnost	společnost	k1gFnSc1	společnost
ANB	ANB	kA	ANB
<g/>
,	,	kIx,	,
ZEROSS	ZEROSS	kA	ZEROSS
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
216	[number]	k4	216
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85771	[number]	k4	85771
<g/>
-	-	kIx~	-
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
reference	reference	k1gFnSc1	reference
viz	vidět	k5eAaImRp2nS	vidět
Foldyna	Foldyno	k1gNnSc2	Foldyno
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
–	–	k?	–
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
návrh	návrh	k1gInSc1	návrh
a	a	k8xC	a
technologie	technologie	k1gFnSc1	technologie
svařování	svařování	k1gNnSc2	svařování
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
SDSM	SDSM	kA	SDSM
(	(	kIx(	(
<g/>
Svařování	svařování	k1gNnSc1	svařování
<g/>
,	,	kIx,	,
dělení	dělení	k1gNnSc1	dělení
a	a	k8xC	a
spojování	spojování	k1gNnSc1	spojování
materiálů	materiál	k1gInPc2	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
reference	reference	k1gFnSc1	reference
viz	vidět	k5eAaImRp2nS	vidět
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
4044	[number]	k4	4044
<g/>
.	.	kIx.	.
</s>
<s>
ČÍHAL	Číhal	k1gMnSc1	Číhal
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Mezikrystalová	Mezikrystalový	k2eAgFnSc1d1	Mezikrystalový
koroze	koroze	k1gFnSc1	koroze
ocelí	ocel	k1gFnPc2	ocel
a	a	k8xC	a
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
406	[number]	k4	406
s.	s.	k?	s.
Korozivzdorné	korozivzdorný	k2eAgFnSc2d1	korozivzdorná
oceli	ocel	k1gFnSc2	ocel
-	-	kIx~	-
vlastnosti	vlastnost	k1gFnPc1	vlastnost
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Euro	euro	k1gNnSc1	euro
Inox	Inox	k1gInSc1	Inox
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
EuroInox	EuroInox	k1gInSc1	EuroInox
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
87997	[number]	k4	87997
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
KARLSSON	KARLSSON	kA	KARLSSON
<g/>
,	,	kIx,	,
Leif	Leif	k1gMnSc1	Leif
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Stainless	Stainless	k1gInSc1	Stainless
Steels	Steels	k1gInSc1	Steels
Past	past	k1gFnSc1	past
<g/>
,	,	kIx,	,
Present	Present	k1gInSc1	Present
and	and	k?	and
Future	Futur	k1gInSc5	Futur
<g/>
.	.	kIx.	.
</s>
<s>
Svetsaren	Svetsarna	k1gFnPc2	Svetsarna
<g/>
.	.	kIx.	.
</s>
<s>
Čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
47	[number]	k4	47
<g/>
–	–	k?	–
<g/>
52	[number]	k4	52
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
reference	reference	k1gFnSc1	reference
viz	vidět	k5eAaImRp2nS	vidět
Karlsson	Karlsson	k1gInSc1	Karlsson
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
KRAUS	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Povrchy	povrch	k1gInPc4	povrch
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
úpravy	úprava	k1gFnPc4	úprava
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
2006-07-08	[number]	k4	2006-07-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Třídy	třída	k1gFnPc1	třída
oceli	ocel	k1gFnSc2	ocel
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Korozivzdorná	korozivzdorný	k2eAgFnSc1d1	korozivzdorná
ocel	ocel	k1gFnSc1	ocel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Glossary	Glossara	k1gFnPc1	Glossara
of	of	k?	of
stainless	stainless	k6eAd1	stainless
steel	steet	k5eAaPmAgInS	steet
terms	terms	k1gInSc1	terms
for	forum	k1gNnPc2	forum
non-technical	nonechnicat	k5eAaPmAgInS	non-technicat
users	users	k6eAd1	users
<g/>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Převodní	převodní	k2eAgFnSc2d1	převodní
tabulky	tabulka	k1gFnSc2	tabulka
norem	norma	k1gFnPc2	norma
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Glossary	Glossar	k1gInPc1	Glossar
of	of	k?	of
Stainless	Stainless	k1gInSc1	Stainless
Steel	Steel	k1gInSc1	Steel
Related	Related	k1gInSc1	Related
Terms	Termsa	k1gFnPc2	Termsa
by	by	kYmCp3nS	by
Specialty	Specialt	k1gInPc1	Specialt
Steel	Steela	k1gFnPc2	Steela
Supply	Supply	k1gFnPc2	Supply
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Articles	Articles	k1gInSc1	Articles
About	About	k2eAgInSc1d1	About
Stainless	Stainless	k1gInSc1	Stainless
Steel	Steela	k1gFnPc2	Steela
by	by	kYmCp3nS	by
International	International	k1gFnSc1	International
Stainless	Stainless	k1gInSc4	Stainless
Steel	Steel	k1gInSc1	Steel
Forum	forum	k1gNnSc1	forum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stainless	Stainless	k1gInSc1	Stainless
Steel	Steel	k1gMnSc1	Steel
Properties	Properties	k1gMnSc1	Properties
and	and	k?	and
Corrosion	Corrosion	k1gInSc1	Corrosion
Resistance	Resistanec	k1gMnSc2	Resistanec
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Comprehensive	Comprehensiev	k1gFnPc1	Comprehensiev
Information	Information	k1gInSc4	Information
About	About	k2eAgInSc1d1	About
Stainless	Stainless	k1gInSc1	Stainless
Steel	Steela	k1gFnPc2	Steela
by	by	kYmCp3nS	by
The	The	k1gFnSc1	The
Stainless	Stainless	k1gInSc4	Stainless
Steel	Steel	k1gInSc1	Steel
Information	Information	k1gInSc1	Information
Center	centrum	k1gNnPc2	centrum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Technical	Technical	k1gFnSc1	Technical
Library	Librara	k1gFnSc2	Librara
on	on	k3xPp3gInSc1	on
Stainless	Stainless	k1gInSc1	Stainless
Steel	Steela	k1gFnPc2	Steela
by	by	kYmCp3nS	by
BSSA	BSSA	kA	BSSA
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Comprehensive	Comprehensiev	k1gFnSc2	Comprehensiev
Information	Information	k1gInSc1	Information
About	About	k1gInSc1	About
Metallurgy	Metallurg	k1gInPc1	Metallurg
of	of	k?	of
Stainless	Stainless	k1gInSc1	Stainless
Steel	Steela	k1gFnPc2	Steela
by	by	kYmCp3nS	by
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Converter	Converter	k1gInSc1	Converter
for	forum	k1gNnPc2	forum
Stainless	Stainlessa	k1gFnPc2	Stainlessa
Steel	Steela	k1gFnPc2	Steela
Standards	Standards	k1gInSc1	Standards
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stainless	Stainless	k1gInSc1	Stainless
Steel	Steel	k1gInSc1	Steel
Composition	Composition	k1gInSc4	Composition
</s>
</p>
