<s>
Korozivzdorná	korozivzdorný	k2eAgFnSc1d1
ocel	ocel	k1gFnSc1
(	(	kIx(
<g/>
slangově	slangově	k6eAd1
nerez	nerez	k2eAgFnSc1d1
<g/>
,	,	kIx,
nerezová	rezový	k2eNgFnSc1d1
ocel	ocel	k1gFnSc4
či	či	k8xC
nerezavějící	rezavějící	k2eNgFnSc4d1
ocel	ocel	k1gFnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vysocelegovaná	vysocelegovaný	k2eAgFnSc1d1
ocel	ocel	k1gFnSc1
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
odolností	odolnost	k1gFnSc7
vůči	vůči	k7c3
chemické	chemický	k2eAgFnSc3d1
i	i	k8xC
elektrochemické	elektrochemický	k2eAgFnSc3d1
korozi	koroze	k1gFnSc3
<g/>
.	.	kIx.
</s>