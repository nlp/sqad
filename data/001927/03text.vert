<s>
Melanin	melanin	k1gInSc1	melanin
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
hnědý	hnědý	k2eAgInSc4d1	hnědý
až	až	k8xS	až
černý	černý	k2eAgInSc4d1	černý
pigment	pigment	k1gInSc4	pigment
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
i	i	k8xC	i
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
tyrosinu	tyrosina	k1gFnSc4	tyrosina
či	či	k8xC	či
tryptofanu	tryptofana	k1gFnSc4	tryptofana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
oxidovány	oxidován	k2eAgInPc1d1	oxidován
a	a	k8xC	a
zpolymerovány	zpolymerován	k2eAgInPc1d1	zpolymerován
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
hnědočerný	hnědočerný	k2eAgInSc1d1	hnědočerný
polymer	polymer	k1gInSc1	polymer
eumelanin	eumelanin	k2eAgInSc1d1	eumelanin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
běžná	běžný	k2eAgFnSc1d1	běžná
forma	forma	k1gFnSc1	forma
je	být	k5eAaImIp3nS	být
červenohnědý	červenohnědý	k2eAgInSc4d1	červenohnědý
polymer	polymer	k1gInSc4	polymer
feomelanin	feomelanina	k1gFnPc2	feomelanina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
zrzavé	zrzavý	k2eAgInPc4d1	zrzavý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
pihy	piha	k1gFnPc4	piha
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
mírně	mírně	k6eAd1	mírně
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
chemickou	chemický	k2eAgFnSc4d1	chemická
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
ve	v	k7c6	v
vlasech	vlas	k1gInPc6	vlas
či	či	k8xC	či
v	v	k7c6	v
sítnici	sítnice	k1gFnSc6	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
melanin	melanin	k1gInSc1	melanin
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
peří	peří	k1gNnSc6	peří
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
hmyzí	hmyzí	k2eAgFnSc6d1	hmyzí
vnější	vnější	k2eAgFnSc6d1	vnější
kostře	kostra	k1gFnSc6	kostra
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
inkoustu	inkoust	k1gInSc6	inkoust
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Melanin	melanin	k1gInSc1	melanin
chrání	chránit	k5eAaImIp3nS	chránit
proti	proti	k7c3	proti
poškození	poškození	k1gNnSc3	poškození
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
nadměrně	nadměrně	k6eAd1	nadměrně
vystavena	vystavit	k5eAaPmNgFnS	vystavit
světelnému	světelný	k2eAgInSc3d1	světelný
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétněji	konkrétně	k6eAd2	konkrétně
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmí	smět	k5eNaImIp3nS	smět
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
kyseliny	kyselina	k1gFnSc2	kyselina
listové	listový	k2eAgFnSc2d1	listová
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zase	zase	k9	zase
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
dostatek	dostatek	k1gInSc4	dostatek
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vznikat	vznikat	k5eAaImF	vznikat
vitamín	vitamín	k1gInSc1	vitamín
D.	D.	kA	D.
Pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
především	především	k9	především
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mění	měnit	k5eAaImIp3nS	měnit
z	z	k7c2	z
99,9	[number]	k4	99,9
<g/>
%	%	kIx~	%
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
tak	tak	k6eAd1	tak
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
tvorbě	tvorba	k1gFnSc3	tvorba
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
chrání	chránit	k5eAaImIp3nS	chránit
DNA	dna	k1gFnSc1	dna
buněk	buňka	k1gFnPc2	buňka
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
zhoubného	zhoubný	k2eAgInSc2d1	zhoubný
nádoru	nádor	k1gInSc2	nádor
melanomu	melanom	k1gInSc2	melanom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kůži	kůže	k1gFnSc6	kůže
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
melaninu	melanin	k1gInSc2	melanin
stimulována	stimulovat	k5eAaImNgFnS	stimulovat
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
když	když	k8xS	když
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
tak	tak	k6eAd1	tak
při	při	k7c6	při
opalování	opalování	k1gNnSc6	opalování
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
lidské	lidský	k2eAgFnPc1d1	lidská
rasy	rasa	k1gFnPc1	rasa
mají	mít	k5eAaImIp3nP	mít
geneticky	geneticky	k6eAd1	geneticky
zakódované	zakódovaný	k2eAgFnPc1d1	zakódovaná
odlišné	odlišný	k2eAgFnPc1d1	odlišná
barvy	barva	k1gFnPc1	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zřejmě	zřejmě	k6eAd1	zřejmě
záviselo	záviset	k5eAaImAgNnS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jak	jak	k6eAd1	jak
intenzivním	intenzivní	k2eAgNnSc6d1	intenzivní
slunečním	sluneční	k2eAgNnSc6d1	sluneční
záření	záření	k1gNnSc6	záření
se	se	k3xPyFc4	se
daná	daný	k2eAgFnSc1d1	daná
populace	populace	k1gFnSc1	populace
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Buňka	buňka	k1gFnSc1	buňka
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
melaninu	melanin	k1gInSc2	melanin
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
melanocyt	melanocyt	k1gInSc1	melanocyt
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
přílišné	přílišný	k2eAgFnSc3d1	přílišná
produkci	produkce	k1gFnSc3	produkce
melaninu	melanin	k1gInSc2	melanin
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
melanismus	melanismus	k1gInSc1	melanismus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
melanin	melanin	k1gInSc1	melanin
je	být	k5eAaImIp3nS	být
produkován	produkovat	k5eAaImNgInS	produkovat
v	v	k7c6	v
nižším	nízký	k2eAgNnSc6d2	nižší
množství	množství	k1gNnSc6	množství
(	(	kIx(	(
<g/>
albinismus	albinismus	k1gInSc1	albinismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
