<p>
<s>
Kjúšú	Kjúšú	k?	Kjúšú
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
九	九	k?	九
=	=	kIx~	=
devět	devět	k4xCc1	devět
zemí	zem	k1gFnPc2	zem
<g/>
/	/	kIx~	/
<g/>
provincií	provincie	k1gFnPc2	provincie
-	-	kIx~	-
podle	podle	k7c2	podle
9	[number]	k4	9
správních	správní	k2eAgFnPc2d1	správní
provincií	provincie	k1gFnPc2	provincie
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgMnSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
japonský	japonský	k2eAgInSc1d1	japonský
ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
nejjižněji	jižně	k6eAd3	jižně
a	a	k8xC	a
nejzápadněji	západně	k6eAd3	západně
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
kolébku	kolébka	k1gFnSc4	kolébka
japonské	japonský	k2eAgFnSc2d1	japonská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
i	i	k9	i
pod	pod	k7c7	pod
jmény	jméno	k1gNnPc7	jméno
Kjúkoku	Kjúkok	k1gInSc2	Kjúkok
(	(	kIx(	(
<g/>
九	九	k?	九
<g/>
=	=	kIx~	=
<g/>
devět	devět	k4xCc1	devět
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Činzei	Činzei	k1gNnSc1	Činzei
(	(	kIx(	(
<g/>
鎮	鎮	k?	鎮
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cukuši-šima	Cukuši-šima	k1gFnSc1	Cukuši-šima
(	(	kIx(	(
<g/>
筑	筑	k?	筑
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
13,44	[number]	k4	13,44
milionu	milion	k4xCgInSc2	milion
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
35	[number]	k4	35
640	[number]	k4	640
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Kjúšú	Kjúšú	k?	Kjúšú
je	být	k5eAaImIp3nS	být
hornatý	hornatý	k2eAgInSc4d1	hornatý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
největší	veliký	k2eAgFnSc1d3	veliký
aktivní	aktivní	k2eAgFnSc1d1	aktivní
sopka	sopka	k1gFnSc1	sopka
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
–	–	k?	–
hora	hora	k1gFnSc1	hora
Aso	Aso	k1gFnSc1	Aso
(	(	kIx(	(
<g/>
1	[number]	k4	1
592	[number]	k4	592
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
mnoho	mnoho	k4c4	mnoho
známek	známka	k1gFnPc2	známka
tektonické	tektonický	k2eAgFnSc2d1	tektonická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mnoha	mnoho	k4c2	mnoho
horkých	horký	k2eAgInPc2d1	horký
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgInPc1d3	nejslavnější
prameny	pramen	k1gInPc1	pramen
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Beppu	Bepp	k1gInSc2	Bepp
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hory	hora	k1gFnSc2	hora
Aso	Aso	k1gFnSc2	Aso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
tzv.	tzv.	kA	tzv.
regionu	region	k1gInSc2	region
Kjúšú	Kjúšú	k1gFnSc2	Kjúšú
(	(	kIx(	(
<g/>
九	九	k?	九
<g/>
,	,	kIx,	,
Kjúšú-čihó	Kjúšú-čihó	k1gFnSc1	Kjúšú-čihó
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
sedm	sedm	k4xCc1	sedm
prefektur	prefektura	k1gFnPc2	prefektura
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kjúšú	Kjúšú	k1gFnSc2	Kjúšú
a	a	k8xC	a
také	také	k9	také
prefektura	prefektura	k1gFnSc1	prefektura
Okinawa	Okinawa	k1gFnSc1	Okinawa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
prefektura	prefektura	k1gFnSc1	prefektura
Fukuoka	Fukuoek	k1gInSc2	Fukuoek
</s>
</p>
<p>
<s>
prefektura	prefektura	k1gFnSc1	prefektura
Kagošima	Kagošima	k1gFnSc1	Kagošima
</s>
</p>
<p>
<s>
prefektura	prefektura	k1gFnSc1	prefektura
Kumamoto	Kumamota	k1gFnSc5	Kumamota
</s>
</p>
<p>
<s>
prefektura	prefektura	k1gFnSc1	prefektura
Mijazaki	Mijazak	k1gFnSc2	Mijazak
</s>
</p>
<p>
<s>
prefektura	prefektura	k1gFnSc1	prefektura
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
</s>
</p>
<p>
<s>
prefektura	prefektura	k1gFnSc1	prefektura
Óita	Óit	k1gInSc2	Óit
</s>
</p>
<p>
<s>
prefektura	prefektura	k1gFnSc1	prefektura
SagaNejvětším	SagaNejvětší	k2eAgNnSc7d1	SagaNejvětší
městem	město	k1gNnSc7	město
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Fukuoka	Fukuoek	k1gInSc2	Fukuoek
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Kjúšúská	Kjúšúský	k2eAgFnSc1d1	Kjúšúský
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
důležitými	důležitý	k2eAgInPc7d1	důležitý
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Kitakjúšú	Kitakjúšú	k1gMnPc1	Kitakjúšú
<g/>
,	,	kIx,	,
Kumamoto	Kumamota	k1gFnSc5	Kumamota
<g/>
,	,	kIx,	,
Kagošima	Kagošimum	k1gNnSc2	Kagošimum
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Kjúšú	Kjúšú	k1gFnPc4	Kjúšú
subtropické	subtropický	k2eAgFnPc4d1	subtropická
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Kjúšú	Kjúšú	k1gMnPc2	Kjúšú
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
