<s>
Kjúšú	Kjúšú	k?	Kjúšú
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
九	九	k?	九
=	=	kIx~	=
devět	devět	k4xCc1	devět
zemí	zem	k1gFnPc2	zem
<g/>
/	/	kIx~	/
<g/>
provincií	provincie	k1gFnPc2	provincie
-	-	kIx~	-
podle	podle	k7c2	podle
9	[number]	k4	9
správních	správní	k2eAgFnPc2d1	správní
provincií	provincie	k1gFnPc2	provincie
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgMnSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
japonský	japonský	k2eAgInSc1d1	japonský
ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
nejjižněji	jižně	k6eAd3	jižně
a	a	k8xC	a
nejzápadněji	západně	k6eAd3	západně
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
