<p>
<s>
Opuka	opuka	k1gFnSc1	opuka
je	být	k5eAaImIp3nS	být
usazená	usazený	k2eAgFnSc1d1	usazená
hornina	hornina	k1gFnSc1	hornina
<g/>
,	,	kIx,	,
prachovitý	prachovitý	k2eAgInSc1d1	prachovitý
druh	druh	k1gInSc1	druh
slínovce	slínovec	k1gInSc2	slínovec
<g/>
.	.	kIx.	.
</s>
<s>
Opuka	opuka	k1gFnSc1	opuka
vznikala	vznikat	k5eAaImAgFnS	vznikat
z	z	k7c2	z
nejjemnějších	jemný	k2eAgFnPc2d3	nejjemnější
částic	částice	k1gFnPc2	částice
usazených	usazený	k2eAgFnPc2d1	usazená
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
jílovité	jílovitý	k2eAgFnPc1d1	jílovitá
a	a	k8xC	a
prachovité	prachovitý	k2eAgFnPc1d1	prachovitá
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vápencové	vápencový	k2eAgFnPc4d1	vápencová
složky	složka	k1gFnPc4	složka
a	a	k8xC	a
jehlice	jehlice	k1gFnPc4	jehlice
mořských	mořský	k2eAgFnPc2d1	mořská
hub	houba	k1gFnPc2	houba
mikroskopických	mikroskopický	k2eAgInPc2d1	mikroskopický
rozměrů	rozměr	k1gInPc2	rozměr
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
spongie	spongie	k1gFnSc2	spongie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
těchto	tento	k3xDgFnPc2	tento
jehlic	jehlice	k1gFnPc2	jehlice
určuje	určovat	k5eAaImIp3nS	určovat
její	její	k3xOp3gFnSc4	její
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
trvanlivost	trvanlivost	k1gFnSc4	trvanlivost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgInPc1d1	častý
i	i	k8xC	i
zbytky	zbytek	k1gInPc1	zbytek
dírkonošců	dírkonošec	k1gInPc2	dírkonošec
<g/>
.	.	kIx.	.
</s>
<s>
Běžnou	běžný	k2eAgFnSc7d1	běžná
minerální	minerální	k2eAgFnSc7d1	minerální
příměsí	příměs	k1gFnSc7	příměs
je	být	k5eAaImIp3nS	být
glaukonit	glaukonit	k1gInSc1	glaukonit
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
bělavou	bělavý	k2eAgFnSc4d1	bělavá
až	až	k8xS	až
pískově	pískově	k6eAd1	pískově
žlutošedou	žlutošedý	k2eAgFnSc4d1	žlutošedá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
<g/>
,	,	kIx,	,
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hledisks	hledisks	k6eAd1	hledisks
geologicky	geologicky	k6eAd1	geologicky
nepřesné	přesný	k2eNgFnPc1d1	nepřesná
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
horninu	hornina	k1gFnSc4	hornina
tak	tak	k6eAd1	tak
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
použití	použití	k1gNnSc1	použití
jako	jako	k9	jako
lehce	lehko	k6eAd1	lehko
opracovatelný	opracovatelný	k2eAgInSc1d1	opracovatelný
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
–	–	k?	–
opuka	opuka	k1gFnSc1	opuka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opoka	opoka	k1gFnSc1	opoka
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
litavsky	litavsky	k6eAd1	litavsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gaize	gaize	k1gFnSc1	gaize
(	(	kIx(	(
<g/>
franzouzsky	franzouzsky	k6eAd1	franzouzsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pläner	Pläner	k1gMnSc1	Pläner
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
románském	románský	k2eAgInSc6d1	románský
slohu	sloh	k1gInSc6	sloh
byla	být	k5eAaImAgFnS	být
opuka	opuka	k1gFnSc1	opuka
významným	významný	k2eAgMnSc7d1	významný
stavebním	stavební	k2eAgInSc7d1	stavební
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
byly	být	k5eAaImAgFnP	být
opuky	opuka	k1gFnPc1	opuka
používány	používat	k5eAaImNgFnP	používat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
stavbách	stavba	k1gFnPc6	stavba
románského	románský	k2eAgInSc2d1	románský
slohu	sloh	k1gInSc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
jehlic	jehlice	k1gFnPc2	jehlice
houbiček	houbička	k1gFnPc2	houbička
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
kvalitu	kvalita	k1gFnSc4	kvalita
kamene	kámen	k1gInSc2	kámen
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc4	jeho
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
opuka	opuka	k1gFnSc1	opuka
snadno	snadno	k6eAd1	snadno
opracovává	opracovávat	k5eAaImIp3nS	opracovávat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
v	v	k7c6	v
kamenosochařství	kamenosochařství	k1gNnSc6	kamenosochařství
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nízkém	nízký	k2eAgInSc6d1	nízký
obsahu	obsah	k1gInSc6	obsah
křemičitých	křemičitý	k2eAgFnPc2d1	křemičitá
jehlic	jehlice	k1gFnPc2	jehlice
a	a	k8xC	a
vysokém	vysoký	k2eAgInSc6d1	vysoký
obsahu	obsah	k1gInSc6	obsah
vápnité	vápnitý	k2eAgFnSc2d1	vápnitá
složky	složka	k1gFnSc2	složka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
opuka	opuka	k1gFnSc1	opuka
využívala	využívat	k5eAaImAgFnS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vápna	vápno	k1gNnSc2	vápno
a	a	k8xC	a
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlatavě	zlatavě	k6eAd1	zlatavě
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
pražská	pražský	k2eAgFnSc1d1	Pražská
opuka	opuka	k1gFnSc1	opuka
<g/>
,	,	kIx,	,
těžená	těžený	k2eAgFnSc1d1	těžená
v	v	k7c6	v
lomech	lom	k1gInPc6	lom
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
Hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
v	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
Kopanině	kopanina	k1gFnSc6	kopanina
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vylomení	vylomení	k1gNnSc6	vylomení
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
vlhkost	vlhkost	k1gFnSc1	vlhkost
a	a	k8xC	a
měkkost	měkkost	k1gFnSc1	měkkost
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
velmi	velmi	k6eAd1	velmi
detailní	detailní	k2eAgNnSc1d1	detailní
zpracování	zpracování	k1gNnSc1	zpracování
řezbářskými	řezbářský	k2eAgInPc7d1	řezbářský
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
hojně	hojně	k6eAd1	hojně
využívána	využívat	k5eAaImNgFnS	využívat
jako	jako	k8xC	jako
sochařský	sochařský	k2eAgInSc4d1	sochařský
materiál	materiál	k1gInSc4	materiál
v	v	k7c6	v
období	období	k1gNnSc6	období
gotiky	gotika	k1gFnSc2	gotika
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zhotoveny	zhotovit	k5eAaPmNgFnP	zhotovit
některé	některý	k3yIgFnPc1	některý
sochy	socha	k1gFnPc1	socha
krásného	krásný	k2eAgInSc2d1	krásný
slohu	sloh	k1gInSc2	sloh
(	(	kIx(	(
<g/>
Sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
ze	z	k7c2	z
Slivice	Slivice	k1gFnSc2	Slivice
<g/>
,	,	kIx,	,
Krumlovská	krumlovský	k2eAgFnSc1d1	Krumlovská
madona	madona	k1gFnSc1	madona
<g/>
,	,	kIx,	,
Plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
madona	madona	k1gFnSc1	madona
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
světlé	světlý	k2eAgFnSc2d1	světlá
opuky	opuka	k1gFnSc2	opuka
(	(	kIx(	(
<g/>
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
hornin	hornina	k1gFnPc2	hornina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
také	také	k9	také
některá	některý	k3yIgNnPc1	některý
místa	místo	k1gNnPc1	místo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
pražské	pražský	k2eAgNnSc1d1	Pražské
návrší	návrší	k1gNnSc1	návrší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
svedena	sveden	k2eAgFnSc1d1	svedena
důležitá	důležitý	k2eAgFnSc1d1	důležitá
bitva	bitva	k1gFnSc1	bitva
</s>
</p>
<p>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
stráně	stráň	k1gFnPc1	stráň
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
u	u	k7c2	u
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Pokratického	Pokratický	k2eAgInSc2d1	Pokratický
potoka	potok	k1gInSc2	potok
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
hůra	hůra	k1gFnSc1	hůra
neboli	neboli	k8xC	neboli
Bělice	bělice	k1gFnPc1	bělice
<g/>
,	,	kIx,	,
starší	starý	k2eAgInPc1d2	starší
názvy	název	k1gInPc1	název
kopce	kopec	k1gInSc2	kopec
Přerovská	přerovský	k2eAgFnSc1d1	přerovská
hůra	hůra	k1gFnSc1	hůra
</s>
</p>
<p>
<s>
==	==	k?	==
Některé	některý	k3yIgFnPc4	některý
opukové	opukový	k2eAgFnPc4d1	opuková
stavby	stavba	k1gFnPc4	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Hladová	hladový	k2eAgFnSc1d1	hladová
zeď	zeď	k1gFnSc1	zeď
</s>
</p>
<p>
<s>
Rotunda	rotunda	k1gFnSc1	rotunda
svatého	svatý	k2eAgMnSc2d1	svatý
Martina	Martin	k1gMnSc2	Martin
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
</s>
</p>
<p>
<s>
Rotunda	rotunda	k1gFnSc1	rotunda
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
Říp	Říp	k1gInSc1	Říp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Opočno	Opočno	k1gNnSc1	Opočno
</s>
</p>
<p>
<s>
Opoczno	Opoczna	k1gFnSc5	Opoczna
</s>
</p>
<p>
<s>
Opoczka	Opoczka	k1gFnSc1	Opoczka
</s>
</p>
<p>
<s>
Opoka	Opoka	k1gMnSc1	Opoka
Duża	Duża	k1gMnSc1	Duża
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
opuka	opuka	k1gFnSc1	opuka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
