<s>
Twitter	Twitter	k1gMnSc1	Twitter
je	být	k5eAaImIp3nS	být
poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
mikroblogu	mikroblog	k1gInSc2	mikroblog
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
posílat	posílat	k5eAaImF	posílat
a	a	k8xC	a
číst	číst	k5eAaImF	číst
příspěvky	příspěvek	k1gInPc4	příspěvek
zaslané	zaslaný	k2eAgInPc4d1	zaslaný
jinými	jiný	k2eAgMnPc7d1	jiný
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
,	,	kIx,	,
známé	známá	k1gFnPc4	známá
jako	jako	k8xC	jako
tweety	tweeta	k1gFnPc4	tweeta
<g/>
.	.	kIx.	.
</s>
<s>
Tweet	Tweet	k1gInSc1	Tweet
je	být	k5eAaImIp3nS	být
textový	textový	k2eAgInSc4d1	textový
příspěvek	příspěvek	k1gInSc4	příspěvek
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
maximálně	maximálně	k6eAd1	maximálně
140	[number]	k4	140
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
na	na	k7c6	na
uživatelově	uživatelův	k2eAgFnSc6d1	uživatelova
profilové	profilový	k2eAgFnSc6d1	profilová
stránce	stránka	k1gFnSc6	stránka
a	a	k8xC	a
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
jeho	jeho	k3xOp3gMnPc2	jeho
sledujících	sledující	k2eAgMnPc2d1	sledující
(	(	kIx(	(
<g/>
followers	followers	k6eAd1	followers
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přispěvatelé	přispěvatel	k1gMnPc1	přispěvatel
mohou	moct	k5eAaImIp3nP	moct
omezit	omezit	k5eAaPmF	omezit
doručování	doručování	k1gNnSc4	doručování
příspěvků	příspěvek	k1gInPc2	příspěvek
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
určitých	určitý	k2eAgInPc2d1	určitý
účtů	účet	k1gInPc2	účet
nebo	nebo	k8xC	nebo
povolit	povolit	k5eAaPmF	povolit
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
příspěvkům	příspěvek	k1gInPc3	příspěvek
komukoliv	kdokoliv	k3yInSc3	kdokoliv
(	(	kIx(	(
<g/>
výchozí	výchozí	k2eAgNnPc1d1	výchozí
nastavení	nastavení	k1gNnPc1	nastavení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
zasílají	zasílat	k5eAaImIp3nP	zasílat
nebo	nebo	k8xC	nebo
dostávají	dostávat	k5eAaImIp3nP	dostávat
tweety	tweet	k1gInPc1	tweet
přes	přes	k7c4	přes
stránku	stránka	k1gFnSc4	stránka
Twitteru	Twitter	k1gInSc2	Twitter
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
krátkých	krátká	k1gFnPc2	krátká
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
SMS	SMS	kA	SMS
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
externích	externí	k2eAgFnPc2d1	externí
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zasílání	zasílání	k1gNnSc1	zasílání
SMS	SMS	kA	SMS
zpráv	zpráva	k1gFnPc2	zpráva
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
běžný	běžný	k2eAgInSc4d1	běžný
poplatek	poplatek	k1gInSc4	poplatek
poskytovatele	poskytovatel	k1gMnSc2	poskytovatel
telefonních	telefonní	k2eAgFnPc2d1	telefonní
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Jackem	Jacko	k1gNnSc7	Jacko
Dorsey	Dorsea	k1gFnSc2	Dorsea
Twitter	Twitter	k1gMnSc1	Twitter
získal	získat	k5eAaPmAgMnS	získat
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
a	a	k8xC	a
popularitu	popularita	k1gFnSc4	popularita
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
SMS	SMS	kA	SMS
internetu	internet	k1gInSc3	internet
<g/>
"	"	kIx"	"
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stránky	stránka	k1gFnPc1	stránka
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
funkčnost	funkčnost	k1gFnSc4	funkčnost
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
svému	své	k1gNnSc3	své
rozhraní	rozhraní	k1gNnSc2	rozhraní
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
aplikací	aplikace	k1gFnPc2	aplikace
-	-	kIx~	-
API	API	kA	API
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
desktopové	desktopový	k2eAgFnPc4d1	desktopová
<g/>
,	,	kIx,	,
mobilní	mobilní	k2eAgFnPc4d1	mobilní
a	a	k8xC	a
webové	webový	k2eAgFnPc4d1	webová
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
odesílat	odesílat	k5eAaImF	odesílat
a	a	k8xC	a
přijímat	přijímat	k5eAaImF	přijímat
krátké	krátký	k2eAgFnPc4d1	krátká
textové	textový	k2eAgFnPc4d1	textová
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
aplikace	aplikace	k1gFnPc1	aplikace
často	často	k6eAd1	často
zastiňují	zastiňovat	k5eAaImIp3nP	zastiňovat
samotnou	samotný	k2eAgFnSc4d1	samotná
službu	služba	k1gFnSc4	služba
Twitter	Twittra	k1gFnPc2	Twittra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
měl	mít	k5eAaImAgInS	mít
Twitter	Twitter	k1gInSc1	Twitter
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
připojila	připojit	k5eAaPmAgFnS	připojit
alespoň	alespoň	k9	alespoň
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
na	na	k7c4	na
Twitter	Twitter	k1gInSc4	Twitter
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
denně	denně	k6eAd1	denně
napsali	napsat	k5eAaBmAgMnP	napsat
kolem	kolem	k7c2	kolem
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
tweetů	tweet	k1gInPc2	tweet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
Twitter	Twitter	k1gInSc1	Twitter
dostupný	dostupný	k2eAgInSc1d1	dostupný
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
v	v	k7c4	v
beta	beta	k1gNnSc4	beta
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
české	český	k2eAgFnSc6d1	Česká
jazykové	jazykový	k2eAgFnSc6d1	jazyková
lokalizaci	lokalizace	k1gFnSc6	lokalizace
<g/>
.	.	kIx.	.
</s>
<s>
Hashtag	Hashtag	k1gInSc1	Hashtag
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
anebo	anebo	k8xC	anebo
fráze	fráze	k1gFnSc1	fráze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
#	#	kIx~	#
(	(	kIx(	(
<g/>
dvojkřížkem	dvojkřížek	k1gInSc7	dvojkřížek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Twitter	Twitter	k1gInSc1	Twitter
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
také	také	k9	také
např.	např.	kA	např.
Google	Google	k1gFnSc1	Google
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
na	na	k7c4	na
hashtag	hashtag	k1gInSc4	hashtag
zareaguje	zareagovat	k5eAaPmIp3nS	zareagovat
a	a	k8xC	a
promění	proměnit	k5eAaPmIp3nS	proměnit
jej	on	k3xPp3gMnSc4	on
v	v	k7c4	v
odkaz	odkaz	k1gInSc4	odkaz
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
další	další	k2eAgInPc4d1	další
stejné	stejný	k2eAgInPc4d1	stejný
hashtagy	hashtag	k1gInPc4	hashtag
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
hashtagů	hashtag	k1gMnPc2	hashtag
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dohledat	dohledat	k5eAaPmF	dohledat
různé	různý	k2eAgInPc4d1	různý
trendy	trend	k1gInPc4	trend
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc4d1	nový
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
svátky	svátek	k1gInPc1	svátek
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
označit	označit	k5eAaPmF	označit
význam	význam	k1gInSc4	význam
příspěvků	příspěvek	k1gInPc2	příspěvek
(	(	kIx(	(
<g/>
sarkasmus	sarkasmus	k1gInSc1	sarkasmus
<g/>
,	,	kIx,	,
morální	morální	k2eAgNnSc1d1	morální
povzbuzení	povzbuzení	k1gNnSc1	povzbuzení
<g/>
,	,	kIx,	,
hněv	hněv	k1gInSc1	hněv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hashtag	Hashtag	k1gInSc4	Hashtag
byl	být	k5eAaImAgInS	být
American	American	k1gInSc1	American
Dialect	Dialect	k1gInSc1	Dialect
Society	societa	k1gFnSc2	societa
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
slovem	slovo	k1gNnSc7	slovo
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
#	#	kIx~	#
<g/>
business	business	k1gInSc4	business
-	-	kIx~	-
sdělení	sdělení	k1gNnSc4	sdělení
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
obchodu	obchod	k1gInSc2	obchod
#	#	kIx~	#
<g/>
video	video	k1gNnSc1	video
-	-	kIx~	-
tématem	téma	k1gNnSc7	téma
příspěvku	příspěvek	k1gInSc2	příspěvek
je	být	k5eAaImIp3nS	být
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
hashtag	hashtag	k1gInSc1	hashtag
dává	dávat	k5eAaImIp3nS	dávat
za	za	k7c4	za
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
adresu	adresa	k1gFnSc4	adresa
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
ff	ff	kA	ff
-	-	kIx~	-
followfriday	followfridaa	k1gMnSc2	followfridaa
-	-	kIx~	-
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
doporučíte	doporučit	k5eAaPmIp2nP	doporučit
někoho	někdo	k3yInSc4	někdo
zajímavého	zajímavý	k2eAgMnSc4d1	zajímavý
ostatním	ostatní	k1gNnSc7	ostatní
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
<g/>
.	.	kIx.	.
#	#	kIx~	#
<g/>
fail	fainout	k5eAaPmAgMnS	fainout
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
hashtag	hashtag	k1gInSc1	hashtag
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
satirický	satirický	k2eAgInSc1d1	satirický
podtext	podtext	k1gInSc1	podtext
příspěvku	příspěvek	k1gInSc2	příspěvek
<g/>
.	.	kIx.	.
#	#	kIx~	#
<g/>
yolo	yolo	k1gNnSc1	yolo
-	-	kIx~	-
You	You	k1gFnSc1	You
only	onla	k1gFnSc2	onla
live	live	k1gFnSc1	live
once	once	k1gFnSc1	once
-	-	kIx~	-
žiješ	žít	k5eAaImIp2nS	žít
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
#	#	kIx~	#
<g/>
ttylxox	ttylxox	k1gInSc1	ttylxox
-	-	kIx~	-
talk	talk	k6eAd1	talk
to	ten	k3xDgNnSc1	ten
you	you	k?	you
later	later	k1gInSc1	later
xox	xox	k?	xox
-	-	kIx~	-
promluvíme	promluvit	k5eAaPmIp1nP	promluvit
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
xox	xox	k?	xox
(	(	kIx(	(
<g/>
xox	xox	k?	xox
je	být	k5eAaImIp3nS	být
smajlík	smajlík	k?	smajlík
pro	pro	k7c4	pro
objímám	objímat	k5eAaImIp1nS	objímat
a	a	k8xC	a
dávám	dávat	k5eAaImIp1nS	dávat
pusu	pusa	k1gFnSc4	pusa
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
nejvíce	hodně	k6eAd3	hodně
sledovaných	sledovaný	k2eAgFnPc2d1	sledovaná
<g />
.	.	kIx.	.
</s>
<s>
účtů	účet	k1gInPc2	účet
<g/>
,	,	kIx,	,
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
sledujících	sledující	k2eAgMnPc2d1	sledující
(	(	kIx(	(
<g/>
followerů	follower	k1gMnPc2	follower
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Katy	Kata	k1gFnSc2	Kata
Perry	Perra	k1gFnSc2	Perra
<g/>
:	:	kIx,	:
92	[number]	k4	92
747	[number]	k4	747
154	[number]	k4	154
Justin	Justin	k1gMnSc1	Justin
Bieber	Bieber	k1gMnSc1	Bieber
<g/>
:	:	kIx,	:
87	[number]	k4	87
865	[number]	k4	865
572	[number]	k4	572
Taylor	Taylor	k1gMnSc1	Taylor
Swift	Swift	k1gMnSc1	Swift
<g/>
:	:	kIx,	:
80	[number]	k4	80
876	[number]	k4	876
353	[number]	k4	353
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
<g/>
:	:	kIx,	:
77	[number]	k4	77
327	[number]	k4	327
956	[number]	k4	956
<g />
.	.	kIx.	.
</s>
<s>
Rihanna	Rihanna	k1gFnSc1	Rihanna
<g/>
:	:	kIx,	:
65	[number]	k4	65
550	[number]	k4	550
206	[number]	k4	206
YouTube	YouTub	k1gInSc5	YouTub
<g/>
:	:	kIx,	:
63	[number]	k4	63
968	[number]	k4	968
975	[number]	k4	975
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gFnSc1	Gaga
<g/>
:	:	kIx,	:
63	[number]	k4	63
416	[number]	k4	416
774	[number]	k4	774
Ellen	Ellna	k1gFnPc2	Ellna
DeGeneresová	DeGeneresový	k2eAgFnSc1d1	DeGeneresový
<g/>
:	:	kIx,	:
62	[number]	k4	62
106	[number]	k4	106
303	[number]	k4	303
Twitter	Twittra	k1gFnPc2	Twittra
<g/>
:	:	kIx,	:
56	[number]	k4	56
751	[number]	k4	751
280	[number]	k4	280
Justin	Justina	k1gFnPc2	Justina
Timberlake	Timberlake	k1gFnPc2	Timberlake
<g/>
:	:	kIx,	:
56	[number]	k4	56
574	[number]	k4	574
198	[number]	k4	198
Nejstarších	starý	k2eAgInPc2d3	nejstarší
14	[number]	k4	14
účtů	účet	k1gInPc2	účet
bylo	být	k5eAaImAgNnS	být
aktivováno	aktivován	k2eAgNnSc4d1	aktivováno
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
@	@	kIx~	@
<g/>
jack	jack	k1gMnSc1	jack
(	(	kIx(	(
<g/>
Jack	Jack	k1gMnSc1	Jack
Dorsey	Dorsea	k1gFnSc2	Dorsea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
@	@	kIx~	@
<g/>
biz	biz	k?	biz
(	(	kIx(	(
<g/>
Biz	Biz	k1gMnSc1	Biz
Stone	ston	k1gInSc5	ston
<g/>
)	)	kIx)	)
a	a	k8xC	a
@	@	kIx~	@
<g/>
noah	noah	k1gInSc1	noah
(	(	kIx(	(
<g/>
Noah	Noah	k1gInSc1	Noah
Glass	Glass	k1gInSc1	Glass
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
všechny	všechen	k3xTgInPc4	všechen
účty	účet	k1gInPc4	účet
patřily	patřit	k5eAaImAgInP	patřit
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
Twitteru	Twitter	k1gInSc2	Twitter
<g/>
.	.	kIx.	.
</s>
<s>
Nejdiskutovanějším	diskutovaný	k2eAgNnSc7d3	nejdiskutovanější
tématem	téma	k1gNnSc7	téma
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hashtag	hashtag	k1gMnSc1	hashtag
"	"	kIx"	"
<g/>
#	#	kIx~	#
<g/>
ALDubEBTamangPanahon	ALDubEBTamangPanahon	k1gInSc1	ALDubEBTamangPanahon
<g/>
"	"	kIx"	"
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
od	od	k7c2	od
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
zamilované	zamilovaný	k2eAgFnSc2d1	zamilovaná
dvojice	dvojice	k1gFnSc2	dvojice
AlDub	AlDuba	k1gFnPc2	AlDuba
se	s	k7c7	s
41	[number]	k4	41
miliony	milion	k4xCgInPc7	milion
komentáři	komentář	k1gInPc7	komentář
(	(	kIx(	(
<g/>
tweety	tweet	k1gInPc7	tweet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
části	část	k1gFnSc3	část
představení	představení	k1gNnSc2	představení
"	"	kIx"	"
<g/>
Kalyeserye	Kalyeserye	k1gNnSc2	Kalyeserye
<g/>
"	"	kIx"	"
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
speciálního	speciální	k2eAgInSc2d1	speciální
koncertu	koncert	k1gInSc2	koncert
televizní	televizní	k2eAgFnSc2d1	televizní
show	show	k1gFnSc2	show
Eat	Eat	k1gMnSc2	Eat
Bulaga	Bulag	k1gMnSc2	Bulag
<g/>
,	,	kIx,	,
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Eat	Eat	k1gFnSc7	Eat
Bulaga	Bulaga	k1gFnSc1	Bulaga
<g/>
:	:	kIx,	:
Sa	Sa	k1gMnSc1	Sa
Tamang	Tamang	k1gMnSc1	Tamang
Panahon	Panahon	k1gMnSc1	Panahon
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
ve	v	k7c6	v
Philippine	Philippin	k1gInSc5	Philippin
Arena	Aren	k1gMnSc2	Aren
a	a	k8xC	a
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
jej	on	k3xPp3gMnSc4	on
přes	přes	k7c4	přes
55	[number]	k4	55
tisíc	tisíc	k4xCgInPc2	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Nejdiskutovanější	diskutovaný	k2eAgFnSc7d3	Nejdiskutovanější
sportovní	sportovní	k2eAgFnSc7d1	sportovní
událostí	událost	k1gFnSc7	událost
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
35,6	[number]	k4	35,6
miliony	milion	k4xCgInPc7	milion
tweety	tweet	k1gMnPc7	tweet
semifinále	semifinále	k1gNnSc4	semifinále
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2014	[number]	k4	2014
mezi	mezi	k7c7	mezi
Brazílií	Brazílie	k1gFnSc7	Brazílie
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Guinnessovy	Guinnessův	k2eAgFnSc2d1	Guinnessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
získal	získat	k5eAaPmAgMnS	získat
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
sledujících	sledující	k2eAgMnPc2d1	sledující
nejrychleji	rychle	k6eAd3	rychle
herec	herec	k1gMnSc1	herec
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
stačilo	stačit	k5eAaBmAgNnS	stačit
23	[number]	k4	23
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
22	[number]	k4	22
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Twitter	Twittra	k1gFnPc2	Twittra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Bednář	Bednář	k1gMnSc1	Bednář
<g/>
:	:	kIx,	:
Marketing	marketing	k1gInSc1	marketing
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
-	-	kIx~	-
prosaďte	prosadit	k5eAaPmRp2nP	prosadit
se	se	k3xPyFc4	se
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
a	a	k8xC	a
Twitteru	Twitter	k1gInSc6	Twitter
<g/>
,	,	kIx,	,
Computer	computer	k1gInSc1	computer
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-251-3320-0	[number]	k4	978-80-251-3320-0
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Twitter	Twittra	k1gFnPc2	Twittra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
twitter	twittra	k1gFnPc2	twittra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
(	(	kIx(	(
<g/>
mobilní	mobilní	k2eAgFnSc1d1	mobilní
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
kanál	kanál	k1gInSc1	kanál
Twitteru	Twitter	k1gInSc2	Twitter
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
