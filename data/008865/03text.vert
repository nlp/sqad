<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
MR	MR	kA	MR
oficiálním	oficiální	k2eAgInSc7d1	oficiální
státním	státní	k2eAgInSc7d1	státní
symbolem	symbol	k1gInSc7	symbol
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
reprezentujícím	reprezentující	k2eAgNnSc6d1	reprezentující
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
<g/>
Íránská	íránský	k2eAgFnSc1d1	íránská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
maďarské	maďarský	k2eAgFnSc3d1	maďarská
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zelené	zelený	k2eAgInPc4d1	zelený
a	a	k8xC	a
červené	červený	k2eAgInPc4d1	červený
pruhy	pruh	k1gInPc4	pruh
jsou	být	k5eAaImIp3nP	být
obráceny	obrácen	k2eAgInPc1d1	obrácen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
==	==	k?	==
</s>
</p>
<p>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
stejně	stejně	k6eAd1	stejně
široké	široký	k2eAgInPc4d1	široký
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
používat	používat	k5eAaImF	používat
v	v	k7c6	v
poměrech	poměr	k1gInPc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
vlajky	vlajka	k1gFnSc2	vlajka
umístěn	umístěn	k2eAgInSc1d1	umístěn
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
neoficiální	neoficiální	k2eAgFnSc6d1	neoficiální
verzi	verze	k1gFnSc6	verze
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
převažující	převažující	k2eAgFnSc1d1	převažující
tendence	tendence	k1gFnSc1	tendence
užívat	užívat	k5eAaImF	užívat
vlajku	vlajka	k1gFnSc4	vlajka
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
vlajky	vlajka	k1gFnSc2	vlajka
státní	státní	k2eAgFnSc2d1	státní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejenom	nejenom	k6eAd1	nejenom
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
prolitou	prolitý	k2eAgFnSc4d1	prolitá
vlastenci	vlastenec	k1gMnPc1	vlastenec
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
oddanost	oddanost	k1gFnSc4	oddanost
a	a	k8xC	a
čistotu	čistota	k1gFnSc4	čistota
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
naději	naděje	k1gFnSc4	naděje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nejstaršími	starý	k2eAgFnPc7d3	nejstarší
maďarskými	maďarský	k2eAgFnPc7d1	maďarská
barvami	barva	k1gFnPc7	barva
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
Arpádovské	Arpádovský	k2eAgFnSc2d1	Arpádovský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Červeno-bílo-zelená	Červenoíloelený	k2eAgFnSc1d1	Červeno-bílo-zelený
vlajka	vlajka	k1gFnSc1	vlajka
poprvé	poprvé	k6eAd1	poprvé
zavlála	zavlát	k5eAaPmAgFnS	zavlát
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
historie	historie	k1gFnSc1	historie
používání	používání	k1gNnSc2	používání
těchto	tento	k3xDgFnPc2	tento
barev	barva	k1gFnPc2	barva
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1222	[number]	k4	1222
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pečeť	pečeť	k1gFnSc1	pečeť
krále	král	k1gMnSc2	král
Ondřeje	Ondřej	k1gMnSc2	Ondřej
II	II	kA	II
<g/>
.	.	kIx.	.
přidržovala	přidržovat	k5eAaImAgFnS	přidržovat
stuha	stuha	k1gFnSc1	stuha
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
<g/>
Červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
zelená	zelená	k1gFnSc1	zelená
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
jako	jako	k9	jako
barvy	barva	k1gFnPc1	barva
Uherska	Uhersko	k1gNnSc2	Uhersko
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
barvami	barva	k1gFnPc7	barva
uherského	uherský	k2eAgInSc2d1	uherský
znaku	znak	k1gInSc2	znak
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
barvy	barva	k1gFnPc1	barva
definují	definovat	k5eAaBmIp3nP	definovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
barvami	barva	k1gFnPc7	barva
vlajky	vlajka	k1gFnSc2	vlajka
Rakouska	Rakousko	k1gNnSc2	Rakousko
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
letech	let	k1gInPc6	let
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
i	i	k9	i
ve	v	k7c6	v
vlající	vlající	k2eAgFnSc6d1	vlající
části	část	k1gFnSc6	část
obchodní	obchodní	k2eAgFnSc2d1	obchodní
vlajky	vlajka	k1gFnSc2	vlajka
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
První	první	k4xOgFnSc2	první
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
socialistické	socialistický	k2eAgFnSc2d1	socialistická
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
barvám	barva	k1gFnPc3	barva
přisuzovaly	přisuzovat	k5eAaImAgFnP	přisuzovat
(	(	kIx(	(
<g/>
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
zase	zase	k9	zase
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
interpretace	interpretace	k1gFnSc1	interpretace
<g/>
,	,	kIx,	,
že	že	k8xS	že
obětavost	obětavost	k1gFnSc1	obětavost
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
a	a	k8xC	a
čistý	čistý	k2eAgInSc4d1	čistý
charakter	charakter	k1gInSc4	charakter
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
deštěm	dešť	k1gInSc7	dešť
pro	pro	k7c4	pro
úrodnou	úrodný	k2eAgFnSc4d1	úrodná
půdu	půda	k1gFnSc4	půda
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
<g/>
,	,	kIx,	,
když	když	k8xS	když
uzraje	uzrát	k5eAaPmIp3nS	uzrát
v	v	k7c6	v
míru	mír	k1gInSc6	mír
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přinese	přinést	k5eAaPmIp3nS	přinést
bohaté	bohatý	k2eAgInPc4d1	bohatý
plody	plod	k1gInPc4	plod
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
maďarské	maďarský	k2eAgFnPc1d1	maďarská
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgFnPc1d1	podobná
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Magyarország	Magyarország	k1gMnSc1	Magyarország
zászlaja	zászlaja	k1gMnSc1	zászlaja
na	na	k7c6	na
maďarské	maďarský	k2eAgFnSc6d1	maďarská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BROŽEK	Brožek	k1gMnSc1	Brožek
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gNnSc1	lexikon
vlajek	vlajka	k1gFnPc2	vlajka
a	a	k8xC	a
znaků	znak	k1gInPc2	znak
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
223	[number]	k4	223
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7011	[number]	k4	7011
<g/>
-	-	kIx~	-
<g/>
776	[number]	k4	776
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUCHA	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
a	a	k8xC	a
Znaky	znak	k1gInPc1	znak
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
GKP	GKP	kA	GKP
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
215	[number]	k4	215
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
maďarská	maďarský	k2eAgFnSc1d1	maďarská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Flags	Flags	k1gInSc1	Flags
of	of	k?	of
Hungary	Hungara	k1gFnSc2	Hungara
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
National	National	k1gFnSc1	National
flag	flag	k1gInSc1	flag
of	of	k?	of
Hungary	Hungara	k1gFnSc2	Hungara
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Flags	Flags	k1gInSc1	Flags
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
-	-	kIx~	-
Hungary	Hungara	k1gFnPc1	Hungara
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Magyar	Magyar	k1gMnSc1	Magyar
Állami	Álla	k1gFnPc7	Álla
Jelképek	Jelképek	k1gInSc1	Jelképek
-	-	kIx~	-
Nemzeti	Nemzeti	k1gFnSc1	Nemzeti
zászló	zászló	k?	zászló
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Magyarország	Magyarország	k1gInSc4	Magyarország
nemzeti	mzet	k5eNaPmF	mzet
jelképei	jelképei	k6eAd1	jelképei
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
)	)	kIx)	)
Magyarország	Magyarország	k1gInSc1	Magyarország
legfontosabb	legfontosabba	k1gFnPc2	legfontosabba
jelképei	jelképe	k1gFnSc2	jelképe
és	és	k?	és
ereklyéi	ereklyé	k1gFnSc2	ereklyé
</s>
</p>
