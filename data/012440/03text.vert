<p>
<s>
Olgoj	Olgoj	k1gInSc1	Olgoj
chorchoj	chorchoj	k1gInSc1	chorchoj
(	(	kIx(	(
<g/>
mongolsky	mongolsky	k6eAd1	mongolsky
о	о	k?	о
х	х	k?	х
<g/>
;	;	kIx,	;
olgoj	olgoj	k1gInSc1	olgoj
=	=	kIx~	=
červovitý	červovitý	k2eAgInSc1d1	červovitý
přívěsek	přívěsek	k1gInSc1	přívěsek
<g/>
,	,	kIx,	,
chorchoj	chorchoj	k1gInSc1	chorchoj
=	=	kIx~	=
červ	červ	k1gMnSc1	červ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mytický	mytický	k2eAgMnSc1d1	mytický
písečný	písečný	k2eAgMnSc1d1	písečný
červ	červ	k1gMnSc1	červ
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výskyt	výskyt	k1gInSc1	výskyt
se	se	k3xPyFc4	se
často	často	k6eAd1	často
situuje	situovat	k5eAaBmIp3nS	situovat
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
asijské	asijský	k2eAgFnSc2d1	asijská
poušti	poušť	k1gFnSc3	poušť
Gobi	Gobi	k1gNnSc6	Gobi
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
tohoto	tento	k3xDgMnSc4	tento
tvora	tvor	k1gMnSc4	tvor
nebyla	být	k5eNaImAgFnS	být
dokázána	dokázán	k2eAgFnSc1d1	dokázána
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Setkání	setkání	k1gNnSc1	setkání
a	a	k8xC	a
pátrání	pátrání	k1gNnSc1	pátrání
==	==	k?	==
</s>
</p>
<p>
<s>
Ač	ač	k8xS	ač
existují	existovat	k5eAaImIp3nP	existovat
svědectví	svědectví	k1gNnSc4	svědectví
několika	několik	k4yIc2	několik
očitých	očitý	k2eAgMnPc2d1	očitý
svědků	svědek	k1gMnPc2	svědek
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
legend	legenda	k1gFnPc2	legenda
o	o	k7c6	o
tomto	tento	k3xDgMnSc6	tento
pouštním	pouštní	k2eAgMnSc6d1	pouštní
červu	červ	k1gMnSc6	červ
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pochybná	pochybný	k2eAgFnSc1d1	pochybná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
vědců	vědec	k1gMnPc2	vědec
tohoto	tento	k3xDgMnSc2	tento
mytického	mytický	k2eAgMnSc2d1	mytický
tvora	tvor	k1gMnSc2	tvor
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
Mongoly	Mongol	k1gMnPc4	Mongol
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
nadpřirozenou	nadpřirozený	k2eAgFnSc4d1	nadpřirozená
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
jen	jen	k9	jen
vyslovit	vyslovit	k5eAaPmF	vyslovit
<g/>
.	.	kIx.	.
</s>
<s>
Svědectví	svědectví	k1gNnPc1	svědectví
jsou	být	k5eAaImIp3nP	být
údajně	údajně	k6eAd1	údajně
téměř	téměř	k6eAd1	téměř
shodná	shodný	k2eAgFnSc1d1	shodná
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
nelze	lze	k6eNd1	lze
možnost	možnost	k1gFnSc1	možnost
existence	existence	k1gFnSc2	existence
zcela	zcela	k6eAd1	zcela
jednoznačně	jednoznačně	k6eAd1	jednoznačně
popřít	popřít	k5eAaPmF	popřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Údajně	údajně	k6eAd1	údajně
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
situace	situace	k1gFnPc1	situace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pastevci	pastevec	k1gMnPc1	pastevec
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
olgojem	olgoj	k1gInSc7	olgoj
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
takový	takový	k3xDgInSc4	takový
příběh	příběh	k1gInSc1	příběh
popisuje	popisovat	k5eAaImIp3nS	popisovat
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jednou	jednou	k6eAd1	jednou
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
jurty	jurta	k1gFnSc2	jurta
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
tam	tam	k6eAd1	tam
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
ležet	ležet	k5eAaImF	ležet
olgoje	olgoj	k1gInPc4	olgoj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
popadl	popadnout	k5eAaPmAgMnS	popadnout
ocelový	ocelový	k2eAgInSc4d1	ocelový
kotlík	kotlík	k1gInSc4	kotlík
na	na	k7c4	na
čaj	čaj	k1gInSc4	čaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jurt	jurta	k1gFnPc2	jurta
<g/>
,	,	kIx,	,
olgoje	olgoj	k1gInPc4	olgoj
jím	jíst	k5eAaImIp1nS	jíst
přikryl	přikrýt	k5eAaPmAgMnS	přikrýt
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
ven	ven	k6eAd1	ven
před	před	k7c4	před
jurtu	jurta	k1gFnSc4	jurta
<g/>
.	.	kIx.	.
</s>
<s>
Kotlík	kotlík	k1gInSc1	kotlík
ovšem	ovšem	k9	ovšem
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
zežloutl	zežloutnout	k5eAaPmAgMnS	zežloutnout
a	a	k8xC	a
jevil	jevit	k5eAaImAgMnS	jevit
známky	známka	k1gFnPc4	známka
jako	jako	k9	jako
po	po	k7c4	po
leptání	leptání	k1gNnSc4	leptání
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
údajně	údajně	k6eAd1	údajně
všechny	všechen	k3xTgFnPc4	všechen
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přišly	přijít	k5eAaPmAgFnP	přijít
do	do	k7c2	do
přímého	přímý	k2eAgInSc2d1	přímý
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
olgojem	olgoj	k1gInSc7	olgoj
<g/>
,	,	kIx,	,
žloutnou	žloutnout	k5eAaImIp3nP	žloutnout
kvůli	kvůli	k7c3	kvůli
působení	působení	k1gNnSc3	působení
silného	silný	k2eAgInSc2d1	silný
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
takového	takový	k3xDgNnSc2	takový
složení	složení	k1gNnSc2	složení
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
leptat	leptat	k5eAaImF	leptat
i	i	k9	i
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
textech	text	k1gInPc6	text
o	o	k7c6	o
olgoji	olgoj	k1gInSc6	olgoj
objevují	objevovat	k5eAaImIp3nP	objevovat
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jenom	jenom	k9	jenom
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Pátrat	pátrat	k5eAaImF	pátrat
se	se	k3xPyFc4	se
po	po	k7c6	po
olgojovi	olgoj	k1gMnSc6	olgoj
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
česká	český	k2eAgFnSc1d1	Česká
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1991	[number]	k4	1991
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Fate	Fat	k1gInSc2	Fat
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Ivan	Ivan	k1gMnSc1	Ivan
Mackerle	Mackerle	k1gFnSc2	Mackerle
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
olgoj	olgoj	k1gInSc1	olgoj
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
zoologové	zoolog	k1gMnPc1	zoolog
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
scinkovitých	scinkovitý	k2eAgFnPc2d1	scinkovitý
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
–	–	k?	–
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
málo	málo	k6eAd1	málo
podobní	podobný	k2eAgMnPc1d1	podobný
plazům	plaz	k1gInPc3	plaz
<g/>
,	,	kIx,	,
např.	např.	kA	např.
australský	australský	k2eAgMnSc1d1	australský
scink	scink	k1gMnSc1	scink
uťatý	uťatý	k2eAgMnSc1d1	uťatý
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hlava	hlava	k1gFnSc1	hlava
i	i	k8xC	i
ocas	ocas	k1gInSc1	ocas
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
rozeznatelné	rozeznatelný	k2eAgNnSc4d1	rozeznatelné
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
scinci	scinek	k1gMnPc1	scinek
mají	mít	k5eAaImIp3nP	mít
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
nožky	nožka	k1gFnSc2	nožka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
všechna	všechen	k3xTgNnPc4	všechen
svědectví	svědectví	k1gNnPc4	svědectví
o	o	k7c6	o
olgojovi	olgojův	k2eAgMnPc1d1	olgojův
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
bez	bez	k7c2	bez
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xS	jako
hladký	hladký	k2eAgMnSc1d1	hladký
živočich	živočich	k1gMnSc1	živočich
připomínající	připomínající	k2eAgFnSc4d1	připomínající
přerostlou	přerostlý	k2eAgFnSc4d1	přerostlá
žížalu	žížala	k1gFnSc4	žížala
nebo	nebo	k8xC	nebo
také	také	k9	také
tlusté	tlustý	k2eAgNnSc4d1	tlusté
střevo	střevo	k1gNnSc4	střevo
naplněné	naplněný	k2eAgNnSc4d1	naplněné
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
údajně	údajně	k6eAd1	údajně
délky	délka	k1gFnPc1	délka
0,5	[number]	k4	0,5
až	až	k9	až
1,5	[number]	k4	1,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
jeho	jeho	k3xOp3gInSc2	jeho
zevnějšku	zevnějšek	k1gInSc2	zevnějšek
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
výraz	výraz	k1gInSc4	výraz
"	"	kIx"	"
<g/>
šiška	šiška	k1gFnSc1	šiška
salámu	salám	k1gInSc2	salám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
výpovědí	výpověď	k1gFnPc2	výpověď
dokáže	dokázat	k5eAaPmIp3nS	dokázat
člověka	člověk	k1gMnSc4	člověk
usmrtit	usmrtit	k5eAaPmF	usmrtit
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
až	až	k6eAd1	až
šesti	šest	k4xCc2	šest
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
narůžovělá	narůžovělý	k2eAgFnSc1d1	narůžovělá
barva	barva	k1gFnSc1	barva
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
rudou	rudý	k2eAgFnSc4d1	rudá
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
vztyčí	vztyčit	k5eAaPmIp3nS	vztyčit
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
velmi	velmi	k6eAd1	velmi
ztmavne	ztmavnout	k5eAaPmIp3nS	ztmavnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
jen	jen	k9	jen
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
nejteplejším	teplý	k2eAgNnSc6d3	nejteplejší
období	období	k1gNnSc6	období
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zahrabává	zahrabávat	k5eAaImIp3nS	zahrabávat
do	do	k7c2	do
písku	písek	k1gInSc2	písek
nebo	nebo	k8xC	nebo
zalézá	zalézat	k5eAaImIp3nS	zalézat
do	do	k7c2	do
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
se	se	k3xPyFc4	se
neplazí	plazit	k5eNaImIp3nS	plazit
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
koulí	koulet	k5eAaImIp3nS	koulet
se	se	k3xPyFc4	se
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
zametal	zametat	k5eAaImAgMnS	zametat
<g/>
,	,	kIx,	,
a	a	k8xC	a
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
tak	tak	k6eAd1	tak
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
verze	verze	k1gFnSc1	verze
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
olgoj	olgoj	k1gInSc1	olgoj
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
útok	útok	k1gInSc4	útok
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
generuje	generovat	k5eAaImIp3nS	generovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
pomocí	pomocí	k7c2	pomocí
výboje	výboj	k1gInSc2	výboj
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
olgoj	olgoj	k1gInSc1	olgoj
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
vak	vak	k1gInSc4	vak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lépe	dobře	k6eAd2	dobře
bublinu	bublina	k1gFnSc4	bublina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
naplní	naplnit	k5eAaPmIp3nS	naplnit
jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pomocí	pomocí	k7c2	pomocí
složek	složka	k1gFnPc2	složka
své	svůj	k3xOyFgFnSc2	svůj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
a	a	k8xC	a
rychlým	rychlý	k2eAgNnSc7d1	rychlé
smrštěním	smrštění	k1gNnSc7	smrštění
bubliny	bublina	k1gFnSc2	bublina
naplněné	naplněný	k2eAgFnSc2d1	naplněná
jedem	jed	k1gInSc7	jed
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
protržení	protržení	k1gNnSc3	protržení
a	a	k8xC	a
vystříknutí	vystříknutí	k1gNnSc3	vystříknutí
jedu	jed	k1gInSc2	jed
na	na	k7c4	na
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
jedu	jed	k1gInSc2	jed
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
silné	silný	k2eAgFnPc1d1	silná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabilo	zabít	k5eAaPmAgNnS	zabít
dospělého	dospělý	k1gMnSc4	dospělý
člověka	člověk	k1gMnSc4	člověk
po	po	k7c6	po
několika	několik	k4yIc6	několik
sekundách	sekunda	k1gFnPc6	sekunda
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturní	kulturní	k2eAgInSc1d1	kulturní
odraz	odraz	k1gInSc1	odraz
==	==	k?	==
</s>
</p>
<p>
<s>
Červ	červ	k1gMnSc1	červ
olgoj	olgoj	k1gInSc4	olgoj
chorchoj	chorchoj	k1gInSc1	chorchoj
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
povídce	povídka	k1gFnSc6	povídka
Ivana	Ivan	k1gMnSc2	Ivan
Jefremova	Jefremův	k2eAgMnSc2d1	Jefremův
(	(	kIx(	(
<g/>
napsána	napsán	k2eAgFnSc1d1	napsána
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
krátce	krátce	k6eAd1	krátce
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
Planetě	planeta	k1gFnSc6	planeta
nachových	nachový	k2eAgNnPc2d1	nachové
mračen	mračno	k1gNnPc2	mračno
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dvou	dva	k4xCgInPc6	dva
dílech	díl	k1gInPc6	díl
bratrů	bratr	k1gMnPc2	bratr
Arkadije	Arkadije	k1gMnSc2	Arkadije
a	a	k8xC	a
Borise	Boris	k1gMnSc2	Boris
Strugackých	Strugackých	k2eAgMnSc2d1	Strugackých
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
námětem	námět	k1gInSc7	námět
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
příběhů	příběh	k1gInPc2	příběh
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
komiksovém	komiksový	k2eAgInSc6d1	komiksový
týdeníku	týdeník	k1gInSc6	týdeník
2000	[number]	k4	2000
AD	ad	k7c4	ad
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
románu	román	k1gInSc6	román
Spook	Spook	k1gInSc1	Spook
Country	country	k2eAgInSc1d1	country
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
spisovatele	spisovatel	k1gMnSc2	spisovatel
Williama	William	k1gMnSc2	William
Gibsona	Gibson	k1gMnSc2	Gibson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
olgoj	olgoj	k1gInSc1	olgoj
chorchoj	chorchoj	k1gInSc1	chorchoj
inspirací	inspirace	k1gFnPc2	inspirace
Franku	frank	k1gInSc6	frank
Herbertovi	Herbertův	k2eAgMnPc1d1	Herbertův
pro	pro	k7c4	pro
pouštní	pouštní	k2eAgMnPc4d1	pouštní
červy	červ	k1gMnPc4	červ
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
slavném	slavný	k2eAgInSc6d1	slavný
románu	román	k1gInSc6	román
Duna	duna	k1gFnSc1	duna
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Olgoj	Olgoj	k1gInSc1	Olgoj
chorchoj	chorchoj	k1gInSc1	chorchoj
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
v	v	k7c6	v
bestiáři	bestiář	k1gInSc6	bestiář
Dračího	dračí	k2eAgNnSc2d1	dračí
doupěte	doupě	k1gNnSc2	doupě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olgoj	Olgoj	k1gInSc1	Olgoj
Chorchoj	Chorchoj	k1gInSc1	Chorchoj
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
i	i	k9	i
píseň	píseň	k1gFnSc4	píseň
skupiny	skupina	k1gFnSc2	skupina
Medvěd	medvěd	k1gMnSc1	medvěd
009	[number]	k4	009
</s>
</p>
<p>
<s>
Olgoj	Olgoj	k1gInSc1	Olgoj
chorchoj	chorchoj	k1gInSc1	chorchoj
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Součka	Souček	k1gMnSc4	Souček
Krotitelé	krotitel	k1gMnPc1	krotitel
ďáblů	ďábel	k1gMnPc2	ďábel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olgoj	Olgoj	k1gInSc1	Olgoj
Chorchoj	Chorchoj	k1gInSc1	Chorchoj
(	(	kIx(	(
<g/>
studio	studio	k1gNnSc1	studio
<g/>
)	)	kIx)	)
–	–	k?	–
známé	známý	k2eAgNnSc1d1	známé
české	český	k2eAgNnSc1d1	české
designérské	designérský	k2eAgNnSc1d1	designérské
studio	studio	k1gNnSc1	studio
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Svatoš	Svatoš	k1gMnSc1	Svatoš
<g/>
:	:	kIx,	:
Žijí	žít	k5eAaImIp3nP	žít
nestvůry	nestvůra	k1gFnPc1	nestvůra
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
;	;	kIx,	;
časopis	časopis	k1gInSc1	časopis
ABC	ABC	kA	ABC
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
II	II	kA	II
<g/>
:	:	kIx,	:
Bestiář	bestiář	k1gInSc1	bestiář
<g/>
;	;	kIx,	;
Altar	Altar	k1gInSc1	Altar
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85979	[number]	k4	85979
<g/>
-	-	kIx~	-
<g/>
69	[number]	k4	69
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
160	[number]	k4	160
s	s	k7c7	s
</s>
</p>
<p>
<s>
Lada	Lada	k1gFnSc1	Lada
Hubatová-Vacková	Hubatová-Vacková	k1gFnSc1	Hubatová-Vacková
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Koryčánek	Koryčánek	k1gMnSc1	Koryčánek
<g/>
:	:	kIx,	:
Olgoj	Olgoj	k1gInSc1	Olgoj
Chorchoj	Chorchoj	k1gInSc1	Chorchoj
<g/>
:	:	kIx,	:
Logika	logika	k1gFnSc1	logika
Emoce	emoce	k1gFnSc1	emoce
<g/>
;	;	kIx,	;
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7027	[number]	k4	7027
<g/>
-	-	kIx~	-
<g/>
301	[number]	k4	301
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
256	[number]	k4	256
s	s	k7c7	s
(	(	kIx(	(
<g/>
profesní	profesní	k2eAgFnSc1d1	profesní
dráha	dráha	k1gFnSc1	dráha
studia	studio	k1gNnSc2	studio
Olgoj	Olgoj	k1gInSc4	Olgoj
Chorchoj	Chorchoj	k1gInSc4	Chorchoj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Mackerle	Mackerle	k1gNnSc2	Mackerle
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Skupien	Skupien	k1gInSc1	Skupien
<g/>
:	:	kIx,	:
Záhada	záhada	k1gFnSc1	záhada
písečného	písečný	k2eAgMnSc2d1	písečný
netvora	netvor	k1gMnSc2	netvor
<g/>
,	,	kIx,	,
československý	československý	k2eAgInSc1d1	československý
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
34	[number]	k4	34
minut	minuta	k1gFnPc2	minuta
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olgoj	Olgoj	k1gInSc1	Olgoj
chorchoj	chorchoj	k1gInSc4	chorchoj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Olgoj	Olgoj	k1gInSc1	Olgoj
chorchoj	chorchoj	k1gInSc1	chorchoj
–	–	k?	–
střevo	střevo	k1gNnSc4	střevo
plné	plný	k2eAgFnSc2d1	plná
krve	krev	k1gFnSc2	krev
</s>
</p>
<p>
<s>
Náplava	náplava	k1gFnSc1	náplava
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
:	:	kIx,	:
Tajemný	tajemný	k2eAgMnSc1d1	tajemný
démon	démon	k1gMnSc1	démon
pouště	poušť	k1gFnSc2	poušť
Gobi	Gobi	k1gFnSc2	Gobi
–	–	k?	–
Koktejl	koktejl	k1gInSc1	koktejl
<g/>
,	,	kIx,	,
prosinec	prosinec	k1gInSc1	prosinec
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
registrované	registrovaný	k2eAgInPc4d1	registrovaný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mackerle	Mackerle	k1gFnSc1	Mackerle
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Youtube	Youtubat	k5eAaPmIp3nS	Youtubat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Záhada	záhada	k1gFnSc1	záhada
písečného	písečný	k2eAgMnSc2d1	písečný
netvora	netvor	k1gMnSc2	netvor
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Interview	interview	k1gNnSc1	interview
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Mackerle	Mackerle	k1gFnSc2	Mackerle
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Mongolian	Mongolian	k1gMnSc1	Mongolian
Death	Death	k1gMnSc1	Death
Worm	Worm	k1gMnSc1	Worm
–	–	k?	–
theories	theories	k1gMnSc1	theories
</s>
</p>
