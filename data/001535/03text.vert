<s>
Max	Max	k1gMnSc1	Max
Karl	Karl	k1gMnSc1	Karl
Ernst	Ernst	k1gMnSc1	Ernst
Ludwig	Ludwig	k1gMnSc1	Ludwig
Planck	Planck	k1gMnSc1	Planck
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1858	[number]	k4	1858
Kiel	Kiel	k1gInSc1	Kiel
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1947	[number]	k4	1947
Göttingen	Göttingen	k1gNnSc2	Göttingen
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
u	u	k7c2	u
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
německých	německý	k2eAgMnPc2d1	německý
fyziků	fyzik	k1gMnPc2	fyzik
Helmholtze	Helmholtze	k1gFnSc2	Helmholtze
a	a	k8xC	a
Kirchhoffa	Kirchhoff	k1gMnSc2	Kirchhoff
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1879	[number]	k4	1879
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc4	doktorát
na	na	k7c6	na
mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
dokončil	dokončit	k5eAaPmAgMnS	dokončit
dizertační	dizertační	k2eAgFnSc4d1	dizertační
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
soukromým	soukromý	k2eAgMnSc7d1	soukromý
docentem	docent	k1gMnSc7	docent
na	na	k7c6	na
mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
otce	otec	k1gMnSc2	otec
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Kielu	Kiel	k1gInSc6	Kiel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
po	po	k7c6	po
Helmholtzovi	Helmholtz	k1gMnSc3	Helmholtz
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
řádného	řádný	k2eAgMnSc2d1	řádný
profesora	profesor	k1gMnSc2	profesor
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
své	svůj	k3xOyFgFnSc2	svůj
vědecké	vědecký	k2eAgFnSc2d1	vědecká
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
termodynamice	termodynamika	k1gFnSc6	termodynamika
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gNnSc4	on
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
Kirchhoffova	Kirchhoffův	k2eAgFnSc1d1	Kirchhoffova
práce	práce	k1gFnSc1	práce
o	o	k7c6	o
záření	záření	k1gNnSc6	záření
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
problémem	problém	k1gInSc7	problém
záření	záření	k1gNnSc2	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
největších	veliký	k2eAgInPc2d3	veliký
vědeckých	vědecký	k2eAgInPc2d1	vědecký
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
objevil	objevit	k5eAaPmAgInS	objevit
základní	základní	k2eAgFnSc4d1	základní
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
konstantu	konstanta	k1gFnSc4	konstanta
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Planckova	Planckov	k1gInSc2	Planckov
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
popsal	popsat	k5eAaPmAgInS	popsat
sadu	sada	k1gFnSc4	sada
tzv.	tzv.	kA	tzv.
Planckových	Planckův	k2eAgFnPc2d1	Planckova
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
udávající	udávající	k2eAgNnPc1d1	udávající
přirozená	přirozený	k2eAgNnPc1d1	přirozené
měřítka	měřítko	k1gNnPc1	měřítko
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
objevil	objevit	k5eAaPmAgInS	objevit
správný	správný	k2eAgInSc1d1	správný
zákon	zákon	k1gInSc1	zákon
vyzařování	vyzařování	k1gNnSc2	vyzařování
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
-	-	kIx~	-
průlomový	průlomový	k2eAgInSc1d1	průlomový
článek	článek	k1gInSc1	článek
v	v	k7c4	v
Annalen	Annalen	k2eAgInSc4d1	Annalen
der	drát	k5eAaImRp2nS	drát
Physik	Physik	k1gInSc1	Physik
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
zákona	zákon	k1gInSc2	zákon
vyzařování	vyzařování	k1gNnSc2	vyzařování
formuloval	formulovat	k5eAaImAgMnS	formulovat
hypotézu	hypotéza	k1gFnSc4	hypotéza
kvantování	kvantování	k1gNnSc4	kvantování
energie	energie	k1gFnSc2	energie
oscilátorů	oscilátor	k1gInPc2	oscilátor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
dosah	dosah	k1gInSc1	dosah
myšlenky	myšlenka	k1gFnSc2	myšlenka
kvantování	kvantování	k1gNnSc1	kvantování
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
teprve	teprve	k6eAd1	teprve
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
postuloval	postulovat	k5eAaImAgMnS	postulovat
kvantování	kvantování	k1gNnPc4	kvantování
energie	energie	k1gFnSc2	energie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
teorií	teorie	k1gFnSc7	teorie
okamžitě	okamžitě	k6eAd1	okamžitě
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1905	[number]	k4	1905
-	-	kIx~	-
1909	[number]	k4	1909
předsedal	předsedat	k5eAaImAgMnS	předsedat
Německé	německý	k2eAgFnSc3d1	německá
fyzikální	fyzikální	k2eAgFnSc3d1	fyzikální
společnosti	společnost	k1gFnSc3	společnost
(	(	kIx(	(
<g/>
Deutsche	Deutsche	k1gInSc1	Deutsche
Physikalische	Physikalische	k1gNnSc1	Physikalische
Gesellschaft	Gesellschaft	k1gInSc1	Gesellschaft
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
záření	záření	k1gNnSc6	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelová	k1gFnSc7	Nobelová
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
,	,	kIx,	,
estetice	estetika	k1gFnSc3	estetika
a	a	k8xC	a
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
změnil	změnit	k5eAaPmAgInS	změnit
rasové	rasový	k2eAgInPc4d1	rasový
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
ušetřil	ušetřit	k5eAaPmAgMnS	ušetřit
vědce	vědec	k1gMnSc4	vědec
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Erwin	Erwin	k1gMnSc1	Erwin
umučen	umučit	k5eAaPmNgMnS	umučit
gestapem	gestapo	k1gNnSc7	gestapo
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
neúspěšným	úspěšný	k2eNgInSc7d1	neúspěšný
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Pour	Pour	k1gMnSc1	Pour
le	le	k?	le
Mérite	Mérit	k1gInSc5	Mérit
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
Lorentzova	Lorentzův	k2eAgFnSc1d1	Lorentzova
medaile	medaile	k1gFnSc1	medaile
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Adlerschild	Adlerschild	k1gInSc1	Adlerschild
des	des	k1gNnSc1	des
Deutschen	Deutschen	k1gInSc1	Deutschen
Reiches	Reiches	k1gMnSc1	Reiches
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Medaile	medaile	k1gFnSc1	medaile
Maxe	Max	k1gMnSc2	Max
Plancka	Plancka	k1gFnSc1	Plancka
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
čestné	čestný	k2eAgInPc1d1	čestný
doktoráty	doktorát	k1gInPc1	doktorát
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
,	,	kIx,	,
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
Rostocku	Rostock	k1gInSc6	Rostock
<g/>
,	,	kIx,	,
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
Aténách	Atény	k1gFnPc6	Atény
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnPc1	Cambridge
<g/>
,	,	kIx,	,
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Glasgowě	Glasgow	k1gInSc6	Glasgow
Asteroid	asteroida	k1gFnPc2	asteroida
1069	[number]	k4	1069
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
"	"	kIx"	"
<g/>
Stella	Stella	k1gFnSc1	Stella
Planckia	Planckia	k1gFnSc1	Planckia
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Planck	Planck	k1gMnSc1	Planck
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
Vorlesungen	Vorlesungen	k1gInSc1	Vorlesungen
über	über	k1gMnSc1	über
Thermodynamik	Thermodynamik	k1gMnSc1	Thermodynamik
Planck	Planck	k1gMnSc1	Planck
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Entropy	Entropa	k1gFnPc1	Entropa
and	and	k?	and
Temperature	Temperatur	k1gMnSc5	Temperatur
of	of	k?	of
Radiant	radiant	k1gInSc1	radiant
Heat	Heat	k1gInSc1	Heat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Annalen	Annalen	k2eAgInSc1d1	Annalen
der	drát	k5eAaImRp2nS	drát
Physik	Physikum	k1gNnPc2	Physikum
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
no	no	k9	no
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
April	April	k1gMnSc1	April
<g/>
,	,	kIx,	,
pg	pg	k?	pg
<g/>
.	.	kIx.	.
719	[number]	k4	719
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
Planck	Planck	k1gMnSc1	Planck
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Law	Law	k1gFnSc3	Law
of	of	k?	of
Distribution	Distribution	k1gInSc4	Distribution
of	of	k?	of
Energy	Energ	k1gInPc4	Energ
in	in	k?	in
the	the	k?	the
Normal	Normal	k1gInSc1	Normal
Spectrum	Spectrum	k1gNnSc1	Spectrum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Annalen	Annalen	k2eAgInSc4d1	Annalen
der	drát	k5eAaImRp2nS	drát
Physik	Physikum	k1gNnPc2	Physikum
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
p.	p.	k?	p.
553	[number]	k4	553
ff	ff	kA	ff
<g/>
.	.	kIx.	.
</s>
