<s>
Trog	trog	k1gInSc1	trog
neboli	neboli	k8xC	neboli
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
údolí	údolí	k1gNnSc2	údolí
s	s	k7c7	s
typickým	typický	k2eAgInSc7d1	typický
tvarem	tvar	k1gInSc7	tvar
písmene	písmeno	k1gNnSc2	písmeno
U	U	kA	U
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
erozivní	erozivní	k2eAgFnSc2d1	erozivní
činnosti	činnost	k1gFnSc2	činnost
postupujícího	postupující	k2eAgInSc2d1	postupující
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
