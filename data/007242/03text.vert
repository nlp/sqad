<s>
Trog	trog	k1gInSc1	trog
neboli	neboli	k8xC	neboli
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
údolí	údolí	k1gNnSc2	údolí
s	s	k7c7	s
typickým	typický	k2eAgInSc7d1	typický
tvarem	tvar	k1gInSc7	tvar
písmene	písmeno	k1gNnSc2	písmeno
U	U	kA	U
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
erozivní	erozivní	k2eAgFnSc2d1	erozivní
činnosti	činnost	k1gFnSc2	činnost
postupujícího	postupující	k2eAgInSc2d1	postupující
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Vznikající	vznikající	k2eAgInSc1d1	vznikající
ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
karu	kar	k1gInSc6	kar
(	(	kIx(	(
<g/>
ledovcovém	ledovcový	k2eAgInSc6d1	ledovcový
kotli	kotel	k1gInSc6	kotel
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
tendence	tendence	k1gFnSc1	tendence
sestupovat	sestupovat	k5eAaImF	sestupovat
po	po	k7c6	po
zakřiveném	zakřivený	k2eAgInSc6d1	zakřivený
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pomalu	pomalu	k6eAd1	pomalu
odtéká	odtékat	k5eAaImIp3nS	odtékat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
ledovcových	ledovcový	k2eAgInPc6d1	ledovcový
splazech	splaz	k1gInPc6	splaz
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
odtékání	odtékání	k1gNnSc2	odtékání
tlačí	tlačit	k5eAaImIp3nS	tlačit
před	před	k7c4	před
(	(	kIx(	(
<g/>
a	a	k8xC	a
pod	pod	k7c4	pod
<g/>
)	)	kIx)	)
sebou	se	k3xPyFc7	se
velikou	veliký	k2eAgFnSc4d1	veliká
změť	změť	k1gFnSc4	změť
klastů	klast	k1gInPc2	klast
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Působící	působící	k2eAgInSc1d1	působící
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
klasty	klast	k1gInPc4	klast
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zabořují	zabořovat	k5eAaImIp3nP	zabořovat
do	do	k7c2	do
podloží	podloží	k1gNnSc2	podloží
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obrušují	obrušovat	k5eAaImIp3nP	obrušovat
a	a	k8xC	a
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dávají	dávat	k5eAaImIp3nP	dávat
podnět	podnět	k1gInSc4	podnět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
obrušování	obrušování	k1gNnSc2	obrušování
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
odolnosti	odolnost	k1gFnSc6	odolnost
rozrušované	rozrušovaný	k2eAgFnSc2d1	rozrušovaný
horniny	hornina	k1gFnSc2	hornina
a	a	k8xC	a
na	na	k7c4	na
spádnici	spádnice	k1gFnSc4	spádnice
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yIgFnPc4	který
ledovec	ledovec	k1gInSc1	ledovec
odtéká	odtékat	k5eAaImIp3nS	odtékat
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
ledovec	ledovec	k1gInSc1	ledovec
rozrušuje	rozrušovat	k5eAaImIp3nS	rozrušovat
podklad	podklad	k1gInSc4	podklad
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
spádnici	spádnice	k1gFnSc4	spádnice
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
rychleji	rychle	k6eAd2	rychle
odtéká	odtékat	k5eAaImIp3nS	odtékat
a	a	k8xC	a
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
erozivní	erozivní	k2eAgFnSc4d1	erozivní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
tlačící	tlačící	k2eAgFnSc2d1	tlačící
masy	masa	k1gFnSc2	masa
úlomků	úlomek	k1gInPc2	úlomek
se	se	k3xPyFc4	se
údolí	údolí	k1gNnSc2	údolí
neustále	neustále	k6eAd1	neustále
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
a	a	k8xC	a
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
údolí	údolí	k1gNnSc1	údolí
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
o	o	k7c4	o
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
dokonce	dokonce	k9	dokonce
u	u	k7c2	u
fjordů	fjord	k1gInPc2	fjord
až	až	k9	až
o	o	k7c4	o
2	[number]	k4	2
km	km	kA	km
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
problematika	problematika	k1gFnSc1	problematika
složitější	složitý	k2eAgFnSc1d2	složitější
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
výšky	výška	k1gFnSc2	výška
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trog	trog	k1gInSc1	trog
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
