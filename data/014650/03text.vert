<s>
Hospodářské	hospodářský	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
</s>
<s>
Tur	tur	k1gMnSc1
domácí	domácí	k1gMnSc1
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc4
z	z	k7c2
nejběžnějších	běžný	k2eAgNnPc2d3
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
</s>
<s>
Kur	kur	k1gMnSc1
domácí	domácí	k2eAgFnSc2d1
</s>
<s>
Hospodářské	hospodářský	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
je	být	k5eAaImIp3nS
zvíře	zvíře	k1gNnSc1
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yRgInSc2
se	se	k3xPyFc4
sleduje	sledovat	k5eAaImIp3nS
přímý	přímý	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
užitek	užitek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pojem	pojem	k1gInSc1
se	se	k3xPyFc4
obsahově	obsahově	k6eAd1
překrývá	překrývat	k5eAaImIp3nS
s	s	k7c7
termínem	termín	k1gInSc7
domácí	domácí	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
<g/>
,	,	kIx,
nejde	jít	k5eNaImIp3nS
však	však	k9
o	o	k7c4
synonyma	synonymum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
domácí	domácí	k1gMnPc4
(	(	kIx(
<g/>
domestikovaná	domestikovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
zvířata	zvíře	k1gNnPc1
patří	patřit	k5eAaImIp3nP
jak	jak	k6eAd1
druhy	druh	k1gInPc1
chované	chovaný	k2eAgInPc1d1
pro	pro	k7c4
hospodářský	hospodářský	k2eAgInSc4d1
užitek	užitek	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
zvířata	zvíře	k1gNnPc1
chovaná	chovaný	k2eAgNnPc1d1
jako	jako	k8xC,k8xS
domácí	domácí	k2eAgMnPc1d1
mazlíčci	mazlíček	k1gMnPc1
a	a	k8xC
společníci	společník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimoto	mimoto	k6eAd1
se	se	k3xPyFc4
hospodářsky	hospodářsky	k6eAd1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
volně	volně	k6eAd1
žijící	žijící	k2eAgFnSc1d1
nebo	nebo	k8xC
polodivoká	polodivoký	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
nebyla	být	k5eNaImAgNnP
domestikována	domestikovat	k5eAaBmNgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářský	hospodářský	k2eAgInSc1d1
užitek	užitek	k1gInSc1
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
nejen	nejen	k6eAd1
ze	z	k7c2
savců	savec	k1gMnPc2
nebo	nebo	k8xC
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
obecně	obecně	k6eAd1
vlastně	vlastně	k9
ze	z	k7c2
všech	všecek	k3xTgInPc2
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Hospodářská	hospodářský	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
podle	podle	k7c2
druhu	druh	k1gInSc2
a	a	k8xC
užitku	užitek	k1gInSc2
</s>
<s>
Hospodářským	hospodářský	k2eAgInSc7d1
užitkem	užitek	k1gInSc7
ze	z	k7c2
zvířat	zvíře	k1gNnPc2
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
komodita	komodita	k1gFnSc1
<g/>
:	:	kIx,
mléko	mléko	k1gNnSc1
<g/>
,	,	kIx,
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
tuk	tuk	k1gInSc1
<g/>
,	,	kIx,
vejce	vejce	k1gNnSc1
či	či	k8xC
kožešina	kožešina	k1gFnSc1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
to	ten	k3xDgNnSc4
být	být	k5eAaImF
ale	ale	k8xC
i	i	k9
kosti	kost	k1gFnPc1
<g/>
,	,	kIx,
paznehty	pazneht	k1gInPc1
<g/>
,	,	kIx,
rohy	roh	k1gInPc1
nebo	nebo	k8xC
parohy	paroh	k1gInPc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
či	či	k8xC
chlupy	chlup	k1gInPc1
<g/>
,	,	kIx,
sladký	sladký	k2eAgInSc4d1
med	med	k1gInSc4
či	či	k8xC
měňavé	měňavý	k2eAgFnPc4d1
perly	perla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
pak	pak	k6eAd1
užitečná	užitečný	k2eAgFnSc1d1
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
tažná	tažný	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
či	či	k8xC
jezdecká	jezdecký	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
především	především	k9
koně	kůň	k1gMnPc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
skot	skot	k1gInSc1
nebo	nebo	k8xC
osli	osel	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
exotických	exotický	k2eAgFnPc6d1
jižních	jižní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
ale	ale	k8xC
také	také	k9
sloni	slon	k1gMnPc1
a	a	k8xC
velbloudi	velbloud	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
severských	severský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
pak	pak	k6eAd1
také	také	k9
sobi	sob	k1gMnPc1
a	a	k8xC
psi	pes	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Hospodářská	hospodářský	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
pro	pro	k7c4
komodity	komodita	k1gFnPc4
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1
hospodářskými	hospodářský	k2eAgFnPc7d1
zvířaty	zvíře	k1gNnPc7
jsou	být	k5eAaImIp3nP
v	v	k7c6
ČR	ČR	kA
pro	pro	k7c4
maso	maso	k1gNnSc4
<g/>
:	:	kIx,
skot	skot	k1gInSc1
<g/>
,	,	kIx,
prasata	prase	k1gNnPc4
<g/>
,	,	kIx,
drůbež	drůbež	k1gFnSc4
(	(	kIx(
<g/>
kuřata	kuře	k1gNnPc1
<g/>
,	,	kIx,
krůty	krůta	k1gFnPc1
<g/>
,	,	kIx,
kachny	kachna	k1gFnPc1
<g/>
,	,	kIx,
pižmovky	pižmovka	k1gFnPc1
a	a	k8xC
husy	husa	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
ovce	ovce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minoritní	minoritní	k2eAgInSc4d1
význam	význam	k1gInSc4
mají	mít	k5eAaImIp3nP
kozy	koza	k1gFnPc1
<g/>
,	,	kIx,
králíci	králík	k1gMnPc1
<g/>
,	,	kIx,
perličky	perlička	k1gFnPc1
<g/>
,	,	kIx,
holubi	holub	k1gMnPc1
a	a	k8xC
jelenovití	jelenovití	k1gMnPc1
<g/>
,	,	kIx,
zejména	zejména	k9
sika	sika	k1gMnSc1
<g/>
,	,	kIx,
daněk	daněk	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
a	a	k8xC
jelen	jelen	k1gMnSc1
<g/>
,	,	kIx,
chovaní	chovaný	k2eAgMnPc1d1
na	na	k7c6
farmách	farma	k1gFnPc6
pro	pro	k7c4
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
parohy	paroh	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
nepatrný	patrný	k2eNgInSc4d1,k2eAgInSc4d1
význam	význam	k1gInSc4
mají	mít	k5eAaImIp3nP
v	v	k7c6
české	český	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
masa	maso	k1gNnSc2
nutrie	nutrie	k1gFnSc2
<g/>
,	,	kIx,
koně	kůň	k1gMnPc1
a	a	k8xC
pštrosi	pštros	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
význam	význam	k1gInSc1
v	v	k7c6
produkci	produkce	k1gFnSc6
masa	maso	k1gNnSc2
má	mít	k5eAaImIp3nS
i	i	k9
chov	chov	k1gInSc1
ryb	ryba	k1gFnPc2
-	-	kIx~
rybníkářství	rybníkářství	k1gNnSc1
a	a	k8xC
akvakultura	akvakultura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
mají	mít	k5eAaImIp3nP
největší	veliký	k2eAgInSc4d3
význam	význam	k1gInSc4
kapr	kapr	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
pstruh	pstruh	k1gMnSc1
americký	americký	k2eAgMnSc1d1
duhový	duhový	k2eAgMnSc1d1
<g/>
,	,	kIx,
amur	amur	k1gMnSc1
bílý	bílý	k1gMnSc1
<g/>
,	,	kIx,
tolstolobik	tolstolobik	k1gMnSc1
bílý	bílý	k1gMnSc1
a	a	k8xC
tolstolobec	tolstolobec	k1gInSc1
pestrý	pestrý	k2eAgInSc1d1
<g/>
,	,	kIx,
menší	malý	k2eAgInSc1d2
význam	význam	k1gInSc1
mají	mít	k5eAaImIp3nP
síhové	síh	k1gMnPc1
<g/>
,	,	kIx,
siveni	siven	k1gMnPc1
<g/>
,	,	kIx,
jeseteři	jeseter	k1gMnPc1
a	a	k8xC
dravé	dravý	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgMnPc3d1
chovaným	chovaný	k2eAgMnPc3d1
druhům	druh	k1gInPc3
ryb	ryba	k1gFnPc2
patří	patřit	k5eAaImIp3nP
lososi	losos	k1gMnPc1
<g/>
,	,	kIx,
tilapie	tilapie	k1gFnPc1
nilská	nilský	k2eAgFnSc1d1
nebo	nebo	k8xC
pangas	pangas	k1gInSc1
spodnooký	spodnooký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vejce	vejce	k1gNnPc4
se	se	k3xPyFc4
v	v	k7c6
ČR	ČR	kA
chovají	chovat	k5eAaImIp3nP
takřka	takřka	k6eAd1
výhradně	výhradně	k6eAd1
slepice	slepice	k1gFnSc2
<g/>
,	,	kIx,
minoritní	minoritní	k2eAgInSc4d1
význam	význam	k1gInSc4
mají	mít	k5eAaImIp3nP
perličky	perlička	k1gFnPc1
a	a	k8xC
japonské	japonský	k2eAgFnPc1d1
křepelky	křepelka	k1gFnPc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
holubi	holub	k1gMnPc1
a	a	k8xC
pštrosi	pštros	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kachní	kachní	k2eAgNnSc1d1
vejce	vejce	k1gNnPc1
jsou	být	k5eAaImIp3nP
oblíbena	oblíben	k2eAgNnPc1d1
v	v	k7c6
Číně	Čína	k1gFnSc6
a	a	k8xC
Jihovýchodní	jihovýchodní	k2eAgFnSc3d1
Asii	Asie	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
obvyle	obvyl	k1gInSc5
nekonzumují	konzumovat	k5eNaBmIp3nP
<g/>
,	,	kIx,
neboť	neboť	k8xC
mohou	moct	k5eAaImIp3nP
představovat	představovat	k5eAaImF
zdravotní	zdravotní	k2eAgNnSc4d1
riziko	riziko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejce	vejce	k1gNnSc1
hus	husa	k1gFnPc2
a	a	k8xC
krůt	krůta	k1gFnPc2
se	se	k3xPyFc4
nekonzumují	konzumovat	k5eNaBmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
mléko	mléko	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
chován	chovat	k5eAaImNgInS
zejména	zejména	k9
skot	skot	k1gInSc1
<g/>
,	,	kIx,
méně	málo	k6eAd2
ovce	ovce	k1gFnSc2
a	a	k8xC
kozy	koza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinde	jinde	k6eAd1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
především	především	k6eAd1
v	v	k7c6
Asii	Asie	k1gFnSc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
mléko	mléko	k1gNnSc4
chován	chovat	k5eAaImNgMnS
také	také	k9
kůň	kůň	k1gMnSc1
domácí	domácí	k1gMnSc1
<g/>
,	,	kIx,
buvol	buvol	k1gMnSc1
domácí	domácí	k1gMnSc1
<g/>
,	,	kIx,
velbloud	velbloud	k1gMnSc1
jednohrbý	jednohrbý	k2eAgMnSc1d1
a	a	k8xC
dvouhrbý	dvouhrbý	k2eAgMnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
a	a	k8xC
sob	sob	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vlnu	vlna	k1gFnSc4
se	se	k3xPyFc4
v	v	k7c6
ČR	ČR	kA
chovají	chovat	k5eAaImIp3nP
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
ovce	ovce	k1gFnSc1
<g/>
,	,	kIx,
dílčí	dílčí	k2eAgInSc1d1
význam	význam	k1gInSc1
maí	maí	k?
králíci	králík	k1gMnPc1
a	a	k8xC
srstnatá	srstnatý	k2eAgNnPc1d1
plemena	plemeno	k1gNnPc1
koz	koza	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
Asii	Asie	k1gFnSc6
poskytují	poskytovat	k5eAaImIp3nP
vlnu	vlna	k1gFnSc4
také	také	k9
velbloudi	velbloud	k1gMnPc1
a	a	k8xC
jaci	jak	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
lama	lama	k1gFnSc1
alpaka	alpaka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuk	tuk	k1gInSc1
(	(	kIx(
<g/>
sádlo	sádlo	k1gNnSc1
nebo	nebo	k8xC
lůj	lůj	k1gInSc1
<g/>
)	)	kIx)
produkují	produkovat	k5eAaImIp3nP
prasata	prase	k1gNnPc4
<g/>
,	,	kIx,
ovce	ovce	k1gFnPc4
<g/>
,	,	kIx,
skot	skot	k1gInSc1
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
husa	husa	k1gFnSc1
<g/>
,	,	kIx,
kachna	kachna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
máslo	máslo	k1gNnSc1
<g/>
,	,	kIx,
získávané	získávaný	k2eAgNnSc1d1
z	z	k7c2
mléka	mléko	k1gNnSc2
a	a	k8xC
lanolin	lanolin	k1gInSc1
z	z	k7c2
ovčí	ovčí	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chov	chov	k1gInSc1
zvířat	zvíře	k1gNnPc2
pro	pro	k7c4
kožešinu	kožešina	k1gFnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
celosvětově	celosvětově	k6eAd1
na	na	k7c6
ústupu	ústup	k1gInSc6
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
se	se	k3xPyFc4
vyžívají	vyžívat	k5eAaImIp3nP
kožešiny	kožešina	k1gFnPc1
domácích	domácí	k2eAgFnPc2d1
ovcí	ovce	k1gFnPc2
<g/>
,	,	kIx,
králíků	králík	k1gMnPc2
a	a	k8xC
koz	koza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílčí	dílčí	k2eAgInSc4d1
význam	význam	k1gInSc4
mají	mít	k5eAaImIp3nP
též	též	k9
nutrie	nutrie	k1gFnPc1
<g/>
,	,	kIx,
lišky	liška	k1gFnPc1
obecné	obecný	k2eAgFnSc2d1
a	a	k8xC
polární	polární	k2eAgFnSc2d1
(	(	kIx(
<g/>
i	i	k9
jejich	jejich	k3xOp3gFnPc1
barevné	barevný	k2eAgFnPc1d1
variety	varieta	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
norci	norec	k1gMnPc1
američtí	americký	k2eAgMnPc1d1
<g/>
,	,	kIx,
činčily	činčila	k1gFnPc1
nebo	nebo	k8xC
mývalové	mýval	k1gMnPc1
<g/>
,	,	kIx,
<g/>
dosud	dosud	k6eAd1
jsou	být	k5eAaImIp3nP
také	také	k9
pro	pro	k7c4
kožešinu	kožešina	k1gFnSc4
využívání	využívání	k1gNnSc4
domácí	domácí	k2eAgMnPc1d1
psi	pes	k1gMnPc1
a	a	k8xC
kočky	kočka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
produkci	produkce	k1gFnSc3
kůže	kůže	k1gFnSc2
slouží	sloužit	k5eAaImIp3nP
hlavně	hlavně	k6eAd1
běžná	běžný	k2eAgNnPc1d1
hospodářská	hospodářský	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
-	-	kIx~
skot	skot	k1gInSc1
<g/>
,	,	kIx,
prasata	prase	k1gNnPc1
<g/>
,	,	kIx,
koně	kůň	k1gMnPc4
<g/>
,	,	kIx,
ovce	ovce	k1gFnPc4
<g/>
,	,	kIx,
kozy	koza	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
krokodýli	krokodýl	k1gMnPc1
a	a	k8xC
aligátoři	aligátor	k1gMnPc1
<g/>
,	,	kIx,
chovaní	chovaný	k2eAgMnPc1d1
na	na	k7c6
specializovaných	specializovaný	k2eAgFnPc6d1
farmách	farma	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
kůži	kůže	k1gFnSc4
<g/>
,	,	kIx,
maso	maso	k1gNnSc4
a	a	k8xC
pro	pro	k7c4
farmaceutické	farmaceutický	k2eAgInPc4d1
účely	účel	k1gInPc4
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
hlavně	hlavně	k9
v	v	k7c6
Asii	Asie	k1gFnSc6
i	i	k9
někteří	některý	k3yIgMnPc1
hadi	had	k1gMnPc1
a	a	k8xC
ještěři	ještěr	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
mechanizací	mechanizace	k1gFnSc7
v	v	k7c6
dopravě	doprava	k1gFnSc6
<g/>
,	,	kIx,
zemědělství	zemědělství	k1gNnSc6
a	a	k8xC
lesnictví	lesnictví	k1gNnSc6
klesá	klesat	k5eAaImIp3nS
význam	význam	k1gInSc1
zvířat	zvíře	k1gNnPc2
jako	jako	k8xC,k8xS
pracovní	pracovní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
včetně	včetně	k7c2
ČR	ČR	kA
sloužil	sloužit	k5eAaImAgInS
jako	jako	k9
pracovní	pracovní	k2eAgNnSc4d1
a	a	k8xC
dopravní	dopravní	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
zejména	zejména	k9
kůň	kůň	k1gMnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
vůl	vůl	k1gMnSc1
a	a	k8xC
osel	osel	k1gMnSc1
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
pes	pes	k1gMnSc1
(	(	kIx(
<g/>
u	u	k7c2
řezníků	řezník	k1gMnPc2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Asie	Asie	k1gFnSc2
mají	mít	k5eAaImIp3nP
dosud	dosud	k6eAd1
význam	význam	k1gInSc4
tažní	tažní	k2eAgInSc4d1
<g/>
,	,	kIx,
vzácněji	vzácně	k6eAd2
jezdečtí	jezdecký	k2eAgMnPc1d1
sobi	sob	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
Americe	Amerika	k1gFnSc6
také	také	k6eAd1
saňoví	saňový	k2eAgMnPc1d1
psi	pes	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
nošení	nošení	k1gNnSc3
nákladů	náklad	k1gInPc2
lama	lama	k1gFnSc1
krotká	krotký	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
horách	hora	k1gFnPc6
centrální	centrální	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
jak	jak	k6eAd1
a	a	k8xC
výjimečně	výjimečně	k6eAd1
i	i	k9
berani	beran	k1gMnPc1
některých	některý	k3yIgNnPc2
plemen	plemeno	k1gNnPc2
ovcí	ovce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
aridních	aridní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Asie	Asie	k1gFnSc2
a	a	k8xC
Afriky	Afrika	k1gFnSc2
oba	dva	k4xCgInPc4
druhy	druh	k1gInPc4
velbloudů	velbloud	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velbloudi	velbloud	k1gMnPc1
(	(	kIx(
<g/>
hlavně	hlavně	k6eAd1
jednohrbí	jednohrbý	k2eAgMnPc1d1
<g/>
)	)	kIx)
se	se	k3xPyFc4
používjí	používjet	k5eAaImIp3nS,k5eAaPmIp3nS
rovněž	rovněž	k9
k	k	k7c3
jízdě	jízda	k1gFnSc3
a	a	k8xC
výjimečně	výjimečně	k6eAd1
i	i	k9
k	k	k7c3
zápřahu	zápřah	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Indii	Indie	k1gFnSc6
a	a	k8xC
Jihovýchodní	jihovýchodní	k2eAgFnSc3d1
Asii	Asie	k1gFnSc3
je	být	k5eAaImIp3nS
především	především	k9
v	v	k7c6
lesnictví	lesnictví	k1gNnSc6
při	při	k7c6
těžbě	těžba	k1gFnSc6
dřeva	dřevo	k1gNnSc2
používáni	používán	k2eAgMnPc1d1
ochočení	ochočený	k2eAgMnPc1d1
sloni	slon	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
slonů	slon	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
klasickou	klasický	k2eAgFnSc4d1
odmestkaci	odmestkace	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c6
ochočování	ochočování	k1gNnSc6
odchycených	odchycený	k2eAgNnPc2d1
volně	volně	k6eAd1
žijících	žijící	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
laboratorní	laboratorní	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
slouží	sloužit	k5eAaImIp3nP
hlavně	hlavně	k9
žáby	žába	k1gFnPc1
drápatky	drápatka	k1gFnPc1
<g/>
,	,	kIx,
axolotli	axolotl	k1gMnPc1
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
ryby	ryba	k1gFnPc1
(	(	kIx(
<g/>
danio	danio	k6eAd1
pruhované	pruhovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
gupka	gupka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
morčata	morče	k1gNnPc1
<g/>
,	,	kIx,
králíci	králík	k1gMnPc1
<g/>
,	,	kIx,
myši	myš	k1gFnPc1
<g/>
,	,	kIx,
potkani	potkan	k1gMnPc1
<g/>
,	,	kIx,
méně	málo	k6eAd2
křečci	křeček	k1gMnPc1
<g/>
,	,	kIx,
domácí	domácí	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
<g/>
,	,	kIx,
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
prasata	prase	k1gNnPc1
a	a	k8xC
primáti	primát	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současným	současný	k2eAgInSc7d1
trendem	trend	k1gInSc7
však	však	k9
je	být	k5eAaImIp3nS
využití	využití	k1gNnSc4
zvířat	zvíře	k1gNnPc2
při	při	k7c6
biologickém	biologický	k2eAgNnSc6d1
<g/>
,	,	kIx,
medicínském	medicínský	k2eAgNnSc6d1
<g/>
,	,	kIx,
farmaceutickém	farmaceutický	k2eAgNnSc6d1
či	či	k8xC
kosmetikcém	kosmetikcé	k1gNnSc6
výzkumu	výzkum	k1gInSc2
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
omezit	omezit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
hospodářskými	hospodářský	k2eAgNnPc7d1
zvířaty	zvíře	k1gNnPc7
najdeme	najít	k5eAaPmIp1nP
i	i	k9
bezobratlé	bezobratlý	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hmyzu	hmyz	k1gInSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
především	především	k9
včela	včela	k1gFnSc1
medonosná	medonosný	k2eAgFnSc1d1
<g/>
,	,	kIx,
chovaná	chovaný	k2eAgFnSc1d1
jak	jak	k8xS,k8xC
pro	pro	k7c4
med	med	k1gInSc4
jako	jako	k8xC,k8xS
potravinu	potravina	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
pro	pro	k7c4
vosk	vosk	k1gInSc4
a	a	k8xC
další	další	k2eAgInPc4d1
včelí	včelí	k2eAgInPc4d1
produkty	produkt	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
bourec	bourec	k1gMnSc1
morušový	morušový	k2eAgMnSc1d1
<g/>
,	,	kIx,
chovaný	chovaný	k2eAgMnSc1d1
pro	pro	k7c4
hedvábné	hedvábný	k2eAgNnSc4d1
vlákno	vlákno	k1gNnSc4
<g/>
,	,	kIx,
méně	málo	k6eAd2
jako	jako	k9
potravina	potravina	k1gFnSc1
(	(	kIx(
<g/>
larvy	larva	k1gFnPc1
a	a	k8xC
kukly	kukla	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Asii	Asie	k1gFnSc6
oblíbenou	oblíbený	k2eAgFnSc7d1
delikatesou	delikatesa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
se	se	k3xPyFc4
začínají	začínat	k5eAaImIp3nP
využívat	využívat	k5eAaPmF,k5eAaImF
také	také	k9
roztoči	roztoč	k1gMnPc1
<g/>
,	,	kIx,
především	především	k9
ti	ten	k3xDgMnPc1
draví	dravý	k2eAgMnPc1d1
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
jako	jako	k8xS,k8xC
ochrana	ochrana	k1gFnSc1
před	před	k7c7
škůdci	škůdce	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roztoči	roztoč	k1gMnSc3
se	se	k3xPyFc4
však	však	k9
používají	používat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
potravinářství	potravinářství	k1gNnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
třeba	třeba	k6eAd1
při	při	k7c6
výrobě	výroba	k1gFnSc6
a	a	k8xC
zrání	zrání	k1gNnSc4
některých	některý	k3yIgInPc2
sýrů	sýr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
mořských	mořský	k2eAgMnPc2d1
či	či	k8xC
sladkovodních	sladkovodní	k2eAgMnPc2d1
mlžů	mlž	k1gMnPc2
se	se	k3xPyFc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
perlorodky	perlorodka	k1gFnPc1
pro	pro	k7c4
perly	perla	k1gFnPc4
a	a	k8xC
ústřice	ústřice	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
slávky	slávka	k1gFnPc4
<g/>
)	)	kIx)
pro	pro	k7c4
jídlo	jídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
také	také	k9
suchozemští	suchozemský	k2eAgMnPc1d1
plži	plž	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
hlemýždi	hlemýžď	k1gMnPc1
nebo	nebo	k8xC
oblovky	oblovka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Průmyslově	průmyslově	k6eAd1
se	se	k3xPyFc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
korýši	korýš	k1gMnPc1
<g/>
:	:	kIx,
raci	rak	k1gMnPc1
<g/>
,	,	kIx,
langusty	langusta	k1gFnPc1
<g/>
,	,	kIx,
krabi	krab	k1gMnPc1
<g/>
,	,	kIx,
humr	humr	k1gMnSc1
i	i	k8xC
garnáti	garnát	k1gMnPc1
a	a	k8xC
krevety	kreveta	k1gFnPc1
<g/>
,	,	kIx,
hlavně	hlavně	k9
jako	jako	k9
potrava	potrava	k1gFnSc1
<g/>
,	,	kIx,
nově	nově	k6eAd1
i	i	k9
jako	jako	k9
zdroj	zdroj	k1gInSc4
chitinu	chitin	k1gInSc2
jako	jako	k8xS,k8xC
materiálu	materiál	k1gInSc2
pro	pro	k7c4
technologické	technologický	k2eAgNnSc4d1
použití	použití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Zejména	zejména	k9
v	v	k7c6
Asii	Asie	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
hospodářské	hospodářský	k2eAgInPc4d1
účely	účel	k1gInPc4
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
i	i	k9
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
hadi	had	k1gMnPc1
<g/>
,	,	kIx,
želvy	želva	k1gFnPc1
<g/>
,	,	kIx,
krokodýli	krokodýl	k1gMnPc1
i	i	k8xC
aligátoři	aligátor	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
především	především	k6eAd1
pro	pro	k7c4
kůže	kůže	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
obojživelníci	obojživelník	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
skokan	skokan	k1gMnSc1
volský	volský	k2eAgMnSc1d1
<g/>
,	,	kIx,
především	především	k9
pro	pro	k7c4
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Hospodářská	hospodářský	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
pro	pro	k7c4
práci	práce	k1gFnSc4
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1
případem	případ	k1gInSc7
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
jistě	jistě	k9
pes	pes	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
jak	jak	k8xC,k8xS
hospodářským	hospodářský	k2eAgNnSc7d1
zvířetem	zvíře	k1gNnSc7
pro	pro	k7c4
užitek	užitek	k1gInSc4
z	z	k7c2
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
pastevečtí	pastevecký	k2eAgMnPc1d1
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
hlídací	hlídací	k2eAgMnPc1d1
a	a	k8xC
bojoví	bojový	k2eAgMnPc1d1
<g/>
,	,	kIx,
záchranářští	záchranářský	k2eAgMnPc1d1
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
slepečtí	slepecký	k2eAgMnPc1d1
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
tažní	tažní	k2eAgMnPc1d1
psi	pes	k1gMnPc1
do	do	k7c2
spřežení	spřežení	k1gNnSc2
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
komoditu	komodita	k1gFnSc4
(	(	kIx(
<g/>
krev	krev	k1gFnSc4
pro	pro	k7c4
lékařské	lékařský	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
<g/>
,	,	kIx,
dokonce	dokonce	k9
na	na	k7c4
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
i	i	k9
vlna	vlna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
i	i	k9
domácím	domácí	k2eAgMnSc7d1
miláčkem	miláček	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unikátní	unikátní	k2eAgInSc1d1
užitek	užitek	k1gInSc1
byl	být	k5eAaImAgInS
z	z	k7c2
bezsrsté	bezsrstý	k2eAgFnSc2d1
rasy	rasa	k1gFnSc2
určené	určený	k2eAgFnPc1d1
pro	pro	k7c4
zahřívání	zahřívání	k1gNnSc4
postele	postel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Domestikace	domestikace	k1gFnSc1
</s>
<s>
Termín	termín	k1gInSc4
hospodářské	hospodářský	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
termínem	termín	k1gInSc7
domestikace	domestikace	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
většina	většina	k1gFnSc1
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
jsou	být	k5eAaImIp3nP
zvířata	zvíře	k1gNnPc1
domestikovaná	domestikovaný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
se	se	k3xPyFc4
ale	ale	k9
setkat	setkat	k5eAaPmF
i	i	k9
se	s	k7c7
zvířaty	zvíře	k1gNnPc7
divokými	divoký	k2eAgNnPc7d1
i	i	k9
polodomestikovanými	polodomestikovaný	k2eAgFnPc7d1
(	(	kIx(
<g/>
antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
bez	bez	k7c2
jakéhokoli	jakýkoli	k3yIgInSc2
vztahu	vztah	k1gInSc2
k	k	k7c3
člověku	člověk	k1gMnSc3
(	(	kIx(
<g/>
korýši	korýš	k1gMnPc1
<g/>
,	,	kIx,
plazi	plaz	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Domácí	domácí	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
</s>
<s>
Plemena	plemeno	k1gNnPc1
zvířat	zvíře	k1gNnPc2
</s>
<s>
Označování	označování	k1gNnSc1
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
</s>
<s>
Živočišný	živočišný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4023819-2	4023819-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13375	#num#	k4
</s>
