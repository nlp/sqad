<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Beskydy	Beskydy	k1gFnPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
BeskydyIUCN	BeskydyIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
Beskydy	Beskyd	k1gInPc1
od	od	k7c2
Kunčic	Kunčice	k1gFnPc2
pod	pod	k7c7
OndřejníkemZákladní	OndřejníkemZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1973	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
350	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1160	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Beskydy	Beskyd	k1gInPc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInPc4
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgMnSc1d1
a	a	k8xC
Zlínský	zlínský	k2eAgMnSc1d1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
51,93	51,93	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
14,51	14,51	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Beskydy	Beskydy	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
82	#num#	k4
Web	web	k1gInSc1
</s>
<s>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
hyperlink	hyperlink	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
/	/	kIx~
<g/>
Beskydy	Beskydy	k1gFnPc1
<g/>
/	/	kIx~
<g/>
index	index	k1gInSc1
<g/>
.	.	kIx.
<g/>
htm	htm	k?
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Beskydy	Beskydy	k1gFnPc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
CHKO	CHKO	kA
Beskydy	Beskydy	k1gFnPc4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1973	#num#	k4
<g/>
,	,	kIx,
vládním	vládní	k2eAgInSc7d1
výnosem	výnos	k1gInSc7
MK	MK	kA
ČSR	ČSR	kA
č.	č.	k?
<g/>
j.	j.	k?
5373	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
chráněnou	chráněný	k2eAgFnSc4d1
krajinnou	krajinný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozloha	rozloha	k1gFnSc1
1160	#num#	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Les	les	k1gInSc1
pokrývá	pokrývat	k5eAaImIp3nS
71	#num#	k4
%	%	kIx~
území	území	k1gNnSc6
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
350	#num#	k4
(	(	kIx(
<g/>
Zubří	zubří	k2eAgFnSc2d1
<g/>
)	)	kIx)
–	–	k?
1324	#num#	k4
(	(	kIx(
<g/>
Lysá	Lysá	k1gFnSc1
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
m	m	kA
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
60	#num#	k4
maloplošných	maloplošný	k2eAgFnPc2d1
zvlášť	zvlášť	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
MCHÚ	MCHÚ	kA
<g/>
)	)	kIx)
</s>
<s>
Důvody	důvod	k1gInPc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Důvodem	důvod	k1gInSc7
vyhlášení	vyhlášení	k1gNnSc2
byly	být	k5eAaImAgInP
její	její	k3xOp3gFnPc4
výjimečné	výjimečný	k2eAgFnPc4d1
přírodní	přírodní	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
<g/>
,	,	kIx,
zejména	zejména	k9
původní	původní	k2eAgInPc4d1
pralesovité	pralesovitý	k2eAgInPc4d1
lesní	lesní	k2eAgInPc4d1
porosty	porost	k1gInPc4
s	s	k7c7
výskytem	výskyt	k1gInSc7
vzácných	vzácný	k2eAgInPc2d1
karpatských	karpatský	k2eAgInPc2d1
živočišných	živočišný	k2eAgInPc2d1
i	i	k8xC
rostlinných	rostlinný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
druhově	druhově	k6eAd1
pestrá	pestrý	k2eAgNnPc1d1
luční	luční	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
<g/>
,	,	kIx,
unikátní	unikátní	k2eAgFnPc1d1
povrchové	povrchový	k2eAgFnPc1d1
i	i	k8xC
podzemní	podzemní	k2eAgInPc1d1
pseudokrasové	pseudokrasový	k2eAgInPc1d1
jevy	jev	k1gInPc1
a	a	k8xC
také	také	k9
mimořádná	mimořádný	k2eAgFnSc1d1
estetická	estetický	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
a	a	k8xC
pestrost	pestrost	k1gFnSc1
ojedinělého	ojedinělý	k2eAgInSc2d1
typu	typ	k1gInSc2
krajiny	krajina	k1gFnSc2
vzniklého	vzniklý	k2eAgNnSc2d1
historickým	historický	k2eAgNnSc7d1
soužitím	soužití	k1gNnSc7
člověka	člověk	k1gMnSc2
s	s	k7c7
tímto	tento	k3xDgNnSc7
územím	území	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
přírodní	přírodní	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Beskydy	Beskyd	k1gInPc1
je	být	k5eAaImIp3nS
podtržen	podtržen	k2eAgInSc1d1
vyhlášením	vyhlášení	k1gNnSc7
50	#num#	k4
maloplošných	maloplošný	k2eAgNnPc2d1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
územním	územní	k2eAgNnSc7d1
překrytím	překrytí	k1gNnSc7
CHKO	CHKO	kA
s	s	k7c7
mezinárodně	mezinárodně	k6eAd1
významným	významný	k2eAgNnSc7d1
ptačím	ptačí	k2eAgNnSc7d1
územím	území	k1gNnSc7
a	a	k8xC
s	s	k7c7
chráněnou	chráněný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
přirozené	přirozený	k2eAgFnSc2d1
akumulace	akumulace	k1gFnSc2
vod	voda	k1gFnPc2
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
i	i	k8xC
jejím	její	k3xOp3gMnSc7
nadregionálním	nadregionální	k2eAgInSc7d1
rekreačním	rekreační	k2eAgInSc7d1
významem	význam	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
jsou	být	k5eAaImIp3nP
chráněna	chráněn	k2eAgFnSc1d1
původní	původní	k2eAgFnSc1d1
luční	luční	k2eAgFnSc1d1
a	a	k8xC
lesní	lesní	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celá	k1gFnSc2
území	území	k1gNnSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
orografických	orografický	k2eAgInPc2d1
celků	celek	k1gInPc2
<g/>
:	:	kIx,
Moravskoslezských	moravskoslezský	k2eAgInPc2d1
Beskyd	Beskydy	k1gInPc2
<g/>
,	,	kIx,
Rožnovské	Rožnovské	k2eAgFnSc2d1
brázdy	brázda	k1gFnSc2
<g/>
,	,	kIx,
Vsetínských	vsetínský	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
a	a	k8xC
Javorníků	Javorník	k1gInPc2
(	(	kIx(
<g/>
české	český	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
a	a	k8xC
Zlínského	zlínský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Územím	území	k1gNnSc7
CHKO	CHKO	kA
protékají	protékat	k5eAaImIp3nP
řeky	řeka	k1gFnSc2
Vsetínská	vsetínský	k2eAgFnSc1d1
Bečva	Bečva	k1gFnSc1
<g/>
,	,	kIx,
Rožnovská	rožnovský	k2eAgFnSc1d1
Bečva	Bečva	k1gFnSc1
<g/>
,	,	kIx,
Ostravice	Ostravice	k1gFnSc1
a	a	k8xC
Morávka	Morávek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
<g/>
:	:	kIx,
Radegast	Radegast	k1gMnSc1
<g/>
,	,	kIx,
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
Lysá	Lysá	k1gFnSc1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Stezka	stezka	k1gFnSc1
pokladů	poklad	k1gInPc2
Godula	Godula	k1gFnSc1
a	a	k8xC
Hradisko	hradisko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
šelem	šelma	k1gFnPc2
</s>
<s>
Rys	Rys	k1gMnSc1
ostrovid	ostrovid	k1gMnSc1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
lynx	lynx	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
mapován	mapován	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
velkých	velký	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výsledků	výsledek	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
v	v	k7c6
oblasti	oblast	k1gFnSc6
žije	žít	k5eAaImIp3nS
minimálně	minimálně	k6eAd1
16	#num#	k4
rysů	rys	k1gInPc2
<g/>
,	,	kIx,
8	#num#	k4
vlků	vlk	k1gMnPc2
a	a	k8xC
tři	tři	k4xCgMnPc1
medvědi	medvěd	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Kněhyně	kněhyně	k1gFnSc1
-	-	kIx~
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
</s>
<s>
NPR	NPR	kA
Mazák	mazák	k1gMnSc1
</s>
<s>
NPR	NPR	kA
Mionší	Mionší	k1gNnSc1
</s>
<s>
NPR	NPR	kA
Pulčín	Pulčín	k1gInSc1
-	-	kIx~
Hradisko	hradisko	k1gNnSc1
</s>
<s>
NPR	NPR	kA
Radhošť	Radhošť	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Razula	Razula	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Salajka	salajka	k1gFnSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
PR	pr	k0
Bučací	Bučací	k2eAgFnSc1d1
potok	potok	k1gInSc4
</s>
<s>
PR	pr	k0
Čerňavina	Čerňavin	k2eAgFnSc1d1
</s>
<s>
PR	pr	k0
Draplavý	draplavý	k2eAgInSc1d1
</s>
<s>
PR	pr	k0
Galovské	Galovské	k2eAgFnSc1d1
lúky	lúk	k2eAgMnPc4d1
</s>
<s>
PR	pr	k0
Huštýn	Huštýn	k1gInSc1
</s>
<s>
PR	pr	k0
Klíny	klín	k1gInPc1
</s>
<s>
PR	pr	k0
Kutaný	kutaný	k2eAgInSc1d1
</s>
<s>
PR	pr	k0
Malenovický	Malenovický	k2eAgMnSc1d1
kotel	kotel	k1gInSc4
</s>
<s>
PR	pr	k0
Malý	malý	k2eAgInSc1d1
Smrk	smrk	k1gInSc4
</s>
<s>
PR	pr	k0
Mazácký	mazácký	k2eAgMnSc5d1
Grúnik	Grúnik	k1gMnSc1
</s>
<s>
PR	pr	k0
Noříčí	Noříčí	k2eAgFnSc1d1
</s>
<s>
PR	pr	k0
Poledňana	Poledňan	k1gMnSc4
</s>
<s>
PR	pr	k0
Smrk	smrk	k1gInSc4
</s>
<s>
PR	pr	k0
Studenčany	Studenčan	k1gMnPc4
</s>
<s>
PR	pr	k0
Travný	travný	k2eAgInSc4d1
</s>
<s>
PR	pr	k0
Travný	travný	k2eAgInSc5d1
potok	potok	k1gInSc4
</s>
<s>
PR	pr	k0
Trojačka	Trojačka	k1gFnSc1
</s>
<s>
PR	pr	k0
V	v	k7c6
Podolánkách	Podolánka	k1gFnPc6
</s>
<s>
PR	pr	k0
Velký	velký	k2eAgInSc1d1
Polom	polom	k1gInSc1
</s>
<s>
PR	pr	k0
Zimný	zimný	k2eAgInSc5d1
potok	potok	k1gInSc4
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
PP	PP	kA
Brodská	Brodská	k1gFnSc1
</s>
<s>
PP	PP	kA
Byčinec	Byčinec	k1gMnSc1
</s>
<s>
PP	PP	kA
Filůvka	Filůvka	k1gFnSc1
</s>
<s>
PP	PP	kA
Kladnatá	Kladnatý	k2eAgFnSc1d1
-	-	kIx~
Grapy	Grapy	k?
</s>
<s>
PP	PP	kA
Kněhyňská	Kněhyňský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
PP	PP	kA
Kudlačena	Kudlačen	k2eAgFnSc1d1
</s>
<s>
PP	PP	kA
Kyčmol	Kyčmol	k1gInSc1
</s>
<s>
PP	PP	kA
Lišková	Lišková	k1gFnSc1
</s>
<s>
PP	PP	kA
Motyčanka	Motyčanka	k1gFnSc1
</s>
<s>
PP	PP	kA
Obidová	Obidová	k1gFnSc1
</s>
<s>
PP	PP	kA
Ondrášovy	Ondrášův	k2eAgFnSc2d1
díry	díra	k1gFnSc2
</s>
<s>
PP	PP	kA
Pod	pod	k7c7
Juráškou	Juráška	k1gFnSc7
</s>
<s>
PP	PP	kA
Pod	pod	k7c7
Lukšincem	Lukšinec	k1gInSc7
</s>
<s>
PP	PP	kA
Podgrúň	Podgrúň	k1gFnSc1
</s>
<s>
PP	PP	kA
Poskla	Poskla	k1gFnSc1
</s>
<s>
PP	PP	kA
Rákosina	rákosina	k1gFnSc1
ve	v	k7c6
Stříteži	Střítež	k1gFnSc6
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
</s>
<s>
PP	PP	kA
Skálí	skálí	k1gNnSc1
</s>
<s>
PP	PP	kA
Smradlavá	smradlavý	k2eAgFnSc1d1
</s>
<s>
PP	PP	kA
Stříbrník	stříbrník	k1gMnSc1
</s>
<s>
PP	PP	kA
Uherská	uherský	k2eAgFnSc1d1
</s>
<s>
PP	PP	kA
Vachalka	Vachalka	k1gFnSc1
</s>
<s>
PP	PP	kA
Velký	velký	k2eAgInSc4d1
kámen	kámen	k1gInSc4
</s>
<s>
PP	PP	kA
Vodopády	vodopád	k1gInPc4
Satiny	Satin	k2eAgInPc4d1
</s>
<s>
PP	PP	kA
Zubří	zubří	k2eAgInSc4d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Beskydy	Beskyd	k1gInPc4
-	-	kIx~
základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
↑	↑	k?
Ekologové	ekolog	k1gMnPc1
začali	začít	k5eAaPmAgMnP
v	v	k7c6
Beskydech	Beskyd	k1gInPc6
mapovat	mapovat	k5eAaImF
výskyt	výskyt	k1gInSc4
velkých	velký	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2010-2-25	2010-2-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Beskydy	Beskyd	k1gInPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Beskydy	Beskyd	k1gInPc4
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
CHKO	CHKO	kA
Beskydy	Beskydy	k1gFnPc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Beskydy	Beskydy	k1gFnPc1
</s>
<s>
Turistické	turistický	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c4
CHKO	CHKO	kA
Beskydy	Beskydy	k1gFnPc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kněhyně	kněhyně	k1gFnSc1
–	–	k?
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
•	•	k?
Mazák	mazák	k1gInSc1
•	•	k?
Mionší	Mionší	k2eAgInSc1d1
•	•	k?
Pulčín	Pulčína	k1gFnPc2
–	–	k?
Hradisko	hradisko	k1gNnSc4
•	•	k?
Radhošť	Radhošť	k1gInSc1
•	•	k?
Razula	Razula	k1gFnSc1
•	•	k?
Salajka	salajka	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Bučací	Bučací	k2eAgInSc1d1
potrok	potrok	k1gInSc1
•	•	k?
Čerňavina	Čerňavina	k1gFnSc1
•	•	k?
Draplavý	draplavý	k2eAgInSc1d1
•	•	k?
Galovské	Galovský	k2eAgFnSc2d1
lúky	lúka	k1gFnSc2
•	•	k?
Gutské	Gutský	k2eAgNnSc1d1
peklo	peklo	k1gNnSc1
•	•	k?
Huštýn	Huštýn	k1gMnSc1
•	•	k?
Klíny	klín	k1gInPc1
•	•	k?
Kutaný	kutaný	k2eAgInSc1d1
•	•	k?
Losový	losový	k2eAgInSc1d1
•	•	k?
Lysá	Lysá	k1gFnSc1
hora	hora	k1gFnSc1
•	•	k?
Makyta	makyta	k1gFnSc1
•	•	k?
Malenovický	Malenovický	k2eAgInSc1d1
kotel	kotel	k1gInSc1
•	•	k?
Malý	malý	k2eAgInSc1d1
Javorník	Javorník	k1gInSc1
•	•	k?
Malý	malý	k2eAgInSc1d1
Smrk	smrk	k1gInSc1
•	•	k?
Mazácký	mazácký	k2eAgInSc1d1
Grúnik	Grúnik	k1gInSc1
•	•	k?
Noříčí	Noříčí	k1gNnSc2
•	•	k?
Poledňana	Poledňan	k1gMnSc2
•	•	k?
Ropice	Ropic	k1gMnSc2
•	•	k?
Smrk	smrk	k1gInSc1
•	•	k?
Studenčany	Studenčan	k1gMnPc4
•	•	k?
Travný	travný	k2eAgInSc1d1
•	•	k?
Travný	travný	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Trojačka	Trojačka	k1gFnSc1
•	•	k?
Uplaz	Uplaz	k1gInSc1
•	•	k?
V	v	k7c6
Podolánkách	Podolánka	k1gFnPc6
•	•	k?
Velký	velký	k2eAgInSc1d1
Polom	polom	k1gInSc1
•	•	k?
Zimný	zimný	k2eAgInSc1d1
potok	potok	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Brodská	Brodská	k1gFnSc1
•	•	k?
Byčinec	Byčinec	k1gInSc1
•	•	k?
Kladnatá	Kladnatý	k2eAgFnSc1d1
–	–	k?
Grapy	Grapy	k?
•	•	k?
Kněhyňská	Kněhyňský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
•	•	k?
Kudlačena	Kudlačen	k2eAgFnSc1d1
•	•	k?
Kyčmol	Kyčmol	k1gInSc1
•	•	k?
Lišková	Lišková	k1gFnSc1
•	•	k?
Motyčanka	Motyčanka	k1gFnSc1
•	•	k?
Obidová	Obidová	k1gFnSc1
•	•	k?
Ondrášovy	Ondrášův	k2eAgFnSc2d1
díry	díra	k1gFnSc2
•	•	k?
Pod	pod	k7c7
Juráškou	Juráška	k1gFnSc7
•	•	k?
Pod	pod	k7c7
Lukšincem	Lukšinec	k1gInSc7
•	•	k?
Podgruň	Podgruň	k1gFnSc1
•	•	k?
Poskla	Poskla	k1gFnSc1
•	•	k?
Rákosina	rákosina	k1gFnSc1
Střítež	Střítež	k1gFnSc1
•	•	k?
Skálí	skálí	k1gNnSc2
•	•	k?
Smradlavá	smradlavý	k2eAgFnSc1d1
•	•	k?
Stříbrník	stříbrník	k1gInSc1
•	•	k?
Uherská	uherský	k2eAgFnSc1d1
•	•	k?
Vachalka	Vachalka	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc4d1
kámen	kámen	k1gInSc4
•	•	k?
Vodopády	vodopád	k1gInPc4
Satiny	Satina	k1gMnSc2
•	•	k?
Vysutý	vysutý	k2eAgInSc4d1
•	•	k?
Zubří	zubří	k2eAgNnSc1d1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Frýdek-Místek	Frýdek-Místka	k1gFnPc2
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Beskydy	Beskyd	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Čantoria	Čantorium	k1gNnSc2
•	•	k?
Kněhyně	kněhyně	k1gFnSc2
–	–	k?
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
•	•	k?
Mazák	mazák	k1gInSc1
•	•	k?
Mionší	Mionší	k2eAgInSc1d1
•	•	k?
Salajka	salajka	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Skalická	Skalická	k1gFnSc1
Morávka	Morávek	k1gMnSc2
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Bučací	Bučací	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Bukovec	Bukovec	k1gInSc1
•	•	k?
Čerňavina	Čerňavina	k1gFnSc1
•	•	k?
Draplavý	draplavý	k2eAgInSc4d1
•	•	k?
Gutské	Gutský	k2eAgNnSc1d1
peklo	peklo	k1gNnSc1
•	•	k?
Klíny	klín	k1gInPc1
•	•	k?
Kršle	Kršle	k1gInSc1
•	•	k?
Les	les	k1gInSc4
Na	na	k7c4
Rozdílné	rozdílný	k2eAgNnSc4d1
•	•	k?
Lysá	lysat	k5eAaImIp3nS
hora	hora	k1gFnSc1
•	•	k?
Malenovický	Malenovický	k2eAgInSc1d1
kotel	kotel	k1gInSc1
•	•	k?
Malý	malý	k2eAgInSc1d1
Smrk	smrk	k1gInSc1
•	•	k?
Mazácký	mazácký	k2eAgInSc1d1
Grúnik	Grúnik	k1gInSc1
•	•	k?
Novodvorský	novodvorský	k2eAgInSc1d1
močál	močál	k1gInSc1
•	•	k?
Palkovické	Palkovický	k2eAgFnPc4d1
hůrky	hůrka	k1gFnPc4
•	•	k?
Plenisko	Plenisko	k1gNnSc1
•	•	k?
Poledňana	Poledňan	k1gMnSc2
•	•	k?
Ropice	Ropic	k1gMnSc2
•	•	k?
Rybníky	rybník	k1gInPc4
•	•	k?
Skalka	skalka	k1gFnSc1
•	•	k?
Smrk	smrk	k1gInSc1
•	•	k?
Studenčany	Studenčan	k1gMnPc4
•	•	k?
Travný	travný	k2eAgInSc1d1
•	•	k?
Travný	travný	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Uplaz	Uplaz	k1gInSc1
•	•	k?
V	v	k7c6
Podolánkách	Podolánka	k1gFnPc6
•	•	k?
Velké	velký	k2eAgInPc1d1
doly	dol	k1gInPc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Polom	polom	k1gInSc1
•	•	k?
Vřesová	vřesový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Zimný	zimný	k2eAgInSc4d1
potok	potok	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Byčinec	Byčinec	k1gInSc1
•	•	k?
Filipka	Filipka	k1gFnSc1
•	•	k?
Filůvka	Filůvka	k1gFnSc1
•	•	k?
Hradní	hradní	k2eAgInSc1d1
vrch	vrch	k1gInSc1
Hukvaldy	Hukvaldy	k1gInPc1
•	•	k?
Hukvaldy	Hukvaldy	k1gInPc1
•	•	k?
Kamenec	Kamenec	k1gInSc1
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
•	•	k?
Kněhyňská	Kněhyňský	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
•	•	k?
Koryto	koryto	k1gNnSc1
řeky	řeka	k1gFnSc2
Ostravice	Ostravice	k1gFnSc2
•	•	k?
Kyčmol	Kyčmol	k1gInSc1
•	•	k?
Lišková	Lišková	k1gFnSc1
•	•	k?
Motyčanka	Motyčanka	k1gFnSc1
•	•	k?
Niva	niva	k1gFnSc1
Morávky	Morávek	k1gMnPc4
•	•	k?
Obidová	Obidová	k1gFnSc1
•	•	k?
Ondrášovy	Ondrášův	k2eAgFnPc4d1
díry	díra	k1gFnPc4
•	•	k?
Paskov	Paskov	k1gInSc1
•	•	k?
Pod	pod	k7c7
hájenkou	hájenka	k1gFnSc7
Kyčera	Kyčero	k1gNnSc2
•	•	k?
Pod	pod	k7c7
Hukvaldskou	hukvaldský	k2eAgFnSc7d1
oborou	obora	k1gFnSc7
•	•	k?
Pod	pod	k7c7
Lukšincem	Lukšinec	k1gInSc7
•	•	k?
Podgrúň	Podgrúň	k1gFnSc1
•	•	k?
Profil	profil	k1gInSc1
Morávky	Morávek	k1gMnPc4
•	•	k?
Rohovec	rohovec	k1gInSc4
•	•	k?
Vodopády	vodopád	k1gInPc4
Satiny	Satina	k1gFnSc2
•	•	k?
Vysutý	vysutý	k2eAgInSc1d1
•	•	k?
Žermanický	žermanický	k2eAgInSc1d1
lom	lom	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Beskydy	Beskydy	k1gFnPc1
•	•	k?
Poodří	Poodří	k1gNnPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Radhošť	Radhošť	k1gMnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Šipka	šipka	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bartošovický	Bartošovický	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Bařiny	Bařina	k1gMnSc2
•	•	k?
Bažantula	Bažantul	k1gMnSc2
•	•	k?
Huštýn	Huštýn	k1gInSc1
•	•	k?
Koryta	koryto	k1gNnSc2
•	•	k?
Kotvice	kotvice	k1gFnSc1
•	•	k?
Královec	Královec	k1gInSc1
•	•	k?
Noříčí	Noříčí	k2eAgInSc1d1
•	•	k?
Rákosina	rákosina	k1gFnSc1
•	•	k?
Rybníky	rybník	k1gInPc1
v	v	k7c6
Trnávce	Trnávka	k1gFnSc6
•	•	k?
Suchá	suchý	k2eAgFnSc1d1
Dora	Dora	k1gFnSc1
•	•	k?
Svinec	svinec	k1gInSc1
•	•	k?
Trojačka	Trojačka	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Domorazské	Domorazský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Kamenárka	Kamenárka	k1gFnSc1
•	•	k?
Meandry	meandr	k1gInPc1
Staré	Staré	k2eAgInPc1d1
Odry	odr	k1gInPc1
•	•	k?
Na	na	k7c6
Čermence	Čermenka	k1gFnSc6
•	•	k?
Pikritové	Pikritový	k2eAgFnSc6d1
mandlovce	mandlovka	k1gFnSc6
u	u	k7c2
Kojetína	Kojetín	k1gInSc2
•	•	k?
Polštářové	polštářový	k2eAgFnSc2d1
lávy	láva	k1gFnSc2
ve	v	k7c6
Straníku	straník	k1gMnSc6
•	•	k?
Prameny	pramen	k1gInPc1
Zrzávky	Zrzávka	k1gFnSc2
•	•	k?
Pusté	pustý	k2eAgFnSc2d1
nivy	niva	k1gFnSc2
•	•	k?
Sedlnické	Sedlnický	k2eAgFnSc2d1
sněženky	sněženka	k1gFnSc2
•	•	k?
Stříbrné	stříbrný	k2eAgNnSc4d1
jezírko	jezírko	k1gNnSc4
•	•	k?
Travertinová	travertinový	k2eAgFnSc1d1
kaskáda	kaskáda	k1gFnSc1
•	•	k?
Váňův	Váňův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Vrásový	vrásový	k2eAgInSc1d1
soubor	soubor	k1gInSc1
v	v	k7c4
Klokočůvku	Klokočůvka	k1gFnSc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Vsetín	Vsetín	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc4
</s>
<s>
Hostýnské	hostýnský	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kněhyně	kněhyně	k1gFnSc1
–	–	k?
Čertův	čertův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
•	•	k?
Pulčín	Pulčína	k1gFnPc2
–	–	k?
Hradisko	hradisko	k1gNnSc4
•	•	k?
Razula	Razula	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Dubcová	Dubcová	k1gFnSc1
•	•	k?
Galovské	Galovská	k1gFnSc2
lúky	lúka	k1gFnSc2
•	•	k?
Halvovský	Halvovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Choryňský	Choryňský	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
•	•	k?
Klenov	Klenov	k1gInSc1
•	•	k?
Kutaný	kutaný	k2eAgInSc1d1
•	•	k?
Losový	losový	k2eAgInSc1d1
•	•	k?
Makyta	makyta	k1gFnSc1
•	•	k?
Malý	malý	k2eAgInSc1d1
Javorník	Javorník	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bečevná	Bečevný	k2eAgFnSc1d1
•	•	k?
Bražiska	Bražiska	k1gFnSc1
•	•	k?
Brodská	Brodská	k1gFnSc1
•	•	k?
Čertovy	čertův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Choryňská	Choryňský	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
•	•	k?
Jarcovská	Jarcovský	k2eAgFnSc1d1
kula	kula	k1gFnSc1
•	•	k?
Jasenice	Jasenice	k1gFnSc1
•	•	k?
Ježůvka	Ježůvka	k1gFnSc1
•	•	k?
Kladnatá	Kladnatý	k2eAgFnSc1d1
–	–	k?
Grapy	Grapy	k?
•	•	k?
Kopce	kopec	k1gInSc2
•	•	k?
Kotrlé	Kotrlý	k2eAgNnSc1d1
•	•	k?
Křížový	křížový	k2eAgMnSc1d1
•	•	k?
Kudlačena	Kudlačen	k2eAgMnSc4d1
•	•	k?
Lačnov	Lačnov	k1gInSc1
•	•	k?
Louka	louka	k1gFnSc1
pod	pod	k7c7
Rančem	ranč	k1gInSc7
•	•	k?
Louky	louka	k1gFnSc2
pod	pod	k7c7
Štípou	štípa	k1gFnSc7
•	•	k?
Lúčky	Lúčka	k1gFnSc2
–	–	k?
Roveňky	Roveňka	k1gFnSc2
•	•	k?
Mokřady	mokřad	k1gInPc4
Vesník	vesník	k1gMnSc1
•	•	k?
Nad	nad	k7c7
Jasenkou	Jasenka	k1gFnSc7
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Pivovařiska	Pivovařiska	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Juráškou	Juráška	k1gFnSc7
•	•	k?
Poskla	Poskla	k1gFnSc2
•	•	k?
Pozděchov	Pozděchov	k1gInSc1
•	•	k?
Prlov	Prlov	k1gInSc1
•	•	k?
Rákosina	rákosina	k1gFnSc1
ve	v	k7c6
Stříteži	Střítež	k1gFnSc6
nad	nad	k7c7
Bečvou	Bečva	k1gFnSc7
•	•	k?
Růžděcký	Růžděcký	k1gMnSc1
Vesník	vesník	k1gMnSc1
•	•	k?
Rybník	rybník	k1gInSc1
Neratov	Neratov	k1gInSc1
•	•	k?
Semetín	Semetín	k1gMnSc1
–	–	k?
lesní	lesní	k2eAgNnSc1d1
prameniště	prameniště	k1gNnSc1
•	•	k?
Semetín	Semetín	k1gMnSc1
–	–	k?
luční	luční	k2eAgNnSc4d1
prameniště	prameniště	k1gNnSc4
•	•	k?
Skálí	skálí	k1gNnSc2
•	•	k?
Smradlavá	smradlavý	k2eAgFnSc1d1
•	•	k?
Stříbrník	stříbrník	k1gInSc4
•	•	k?
Sucháčkovy	sucháčkův	k2eAgFnSc2d1
paseky	paseka	k1gFnSc2
•	•	k?
Svantovítova	Svantovítův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Trubiska	Trubiska	k1gFnSc1
•	•	k?
U	u	k7c2
Vaňků	Vaněk	k1gMnPc2
•	•	k?
Uherská	uherský	k2eAgFnSc1d1
•	•	k?
Vachalka	Vachalka	k1gFnSc1
•	•	k?
Vršky	vršek	k1gInPc1
–	–	k?
Díly	díl	k1gInPc1
•	•	k?
Zbrankova	Zbrankův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Zubří	zubří	k2eAgFnSc7d1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
</s>
