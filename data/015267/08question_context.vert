<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Beskydy	Beskydy	k1gFnPc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
CHKO	CHKO	kA
Beskydy	Beskydy	k1gFnPc4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1973	#num#	k4
<g/>
,	,	kIx,
vládním	vládní	k2eAgInSc7d1
výnosem	výnos	k1gInSc7
MK	MK	kA
ČSR	ČSR	kA
č.	č.	k?
<g/>
j.	j.	k?
5373	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
chráněnou	chráněný	k2eAgFnSc4d1
krajinnou	krajinný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>