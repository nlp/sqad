<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1968	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
okresů	okres	k1gInPc2
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
a	a	k8xC
Semily	Semily	k1gInPc1
<g/>
.	.	kIx.
</s>