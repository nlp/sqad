<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jizerské	jizerský	k2eAgFnSc2d1
horyIUCN	horyIUCN	k?
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Jizerohorské	Jizerohorský	k2eAgFnSc2d1
bučinyZákladní	bučinyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1967	#num#	k4
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
325	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
368	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Tanvald	Tanvald	k1gInSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
<g/>
,	,	kIx,
Hejnice	Hejnice	k1gFnSc1
<g/>
,	,	kIx,
Frýdlant	Frýdlant	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
45,1	45,1	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
17	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
52	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.jizerskehory.ochranaprirody.cz	www.jizerskehory.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1968	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
okresů	okres	k1gInPc2
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
a	a	k8xC
Semily	Semily	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
větší	veliký	k2eAgFnSc6d2
části	část	k1gFnSc6
plochy	plocha	k1gFnSc2
Jizerských	jizerský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
přímo	přímo	k6eAd1
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
Krkonošským	krkonošský	k2eAgInSc7d1
národním	národní	k2eAgInSc7d1
parkem	park	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
dnešní	dnešní	k2eAgInSc1d1
výměr	výměr	k1gInSc1
je	být	k5eAaImIp3nS
368	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
274	#num#	k4
km²	km²	k?
lesní	lesní	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
325	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
m.	m.	k?
Sídlo	sídlo	k1gNnSc4
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
vyhlášení	vyhlášení	k1gNnSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
postupné	postupný	k2eAgFnSc3d1
devastaci	devastace	k1gFnSc3
lesů	les	k1gInPc2
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
pro	pro	k7c4
provoz	provoz	k1gInSc4
nových	nový	k2eAgFnPc2d1
uhelných	uhelný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
(	(	kIx(
<g/>
exhalace	exhalace	k1gFnSc1
<g/>
,	,	kIx,
popílek	popílek	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
německé	německý	k2eAgFnSc6d1
a	a	k8xC
polské	polský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
několik	několik	k4yIc1
epidemií	epidemie	k1gFnPc2
lesních	lesní	k2eAgMnPc2d1
škůdců	škůdce	k1gMnPc2
a	a	k8xC
nakonec	nakonec	k6eAd1
těžká	těžký	k2eAgFnSc1d1
mechanizace	mechanizace	k1gFnSc1
lesních	lesní	k2eAgInPc2d1
závodů	závod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
pozvolné	pozvolný	k2eAgFnSc3d1
nápravě	náprava	k1gFnSc3
<g/>
,	,	kIx,
obnově	obnova	k1gFnSc3
lesa	les	k1gInSc2
dochází	docházet	k5eAaImIp3nS
až	až	k9
po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
obnovy	obnova	k1gFnSc2
však	však	k9
brzdí	brzdit	k5eAaImIp3nS
rozvoj	rozvoj	k1gInSc4
motorizace	motorizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
CHKO	CHKO	kA
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Jizerskohorské	jizerskohorský	k2eAgFnPc4d1
bučiny	bučina	k1gFnPc4
(	(	kIx(
<g/>
vyhlášeny	vyhlášen	k2eAgFnPc4d1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
spojením	spojení	k1gNnSc7
a	a	k8xC
rozšířením	rozšíření	k1gNnSc7
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
samostatných	samostatný	k2eAgInPc2d1
NPR	NPR	kA
Frýdlantské	frýdlantský	k2eAgNnSc1d1
cimbuří	cimbuří	k1gNnSc1
<g/>
,	,	kIx,
Paličník	Paličník	k1gInSc1
<g/>
,	,	kIx,
Poledník	poledník	k1gInSc1
<g/>
,	,	kIx,
Stržový	Stržový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Špičák	špičák	k1gInSc1
<g/>
,	,	kIx,
Štolpichy	Štolpich	k1gInPc1
a	a	k8xC
Tišina	tišina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
NPR	NPR	kA
Rašeliniště	rašeliniště	k1gNnSc4
Jizery	Jizera	k1gFnSc2
</s>
<s>
NPR	NPR	kA
Rašeliniště	rašeliniště	k1gNnSc4
Jizerky	Jizerka	k1gFnSc2
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
NPR	NPR	kA
činí	činit	k5eAaImIp3nS
3082	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
1824	#num#	k4
ha	ha	kA
(	(	kIx(
<g/>
tj.	tj.	kA
59	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
na	na	k7c4
ochranné	ochranný	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
(	(	kIx(
<g/>
41	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
na	na	k7c6
tzv.	tzv.	kA
jádrová	jádrový	k2eAgNnPc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
tedy	tedy	k9
bývalé	bývalý	k2eAgFnSc2d1
nebo	nebo	k8xC
dosud	dosud	k6eAd1
existující	existující	k2eAgInSc1d1
shora	shora	k6eAd1
uvedené	uvedený	k2eAgFnSc6d1
NPR	NPR	kA
<g/>
.	.	kIx.
</s>
<s>
Skály	skála	k1gFnPc1
na	na	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
vrcholu	vrchol	k1gInSc2
Špičák	špičák	k1gInSc1
</s>
<s>
Západní	západní	k2eAgNnSc1d1
úbočí	úbočí	k1gNnSc1
Ostrého	ostrý	k2eAgInSc2d1
hřebenu	hřeben	k1gInSc2
</s>
<s>
Cesta	cesta	k1gFnSc1
na	na	k7c6
západním	západní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
Ostrého	ostrý	k2eAgInSc2d1
hřebenu	hřeben	k1gInSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
PR	pr	k0
Bukovec	Bukovec	k1gInSc4
</s>
<s>
PR	pr	k0
Černá	černý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
</s>
<s>
PR	pr	k0
Černá	černý	k2eAgFnSc1d1
jezírka	jezírko	k1gNnSc2
</s>
<s>
PR	pr	k0
Jedlový	jedlový	k2eAgInSc1d1
důl	důl	k1gInSc4
</s>
<s>
PR	pr	k0
Klečové	klečový	k2eAgMnPc4d1
louky	louka	k1gFnPc4
</s>
<s>
PR	pr	k0
Klikvová	Klikvový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
PR	pr	k0
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
</s>
<s>
PR	pr	k0
Na	na	k7c4
Čihadle	čihadlo	k1gNnSc6
</s>
<s>
PR	pr	k0
Nová	nový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
PR	pr	k0
Prales	prales	k1gInSc4
Jizera	Jizera	k1gFnSc1
</s>
<s>
PR	pr	k0
Ptačí	ptačí	k2eAgFnSc1d1
kupy	kupa	k1gFnPc1
</s>
<s>
PR	pr	k0
Rybí	rybí	k2eAgFnSc1d1
loučky	loučka	k1gFnPc1
</s>
<s>
PR	pr	k0
Vápenný	vápenný	k2eAgInSc4d1
vrch	vrch	k1gInSc1
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
PR	pr	k0
je	být	k5eAaImIp3nS
617	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
197	#num#	k4
ha	ha	kA
(	(	kIx(
<g/>
32	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
na	na	k7c4
ochranné	ochranný	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
</s>
<s>
Černá	Černá	k1gFnSc1
jezírka	jezírko	k1gNnSc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Klečová	klečový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
Klikva	klikva	k1gFnSc1
bahenní	bahenní	k2eAgFnSc1d1
v	v	k7c6
PR	pr	k0
Klikvová	Klikvový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
</s>
<s>
Na	na	k7c6
Čihadle	čihadlo	k1gNnSc6
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
PP	PP	kA
Klečoviště	Klečoviště	k1gNnSc1
na	na	k7c6
Smrku	smrk	k1gInSc6
</s>
<s>
PP	PP	kA
Na	na	k7c6
Kneipě	Kneipa	k1gFnSc6
</s>
<s>
PP	PP	kA
Pod	pod	k7c7
Dračí	dračí	k2eAgFnSc7d1
skálou	skála	k1gFnSc7
</s>
<s>
PP	PP	kA
Pod	pod	k7c7
Smrkem	smrk	k1gInSc7
</s>
<s>
PP	PP	kA
U	u	k7c2
posedu	posed	k1gInSc2
</s>
<s>
PP	PP	kA
Vlčí	vlčí	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
PP	PP	kA
Quarré	Quarrý	k2eAgFnPc1d1
</s>
<s>
PP	PP	kA
Tichá	Tichá	k1gFnSc1
říčka	říčka	k1gFnSc1
</s>
<s>
PP	PP	kA
Fojtecký	Fojtecký	k2eAgInSc4d1
mokřad	mokřad	k1gInSc4
</s>
<s>
PP	PP	kA
Jindřichovský	Jindřichovský	k2eAgInSc4d1
mokřad	mokřad	k1gInSc4
</s>
<s>
PP	PP	kA
Černá	černý	k2eAgFnSc1d1
Desná	Desná	k1gFnSc1
</s>
<s>
PP	PP	kA
Tesařov	Tesařov	k1gInSc1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
PP	PP	kA
byla	být	k5eAaImAgFnS
přes	přes	k7c4
24	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Kneipě	Kneipa	k1gFnSc6
</s>
<s>
Jindřichovský	Jindřichovský	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
</s>
<s>
Vlčí	vlčí	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
Fojtecký	Fojtecký	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
</s>
<s>
Klečoviště	Klečoviště	k1gNnSc1
na	na	k7c6
Smrku	smrk	k1gInSc6
</s>
<s>
Pod	pod	k7c7
Smrkem	smrk	k1gInSc7
</s>
<s>
Přechodná	přechodný	k2eAgFnSc1d1
chráněná	chráněný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
nejsou	být	k5eNaImIp3nP
vymezeny	vymezit	k5eAaPmNgFnP
žádné	žádný	k3yNgFnPc1
přechodně	přechodně	k6eAd1
chráněné	chráněný	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Geologie	geologie	k1gFnPc1
a	a	k8xC
půdní	půdní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Většinu	většina	k1gFnSc4
území	území	k1gNnSc2
CHKO	CHKO	kA
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
tvoří	tvořit	k5eAaImIp3nP
horniny	hornina	k1gFnPc1
krkonošsko-jizerského	krkonošsko-jizerský	k2eAgInSc2d1
plutonu	pluton	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
granitem	granit	k1gInSc7
několika	několik	k4yIc2
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemismus	chemismus	k1gInSc4
granitů	granit	k1gInPc2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
geomorfologii	geomorfologie	k1gFnSc3
i	i	k8xC
složení	složení	k1gNnSc3
vegetačního	vegetační	k2eAgInSc2d1
porkyvu	porkyv	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dává	dávat	k5eAaImIp3nS
Jizerským	jizerský	k2eAgFnPc3d1
horám	hora	k1gFnPc3
jedinečný	jedinečný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
jsou	být	k5eAaImIp3nP
granity	granit	k1gInPc4
prostoupeny	prostoupen	k2eAgInPc4d1
mladšími	mladý	k2eAgInPc7d2
vulkanity	vulkanit	k1gInPc7
třetihorního	třetihorní	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
Bukovec	Bukovec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
okrajové	okrajový	k2eAgFnSc3d1
části	část	k1gFnSc3
plutonu	pluton	k1gInSc2
jsou	být	k5eAaImIp3nP
metamorfované	metamorfovaný	k2eAgFnPc1d1
(	(	kIx(
<g/>
mramor	mramor	k1gInSc1
na	na	k7c6
Vápenném	vápenný	k2eAgInSc6d1
vrchu	vrch	k1gInSc6
<g/>
,	,	kIx,
krystalické	krystalický	k2eAgFnSc2d1
břidlice	břidlice	k1gFnSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Smrku	smrk	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
vystavena	vystavit	k5eAaPmNgFnS
dlouhotrvajícímu	dlouhotrvající	k2eAgNnSc3d1
zvětrávání	zvětrávání	k1gNnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
podmiňuje	podmiňovat	k5eAaImIp3nS
její	její	k3xOp3gInSc4
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
balvanů	balvan	k1gInPc2
a	a	k8xC
kamenných	kamenný	k2eAgNnPc2d1
moří	moře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ŠEBELKA	ŠEBELKA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
umíraly	umírat	k5eAaImAgInP
stromy	strom	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teď	teď	k6eAd1
se	s	k7c7
sem	sem	k6eAd1
zase	zase	k9
pomalu	pomalu	k6eAd1
vrací	vracet	k5eAaImIp3nS
život	život	k1gInSc1
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
MF	MF	kA
DNES	dnes	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-07	2013-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Geologie	geologie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
CHKO	CHKO	kA
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
Národní	národní	k2eAgFnSc2d1
parky	park	k1gInPc1
</s>
<s>
Krkonošský	krkonošský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jizerské	jizerský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
Jizerky	Jizerka	k1gFnSc2
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Jizery	Jizera	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Suché	Suché	k2eAgFnPc1d1
skály	skála	k1gFnPc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Bučiny	bučina	k1gFnPc1
u	u	k7c2
Rakous	Rakousy	k1gInPc2
•	•	k?
Bukovec	Bukovec	k1gInSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
hora	hora	k1gFnSc1
•	•	k?
Černá	Černá	k1gFnSc1
jezírka	jezírko	k1gNnSc2
•	•	k?
Jedlový	jedlový	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Klikvová	Klikvová	k1gFnSc1
louka	louka	k1gFnSc1
•	•	k?
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Rybí	rybí	k2eAgFnPc4d1
loučky	loučka	k1gFnPc4
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Desná	Desná	k1gFnSc1
•	•	k?
Jindřichovský	Jindřichovský	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
•	•	k?
Lukášov	Lukášov	k1gInSc1
•	•	k?
Na	na	k7c6
Vápenici	vápenice	k1gFnSc6
•	•	k?
Ondříkovický	Ondříkovický	k2eAgInSc1d1
pseudokrasový	pseudokrasový	k2eAgInSc1d1
systém	systém	k1gInSc1
•	•	k?
Podloučky	Podloučka	k1gFnSc2
•	•	k?
Rádlo	rádlo	k1gNnSc1
•	•	k?
Tesařov	Tesařovo	k1gNnPc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
