<s>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Harry	Harr	k1gInPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
and	and	k?	and
the	the	k?	the
Prisoner	Prisoner	k1gInSc1	Prisoner
of	of	k?	of
Azkaban	Azkaban	k1gInSc1	Azkaban
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc1	třetí
díl	díl	k1gInSc1	díl
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
od	od	k7c2	od
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc4d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
režisérem	režisér	k1gMnSc7	režisér
Alfonsem	Alfons	k1gMnSc7	Alfons
Cuarónem	Cuarón	k1gMnSc7	Cuarón
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
Oscary	Oscara	k1gFnPc4	Oscara
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
publika	publikum	k1gNnSc2	publikum
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
ve	v	k7c6	v
zlém	zlé	k1gNnSc6	zlé
opouští	opouštět	k5eAaImIp3nP	opouštět
Dursleyovy	Dursleyův	k2eAgFnPc1d1	Dursleyův
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
přijde	přijít	k5eAaPmIp3nS	přijít
neoblíbená	oblíbený	k2eNgFnSc1d1	neoblíbená
teta	teta	k1gFnSc1	teta
Marge	Marg	k1gFnSc2	Marg
<g/>
.	.	kIx.	.
</s>
<s>
Nemaje	mít	k5eNaImSgMnS	mít
kam	kam	k6eAd1	kam
jít	jít	k5eAaImF	jít
a	a	k8xC	a
vystrašen	vystrašen	k2eAgInSc4d1	vystrašen
velkým	velký	k2eAgMnSc7d1	velký
černým	černý	k2eAgMnSc7d1	černý
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Harry	Harr	k1gMnPc4	Harr
vyzvednut	vyzvednut	k2eAgInSc1d1	vyzvednut
Záchranným	záchranný	k2eAgInSc7d1	záchranný
autobusem	autobus	k1gInSc7	autobus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
odveze	odvézt	k5eAaPmIp3nS	odvézt
do	do	k7c2	do
Děravého	děravý	k2eAgInSc2d1	děravý
kotle	kotel	k1gInSc2	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
že	že	k8xS	že
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
jde	jít	k5eAaImIp3nS	jít
vrah	vrah	k1gMnSc1	vrah
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
uprchlý	uprchlý	k1gMnSc1	uprchlý
z	z	k7c2	z
Azkabanského	Azkabanský	k2eAgNnSc2d1	Azkabanské
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkají	setkat	k5eAaPmIp3nP	setkat
s	s	k7c7	s
mozkomory	mozkomor	k1gMnPc7	mozkomor
-	-	kIx~	-
strážci	strážce	k1gMnPc7	strážce
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
,	,	kIx,	,
chránícími	chránící	k2eAgFnPc7d1	chránící
školu	škola	k1gFnSc4	škola
před	před	k7c7	před
Siriusem	Sirius	k1gMnSc7	Sirius
Blackem	Black	k1gInSc7	Black
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
mozkomorem	mozkomor	k1gInSc7	mozkomor
Harry	Harra	k1gMnSc2	Harra
ztratí	ztratit	k5eAaPmIp3nS	ztratit
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
mozkomory	mozkomor	k1gInPc4	mozkomor
zažene	zahnat	k5eAaPmIp3nS	zahnat
nový	nový	k2eAgMnSc1d1	nový
učitel	učitel	k1gMnSc1	učitel
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
Hagrid	Hagrida	k1gFnPc2	Hagrida
začíná	začínat	k5eAaImIp3nS	začínat
nově	nově	k6eAd1	nově
učit	učit	k5eAaImF	učit
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
kouzelné	kouzelný	k2eAgMnPc4d1	kouzelný
tvory	tvor	k1gMnPc4	tvor
<g/>
;	;	kIx,	;
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
hodině	hodina	k1gFnSc6	hodina
Draco	Draco	k1gNnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
neposlouchá	poslouchat	k5eNaImIp3nS	poslouchat
Hagrida	Hagrida	k1gFnSc1	Hagrida
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
napaden	napadnout	k5eAaPmNgInS	napadnout
hipogryfem	hipogryf	k1gInSc7	hipogryf
(	(	kIx(	(
<g/>
napůl	napůl	k6eAd1	napůl
koněm	kůň	k1gMnSc7	kůň
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
orlem	orel	k1gMnSc7	orel
<g/>
)	)	kIx)	)
Klofanem	klofan	k1gInSc7	klofan
<g/>
.	.	kIx.	.
</s>
<s>
Draco	Draco	k6eAd1	Draco
si	se	k3xPyFc3	se
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
svému	svůj	k3xOyFgMnSc3	svůj
vlivnému	vlivný	k2eAgMnSc3d1	vlivný
otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Hermionin	Hermionin	k2eAgMnSc1d1	Hermionin
nový	nový	k2eAgMnSc1d1	nový
domácí	domácí	k2eAgMnSc1d1	domácí
mazlíček	mazlíček	k1gMnSc1	mazlíček
-	-	kIx~	-
kočka	kočka	k1gFnSc1	kočka
Křivonožka	křivonožka	k1gMnSc1	křivonožka
-	-	kIx~	-
rozdmýchá	rozdmýchat	k5eAaPmIp3nS	rozdmýchat
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
Hermionou	Hermiona	k1gFnSc7	Hermiona
a	a	k8xC	a
Ronem	ron	k1gInSc7	ron
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Ronovi	Ron	k1gMnSc3	Ron
za	za	k7c2	za
podezřelých	podezřelý	k2eAgFnPc2d1	podezřelá
okolností	okolnost	k1gFnPc2	okolnost
ztratí	ztratit	k5eAaPmIp3nS	ztratit
jeho	jeho	k3xOp3gFnSc1	jeho
krysa	krysa	k1gFnSc1	krysa
Prašivka	prašivka	k1gFnSc1	prašivka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Vánoci	Vánoce	k1gFnPc7	Vánoce
dají	dát	k5eAaPmIp3nP	dát
Weasleyovic	Weasleyovice	k1gFnPc2	Weasleyovice
dvojčata	dvojče	k1gNnPc1	dvojče
Harrymu	Harrym	k1gInSc3	Harrym
"	"	kIx"	"
<g/>
Pobertův	pobertův	k2eAgInSc1d1	pobertův
plánek	plánek	k1gInSc1	plánek
<g/>
"	"	kIx"	"
-	-	kIx~	-
úplnou	úplný	k2eAgFnSc4d1	úplná
mapu	mapa	k1gFnSc4	mapa
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
poloha	poloha	k1gFnSc1	poloha
každého	každý	k3xTgMnSc2	každý
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
hradu	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
mapy	mapa	k1gFnSc2	mapa
projde	projít	k5eAaPmIp3nS	projít
do	do	k7c2	do
Prasinek	Prasinka	k1gFnPc2	Prasinka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
hovoru	hovor	k1gInSc6	hovor
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
kmotrem	kmotr	k1gMnSc7	kmotr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zradil	zradit	k5eAaPmAgMnS	zradit
jeho	jeho	k3xOp3gMnPc4	jeho
rodiče	rodič	k1gMnPc4	rodič
a	a	k8xC	a
zabil	zabít	k5eAaPmAgMnS	zabít
jejich	jejich	k3xOp3gMnSc4	jejich
kamaráda	kamarád	k1gMnSc4	kamarád
Petera	Peter	k1gMnSc4	Peter
Pettigrewa	Pettigrewus	k1gMnSc4	Pettigrewus
<g/>
.	.	kIx.	.
</s>
<s>
Trojice	trojice	k1gFnSc1	trojice
nerozlučných	rozlučný	k2eNgMnPc2d1	nerozlučný
kamarádů	kamarád	k1gMnPc2	kamarád
se	se	k3xPyFc4	se
od	od	k7c2	od
Hagrida	Hagrid	k1gMnSc2	Hagrid
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klofan	klofan	k1gInSc1	klofan
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
intrikám	intrika	k1gFnPc3	intrika
Malfoyova	Malfoyův	k2eAgInSc2d1	Malfoyův
otce	otec	k1gMnSc2	otec
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
Hagrida	Hagrida	k1gFnSc1	Hagrida
utěšit	utěšit	k5eAaPmF	utěšit
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
chatě	chata	k1gFnSc6	chata
najdou	najít	k5eAaPmIp3nP	najít
živého	živý	k2eAgMnSc4d1	živý
Prašivku	prašivka	k1gFnSc4	prašivka
<g/>
.	.	kIx.	.
</s>
<s>
Křivonožka	křivonožka	k1gMnSc1	křivonožka
ale	ale	k9	ale
Prašivku	prašivka	k1gFnSc4	prašivka
zažene	zahnat	k5eAaPmIp3nS	zahnat
pod	pod	k7c4	pod
Vrbu	vrba	k1gFnSc4	vrba
mlátičku	mlátička	k1gFnSc4	mlátička
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
velký	velký	k2eAgMnSc1d1	velký
černý	černý	k2eAgMnSc1d1	černý
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc2	který
Harry	Harra	k1gMnSc2	Harra
viděl	vidět	k5eAaImAgMnS	vidět
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
a	a	k8xC	a
odvleče	odvléct	k5eAaPmIp3nS	odvléct
pod	pod	k7c4	pod
vrbu	vrba	k1gFnSc4	vrba
Rona	Ron	k1gMnSc2	Ron
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Herminona	Herminona	k1gFnSc1	Herminona
je	být	k5eAaImIp3nS	být
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
ukrytým	ukrytý	k2eAgInSc7d1	ukrytý
tunelem	tunel	k1gInSc7	tunel
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
přeměněný	přeměněný	k2eAgMnSc1d1	přeměněný
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
Lupin	lupina	k1gFnPc2	lupina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Hermiona	Hermiona	k1gFnSc1	Hermiona
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
Lupin	lupina	k1gFnPc2	lupina
a	a	k8xC	a
Black	Blacka	k1gFnPc2	Blacka
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ronova	Ronův	k2eAgFnSc1d1	Ronova
krysa	krysa	k1gFnSc1	krysa
je	být	k5eAaImIp3nS	být
přeměněný	přeměněný	k2eAgMnSc1d1	přeměněný
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
Voldemorta	Voldemort	k1gMnSc4	Voldemort
zradil	zradit	k5eAaPmAgMnS	zradit
Harryho	Harry	k1gMnSc4	Harry
rodiče	rodič	k1gMnSc4	rodič
<g/>
,	,	kIx,	,
fingoval	fingovat	k5eAaBmAgMnS	fingovat
svoji	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
vinu	vina	k1gFnSc4	vina
svedl	svést	k5eAaPmAgInS	svést
na	na	k7c4	na
Blacka	Blacko	k1gNnPc4	Blacko
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
Siriusovi	Sirius	k1gMnSc6	Sirius
nedovolí	dovolit	k5eNaPmIp3nP	dovolit
Pettigrewa	Pettigrewa	k1gMnSc1	Pettigrewa
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
jej	on	k3xPp3gInSc4	on
eskortují	eskortovat	k5eAaBmIp3nP	eskortovat
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
ale	ale	k8xC	ale
vysvitne	vysvitnout	k5eAaPmIp3nS	vysvitnout
úplněk	úplněk	k1gInSc4	úplněk
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lupin	lupina	k1gFnPc2	lupina
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
ve	v	k7c4	v
vlkodlaka	vlkodlak	k1gMnSc4	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
zmatku	zmatek	k1gInSc6	zmatek
Pettigrew	Pettigrew	k1gFnSc2	Pettigrew
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Blacka	Blacko	k1gNnPc4	Blacko
<g/>
,	,	kIx,	,
Harryho	Harry	k1gMnSc2	Harry
a	a	k8xC	a
Hermionu	Hermion	k1gInSc2	Hermion
začínají	začínat	k5eAaImIp3nP	začínat
útočit	útočit	k5eAaImF	útočit
mozkomorové	mozkomorová	k1gFnPc4	mozkomorová
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
ale	ale	k8xC	ale
zažene	zahnat	k5eAaPmIp3nS	zahnat
tajemná	tajemný	k2eAgFnSc1d1	tajemná
figura	figura	k1gFnSc1	figura
pomocí	pomocí	k7c2	pomocí
kouzla	kouzlo	k1gNnSc2	kouzlo
Patron	patrona	k1gFnPc2	patrona
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
věří	věřit	k5eAaImIp3nS	věřit
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zachráněn	zachránit	k5eAaPmNgMnS	zachránit
duchem	duch	k1gMnSc7	duch
svého	svůj	k1gMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gInSc1	Black
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
nevině	nevina	k1gFnSc6	nevina
nikdo	nikdo	k3yNnSc1	nikdo
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lapen	lapit	k5eAaPmNgMnS	lapit
a	a	k8xC	a
zavřen	zavřít	k5eAaPmNgMnS	zavřít
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
popravu	poprava	k1gFnSc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
hrdinům	hrdina	k1gMnPc3	hrdina
poněkud	poněkud	k6eAd1	poněkud
tajemně	tajemně	k6eAd1	tajemně
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
zachránit	zachránit	k5eAaPmF	zachránit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
prozradí	prozradit	k5eAaPmIp3nS	prozradit
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgInSc1d1	vlastní
Obraceč	obraceč	k1gInSc1	obraceč
času	čas	k1gInSc2	čas
-	-	kIx~	-
nástroj	nástroj	k1gInSc1	nástroj
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
vracet	vracet	k5eAaImF	vracet
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
zvládala	zvládat	k5eAaImAgFnS	zvládat
svůj	svůj	k3xOyFgInSc4	svůj
přeplněný	přeplněný	k2eAgInSc4d1	přeplněný
rozvrh	rozvrh	k1gInSc4	rozvrh
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gMnSc2	Harra
a	a	k8xC	a
Hermiona	Hermion	k1gMnSc2	Hermion
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
Klofana	Klofana	k1gFnSc1	Klofana
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
čekají	čekat	k5eAaImIp3nP	čekat
u	u	k7c2	u
východu	východ	k1gInSc2	východ
z	z	k7c2	z
tunelu	tunel	k1gInSc2	tunel
na	na	k7c4	na
Blacka	Blacko	k1gNnPc4	Blacko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
druzí	druhý	k4xOgMnPc1	druhý
<g/>
"	"	kIx"	"
Harry	Harro	k1gNnPc7	Harro
<g/>
,	,	kIx,	,
Hermiona	Hermiona	k1gFnSc1	Hermiona
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
napadáni	napadán	k2eAgMnPc1d1	napadán
mozkomory	mozkomora	k1gFnSc2	mozkomora
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnSc2	Harra
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
vyčaroval	vyčarovat	k5eAaPmAgMnS	vyčarovat
Patrona	patron	k1gMnSc4	patron
<g/>
,	,	kIx,	,
a	a	k8xC	a
zachrání	zachránit	k5eAaPmIp3nP	zachránit
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Klofana	Klofana	k1gFnSc1	Klofana
pak	pak	k6eAd1	pak
Harry	Harra	k1gFnSc2	Harra
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
Blacka	Blacka	k1gFnSc1	Blacka
<g/>
,	,	kIx,	,
a	a	k8xC	a
zatímco	zatímco	k8xS	zatímco
ten	ten	k3xDgMnSc1	ten
odlétá	odlétat	k5eAaPmIp3nS	odlétat
<g/>
,	,	kIx,	,
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začali	začít	k5eAaPmAgMnP	začít
cestu	cesta	k1gFnSc4	cesta
časem	časem	k6eAd1	časem
<g/>
.	.	kIx.	.
</s>
