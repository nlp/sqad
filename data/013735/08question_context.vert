<s>
Kvartsextakord	kvartsextakord	k1gInSc1
je	být	k5eAaImIp3nS
termín	termín	k1gInSc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
hudební	hudební	k2eAgFnSc2d1
nauky	nauka	k1gFnSc2
<g/>
,	,	kIx,
označující	označující	k2eAgInSc4d1
souzvuk	souzvuk	k1gInSc4
tří	tři	k4xCgFnPc2
a	a	k8xC
více	hodně	k6eAd2
tónů	tón	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgInP
podle	podle	k7c2
určitého	určitý	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>