<s>
Kvartsextakord	kvartsextakord	k1gInSc1
</s>
<s>
Kvartsextakord	kvartsextakord	k1gInSc1
je	být	k5eAaImIp3nS
termín	termín	k1gInSc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
hudební	hudební	k2eAgFnSc2d1
nauky	nauka	k1gFnSc2
<g/>
,	,	kIx,
označující	označující	k2eAgInSc4d1
souzvuk	souzvuk	k1gInSc4
tří	tři	k4xCgFnPc2
a	a	k8xC
více	hodně	k6eAd2
tónů	tón	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgInP
podle	podle	k7c2
určitého	určitý	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
hudební	hudební	k2eAgInSc4d1
akord	akord	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
druhým	druhý	k4xOgInSc7
obratem	obrat	k1gInSc7
kvintakordu	kvintakord	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hudebních	hudební	k2eAgFnPc6d1
školách	škola	k1gFnPc6
se	se	k3xPyFc4
často	často	k6eAd1
užívá	užívat	k5eAaImIp3nS
na	na	k7c4
procvičování	procvičování	k1gNnSc4
hudebního	hudební	k2eAgInSc2d1
sluchu	sluch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
kvartsextakordů	kvartsextakord	k1gInPc2
</s>
<s>
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
výše	vysoce	k6eAd2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
kvartsextakord	kvartsextakord	k1gInSc1
druhým	druhý	k4xOgInSc7
obratem	obrat	k1gInSc7
kvintakordu	kvintakord	k1gInSc2
-	-	kIx~
tj.	tj.	kA
kvintakordem	kvintakord	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
postaven	postavit	k5eAaPmNgInS
nikoliv	nikoliv	k9
na	na	k7c6
svém	svůj	k3xOyFgInSc6
základním	základní	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
kvintě	kvinta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
Pro	pro	k7c4
akord	akord	k1gInSc4
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
=	=	kIx~
c-e-g	c-e-g	k1gMnSc1
je	být	k5eAaImIp3nS
druhým	druhý	k4xOgInSc7
obratem	obrat	k1gInSc7
(	(	kIx(
<g/>
a	a	k8xC
tedy	tedy	k9
kvartsextakordem	kvartsextakord	k1gInSc7
<g/>
)	)	kIx)
akord	akord	k1gInSc1
g-c-	g-c-	k?
<g/>
e.	e.	k?
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
kvintakordů	kvintakord	k1gInPc2
existují	existovat	k5eAaImIp3nP
i	i	k9
u	u	k7c2
kvartsextakordů	kvartsextakord	k1gInPc2
čtyři	čtyři	k4xCgInPc4
základní	základní	k2eAgInPc4d1
typy	typ	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
ukazuje	ukazovat	k5eAaImIp3nS
následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
příklad	příklad	k1gInSc1
jsou	být	k5eAaImIp3nP
použity	použít	k5eAaPmNgInP
akordy	akord	k1gInPc7
od	od	k7c2
základního	základní	k2eAgInSc2d1
tónu	tón	k1gInSc2
c	c	k0
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštním	zvláštní	k2eAgInSc7d1
případem	případ	k1gInSc7
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc4
obrat	obrat	k1gInSc4
zvětšeného	zvětšený	k2eAgInSc2d1
kvintakordu	kvintakord	k1gInSc2
-	-	kIx~
tento	tento	k3xDgInSc1
akord	akord	k1gInSc1
je	být	k5eAaImIp3nS
symetrický	symetrický	k2eAgInSc1d1
a	a	k8xC
nemá	mít	k5eNaImIp3nS
u	u	k7c2
něj	on	k3xPp3gInSc2
smysl	smysl	k1gInSc4
mluvit	mluvit	k5eAaImF
o	o	k7c6
obratech	obrat	k1gInPc6
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gFnSc1
„	„	k?
<g/>
obrácením	obrácení	k1gNnSc7
<g/>
“	“	k?
vzniká	vznikat	k5eAaImIp3nS
stejný	stejný	k2eAgInSc4d1
akord	akord	k1gInSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
tabulka	tabulka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kvintakord	kvintakord	k1gInSc1
</s>
<s>
Akordová	akordový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
</s>
<s>
Složení	složení	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
c	c	k0
<g/>
)	)	kIx)
</s>
<s>
Složení	složení	k1gNnSc1
kvartsextakordu	kvartsextakord	k1gInSc2
(	(	kIx(
<g/>
od	od	k7c2
c	c	k0
<g/>
)	)	kIx)
</s>
<s>
Intervaly	interval	k1gInPc1
kvartsextakordu	kvartsextakord	k1gInSc2
</s>
<s>
Značka	značka	k1gFnSc1
kvartsextakordu	kvartsextakord	k1gInSc2
</s>
<s>
durový	durový	k2eAgInSc1d1
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
c-e-g	c-e-g	k1gMnSc1
</s>
<s>
g-c-e	g-c-e	k6eAd1
</s>
<s>
čistá	čistý	k2eAgFnSc1d1
kvarta	kvarta	k1gFnSc1
<g/>
,	,	kIx,
velká	velký	k2eAgFnSc1d1
sexta	sexta	k1gFnSc1
</s>
<s>
C	C	kA
</s>
<s>
G	G	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
G	G	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
mollový	mollový	k2eAgInSc1d1
</s>
<s>
C	C	kA
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Cmi	Cmi	k1gMnSc5
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
c-es-g	c-es-g	k1gMnSc1
</s>
<s>
g-c-es	g-c-es	k1gMnSc1
</s>
<s>
čistá	čistý	k2eAgFnSc1d1
kvarta	kvarta	k1gFnSc1
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
sexta	sexta	k1gFnSc1
</s>
<s>
C	C	kA
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
G	G	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
Cmi	Cmi	k1gFnSc1
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
G	G	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
zvětšený	zvětšený	k2eAgInSc1d1
</s>
<s>
C	C	kA
</s>
<s>
5	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
c-e-gis	c-e-gis	k1gFnSc1
</s>
<s>
gis-c-e	gis-c-e	k6eAd1
</s>
<s>
zmenšená	zmenšený	k2eAgFnSc1d1
kvarta	kvarta	k1gFnSc1
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
sexta	sexta	k1gFnSc1
</s>
<s>
G	G	kA
</s>
<s>
#	#	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
G	G	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
#	#	kIx~
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
zmenšený	zmenšený	k2eAgInSc1d1
</s>
<s>
C	C	kA
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Cmi	Cmi	k1gMnPc6
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
c-es-ges	c-es-ges	k1gMnSc1
</s>
<s>
ges-c-es	ges-c-es	k1gMnSc1
</s>
<s>
zvětšená	zvětšený	k2eAgFnSc1d1
kvarta	kvarta	k1gFnSc1
<g/>
,	,	kIx,
velká	velký	k2eAgFnSc1d1
sexta	sexta	k1gFnSc1
</s>
<s>
C	C	kA
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
G	G	kA
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
Cmi	Cmi	k1gFnSc1
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
G	G	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Značení	značení	k1gNnSc1
a	a	k8xC
význam	význam	k1gInSc1
kvartsextakordu	kvartsextakord	k1gInSc2
</s>
<s>
Z	z	k7c2
předchozí	předchozí	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
je	být	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
způsobem	způsob	k1gInSc7
jsou	být	k5eAaImIp3nP
obraty	obrat	k1gInPc4
obecně	obecně	k6eAd1
(	(	kIx(
<g/>
tedy	tedy	k9
i	i	k9
kvartsextakordy	kvartsextakord	k1gInPc1
<g/>
)	)	kIx)
značeny	značen	k2eAgFnPc1d1
(	(	kIx(
<g/>
zlomkem	zlomek	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
čitateli	čitatel	k1gInSc6
je	být	k5eAaImIp3nS
akordová	akordový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
a	a	k8xC
ve	v	k7c6
jmenovateli	jmenovatel	k1gInSc6
nejnižší	nízký	k2eAgInSc4d3
tón	tón	k1gInSc4
příslušného	příslušný	k2eAgInSc2d1
obratu	obrat	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podrobnosti	podrobnost	k1gFnPc4
ke	k	k7c3
značení	značení	k1gNnSc3
a	a	k8xC
významu	význam	k1gInSc3
obratů	obrat	k1gInPc2
lze	lze	k6eAd1
najít	najít	k5eAaPmF
v	v	k7c6
článku	článek	k1gInSc6
Obrat	obrat	k1gInSc1
akordu	akord	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Obrat	obrat	k1gInSc1
akordu	akord	k1gInSc2
</s>
<s>
Sextakord	sextakord	k1gInSc1
</s>
<s>
Kvintakord	kvintakord	k1gInSc1
</s>
<s>
Terciový	terciový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Akordová	akordový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
akordových	akordový	k2eAgFnPc2d1
značek	značka	k1gFnPc2
</s>
