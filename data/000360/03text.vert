<s>
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
(	(	kIx(	(
<g/>
také	také	k9	také
Pýthagorás	Pýthagorás	k1gInSc1	Pýthagorás
<g/>
,	,	kIx,	,
řec.	řec.	k?	řec.
Π	Π	k?	Π
ο	ο	k?	ο
Σ	Σ	k?	Σ
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
570	[number]	k4	570
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ostrov	ostrov	k1gInSc1	ostrov
Samos	Samos	k1gInSc1	Samos
-	-	kIx~	-
po	po	k7c4	po
510	[number]	k4	510
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Krotón	Krotón	k1gMnSc1	Krotón
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
legendární	legendární	k2eAgMnSc1d1	legendární
řecký	řecký	k2eAgMnSc1d1	řecký
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
i	i	k8xC	i
kněz	kněz	k1gMnSc1	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
veřejně	veřejně	k6eAd1	veřejně
činný	činný	k2eAgMnSc1d1	činný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
často	často	k6eAd1	často
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
nějaké	nějaký	k3yIgNnSc4	nějaký
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nezachovalo	zachovat	k5eNaPmAgNnS	zachovat
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
však	však	k9	však
velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc4d1	významná
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
výklady	výklad	k1gInPc4	výklad
i	i	k8xC	i
legendy	legenda	k1gFnSc2	legenda
jeho	jeho	k3xOp3gMnPc2	jeho
následovníků	následovník	k1gMnPc2	následovník
překryly	překrýt	k5eAaPmAgFnP	překrýt
jeho	jeho	k3xOp3gFnPc4	jeho
původní	původní	k2eAgFnPc4d1	původní
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
rekonstruují	rekonstruovat	k5eAaBmIp3nP	rekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Pythagorejská	pythagorejský	k2eAgFnSc1d1	pythagorejská
tradice	tradice	k1gFnSc1	tradice
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Platóna	Platón	k1gMnSc4	Platón
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
živá	živý	k2eAgFnSc1d1	živá
v	v	k7c6	v
novoplatónismu	novoplatónismus	k1gInSc6	novoplatónismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
-	-	kIx~	-
často	často	k6eAd1	často
fantastických	fantastický	k2eAgInPc2d1	fantastický
-	-	kIx~	-
podobách	podoba	k1gFnPc6	podoba
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
otec	otec	k1gMnSc1	otec
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Samos	Samos	k1gInSc1	Samos
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
snad	snad	k9	snad
kupec	kupec	k1gMnSc1	kupec
nebo	nebo	k8xC	nebo
rytec	rytec	k1gMnSc1	rytec
prstenů	prsten	k1gInPc2	prsten
Mésarchos	Mésarchos	k1gInSc4	Mésarchos
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
učiteli	učitel	k1gMnPc7	učitel
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
Ferekýdés	Ferekýdés	k1gInSc1	Ferekýdés
ze	z	k7c2	z
Syru	Syrus	k1gInSc2	Syrus
a	a	k8xC	a
Anaximandros	Anaximandrosa	k1gFnPc2	Anaximandrosa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Babylonii	Babylonie	k1gFnSc6	Babylonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
východními	východní	k2eAgFnPc7d1	východní
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
myšlenkami	myšlenka	k1gFnPc7	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
538	[number]	k4	538
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
Samu	Samos	k1gInSc6	Samos
tyran	tyran	k1gMnSc1	tyran
Polykratés	Polykratés	k1gInSc1	Polykratés
<g/>
,	,	kIx,	,
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
530	[number]	k4	530
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Crotone	Croton	k1gInSc5	Croton
v	v	k7c6	v
Kalabrii	Kalabrie	k1gFnSc6	Kalabrie
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc7	žák
podle	podle	k7c2	podle
přísných	přísný	k2eAgNnPc2d1	přísné
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
společenství	společenství	k1gNnSc6	společenství
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
i	i	k9	i
značný	značný	k2eAgInSc4d1	značný
veřejný	veřejný	k2eAgInSc4d1	veřejný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Theano	Theana	k1gFnSc5	Theana
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
také	také	k9	také
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
městem	město	k1gNnSc7	město
Sybaris	Sybaris	k1gFnSc2	Sybaris
krotónští	krotónský	k2eAgMnPc1d1	krotónský
roku	rok	k1gInSc2	rok
510	[number]	k4	510
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
kvůli	kvůli	k7c3	kvůli
dělení	dělení	k1gNnSc3	dělení
dobyté	dobytý	k2eAgFnSc2d1	dobytá
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
hněv	hněv	k1gInSc4	hněv
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
proti	proti	k7c3	proti
Pythagorovi	Pythagoras	k1gMnSc3	Pythagoras
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
města	město	k1gNnSc2	město
a	a	k8xC	a
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
asi	asi	k9	asi
160	[number]	k4	160
km	km	kA	km
severněji	severně	k6eAd2	severně
v	v	k7c6	v
Metapontu	Metapont	k1gInSc6	Metapont
u	u	k7c2	u
Tarenta	Tarento	k1gNnSc2	Tarento
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
prý	prý	k9	prý
občané	občan	k1gMnPc1	občan
zřídili	zřídit	k5eAaPmAgMnP	zřídit
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
domě	dům	k1gInSc6	dům
chrám	chrám	k1gInSc1	chrám
bohyně	bohyně	k1gFnSc2	bohyně
Déméter	Démétér	k1gFnPc2	Démétér
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
svědectví	svědectví	k1gNnSc1	svědectví
o	o	k7c6	o
Pythagorovi	Pythagoras	k1gMnSc6	Pythagoras
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
básníka	básník	k1gMnSc2	básník
Ióna	Ión	k1gMnSc2	Ión
z	z	k7c2	z
Chiu	Chius	k1gInSc2	Chius
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
psal	psát	k5eAaImAgMnS	psát
básně	báseň	k1gFnSc2	báseň
a	a	k8xC	a
pozdější	pozdní	k2eAgMnPc1d2	pozdější
římští	římský	k2eAgMnPc1d1	římský
autoři	autor	k1gMnPc1	autor
mu	on	k3xPp3gMnSc3	on
připisují	připisovat	k5eAaImIp3nP	připisovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Posvátná	posvátný	k2eAgFnSc1d1	posvátná
řeč	řeč	k1gFnSc1	řeč
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Hieros	Hierosa	k1gFnPc2	Hierosa
logos	logos	k1gInSc1	logos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
cituje	citovat	k5eAaBmIp3nS	citovat
začátek	začátek	k1gInSc1	začátek
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mladíci	mladík	k1gMnPc1	mladík
<g/>
,	,	kIx,	,
přec	přec	k8xC	přec
v	v	k7c6	v
úctě	úcta	k1gFnSc6	úcta
chovejte	chovat	k5eAaImRp2nP	chovat
s	s	k7c7	s
tichostí	tichost	k1gFnSc7	tichost
toto	tento	k3xDgNnSc4	tento
vše	všechen	k3xTgNnSc4	všechen
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Slavné	slavný	k2eAgInPc4d1	slavný
"	"	kIx"	"
<g/>
Zlaté	zlatý	k2eAgInPc4d1	zlatý
verše	verš	k1gInPc4	verš
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Carmen	Carmen	k2eAgInSc1d1	Carmen
aureum	aureum	k1gInSc1	aureum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jistě	jistě	k6eAd1	jistě
pozdější	pozdní	k2eAgMnPc1d2	pozdější
<g/>
,	,	kIx,	,
možná	možná	k9	možná
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
části	část	k1gFnSc2	část
této	tento	k3xDgFnSc2	tento
básně	báseň	k1gFnSc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
bohů	bůh	k1gMnPc2	bůh
i	i	k8xC	i
zemřelých	zemřelý	k2eAgMnPc2d1	zemřelý
předků	předek	k1gMnPc2	předek
<g/>
,	,	kIx,	,
k	k	k7c3	k
věrnému	věrný	k2eAgNnSc3d1	věrné
přátelství	přátelství	k1gNnSc3	přátelství
a	a	k8xC	a
slibuje	slibovat	k5eAaImIp3nS	slibovat
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
jeho	jeho	k3xOp3gNnSc3	jeho
současník	současník	k1gMnSc1	současník
Hérakleitos	Hérakleitos	k1gMnSc1	Hérakleitos
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
kdo	kdo	k3yInSc1	kdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
vytýká	vytýkat	k5eAaImIp3nS	vytýkat
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
"	"	kIx"	"
<g/>
mnohoučenost	mnohoučenost	k1gFnSc1	mnohoučenost
<g/>
"	"	kIx"	"
a	a	k8xC	a
úskočná	úskočný	k2eAgNnPc4d1	úskočné
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
rozporným	rozporný	k2eAgMnPc3d1	rozporný
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
pozdním	pozdní	k2eAgFnPc3d1	pozdní
zprávám	zpráva	k1gFnPc3	zpráva
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Pythagorovo	Pythagorův	k2eAgNnSc1d1	Pythagorovo
učení	učení	k1gNnSc1	učení
záhadou	záhada	k1gFnSc7	záhada
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
náboženské	náboženský	k2eAgNnSc4d1	náboženské
učení	učení	k1gNnSc4	učení
i	i	k8xC	i
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
možná	možná	k9	možná
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
mystiku	mystika	k1gFnSc4	mystika
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Badatelé	badatel	k1gMnPc1	badatel
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
táborů	tábor	k1gInPc2	tábor
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
víc	hodně	k6eAd2	hodně
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
<g/>
.	.	kIx.	.
</s>
<s>
W.	W.	kA	W.
Burkert	Burkert	k1gInSc1	Burkert
a	a	k8xC	a
E.	E.	kA	E.
R.	R.	kA	R.
Dodds	Dodds	k1gInSc4	Dodds
přirovnávají	přirovnávat	k5eAaImIp3nP	přirovnávat
Pythagora	Pythagoras	k1gMnSc4	Pythagoras
k	k	k7c3	k
"	"	kIx"	"
<g/>
šamanovi	šaman	k1gMnSc6	šaman
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc7	žák
posvátnou	posvátný	k2eAgFnSc7d1	posvátná
autoritu	autorita	k1gFnSc4	autorita
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
W.	W.	kA	W.
Jaeger	Jaeger	k1gInSc1	Jaeger
a	a	k8xC	a
L.	L.	kA	L.
Zhmud	Zhmud	k1gInSc1	Zhmud
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vidí	vidět	k5eAaImIp3nS	vidět
především	především	k9	především
učence	učenec	k1gMnPc4	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
první	první	k4xOgNnSc4	první
mínění	mínění	k1gNnSc4	mínění
svědčí	svědčit	k5eAaImIp3nS	svědčit
zachované	zachovaný	k2eAgInPc4d1	zachovaný
zlomky	zlomek	k1gInPc4	zlomek
a	a	k8xC	a
zejména	zejména	k9	zejména
zázraky	zázrak	k1gInPc1	zázrak
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
legendy	legenda	k1gFnPc1	legenda
hojně	hojně	k6eAd1	hojně
připisují	připisovat	k5eAaImIp3nP	připisovat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
druhé	druhý	k4xOgInPc4	druhý
objevy	objev	k1gInPc4	objev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
Pythagorovi	Pythagorův	k2eAgMnPc1d1	Pythagorův
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
a	a	k8xC	a
Aristoxenos	Aristoxenos	k1gInSc1	Aristoxenos
mu	on	k3xPp3gMnSc3	on
připisují	připisovat	k5eAaImIp3nP	připisovat
velké	velký	k2eAgFnPc4d1	velká
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
řeckou	řecký	k2eAgFnSc4d1	řecká
matematiku	matematika	k1gFnSc4	matematika
i	i	k8xC	i
astronomii	astronomie	k1gFnSc4	astronomie
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
obojí	oboj	k1gFnSc7	oboj
později	pozdě	k6eAd2	pozdě
pěstovalo	pěstovat	k5eAaImAgNnS	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
zlomků	zlomek	k1gInPc2	zlomek
a	a	k8xC	a
citátů	citát	k1gInPc2	citát
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
Pythagora	Pythagoras	k1gMnSc4	Pythagoras
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
učení	učení	k1gNnSc1	učení
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
jiných	jiný	k2eAgMnPc2d1	jiný
tvorů	tvor	k1gMnPc2	tvor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
vězněna	věznit	k5eAaImNgFnS	věznit
jako	jako	k8xC	jako
v	v	k7c6	v
hrobě	hrob	k1gInSc6	hrob
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
hrob	hrob	k1gInSc1	hrob
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
sóma	sóma	k1gFnSc1	sóma
-	-	kIx~	-
séma	séma	k1gNnSc1	séma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
a	a	k8xC	a
posláním	poslání	k1gNnSc7	poslání
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
duši	duše	k1gFnSc4	duše
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
připravovat	připravovat	k5eAaImF	připravovat
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
prý	prý	k9	prý
také	také	k9	také
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
s	s	k7c7	s
diváky	divák	k1gMnPc7	divák
na	na	k7c6	na
olympijské	olympijský	k2eAgFnSc6d1	olympijská
slavnosti	slavnost	k1gFnSc6	slavnost
<g/>
:	:	kIx,	:
jedni	jeden	k4xCgMnPc1	jeden
tam	tam	k6eAd1	tam
hledají	hledat	k5eAaImIp3nP	hledat
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
druzí	druhý	k4xOgMnPc1	druhý
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
třetí	třetí	k4xOgMnPc1	třetí
-	-	kIx~	-
filosofové	filosof	k1gMnPc1	filosof
-	-	kIx~	-
jen	jen	k6eAd1	jen
přihlížejí	přihlížet	k5eAaImIp3nP	přihlížet
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
i	i	k9	i
Platónovo	Platónův	k2eAgNnSc1d1	Platónovo
a	a	k8xC	a
Aristotelovo	Aristotelův	k2eAgNnSc1d1	Aristotelovo
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
teoretický	teoretický	k2eAgInSc4d1	teoretický
postoj	postoj	k1gInSc4	postoj
ke	k	k7c3	k
světu	svět	k1gInSc3	svět
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
(	(	kIx(	(
<g/>
řecké	řecký	k2eAgNnSc1d1	řecké
theóros	theórosa	k1gFnPc2	theórosa
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
divák	divák	k1gMnSc1	divák
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
astronomického	astronomický	k2eAgInSc2d1	astronomický
zájmu	zájem	k1gInSc2	zájem
pythagorejské	pythagorejský	k2eAgFnSc2d1	pythagorejská
školy	škola	k1gFnSc2	škola
vyplynul	vyplynout	k5eAaPmAgMnS	vyplynout
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
helénismu	helénismus	k1gInSc2	helénismus
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
astrologický	astrologický	k2eAgInSc1d1	astrologický
fatalismus	fatalismus	k1gInSc1	fatalismus
<g/>
"	"	kIx"	"
-	-	kIx~	-
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
nezvratném	zvratný	k2eNgInSc6d1	nezvratný
osudu	osud	k1gInSc6	osud
<g/>
,	,	kIx,	,
určeném	určený	k2eAgInSc6d1	určený
polohou	poloha	k1gFnSc7	poloha
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
pak	pak	k6eAd1	pak
i	i	k9	i
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
"	"	kIx"	"
<g/>
věčném	věčný	k2eAgInSc6d1	věčný
návratu	návrat	k1gInSc6	návrat
<g/>
"	"	kIx"	"
téhož	týž	k3xTgNnSc2	týž
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
Velkého	velký	k2eAgInSc2d1	velký
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
převzal	převzít	k5eAaPmAgMnS	převzít
Nietzsche	Nietzsch	k1gMnSc4	Nietzsch
<g/>
.	.	kIx.	.
</s>
<s>
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
"	"	kIx"	"
<g/>
škola	škola	k1gFnSc1	škola
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
dnešních	dnešní	k2eAgFnPc2d1	dnešní
představ	představa	k1gFnPc2	představa
asi	asi	k9	asi
podobala	podobat	k5eAaImAgFnS	podobat
spíše	spíše	k9	spíše
klášteru	klášter	k1gInSc3	klášter
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
asketickým	asketický	k2eAgInSc7d1	asketický
životem	život	k1gInSc7	život
a	a	k8xC	a
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
<g/>
,	,	kIx,	,
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
přísnou	přísný	k2eAgFnSc4d1	přísná
disciplínu	disciplína	k1gFnSc4	disciplína
se	s	k7c7	s
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
čistotu	čistota	k1gFnSc4	čistota
<g/>
,	,	kIx,	,
účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
však	však	k9	však
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pythagorovi	Pythagoras	k1gMnSc3	Pythagoras
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
zavedení	zavedení	k1gNnSc1	zavedení
pojmu	pojem	k1gInSc2	pojem
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
:	:	kIx,	:
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
žáci	žák	k1gMnPc1	žák
nazývali	nazývat	k5eAaImAgMnP	nazývat
sofos	sofos	k1gInSc4	sofos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mudrc	mudrc	k1gMnSc1	mudrc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
mu	on	k3xPp3gMnSc3	on
raději	rád	k6eAd2	rád
říkají	říkat	k5eAaImIp3nP	říkat
"	"	kIx"	"
<g/>
milovník	milovník	k1gMnSc1	milovník
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
filosofos	filosofos	k1gInSc1	filosofos
z	z	k7c2	z
filein	fileina	k1gFnPc2	fileina
-	-	kIx~	-
"	"	kIx"	"
<g/>
milovat	milovat	k5eAaImF	milovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
sofos	sofos	k1gMnSc1	sofos
-	-	kIx~	-
"	"	kIx"	"
<g/>
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
začali	začít	k5eAaPmAgMnP	začít
říkat	říkat	k5eAaImF	říkat
filosofové	filosof	k1gMnPc1	filosof
<g/>
.	.	kIx.	.
</s>
<s>
Připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
výraz	výraz	k1gInSc1	výraz
kosmos	kosmos	k1gInSc1	kosmos
(	(	kIx(	(
<g/>
od	od	k7c2	od
kosmeó	kosmeó	k?	kosmeó
<g/>
,	,	kIx,	,
zdobit	zdobit	k5eAaImF	zdobit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prý	prý	k9	prý
ve	v	k7c6	v
Vesmíru	vesmír	k1gInSc6	vesmír
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
jeho	jeho	k3xOp3gInSc4	jeho
úžasný	úžasný	k2eAgInSc4d1	úžasný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
výklad	výklad	k1gInSc1	výklad
u	u	k7c2	u
Diogéna	Diogén	k1gMnSc2	Diogén
Laertia	Laertius	k1gMnSc2	Laertius
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
počátek	počátek	k1gInSc4	počátek
Vesmíru	vesmír	k1gInSc2	vesmír
od	od	k7c2	od
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mužského	mužský	k2eAgInSc2d1	mužský
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Jednoho	jeden	k4xCgInSc2	jeden
a	a	k8xC	a
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ženské	ženský	k2eAgFnPc4d1	ženská
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
neohraničené	ohraničený	k2eNgFnPc1d1	neohraničená
dvojice	dvojice	k1gFnPc1	dvojice
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
učení	učení	k1gNnSc6	učení
o	o	k7c6	o
Jin	Jin	k1gFnSc6	Jin
a	a	k8xC	a
Jang	Jang	k1gMnSc1	Jang
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
protiklad	protiklad	k1gInSc4	protiklad
lichých	lichý	k2eAgNnPc2d1	liché
a	a	k8xC	a
sudých	sudý	k2eAgNnPc2d1	sudé
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
buduje	budovat	k5eAaImIp3nS	budovat
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
stavba	stavba	k1gFnSc1	stavba
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
poměry	poměr	k1gInPc4	poměr
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnPc4	číslo
a	a	k8xC	a
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Nejdokonalejší	dokonalý	k2eAgInPc4d3	nejdokonalejší
geometrické	geometrický	k2eAgInPc4d1	geometrický
obrazce	obrazec	k1gInPc4	obrazec
jsou	být	k5eAaImIp3nP	být
koule	koule	k1gFnPc1	koule
a	a	k8xC	a
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
čtverec	čtverec	k1gInSc1	čtverec
jakožto	jakožto	k8xS	jakožto
symbol	symbol	k1gInSc1	symbol
čtyř	čtyři	k4xCgInPc2	čtyři
živlů	živel	k1gInPc2	živel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pythagorejské	pythagorejský	k2eAgInPc4d1	pythagorejský
pojmy	pojem	k1gInPc4	pojem
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
"	"	kIx"	"
<g/>
čtveřina	čtveřina	k1gFnSc1	čtveřina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tetraktys	tetraktys	k1gInSc1	tetraktys
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
posloupnost	posloupnost	k1gFnSc4	posloupnost
čísel	číslo	k1gNnPc2	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
součet	součet	k1gInSc1	součet
je	být	k5eAaImIp3nS	být
deset	deset	k4xCc4	deset
<g/>
.	.	kIx.	.
</s>
<s>
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc1	jeho
škola	škola	k1gFnSc1	škola
objevili	objevit	k5eAaPmAgMnP	objevit
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
délkou	délka	k1gFnSc7	délka
struny	struna	k1gFnPc1	struna
a	a	k8xC	a
tóny	tón	k1gInPc1	tón
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
:	:	kIx,	:
poloviční	poloviční	k2eAgFnSc1d1	poloviční
struna	struna	k1gFnSc1	struna
zní	znět	k5eAaImIp3nS	znět
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
výš	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
dvoutřetinová	dvoutřetinový	k2eAgFnSc1d1	dvoutřetinová
o	o	k7c4	o
kvintu	kvinta	k1gFnSc4	kvinta
atd.	atd.	kA	atd.
Na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
diatonická	diatonický	k2eAgFnSc1d1	diatonická
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
pythagorejské	pythagorejský	k2eAgNnSc1d1	pythagorejské
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
i	i	k9	i
představa	představa	k1gFnSc1	představa
harmonie	harmonie	k1gFnSc2	harmonie
sfér	sféra	k1gFnPc2	sféra
<g/>
:	:	kIx,	:
průměry	průměr	k1gInPc1	průměr
planetárních	planetární	k2eAgFnPc2d1	planetární
sfér	sféra	k1gFnPc2	sféra
(	(	kIx(	(
<g/>
koulí	koule	k1gFnPc2	koule
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
poměru	poměr	k1gInSc6	poměr
a	a	k8xC	a
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pohybu	pohyb	k1gInSc6	pohyb
vydávají	vydávat	k5eAaPmIp3nP	vydávat
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
neslyšitelný	slyšitelný	k2eNgInSc1d1	neslyšitelný
harmonický	harmonický	k2eAgInSc4d1	harmonický
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
význam	význam	k1gInSc4	význam
měla	mít	k5eAaImAgFnS	mít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
součet	součet	k1gInSc1	součet
obsahů	obsah	k1gInPc2	obsah
čtverců	čtverec	k1gInPc2	čtverec
nad	nad	k7c7	nad
oběma	dva	k4xCgFnPc7	dva
odvěsnami	odvěsna	k1gFnPc7	odvěsna
pravoúhlého	pravoúhlý	k2eAgInSc2d1	pravoúhlý
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
obsahu	obsah	k1gInSc3	obsah
čtverce	čtverec	k1gInSc2	čtverec
nad	nad	k7c7	nad
přeponou	přepona	k1gFnSc7	přepona
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
kultury	kultura	k1gFnPc1	kultura
věděly	vědět	k5eAaImAgFnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
a	a	k8xC	a
Číňané	Číňan	k1gMnPc1	Číňan
to	ten	k3xDgNnSc4	ten
dovedli	dovést	k5eAaPmAgMnP	dovést
i	i	k9	i
geometricky	geometricky	k6eAd1	geometricky
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
důkaz	důkaz	k1gInSc1	důkaz
věty	věta	k1gFnSc2	věta
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
připisoval	připisovat	k5eAaImAgInS	připisovat
Egypťanům	Egypťan	k1gMnPc3	Egypťan
či	či	k8xC	či
Babylóňanům	Babylóňan	k1gMnPc3	Babylóňan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
měl	mít	k5eAaImAgMnS	mít
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
seznámit	seznámit	k5eAaPmF	seznámit
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgMnPc1d1	moderní
badatelé	badatel	k1gMnPc1	badatel
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
hlavně	hlavně	k9	hlavně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochybují	pochybovat	k5eAaImIp3nP	pochybovat
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
domluvy	domluva	k1gFnSc2	domluva
a	a	k8xC	a
jazykových	jazykový	k2eAgFnPc6d1	jazyková
znalostech	znalost	k1gFnPc6	znalost
obou	dva	k4xCgInPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
matematika	matematika	k1gFnSc1	matematika
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
nalezla	naleznout	k5eAaPmAgFnS	naleznout
neobyčejně	obyčejně	k6eNd1	obyčejně
důmyslné	důmyslný	k2eAgInPc4d1	důmyslný
obecné	obecný	k2eAgInPc4d1	obecný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
Eukleidés	Eukleidés	k1gInSc4	Eukleidés
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vedlejším	vedlejší	k2eAgInSc6d1	vedlejší
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgInSc1d1	jiný
<g/>
,	,	kIx,	,
jednodušší	jednoduchý	k2eAgInSc1d2	jednodušší
geometrický	geometrický	k2eAgInSc1d1	geometrický
důkaz	důkaz	k1gInSc1	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
Pythagorovu	Pythagorův	k2eAgMnSc3d1	Pythagorův
žáku	žák	k1gMnSc3	žák
Hippassovi	Hippass	k1gMnSc3	Hippass
z	z	k7c2	z
Metapontu	Metapont	k1gInSc2	Metapont
připisuje	připisovat	k5eAaImIp3nS	připisovat
i	i	k9	i
objev	objev	k1gInSc1	objev
nesouměřitelných	souměřitelný	k2eNgNnPc2d1	nesouměřitelné
čili	čili	k8xC	čili
iracionálních	iracionální	k2eAgNnPc2d1	iracionální
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc4	jenž
nelze	lze	k6eNd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
zlomkem	zlomek	k1gInSc7	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
jej	on	k3xPp3gNnSc4	on
snad	snad	k9	snad
na	na	k7c6	na
příkladě	příklad	k1gInSc6	příklad
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
čtverce	čtverec	k1gInSc2	čtverec
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
na	na	k7c6	na
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
pětiúhelníku	pětiúhelník	k1gInSc6	pětiúhelník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
strany	strana	k1gFnPc1	strana
rovněž	rovněž	k9	rovněž
nemají	mít	k5eNaImIp3nP	mít
racionální	racionální	k2eAgFnSc2d1	racionální
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Hippassovi	Hippass	k1gMnSc6	Hippass
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
Pythagorovy	Pythagorův	k2eAgFnSc2d1	Pythagorova
školy	škola	k1gFnSc2	škola
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
<g/>
,	,	kIx,	,
prý	prý	k9	prý
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
vyzrazení	vyzrazení	k1gNnSc4	vyzrazení
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starověkých	starověký	k2eAgNnPc2d1	starověké
svědectví	svědectví	k1gNnSc2	svědectví
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
těžkou	těžký	k2eAgFnSc4d1	těžká
krizi	krize	k1gFnSc4	krize
pythagorejství	pythagorejství	k1gNnSc2	pythagorejství
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
otřásl	otřást	k5eAaPmAgMnS	otřást
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
racionální	racionální	k2eAgFnSc4d1	racionální
povahu	povaha	k1gFnSc4	povaha
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
důsledky	důsledek	k1gInPc7	důsledek
se	se	k3xPyFc4	se
vypořádával	vypořádávat	k5eAaImAgInS	vypořádávat
i	i	k9	i
Platón	platón	k1gInSc1	platón
<g/>
.	.	kIx.	.
</s>
<s>
Pythagorejská	pythagorejský	k2eAgFnSc1d1	pythagorejská
škola	škola	k1gFnSc1	škola
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
a	a	k8xC	a
těšila	těšit	k5eAaImAgFnS	těšit
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Hérakleita	Hérakleita	k1gFnSc1	Hérakleita
<g/>
)	)	kIx)	)
velké	velký	k2eAgFnSc3d1	velká
úctě	úcta	k1gFnSc3	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
se	se	k3xPyFc4	se
s	s	k7c7	s
pythagorejskými	pythagorejský	k2eAgNnPc7d1	pythagorejské
tématy	téma	k1gNnPc7	téma
zabýval	zabývat	k5eAaImAgInS	zabývat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Timaiu	Timaium	k1gNnSc6	Timaium
a	a	k8xC	a
ve	v	k7c6	v
Faidónu	Faidón	k1gInSc6	Faidón
<g/>
.	.	kIx.	.
</s>
<s>
Uctivě	uctivě	k6eAd1	uctivě
o	o	k7c6	o
Pythagorovi	Pythagoras	k1gMnSc6	Pythagoras
píše	psát	k5eAaImIp3nS	psát
Cicero	Cicero	k1gMnSc1	Cicero
i	i	k8xC	i
Ovidius	Ovidius	k1gMnSc1	Ovidius
<g/>
,	,	kIx,	,
Kléméns	Kléméns	k1gInSc1	Kléméns
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
i	i	k8xC	i
Hippolyt	Hippolyt	k1gInSc1	Hippolyt
Římský	římský	k2eAgInSc1d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pythagorejce	pythagorejec	k1gMnPc4	pythagorejec
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
Pico	Pico	k1gMnSc1	Pico
della	della	k1gMnSc1	della
Mirandola	Mirandola	k1gFnSc1	Mirandola
<g/>
,	,	kIx,	,
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
i	i	k8xC	i
Giambattista	Giambattista	k1gMnSc1	Giambattista
Vico	Vico	k1gMnSc1	Vico
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Pythagorovi	Pythagoras	k1gMnSc6	Pythagoras
je	být	k5eAaImIp3nS	být
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
kráter	kráter	k1gInSc1	kráter
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
planetka	planetka	k1gFnSc1	planetka
6143	[number]	k4	6143
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Pavel	Pavel	k1gMnSc1	Pavel
Hobl	Hobl	k1gMnSc1	Hobl
natočil	natočit	k5eAaBmAgMnS	natočit
film	film	k1gInSc4	film
30	[number]	k4	30
panen	panna	k1gFnPc2	panna
a	a	k8xC	a
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
<g/>
.	.	kIx.	.
</s>
<s>
Matematická	matematický	k2eAgFnSc1d1	matematická
soutěž	soutěž	k1gFnSc1	soutěž
Pythagoriáda	Pythagoriáda	k1gFnSc1	Pythagoriáda
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
žákům	žák	k1gMnPc3	žák
páté	pátá	k1gFnSc2	pátá
<g/>
,	,	kIx,	,
šesté	šestý	k4xOgInPc4	šestý
<g/>
,	,	kIx,	,
sedmé	sedmý	k4xOgInPc4	sedmý
<g/>
,	,	kIx,	,
osmé	osmý	k4xOgNnSc4	osmý
a	a	k8xC	a
příslušným	příslušný	k2eAgInPc3d1	příslušný
ročníkům	ročník	k1gInPc3	ročník
víceletých	víceletý	k2eAgFnPc2d1	víceletá
gymnázií	gymnázium	k1gNnPc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
/	/	kIx~	/
<g/>
1979	[number]	k4	1979
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
pedagogický	pedagogický	k2eAgInSc1d1	pedagogický
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
školní	školní	k2eAgInSc4d1	školní
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgInSc4d1	okresní
a	a	k8xC	a
krajské	krajský	k2eAgNnSc4d1	krajské
kolo	kolo	k1gNnSc4	kolo
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
