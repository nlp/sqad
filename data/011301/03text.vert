<p>
<s>
Vinson	Vinson	k1gMnSc1	Vinson
Massif	Massif	k1gMnSc1	Massif
(	(	kIx(	(
<g/>
také	také	k9	také
Mount	Mount	k1gMnSc1	Mount
Vinson	Vinson	k1gMnSc1	Vinson
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
1200	[number]	k4	1200
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
poledníku	poledník	k1gInSc6	poledník
jako	jako	k9	jako
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
4892	[number]	k4	4892
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
místo	místo	k1gNnSc4	místo
Mount	Mount	k1gMnSc1	Mount
Vinson	Vinson	k1gMnSc1	Vinson
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
označené	označený	k2eAgFnSc2d1	označená
US-ACAN	US-ACAN	k1gFnSc2	US-ACAN
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Masiv	masiv	k1gInSc1	masiv
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
21	[number]	k4	21
kilometrů	kilometr	k1gInPc2	kilometr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
13	[number]	k4	13
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
masiv	masiv	k1gInSc1	masiv
zakončen	zakončen	k2eAgInSc1d1	zakončen
vrcholem	vrchol	k1gInSc7	vrchol
Mount	Mount	k1gMnSc1	Mount
Craddock	Craddock	k1gMnSc1	Craddock
(	(	kIx(	(
<g/>
4368	[number]	k4	4368
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
však	však	k9	však
Mount	Mount	k1gMnSc1	Mount
Rutford	Rutford	k1gMnSc1	Rutford
(	(	kIx(	(
<g/>
4477	[number]	k4	4477
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vinson	Vinson	k1gMnSc1	Vinson
Massif	Massif	k1gMnSc1	Massif
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
hřebeni	hřeben	k1gInSc6	hřeben
Sentinel	sentinel	k1gInSc1	sentinel
pohoří	pohoří	k1gNnSc2	pohoří
Ellsworth	Ellswortha	k1gFnPc2	Ellswortha
<g/>
.	.	kIx.	.
</s>
<s>
Hřeben	hřeben	k1gInSc1	hřeben
Sentinel	sentinel	k1gInSc1	sentinel
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
zpozorován	zpozorován	k2eAgMnSc1d1	zpozorován
a	a	k8xC	a
fotografován	fotografován	k2eAgMnSc1d1	fotografován
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
amerického	americký	k2eAgMnSc2d1	americký
kongresmana	kongresman	k1gMnSc2	kongresman
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Georgie	Georgie	k1gFnSc2	Georgie
Carla	Carl	k1gMnSc2	Carl
G.	G.	kA	G.
Vinsona	Vinson	k1gMnSc2	Vinson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
významně	významně	k6eAd1	významně
podporoval	podporovat	k5eAaImAgMnS	podporovat
průzkum	průzkum	k1gInSc4	průzkum
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Objeven	objevit	k5eAaPmNgInS	objevit
byl	být	k5eAaImAgInS	být
náhodou	náhodou	k6eAd1	náhodou
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gNnSc4	on
spatřila	spatřit	k5eAaPmAgFnS	spatřit
posádka	posádka	k1gFnSc1	posádka
letadla	letadlo	k1gNnSc2	letadlo
US	US	kA	US
Navy	Navy	k?	Navy
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Byrd	Byrd	k1gMnSc1	Byrd
Station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výška	výška	k1gFnSc1	výška
==	==	k?	==
</s>
</p>
<p>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
měření	měření	k1gNnSc1	měření
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
5140	[number]	k4	5140
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
tým	tým	k1gInSc4	tým
amerických	americký	k2eAgMnPc2d1	americký
horolezců	horolezec	k1gMnPc2	horolezec
doplněný	doplněný	k2eAgInSc4d1	doplněný
dvěma	dva	k4xCgFnPc7	dva
německými	německý	k2eAgMnPc7d1	německý
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
sovětským	sovětský	k2eAgMnSc7d1	sovětský
horolezcem	horolezec	k1gMnSc7	horolezec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
zde	zde	k6eAd1	zde
červenou	červený	k2eAgFnSc4d1	červená
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
lyžařskou	lyžařský	k2eAgFnSc4d1	lyžařská
tyčku	tyčka	k1gFnSc4	tyčka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pomohla	pomoct	k5eAaPmAgFnS	pomoct
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
přesnějšího	přesný	k2eAgNnSc2d2	přesnější
měření	měření	k1gNnSc2	měření
4897	[number]	k4	4897
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
tým	tým	k1gInSc1	tým
sponzorovaný	sponzorovaný	k2eAgInSc1d1	sponzorovaný
seriálem	seriál	k1gInSc7	seriál
NOVA	novum	k1gNnSc2	novum
(	(	kIx(	(
<g/>
produkce	produkce	k1gFnSc1	produkce
<g />
.	.	kIx.	.
</s>
<s>
WGBH	WGBH	kA	WGBH
Boston	Boston	k1gInSc1	Boston
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
vystoupal	vystoupat	k5eAaPmAgMnS	vystoupat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
jižní	jižní	k2eAgFnSc7d1	jižní
stěnou	stěna	k1gFnSc7	stěna
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
GPS	GPS	kA	GPS
zjistil	zjistit	k5eAaPmAgInS	zjistit
novou	nový	k2eAgFnSc4d1	nová
výšku	výška	k1gFnSc4	výška
4900,3	[number]	k4	4900,3
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
Současná	současný	k2eAgFnSc1d1	současná
výška	výška	k1gFnSc1	výška
(	(	kIx(	(
<g/>
4892	[number]	k4	4892
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
měření	měření	k1gNnSc2	měření
pomocí	pomoc	k1gFnPc2	pomoc
GPS	GPS	kA	GPS
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
provedl	provést	k5eAaPmAgInS	provést
tým	tým	k1gInSc1	tým
Omega	omega	k1gNnSc1	omega
Foundation	Foundation	k1gInSc1	Foundation
Australana	Australan	k1gMnSc2	Australan
Damiena	Damien	k1gMnSc2	Damien
Gildea	Gildeus	k1gMnSc2	Gildeus
a	a	k8xC	a
Rodriga	Rodrig	k1gMnSc2	Rodrig
Fica	Fico	k1gMnSc2	Fico
a	a	k8xC	a
Camila	Camila	k1gFnSc1	Camila
Rada	rada	k1gFnSc1	rada
z	z	k7c2	z
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
umístila	umístit	k5eAaPmAgFnS	umístit
Omega	omega	k1gFnSc1	omega
Foundation	Foundation	k1gInSc4	Foundation
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
GPS	GPS	kA	GPS
přijímač	přijímač	k1gInSc1	přijímač
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nějž	jenž	k3xRgNnSc2	jenž
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
získána	získán	k2eAgNnPc4d1	získáno
přesnější	přesný	k2eAgNnPc4d2	přesnější
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vinson	Vinson	k1gInSc1	Vinson
Massif	Massif	k1gInSc4	Massif
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vinson	Vinson	k1gMnSc1	Vinson
Massif	Massif	k1gMnSc1	Massif
na	na	k7c6	na
TierraWiki	TierraWik	k1gFnSc6	TierraWik
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
