<s>
Karlstad	Karlstad	k6eAd1	Karlstad
je	být	k5eAaImIp3nS	být
švédské	švédský	k2eAgNnSc1d1	švédské
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
provincie	provincie	k1gFnSc2	provincie
Värmland	Värmlanda	k1gFnPc2	Värmlanda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
největšího	veliký	k2eAgNnSc2d3	veliký
švédského	švédský	k2eAgNnSc2d1	švédské
jezera	jezero	k1gNnSc2	jezero
Vänern	Vänerna	k1gFnPc2	Vänerna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
mělo	mít	k5eAaImAgNnS	mít
61	[number]	k4	61
685	[number]	k4	685
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Karlstad	Karlstad	k1gInSc1	Karlstad
je	být	k5eAaImIp3nS	být
pokládáno	pokládán	k2eAgNnSc4d1	pokládáno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejslunečnějších	sluneční	k2eAgNnPc2d3	nejslunečnější
měst	město	k1gNnPc2	město
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Karlstad	Karlstad	k1gInSc1	Karlstad
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
několika	několik	k4yIc2	několik
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
také	také	k9	také
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
(	(	kIx(	(
<g/>
Karlstads	Karlstads	k1gInSc1	Karlstads
universitet	universitet	k1gInSc1	universitet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
města	město	k1gNnSc2	město
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnPc2	období
Vikingů	Viking	k1gMnPc2	Viking
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1000	[number]	k4	1000
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Statut	statut	k1gInSc1	statut
města	město	k1gNnSc2	město
udělil	udělit	k5eAaPmAgInS	udělit
Karlstadu	Karlstada	k1gFnSc4	Karlstada
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1584	[number]	k4	1584
švédský	švédský	k2eAgMnSc1d1	švédský
vévoda	vévoda	k1gMnSc1	vévoda
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
korunován	korunovat	k5eAaBmNgMnS	korunovat
králem	král	k1gMnSc7	král
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Karel	Karel	k1gMnSc1	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
od	od	k7c2	od
krále	král	k1gMnSc2	král
–	–	k?	–
Karlstad	Karlstad	k1gInSc1	Karlstad
znamená	znamenat	k5eAaImIp3nS	znamenat
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
také	také	k9	také
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
Karlstad	Karlstad	k1gInSc1	Karlstad
regionálním	regionální	k2eAgNnSc7d1	regionální
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
a	a	k8xC	a
přidělil	přidělit	k5eAaPmAgInS	přidělit
mu	on	k3xPp3gMnSc3	on
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
pozemků	pozemek	k1gInPc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
si	se	k3xPyFc3	se
vévoda	vévoda	k1gMnSc1	vévoda
postavil	postavit	k5eAaPmAgMnS	postavit
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Kungsgå	Kungsgå	k1gFnSc1	Kungsgå
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
letech	let	k1gInPc6	let
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
postavil	postavit	k5eAaPmAgMnS	postavit
Christian	Christian	k1gMnSc1	Christian
Haller	Haller	k1gMnSc1	Haller
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Karlstad	Karlstad	k6eAd1	Karlstad
se	se	k3xPyFc4	se
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
moderních	moderní	k2eAgFnPc2d1	moderní
dějin	dějiny	k1gFnPc2	dějiny
celého	celý	k2eAgNnSc2d1	celé
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1809	[number]	k4	1809
část	část	k1gFnSc1	část
švédské	švédský	k2eAgFnSc2d1	švédská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
velel	velet	k5eAaImAgMnS	velet
podplukovník	podplukovník	k1gMnSc1	podplukovník
Georg	Georg	k1gMnSc1	Georg
Adlersparre	Adlersparr	k1gInSc5	Adlersparr
<g/>
,	,	kIx,	,
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Karlstad	Karlstad	k1gInSc4	Karlstad
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
podplukovník	podplukovník	k1gMnSc1	podplukovník
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
úmysl	úmysl	k1gInSc1	úmysl
svrhnout	svrhnout	k5eAaPmF	svrhnout
švédského	švédský	k2eAgMnSc4d1	švédský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
Gustav	Gustav	k1gMnSc1	Gustav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
armáda	armáda	k1gFnSc1	armáda
poté	poté	k6eAd1	poté
zahájila	zahájit	k5eAaPmAgFnS	zahájit
pochod	pochod	k1gInSc4	pochod
na	na	k7c4	na
Stockholm	Stockholm	k1gInSc4	Stockholm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
krále	král	k1gMnSc2	král
zajala	zajmout	k5eAaPmAgFnS	zajmout
a	a	k8xC	a
uvěznila	uvěznit	k5eAaPmAgFnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Karlstad	Karlstad	k6eAd1	Karlstad
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
čtyři	čtyři	k4xCgInPc4	čtyři
velké	velký	k2eAgInPc4d1	velký
požáry	požár	k1gInPc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posledním	poslední	k2eAgInSc6d1	poslední
požáru	požár	k1gInSc6	požár
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1865	[number]	k4	1865
bylo	být	k5eAaImAgNnS	být
–	–	k?	–
kromě	kromě	k7c2	kromě
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
několika	několik	k4yIc2	několik
domů	dům	k1gInPc2	dům
–	–	k?	–
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Karlstad	Karlstad	k1gInSc1	Karlstad
byl	být	k5eAaImAgInS	být
potom	potom	k6eAd1	potom
přestavěn	přestavět	k5eAaPmNgInS	přestavět
podle	podle	k7c2	podle
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
stylu	styl	k1gInSc6	styl
se	s	k7c7	s
širokými	široký	k2eAgFnPc7d1	široká
ulicemi	ulice	k1gFnPc7	ulice
<g/>
,	,	kIx,	,
obklopenými	obklopený	k2eAgInPc7d1	obklopený
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Karlstadu	Karlstad	k1gInSc6	Karlstad
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
unie	unie	k1gFnSc2	unie
mezi	mezi	k7c7	mezi
Norskem	Norsko	k1gNnSc7	Norsko
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karlstadu	Karlstad	k1gInSc6	Karlstad
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
sportem	sport	k1gInSc7	sport
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
hokejovým	hokejový	k2eAgInSc7d1	hokejový
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
Färjestads	Färjestads	k1gInSc1	Färjestads
BK	BK	kA	BK
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
švédské	švédský	k2eAgFnSc6d1	švédská
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
švédský	švédský	k2eAgInSc1d1	švédský
šampionát	šampionát	k1gInSc1	šampionát
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
mužstvem	mužstvo	k1gNnSc7	mužstvo
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
švédská	švédský	k2eAgFnSc1d1	švédská
Elitserien	Elitserien	k1gInSc1	Elitserien
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hokejový	hokejový	k2eAgInSc4d1	hokejový
tým	tým	k1gInSc4	tým
Färjestads	Färjestadsa	k1gFnPc2	Färjestadsa
BK	BK	kA	BK
hráli	hrát	k5eAaImAgMnP	hrát
i	i	k9	i
známí	známý	k2eAgMnPc1d1	známý
švédští	švédský	k2eAgMnPc1d1	švédský
hokejisté	hokejista	k1gMnPc1	hokejista
Hå	Hå	k1gMnSc1	Hå
Loob	Loob	k1gMnSc1	Loob
a	a	k8xC	a
Jörgen	Jörgen	k1gInSc1	Jörgen
Jönsson	Jönssona	k1gFnPc2	Jönssona
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
hokejové	hokejový	k2eAgInPc1d1	hokejový
kluby	klub	k1gInPc1	klub
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
i	i	k9	i
několik	několik	k4yIc4	několik
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žádný	žádný	k3yNgInSc4	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nehraje	hrát	k5eNaImIp3nS	hrát
ve	v	k7c6	v
švédské	švédský	k2eAgFnSc6d1	švédská
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
provozují	provozovat	k5eAaImIp3nP	provozovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
populární	populární	k2eAgInPc4d1	populární
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Bandy	bandy	k1gNnSc1	bandy
–	–	k?	–
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
míčový	míčový	k2eAgInSc1d1	míčový
sport	sport	k1gInSc1	sport
hraný	hraný	k2eAgInSc1d1	hraný
na	na	k7c6	na
ledové	ledový	k2eAgFnSc6d1	ledová
ploše	plocha	k1gFnSc6	plocha
–	–	k?	–
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
nejvíce	hodně	k6eAd3	hodně
populárním	populární	k2eAgInSc7d1	populární
sportem	sport	k1gInSc7	sport
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
dvou	dva	k4xCgInPc2	dva
historicky	historicky	k6eAd1	historicky
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
klubů	klub	k1gInPc2	klub
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Boltic	Boltice	k1gFnPc2	Boltice
a	a	k8xC	a
IF-Karlstad	IF-Karlstad	k1gInSc1	IF-Karlstad
Göta	Göt	k1gInSc2	Göt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
kluby	klub	k1gInPc1	klub
sloučeny	sloučen	k2eAgInPc1d1	sloučen
do	do	k7c2	do
BS	BS	kA	BS
BolticGöta	BolticGöt	k1gInSc2	BolticGöt
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
klubem	klub	k1gInSc7	klub
Carlstad	Carlstad	k1gInSc1	Carlstad
Crusaders	Crusadersa	k1gFnPc2	Crusadersa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
švédské	švédský	k2eAgFnSc6d1	švédská
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
švédský	švédský	k2eAgInSc1d1	švédský
šampionát	šampionát	k1gInSc1	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
prominentním	prominentní	k2eAgInSc7d1	prominentní
sportovním	sportovní	k2eAgInSc7d1	sportovní
klubem	klub	k1gInSc7	klub
v	v	k7c6	v
Karlstadu	Karlstad	k1gInSc6	Karlstad
je	být	k5eAaImIp3nS	být
OK	oka	k1gFnPc2	oka
Tyr	Tyr	k1gMnSc1	Tyr
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
švédských	švédský	k2eAgInPc2d1	švédský
klubů	klub	k1gInPc2	klub
zaměřených	zaměřený	k2eAgInPc2d1	zaměřený
na	na	k7c4	na
orientační	orientační	k2eAgInSc4d1	orientační
běh	běh	k1gInSc4	běh
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
také	také	k9	také
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
orientačního	orientační	k2eAgInSc2d1	orientační
běhu	běh	k1gInSc2	běh
(	(	kIx(	(
<g/>
IOF	IOF	kA	IOF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
komplex	komplex	k1gInSc1	komplex
Tingvalla	Tingvall	k1gMnSc2	Tingvall
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
umělých	umělý	k2eAgFnPc2d1	umělá
ledových	ledový	k2eAgFnPc2d1	ledová
ploch	plocha	k1gFnPc2	plocha
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
zimního	zimní	k2eAgInSc2d1	zimní
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
sportovního	sportovní	k2eAgInSc2d1	sportovní
komplexu	komplex	k1gInSc2	komplex
Tingvalla	Tingvallo	k1gNnSc2	Tingvallo
je	být	k5eAaImIp3nS	být
též	též	k9	též
atletický	atletický	k2eAgInSc1d1	atletický
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
už	už	k6eAd1	už
desítky	desítka	k1gFnPc1	desítka
let	léto	k1gNnPc2	léto
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
mítink	mítink	k1gInSc1	mítink
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Götagalan	Götagalan	k1gInSc4	Götagalan
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
známých	známý	k2eAgMnPc2d1	známý
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
reprezentantů	reprezentant	k1gMnPc2	reprezentant
na	na	k7c6	na
mítinku	mítink	k1gInSc6	mítink
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
pravidelně	pravidelně	k6eAd1	pravidelně
závodil	závodit	k5eAaImAgMnS	závodit
např.	např.	kA	např.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
.	.	kIx.	.
</s>
<s>
Karlstad	Karlstad	k6eAd1	Karlstad
je	být	k5eAaImIp3nS	být
hostitelem	hostitel	k1gMnSc7	hostitel
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Konalo	konat	k5eAaImAgNnS	konat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
např.	např.	kA	např.
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
IIHF	IIHF	kA	IIHF
v	v	k7c4	v
inline	inlinout	k5eAaPmIp3nS	inlinout
hokeji	hokej	k1gInSc6	hokej
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
české	český	k2eAgNnSc1d1	české
reprezentační	reprezentační	k2eAgNnSc1d1	reprezentační
mužstvo	mužstvo	k1gNnSc1	mužstvo
získalo	získat	k5eAaPmAgNnS	získat
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Karlstad	Karlstad	k6eAd1	Karlstad
je	být	k5eAaImIp3nS	být
také	také	k9	také
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
místem	místo	k1gNnSc7	místo
startu	start	k1gInSc2	start
a	a	k8xC	a
cíle	cíl	k1gInSc2	cíl
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
zkoušek	zkouška	k1gFnPc2	zkouška
na	na	k7c4	na
Švédské	švédský	k2eAgNnSc4d1	švédské
rallye	rallye	k1gNnSc4	rallye
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
90	[number]	k4	90
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
trať	trať	k1gFnSc1	trať
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgFnSc4d1	sportovní
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
cyklisté	cyklista	k1gMnPc1	cyklista
<g/>
,	,	kIx,	,
lyžaři	lyžař	k1gMnPc1	lyžař
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
bruslaři	bruslař	k1gMnPc1	bruslař
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
pilinami	pilina	k1gFnPc7	pilina
upravených	upravený	k2eAgFnPc2d1	upravená
cest	cesta	k1gFnPc2	cesta
pro	pro	k7c4	pro
jogging	jogging	k1gInSc4	jogging
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
osvětlené	osvětlený	k2eAgFnPc1d1	osvětlená
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
těchto	tento	k3xDgFnPc2	tento
cest	cesta	k1gFnPc2	cesta
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
lyžařské	lyžařský	k2eAgFnPc4d1	lyžařská
tratě	trať	k1gFnPc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Stefan	Stefan	k1gMnSc1	Stefan
Holm	Holm	k1gMnSc1	Holm
–	–	k?	–
skokan	skokan	k1gMnSc1	skokan
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
Ulf	Ulf	k1gMnSc1	Ulf
Sterner	Sterner	k1gMnSc1	Sterner
–	–	k?	–
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
Jonas	Jonas	k1gMnSc1	Jonas
Leandersson	Leandersson	k1gMnSc1	Leandersson
–	–	k?	–
orientační	orientační	k2eAgMnSc1d1	orientační
běžec	běžec	k1gMnSc1	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
v	v	k7c6	v
Karlstadu	Karlstad	k1gInSc6	Karlstad
zemřel	zemřít	k5eAaPmAgMnS	zemřít
švédský	švédský	k2eAgMnSc1d1	švédský
botanik	botanik	k1gMnSc1	botanik
Carl	Carl	k1gMnSc1	Carl
Adolf	Adolf	k1gMnSc1	Adolf
Agardh	Agardh	k1gMnSc1	Agardh
<g/>
.	.	kIx.	.
</s>
<s>
Moss	Moss	k1gInSc1	Moss
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
Horsens	Horsensa	k1gFnPc2	Horsensa
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
Blönduós	Blönduós	k1gInSc1	Blönduós
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
Jõ	Jõ	k1gFnSc1	Jõ
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Gaziantep	Gaziantep	k1gInSc1	Gaziantep
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Karlstad	Karlstad	k1gInSc1	Karlstad
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karlstad	Karlstad	k1gInSc4	Karlstad
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Stránky	stránka	k1gFnSc2	stránka
univerzity	univerzita	k1gFnSc2	univerzita
Filmen	Filmen	k1gInSc1	Filmen
om	om	k?	om
Karlstad	Karlstad	k1gInSc1	Karlstad
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
</s>
