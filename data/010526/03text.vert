<p>
<s>
Kauza	kauza	k1gFnSc1	kauza
bytů	byt	k1gInPc2	byt
OKD	OKD	kA	OKD
je	být	k5eAaImIp3nS	být
spor	spor	k1gInSc4	spor
probíhající	probíhající	k2eAgInSc4d1	probíhající
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
financí	finance	k1gFnSc7	finance
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
OKD	OKD	kA	OKD
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
údajně	údajně	k6eAd1	údajně
nebyla	být	k5eNaImAgNnP	být
vypracována	vypracovat	k5eAaPmNgNnP	vypracovat
zcela	zcela	k6eAd1	zcela
seriozně	seriozně	k6eAd1	seriozně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kvůli	kvůli	k7c3	kvůli
postupu	postup	k1gInSc3	postup
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
interpelován	interpelován	k2eAgInSc4d1	interpelován
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
podává	podávat	k5eAaImIp3nS	podávat
Sdružení	sdružení	k1gNnSc2	sdružení
nájemníků	nájemník	k1gMnPc2	nájemník
BYTYOKD	BYTYOKD	kA	BYTYOKD
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
opakovaně	opakovaně	k6eAd1	opakovaně
stížnost	stížnost	k1gFnSc4	stížnost
pro	pro	k7c4	pro
porušení	porušení	k1gNnSc4	porušení
a	a	k8xC	a
neplnění	neplnění	k1gNnSc4	neplnění
privatizační	privatizační	k2eAgFnSc2d1	privatizační
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
mezi	mezi	k7c7	mezi
státem	stát	k1gInSc7	stát
a	a	k8xC	a
OKD	OKD	kA	OKD
a.s.	a.s.	k?	a.s.
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
kupujícího	kupující	k1gMnSc2	kupující
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
mezi	mezi	k7c7	mezi
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
Fond	fond	k1gInSc1	fond
národního	národní	k2eAgInSc2d1	národní
majetku	majetek	k1gInSc2	majetek
<g/>
)	)	kIx)	)
a	a	k8xC	a
společností	společnost	k1gFnSc7	společnost
OKD	OKD	kA	OKD
a.s.	a.s.	k?	a.s.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
stát	stát	k1gInSc4	stát
ji	on	k3xPp3gFnSc4	on
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
stížnostmi	stížnost	k1gFnPc7	stížnost
Sdružení	sdružení	k1gNnSc2	sdružení
nájemníků	nájemník	k1gMnPc2	nájemník
BYTYOKD	BYTYOKD	kA	BYTYOKD
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
několikrát	několikrát	k6eAd1	několikrát
kvůli	kvůli	k7c3	kvůli
liknavému	liknavý	k2eAgInSc3d1	liknavý
postupu	postup	k1gInSc3	postup
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
interpelován	interpelován	k2eAgMnSc1d1	interpelován
a	a	k8xC	a
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
odpovědí	odpověď	k1gFnPc2	odpověď
na	na	k7c4	na
písemnou	písemný	k2eAgFnSc4d1	písemná
interpelaci	interpelace	k1gFnSc4	interpelace
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
říká	říkat	k5eAaImIp3nS	říkat
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g />
.	.	kIx.	.
</s>
<s>
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
změnilo	změnit	k5eAaPmAgNnS	změnit
svoji	svůj	k3xOyFgFnSc4	svůj
strategii	strategie	k1gFnSc4	strategie
vůči	vůči	k7c3	vůči
arbitrážím	arbitráž	k1gFnPc3	arbitráž
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
soudním	soudní	k2eAgInPc3d1	soudní
sporům	spor	k1gInPc3	spor
a	a	k8xC	a
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
účelovým	účelový	k2eAgInSc7d1	účelový
soudním	soudní	k2eAgInSc7d1	soudní
sporem	spor	k1gInSc7	spor
vyhrábnout	vyhrábnout	k5eAaPmF	vyhrábnout
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
poměrně	poměrně	k6eAd1	poměrně
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
jsme	být	k5eAaImIp1nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chováme	chovat	k5eAaImIp1nP	chovat
velmi	velmi	k6eAd1	velmi
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
bereme	brát	k5eAaImIp1nP	brát
si	se	k3xPyFc3	se
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
právníky	právník	k1gMnPc4	právník
a	a	k8xC	a
rveme	rvát	k5eAaImIp1nP	rvát
se	se	k3xPyFc4	se
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
naše	náš	k3xOp1gFnPc1	náš
vlastní	vlastní	k2eAgFnPc1d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
já	já	k3xPp1nSc1	já
nemám	mít	k5eNaImIp1nS	mít
právo	právo	k1gNnSc4	právo
chodit	chodit	k5eAaImF	chodit
dopředu	dopředu	k6eAd1	dopředu
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgNnPc6	který
mně	já	k3xPp1nSc3	já
všechny	všechen	k3xTgFnPc4	všechen
mé	můj	k3xOp1gFnPc4	můj
právní	právní	k2eAgFnPc4d1	právní
autority	autorita	k1gFnPc4	autorita
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
prohrané	prohraný	k2eAgFnPc1d1	prohraná
<g/>
,	,	kIx,	,
a	a	k8xC	a
vkládat	vkládat	k5eAaImF	vkládat
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
peníze	peníz	k1gInPc1	peníz
daňových	daňový	k2eAgMnPc2d1	daňový
poplatníků	poplatník	k1gMnPc2	poplatník
jenom	jenom	k9	jenom
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
že	že	k8xS	že
smlouva	smlouva	k1gFnSc1	smlouva
bude	být	k5eAaImBp3nS	být
porušena	porušit	k5eAaPmNgFnS	porušit
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
vám	vy	k3xPp2nPc3	vy
tedy	tedy	k9	tedy
nabídnu	nabídnout	k5eAaPmIp1nS	nabídnout
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
místopředsedo	místopředseda	k1gMnSc5	místopředseda
Zaorálku	Zaorálek	k1gMnSc5	Zaorálek
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgInSc4d1	jiný
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Zavažte	zavázat	k5eAaPmRp2nP	zavázat
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
<g/>
,	,	kIx,	,
že	že	k8xS	že
budete	být	k5eAaImBp2nP	být
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
prostředků	prostředek	k1gInPc2	prostředek
hradit	hradit	k5eAaImF	hradit
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
soudní	soudní	k2eAgInSc4d1	soudní
spor	spor	k1gInSc4	spor
<g/>
,	,	kIx,	,
podepište	podepsat	k5eAaPmRp2nP	podepsat
mi	já	k3xPp1nSc3	já
bianko	bianko	k6eAd1	bianko
směnku	směnka	k1gFnSc4	směnka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
uložím	uložit	k5eAaPmIp1nS	uložit
v	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
budeme	být	k5eAaImBp1nP	být
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
spolu	spolu	k6eAd1	spolu
průběh	průběh	k1gInSc4	průběh
soudního	soudní	k2eAgInSc2d1	soudní
sporu	spor	k1gInSc2	spor
i	i	k9	i
výběr	výběr	k1gInSc1	výběr
právníků	právník	k1gMnPc2	právník
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
prohrajeme	prohrát	k5eAaPmIp1nP	prohrát
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
vaše	váš	k3xOp2gInPc4	váš
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
nebojím	bát	k5eNaImIp1nS	bát
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
jdu	jít	k5eAaImIp1nS	jít
<g/>
!	!	kIx.	!
</s>
<s>
Pokud	pokud	k8xS	pokud
vyhrajeme	vyhrát	k5eAaPmIp1nP	vyhrát
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
vám	vy	k3xPp2nPc3	vy
peníze	peníz	k1gInPc4	peníz
vrátím	vrátit	k5eAaPmIp1nS	vrátit
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
mohu	moct	k5eAaImIp1nS	moct
udělat	udělat	k5eAaPmF	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
daňových	daňový	k2eAgMnPc2d1	daňový
poplatníků	poplatník	k1gMnPc2	poplatník
do	do	k7c2	do
toho	ten	k3xDgInSc2	ten
dávat	dávat	k5eAaImF	dávat
nebudu	být	k5eNaImBp1nS	být
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
nájemníků	nájemník	k1gMnPc2	nájemník
BYTYOKD	BYTYOKD	kA	BYTYOKD
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tiskové	tiskový	k2eAgFnSc6d1	tisková
zprávě	zpráva	k1gFnSc6	zpráva
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Advokátní	advokátní	k2eAgFnSc1d1	advokátní
kancelář	kancelář	k1gFnSc1	kancelář
Allen	Allen	k1gMnSc1	Allen
&	&	k?	&
Overy	Overa	k1gFnSc2	Overa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
akcií	akcie	k1gFnPc2	akcie
OKD	OKD	kA	OKD
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
státu	stát	k1gInSc3	stát
a	a	k8xC	a
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
financí	finance	k1gFnPc2	finance
odborné	odborný	k2eAgInPc1d1	odborný
posudky	posudek	k1gInPc1	posudek
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
plnění	plnění	k1gNnSc2	plnění
této	tento	k3xDgFnSc2	tento
Smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
pracovala	pracovat	k5eAaImAgFnS	pracovat
i	i	k9	i
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sdružení	sdružení	k1gNnSc1	sdružení
BYTYOKD	BYTYOKD	kA	BYTYOKD
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
tiskové	tiskový	k2eAgFnSc6d1	tisková
zprávě	zpráva	k1gFnSc6	zpráva
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
závažný	závažný	k2eAgInSc4d1	závažný
střet	střet	k1gInSc4	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
při	při	k7c6	při
projednávání	projednávání	k1gNnSc6	projednávání
bodu	bod	k1gInSc2	bod
"	"	kIx"	"
<g/>
Zpráva	zpráva	k1gFnSc1	zpráva
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
řešení	řešení	k1gNnSc4	řešení
problematiky	problematika	k1gFnSc2	problematika
tzv.	tzv.	kA	tzv.
bytového	bytový	k2eAgInSc2d1	bytový
fondu	fond	k1gInSc2	fond
OKD	OKD	kA	OKD
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
</s>
</p>
<p>
<s>
ochrany	ochrana	k1gFnSc2	ochrana
oprávněných	oprávněný	k2eAgNnPc2d1	oprávněné
práv	právo	k1gNnPc2	právo
nájemníků	nájemník	k1gMnPc2	nájemník
a	a	k8xC	a
zachováním	zachování	k1gNnSc7	zachování
závazků	závazek	k1gInPc2	závazek
předkupního	předkupní	k2eAgNnSc2d1	předkupní
práva	právo	k1gNnSc2	právo
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
uvedených	uvedený	k2eAgFnPc2d1	uvedená
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
"	"	kIx"	"
Lubomír	Lubomír	k1gMnSc1	Lubomír
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
přímo	přímo	k6eAd1	přímo
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
Miroslava	Miroslav	k1gMnSc4	Miroslav
Kalouska	Kalousek	k1gMnSc4	Kalousek
z	z	k7c2	z
nečestného	čestný	k2eNgNnSc2d1	nečestné
jednání	jednání	k1gNnSc2	jednání
když	když	k8xS	když
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
tyto	tento	k3xDgFnPc4	tento
věty	věta	k1gFnPc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vy	vy	k3xPp2nPc1	vy
říkáte	říkat	k5eAaImIp2nP	říkat
argumenty	argument	k1gInPc1	argument
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tady	tady	k6eAd1	tady
děláte	dělat	k5eAaImIp2nP	dělat
absurdní	absurdní	k2eAgFnSc4d1	absurdní
<g />
.	.	kIx.	.
</s>
<s>
politický	politický	k2eAgInSc1d1	politický
spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tady	tady	k6eAd1	tady
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
velká	velký	k2eAgFnSc1d1	velká
společnost	společnost	k1gFnSc1	společnost
snaží	snažit	k5eAaImIp3nS	snažit
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
35	[number]	k4	35
<g/>
miliardovému	miliardový	k2eAgInSc3d1	miliardový
majetku	majetek	k1gInSc3	majetek
<g/>
,	,	kIx,	,
a	a	k8xC	a
my	my	k3xPp1nPc1	my
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
dělat	dělat	k5eAaImF	dělat
něco	něco	k3yInSc4	něco
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
nejsme	být	k5eNaImIp1nP	být
všichni	všechen	k3xTgMnPc1	všechen
úplně	úplně	k6eAd1	úplně
vyřazeni	vyřadit	k5eAaPmNgMnP	vyřadit
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
za	za	k7c2	za
naší	náš	k3xOp1gFnSc2	náš
tiché	tichý	k2eAgFnSc2d1	tichá
asistence	asistence	k1gFnSc2	asistence
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
u	u	k7c2	u
pana	pan	k1gMnSc2	pan
Kalouska	Kalousek	k1gMnSc2	Kalousek
tomu	ten	k3xDgNnSc3	ten
rozumím	rozumět	k5eAaImIp1nS	rozumět
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
vazeb	vazba	k1gFnPc2	vazba
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
zřejmě	zřejmě	k6eAd1	zřejmě
zájem	zájem	k1gInSc4	zájem
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
nás	my	k3xPp1nPc4	my
mělo	mít	k5eAaImAgNnS	mít
zajímat	zajímat	k5eAaImF	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
ministrem	ministr	k1gMnSc7	ministr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
chránit	chránit	k5eAaImF	chránit
občany	občan	k1gMnPc4	občan
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
chránil	chránit	k5eAaImAgMnS	chránit
pana	pan	k1gMnSc2	pan
Bakalu	Bakal	k1gMnSc3	Bakal
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
OKD	OKD	kA	OKD
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bakala	Bakal	k1gMnSc2	Bakal
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
<g/>
:	:	kIx,	:
Stát	stát	k1gInSc1	stát
prodal	prodat	k5eAaPmAgInS	prodat
OKD	OKD	kA	OKD
pod	pod	k7c7	pod
cenou	cena	k1gFnSc7	cena
a	a	k8xC	a
Sobotkův	Sobotkův	k2eAgInSc1d1	Sobotkův
úřad	úřad	k1gInSc1	úřad
to	ten	k3xDgNnSc1	ten
kryl	krýt	k5eAaImAgInS	krýt
</s>
</p>
