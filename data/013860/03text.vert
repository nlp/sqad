<s>
Křižanovice	Křižanovice	k1gFnSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Vyškov	Vyškov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
obci	obec	k1gFnSc6
ležící	ležící	k2eAgInSc4d1
nedaleko	nedaleko	k7c2
Bučovic	Bučovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
obci	obec	k1gFnSc6
ležící	ležící	k2eAgNnPc1d1
nedaleko	nedaleko	k7c2
Vyškova	Vyškov	k1gInSc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Křižanovice	Křižanovice	k1gFnSc2
u	u	k7c2
Vyškova	Vyškov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Křižanovice	Křižanovice	k1gFnSc1
Pomník	pomník	k1gInSc1
obětem	oběť	k1gFnPc3
I.	I.	kA
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
škola	škola	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0646	CZ0646	k4
593222	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Bučovice	Bučovice	k1gFnPc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vyškov	Vyškov	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
646	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
802	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
4,79	4,79	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Křižanovice	Křižanovice	k1gFnSc1
u	u	k7c2
Bučovic	Bučovice	k1gFnPc2
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
209	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
683	#num#	k4
57	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Křižanovice	Křižanovice	k1gFnSc1
85685	#num#	k4
01	#num#	k4
Bučovice	Bučovice	k1gFnPc1
ou.krizanovice@worldonline.cz	ou.krizanovice@worldonline.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Cenek	Cenek	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
krizanovice	krizanovice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Křižanovice	Křižanovice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
593222	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
76481	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Křižanovice	Křižanovice	k1gFnPc1
jsou	být	k5eAaImIp3nP
obec	obec	k1gFnSc4
v	v	k7c6
okrese	okres	k1gInSc6
Vyškov	Vyškov	k1gInSc1
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
5	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
Bučovic	Bučovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
802	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1131	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
před	před	k7c7
bitvou	bitva	k1gFnSc7
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
soboty	sobota	k1gFnSc2
na	na	k7c4
neděli	neděle	k1gFnSc4
zde	zde	k6eAd1
přespal	přespat	k5eAaPmAgMnS
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
a	a	k8xC
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
Zatímco	zatímco	k8xS
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
přespal	přespat	k5eAaPmAgMnS
na	na	k7c6
faře	fara	k1gFnSc6
<g/>
,	,	kIx,
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
přespal	přespat	k5eAaPmAgMnS
na	na	k7c6
statku	statek	k1gInSc6
č.	č.	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
rozdal	rozdat	k5eAaPmAgMnS
mnoho	mnoho	k4c1
dukátů	dukát	k1gInPc2
a	a	k8xC
k	k	k7c3
večeři	večeře	k1gFnSc6
si	se	k3xPyFc3
prý	prý	k9
poručil	poručit	k5eAaPmAgInS
30	#num#	k4
vajec	vejce	k1gNnPc2
na	na	k7c4
tvrdo	tvrdo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
k	k	k7c3
počátku	počátek	k1gInSc3
roku	rok	k1gInSc2
2016	#num#	k4
žilo	žít	k5eAaImAgNnS
celkem	celek	k1gInSc7
792	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
407	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
385	#num#	k4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
věk	věk	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
dosahoval	dosahovat	k5eAaImAgInS
41	#num#	k4
let	rok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
domů	dům	k1gInPc2
a	a	k8xC
bytů	byt	k1gInPc2
<g/>
,	,	kIx,
provedeného	provedený	k2eAgInSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
žilo	žít	k5eAaImAgNnS
v	v	k7c6
obci	obec	k1gFnSc6
754	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
z	z	k7c2
nich	on	k3xPp3gInPc2
bylo	být	k5eAaImAgNnS
(	(	kIx(
<g/>
15,9	15,9	k4
%	%	kIx~
<g/>
)	)	kIx)
obyvatel	obyvatel	k1gMnPc2
ve	v	k7c6
věku	věk	k1gInSc6
od	od	k7c2
30	#num#	k4
do	do	k7c2
39	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
do	do	k7c2
14	#num#	k4
let	léto	k1gNnPc2
věku	věk	k1gInSc2
tvořily	tvořit	k5eAaImAgFnP
13,8	13,8	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
senioři	senior	k1gMnPc1
nad	nad	k7c4
70	#num#	k4
let	léto	k1gNnPc2
úhrnem	úhrnem	k6eAd1
6,5	6,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkem	celkem	k6eAd1
650	#num#	k4
občanů	občan	k1gMnPc2
obce	obec	k1gFnSc2
starších	starší	k1gMnPc2
15	#num#	k4
let	léto	k1gNnPc2
mělo	mít	k5eAaImAgNnS
vzdělání	vzdělání	k1gNnSc1
38,8	38,8	k4
%	%	kIx~
střední	střední	k2eAgNnSc1d1
vč.	vč.	k?
vyučení	vyučení	k1gNnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
maturity	maturita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
vysokoškoláků	vysokoškolák	k1gMnPc2
dosahoval	dosahovat	k5eAaImAgInS
7,2	7,2	k4
%	%	kIx~
a	a	k8xC
bez	bez	k7c2
vzdělání	vzdělání	k1gNnSc2
bylo	být	k5eAaImAgNnS
naopak	naopak	k6eAd1
0,2	0,2	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
cenzu	cenzus	k1gInSc2
dále	daleko	k6eAd2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
městě	město	k1gNnSc6
žilo	žít	k5eAaImAgNnS
404	#num#	k4
ekonomicky	ekonomicky	k6eAd1
aktivních	aktivní	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
91,6	91,6	k4
%	%	kIx~
z	z	k7c2
nich	on	k3xPp3gFnPc2
se	se	k3xPyFc4
řadilo	řadit	k5eAaImAgNnS
mezi	mezi	k7c4
zaměstnané	zaměstnaný	k1gMnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
74,3	74,3	k4
%	%	kIx~
patřilo	patřit	k5eAaImAgNnS
mezi	mezi	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
,	,	kIx,
2,2	2,2	k4
%	%	kIx~
k	k	k7c3
zaměstnavatelům	zaměstnavatel	k1gMnPc3
a	a	k8xC
zbytek	zbytek	k1gInSc4
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c4
vlastní	vlastní	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
celých	celý	k2eAgNnPc2d1
44,2	44,2	k4
%	%	kIx~
občanů	občan	k1gMnPc2
nebylo	být	k5eNaImAgNnS
ekonomicky	ekonomicky	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
nepracující	pracující	k2eNgMnPc1d1
důchodci	důchodce	k1gMnPc1
či	či	k8xC
žáci	žák	k1gMnPc1
<g/>
,	,	kIx,
studenti	student	k1gMnPc1
nebo	nebo	k8xC
učni	učeň	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
zbytek	zbytek	k1gInSc1
svou	svůj	k3xOyFgFnSc4
ekonomickou	ekonomický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
uvést	uvést	k5eAaPmF
nechtěl	chtít	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Úhrnem	úhrnem	k6eAd1
332	#num#	k4
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
44	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
české	český	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
220	#num#	k4
obyvatel	obyvatel	k1gMnPc2
bylo	být	k5eAaImAgNnS
Moravanů	Moravan	k1gMnPc2
a	a	k8xC
1	#num#	k4
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celých	celý	k2eAgMnPc2d1
342	#num#	k4
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
však	však	k9
svou	svůj	k3xOyFgFnSc4
národnost	národnost	k1gFnSc4
neuvedlo	uvést	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
za	za	k7c4
celou	celý	k2eAgFnSc4d1
obec	obec	k1gFnSc4
i	i	k9
za	za	k7c4
jeho	jeho	k3xOp3gFnPc4
jednotlivé	jednotlivý	k2eAgFnPc4d1
části	část	k1gFnPc4
uvádí	uvádět	k5eAaImIp3nS
tabulka	tabulka	k1gFnSc1
níže	níže	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
zobrazuje	zobrazovat	k5eAaImIp3nS
i	i	k9
příslušnost	příslušnost	k1gFnSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
k	k	k7c3
obci	obec	k1gFnSc3
či	či	k8xC
následné	následný	k2eAgNnSc4d1
odtržení	odtržení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Místní	místní	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
část	část	k1gFnSc1
Křižanovice	Křižanovice	k1gFnSc2
<g/>
477548554526611698742701750741768709709754	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
</s>
<s>
část	část	k1gFnSc1
Křižanovice	Křižanovice	k1gFnSc2
<g/>
91105108110135139169187185194208234234250	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatelData	obyvatelDat	k1gMnSc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
Wikidat	Wikidat	k1gFnSc2
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Vyškov	Vyškov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Boží	boží	k2eAgFnSc1d1
muka	muka	k1gFnSc1
„	„	k?
<g/>
Isidorek	Isidorek	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
u	u	k7c2
kostela	kostel	k1gInSc2
</s>
<s>
Pomník	pomník	k1gInSc1
Františky	Františka	k1gFnSc2
Škodové	Škodová	k1gFnSc2
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
</s>
<s>
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
Sokolovna	sokolovna	k1gFnSc1
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Křižanovice	Křižanovice	k1gFnSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Vyškov	Vyškov	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2011-03-26	2011-03-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Křižanovice	Křižanovice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2015-12-21	2015-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Křižanovice	Křižanovice	k1gFnSc1
u	u	k7c2
Bučovic	Bučovice	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Křižanovice	Křižanovice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Křižanovice	Křižanovice	k1gFnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Křižanovice	Křižanovice	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
<g/>
,	,	kIx,
obce	obec	k1gFnSc2
a	a	k8xC
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
okresu	okres	k1gInSc2
Vyškov	Vyškov	k1gInSc1
</s>
<s>
Bohaté	bohatý	k2eAgFnPc4d1
Málkovice	Málkovice	k1gFnPc4
•	•	k?
Bohdalice-Pavlovice	Bohdalice-Pavlovice	k1gFnPc4
•	•	k?
Bošovice	Bošovice	k1gFnPc4
•	•	k?
Brankovice	Brankovice	k1gFnPc4
•	•	k?
Bučovice	Bučovice	k1gFnPc4
•	•	k?
Dětkovice	Dětkovice	k1gFnSc2
•	•	k?
Dobročkovice	Dobročkovice	k1gFnSc2
•	•	k?
Dražovice	Dražovice	k1gFnSc1
•	•	k?
Drnovice	Drnovice	k1gInPc1
•	•	k?
Drysice	Drysice	k1gFnSc2
•	•	k?
Habrovany	Habrovan	k1gMnPc4
•	•	k?
Heršpice	Heršpice	k1gFnPc4
•	•	k?
Hlubočany	Hlubočan	k1gMnPc4
•	•	k?
Hodějice	Hodějice	k1gFnSc1
•	•	k?
Holubice	holubice	k1gFnSc2
•	•	k?
Hostěrádky-Rešov	Hostěrádky-Rešov	k1gInSc1
•	•	k?
Hoštice-Heroltice	Hoštice-Heroltice	k1gFnSc2
•	•	k?
Hrušky	hruška	k1gFnSc2
•	•	k?
Hvězdlice	Hvězdlice	k1gFnSc2
•	•	k?
Chvalkovice	Chvalkovice	k1gFnSc2
•	•	k?
Ivanovice	Ivanovice	k1gFnPc1
na	na	k7c6
Hané	Haná	k1gFnSc6
•	•	k?
Ježkovice	Ježkovice	k1gFnSc2
•	•	k?
Kobeřice	Kobeřice	k1gFnSc2
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kojátky	Kojátka	k1gFnSc2
•	•	k?
Komořany	Komořan	k1gMnPc4
•	•	k?
Kozlany	Kozlana	k1gFnPc4
•	•	k?
Kožušice	Kožušice	k1gFnPc4
•	•	k?
Krásensko	Krásensko	k1gNnSc1
•	•	k?
Křenovice	Křenovice	k1gFnSc2
•	•	k?
Křižanovice	Křižanovice	k1gFnSc2
•	•	k?
Křižanovice	Křižanovice	k1gFnSc2
u	u	k7c2
Vyškova	Vyškov	k1gInSc2
•	•	k?
Kučerov	Kučerov	k1gInSc1
•	•	k?
Letonice	Letonice	k1gFnSc2
•	•	k?
Lovčičky	Lovčička	k1gFnSc2
•	•	k?
Luleč	Luleč	k1gMnSc1
•	•	k?
Lysovice	Lysovice	k1gFnSc2
•	•	k?
Malínky	Malínka	k1gFnSc2
•	•	k?
Medlovice	Medlovice	k1gFnSc2
•	•	k?
Milešovice	Milešovice	k1gFnSc2
•	•	k?
Milonice	Milonice	k1gFnSc2
•	•	k?
Moravské	moravský	k2eAgFnSc2d1
Málkovice	Málkovice	k1gFnSc2
•	•	k?
Mouřínov	Mouřínov	k1gInSc1
•	•	k?
Němčany	Němčan	k1gMnPc4
•	•	k?
Nemochovice	Nemochovice	k1gFnSc1
•	•	k?
Nemojany	Nemojana	k1gFnSc2
•	•	k?
Nemotice	Nemotice	k1gFnSc2
•	•	k?
Nesovice	Nesovice	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nevojice	Nevojice	k1gFnSc2
•	•	k?
Nížkovice	Nížkovice	k1gFnSc2
•	•	k?
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
•	•	k?
Olšany	Olšany	k1gInPc4
•	•	k?
Orlovice	Orlovice	k1gFnSc2
•	•	k?
Otnice	Otnice	k1gFnSc2
•	•	k?
Podbřežice	Podbřežice	k1gFnSc2
•	•	k?
Podivice	Podivice	k1gFnSc1
•	•	k?
Podomí	Podomí	k1gNnPc2
•	•	k?
Prusy-Boškůvky	Prusy-Boškůvka	k1gFnSc2
•	•	k?
Pustiměř	Pustiměř	k1gMnSc1
•	•	k?
Račice-Pístovice	Račice-Pístovice	k1gFnSc2
•	•	k?
Radslavice	Radslavice	k1gFnSc2
•	•	k?
Rašovice	Rašovice	k1gFnSc2
•	•	k?
Rostěnice-Zvonovice	Rostěnice-Zvonovice	k1gFnSc2
•	•	k?
Rousínov	Rousínov	k1gInSc1
•	•	k?
Ruprechtov	Ruprechtov	k1gInSc1
•	•	k?
Rybníček	rybníček	k1gInSc1
•	•	k?
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Snovídky	Snovídka	k1gFnSc2
•	•	k?
Studnice	studnice	k1gFnSc2
•	•	k?
Šaratice	šaratice	k1gFnSc2
•	•	k?
Švábenice	Švábenice	k1gFnSc2
•	•	k?
Topolany	Topolana	k1gFnSc2
•	•	k?
Tučapy	Tučapa	k1gFnSc2
•	•	k?
Uhřice	Uhřice	k1gFnSc2
•	•	k?
Vážany	Vážana	k1gFnSc2
•	•	k?
Vážany	Vážana	k1gFnSc2
nad	nad	k7c7
Litavou	Litava	k1gFnSc7
•	•	k?
Velešovice	Velešovice	k1gFnSc2
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zbýšov	Zbýšov	k1gInSc1
•	•	k?
Zelená	zelenat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Březina	Březina	k1gFnSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
