<p>
<s>
Bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
staré	starý	k2eAgNnSc1d1	staré
plemeno	plemeno	k1gNnSc1	plemeno
holuba	holub	k1gMnSc2	holub
domácího	domácí	k2eAgMnSc2d1	domácí
pocházející	pocházející	k2eAgInSc4d1	pocházející
z	z	k7c2	z
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
plemenné	plemenný	k2eAgFnSc2d1	plemenná
skupiny	skupina	k1gFnSc2	skupina
bubláků	bublák	k1gMnPc2	bublák
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jsou	být	k5eAaImIp3nP	být
holubi	holub	k1gMnPc1	holub
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
specifický	specifický	k2eAgInSc1d1	specifický
hlasový	hlasový	k2eAgInSc1d1	hlasový
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bublání	bublání	k1gNnSc2	bublání
<g/>
.	.	kIx.	.
</s>
<s>
Vrkání	vrkání	k1gNnSc1	vrkání
je	být	k5eAaImIp3nS	být
modifikováno	modifikovat	k5eAaBmNgNnS	modifikovat
do	do	k7c2	do
déletrvajícího	déletrvající	k2eAgInSc2d1	déletrvající
zvuku	zvuk	k1gInSc2	zvuk
připomínajícího	připomínající	k2eAgInSc2d1	připomínající
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
zurčení	zurčení	k1gNnPc4	zurčení
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
zvonění	zvonění	k1gNnSc1	zvonění
zvonů	zvon	k1gInPc2	zvon
nebo	nebo	k8xC	nebo
bubnování	bubnování	k1gNnSc2	bubnování
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bucharského	bucharský	k2eAgMnSc2d1	bucharský
bubláka	bublák	k1gMnSc2	bublák
se	se	k3xPyFc4	se
však	však	k9	však
šlechtění	šlechtění	k1gNnSc1	šlechtění
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
především	především	k9	především
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
opeření	opeření	k1gNnSc2	opeření
a	a	k8xC	a
rozvinutí	rozvinutí	k1gNnSc2	rozvinutí
pernatých	pernatý	k2eAgFnPc2d1	pernatá
ozdob	ozdoba	k1gFnPc2	ozdoba
a	a	k8xC	a
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
melodičnost	melodičnost	k1gFnSc4	melodičnost
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
bublání	bublání	k1gNnSc2	bublání
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
<g/>
Bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
plemenem	plemeno	k1gNnSc7	plemeno
bubláků	bublák	k1gMnPc2	bublák
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
také	také	k9	také
nejznámější	známý	k2eAgFnSc1d3	nejznámější
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
ale	ale	k9	ale
téměř	téměř	k6eAd1	téměř
nechová	chovat	k5eNaImIp3nS	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Plemeno	plemeno	k1gNnSc1	plemeno
se	se	k3xPyFc4	se
však	však	k9	však
podílelo	podílet	k5eAaImAgNnS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
většiny	většina	k1gFnSc2	většina
jiných	jiný	k2eAgNnPc2d1	jiné
plemen	plemeno	k1gNnPc2	plemeno
bubláků	bublák	k1gMnPc2	bublák
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
plemene	plemeno	k1gNnSc2	plemeno
český	český	k2eAgMnSc1d1	český
bublák	bublák	k1gMnSc1	bublák
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
vzorníku	vzorník	k1gInSc6	vzorník
plemen	plemeno	k1gNnPc2	plemeno
holubů	holub	k1gMnPc2	holub
se	se	k3xPyFc4	se
bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
barevné	barevný	k2eAgMnPc4d1	barevný
holuby	holub	k1gMnPc4	holub
do	do	k7c2	do
podskupiny	podskupina	k1gFnSc2	podskupina
bubláci	bublák	k1gMnPc1	bublák
<g/>
,	,	kIx,	,
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
plemen	plemeno	k1gNnPc2	plemeno
EE	EE	kA	EE
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
samostatné	samostatný	k2eAgFnSc2d1	samostatná
plemenné	plemenný	k2eAgFnSc2d1	plemenná
skupiny	skupina	k1gFnSc2	skupina
bubláci	bublák	k1gMnPc1	bublák
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
0	[number]	k4	0
<g/>
501	[number]	k4	501
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozvoji	rozvoj	k1gInSc3	rozvoj
pernatých	pernatý	k2eAgFnPc2d1	pernatá
ozdob	ozdoba	k1gFnPc2	ozdoba
je	být	k5eAaImIp3nS	být
bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
někdy	někdy	k6eAd1	někdy
řazen	řadit	k5eAaImNgMnS	řadit
i	i	k9	i
mezi	mezi	k7c4	mezi
strukturové	strukturový	k2eAgMnPc4d1	strukturový
holuby	holub	k1gMnPc4	holub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgNnSc4d1	staré
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
získal	získat	k5eAaPmAgInS	získat
podle	podle	k7c2	podle
uzbeckého	uzbecký	k2eAgNnSc2d1	uzbecké
města	město	k1gNnSc2	město
Buchara	Buchar	k1gMnSc2	Buchar
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jenž	k3xRgNnSc2	jenž
okolí	okolí	k1gNnSc2	okolí
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
šlechtění	šlechtění	k1gNnPc4	šlechtění
evropských	evropský	k2eAgNnPc2d1	Evropské
rousných	rousný	k2eAgNnPc2d1	rousné
plemen	plemeno	k1gNnPc2	plemeno
bubláků	bublák	k1gMnPc2	bublák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nacházel	nacházet	k5eAaImAgMnS	nacházet
chov	chov	k1gInSc4	chov
bucharského	bucharský	k2eAgMnSc2d1	bucharský
bubláka	bublák	k1gMnSc2	bublák
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
chovatelem	chovatel	k1gMnSc7	chovatel
byl	být	k5eAaImAgMnS	být
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
časopisu	časopis	k1gInSc2	časopis
Zvířena	zvířena	k1gFnSc1	zvířena
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgMnSc1d1	velký
holub	holub	k1gMnSc1	holub
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
samotného	samotný	k2eAgInSc2d1	samotný
vzrůstu	vzrůst	k1gInSc2	vzrůst
ptáka	pták	k1gMnSc2	pták
dán	dát	k5eAaPmNgInS	dát
i	i	k9	i
výrazně	výrazně	k6eAd1	výrazně
nakypřeným	nakypřený	k2eAgNnSc7d1	nakypřené
<g/>
,	,	kIx,	,
bohatým	bohatý	k2eAgNnSc7d1	bohaté
opeřením	opeření	k1gNnSc7	opeření
<g/>
.	.	kIx.	.
</s>
<s>
Pernaté	pernatý	k2eAgFnPc1d1	pernatá
ozdoby	ozdoba	k1gFnPc1	ozdoba
jsou	být	k5eAaImIp3nP	být
mohutně	mohutně	k6eAd1	mohutně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
<g/>
,	,	kIx,	,
čelní	čelní	k2eAgFnSc1d1	čelní
růžice	růžice	k1gFnSc1	růžice
překrývá	překrývat	k5eAaImIp3nS	překrývat
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
lasturovitou	lasturovitý	k2eAgFnSc7d1	lasturovitá
chocholkou	chocholka	k1gFnSc7	chocholka
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgFnPc1d1	porostlá
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
<g/>
,	,	kIx,	,
do	do	k7c2	do
široka	široek	k1gInSc2	široek
rozloženými	rozložený	k2eAgInPc7d1	rozložený
rousy	rous	k1gInPc7	rous
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
,	,	kIx,	,
zdobená	zdobený	k2eAgFnSc1d1	zdobená
dvěma	dva	k4xCgInPc7	dva
vrkoči	vrkoč	k1gInPc7	vrkoč
<g/>
:	:	kIx,	:
přední	přední	k2eAgFnSc4d1	přední
vrkoč	vrkoč	k1gFnSc4	vrkoč
je	být	k5eAaImIp3nS	být
bohatě	bohatě	k6eAd1	bohatě
propeřená	propeřený	k2eAgFnSc1d1	propeřený
čelní	čelní	k2eAgFnSc1d1	čelní
růžice	růžice	k1gFnSc1	růžice
se	s	k7c7	s
středem	střed	k1gInSc7	střed
nad	nad	k7c7	nad
ozobím	ozobí	k1gNnSc7	ozobí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
víru	vír	k1gInSc2	vír
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
pírka	pírko	k1gNnPc1	pírko
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
<g/>
,	,	kIx,	,
vpředu	vpředu	k6eAd1	vpředu
zakrývají	zakrývat	k5eAaImIp3nP	zakrývat
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
kryjí	krýt	k5eAaImIp3nP	krýt
celé	celý	k2eAgNnSc4d1	celé
temeno	temeno	k1gNnSc4	temeno
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vzadu	vzadu	k6eAd1	vzadu
se	se	k3xPyFc4	se
čelní	čelní	k2eAgFnSc1d1	čelní
růžice	růžice	k1gFnSc1	růžice
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
opeřením	opeření	k1gNnSc7	opeření
zadního	zadní	k2eAgInSc2d1	zadní
vrkoče	vrkoč	k1gInSc2	vrkoč
<g/>
.	.	kIx.	.
</s>
<s>
Zadním	zadní	k2eAgInSc7d1	zadní
vrkočem	vrkoč	k1gInSc7	vrkoč
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
postavená	postavený	k2eAgFnSc1d1	postavená
bohatá	bohatý	k2eAgFnSc1d1	bohatá
lasturovitá	lasturovitý	k2eAgFnSc1d1	lasturovitá
chocholka	chocholka	k1gFnSc1	chocholka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
ucha	ucho	k1gNnSc2	ucho
k	k	k7c3	k
uchu	ucho	k1gNnSc3	ucho
ptáka	pták	k1gMnSc2	pták
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zakončená	zakončený	k2eAgFnSc1d1	zakončená
výraznými	výrazný	k2eAgFnPc7d1	výrazná
postranními	postranní	k2eAgFnPc7d1	postranní
růžicemi	růžice	k1gFnPc7	růžice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zátylí	zátylí	k1gNnSc6	zátylí
vrkoč	vrkoč	k1gInSc1	vrkoč
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hřívy	hříva	k1gFnSc2	hříva
až	až	k9	až
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
a	a	k8xC	a
jemným	jemný	k2eAgNnSc7d1	jemné
ozobím	ozobí	k1gNnSc7	ozobí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
černých	černý	k2eAgInPc2d1	černý
a	a	k8xC	a
modrých	modrý	k2eAgInPc2d1	modrý
barevných	barevný	k2eAgInPc2d1	barevný
rázů	ráz	k1gInPc2	ráz
je	být	k5eAaImIp3nS	být
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
,	,	kIx,	,
u	u	k7c2	u
ostatních	ostatní	k1gNnPc2	ostatní
rohový	rohový	k2eAgMnSc1d1	rohový
až	až	k8xS	až
narůžovělý	narůžovělý	k2eAgMnSc1d1	narůžovělý
<g/>
.	.	kIx.	.
</s>
<s>
Oční	oční	k2eAgFnSc1d1	oční
duhovka	duhovka	k1gFnSc1	duhovka
je	být	k5eAaImIp3nS	být
perlová	perlový	k2eAgFnSc1d1	Perlová
<g/>
,	,	kIx,	,
u	u	k7c2	u
bílých	bílý	k2eAgMnPc2d1	bílý
tmavá	tmavý	k2eAgNnPc1d1	tmavé
<g/>
,	,	kIx,	,
vikvová	vikvový	k2eAgNnPc1d1	vikvový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
širokou	široký	k2eAgFnSc4d1	široká
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
klenutou	klenutý	k2eAgFnSc4d1	klenutá
hruď	hruď	k1gFnSc4	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
a	a	k8xC	a
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
hřbet	hřbet	k1gInSc1	hřbet
se	se	k3xPyFc4	se
jen	jen	k9	jen
málo	málo	k6eAd1	málo
svažuje	svažovat	k5eAaImIp3nS	svažovat
a	a	k8xC	a
držení	držení	k1gNnSc1	držení
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
skoro	skoro	k6eAd1	skoro
vodorovné	vodorovný	k2eAgNnSc1d1	vodorovné
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
nesen	nést	k5eAaImNgInS	nést
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
letky	letek	k1gInPc1	letek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
z	z	k7c2	z
lýtek	lýtko	k1gNnPc2	lýtko
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
supí	supí	k2eAgNnPc1d1	supí
pera	pero	k1gNnPc1	pero
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
bohatý	bohatý	k2eAgInSc4d1	bohatý
rous	rous	k1gInSc4	rous
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
uspořádaný	uspořádaný	k2eAgInSc1d1	uspořádaný
do	do	k7c2	do
půlkruhu	půlkruh	k1gInSc2	půlkruh
<g/>
,	,	kIx,	,
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
prostor	prostor	k1gInSc1	prostor
mezi	mezi	k7c7	mezi
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
zepředu	zepředu	k6eAd1	zepředu
rous	rous	k1gInSc1	rous
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
všechen	všechen	k3xTgInSc4	všechen
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
kýlem	kýl	k1gInSc7	kýl
hrudní	hrudní	k2eAgFnSc2d1	hrudní
kosti	kost	k1gFnSc2	kost
ptáka	pták	k1gMnSc2	pták
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
silueta	silueta	k1gFnSc1	silueta
ptáka	pták	k1gMnSc2	pták
vyplněná	vyplněná	k1gFnSc1	vyplněná
a	a	k8xC	a
shora	shora	k6eAd1	shora
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opeření	opeření	k1gNnSc1	opeření
je	být	k5eAaImIp3nS	být
husté	hustý	k2eAgNnSc1d1	husté
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
kypré	kyprý	k2eAgFnPc1d1	kyprá
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
přiléhavé	přiléhavý	k2eAgNnSc4d1	přiléhavé
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
barva	barva	k1gFnSc1	barva
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
rozložená	rozložený	k2eAgFnSc1d1	rozložená
<g/>
.	.	kIx.	.
</s>
<s>
Bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
je	on	k3xPp3gNnSc4	on
chová	chovat	k5eAaImIp3nS	chovat
především	především	k9	především
v	v	k7c6	v
rázu	ráz	k1gInSc6	ráz
bílém	bílý	k2eAgInSc6d1	bílý
<g/>
,	,	kIx,	,
modrém	modré	k1gNnSc6	modré
bezpruhém	bezpruhý	k2eAgNnSc6d1	bezpruhé
<g/>
,	,	kIx,	,
pruhovém	pruhový	k2eAgNnSc6d1	pruhové
i	i	k8xC	i
kapratém	kapratý	k2eAgMnSc6d1	kapratý
<g/>
,	,	kIx,	,
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
plnobarevné	plnobarevný	k2eAgFnSc2d1	plnobarevná
černé	černá	k1gFnSc2	černá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
a	a	k8xC	a
žluté	žlutý	k2eAgFnPc1d1	žlutá
a	a	k8xC	a
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
barvách	barva	k1gFnPc6	barva
i	i	k9	i
jako	jako	k9	jako
stříkaný	stříkaný	k2eAgMnSc1d1	stříkaný
či	či	k8xC	či
strakatý	strakatý	k2eAgMnSc1d1	strakatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výhradně	výhradně	k6eAd1	výhradně
okrasné	okrasný	k2eAgNnSc1d1	okrasné
plemeno	plemeno	k1gNnSc1	plemeno
vhodné	vhodný	k2eAgNnSc1d1	vhodné
do	do	k7c2	do
voliérového	voliérový	k2eAgInSc2d1	voliérový
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PETRŽÍLKA	PETRŽÍLKA	kA	PETRŽÍLKA
<g/>
,	,	kIx,	,
Slavibor	Slavibor	k1gMnSc1	Slavibor
<g/>
;	;	kIx,	;
TYLLER	TYLLER	kA	TYLLER
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Holubi	holub	k1gMnPc1	holub
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
223	[number]	k4	223
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7151	[number]	k4	7151
<g/>
-	-	kIx~	-
<g/>
235	[number]	k4	235
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TUREČEK	Tureček	k1gMnSc1	Tureček
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Holubářství	holubářství	k1gNnSc1	holubářství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
156	[number]	k4	156
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BUREŠ	Bureš	k1gMnSc1	Bureš
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
ZAVADIL	Zavadil	k1gMnSc1	Zavadil
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
<g/>
.	.	kIx.	.
</s>
<s>
Příručka	příručka	k1gFnSc1	příručka
chovatele	chovatel	k1gMnSc2	chovatel
holubů	holub	k1gMnPc2	holub
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
322	[number]	k4	322
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bucharský	bucharský	k2eAgMnSc1d1	bucharský
bublák	bublák	k1gMnSc1	bublák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
chovatelů	chovatel	k1gMnPc2	chovatel
holubů	holub	k1gMnPc2	holub
českých	český	k2eAgMnPc2d1	český
bubláků	bublák	k1gMnPc2	bublák
</s>
</p>
