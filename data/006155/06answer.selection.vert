<s>
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
[	[	kIx(	[
<g/>
Šó	Šó	k1gMnSc1	Šó
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Dublin	Dublin	k1gInSc1	Dublin
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Ayot	Ayot	k1gInSc1	Ayot
St.	st.	kA	st.
Lawrence	Lawrence	k1gFnSc2	Lawrence
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
