<s>
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
[	[	kIx(	[
<g/>
Šó	Šó	k1gMnSc1	Šó
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Dublin	Dublin	k1gInSc1	Dublin
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Ayot	Ayot	k1gInSc1	Ayot
St.	st.	kA	st.
Lawrence	Lawrence	k1gFnSc2	Lawrence
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Shaw	Shaw	k?	Shaw
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosti	dosti	k6eAd1	dosti
zchudlé	zchudlý	k2eAgFnPc4d1	zchudlá
protestantské	protestantský	k2eAgFnPc4d1	protestantská
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
neúspěšným	úspěšný	k2eNgMnSc7d1	neúspěšný
obchodníkem	obchodník	k1gMnSc7	obchodník
s	s	k7c7	s
obilím	obilí	k1gNnSc7	obilí
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
alkoholik	alkoholik	k1gMnSc1	alkoholik
a	a	k8xC	a
nestaral	starat	k5eNaImAgMnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
synovo	synův	k2eAgNnSc4d1	synovo
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Shaw	Shaw	k?	Shaw
brzy	brzy	k6eAd1	brzy
poznal	poznat	k5eAaPmAgMnS	poznat
praktický	praktický	k2eAgInSc4d1	praktický
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
musel	muset	k5eAaImAgMnS	muset
sám	sám	k3xTgInSc4	sám
živit	živit	k5eAaImF	živit
(	(	kIx(	(
<g/>
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
v	v	k7c6	v
dublinské	dublinský	k2eAgFnSc6d1	Dublinská
realitní	realitní	k2eAgFnSc6d1	realitní
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
měla	mít	k5eAaImAgFnS	mít
jedině	jedině	k6eAd1	jedině
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
opustila	opustit	k5eAaPmAgFnS	opustit
manžela	manžel	k1gMnSc4	manžel
a	a	k8xC	a
odjela	odjet	k5eAaPmAgFnS	odjet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
učitelem	učitel	k1gMnSc7	učitel
zpěvu	zpěv	k1gInSc2	zpěv
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
zpěv	zpěv	k1gInSc1	zpěv
sama	sám	k3xTgMnSc4	sám
vyučovat	vyučovat	k5eAaImF	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
přijel	přijet	k5eAaPmAgMnS	přijet
Shaw	Shaw	k1gMnSc1	Shaw
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
za	za	k7c7	za
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
dalších	další	k2eAgNnPc2d1	další
třicet	třicet	k4xCc1	třicet
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
na	na	k7c4	na
otcův	otcův	k2eAgInSc4d1	otcův
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
články	článek	k1gInPc7	článek
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
The	The	k1gMnSc1	The
Hornet	Hornet	k1gMnSc1	Hornet
(	(	kIx(	(
<g/>
Sršeň	sršeň	k1gMnSc1	sršeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
se	se	k3xPyFc4	se
jako	jako	k9	jako
samouk	samouk	k1gMnSc1	samouk
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
Britského	britský	k2eAgNnSc2d1	Britské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc1d1	jiné
četbou	četba	k1gFnSc7	četba
partitur	partitura	k1gFnPc2	partitura
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
a	a	k8xC	a
děl	dít	k5eAaBmAgInS	dít
Karla	Karel	k1gMnSc4	Karel
Marxe	Marx	k1gMnSc4	Marx
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
myšlenky	myšlenka	k1gFnPc4	myšlenka
o	o	k7c6	o
třídním	třídní	k2eAgInSc6d1	třídní
boji	boj	k1gInSc6	boj
a	a	k8xC	a
revolučním	revoluční	k2eAgNnSc6d1	revoluční
poslání	poslání	k1gNnSc6	poslání
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
však	však	k9	však
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
propagátorem	propagátor	k1gMnSc7	propagátor
norského	norský	k2eAgMnSc2d1	norský
dramatika	dramatik	k1gMnSc2	dramatik
Henrika	Henrik	k1gMnSc2	Henrik
Ibsena	Ibsen	k1gMnSc2	Ibsen
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
také	také	k6eAd1	také
akce	akce	k1gFnSc2	akce
různých	různý	k2eAgFnPc2d1	různá
filozofických	filozofický	k2eAgFnPc2d1	filozofická
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
amerického	americký	k2eAgMnSc2d1	americký
ekonoma	ekonom	k1gMnSc2	ekonom
Henryho	Henry	k1gMnSc2	Henry
George	Georg	k1gMnSc2	Georg
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
přivedly	přivést	k5eAaPmAgFnP	přivést
k	k	k7c3	k
reformnímu	reformní	k2eAgInSc3d1	reformní
socialismu	socialismus	k1gInSc3	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
společně	společně	k6eAd1	společně
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
labouristickým	labouristický	k2eAgMnSc7d1	labouristický
politikem	politik	k1gMnSc7	politik
<g/>
,	,	kIx,	,
ekonomem	ekonom	k1gMnSc7	ekonom
a	a	k8xC	a
publicistou	publicista	k1gMnSc7	publicista
Sidneym	Sidneym	k1gInSc4	Sidneym
Jamesem	James	k1gMnSc7	James
Webbem	Webb	k1gMnSc7	Webb
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
Fabiánskou	fabiánský	k2eAgFnSc4d1	Fabiánská
společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
Fabian	Fabian	k1gMnSc1	Fabian
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
šířila	šířit	k5eAaImAgFnS	šířit
teorii	teorie	k1gFnSc4	teorie
fabiánského	fabiánský	k2eAgInSc2d1	fabiánský
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
odmítali	odmítat	k5eAaImAgMnP	odmítat
násilí	násilí	k1gNnSc4	násilí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Marxovy	Marxův	k2eAgFnPc1d1	Marxova
myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c6	o
nastolení	nastolení	k1gNnSc6	nastolení
socialismu	socialismus	k1gInSc2	socialismus
revolučním	revoluční	k2eAgInSc7d1	revoluční
bojem	boj	k1gInSc7	boj
<g/>
,	,	kIx,	,
a	a	k8xC	a
připravovali	připravovat	k5eAaImAgMnP	připravovat
porážku	porážka	k1gFnSc4	porážka
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
pomocí	pomocí	k7c2	pomocí
postupných	postupný	k2eAgFnPc2d1	postupná
reforem	reforma	k1gFnPc2	reforma
a	a	k8xC	a
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Shaw	Shaw	k1gMnSc1	Shaw
prosadil	prosadit	k5eAaPmAgMnS	prosadit
jako	jako	k9	jako
pronikavý	pronikavý	k2eAgMnSc1d1	pronikavý
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
literární	literární	k2eAgMnSc1d1	literární
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
výtvarné	výtvarný	k2eAgFnPc1d1	výtvarná
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
recenze	recenze	k1gFnPc1	recenze
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
londýnském	londýnský	k2eAgInSc6d1	londýnský
časopise	časopis	k1gInSc6	časopis
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
nacionalisticky	nacionalisticky	k6eAd1	nacionalisticky
orientovaném	orientovaný	k2eAgInSc6d1	orientovaný
časopise	časopis	k1gInSc6	časopis
The	The	k1gMnSc3	The
Star	Star	kA	Star
(	(	kIx(	(
<g/>
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
psal	psát	k5eAaImAgInS	psát
i	i	k9	i
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Revue	revue	k1gFnSc1	revue
(	(	kIx(	(
<g/>
Sobotní	sobotní	k2eAgFnSc1d1	sobotní
revue	revue	k1gFnSc1	revue
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
Shaw	Shaw	k1gMnSc1	Shaw
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Charlottou	Charlotta	k1gFnSc7	Charlotta
Payne-Townsendovou	Payne-Townsendový	k2eAgFnSc7d1	Payne-Townsendový
<g/>
.	.	kIx.	.
</s>
<s>
Bezdětné	bezdětný	k2eAgFnPc1d1	bezdětná
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
šťastné	šťastný	k2eAgNnSc1d1	šťastné
manželství	manželství	k1gNnSc1	manželství
skončilo	skončit	k5eAaPmAgNnS	skončit
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
Charlottinou	Charlottin	k2eAgFnSc7d1	Charlottina
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
otřeseno	otřást	k5eAaPmNgNnS	otřást
pouze	pouze	k6eAd1	pouze
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
Shawovou	Shawový	k2eAgFnSc7d1	Shawový
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
herečce	herečka	k1gFnSc3	herečka
Stelle	Stella	k1gFnSc3	Stella
Campbellové	Campbellová	k1gFnSc3	Campbellová
<g/>
.	.	kIx.	.
</s>
<s>
Shaw	Shaw	k?	Shaw
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc1	jeho
pokusy	pokus	k1gInPc1	pokus
nebyly	být	k5eNaImAgInP	být
nijak	nijak	k6eAd1	nijak
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
socialistické	socialistický	k2eAgInPc1d1	socialistický
projevy	projev	k1gInPc1	projev
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
hrabství	hrabství	k1gNnSc2	hrabství
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
odradily	odradit	k5eAaPmAgInP	odradit
většinu	většina	k1gFnSc4	většina
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
skepse	skepse	k1gFnSc2	skepse
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
událostmi	událost	k1gFnPc7	událost
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Shawova	Shawův	k2eAgFnSc1d1	Shawova
žurnalistická	žurnalistický	k2eAgFnSc1d1	žurnalistická
i	i	k8xC	i
finanční	finanční	k2eAgFnSc1d1	finanční
podpora	podpora	k1gFnSc1	podpora
komunistické	komunistický	k2eAgFnSc2d1	komunistická
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
navštívil	navštívit	k5eAaPmAgInS	navštívit
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
sklidil	sklidit	k5eAaPmAgMnS	sklidit
velký	velký	k2eAgInSc4d1	velký
neúspěch	neúspěch	k1gInSc4	neúspěch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přednášel	přednášet	k5eAaImAgMnS	přednášet
o	o	k7c4	o
znárodnění	znárodnění	k1gNnSc4	znárodnění
amerického	americký	k2eAgInSc2d1	americký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
Shawova	Shawův	k2eAgNnSc2d1	Shawův
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
stojí	stát	k5eAaImIp3nS	stát
série	série	k1gFnSc1	série
pěti	pět	k4xCc2	pět
nepříliš	příliš	k6eNd1	příliš
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
románů	román	k1gInPc2	román
napsaných	napsaný	k2eAgInPc2d1	napsaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
čtyři	čtyři	k4xCgMnPc4	čtyři
byly	být	k5eAaImAgFnP	být
nakladatelem	nakladatel	k1gMnSc7	nakladatel
odmítnuty	odmítnout	k5eAaPmNgFnP	odmítnout
a	a	k8xC	a
pátý	pátý	k4xOgInSc1	pátý
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
nejprve	nejprve	k6eAd1	nejprve
časopisecky	časopisecky	k6eAd1	časopisecky
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
nezralá	zralý	k2eNgNnPc4d1	nezralé
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnSc4d1	vyznačující
se	se	k3xPyFc4	se
však	však	k9	však
odvážnou	odvážný	k2eAgFnSc7d1	odvážná
útočností	útočnost	k1gFnSc7	útočnost
a	a	k8xC	a
satirickými	satirický	k2eAgInPc7d1	satirický
šlehy	šleh	k1gInPc7	šleh
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
hrdinou	hrdina	k1gMnSc7	hrdina
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
mladý	mladý	k2eAgMnSc1d1	mladý
výtržník	výtržník	k1gMnSc1	výtržník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pohoršuje	pohoršovat	k5eAaImIp3nS	pohoršovat
měšťáky	měšťák	k1gMnPc4	měšťák
nesentimentální	sentimentální	k2eNgMnPc1d1	nesentimentální
věcností	věcnost	k1gFnSc7	věcnost
i	i	k8xC	i
naprostou	naprostý	k2eAgFnSc7d1	naprostá
neúctou	neúcta	k1gFnSc7	neúcta
ke	k	k7c3	k
společenským	společenský	k2eAgFnPc3d1	společenská
konvencím	konvence	k1gFnPc3	konvence
a	a	k8xC	a
posvátným	posvátný	k2eAgFnPc3d1	posvátná
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
pravé	pravý	k2eAgNnSc4d1	pravé
umělecké	umělecký	k2eAgNnSc4d1	umělecké
pole	pole	k1gNnSc4	pole
našel	najít	k5eAaPmAgMnS	najít
Shaw	Shaw	k1gMnSc1	Shaw
až	až	k9	až
v	v	k7c6	v
dramatické	dramatický	k2eAgFnSc6d1	dramatická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
svůj	svůj	k3xOyFgInSc4	svůj
jasný	jasný	k2eAgInSc4d1	jasný
<g/>
,	,	kIx,	,
kritický	kritický	k2eAgInSc4d1	kritický
a	a	k8xC	a
ironický	ironický	k2eAgInSc4d1	ironický
intelekt	intelekt	k1gInSc4	intelekt
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
anglického	anglický	k2eAgNnSc2d1	anglické
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
anglickým	anglický	k2eAgMnSc7d1	anglický
dramatikem	dramatik	k1gMnSc7	dramatik
po	po	k7c6	po
Williamu	William	k1gInSc6	William
Shakespearovi	Shakespeare	k1gMnSc3	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
hrou	hra	k1gFnSc7	hra
Domy	dům	k1gInPc1	dům
pana	pan	k1gMnSc2	pan
Sartoria	Sartorium	k1gNnSc2	Sartorium
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
následovala	následovat	k5eAaImAgFnS	následovat
řada	řada	k1gFnSc1	řada
vtipných	vtipný	k2eAgFnPc2d1	vtipná
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
satiricky	satiricky	k6eAd1	satiricky
přiostřených	přiostřený	k2eAgNnPc2d1	přiostřené
dramat	drama	k1gNnPc2	drama
s	s	k7c7	s
náměty	námět	k1gInPc7	námět
dobovými	dobový	k2eAgInPc7d1	dobový
(	(	kIx(	(
<g/>
Živnost	živnost	k1gFnSc1	živnost
paní	paní	k1gFnSc2	paní
Warrenové	Warrenová	k1gFnSc2	Warrenová
<g/>
,	,	kIx,	,
Candida	Candida	k1gFnSc1	Candida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historickými	historický	k2eAgInPc7d1	historický
(	(	kIx(	(
<g/>
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
<g/>
)	)	kIx)	)
či	či	k8xC	či
s	s	k7c7	s
úsměvným	úsměvný	k2eAgInSc7d1	úsměvný
až	až	k8xS	až
sardonickým	sardonický	k2eAgInSc7d1	sardonický
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
aktuální	aktuální	k2eAgFnPc4d1	aktuální
otázky	otázka	k1gFnPc4	otázka
ženské	ženský	k2eAgFnSc2d1	ženská
emancipace	emancipace	k1gFnSc2	emancipace
a	a	k8xC	a
na	na	k7c4	na
viktoriánské	viktoriánský	k2eAgFnPc4d1	viktoriánská
představy	představa	k1gFnPc4	představa
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	své	k1gNnSc1	své
hry	hra	k1gFnSc2	hra
Shaw	Shaw	k1gFnSc2	Shaw
tvořil	tvořit	k5eAaImAgInS	tvořit
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
koncepce	koncepce	k1gFnSc2	koncepce
tzv.	tzv.	kA	tzv.
sociálně	sociálně	k6eAd1	sociálně
diskuzní	diskuzní	k2eAgFnSc1d1	diskuzní
komedie	komedie	k1gFnSc1	komedie
a	a	k8xC	a
přivedl	přivést	k5eAaPmAgMnS	přivést
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
debatu	debata	k1gFnSc4	debata
<g/>
,	,	kIx,	,
osvětlující	osvětlující	k2eAgFnSc4d1	osvětlující
ústřední	ústřední	k2eAgFnSc4d1	ústřední
myšlenku	myšlenka	k1gFnSc4	myšlenka
hry	hra	k1gFnSc2	hra
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
náhlých	náhlý	k2eAgInPc6d1	náhlý
a	a	k8xC	a
překvapivých	překvapivý	k2eAgInPc6d1	překvapivý
zvratech	zvrat	k1gInPc6	zvrat
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
jeho	jeho	k3xOp3gFnPc1	jeho
důkladné	důkladný	k2eAgFnPc1d1	důkladná
předmluvy	předmluva	k1gFnPc1	předmluva
k	k	k7c3	k
tištěným	tištěný	k2eAgFnPc3d1	tištěná
vydáním	vydání	k1gNnSc7	vydání
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
eseje	esej	k1gFnPc1	esej
o	o	k7c6	o
jejich	jejich	k3xOp3gFnPc6	jejich
hlavních	hlavní	k2eAgFnPc6d1	hlavní
myšlenkách	myšlenka	k1gFnPc6	myšlenka
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ostrou	ostrý	k2eAgFnSc4d1	ostrá
kritiku	kritika	k1gFnSc4	kritika
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
zbídačování	zbídačování	k1gNnSc2	zbídačování
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Celým	celý	k2eAgInSc7d1	celý
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
Shaw	Shaw	k1gFnSc2	Shaw
směřoval	směřovat	k5eAaImAgInS	směřovat
ke	k	k7c3	k
kritickému	kritický	k2eAgNnSc3d1	kritické
hodnocení	hodnocení	k1gNnSc3	hodnocení
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vidí	vidět	k5eAaImIp3nS	vidět
pod	pod	k7c7	pod
zorným	zorný	k2eAgInSc7d1	zorný
úhlem	úhel	k1gInSc7	úhel
palčivých	palčivý	k2eAgFnPc2d1	palčivá
sociálních	sociální	k2eAgFnPc2d1	sociální
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
svého	svůj	k3xOyFgNnSc2	svůj
pokrokového	pokrokový	k2eAgNnSc2d1	pokrokové
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Shaw	Shaw	k1gMnSc1	Shaw
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
za	za	k7c4	za
idealismus	idealismus	k1gInSc4	idealismus
i	i	k8xC	i
humanitu	humanita	k1gFnSc4	humanita
jeho	jeho	k3xOp3gNnSc2	jeho
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
svěží	svěží	k2eAgFnSc1d1	svěží
satira	satira	k1gFnSc1	satira
často	často	k6eAd1	často
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
osobitou	osobitý	k2eAgFnSc4d1	osobitá
poetickou	poetický	k2eAgFnSc4d1	poetická
krásu	krása	k1gFnSc4	krása
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
pořádán	pořádán	k2eAgInSc4d1	pořádán
Malvernský	Malvernský	k2eAgInSc4d1	Malvernský
divadelní	divadelní	k2eAgInSc4d1	divadelní
festival	festival	k1gInSc4	festival
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
byly	být	k5eAaImAgInP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
některé	některý	k3yIgFnSc2	některý
jeho	jeho	k3xOp3gFnSc2	jeho
pozdní	pozdní	k2eAgFnSc2d1	pozdní
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
začal	začít	k5eAaPmAgMnS	začít
Shaw	Shaw	k1gFnSc4	Shaw
postupně	postupně	k6eAd1	postupně
ztrácet	ztrácet	k5eAaImF	ztrácet
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
stále	stále	k6eAd1	stále
pomalejším	pomalý	k2eAgNnSc7d2	pomalejší
tempem	tempo	k1gNnSc7	tempo
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
úctyhodném	úctyhodný	k2eAgInSc6d1	úctyhodný
věku	věk	k1gInSc6	věk
94	[number]	k4	94
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
venkovské	venkovský	k2eAgFnSc6d1	venkovská
vile	vila	k1gFnSc6	vila
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c4	v
Ayot	Ayot	k1gInSc4	Ayot
St.	st.	kA	st.
Lawrence	Lawrenec	k1gInSc2	Lawrenec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Pětice	pětice	k1gFnSc1	pětice
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
:	:	kIx,	:
An	An	k1gMnSc1	An
Unsocial	Unsocial	k1gMnSc1	Unsocial
Socialist	socialist	k1gMnSc1	socialist
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
Nespolečenský	společenský	k2eNgMnSc1d1	nespolečenský
socialista	socialista	k1gMnSc1	socialista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
Cashel	Cashel	k1gMnSc1	Cashel
Byron	Byron	k1gMnSc1	Byron
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Profession	Profession	k1gInSc1	Profession
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Povolání	povolání	k1gNnSc4	povolání
Cashela	Cashel	k1gMnSc2	Cashel
Byrona	Byron	k1gMnSc2	Byron
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Cashel	Cashel	k1gMnSc1	Cashel
Byron	Byron	k1gMnSc1	Byron
<g/>
,	,	kIx,	,
profesionál	profesionál	k1gMnSc1	profesionál
<g/>
,	,	kIx,	,
Love	lov	k1gInSc5	lov
Among	Among	k1gInSc4	Among
the	the	k?	the
Artists	Artists	k1gInSc1	Artists
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
mezi	mezi	k7c7	mezi
umělci	umělec	k1gMnPc7	umělec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Irrational	Irrational	k1gFnSc2	Irrational
Knot	knot	k1gInSc1	knot
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Nerozvážný	rozvážný	k2eNgInSc1d1	nerozvážný
sňatek	sňatek	k1gInSc1	sňatek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Immaturity	Immaturit	k1gInPc1	Immaturit
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Nezralost	nezralost	k1gFnSc1	nezralost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Quintessence	Quintessence	k1gFnSc1	Quintessence
of	of	k?	of
Ibsenism	Ibsenism	k1gInSc1	Ibsenism
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Kvintesence	kvintesence	k1gFnSc1	kvintesence
ibsenismu	ibsenismus	k1gInSc2	ibsenismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gFnPc1	esej
obhajující	obhajující	k2eAgNnSc1d1	obhajující
dílo	dílo	k1gNnSc1	dílo
Henrika	Henrik	k1gMnSc2	Henrik
Ibsena	Ibsen	k1gMnSc2	Ibsen
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
sociálně	sociálně	k6eAd1	sociálně
kritických	kritický	k2eAgMnPc2d1	kritický
dramatiků	dramatik	k1gMnPc2	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Perfect	Perfect	k1gInSc1	Perfect
Wagnerite	wagnerit	k1gInSc5	wagnerit
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
wagnerián	wagnerián	k1gMnSc1	wagnerián
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
jsou	být	k5eAaImIp3nP	být
opery	opera	k1gFnPc1	opera
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
kriterium	kriterium	k1gNnSc4	kriterium
umělecké	umělecký	k2eAgFnSc2d1	umělecká
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
,	,	kIx,	,
Fabian	Fabian	k1gMnSc1	Fabian
Essays	Essays	k1gInSc4	Essays
on	on	k3xPp3gMnSc1	on
Socialism	Socialism	k1gMnSc1	Socialism
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Fabiánská	fabiánský	k2eAgNnPc1d1	fabiánský
pojednání	pojednání	k1gNnPc1	pojednání
o	o	k7c6	o
socialismu	socialismus	k1gInSc6	socialismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Common	Common	k1gInSc1	Common
Sense	Sense	k1gFnSc1	Sense
of	of	k?	of
Municipal	Municipal	k1gFnSc1	Municipal
Trading	Trading	k1gInSc1	Trading
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Zdravý	zdravý	k2eAgInSc4d1	zdravý
smysl	smysl	k1gInSc4	smysl
obecní	obecní	k2eAgFnSc2d1	obecní
politiky	politika	k1gFnSc2	politika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
Intelligent	Intelligent	k1gMnSc1	Intelligent
Woman	Woman	k1gMnSc1	Woman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc4	ten
Socialism	Socialism	k1gMnSc1	Socialism
and	and	k?	and
Capitalism	Capitalism	k1gMnSc1	Capitalism
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Průvodce	průvodce	k1gMnSc1	průvodce
inteligentní	inteligentní	k2eAgFnSc2d1	inteligentní
ženy	žena	k1gFnSc2	žena
po	po	k7c6	po
socialimu	socialim	k1gInSc6	socialim
a	a	k8xC	a
kapitalismu	kapitalismus	k1gInSc6	kapitalismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
didaktická	didaktický	k2eAgFnSc1d1	didaktická
próza	próza	k1gFnSc1	próza
<g/>
.	.	kIx.	.
</s>
<s>
Music	Music	k1gMnSc1	Music
In	In	k1gMnSc1	In
London	London	k1gMnSc1	London
1890-1894	[number]	k4	1890-1894
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
Our	Our	k1gMnSc1	Our
Theatres	Theatres	k1gMnSc1	Theatres
in	in	k?	in
the	the	k?	the
Nineties	Nineties	k1gInSc1	Nineties
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gFnSc1	náš
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
o	o	k7c6	o
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
the	the	k?	the
Black	Black	k1gInSc1	Black
Girl	girl	k1gFnSc1	girl
in	in	k?	in
Her	hra	k1gFnPc2	hra
Search	Search	k1gInSc1	Search
for	forum	k1gNnPc2	forum
God	God	k1gFnSc2	God
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
černošské	černošský	k2eAgFnSc2d1	černošská
dívky	dívka	k1gFnSc2	dívka
k	k	k7c3	k
bohu	bůh	k1gMnSc3	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
didaktická	didaktický	k2eAgFnSc1d1	didaktická
próza	próza	k1gFnSc1	próza
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hrdinka	hrdinka	k1gFnSc1	hrdinka
prohlédne	prohlédnout	k5eAaPmIp3nS	prohlédnout
nedokonalost	nedokonalost	k1gFnSc4	nedokonalost
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Everybody	Everyboda	k1gFnPc1	Everyboda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Political	Political	k1gFnSc7	Political
What	Whata	k1gFnPc2	Whata
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
What	Whata	k1gFnPc2	Whata
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Politika	politika	k1gFnSc1	politika
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
Sixteen	Sixteen	k2eAgMnSc1d1	Sixteen
Self	Self	k1gMnSc1	Self
Sketches	Sketches	k1gMnSc1	Sketches
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Šestnáct	šestnáct	k4xCc1	šestnáct
autobiografických	autobiografický	k2eAgFnPc2d1	autobiografická
skic	skica	k1gFnPc2	skica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
An	An	k1gMnSc1	An
Unfinished	Unfinished	k1gMnSc1	Unfinished
Novel	novela	k1gFnPc2	novela
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fragment	fragment	k1gInSc4	fragment
napsaný	napsaný	k2eAgInSc4d1	napsaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Play	play	k0	play
Pleasant	Pleasant	k1gMnSc1	Pleasant
and	and	k?	and
Unpleasant	Unpleasant	k1gMnSc1	Unpleasant
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Hry	hra	k1gFnPc1	hra
utěšené	utěšený	k2eAgFnPc1d1	utěšená
a	a	k8xC	a
neutěšené	utěšený	k2eNgFnPc1d1	neutěšená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tiskem	tisk	k1gInSc7	tisk
vydaná	vydaný	k2eAgFnSc1d1	vydaná
sbírka	sbírka	k1gFnSc1	sbírka
prvních	první	k4xOgFnPc2	první
sedmi	sedm	k4xCc2	sedm
Shawových	Shawův	k2eAgFnPc2d1	Shawova
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc4	tři
úvodní	úvodní	k2eAgNnPc4d1	úvodní
satirická	satirický	k2eAgNnPc4d1	satirické
dramata	drama	k1gNnPc4	drama
označil	označit	k5eAaPmAgInS	označit
autor	autor	k1gMnSc1	autor
jako	jako	k8xC	jako
Hry	hra	k1gFnPc1	hra
neutěšené	utěšený	k2eNgFnPc1d1	neutěšená
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnPc1d1	následující
čtyři	čtyři	k4xCgFnPc1	čtyři
komedie	komedie	k1gFnPc1	komedie
jako	jako	k8xC	jako
Hry	hra	k1gFnPc1	hra
utěšené	utěšený	k2eAgFnPc1d1	utěšená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgInPc6	ten
se	se	k3xPyFc4	se
Shaw	Shaw	k1gMnSc1	Shaw
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
již	již	k6eAd1	již
nesoustředil	soustředit	k5eNaPmAgMnS	soustředit
na	na	k7c4	na
společenské	společenský	k2eAgInPc4d1	společenský
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
psal	psát	k5eAaImAgMnS	psát
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
satiru	satira	k1gFnSc4	satira
na	na	k7c6	na
pošetilosti	pošetilost	k1gFnSc6	pošetilost
morálky	morálka	k1gFnSc2	morálka
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
<s>
Widower	Widower	k1gInSc1	Widower
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Domy	dům	k1gInPc1	dům
pana	pan	k1gMnSc2	pan
Sartoria	Sartorium	k1gNnSc2	Sartorium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shawova	Shawův	k2eAgFnSc1d1	Shawova
dramatická	dramatický	k2eAgFnSc1d1	dramatická
prvotina	prvotina	k1gFnSc1	prvotina
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Neutěšených	utěšený	k2eNgFnPc2d1	neutěšená
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
Shaw	Shaw	k1gFnSc6	Shaw
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
souvislost	souvislost	k1gFnSc4	souvislost
sexuální	sexuální	k2eAgFnSc2d1	sexuální
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Philander	Philander	k1gInSc1	Philander
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Záletník	záletník	k1gMnSc1	záletník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Záletník	záletník	k1gMnSc1	záletník
Leonard	Leonard	k1gMnSc1	Leonard
nepříliš	příliš	k6eNd1	příliš
zdařilá	zdařilý	k2eAgFnSc1d1	zdařilá
druhá	druhý	k4xOgFnSc1	druhý
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Neutěšených	utěšený	k2eNgFnPc2d1	neutěšená
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
hrána	hrát	k5eAaImNgNnP	hrát
až	až	k9	až
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
problematiku	problematika	k1gFnSc4	problematika
sexu	sex	k1gInSc2	sex
a	a	k8xC	a
také	také	k9	také
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
nedůslednou	důsledný	k2eNgFnSc4d1	nedůsledná
ženskou	ženský	k2eAgFnSc4d1	ženská
emancipaci	emancipace	k1gFnSc4	emancipace
<g/>
.	.	kIx.	.
</s>
<s>
Mrs	Mrs	k?	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Warren	Warrna	k1gFnPc2	Warrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Profession	Profession	k1gInSc1	Profession
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Živnost	živnost	k1gFnSc1	živnost
paní	paní	k1gFnSc2	paní
Warrenové	Warrenová	k1gFnSc2	Warrenová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
dílo	dílo	k1gNnSc1	dílo
Shawova	Shawův	k2eAgNnSc2d1	Shawův
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
a	a	k8xC	a
poslední	poslední	k2eAgInPc4d1	poslední
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Neutěšených	utěšený	k2eNgFnPc2d1	neutěšená
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
provozování	provozování	k1gNnSc1	provozování
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Arms	Arms	k1gInSc1	Arms
and	and	k?	and
the	the	k?	the
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Válka	válka	k1gFnSc1	válka
a	a	k8xC	a
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Utěšených	utěšený	k2eAgFnPc2d1	utěšená
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
satirou	satira	k1gFnSc7	satira
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
naivní	naivní	k2eAgFnSc2d1	naivní
představy	představa	k1gFnSc2	představa
o	o	k7c6	o
válečném	válečný	k2eAgNnSc6d1	válečné
hrdinství	hrdinství	k1gNnSc6	hrdinství
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
za	za	k7c2	za
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
bulharsko-srbské	bulharskorbský	k2eAgFnSc2d1	bulharsko-srbský
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
Čokoládový	čokoládový	k2eAgMnSc1d1	čokoládový
hrdina	hrdina	k1gMnSc1	hrdina
je	být	k5eAaImIp3nS	být
převodem	převod	k1gInSc7	převod
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
operety	opereta	k1gFnSc2	opereta
Der	drát	k5eAaImRp2nS	drát
tapfere	tapfrat	k5eAaPmIp3nS	tapfrat
Soldat	Soldat	k1gMnSc7	Soldat
Oscara	Oscar	k1gMnSc2	Oscar
Strause	Strause	k1gFnSc2	Strause
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
The	The	k1gFnPc2	The
Chocolate	Chocolat	k1gInSc5	Chocolat
Soldier	Soldiero	k1gNnPc2	Soldiero
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
srbský	srbský	k2eAgMnSc1d1	srbský
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
čokoládě	čokoláda	k1gFnSc3	čokoláda
před	před	k7c7	před
bojem	boj	k1gInSc7	boj
<g/>
.	.	kIx.	.
</s>
<s>
Candida	Candida	k1gFnSc1	Candida
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Utěšených	utěšený	k2eAgFnPc2d1	utěšená
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
hrána	hrát	k5eAaImNgFnS	hrát
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
velice	velice	k6eAd1	velice
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
hitem	hit	k1gInSc7	hit
newyorské	newyorský	k2eAgFnSc2d1	newyorská
divadelní	divadelní	k2eAgFnSc2d1	divadelní
sezóny	sezóna	k1gFnSc2	sezóna
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
satiru	satira	k1gFnSc4	satira
na	na	k7c4	na
romantické	romantický	k2eAgNnSc4d1	romantické
pojetí	pojetí	k1gNnSc4	pojetí
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
of	of	k?	of
Destiny	Destina	k1gFnSc2	Destina
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Osudový	osudový	k2eAgMnSc1d1	osudový
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Utěšených	utěšený	k2eAgFnPc2d1	utěšená
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
o	o	k7c6	o
mladém	mladý	k2eAgMnSc6d1	mladý
Napoleonovi	Napoleon	k1gMnSc6	Napoleon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
italského	italský	k2eAgNnSc2d1	italské
tažení	tažení	k1gNnSc2	tažení
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
-	-	kIx~	-
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
prožívá	prožívat	k5eAaImIp3nS	prožívat
milostnou	milostný	k2eAgFnSc4d1	milostná
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
jde	jít	k5eAaImIp3nS	jít
dál	daleko	k6eAd2	daleko
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
kariérou	kariéra	k1gFnSc7	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
satirou	satira	k1gFnSc7	satira
na	na	k7c4	na
romantické	romantický	k2eAgNnSc4d1	romantické
pojetí	pojetí	k1gNnSc4	pojetí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
Napoleona	Napoleon	k1gMnSc2	Napoleon
však	však	k9	však
Shaw	Shaw	k1gMnSc1	Shaw
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
jako	jako	k8xC	jako
realistického	realistický	k2eAgMnSc4d1	realistický
politika	politik	k1gMnSc4	politik
a	a	k8xC	a
obdivuhodného	obdivuhodný	k2eAgMnSc4d1	obdivuhodný
muže	muž	k1gMnSc4	muž
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnSc2d1	silná
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
You	You	k?	You
Never	Never	k1gMnSc1	Never
Can	Can	k1gMnSc1	Can
Tell	Tell	k1gMnSc1	Tell
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
nikdy	nikdy	k6eAd1	nikdy
neví	vědět	k5eNaImIp3nS	vědět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Utěšených	utěšený	k2eAgFnPc2d1	utěšená
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
hrána	hrát	k5eAaImNgFnS	hrát
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
komedii	komedie	k1gFnSc4	komedie
líčící	líčící	k2eAgInPc4d1	líčící
paradoxy	paradox	k1gInPc4	paradox
v	v	k7c6	v
milostných	milostný	k2eAgInPc6d1	milostný
a	a	k8xC	a
rodinných	rodinný	k2eAgInPc6d1	rodinný
vztazích	vztah	k1gInPc6	vztah
a	a	k8xC	a
ukazující	ukazující	k2eAgFnPc4d1	ukazující
na	na	k7c4	na
pošetilost	pošetilost	k1gFnSc4	pošetilost
romantického	romantický	k2eAgNnSc2d1	romantické
pojetí	pojetí	k1gNnSc2	pojetí
námluv	námluva	k1gFnPc2	námluva
a	a	k8xC	a
dvoření	dvoření	k1gNnPc2	dvoření
<g/>
.	.	kIx.	.
</s>
<s>
Three	Threat	k5eAaPmIp3nS	Threat
Plays	Plays	k1gInSc1	Plays
for	forum	k1gNnPc2	forum
Puritans	Puritans	k1gInSc1	Puritans
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
hry	hra	k1gFnPc1	hra
pro	pro	k7c4	pro
puritány	puritán	k1gMnPc4	puritán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
tiskem	tisk	k1gInSc7	tisk
vydaná	vydaný	k2eAgFnSc1d1	vydaná
sbírka	sbírka	k1gFnSc1	sbírka
tří	tři	k4xCgFnPc2	tři
dalších	další	k2eAgFnPc2d1	další
Shawových	Shawův	k2eAgFnPc2d1	Shawova
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Puritáni	puritán	k1gMnPc1	puritán
nebyli	být	k5eNaImAgMnP	být
pro	pro	k7c4	pro
Shawa	Shaw	k1gInSc2	Shaw
náboženští	náboženský	k2eAgMnPc1d1	náboženský
horlivci	horlivec	k1gMnPc1	horlivec
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
střízliví	střízlivý	k2eAgMnPc1d1	střízlivý
realisté	realista	k1gMnPc1	realista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
on	on	k3xPp3gMnSc1	on
zavrhovali	zavrhovat	k5eAaImAgMnP	zavrhovat
sentiment	sentiment	k1gInSc4	sentiment
i	i	k8xC	i
sebeklam	sebeklam	k1gInSc4	sebeklam
a	a	k8xC	a
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
osvobozující	osvobozující	k2eAgNnSc4d1	osvobozující
poznání	poznání	k1gNnSc4	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Devil	Devil	k1gInSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Disciple	Disciple	k1gFnSc7	Disciple
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
kvítko	kvítko	k1gNnSc1	kvítko
nebo	nebo	k8xC	nebo
Ďáblův	ďáblův	k2eAgMnSc1d1	ďáblův
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
odehrávající	odehrávající	k2eAgFnSc1d1	odehrávající
se	se	k3xPyFc4	se
během	během	k7c2	během
Americké	americký	k2eAgFnSc2d1	americká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
and	and	k?	and
Cleopatra	Cleopatra	k1gFnSc1	Cleopatra
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
parodii	parodie	k1gFnSc4	parodie
tragédie	tragédie	k1gFnSc2	tragédie
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
Antonius	Antonius	k1gMnSc1	Antonius
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
,	,	kIx,	,
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
proti	proti	k7c3	proti
idealizujícímu	idealizující	k2eAgNnSc3d1	idealizující
pojetí	pojetí	k1gNnSc3	pojetí
velké	velký	k2eAgFnSc2d1	velká
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Captain	Captain	k2eAgInSc1d1	Captain
Brassbound	Brassbound	k1gInSc1	Brassbound
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Conversion	Conversion	k1gInSc1	Conversion
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Obrácení	obrácení	k1gNnSc4	obrácení
kapitána	kapitán	k1gMnSc2	kapitán
Brassbounda	Brassbound	k1gMnSc2	Brassbound
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
uvedená	uvedený	k2eAgFnSc1d1	uvedená
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
je	být	k5eAaImIp3nS	být
parodií	parodie	k1gFnSc7	parodie
exotických	exotický	k2eAgFnPc2d1	exotická
melodramat	melodramat	k?	melodramat
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
britští	britský	k2eAgMnPc1d1	britský
vojáci	voják	k1gMnPc1	voják
často	často	k6eAd1	často
zachraňují	zachraňovat	k5eAaImIp3nP	zachraňovat
bílé	bílý	k2eAgFnPc1d1	bílá
dámy	dáma	k1gFnPc1	dáma
před	před	k7c7	před
nebezpečnými	bezpečný	k2eNgMnPc7d1	nebezpečný
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Admirable	Admirable	k1gFnSc1	Admirable
Bashville	Bashville	k1gFnSc1	Bashville
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Vzorný	vzorný	k2eAgMnSc1d1	vzorný
sluha	sluha	k1gMnSc1	sluha
Bashville	Bashville	k1gFnSc2	Bashville
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
.	.	kIx.	.
</s>
<s>
Man	Man	k1gMnSc1	Man
and	and	k?	and
Superman	superman	k1gMnSc1	superman
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
nadčlověk	nadčlověk	k1gMnSc1	nadčlověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofická	filozofický	k2eAgFnSc1d1	filozofická
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
prvně	prvně	k?	prvně
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výrazem	výraz	k1gInSc7	výraz
Shawova	Shawův	k2eAgNnSc2d1	Shawův
pojetí	pojetí	k1gNnSc2	pojetí
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc7	jehož
hlavními	hlavní	k2eAgFnPc7d1	hlavní
příčinami	příčina	k1gFnPc7	příčina
jsou	být	k5eAaImIp3nP	být
Nietschova	Nietschův	k2eAgFnSc1d1	Nietschův
vůle	vůle	k1gFnSc1	vůle
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
bergsonovská	bergsonovský	k2eAgFnSc1d1	bergsonovský
životní	životní	k2eAgFnSc1d1	životní
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
boji	boj	k1gInSc3	boj
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Bull	bulla	k1gFnPc2	bulla
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Other	Other	k1gInSc1	Other
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Druhý	druhý	k4xOgInSc1	druhý
ostrov	ostrov	k1gInSc1	ostrov
Johna	John	k1gMnSc2	John
Bulla	bulla	k1gFnSc1	bulla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
komedie	komedie	k1gFnSc1	komedie
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
hře	hra	k1gFnSc6	hra
paradoxů	paradox	k1gInPc2	paradox
<g/>
.	.	kIx.	.
</s>
<s>
How	How	k?	How
He	he	k0	he
Lied	Lied	k1gInSc1	Lied
to	ten	k3xDgNnSc1	ten
Her	hra	k1gFnPc2	hra
Husband	Husband	k1gInSc1	Husband
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Jak	jak	k8xS	jak
lhal	lhát	k5eAaImAgMnS	lhát
jejímu	její	k3xOp3gMnSc3	její
manželu	manžel	k1gMnSc3	manžel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Doctor	Doctor	k1gMnSc1	Doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dilemma	Dilemmum	k1gNnPc1	Dilemmum
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
rozpacích	rozpak	k1gInPc6	rozpak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
Shaw	Shaw	k1gMnSc1	Shaw
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
odpudivý	odpudivý	k2eAgInSc4d1	odpudivý
obraz	obraz	k1gInSc4	obraz
lékaře	lékař	k1gMnSc2	lékař
podvodníka	podvodník	k1gMnSc2	podvodník
<g/>
,	,	kIx,	,
kontrastující	kontrastující	k2eAgFnSc1d1	kontrastující
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
tuberkulózního	tuberkulózní	k2eAgMnSc2d1	tuberkulózní
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
amorálnost	amorálnost	k1gFnSc4	amorálnost
<g/>
,	,	kIx,	,
ukázat	ukázat	k5eAaPmF	ukázat
lidem	lid	k1gInSc7	lid
jejich	jejich	k3xOp3gFnSc4	jejich
pravou	pravý	k2eAgFnSc4d1	pravá
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Major	major	k1gMnSc1	major
Barbara	barbar	k1gMnSc2	barbar
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Majorka	majorka	k1gFnSc1	majorka
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociálně	sociálně	k6eAd1	sociálně
laděná	laděný	k2eAgFnSc1d1	laděná
komedie	komedie	k1gFnSc1	komedie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
problematikou	problematika	k1gFnSc7	problematika
filantropie	filantropie	k1gFnSc2	filantropie
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
majorka	majorka	k1gFnSc1	majorka
Armády	armáda	k1gFnSc2	armáda
spásy	spása	k1gFnSc2	spása
se	se	k3xPyFc4	se
charitativní	charitativní	k2eAgFnPc1d1	charitativní
činnosti	činnost	k1gFnPc1	činnost
snaží	snažit	k5eAaImIp3nP	snažit
protestovat	protestovat	k5eAaBmF	protestovat
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
zbrojařskému	zbrojařský	k2eAgMnSc3d1	zbrojařský
milionáři	milionář	k1gMnSc3	milionář
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
filantropie	filantropie	k1gFnSc1	filantropie
nemůže	moct	k5eNaImIp3nS	moct
chudobu	chudoba	k1gFnSc4	chudoba
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
nesentimentální	sentimentální	k2eNgMnSc1d1	nesentimentální
otec	otec	k1gMnSc1	otec
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
nakonec	nakonec	k6eAd1	nakonec
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
chudobě	chudoba	k1gFnSc3	chudoba
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
bojovat	bojovat	k5eAaImF	bojovat
jen	jen	k9	jen
intelektuální	intelektuální	k2eAgFnSc7d1	intelektuální
kontrolou	kontrola	k1gFnSc7	kontrola
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Getting	Getting	k1gInSc1	Getting
Married	Married	k1gInSc1	Married
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Ženění	ženění	k1gNnSc2	ženění
a	a	k8xC	a
vdávání	vdávání	k1gNnSc2	vdávání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
manželství	manželství	k1gNnSc6	manželství
a	a	k8xC	a
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Glimpse	Glimpse	k1gFnSc1	Glimpse
of	of	k?	of
Reality	realita	k1gFnSc2	realita
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
Záblesk	záblesk	k1gInSc1	záblesk
pravdy	pravda	k1gFnSc2	pravda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
odehrávající	odehrávající	k2eAgFnSc2d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Shewing-up	Shewingp	k1gInSc1	Shewing-up
of	of	k?	of
Blanco	blanco	k2eAgInSc1d1	blanco
Posnet	Posnet	k1gInSc1	Posnet
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
Prohlédnutí	prohlédnutí	k1gNnSc1	prohlédnutí
Blanco	blanco	k2eAgNnSc2d1	blanco
Posneta	Posneto	k1gNnSc2	Posneto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
,	,	kIx,	,
Misalliance	Misalliance	k1gFnSc1	Misalliance
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Mesaliance	mesaliance	k1gFnSc1	mesaliance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
Shaw	Shaw	k1gFnSc6	Shaw
dále	daleko	k6eAd2	daleko
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
manželství	manželství	k1gNnSc4	manželství
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Ženění	ženění	k1gNnSc2	ženění
a	a	k8xC	a
vdávání	vdávání	k1gNnSc2	vdávání
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
hry	hra	k1gFnSc2	hra
znamená	znamenat	k5eAaImIp3nS	znamenat
společensky	společensky	k6eAd1	společensky
nerovný	rovný	k2eNgInSc1d1	nerovný
sňatek	sňatek	k1gInSc1	sňatek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dark	Dark	k1gMnSc1	Dark
Lady	Lada	k1gFnSc2	Lada
of	of	k?	of
the	the	k?	the
Sonnets	Sonnets	k1gInSc1	Sonnets
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
dáma	dáma	k1gFnSc1	dáma
sonetů	sonet	k1gInPc2	sonet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vtipná	vtipný	k2eAgFnSc1d1	vtipná
a	a	k8xC	a
intelektuálně	intelektuálně	k6eAd1	intelektuálně
okouzlující	okouzlující	k2eAgFnSc1d1	okouzlující
komedie	komedie	k1gFnSc1	komedie
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
tajemnou	tajemný	k2eAgFnSc7d1	tajemná
černou	černý	k2eAgFnSc7d1	černá
dámou	dáma	k1gFnSc7	dáma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
věnována	věnován	k2eAgFnSc1d1	věnována
část	část	k1gFnSc1	část
Sonetů	sonet	k1gInPc2	sonet
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Fanny	Fanna	k1gFnPc1	Fanna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
First	First	k1gInSc1	First
Play	play	k0	play
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Fannina	Fannin	k2eAgFnSc1d1	Fannin
první	první	k4xOgFnSc1	první
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
představení	představení	k1gNnSc1	představení
anonymní	anonymní	k2eAgFnSc2d1	anonymní
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
mudrování	mudrování	k1gNnSc2	mudrování
čtyř	čtyři	k4xCgMnPc2	čtyři
prominentních	prominentní	k2eAgMnPc2d1	prominentní
kritiků	kritik	k1gMnPc2	kritik
snažících	snažící	k2eAgMnPc2d1	snažící
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
autora	autor	k1gMnSc4	autor
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
není	být	k5eNaImIp3nS	být
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
titulní	titulní	k2eAgFnSc1d1	titulní
hrdinka	hrdinka	k1gFnSc1	hrdinka
<g/>
.	.	kIx.	.
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snad	snad	k9	snad
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
Shawova	Shawův	k2eAgFnSc1d1	Shawova
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
viktoriánské	viktoriánský	k2eAgFnPc4d1	viktoriánská
představy	představa	k1gFnPc4	představa
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
hraná	hraný	k2eAgFnSc1d1	hraná
nejprve	nejprve	k6eAd1	nejprve
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
i	i	k8xC	i
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
tiskem	tisk	k1gInSc7	tisk
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Androcles	Androcles	k1gInSc1	Androcles
and	and	k?	and
the	the	k?	the
Lion	Lion	k1gMnSc1	Lion
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Androkles	Androkles	k1gMnSc1	Androkles
a	a	k8xC	a
lev	lev	k1gMnSc1	lev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
římského	římský	k2eAgNnSc2d1	římské
pronásledování	pronásledování	k1gNnSc2	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
se	s	k7c7	s
Shawovou	Shawový	k2eAgFnSc7d1	Shawový
předmluvou	předmluva	k1gFnSc7	předmluva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výsledky	výsledek	k1gInPc4	výsledek
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
autorova	autorův	k2eAgInSc2d1	autorův
výzkumu	výzkum	k1gInSc2	výzkum
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Great	Great	k1gInSc1	Great
Catherine	Catherin	k1gInSc5	Catherin
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Veliká	veliký	k2eAgFnSc1d1	veliká
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Press	Press	k1gInSc1	Press
Cuttings	Cuttings	k1gInSc1	Cuttings
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Výstřižky	výstřižek	k1gInPc4	výstřižek
z	z	k7c2	z
novin	novina	k1gFnPc2	novina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Music	Music	k1gMnSc1	Music
Cure	Cure	k1gInSc1	Cure
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Léčení	léčení	k1gNnSc2	léčení
hudbou	hudba	k1gFnSc7	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Augustus	Augustus	k1gInSc1	Augustus
Does	Does	k1gInSc1	Does
His	his	k1gNnSc6	his
Bit	bit	k1gInSc1	bit
((	((	k?	((
<g/>
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Jak	jak	k8xS	jak
August	August	k1gMnSc1	August
vykonal	vykonat	k5eAaPmAgMnS	vykonat
svou	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořká	hořký	k2eAgFnSc1d1	hořká
společenská	společenský	k2eAgFnSc1d1	společenská
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
Shawovy	Shawův	k2eAgInPc1d1	Shawův
skeptické	skeptický	k2eAgInPc1d1	skeptický
názory	názor	k1gInPc1	názor
na	na	k7c4	na
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
lidstvo	lidstvo	k1gNnSc4	lidstvo
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Back	Back	k6eAd1	Back
to	ten	k3xDgNnSc4	ten
Methuselah	Methuselah	k1gMnSc1	Methuselah
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Methusalemovi	Methusalem	k1gMnSc3	Methusalem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofická	filozofický	k2eAgFnSc1d1	filozofická
utopie	utopie	k1gFnSc1	utopie
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
pěti	pět	k4xCc2	pět
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
zobrazující	zobrazující	k2eAgMnSc1d1	zobrazující
nietzscheovského	nietzscheovského	k2eAgMnSc1d1	nietzscheovského
nadčlověka	nadčlověk	k1gMnSc4	nadčlověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
dlouhověkost	dlouhověkost	k1gFnSc1	dlouhověkost
umožní	umožnit	k5eAaPmIp3nS	umožnit
lidstvu	lidstvo	k1gNnSc6	lidstvo
vyřešit	vyřešit	k5eAaPmF	vyřešit
naléhavé	naléhavý	k2eAgInPc4d1	naléhavý
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
<g/>
.	.	kIx.	.
</s>
<s>
Saint	Saint	k1gMnSc1	Saint
Joan	Joan	k1gMnSc1	Joan
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
hra	hra	k1gFnSc1	hra
o	o	k7c6	o
francouzské	francouzský	k2eAgFnSc6d1	francouzská
národní	národní	k2eAgFnSc6d1	národní
hrdince	hrdinka	k1gFnSc6	hrdinka
Johance	Johanka	k1gFnSc6	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
představitelkou	představitelka	k1gFnSc7	představitelka
zdravého	zdravý	k2eAgInSc2d1	zdravý
rozumu	rozum	k1gInSc2	rozum
rebelujícího	rebelující	k2eAgInSc2d1	rebelující
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
<g/>
,	,	kIx,	,
dogmatismu	dogmatismus	k1gInSc2	dogmatismus
a	a	k8xC	a
autoritářství	autoritářství	k1gNnSc2	autoritářství
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Apple	Apple	kA	Apple
Cart	Cart	k1gInSc1	Cart
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Trakař	trakař	k1gInSc1	trakař
jablek	jablko	k1gNnPc2	jablko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
paradoxech	paradox	k1gInPc6	paradox
britské	britský	k2eAgFnSc2d1	britská
demokracie	demokracie	k1gFnSc2	demokracie
i	i	k8xC	i
angloamerických	angloamerický	k2eAgInPc2d1	angloamerický
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Americký	americký	k2eAgMnSc1d1	americký
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Too	Too	k?	Too
True	Tru	k1gFnSc2	Tru
to	ten	k3xDgNnSc1	ten
be	be	k?	be
Good	Good	k1gInSc1	Good
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Příliš	příliš	k6eAd1	příliš
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
navazující	navazující	k2eAgNnSc4d1	navazující
tematicky	tematicky	k6eAd1	tematicky
na	na	k7c4	na
Dům	dům	k1gInSc4	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Rocks	Rocks	k1gInSc1	Rocks
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Na	na	k7c4	na
úskalí	úskalí	k1gNnSc4	úskalí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Six	Six	k1gFnSc2	Six
of	of	k?	of
Calais	Calais	k1gNnSc1	Calais
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Měšťané	měšťan	k1gMnPc1	měšťan
calaisští	calaisštit	k5eAaPmIp3nP	calaisštit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Shawa	Shawa	k1gMnSc1	Shawa
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
válečný	válečný	k2eAgInSc4d1	válečný
příběh	příběh	k1gInSc4	příběh
francouzského	francouzský	k2eAgMnSc2d1	francouzský
kronikáře	kronikář	k1gMnSc2	kronikář
Jeana	Jean	k1gMnSc2	Jean
Froissarta	Froissart	k1gMnSc2	Froissart
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Millionairess	Millionairess	k1gInSc1	Millionairess
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Milionářka	milionářka	k1gFnSc1	milionářka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fraška	fraška	k1gFnSc1	fraška
o	o	k7c4	o
působení	působení	k1gNnSc4	působení
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc4	obraz
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
fašističtí	fašistický	k2eAgMnPc1d1	fašistický
diktátoři	diktátor	k1gMnPc1	diktátor
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Good	Good	k1gMnSc1	Good
King	King	k1gMnSc1	King
Charles	Charles	k1gMnSc1	Charles
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Golden	Goldno	k1gNnPc2	Goldno
Days	Days	k1gInSc1	Days
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Za	za	k7c2	za
zlatých	zlatý	k2eAgInPc2d1	zlatý
časů	čas	k1gInPc2	čas
dobrého	dobrý	k2eAgMnSc2d1	dobrý
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
odehrávající	odehrávající	k2eAgFnSc1d1	odehrávající
se	se	k3xPyFc4	se
za	za	k7c4	za
restaurace	restaurace	k1gFnPc4	restaurace
monarchie	monarchie	k1gFnSc2	monarchie
po	po	k7c6	po
anglické	anglický	k2eAgFnSc6d1	anglická
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Buoyant	Buoyant	k1gInSc1	Buoyant
Billions	Billions	k1gInSc1	Billions
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Buoyantovy	Buoyantův	k2eAgFnPc4d1	Buoyantův
miliardy	miliarda	k4xCgFnSc2	miliarda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
bouyant	bouyanta	k1gFnPc2	bouyanta
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
optimistický	optimistický	k2eAgInSc4d1	optimistický
nebo	nebo	k8xC	nebo
lehkomyslný	lehkomyslný	k2eAgInSc4d1	lehkomyslný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shakes	Shakes	k1gMnSc1	Shakes
versus	versus	k7c1	versus
Shav	Shav	k1gMnSc1	Shav
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Shakes	Shakes	k1gInSc1	Shakes
proti	proti	k7c3	proti
Shavovi	Shava	k1gMnSc3	Shava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
Shawovo	Shawův	k2eAgNnSc4d1	Shawův
dokončené	dokončený	k2eAgNnSc4d1	dokončené
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
loutková	loutkový	k2eAgFnSc1d1	loutková
groteska	groteska	k1gFnSc1	groteska
prvně	prvně	k?	prvně
hraná	hraný	k2eAgFnSc1d1	hraná
na	na	k7c6	na
Malvernském	Malvernský	k2eAgInSc6d1	Malvernský
festivalu	festival	k1gInSc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
How	How	k?	How
He	he	k0	he
Lied	Lied	k1gInSc1	Lied
to	ten	k3xDgNnSc1	ten
Her	hra	k1gFnPc2	hra
Husband	Husband	k1gInSc1	Husband
(	(	kIx(	(
<g/>
Jak	jak	k8xC	jak
lhal	lhát	k5eAaImAgMnS	lhát
jejímu	její	k3xOp3gMnSc3	její
manželu	manžel	k1gMnSc3	manžel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Cecil	Cecil	k1gMnSc1	Cecil
Lewis	Lewis	k1gInSc1	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Arms	Arms	k1gInSc1	Arms
and	and	k?	and
the	the	k?	the
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
Čokoládový	čokoládový	k2eAgMnSc1d1	čokoládový
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Cecil	Cecil	k1gMnSc1	Cecil
Lewis	Lewis	k1gInSc1	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Erich	Erich	k1gMnSc1	Erich
Engel	Engel	k1gMnSc1	Engel
<g/>
.	.	kIx.	.
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ludwig	Ludwig	k1gMnSc1	Ludwig
Berger	Berger	k1gMnSc1	Berger
<g/>
.	.	kIx.	.
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Anthony	Anthona	k1gFnSc2	Anthona
Asquith	Asquitha	k1gFnPc2	Asquitha
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Leslie	Leslie	k1gFnSc2	Leslie
Howard	Howard	k1gInSc1	Howard
a	a	k8xC	a
Wendy	Wenda	k1gFnSc2	Wenda
Hillerová	Hillerová	k1gFnSc1	Hillerová
<g/>
.	.	kIx.	.
</s>
<s>
Major	major	k1gMnSc1	major
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Gabriel	Gabriel	k1gMnSc1	Gabriel
Pascal	Pascal	k1gMnSc1	Pascal
<g/>
.	.	kIx.	.
</s>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
and	and	k?	and
Cleopatra	Cleopatra	k1gFnSc1	Cleopatra
(	(	kIx(	(
<g/>
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Gabriel	Gabriel	k1gMnSc1	Gabriel
Pascal	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Claude	Claud	k1gInSc5	Claud
Rains	Rains	k1gInSc4	Rains
a	a	k8xC	a
Vivien	Vivien	k1gInSc4	Vivien
Leighová	Leighový	k2eAgFnSc1d1	Leighová
<g/>
.	.	kIx.	.
</s>
<s>
Saint	Saint	k1gMnSc1	Saint
Joan	Joan	k1gMnSc1	Joan
(	(	kIx(	(
<g/>
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Otto	Otto	k1gMnSc1	Otto
Preminger	Preminger	k1gMnSc1	Preminger
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Jean	Jean	k1gMnSc1	Jean
Sebergová	Sebergový	k2eAgFnSc1d1	Sebergová
<g/>
,	,	kIx,	,
scenáristická	scenáristický	k2eAgFnSc1d1	scenáristická
úprava	úprava	k1gFnSc1	úprava
Graham	graham	k1gInSc1	graham
Greene	Green	k1gInSc5	Green
<g/>
.	.	kIx.	.
</s>
<s>
Doctor	Doctor	k1gInSc1	Doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dilemma	Dilemm	k1gMnSc4	Dilemm
(	(	kIx(	(
<g/>
Lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
rozpacích	rozpak	k1gInPc6	rozpak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Anthony	Anthona	k1gFnSc2	Anthona
Asquith	Asquitha	k1gFnPc2	Asquitha
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Devil	Devil	k1gInSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Disciple	Disciple	k1gMnSc7	Disciple
(	(	kIx(	(
<g/>
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Guy	Guy	k1gMnSc1	Guy
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Burt	Burt	k1gMnSc1	Burt
Lancaster	Lancaster	k1gMnSc1	Lancaster
<g/>
,	,	kIx,	,
Kirk	Kirk	k1gMnSc1	Kirk
Douglas	Douglas	k1gMnSc1	Douglas
a	a	k8xC	a
Laurence	Laurence	k1gFnSc1	Laurence
Olivier	Olivira	k1gFnPc2	Olivira
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Millionaires	Millionaires	k1gInSc1	Millionaires
(	(	kIx(	(
<g/>
Milionářka	milionářka	k1gFnSc1	milionářka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Anthony	Anthona	k1gFnSc2	Anthona
Asquith	Asquitha	k1gFnPc2	Asquitha
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Sophia	Sophia	k1gFnSc1	Sophia
Lorenová	Lorenová	k1gFnSc1	Lorenová
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Sellers	Sellers	k1gInSc1	Sellers
a	a	k8xC	a
Vittorio	Vittorio	k1gNnSc1	Vittorio
de	de	k?	de
Sica	Sic	k1gInSc2	Sic
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
Fair	fair	k6eAd1	fair
Lady	lady	k1gFnPc7	lady
<g/>
,	,	kIx,	,
USA	USA	kA	USA
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
George	George	k1gNnSc2	George
Cukor	Cukora	k1gFnPc2	Cukora
<g/>
,	,	kIx,	,
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
zfilmování	zfilmování	k1gNnSc1	zfilmování
amerického	americký	k2eAgInSc2d1	americký
muzikálu	muzikál	k1gInSc2	muzikál
podle	podle	k7c2	podle
Shawovy	Shawův	k2eAgFnSc2d1	Shawova
komedie	komedie	k1gFnSc2	komedie
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
rolích	role	k1gFnPc6	role
Rex	Rex	k1gMnSc1	Rex
Harrison	Harrison	k1gMnSc1	Harrison
a	a	k8xC	a
Audrey	Audre	k2eAgFnPc1d1	Audre
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
<g/>
.	.	kIx.	.
</s>
<s>
Great	Great	k1gInSc1	Great
Catherine	Catherin	k1gInSc5	Catherin
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Gordon	Gordon	k1gMnSc1	Gordon
Flemyng	Flemyng	k1gMnSc1	Flemyng
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
rolích	role	k1gFnPc6	role
Peter	Peter	k1gMnSc1	Peter
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Toole	Tool	k1gMnSc2	Tool
a	a	k8xC	a
Jeanne	Jeann	k1gInSc5	Jeann
Moreau	Moreaum	k1gNnSc3	Moreaum
<g/>
.	.	kIx.	.
С	С	k?	С
б	б	k?	б
(	(	kIx(	(
<g/>
Dům	dům	k1gInSc1	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Aleksandr	Aleksandr	k1gInSc1	Aleksandr
Sokurov	Sokurov	k1gInSc1	Sokurov
<g/>
,	,	kIx,	,
Fabianská	Fabianský	k2eAgNnPc1d1	Fabianský
pojednání	pojednání	k1gNnPc1	pojednání
o	o	k7c6	o
socialismu	socialismus	k1gInSc3	socialismus
<g/>
,	,	kIx,	,
Rozhledy	rozhled	k1gInPc4	rozhled
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Pelcl	Pelcl	k1gMnSc1	Pelcl
<g/>
,	,	kIx,	,
Pekelník	pekelník	k1gMnSc1	pekelník
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
vydal	vydat	k5eAaPmAgMnS	vydat
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Cashel	Cashel	k1gMnSc1	Cashel
Byron	Byron	k1gMnSc1	Byron
<g/>
,	,	kIx,	,
profesionál	profesionál	k1gMnSc1	profesionál
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Živnost	živnost	k1gFnSc1	živnost
paní	paní	k1gFnSc2	paní
Warrenové	Warrenová	k1gFnSc2	Warrenová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Gustav	Gustav	k1gMnSc1	Gustav
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
nadčlověk	nadčlověk	k1gMnSc1	nadčlověk
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gMnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Mesaliance	mesaliance	k1gFnSc1	mesaliance
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Kapitoly	kapitola	k1gFnPc4	kapitola
o	o	k7c4	o
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Kočí	Kočí	k1gFnPc1	Kočí
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
Hoch	hoch	k1gMnSc1	hoch
<g/>
,	,	kIx,	,
Fannina	Fannin	k2eAgFnSc1d1	Fannin
první	první	k4xOgFnSc1	první
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Čokoládový	čokoládový	k2eAgMnSc1d1	čokoládový
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
ČDLJ	ČDLJ	kA	ČDLJ
1954	[number]	k4	1954
a	a	k8xC	a
Orbis	orbis	k1gInSc1	orbis
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
rozpacích	rozpak	k1gInPc6	rozpak
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
a	a	k8xC	a
Emil	Emil	k1gMnSc1	Emil
Seeland	Seelanda	k1gFnPc2	Seelanda
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Zdravý	zdravý	k2eAgInSc4d1	zdravý
smysl	smysl	k1gInSc4	smysl
obecní	obecní	k2eAgFnSc2d1	obecní
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Svěcený	svěcený	k2eAgMnSc1d1	svěcený
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Bohumila	Bohumila	k1gFnSc1	Bohumila
Kubertová	Kubertová	k1gFnSc1	Kubertová
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Pflanzerová	Pflanzerová	k1gFnSc1	Pflanzerová
a	a	k8xC	a
Klára	Klára	k1gFnSc1	Klára
Pražáková	Pražáková	k1gFnSc1	Pražáková
<g/>
,	,	kIx,	,
Povolání	povolání	k1gNnSc1	povolání
Cashela	Cashel	k1gMnSc2	Cashel
Byrona	Byron	k1gMnSc2	Byron
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g/>
,	,	kIx,	,
Průvodce	průvodce	k1gMnSc1	průvodce
inteligentní	inteligentní	k2eAgFnSc2d1	inteligentní
ženy	žena	k1gFnSc2	žena
po	po	k7c6	po
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Major	major	k1gMnSc1	major
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Miloslava	Miloslav	k1gMnSc2	Miloslav
Davidová	Davidová	k1gFnSc1	Davidová
a	a	k8xC	a
Aloys	Aloysa	k1gFnPc2	Aloysa
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
Zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Methusalemovi	Methusalem	k1gMnSc3	Methusalem
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Veliká	veliký	k2eAgFnSc1d1	veliká
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Gintl	Gintl	k1gMnSc1	Gintl
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vendyš	Vendyš	k1gMnSc1	Vendyš
<g/>
,	,	kIx,	,
Drobnosti	drobnost	k1gFnPc1	drobnost
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc4	hra
Vzorný	vzorný	k2eAgMnSc1d1	vzorný
sluha	sluha	k1gMnSc1	sluha
Bashville	Bashville	k1gFnSc2	Bashville
a	a	k8xC	a
Jak	jak	k8xC	jak
lhal	lhát	k5eAaImAgMnS	lhát
jejímu	její	k3xOp3gMnSc3	její
manželu	manžel	k1gMnSc3	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
ostrov	ostrov	k1gInSc1	ostrov
Johna	John	k1gMnSc2	John
Bulla	bulla	k1gFnSc1	bulla
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Emilie	Emilie	k1gFnSc1	Emilie
K.	K.	kA	K.
Škrachová	Škrachová	k1gFnSc1	Škrachová
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
nikdy	nikdy	k6eAd1	nikdy
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Nespolečenský	společenský	k2eNgMnSc1d1	nespolečenský
socialista	socialista	k1gMnSc1	socialista
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g/>
,	,	kIx,	,
Socialismus	socialismus	k1gInSc1	socialismus
pro	pro	k7c4	pro
milionáře	milionář	k1gMnSc4	milionář
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Ženění	ženění	k1gNnSc1	ženění
a	a	k8xC	a
vdávání	vdávání	k1gNnSc1	vdávání
<g/>
,	,	kIx,	,
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Emilie	Emilie	k1gFnSc1	Emilie
K.	K.	kA	K.
Škrachová	Škrachová	k1gFnSc1	Škrachová
<g/>
,	,	kIx,	,
Nerozvážný	rozvážný	k2eNgInSc1d1	nerozvážný
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
dáma	dáma	k1gFnSc1	dáma
sonetů	sonet	k1gInPc2	sonet
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Co	co	k3yRnSc4	co
řekl	říct	k5eAaPmAgMnS	říct
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
Američanům	Američan	k1gMnPc3	Američan
o	o	k7c6	o
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Fromek	Fromek	k1gInSc1	Fromek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
mezi	mezi	k7c7	mezi
umělci	umělec	k1gMnPc7	umělec
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g/>
,	,	kIx,	,
Obrácení	obrácení	k1gNnSc1	obrácení
kapitána	kapitán	k1gMnSc2	kapitán
Brassbounda	Brassbound	k1gMnSc2	Brassbound
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Procházka	Procházka	k1gMnSc1	Procházka
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
<g/>
,	,	kIx,	,
Trakař	trakař	k1gInSc4	trakař
jablek	jablko	k1gNnPc2	jablko
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
wagnerián	wagnerián	k1gMnSc1	wagnerián
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Drobnosti	drobnost	k1gFnPc1	drobnost
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Karel	Karel	k1gMnSc1	Karel
Mušek	muška	k1gFnPc2	muška
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnSc2	hra
Výstřižky	výstřižek	k1gInPc4	výstřižek
z	z	k7c2	z
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
Prohlédnutí	prohlédnutí	k1gNnPc2	prohlédnutí
Blanco	blanco	k2eAgNnSc2d1	blanco
Posneta	Posneto	k1gNnSc2	Posneto
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc2d1	družstevní
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Bedřich	Bedřich	k1gMnSc1	Bedřich
Felix	Felix	k1gMnSc1	Felix
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vendyš	Vendyš	k1gMnSc1	Vendyš
<g/>
,	,	kIx,	,
Nezralost	nezralost	k1gFnSc1	nezralost
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g/>
,	,	kIx,	,
Milionářka	milionářka	k1gFnSc1	milionářka
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Na	na	k7c4	na
úskalí	úskalí	k1gNnSc4	úskalí
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vendyš	Vendyš	k1gMnSc1	Vendyš
<g/>
,	,	kIx,	,
Nebeský	nebeský	k2eAgMnSc1d1	nebeský
football	football	k1gMnSc1	football
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
černošské	černošský	k2eAgFnSc2d1	černošská
dívky	dívka	k1gFnSc2	dívka
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
Ďáblův	ďáblův	k2eAgMnSc1d1	ďáblův
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
Městská	městský	k2eAgNnPc1d1	Městské
divadla	divadlo	k1gNnPc1	divadlo
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Politika	politika	k1gFnSc1	politika
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Alfred	Alfred	k1gMnSc1	Alfred
Pflanzer	Pflanzer	k1gMnSc1	Pflanzer
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Domy	dům	k1gInPc1	dům
pana	pan	k1gMnSc2	pan
Sartoria	Sartorium	k1gNnSc2	Sartorium
<g/>
,	,	kIx,	,
Městská	městský	k2eAgNnPc4d1	Městské
divadla	divadlo	k1gNnPc4	divadlo
pražská	pražský	k2eAgNnPc4d1	Pražské
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Léčení	léčení	k1gNnSc1	léčení
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Měšťané	měšťan	k1gMnPc1	měšťan
calaisští	calaisštit	k5eAaPmIp3nP	calaisštit
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Teatuer	Teatuer	k1gMnSc1	Teatuer
<g/>
,	,	kIx,	,
Americký	americký	k2eAgMnSc1d1	americký
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Domy	dům	k1gInPc1	dům
pana	pan	k1gMnSc2	pan
Sartoria	Sartorium	k1gNnSc2	Sartorium
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
DILIA	DILIA	kA	DILIA
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Záblesk	záblesk	k1gInSc1	záblesk
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Živnost	živnost	k1gFnSc1	živnost
paní	paní	k1gFnSc2	paní
Warrenové	Warrenová	k1gFnSc2	Warrenová
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Teatuer	Teatuer	k1gMnSc1	Teatuer
<g/>
,	,	kIx,	,
Androkles	Androkles	k1gMnSc1	Androkles
a	a	k8xC	a
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
kvítko	kvítko	k1gNnSc1	kvítko
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Čokoládový	čokoládový	k2eAgMnSc1d1	čokoládový
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ota	Ota	k1gMnSc1	Ota
Ornest	Ornest	k1gMnSc1	Ornest
Majorka	majorka	k1gFnSc1	majorka
Barbora	Barbora	k1gFnSc1	Barbora
<g/>
,	,	kIx,	,
ČDLJ	ČDLJ	kA	ČDLJ
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Aloys	Aloys	k1gInSc4	Aloys
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Orbis	orbis	k1gInSc1	orbis
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
I.	I.	kA	I.
<g/>
-II	-II	k?	-II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Kondrysová	Kondrysová	k1gFnSc1	Kondrysová
<g/>
,	,	kIx,	,
Rostislav	Rostislav	k1gMnSc1	Rostislav
Kocourek	Kocourek	k1gMnSc1	Kocourek
a	a	k8xC	a
Květa	Květa	k1gFnSc1	Květa
Marysková	Marysková	k1gFnSc1	Marysková
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hry	hra	k1gFnPc1	hra
Vdovcovy	vdovcův	k2eAgFnPc1d1	vdovcův
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
Živnost	živnost	k1gFnSc4	živnost
paní	paní	k1gFnSc2	paní
Warrenové	Warrenová	k1gFnSc2	Warrenová
<g/>
,	,	kIx,	,
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
kvítko	kvítko	k1gNnSc1	kvítko
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
,	,	kIx,	,
Androkles	Androkles	k1gInSc1	Androkles
a	a	k8xC	a
lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
Záblesk	záblesk	k1gInSc1	záblesk
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
Na	na	k7c4	na
úskalí	úskalí	k1gNnSc4	úskalí
a	a	k8xC	a
Milionářka	milionářka	k1gFnSc1	milionářka
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
August	August	k1gMnSc1	August
vykonal	vykonat	k5eAaPmAgMnS	vykonat
svou	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Věra	Věra	k1gFnSc1	Věra
Nosková	Nosková	k1gFnSc1	Nosková
<g/>
,	,	kIx,	,
Candida	Candida	k1gFnSc1	Candida
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Pavla	Pavla	k1gFnSc1	Pavla
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
Druhý	druhý	k4xOgInSc1	druhý
ostrov	ostrov	k1gInSc1	ostrov
Johna	John	k1gMnSc2	John
Bulla	bulla	k1gFnSc1	bulla
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Člověk	člověk	k1gMnSc1	člověk
nikdy	nikdy	k6eAd1	nikdy
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Milan	Milan	k1gMnSc1	Milan
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Orbis	orbis	k1gInSc1	orbis
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Frank	Frank	k1gMnSc1	Frank
Tetauer	Tetauer	k1gMnSc1	Tetauer
<g/>
,	,	kIx,	,
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellar	k1gMnSc3	Pellar
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ota	Ota	k1gMnSc1	Ota
Ornest	Ornest	k1gMnSc1	Ornest
<g/>
,	,	kIx,	,
Záletník	záletník	k1gMnSc1	záletník
<g />
.	.	kIx.	.
</s>
<s>
Leonard	Leonard	k1gMnSc1	Leonard
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Ota	Ota	k1gMnSc1	Ota
Ornest	Ornest	k1gMnSc1	Ornest
<g/>
,	,	kIx,	,
Živnost	živnost	k1gFnSc1	živnost
paní	paní	k1gFnSc2	paní
Warrenové	Warrenová	k1gFnSc2	Warrenová
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Břetislav	Břetislav	k1gMnSc1	Břetislav
Hodek	Hodek	k1gMnSc1	Hodek
<g/>
,	,	kIx,	,
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Milan	Milan	k1gMnSc1	Milan
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
a	a	k8xC	a
Artur	Artur	k1gMnSc1	Artur
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
a	a	k8xC	a
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Nebeský	nebeský	k2eAgInSc1d1	nebeský
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Eva	Eva	k1gFnSc1	Eva
Kotulová	Kotulový	k2eAgFnSc1d1	Kotulová
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Břetislav	Břetislav	k1gMnSc1	Břetislav
Hodek	Hodek	k1gMnSc1	Hodek
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
ze	z	k7c2	z
Shawových	Shawová	k1gFnPc2	Shawová
Šestnácti	šestnáct	k4xCc2	šestnáct
autobiografických	autobiografický	k2eAgFnPc2d1	autobiografická
skic	skica	k1gFnPc2	skica
<g/>
.	.	kIx.	.
</s>
<s>
Čokoládový	čokoládový	k2eAgMnSc1d1	čokoládový
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Ludmila	Ludmila	k1gFnSc1	Ludmila
Janská	janský	k2eAgFnSc1d1	Janská
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Milan	Milan	k1gMnSc1	Milan
Lukeš	Lukeš	k1gMnSc1	Lukeš
Divadelní	divadelní	k2eAgFnSc4d1	divadelní
moudrost	moudrost	k1gFnSc4	moudrost
Bernarda	Bernard	k1gMnSc2	Bernard
Shawa	Shawus	k1gMnSc2	Shawus
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Břetislav	Břetislav	k1gMnSc1	Břetislav
Hodek	Hodek	k1gMnSc1	Hodek
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Živnost	živnost	k1gFnSc1	živnost
paní	paní	k1gFnSc2	paní
Warrenové	Warrenové	k2eAgFnSc1d1	Warrenové
<g/>
,	,	kIx,	,
Pygmalion	Pygmalion	k1gInSc1	Pygmalion
a	a	k8xC	a
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
zlomených	zlomený	k2eAgNnPc2d1	zlomené
srdcí	srdce	k1gNnPc2	srdce
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kořán	kořán	k1gInSc4	kořán
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gFnPc4	Shaw
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
George	Georg	k1gFnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
George	George	k1gNnSc4	George
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
Plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
Osoba	osoba	k1gFnSc1	osoba
George	Georg	k1gFnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
http://nobelprize.org/nobel_prizes/literature/laureates/1925/shaw-bio.html	[url]	k1gInSc1	http://nobelprize.org/nobel_prizes/literature/laureates/1925/shaw-bio.html
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
http://www.kirjasto.sci.fi/gbshaw.htm	[url]	k6eAd1	http://www.kirjasto.sci.fi/gbshaw.htm
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
http://www.shawchicago.org/shawtimeline.html	[url]	k5eAaPmAgMnS	http://www.shawchicago.org/shawtimeline.html
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
PRAŽÁKOVÁ	Pražáková	k1gFnSc1	Pražáková
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
včerejška	včerejšek	k1gInSc2	včerejšek
k	k	k7c3	k
zítřku	zítřek	k1gInSc3	zítřek
:	:	kIx,	:
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
dramatě	drama	k1gNnSc6	drama
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
kapitol	kapitola	k1gFnPc2	kapitola
věnovyných	věnovyných	k2eAgInSc2d1	věnovyných
dílu	díl	k1gInSc2	díl
G.B.	G.B.	k1gFnSc2	G.B.
<g/>
S.	S.	kA	S.
<g/>
.	.	kIx.	.
</s>
<s>
CHESTERTON	CHESTERTON	kA	CHESTERTON
<g/>
,	,	kIx,	,
Gilbert	Gilbert	k1gMnSc1	Gilbert
Keith	Keith	k1gMnSc1	Keith
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Staša	Staš	k1gInSc2	Staš
Jílovská	jílovský	k2eAgNnPc1d1	Jílovské
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
161	[number]	k4	161
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
