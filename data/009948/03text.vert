<p>
<s>
Chajim	Chajim	k1gMnSc1	Chajim
Oron	Oron	k1gMnSc1	Oron
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ח	ח	k?	ח
א	א	k?	א
<g/>
,	,	kIx,	,
též	též	k9	též
Chajim	Chajim	k1gMnSc1	Chajim
Džumas	Džumas	k1gMnSc1	Džumas
Oron	Oron	k1gMnSc1	Oron
<g/>
,	,	kIx,	,
ח	ח	k?	ח
ג	ג	k?	ג
<g/>
'	'	kIx"	'
<g/>
ו	ו	k?	ו
א	א	k?	א
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
poslanec	poslanec	k1gMnSc1	poslanec
Knesetu	Kneset	k1gInSc2	Kneset
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Nové	Nová	k1gFnSc2	Nová
hnutí-Merec	hnutí-Merec	k1gInSc1	hnutí-Merec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Lahav	Lahava	k1gFnPc2	Lahava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
izraelské	izraelský	k2eAgFnSc6d1	izraelská
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
u	u	k7c2	u
jednotek	jednotka	k1gFnPc2	jednotka
nachal	nachal	k1gMnSc1	nachal
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
hebrejsky	hebrejsky	k6eAd1	hebrejsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
organizace	organizace	k1gFnSc2	organizace
ha-Šomer	ha-Šomer	k1gMnSc1	ha-Šomer
ha-ca	haa	k1gMnSc1	ha-ca
<g/>
'	'	kIx"	'
<g/>
ir	ir	k?	ir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
a	a	k8xC	a
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
tajemníkem	tajemník	k1gInSc7	tajemník
hnutí	hnutí	k1gNnSc2	hnutí
ha-Kibuc	ha-Kibuc	k6eAd1	ha-Kibuc
ha-arci	harec	k1gInSc3	ha-arec
sdružujícího	sdružující	k2eAgInSc2d1	sdružující
kibucy	kibuc	k1gInPc1	kibuc
napojené	napojený	k2eAgInPc1d1	napojený
na	na	k7c4	na
ha-Šomer	ha-Šomer	k1gInSc4	ha-Šomer
ha-ca	ha	k1gInSc2	ha-c
<g/>
'	'	kIx"	'
<g/>
ir	ir	k?	ir
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
hnutí	hnutí	k1gNnSc2	hnutí
Mír	Míra	k1gFnPc2	Míra
nyní	nyní	k6eAd1	nyní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
pokladníkem	pokladník	k1gMnSc7	pokladník
odborové	odborový	k2eAgFnSc2d1	odborová
centrály	centrála	k1gFnSc2	centrála
Histadrut	Histadrut	k1gMnSc1	Histadrut
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
Knesetu	Kneset	k1gInSc2	Kneset
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
už	už	k6eAd1	už
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Mapam	Mapam	k1gInSc1	Mapam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
finančního	finanční	k2eAgInSc2d1	finanční
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Nové	Nová	k1gFnSc2	Nová
hnutí-Merec	hnutí-Merec	k1gInSc1	hnutí-Merec
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Merec	Merec	k1gInSc1	Merec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
předsedou	předseda	k1gMnSc7	předseda
etického	etický	k2eAgInSc2d1	etický
výboru	výbor	k1gInSc2	výbor
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
výboru	výbor	k1gInSc2	výbor
House	house	k1gNnSc1	house
committee	committee	k1gNnSc2	committee
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
finančního	finanční	k2eAgInSc2d1	finanční
a	a	k8xC	a
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
opětovně	opětovně	k6eAd1	opětovně
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
funkčním	funkční	k2eAgNnSc6d1	funkční
období	období	k1gNnSc6	období
zasedal	zasedat	k5eAaImAgInS	zasedat
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
House	house	k1gNnSc4	house
Committee	Committe	k1gInSc2	Committe
a	a	k8xC	a
ve	v	k7c6	v
finančním	finanční	k2eAgInSc6d1	finanční
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Mandátu	mandát	k1gInSc3	mandát
se	se	k3xPyFc4	se
ale	ale	k9	ale
vzdal	vzdát	k5eAaPmAgMnS	vzdát
předčasně	předčasně	k6eAd1	předčasně
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgInS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Usedl	usednout	k5eAaPmAgMnS	usednout
potom	potom	k6eAd1	potom
do	do	k7c2	do
finančního	finanční	k2eAgInSc2d1	finanční
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
do	do	k7c2	do
etického	etický	k2eAgInSc2d1	etický
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Knesetu	Kneset	k1gInSc6	Kneset
usedl	usednout	k5eAaPmAgMnS	usednout
rovněž	rovněž	k9	rovněž
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
pak	pak	k6eAd1	pak
post	post	k1gInSc4	post
člena	člen	k1gMnSc2	člen
finančního	finanční	k2eAgInSc2d1	finanční
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
etického	etický	k2eAgInSc2d1	etický
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
záležitosti	záležitost	k1gFnPc4	záležitost
a	a	k8xC	a
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Předsedal	předsedat	k5eAaImAgMnS	předsedat
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
obětí	oběť	k1gFnSc7	oběť
nacistického	nacistický	k2eAgInSc2d1	nacistický
teroru	teror	k1gInSc2	teror
a	a	k8xC	a
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
měst	město	k1gNnPc2	město
sousedících	sousedící	k2eAgNnPc2d1	sousedící
z	z	k7c2	z
Gazou	Gaza	k1gFnSc7	Gaza
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
funkci	funkce	k1gFnSc6	funkce
člena	člen	k1gMnSc2	člen
finančního	finanční	k2eAgInSc2d1	finanční
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
a	a	k8xC	a
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
výboru	výbor	k1gInSc6	výbor
House	house	k1gNnSc1	house
Committee	Committee	k1gInSc1	Committee
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
podvýboru	podvýbor	k1gInSc2	podvýbor
pro	pro	k7c4	pro
formulaci	formulace	k1gFnSc4	formulace
etických	etický	k2eAgNnPc2d1	etické
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
Knesetu	Kneset	k1gInSc2	Kneset
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
klubu	klub	k1gInSc2	klub
poslanců	poslanec	k1gMnPc2	poslanec
Merec	Merec	k1gInSc4	Merec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
byl	být	k5eAaImAgMnS	být
Ministrem	ministr	k1gMnSc7	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chajim	Chajima	k1gFnPc2	Chajima
Oron	Oron	k1gNnSc4	Oron
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kneset	Kneset	k1gMnSc1	Kneset
–	–	k?	–
Chajim	Chajim	k1gMnSc1	Chajim
Oron	Oron	k1gMnSc1	Oron
</s>
</p>
