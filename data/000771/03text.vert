<s>
ISBN	ISBN	kA	ISBN
(	(	kIx(	(
<g/>
International	International	k1gFnPc2	International
Standard	standard	k1gInSc1	standard
Book	Book	k1gMnSc1	Book
Number	Number	k1gMnSc1	Number
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
standardní	standardní	k2eAgNnSc1d1	standardní
číslo	číslo	k1gNnSc1	číslo
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
číselný	číselný	k2eAgInSc1d1	číselný
kód	kód	k1gInSc1	kód
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
identifikaci	identifikace	k1gFnSc4	identifikace
knižních	knižní	k2eAgNnPc2d1	knižní
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
arabských	arabský	k2eAgFnPc2d1	arabská
číslic	číslice	k1gFnPc2	číslice
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
objevuje	objevovat	k5eAaImIp3nS	objevovat
znak	znak	k1gInSc1	znak
X	X	kA	X
(	(	kIx(	(
<g/>
římská	římský	k2eAgFnSc1d1	římská
číslice	číslice	k1gFnSc1	číslice
10	[number]	k4	10
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozčleňující	rozčleňující	k2eAgInPc4d1	rozčleňující
spojovníky	spojovník	k1gInPc4	spojovník
či	či	k8xC	či
mezery	mezera	k1gFnPc4	mezera
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
je	být	k5eAaImIp3nS	být
specifikováno	specifikovat	k5eAaBmNgNnS	specifikovat
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
standardem	standard	k1gInSc7	standard
ISO	ISO	kA	ISO
2108	[number]	k4	2108
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
převzatým	převzatý	k2eAgInSc7d1	převzatý
jako	jako	k9	jako
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
2108	[number]	k4	2108
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
ISBN	ISBN	kA	ISBN
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
zavedeno	zavést	k5eAaPmNgNnS	zavést
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
v	v	k7c6	v
ČSN	ČSN	kA	ČSN
01	[number]	k4	01
0	[number]	k4	0
<g/>
189	[number]	k4	189
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
knihách	kniha	k1gFnPc6	kniha
bývá	bývat	k5eAaImIp3nS	bývat
ISBN	ISBN	kA	ISBN
zpravidla	zpravidla	k6eAd1	zpravidla
uvedeno	uvést	k5eAaPmNgNnS	uvést
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
titulního	titulní	k2eAgInSc2d1	titulní
listu	list	k1gInSc2	list
a	a	k8xC	a
v	v	k7c6	v
čárovém	čárový	k2eAgInSc6d1	čárový
kódu	kód	k1gInSc6	kód
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
britská	britský	k2eAgFnSc1d1	britská
knihkupecká	knihkupecký	k2eAgFnSc1d1	knihkupecká
firma	firma	k1gFnSc1	firma
W	W	kA	W
H	H	kA	H
Smith	Smith	k1gInSc4	Smith
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zavést	zavést	k5eAaPmF	zavést
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
počítačový	počítačový	k2eAgInSc1d1	počítačový
systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
systém	systém	k1gInSc4	systém
číslování	číslování	k1gNnSc2	číslování
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
návrh	návrh	k1gInSc4	návrh
požádala	požádat	k5eAaPmAgFnS	požádat
sdružení	sdružení	k1gNnSc2	sdružení
britských	britský	k2eAgMnPc2d1	britský
vydavatelů	vydavatel	k1gMnPc2	vydavatel
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tímto	tento	k3xDgInSc7	tento
úkolem	úkol	k1gInSc7	úkol
pověřilo	pověřit	k5eAaPmAgNnS	pověřit
profesora	profesor	k1gMnSc4	profesor
Gordona	Gordon	k1gMnSc4	Gordon
Fostera	Foster	k1gMnSc4	Foster
z	z	k7c2	z
London	London	k1gMnSc1	London
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Economics	Economicsa	k1gFnPc2	Economicsa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
odborníků	odborník	k1gMnPc2	odborník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
systém	systém	k1gInSc4	systém
používající	používající	k2eAgInPc4d1	používající
devíticiferné	devíticiferný	k2eAgInPc4d1	devíticiferný
kódy	kód	k1gInPc4	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
SBN	SBN	kA	SBN
(	(	kIx(	(
<g/>
Standard	standard	k1gInSc1	standard
Book	Book	k1gMnSc1	Book
Numbering	Numbering	k1gInSc1	Numbering
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
standardizaci	standardizace	k1gFnSc4	standardizace
(	(	kIx(	(
<g/>
ISO	ISO	kA	ISO
<g/>
)	)	kIx)	)
začala	začít	k5eAaPmAgFnS	začít
zvažovat	zvažovat	k5eAaImF	zvažovat
jeho	jeho	k3xOp3gMnPc3	jeho
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
využití	využití	k1gNnPc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jednání	jednání	k1gNnSc2	jednání
technické	technický	k2eAgFnSc2d1	technická
komise	komise	k1gFnSc2	komise
bylo	být	k5eAaImAgNnS	být
převzetí	převzetí	k1gNnSc4	převzetí
SBN	SBN	kA	SBN
jako	jako	k8xS	jako
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
standardu	standard	k1gInSc2	standard
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
ISBN	ISBN	kA	ISBN
<g/>
)	)	kIx)	)
ISO	ISO	kA	ISO
2108	[number]	k4	2108
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
SBN	SBN	kA	SBN
se	se	k3xPyFc4	se
ISBN	ISBN	kA	ISBN
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
doplnění	doplnění	k1gNnSc6	doplnění
označení	označení	k1gNnSc2	označení
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
SBN	SBN	kA	SBN
se	se	k3xPyFc4	se
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc1d1	mluvící
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
ze	z	k7c2	z
SBN	SBN	kA	SBN
lze	lze	k6eAd1	lze
platné	platný	k2eAgInPc4d1	platný
ISBN	ISBN	kA	ISBN
vytvořit	vytvořit	k5eAaPmF	vytvořit
přidáním	přidání	k1gNnSc7	přidání
číslice	číslice	k1gFnSc2	číslice
0	[number]	k4	0
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1972	[number]	k4	1972
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
ISBN	ISBN	kA	ISBN
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
systém	systém	k1gInSc4	systém
ISBN	ISBN	kA	ISBN
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
měl	mít	k5eAaImAgInS	mít
ISBN	ISBN	kA	ISBN
kód	kód	k1gInSc4	kód
deset	deset	k4xCc1	deset
cifer	cifra	k1gFnPc2	cifra
(	(	kIx(	(
<g/>
devět	devět	k4xCc4	devět
významových	významový	k2eAgFnPc2d1	významová
a	a	k8xC	a
kontrolní	kontrolní	k2eAgFnSc4d1	kontrolní
číslici	číslice	k1gFnSc4	číslice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
kód	kód	k1gInSc1	kód
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
na	na	k7c4	na
třináct	třináct	k4xCc4	třináct
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
sjednocení	sjednocení	k1gNnSc4	sjednocení
s	s	k7c7	s
reprezentaci	reprezentace	k1gFnSc4	reprezentace
ISBN	ISBN	kA	ISBN
v	v	k7c6	v
čárovém	čárový	k2eAgInSc6d1	čárový
kódu	kód	k1gInSc6	kód
a	a	k8xC	a
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
kapacity	kapacita	k1gFnSc2	kapacita
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
EAN	EAN	kA	EAN
prefix	prefix	k1gInSc4	prefix
978	[number]	k4	978
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
prefix	prefix	k1gInSc4	prefix
979	[number]	k4	979
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
kapacita	kapacita	k1gFnSc1	kapacita
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
na	na	k7c4	na
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
změnami	změna	k1gFnPc7	změna
byla	být	k5eAaImAgFnS	být
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
ISBN	ISBN	kA	ISBN
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
má	mít	k5eAaImIp3nS	mít
pevně	pevně	k6eAd1	pevně
danou	daný	k2eAgFnSc4d1	daná
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
identifikátoru	identifikátor	k1gInSc2	identifikátor
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
původu	původ	k1gInSc6	původ
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
částí	část	k1gFnPc2	část
proměnlivé	proměnlivý	k2eAgFnSc2d1	proměnlivá
délky	délka	k1gFnSc2	délka
spojených	spojený	k2eAgFnPc2d1	spojená
spojovníky	spojovník	k1gInPc1	spojovník
nebo	nebo	k8xC	nebo
oddělených	oddělený	k2eAgFnPc6d1	oddělená
mezerami	mezera	k1gFnPc7	mezera
<g/>
;	;	kIx,	;
před	před	k7c7	před
kódem	kód	k1gInSc7	kód
se	s	k7c7	s
(	(	kIx(	(
<g/>
povinně	povinně	k6eAd1	povinně
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
označení	označení	k1gNnSc4	označení
ISBN	ISBN	kA	ISBN
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
deseticiferné	deseticiferný	k2eAgInPc1d1	deseticiferný
ISBN	ISBN	kA	ISBN
(	(	kIx(	(
<g/>
označované	označovaný	k2eAgFnPc1d1	označovaná
dnes	dnes	k6eAd1	dnes
jako	jako	k9	jako
ISBN-	ISBN-	k1gFnSc1	ISBN-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgFnPc1	čtyři
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
třináctimístné	třináctimístný	k2eAgNnSc1d1	třináctimístné
(	(	kIx(	(
<g/>
ISBN-	ISBN-	k1gFnSc1	ISBN-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
skupinu	skupina	k1gFnSc4	skupina
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
ISBN-13	ISBN-13	k4	ISBN-13
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
identickou	identický	k2eAgFnSc4d1	identická
strukturu	struktura	k1gFnSc4	struktura
jako	jako	k8xC	jako
čárový	čárový	k2eAgInSc4d1	čárový
kód	kód	k1gInSc4	kód
EAN-	EAN-	k1gFnPc2	EAN-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
knihách	kniha	k1gFnPc6	kniha
jako	jako	k8xS	jako
čárový	čárový	k2eAgInSc4d1	čárový
kód	kód	k1gInSc4	kód
uvedeno	uvést	k5eAaPmNgNnS	uvést
přímo	přímo	k6eAd1	přímo
ISBN	ISBN	kA	ISBN
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
U	u	k7c2	u
staršího	starý	k2eAgMnSc2d2	starší
ISBN-10	ISBN-10	k1gMnSc2	ISBN-10
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k6eAd1	potřeba
drobná	drobný	k2eAgFnSc1d1	drobná
úprava	úprava	k1gFnSc1	úprava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc7	první
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ISBN-13	ISBN-13	k1gFnSc2	ISBN-13
nově	nově	k6eAd1	nově
přidaný	přidaný	k2eAgInSc1d1	přidaný
prefix	prefix	k1gInSc1	prefix
<g/>
:	:	kIx,	:
konstanta	konstanta	k1gFnSc1	konstanta
978	[number]	k4	978
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
knihy	kniha	k1gFnPc4	kniha
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
979	[number]	k4	979
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
hudebniny	hudebnina	k1gFnPc4	hudebnina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
prefix	prefix	k1gInSc4	prefix
čárového	čárový	k2eAgInSc2d1	čárový
kódu	kód	k1gInSc2	kód
podle	podle	k7c2	podle
GS	GS	kA	GS
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
kódu	kód	k1gInSc2	kód
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ISBN-13	ISBN-13	k1gFnSc2	ISBN-13
i	i	k8xC	i
ISBN-10	ISBN-10	k1gFnSc2	ISBN-10
v	v	k7c6	v
principu	princip	k1gInSc6	princip
shodný	shodný	k2eAgInSc1d1	shodný
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
částí	část	k1gFnSc7	část
ISBN	ISBN	kA	ISBN
(	(	kIx(	(
<g/>
u	u	k7c2	u
ISBN-10	ISBN-10	k1gFnSc2	ISBN-10
první	první	k4xOgFnSc1	první
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
identifikátor	identifikátor	k1gInSc4	identifikátor
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
či	či	k8xC	či
jazykový	jazykový	k2eAgInSc1d1	jazykový
region	region	k1gInSc1	region
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jednu	jeden	k4xCgFnSc4	jeden
až	až	k9	až
pět	pět	k4xCc4	pět
cifer	cifra	k1gFnPc2	cifra
<g/>
,	,	kIx,	,
významy	význam	k1gInPc4	význam
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
ISBN	ISBN	kA	ISBN
<g/>
.	.	kIx.	.
</s>
<s>
Kódy	kód	k1gInPc1	kód
0	[number]	k4	0
a	a	k8xC	a
1	[number]	k4	1
označují	označovat	k5eAaImIp3nP	označovat
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnPc4d1	mluvící
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
2	[number]	k4	2
francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgFnSc1d1	mluvící
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
3	[number]	k4	3
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
4	[number]	k4	4
japonštinu	japonština	k1gFnSc4	japonština
<g/>
,	,	kIx,	,
5	[number]	k4	5
ruštinu	ruština	k1gFnSc4	ruština
<g/>
,	,	kIx,	,
7	[number]	k4	7
čínštinu	čínština	k1gFnSc4	čínština
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc4	Slovensko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
identifikátor	identifikátor	k1gInSc1	identifikátor
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
Identifikátor	identifikátor	k1gInSc1	identifikátor
skupiny	skupina	k1gFnSc2	skupina
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
pět	pět	k4xCc4	pět
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Bhútán	Bhútán	k1gInSc1	Bhútán
má	mít	k5eAaImIp3nS	mít
kód	kód	k1gInSc4	kód
99936	[number]	k4	99936
<g/>
.	.	kIx.	.
</s>
<s>
Identifikátory	identifikátor	k1gInPc1	identifikátor
skupiny	skupina	k1gFnSc2	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
prefixový	prefixový	k2eAgInSc4d1	prefixový
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
délku	délka	k1gFnSc4	délka
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
lze	lze	k6eAd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určit	určit	k5eAaPmF	určit
i	i	k9	i
bez	bez	k7c2	bez
pomlček	pomlčka	k1gFnPc2	pomlčka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
identifikace	identifikace	k1gFnSc1	identifikace
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
vydavatele	vydavatel	k1gMnSc4	vydavatel
knihy	kniha	k1gFnSc2	kniha
pomocí	pomocí	k7c2	pomocí
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dané	daný	k2eAgFnSc2d1	daná
skupiny	skupina	k1gFnSc2	skupina
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
vydavatele	vydavatel	k1gMnSc2	vydavatel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
max	max	kA	max
<g/>
.	.	kIx.	.
sedmimístný	sedmimístný	k2eAgInSc4d1	sedmimístný
<g/>
,	,	kIx,	,
kódy	kód	k1gInPc1	kód
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
úřady	úřad	k1gInPc7	úřad
ISBN	ISBN	kA	ISBN
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
skupinách	skupina	k1gFnPc6	skupina
(	(	kIx(	(
<g/>
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
regionech	region	k1gInPc6	region
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
úřadem	úřad	k1gInSc7	úřad
Národní	národní	k2eAgFnSc1d1	národní
agentura	agentura	k1gFnSc1	agentura
ISBN	ISBN	kA	ISBN
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
při	při	k7c6	při
Národní	národní	k2eAgFnSc6d1	národní
knihovně	knihovna	k1gFnSc6	knihovna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1	seznam
všech	všecek	k3xTgMnPc2	všecek
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
vydavatelů	vydavatel	k1gMnPc2	vydavatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
knihy	kniha	k1gFnSc2	kniha
či	či	k8xC	či
CD-ROM	CD-ROM	k1gFnSc2	CD-ROM
zakoupit	zakoupit	k5eAaPmF	zakoupit
od	od	k7c2	od
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
agentury	agentura	k1gFnSc2	agentura
ISBN	ISBN	kA	ISBN
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
však	však	k9	však
více	hodně	k6eAd2	hodně
než	než	k8xS	než
€	€	k?	€
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Předposlední	předposlední	k2eAgFnSc1d1	předposlední
část	část	k1gFnSc1	část
popisuje	popisovat	k5eAaImIp3nS	popisovat
již	již	k6eAd1	již
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
vydání	vydání	k1gNnSc4	vydání
příslušné	příslušný	k2eAgFnSc2d1	příslušná
knihy	kniha	k1gFnSc2	kniha
u	u	k7c2	u
daného	daný	k2eAgMnSc2d1	daný
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
max	max	kA	max
<g/>
.	.	kIx.	.
šest	šest	k4xCc4	šest
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
pevné	pevný	k2eAgFnSc3d1	pevná
délce	délka	k1gFnSc3	délka
ISBN	ISBN	kA	ISBN
je	být	k5eAaImIp3nS	být
zarovnáno	zarovnán	k2eAgNnSc1d1	zarovnáno
zleva	zleva	k6eAd1	zleva
nulami	nula	k1gFnPc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Číslování	číslování	k1gNnSc1	číslování
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
si	se	k3xPyFc3	se
určuje	určovat	k5eAaImIp3nS	určovat
sám	sám	k3xTgMnSc1	sám
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
částí	část	k1gFnSc7	část
ISBN	ISBN	kA	ISBN
je	být	k5eAaImIp3nS	být
kontrolní	kontrolní	k2eAgFnPc4d1	kontrolní
číslice	číslice	k1gFnPc4	číslice
<g/>
:	:	kIx,	:
vždy	vždy	k6eAd1	vždy
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
platnosti	platnost	k1gFnSc2	platnost
ISBN	ISBN	kA	ISBN
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
např.	např.	kA	např.
k	k	k7c3	k
překlepu	překlep	k1gInSc3	překlep
při	při	k7c6	při
zadávání	zadávání	k1gNnSc6	zadávání
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
zcela	zcela	k6eAd1	zcela
jiná	jiný	k2eAgFnSc1d1	jiná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
detekována	detekován	k2eAgFnSc1d1	detekována
chyba	chyba	k1gFnSc1	chyba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
číslice	číslice	k1gFnSc2	číslice
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nP	lišit
u	u	k7c2	u
ISBN-10	ISBN-10	k1gFnSc2	ISBN-10
a	a	k8xC	a
ISBN-	ISBN-	k1gFnSc7	ISBN-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ISBN-10	ISBN-10	k1gFnSc2	ISBN-10
se	se	k3xPyFc4	se
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
číslice	číslice	k1gFnSc1	číslice
získá	získat	k5eAaPmIp3nS	získat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zbytek	zbytek	k1gInSc1	zbytek
po	po	k7c6	po
dělení	dělení	k1gNnSc6	dělení
váženého	vážený	k2eAgInSc2d1	vážený
součtu	součet	k1gInSc2	součet
všech	všecek	k3xTgFnPc2	všecek
číslic	číslice	k1gFnPc2	číslice
jedenácti	jedenáct	k4xCc2	jedenáct
byl	být	k5eAaImAgInS	být
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
číslice	číslice	k1gFnSc1	číslice
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k8xC	i
hodnotu	hodnota	k1gFnSc4	hodnota
10	[number]	k4	10
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
znakem	znak	k1gInSc7	znak
X.	X.	kA	X.
Jako	jako	k8xS	jako
váhy	váha	k1gFnPc1	váha
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
čísla	číslo	k1gNnPc4	číslo
(	(	kIx(	(
<g/>
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
10	[number]	k4	10
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
cifru	cifra	k1gFnSc4	cifra
kódu	kód	k1gInSc2	kód
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
kontrolní	kontrolní	k2eAgFnSc4d1	kontrolní
číslici	číslice	k1gFnSc4	číslice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
součet	součet	k1gInSc1	součet
je	být	k5eAaImIp3nS	být
143	[number]	k4	143
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
:	:	kIx,	:
143	[number]	k4	143
mod	mod	k?	mod
11	[number]	k4	11
=	=	kIx~	=
0	[number]	k4	0
(	(	kIx(	(
<g/>
143	[number]	k4	143
=	=	kIx~	=
13	[number]	k4	13
·	·	k?	·
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-204-0105-9	[number]	k4	80-204-0105-9
je	být	k5eAaImIp3nS	být
platné	platný	k2eAgNnSc1d1	platné
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmus	algoritmus	k1gInSc1	algoritmus
pro	pro	k7c4	pro
kontrolní	kontrolní	k2eAgFnSc4d1	kontrolní
číslici	číslice	k1gFnSc4	číslice
pro	pro	k7c4	pro
ISBN-13	ISBN-13	k1gFnSc4	ISBN-13
je	být	k5eAaImIp3nS	být
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
výpočtem	výpočet	k1gInSc7	výpočet
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
číslice	číslice	k1gFnSc2	číslice
u	u	k7c2	u
čárových	čárový	k2eAgInPc2d1	čárový
kódů	kód	k1gInPc2	kód
EAN-	EAN-	k1gFnSc2	EAN-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
číslice	číslice	k1gFnSc1	číslice
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
volí	volit	k5eAaImIp3nS	volit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zbytek	zbytek	k1gInSc1	zbytek
po	po	k7c6	po
dělení	dělení	k1gNnSc6	dělení
váženého	vážený	k2eAgInSc2d1	vážený
součtu	součet	k1gInSc2	součet
všech	všecek	k3xTgFnPc2	všecek
číslic	číslice	k1gFnPc2	číslice
deseti	deset	k4xCc7	deset
byl	být	k5eAaImAgInS	být
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jako	jako	k8xS	jako
váhy	váha	k1gFnPc1	váha
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
střídavě	střídavě	k6eAd1	střídavě
čísla	číslo	k1gNnPc4	číslo
1	[number]	k4	1
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dělení	dělení	k1gNnSc1	dělení
deseti	deset	k4xCc7	deset
namísto	namísto	k7c2	namísto
dělení	dělení	k1gNnSc3	dělení
jedenácti	jedenáct	k4xCc2	jedenáct
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ISBN-10	ISBN-10	k1gFnSc2	ISBN-10
nepoužívá	používat	k5eNaImIp3nS	používat
znak	znak	k1gInSc1	znak
X.	X.	kA	X.
<g/>
)	)	kIx)	)
Výsledný	výsledný	k2eAgInSc1d1	výsledný
součet	součet	k1gInSc1	součet
je	být	k5eAaImIp3nS	být
140	[number]	k4	140
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
:	:	kIx,	:
140	[number]	k4	140
mod	mod	k?	mod
10	[number]	k4	10
=	=	kIx~	=
0	[number]	k4	0
(	(	kIx(	(
<g/>
140	[number]	k4	140
=	=	kIx~	=
14	[number]	k4	14
·	·	k?	·
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7203-884-8	[number]	k4	978-80-7203-884-8
je	být	k5eAaImIp3nS	být
platné	platný	k2eAgNnSc1d1	platné
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ISBN-10	ISBN-10	k1gMnSc2	ISBN-10
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
algoritmus	algoritmus	k1gInSc1	algoritmus
liší	lišit	k5eAaImIp3nS	lišit
použitým	použitý	k2eAgInSc7d1	použitý
dělitelem	dělitel	k1gInSc7	dělitel
(	(	kIx(	(
<g/>
deset	deset	k4xCc1	deset
namísto	namísto	k7c2	namísto
jedenácti	jedenáct	k4xCc2	jedenáct
<g/>
)	)	kIx)	)
a	a	k8xC	a
váhami	váha	k1gFnPc7	váha
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgFnSc1d2	horší
schopnost	schopnost	k1gFnSc1	schopnost
odhalování	odhalování	k1gNnSc2	odhalování
chyb	chyba	k1gFnPc2	chyba
-	-	kIx~	-
algoritmus	algoritmus	k1gInSc1	algoritmus
u	u	k7c2	u
EAN-13	EAN-13	k1gFnSc2	EAN-13
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
některé	některý	k3yIgInPc4	některý
záměny	záměna	k1gFnPc1	záměna
sousedních	sousední	k2eAgFnPc2d1	sousední
cifer	cifra	k1gFnPc2	cifra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ISBN	ISBN	kA	ISBN
978-0-306-40615-7	[number]	k4	978-0-306-40615-7
a	a	k8xC	a
978-0-306-40165-7	[number]	k4	978-0-306-40165-7
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jen	jen	k6eAd1	jen
záměnou	záměna	k1gFnSc7	záměna
dvou	dva	k4xCgInPc2	dva
následujících	následující	k2eAgInPc2d1	následující
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
platné	platný	k2eAgInPc1d1	platný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
přiděleným	přidělený	k2eAgInSc7d1	přidělený
kódem	kód	k1gInSc7	kód
ISBN-10	ISBN-10	k1gFnSc2	ISBN-10
označovala	označovat	k5eAaImAgFnS	označovat
čárovým	čárový	k2eAgInSc7d1	čárový
kódem	kód	k1gInSc7	kód
EAN-	EAN-	k1gFnSc1	EAN-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
odvodil	odvodit	k5eAaPmAgMnS	odvodit
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
kód	kód	k1gInSc4	kód
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
ISBN	ISBN	kA	ISBN
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
deseticiferného	deseticiferný	k2eAgNnSc2d1	deseticiferný
ISBN	ISBN	kA	ISBN
odstranila	odstranit	k5eAaPmAgFnS	odstranit
poslední	poslední	k2eAgInSc4d1	poslední
(	(	kIx(	(
<g/>
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
<g/>
)	)	kIx)	)
číslice	číslice	k1gFnSc1	číslice
a	a	k8xC	a
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
se	se	k3xPyFc4	se
doplnil	doplnit	k5eAaPmAgMnS	doplnit
prefix	prefix	k1gInSc4	prefix
978	[number]	k4	978
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Třináctou	třináctý	k4xOgFnSc7	třináctý
číslicí	číslice	k1gFnSc7	číslice
výsledného	výsledný	k2eAgInSc2d1	výsledný
kódu	kód	k1gInSc2	kód
je	být	k5eAaImIp3nS	být
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
číslice	číslice	k1gFnSc1	číslice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
standardním	standardní	k2eAgInSc7d1	standardní
postupem	postup	k1gInSc7	postup
definovaným	definovaný	k2eAgInSc7d1	definovaný
v	v	k7c6	v
EAN-	EAN-	k1gFnSc6	EAN-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Například	například	k6eAd1	například
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
ISBN	ISBN	kA	ISBN
80-200-0980-9	[number]	k4	80-200-0980-9
má	mít	k5eAaImIp3nS	mít
čárový	čárový	k2eAgInSc4d1	čárový
kód	kód	k1gInSc4	kód
9788020009807	[number]	k4	9788020009807
<g/>
.	.	kIx.	.
</s>
<s>
Postupovat	postupovat	k5eAaImF	postupovat
lze	lze	k6eAd1	lze
i	i	k9	i
opačně	opačně	k6eAd1	opačně
<g/>
:	:	kIx,	:
z	z	k7c2	z
čárového	čárový	k2eAgInSc2d1	čárový
kódu	kód	k1gInSc2	kód
se	se	k3xPyFc4	se
odstraní	odstranit	k5eAaPmIp3nS	odstranit
prefix	prefix	k1gInSc1	prefix
978	[number]	k4	978
a	a	k8xC	a
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
číslice	číslice	k1gFnSc1	číslice
<g/>
,	,	kIx,	,
po	po	k7c6	po
doplnění	doplnění	k1gNnSc6	doplnění
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
číslice	číslice	k1gFnSc2	číslice
ISBN	ISBN	kA	ISBN
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
ISBN	ISBN	kA	ISBN
kód	kód	k1gInSc1	kód
bez	bez	k7c2	bez
rozdělení	rozdělení	k1gNnSc2	rozdělení
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ISBN-13	ISBN-13	k1gMnPc2	ISBN-13
již	již	k9	již
tato	tento	k3xDgFnSc1	tento
úprava	úprava	k1gFnSc1	úprava
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
čárovým	čárový	k2eAgInSc7d1	čárový
kódem	kód	k1gInSc7	kód
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
ISBN	ISBN	kA	ISBN
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
ASIN	ASIN	kA	ASIN
-	-	kIx~	-
Amazon	amazona	k1gFnPc2	amazona
Standard	standard	k1gInSc1	standard
Identification	Identification	k1gInSc1	Identification
Number	Number	k1gInSc4	Number
DOI	DOI	kA	DOI
-	-	kIx~	-
Digital	Digital	kA	Digital
Object	Object	k2eAgInSc1d1	Object
Identifier	Identifier	k1gInSc1	Identifier
ISAN	ISAN	kA	ISAN
-	-	kIx~	-
International	International	k1gFnSc1	International
Standard	standard	k1gInSc1	standard
Audiovisual	Audiovisual	k1gInSc1	Audiovisual
Number	Number	k1gInSc4	Number
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
http://www.nlc-bnc.ca/iso/tc46sc9/isan.htm	[url]	k1gInSc1	http://www.nlc-bnc.ca/iso/tc46sc9/isan.htm
<g/>
)	)	kIx)	)
ISMN	ISMN	kA	ISMN
-	-	kIx~	-
International	International	k1gFnPc2	International
Standard	standard	k1gInSc1	standard
Music	Music	k1gMnSc1	Music
Number	Numbra	k1gFnPc2	Numbra
for	forum	k1gNnPc2	forum
Printed	Printed	k1gMnSc1	Printed
Music	Music	k1gMnSc1	Music
ISRC	ISRC	kA	ISRC
-	-	kIx~	-
The	The	k1gMnSc2	The
International	International	k1gFnSc1	International
Standard	standard	k1gInSc1	standard
Recording	Recording	k1gInSc1	Recording
Code	Code	k1gInSc4	Code
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
http://www.ifpi.org/isrc/	[url]	k?	http://www.ifpi.org/isrc/
<g/>
)	)	kIx)	)
ISRN	ISRN	kA	ISRN
-	-	kIx~	-
International	International	k1gFnSc1	International
Standard	standard	k1gInSc1	standard
Technical	Technical	k1gFnSc1	Technical
<g />
.	.	kIx.	.
</s>
<s>
Report	report	k1gInSc1	report
Number	Number	k1gInSc1	Number
ISSN	ISSN	kA	ISSN
-	-	kIx~	-
International	International	k1gFnPc2	International
Standard	standard	k1gInSc1	standard
Serial	Serial	k1gMnSc1	Serial
Number	Number	k1gMnSc1	Number
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
periodika	periodikum	k1gNnPc4	periodikum
<g/>
)	)	kIx)	)
ISWC	ISWC	kA	ISWC
-	-	kIx~	-
International	International	k1gFnSc1	International
Standard	standard	k1gInSc1	standard
Musical	musical	k1gInSc1	musical
Work	Work	k1gInSc4	Work
Code	Cod	k1gFnSc2	Cod
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
International	International	k1gFnSc2	International
Standard	standard	k1gInSc1	standard
Book	Book	k1gInSc1	Book
Number	Number	k1gInSc4	Number
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
International	International	k1gFnSc1	International
Standard	standard	k1gInSc1	standard
Book	Book	k1gMnSc1	Book
Number	Number	k1gMnSc1	Number
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
ISBN	ISBN	kA	ISBN
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
knihovně	knihovna	k1gFnSc6	knihovna
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
ISBN	ISBN	kA	ISBN
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ISBN	ISBN	kA	ISBN
a	a	k8xC	a
online	onlinout	k5eAaPmIp3nS	onlinout
publikace	publikace	k1gFnSc1	publikace
-	-	kIx~	-
článek	článek	k1gInSc1	článek
o	o	k7c6	o
přidělování	přidělování	k1gNnSc6	přidělování
čísel	číslo	k1gNnPc2	číslo
ISBN	ISBN	kA	ISBN
elektronickým	elektronický	k2eAgFnPc3d1	elektronická
publikacím	publikace	k1gFnPc3	publikace
<g/>
,	,	kIx,	,
leden	leden	k1gInSc1	leden
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
www.isbn-international.org	www.isbnnternational.org	k1gInSc1	www.isbn-international.org
-	-	kIx~	-
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
ISBN	ISBN	kA	ISBN
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Standard	standard	k1gInSc1	standard
ISO	ISO	kA	ISO
2108	[number]	k4	2108
<g/>
:	:	kIx,	:
<g/>
2005	[number]	k4	2005
na	na	k7c4	na
www.iso.org	www.iso.org	k1gInSc4	www.iso.org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
RFC	RFC	kA	RFC
3187	[number]	k4	3187
Using	Using	k1gInSc4	Using
International	International	k1gMnPc2	International
Standard	standard	k1gInSc1	standard
Book	Booka	k1gFnPc2	Booka
Numbers	Numbersa	k1gFnPc2	Numbersa
as	as	k9	as
Uniform	Uniform	k1gInSc4	Uniform
resource	resourka	k1gFnSc3	resourka
names	names	k1gInSc1	names
-	-	kIx~	-
použití	použití	k1gNnSc1	použití
ISBN	ISBN	kA	ISBN
jako	jako	k8xS	jako
URN	URN	kA	URN
Hledání	hledání	k1gNnSc1	hledání
podle	podle	k7c2	podle
ISBN	ISBN	kA	ISBN
</s>
