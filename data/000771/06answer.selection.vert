<s>
ISBN	ISBN	kA	ISBN
(	(	kIx(	(
<g/>
International	International	k1gFnPc2	International
Standard	standard	k1gInSc1	standard
Book	Book	k1gMnSc1	Book
Number	Number	k1gMnSc1	Number
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
standardní	standardní	k2eAgNnSc1d1	standardní
číslo	číslo	k1gNnSc1	číslo
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
číselný	číselný	k2eAgInSc1d1	číselný
kód	kód	k1gInSc1	kód
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
identifikaci	identifikace	k1gFnSc4	identifikace
knižních	knižní	k2eAgNnPc2d1	knižní
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
