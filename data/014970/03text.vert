<s>
Naohiro	Naohiro	k6eAd1
Išikawa	Išikaw	k2eAgFnSc1d1
</s>
<s>
Naohiro	Naohiro	k1gNnSc1
Išikawa	Išikawa	k1gFnSc1
Naohiro	Naohiro	k1gNnSc1
IšikawaOsobní	IšikawaOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Naohiro	Naohiro	k1gNnSc1
Išikawa	Išikawum	k1gNnSc2
Datum	datum	k1gInSc4
narození	narození	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
(	(	kIx(
<g/>
39	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Kanagawa	Kanagawa	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Současný	současný	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
FC	FC	kA
Tokyo	Tokyo	k6eAd1
Číslo	číslo	k1gNnSc1
dresu	dres	k1gInSc2
</s>
<s>
18	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
záložník	záložník	k1gMnSc1
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
20022002	#num#	k4
<g/>
–	–	k?
Yokohama	Yokohama	k1gNnSc1
F.	F.	kA
Marinos	Marinosa	k1gFnPc2
FC	FC	kA
Tokyo	Tokyo	k6eAd1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
Japonsko	Japonsko	k1gNnSc4
<g/>
6	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Naohiro	Naohiro	k1gNnSc1
Išikawa	Išikaw	k1gInSc2
(	(	kIx(
<g/>
*	*	kIx~
12	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1981	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
japonský	japonský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Naohiro	Naohiro	k6eAd1
Išikawa	Išikawa	k1gMnSc1
odehrál	odehrát	k5eAaPmAgMnS
6	#num#	k4
reprezentačních	reprezentační	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
japonskou	japonský	k2eAgFnSc7d1
reprezentací	reprezentace	k1gFnSc7
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Statistiky	statistika	k1gFnPc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
RokyZáp	RokyZáp	k1gInSc1
<g/>
.	.	kIx.
<g/>
Góly	gól	k1gInPc1
</s>
<s>
200310	#num#	k4
</s>
<s>
200410	#num#	k4
</s>
<s>
200500	#num#	k4
</s>
<s>
200600	#num#	k4
</s>
<s>
200700	#num#	k4
</s>
<s>
200800	#num#	k4
</s>
<s>
200920	#num#	k4
</s>
<s>
201010	#num#	k4
</s>
<s>
201100	#num#	k4
</s>
<s>
201210	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
60	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Naohiro	Naohiro	k1gNnSc1
Išikawa	Išikaw	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
National	Nationat	k5eAaPmAgInS,k5eAaImAgInS
Football	Football	k1gMnSc1
Teams	Teamsa	k1gFnPc2
</s>
<s>
Japan	japan	k1gInSc1
National	National	k1gFnSc2
Football	Footballa	k1gFnPc2
Team	team	k1gInSc1
Database	Databas	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
251316631	#num#	k4
</s>
