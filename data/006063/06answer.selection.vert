<s>
Kvarková	Kvarkový	k2eAgFnSc1d1	Kvarkový
struktura	struktura	k1gFnSc1	struktura
byla	být	k5eAaImAgFnS	být
teoreticky	teoreticky	k6eAd1	teoreticky
předpovězena	předpovězet	k5eAaImNgFnS	předpovězet
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
M.	M.	kA	M.
Gell-Mannem	Gell-Mann	k1gMnSc7	Gell-Mann
a	a	k8xC	a
G.	G.	kA	G.
Zweigem	Zweig	k1gMnSc7	Zweig
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
