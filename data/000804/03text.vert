<s>
Louisville	Louisville	k6eAd1	Louisville
je	být	k5eAaImIp3nS	být
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Kentucky	Kentucka	k1gFnSc2	Kentucka
a	a	k8xC	a
šestnácté	šestnáctý	k4xOgInPc1	šestnáctý
nebo	nebo	k8xC	nebo
dvacáté	dvacátý	k4xOgInPc1	dvacátý
sedmé	sedmý	k4xOgInPc1	sedmý
nejlidnatější	lidnatý	k2eAgInPc1d3	nejlidnatější
město	město	k1gNnSc4	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
počítání	počítání	k1gNnSc2	počítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Kentucky	Kentucka	k1gFnSc2	Kentucka
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
.	.	kIx.	.
</s>
<s>
Louisville	Louisville	k1gFnSc1	Louisville
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Jefferson	Jeffersona	k1gFnPc2	Jeffersona
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
1032	[number]	k4	1032
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
Louisville	Louisvilla	k1gFnSc6	Louisvilla
žilo	žít	k5eAaImAgNnS	žít
741	[number]	k4	741
096	[number]	k4	096
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
aglomerace	aglomerace	k1gFnSc2	aglomerace
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1778	[number]	k4	1778
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
často	často	k6eAd1	často
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejsevernější	severní	k2eAgNnSc1d3	nejsevernější
jižní	jižní	k2eAgNnSc1d1	jižní
město	město	k1gNnSc1	město
a	a	k8xC	a
nejjižnější	jižní	k2eAgNnSc1d3	nejjižnější
severní	severní	k2eAgNnSc1d1	severní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
mísí	mísit	k5eAaImIp3nS	mísit
kultura	kultura	k1gFnSc1	kultura
jak	jak	k6eAd1	jak
amerického	americký	k2eAgInSc2d1	americký
Jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Středozápadu	středozápad	k1gInSc2	středozápad
<g/>
.	.	kIx.	.
</s>
<s>
Louisville	Louisville	k6eAd1	Louisville
je	být	k5eAaImIp3nS	být
také	také	k9	také
proslaveno	proslaven	k2eAgNnSc1d1	proslaveno
závody	závod	k1gInPc7	závod
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
například	například	k6eAd1	například
závodiště	závodiště	k1gNnSc1	závodiště
Churchill	Churchilla	k1gFnPc2	Churchilla
Downs	Downsa	k1gFnPc2	Downsa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
597	[number]	k4	597
337	[number]	k4	337
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
70,6	[number]	k4	70,6
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
22,9	[number]	k4	22,9
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
Indiáni	Indián	k1gMnPc1	Indián
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Šimon	Šimon	k1gMnSc1	Šimon
Agranat	Agranat	k1gMnSc1	Agranat
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc4	Izrael
Muhammad	Muhammad	k1gInSc1	Muhammad
Ali	Ali	k1gFnSc1	Ali
-	-	kIx~	-
boxer	boxer	k1gMnSc1	boxer
Nicole	Nicole	k1gFnSc2	Nicole
Scherzingerová	Scherzingerová	k1gFnSc1	Scherzingerová
-	-	kIx~	-
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jennifer	Jennifer	k1gInSc1	Jennifer
Lawrence	Lawrence	k1gFnSc1	Lawrence
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Ťiou-ťiang	Ťiou-ťiang	k1gInSc1	Ťiou-ťiang
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
La	la	k1gNnSc2	la
Plata	plato	k1gNnSc2	plato
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
Mohuč	Mohuč	k1gFnSc1	Mohuč
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Montpellier	Montpellira	k1gFnPc2	Montpellira
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Perm	perm	k1gInSc1	perm
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Quito	Quit	k2eAgNnSc1d1	Quito
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
Tamale	Tamala	k1gFnSc3	Tamala
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
</s>
