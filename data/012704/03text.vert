<p>
<s>
XXXI	XXXI	kA	XXXI
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
Jogos	Jogos	k1gMnSc1	Jogos
Olímpicos	Olímpicos	k1gMnSc1	Olímpicos
de	de	k?	de
Verã	Verã	k1gFnSc2	Verã
de	de	k?	de
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Hry	hra	k1gFnPc1	hra
XXXI	XXXI	kA	XXXI
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
Jogos	Jogos	k1gInSc1	Jogos
da	da	k?	da
XXXI	XXXI	kA	XXXI
Olimpíada	Olimpíada	k1gFnSc1	Olimpíada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
představovaly	představovat	k5eAaImAgInP	představovat
velkou	velký	k2eAgFnSc4d1	velká
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
sportovní	sportovní	k2eAgFnSc4d1	sportovní
událost	událost	k1gFnSc4	událost
–	–	k?	–
pokračování	pokračování	k1gNnSc3	pokračování
tradice	tradice	k1gFnSc2	tradice
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
organizovanou	organizovaný	k2eAgFnSc4d1	organizovaná
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
olympijským	olympijský	k2eAgInSc7d1	olympijský
výborem	výbor	k1gInSc7	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
místo	místo	k1gNnSc4	místo
konání	konání	k1gNnSc2	konání
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
brazilské	brazilský	k2eAgNnSc1d1	brazilské
město	město	k1gNnSc1	město
Rio	Rio	k1gFnSc2	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
stadiónu	stadión	k1gInSc6	stadión
Maracanã	Maracanã	k1gFnPc2	Maracanã
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
zahájení	zahájení	k1gNnSc2	zahájení
<g/>
.	.	kIx.	.
</s>
<s>
Zakončení	zakončení	k1gNnSc1	zakončení
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
fotbalistů	fotbalista	k1gMnPc2	fotbalista
se	se	k3xPyFc4	se
rozeběhla	rozeběhnout	k5eAaPmAgFnS	rozeběhnout
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
soutěžily	soutěžit	k5eAaImAgInP	soutěžit
v	v	k7c6	v
rekordním	rekordní	k2eAgInSc6d1	rekordní
počtu	počet	k1gInSc6	počet
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
her	hra	k1gFnPc2	hra
účastnilo	účastnit	k5eAaImAgNnS	účastnit
11	[number]	k4	11
303	[number]	k4	303
sportovců	sportovec	k1gMnPc2	sportovec
z	z	k7c2	z
206	[number]	k4	206
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
(	(	kIx(	(
<g/>
NOV	nov	k1gInSc1	nov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
premiérových	premiérový	k2eAgMnPc2d1	premiérový
účastníků	účastník	k1gMnPc2	účastník
Kosova	Kosův	k2eAgMnSc2d1	Kosův
a	a	k8xC	a
Jižního	jižní	k2eAgInSc2d1	jižní
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Soupeření	soupeření	k1gNnSc1	soupeření
o	o	k7c4	o
306	[number]	k4	306
sad	sada	k1gFnPc2	sada
medailí	medaile	k1gFnPc2	medaile
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
ve	v	k7c6	v
28	[number]	k4	28
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
35	[number]	k4	35
sportovištích	sportoviště	k1gNnPc6	sportoviště
<g/>
,	,	kIx,	,
situovaných	situovaný	k2eAgFnPc2d1	situovaná
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
západní	západní	k2eAgFnSc2d1	západní
zóny	zóna	k1gFnSc2	zóna
Ria	Ria	k1gFnSc2	Ria
v	v	k7c6	v
Barra	Barra	k1gFnSc1	Barra
da	da	k?	da
Tijuca	Tijuca	k1gFnSc1	Tijuca
<g/>
.	.	kIx.	.
</s>
<s>
Olympiádu	olympiáda	k1gFnSc4	olympiáda
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
i	i	k9	i
virus	virus	k1gInSc1	virus
zika	zikum	k1gNnSc2	zikum
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yRgNnSc3	který
do	do	k7c2	do
Ria	Ria	k1gFnSc2	Ria
nepřijeli	přijet	k5eNaPmAgMnP	přijet
někteří	některý	k3yIgMnPc1	některý
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
Čechů	Čech	k1gMnPc2	Čech
např.	např.	kA	např.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gMnSc1	Berdych
<g/>
,	,	kIx,	,
Karolína	Karolína	k1gFnSc1	Karolína
Plíšková	plíškový	k2eAgFnSc1d1	Plíšková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rio	Rio	k?	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
jihoamerickým	jihoamerický	k2eAgNnSc7d1	jihoamerické
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hostitelské	hostitelský	k2eAgNnSc1d1	hostitelské
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
na	na	k7c6	na
třináctém	třináctý	k4xOgInSc6	třináctý
olympijském	olympijský	k2eAgInSc6d1	olympijský
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
dánské	dánský	k2eAgFnSc6d1	dánská
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Volba	volba	k1gFnSc1	volba
pořadatele	pořadatel	k1gMnSc2	pořadatel
==	==	k?	==
</s>
</p>
<p>
<s>
Města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
uspořádání	uspořádání	k1gNnSc6	uspořádání
LOH	LOH	kA	LOH
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
podat	podat	k5eAaPmF	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgNnPc1d1	potenciální
kandidátská	kandidátský	k2eAgNnPc1d1	kandidátské
města	město	k1gNnPc1	město
prošla	projít	k5eAaPmAgFnS	projít
výběrovým	výběrový	k2eAgNnSc7d1	výběrové
řízením	řízení	k1gNnSc7	řízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
celkem	celkem	k6eAd1	celkem
jedenáct	jedenáct	k4xCc1	jedenáct
kritérií	kritérion	k1gNnPc2	kritérion
(	(	kIx(	(
<g/>
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc1	ubytování
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
pak	pak	k6eAd1	pak
komise	komise	k1gFnSc1	komise
zúžila	zúžit	k5eAaPmAgFnS	zúžit
výběr	výběr	k1gInSc4	výběr
na	na	k7c4	na
čtyři	čtyři	k4xCgMnPc4	čtyři
adepty	adept	k1gMnPc4	adept
<g/>
:	:	kIx,	:
americké	americký	k2eAgNnSc4d1	americké
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgInSc4d1	španělský
Madrid	Madrid	k1gInSc4	Madrid
<g/>
,	,	kIx,	,
brazilské	brazilský	k2eAgFnSc3d1	brazilská
Rio	Rio	k1gFnSc3	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
a	a	k8xC	a
japonské	japonský	k2eAgNnSc4d1	Japonské
Tokio	Tokio	k1gNnSc4	Tokio
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
hostilo	hostit	k5eAaImAgNnS	hostit
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
1964	[number]	k4	1964
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
je	on	k3xPp3gFnPc4	on
bude	být	k5eAaImBp3nS	být
hostit	hostit	k5eAaImF	hostit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
MOV	MOV	kA	MOV
<g/>
)	)	kIx)	)
nepodporoval	podporovat	k5eNaImAgMnS	podporovat
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
navzdory	navzdory	k7c3	navzdory
vyššímu	vysoký	k2eAgInSc3d2	vyšší
ohlasu	ohlas	k1gInSc3	ohlas
než	než	k8xS	než
vybrané	vybraný	k2eAgFnSc3d1	vybraná
Rio	Rio	k1gFnSc3	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
záměr	záměr	k1gInSc4	záměr
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
pořádat	pořádat	k5eAaImF	pořádat
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
nevyhovoval	vyhovovat	k5eNaImAgMnS	vyhovovat
kalendáři	kalendář	k1gInPc7	kalendář
MOV	MOV	kA	MOV
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Baku	Baku	k1gNnSc6	Baku
také	také	k9	také
nepřešly	přejít	k5eNaPmAgFnP	přejít
přes	přes	k7c4	přes
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
10	[number]	k4	10
<g/>
členné	členný	k2eAgFnSc2d1	členná
hodnotící	hodnotící	k2eAgFnSc2d1	hodnotící
komise	komise	k1gFnSc2	komise
stanula	stanout	k5eAaPmAgFnS	stanout
Maročanka	Maročanka	k1gFnSc1	Maročanka
Nawal	Nawal	k1gMnSc1	Nawal
El	Ela	k1gFnPc2	Ela
Moutawakel	Moutawakel	k1gMnSc1	Moutawakel
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k6eAd1	již
předsedala	předsedat	k5eAaImAgFnS	předsedat
hodnotící	hodnotící	k2eAgFnSc4d1	hodnotící
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
kandidatury	kandidatura	k1gFnPc4	kandidatura
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
provedla	provést	k5eAaPmAgFnS	provést
komise	komise	k1gFnSc1	komise
inspekce	inspekce	k1gFnSc1	inspekce
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
MOV	MOV	kA	MOV
obsáhlé	obsáhlý	k2eAgNnSc4d1	obsáhlé
odborné	odborný	k2eAgNnSc4d1	odborné
posouzení	posouzení	k1gNnSc4	posouzení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MOV	MOV	kA	MOV
udělalo	udělat	k5eAaPmAgNnS	udělat
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
místech	místo	k1gNnPc6	místo
mnoho	mnoho	k6eAd1	mnoho
omezení	omezení	k1gNnSc3	omezení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránil	zabránit	k5eAaPmAgInS	zabránit
kandidujícím	kandidující	k2eAgNnPc3d1	kandidující
městům	město	k1gNnPc3	město
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
nebo	nebo	k8xC	nebo
v	v	k7c6	v
přímém	přímý	k2eAgNnSc6d1	přímé
ovlivňování	ovlivňování	k1gNnSc6	ovlivňování
115	[number]	k4	115
hlasujících	hlasující	k2eAgInPc2d1	hlasující
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
kandidující	kandidující	k2eAgNnPc4d1	kandidující
města	město	k1gNnPc4	město
investovaly	investovat	k5eAaBmAgFnP	investovat
do	do	k7c2	do
svých	svůj	k3xOyFgMnPc2	svůj
PR	pr	k0	pr
a	a	k8xC	a
mediálních	mediální	k2eAgInPc2d1	mediální
programů	program	k1gInPc2	program
nemalé	malý	k2eNgFnSc2d1	nemalá
částky	částka	k1gFnSc2	částka
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
nepřímo	přímo	k6eNd1	přímo
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
členy	člen	k1gMnPc4	člen
MOV	MOV	kA	MOV
<g/>
,	,	kIx,	,
sbírat	sbírat	k5eAaImF	sbírat
domácí	domácí	k2eAgFnSc4d1	domácí
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
sportovních	sportovní	k2eAgNnPc2d1	sportovní
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
médií	médium	k1gNnPc2	médium
jako	jako	k9	jako
takových	takový	k3xDgFnPc2	takový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
v	v	k7c6	v
dánském	dánský	k2eAgNnSc6d1	dánské
hlavní	hlavní	k2eAgFnSc7d1	hlavní
městě	město	k1gNnSc6	město
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc4	Chicago
a	a	k8xC	a
Tokio	Tokio	k1gNnSc4	Tokio
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
a	a	k8xC	a
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
,	,	kIx,	,
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
posledního	poslední	k2eAgNnSc2d1	poslední
kola	kolo	k1gNnSc2	kolo
mířilo	mířit	k5eAaImAgNnS	mířit
Rio	Rio	k1gFnSc3	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
náskokem	náskok	k1gInSc7	náskok
před	před	k7c7	před
Madridem	Madrid	k1gInSc7	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
také	také	k6eAd1	také
udrželo	udržet	k5eAaPmAgNnS	udržet
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
jako	jako	k8xS	jako
hostitel	hostitel	k1gMnSc1	hostitel
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předem	předem	k6eAd1	předem
zamítnutí	zamítnutý	k2eAgMnPc1d1	zamítnutý
kandidáti	kandidát	k1gMnPc1	kandidát
===	===	k?	===
</s>
</p>
<p>
<s>
Baku	Baku	k1gNnSc1	Baku
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
</s>
</p>
<p>
<s>
Dauhá	Dauhat	k5eAaImIp3nS	Dauhat
<g/>
,	,	kIx,	,
Katar	katar	k1gInSc1	katar
</s>
</p>
<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
</s>
</p>
<p>
<s>
===	===	k?	===
Kandidáti	kandidát	k1gMnPc1	kandidát
a	a	k8xC	a
hlasování	hlasování	k1gNnSc1	hlasování
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Olympijská	olympijský	k2eAgNnPc1d1	Olympijské
sportoviště	sportoviště	k1gNnPc1	sportoviště
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Olympijský	olympijský	k2eAgInSc1d1	olympijský
park	park	k1gInSc1	park
Barra	Barr	k1gInSc2	Barr
===	===	k?	===
</s>
</p>
<p>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Olympijský	olympijský	k2eAgInSc1d1	olympijský
parkArena	parkAret	k5eAaPmNgFnS	parkAret
Carioca	carioca	k1gFnSc1	carioca
1	[number]	k4	1
<g/>
:	:	kIx,	:
basketbal	basketbal	k1gInSc4	basketbal
</s>
</p>
<p>
<s>
Arena	Areen	k2eAgFnSc1d1	Arena
Carioca	carioca	k1gFnSc1	carioca
2	[number]	k4	2
<g/>
:	:	kIx,	:
zápas	zápas	k1gInSc4	zápas
řecko-římský	řecko-římský	k2eAgInSc4d1	řecko-římský
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc4	zápas
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
judo	judo	k1gNnSc1	judo
</s>
</p>
<p>
<s>
Arena	Areen	k2eAgFnSc1d1	Arena
Carioca	carioca	k1gFnSc1	carioca
3	[number]	k4	3
<g/>
:	:	kIx,	:
šerm	šerm	k1gInSc1	šerm
<g/>
,	,	kIx,	,
taekwondo	taekwondo	k1gNnSc1	taekwondo
</s>
</p>
<p>
<s>
Arena	Arena	k6eAd1	Arena
do	do	k7c2	do
Futuro	Futura	k1gFnSc5	Futura
<g/>
:	:	kIx,	:
házená	házený	k2eAgNnPc1d1	házené
</s>
</p>
<p>
<s>
Parque	Parquat	k5eAaPmIp3nS	Parquat
Aquático	Aquático	k6eAd1	Aquático
Maria	Maria	k1gFnSc1	Maria
Lenk	Lenka	k1gFnPc2	Lenka
<g/>
:	:	kIx,	:
skoky	skok	k1gInPc7	skok
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
synchronizované	synchronizovaný	k2eAgNnSc1d1	synchronizované
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgNnSc1d1	vodní
pólo	pólo	k1gNnSc1	pólo
</s>
</p>
<p>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
stadion	stadion	k1gInSc1	stadion
plaveckých	plavecký	k2eAgInPc2d1	plavecký
sportů	sport	k1gInPc2	sport
<g/>
:	:	kIx,	:
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgNnSc1d1	vodní
pólo	pólo	k1gNnSc1	pólo
</s>
</p>
<p>
<s>
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
tenisové	tenisový	k2eAgNnSc1d1	tenisové
centrum	centrum	k1gNnSc1	centrum
<g/>
:	:	kIx,	:
tenis	tenis	k1gInSc1	tenis
</s>
</p>
<p>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
aréna	aréna	k1gFnSc1	aréna
<g/>
:	:	kIx,	:
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastika	gymnastika	k1gFnSc1	gymnastika
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
gymnastika	gymnastika	k1gFnSc1	gymnastika
<g/>
,	,	kIx,	,
skoky	skok	k1gInPc1	skok
na	na	k7c6	na
trampolíně	trampolína	k1gFnSc6	trampolína
</s>
</p>
<p>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
velodrom	velodrom	k1gInSc1	velodrom
<g/>
:	:	kIx,	:
dráhová	dráhový	k2eAgFnSc1d1	dráhová
cyklistika	cyklistika	k1gFnSc1	cyklistika
</s>
</p>
<p>
<s>
===	===	k?	===
Maracanã	Maracanã	k1gMnPc5	Maracanã
===	===	k?	===
</s>
</p>
<p>
<s>
Estádio	Estádio	k6eAd1	Estádio
do	do	k7c2	do
Maracanã	Maracanã	k1gFnSc2	Maracanã
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
slavností	slavnost	k1gFnSc7	slavnost
zahájení	zahájení	k1gNnSc2	zahájení
a	a	k8xC	a
zakončení	zakončení	k1gNnSc2	zakončení
LOH	LOH	kA	LOH
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estádio	Estádio	k1gMnSc1	Estádio
Olímpico	Olímpico	k1gMnSc1	Olímpico
Joã	Joã	k1gMnSc1	Joã
Havelange	Havelange	k1gFnSc1	Havelange
(	(	kIx(	(
<g/>
atletika	atletika	k1gFnSc1	atletika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ginásio	Ginásio	k6eAd1	Ginásio
do	do	k7c2	do
Maracanã	Maracanã	k1gFnSc2	Maracanã
(	(	kIx(	(
<g/>
volejbal	volejbal	k1gInSc1	volejbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sambadrome	Sambadrom	k1gInSc5	Sambadrom
Marquê	Marquê	k1gMnPc4	Marquê
de	de	k?	de
Sapucaí	Sapucaí	k1gFnSc1	Sapucaí
(	(	kIx(	(
<g/>
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
<g/>
,	,	kIx,	,
atletika	atletika	k1gFnSc1	atletika
<g/>
/	/	kIx~	/
<g/>
maraton	maraton	k1gInSc1	maraton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Copacabana	Copacabana	k1gFnSc1	Copacabana
===	===	k?	===
</s>
</p>
<p>
<s>
Copacabana	Copacabana	k1gFnSc1	Copacabana
(	(	kIx(	(
<g/>
plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Flamengo	flamengo	k1gNnSc1	flamengo
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
atletika	atletika	k1gFnSc1	atletika
<g/>
/	/	kIx~	/
<g/>
rychlá	rychlý	k2eAgFnSc1d1	rychlá
chůze	chůze	k1gFnSc1	chůze
<g/>
,	,	kIx,	,
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
/	/	kIx~	/
<g/>
silniční	silniční	k2eAgFnSc1d1	silniční
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fort	Fort	k?	Fort
Copacabana	Copacabana	k1gFnSc1	Copacabana
(	(	kIx(	(
plavání	plavání	k1gNnSc1	plavání
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
km	km	kA	km
maratón	maratón	k1gInSc4	maratón
<g/>
,	,	kIx,	,
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
/	/	kIx~	/
<g/>
časovka	časovka	k1gFnSc1	časovka
<g/>
,	,	kIx,	,
triatlon	triatlon	k1gInSc1	triatlon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rodrigo	Rodrigo	k1gMnSc1	Rodrigo
de	de	k?	de
Freitas	Freitas	k1gMnSc1	Freitas
Lagoon	Lagoon	k1gMnSc1	Lagoon
(	(	kIx(	(
<g/>
kanoistika	kanoistika	k1gFnSc1	kanoistika
<g/>
,	,	kIx,	,
veslování	veslování	k1gNnSc1	veslování
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marina	Marina	k1gFnSc1	Marina
da	da	k?	da
Glória	Glórium	k1gNnSc2	Glórium
(	(	kIx(	(
<g/>
jachting	jachting	k1gInSc1	jachting
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Deodoro	Deodora	k1gFnSc5	Deodora
===	===	k?	===
</s>
</p>
<p>
<s>
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
jezdecké	jezdecký	k2eAgNnSc1d1	jezdecké
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
jezdectví	jezdectví	k1gNnSc1	jezdectví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
National	Nationat	k5eAaImAgInS	Nationat
Shooting	Shooting	k1gInSc1	Shooting
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
střelba	střelba	k1gFnSc1	střelba
<g/>
)	)	kIx)	)
<g/>
Olympic	Olympic	k1gMnSc1	Olympic
Mountain	Mountain	k1gMnSc1	Mountain
Bike	Bike	k1gFnPc2	Bike
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
/	/	kIx~	/
<g/>
horská	horský	k2eAgNnPc4d1	horské
kola	kolo	k1gNnPc4	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Olympic	Olympic	k1gMnSc1	Olympic
BMX	BMX	kA	BMX
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
/	/	kIx~	/
<g/>
BMX	BMX	kA	BMX
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Olympic	Olympic	k1gMnSc1	Olympic
Whitewater	Whitewater	k1gInSc4	Whitewater
Stadium	stadium	k1gNnSc1	stadium
(	(	kIx(	(
<g/>
slalom	slalom	k1gInSc1	slalom
na	na	k7c6	na
divoké	divoký	k2eAgFnSc6d1	divoká
vodě	voda	k1gFnSc6	voda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Sportoviště	sportoviště	k1gNnPc4	sportoviště
mimo	mimo	k7c4	mimo
Rio	Rio	k1gFnSc4	Rio
===	===	k?	===
</s>
</p>
<p>
<s>
Itaipava	Itaipava	k1gFnSc1	Itaipava
Arena	Aren	k1gInSc2	Aren
Fonte	font	k1gInSc5	font
Nova-	Nova-	k1gMnSc1	Nova-
Salvador	Salvador	k1gInSc1	Salvador
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estádio	Estádio	k1gMnSc1	Estádio
Nacional	Nacional	k1gMnSc2	Nacional
Mané	Mané	k1gNnSc2	Mané
Garrincha-	Garrincha-	k1gMnSc2	Garrincha-
Brasília	Brasílius	k1gMnSc2	Brasílius
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estádio	Estádio	k1gMnSc1	Estádio
Mineirã	Mineirã	k1gMnSc1	Mineirã
Belo	Bela	k1gFnSc5	Bela
Horizonte	horizont	k1gInSc5	horizont
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc4	fotbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arena	Areen	k2eAgFnSc1d1	Arena
Corinthians-	Corinthians-	k1gFnSc1	Corinthians-
Sã	Sã	k1gFnSc2	Sã
Paulo	Paula	k1gFnSc5	Paula
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc4	fotbal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Účast	účast	k1gFnSc1	účast
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
==	==	k?	==
</s>
</p>
<p>
<s>
201	[number]	k4	201
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS	kvalifikovat
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
atleta	atlet	k1gMnSc4	atlet
<g/>
.	.	kIx.	.
</s>
<s>
Účastnily	účastnit	k5eAaImAgInP	účastnit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
dva	dva	k4xCgInPc4	dva
nezávislé	závislý	k2eNgInPc4d1	nezávislý
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
atletický	atletický	k2eAgInSc1d1	atletický
tým	tým	k1gInSc1	tým
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nezávislý	závislý	k2eNgInSc1d1	nezávislý
olympijský	olympijský	k2eAgInSc1d1	olympijský
atletický	atletický	k2eAgInSc1d1	atletický
tým	tým	k1gInSc1	tým
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
Súdán	Súdán	k1gInSc1	Súdán
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
olympijské	olympijský	k2eAgFnSc6d1	olympijská
výpravě	výprava	k1gFnSc6	výprava
se	se	k3xPyFc4	se
představuje	představovat	k5eAaImIp3nS	představovat
104	[number]	k4	104
českých	český	k2eAgMnPc2d1	český
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soutěže	soutěž	k1gFnSc2	soutěž
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
XXXI	XXXI	kA	XXXI
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
soutěže	soutěž	k1gFnPc1	soutěž
ve	v	k7c6	v
32	[number]	k4	32
sportovních	sportovní	k2eAgNnPc6d1	sportovní
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Novými	nový	k2eAgInPc7d1	nový
sporty	sport	k1gInPc7	sport
v	v	k7c6	v
programu	program	k1gInSc6	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
golf	golf	k1gInSc4	golf
po	po	k7c6	po
112	[number]	k4	112
letech	let	k1gInPc6	let
a	a	k8xC	a
ragby	ragby	k1gNnSc1	ragby
po	po	k7c6	po
92	[number]	k4	92
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
odvětví	odvětví	k1gNnSc1	odvětví
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Kalendář	kalendář	k1gInSc1	kalendář
soutěží	soutěž	k1gFnPc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Pořadí	pořadí	k1gNnSc1	pořadí
národů	národ	k1gInPc2	národ
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgFnPc4	třetí
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
po	po	k7c6	po
australských	australský	k2eAgFnPc6d1	australská
olympiádách	olympiáda	k1gFnPc6	olympiáda
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
a	a	k8xC	a
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgInPc1	druhý
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
nejblíže	blízce	k6eAd3	blízce
zemskému	zemský	k2eAgInSc3d1	zemský
rovníku	rovník	k1gInSc3	rovník
(	(	kIx(	(
<g/>
nejblíže	blízce	k6eAd3	blízce
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
1968	[number]	k4	1968
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
letní	letní	k2eAgFnSc1d1	letní
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
kompletně	kompletně	k6eAd1	kompletně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
příslušné	příslušný	k2eAgFnSc6d1	příslušná
polokouli	polokoule	k1gFnSc6	polokoule
zimní	zimní	k2eAgNnSc1d1	zimní
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2000	[number]	k4	2000
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
jara	jaro	k1gNnSc2	jaro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvaceti	dvacet	k4xCc2	dvacet
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
Brazílie	Brazílie	k1gFnSc2	Brazílie
hostila	hostit	k5eAaImAgFnS	hostit
po	po	k7c6	po
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2014	[number]	k4	2014
druhou	druhý	k4xOgFnSc4	druhý
nejexponovanější	exponovaný	k2eAgFnSc4d3	nejexponovanější
sportovní	sportovní	k2eAgFnSc4d1	sportovní
akci	akce	k1gFnSc4	akce
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
štafety	štafeta	k1gFnSc2	štafeta
s	s	k7c7	s
olympijským	olympijský	k2eAgInSc7d1	olympijský
ohněm	oheň	k1gInSc7	oheň
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
téměř	téměř	k6eAd1	téměř
12	[number]	k4	12
tisíc	tisíc	k4xCgInPc2	tisíc
běžců	běžec	k1gMnPc2	běžec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
celkem	celkem	k6eAd1	celkem
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vážné	vážný	k2eAgFnSc3d1	vážná
nemoci	nemoc	k1gFnSc3	nemoc
nemohl	moct	k5eNaImAgInS	moct
zapálit	zapálit	k5eAaPmF	zapálit
oheň	oheň	k1gInSc1	oheň
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
legenda	legenda	k1gFnSc1	legenda
Brazilec	Brazilec	k1gMnSc1	Brazilec
Pelé	Pelá	k1gFnSc2	Pelá
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
schválil	schválit	k5eAaPmAgMnS	schválit
MOV	MOV	kA	MOV
nové	nový	k2eAgInPc1d1	nový
sporty	sport	k1gInPc1	sport
pro	pro	k7c4	pro
LOH	LOH	kA	LOH
2020	[number]	k4	2020
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
:	:	kIx,	:
sportovní	sportovní	k2eAgNnPc1d1	sportovní
lezení	lezení	k1gNnPc1	lezení
<g/>
,	,	kIx,	,
baseball	baseball	k1gInSc1	baseball
<g/>
/	/	kIx~	/
<g/>
softball	softball	k1gInSc1	softball
<g/>
,	,	kIx,	,
karate	karate	k1gNnSc1	karate
<g/>
,	,	kIx,	,
skateboarding	skateboarding	k1gInSc1	skateboarding
a	a	k8xC	a
</s>
</p>
<p>
<s>
surfing	surfing	k1gInSc1	surfing
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
skialpinistická	skialpinistický	k2eAgFnSc1d1	skialpinistická
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
ISMF	ISMF	kA	ISMF
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
MOV	MOV	kA	MOV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kontroverze	kontroverze	k1gFnSc1	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obavy	obava	k1gFnPc1	obava
o	o	k7c6	o
dokončení	dokončení	k1gNnSc6	dokončení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
viceprezident	viceprezident	k1gMnSc1	viceprezident
MOV	MOV	kA	MOV
John	John	k1gMnSc1	John
Coates	Coates	k1gMnSc1	Coates
označil	označit	k5eAaPmAgMnS	označit
brazilské	brazilský	k2eAgFnSc2d1	brazilská
přípravy	příprava	k1gFnSc2	příprava
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
jsem	být	k5eAaImIp1nS	být
zažil	zažít	k5eAaPmAgMnS	zažít
<g/>
"	"	kIx"	"
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavební	stavební	k2eAgInPc1d1	stavební
a	a	k8xC	a
infrastrukturní	infrastrukturní	k2eAgInPc1d1	infrastrukturní
projekty	projekt	k1gInPc1	projekt
byly	být	k5eAaImAgInP	být
silně	silně	k6eAd1	silně
pozadu	pozadu	k6eAd1	pozadu
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
London	London	k1gMnSc1	London
Evening	Evening	k1gInSc1	Evening
Standard	standard	k1gInSc1	standard
ho	on	k3xPp3gInSc4	on
citovaly	citovat	k5eAaBmAgFnP	citovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
MOV	MOV	kA	MOV
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pracovní	pracovní	k2eAgFnSc4d1	pracovní
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
urychlit	urychlit	k5eAaPmF	urychlit
přípravy	příprava	k1gFnSc2	příprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kritická	kritický	k2eAgFnSc1d1	kritická
<g/>
"	"	kIx"	"
a	a	k8xC	a
došly	dojít	k5eAaPmAgFnP	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
zásah	zásah	k1gInSc1	zásah
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
bezprecedentní	bezprecedentní	k2eAgNnSc4d1	bezprecedentní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Brazílie	Brazílie	k1gFnSc1	Brazílie
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
největší	veliký	k2eAgFnSc2d3	veliký
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
recese	recese	k1gFnSc2	recese
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
otázky	otázka	k1gFnPc4	otázka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	on	k3xPp3gFnPc4	on
země	zem	k1gFnPc4	zem
dostatečně	dostatečně	k6eAd1	dostatečně
ekonomicky	ekonomicky	k6eAd1	ekonomicky
připravena	připravit	k5eAaPmNgFnS	připravit
pro	pro	k7c4	pro
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
olympiádu	olympiáda	k1gFnSc4	olympiáda
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
se	se	k3xPyFc4	se
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
112	[number]	k4	112
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
dřívějšími	dřívější	k2eAgFnPc7d1	dřívější
hrami	hra	k1gFnPc7	hra
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
rozpočet	rozpočet	k1gInSc1	rozpočet
stále	stále	k6eAd1	stále
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Olympiáda	olympiáda	k1gFnSc1	olympiáda
a	a	k8xC	a
pořádání	pořádání	k1gNnSc1	pořádání
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
před	před	k7c7	před
dvěma	dva	k4xCgNnPc7	dva
lety	léto	k1gNnPc7	léto
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
regionu	region	k1gInSc2	region
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
bankrotu	bankrot	k1gInSc2	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
také	také	k9	také
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
skandál	skandál	k1gInSc1	skandál
s	s	k7c7	s
praním	praní	k1gNnSc7	praní
špinavých	špinavý	k2eAgInPc2d1	špinavý
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
korupcí	korupce	k1gFnPc2	korupce
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
do	do	k7c2	do
plnohodnotné	plnohodnotný	k2eAgFnSc2d1	plnohodnotná
politické	politický	k2eAgFnSc2d1	politická
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
masivním	masivní	k2eAgFnPc3d1	masivní
demonstracím	demonstrace	k1gFnPc3	demonstrace
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
miliony	milion	k4xCgInPc7	milion
demonstranty	demonstrant	k1gMnPc7	demonstrant
<g/>
.	.	kIx.	.
<g/>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
stála	stát	k5eAaImAgFnS	stát
všechna	všechen	k3xTgNnPc4	všechen
sportoviště	sportoviště	k1gNnPc4	sportoviště
už	už	k6eAd1	už
rok	rok	k1gInSc4	rok
před	před	k7c7	před
hrami	hra	k1gFnPc7	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
dokončovaly	dokončovat	k5eAaImAgFnP	dokončovat
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
počáteční	počáteční	k2eAgFnPc4d1	počáteční
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
Olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgMnSc1d1	kompletní
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
stadionů	stadion	k1gInPc2	stadion
Velódromo	Velódroma	k1gFnSc5	Velódroma
Municipal	Municipal	k1gMnSc7	Municipal
do	do	k7c2	do
Rio	Rio	k1gFnSc1	Rio
(	(	kIx(	(
<g/>
76	[number]	k4	76
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arena	Arena	k1gFnSc1	Arena
da	da	k?	da
Juventude	Juventud	k1gInSc5	Juventud
(	(	kIx(	(
<g/>
75	[number]	k4	75
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Australský	australský	k2eAgInSc1d1	australský
olympijský	olympijský	k2eAgInSc1d1	olympijský
tým	tým	k1gInSc1	tým
bojkotoval	bojkotovat	k5eAaImAgInS	bojkotovat
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
vesnici	vesnice	k1gFnSc4	vesnice
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
úředníci	úředník	k1gMnPc1	úředník
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
přidělené	přidělený	k2eAgInPc4d1	přidělený
apartmány	apartmán	k1gInPc4	apartmán
za	za	k7c2	za
"	"	kIx"	"
<g/>
neobyvatelné	obyvatelný	k2eNgFnSc2d1	neobyvatelná
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ucpané	ucpaný	k2eAgFnPc1d1	ucpaná
toalety	toaleta	k1gFnPc1	toaleta
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
kapající	kapající	k2eAgFnSc1d1	kapající
ze	z	k7c2	z
stropu	strop	k1gInSc2	strop
<g/>
,	,	kIx,	,
odkryté	odkrytý	k2eAgFnPc1d1	odkrytá
elektroinstalace	elektroinstalace	k1gFnPc1	elektroinstalace
<g/>
,	,	kIx,	,
potemnělé	potemnělý	k2eAgNnSc1d1	potemnělé
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebyla	být	k5eNaImAgNnP	být
nainstalována	nainstalovat	k5eAaPmNgNnP	nainstalovat
žádná	žádný	k3yNgNnPc1	žádný
osvětlení	osvětlení	k1gNnPc1	osvětlení
a	a	k8xC	a
špinavé	špinavý	k2eAgFnPc1d1	špinavá
podlahy	podlaha	k1gFnPc1	podlaha
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nahlášené	nahlášený	k2eAgInPc4d1	nahlášený
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Australští	australský	k2eAgMnPc1d1	australský
sportovci	sportovec	k1gMnPc1	sportovec
proto	proto	k8xC	proto
zatím	zatím	k6eAd1	zatím
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
blízkých	blízký	k2eAgInPc6d1	blízký
hotelech	hotel	k1gInPc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
si	se	k3xPyFc3	se
stěžují	stěžovat	k5eAaImIp3nP	stěžovat
na	na	k7c4	na
podobné	podobný	k2eAgInPc4d1	podobný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
si	se	k3xPyFc3	se
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
na	na	k7c4	na
slabší	slabý	k2eAgFnSc4d2	slabší
úroveň	úroveň	k1gFnSc4	úroveň
areálu	areál	k1gInSc2	areál
a	a	k8xC	a
tenisového	tenisový	k2eAgNnSc2d1	tenisové
zázemí	zázemí	k1gNnSc2	zázemí
a	a	k8xC	a
Rio	Rio	k1gFnSc1	Rio
hodnotila	hodnotit	k5eAaImAgFnS	hodnotit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
posledních	poslední	k2eAgFnPc2d1	poslední
olympiád	olympiáda	k1gFnPc2	olympiáda
jako	jako	k8xC	jako
zatím	zatím	k6eAd1	zatím
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
slibům	slib	k1gInPc3	slib
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
existovali	existovat	k5eAaImAgMnP	existovat
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
před	před	k7c7	před
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
začátkem	začátek	k1gInSc7	začátek
her	hra	k1gFnPc2	hra
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
federální	federální	k2eAgFnSc1d1	federální
policie	policie	k1gFnSc1	policie
zatkla	zatknout	k5eAaPmAgFnS	zatknout
islámskou	islámský	k2eAgFnSc4d1	islámská
džihádistickou	džihádistický	k2eAgFnSc4d1	džihádistická
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
plánovala	plánovat	k5eAaImAgFnS	plánovat
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
útok	útok	k1gInSc4	útok
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
během	běh	k1gInSc7	běh
Mnichovského	mnichovský	k2eAgInSc2d1	mnichovský
masakru	masakr	k1gInSc2	masakr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
spojením	spojení	k1gNnSc7	spojení
na	na	k7c4	na
Islámský	islámský	k2eAgInSc4d1	islámský
stát	stát	k1gInSc4	stát
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
dva	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
<g/>
.	.	kIx.	.
<g/>
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
je	být	k5eAaImIp3nS	být
dlouho	dlouho	k6eAd1	dlouho
známo	znám	k2eAgNnSc1d1	známo
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
přepadení	přepadení	k1gNnSc2	přepadení
a	a	k8xC	a
krádeží	krádež	k1gFnPc2	krádež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stalo	stát	k5eAaPmAgNnS	stát
přes	přes	k7c4	přes
48	[number]	k4	48
700	[number]	k4	700
přepadení	přepadení	k1gNnPc2	přepadení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
trojnásobek	trojnásobek	k1gInSc4	trojnásobek
počtu	počet	k1gInSc2	počet
přepadení	přepadení	k1gNnSc2	přepadení
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
30	[number]	k4	30
procent	procento	k1gNnPc2	procento
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
navíc	navíc	k6eAd1	navíc
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
loupežného	loupežný	k2eAgNnSc2d1	loupežné
přepadení	přepadení	k1gNnSc2	přepadení
o	o	k7c4	o
14	[number]	k4	14
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
her	hra	k1gFnPc2	hra
byli	být	k5eAaImAgMnP	být
tři	tři	k4xCgMnPc4	tři
španělští	španělský	k2eAgMnPc1d1	španělský
členové	člen	k1gMnPc1	člen
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
australská	australský	k2eAgFnSc1d1	australská
členka	členka	k1gFnSc1	členka
olympijského	olympijský	k2eAgInSc2d1	olympijský
jachtařského	jachtařský	k2eAgInSc2d1	jachtařský
týmu	tým	k1gInSc2	tým
přepadeni	přepadnout	k5eAaPmNgMnP	přepadnout
a	a	k8xC	a
okradeni	okrást	k5eAaPmNgMnP	okrást
útočníky	útočník	k1gMnPc7	útočník
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
pistolemi	pistol	k1gFnPc7	pistol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
střelbě	střelba	k1gFnSc6	střelba
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
rychlostní	rychlostní	k2eAgFnSc6d1	rychlostní
silnici	silnice	k1gFnSc6	silnice
k	k	k7c3	k
olympijskému	olympijský	k2eAgInSc3d1	olympijský
areálu	areál	k1gInSc3	areál
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
i	i	k9	i
sedmnáctiletá	sedmnáctiletý	k2eAgFnSc1d1	sedmnáctiletá
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
země	zem	k1gFnSc2	zem
zvažovaly	zvažovat	k5eAaImAgInP	zvažovat
zařízení	zařízení	k1gNnSc4	zařízení
svých	svůj	k3xOyFgFnPc2	svůj
soukromých	soukromý	k2eAgFnPc2d1	soukromá
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
<g/>
.	.	kIx.	.
<g/>
Reprezentanta	reprezentant	k1gMnSc2	reprezentant
Nového	Nový	k1gMnSc2	Nový
Zélandu	Zéland	k1gInSc2	Zéland
Jasona	Jason	k1gMnSc2	Jason
Lee	Lea	k1gFnSc3	Lea
unesli	unést	k5eAaPmAgMnP	unést
autem	aut	k1gInSc7	aut
neznámí	známý	k2eNgMnPc1d1	neznámý
únosci	únosce	k1gMnPc1	únosce
<g/>
,	,	kIx,	,
po	po	k7c6	po
vybrání	vybrání	k1gNnSc6	vybrání
hotovosti	hotovost	k1gFnSc2	hotovost
z	z	k7c2	z
bankomatu	bankomat	k1gInSc2	bankomat
a	a	k8xC	a
předání	předání	k1gNnSc2	předání
částky	částka	k1gFnSc2	částka
byl	být	k5eAaImAgInS	být
jen	jen	k6eAd1	jen
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
auta	auto	k1gNnSc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
olympijské	olympijský	k2eAgFnSc6d1	olympijská
vesnici	vesnice	k1gFnSc6	vesnice
byli	být	k5eAaImAgMnP	být
okradeni	okraden	k2eAgMnPc1d1	okraden
členové	člen	k1gMnPc1	člen
dánského	dánský	k2eAgInSc2d1	dánský
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
z	z	k7c2	z
pokojů	pokoj	k1gInPc2	pokoj
jim	on	k3xPp3gInPc3	on
byla	být	k5eAaImAgFnS	být
odcizena	odcizen	k2eAgFnSc1d1	odcizena
elektronika	elektronika	k1gFnSc1	elektronika
i	i	k8xC	i
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Okradení	okradení	k1gNnSc1	okradení
a	a	k8xC	a
přepadení	přepadení	k1gNnSc1	přepadení
byly	být	k5eAaImAgInP	být
i	i	k8xC	i
novináři	novinář	k1gMnPc1	novinář
a	a	k8xC	a
turisti	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
zloději	zloděj	k1gMnPc1	zloděj
také	také	k9	také
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
celý	celý	k2eAgInSc4d1	celý
autobus	autobus	k1gInSc4	autobus
se	s	k7c7	s
třiceti	třicet	k4xCc7	třicet
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
basketbalisté	basketbalista	k1gMnPc1	basketbalista
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
přestřelky	přestřelka	k1gFnSc2	přestřelka
policie	policie	k1gFnSc2	policie
s	s	k7c7	s
gangem	gang	k1gInSc7	gang
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
přepadl	přepadnout	k5eAaPmAgInS	přepadnout
a	a	k8xC	a
okradl	okrást	k5eAaPmAgInS	okrást
i	i	k9	i
portugalského	portugalský	k2eAgMnSc2d1	portugalský
ministra	ministr	k1gMnSc2	ministr
školství	školství	k1gNnSc2	školství
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
ukončení	ukončení	k1gNnSc2	ukončení
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
cyklistické	cyklistický	k2eAgFnSc2d1	cyklistická
stezky	stezka	k1gFnSc2	stezka
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnSc2d1	postavená
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
hry	hra	k1gFnPc4	hra
zasažena	zasažen	k2eAgFnSc1d1	zasažena
vlnou	vlna	k1gFnSc7	vlna
a	a	k8xC	a
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
smrt	smrt	k1gFnSc4	smrt
dvou	dva	k4xCgMnPc2	dva
chodců	chodec	k1gMnPc2	chodec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Virus	virus	k1gInSc1	virus
zika	zik	k1gInSc2	zik
===	===	k?	===
</s>
</p>
<p>
<s>
Dramatické	dramatický	k2eAgNnSc4d1	dramatické
rozšíření	rozšíření	k1gNnSc4	rozšíření
viru	vir	k1gInSc2	vir
zika	zik	k1gInSc2	zik
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
obavy	obava	k1gFnPc4	obava
ohledně	ohledně	k7c2	ohledně
jeho	on	k3xPp3gInSc2	on
možného	možný	k2eAgInSc2d1	možný
dopadu	dopad	k1gInSc2	dopad
na	na	k7c4	na
sportovce	sportovec	k1gMnPc4	sportovec
a	a	k8xC	a
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
výskyt	výskyt	k1gInSc1	výskyt
viru	vir	k1gInSc2	vir
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
právě	právě	k9	právě
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
(	(	kIx(	(
<g/>
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgInPc1d1	četný
výzvy	výzev	k1gInPc1	výzev
vědců	vědec	k1gMnPc2	vědec
žádaly	žádat	k5eAaImAgInP	žádat
odložení	odložení	k1gNnSc4	odložení
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
by	by	kYmCp3nP	by
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
návštěvníci	návštěvník	k1gMnPc1	návštěvník
mohli	moct	k5eAaImAgMnP	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
virus	virus	k1gInSc1	virus
rychle	rychle	k6eAd1	rychle
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
sportovců	sportovec	k1gMnPc2	sportovec
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
hrách	hrách	k1gInSc4	hrách
kvůli	kvůli	k7c3	kvůli
znepokojení	znepokojení	k1gNnSc3	znepokojení
nad	nad	k7c7	nad
virem	vir	k1gInSc7	vir
Zika	Zika	k1gMnSc1	Zika
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Tomáš	Tomáš	k1gMnSc1	Tomáš
Berdych	Berdych	k1gInSc1	Berdych
<g/>
,	,	kIx,	,
Karolína	Karolína	k1gFnSc1	Karolína
Plíšková	plíškový	k2eAgFnSc1d1	Plíšková
<g/>
,	,	kIx,	,
Milos	Milos	k1gInSc1	Milos
Raonic	Raonice	k1gFnPc2	Raonice
<g/>
,	,	kIx,	,
Simona	Simona	k1gFnSc1	Simona
Halepová	Halepová	k1gFnSc1	Halepová
<g/>
,	,	kIx,	,
Jason	Jason	k1gNnSc1	Jason
Day	Day	k1gFnSc2	Day
<g/>
,	,	kIx,	,
Tejay	Tejaa	k1gFnSc2	Tejaa
van	vana	k1gFnPc2	vana
Garderen	Garderna	k1gFnPc2	Garderna
<g/>
,	,	kIx,	,
Branden	Brandna	k1gFnPc2	Brandna
Grace	Graec	k1gInSc2	Graec
<g/>
,	,	kIx,	,
Dustin	Dustin	k1gMnSc1	Dustin
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Shane	Shan	k1gInSc5	Shan
Lowry	Lowr	k1gInPc1	Lowr
<g/>
,	,	kIx,	,
Rory	Ror	k2eAgInPc1d1	Ror
McIlroy	McIlroy	k1gInPc1	McIlroy
<g/>
,	,	kIx,	,
Marc	Marc	k1gFnSc1	Marc
Leishman	Leishman	k1gMnSc1	Leishman
<g/>
,	,	kIx,	,
Charl	Charl	k1gMnSc1	Charl
Schwartzel	Schwartzel	k1gMnSc1	Schwartzel
<g/>
,	,	kIx,	,
Angelo	Angela	k1gFnSc5	Angela
Que	Que	k1gMnSc1	Que
a	a	k8xC	a
Jordan	Jordan	k1gMnSc1	Jordan
Spieth	Spieth	k1gMnSc1	Spieth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
ale	ale	k8xC	ale
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
nebyly	být	k5eNaImAgFnP	být
mezi	mezi	k7c7	mezi
sportovci	sportovec	k1gMnPc7	sportovec
ani	ani	k8xC	ani
návštěvníky	návštěvník	k1gMnPc7	návštěvník
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
případy	případ	k1gInPc1	případ
výskytu	výskyt	k1gInSc2	výskyt
viru	vir	k1gInSc2	vir
Zika	Zika	k1gMnSc1	Zika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zákaz	zákaz	k1gInSc1	zákaz
startu	start	k1gInSc2	start
sportovců	sportovec	k1gMnPc2	sportovec
===	===	k?	===
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
atletická	atletický	k2eAgFnSc1d1	atletická
federace	federace	k1gFnSc1	federace
IAAF	IAAF	kA	IAAF
suspendovala	suspendovat	k5eAaPmAgFnS	suspendovat
ruský	ruský	k2eAgInSc4d1	ruský
atletický	atletický	k2eAgInSc4d1	atletický
svaz	svaz	k1gInSc4	svaz
kvůli	kvůli	k7c3	kvůli
systematickému	systematický	k2eAgInSc3d1	systematický
dopingu	doping	k1gInSc3	doping
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
68	[number]	k4	68
ruských	ruský	k2eAgMnPc2d1	ruský
atletů	atlet	k1gMnPc2	atlet
se	se	k3xPyFc4	se
dovolala	dovolat	k5eAaPmAgFnS	dovolat
k	k	k7c3	k
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
sportovní	sportovní	k2eAgFnSc3d1	sportovní
arbitráži	arbitráž	k1gFnSc3	arbitráž
CAS	CAS	kA	CAS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
odvolání	odvolání	k1gNnSc3	odvolání
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
ministr	ministr	k1gMnSc1	ministr
sportu	sport	k1gInSc2	sport
Vitalij	Vitalij	k1gMnSc1	Vitalij
Mutko	Mutko	k1gNnSc4	Mutko
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
nad	nad	k7c7	nad
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
lítost	lítost	k1gFnSc4	lítost
<g/>
.	.	kIx.	.
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
také	také	k9	také
hrozil	hrozit	k5eAaImAgInS	hrozit
vyloučením	vyloučení	k1gNnPc3	vyloučení
celému	celý	k2eAgInSc3d1	celý
ruskému	ruský	k2eAgInSc3d1	ruský
olympijskému	olympijský	k2eAgInSc3d1	olympijský
týmu	tým	k1gInSc3	tým
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
opatření	opatření	k1gNnSc1	opatření
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
hry	hra	k1gFnPc4	hra
nesmí	smět	k5eNaImIp3nS	smět
nominovat	nominovat	k5eAaBmF	nominovat
Rusko	Rusko	k1gNnSc1	Rusko
žádného	žádný	k1gMnSc2	žádný
sportovce	sportovec	k1gMnSc2	sportovec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dostal	dostat	k5eAaPmAgMnS	dostat
trest	trest	k1gInSc4	trest
za	za	k7c4	za
doping	doping	k1gInSc4	doping
<g/>
.	.	kIx.	.
<g/>
Pozitivními	pozitivní	k2eAgInPc7d1	pozitivní
dopingovými	dopingový	k2eAgInPc7d1	dopingový
testy	test	k1gInPc7	test
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
také	také	k9	také
bulharský	bulharský	k2eAgInSc1d1	bulharský
tým	tým	k1gInSc1	tým
vzpěračů	vzpěrač	k1gMnPc2	vzpěrač
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vměšování	vměšování	k1gNnSc3	vměšování
státu	stát	k1gInSc2	stát
do	do	k7c2	do
chodu	chod	k1gInSc2	chod
sportovních	sportovní	k2eAgFnPc2d1	sportovní
organizací	organizace	k1gFnPc2	organizace
byl	být	k5eAaImAgInS	být
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
,	,	kIx,	,
kuvajtští	kuvajtský	k2eAgMnPc1d1	kuvajtský
sportovci	sportovec	k1gMnPc1	sportovec
tak	tak	k9	tak
mohli	moct	k5eAaImAgMnP	moct
závodit	závodit	k5eAaImF	závodit
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
jen	jen	k9	jen
pod	pod	k7c7	pod
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kanalizace	kanalizace	k1gFnSc1	kanalizace
===	===	k?	===
</s>
</p>
<p>
<s>
Záliv	záliv	k1gInSc1	záliv
Guanabara	Guanabara	k1gFnSc1	Guanabara
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
vody	voda	k1gFnSc2	voda
budou	být	k5eAaImBp3nP	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
jachting	jachting	k1gInSc4	jachting
a	a	k8xC	a
windsurfing	windsurfing	k1gInSc4	windsurfing
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
znečištěný	znečištěný	k2eAgInSc1d1	znečištěný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgFnPc7d1	hlavní
příčinami	příčina	k1gFnPc7	příčina
znečištění	znečištění	k1gNnSc2	znečištění
je	být	k5eAaImIp3nS	být
odpad	odpad	k1gInSc1	odpad
přiváděný	přiváděný	k2eAgInSc1d1	přiváděný
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
přes	přes	k7c4	přes
znečištěné	znečištěný	k2eAgFnPc4d1	znečištěná
řeky	řeka	k1gFnPc4	řeka
z	z	k7c2	z
chudinských	chudinský	k2eAgFnPc2d1	chudinská
čtvrtích	čtvrt	k1gFnPc6	čtvrt
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
polykat	polykat	k5eAaImF	polykat
a	a	k8xC	a
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
byla	být	k5eAaImAgFnS	být
i	i	k9	i
její	její	k3xOp3gFnSc4	její
vhodnost	vhodnost	k1gFnSc4	vhodnost
pro	pro	k7c4	pro
plavání	plavání	k1gNnSc4	plavání
<g/>
.	.	kIx.	.
</s>
<s>
Cíl	cíl	k1gInSc1	cíl
vyčištění	vyčištění	k1gNnSc1	vyčištění
80	[number]	k4	80
%	%	kIx~	%
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
přiváděny	přivádět	k5eAaImNgFnP	přivádět
do	do	k7c2	do
zátoky	zátoka	k1gFnSc2	zátoka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
splnit	splnit	k5eAaPmF	splnit
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
60	[number]	k4	60
%	%	kIx~	%
z	z	k7c2	z
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
bylo	být	k5eAaImAgNnS	být
vyčištěno	vyčistit	k5eAaPmNgNnS	vyčistit
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vyčištění	vyčištění	k1gNnPc2	vyčištění
65	[number]	k4	65
%	%	kIx~	%
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
ke	k	k7c3	k
spuštění	spuštění	k1gNnSc3	spuštění
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
<g/>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
jachtař	jachtař	k1gMnSc1	jachtař
Nico	Nico	k1gMnSc1	Nico
Delle	Delle	k1gFnSc2	Delle
Karth	Karth	k1gMnSc1	Karth
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kdy	kdy	k6eAd1	kdy
trénoval	trénovat	k5eAaImAgMnS	trénovat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
prý	prý	k9	prý
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
byly	být	k5eAaImAgInP	být
odpadky	odpadek	k1gInPc1	odpadek
od	od	k7c2	od
pneumatik	pneumatika	k1gFnPc2	pneumatika
až	až	k9	až
po	po	k7c4	po
plovoucí	plovoucí	k2eAgFnPc4d1	plovoucí
matrace	matrace	k1gFnPc4	matrace
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
prý	prý	k9	prý
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
špatně	špatně	k6eAd1	špatně
vyčištěná	vyčištěný	k2eAgFnSc1d1	vyčištěná
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
bál	bát	k5eAaImAgMnS	bát
i	i	k9	i
strčit	strčit	k5eAaPmF	strčit
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Brazilští	brazilský	k2eAgMnPc1d1	brazilský
vědci	vědec	k1gMnPc1	vědec
našli	najít	k5eAaPmAgMnP	najít
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
odolné	odolný	k2eAgFnSc2d1	odolná
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Viry	vir	k1gInPc1	vir
ve	v	k7c6	v
vodních	vodní	k2eAgFnPc6d1	vodní
cestách	cesta	k1gFnPc6	cesta
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
byly	být	k5eAaImAgFnP	být
změřeny	změřit	k5eAaPmNgFnP	změřit
na	na	k7c4	na
1,7	[number]	k4	1,7
milionů	milion	k4xCgInPc2	milion
násobku	násobek	k1gInSc2	násobek
úrovně	úroveň	k1gFnSc2	úroveň
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
německý	německý	k2eAgMnSc1d1	německý
jachtař	jachtař	k1gMnSc1	jachtař
Erik	Erika	k1gFnPc2	Erika
Heil	Heila	k1gFnPc2	Heila
byl	být	k5eAaImAgInS	být
infikován	infikovat	k5eAaBmNgInS	infikovat
multirezistentními	multirezistentní	k2eAgFnPc7d1	multirezistentní
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
kanalizací	kanalizace	k1gFnSc7	kanalizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organizace	organizace	k1gFnSc2	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
zápase	zápas	k1gInSc6	zápas
Nigérie	Nigérie	k1gFnSc2	Nigérie
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
byla	být	k5eAaImAgFnS	být
místo	místo	k1gNnSc4	místo
hymny	hymna	k1gFnSc2	hymna
Nigérie	Nigérie	k1gFnSc2	Nigérie
zahraná	zahraný	k2eAgFnSc1d1	zahraná
hymna	hymna	k1gFnSc1	hymna
Nigeru	Niger	k1gInSc2	Niger
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
naštvalo	naštvat	k5eAaPmAgNnS	naštvat
fanoušky	fanoušek	k1gMnPc4	fanoušek
a	a	k8xC	a
během	během	k7c2	během
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
minuty	minuta	k1gFnSc2	minuta
se	se	k3xPyFc4	se
organizátoři	organizátor	k1gMnPc1	organizátor
omluvili	omluvit	k5eAaPmAgMnP	omluvit
<g/>
.	.	kIx.	.
<g/>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
užívaná	užívaný	k2eAgFnSc1d1	užívaná
špatná	špatný	k2eAgFnSc1d1	špatná
verze	verze	k1gFnSc1	verze
čínské	čínský	k2eAgFnSc2d1	čínská
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
čtyři	čtyři	k4xCgFnPc4	čtyři
menší	malý	k2eAgFnPc4d2	menší
hvězdy	hvězda	k1gFnPc4	hvězda
směřovaly	směřovat	k5eAaImAgFnP	směřovat
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
vlajky	vlajka	k1gFnPc1	vlajka
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
oficiálních	oficiální	k2eAgInPc6d1	oficiální
materiálech	materiál	k1gInPc6	materiál
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vlajky	vlajka	k1gFnSc2	vlajka
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
delegace	delegace	k1gFnSc2	delegace
sportovců	sportovec	k1gMnPc2	sportovec
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naštvalo	naštvat	k5eAaPmAgNnS	naštvat
čínská	čínský	k2eAgNnPc4d1	čínské
státní	státní	k2eAgNnPc4d1	státní
média	médium	k1gNnPc4	médium
<g/>
.9	.9	k4	.9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
bazén	bazén	k1gInSc1	bazén
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
pro	pro	k7c4	pro
potápěčské	potápěčský	k2eAgFnPc4d1	potápěčská
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
přebarvovat	přebarvovat	k5eAaImF	přebarvovat
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
namísto	namísto	k7c2	namísto
své	svůj	k3xOyFgFnSc2	svůj
přirozené	přirozený	k2eAgFnSc2d1	přirozená
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
hrálo	hrát	k5eAaImAgNnS	hrát
vodního	vodní	k2eAgNnSc2d1	vodní
pólo	pólo	k1gNnSc4	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
sportovců	sportovec	k1gMnPc2	sportovec
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
jejich	jejich	k3xOp3gInSc4	jejich
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
že	že	k9	že
jim	on	k3xPp3gMnPc3	on
brání	bránit	k5eAaImIp3nS	bránit
ve	v	k7c6	v
viditelnosti	viditelnost	k1gFnSc6	viditelnost
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dráždilo	dráždit	k5eAaImAgNnS	dráždit
jejich	jejich	k3xOp3gNnPc4	jejich
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
barvy	barva	k1gFnSc2	barva
byla	být	k5eAaImAgFnS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
přidáním	přidání	k1gNnSc7	přidání
dechloračního	dechlorační	k2eAgInSc2d1	dechlorační
sterilizátoru	sterilizátor	k1gInSc2	sterilizátor
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
chlorovaná	chlorovaný	k2eAgFnSc1d1	chlorovaná
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
neutralizoval	neutralizovat	k5eAaBmAgInS	neutralizovat
sterilizační	sterilizační	k2eAgInSc1d1	sterilizační
účinek	účinek	k1gInSc1	účinek
chloru	chlor	k1gInSc2	chlor
<g/>
.15	.15	k4	.15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
zřítila	zřítit	k5eAaPmAgFnS	zřítit
visutá	visutý	k2eAgFnSc1d1	visutá
TV	TV	kA	TV
kamera	kamera	k1gFnSc1	kamera
na	na	k7c6	na
olympijském	olympijský	k2eAgInSc6d1	olympijský
parku	park	k1gInSc6	park
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
zranila	zranit	k5eAaPmAgFnS	zranit
sedm	sedm	k4xCc4	sedm
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jedenáctileté	jedenáctiletý	k2eAgFnSc2d1	jedenáctiletá
dívky	dívka	k1gFnSc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Organizátoři	organizátor	k1gMnPc1	organizátor
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přetrhl	přetrhnout	k5eAaPmAgInS	přetrhnout
kabel	kabel	k1gInSc1	kabel
držící	držící	k2eAgInSc1d1	držící
zařízení	zařízení	k1gNnSc3	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
nalezli	nalézt	k5eAaBmAgMnP	nalézt
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nedaleko	nedaleko	k7c2	nedaleko
jachtařských	jachtařský	k2eAgFnPc2d1	jachtařská
her	hra	k1gFnPc2	hra
uříznutou	uříznutý	k2eAgFnSc4d1	uříznutá
nohu	noha	k1gFnSc4	noha
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
Oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgFnPc3d1	minulá
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
olympiádu	olympiáda	k1gFnSc4	olympiáda
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
malý	malý	k2eAgInSc4d1	malý
zájem	zájem	k1gInSc4	zájem
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Prázdná	prázdný	k2eAgNnPc4d1	prázdné
sedadla	sedadlo	k1gNnPc4	sedadlo
byla	být	k5eAaImAgFnS	být
zčásti	zčásti	k6eAd1	zčásti
způsobena	způsobit	k5eAaPmNgFnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
11	[number]	k4	11
%	%	kIx~	%
prodaných	prodaný	k2eAgInPc2d1	prodaný
lístků	lístek	k1gInPc2	lístek
nebylo	být	k5eNaImAgNnS	být
uplatněných	uplatněný	k2eAgNnPc2d1	uplatněné
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
MOV	MOV	kA	MOV
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rio	Rio	k1gFnPc2	Rio
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
nejtěžšími	těžký	k2eAgFnPc7d3	nejtěžší
hrami	hra	k1gFnPc7	hra
a	a	k8xC	a
počet	počet	k1gInSc1	počet
diváků	divák	k1gMnPc2	divák
byl	být	k5eAaImAgMnS	být
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
,	,	kIx,	,
kusy	kus	k1gInPc1	kus
prázdných	prázdný	k2eAgNnPc2d1	prázdné
míst	místo	k1gNnPc2	místo
byly	být	k5eAaImAgFnP	být
zdrojem	zdroj	k1gInSc7	zdroj
frustrace	frustrace	k1gFnSc2	frustrace
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
se	se	k3xPyFc4	se
často	často	k6eAd1	často
chovali	chovat	k5eAaImAgMnP	chovat
hlučně	hlučně	k6eAd1	hlučně
<g/>
,	,	kIx,	,
a	a	k8xC	a
nesportovně	sportovně	k6eNd1	sportovně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pískání	pískání	k1gNnSc4	pískání
a	a	k8xC	a
bučení	bučení	k1gNnSc4	bučení
si	se	k3xPyFc3	se
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
například	například	k6eAd1	například
volejbalistky	volejbalistka	k1gFnPc4	volejbalistka
Markéta	Markéta	k1gFnSc1	Markéta
Sluková	Sluková	k1gFnSc1	Sluková
s	s	k7c7	s
Barborou	Barbora	k1gFnSc7	Barbora
Hermannovou	Hermannová	k1gFnSc7	Hermannová
a	a	k8xC	a
tyčkař	tyčkař	k1gMnSc1	tyčkař
Renaud	Renaud	k1gMnSc1	Renaud
Lavillenie	Lavillenie	k1gFnSc1	Lavillenie
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
Brazilců	Brazilec	k1gMnPc2	Brazilec
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
šéf	šéf	k1gMnSc1	šéf
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
Thomas	Thomas	k1gMnSc1	Thomas
Bach	Bach	k1gMnSc1	Bach
<g/>
.	.	kIx.	.
<g/>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
olympiádě	olympiáda	k1gFnSc6	olympiáda
jsou	být	k5eAaImIp3nP	být
sportoviště	sportoviště	k1gNnSc4	sportoviště
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
zdevastovaná	zdevastovaný	k2eAgFnSc1d1	zdevastovaná
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
opuštěná	opuštěný	k2eAgFnSc1d1	opuštěná
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
Maracanã	Maracanã	k1gMnPc2	Maracanã
byl	být	k5eAaImAgInS	být
vyrabovaný	vyrabovaný	k2eAgInSc1d1	vyrabovaný
a	a	k8xC	a
elektřina	elektřina	k1gFnSc1	elektřina
byla	být	k5eAaImAgFnS	být
úplně	úplně	k6eAd1	úplně
vypojená	vypojený	k2eAgFnSc1d1	vypojená
po	po	k7c6	po
obrovském	obrovský	k2eAgInSc6d1	obrovský
nesplaceném	splacený	k2eNgInSc6d1	nesplacený
účtu	účet	k1gInSc6	účet
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
950	[number]	k4	950
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Olympiáda	olympiáda	k1gFnSc1	olympiáda
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Brazílii	Brazílie	k1gFnSc4	Brazílie
zničující	zničující	k2eAgInPc4d1	zničující
a	a	k8xC	a
přidala	přidat	k5eAaPmAgFnS	přidat
na	na	k7c4	na
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
zničující	zničující	k2eAgFnSc3d1	zničující
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
recesi	recese	k1gFnSc3	recese
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
věřitelé	věřitel	k1gMnPc1	věřitel
stále	stále	k6eAd1	stále
poptávají	poptávat	k5eAaImIp3nP	poptávat
dluh	dluh	k1gInSc4	dluh
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
3600	[number]	k4	3600
nových	nový	k2eAgInPc2d1	nový
bytů	byt	k1gInPc2	byt
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
nevyužitých	využitý	k2eNgFnPc2d1	nevyužitá
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
260	[number]	k4	260
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
za	za	k7c4	za
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
korupce	korupce	k1gFnSc2	korupce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
dít	dít	k5eAaBmF	dít
při	při	k7c6	při
stavbách	stavba	k1gFnPc6	stavba
olympijských	olympijský	k2eAgInPc2d1	olympijský
stavebních	stavební	k2eAgInPc2d1	stavební
projektů	projekt	k1gInPc2	projekt
u	u	k7c2	u
brazilských	brazilský	k2eAgMnPc2d1	brazilský
podnikatelů	podnikatel	k1gMnPc2	podnikatel
i	i	k8xC	i
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2016	[number]	k4	2016
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
2016	[number]	k4	2016
–	–	k?	–
oficiální	oficiální	k2eAgInSc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
