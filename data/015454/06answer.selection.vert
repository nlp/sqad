<s>
Řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
(	(	kIx(
<g/>
též	též	k9
ATC	ATC	kA
z	z	k7c2
–	–	k?
anglického	anglický	k2eAgMnSc4d1
air	air	k?
traffic	traffic	k1gMnSc1
control	control	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
poskytovaná	poskytovaný	k2eAgFnSc1d1
ze	z	k7c2
země	zem	k1gFnSc2
letadlům	letadlo	k1gNnPc3
na	na	k7c4
pohybujícím	pohybující	k2eAgInPc3d1
se	se	k3xPyFc4
v	v	k7c6
řízeném	řízený	k2eAgInSc6d1
vzdušném	vzdušný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
řízeném	řízený	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
</s>