<s>
Řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Řídicí	řídicí	k2eAgFnSc1d1
věž	věž	k1gFnSc1
letiště	letiště	k1gNnSc2
Paříž-Orly	Paříž-Orla	k1gFnSc2
</s>
<s>
Řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
(	(	kIx(
<g/>
též	též	k9
ATC	ATC	kA
z	z	k7c2
–	–	k?
anglického	anglický	k2eAgMnSc4d1
air	air	k?
traffic	traffic	k1gMnSc1
control	control	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
poskytovaná	poskytovaný	k2eAgFnSc1d1
ze	z	k7c2
země	zem	k1gFnSc2
letadlům	letadlo	k1gNnPc3
na	na	k7c4
pohybujícím	pohybující	k2eAgInPc3d1
se	se	k3xPyFc4
v	v	k7c6
řízeném	řízený	k2eAgInSc6d1
vzdušném	vzdušný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
řízeném	řízený	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
smyslem	smysl	k1gInSc7
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
je	být	k5eAaImIp3nS
předcházet	předcházet	k5eAaImF
srážkám	srážka	k1gFnPc3
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
i	i	k8xC
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
řídící	řídící	k2eAgFnSc1d1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
poskytují	poskytovat	k5eAaImIp3nP
pilotům	pilot	k1gMnPc3
obvykle	obvykle	k6eAd1
i	i	k9
další	další	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
navigační	navigační	k2eAgFnSc1d1
pomoc	pomoc	k1gFnSc1
nebo	nebo	k8xC
informační	informační	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
českém	český	k2eAgInSc6d1
vzdušném	vzdušný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
a	a	k8xC
na	na	k7c6
velkých	velký	k2eAgNnPc6d1
českých	český	k2eAgNnPc6d1
letištích	letiště	k1gNnPc6
tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
zajišťuje	zajišťovat	k5eAaImIp3nS
státní	státní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Profese	profese	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
službu	služba	k1gFnSc4
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
řídící	řídící	k2eAgFnSc1d1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
často	často	k6eAd1
bývá	bývat	k5eAaImIp3nS
ale	ale	k8xC
používán	používat	k5eAaImNgInS
také	také	k9
nepřesný	přesný	k2eNgInSc1d1
termín	termín	k1gInSc1
letecký	letecký	k2eAgMnSc1d1
dispečer	dispečer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
chybný	chybný	k2eAgInSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
letečtí	letecký	k2eAgMnPc1d1
dispečeři	dispečer	k1gMnPc1
lety	let	k1gInPc4
plánují	plánovat	k5eAaImIp3nP
a	a	k8xC
neřídí	řídit	k5eNaImIp3nS
provoz	provoz	k1gInSc4
v	v	k7c6
letovém	letový	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Standardy	standard	k1gInPc1
</s>
<s>
Celosvětově	celosvětově	k6eAd1
používaným	používaný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
v	v	k7c6
řízení	řízení	k1gNnSc6
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
pravidly	pravidlo	k1gNnPc7
ICAO	ICAO	kA
angličtina	angličtina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
zemí	zem	k1gFnPc2
je	být	k5eAaImIp3nS
dovoleno	dovolit	k5eAaPmNgNnS
i	i	k9
používání	používání	k1gNnSc1
místního	místní	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
ke	k	k7c3
komunikaci	komunikace	k1gFnSc3
mezi	mezi	k7c7
pilotem	pilot	k1gMnSc7
soukromých	soukromý	k2eAgMnPc2d1
či	či	k8xC
výcvikových	výcvikový	k2eAgInPc2d1
letů	let	k1gInPc2
a	a	k8xC
řídícím	řídící	k2eAgInSc7d1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
většině	většina	k1gFnSc6
světa	svět	k1gInSc2
se	se	k3xPyFc4
při	při	k7c6
řízení	řízení	k1gNnSc6
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
letectví	letectví	k1gNnSc6
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
používají	používat	k5eAaImIp3nP
imperiální	imperiální	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
–	–	k?
tedy	tedy	k8xC
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
se	se	k3xPyFc4
měří	měřit	k5eAaImIp3nS
ve	v	k7c6
stopách	stopa	k1gFnPc6
<g/>
,	,	kIx,
rychlost	rychlost	k1gFnSc1
v	v	k7c6
uzlech	uzel	k1gInPc6
a	a	k8xC
vzdálenost	vzdálenost	k1gFnSc4
v	v	k7c6
námořních	námořní	k2eAgFnPc6d1
mílích	míle	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Specializovaná	specializovaný	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
řízení	řízení	k1gNnSc2
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
řízených	řízený	k2eAgInPc2d1
letů	let	k1gInPc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
zajištěna	zajistit	k5eAaPmNgFnS
od	od	k7c2
pojíždění	pojíždění	k1gNnSc2
na	na	k7c6
letišti	letiště	k1gNnSc6
před	před	k7c7
startem	start	k1gInSc7
až	až	k9
po	po	k7c6
dokončení	dokončení	k1gNnSc6
letu	let	k1gInSc2
a	a	k8xC
bezpečné	bezpečný	k2eAgNnSc4d1
zastavení	zastavení	k1gNnSc4
na	na	k7c6
cílovém	cílový	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
poskytována	poskytovat	k5eAaImNgFnS
obvykle	obvykle	k6eAd1
třemi	tři	k4xCgNnPc7
navzájem	navzájem	k6eAd1
spolupracujícími	spolupracující	k2eAgFnPc7d1
specializovanými	specializovaný	k2eAgFnPc7d1
středisky	středisko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Letištní	letištní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
</s>
<s>
Pro	pro	k7c4
letištní	letištní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
řízení	řízení	k1gNnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
TWR	TWR	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
tower	tower	k1gInSc1
–	–	k?
věž	věž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
základním	základní	k2eAgInSc7d1
bodem	bod	k1gInSc7
pro	pro	k7c4
řízení	řízení	k1gNnSc4
provozu	provoz	k1gInSc2
na	na	k7c6
letišti	letiště	k1gNnSc6
je	být	k5eAaImIp3nS
řídící	řídící	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídící	řídící	k2eAgFnSc2d1
na	na	k7c6
věži	věž	k1gFnSc6
jsou	být	k5eAaImIp3nP
zodpovědní	zodpovědný	k2eAgMnPc1d1
za	za	k7c4
bezpečný	bezpečný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
na	na	k7c6
přistávací	přistávací	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
(	(	kIx(
<g/>
povolení	povolení	k1gNnSc1
vstupu	vstup	k1gInSc2
a	a	k8xC
přejíždění	přejíždění	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pojezdových	pojezdový	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
v	v	k7c6
nepřítomnosti	nepřítomnost	k1gFnSc6
pozice	pozice	k1gFnSc2
GND	GND	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
řízeném	řízený	k2eAgInSc6d1
okrsku	okrsek	k1gInSc6
(	(	kIx(
<g/>
zvaném	zvaný	k2eAgInSc6d1
též	též	k9
CTR	CTR	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Control	Control	k1gInSc1
Zone	Zon	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc4d1
vzdušný	vzdušný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
v	v	k7c6
bezprostředním	bezprostřední	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
letiště	letiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídící	řídící	k2eAgFnSc2d1
na	na	k7c6
věži	věž	k1gFnSc6
zajišťují	zajišťovat	k5eAaImIp3nP
nejen	nejen	k6eAd1
bezpečný	bezpečný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pozemních	pozemní	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
po	po	k7c6
letištní	letištní	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídící	řídící	k2eAgFnSc1d1
věž	věž	k1gFnSc1
také	také	k9
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
letové	letový	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
když	když	k8xS
není	být	k5eNaImIp3nS
přítomna	přítomen	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
GND	GND	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
poskytována	poskytován	k2eAgFnSc1d1
na	na	k7c6
těchto	tento	k3xDgNnPc6
civilních	civilní	k2eAgNnPc6d1
letištích	letiště	k1gNnPc6
<g/>
:	:	kIx,
Letiště	letiště	k1gNnPc1
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Letiště	letiště	k1gNnSc2
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
<g/>
,	,	kIx,
Letiště	letiště	k1gNnSc4
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Letiště	letiště	k1gNnSc2
Vodochody	Vodochod	k1gInPc4
<g/>
,	,	kIx,
Letiště	letiště	k1gNnSc1
Kunovice	Kunovice	k1gFnPc1
<g/>
,	,	kIx,
Letiště	letiště	k1gNnSc1
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
a	a	k8xC
také	také	k9
Letiště	letiště	k1gNnSc1
Pardubice	Pardubice	k1gInPc1
se	s	k7c7
smíšeným	smíšený	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
a	a	k8xC
civilním	civilní	k2eAgInSc7d1
provozem	provoz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Přibližovací	přibližovací	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
</s>
<s>
Přibližovací	přibližovací	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
zkratkou	zkratka	k1gFnSc7
APP	APP	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
approach	approach	k1gInSc1
–	–	k?
přiblížení	přiblížení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
udržovat	udržovat	k5eAaImF
bezpečný	bezpečný	k2eAgInSc4d1
a	a	k8xC
plynulý	plynulý	k2eAgInSc4d1
provoz	provoz	k1gInSc4
v	v	k7c6
koncové	koncový	k2eAgFnSc6d1
řízené	řízený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
(	(	kIx(
<g/>
zvané	zvaný	k2eAgNnSc1d1
též	též	k9
TMA	tma	k6eAd1
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Terminal	Terminal	k1gMnSc2
Manouvering	Manouvering	k1gInSc4
Area	area	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
vzdušný	vzdušný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
v	v	k7c6
širším	široký	k2eAgNnSc6d2
okolí	okolí	k1gNnSc6
letiště	letiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Práce	práce	k1gFnSc1
přibližovací	přibližovací	k2eAgFnSc2d1
služby	služba	k1gFnSc2
řízení	řízení	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
mnohém	mnohé	k1gNnSc6
podobá	podobat	k5eAaImIp3nS
oblastní	oblastní	k2eAgFnSc3d1
službě	služba	k1gFnSc3
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
zajišťování	zajišťování	k1gNnSc4
rozstupů	rozstup	k1gInPc2
mezi	mezi	k7c7
letadly	letadlo	k1gNnPc7
během	během	k7c2
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
vysoké	vysoký	k2eAgFnSc3d1
hustotě	hustota	k1gFnSc3
provozu	provoz	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
letišť	letiště	k1gNnPc2
se	se	k3xPyFc4
ale	ale	k8xC
přibližovací	přibližovací	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
v	v	k7c6
mnohém	mnohé	k1gNnSc6
odlišuje	odlišovat	k5eAaImIp3nS
od	od	k7c2
oblastního	oblastní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
a	a	k8xC
proto	proto	k8xC
jde	jít	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
o	o	k7c4
dvě	dva	k4xCgFnPc4
různé	různý	k2eAgFnPc4d1
specializace	specializace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
přibližovací	přibližovací	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
charakterizovat	charakterizovat	k5eAaBmF
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
do	do	k7c2
TMA	tma	k6eAd1
vstupují	vstupovat	k5eAaImIp3nP
z	z	k7c2
různých	různý	k2eAgInPc2d1
směrů	směr	k1gInPc2
letadla	letadlo	k1gNnSc2
mířící	mířící	k2eAgFnSc2d1
na	na	k7c4
letiště	letiště	k1gNnSc4
a	a	k8xC
úkolem	úkol	k1gInSc7
APP	APP	kA
je	být	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
jejich	jejich	k3xOp3gNnSc4
bezpečné	bezpečný	k2eAgNnSc4d1
přiblížení	přiblížení	k1gNnSc4
k	k	k7c3
letišti	letiště	k1gNnSc3
a	a	k8xC
seřadit	seřadit	k5eAaPmF
je	on	k3xPp3gInPc4
za	za	k7c7
sebou	se	k3xPyFc7
v	v	k7c6
ose	osa	k1gFnSc6
přistávací	přistávací	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
APP	APP	kA
zajišťuje	zajišťovat	k5eAaImIp3nS
bezpečný	bezpečný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
všech	všecek	k3xTgNnPc2
letadel	letadlo	k1gNnPc2
odlétávajících	odlétávající	k2eAgNnPc2d1
z	z	k7c2
letiště	letiště	k1gNnSc2
a	a	k8xC
prolétávajících	prolétávající	k2eAgMnPc2d1
skrz	skrz	k6eAd1
TMA	tma	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
obvykle	obvykle	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
přibližovací	přibližovací	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
řízení	řízení	k1gNnSc2
přímo	přímo	k6eAd1
na	na	k7c6
dotčeném	dotčený	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
letiště	letiště	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
APP	APP	kA
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
IATCC	IATCC	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Integrované	integrovaný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
Jenči	Jenč	k1gInSc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oblastní	oblastní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
</s>
<s>
Třetím	třetí	k4xOgInSc7
základním	základní	k2eAgInSc7d1
pilířem	pilíř	k1gInSc7
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
je	být	k5eAaImIp3nS
oblastní	oblastní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
zkratkou	zkratka	k1gFnSc7
ACC	ACC	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Area	Ares	k1gMnSc2
Control	Controla	k1gFnPc2
Centre	centr	k1gInSc5
–	–	k?
oblastní	oblastní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
řízení	řízení	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblastní	oblastní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
řízení	řízení	k1gNnSc4
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
v	v	k7c6
příslušné	příslušný	k2eAgFnSc6d1
řízené	řízený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
(	(	kIx(
<g/>
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
zkratka	zkratka	k1gFnSc1
CTA	CTA	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Controlled	Controlled	k1gInSc1
Area	area	k1gFnSc1
–	–	k?
řízená	řízený	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
typicky	typicky	k6eAd1
velká	velký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
řízeného	řízený	k2eAgInSc2d1
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
zahrnující	zahrnující	k2eAgNnSc4d1
území	území	k1gNnSc4
celého	celý	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
sídlí	sídlet	k5eAaImIp3nS
oblastní	oblastní	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
APP	APP	kA
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
IATCC	IATCC	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Itegrované	Itegrovaný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
řízení	řízení	k1gNnSc2
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
Jenči	Jenč	k1gInSc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
Pozice	pozice	k1gFnPc1
pak	pak	k6eAd1
jsou	být	k5eAaImIp3nP
GND	GND	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
ground	ground	k1gMnSc1
<g/>
,	,	kIx,
zem	zem	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
DEL	DEL	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Delivery	Delivera	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
RADIO	radio	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozice	pozice	k1gFnSc1
GND	GND	kA
a	a	k8xC
DEL	DEL	kA
jsou	být	k5eAaImIp3nP
přítomni	přítomen	k2eAgMnPc1d1
pouze	pouze	k6eAd1
v	v	k7c4
určitý	určitý	k2eAgInSc4d1
čas	čas	k1gInSc4
a	a	k8xC
pouze	pouze	k6eAd1
na	na	k7c6
řízeních	řízení	k1gNnPc6
letištích	letiště	k1gNnPc6
(	(	kIx(
<g/>
tj.	tj.	kA
letiště	letiště	k1gNnSc1
s	s	k7c7
pozicí	pozice	k1gFnSc7
TWR	TWR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozice	pozice	k1gFnSc1
GND	GND	kA
zastává	zastávat	k5eAaImIp3nS
pojezdové	pojezdový	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
(	(	kIx(
<g/>
tj.	tj.	kA
pojíždění	pojíždění	k1gNnSc1
letadel	letadlo	k1gNnPc2
po	po	k7c6
pojezdových	pojezdový	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
až	až	k9
na	na	k7c4
H	H	kA
<g/>
/	/	kIx~
<g/>
P	P	kA
dráhy	dráha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozice	pozice	k1gFnSc1
DEL	DEL	kA
zajišťuje	zajišťovat	k5eAaImIp3nS
vydávání	vydávání	k1gNnSc1
letových	letový	k2eAgNnPc2d1
povolení	povolení	k1gNnPc2
(	(	kIx(
<g/>
po	po	k7c6
konzultaci	konzultace	k1gFnSc6
se	s	k7c7
stanovištěm	stanoviště	k1gNnSc7
APP	APP	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
ACC	ACC	kA
nebo	nebo	k8xC
TWR	TWR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozice	pozice	k1gFnSc1
RADIO	radio	k1gNnSc4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
informace	informace	k1gFnPc4
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
na	na	k7c6
neřízených	řízený	k2eNgNnPc6d1
letištích	letiště	k1gNnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dálková	dálkový	k2eAgFnSc1d1
letištní	letištní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
řízení	řízení	k1gNnSc2
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
dálkové	dálkový	k2eAgFnSc2d1
letištní	letištní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
řízení	řízení	k1gNnSc2
není	být	k5eNaImIp3nS
provoz	provoz	k1gInSc4
na	na	k7c6
určitém	určitý	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
řízen	řídit	k5eAaImNgInS
z	z	k7c2
řídíce	řídit	k5eAaImSgFnP
věže	věž	k1gFnPc1
daného	daný	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
(	(	kIx(
<g/>
služba	služba	k1gFnSc1
TWR	TWR	kA
popsaná	popsaný	k2eAgFnSc1d1
výše	výše	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
dálku	dálka	k1gFnSc4
z	z	k7c2
jiného	jiný	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
většího	veliký	k2eAgInSc2d2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
na	na	k7c4
takové	takový	k3xDgInPc4
úkoly	úkol	k1gInPc4
dostatečně	dostatečně	k6eAd1
personálně	personálně	k6eAd1
vybaveno	vybavit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgNnSc7
letištěm	letiště	k1gNnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
používán	používat	k5eAaImNgInS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
letiště	letiště	k1gNnSc1
Örnsköldsvik	Örnsköldsvika	k1gFnPc2
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Švédsku	Švédsko	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
na	na	k7c4
dálku	dálka	k1gFnSc4
řízeno	řízen	k2eAgNnSc1d1
z	z	k7c2
většího	veliký	k2eAgNnSc2d2
<g/>
,	,	kIx,
jižněji	jižně	k6eAd2
položeného	položený	k2eAgNnSc2d1
<g/>
,	,	kIx,
asi	asi	k9
130	#num#	k4
kilometrů	kilometr	k1gInPc2
vzdáleného	vzdálený	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
Sundsvall-Timrå	Sundsvall-Timrå	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povolení	povolení	k1gNnSc1
pro	pro	k7c4
tento	tento	k3xDgInSc4
způsob	způsob	k1gInSc4
řízení	řízení	k1gNnSc2
získalo	získat	k5eAaPmAgNnS
letiště	letiště	k1gNnSc4
Örnsköldsvik	Örnsköldsvika	k1gFnPc2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
dubna	duben	k1gInSc2
2015	#num#	k4
je	být	k5eAaImIp3nS
systém	systém	k1gInSc4
v	v	k7c6
rutinním	rutinní	k2eAgInSc6d1
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
a	a	k8xC
technologie	technologie	k1gFnSc1
vyvíjela	vyvíjet	k5eAaImAgFnS
10	#num#	k4
let	léto	k1gNnPc2
švédská	švédský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Saab	Saab	k1gInSc1
a	a	k8xC
nazvalo	nazvat	k5eAaPmAgNnS,k5eAaBmAgNnS
ho	on	k3xPp3gInSc4
Saab	Saab	k1gInSc4
Remote	Remot	k1gInSc5
Tower	Tower	k1gInSc1
System	Syst	k1gMnSc7
(	(	kIx(
<g/>
RTS	RTS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
je	být	k5eAaImIp3nS
nainstalována	nainstalovat	k5eAaPmNgFnS
na	na	k7c6
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bude	být	k5eAaImBp3nS
na	na	k7c4
dálku	dálka	k1gFnSc4
řízeno	řízen	k2eAgNnSc1d1
a	a	k8xC
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
kamer	kamera	k1gFnPc2
o	o	k7c6
vysokém	vysoký	k2eAgNnSc6d1
rozlišení	rozlišení	k1gNnSc6
<g/>
,	,	kIx,
mikrofonů	mikrofon	k1gInPc2
<g/>
,	,	kIx,
signálních	signální	k2eAgNnPc2d1
světel	světlo	k1gNnPc2
<g/>
,	,	kIx,
meteorologických	meteorologický	k2eAgInPc2d1
senzorů	senzor	k1gInPc2
a	a	k8xC
dalšího	další	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
takto	takto	k6eAd1
získané	získaný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
jsou	být	k5eAaImIp3nP
přenášeny	přenášet	k5eAaImNgFnP
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
do	do	k7c2
druhé	druhý	k4xOgFnSc2
části	část	k1gFnSc2
systému	systém	k1gInSc2
<g/>
,	,	kIx,
nazvaného	nazvaný	k2eAgInSc2d1
Remote	Remot	k1gInSc5
Tower	Tower	k1gInSc1
Center	centrum	k1gNnPc2
(	(	kIx(
<g/>
RTC	RTC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
lidé	člověk	k1gMnPc1
řídí	řídit	k5eAaImIp3nP
letový	letový	k2eAgInSc4d1
provoz	provoz	k1gInSc4
víceméně	víceméně	k9
obvyklým	obvyklý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
veškeré	veškerý	k3xTgFnPc1
informace	informace	k1gFnPc1
získávají	získávat	k5eAaImIp3nP
na	na	k7c4
dálku	dálka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
nasazení	nasazení	k1gNnSc6
systému	systém	k1gInSc2
na	na	k7c6
letišti	letiště	k1gNnSc6
Örnsköldsvik	Örnsköldsvika	k1gFnPc2
se	se	k3xPyFc4
plánuje	plánovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
využití	využití	k1gNnSc4
na	na	k7c6
dalších	další	k2eAgNnPc6d1
švédských	švédský	k2eAgNnPc6d1
letištích	letiště	k1gNnPc6
a	a	k8xC
projevily	projevit	k5eAaPmAgFnP
o	o	k7c4
něj	on	k3xPp3gInSc4
zájem	zájem	k1gInSc4
i	i	k8xC
jiné	jiný	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
leteckých	letecký	k2eAgFnPc2d1
zkratek	zkratka	k1gFnPc2
</s>
<s>
Řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
first	first	k1gInSc1
remote	remot	k1gInSc5
air	air	k?
traffic	traffic	k1gMnSc1
control	controla	k1gFnPc2
tower	tower	k1gMnSc1
to	ten	k3xDgNnSc4
open	open	k1gNnSc4
in	in	k?
Sweden	Swedna	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Atlas	Atlas	k1gMnSc1
<g/>
,	,	kIx,
2014-06-29	2014-06-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
SZANDY	SZANDY	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World-first	World-first	k1gInSc1
remote	remot	k1gInSc5
air	air	k?
traffic	traffice	k1gFnPc2
control	controla	k1gFnPc2
system	syst	k1gInSc7
lands	lands	k6eAd1
in	in	k?
Sweden	Swedna	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Atlas	Atlas	k1gMnSc1
<g/>
,	,	kIx,
2015-04-29	2015-04-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Řízení	řízení	k1gNnSc1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
1171	#num#	k4
</s>
