<s>
Joanne	Joannout	k5eAaImIp3nS	Joannout
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
OBE	Ob	k1gInSc5	Ob
FRSL	FRSL	kA	FRSL
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
Joanne	Joann	k1gInSc5	Joann
Rowling	Rowling	k1gInSc4	Rowling
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1965	[number]	k4	1965
v	v	k7c6	v
Yate	Yate	k1gFnSc6	Yate
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
píšící	píšící	k2eAgFnSc1d1	píšící
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
(	(	kIx(	(
<g/>
J.	J.	kA	J.
K.	K.	kA	K.
Rowling	Rowling	k1gInSc1	Rowling
<g/>
)	)	kIx)	)
či	či	k8xC	či
pseudonymem	pseudonym	k1gInSc7	pseudonym
Robert	Roberta	k1gFnPc2	Roberta
Galbraith	Galbraitha	k1gFnPc2	Galbraitha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
sedmidílné	sedmidílný	k2eAgFnSc3d1	sedmidílná
řadě	řada	k1gFnSc3	řada
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
čarodějnickém	čarodějnický	k2eAgMnSc6d1	čarodějnický
učni	učeň	k1gMnSc6	učeň
Harry	Harra	k1gFnSc2	Harra
Potterovi	Potter	k1gMnSc3	Potter
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
úspěch	úspěch	k1gInSc4	úspěch
včetně	včetně	k7c2	včetně
řady	řada	k1gFnSc2	řada
ocenění	ocenění	k1gNnSc2	ocenění
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
