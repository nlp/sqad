<s>
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Spencer	Spencer	k1gMnSc1	Spencer
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1889	[number]	k4	1889
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1977	[number]	k4	1977
Corsier-sur-Vevey	Corsierur-Vevea	k1gFnSc2	Corsier-sur-Vevea
<g/>
,	,	kIx,	,
kanton	kanton	k1gInSc4	kanton
Vaud	Vauda	k1gFnPc2	Vauda
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
světovým	světový	k2eAgMnPc3d1	světový
filmovým	filmový	k2eAgMnPc3d1	filmový
tvůrcům	tvůrce	k1gMnPc3	tvůrce
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
filmy	film	k1gInPc4	film
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
od	od	k7c2	od
námětů	námět	k1gInPc2	námět
přes	přes	k7c4	přes
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
až	až	k9	až
po	po	k7c4	po
účinkování	účinkování	k1gNnSc4	účinkování
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Chaplinovi	Chaplinův	k2eAgMnPc1d1	Chaplinův
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
živili	živit	k5eAaImAgMnP	živit
jako	jako	k9	jako
pouliční	pouliční	k2eAgMnPc1d1	pouliční
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
-	-	kIx~	-
babička	babička	k1gFnSc1	babička
rozená	rozený	k2eAgFnSc1d1	rozená
Smith	Smith	k1gInSc1	Smith
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
poloviční	poloviční	k2eAgFnSc1d1	poloviční
cikánka	cikánka	k1gFnSc1	cikánka
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
nebo	nebo	k8xC	nebo
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaPmF	nalézt
doklad	doklad	k1gInSc4	doklad
o	o	k7c4	o
Chaplinově	Chaplinově	k1gFnSc4	Chaplinově
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
původ	původ	k1gInSc4	původ
je	být	k5eAaImIp3nS	být
obestřen	obestřít	k5eAaPmNgMnS	obestřít
rouškou	rouška	k1gFnSc7	rouška
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
herectví	herectví	k1gNnSc3	herectví
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
už	už	k6eAd1	už
jako	jako	k9	jako
osmiletý	osmiletý	k2eAgMnSc1d1	osmiletý
<g/>
,	,	kIx,	,
když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
divadelní	divadelní	k2eAgMnSc1d1	divadelní
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
na	na	k7c6	na
scénách	scéna	k1gFnPc6	scéna
londýnských	londýnský	k2eAgInPc2d1	londýnský
kabaretů	kabaret	k1gInPc2	kabaret
a	a	k8xC	a
music-hallů	musicall	k1gInPc2	music-hall
jako	jako	k8xS	jako
burleskní	burleskní	k2eAgMnSc1d1	burleskní
komik	komik	k1gMnSc1	komik
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
pantomimickým	pantomimický	k2eAgInSc7d1	pantomimický
talentem	talent	k1gInSc7	talent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Caseyho	Casey	k1gMnSc2	Casey
cirkuse	cirkus	k1gInSc6	cirkus
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
zájezdovém	zájezdový	k2eAgNnSc6d1	zájezdové
divadle	divadlo	k1gNnSc6	divadlo
divadelního	divadelní	k2eAgMnSc2d1	divadelní
podnikatele	podnikatel	k1gMnSc2	podnikatel
Freda	Fred	k1gMnSc2	Fred
Karna	Karn	k1gMnSc2	Karn
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
úspěch	úspěch	k1gInSc1	úspěch
slavil	slavit	k5eAaImAgInS	slavit
s	s	k7c7	s
představením	představení	k1gNnSc7	představení
Noc	noc	k1gFnSc4	noc
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
music-hallu	musicallo	k1gNnSc6	music-hallo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
filmovým	filmový	k2eAgMnSc7d1	filmový
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
režisérem	režisér	k1gMnSc7	režisér
Mackem	Macek	k1gMnSc7	Macek
Sennettem	Sennett	k1gMnSc7	Sennett
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
společně	společně	k6eAd1	společně
podepsal	podepsat	k5eAaPmAgMnS	podepsat
produkční	produkční	k2eAgFnSc4d1	produkční
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
filmovou	filmový	k2eAgFnSc7d1	filmová
společností	společnost	k1gFnSc7	společnost
Keystone	Keyston	k1gInSc5	Keyston
<g/>
,	,	kIx,	,
specializující	specializující	k2eAgFnSc7d1	specializující
se	se	k3xPyFc4	se
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
grotesek	groteska	k1gFnPc2	groteska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
filmových	filmový	k2eAgFnPc2d1	filmová
postav	postava	k1gFnPc2	postava
-	-	kIx~	-
komickou	komický	k2eAgFnSc4d1	komická
postavu	postava	k1gFnSc4	postava
tuláka	tulák	k1gMnSc2	tulák
Charlieho	Charlie	k1gMnSc2	Charlie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
celosvětově	celosvětově	k6eAd1	celosvětově
slavnou	slavný	k2eAgFnSc4d1	slavná
<g/>
.	.	kIx.	.
</s>
<s>
Chaplin	Chaplin	k1gInSc1	Chaplin
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
do	do	k7c2	do
kinematografie	kinematografie	k1gFnSc2	kinematografie
vnesl	vnést	k5eAaPmAgMnS	vnést
opravdové	opravdový	k2eAgFnPc4d1	opravdová
lidské	lidský	k2eAgFnPc4d1	lidská
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
formální	formální	k2eAgFnSc6d1	formální
stránce	stránka	k1gFnSc6	stránka
diváky	divák	k1gMnPc4	divák
příliš	příliš	k6eAd1	příliš
neoslňoval	oslňovat	k5eNaImAgMnS	oslňovat
filmovou	filmový	k2eAgFnSc7d1	filmová
montáží	montáž	k1gFnSc7	montáž
(	(	kIx(	(
<g/>
scény	scéna	k1gFnSc2	scéna
natáčel	natáčet	k5eAaImAgMnS	natáčet
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
polocelkových	polocelkův	k2eAgInPc6d1	polocelkův
či	či	k8xC	či
celkových	celkový	k2eAgInPc6d1	celkový
záběrech	záběr	k1gInPc6	záběr
bez	bez	k1gInSc4	bez
dynamických	dynamický	k2eAgInPc2d1	dynamický
střihů	střih	k1gInPc2	střih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
názorově	názorově	k6eAd1	názorově
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
filmovým	filmový	k2eAgInSc7d1	filmový
Hollywoodem	Hollywood	k1gInSc7	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
mu	on	k3xPp3gNnSc3	on
po	po	k7c6	po
pobytu	pobyt	k1gInSc6	pobyt
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
"	"	kIx"	"
<g/>
neamerickou	americký	k2eNgFnSc4d1	neamerická
činnost	činnost	k1gFnSc4	činnost
<g/>
"	"	kIx"	"
nebyl	být	k5eNaImAgInS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
návrat	návrat	k1gInSc1	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
obětí	oběť	k1gFnSc7	oběť
mccarthismu	mccarthismus	k1gInSc2	mccarthismus
<g/>
.	.	kIx.	.
</s>
<s>
Usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
státu	stát	k1gInSc2	stát
amerických	americký	k2eAgMnPc2d1	americký
mu	on	k3xPp3gNnSc3	on
povolili	povolit	k5eAaPmAgMnP	povolit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
udělen	udělen	k2eAgMnSc1d1	udělen
čestný	čestný	k2eAgMnSc1d1	čestný
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
poslední	poslední	k2eAgFnSc7d1	poslední
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Oona	Oona	k1gFnSc1	Oona
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neillová	Neillová	k1gFnSc1	Neillová
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
dramatika	dramatik	k1gMnSc2	dramatik
Eugena	Eugen	k1gMnSc2	Eugen
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neilla	Neill	k1gMnSc2	Neill
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
celkem	celkem	k6eAd1	celkem
osm	osm	k4xCc4	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
Geraldine	Geraldin	k1gInSc5	Geraldin
Chaplinová	Chaplinový	k2eAgNnPc1d1	Chaplinový
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tanečnicí	tanečnice	k1gFnSc7	tanečnice
a	a	k8xC	a
herečkou	herečka	k1gFnSc7	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Chaplin	Chaplin	k1gInSc1	Chaplin
uměl	umět	k5eAaImAgInS	umět
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
plátně	plátno	k1gNnSc6	plátno
a	a	k8xC	a
hudebně	hudebně	k6eAd1	hudebně
<g/>
.	.	kIx.	.
</s>
<s>
Dokladem	doklad	k1gInSc7	doklad
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vlastní	vlastní	k2eAgFnSc1d1	vlastní
biografie	biografie	k1gFnSc1	biografie
s	s	k7c7	s
názvem	název	k1gInSc7	název
Můj	můj	k1gMnSc1	můj
životopis	životopis	k1gInSc1	životopis
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vyšel	vyjít	k5eAaPmAgInS	vyjít
poprvé	poprvé	k6eAd1	poprvé
již	již	k6eAd1	již
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Františka	František	k1gMnSc2	František
Vrby	Vrba	k1gMnSc2	Vrba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
skrupulí	skrupule	k1gFnPc2	skrupule
ohlíží	ohlížet	k5eAaImIp3nP	ohlížet
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
životem	život	k1gInSc7	život
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
události	událost	k1gFnPc4	událost
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
vnímal	vnímat	k5eAaImAgInS	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
v	v	k7c6	v
dobových	dobový	k2eAgFnPc6d1	dobová
souvislostech	souvislost	k1gFnPc6	souvislost
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jej	on	k3xPp3gNnSc4	on
provázely	provázet	k5eAaImAgFnP	provázet
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
knihy	kniha	k1gFnSc2	kniha
věnuje	věnovat	k5eAaPmIp3nS	věnovat
svému	svůj	k3xOyFgNnSc3	svůj
chudému	chudý	k2eAgNnSc3d1	chudé
dětství	dětství	k1gNnSc3	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
také	také	k9	také
objasnit	objasnit	k5eAaPmF	objasnit
některé	některý	k3yIgFnPc4	některý
kauzy	kauza	k1gFnPc4	kauza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
nechaly	nechat	k5eAaPmAgFnP	nechat
poklesnout	poklesnout	k5eAaPmF	poklesnout
jeho	jeho	k3xOp3gFnSc4	jeho
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc4d1	krásný
příběh	příběh	k1gInSc4	příběh
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nebál	bát	k5eNaImAgMnS	bát
stát	stát	k5eAaPmF	stát
si	se	k3xPyFc3	se
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgMnSc1d1	vědom
svých	svůj	k3xOyFgFnPc2	svůj
předností	přednost	k1gFnPc2	přednost
i	i	k8xC	i
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
výher	výhra	k1gFnPc2	výhra
i	i	k8xC	i
životních	životní	k2eAgNnPc2d1	životní
zklamání	zklamání	k1gNnPc2	zklamání
a	a	k8xC	a
dokládá	dokládat	k5eAaImIp3nS	dokládat
Chaplinovu	Chaplinův	k2eAgFnSc4d1	Chaplinova
schopnost	schopnost	k1gFnSc4	schopnost
kritického	kritický	k2eAgNnSc2d1	kritické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
biografie	biografie	k1gFnSc1	biografie
propletená	propletený	k2eAgFnSc1d1	propletená
mnoha	mnoho	k4c2	mnoho
zajímavými	zajímavý	k2eAgFnPc7d1	zajímavá
myšlenkami	myšlenka	k1gFnPc7	myšlenka
o	o	k7c6	o
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
mezilidských	mezilidský	k2eAgInPc6d1	mezilidský
vztazích	vztah	k1gInPc6	vztah
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
životě	život	k1gInSc6	život
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Filmografie	filmografie	k1gFnSc2	filmografie
Charlieho	Charlie	k1gMnSc2	Charlie
Chaplina	Chaplin	k2eAgMnSc2d1	Chaplin
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
filmovou	filmový	k2eAgFnSc4d1	filmová
kariéru	kariéra	k1gFnSc4	kariéra
natočil	natočit	k5eAaBmAgMnS	natočit
kolem	kolem	k7c2	kolem
devadesáti	devadesát	k4xCc2	devadesát
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
většině	většina	k1gFnSc3	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
napsal	napsat	k5eAaPmAgInS	napsat
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
je	být	k5eAaImIp3nS	být
režíroval	režírovat	k5eAaImAgMnS	režírovat
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
i	i	k9	i
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
skládal	skládat	k5eAaImAgMnS	skládat
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
novým	nový	k2eAgInPc3d1	nový
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
slavným	slavný	k2eAgInPc3d1	slavný
němým	němý	k2eAgInPc3d1	němý
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
i	i	k8xC	i
vlastní	vlastní	k2eAgInSc1d1	vlastní
hudební	hudební	k2eAgInSc1d1	hudební
doprovod	doprovod	k1gInSc1	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
plátně	plátno	k1gNnSc6	plátno
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
debut	debut	k1gInSc4	debut
odbyl	odbýt	k5eAaPmAgMnS	odbýt
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Chaplin	Chaplina	k1gFnPc2	Chaplina
si	se	k3xPyFc3	se
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
(	(	kIx(	(
<g/>
Making	Making	k1gInSc1	Making
a	a	k8xC	a
Living	Living	k1gInSc1	Living
<g/>
;	;	kIx,	;
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
filmové	filmový	k2eAgFnSc2d1	filmová
společnosti	společnost	k1gFnSc2	společnost
Keystone	Keyston	k1gInSc5	Keyston
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
podepsal	podepsat	k5eAaPmAgInS	podepsat
jednoletý	jednoletý	k2eAgInSc1d1	jednoletý
kontrakt	kontrakt	k1gInSc1	kontrakt
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tohoto	tento	k3xDgInSc2	tento
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
natočil	natočit	k5eAaBmAgMnS	natočit
celkem	celkem	k6eAd1	celkem
35	[number]	k4	35
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
dlouhometrážní	dlouhometrážní	k2eAgMnSc1d1	dlouhometrážní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
podepsal	podepsat	k5eAaPmAgInS	podepsat
nový	nový	k2eAgInSc1d1	nový
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
majiteli	majitel	k1gMnPc7	majitel
společnosti	společnost	k1gFnSc2	společnost
Essaney	Essaney	k1gInPc4	Essaney
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
natočil	natočit	k5eAaBmAgInS	natočit
15	[number]	k4	15
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
společnost	společnost	k1gFnSc1	společnost
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
podepsal	podepsat	k5eAaPmAgInS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
12	[number]	k4	12
dvoudílných	dvoudílný	k2eAgFnPc2d1	dvoudílná
komedií	komedie	k1gFnPc2	komedie
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Mutual	Mutual	k1gInSc4	Mutual
Film	film	k1gInSc1	film
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
12	[number]	k4	12
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
krátkometrážní	krátkometrážní	k2eAgFnSc2d1	krátkometrážní
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
k	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
snímky	snímka	k1gFnPc4	snímka
Chaplin	Chaplin	k2eAgInSc1d1	Chaplin
hasičem	hasič	k1gMnSc7	hasič
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Fireman	Fireman	k1gMnSc1	Fireman
<g/>
;	;	kIx,	;
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chaplin	Chaplin	k2eAgInSc1d1	Chaplin
vystěhovalcem	vystěhovalec	k1gMnSc7	vystěhovalec
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Immigrant	Immigrant	k1gMnSc1	Immigrant
<g/>
;	;	kIx,	;
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
odhadcem	odhadce	k1gMnSc7	odhadce
v	v	k7c6	v
zastavárně	zastavárna	k1gFnSc6	zastavárna
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Pawnshop	Pawnshop	k1gInSc1	Pawnshop
<g/>
;	;	kIx,	;
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
snímek	snímek	k1gInSc1	snímek
Chaplin	Chaplin	k2eAgInSc1d1	Chaplin
strážcem	strážce	k1gMnSc7	strážce
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
(	(	kIx(	(
<g/>
Easy	Easy	k1gInPc4	Easy
Street	Street	k1gInSc1	Street
<g/>
;	;	kIx,	;
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
společně	společně	k6eAd1	společně
s	s	k7c7	s
Mary	Mary	k1gFnSc7	Mary
Pickfordovou	Pickfordová	k1gFnSc7	Pickfordová
<g/>
,	,	kIx,	,
Douglasem	Douglas	k1gMnSc7	Douglas
Fairbanksem	Fairbanks	k1gMnSc7	Fairbanks
a	a	k8xC	a
Davidem	David	k1gMnSc7	David
Wark	Warka	k1gFnPc2	Warka
Griffithem	Griffith	k1gInSc7	Griffith
založil	založit	k5eAaPmAgMnS	založit
filmovou	filmový	k2eAgFnSc4d1	filmová
společnost	společnost	k1gFnSc4	společnost
United	United	k1gMnSc1	United
Artists	Artistsa	k1gFnPc2	Artistsa
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
až	až	k9	až
1952	[number]	k4	1952
výhradně	výhradně	k6eAd1	výhradně
natočil	natočit	k5eAaBmAgMnS	natočit
své	svůj	k3xOyFgInPc4	svůj
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
i	i	k8xC	i
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
opojení	opojení	k1gNnSc1	opojení
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Gold	Gold	k1gMnSc1	Gold
Rush	Rush	k1gMnSc1	Rush
<g/>
;	;	kIx,	;
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Světla	světlo	k1gNnSc2	světlo
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
(	(	kIx(	(
<g/>
City	city	k1gNnSc1	city
Lights	Lightsa	k1gFnPc2	Lightsa
<g/>
;	;	kIx,	;
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgFnSc4d1	moderní
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
Modern	Modern	k1gMnSc1	Modern
Times	Times	k1gMnSc1	Times
<g/>
;	;	kIx,	;
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
a	a	k8xC	a
Diktátor	diktátor	k1gMnSc1	diktátor
(	(	kIx(	(
<g/>
The	The	k1gFnSc6	The
Great	Great	k2eAgMnSc1d1	Great
Dictator	Dictator	k1gMnSc1	Dictator
<g/>
;	;	kIx,	;
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
plátně	plátno	k1gNnSc6	plátno
mihl	mihnout	k5eAaPmAgMnS	mihnout
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
roli	role	k1gFnSc6	role
lodního	lodní	k2eAgMnSc2d1	lodní
stevarda	stevard	k1gMnSc2	stevard
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Hongkongu	Hongkong	k1gInSc2	Hongkong
(	(	kIx(	(
<g/>
A	a	k9	a
Countess	Countess	k1gInSc1	Countess
from	from	k6eAd1	from
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
napsal	napsat	k5eAaPmAgMnS	napsat
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
hudební	hudební	k2eAgInSc4d1	hudební
doprovod	doprovod	k1gInSc4	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
pražského	pražský	k2eAgNnSc2d1	Pražské
divadla	divadlo	k1gNnSc2	divadlo
Varieté	varieté	k1gNnSc2	varieté
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
Karnova	Karnův	k2eAgFnSc1d1	Karnova
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
účinkujícími	účinkující	k1gMnPc7	účinkující
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
mladý	mladý	k2eAgInSc1d1	mladý
Chaplin	Chaplin	k1gInSc1	Chaplin
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgMnSc1d1	další
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jistý	jistý	k2eAgMnSc1d1	jistý
Stanley	Stanley	k1gInPc7	Stanley
Artur	Artur	k1gMnSc1	Artur
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
později	pozdě	k6eAd2	pozdě
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Stan	stan	k1gInSc1	stan
Laurel	Laurel	k1gInSc1	Laurel
<g/>
.	.	kIx.	.
</s>
<s>
Chaplinovo	Chaplinův	k2eAgNnSc1d1	Chaplinovo
účinkování	účinkování	k1gNnSc1	účinkování
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
vnučka	vnučka	k1gFnSc1	vnučka
Kiera	Kiera	k1gFnSc1	Kiera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
také	také	k9	také
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
dědeček	dědeček	k1gMnSc1	dědeček
měl	mít	k5eAaImAgMnS	mít
Československo	Československo	k1gNnSc4	Československo
rád	rád	k2eAgMnSc1d1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
brněnského	brněnský	k2eAgNnSc2d1	brněnské
avantgardního	avantgardní	k2eAgNnSc2d1	avantgardní
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
sdružení	sdružení	k1gNnSc2	sdružení
Devětsil	Devětsil	k1gInSc4	Devětsil
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
Chaplinovi	Chaplinův	k2eAgMnPc1d1	Chaplinův
(	(	kIx(	(
<g/>
osloveni	osloven	k2eAgMnPc1d1	osloven
byli	být	k5eAaImAgMnP	být
také	také	k9	také
Douglas	Douglas	k1gMnSc1	Douglas
Fairbanks	Fairbanks	k1gInSc1	Fairbanks
a	a	k8xC	a
Harold	Harold	k1gMnSc1	Harold
Lloyd	Lloyd	k1gMnSc1	Lloyd
<g/>
)	)	kIx)	)
čestné	čestný	k2eAgNnSc1d1	čestné
členství	členství	k1gNnSc1	členství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
ze	z	k7c2	z
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1925	[number]	k4	1925
Chaplin	Chaplin	k1gInSc1	Chaplin
přijal	přijmout	k5eAaPmAgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Chaplinovi	Chaplin	k1gMnSc6	Chaplin
psali	psát	k5eAaImAgMnP	psát
i	i	k9	i
významní	významný	k2eAgMnPc1d1	významný
čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
oslavný	oslavný	k2eAgInSc1d1	oslavný
(	(	kIx(	(
<g/>
obdivný	obdivný	k2eAgInSc1d1	obdivný
<g/>
)	)	kIx)	)
fejeton	fejeton	k1gInSc1	fejeton
s	s	k7c7	s
názvem	název	k1gInSc7	název
Chaplin	Chaplina	k1gFnPc2	Chaplina
čili	čili	k8xC	čili
O	o	k7c6	o
realismu	realismus	k1gInSc6	realismus
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
filmové	filmový	k2eAgNnSc1d1	filmové
libreto	libreto	k1gNnSc1	libreto
Charlie	Charlie	k1gMnSc1	Charlie
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
<g/>
:	:	kIx,	:
improvizovaná	improvizovaný	k2eAgFnSc1d1	improvizovaná
chapliniáda	chapliniáda	k1gFnSc1	chapliniáda
o	o	k7c6	o
2	[number]	k4	2
epochách	epocha	k1gFnPc6	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Chaplinově	Chaplinově	k1gFnSc6	Chaplinově
osobní	osobní	k2eAgFnSc2d1	osobní
blízkosti	blízkost	k1gFnSc2	blízkost
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Rudolf	Rudolf	k1gMnSc1	Rudolf
Myzet	Myzet	k1gMnSc1	Myzet
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
filmem	film	k1gInSc7	film
Cirkus	cirkus	k1gInSc1	cirkus
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Circus	Circus	k1gMnSc1	Circus
<g/>
;	;	kIx,	;
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
rolích	role	k1gFnPc6	role
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
jeho	jeho	k3xOp3gInPc6	jeho
následujících	následující	k2eAgInPc6d1	následující
filmech	film	k1gInPc6	film
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
věnoval	věnovat	k5eAaImAgMnS	věnovat
Chaplin	Chaplin	k1gInSc4	Chaplin
Československému	československý	k2eAgNnSc3d1	Československé
kinematografickému	kinematografický	k2eAgNnSc3d1	kinematografické
muzeu	muzeum	k1gNnSc3	muzeum
při	při	k7c6	při
Technickém	technický	k2eAgNnSc6d1	technické
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
několik	několik	k4yIc4	několik
rekvizit	rekvizita	k1gFnPc2	rekvizita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spoluvytvářely	spoluvytvářet	k5eAaImAgFnP	spoluvytvářet
vnější	vnější	k2eAgFnSc4d1	vnější
podobu	podoba	k1gFnSc4	podoba
jeho	on	k3xPp3gMnSc2	on
tuláka	tulák	k1gMnSc2	tulák
-	-	kIx~	-
buřinku	buřinka	k1gFnSc4	buřinka
<g/>
,	,	kIx,	,
boty	bota	k1gFnPc4	bota
a	a	k8xC	a
bambusovou	bambusový	k2eAgFnSc4d1	bambusová
hůlku	hůlka	k1gFnSc4	hůlka
<g/>
.	.	kIx.	.
</s>
<s>
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k1gInSc4	Chaplin
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
pacifista	pacifista	k1gMnSc1	pacifista
a	a	k8xC	a
silný	silný	k2eAgMnSc1d1	silný
odpůrce	odpůrce	k1gMnSc1	odpůrce
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
prý	prý	k9	prý
jeho	jeho	k3xOp3gInSc4	jeho
film	film	k1gInSc4	film
Diktátor	diktátor	k1gMnSc1	diktátor
rozzuřil	rozzuřit	k5eAaPmAgMnS	rozzuřit
k	k	k7c3	k
nepříčetnosti	nepříčetnost	k1gFnSc3	nepříčetnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Charlie	Charlie	k1gMnSc1	Charlie
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
životních	životní	k2eAgInPc2d1	životní
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
angažoval	angažovat	k5eAaBmAgMnS	angažovat
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
na	na	k7c4	na
znovuvybudování	znovuvybudování	k1gNnSc4	znovuvybudování
Lidic	Lidice	k1gInPc2	Lidice
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
tajně	tajně	k6eAd1	tajně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
soutěže	soutěž	k1gFnPc4	soutěž
napodobitelů	napodobitel	k1gMnPc2	napodobitel
Charlieho	Charlie	k1gMnSc4	Charlie
Chaplina	Chaplin	k2eAgFnSc1d1	Chaplina
<g/>
.	.	kIx.	.
</s>
<s>
Skončil	skončit	k5eAaPmAgMnS	skončit
až	až	k9	až
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
