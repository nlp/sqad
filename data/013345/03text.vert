<p>
<s>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
a	a	k8xC	a
fotografiemi	fotografia	k1gFnPc7	fotografia
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
růstu	růst	k1gInSc6	růst
štěňátka	štěňátko	k1gNnSc2	štěňátko
–	–	k?	–
foxteriéra	foxteriér	k1gMnSc4	foxteriér
Dášeňky	Dášeňka	k1gFnSc2	Dášeňka
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
psí	psí	k2eAgFnPc4d1	psí
pohádky	pohádka	k1gFnPc4	pohádka
a	a	k8xC	a
bajky	bajka	k1gFnPc4	bajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
knížka	knížka	k1gFnSc1	knížka
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
malé	malý	k2eAgMnPc4d1	malý
čtenáře	čtenář	k1gMnPc4	čtenář
o	o	k7c6	o
malém	malý	k2eAgNnSc6d1	malé
nezbedném	nezbedný	k2eAgNnSc6d1	nezbedné
štěněti	štěně	k1gNnSc6	štěně
<g/>
.	.	kIx.	.
</s>
<s>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
je	být	k5eAaImIp3nS	být
drsnosrstý	drsnosrstý	k2eAgMnSc1d1	drsnosrstý
foxteriér	foxteriér	k1gMnSc1	foxteriér
a	a	k8xC	a
"	"	kIx"	"
<g/>
trápí	trápit	k5eAaImIp3nS	trápit
<g/>
"	"	kIx"	"
nejen	nejen	k6eAd1	nejen
svou	svůj	k3xOyFgFnSc4	svůj
maminku	maminka	k1gFnSc4	maminka
Iris	iris	k1gFnSc4	iris
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k8xC	i
svého	svůj	k3xOyFgMnSc2	svůj
páníčka	páníček	k1gMnSc2	páníček
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
je	být	k5eAaImIp3nS	být
i	i	k9	i
autorem	autor	k1gMnSc7	autor
obrázků	obrázek	k1gInPc2	obrázek
a	a	k8xC	a
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
dozvíme	dozvědět	k5eAaPmIp1nP	dozvědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc4	jaký
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gInPc1	její
první	první	k4xOgInPc1	první
krůčky	krůček	k1gInPc1	krůček
a	a	k8xC	a
jak	jak	k8xC	jak
objevuje	objevovat	k5eAaImIp3nS	objevovat
svět	svět	k1gInSc1	svět
-	-	kIx~	-
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
zoubky	zoubek	k1gInPc7	zoubek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
kapitolách	kapitola	k1gFnPc6	kapitola
si	se	k3xPyFc3	se
potom	potom	k6eAd1	potom
přečteme	přečíst	k5eAaPmIp1nP	přečíst
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
vyfotit	vyfotit	k5eAaPmF	vyfotit
malou	malý	k2eAgFnSc4d1	malá
psí	psí	k2eAgFnSc4d1	psí
slečnu	slečna	k1gFnSc4	slečna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neposedí	posedit	k5eNaPmIp3nS	posedit
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
však	však	k9	však
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
a	a	k8xC	a
i	i	k8xC	i
čtenáři	čtenář	k1gMnSc3	čtenář
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
několik	několik	k4yIc4	několik
pohádek	pohádka	k1gFnPc2	pohádka
pro	pro	k7c4	pro
malé	malý	k2eAgMnPc4d1	malý
pejsky	pejsek	k1gMnPc4	pejsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
knížka	knížka	k1gFnSc1	knížka
byla	být	k5eAaImAgFnS	být
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
jako	jako	k8xS	jako
mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc4	příběh
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
například	například	k6eAd1	například
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Sommer	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šrámek	Šrámek	k1gMnSc1	Šrámek
nebo	nebo	k8xC	nebo
Petr	Petr	k1gMnSc1	Petr
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
"	"	kIx"	"
<g/>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
<g/>
"	"	kIx"	"
neunikla	uniknout	k5eNaPmAgFnS	uniknout
pozornosti	pozornost	k1gFnSc2	pozornost
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
Břetislava	Břetislav	k1gMnSc2	Břetislav
Pojara	Pojar	k1gMnSc2	Pojar
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skutečný	skutečný	k2eAgInSc4d1	skutečný
život	život	k1gInSc4	život
malého	malý	k2eAgNnSc2d1	malé
štěněte	štěně	k1gNnSc2	štěně
je	být	k5eAaImIp3nS	být
protkán	protkat	k5eAaPmNgInS	protkat
psími	psí	k2eAgFnPc7d1	psí
pohádkami	pohádka	k1gFnPc7	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
psí	psí	k2eAgFnSc2d1	psí
slečny	slečna	k1gFnSc2	slečna
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
osmiminutových	osmiminutový	k2eAgInPc2d1	osmiminutový
dílů	díl	k1gInPc2	díl
(	(	kIx(	(
<g/>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
uviděla	uvidět	k5eAaPmAgFnS	uvidět
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
Co	co	k3yRnSc1	co
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
Mnoho	mnoho	k6eAd1	mnoho
vody	voda	k1gFnSc2	voda
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
sportovala	sportovat	k5eAaImAgFnS	sportovat
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
<g/>
)	)	kIx)	)
a	a	k8xC	a
role	role	k1gFnSc1	role
vypravěče	vypravěč	k1gMnSc2	vypravěč
byla	být	k5eAaImAgFnS	být
svěřena	svěřit	k5eAaPmNgFnS	svěřit
Martinu	Martin	k2eAgMnSc3d1	Martin
Růžkovi	Růžek	k1gMnSc3	Růžek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
<g/>
.	.	kIx.	.
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
<p>
<s>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
díla	dílo	k1gNnSc2	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
<p>
<s>
Česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
</s>
</p>
