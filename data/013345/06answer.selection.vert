<s>
Dášeňka	Dášeňka	k1gFnSc1	Dášeňka
čili	čili	k8xC	čili
život	život	k1gInSc1	život
štěněte	štěně	k1gNnSc2	štěně
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
a	a	k8xC	a
fotografiemi	fotografia	k1gFnPc7	fotografia
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
