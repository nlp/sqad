<s>
Již	již	k9
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
deseti	deset	k4xCc6
letech	let	k1gInPc6
našel	najít	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
byla	být	k5eAaImAgFnS
Velká	velký	k2eAgFnSc1d1
Fermatova	Fermatův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
,	,	kIx,
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnSc1
formulace	formulace	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
on	on	k3xPp3gInSc1
ji	on	k3xPp3gFnSc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
deseti	deset	k4xCc6
letech	léto	k1gNnPc6
byl	být	k5eAaImAgMnS
schopen	schopen	k2eAgMnSc1d1
pochopit	pochopit	k5eAaPmF
<g/>
.	.	kIx.
</s>