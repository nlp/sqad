<s>
Andrew	Andrew	k?
Wiles	Wiles	k1gInSc1
</s>
<s>
Andrew	Andrew	k?
Wiles	Wiles	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1953	#num#	k4
(	(	kIx(
<g/>
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Cambridge	Cambridge	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Merton	Merton	k1gInSc1
College	Colleg	k1gInSc2
(	(	kIx(
<g/>
do	do	k7c2
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
Clare	Clar	k1gInSc5
CollegeUniverzita	CollegeUniverzita	k1gFnSc1
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
Pracoviště	pracoviště	k1gNnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
Princetonská	Princetonský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Obor	obor	k1gInSc1
</s>
<s>
teorie	teorie	k1gFnSc1
čísel	číslo	k1gNnPc2
Ocenění	ocenění	k1gNnPc2
</s>
<s>
Whiteheadova	Whiteheadův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
člen	člen	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
Rolf	Rolf	k1gMnSc1
Schock	Schock	k1gMnSc1
Prize	Prize	k1gFnSc2
in	in	k?
Mathematics	Mathematics	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
Prix	Prix	k1gInSc1
Fermat	fermata	k1gFnPc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
Královská	královský	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Rodiče	rodič	k1gMnPc1
</s>
<s>
Maurice	Maurika	k1gFnSc3
Wiles	Wiles	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sir	sir	k1gMnSc1
Andrew	Andrew	k1gMnSc1
John	John	k1gMnSc1
Wiles	Wiles	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1953	#num#	k4
Cambridge	Cambridge	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
držitel	držitel	k1gMnSc1
mnoha	mnoho	k4c2
vědeckých	vědecký	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
a	a	k8xC
člen	člen	k1gMnSc1
několika	několik	k4yIc2
vědeckých	vědecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslavil	proslavit	k5eAaPmAgMnS
se	se	k3xPyFc4
zejména	zejména	k9
svým	svůj	k3xOyFgInSc7
důkazem	důkaz	k1gInSc7
Velké	velký	k2eAgFnSc2d1
Fermatovy	Fermatův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
získaným	získaný	k2eAgInSc7d1
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
vědecká	vědecký	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
Již	již	k9
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
deseti	deset	k4xCc6
letech	let	k1gInPc6
našel	najít	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
byla	být	k5eAaImAgFnS
Velká	velký	k2eAgFnSc1d1
Fermatova	Fermatův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
,	,	kIx,
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnSc1
formulace	formulace	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
on	on	k3xPp3gInSc1
ji	on	k3xPp3gFnSc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
deseti	deset	k4xCc6
letech	léto	k1gNnPc6
byl	být	k5eAaImAgMnS
schopen	schopen	k2eAgMnSc1d1
pochopit	pochopit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
chvíle	chvíle	k1gFnSc2
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
neopustí	opustit	k5eNaPmIp3nS
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
jej	on	k3xPp3gNnSc4
sám	sám	k3xTgMnSc1
nevyřeší	vyřešit	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
získal	získat	k5eAaPmAgMnS
základní	základní	k2eAgNnSc4d1
a	a	k8xC
středoškolské	středoškolský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Oxfordské	oxfordský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
titul	titul	k1gInSc4
bakaláře	bakalář	k1gMnSc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Následně	následně	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
na	na	k7c6
Princetonské	Princetonský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
působil	působit	k5eAaImAgMnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
ve	v	k7c6
stopách	stopa	k1gFnPc6
svého	své	k1gNnSc2
otce	otec	k1gMnSc2
na	na	k7c6
Oxfordu	Oxford	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
důkazu	důkaz	k1gInSc2
Velké	velký	k2eAgFnSc2d1
Fermatovy	Fermatův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
vytvořeného	vytvořený	k2eAgNnSc2d1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
Richarda	Richard	k1gMnSc2
Taylora	Taylor	k1gMnSc2
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgInS
další	další	k2eAgInPc4d1
důležité	důležitý	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
teorie	teorie	k1gFnSc2
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
profesorem	profesor	k1gMnSc7
a	a	k8xC
vedoucím	vedoucí	k1gMnSc7
katedry	katedra	k1gFnSc2
matematiky	matematika	k1gFnSc2
na	na	k7c6
Princetonské	Princetonský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svůj	svůj	k3xOyFgInSc4
největší	veliký	k2eAgInSc4d3
objev	objev	k1gInSc4
byl	být	k5eAaImAgInS
oceněn	ocenit	k5eAaPmNgInS
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
cen	cena	k1gFnPc2
a	a	k8xC
vyznamenání	vyznamenání	k1gNnPc2
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
Abelovu	Abelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
Fermatově	Fermatův	k2eAgInSc6d1
problému	problém	k1gInSc6
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
řešení	řešení	k1gNnSc4
vyšla	vyjít	k5eAaPmAgFnS
kniha	kniha	k1gFnSc1
Simona	Simona	k1gFnSc1
Singha	Singha	k1gFnSc1
<g/>
:	:	kIx,
Fermat	fermata	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Last	Last	k1gInSc1
Theorem	Theor	k1gInSc7
(	(	kIx(
<g/>
v	v	k7c6
češtině	čeština	k1gFnSc6
Velká	velký	k2eAgFnSc1d1
Fermatova	Fermatův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
1964	#num#	k4
<g/>
-	-	kIx~
<g/>
,	,	kIx,
Singh	Singh	k1gMnSc1
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Fermatova	Fermatův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
:	:	kIx,
dramatická	dramatický	k2eAgFnSc1d1
historie	historie	k1gFnSc1
řešení	řešení	k1gNnSc2
největšího	veliký	k2eAgInSc2d3
matematického	matematický	k2eAgInSc2d1
problému	problém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
vyd	vyd	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
V	v	k7c6
upr	upr	k?
<g/>
.	.	kIx.
a	a	k8xC
dopl	doplit	k5eAaPmRp2nS
<g/>
.	.	kIx.
podobě	podoba	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
286	#num#	k4
s.	s.	k?
s.	s.	k?
ISBN	ISBN	kA
9788020014832	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
137331186	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
WILES	WILES	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modular	Modulara	k1gFnPc2
elliptic	elliptice	k1gFnPc2
curves	curves	k1gInSc1
and	and	k?
Fermat	fermata	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Last	Last	k1gInSc1
Theorem	Theor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annals	Annals	k1gInSc1
of	of	k?
Mathematics	Mathematics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
142	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
443	#num#	k4
<g/>
-	-	kIx~
<g/>
551	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
486	#num#	k4
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Andrew	Andrew	k1gFnSc2
Wiles	Wiles	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Držitelé	držitel	k1gMnPc1
Abelovy	Abelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
</s>
<s>
Jean-Pierre	Jean-Pierr	k1gMnSc5
Serre	Serr	k1gMnSc5
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Atiyah	Atiyah	k1gMnSc1
<g/>
,	,	kIx,
Isadore	Isador	k1gInSc5
Singer	Singer	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peter	Peter	k1gMnSc1
Lax	Lax	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lennart	Lennart	k1gInSc1
Carleson	Carleson	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Srinivasa	Srinivasa	k1gFnSc1
Varadhan	Varadhan	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Griggs	Griggsa	k1gFnPc2
Thompson	Thompson	k1gMnSc1
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
Tits	Titsa	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michail	Michail	k1gInSc1
Gromov	Gromov	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Tate	Tat	k1gMnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Milnor	Milnor	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Endre	Endr	k1gInSc5
Szemerédi	Szemeréd	k1gMnPc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre	Pierr	k1gMnSc5
Deligne	Delign	k1gMnSc5
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jakov	Jakov	k1gInSc1
Sinaj	Sinaj	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Forbes	forbes	k1gInSc4
Nash	Nash	k1gMnSc1
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
Nirenberg	Nirenberg	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Andrew	Andrew	k1gMnSc1
Wiles	Wiles	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Yves	Yves	k1gInSc1
Meyer	Meyer	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Robert	Robert	k1gMnSc1
Langlands	Langlands	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karen	Karen	k1gInSc1
Uhlenbeck	Uhlenbeck	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nositelé	nositel	k1gMnPc1
Wolfovy	Wolfův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
matematiku	matematika	k1gFnSc4
1978	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
</s>
<s>
Israel	Israel	k1gInSc1
Gelfand	Gelfand	k1gInSc1
/	/	kIx~
Carl	Carl	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Siegel	Siegel	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jean	Jean	k1gMnSc1
Leray	Leraa	k1gFnSc2
/	/	kIx~
André	André	k1gMnSc1
Weil	Weil	k1gMnSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
Henri	Henri	k6eAd1
Cartan	Cartan	k1gInSc1
/	/	kIx~
Andrej	Andrej	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Kolmogorov	Kolmogorov	k1gInSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lars	Lars	k1gInSc1
Ahlfors	Ahlfors	k1gInSc1
/	/	kIx~
Oscar	Oscar	k1gInSc1
Zariski	Zarisk	k1gFnSc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Hassler	Hassler	k1gInSc1
Whitney	Whitnea	k1gFnSc2
/	/	kIx~
Mark	Mark	k1gMnSc1
Grigorievič	Grigorievič	k1gMnSc1
Krejn	Krejn	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Čchen	Čchen	k1gInSc1
Sing-šen	Sing-šen	k2eAgInSc1d1
/	/	kIx~
Pál	Pál	k1gFnSc1
Erdős	Erdősa	k1gFnPc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Kunihiko	Kunihika	k1gFnSc5
Kodaira	Kodaira	k1gMnSc1
/	/	kIx~
Hans	Hans	k1gMnSc1
Lewy	Lewa	k1gMnSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Samuel	Samuel	k1gMnSc1
Eilenberg	Eilenberg	k1gMnSc1
/	/	kIx~
Atle	Atle	k1gFnSc1
Selberg	Selberg	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Kijoši	Kijoš	k1gMnPc1
Itó	Itó	k1gFnSc2
/	/	kIx~
Peter	Peter	k1gMnSc1
Lax	Lax	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
Hirzebruch	Hirzebruch	k1gMnSc1
/	/	kIx~
Lars	Lars	k1gInSc1
Hörmander	Hörmander	k1gInSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Alberto	Alberta	k1gFnSc5
Calderón	Calderón	k1gMnSc1
/	/	kIx~
John	John	k1gMnSc1
Milnor	Milnor	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
</s>
<s>
Ennio	Ennio	k6eAd1
de	de	k?
Giorgi	Giorgi	k1gNnPc1
/	/	kIx~
Ilja	Ilja	k1gFnSc1
Pjatěckij-Šapiro	Pjatěckij-Šapiro	k1gNnSc4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lennart	Lennart	k1gInSc1
Carleson	Carleson	k1gInSc1
/	/	kIx~
John	John	k1gMnSc1
Griggs	Griggsa	k1gFnPc2
Thompson	Thompson	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Michail	Michail	k1gMnSc1
Gromov	Gromov	k1gInSc1
/	/	kIx~
Jacques	Jacques	k1gMnSc1
Tits	Titsa	k1gFnPc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jürgen	Jürgen	k1gInSc1
Moser	Moser	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Robert	Robert	k1gMnSc1
Langlands	Langlands	k1gInSc1
/	/	kIx~
Andrew	Andrew	k1gMnSc1
Wiles	Wiles	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Joseph	Joseph	k1gMnSc1
Keller	Keller	k1gMnSc1
/	/	kIx~
Jakov	Jakov	k1gInSc1
Sinaj	Sinaj	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
László	László	k?
Lovász	Lovász	k1gMnSc1
/	/	kIx~
Elias	Elias	k1gMnSc1
Menachem	Menach	k1gMnSc7
Stein	Stein	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
Raoul	Raoul	k1gInSc1
Bott	Bott	k1gInSc1
/	/	kIx~
Jean-Pierre	Jean-Pierr	k1gMnSc5
Serre	Serr	k1gMnSc5
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Arnold	Arnold	k1gMnSc1
/	/	kIx~
Saharon	Saharon	k1gMnSc1
Šelach	Šelach	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Mikio	Mikio	k1gMnSc1
Sató	Sató	k1gMnSc1
/	/	kIx~
John	John	k1gMnSc1
Tate	Tat	k1gMnSc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Grigorij	Grigorít	k5eAaPmRp2nS
Alexandrovič	Alexandrovič	k1gInSc4
Margulis	Margulis	k1gFnPc4
/	/	kIx~
Sergej	Sergej	k1gMnSc1
Petrovič	Petrovič	k1gMnSc1
Novikov	Novikov	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Stephen	Stephen	k2eAgInSc1d1
Smale	Smale	k1gInSc1
/	/	kIx~
Hilel	Hilel	k1gMnSc1
Fürstenberg	Fürstenberg	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Pierre	Pierr	k1gMnSc5
Deligne	Delign	k1gMnSc5
/	/	kIx~
Phillip	Phillip	k1gInSc1
Griffiths	Griffiths	k1gInSc1
/	/	kIx~
David	David	k1gMnSc1
Mumford	Mumford	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
Čchiou	Čchiou	k6eAd1
Čcheng-tung	Čcheng-tung	k1gInSc1
/	/	kIx~
Dennis	Dennis	k1gFnSc1
Sullivan	Sullivan	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Michael	Michael	k1gMnSc1
Aschbacher	Aschbachra	k1gFnPc2
/	/	kIx~
Luís	Luís	k1gInSc1
Angel	Angela	k1gFnPc2
Caffarelli	Caffarell	k1gMnPc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
George	Georg	k1gMnSc4
Mostow	Mostow	k1gMnSc1
/	/	kIx~
Michael	Michael	k1gMnSc1
Artin	Artin	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Peter	Peter	k1gMnSc1
Sarnak	Sarnak	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
James	James	k1gMnSc1
Arthur	Arthur	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Richard	Richard	k1gMnSc1
Schoen	Schoen	k2eAgMnSc1d1
/	/	kIx~
Charles	Charles	k1gMnSc1
Fefferman	Fefferman	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Bejlinson	Bejlinson	k1gMnSc1
/	/	kIx~
Vladimir	Vladimir	k1gMnSc1
Geršonovič	Geršonovič	k1gMnSc1
Drinfeld	Drinfeld	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jean-François	Jean-François	k1gInSc1
Le	Le	k1gMnSc1
Gall	Gall	k1gMnSc1
/	/	kIx~
Gregory	Gregor	k1gMnPc4
F.	F.	kA
Lawler	Lawler	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2020	#num#	k4
<g/>
–	–	k?
<g/>
2029	#num#	k4
</s>
<s>
Simon	Simon	k1gMnSc1
Donaldson	Donaldson	k1gMnSc1
/	/	kIx~
Jakov	Jakov	k1gInSc1
Matvejevič	Matvejevič	k1gMnSc1
Eliašberg	Eliašberg	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22578	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
12010105X	12010105X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1075	#num#	k4
0153	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
97052585	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85443439	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
97052585	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
