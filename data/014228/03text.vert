<s>
First-person	First-person	k1gMnSc1
shooter	shooter	k1gMnSc1
</s>
<s>
First-person	First-person	k1gMnSc1
shooter	shooter	k1gMnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
FPS	FPS	kA
<g/>
;	;	kIx,
česky	česky	k6eAd1
střílečka	střílečka	k1gFnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
podžánr	podžánr	k1gMnSc1
stříleček	střílečka	k1gFnPc2
charakteristický	charakteristický	k2eAgInSc4d1
simulací	simulace	k1gFnSc7
vlastního	vlastní	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
herní	herní	k2eAgFnSc2d1
postavy	postava	k1gFnSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
postavy	postava	k1gFnPc1
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
hráč	hráč	k1gMnSc1
jedná	jednat	k5eAaImIp3nS
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
hře	hra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pojem	pojem	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
k	k	k7c3
odlišení	odlišení	k1gNnSc3
od	od	k7c2
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
zobrazovaných	zobrazovaný	k2eAgFnPc2d1
z	z	k7c2
pohledu	pohled	k1gInSc2
třetí	třetí	k4xOgFnSc2
osoby	osoba	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
third-person	third-person	k1gMnSc1
shooter	shooter	k1gMnSc1
(	(	kIx(
<g/>
TPS	TPS	kA
<g/>
)	)	kIx)
–	–	k?
střílečka	střílečka	k1gFnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
třetí	třetí	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
počítačových	počítačový	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
a	a	k8xC
videoherní	videoherní	k2eAgNnPc4d1
periodika	periodikum	k1gNnPc4
užívaly	užívat	k5eAaImAgFnP
dnes	dnes	k6eAd1
už	už	k6eAd1
zastaralý	zastaralý	k2eAgInSc1d1
výraz	výraz	k1gInSc1
doomovka	doomovka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
doom	doom	k6eAd1
klon	klon	k1gInSc4
pak	pak	k6eAd1
mělo	mít	k5eAaImAgNnS
celostvětový	celostvětový	k2eAgInSc4d1
význam	význam	k1gInSc4
(	(	kIx(
<g/>
oba	dva	k4xCgInPc1
výrazy	výraz	k1gInPc1
odkazují	odkazovat	k5eAaImIp3nP
na	na	k7c4
legendární	legendární	k2eAgFnSc4d1
počítačovou	počítačový	k2eAgFnSc4d1
hru	hra	k1gFnSc4
Doom	Dooma	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jako	jako	k9
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
her	hra	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
založila	založit	k5eAaPmAgFnS
žánr	žánr	k1gInSc4
her	hra	k1gFnPc2
FPS	FPS	kA
na	na	k7c6
PC	PC	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Wolfenstein	Wolfenstein	k1gInSc4
3D	3D	k4
od	od	k7c2
společnosti	společnost	k1gFnSc2
id	idy	k1gFnPc2
Software	software	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
kultovní	kultovní	k2eAgFnSc4d1
a	a	k8xC
průkopnickou	průkopnický	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
poměrně	poměrně	k6eAd1
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
,	,	kIx,
postava	postava	k1gFnSc1
hráče	hráč	k1gMnSc2
byla	být	k5eAaImAgFnS
agentem	agens	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
zachránit	zachránit	k5eAaPmF
z	z	k7c2
nacistické	nacistický	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
výběr	výběr	k1gInSc4
bylo	být	k5eAaImAgNnS
několik	několik	k4yIc1
zbraní	zbraň	k1gFnPc2
jako	jako	k8xS,k8xC
nůž	nůž	k1gInSc1
<g/>
,	,	kIx,
pistole	pistole	k1gFnSc1
<g/>
,	,	kIx,
samopal	samopal	k1gInSc1
a	a	k8xC
vícehlavňový	vícehlavňový	k2eAgInSc1d1
kulomet	kulomet	k1gInSc1
gatling	gatling	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřátelé	nepřítel	k1gMnPc1
byli	být	k5eAaImAgMnP
prostí	prostý	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
s	s	k7c7
různou	různý	k2eAgFnSc4d1
vyzbrojí	vyzbrojit	k5eAaPmIp3nP
<g/>
,	,	kIx,
občas	občas	k6eAd1
pes	pes	k1gMnSc1
nebo	nebo	k8xC
velitel	velitel	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
závěrečné	závěrečný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
s	s	k7c7
kulometem	kulomet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
klasickou	klasický	k2eAgFnSc7d1
hrou	hra	k1gFnSc7
je	být	k5eAaImIp3nS
Doom	Doom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
posílila	posílit	k5eAaPmAgFnS
žánr	žánr	k1gInSc4
FPS	FPS	kA
her	hra	k1gFnPc2
a	a	k8xC
přidala	přidat	k5eAaPmAgFnS
jim	on	k3xPp3gMnPc3
ještě	ještě	k9
hru	hra	k1gFnSc4
více	hodně	k6eAd2
hráčů	hráč	k1gMnPc2
–	–	k?
multiplayer	multiplayero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
hra	hra	k1gFnSc1
již	již	k6eAd1
byla	být	k5eAaImAgFnS
náročnější	náročný	k2eAgFnSc1d2
–	–	k?
běžela	běžet	k5eAaImAgFnS
na	na	k7c6
strojích	stroj	k1gInPc6
Intel	Intel	kA
80386	#num#	k4
s	s	k7c7
4MB	4MB	k4
paměti	paměť	k1gFnSc2
–	–	k?
a	a	k8xC
měla	mít	k5eAaImAgFnS
více	hodně	k6eAd2
úrovní	úroveň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
mezníkem	mezník	k1gInSc7
byl	být	k5eAaImAgInS
revoluční	revoluční	k2eAgInSc1d1
Quake	Quake	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
kompletně	kompletně	k6eAd1
3D	3D	k4
a	a	k8xC
neobsahoval	obsahovat	k5eNaImAgMnS
žádné	žádný	k3yNgInPc4
sprity	sprit	k1gInPc4
(	(	kIx(
<g/>
animovaná	animovaný	k2eAgFnSc1d1
textura	textura	k1gFnSc1
<g/>
,	,	kIx,
místo	místo	k1gNnSc1
postav	postava	k1gFnPc2
nebo	nebo	k8xC
objektů	objekt	k1gInPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
později	pozdě	k6eAd2
podporoval	podporovat	k5eAaImAgInS
i	i	k9
3D	3D	k4
akceleraci	akcelerace	k1gFnSc4
grafiky	grafika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
cíleně	cíleně	k6eAd1
vytvořena	vytvořit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
otevřená	otevřený	k2eAgFnSc1d1
<g/>
,	,	kIx,
přídavky	přídavka	k1gFnPc1
a	a	k8xC
mapy	mapa	k1gFnPc1
se	se	k3xPyFc4
daly	dát	k5eAaPmAgFnP
vytvořit	vytvořit	k5eAaPmF
pomocí	pomocí	k7c2
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quake	Quake	k1gInSc1
používal	používat	k5eAaImAgInS
pro	pro	k7c4
skriptování	skriptování	k1gNnSc4
vlastní	vlastní	k2eAgInSc1d1
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Quake	Quak	k1gInSc2
C.	C.	kA
</s>
<s>
Na	na	k7c6
všech	všecek	k3xTgFnPc6
těchto	tento	k3xDgFnPc6
hrách	hra	k1gFnPc6
vytvořila	vytvořit	k5eAaPmAgFnS
komunita	komunita	k1gFnSc1
hráčů	hráč	k1gMnPc2
různé	různý	k2eAgFnSc2d1
vlastní	vlastní	k2eAgFnSc2d1
přídavky	přídavka	k1gFnSc2
a	a	k8xC
některé	některý	k3yIgFnPc1
společnosti	společnost	k1gFnPc1
si	se	k3xPyFc3
koupily	koupit	k5eAaPmAgFnP
licenci	licence	k1gFnSc4
na	na	k7c4
herní	herní	k2eAgFnPc4d1
3D	3D	k4
engine	enginout	k5eAaPmIp3nS
a	a	k8xC
vytvořily	vytvořit	k5eAaPmAgInP
vlastní	vlastní	k2eAgFnSc4d1
hru	hra	k1gFnSc4
na	na	k7c6
tomto	tento	k3xDgNnSc6
engine	enginout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgFnSc7d3
jsou	být	k5eAaImIp3nP
Hexen	Hexna	k1gFnPc2
a	a	k8xC
Heretic	Heretice	k1gFnPc2
což	což	k9
je	být	k5eAaImIp3nS
obdoba	obdoba	k1gFnSc1
Doomu	Doom	k1gInSc2
ve	v	k7c4
fantasy	fantas	k1gInPc4
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Konkurenčními	konkurenční	k2eAgInPc7d1
projekty	projekt	k1gInPc7
byly	být	k5eAaImAgFnP
hry	hra	k1gFnPc4
Duke	Duke	k1gNnSc2
Nukem	nuk	k1gInSc7
3	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
Unreal	Unreal	k1gMnSc1
a	a	k8xC
Half-Life	Half-Lif	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
projekty	projekt	k1gInPc1
odstartovaly	odstartovat	k5eAaPmAgInP
vznik	vznik	k1gInSc4
silného	silný	k2eAgNnSc2d1
konkurenčního	konkurenční	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
existuje	existovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duke	Duke	k1gFnPc2
Nukem	nuk	k1gInSc7
3D	3D	k4
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
starším	starý	k2eAgInSc6d2
typu	typ	k1gInSc6
engine	enginout	k5eAaPmIp3nS
s	s	k7c7
3D	3D	k4
konstrukcí	konstrukce	k1gFnPc2
map	mapa	k1gFnPc2
ale	ale	k8xC
se	s	k7c7
zastaralou	zastaralý	k2eAgFnSc7d1
grafikou	grafika	k1gFnSc7
a	a	k8xC
dalšími	další	k2eAgNnPc7d1
omezeními	omezení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sázel	sázet	k5eAaImAgMnS
zejména	zejména	k9
na	na	k7c4
zábavnost	zábavnost	k1gFnSc4
<g/>
,	,	kIx,
výbornou	výborný	k2eAgFnSc4d1
hratelnost	hratelnost	k1gFnSc4
a	a	k8xC
interaktivní	interaktivní	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
Unreal	Unreal	k1gInSc4
používá	používat	k5eAaImIp3nS
svůj	svůj	k3xOyFgMnSc1
vlastní	vlastní	k2eAgMnSc1d1
engine	enginout	k5eAaPmIp3nS
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
vyvíjen	vyvíjen	k2eAgInSc1d1
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejlicencovanější	licencovaný	k2eAgInSc4d3
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
enginem	engin	k1gInSc7
Doom	Doom	k1gInSc4
3	#num#	k4
a	a	k8xC
Quake	Quak	k1gInSc2
3	#num#	k4
od	od	k7c2
společnosti	společnost	k1gFnSc2
ID	Ida	k1gFnPc2
Software	software	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unreal	Unreal	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
engine	enginout	k5eAaPmIp3nS
zaujal	zaujmout	k5eAaPmAgMnS
velice	velice	k6eAd1
pěknou	pěkný	k2eAgFnSc7d1
grafikou	grafika	k1gFnSc7
<g/>
,	,	kIx,
výbornou	výborný	k2eAgFnSc7d1
hudbou	hudba	k1gFnSc7
<g/>
,	,	kIx,
atmosférou	atmosféra	k1gFnSc7
a	a	k8xC
pokročilou	pokročilý	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
(	(	kIx(
<g/>
AI	AI	kA
–	–	k?
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
programoval	programovat	k5eAaImAgMnS
autor	autor	k1gMnSc1
populárních	populární	k2eAgMnPc2d1
botů	bot	k1gMnPc2
pro	pro	k7c4
Quake	Quake	k1gInSc4
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
her	hra	k1gFnPc2
obsahovala	obsahovat	k5eAaImAgFnS
určité	určitý	k2eAgInPc4d1
interaktivní	interaktivní	k2eAgInPc4d1
prvky	prvek	k1gInPc4
–	–	k?
nešlo	jít	k5eNaImAgNnS
jen	jen	k9
o	o	k7c4
pouhé	pouhý	k2eAgNnSc4d1
střílení	střílení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Half-Life	Half-Lif	k1gInSc5
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
rozšířeném	rozšířený	k2eAgInSc6d1
engine	enginout	k5eAaPmIp3nS
Quake	Quake	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
umožňoval	umožňovat	k5eAaImAgInS
jako	jako	k9
jedna	jeden	k4xCgFnSc1
s	s	k7c7
FPS	FPS	kA
her	hra	k1gFnPc2
interaktivitu	interaktivita	k1gFnSc4
nejen	nejen	k6eAd1
s	s	k7c7
prostředím	prostředí	k1gNnSc7
ale	ale	k8xC
i	i	k9
s	s	k7c7
osobami	osoba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
velice	velice	k6eAd1
populární	populární	k2eAgFnSc1d1
díky	díky	k7c3
poutavému	poutavý	k2eAgInSc3d1
příběhu	příběh	k1gInSc3
<g/>
,	,	kIx,
vysoké	vysoký	k2eAgFnSc3d1
hratelnosti	hratelnost	k1gFnSc3
a	a	k8xC
díky	díky	k7c3
velkým	velký	k2eAgFnPc3d1
možnostem	možnost	k1gFnPc3
rozšíření	rozšíření	k1gNnSc2
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pro	pro	k7c4
hru	hra	k1gFnSc4
vyšla	vyjít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejznámějších	známý	k2eAgNnPc2d3
rozšíření	rozšíření	k1gNnPc2
je	být	k5eAaImIp3nS
Counter-Strike	Counter-Strik	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
původně	původně	k6eAd1
vyšlo	vyjít	k5eAaPmAgNnS
jako	jako	k9
doplněk	doplněk	k1gInSc4
zdarma	zdarma	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
verze	verze	k1gFnSc2
1.6	1.6	k4
je	být	k5eAaImIp3nS
vydavatelem	vydavatel	k1gMnSc7
šířen	šířit	k5eAaImNgMnS
pouze	pouze	k6eAd1
jako	jako	k8xC,k8xS
samostatná	samostatný	k2eAgFnSc1d1
komerční	komerční	k2eAgFnSc1d1
hra	hra	k1gFnSc1
nebo	nebo	k8xC
doplněk	doplněk	k1gInSc1
ke	k	k7c3
hře	hra	k1gFnSc3
Half-Life	Half-Lif	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
her	hra	k1gFnPc2
</s>
<s>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Far	fara	k1gFnPc2
Cry	Cry	k1gFnSc2
</s>
<s>
Wolfenstein	Wolfenstein	k1gInSc1
3D	3D	k4
</s>
<s>
Blood	Blood	k1gInSc1
</s>
<s>
Half	halfa	k1gFnPc2
Life	Life	k1gNnPc2
2	#num#	k4
</s>
<s>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Quake	Quak	k1gFnSc2
</s>
<s>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Unreal	Unreal	k1gInSc1
</s>
<s>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Half-Life	Half-Lif	k1gInSc5
</s>
<s>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Dark	Dark	k1gMnSc1
Forces	Forces	k1gMnSc1
</s>
<s>
Team	team	k1gInSc1
Fortress	Fortress	k1gInSc1
2	#num#	k4
</s>
<s>
FPScore	FPScor	k1gMnSc5
</s>
<s>
F.	F.	kA
<g/>
E.A.	E.A.	k1gMnSc1
<g/>
R.	R.	kA
</s>
<s>
Duke	Duke	k6eAd1
Nukem	nuk	k1gInSc7
3D	3D	k4
</s>
<s>
War	War	k?
Rock	rock	k1gInSc1
</s>
<s>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
</s>
<s>
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Battlefield	Battlefielda	k1gFnPc2
</s>
<s>
Counter	Counter	k1gInSc1
Strike	Strik	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Žánry	žánr	k1gInPc4
videoher	videohra	k1gFnPc2
Akční	akční	k2eAgFnSc1d1
</s>
<s>
beat	beat	k1gInSc1
'	'	kIx"
<g/>
em	em	k?
up	up	k?
</s>
<s>
hack	hack	k1gInSc1
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
slash	slash	k1gMnSc1
</s>
<s>
bojová	bojový	k2eAgNnPc1d1
</s>
<s>
plošinovka	plošinovka	k1gFnSc1
</s>
<s>
střílečka	střílečka	k1gFnSc1
</s>
<s>
first-person	first-person	k1gMnSc1
</s>
<s>
third-person	third-person	k1gMnSc1
</s>
<s>
light	light	k1gInSc1
gun	gun	k?
</s>
<s>
shoot	shoot	k1gInSc1
'	'	kIx"
<g/>
em	em	k?
up	up	k?
</s>
<s>
taktická	taktický	k2eAgFnSc1d1
</s>
<s>
hra	hra	k1gFnSc1
o	o	k7c4
přežití	přežití	k1gNnSc4
</s>
<s>
battle	battlat	k5eAaPmIp3nS
royale	royale	k6eAd1
Akční	akční	k2eAgFnSc1d1
adventura	adventura	k1gFnSc1
</s>
<s>
klon	klon	k1gInSc1
Grand	grand	k1gMnSc1
Theft	Theft	k1gMnSc1
Auto	auto	k1gNnSc1
</s>
<s>
immersive	immersiev	k1gFnPc1
sim	sima	k1gFnPc2
</s>
<s>
metroidvania	metroidvanium	k1gNnPc1
</s>
<s>
stealth	stealth	k1gMnSc1
</s>
<s>
survival	survivat	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
horor	horor	k1gInSc1
Adventura	adventura	k1gFnSc1
</s>
<s>
escape	escapat	k5eAaPmIp3nS
the	the	k?
room	room	k6eAd1
</s>
<s>
interaktivní	interaktivní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
vizuální	vizuální	k2eAgInSc4d1
román	román	k1gInSc4
MMO	MMO	kA
</s>
<s>
MMOFPS	MMOFPS	kA
</s>
<s>
MMORPG	MMORPG	kA
</s>
<s>
MMORTS	MMORTS	kA
</s>
<s>
MUD	MUD	kA
Hra	hra	k1gFnSc1
na	na	k7c4
hrdiny	hrdina	k1gMnPc4
</s>
<s>
akční	akční	k2eAgFnSc1d1
RPG	RPG	kA
</s>
<s>
dungeon	dungeon	k1gMnSc1
crawl	crawl	k1gMnSc1
</s>
<s>
roguelike	roguelike	k6eAd1
</s>
<s>
taktické	taktický	k2eAgInPc1d1
RPG	RPG	kA
Simulátor	simulátor	k1gInSc1
</s>
<s>
stavební	stavební	k2eAgFnPc1d1
</s>
<s>
obchodu	obchod	k1gInSc2
</s>
<s>
budovatelská	budovatelský	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
</s>
<s>
politický	politický	k2eAgInSc1d1
</s>
<s>
života	život	k1gInSc2
</s>
<s>
rande	rande	k1gNnSc1
</s>
<s>
sportovní	sportovní	k2eAgFnSc1d1
Strategie	strategie	k1gFnSc1
</s>
<s>
4X	4X	k4
</s>
<s>
auto	auto	k1gNnSc1
battler	battler	k1gMnSc1
</s>
<s>
MOBA	MOBA	kA
</s>
<s>
realtimová	realtimový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
</s>
<s>
realtimová	realtimový	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
</s>
<s>
tower	tower	k1gInSc1
defense	defense	k1gFnSc2
</s>
<s>
tahová	tahový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
</s>
<s>
tahová	tahový	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
</s>
<s>
artillery	artiller	k1gInPc1
</s>
<s>
wargame	wargam	k1gInSc5
</s>
<s>
grand	grand	k1gMnSc1
strategy	stratega	k1gFnSc2
wargame	wargam	k1gInSc5
Dopravní	dopravní	k2eAgInPc1d1
simulátor	simulátor	k1gInSc4
</s>
<s>
letecký	letecký	k2eAgInSc1d1
</s>
<s>
vojenský	vojenský	k2eAgInSc1d1
</s>
<s>
vesmírný	vesmírný	k2eAgInSc1d1
</s>
<s>
závodní	závodní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
kart	kart	k2eAgInSc1d1
racing	racing	k1gInSc1
</s>
<s>
závodní	závodní	k2eAgInSc1d1
simulátor	simulátor	k1gInSc1
</s>
<s>
ponorky	ponorka	k1gFnPc1
</s>
<s>
vlakový	vlakový	k2eAgInSc1d1
Další	další	k2eAgInSc1d1
žánry	žánr	k1gInPc7
</s>
<s>
klon	klon	k1gInSc1
Breakout	Breakout	k1gMnSc1
</s>
<s>
deck-building	deck-building	k1gInSc1
</s>
<s>
roguelike	roguelike	k6eAd1
deck-building	deck-building	k1gInSc1
</s>
<s>
digitální	digitální	k2eAgFnSc1d1
sběratelská	sběratelský	k2eAgFnSc1d1
karetní	karetní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
eroge	eroge	k6eAd1
</s>
<s>
exergaming	exergaming	k1gInSc1
</s>
<s>
hororová	hororový	k2eAgFnSc1d1
</s>
<s>
klikací	klikací	k2eAgFnSc1d1
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
</s>
<s>
rytmická	rytmický	k2eAgFnSc1d1
</s>
<s>
non-game	non-gam	k1gInSc5
</s>
<s>
párty	párty	k1gFnSc1
</s>
<s>
programovací	programovací	k2eAgInSc1d1
</s>
<s>
logická	logický	k2eAgFnSc1d1
</s>
<s>
sokoban	sokoban	k1gInSc4
Související	související	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
AAA	AAA	kA
videohra	videohra	k1gFnSc1
</s>
<s>
reklama	reklama	k1gFnSc1
ve	v	k7c6
videohrách	videohra	k1gFnPc6
</s>
<s>
advergaming	advergaming	k1gInSc1
</s>
<s>
in-game	in-gam	k1gInSc5
reklama	reklama	k1gFnSc1
</s>
<s>
arkáda	arkáda	k1gFnSc1
</s>
<s>
audiohra	audiohra	k1gFnSc1
</s>
<s>
casual	casual	k1gInSc1
videohra	videohra	k1gFnSc1
</s>
<s>
křesťanská	křesťanský	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
crossover	crossover	k1gInSc1
videohra	videohra	k1gFnSc1
</s>
<s>
kultovní	kultovní	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
vzdělávací	vzdělávací	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
FMV	FMV	kA
</s>
<s>
gamifikace	gamifikace	k1gFnSc1
</s>
<s>
nezávislá	závislý	k2eNgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
multiplayer	multiplayer	k1gMnSc1
</s>
<s>
singleplayer	singleplayer	k1gMnSc1
</s>
<s>
kooperace	kooperace	k1gFnSc1
</s>
<s>
nelineární	lineární	k2eNgFnSc1d1
hratelnost	hratelnost	k1gFnSc1
</s>
<s>
open	open	k1gMnSc1
world	world	k1gMnSc1
</s>
<s>
sandboxová	sandboxový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
</s>
<s>
nenásilná	násilný	k2eNgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
online	onlinout	k5eAaPmIp3nS
hra	hra	k1gFnSc1
</s>
<s>
webová	webový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
</s>
<s>
sázení	sázení	k1gNnSc1
po	po	k7c6
internetu	internet	k1gInSc6
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c6
sociální	sociální	k2eAgFnSc6d1
síti	síť	k1gFnSc6
</s>
<s>
vážná	vážný	k2eAgFnSc1d1
videohra	videohra	k1gFnSc1
</s>
<s>
toys-to-life	toys-to-lif	k1gMnSc5
</s>
<s>
videoherní	videoherní	k2eAgInSc1d1
klon	klon	k1gInSc1
kategorie	kategorie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7709631-9	7709631-9	k4
</s>
