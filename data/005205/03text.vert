<p>
<s>
Quenya	Quenya	k1gFnSc1	Quenya
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
taky	taky	k9	taky
quenijština	quenijština	k1gFnSc1	quenijština
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
Vznešená	vznešený	k2eAgFnSc1d1	vznešená
elfština	elfština	k1gFnSc1	elfština
(	(	kIx(	(
<g/>
čtěte	číst	k5eAaImRp2nP	číst
kwenja	kwenja	k6eAd1	kwenja
<g/>
,	,	kIx,	,
kwenyjština	kwenyjština	k1gFnSc1	kwenyjština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
jazyk	jazyk	k1gInSc1	jazyk
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
Hobit	hobit	k1gMnSc1	hobit
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
-	-	kIx~	-
rodná	rodný	k2eAgFnSc1d1	rodná
řeč	řeč	k1gFnSc1	řeč
Vznešených	vznešený	k2eAgMnPc2d1	vznešený
elfů	elf	k1gMnPc2	elf
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Vanyar	Vanyara	k1gFnPc2	Vanyara
a	a	k8xC	a
Noldor	Noldora	k1gFnPc2	Noldora
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Quenya	Quenya	k1gFnSc1	Quenya
<g/>
"	"	kIx"	"
v	v	k7c6	v
quenijštině	quenijština	k1gFnSc6	quenijština
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
řeč	řeč	k1gFnSc1	řeč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
mytologie	mytologie	k1gFnSc2	mytologie
se	se	k3xPyFc4	se
quenijština	quenijština	k1gFnSc1	quenijština
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
Fëanorových	Fëanorův	k2eAgFnPc2d1	Fëanorova
tengwar	tengwara	k1gFnPc2	tengwara
<g/>
,	,	kIx,	,
standardně	standardně	k6eAd1	standardně
se	se	k3xPyFc4	se
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
latinkou	latinka	k1gFnSc7	latinka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
delší	dlouhý	k2eAgInPc4d2	delší
publikované	publikovaný	k2eAgInPc4d1	publikovaný
Tolkienovy	Tolkienův	k2eAgInPc4d1	Tolkienův
texty	text	k1gInPc4	text
v	v	k7c6	v
quenijštině	quenijština	k1gFnSc6	quenijština
patří	patřit	k5eAaImIp3nS	patřit
Galadrielina	Galadrielin	k2eAgFnSc1d1	Galadrielina
báseň	báseň	k1gFnSc1	báseň
Namárie	Namárie	k1gFnSc1	Namárie
a	a	k8xC	a
Aragornova	Aragornův	k2eAgFnSc1d1	Aragornova
korunovační	korunovační	k2eAgFnSc1d1	korunovační
deklarace	deklarace	k1gFnSc1	deklarace
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
a	a	k8xC	a
Cirionova	Cirionův	k2eAgFnSc1d1	Cirionův
přísaha	přísaha	k1gFnSc1	přísaha
Eorlovi	Eorlův	k2eAgMnPc1d1	Eorlův
v	v	k7c6	v
Nedokončených	dokončený	k2eNgInPc6d1	nedokončený
příbězích	příběh	k1gInPc6	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
historie	historie	k1gFnSc2	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Quenya	Quenya	k1gMnSc1	Quenya
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
prajazyku	prajazyk	k1gInSc6	prajazyk
všech	všecek	k3xTgFnPc2	všecek
Eldar	Eldara	k1gFnPc2	Eldara
<g/>
,	,	kIx,	,
praeldarštině	praeldarština	k1gFnSc3	praeldarština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tři	tři	k4xCgInPc1	tři
klany	klan	k1gInPc1	klan
Eldar	Eldara	k1gFnPc2	Eldara
přišly	přijít	k5eAaPmAgInP	přijít
na	na	k7c6	na
pozvání	pozvání	k1gNnSc6	pozvání
Valar	Valara	k1gFnPc2	Valara
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
do	do	k7c2	do
Valinoru	Valinor	k1gInSc2	Valinor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
již	již	k6eAd1	již
praeldarština	praeldarština	k1gFnSc1	praeldarština
rozrůznila	rozrůznit	k5eAaPmAgFnS	rozrůznit
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
klanu	klan	k1gInSc2	klan
Teleri	Teler	k1gFnSc2	Teler
-	-	kIx~	-
quenijštině	quenijština	k1gFnSc6	quenijština
blízkou	blízký	k2eAgFnSc4d1	blízká
telerijštinu	telerijština	k1gFnSc4	telerijština
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
Quenijštinu	Quenijština	k1gFnSc4	Quenijština
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc4	jazyk
Vanyar	Vanyara	k1gFnPc2	Vanyara
a	a	k8xC	a
Noldor	Noldora	k1gFnPc2	Noldora
<g/>
,	,	kIx,	,
rozlišený	rozlišený	k2eAgMnSc1d1	rozlišený
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
blízká	blízký	k2eAgNnPc4d1	blízké
nářečí	nářečí	k1gNnPc4	nářečí
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
té	ten	k3xDgFnSc2	ten
části	část	k1gFnSc2	část
Teleri	Teler	k1gFnSc2	Teler
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nakonec	nakonec	k6eAd1	nakonec
zůstali	zůstat	k5eAaPmAgMnP	zůstat
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
Šedými	šedý	k2eAgMnPc7d1	šedý
elfy	elf	k1gMnPc7	elf
Beleriandu	Beleriand	k1gInSc2	Beleriand
-	-	kIx~	-
Sindar	Sindar	k1gInSc1	Sindar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
během	během	k7c2	během
odloučení	odloučení	k1gNnSc2	odloučení
velmi	velmi	k6eAd1	velmi
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
v	v	k7c4	v
sindarštinu	sindarština	k1gFnSc4	sindarština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Quenijštinu	Quenijština	k1gFnSc4	Quenijština
přinesli	přinést	k5eAaPmAgMnP	přinést
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
prvního	první	k4xOgInSc2	první
věku	věk	k1gInSc2	věk
Středozemě	Středozem	k1gFnSc2	Středozem
Noldor	Noldora	k1gFnPc2	Noldora
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
Valinoru	Valinor	k1gInSc2	Valinor
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
Morgothovi	Morgothův	k2eAgMnPc1d1	Morgothův
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
zpět	zpět	k6eAd1	zpět
tři	tři	k4xCgFnPc4	tři
Fëanorovy	Fëanorův	k2eAgFnPc4d1	Fëanorova
Silmarily	Silmarila	k1gFnPc4	Silmarila
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
beleriandských	beleriandský	k2eAgFnPc2d1	beleriandský
Sindar	Sindara	k1gFnPc2	Sindara
však	však	k9	však
její	její	k3xOp3gNnSc1	její
používání	používání	k1gNnSc1	používání
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
říši	říš	k1gFnSc6	říš
zakázal	zakázat	k5eAaPmAgMnS	zakázat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zločinům	zločin	k1gInPc3	zločin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Noldor	Noldor	k1gInSc4	Noldor
spáchali	spáchat	k5eAaPmAgMnP	spáchat
na	na	k7c6	na
telerijských	telerijský	k2eAgFnPc6d1	telerijský
příbuzných	příbuzná	k1gFnPc6	příbuzná
Sindar	Sindara	k1gFnPc2	Sindara
ve	v	k7c6	v
Valinoru	Valinor	k1gInSc6	Valinor
při	při	k7c6	při
zabíjení	zabíjení	k1gNnSc6	zabíjení
rodných	rodný	k2eAgInPc2d1	rodný
<g/>
.	.	kIx.	.
</s>
<s>
Noldor	Noldor	k1gInSc1	Noldor
proto	proto	k8xC	proto
převzali	převzít	k5eAaPmAgMnP	převzít
pro	pro	k7c4	pro
běžnou	běžný	k2eAgFnSc4d1	běžná
komunikaci	komunikace	k1gFnSc4	komunikace
sindarštinu	sindarština	k1gFnSc4	sindarština
a	a	k8xC	a
Quenya	Quenya	k1gFnSc1	Quenya
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jazykem	jazyk	k1gInSc7	jazyk
písemných	písemný	k2eAgInPc2d1	písemný
záznamů	záznam	k1gInPc2	záznam
<g/>
,	,	kIx,	,
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
ceremonií	ceremonie	k1gFnPc2	ceremonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
Noldor	Noldora	k1gFnPc2	Noldora
se	se	k3xPyFc4	se
quenijštině	quenijština	k1gFnSc6	quenijština
i	i	k8xC	i
sindarštině	sindarština	k1gFnSc6	sindarština
naučili	naučit	k5eAaPmAgMnP	naučit
též	též	k9	též
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Tří	tři	k4xCgInPc2	tři
domů	dům	k1gInPc2	dům
Edain	Edaina	k1gFnPc2	Edaina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
po	po	k7c6	po
Morgothově	Morgothův	k2eAgFnSc6d1	Morgothova
porážce	porážka	k1gFnSc6	porážka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
věku	věk	k1gInSc2	věk
dostali	dostat	k5eAaPmAgMnP	dostat
od	od	k7c2	od
Valar	Valara	k1gFnPc2	Valara
darem	dar	k1gInSc7	dar
ostrovní	ostrovní	k2eAgFnSc3d1	ostrovní
říši	říš	k1gFnSc3	říš
Númenor	Númenor	k1gInSc4	Númenor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Quenya	Quenya	k1gFnSc1	Quenya
stala	stát	k5eAaPmAgFnS	stát
ceremoniálním	ceremoniální	k2eAgInSc7d1	ceremoniální
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
běžně	běžně	k6eAd1	běžně
nehovořilo	hovořit	k5eNaImAgNnS	hovořit
<g/>
,	,	kIx,	,
používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pro	pro	k7c4	pro
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
postavení	postavení	k1gNnSc1	postavení
quenijštiny	quenijština	k1gFnSc2	quenijština
se	se	k3xPyFc4	se
nezměnilo	změnit	k5eNaPmAgNnS	změnit
ani	ani	k8xC	ani
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
znalost	znalost	k1gFnSc1	znalost
udržovala	udržovat	k5eAaImAgFnS	udržovat
jednak	jednak	k8xC	jednak
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
noldorského	noldorský	k2eAgNnSc2d1	noldorský
osídlení	osídlení	k1gNnSc2	osídlení
(	(	kIx(	(
<g/>
Šedé	Šedé	k2eAgInPc1d1	Šedé
přístavy	přístav	k1gInPc1	přístav
<g/>
,	,	kIx,	,
Roklinka	roklinka	k1gFnSc1	roklinka
<g/>
,	,	kIx,	,
Lothlórien	Lothlórien	k1gInSc1	Lothlórien
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
mezi	mezi	k7c4	mezi
potomky	potomek	k1gMnPc4	potomek
Númenorejců	Númenorejec	k1gMnPc2	Númenorejec
Dúnadany	Dúnadana	k1gFnSc2	Dúnadana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reálná	reálný	k2eAgFnSc1d1	reálná
historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgMnS	začít
nazývat	nazývat	k5eAaImF	nazývat
"	"	kIx"	"
<g/>
Quenya	Quenya	k1gFnSc1	Quenya
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Tolkien	Tolkien	k1gInSc1	Tolkien
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
okouzlení	okouzlení	k1gNnSc1	okouzlení
finštinou	finština	k1gFnSc7	finština
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
"	"	kIx"	"
<g/>
Qenya	Qenya	k1gFnSc1	Qenya
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
výslovnost	výslovnost	k1gFnSc1	výslovnost
byla	být	k5eAaImAgFnS	být
tatáž	týž	k3xTgFnSc1	týž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
první	první	k4xOgFnSc7	první
verzí	verze	k1gFnSc7	verze
jeho	jeho	k3xOp3gFnSc2	jeho
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
Silmarillionem	Silmarillion	k1gInSc7	Silmarillion
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
r.	r.	kA	r.
1973	[number]	k4	1973
Tolkien	Tolkien	k1gInSc4	Tolkien
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
nikdy	nikdy	k6eAd1	nikdy
nepřestal	přestat	k5eNaPmAgMnS	přestat
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
doznal	doznat	k5eAaPmAgInS	doznat
jazyk	jazyk	k1gInSc1	jazyk
podstatných	podstatný	k2eAgFnPc2d1	podstatná
změn	změna	k1gFnPc2	změna
nejen	nejen	k6eAd1	nejen
co	co	k9	co
do	do	k7c2	do
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
a	a	k8xC	a
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
i	i	k9	i
co	co	k3yQnSc1	co
do	do	k7c2	do
fonologie	fonologie	k1gFnSc2	fonologie
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
co	co	k3yRnSc1	co
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
"	"	kIx"	"
<g/>
interní	interní	k2eAgFnSc2d1	interní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyplývajícího	vyplývající	k2eAgInSc2d1	vyplývající
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
jazykům	jazyk	k1gInPc3	jazyk
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
vývoji	vývoj	k1gInSc6	vývoj
svědčí	svědčit	k5eAaImIp3nS	svědčit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
doposud	doposud	k6eAd1	doposud
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
části	část	k1gFnSc6	část
nepublikované	publikovaný	k2eNgFnSc2d1	nepublikovaná
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
písemné	písemný	k2eAgFnSc2d1	písemná
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc2	jeho
fiktivních	fiktivní	k2eAgInPc2d1	fiktivní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
Quenya	Queny	k2eAgFnSc1d1	Quenya
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
označuje	označovat	k5eAaImIp3nS	označovat
verze	verze	k1gFnSc1	verze
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ukázka	ukázka	k1gFnSc1	ukázka
"	"	kIx"	"
<g/>
externího	externí	k2eAgInSc2d1	externí
<g/>
"	"	kIx"	"
vývoje	vývoj	k1gInSc2	vývoj
quenijštiny	quenijština	k1gFnSc2	quenijština
===	===	k?	===
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
básně	báseň	k1gFnSc2	báseň
Markirya	Markirya	k1gFnSc1	Markirya
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejdelších	dlouhý	k2eAgMnPc2d3	nejdelší
Tolkienových	Tolkienův	k2eAgMnPc2d1	Tolkienův
textů	text	k1gInPc2	text
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
Quenya	Quenya	k1gMnSc1	Quenya
<g/>
/	/	kIx~	/
<g/>
Qenya	Qenya	k1gMnSc1	Qenya
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
jejím	její	k3xOp3gInSc7	její
"	"	kIx"	"
<g/>
překladem	překlad	k1gInSc7	překlad
<g/>
"	"	kIx"	"
do	do	k7c2	do
aktuální	aktuální	k2eAgFnSc2d1	aktuální
podoby	podoba	k1gFnSc2	podoba
quenijštiny	quenijština	k1gFnSc2	quenijština
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Tolkien	Tolkien	k1gInSc1	Tolkien
pořídil	pořídit	k5eAaPmAgInS	pořídit
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
svého	své	k1gNnSc2	své
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
sloupci	sloupec	k1gInSc6	sloupec
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
druhé	druhý	k4xOgFnSc2	druhý
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
pořízený	pořízený	k2eAgInSc4d1	pořízený
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
Tolkienův	Tolkienův	k2eAgInSc4d1	Tolkienův
vlastní	vlastní	k2eAgInSc4d1	vlastní
překlad	překlad	k1gInSc4	překlad
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Problematika	problematika	k1gFnSc1	problematika
"	"	kIx"	"
<g/>
definitivní	definitivní	k2eAgFnSc1d1	definitivní
quenijštiny	quenijština	k1gFnPc1	quenijština
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tolkien	Tolkien	k1gInSc1	Tolkien
nikdy	nikdy	k6eAd1	nikdy
nevytvořil	vytvořit	k5eNaPmAgInS	vytvořit
definitivní	definitivní	k2eAgFnSc4d1	definitivní
verzi	verze	k1gFnSc4	verze
quenijštiny	quenijština	k1gFnSc2	quenijština
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nemožné	možný	k2eNgNnSc1d1	nemožné
popisovat	popisovat	k5eAaImF	popisovat
quenijštinu	quenijština	k1gFnSc4	quenijština
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc4d1	jediný
koherentní	koherentní	k2eAgInSc4d1	koherentní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
:	:	kIx,	:
dokonce	dokonce	k9	dokonce
Tolkienovy	Tolkienův	k2eAgInPc4d1	Tolkienův
texty	text	k1gInPc4	text
časově	časově	k6eAd1	časově
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgInPc1d1	blízký
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
uplatňovaných	uplatňovaný	k2eAgNnPc6d1	uplatňované
gramatických	gramatický	k2eAgNnPc6d1	gramatické
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Tolkien	Tolkien	k1gInSc4	Tolkien
často	často	k6eAd1	často
psal	psát	k5eAaImAgMnS	psát
texty	text	k1gInPc4	text
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
jazyce	jazyk	k1gInSc6	jazyk
čistě	čistě	k6eAd1	čistě
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
"	"	kIx"	"
<g/>
experimentu	experiment	k1gInSc2	experiment
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyzkoušení	vyzkoušení	k1gNnSc4	vyzkoušení
nějakého	nějaký	k3yIgInSc2	nějaký
bezprostředního	bezprostřední	k2eAgInSc2d1	bezprostřední
nápadu	nápad	k1gInSc2	nápad
ohledně	ohledně	k7c2	ohledně
<g />
.	.	kIx.	.
</s>
<s>
nějakého	nějaký	k3yIgInSc2	nějaký
gramatického	gramatický	k2eAgInSc2d1	gramatický
jevu	jev	k1gInSc2	jev
nebo	nebo	k8xC	nebo
etymologie	etymologie	k1gFnSc1	etymologie
nějakého	nějaký	k3yIgNnSc2	nějaký
slova	slovo	k1gNnSc2	slovo
apod.	apod.	kA	apod.
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
probíhá	probíhat	k5eAaImIp3nS	probíhat
zároveň	zároveň	k6eAd1	zároveň
proces	proces	k1gInSc4	proces
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
postupného	postupný	k2eAgNnSc2d1	postupné
publikování	publikování	k1gNnSc2	publikování
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
lingvistické	lingvistický	k2eAgFnSc2d1	lingvistická
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
týmem	tým	k1gInSc7	tým
editorů	editor	k1gInPc2	editor
ustanovených	ustanovený	k2eAgInPc2d1	ustanovený
Tolkienovým	Tolkienův	k2eAgMnSc7d1	Tolkienův
synem	syn	k1gMnSc7	syn
Christopherem	Christopher	k1gMnSc7	Christopher
<g/>
,	,	kIx,	,
a	a	k8xC	a
souběžné	souběžný	k2eAgInPc1d1	souběžný
pokusy	pokus	k1gInPc1	pokus
na	na	k7c6	na
základě	základ	k1gInSc6	základ
publikovaných	publikovaný	k2eAgInPc2d1	publikovaný
textů	text	k1gInPc2	text
a	a	k8xC	a
takto	takto	k6eAd1	takto
postupného	postupný	k2eAgInSc2d1	postupný
doplňovaného	doplňovaný	k2eAgInSc2d1	doplňovaný
materiálu	materiál	k1gInSc2	materiál
vytvořit	vytvořit	k5eAaPmF	vytvořit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
víceméně	víceméně	k9	víceméně
umělý	umělý	k2eAgInSc4d1	umělý
standard	standard	k1gInSc4	standard
lexika	lexikon	k1gNnSc2	lexikon
a	a	k8xC	a
gramatiky	gramatika	k1gFnSc2	gramatika
quenijštiny	quenijština	k1gFnSc2	quenijština
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
Tento	tento	k3xDgInSc1	tento
standard	standard	k1gInSc1	standard
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
formě	forma	k1gFnSc6	forma
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
jazyk	jazyk	k1gMnSc1	jazyk
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
Pánovi	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
se	se	k3xPyFc4	se
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
Tolkienova	Tolkienův	k2eAgNnSc2d1	Tolkienovo
díla	dílo	k1gNnSc2	dílo
"	"	kIx"	"
<g/>
Etymologies	Etymologies	k1gInSc1	Etymologies
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgInSc2d1	vydaný
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
The	The	k1gFnSc2	The
History	Histor	k1gInPc4	Histor
of	of	k?	of
Middle	Middle	k1gFnSc2	Middle
Earth	Eartha	k1gFnPc2	Eartha
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
5	[number]	k4	5
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
poněkud	poněkud	k6eAd1	poněkud
ranější	raný	k2eAgFnSc4d2	ranější
verzi	verze	k1gFnSc4	verze
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
umožnit	umožnit	k5eAaPmF	umožnit
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
jazyka	jazyk	k1gInSc2	jazyk
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
mínění	mínění	k1gNnSc2	mínění
některých	některý	k3yIgMnPc2	některý
významných	významný	k2eAgMnPc2d1	významný
tolkienovských	tolkienovský	k2eAgMnPc2d1	tolkienovský
lingvistů	lingvista	k1gMnPc2	lingvista
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
však	však	k9	však
takto	takto	k6eAd1	takto
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
útvar	útvar	k1gInSc1	útvar
neměl	mít	k5eNaImAgInS	mít
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
Quenya	Quenya	k1gFnSc1	Quenya
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
Neo-Quenya	Neo-Quenya	k1gFnSc1	Neo-Quenya
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
pravidlem	pravidlo	k1gNnSc7	pravidlo
komunity	komunita	k1gFnSc2	komunita
uživatelů	uživatel	k1gMnPc2	uživatel
quenijštiny	quenijština	k1gFnSc2	quenijština
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
písemných	písemný	k2eAgInPc6d1	písemný
textech	text	k1gInPc6	text
důsledně	důsledně	k6eAd1	důsledně
označovat	označovat	k5eAaImF	označovat
extrapolovaná	extrapolovaný	k2eAgFnSc1d1	extrapolovaná
a	a	k8xC	a
v	v	k7c6	v
Tolkienových	Tolkienův	k2eAgInPc6d1	Tolkienův
textech	text	k1gInPc6	text
neatestovaná	atestovaný	k2eNgNnPc4d1	atestovaný
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
gramatické	gramatický	k2eAgInPc4d1	gramatický
tvary	tvar	k1gInPc4	tvar
a	a	k8xC	a
významy	význam	k1gInPc4	význam
(	(	kIx(	(
<g/>
standardní	standardní	k2eAgInSc4d1	standardní
způsob	způsob	k1gInSc4	způsob
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
hvězdičky	hvězdička	k1gFnSc2	hvězdička
<g/>
:	:	kIx,	:
*	*	kIx~	*
<g/>
hantanyel	hantanyel	k1gInSc1	hantanyel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
popis	popis	k1gInSc1	popis
quenijštiny	quenijština	k1gFnSc2	quenijština
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutně	nutně	k6eAd1	nutně
neadekvátní	adekvátní	k2eNgInPc1d1	neadekvátní
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
podobu	podoba	k1gFnSc4	podoba
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
zjednodušující	zjednodušující	k2eAgInSc4d1	zjednodušující
přehled	přehled	k1gInSc4	přehled
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
spletitou	spletitý	k2eAgFnSc4d1	spletitá
problematiku	problematika	k1gFnSc4	problematika
spojenou	spojený	k2eAgFnSc4d1	spojená
takřka	takřka	k6eAd1	takřka
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
gramatickým	gramatický	k2eAgInSc7d1	gramatický
jevem	jev	k1gInSc7	jev
quenijštiny	quenijština	k1gFnSc2	quenijština
a	a	k8xC	a
zejména	zejména	k9	zejména
fakt	fakt	k1gInSc1	fakt
neustálého	neustálý	k2eAgInSc2d1	neustálý
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
Tolkienových	Tolkienův	k2eAgFnPc6d1	Tolkienova
představách	představa	k1gFnPc6	představa
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
ke	k	k7c3	k
každému	každý	k3xTgNnSc3	každý
uvedenému	uvedený	k2eAgNnSc3d1	uvedené
tvrzení	tvrzení	k1gNnSc3	tvrzení
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
nějakou	nějaký	k3yIgFnSc4	nějaký
výjimku	výjimka	k1gFnSc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Adekvátnější	adekvátní	k2eAgFnPc4d2	adekvátnější
informace	informace	k1gFnPc4	informace
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
následujících	následující	k2eAgNnPc6d1	následující
místech	místo	k1gNnPc6	místo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Elvish	Elvish	k1gInSc1	Elvish
Liguistic	Liguistice	k1gFnPc2	Liguistice
Fellowship-	Fellowship-	k1gFnPc2	Fellowship-
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
časopisy	časopis	k1gInPc4	časopis
Parma	Parma	k1gFnSc1	Parma
Eldalamberon	Eldalamberon	k1gMnSc1	Eldalamberon
a	a	k8xC	a
Vinyar	Vinyar	k1gMnSc1	Vinyar
Tengwar	Tengwar	k1gMnSc1	Tengwar
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
vycházejí	vycházet	k5eAaImIp3nP	vycházet
edice	edice	k1gFnPc1	edice
doposud	doposud	k6eAd1	doposud
nepublikovaných	publikovaný	k2eNgFnPc2d1	nepublikovaná
Tolkienových	Tolkienová	k1gFnPc2	Tolkienová
lingvistických	lingvistický	k2eAgFnPc2d1	lingvistická
materiálůArdalambion-	materiálůArdalambion-	k?	materiálůArdalambion-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
relativně	relativně	k6eAd1	relativně
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
stránek	stránka	k1gFnPc2	stránka
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
Tolkienovým	Tolkienův	k2eAgInPc3d1	Tolkienův
jazykům	jazyk	k1gInPc3	jazyk
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
uvedené	uvedený	k2eAgInPc1d1	uvedený
odkazy	odkaz	k1gInPc1	odkaz
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
vesměs	vesměs	k6eAd1	vesměs
relativně	relativně	k6eAd1	relativně
spolehlivými	spolehlivý	k2eAgInPc7d1	spolehlivý
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
spolehlivé	spolehlivý	k2eAgFnSc2d1	spolehlivá
lze	lze	k6eAd1	lze
pokládat	pokládat	k5eAaImF	pokládat
především	především	k9	především
materiály	materiál	k1gInPc4	materiál
pocházející	pocházející	k2eAgFnSc4d1	pocházející
od	od	k7c2	od
následujících	následující	k2eAgMnPc2d1	následující
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
F.	F.	kA	F.
Hostetter	Hostetter	k1gMnSc1	Hostetter
</s>
</p>
<p>
<s>
Bill	Bill	k1gMnSc1	Bill
Welden	Weldna	k1gFnPc2	Weldna
</s>
</p>
<p>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
Wynne	Wynn	k1gMnSc5	Wynn
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Gilson	Gilsona	k1gFnPc2	Gilsona
</s>
</p>
<p>
<s>
Helge	Helge	k1gFnSc1	Helge
Fauskanger	Fauskangra	k1gFnPc2	Fauskangra
</s>
</p>
<p>
<s>
Ryszard	Ryszard	k1gInSc1	Ryszard
Derdzinski	Derdzinsk	k1gFnSc2	Derdzinsk
</s>
</p>
<p>
<s>
Edouard	Edouard	k1gMnSc1	Edouard
Kloczko	Kloczka	k1gFnSc5	Kloczka
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
KiltzDalším	KiltzDalší	k2eAgInSc7d1	KiltzDalší
spolehlivým	spolehlivý	k2eAgInSc7d1	spolehlivý
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
maillist	maillist	k1gInSc1	maillist
Lambengolmor	Lambengolmora	k1gFnPc2	Lambengolmora
na	na	k7c4	na
YahooGroups	YahooGroups	k1gInSc4	YahooGroups
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fonologie	fonologie	k1gFnSc2	fonologie
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
quenijštinu	quenijština	k1gFnSc4	quenijština
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
velmi	velmi	k6eAd1	velmi
restriktivní	restriktivní	k2eAgNnPc1d1	restriktivní
fonologická	fonologický	k2eAgNnPc1d1	fonologické
pravidla	pravidlo	k1gNnPc1	pravidlo
stanovující	stanovující	k2eAgNnPc1d1	stanovující
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc1	jaký
hlásky	hlásek	k1gInPc1	hlásek
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
kombinace	kombinace	k1gFnPc1	kombinace
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
<g/>
;	;	kIx,	;
tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc4	pravidlo
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
"	"	kIx"	"
<g/>
libozvučnosti	libozvučnost	k1gFnSc2	libozvučnost
<g/>
"	"	kIx"	"
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
quenijštiny	quenijština	k1gFnSc2	quenijština
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
zřejmě	zřejmě	k6eAd1	zřejmě
nejvíce	hodně	k6eAd3	hodně
finštinou	finština	k1gFnSc7	finština
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
italštinou	italština	k1gFnSc7	italština
a	a	k8xC	a
latinou	latina	k1gFnSc7	latina
<g/>
;	;	kIx,	;
výslovnost	výslovnost	k1gFnSc1	výslovnost
quenijštiny	quenijština	k1gFnSc2	quenijština
by	by	kYmCp3nP	by
tak	tak	k9	tak
Čechovi	Čech	k1gMnSc3	Čech
neměla	mít	k5eNaImAgFnS	mít
činit	činit	k5eAaImF	činit
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
hlásek	hláska	k1gFnPc2	hláska
jsou	být	k5eAaImIp3nP	být
quenijské	quenijský	k2eAgInPc1d1	quenijský
zvuky	zvuk	k1gInPc1	zvuk
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
obdobnými	obdobný	k2eAgFnPc7d1	obdobná
českými	český	k2eAgFnPc7d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
přehledu	přehled	k1gInSc6	přehled
jsou	být	k5eAaImIp3nP	být
hlásky	hlásek	k1gInPc1	hlásek
uvedené	uvedený	k2eAgFnSc2d1	uvedená
hlásky	hláska	k1gFnSc2	hláska
podle	podle	k7c2	podle
standardní	standardní	k2eAgFnSc2d1	standardní
Tolkienovy	Tolkienův	k2eAgFnSc2d1	Tolkienova
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samohlásky	samohláska	k1gFnSc2	samohláska
===	===	k?	===
</s>
</p>
<p>
<s>
Quenya	Quenya	k6eAd1	Quenya
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
5	[number]	k4	5
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k8xC	a
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
uTyto	uTyto	k1gNnSc4	uTyto
samohlásky	samohláska	k1gFnSc2	samohláska
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
krátké	krátký	k2eAgNnSc4d1	krátké
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
;	;	kIx,	;
přičemž	přičemž	k6eAd1	přičemž
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
é	é	k0	é
a	a	k8xC	a
ó	ó	k0	ó
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
zavřenější	zavřený	k2eAgInSc4d2	zavřený
než	než	k8xS	než
jejich	jejich	k3xOp3gInPc4	jejich
krátké	krátký	k2eAgInPc4d1	krátký
protějšky	protějšek	k1gInPc4	protějšek
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
těch	ten	k3xDgMnPc2	ten
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
Obecná	obecný	k2eAgFnSc1d1	obecná
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
tyto	tento	k3xDgFnPc4	tento
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
samohlásky	samohláska	k1gFnPc4	samohláska
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
diftongizaci	diftongizace	k1gFnSc3	diftongizace
<g/>
:	:	kIx,	:
ei	ei	k?	ei
a	a	k8xC	a
ou	ou	k0	ou
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výslovnost	výslovnost	k1gFnSc1	výslovnost
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
chybnou	chybný	k2eAgFnSc4d1	chybná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
v	v	k7c6	v
latinském	latinský	k2eAgInSc6d1	latinský
přepisu	přepis	k1gInSc6	přepis
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
čárkou	čárka	k1gFnSc7	čárka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
daleka	daleko	k1gNnSc2	daleko
nejčastější	častý	k2eAgFnSc7d3	nejčastější
samohláskou	samohláska	k1gFnSc7	samohláska
je	být	k5eAaImIp3nS	být
a.	a.	k?	a.
</s>
</p>
<p>
<s>
===	===	k?	===
Dvojhlásky	dvojhláska	k1gFnSc2	dvojhláska
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
quenijštině	quenijština	k1gFnSc6	quenijština
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
6	[number]	k4	6
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ai	ai	k?	ai
<g/>
,	,	kIx,	,
oi	oi	k?	oi
<g/>
,	,	kIx,	,
ui	ui	k?	ui
</s>
</p>
<p>
<s>
au	au	k0	au
<g/>
,	,	kIx,	,
eu	eu	k?	eu
<g/>
,	,	kIx,	,
iuVšechny	iuVšechen	k2eAgFnPc1d1	iuVšechen
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
quenijštině	quenijština	k1gFnSc6	quenijština
klesavé	klesavý	k2eAgFnPc1d1	klesavá
-	-	kIx~	-
přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
první	první	k4xOgFnSc6	první
složce	složka	k1gFnSc6	složka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
iu	iu	k?	iu
začalo	začít	k5eAaPmAgNnS	začít
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
stoupavě	stoupavě	k6eAd1	stoupavě
<g/>
,	,	kIx,	,
jako	jako	k9	jako
ju	ju	k0	ju
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
kombinace	kombinace	k1gFnPc1	kombinace
samohlásek	samohláska	k1gFnPc2	samohláska
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
dvojslabičné	dvojslabičný	k2eAgInPc1d1	dvojslabičný
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
samohlásky	samohláska	k1gFnPc4	samohláska
za	za	k7c7	za
sebou	se	k3xPyFc7	se
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
samohláska	samohláska	k1gFnSc1	samohláska
a	a	k8xC	a
dvojhláska	dvojhláska	k1gFnSc1	dvojhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Typické	typický	k2eAgFnPc1d1	typická
dvojslabičné	dvojslabičný	k2eAgFnPc1d1	dvojslabičná
samohláskové	samohláskový	k2eAgFnPc1d1	samohlásková
kombinace	kombinace	k1gFnPc1	kombinace
===	===	k?	===
</s>
</p>
<p>
<s>
ëa	ëa	k?	ëa
<g/>
,	,	kIx,	,
ëo	ëo	k?	ëo
<g/>
,	,	kIx,	,
ië	ië	k?	ië
<g/>
,	,	kIx,	,
io	io	k?	io
<g/>
(	(	kIx(	(
<g/>
Dieresis	Dieresis	k1gFnSc1	Dieresis
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
tečky	tečka	k1gFnPc1	tečka
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
e	e	k0	e
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
upozornění	upozornění	k1gNnSc4	upozornění
na	na	k7c4	na
</s>
</p>
<p>
<s>
dvojslabičnou	dvojslabičný	k2eAgFnSc4d1	dvojslabičná
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
anglického	anglický	k2eAgMnSc4d1	anglický
čtenáře	čtenář	k1gMnSc4	čtenář
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Souhlásky	souhláska	k1gFnSc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
quenijštině	quenijština	k1gFnSc6	quenijština
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
následující	následující	k2eAgFnPc4d1	následující
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
t	t	k?	t
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
n	n	k0	n
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
ñ	ñ	k?	ñ
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
l	l	kA	l
<g/>
,	,	kIx,	,
hl	hl	k?	hl
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
hr	hr	k2eAgFnSc1d1	hr
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
hy	hy	k0	hy
<g/>
,	,	kIx,	,
hw	hw	k?	hw
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
th	th	k?	th
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
sPoznámky	sPoznámka	k1gFnPc4	sPoznámka
k	k	k7c3	k
výslovnosti	výslovnost	k1gFnSc3	výslovnost
aj.	aj.	kA	aj.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
hlásky	hláska	k1gFnSc2	hláska
k	k	k7c3	k
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
většinou	většina	k1gFnSc7	většina
písmeno	písmeno	k1gNnSc1	písmeno
c	c	k0	c
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
slovech	slovo	k1gNnPc6	slovo
tradičně	tradičně	k6eAd1	tradičně
k	k	k7c3	k
(	(	kIx(	(
<g/>
Tolkien	Tolkien	k2eAgMnSc1d1	Tolkien
sám	sám	k3xTgMnSc1	sám
nebyl	být	k5eNaImAgInS	být
konzistentní	konzistentní	k2eAgMnSc1d1	konzistentní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psané	psaný	k2eAgNnSc1d1	psané
c	c	k0	c
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
vždy	vždy	k6eAd1	vždy
k.	k.	k?	k.
</s>
</p>
<p>
<s>
ñ	ñ	k?	ñ
znamená	znamenat	k5eAaImIp3nS	znamenat
zadopatrovou	zadopatrový	k2eAgFnSc4d1	zadopatrový
nosovku	nosovka	k1gFnSc4	nosovka
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
n	n	k0	n
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
banka	banka	k1gFnSc1	banka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
vyslovovalo	vyslovovat	k5eAaImAgNnS	vyslovovat
jako	jako	k9	jako
běžné	běžný	k2eAgNnSc1d1	běžné
n	n	k0	n
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
zachovával	zachovávat	k5eAaImAgInS	zachovávat
v	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
v	v	k7c4	v
tengwar	tengwar	k1gInSc4	tengwar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
hl	hl	k?	hl
a	a	k8xC	a
hr	hr	k6eAd1	hr
označují	označovat	k5eAaImIp3nP	označovat
neznělé	znělý	k2eNgFnPc4d1	neznělá
l	l	kA	l
a	a	k8xC	a
r	r	kA	r
(	(	kIx(	(
<g/>
vyslovit	vyslovit	k5eAaPmF	vyslovit
jakoby	jakoby	k8xS	jakoby
šeptem	šeptem	k6eAd1	šeptem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
y	y	k?	y
označuje	označovat	k5eAaImIp3nS	označovat
hlásku	hláska	k1gFnSc4	hláska
j	j	k?	j
<g/>
,	,	kIx,	,
w	w	k?	w
anglické	anglický	k2eAgInPc4d1	anglický
obouretné	obouretný	k2eAgInPc4d1	obouretný
w	w	k?	w
jako	jako	k8xS	jako
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
window	window	k?	window
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
w	w	k?	w
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
v.	v.	k?	v.
</s>
</p>
<p>
<s>
hy	hy	k0	hy
snad	snad	k9	snad
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
h	h	k?	h
<g/>
+	+	kIx~	+
<g/>
j	j	k?	j
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
již	již	k9	již
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
hláska	hláska	k1gFnSc1	hláska
<g/>
,	,	kIx,	,
asi	asi	k9	asi
jako	jako	k9	jako
ch	ch	k0	ch
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
ich	ich	k?	ich
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ich-laut	ichaut	k1gInSc1	ich-laut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
něco	něco	k3yInSc1	něco
mezi	mezi	k7c4	mezi
ch	ch	k0	ch
a	a	k8xC	a
š	š	k?	š
(	(	kIx(	(
<g/>
jako	jako	k9	jako
š	š	k?	š
hlásku	hláska	k1gFnSc4	hláska
nesprávně	správně	k6eNd1	správně
vyslovovali	vyslovovat	k5eAaImAgMnP	vyslovovat
mluvčí	mluvčí	k1gMnPc1	mluvčí
Obecné	obecný	k2eAgFnSc2d1	obecná
řeči	řeč	k1gFnSc2	řeč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
hw	hw	k?	hw
snad	snad	k9	snad
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
h	h	k?	h
<g/>
+	+	kIx~	+
<g/>
w	w	k?	w
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
již	již	k9	již
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
hláska	hláska	k1gFnSc1	hláska
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
neznělé	znělý	k2eNgNnSc1d1	neznělé
w	w	k?	w
jako	jako	k8xC	jako
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
(	(	kIx(	(
<g/>
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
severoanglické	severoanglický	k2eAgFnPc1d1	severoanglická
<g/>
)	)	kIx)	)
výslovnosti	výslovnost	k1gFnPc1	výslovnost
slov	slovo	k1gNnPc2	slovo
jako	jako	k8xC	jako
when	when	k1gMnSc1	when
<g/>
,	,	kIx,	,
which	which	k1gMnSc1	which
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
hw	hw	k?	hw
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
w	w	k?	w
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
f	f	k?	f
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
th	th	k?	th
označuje	označovat	k5eAaImIp3nS	označovat
neznělé	znělý	k2eNgFnPc4d1	neznělá
anglické	anglický	k2eAgFnPc4d1	anglická
th	th	k?	th
jako	jako	k8xS	jako
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
thick	thicka	k1gFnPc2	thicka
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
vyslovené	vyslovený	k2eAgNnSc1d1	vyslovené
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hláska	hláska	k1gFnSc1	hláska
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
Noldor	Noldora	k1gFnPc2	Noldora
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
s	s	k7c7	s
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
se	se	k3xPyFc4	se
v	v	k7c4	v
tengwar	tengwar	k1gInSc4	tengwar
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
různá	různý	k2eAgNnPc1d1	různé
písmena	písmeno	k1gNnPc1	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
standardním	standardní	k2eAgInSc6d1	standardní
přepisu	přepis	k1gInSc6	přepis
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
většinou	většina	k1gFnSc7	většina
s.	s.	k?	s.
</s>
</p>
<p>
<s>
f	f	k?	f
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
labio-dentálně	labioentálně	k6eAd1	labio-dentálně
(	(	kIx(	(
<g/>
škvíra	škvíra	k1gFnSc1	škvíra
mezi	mezi	k7c7	mezi
horními	horní	k2eAgInPc7d1	horní
zuby	zub	k1gInPc7	zub
a	a	k8xC	a
dolním	dolní	k2eAgInSc7d1	dolní
rtem	ret	k1gInSc7	ret
<g/>
,	,	kIx,	,
ne	ne	k9	ne
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
rty	ret	k1gInPc7	ret
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišilo	odlišit	k5eAaPmAgNnS	odlišit
od	od	k7c2	od
hw	hw	k?	hw
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
v	v	k7c4	v
je	on	k3xPp3gInPc4	on
třeba	třeba	k6eAd1	třeba
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
w.	w.	k?	w.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
;	;	kIx,	;
d	d	k?	d
<g/>
,	,	kIx,	,
b	b	k?	b
a	a	k8xC	a
g	g	kA	g
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
shlucích	shluk	k1gInPc6	shluk
společně	společně	k6eAd1	společně
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
souhláskami	souhláska	k1gFnPc7	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Souhláskové	souhláskový	k2eAgInPc1d1	souhláskový
semi-shluky	semihluk	k1gInPc1	semi-shluk
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Souhláskovými	souhláskový	k2eAgInPc7d1	souhláskový
semi-shluky	semihluk	k1gInPc7	semi-shluk
<g/>
"	"	kIx"	"
nazývám	nazývat	k5eAaImIp1nS	nazývat
shluky	shluk	k1gInPc1	shluk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
druhým	druhý	k4xOgInSc7	druhý
elementem	element	k1gInSc7	element
je	být	k5eAaImIp3nS	být
y	y	k?	y
nebo	nebo	k8xC	nebo
w.	w.	k?	w.
Důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
sdílejí	sdílet	k5eAaImIp3nP	sdílet
některé	některý	k3yIgFnPc4	některý
fonologické	fonologický	k2eAgFnPc4d1	fonologická
charakteristiky	charakteristika	k1gFnPc4	charakteristika
s	s	k7c7	s
pravými	pravý	k2eAgInPc7d1	pravý
dvoj-souhláskovými	dvojouhláskův	k2eAgInPc7d1	dvoj-souhláskův
shluky	shluk	k1gInPc7	shluk
<g/>
,	,	kIx,	,
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
charakteristiky	charakteristika	k1gFnPc1	charakteristika
s	s	k7c7	s
unitárními	unitární	k2eAgFnPc7d1	unitární
souhláskami	souhláska	k1gFnPc7	souhláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
unitární	unitární	k2eAgFnPc1d1	unitární
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
smějí	smát	k5eAaImIp3nP	smát
stát	stát	k5eAaImF	stát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
</s>
</p>
<p>
<s>
smí	smět	k5eAaImIp3nS	smět
je	on	k3xPp3gNnSc4	on
(	(	kIx(	(
<g/>
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
okolností	okolnost	k1gFnPc2	okolnost
<g/>
)	)	kIx)	)
předcházet	předcházet	k5eAaImF	předcházet
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
</s>
</p>
<p>
<s>
smí	smět	k5eAaImIp3nS	smět
je	on	k3xPp3gNnSc4	on
předcházet	předcházet	k5eAaImF	předcházet
dvojhláskaJako	dvojhláskaJako	k6eAd1	dvojhláskaJako
shluky	shluk	k1gInPc1	shluk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
činí	činit	k5eAaImIp3nS	činit
slabiku	slabika	k1gFnSc4	slabika
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
přízvuku	přízvuk	k1gInSc2	přízvuk
</s>
</p>
<p>
<s>
nesmějí	smát	k5eNaImIp3nP	smát
stát	stát	k5eAaImF	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slovaMezi	slovaMeze	k1gFnSc4	slovaMeze
odborníky	odborník	k1gMnPc7	odborník
nepanuje	panovat	k5eNaImIp3nS	panovat
jednotný	jednotný	k2eAgInSc1d1	jednotný
názor	názor	k1gInSc1	názor
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
výslovnost	výslovnost	k1gFnSc4	výslovnost
(	(	kIx(	(
<g/>
Tolkienovy	Tolkienův	k2eAgInPc1d1	Tolkienův
údaje	údaj	k1gInPc1	údaj
nejsou	být	k5eNaImIp3nP	být
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
přichází	přicházet	k5eAaImIp3nS	přicházet
spektrum	spektrum	k1gNnSc1	spektrum
od	od	k7c2	od
zcela	zcela	k6eAd1	zcela
unitárního	unitární	k2eAgInSc2d1	unitární
zvuku	zvuk	k1gInSc2	zvuk
až	až	k9	až
po	po	k7c4	po
rozlišené	rozlišený	k2eAgFnPc4d1	rozlišená
dvě	dva	k4xCgFnPc4	dva
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
výslovnost	výslovnost	k1gFnSc1	výslovnost
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
okolnostech	okolnost	k1gFnPc6	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
výpisu	výpis	k1gInSc2	výpis
zahrnuji	zahrnovat	k5eAaImIp1nS	zahrnovat
výše	výše	k1gFnPc4	výše
uvedené	uvedený	k2eAgFnPc4d1	uvedená
hy	hy	k0	hy
a	a	k8xC	a
hw	hw	k?	hw
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ačkoliv	ačkoliv	k8xS	ačkoliv
podle	podle	k7c2	podle
Tolkiena	Tolkieno	k1gNnSc2	Tolkieno
jde	jít	k5eAaImIp3nS	jít
foneticky	foneticky	k6eAd1	foneticky
jasně	jasně	k6eAd1	jasně
o	o	k7c4	o
unitární	unitární	k2eAgInPc4d1	unitární
hlásky	hlásek	k1gInPc4	hlásek
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
systematicky	systematicky	k6eAd1	systematicky
náležejí	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
semi-shluků	semihluk	k1gMnPc2	semi-shluk
<g/>
;	;	kIx,	;
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
charakter	charakter	k1gInSc4	charakter
shluku	shluk	k1gInSc2	shluk
nepodržují	podržovat	k5eNaImIp3nP	podržovat
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tato	tento	k3xDgFnSc1	tento
kombinace	kombinace	k1gFnSc1	kombinace
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
sekundárním	sekundární	k2eAgInSc7d1	sekundární
kontaktem	kontakt	k1gInSc7	kontakt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kmen	kmen	k1gInSc1	kmen
<g/>
+	+	kIx~	+
<g/>
přípona	přípona	k1gFnSc1	přípona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
každého	každý	k3xTgInSc2	každý
"	"	kIx"	"
<g/>
semi-shluku	semihluk	k1gInSc2	semi-shluk
<g/>
"	"	kIx"	"
uvádím	uvádět	k5eAaImIp1nS	uvádět
předpokládanou	předpokládaný	k2eAgFnSc4d1	předpokládaná
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pokládám	pokládat	k5eAaImIp1nS	pokládat
za	za	k7c4	za
nejpravděpodobnější	pravděpodobný	k2eAgFnSc4d3	nejpravděpodobnější
<g/>
,	,	kIx,	,
po	po	k7c4	po
tu	ten	k3xDgFnSc4	ten
nejméně	málo	k6eAd3	málo
pravděpodobnou	pravděpodobný	k2eAgFnSc4d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Semi-shluky	Semihluk	k1gInPc1	Semi-shluk
jsou	být	k5eAaImIp3nP	být
typickým	typický	k2eAgInSc7d1	typický
prvkem	prvek	k1gInSc7	prvek
quenijské	quenijský	k2eAgFnSc2d1	quenijská
fonologie	fonologie	k1gFnSc2	fonologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
palatalizované	palatalizovaný	k2eAgInPc1d1	palatalizovaný
souhláskyty	souhláskyt	k1gInPc1	souhláskyt
-	-	kIx~	-
tj	tj	kA	tj
<g/>
,	,	kIx,	,
ťj	ťj	k?	ťj
<g/>
,	,	kIx,	,
ť	ť	k?	ť
(	(	kIx(	(
<g/>
mluvčí	mluvčí	k1gMnSc1	mluvčí
obecné	obecný	k2eAgFnSc2d1	obecná
řeči	řeč	k1gFnSc2	řeč
prý	prý	k9	prý
vyslovovali	vyslovovat	k5eAaImAgMnP	vyslovovat
č.	č.	k?	č.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ny	ny	k?	ny
-	-	kIx~	-
nj	nj	k?	nj
<g/>
,	,	kIx,	,
ňj	ňj	k?	ňj
<g/>
,	,	kIx,	,
ň	ň	k?	ň
</s>
</p>
<p>
<s>
hy	hy	k0	hy
-	-	kIx~	-
ich-laut	ichaut	k1gMnSc1	ich-laut
<g/>
,	,	kIx,	,
ich-laut	ichaut	k1gMnSc1	ich-laut
<g/>
+	+	kIx~	+
<g/>
j	j	k?	j
</s>
</p>
<p>
<s>
ry	ry	k?	ry
-	-	kIx~	-
rj	rj	k?	rj
</s>
</p>
<p>
<s>
ly	ly	k?	ly
-	-	kIx~	-
lj	lj	k?	lj
<g/>
,	,	kIx,	,
ľj	ľj	k?	ľj
<g/>
,	,	kIx,	,
ľ	ľ	k?	ľ
</s>
</p>
<p>
<s>
ky	ky	k?	ky
-	-	kIx~	-
kj	kj	k?	kj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
viz	vidět	k5eAaImRp2nS	vidět
ty	ty	k3xPp2nSc5	ty
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
kombinace	kombinace	k1gFnSc1	kombinace
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
jediném	jediný	k2eAgNnSc6d1	jediné
známém	známý	k2eAgNnSc6d1	známé
slově	slovo	k1gNnSc6	slovo
Erukyermë	Erukyermë	k1gFnSc2	Erukyermë
<g/>
)	)	kIx)	)
<g/>
labializované	labializovaný	k2eAgNnSc1d1	labializovaný
souhláskytw	souhláskytw	k?	souhláskytw
tw	tw	k?	tw
</s>
</p>
<p>
<s>
nw	nw	k?	nw
nw	nw	k?	nw
</s>
</p>
<p>
<s>
ñ	ñ	k?	ñ
původně	původně	k6eAd1	původně
ñ	ñ	k?	ñ
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nw	nw	k?	nw
</s>
</p>
<p>
<s>
hw	hw	k?	hw
neznělé	znělý	k2eNgFnSc2d1	neznělá
w	w	k?	w
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
snad	snad	k9	snad
někdy	někdy	k6eAd1	někdy
též	též	k9	též
h	h	k?	h
+	+	kIx~	+
(	(	kIx(	(
<g/>
neznělé	znělý	k2eNgFnSc3d1	neznělá
<g/>
)	)	kIx)	)
w	w	k?	w
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
něco	něco	k3yInSc4	něco
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rw	rw	k?	rw
rw	rw	k?	rw
</s>
</p>
<p>
<s>
lw	lw	k?	lw
lw	lw	k?	lw
</s>
</p>
<p>
<s>
qu	qu	k?	qu
kw	kw	kA	kw
</s>
</p>
<p>
<s>
===	===	k?	===
Pravé	pravý	k2eAgInPc1d1	pravý
souhláskové	souhláskový	k2eAgInPc1d1	souhláskový
shluky	shluk	k1gInPc1	shluk
===	===	k?	===
</s>
</p>
<p>
<s>
Pravé	pravý	k2eAgInPc1d1	pravý
souhláskové	souhláskový	k2eAgInPc1d1	souhláskový
shluky	shluk	k1gInPc1	shluk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nesmějí	smát	k5eNaImIp3nP	smát
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
</s>
</p>
<p>
<s>
nesmějí	smát	k5eNaImIp3nP	smát
(	(	kIx(	(
<g/>
až	až	k8xS	až
na	na	k7c4	na
jedinou	jediný	k2eAgFnSc4d1	jediná
výjimku	výjimka	k1gFnSc4	výjimka
nt	nt	k?	nt
<g/>
)	)	kIx)	)
stát	stát	k1gInSc1	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
</s>
</p>
<p>
<s>
nesmí	smět	k5eNaImIp3nS	smět
je	být	k5eAaImIp3nS	být
předcházet	předcházet	k5eAaImF	předcházet
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
ani	ani	k8xC	ani
dvojhláska	dvojhláska	k1gFnSc1	dvojhláska
</s>
</p>
<p>
<s>
činí	činit	k5eAaImIp3nS	činit
slabiku	slabika	k1gFnSc4	slabika
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
přízvukupp	přízvukupp	k1gMnSc1	přízvukupp
<g/>
,	,	kIx,	,
tt	tt	k?	tt
<g/>
,	,	kIx,	,
kk	kk	k?	kk
<g/>
,	,	kIx,	,
nn	nn	k?	nn
<g/>
,	,	kIx,	,
mm	mm	kA	mm
<g/>
,	,	kIx,	,
ll	ll	k?	ll
<g/>
,	,	kIx,	,
rr	rr	k?	rr
<g/>
,	,	kIx,	,
ss	ss	k?	ss
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
zdvojeně	zdvojeně	k6eAd1	zdvojeně
či	či	k8xC	či
dlouze	dlouho	k6eAd1	dlouho
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nt	nt	k?	nt
<g/>
,	,	kIx,	,
lt	lt	k?	lt
<g/>
,	,	kIx,	,
rt	rt	k?	rt
<g/>
,	,	kIx,	,
st	st	kA	st
<g/>
,	,	kIx,	,
ht	ht	k?	ht
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
ich-laut	ichaut	k1gInSc1	ich-laut
+	+	kIx~	+
t	t	k?	t
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nty	nty	k?	nty
<g/>
,	,	kIx,	,
lty	lty	k?	lty
<g/>
,	,	kIx,	,
rty	ret	k1gInPc4	ret
<g/>
,	,	kIx,	,
sty	sto	k4xCgNnPc7	sto
<g/>
,	,	kIx,	,
hty	hty	k?	hty
</s>
</p>
<p>
<s>
mp	mp	k?	mp
<g/>
,	,	kIx,	,
lp	lp	k?	lp
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ñ	ñ	k?	ñ
<g/>
,	,	kIx,	,
lk	lk	k?	lk
<g/>
,	,	kIx,	,
rk	rk	k?	rk
<g/>
,	,	kIx,	,
sk	sk	k?	sk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ñ	ñ	k?	ñ
<g/>
,	,	kIx,	,
lqu	lqu	k?	lqu
<g/>
,	,	kIx,	,
rqu	rqu	k?	rqu
<g/>
,	,	kIx,	,
squ	squ	k?	squ
</s>
</p>
<p>
<s>
mb	mb	k?	mb
<g/>
,	,	kIx,	,
nd	nd	k?	nd
<g/>
,	,	kIx,	,
ñ	ñ	k?	ñ
<g/>
,	,	kIx,	,
ñ	ñ	k?	ñ
</s>
</p>
<p>
<s>
ts	ts	k0	ts
<g/>
,	,	kIx,	,
x	x	k?	x
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
ks	ks	kA	ks
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ld	ld	k?	ld
<g/>
,	,	kIx,	,
rd	rd	k?	rd
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
mn	mn	k?	mn
<g/>
,	,	kIx,	,
rn	rn	k?	rn
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
lm	lm	k?	lm
<g/>
,	,	kIx,	,
rm	rm	k?	rm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
lv	lv	k?	lv
<g/>
,	,	kIx,	,
rv	rv	k?	rv
(	(	kIx(	(
<g/>
lv	lv	k?	lv
někteří	některý	k3yIgMnPc1	některý
elfové	elf	k1gMnPc1	elf
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k9	jako
lb	lb	k?	lb
<g/>
)	)	kIx)	)
<g/>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ojediněle	ojediněle	k6eAd1	ojediněle
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jiné	jiný	k2eAgInPc1d1	jiný
shluky	shluk	k1gInPc1	shluk
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zejména	zejména	k9	zejména
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sekundárního	sekundární	k2eAgInSc2d1	sekundární
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
skládání	skládání	k1gNnSc4	skládání
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
pravidelně	pravidelně	k6eAd1	pravidelně
k	k	k7c3	k
asimilaci	asimilace	k1gFnSc3	asimilace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
telep	telep	k1gMnSc1	telep
+	+	kIx~	+
nar	nar	kA	nar
→	→	k?	→
Telemnar	Telemnar	k1gInSc1	Telemnar
</s>
</p>
<p>
<s>
elen	elen	k1gNnSc1	elen
+	+	kIx~	+
sar	sar	k?	sar
→	→	k?	→
Elessar	Elessar	k1gInSc1	Elessar
</s>
</p>
<p>
<s>
tul	tula	k1gFnPc2	tula
+	+	kIx~	+
na	na	k7c6	na
→	→	k?	→
tulda	tulda	k1gMnSc1	tulda
</s>
</p>
<p>
<s>
===	===	k?	===
Některá	některý	k3yIgNnPc1	některý
fonologická	fonologický	k2eAgNnPc1d1	fonologické
pravidla	pravidlo	k1gNnPc1	pravidlo
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
buď	buď	k8xC	buď
samohláska	samohláska	k1gFnSc1	samohláska
nebo	nebo	k8xC	nebo
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
:	:	kIx,	:
<g/>
t	t	k?	t
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
sNa	sen	k1gInSc2	sen
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
shluk	shluk	k1gInSc1	shluk
ani	ani	k8xC	ani
semi-shluk	semihluk	k1gInSc1	semi-shluk
<g/>
.	.	kIx.	.
</s>
<s>
Výjimka	výjimka	k1gFnSc1	výjimka
je	být	k5eAaImIp3nS	být
nt	nt	k?	nt
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
příponě	přípona	k1gFnSc6	přípona
dativu	dativ	k1gInSc2	dativ
duálu	duál	k1gInSc2	duál
(	(	kIx(	(
<g/>
máryant	máryant	k1gInSc1	máryant
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gInSc1	jeho
rukoum	rukoum	k1gInSc1	rukoum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shluk	shluk	k1gInSc1	shluk
souhlásek	souhláska	k1gFnPc2	souhláska
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
jen	jen	k6eAd1	jen
dvě	dva	k4xCgFnPc4	dva
souhlásky	souhláska	k1gFnPc4	souhláska
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
souhlásku	souhláska	k1gFnSc4	souhláska
a	a	k8xC	a
semishluk	semishluk	k1gInSc4	semishluk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
samohláskami	samohláska	k1gFnPc7	samohláska
(	(	kIx(	(
<g/>
ne	ne	k9	ne
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
-	-	kIx~	-
výjimka	výjimka	k1gFnSc1	výjimka
"	"	kIx"	"
<g/>
nt	nt	k?	nt
<g/>
"	"	kIx"	"
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádných	žádný	k3yNgFnPc2	žádný
okolností	okolnost	k1gFnPc2	okolnost
jej	on	k3xPp3gInSc4	on
nepředchází	předcházet	k5eNaImIp3nS	předcházet
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
ani	ani	k8xC	ani
dvojhláska	dvojhláska	k1gFnSc1	dvojhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Semi-shluk	Semihluk	k6eAd1	Semi-shluk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nebo	nebo	k8xC	nebo
uprostřed	uprostřed	k7c2	uprostřed
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
být	být	k5eAaImF	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
(	(	kIx(	(
<g/>
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
díky	díky	k7c3	díky
druhotnému	druhotný	k2eAgInSc3d1	druhotný
kontaktu	kontakt	k1gInSc3	kontakt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojhláska	dvojhláska	k1gFnSc1	dvojhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
y	y	k?	y
a	a	k8xC	a
palatální	palatální	k2eAgFnSc1d1	palatální
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
semi-shluky	semihluk	k1gInPc4	semi-shluk
nemůže	moct	k5eNaImIp3nS	moct
následovat	následovat	k5eAaImF	následovat
i.	i.	k?	i.
</s>
</p>
<p>
<s>
w	w	k?	w
a	a	k8xC	a
labiální	labiální	k2eAgFnSc1d1	labiální
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
w	w	k?	w
<g/>
)	)	kIx)	)
semi-shluky	semihluk	k1gInPc4	semi-shluk
nemůže	moct	k5eNaImIp3nS	moct
následovat	následovat	k5eAaImF	následovat
u.	u.	k?	u.
</s>
</p>
<p>
<s>
hr	hr	k6eAd1	hr
a	a	k8xC	a
hl	hl	k?	hl
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
w	w	k?	w
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
mezi	mezi	k7c7	mezi
samohláskami	samohláska	k1gFnPc7	samohláska
je	být	k5eAaImIp3nS	být
archaismus	archaismus	k1gInSc1	archaismus
-	-	kIx~	-
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
v.	v.	k?	v.
</s>
</p>
<p>
<s>
Slovní	slovní	k2eAgInSc1d1	slovní
přízvuk	přízvuk	k1gInSc1	přízvuk
u	u	k7c2	u
dvojslabičných	dvojslabičný	k2eAgNnPc2d1	dvojslabičné
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
troj-	troj-	k?	troj-
a	a	k8xC	a
víceslabičných	víceslabičný	k2eAgInPc2d1	víceslabičný
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
předposlední	předposlední	k2eAgFnSc6d1	předposlední
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
slabice	slabika	k1gFnSc6	slabika
od	od	k7c2	od
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
slabika	slabika	k1gFnSc1	slabika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
dvojhlásku	dvojhláska	k1gFnSc4	dvojhláska
<g/>
,	,	kIx,	,
shluk	shluk	k1gInSc1	shluk
nebo	nebo	k8xC	nebo
semi-shluk	semihluk	k1gInSc1	semi-shluk
<g/>
.	.	kIx.	.
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
slabika	slabika	k1gFnSc1	slabika
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
samohláskou	samohláska	k1gFnSc7	samohláska
následovanou	následovaný	k2eAgFnSc4d1	následovaná
jedinou	jediný	k2eAgFnSc4d1	jediná
nebo	nebo	k8xC	nebo
žádnou	žádný	k3yNgFnSc7	žádný
unitární	unitární	k2eAgFnSc7d1	unitární
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
skloňování	skloňování	k1gNnSc3	skloňování
či	či	k8xC	či
skládání	skládání	k1gNnSc4	skládání
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
padnout	padnout	k5eAaPmF	padnout
přízvuk	přízvuk	k1gInSc4	přízvuk
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
slabice	slabika	k1gFnSc6	slabika
od	od	k7c2	od
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
následovanou	následovaný	k2eAgFnSc7d1	následovaná
bezprostředně	bezprostředně	k6eAd1	bezprostředně
další	další	k2eAgFnSc7d1	další
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
samohláska	samohláska	k1gFnSc1	samohláska
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
slabice	slabika	k1gFnSc6	slabika
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
a	a	k8xC	a
přízvuk	přízvuk	k1gInSc1	přízvuk
padne	padnout	k5eAaImIp3nS	padnout
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
:	:	kIx,	:
<g/>
Eldalie	Eldalie	k1gFnSc2	Eldalie
+	+	kIx~	+
va	va	k0wR	va
→	→	k?	→
Eldaliéva	Eldaliévo	k1gNnPc1	Eldaliévo
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
Eldalieva	Eldalievo	k1gNnSc2	Eldalievo
<g/>
)	)	kIx)	)
<g/>
ale	ale	k8xC	ale
</s>
</p>
<p>
<s>
Valinóre	Valinór	k1gMnSc5	Valinór
+	+	kIx~	+
va	va	k0wR	va
→	→	k?	→
ValinórevaStane	ValinórevaStan	k1gInSc5	ValinórevaStan
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
slabika	slabika	k1gFnSc1	slabika
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
samohláskou	samohláska	k1gFnSc7	samohláska
třetí	třetí	k4xOgInSc4	třetí
od	od	k7c2	od
konce	konec	k1gInSc2	konec
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
přízvuku	přízvuk	k1gInSc2	přízvuk
<g/>
,	,	kIx,	,
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
se	se	k3xPyFc4	se
<g/>
:	:	kIx,	:
<g/>
Endóre	Endór	k1gInSc5	Endór
+	+	kIx~	+
nna	nna	k?	nna
→	→	k?	→
EndorennaQuenya	EndorennaQuenya	k1gFnSc1	EndorennaQuenya
většinou	většinou	k6eAd1	většinou
netoleruje	tolerovat	k5eNaImIp3nS	tolerovat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
tři	tři	k4xCgFnPc1	tři
krátké	krátká	k1gFnPc1	krátká
slabiky	slabika	k1gFnSc2	slabika
za	za	k7c7	za
sebou	se	k3xPyFc7	se
-	-	kIx~	-
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
k	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
přízvučná	přízvučný	k2eAgFnSc1d1	přízvučná
slabika	slabika	k1gFnSc1	slabika
se	se	k3xPyFc4	se
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
<g/>
:	:	kIx,	:
<g/>
vanima	vanima	k1gFnSc1	vanima
+	+	kIx~	+
lion	lion	k?	lion
→	→	k?	→
vanimálion	vanimálion	k1gInSc1	vanimálion
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Charakter	charakter	k1gInSc1	charakter
quenijské	quenijský	k2eAgFnSc2d1	quenijská
gramatiky	gramatika	k1gFnSc2	gramatika
===	===	k?	===
</s>
</p>
<p>
<s>
Quenya	Quenya	k1gFnSc1	Quenya
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
jazyků	jazyk	k1gInPc2	jazyk
aglutinujících	aglutinující	k2eAgInPc2d1	aglutinující
a	a	k8xC	a
flektujících	flektující	k2eAgInPc2d1	flektující
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
blíže	blízce	k6eAd2	blízce
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
jazykům	jazyk	k1gMnPc3	jazyk
aglutinujícím	aglutinující	k2eAgMnPc3d1	aglutinující
<g/>
.	.	kIx.	.
</s>
<s>
Rozličné	rozličný	k2eAgFnSc2d1	rozličná
syntaktické	syntaktický	k2eAgFnSc2d1	syntaktická
funkce	funkce	k1gFnSc2	funkce
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
většinou	většina	k1gFnSc7	většina
pomocí	pomoc	k1gFnPc2	pomoc
přípon	přípona	k1gFnPc2	přípona
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
možné	možný	k2eAgNnSc1d1	možné
řadit	řadit	k5eAaImF	řadit
"	"	kIx"	"
<g/>
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
"	"	kIx"	"
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
ugrofinských	ugrofinský	k2eAgInPc6d1	ugrofinský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnSc2	některý
gramatické	gramatický	k2eAgFnSc2d1	gramatická
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
však	však	k9	však
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
ohýbáním	ohýbání	k1gNnSc7	ohýbání
pomocí	pomoc	k1gFnSc7	pomoc
přípon	přípona	k1gFnPc2	přípona
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
kmeni	kmen	k1gInSc6	kmen
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
quenijštiny	quenijština	k1gFnSc2	quenijština
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vypozorován	vypozorovat	k5eAaPmNgInS	vypozorovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zásada	zásada	k1gFnSc1	zásada
non-redundance	nonedundance	k1gFnSc1	non-redundance
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
že	že	k8xS	že
určitá	určitý	k2eAgFnSc1d1	určitá
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
:	:	kIx,	:
to	ten	k3xDgNnSc4	ten
např.	např.	kA	např.
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
substantiva	substantivum	k1gNnSc2	substantivum
určeného	určený	k2eAgInSc2d1	určený
adjektivem	adjektivum	k1gNnSc7	adjektivum
přibírá	přibírat	k5eAaImIp3nS	přibírat
pádovou	pádový	k2eAgFnSc4d1	pádová
příponu	přípona	k1gFnSc4	přípona
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
-	-	kIx~	-
nedochází	docházet	k5eNaImIp3nS	docházet
tedy	tedy	k9	tedy
ke	k	k7c3	k
shodě	shoda	k1gFnSc3	shoda
v	v	k7c6	v
pádu	pád	k1gInSc6	pád
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
ke	k	k7c3	k
shodě	shoda	k1gFnSc3	shoda
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
ano	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
quenijštiny	quenijština	k1gFnSc2	quenijština
jsou	být	k5eAaImIp3nP	být
zájmenné	zájmenný	k2eAgFnPc1d1	zájmenná
přípony	přípona	k1gFnPc1	přípona
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
především	především	k9	především
místo	místo	k7c2	místo
osobních	osobní	k2eAgFnPc2d1	osobní
přípon	přípona	k1gFnPc2	přípona
k	k	k7c3	k
časování	časování	k1gNnSc3	časování
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
formě	forma	k1gFnSc6	forma
potom	potom	k6eAd1	potom
jako	jako	k8xC	jako
koncovky	koncovka	k1gFnSc2	koncovka
jmen	jméno	k1gNnPc2	jméno
místo	místo	k7c2	místo
přivlastňovacích	přivlastňovací	k2eAgFnPc2d1	přivlastňovací
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
Zájmenné	zájmenný	k2eAgFnPc1d1	zájmenná
přípony	přípona	k1gFnPc1	přípona
mohou	moct	k5eAaImIp3nP	moct
též	též	k9	též
vyznačovat	vyznačovat	k5eAaImF	vyznačovat
předmět	předmět	k1gInSc4	předmět
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
textů	text	k1gInPc2	text
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
kombinovat	kombinovat	k5eAaImF	kombinovat
i	i	k9	i
s	s	k7c7	s
předložkami	předložka	k1gFnPc7	předložka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
utúvie	utúvie	k1gFnSc1	utúvie
"	"	kIx"	"
<g/>
najít	najít	k5eAaPmF	najít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
perf	perf	k1gInSc1	perf
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
-nye	ye	k1gFnSc1	-nye
"	"	kIx"	"
<g/>
koncovka	koncovka	k1gFnSc1	koncovka
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
+	+	kIx~	+
-s	-s	k?	-s
"	"	kIx"	"
<g/>
koncovka	koncovka	k1gFnSc1	koncovka
to	ten	k3xDgNnSc1	ten
<g/>
/	/	kIx~	/
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
→	→	k?	→
utúvienyes	utúvienyes	k1gMnSc1	utúvienyes
"	"	kIx"	"
<g/>
našel	najít	k5eAaPmAgMnS	najít
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
óre	óre	k?	óre
"	"	kIx"	"
<g/>
srdce	srdce	k1gNnSc1	srdce
<g/>
"	"	kIx"	"
+	+	kIx~	+
-nya	y	k2eAgFnSc1d1	-ny
"	"	kIx"	"
<g/>
koncovka	koncovka	k1gFnSc1	koncovka
moje	můj	k3xOp1gFnSc1	můj
<g/>
"	"	kIx"	"
→	→	k?	→
órenya	órenya	k1gFnSc1	órenya
"	"	kIx"	"
<g/>
moje	můj	k3xOp1gNnSc4	můj
srdce	srdce	k1gNnSc4	srdce
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Quenya	Quenya	k6eAd1	Quenya
má	mít	k5eAaImIp3nS	mít
vůbec	vůbec	k9	vůbec
poměrně	poměrně	k6eAd1	poměrně
bohatý	bohatý	k2eAgMnSc1d1	bohatý
(	(	kIx(	(
<g/>
a	a	k8xC	a
díky	díky	k7c3	díky
Tolkienovým	Tolkienův	k2eAgFnPc3d1	Tolkienova
častým	častý	k2eAgFnPc3d1	častá
změnám	změna	k1gFnPc3	změna
velmi	velmi	k6eAd1	velmi
nepřehledný	přehledný	k2eNgInSc4d1	nepřehledný
<g/>
)	)	kIx)	)
systém	systém	k1gInSc1	systém
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
rozlišující	rozlišující	k2eAgNnPc4d1	rozlišující
například	například	k6eAd1	například
u	u	k7c2	u
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
druhé	druhý	k4xOgFnSc6	druhý
osoby	osoba	k1gFnPc1	osoba
formu	forma	k1gFnSc4	forma
důvěrnou	důvěrný	k2eAgFnSc4d1	důvěrná
a	a	k8xC	a
formální	formální	k2eAgFnSc4d1	formální
<g/>
,	,	kIx,	,
či	či	k8xC	či
u	u	k7c2	u
první	první	k4xOgFnSc2	první
osoby	osoba	k1gFnSc2	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
zájmeno	zájmeno	k1gNnSc4	zájmeno
"	"	kIx"	"
<g/>
inkluzivní	inkluzivnět	k5eAaPmIp3nS	inkluzivnět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
my	my	k3xPp1nPc1	my
včetně	včetně	k7c2	včetně
tebe	ty	k3xPp2nSc2	ty
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
exkluzivní	exkluzivní	k2eAgMnPc4d1	exkluzivní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
ty	ty	k3xPp2nSc1	ty
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
syntaxe	syntax	k1gFnSc2	syntax
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
povšimnout	povšimnout	k5eAaPmF	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohatství	bohatství	k1gNnSc4	bohatství
jmenných	jmenný	k2eAgInPc2d1	jmenný
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
existence	existence	k1gFnSc1	existence
skloňovatelného	skloňovatelný	k2eAgNnSc2d1	skloňovatelný
gerundia	gerundium	k1gNnSc2	gerundium
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tvorbu	tvorba	k1gFnSc4	tvorba
různých	různý	k2eAgFnPc2d1	různá
jmenných	jmenný	k2eAgFnPc2d1	jmenná
a	a	k8xC	a
infinitivních	infinitivní	k2eAgFnPc2d1	infinitivní
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
nahrazujících	nahrazující	k2eAgMnPc6d1	nahrazující
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
syntaxe	syntaxe	k1gFnSc1	syntaxe
quenijštiny	quenijština	k1gFnSc2	quenijština
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
podobný	podobný	k2eAgInSc4d1	podobný
klasickým	klasický	k2eAgInPc3d1	klasický
jazykům	jazyk	k1gInPc3	jazyk
-	-	kIx~	-
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
snad	snad	k9	snad
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
klasické	klasický	k2eAgFnSc3d1	klasická
řečtině	řečtina	k1gFnSc3	řečtina
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
např.	např.	kA	např.
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
určitého	určitý	k2eAgInSc2d1	určitý
členu	člen	k1gInSc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Některé	některý	k3yIgFnPc4	některý
gramatické	gramatický	k2eAgFnPc4d1	gramatická
kategorie	kategorie	k1gFnPc4	kategorie
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Jmenné	jmenný	k2eAgInPc1d1	jmenný
pády	pád	k1gInPc1	pád
====	====	k?	====
</s>
</p>
<p>
<s>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
1	[number]	k4	1
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
4	[number]	k4	4
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
-	-	kIx~	-
označuje	označovat	k5eAaImIp3nS	označovat
přímý	přímý	k2eAgInSc4d1	přímý
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Formou	forma	k1gFnSc7	forma
se	se	k3xPyFc4	se
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
archaické	archaický	k2eAgFnSc6d1	archaická
formě	forma	k1gFnSc6	forma
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
Valinorské	Valinorský	k2eAgFnSc6d1	Valinorský
Quenijštině	Quenijština	k1gFnSc6	Quenijština
<g/>
)	)	kIx)	)
prodloužením	prodloužení	k1gNnSc7	prodloužení
konc	konc	k6eAd1	konc
<g/>
.	.	kIx.	.
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
a	a	k8xC	a
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
Noldor	Noldora	k1gFnPc2	Noldora
však	však	k9	však
postupně	postupně	k6eAd1	postupně
odlišený	odlišený	k2eAgInSc4d1	odlišený
tvar	tvar	k1gInSc4	tvar
pro	pro	k7c4	pro
akuzativ	akuzativ	k1gInSc4	akuzativ
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nahrazem	nahraz	k1gInSc7	nahraz
tvarem	tvar	k1gInSc7	tvar
nominativu	nominativ	k1gInSc2	nominativ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
2	[number]	k4	2
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
koncovka	koncovka	k1gFnSc1	koncovka
-o	-o	k?	-o
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
posesiv	posesivum	k1gNnPc2	posesivum
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
asociativ	asociativ	k1gInSc1	asociativ
či	či	k8xC	či
"	"	kIx"	"
<g/>
adjektivní	adjektivní	k2eAgInSc1d1	adjektivní
pád	pád	k1gInSc1	pád
<g/>
"	"	kIx"	"
-	-	kIx~	-
významem	význam	k1gInSc7	význam
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
přídavnému	přídavný	k2eAgNnSc3d1	přídavné
jménu	jméno	k1gNnSc3	jméno
odvozenému	odvozený	k2eAgInSc3d1	odvozený
od	od	k7c2	od
daného	daný	k2eAgNnSc2d1	dané
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
užití	užití	k1gNnSc1	užití
často	často	k6eAd1	často
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
užitím	užití	k1gNnSc7	užití
genitivu	genitiv	k1gInSc2	genitiv
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
posesiv	posesivum	k1gNnPc2	posesivum
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
přijmout	přijmout	k5eAaPmF	přijmout
koncovku	koncovka	k1gFnSc4	koncovka
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
řídícím	řídící	k2eAgNnSc6d1	řídící
jménu	jméno	k1gNnSc6	jméno
(	(	kIx(	(
<g/>
aran	aran	k1gNnSc1	aran
Ñ	Ñ	k?	Ñ
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
Noldor	Noldor	k1gMnSc1	Noldor
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
noldorský	noldorský	k2eAgMnSc1d1	noldorský
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
>>	>>	k?	>>
"	"	kIx"	"
<g/>
arani	aranit	k5eAaPmRp2nS	aranit
Ñ	Ñ	k?	Ñ
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
králové	králová	k1gFnSc3	králová
Noldor	Noldora	k1gFnPc2	Noldora
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
noldorští	noldorský	k2eAgMnPc1d1	noldorský
králové	král	k1gMnPc1	král
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
je	být	k5eAaImIp3nS	být
-va	a	k?	-va
či	či	k8xC	či
-wa	a	k?	-wa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
3	[number]	k4	3
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
-	-	kIx~	-
označuje	označovat	k5eAaImIp3nS	označovat
nepřímý	přímý	k2eNgInSc1d1	nepřímý
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
-in	n	k?	-in
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lokativ	lokativ	k1gInSc1	lokativ
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
kde	kde	k6eAd1	kde
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
sse	sse	k?	sse
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
ssen	ssen	k1gMnSc1	ssen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ablativ	ablativ	k1gInSc1	ablativ
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
odkud	odkud	k6eAd1	odkud
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
llo	llo	k?	llo
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
llon	llon	k1gMnSc1	llon
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
llor	llor	k1gMnSc1	llor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
allativ	allativ	k1gInSc1	allativ
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
kam	kam	k6eAd1	kam
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
nna	nna	k?	nna
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
nnar	nnar	k1gMnSc1	nnar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
7	[number]	k4	7
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
cosi	cosi	k3yInSc1	cosi
konáno	konán	k2eAgNnSc1d1	konáno
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
něco	něco	k3yInSc4	něco
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
nen	nen	k?	nen
<g/>
,	,	kIx,	,
v	v	k7c6	v
plur	plur	k1gMnSc1	plur
<g/>
.	.	kIx.	.
-inen	nen	k1gInSc1	-inen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
respektiv	respektiv	k1gInSc1	respektiv
(	(	kIx(	(
<g/>
funkce	funkce	k1gFnSc1	funkce
pádu	pád	k1gInSc2	pád
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
způsob	způsob	k1gInSc1	způsob
jeho	on	k3xPp3gNnSc2	on
tvoření	tvoření	k1gNnSc2	tvoření
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
analogický	analogický	k2eAgInSc1d1	analogický
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
lokativu	lokativ	k1gInSc3	lokativ
jako	jako	k9	jako
genitiv	genitiv	k1gInSc1	genitiv
k	k	k7c3	k
ablativu	ablativ	k1gInSc3	ablativ
a	a	k8xC	a
dativ	dativ	k1gInSc1	dativ
k	k	k7c3	k
alativu	alativ	k1gInSc3	alativ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
se	se	k3xPyFc4	se
na	na	k7c4	na
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
s.	s.	k?	s.
<g/>
Mezi	mezi	k7c7	mezi
pády	pád	k1gInPc7	pád
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tři	tři	k4xCgFnPc1	tři
dvojice	dvojice	k1gFnPc1	dvojice
"	"	kIx"	"
<g/>
silnějšího	silný	k2eAgInSc2d2	silnější
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
slabšího	slabý	k2eAgInSc2d2	slabší
<g/>
"	"	kIx"	"
pádu	pád	k1gInSc2	pád
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
blízké	blízký	k2eAgNnSc4d1	blízké
jak	jak	k6eAd1	jak
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
morfologickým	morfologický	k2eAgInSc7d1	morfologický
způsobem	způsob	k1gInSc7	způsob
tvoření	tvoření	k1gNnSc2	tvoření
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ablativ	ablativ	k1gInSc1	ablativ
(	(	kIx(	(
<g/>
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
)	)	kIx)	)
-	-	kIx~	-
genitiv	genitiv	k1gInSc1	genitiv
(	(	kIx(	(
<g/>
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
allativ	allativ	k1gInSc1	allativ
(	(	kIx(	(
<g/>
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
)	)	kIx)	)
-	-	kIx~	-
dativ	dativ	k1gInSc1	dativ
(	(	kIx(	(
<g/>
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lokál	lokál	k1gInSc1	lokál
(	(	kIx(	(
<g/>
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
)	)	kIx)	)
-	-	kIx~	-
respektiv	respektiv	k1gInSc1	respektiv
(	(	kIx(	(
<g/>
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
)	)	kIx)	)
<g/>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
slabší	slabý	k2eAgMnSc1d2	slabší
<g/>
"	"	kIx"	"
pád	pád	k1gInSc1	pád
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
zastupovat	zastupovat	k5eAaImF	zastupovat
svůj	svůj	k3xOyFgInSc4	svůj
významový	významový	k2eAgInSc4d1	významový
"	"	kIx"	"
<g/>
silnější	silný	k2eAgInSc4d2	silnější
<g/>
"	"	kIx"	"
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Slovesné	slovesný	k2eAgInPc1d1	slovesný
časy	čas	k1gInPc1	čas
====	====	k?	====
</s>
</p>
<p>
<s>
aorist	aorist	k1gInSc1	aorist
či	či	k8xC	či
prostý	prostý	k2eAgInSc1d1	prostý
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
-	-	kIx~	-
Označuje	označovat	k5eAaImIp3nS	označovat
děj	děj	k1gInSc1	děj
probíhající	probíhající	k2eAgMnSc1d1	probíhající
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
neurčené	určený	k2eNgFnSc6d1	neurčená
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nadčasové	nadčasový	k2eAgFnSc2d1	nadčasová
pravdy	pravda	k1gFnSc2	pravda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
široce	široko	k6eAd1	široko
chápané	chápaný	k2eAgFnSc6d1	chápaná
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
důrazu	důraz	k1gInSc2	důraz
na	na	k7c4	na
přítomný	přítomný	k2eAgInSc4d1	přítomný
okamžik	okamžik	k1gInSc4	okamžik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
anglickému	anglický	k2eAgInSc3d1	anglický
prostému	prostý	k2eAgInSc3d1	prostý
přítomnému	přítomný	k2eAgInSc3d1	přítomný
času	čas	k1gInSc3	čas
<g/>
.	.	kIx.	.
<g/>
tulin	tulin	k1gMnSc1	tulin
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
přicházím	přicházet	k5eAaImIp1nS	přicházet
<g/>
"	"	kIx"	"
<g/>
přítomný	přítomný	k2eAgInSc1d1	přítomný
průběhový	průběhový	k2eAgInSc1d1	průběhový
čas	čas	k1gInSc1	čas
či	či	k8xC	či
kontinuativ	kontinuativ	k1gInSc1	kontinuativ
-	-	kIx~	-
Označuje	označovat	k5eAaImIp3nS	označovat
děj	děj	k1gInSc1	děj
probíhající	probíhající	k2eAgMnSc1d1	probíhající
v	v	k7c6	v
právě	právě	k6eAd1	právě
přítomném	přítomný	k2eAgInSc6d1	přítomný
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
anglickému	anglický	k2eAgInSc3d1	anglický
přítomnému	přítomný	k2eAgInSc3d1	přítomný
průběhovému	průběhový	k2eAgInSc3d1	průběhový
času	čas	k1gInSc3	čas
<g/>
.	.	kIx.	.
<g/>
túlan	túlan	k1gInSc1	túlan
"	"	kIx"	"
<g/>
právě	právě	k9	právě
přicházím	přicházet	k5eAaImIp1nS	přicházet
<g/>
"	"	kIx"	"
<g/>
perfektum	perfektum	k1gNnSc1	perfektum
-	-	kIx~	-
Označuje	označovat	k5eAaImIp3nS	označovat
přítomný	přítomný	k2eAgInSc1d1	přítomný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
minulého	minulý	k2eAgInSc2d1	minulý
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
perfektu	perfektum	k1gNnSc3	perfektum
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
řečtině	řečtina	k1gFnSc6	řečtina
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
anglickému	anglický	k2eAgInSc3d1	anglický
předpřítomnému	předpřítomný	k2eAgInSc3d1	předpřítomný
času	čas	k1gInSc3	čas
(	(	kIx(	(
<g/>
present	present	k1gMnSc1	present
perfect	perfect	k1gMnSc1	perfect
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
utúlien	utúlien	k1gInSc1	utúlien
"	"	kIx"	"
<g/>
přišel	přijít	k5eAaPmAgInS	přijít
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
jsem	být	k5eAaImIp1nS	být
tady	tady	k6eAd1	tady
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
-	-	kIx~	-
Označuje	označovat	k5eAaImIp3nS	označovat
děj	děj	k1gInSc4	děj
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českému	český	k2eAgInSc3d1	český
minulému	minulý	k2eAgInSc3d1	minulý
času	čas	k1gInSc3	čas
<g/>
.	.	kIx.	.
<g/>
túlen	túlen	k2eAgInSc1d1	túlen
/	/	kIx~	/
tullen	tullen	k1gInSc1	tullen
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
z	z	k7c2	z
tvarů	tvar	k1gInPc2	tvar
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
správně	správně	k6eAd1	správně
<g/>
"	"	kIx"	"
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určit	určit	k5eAaPmF	určit
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
přišel	přijít	k5eAaPmAgMnS	přijít
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přicházel	přicházet	k5eAaImAgMnS	přicházet
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
<g/>
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
-	-	kIx~	-
Označuje	označovat	k5eAaImIp3nS	označovat
děj	děj	k1gInSc4	děj
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českému	český	k2eAgInSc3d1	český
budoucímu	budoucí	k2eAgInSc3d1	budoucí
času	čas	k1gInSc3	čas
<g/>
.	.	kIx.	.
<g/>
tuluvan	tuluvan	k1gMnSc1	tuluvan
-	-	kIx~	-
"	"	kIx"	"
<g/>
přijdu	přijít	k5eAaPmIp1nS	přijít
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgInPc1d1	další
slovesné	slovesný	k2eAgInPc1d1	slovesný
tvary	tvar	k1gInPc1	tvar
====	====	k?	====
</s>
</p>
<p>
<s>
infinitivtulë	infinitivtulë	k?	infinitivtulë
"	"	kIx"	"
<g/>
přijít	přijít	k5eAaPmF	přijít
<g/>
"	"	kIx"	"
<g/>
gerundium	gerundium	k1gNnSc4	gerundium
-	-	kIx~	-
"	"	kIx"	"
<g/>
Skloňovatelný	skloňovatelný	k2eAgInSc1d1	skloňovatelný
infinitiv	infinitiv	k1gInSc1	infinitiv
<g/>
"	"	kIx"	"
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
slovesné	slovesný	k2eAgNnSc1d1	slovesné
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
latinskému	latinský	k2eAgNnSc3d1	latinské
a	a	k8xC	a
anglickému	anglický	k2eAgNnSc3d1	anglické
gerundiu	gerundium	k1gNnSc3	gerundium
<g/>
.	.	kIx.	.
<g/>
tulië	tulië	k?	tulië
"	"	kIx"	"
<g/>
přijít	přijít	k5eAaPmF	přijít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přicházení	přicházení	k1gNnSc2	přicházení
<g/>
"	"	kIx"	"
<g/>
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
infinitiv	infinitiv	k1gInSc1	infinitiv
-	-	kIx~	-
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
místo	místo	k7c2	místo
běžného	běžný	k2eAgInSc2d1	běžný
infinitivutulita	infinitivutulita	k1gFnSc1	infinitivutulita
"	"	kIx"	"
<g/>
přijít	přijít	k5eAaPmF	přijít
<g/>
"	"	kIx"	"
<g/>
příčestí	příčestí	k1gNnSc4	příčestí
minulé	minulý	k2eAgFnSc2d1	minulá
<g/>
/	/	kIx~	/
<g/>
trpné	trpný	k2eAgFnSc2d1	trpná
-	-	kIx~	-
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českému	český	k2eAgNnSc3d1	české
trpnému	trpný	k2eAgNnSc3d1	trpné
příčestí	příčestí	k1gNnSc3	příčestí
<g/>
,	,	kIx,	,
anglickému	anglický	k2eAgNnSc3d1	anglické
a	a	k8xC	a
latinskému	latinský	k2eAgNnSc3d1	latinské
participiu	participium	k1gNnSc3	participium
perfekta	perfektum	k1gNnSc2	perfektum
pasiva	pasivum	k1gNnSc2	pasivum
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nepřechodných	přechodný	k2eNgNnPc2d1	nepřechodné
sloves	sloveso	k1gNnPc2	sloveso
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
činný	činný	k2eAgInSc1d1	činný
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
<g/>
tulda	tulda	k1gMnSc1	tulda
"	"	kIx"	"
<g/>
přišlý	přišlý	k2eAgMnSc1d1	přišlý
<g/>
"	"	kIx"	"
<g/>
příčestí	příčestí	k1gNnSc1	příčestí
přítomné	přítomný	k2eAgNnSc1d1	přítomné
<g/>
/	/	kIx~	/
<g/>
činné	činný	k2eAgNnSc4d1	činné
-	-	kIx~	-
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
českému	český	k2eAgInSc3d1	český
přechodníku	přechodník	k1gInSc3	přechodník
přítomnému	přítomný	k2eAgNnSc3d1	přítomné
nebo	nebo	k8xC	nebo
přídavnému	přídavný	k2eAgNnSc3d1	přídavné
jménu	jméno	k1gNnSc3	jméno
slovesnému	slovesný	k2eAgNnSc3d1	slovesné
<g/>
,	,	kIx,	,
latinskému	latinský	k2eAgNnSc3d1	latinské
participiu	participium	k1gNnSc3	participium
prézentu	prézens	k1gInSc2	prézens
aktiva	aktivum	k1gNnSc2	aktivum
<g/>
,	,	kIx,	,
anglickému	anglický	k2eAgNnSc3d1	anglické
přídavnému	přídavný	k2eAgNnSc3d1	přídavné
jménu	jméno	k1gNnSc3	jméno
slovésnému	slovésný	k2eAgInSc3d1	slovésný
na	na	k7c4	na
"	"	kIx"	"
<g/>
-ing	ng	k1gInSc4	-ing
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
túlala	túlala	k1gFnSc1	túlala
"	"	kIx"	"
<g/>
přicházející	přicházející	k2eAgMnPc1d1	přicházející
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přicházeje	přicházet	k5eAaImSgMnS	přicházet
<g/>
"	"	kIx"	"
<g/>
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
slovesnétúlale	slovesnétúlale	k6eAd1	slovesnétúlale
"	"	kIx"	"
<g/>
přicházení	přicházený	k2eAgMnPc1d1	přicházený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
příchod	příchod	k1gInSc1	příchod
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
====	====	k?	====
Čísla	číslo	k1gNnPc1	číslo
====	====	k?	====
</s>
</p>
<p>
<s>
singulár	singulár	k1gInSc1	singulár
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
plurál	plurál	k1gInSc1	plurál
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
partitivní	partitivní	k2eAgInSc1d1	partitivní
plurál	plurál	k1gInSc1	plurál
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
části	část	k1gFnSc2	část
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
(	(	kIx(	(
<g/>
několik	několik	k4yIc1	několik
labutí	labuť	k1gFnPc2	labuť
z	z	k7c2	z
labutího	labutí	k2eAgNnSc2d1	labutí
hejna	hejno	k1gNnSc2	hejno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
duál	duál	k1gInSc1	duál
<g/>
,	,	kIx,	,
přirozený	přirozený	k2eAgMnSc1d1	přirozený
pár	pár	k4xCyI	pár
(	(	kIx(	(
<g/>
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
nohou	noha	k1gFnPc6	noha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Písma	písmo	k1gNnSc2	písmo
==	==	k?	==
</s>
</p>
<p>
<s>
Quenya	Quenya	k6eAd1	Quenya
používá	používat	k5eAaImIp3nS	používat
tengwar	tengwar	k1gInSc1	tengwar
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
také	také	k9	také
cirth	cirth	k1gInSc1	cirth
<g/>
)	)	kIx)	)
s	s	k7c7	s
tehtar	tehtar	k1gInSc1	tehtar
za	za	k7c7	za
souhláskami	souhláska	k1gFnPc7	souhláska
s	s	k7c7	s
čistě	čistě	k6eAd1	čistě
fonetickým	fonetický	k2eAgInSc7d1	fonetický
pravopisem	pravopis	k1gInSc7	pravopis
a	a	k8xC	a
latinku	latinka	k1gFnSc4	latinka
s	s	k7c7	s
dvojí	dvojí	k4xRgFnSc7	dvojí
podobou	podoba	k1gFnSc7	podoba
zápisu	zápis	k1gInSc2	zápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
zápis	zápis	k1gInSc1	zápis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
(	(	kIx(	(
<g/>
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
variacemi	variace	k1gFnPc7	variace
<g/>
)	)	kIx)	)
používal	používat	k5eAaImAgInS	používat
Tolkien	Tolkien	k1gInSc1	Tolkien
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fonetický	fonetický	k2eAgInSc1d1	fonetický
zápis	zápis	k1gInSc1	zápis
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
téměř	téměř	k6eAd1	téměř
zápisu	zápis	k1gInSc2	zápis
v	v	k7c4	v
tengwar	tengwar	k1gInSc4	tengwar
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
též	též	k9	též
přepis	přepis	k1gInSc4	přepis
dle	dle	k7c2	dle
Calwen	Calwna	k1gFnPc2	Calwna
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zápis	zápis	k1gInSc4	zápis
přizpůsobený	přizpůsobený	k2eAgInSc4d1	přizpůsobený
Čechům	Čech	k1gMnPc3	Čech
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Quenijština	Quenijština	k1gFnSc1	Quenijština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Angrenost	Angrenost	k1gFnSc1	Angrenost
–	–	k?	–
Česko-quenijský	Českouenijský	k2eAgInSc1d1	Česko-quenijský
a	a	k8xC	a
quenijsko-český	quenijsko-český	k2eAgInSc1d1	quenijsko-český
on-line	onin	k1gInSc5	on-lin
slovník	slovník	k1gInSc4	slovník
</s>
</p>
<p>
<s>
Skromný	skromný	k2eAgInSc4d1	skromný
popis	popis	k1gInSc4	popis
a	a	k8xC	a
učebnice	učebnice	k1gFnPc4	učebnice
</s>
</p>
<p>
<s>
Quenijský	Quenijský	k2eAgInSc1d1	Quenijský
etymologický	etymologický	k2eAgInSc1d1	etymologický
slovník	slovník	k1gInSc1	slovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Evish	Evish	k1gInSc1	Evish
Liguistic	Liguistice	k1gFnPc2	Liguistice
Fellowship	Fellowship	k1gInSc4	Fellowship
–	–	k?	–
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
časopisy	časopis	k1gInPc4	časopis
Parma	Parma	k1gFnSc1	Parma
Eldalamberon	Eldalamberon	k1gMnSc1	Eldalamberon
a	a	k8xC	a
Vinyar	Vinyar	k1gMnSc1	Vinyar
Tengwar	Tengwar	k1gMnSc1	Tengwar
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
vycházejí	vycházet	k5eAaImIp3nP	vycházet
edice	edice	k1gFnPc1	edice
doposud	doposud	k6eAd1	doposud
nepublikovaných	publikovaný	k2eNgInPc2d1	nepublikovaný
Tolkienových	Tolkienův	k2eAgInPc2d1	Tolkienův
lingvistických	lingvistický	k2eAgInPc2d1	lingvistický
materiálů	materiál	k1gInPc2	materiál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ardalambion	Ardalambion	k1gInSc1	Ardalambion
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
relativně	relativně	k6eAd1	relativně
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
stránek	stránka	k1gFnPc2	stránka
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
Tolkienovým	Tolkienův	k2eAgInPc3d1	Tolkienův
jazykům	jazyk	k1gInPc3	jazyk
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
uvedené	uvedený	k2eAgInPc1d1	uvedený
odkazy	odkaz	k1gInPc1	odkaz
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
vesměs	vesměs	k6eAd1	vesměs
relativně	relativně	k6eAd1	relativně
spolehlivými	spolehlivý	k2eAgInPc7d1	spolehlivý
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
