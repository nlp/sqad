<s>
Quenya	Quenya	k1gFnSc1	Quenya
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
taky	taky	k9	taky
quenijština	quenijština	k1gFnSc1	quenijština
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
Vznešená	vznešený	k2eAgFnSc1d1	vznešená
elfština	elfština	k1gFnSc1	elfština
(	(	kIx(	(
<g/>
čtěte	číst	k5eAaImRp2nP	číst
kwenja	kwenja	k6eAd1	kwenja
<g/>
,	,	kIx,	,
kwenyjština	kwenyjština	k1gFnSc1	kwenyjština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
jazyk	jazyk	k1gInSc1	jazyk
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
Hobit	hobit	k1gMnSc1	hobit
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
-	-	kIx~	-
rodná	rodný	k2eAgFnSc1d1	rodná
řeč	řeč	k1gFnSc1	řeč
Vznešených	vznešený	k2eAgMnPc2d1	vznešený
elfů	elf	k1gMnPc2	elf
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Vanyar	Vanyara	k1gFnPc2	Vanyara
a	a	k8xC	a
Noldor	Noldora	k1gFnPc2	Noldora
<g/>
.	.	kIx.	.
</s>
