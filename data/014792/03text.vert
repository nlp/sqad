<s>
Padělek	padělek	k1gInSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Podvrh	podvrh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
vesnici	vesnice	k1gFnSc6
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
<g/>
,	,	kIx,
části	část	k1gFnSc6
města	město	k1gNnSc2
Samobor	Samobora	k1gFnPc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Podvrh	podvrh	k1gInSc1
(	(	kIx(
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Padělek	padělek	k1gInSc1
<g/>
,	,	kIx,
falzifikát	falzifikát	k1gInSc1
nebo	nebo	k8xC
falzum	falzum	k1gNnSc1
je	být	k5eAaImIp3nS
předmět	předmět	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
za	za	k7c4
jiný	jiný	k2eAgInSc4d1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
cennější	cenný	k2eAgInSc4d2
předmět	předmět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
původce	původce	k1gMnSc4
je	být	k5eAaImIp3nS
padělatel	padělatel	k1gMnSc1
nebo	nebo	k8xC
falzátor	falzátor	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
činnost	činnost	k1gFnSc1
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
padělání	padělání	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padělky	padělek	k1gInPc7
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
počínaje	počínaje	k7c7
platidly	platidlo	k1gNnPc7
(	(	kIx(
<g/>
penězokazectví	penězokazectví	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
listinami	listina	k1gFnPc7
a	a	k8xC
dokumenty	dokument	k1gInPc7
<g/>
,	,	kIx,
přes	přes	k7c4
umělecké	umělecký	k2eAgInPc4d1
předměty	předmět	k1gInPc4
<g/>
,	,	kIx,
starožitnost	starožitnost	k1gFnSc4
i	i	k8xC
až	až	k9
po	po	k7c4
průmyslové	průmyslový	k2eAgNnSc4d1
zboží	zboží	k1gNnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
značkové	značkový	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padělání	padělání	k1gNnSc1
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
výtvarných	výtvarný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
veřejných	veřejný	k2eAgFnPc2d1
listin	listina	k1gFnPc2
a	a	k8xC
ochranných	ochranný	k2eAgFnPc2d1
značek	značka	k1gFnPc2
jsou	být	k5eAaImIp3nP
samostatné	samostatný	k2eAgInPc1d1
trestné	trestný	k2eAgInPc1d1
činy	čin	k1gInPc1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInPc1d1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
trestat	trestat	k5eAaImF
jako	jako	k9
podvod	podvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
komerční	komerční	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
způsobené	způsobený	k2eAgFnPc1d1
paděláním	padělání	k1gNnSc7
v	v	k7c6
současnosti	současnost	k1gFnSc6
dosahují	dosahovat	k5eAaImIp3nP
asi	asi	k9
500	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Padělání	padělání	k1gNnSc1
peněz	peníze	k1gInPc2
</s>
<s>
Padělání	padělání	k1gNnSc1
peněz	peníze	k1gInPc2
(	(	kIx(
<g/>
neboli	neboli	k8xC
penězokazectví	penězokazectví	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nezákonné	zákonný	k2eNgNnSc4d1
reprodukování	reprodukování	k1gNnSc4
platidel	platidlo	k1gNnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
reprodukce	reprodukce	k1gFnPc4
vydávány	vydáván	k2eAgFnPc4d1
za	za	k7c4
pravé	pravý	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padělání	padělání	k1gNnSc1
peněz	peníze	k1gInPc2
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
v	v	k7c6
každém	každý	k3xTgInSc6
státě	stát	k1gInSc6
trestným	trestný	k2eAgInSc7d1
činem	čin	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
je	být	k5eAaImIp3nS
dokonce	dokonce	k9
trestáno	trestán	k2eAgNnSc1d1
smrtí	smrtit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
vydávat	vydávat	k5eAaPmF,k5eAaImF
peníze	peníz	k1gInPc4
pouze	pouze	k6eAd1
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
emitentem	emitent	k1gMnSc7
bankovek	bankovka	k1gFnPc2
a	a	k8xC
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
také	také	k9
sleduje	sledovat	k5eAaImIp3nS
výskyt	výskyt	k1gInSc1
padělků	padělek	k1gInPc2
mincí	mince	k1gFnPc2
a	a	k8xC
bankovek	bankovka	k1gFnPc2
v	v	k7c6
oběhu	oběh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
případů	případ	k1gInPc2
padělání	padělání	k1gNnSc2
bankovek	bankovka	k1gFnPc2
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
snížen	snížit	k5eAaPmNgInS
ochrannými	ochranný	k2eAgInPc7d1
prvky	prvek	k1gInPc7
a	a	k8xC
náročnějšími	náročný	k2eAgFnPc7d2
technikami	technika	k1gFnPc7
tisku	tisk	k1gInSc2
originálních	originální	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Padělky	padělek	k1gInPc1
bankovek	bankovka	k1gFnPc2
se	se	k3xPyFc4
hodnotí	hodnotit	k5eAaImIp3nS
podle	podle	k7c2
pětibodové	pětibodový	k2eAgFnSc2d1
stupnice	stupnice	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
neumělý	umělý	k2eNgInSc1d1
padělek	padělek	k1gInSc1
–	–	k?
nevydařená	vydařený	k2eNgFnSc1d1
imitace	imitace	k1gFnSc1
<g/>
,	,	kIx,
ochranné	ochranný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
zcela	zcela	k6eAd1
chybí	chybit	k5eAaPmIp3nP,k5eAaImIp3nP
<g/>
,	,	kIx,
odlišné	odlišný	k2eAgNnSc4d1
barevné	barevný	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
padělek	padělek	k1gInSc1
rozezná	rozeznat	k5eAaPmIp3nS
i	i	k9
laik	laik	k1gMnSc1
</s>
<s>
méně	málo	k6eAd2
zdařilý	zdařilý	k2eAgInSc1d1
padělek	padělek	k1gInSc1
–	–	k?
většinou	většinou	k6eAd1
je	být	k5eAaImIp3nS
použita	použít	k5eAaPmNgFnS
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
tisková	tiskový	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
ochranné	ochranný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
;	;	kIx,
padělek	padělek	k1gInSc1
rozezná	rozeznat	k5eAaPmIp3nS
i	i	k9
pozorný	pozorný	k2eAgMnSc1d1
laik	laik	k1gMnSc1
</s>
<s>
zdařilý	zdařilý	k2eAgInSc1d1
padělek	padělek	k1gInSc1
–	–	k?
je	být	k5eAaImIp3nS
vytištěn	vytisknout	k5eAaPmNgInS
pomocí	pomoc	k1gFnSc7
jiných	jiný	k2eAgFnPc2d1
tiskových	tiskový	k2eAgFnPc2d1
technik	technika	k1gFnPc2
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
napodobeny	napodobit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
ochranné	ochranný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
</s>
<s>
nebezpečný	bezpečný	k2eNgInSc1d1
padělek	padělek	k1gInSc1
–	–	k?
nejsou	být	k5eNaImIp3nP
použity	použit	k2eAgFnPc1d1
všechny	všechen	k3xTgFnPc1
originální	originální	k2eAgFnPc1d1
tiskové	tiskový	k2eAgFnPc1d1
techniky	technika	k1gFnPc1
<g/>
,	,	kIx,
jednotlivé	jednotlivý	k2eAgInPc1d1
ochranné	ochranný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
jsou	být	k5eAaImIp3nP
dobře	dobře	k6eAd1
napodobené	napodobený	k2eAgFnPc1d1
</s>
<s>
velmi	velmi	k6eAd1
nebezpečný	bezpečný	k2eNgInSc1d1
padělek	padělek	k1gInSc1
–	–	k?
téměř	téměř	k6eAd1
se	se	k3xPyFc4
neliší	lišit	k5eNaImIp3nP
od	od	k7c2
originálu	originál	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
použit	použít	k5eAaPmNgInS
dokonalý	dokonalý	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
ochranné	ochranný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
jsou	být	k5eAaImIp3nP
dobře	dobře	k6eAd1
napodobené	napodobený	k2eAgFnPc1d1
</s>
<s>
Padělaná	padělaný	k2eAgFnSc1d1
mince	mince	k1gFnSc1
stejného	stejný	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
jako	jako	k8xC,k8xS
originál	originál	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
méně	málo	k6eAd2
hodnotného	hodnotný	k2eAgInSc2d1
kovu	kov	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
odražek	odražek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Padělání	padělání	k1gNnSc1
peněz	peníze	k1gInPc2
a	a	k8xC
české	český	k2eAgInPc1d1
zákony	zákon	k1gInPc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
jedná	jednat	k5eAaImIp3nS
o	o	k7c6
padělání	padělání	k1gNnSc6
peněz	peníze	k1gInPc2
v	v	k7c6
paragrafech	paragraf	k1gInPc6
233	#num#	k4
až	až	k9
238	#num#	k4
a	a	k8xC
zakazuje	zakazovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
samo	sám	k3xTgNnSc1
padělání	padělání	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
držení	držení	k1gNnSc1
a	a	k8xC
používání	používání	k1gNnSc1
padělaných	padělaný	k2eAgInPc2d1
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
držení	držení	k1gNnSc1
padělatelského	padělatelský	k2eAgNnSc2d1
náčiní	náčiní	k1gNnSc2
a	a	k8xC
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
nejen	nejen	k6eAd1
vzhledem	vzhledem	k7c3
k	k	k7c3
české	český	k2eAgFnSc3d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
jakékoli	jakýkoli	k3yIgFnSc3
jiné	jiný	k2eAgFnSc3d1
měně	měna	k1gFnSc3
<g/>
,	,	kIx,
k	k	k7c3
platebním	platební	k2eAgFnPc3d1
kartám	karta	k1gFnPc3
a	a	k8xC
šekům	šek	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
významu	význam	k1gInSc6
padělání	padělání	k1gNnSc2
svědčí	svědčit	k5eAaImIp3nS
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
těmito	tento	k3xDgInPc7
paragrafy	paragraf	k1gInPc7
začíná	začínat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
Hlava	hlava	k1gFnSc1
VI	VI	kA
o	o	k7c6
hospodářské	hospodářský	k2eAgFnSc6d1
trestné	trestný	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
a	a	k8xC
že	že	k8xS
trestná	trestný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
už	už	k6eAd1
i	i	k8xC
příprava	příprava	k1gFnSc1
k	k	k7c3
němu	on	k3xPp3gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestné	trestný	k2eAgInPc1d1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k8xC
placení	placení	k1gNnSc1
padělanými	padělaný	k2eAgInPc7d1
penězi	peníze	k1gInPc7
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
před	před	k7c7
tím	ten	k3xDgNnSc7
přijal	přijmout	k5eAaPmAgMnS
jako	jako	k9
pravé	pravý	k2eAgNnSc4d1
(	(	kIx(
<g/>
§	§	k?
235	#num#	k4
tr	tr	k?
<g/>
.	.	kIx.
zák	zák	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
dozvěděl	dozvědět	k5eAaPmAgMnS
<g/>
-li	-li	k?
se	se	k3xPyFc4
poté	poté	k6eAd1
o	o	k7c4
jejich	jejich	k3xOp3gNnSc4
padělání	padělání	k1gNnSc4
a	a	k8xC
přesto	přesto	k8xC
se	se	k3xPyFc4
je	on	k3xPp3gFnPc4
snažil	snažit	k5eAaImAgMnS
udat	udat	k5eAaPmF
jako	jako	k9
pravé	pravý	k2eAgMnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Padělání	padělání	k1gNnSc1
a	a	k8xC
pozměňování	pozměňování	k1gNnSc1
veřejných	veřejný	k2eAgFnPc2d1
listin	listina	k1gFnPc2
</s>
<s>
Také	také	k9
padělání	padělání	k1gNnSc1
listin	listina	k1gFnPc2
má	mít	k5eAaImIp3nS
dlouhou	dlouhý	k2eAgFnSc4d1
historii	historie	k1gFnSc4
a	a	k8xC
ve	v	k7c6
starších	starý	k2eAgFnPc6d2
dobách	doba	k1gFnPc6
sloužilo	sloužit	k5eAaImAgNnS
hlavně	hlavně	k9
k	k	k7c3
dokládání	dokládání	k1gNnSc3
majetkových	majetkový	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestní	trestní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
výslovně	výslovně	k6eAd1
zmiňuje	zmiňovat	k5eAaImIp3nS
celní	celní	k2eAgInPc4d1
a	a	k8xC
daňové	daňový	k2eAgInPc4d1
nálepky	nálepek	k1gInPc4
či	či	k8xC
pásky	pásek	k1gInPc4
(	(	kIx(
<g/>
§	§	k?
245	#num#	k4
tr	tr	k?
<g/>
.	.	kIx.
zák	zák	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
včetně	včetně	k7c2
osobních	osobní	k2eAgInPc2d1
dokladů	doklad	k1gInPc2
(	(	kIx(
<g/>
§	§	k?
348	#num#	k4
tr	tr	k?
<g/>
.	.	kIx.
zák	zák	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úřední	úřední	k2eAgNnPc1d1
razítka	razítko	k1gNnPc1
<g/>
,	,	kIx,
lékařské	lékařský	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
a	a	k8xC
posudky	posudek	k1gInPc1
(	(	kIx(
<g/>
§	§	k?
350	#num#	k4
tr	tr	k?
<g/>
.	.	kIx.
zák	zák	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
poštovní	poštovní	k2eAgFnPc4d1
nebo	nebo	k8xC
kolkové	kolkový	k2eAgFnPc4d1
známky	známka	k1gFnPc4
(	(	kIx(
<g/>
§	§	k?
246	#num#	k4
tr	tr	k?
<g/>
.	.	kIx.
zák	zák	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobnou	podobný	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
má	mít	k5eAaImIp3nS
padělání	padělání	k1gNnSc1
puncovních	puncovní	k2eAgFnPc2d1
značek	značka	k1gFnPc2
na	na	k7c6
předmětech	předmět	k1gInPc6
z	z	k7c2
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Padělání	padělání	k1gNnSc1
listin	listina	k1gFnPc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc1
padělají	padělat	k5eAaBmIp3nP
Privilegium	privilegium	k1gNnSc4
maius	maius	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
Oldřich	Oldřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
Rožmberka	Rožmberk	k1gInSc2
takto	takto	k6eAd1
získává	získávat	k5eAaImIp3nS
větší	veliký	k2eAgFnSc4d2
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Padělání	padělání	k1gNnSc1
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
</s>
<s>
Padělkem	padělek	k1gInSc7
je	být	k5eAaImIp3nS
například	například	k6eAd1
obraz	obraz	k1gInSc1
namalovaný	namalovaný	k2eAgInSc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vypadal	vypadat	k5eAaImAgInS,k5eAaPmAgInS
jako	jako	k8xC,k8xS
dílo	dílo	k1gNnSc1
známého	známý	k2eAgMnSc2d1
mistra	mistr	k1gMnSc2
nebo	nebo	k8xC
dokument	dokument	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
předstírá	předstírat	k5eAaImIp3nS
jiného	jiný	k2eAgMnSc4d1
autora	autor	k1gMnSc4
nebo	nebo	k8xC
stáří	stáří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padělatel	padělatel	k1gMnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
v	v	k7c6
jistém	jistý	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
protikladem	protiklad	k1gInSc7
plagiátora	plagiátor	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
cizí	cizit	k5eAaImIp3nS
dílo	dílo	k1gNnSc1
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
padělatel	padělatel	k1gMnSc1
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
vlastní	vlastní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
za	za	k7c4
cizí	cizí	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jakéhosi	jakýsi	k3yIgInSc2
krále	král	k1gMnSc4
padělatelů	padělatel	k1gMnPc2
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
nizozemského	nizozemský	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
van	vana	k1gFnPc2
Meegerena	Meegeren	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
napodoboval	napodobovat	k5eAaImAgMnS
obrazy	obraz	k1gInPc4
Vermeera	Vermeero	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
s	s	k7c7
takovým	takový	k3xDgInSc7
úspěchem	úspěch	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nejen	nejen	k6eAd1
prodával	prodávat	k5eAaImAgMnS
sběratelům	sběratel	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
oklamal	oklamat	k5eAaPmAgMnS
i	i	k9
několik	několik	k4yIc4
znalců	znalec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
by	by	kYmCp3nS
výtvarné	výtvarný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
napodobil	napodobit	k5eAaPmAgInS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
dílo	dílo	k1gNnSc4
jiného	jiný	k2eAgMnSc2d1
autora	autor	k1gMnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
podle	podle	k7c2
§	§	k?
271	#num#	k4
trestního	trestní	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
potrestán	potrestat	k5eAaPmNgMnS
až	až	k6eAd1
tříletým	tříletý	k2eAgNnSc7d1
vězením	vězení	k1gNnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
tím	ten	k3xDgNnSc7
získal	získat	k5eAaPmAgInS
velký	velký	k2eAgInSc4d1
prospěch	prospěch	k1gInSc4
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
deseti	deset	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
české	český	k2eAgFnSc2d1
historie	historie	k1gFnSc2
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgInPc1d1
i	i	k8xC
padělky	padělek	k1gInPc1
literární	literární	k2eAgInPc1d1
<g/>
,	,	kIx,
například	například	k6eAd1
falešné	falešný	k2eAgInPc1d1
Rukopisy	rukopis	k1gInPc1
královédvorský	královédvorský	k2eAgInSc1d1
a	a	k8xC
zelenohorský	zelenohorský	k2eAgInSc1d1
nebo	nebo	k8xC
Velesova	Velesův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
dokázat	dokázat	k5eAaPmF
padělání	padělání	k1gNnSc2
Písně	píseň	k1gFnSc2
Vyšehradské	vyšehradský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Padělání	padělání	k1gNnSc1
značkového	značkový	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
</s>
<s>
Dalším	další	k2eAgMnSc7d1
velmi	velmi	k6eAd1
rozšířeným	rozšířený	k2eAgInSc7d1
druhem	druh	k1gInSc7
padělků	padělek	k1gInPc2
jsou	být	k5eAaImIp3nP
kopie	kopie	k1gFnPc4
průmyslového	průmyslový	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
známých	známý	k2eAgFnPc2d1
značek	značka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
působí	působit	k5eAaImIp3nP
známým	známý	k2eAgFnPc3d1
firmám	firma	k1gFnPc3
obrovské	obrovský	k2eAgFnSc2d1
škody	škoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
(	(	kIx(
<g/>
§	§	k?
268	#num#	k4
<g/>
)	)	kIx)
výslovně	výslovně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
neoprávněné	oprávněný	k2eNgNnSc1d1
užití	užití	k1gNnSc1
cizí	cizí	k2eAgFnSc2d1
ochranné	ochranný	k2eAgFnSc2d1
známky	známka	k1gFnSc2
na	na	k7c6
zboží	zboží	k1gNnSc6
či	či	k8xC
službě	služba	k1gFnSc6
nebo	nebo	k8xC
neoprávněné	oprávněný	k2eNgNnSc1d1
užívání	užívání	k1gNnSc1
cizí	cizí	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
a	a	k8xC
firmy	firma	k1gFnSc2
s	s	k7c7
ní	on	k3xPp3gFnSc7
zaměnitelné	zaměnitelný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Padělky	padělek	k1gInPc1
literárních	literární	k2eAgFnPc2d1
památek	památka	k1gFnPc2
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
padělaly	padělat	k5eAaBmAgFnP
i	i	k9
literární	literární	k2eAgFnPc1d1
památky	památka	k1gFnPc1
ve	v	k7c6
snaze	snaha	k1gFnSc6
dokázat	dokázat	k5eAaPmF
starobylost	starobylost	k1gFnSc4
národní	národní	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
je	být	k5eAaImIp3nS
v	v	k7c6
různé	různý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
zpochybňováno	zpochybňovat	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
falza	falzum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známý	k2eAgInPc1d1
případy	případ	k1gInPc1
jsou	být	k5eAaImIp3nP
skotsko-gaelské	skotsko-gaelský	k2eAgInPc1d1
Ossianovy	Ossianův	k2eAgInPc1d1
zpěvy	zpěv	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
Spor	spora	k1gFnPc2
o	o	k7c4
rukopisy	rukopis	k1gInPc4
královédvorský	královédvorský	k2eAgInSc1d1
a	a	k8xC
zelenohorský	zelenohorský	k2eAgInSc1d1
nebo	nebo	k8xC
ruské	ruský	k2eAgNnSc1d1
Slovo	slovo	k1gNnSc1
o	o	k7c6
pluku	pluk	k1gInSc6
Igorově	Igorův	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Naím	Naím	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
globalizace	globalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Vyšehrad	Vyšehrad	k1gInSc1
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
107	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠÁMAL	Šámal	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
§	§	k?
140	#num#	k4
až	až	k9
421	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komentář	komentář	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
C.	C.	kA
H.	H.	kA
Beck	Beck	k1gMnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7400	#num#	k4
<g/>
-	-	kIx~
<g/>
428	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2377	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Falšování	falšování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
8	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1039	#num#	k4
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgNnSc4d1
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc4
Padělání	padělání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
8	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
831	#num#	k4
</s>
<s>
POLSKOJ	POLSKOJ	kA
<g/>
,	,	kIx,
Georgij	Georgij	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
penězokazeckých	penězokazecký	k2eAgFnPc2d1
dílen	dílna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Lidové	lidový	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
autor	autor	k1gMnSc1
</s>
<s>
copyright	copyright	k1gInSc1
</s>
<s>
copyleft	copyleft	k1gMnSc1
</s>
<s>
dílo	dílo	k1gNnSc1
</s>
<s>
kompilát	kompilát	k1gInSc1
</s>
<s>
kopie	kopie	k1gFnPc1
</s>
<s>
licence	licence	k1gFnSc1
</s>
<s>
plagiát	plagiát	k1gInSc1
</s>
<s>
imitace	imitace	k1gFnSc1
</s>
<s>
fotomanipulace	fotomanipulace	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
padělání	padělání	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Padělek	padělek	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
padělek	padělek	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Falšování	falšování	k1gNnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Podvrženiny	podvrženina	k1gFnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Stejnopis	stejnopis	k1gInSc1
částky	částka	k1gFnSc2
Sb	sb	kA
<g/>
.	.	kIx.
č.	č.	k?
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
byl	být	k5eAaImAgInS
publikován	publikovat	k5eAaBmNgInS
trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4016247-3	4016247-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85033440	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85033440	#num#	k4
</s>
