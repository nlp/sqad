<s>
Foster	Foster	k1gInSc1	Foster
the	the	k?	the
People	People	k1gFnSc1	People
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
<g/>
,	,	kIx,	,
indie	indie	k1gFnSc1	indie
popová	popový	k2eAgFnSc1d1	popová
kapela	kapela	k1gFnSc1	kapela
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
a	a	k8xC	a
fungující	fungující	k2eAgInPc1d1	fungující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
-	-	kIx~	-
frontman	frontman	k1gMnSc1	frontman
Mark	Mark	k1gMnSc1	Mark
Foster	Foster	k1gMnSc1	Foster
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Cubbie	Cubbie	k1gFnSc1	Cubbie
Fink	Finka	k1gFnPc2	Finka
a	a	k8xC	a
Marc	Marc	k1gFnSc4	Marc
Pontius	Pontius	k1gMnSc1	Pontius
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvuku	zvuk	k1gInSc6	zvuk
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
několik	několik	k4yIc1	několik
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jak	jak	k8xS	jak
prvky	prvek	k1gInPc1	prvek
popu	pop	k1gInSc2	pop
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
indie	indie	k1gFnSc2	indie
<g/>
,	,	kIx,	,
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
Foster	Foster	k1gMnSc1	Foster
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
po	po	k7c6	po
strávení	strávení	k1gNnSc6	strávení
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
jako	jako	k8xC	jako
muzikant	muzikant	k1gMnSc1	muzikant
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
reklamních	reklamní	k2eAgInPc2d1	reklamní
jingelů	jingel	k1gInPc2	jingel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
pilotního	pilotní	k2eAgInSc2d1	pilotní
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Pumped	Pumped	k1gInSc1	Pumped
Up	Up	k1gFnSc2	Up
Kicks	Kicksa	k1gFnPc2	Kicksa
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
kapela	kapela	k1gFnSc1	kapela
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
Startime	Startim	k1gInSc5	Startim
International	International	k1gFnPc3	International
a	a	k8xC	a
díky	díky	k7c3	díky
vystupování	vystupování	k1gNnSc3	vystupování
po	po	k7c6	po
menších	malý	k2eAgInPc6d2	menší
klubech	klub	k1gInPc6	klub
a	a	k8xC	a
festivalech	festival	k1gInPc6	festival
Coachella	Coachello	k1gNnSc2	Coachello
a	a	k8xC	a
South	South	k1gMnSc1	South
By	by	k9	by
Southwest	Southwest	k1gMnSc1	Southwest
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
budovat	budovat	k5eAaImF	budovat
fanouškovskou	fanouškovský	k2eAgFnSc4d1	fanouškovská
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
Torches	Torchesa	k1gFnPc2	Torchesa
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Pumped	Pumped	k1gInSc1	Pumped
Up	Up	k1gMnSc1	Up
Kicks	Kicks	k1gInSc1	Kicks
<g/>
"	"	kIx"	"
stal	stát	k5eAaPmAgInS	stát
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
získala	získat	k5eAaPmAgFnS	získat
rovněž	rovněž	k9	rovněž
dvě	dva	k4xCgFnPc1	dva
nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Grammy	Gramma	k1gFnPc4	Gramma
za	za	k7c4	za
Torches	Torches	k1gInSc4	Torches
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pumped	Pumped	k1gInSc1	Pumped
Up	Up	k1gFnSc2	Up
Kicks	Kicksa	k1gFnPc2	Kicksa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gInSc2	on
však	však	k9	však
již	již	k6eAd1	již
začaly	začít	k5eAaPmAgFnP	začít
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Torches	Torches	k1gInSc1	Torches
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Supermodel	Supermodlo	k1gNnPc2	Supermodlo
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Foster	Foster	k1gMnSc1	Foster
the	the	k?	the
People	People	k1gMnSc1	People
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Broken	Broken	k1gInSc1	Broken
Jaw	jawa	k1gFnPc2	jawa
<g/>
/	/	kIx~	/
<g/>
Ruby	rub	k1gInPc1	rub
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Pumped	Pumped	k1gInSc1	Pumped
Up	Up	k1gFnSc2	Up
Kicks	Kicksa	k1gFnPc2	Kicksa
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Helena	Helena	k1gFnSc1	Helena
Beat	beat	k1gInSc1	beat
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Call	Call	k1gMnSc1	Call
It	It	k1gMnSc1	It
What	What	k1gMnSc1	What
You	You	k1gMnSc1	You
Want	Want	k1gMnSc1	Want
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
(	(	kIx(	(
<g/>
Color	Color	k1gInSc1	Color
on	on	k3xPp3gInSc1	on
the	the	k?	the
Walls	Walls	k1gInSc1	Walls
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Houdini	Houdin	k2eAgMnPc1d1	Houdin
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
