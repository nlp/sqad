<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
(	(	kIx(	(
<g/>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Agnesë	Agnesë	k1gFnSc2	Agnesë
Gonxhe	Gonxhe	k1gNnSc3	Gonxhe
Bojaxhiu	Bojaxhium	k1gNnSc3	Bojaxhium
[	[	kIx(	[
<g/>
gondže	gondzat	k5eAaPmIp3nS	gondzat
bojadžiu	bojadžius	k1gMnSc3	bojadžius
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1910	[number]	k4	1910
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
indická	indický	k2eAgFnSc1d1	indická
humanitární	humanitární	k2eAgFnSc1d1	humanitární
pracovnice	pracovnice	k1gFnSc1	pracovnice
a	a	k8xC	a
řeholnice	řeholnice	k1gFnSc1	řeholnice
albánského	albánský	k2eAgInSc2d1	albánský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
uctívaná	uctívaný	k2eAgFnSc1d1	uctívaná
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
