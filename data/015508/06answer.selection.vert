<s>
NP	NP	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
nedeterministicky	deterministicky	k6eNd1
polynomiální	polynomiální	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
množina	množina	k1gFnSc1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
lze	lze	k6eAd1
řešit	řešit	k5eAaImF
v	v	k7c6
polynomiálně	polynomiálně	k6eAd1
omezeném	omezený	k2eAgInSc6d1
čase	čas	k1gInSc6
na	na	k7c6
nedeterministickém	deterministický	k2eNgMnSc6d1
Turingově	Turingově	k1gMnSc6
stroji	stroj	k1gInSc3
-	-	kIx~
na	na	k7c6
počítači	počítač	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgInSc6
kroku	krok	k1gInSc6
rozvětvit	rozvětvit	k5eAaPmF
výpočet	výpočet	k1gInSc4
na	na	k7c4
n	n	k0
větví	větvit	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
posléze	posléze	k6eAd1
řešení	řešení	k1gNnSc1
hledá	hledat	k5eAaImIp3nS
současně	současně	k6eAd1
<g/>
.	.	kIx.
</s>