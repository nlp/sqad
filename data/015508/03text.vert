<s>
NP	NP	kA
(	(	kIx(
<g/>
třída	třída	k1gFnSc1
složitosti	složitost	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
NP	NP	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
nedeterministicky	deterministicky	k6eNd1
polynomiální	polynomiální	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
množina	množina	k1gFnSc1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
lze	lze	k6eAd1
řešit	řešit	k5eAaImF
v	v	k7c6
polynomiálně	polynomiálně	k6eAd1
omezeném	omezený	k2eAgInSc6d1
čase	čas	k1gInSc6
na	na	k7c6
nedeterministickém	deterministický	k2eNgMnSc6d1
Turingově	Turingově	k1gMnSc6
stroji	stroj	k1gInSc3
-	-	kIx~
na	na	k7c6
počítači	počítač	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
v	v	k7c6
každém	každý	k3xTgInSc6
kroku	krok	k1gInSc6
rozvětvit	rozvětvit	k5eAaPmF
výpočet	výpočet	k1gInSc4
na	na	k7c4
n	n	k0
větví	větvit	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
posléze	posléze	k6eAd1
řešení	řešení	k1gNnSc1
hledá	hledat	k5eAaImIp3nS
současně	současně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekvivalentně	ekvivalentně	k6eAd1
se	se	k3xPyFc4
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
stroji	stroj	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
na	na	k7c6
místě	místo	k1gNnSc6
rozhodování	rozhodování	k1gNnSc2
uhodne	uhodnout	k5eAaPmIp3nS
správnou	správný	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
výpočtu	výpočet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alternativně	alternativně	k6eAd1
lze	lze	k6eAd1
tyto	tento	k3xDgInPc1
problémy	problém	k1gInPc1
definovat	definovat	k5eAaBmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
množina	množina	k1gFnSc1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgFnPc2,k3yRgFnPc2,k3yIgFnPc2
lze	lze	k6eAd1
pro	pro	k7c4
dodaný	dodaný	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
v	v	k7c6
polynomiálním	polynomiální	k2eAgInSc6d1
čase	čas	k1gInSc6
ověřit	ověřit	k5eAaPmF
jeho	jeho	k3xOp3gFnSc4
správnost	správnost	k1gFnSc4
(	(	kIx(
<g/>
ale	ale	k8xC
obecně	obecně	k6eAd1
nikoliv	nikoliv	k9
nalézt	nalézt	k5eAaBmF,k5eAaPmF
řešení	řešení	k1gNnSc4
v	v	k7c6
polynomiálním	polynomiální	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Složitostní	Složitostní	k2eAgFnSc1d1
třída	třída	k1gFnSc1
P	P	kA
je	být	k5eAaImIp3nS
obsažena	obsažen	k2eAgFnSc1d1
v	v	k7c6
NP	NP	kA
<g/>
,	,	kIx,
ale	ale	k8xC
NP	NP	kA
obsahuje	obsahovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
důležitých	důležitý	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
zvaných	zvaný	k2eAgInPc2d1
NP-úplné	NP-úplný	k2eAgFnSc3d1
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
není	být	k5eNaImIp3nS
znám	znám	k2eAgInSc4d1
žádný	žádný	k3yNgInSc4
polynomiální	polynomiální	k2eAgInSc4d1
algoritmus	algoritmus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
nejdůležitějším	důležitý	k2eAgInSc7d3
problémem	problém	k1gInSc7
současné	současný	k2eAgFnSc2d1
informatiky	informatika	k1gFnSc2
(	(	kIx(
<g/>
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
Problémy	problém	k1gInPc4
milénia	milénium	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
otázka	otázka	k1gFnSc1
<g/>
,	,	kIx,
zda	zda	k8xS
P	P	kA
=	=	kIx~
NP	NP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
expertů	expert	k1gMnPc2
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
ale	ale	k8xC
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
P	P	kA
je	být	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc7d1
podmnožinou	podmnožina	k1gFnSc7
NP	NP	kA
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
problémů	problém	k1gInPc2
v	v	k7c6
NP	NP	kA
</s>
<s>
Všechny	všechen	k3xTgInPc1
problémy	problém	k1gInPc1
v	v	k7c6
P	P	kA
</s>
<s>
Faktorizace	faktorizace	k1gFnSc1
přirozených	přirozený	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
–	–	k?
tj.	tj.	kA
hledání	hledání	k1gNnSc1
dělitelů	dělitel	k1gInPc2
čísla	číslo	k1gNnSc2
</s>
<s>
Faktorizace	faktorizace	k1gFnSc1
celých	celý	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
</s>
<s>
Izomorfismus	izomorfismus	k1gInSc1
grafů	graf	k1gInPc2
–	–	k?
problém	problém	k1gInSc1
rozhodnutí	rozhodnutí	k1gNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
možné	možný	k2eAgInPc4d1
dva	dva	k4xCgInPc4
grafy	graf	k1gInPc4
shodně	shodně	k6eAd1
nakreslit	nakreslit	k5eAaPmF
</s>
<s>
Problém	problém	k1gInSc1
obchodního	obchodní	k2eAgMnSc2d1
cestujícího	cestující	k1gMnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
