<s>
Kdy	kdy	k6eAd1	kdy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
sir	sir	k1gMnSc1	sir
Edward	Edward	k1gMnSc1	Edward
Victor	Victor	k1gMnSc1	Victor
Appleton	Appleton	k1gInSc4	Appleton
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zíkal	zíkat	k5eAaImAgMnS	zíkat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
především	především	k9	především
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
existence	existence	k1gFnSc2	existence
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
?	?	kIx.	?
</s>
