<s>
Sir	sir	k1gMnSc1	sir
Edward	Edward	k1gMnSc1	Edward
Victor	Victor	k1gMnSc1	Victor
Appleton	Appleton	k1gInSc1	Appleton
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1892	[number]	k4	1892
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
studium	studium	k1gNnSc4	studium
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
tzv.	tzv.	kA	tzv.
Appletonových	Appletonový	k2eAgFnPc2d1	Appletonová
vrstev	vrstva	k1gFnPc2	vrstva
v	v	k7c6	v
ionosféře	ionosféra	k1gFnSc6	ionosféra
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
existence	existence	k1gFnSc2	existence
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
.	.	kIx.	.
</s>
<s>
Appletonovy	Appletonův	k2eAgFnPc1d1	Appletonův
vrstvy	vrstva	k1gFnPc1	vrstva
spolehlivě	spolehlivě	k6eAd1	spolehlivě
odrážejí	odrážet	k5eAaImIp3nP	odrážet
radiové	radiový	k2eAgFnPc1d1	radiová
vlny	vlna	k1gFnPc1	vlna
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odrážejí	odrážet	k5eAaImIp3nP	odrážet
radiové	radiový	k2eAgFnPc1d1	radiová
vlny	vlna	k1gFnPc1	vlna
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
denní	denní	k2eAgFnSc6d1	denní
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
na	na	k7c4	na
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gNnSc7	College
(	(	kIx(	(
<g/>
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
do	do	k7c2	do
Cavendishovy	Cavendishův	k2eAgFnSc2d1	Cavendishova
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
univerzitě	univerzita	k1gFnSc6	univerzita
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokončil	dokončit	k5eAaPmAgInS	dokončit
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
šíření	šíření	k1gNnSc4	šíření
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
radiové	radiový	k2eAgFnPc1d1	radiová
vlny	vlna	k1gFnPc1	vlna
s	s	k7c7	s
dostatečně	dostatečně	k6eAd1	dostatečně
krátkou	krátký	k2eAgFnSc4d1	krátká
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
procházející	procházející	k2eAgFnSc7d1	procházející
dolní	dolní	k2eAgFnSc7d1	dolní
ionosférou	ionosféra	k1gFnSc7	ionosféra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
odraženy	odrazit	k5eAaPmNgFnP	odrazit
horní	horní	k2eAgFnSc7d1	horní
vrstvou	vrstva	k1gFnSc7	vrstva
ionosféry	ionosféra	k1gFnSc2	ionosféra
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
známou	známá	k1gFnSc7	známá
jako	jako	k8xC	jako
Appletonova	Appletonův	k2eAgFnSc1d1	Appletonův
vrstva	vrstva	k1gFnSc1	vrstva
neboli	neboli	k8xC	neboli
F2	F2	k1gFnSc1	F2
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
umožnil	umožnit	k5eAaPmAgInS	umožnit
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
radiovou	radiový	k2eAgFnSc4d1	radiová
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
radaru	radar	k1gInSc2	radar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
na	na	k7c4	na
Cambridge	Cambridge	k1gFnPc4	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sekretářem	sekretář	k1gMnSc7	sekretář
vládního	vládní	k2eAgNnSc2d1	vládní
oddělení	oddělení	k1gNnSc2	oddělení
Department	department	k1gInSc1	department
of	of	k?	of
Scientific	Scientific	k1gMnSc1	Scientific
and	and	k?	and
Industrial	Industrial	k1gMnSc1	Industrial
Research	Research	k1gMnSc1	Research
(	(	kIx(	(
<g/>
Oddělení	oddělení	k1gNnSc1	oddělení
vědeckého	vědecký	k2eAgInSc2d1	vědecký
a	a	k8xC	a
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
radaru	radar	k1gInSc2	radar
a	a	k8xC	a
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rektorem	rektor	k1gMnSc7	rektor
a	a	k8xC	a
vicekancléřem	vicekancléř	k1gMnSc7	vicekancléř
na	na	k7c6	na
Edinburské	Edinburský	k2eAgFnSc6d1	Edinburská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Edward	Edward	k1gMnSc1	Edward
Victor	Victor	k1gMnSc1	Victor
Appleton	Appleton	k1gInSc1	Appleton
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
www.nobelprize.org	www.nobelprize.org	k1gInSc1	www.nobelprize.org
-	-	kIx~	-
Appleton	Appleton	k1gInSc1	Appleton
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Edward	Edward	k1gMnSc1	Edward
Victor	Victor	k1gMnSc1	Victor
Appleton	Appleton	k1gInSc4	Appleton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
