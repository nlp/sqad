<s>
Mistrovství	mistrovství	k1gNnSc1
Afriky	Afrika	k1gFnSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
nejvyšší	vysoký	k2eAgFnSc7d3
kontinentální	kontinentální	k2eAgFnSc7d1
soutěží	soutěž	k1gFnSc7
Afriky	Afrika	k1gFnSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
organizované	organizovaný	k2eAgFnSc6d1
ISSF	ISSF	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c6
egyptské	egyptský	k2eAgFnSc6d1
Káhiře	Káhira	k1gFnSc6
od	od	k7c2
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zúčastnilo	zúčastnit	k5eAaPmAgNnS
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc2
více	hodně	k6eAd2
než	než	k8xS
150	#num#	k4
střelců	střelec	k1gMnPc2
z	z	k7c2
21	#num#	k4
afrických	africký	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>