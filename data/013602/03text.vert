<s>
Mistrovství	mistrovství	k1gNnSc1
Afriky	Afrika	k1gFnSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
2017	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
mistrovství	mistrovství	k1gNnSc6
Afriky	Afrika	k1gFnSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
2017	#num#	k4
2017	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Sport	sport	k1gInSc1
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
Datum	datum	k1gNnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
Místo	místo	k7c2
</s>
<s>
Káhira	Káhira	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Egypt	Egypt	k1gInSc1
Egypt	Egypt	k1gInSc1
Tento	tento	k3xDgInSc4
box	box	k1gInSc4
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
vítěz	vítěz	k1gMnSc1
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
druhý	druhý	k4xOgInSc1
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
</s>
<s>
třetí	třetí	k4xOgMnSc1
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
čtvrtý	čtvrtý	k4xOgMnSc1
</s>
<s>
Maroko	Maroko	k1gNnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Afriky	Afrika	k1gFnSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
nejvyšší	vysoký	k2eAgFnSc7d3
kontinentální	kontinentální	k2eAgFnSc7d1
soutěží	soutěž	k1gFnSc7
Afriky	Afrika	k1gFnSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
organizované	organizovaný	k2eAgFnSc6d1
ISSF	ISSF	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c6
egyptské	egyptský	k2eAgFnSc6d1
Káhiře	Káhira	k1gFnSc6
od	od	k7c2
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zúčastnilo	zúčastnit	k5eAaPmAgNnS
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc2
více	hodně	k6eAd2
než	než	k8xS
150	#num#	k4
střelců	střelec	k1gMnPc2
z	z	k7c2
21	#num#	k4
afrických	africký	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Medailové	medailový	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
států	stát	k1gInPc2
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Egypt	Egypt	k1gInSc1
Egypt	Egypt	k1gInSc1
</s>
<s>
22	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
Tunisko	Tunisko	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Maroko	Maroko	k1gNnSc1
Maroko	Maroko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Senegal	Senegal	k1gInSc1
Senegal	Senegal	k1gInSc1
</s>
<s>
0	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Keňa	Keňa	k1gFnSc1
Keňa	Keňa	k1gFnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ISSF	ISSF	kA
-	-	kIx~
International	International	k1gFnSc1
Shooting	Shooting	k1gInSc4
Sport	sport	k1gInSc1
Federation	Federation	k1gInSc1
-	-	kIx~
issf-sports	issf-sports	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
www.issf-sports.org	www.issf-sports.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Four	Four	k1gMnSc1
YOG	YOG	kA
quotas	quotas	k1gMnSc1
for	forum	k1gNnPc2
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
2018	#num#	k4
delivered	delivered	k1gInSc1
at	at	k?
the	the	k?
13	#num#	k4
<g/>
th	th	k?
African	African	k1gMnSc1
Championship	Championship	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSF	ISSF	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.issf-sports.org/media/calendar/2017/2454/completeresult/AFC%20EGY%202017%20Complete%20Results%2020170502.pdf.mw-parser-output	http://www.issf-sports.org/media/calendar/2017/2454/completeresult/AFC%20EGY%202017%20Complete%20Results%2020170502.pdf.mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
Afriky	Afrika	k1gFnSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
</s>
<s>
1984	#num#	k4
(	(	kIx(
<g/>
Tunis	Tunis	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
1987	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
1993	#num#	k4
(	(	kIx(
<g/>
Bloemfontein	Bloemfonteina	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
1995	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
1997	#num#	k4
(	(	kIx(
<g/>
Bloemfontein	Bloemfonteina	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
1999	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
2003	#num#	k4
(	(	kIx(
<g/>
Pretoria	Pretorium	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
2007	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
2010	#num#	k4
(	(	kIx(
<g/>
Tipaza	Tipaza	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
2011	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
+	+	kIx~
Rabat	rabat	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
2014	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
2015	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
2017	#num#	k4
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
</s>
