<p>
<s>
Klokan	klokan	k1gMnSc1	klokan
tmavý	tmavý	k2eAgMnSc1d1	tmavý
(	(	kIx(	(
<g/>
Dendrolagus	Dendrolagus	k1gInSc1	Dendrolagus
scottae	scotta	k1gInSc2	scotta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
stromového	stromový	k2eAgMnSc2d1	stromový
klokana	klokan	k1gMnSc2	klokan
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
klokanovití	klokanovitý	k2eAgMnPc1d1	klokanovitý
(	(	kIx(	(
<g/>
Macropodidae	Macropodidae	k1gNnSc7	Macropodidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
endemický	endemický	k2eAgMnSc1d1	endemický
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
plochu	plocha	k1gFnSc4	plocha
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
Torricelli	Torricelle	k1gFnSc4	Torricelle
na	na	k7c6	na
Papui-Nové	Papui-Nové	k2eAgFnSc6d1	Papui-Nové
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
subtropické	subtropický	k2eAgNnSc1d1	subtropické
nebo	nebo	k8xC	nebo
tropické	tropický	k2eAgNnSc1d1	tropické
suché	suchý	k2eAgInPc1d1	suchý
lesy	les	k1gInPc1	les
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
ztrátou	ztráta	k1gFnSc7	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
je	být	k5eAaImIp3nS	být
klokan	klokan	k1gMnSc1	klokan
tmavý	tmavý	k2eAgInSc1d1	tmavý
častou	častý	k2eAgFnSc7d1	častá
součástí	součást	k1gFnSc7	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
klokaní	klokaní	k2eAgNnSc1d1	klokaní
maso	maso	k1gNnSc1	maso
tvoří	tvořit	k5eAaImIp3nS	tvořit
téměř	téměř	k6eAd1	téměř
jediný	jediný	k2eAgInSc1d1	jediný
zdroj	zdroj	k1gInSc1	zdroj
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
klokany	klokan	k1gMnPc4	klokan
tmavé	tmavý	k2eAgMnPc4d1	tmavý
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lov	lov	k1gInSc1	lov
pro	pro	k7c4	pro
kožešiny	kožešina	k1gFnPc4	kožešina
a	a	k8xC	a
odlesňování	odlesňování	k1gNnPc4	odlesňování
Papuy-Nové	Papuy-Nové	k2eAgFnPc4d1	Papuy-Nové
Guiney	Guinea	k1gFnPc4	Guinea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Klokan	klokan	k1gMnSc1	klokan
tmavý	tmavý	k2eAgMnSc1d1	tmavý
je	být	k5eAaImIp3nS	být
blízký	blízký	k2eAgMnSc1d1	blízký
příbuzný	příbuzný	k1gMnSc1	příbuzný
klokana	klokan	k1gMnSc2	klokan
Doriova	Doriův	k2eAgFnSc1d1	Doriův
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
od	od	k7c2	od
9	[number]	k4	9
do	do	k7c2	do
11	[number]	k4	11
kg	kg	kA	kg
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
činí	činit	k5eAaImIp3nS	činit
61	[number]	k4	61
až	až	k9	až
62,5	[number]	k4	62,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jen	jen	k9	jen
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
52,5	[number]	k4	52,5
až	až	k8xS	až
59	[number]	k4	59
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
černí	černý	k1gMnPc1	černý
<g/>
,	,	kIx,	,
s	s	k7c7	s
hnědou	hnědý	k2eAgFnSc7d1	hnědá
až	až	k8xS	až
čokoládovou	čokoládový	k2eAgFnSc7d1	čokoládová
srstí	srst	k1gFnSc7	srst
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
silný	silný	k2eAgInSc4d1	silný
zápach	zápach	k1gInSc4	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Klokani	klokan	k1gMnPc1	klokan
tmaví	tmavý	k2eAgMnPc1d1	tmavý
mají	mít	k5eAaImIp3nP	mít
nápadně	nápadně	k6eAd1	nápadně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
čenich	čenich	k1gInSc4	čenich
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopní	schopný	k2eAgMnPc1d1	schopný
dobře	dobře	k6eAd1	dobře
skákat	skákat	k5eAaImF	skákat
dopředu	dopředu	k6eAd1	dopředu
i	i	k9	i
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
klokanů	klokan	k1gMnPc2	klokan
dokáží	dokázat	k5eAaPmIp3nP	dokázat
své	svůj	k3xOyFgFnPc4	svůj
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
zvednout	zvednout	k5eAaPmF	zvednout
až	až	k9	až
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
předpokládáno	předpokládán	k2eAgNnSc1d1	předpokládáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
plodí	plodit	k5eAaImIp3nP	plodit
mladé	mladý	k2eAgFnPc1d1	mladá
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
samostatné	samostatný	k2eAgNnSc1d1	samostatné
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Klokani	klokan	k1gMnPc1	klokan
tmaví	tmavý	k2eAgMnPc1d1	tmavý
bývají	bývat	k5eAaImIp3nP	bývat
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nejinteligentnější	inteligentní	k2eAgFnPc4d3	nejinteligentnější
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
stromových	stromový	k2eAgMnPc2d1	stromový
klokanů	klokan	k1gMnPc2	klokan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Habitat	Habitat	k1gMnPc5	Habitat
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
tmaví	tmavý	k2eAgMnPc1d1	tmavý
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
omezené	omezený	k2eAgFnPc1d1	omezená
možnosti	možnost	k1gFnPc1	možnost
pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
útočiště	útočiště	k1gNnSc2	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
900	[number]	k4	900
do	do	k7c2	do
1	[number]	k4	1
700	[number]	k4	700
m	m	kA	m
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Torricelli	Torricell	k1gMnPc1	Torricell
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
rozsah	rozsah	k1gInSc1	rozsah
jejich	jejich	k3xOp3gInSc2	jejich
areálu	areál	k1gInSc2	areál
výskytu	výskyt	k1gInSc2	výskyt
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
125	[number]	k4	125
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
tropické	tropický	k2eAgInPc1d1	tropický
nebo	nebo	k8xC	nebo
subtropické	subtropický	k2eAgInPc1d1	subtropický
horské	horský	k2eAgInPc1d1	horský
lesy	les	k1gInPc1	les
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
nohoplodů	nohoplod	k1gInPc2	nohoplod
(	(	kIx(	(
<g/>
Podocarpus	Podocarpus	k1gInSc1	Podocarpus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pazeravců	pazeravec	k1gInPc2	pazeravec
(	(	kIx(	(
<g/>
Libocedrus	Libocedrus	k1gInSc1	Libocedrus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
blahočetů	blahočet	k1gInPc2	blahočet
(	(	kIx(	(
<g/>
Araucaria	Araucarium	k1gNnSc2	Araucarium
<g/>
)	)	kIx)	)
a	a	k8xC	a
stromů	strom	k1gInPc2	strom
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Rapanea	Rapane	k1gInSc2	Rapane
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
býložravci	býložravec	k1gMnPc1	býložravec
a	a	k8xC	a
živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
epifytickými	epifytický	k2eAgFnPc7d1	epifytická
kapradinami	kapradina	k1gFnPc7	kapradina
a	a	k8xC	a
listy	list	k1gInPc7	list
vějířovek	vějířovka	k1gFnPc2	vějířovka
(	(	kIx(	(
<g/>
Scaevola	Scaevola	k1gFnSc1	Scaevola
<g/>
)	)	kIx)	)
a	a	k8xC	a
smirkovek	smirkovka	k1gFnPc2	smirkovka
(	(	kIx(	(
<g/>
Tetracera	Tetracera	k1gFnSc1	Tetracera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
ale	ale	k8xC	ale
ani	ani	k8xC	ani
zralým	zralý	k2eAgNnSc7d1	zralé
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
potravu	potrava	k1gFnSc4	potrava
hledají	hledat	k5eAaImIp3nP	hledat
buďto	buďto	k8xC	buďto
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
nebo	nebo	k8xC	nebo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
o	o	k7c6	o
potravě	potrava	k1gFnSc6	potrava
a	a	k8xC	a
životě	život	k1gInSc6	život
klokanů	klokan	k1gMnPc2	klokan
tmavých	tmavý	k2eAgMnPc2d1	tmavý
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
obecně	obecně	k6eAd1	obecně
příliš	příliš	k6eAd1	příliš
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
tmaví	tmavý	k2eAgMnPc1d1	tmavý
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
tedy	tedy	k9	tedy
žádné	žádný	k3yNgNnSc4	žádný
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
připravené	připravený	k2eAgFnPc1d1	připravená
se	se	k3xPyFc4	se
pářit	pářit	k5eAaImF	pářit
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ale	ale	k9	ale
mláďata	mládě	k1gNnPc1	mládě
rodí	rodit	k5eAaImIp3nP	rodit
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
a	a	k8xC	a
období	období	k1gNnSc4	období
březosti	březost	k1gFnSc2	březost
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mládě	mládě	k1gNnSc4	mládě
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stará	starat	k5eAaImIp3nS	starat
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
samci	samec	k1gMnPc1	samec
se	se	k3xPyFc4	se
neangažují	angažovat	k5eNaBmIp3nP	angažovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
stromových	stromový	k2eAgMnPc2d1	stromový
klokanů	klokan	k1gMnPc2	klokan
<g/>
.	.	kIx.	.
</s>
<s>
Novorození	novorozený	k2eAgMnPc1d1	novorozený
klokani	klokan	k1gMnPc1	klokan
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
matčině	matčin	k2eAgInSc6d1	matčin
vaku	vak	k1gInSc6	vak
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
pozemních	pozemní	k2eAgMnPc2d1	pozemní
klokanů	klokan	k1gMnPc2	klokan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
dost	dost	k6eAd1	dost
staří	starý	k2eAgMnPc1d1	starý
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
tedy	tedy	k9	tedy
do	do	k7c2	do
věku	věk	k1gInSc2	věk
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
mládě	mládě	k1gNnSc1	mládě
stráví	strávit	k5eAaPmIp3nS	strávit
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
porodnost	porodnost	k1gFnSc1	porodnost
u	u	k7c2	u
klokanů	klokan	k1gMnPc2	klokan
tmavých	tmavý	k2eAgMnPc2d1	tmavý
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jejich	jejich	k3xOp3gFnSc4	jejich
populaci	populace	k1gFnSc4	populace
negativně	negativně	k6eAd1	negativně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
registrováno	registrovat	k5eAaBmNgNnS	registrovat
jen	jen	k6eAd1	jen
100	[number]	k4	100
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tak	tak	k6eAd1	tak
malému	malý	k2eAgInSc3d1	malý
počtu	počet	k1gInSc3	počet
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
i	i	k8xC	i
průběh	průběh	k1gInSc1	průběh
výběru	výběr	k1gInSc2	výběr
partnera	partner	k1gMnSc2	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
klokan	klokan	k1gMnSc1	klokan
tmavý	tmavý	k2eAgMnSc1d1	tmavý
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
klokani	klokan	k1gMnPc1	klokan
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
skupinkách	skupinka	k1gFnPc6	skupinka
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
kusech	kus	k1gInPc6	kus
<g/>
,	,	kIx,	,
samec	samec	k1gInSc1	samec
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijí	žít	k5eAaImIp3nP	žít
většinou	většinou	k6eAd1	většinou
individuálně	individuálně	k6eAd1	individuálně
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kvůli	kvůli	k7c3	kvůli
nízkému	nízký	k2eAgInSc3d1	nízký
počtu	počet	k1gInSc3	počet
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
tmaví	tmavý	k2eAgMnPc1d1	tmavý
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
lhostejní	lhostejnět	k5eAaImIp3nS	lhostejnět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
drží	držet	k5eAaImIp3nS	držet
se	se	k3xPyFc4	se
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tenkile	Tenkila	k1gFnSc3	Tenkila
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
klokan	klokan	k1gMnSc1	klokan
tmavý	tmavý	k2eAgMnSc1d1	tmavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Dendrolagus	Dendrolagus	k1gInSc1	Dendrolagus
scottae	scottaat	k5eAaPmIp3nS	scottaat
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Tenkile	Tenkile	k6eAd1	Tenkile
Conservation	Conservation	k1gInSc1	Conservation
Alliance	Alliance	k1gFnSc2	Alliance
–	–	k?	–
organizace	organizace	k1gFnSc2	organizace
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
ochranou	ochrana	k1gFnSc7	ochrana
klokanů	klokan	k1gMnPc2	klokan
tmavých	tmavý	k2eAgMnPc2d1	tmavý
</s>
</p>
