<p>
<s>
Třinec	Třinec	k1gInSc1	Třinec
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Trzyniec	Trzyniec	k1gInSc1	Trzyniec
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
Trzynietz	Trzynietz	k1gInSc1	Trzynietz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
32	[number]	k4	32
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
historického	historický	k2eAgNnSc2d1	historické
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
8	[number]	k4	8
541	[number]	k4	541
ha	ha	kA	ha
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
306	[number]	k4	306
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
na	na	k7c6	na
Javorovém	javorový	k2eAgInSc6d1	javorový
vrchu	vrch	k1gInSc6	vrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třincem	Třinec	k1gInSc7	Třinec
protéká	protékat	k5eAaImIp3nS	protékat
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
řeka	řeka	k1gFnSc1	řeka
Olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
výtoku	výtok	k1gInSc2	výtok
z	z	k7c2	z
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Jablunkovu	Jablunkův	k2eAgFnSc4d1	Jablunkova
je	být	k5eAaImIp3nS	být
Třinec	Třinec	k1gInSc1	Třinec
druhým	druhý	k4xOgInSc7	druhý
nejvýchodnějším	východní	k2eAgNnSc7d3	nejvýchodnější
městem	město	k1gNnSc7	město
celého	celý	k2eAgNnSc2d1	celé
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nP	sídlet
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
ocelových	ocelový	k2eAgInPc2d1	ocelový
válcovaných	válcovaný	k2eAgInPc2d1	válcovaný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
Třinecké	třinecký	k2eAgFnPc1d1	třinecká
železárny	železárna	k1gFnPc1	železárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Třinci	Třinec	k1gInSc6	Třinec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1444	[number]	k4	1444
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
pouhou	pouhý	k2eAgFnSc4d1	pouhá
osadu	osada	k1gFnSc4	osada
<g/>
,	,	kIx,	,
statut	statut	k1gInSc1	statut
města	město	k1gNnSc2	město
obec	obec	k1gFnSc1	obec
získala	získat	k5eAaPmAgFnS	získat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Československo-polské	československoolský	k2eAgInPc1d1	československo-polský
spory	spor	k1gInPc1	spor
o	o	k7c4	o
Třinec	Třinec	k1gInSc4	Třinec
===	===	k?	===
</s>
</p>
<p>
<s>
Třinec	Třinec	k1gInSc1	Třinec
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
Těšínskem	Těšínsko	k1gNnSc7	Těšínsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
nejprve	nejprve	k6eAd1	nejprve
včleněn	včlenit	k5eAaPmNgMnS	včlenit
do	do	k7c2	do
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
polské	polský	k2eAgFnSc2d1	polská
části	část	k1gFnSc2	část
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
v	v	k7c6	v
Sedmidenní	sedmidenní	k2eAgFnSc6d1	sedmidenní
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
československé	československý	k2eAgFnPc4d1	Československá
jednotky	jednotka	k1gFnPc4	jednotka
vedl	vést	k5eAaImAgMnS	vést
Josef	Josef	k1gMnSc1	Josef
Šnejdárek	Šnejdárek	k1gMnSc1	Šnejdárek
<g/>
)	)	kIx)	)
obsazen	obsadit	k5eAaPmNgInS	obsadit
Československem	Československo	k1gNnSc7	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
arbitráže	arbitráž	k1gFnSc2	arbitráž
ve	v	k7c6	v
Spa	Spa	k1gFnSc6	Spa
přiřčen	přiřčen	k2eAgInSc1d1	přiřčen
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
dekretem	dekret	k1gInSc7	dekret
vlády	vláda	k1gFnSc2	vláda
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
Třinec	Třinec	k1gInSc1	Třinec
povýšen	povýšit	k5eAaPmNgInS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
byl	být	k5eAaImAgInS	být
Třinec	Třinec	k1gInSc1	Třinec
Polskem	Polsko	k1gNnSc7	Polsko
obsazen	obsadit	k5eAaPmNgInS	obsadit
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Polska	Polsko	k1gNnSc2	Polsko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Československu	Československo	k1gNnSc3	Československo
byl	být	k5eAaImAgInS	být
Třinec	Třinec	k1gInSc1	Třinec
připojen	připojit	k5eAaPmNgInS	připojit
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
Třinec	Třinec	k1gInSc4	Třinec
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
třinecká	třinecký	k2eAgFnSc1d1	třinecká
huť	huť	k1gFnSc1	huť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
jednotřídkou	jednotřídka	k1gFnSc7	jednotřídka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
pracovníků	pracovník	k1gMnPc2	pracovník
hutě	huť	k1gFnSc2	huť
–	–	k?	–
budova	budova	k1gFnSc1	budova
školy	škola	k1gFnSc2	škola
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
rozšiřována	rozšiřován	k2eAgFnSc1d1	rozšiřována
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
základní	základní	k2eAgFnSc2d1	základní
a	a	k8xC	a
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školy	škola	k1gFnSc2	škola
s	s	k7c7	s
polským	polský	k2eAgInSc7d1	polský
vyučovacím	vyučovací	k2eAgInSc7d1	vyučovací
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlakové	vlakový	k2eAgNnSc1d1	vlakové
nádraží	nádraží	k1gNnSc1	nádraží
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1869	[number]	k4	1869
až	až	k9	až
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
přestavba	přestavba	k1gFnSc1	přestavba
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
dokončena	dokončit	k5eAaPmNgFnS	dokončit
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
dni	den	k1gInSc3	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1882	[number]	k4	1882
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
římsko-katolického	římskoatolický	k2eAgInSc2d1	římsko-katolický
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Alberta	Alberta	k1gFnSc1	Alberta
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1885	[number]	k4	1885
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
9	[number]	k4	9
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
české	český	k2eAgFnSc2d1	Česká
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1936	[number]	k4	1936
zřízena	zřízen	k2eAgFnSc1d1	zřízena
závodní	závodní	k2eAgFnSc1d1	závodní
škola	škola	k1gFnSc1	škola
Třineckých	třinecký	k2eAgFnPc2d1	třinecká
železáren	železárna	k1gFnPc2	železárna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Třineckých	třinecký	k2eAgFnPc2d1	třinecká
železáren	železárna	k1gFnPc2	železárna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třinec	Třinec	k1gInSc1	Třinec
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
od	od	k7c2	od
fašismu	fašismus	k1gInSc2	fašismus
3	[number]	k4	3
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
stavba	stavba	k1gFnSc1	stavba
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Dolní	dolní	k2eAgNnPc1d1	dolní
Líštná	Líštné	k1gNnPc1	Líštné
(	(	kIx(	(
<g/>
Sosna	sosna	k1gFnSc1	sosna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otevření	otevření	k1gNnSc6	otevření
prvních	první	k4xOgInPc2	první
pavilónů	pavilón	k1gInPc2	pavilón
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc1	činnost
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Třinec	Třinec	k1gInSc1	Třinec
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
na	na	k7c4	na
Javorový	javorový	k2eAgInSc4d1	javorový
vrch	vrch	k1gInSc4	vrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
letní	letní	k2eAgNnSc4d1	letní
koupaliště	koupaliště	k1gNnSc4	koupaliště
na	na	k7c6	na
Lesní	lesní	k2eAgFnSc6d1	lesní
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
Domu	dům	k1gInSc2	dům
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
MOND	mond	k1gInSc1	mond
<g/>
)	)	kIx)	)
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Lyžbice	Lyžbice	k1gFnSc1	Lyžbice
(	(	kIx(	(
<g/>
Terasa	terasa	k1gFnSc1	terasa
<g/>
)	)	kIx)	)
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
až	až	k9	až
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
výstavby	výstavba	k1gFnSc2	výstavba
zimního	zimní	k2eAgInSc2d1	zimní
stadionu	stadion	k1gInSc2	stadion
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
(	(	kIx(	(
<g/>
nekrytý	krytý	k2eNgMnSc1d1	nekrytý
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
rok	rok	k1gInSc4	rok
1974	[number]	k4	1974
začátek	začátek	k1gInSc1	začátek
zastřešení	zastřešení	k1gNnSc2	zastřešení
<g/>
,	,	kIx,	,
ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
kulturního	kulturní	k2eAgInSc2d1	kulturní
domu	dům	k1gInSc2	dům
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
a	a	k8xC	a
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
otevření	otevření	k1gNnSc4	otevření
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
promítání	promítání	k1gNnSc2	promítání
prvního	první	k4xOgMnSc2	první
filmu	film	k1gInSc2	film
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
Kosmos	kosmos	k1gInSc1	kosmos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
130	[number]	k4	130
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
třinecké	třinecký	k2eAgFnSc2d1	třinecká
huti	huť	k1gFnSc2	huť
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Podnikové	podnikový	k2eAgNnSc1d1	podnikové
muzeum	muzeum	k1gNnSc1	muzeum
Třineckých	třinecký	k2eAgFnPc2d1	třinecká
železáren	železárna	k1gFnPc2	železárna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Muzeum	muzeum	k1gNnSc1	muzeum
Třineckých	třinecký	k2eAgFnPc2d1	třinecká
železáren	železárna	k1gFnPc2	železárna
a	a	k8xC	a
města	město	k1gNnSc2	město
Třince	Třinec	k1gInSc2	Třinec
<g/>
)	)	kIx)	)
jako	jako	k9	jako
první	první	k4xOgNnSc4	první
hutnické	hutnický	k2eAgNnSc4d1	hutnické
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
otevření	otevření	k1gNnSc4	otevření
nové	nový	k2eAgFnSc2d1	nová
polikliniky	poliklinika	k1gFnSc2	poliklinika
(	(	kIx(	(
<g/>
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Podlesí	Podlesí	k1gNnSc2	Podlesí
<g/>
)	)	kIx)	)
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Konská	Konská	k1gFnSc1	Konská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Třineckými	třinecký	k2eAgFnPc7d1	třinecká
železárnami	železárna	k1gFnPc7	železárna
dokončena	dokončit	k5eAaPmNgFnS	dokončit
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Baliny	Balina	k1gFnSc2	Balina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
zázemí	zázemí	k1gNnSc4	zázemí
mnoha	mnoho	k4c3	mnoho
firmám	firma	k1gFnPc3	firma
(	(	kIx(	(
<g/>
Vesuvius	Vesuvius	k1gMnSc1	Vesuvius
Solar	Solar	k1gMnSc1	Solar
Crucible	Crucible	k1gMnSc1	Crucible
<g/>
,	,	kIx,	,
Kern	Kern	k1gMnSc1	Kern
<g/>
,	,	kIx,	,
BZN	BZN	kA	BZN
<g/>
,	,	kIx,	,
JAP	JAP	kA	JAP
Trading	Trading	k1gInSc1	Trading
<g/>
,	,	kIx,	,
DONGWON	DONGWON	kA	DONGWON
CZ	CZ	kA	CZ
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgFnSc3d1	chráněná
dílně	dílna	k1gFnSc3	dílna
Ergon	Ergona	k1gFnPc2	Ergona
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
Silesmont	Silesmonta	k1gFnPc2	Silesmonta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
firmy	firma	k1gFnPc1	firma
a	a	k8xC	a
společnosti	společnost	k1gFnPc1	společnost
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
nová	nový	k2eAgNnPc4d1	nové
pracovní	pracovní	k2eAgNnPc4d1	pracovní
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
podílejí	podílet	k5eAaImIp3nP	podílet
se	se	k3xPyFc4	se
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
na	na	k7c4	na
snižování	snižování	k1gNnSc4	snižování
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k8xC	i
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
–	–	k?	–
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
zastavil	zastavit	k5eAaPmAgInS	zastavit
na	na	k7c6	na
nově	nově	k6eAd1	nově
vybudované	vybudovaný	k2eAgFnSc6d1	vybudovaná
železniční	železniční	k2eAgFnSc6d1	železniční
zastávce	zastávka	k1gFnSc6	zastávka
Třinec	Třinec	k1gInSc4	Třinec
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
–	–	k?	–
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnPc1d1	nová
hokejová	hokejový	k2eAgFnSc1d1	hokejová
haly	hala	k1gFnSc2	hala
(	(	kIx(	(
<g/>
video	video	k1gNnSc4	video
–	–	k?	–
den	den	k1gInSc1	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
slavnostně	slavnostně	k6eAd1	slavnostně
zprovozněn	zprovozněn	k2eAgInSc1d1	zprovozněn
podjezd	podjezd	k1gInSc1	podjezd
pod	pod	k7c7	pod
železniční	železniční	k2eAgFnSc7d1	železniční
tratí	trať	k1gFnSc7	trať
s	s	k7c7	s
názvem	název	k1gInSc7	název
Via	via	k7c4	via
Lyžbice	Lyžbice	k1gFnPc4	Lyžbice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
se	se	k3xPyFc4	se
Třinec	Třinec	k1gInSc1	Třinec
stal	stát	k5eAaPmAgInS	stát
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
5086	[number]	k4	5086
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
k	k	k7c3	k
slezské	slezský	k2eAgFnSc3d1	Slezská
národnosti	národnost	k1gFnSc3	národnost
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
2543	[number]	k4	2543
(	(	kIx(	(
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
německé	německý	k2eAgNnSc4d1	německé
1190	[number]	k4	1190
(	(	kIx(	(
<g/>
23,4	[number]	k4	23,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
polské	polský	k2eAgFnSc2d1	polská
1134	[number]	k4	1134
(	(	kIx(	(
<g/>
22,3	[number]	k4	22,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
jen	jen	k8xS	jen
195	[number]	k4	195
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
necelá	celý	k2eNgFnSc1d1	necelá
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
k	k	k7c3	k
polské	polský	k2eAgFnSc3d1	polská
národnosti	národnost	k1gFnSc3	národnost
hlásilo	hlásit	k5eAaImAgNnS	hlásit
17,7	[number]	k4	17,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
za	za	k7c4	za
Třinec	Třinec	k1gInSc4	Třinec
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
době	doba	k1gFnSc6	doba
patřily	patřit	k5eAaImAgInP	patřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Náboženství	náboženství	k1gNnSc2	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Třinec	Třinec	k1gInSc1	Třinec
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejreligióznějším	religiózní	k2eAgNnPc3d3	religiózní
městům	město	k1gNnPc3	město
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
k	k	k7c3	k
nějakému	nějaký	k3yIgNnSc3	nějaký
náboženství	náboženství	k1gNnSc3	náboženství
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
60,5	[number]	k4	60,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
skladba	skladba	k1gFnSc1	skladba
věřících	věřící	k1gMnPc2	věřící
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pestrá	pestrý	k2eAgFnSc1d1	pestrá
–	–	k?	–
kromě	kromě	k7c2	kromě
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
své	svůj	k3xOyFgInPc4	svůj
sbory	sbor	k1gInPc4	sbor
řada	řada	k1gFnSc1	řada
protestantských	protestantský	k2eAgFnPc2d1	protestantská
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
činnost	činnost	k1gFnSc4	činnost
vyznavači	vyznavač	k1gMnPc7	vyznavač
mesiánského	mesiánský	k2eAgInSc2d1	mesiánský
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
města	město	k1gNnSc2	město
Třince	Třinec	k1gInSc2	Třinec
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
seniorátů	seniorát	k1gInPc2	seniorát
Slezské	slezský	k2eAgFnSc2d1	Slezská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
augsburského	augsburský	k2eAgNnSc2d1	Augsburské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
čtyři	čtyři	k4xCgInPc4	čtyři
sbory	sbor	k1gInPc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
svými	svůj	k3xOyFgInPc7	svůj
chrámy	chrám	k1gInPc7	chrám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svými	svůj	k3xOyFgInPc7	svůj
sbory	sbor	k1gInPc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
jsou	být	k5eAaImIp3nP	být
Svědci	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
popř.	popř.	kA	popř.
Církev	církev	k1gFnSc1	církev
adventistů	adventista	k1gMnPc2	adventista
sedmého	sedmý	k4xOgInSc2	sedmý
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
Církev	církev	k1gFnSc1	církev
Bratrská	bratrský	k2eAgFnSc1d1	bratrská
a	a	k8xC	a
Církev	církev	k1gFnSc1	církev
Apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnSc1	okolí
Třince	Třinec	k1gInSc2	Třinec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Třince	Třinec	k1gInSc2	Třinec
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1	Moravskoslezská
Beskydy	Beskydy	k1gFnPc1	Beskydy
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
nižší	nízký	k2eAgInPc4d2	nižší
Slezské	slezský	k2eAgInPc4d1	slezský
Beskydy	Beskyd	k1gInPc4	Beskyd
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
karpatská	karpatský	k2eAgNnPc1d1	Karpatské
pohoří	pohoří	k1gNnPc1	pohoří
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
oddělena	oddělen	k2eAgFnSc1d1	oddělena
Jablunkovskou	jablunkovský	k2eAgFnSc7d1	Jablunkovská
brázdou	brázda	k1gFnSc7	brázda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
osu	osa	k1gFnSc4	osa
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Třincem	Třinec	k1gInSc7	Třinec
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třinec	Třinec	k1gInSc1	Třinec
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
východiskem	východisko	k1gNnSc7	východisko
turistických	turistický	k2eAgFnPc2d1	turistická
tras	trasa	k1gFnPc2	trasa
na	na	k7c4	na
okolní	okolní	k2eAgInPc4d1	okolní
vrcholy	vrchol	k1gInPc4	vrchol
Javorový	javorový	k2eAgInSc1d1	javorový
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Ostrý	ostrý	k2eAgInSc1d1	ostrý
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Čantoryje	Čantoryje	k1gFnSc1	Čantoryje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
hutnický	hutnický	k2eAgMnSc1d1	hutnický
(	(	kIx(	(
<g/>
Třinecké	třinecký	k2eAgFnPc1d1	třinecká
železárny	železárna	k1gFnPc1	železárna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
</s>
</p>
<p>
<s>
Doprava	doprava	k1gFnSc1	doprava
<g/>
:	:	kIx,	:
železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
320	[number]	k4	320
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
evropská	evropský	k2eAgFnSc1d1	Evropská
silnice	silnice	k1gFnSc1	silnice
E	E	kA	E
75	[number]	k4	75
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
16	[number]	k4	16
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
Veolia	Veolia	k1gFnSc1	Veolia
Transport	transport	k1gInSc1	transport
Morava	Morava	k1gFnSc1	Morava
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Administrativa	administrativa	k1gFnSc1	administrativa
<g/>
:	:	kIx,	:
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Třineckých	třinecký	k2eAgFnPc2d1	třinecká
železáren	železárna	k1gFnPc2	železárna
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
města	město	k1gNnSc2	město
Třince	Třinec	k1gInSc2	Třinec
–	–	k?	–
Frýdecká	frýdecký	k2eAgFnSc1d1	Frýdecká
389	[number]	k4	389
</s>
</p>
<p>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
Třinec	Třinec	k1gInSc1	Třinec
–	–	k?	–
Lidická	lidický	k2eAgNnPc1d1	Lidické
541	[number]	k4	541
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
s	s	k7c7	s
IZS	IZS	kA	IZS
–	–	k?	–
ukázka	ukázka	k1gFnSc1	ukázka
spolupráce	spolupráce	k1gFnSc2	spolupráce
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
hasičů	hasič	k1gMnPc2	hasič
a	a	k8xC	a
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
na	na	k7c4	na
nám.	nám.	k?	nám.
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
před	před	k7c4	před
KD	KD	kA	KD
Trisia	Trisius	k1gMnSc2	Trisius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
nám.	nám.	k?	nám.
Svobody	svoboda	k1gFnSc2	svoboda
526	[number]	k4	526
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Třinecké	třinecký	k2eAgNnSc4d1	třinecké
kulturní	kulturní	k2eAgNnSc4d1	kulturní
léto	léto	k1gNnSc4	léto
</s>
</p>
<p>
<s>
Hutník	hutník	k1gMnSc1	hutník
-	-	kIx~	-
Každoroční	každoroční	k2eAgFnSc1d1	každoroční
hudební	hudební	k2eAgFnSc1d1	hudební
událost	událost	k1gFnSc1	událost
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Třinci	Třinec	k1gInSc6	Třinec
</s>
</p>
<p>
<s>
Beskydská	beskydský	k2eAgFnSc1d1	Beskydská
laťka	laťka	k1gFnSc1	laťka
v	v	k7c4	v
Třinecké	třinecký	k2eAgInPc4d1	třinecký
Werk	Werk	k1gInSc4	Werk
Aréně	aréna	k1gFnSc6	aréna
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc1	Třinec
–	–	k?	–
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
a	a	k8xC	a
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
hokejový	hokejový	k2eAgMnSc1d1	hokejový
mistr	mistr	k1gMnSc1	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
extraligu	extraliga	k1gFnSc4	extraliga
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soutěž	soutěž	k1gFnSc4	soutěž
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
FK	FK	kA	FK
Fotbal	fotbal	k1gInSc1	fotbal
Třinec	Třinec	k1gInSc1	Třinec
-	-	kIx~	-
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
hrál	hrát	k5eAaImAgMnS	hrát
FNL	FNL	kA	FNL
</s>
</p>
<p>
<s>
Od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2019	[number]	k4	2019
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2020	[number]	k4	2020
má	mít	k5eAaImIp3nS	mít
Třinec	Třinec	k1gInSc1	Třinec
a	a	k8xC	a
Ostrava	Ostrava	k1gFnSc1	Ostrava
hostit	hostit	k5eAaImF	hostit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2020	[number]	k4	2020
</s>
</p>
<p>
<s>
==	==	k?	==
Školy	škola	k1gFnPc1	škola
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
TřinceOtto	TřinceOtto	k1gNnSc4	TřinceOtto
Gavenda	Gavend	k1gMnSc2	Gavend
–	–	k?	–
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Labaj	Labaj	k1gMnSc1	Labaj
–	–	k?	–
sportovní	sportovní	k2eAgMnSc1d1	sportovní
trenér	trenér	k1gMnSc1	trenér
</s>
</p>
<p>
<s>
Gertruda	Gertruda	k1gFnSc1	Gertruda
Milerská	Milerský	k2eAgFnSc1d1	Milerská
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Lachsová	Lachsová	k1gFnSc1	Lachsová
–	–	k?	–
účastnice	účastnice	k1gFnSc2	účastnice
národního	národní	k2eAgInSc2d1	národní
boje	boj	k1gInSc2	boj
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
a	a	k8xC	a
Československý	československý	k2eAgMnSc1d1	československý
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
sdružení	sdružení	k1gNnSc2	sdružení
"	"	kIx"	"
<g/>
Ukrývané	ukrývaný	k2eAgNnSc1d1	ukrývané
dítě	dítě	k1gNnSc1	dítě
-	-	kIx~	-
Hidden	Hiddna	k1gFnPc2	Hiddna
Child	Child	k1gInSc1	Child
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Steiner	Steiner	k1gMnSc1	Steiner
–	–	k?	–
důstojník	důstojník	k1gMnSc1	důstojník
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Armádního	armádní	k2eAgInSc2d1	armádní
čs	čs	kA	čs
<g/>
.	.	kIx.	.
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Štývar	Štývar	k1gInSc1	Štývar
–	–	k?	–
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
sbormistr	sbormistr	k1gMnSc1	sbormistr
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Mašková	Mašková	k1gFnSc1	Mašková
–	–	k?	–
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
vítězka	vítězka	k1gFnSc1	vítězka
Superstar	superstar	k1gFnSc1	superstar
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
<g/>
Cena	cena	k1gFnSc1	cena
města	město	k1gNnSc2	město
TřinecJarek	TřinecJarka	k1gFnPc2	TřinecJarka
Cemerek	Cemerka	k1gFnPc2	Cemerka
–	–	k?	–
choreograf	choreograf	k1gMnSc1	choreograf
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
</s>
</p>
<p>
<s>
Lenka	Lenka	k1gFnSc1	Lenka
Cenková	Cenkový	k2eAgFnSc1d1	Cenková
–	–	k?	–
bývalá	bývalý	k2eAgFnSc1d1	bývalá
tenistka	tenistka	k1gFnSc1	tenistka
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Cieslarová	Cieslarový	k2eAgFnSc1d1	Cieslarová
–	–	k?	–
orientační	orientační	k2eAgFnSc1d1	orientační
běžkyně	běžkyně	k1gFnSc1	běžkyně
</s>
</p>
<p>
<s>
Michaela	Michaela	k1gFnSc1	Michaela
Dolinová	Dolinový	k2eAgFnSc1d1	Dolinový
–	–	k?	–
moderátorka	moderátorka	k1gFnSc1	moderátorka
</s>
</p>
<p>
<s>
Renata	Renata	k1gFnSc1	Renata
Drössler	Drössler	k1gInSc1	Drössler
–	–	k?	–
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Ewa	Ewa	k?	Ewa
Farna	Faren	k2eAgFnSc1d1	Farna
–	–	k?	–
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Gavendová-Handzlová	Gavendová-Handzlový	k2eAgFnSc1d1	Gavendová-Handzlový
–	–	k?	–
závodnice	závodnice	k1gFnSc1	závodnice
v	v	k7c6	v
orientačním	orientační	k2eAgInSc6d1	orientační
běhu	běh	k1gInSc6	běh
též	též	k9	též
trenerka	trenerka	k1gFnSc1	trenerka
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Gawlas	Gawlas	k1gMnSc1	Gawlas
–	–	k?	–
senátor	senátor	k1gMnSc1	senátor
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc1	klus
–	–	k?	–
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Konvičková	Konvičkový	k2eAgFnSc1d1	Konvičková
–	–	k?	–
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Kraus	Kraus	k1gMnSc1	Kraus
–	–	k?	–
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Lasota	Lasot	k1gMnSc2	Lasot
–	–	k?	–
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Radovan	Radovan	k1gMnSc1	Radovan
Lipus	Lipus	k1gMnSc1	Lipus
–	–	k?	–
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
Martynek	Martynka	k1gFnPc2	Martynka
–	–	k?	–
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Ovčáček	ovčáček	k1gMnSc1	ovčáček
–	–	k?	–
umělec	umělec	k1gMnSc1	umělec
</s>
</p>
<p>
<s>
Soňa	Soňa	k1gFnSc1	Soňa
Pertlová	Pertlový	k2eAgFnSc1d1	Pertlový
–	–	k?	–
šachistka	šachistka	k1gFnSc1	šachistka
</s>
</p>
<p>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Rakowski	Rakowsk	k1gFnSc2	Rakowsk
–	–	k?	–
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Szurman	Szurman	k1gMnSc1	Szurman
–	–	k?	–
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Rusnok	Rusnok	k1gInSc1	Rusnok
–	–	k?	–
poslanec	poslanec	k1gMnSc1	poslanec
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Sikora	Sikor	k1gMnSc2	Sikor
–	–	k?	–
novinář	novinář	k1gMnSc1	novinář
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Staszko	Staszka	k1gFnSc5	Staszka
–	–	k?	–
hráč	hráč	k1gMnSc1	hráč
pokeru	poker	k1gInSc2	poker
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Svěrkoš	Svěrkoš	k1gMnSc1	Svěrkoš
–	–	k?	–
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Šiška	Šiška	k1gMnSc1	Šiška
–	–	k?	–
moderátor	moderátor	k1gMnSc1	moderátor
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Šulgan	Šulgan	k1gMnSc1	Šulgan
–	–	k?	–
boxer	boxer	k1gMnSc1	boxer
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Štafa	Štaf	k1gMnSc2	Štaf
–	–	k?	–
výtvarník	výtvarník	k1gMnSc1	výtvarník
</s>
</p>
<p>
<s>
Lešek	Lešek	k1gMnSc1	Lešek
Wronka	Wronka	k1gMnSc1	Wronka
–	–	k?	–
producent	producent	k1gMnSc1	producent
</s>
</p>
<p>
<s>
Stanisław	Stanisław	k?	Stanisław
Zahradnik	Zahradnik	k1gMnSc1	Zahradnik
–	–	k?	–
badatel	badatel	k1gMnSc1	badatel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Knybel	Knybel	k1gMnSc1	Knybel
–	–	k?	–
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
–	–	k?	–
starostové	starostová	k1gFnPc4	starostová
Třince	Třinec	k1gInSc2	Třinec
==	==	k?	==
</s>
</p>
<p>
<s>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
Ing.	ing.	kA	ing.
Franz	Franz	k1gMnSc1	Franz
Obtulowicz	Obtulowicz	k1gMnSc1	Obtulowicz
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
Robert	Robert	k1gMnSc1	Robert
Uhlig	Uhlig	k1gMnSc1	Uhlig
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
Josef	Josef	k1gMnSc1	Josef
Bohm	Bohm	k1gMnSc1	Bohm
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
Jan	Jan	k1gMnSc1	Jan
Renda	Renda	k1gMnSc1	Renda
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kořínek	Kořínek	k1gMnSc1	Kořínek
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Pazdírek	pazdírko	k1gNnPc2	pazdírko
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Piotr	Piotr	k1gInSc1	Piotr
Kornuta	Kornut	k2eAgFnSc1d1	Kornuta
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
Augustin	Augustin	k1gMnSc1	Augustin
Hulva	Hulvo	k1gNnSc2	Hulvo
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Dobroslav	Dobroslav	k1gMnSc1	Dobroslav
Lízal	lízat	k5eAaImAgMnS	lízat
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
Jan	Jan	k1gMnSc1	Jan
Kajzar	Kajzar	k1gMnSc1	Kajzar
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
Alois	Alois	k1gMnSc1	Alois
Matloch	Matloch	k1gMnSc1	Matloch
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
Werner	Werner	k1gMnSc1	Werner
Thiers	Thiers	k1gInSc4	Thiers
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Karl	Karl	k1gMnSc1	Karl
Schneider	Schneider	k1gMnSc1	Schneider
vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
</s>
</p>
<p>
<s>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
Franz	Franz	k1gInSc1	Franz
Hiller	Hiller	k1gInSc4	Hiller
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Franciszek	Franciszek	k1gInSc1	Franciszek
Pitucha	Pitucha	k1gFnSc1	Pitucha
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Bohumil	Bohumil	k1gMnSc1	Bohumil
Huvar	Huvar	k1gMnSc1	Huvar
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
Ludvík	Ludvík	k1gMnSc1	Ludvík
Uhlář	uhlář	k1gMnSc1	uhlář
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
Emil	Emil	k1gMnSc1	Emil
Straka	Straka	k1gMnSc1	Straka
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Jerzy	Jerza	k1gFnSc2	Jerza
Klus	klus	k1gInSc1	klus
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Josef	Josef	k1gMnSc1	Josef
Štvrtňa	Štvrtňa	k1gMnSc1	Štvrtňa
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
Edmund	Edmund	k1gMnSc1	Edmund
Solarski	Solarski	k1gNnSc2	Solarski
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
Josef	Josef	k1gMnSc1	Josef
Grycz	Grycz	k1gMnSc1	Grycz
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vencel	Vencel	k1gMnSc1	Vencel
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
Jan	Jan	k1gMnSc1	Jan
Kozielek	Kozielka	k1gFnPc2	Kozielka
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Pavel	Pavel	k1gMnSc1	Pavel
Prokesz	Prokesz	k1gMnSc1	Prokesz
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Ing.	ing.	kA	ing.
Gustav	Gustav	k1gMnSc1	Gustav
Bruk	Bruk	k1gMnSc1	Bruk
pověřen	pověřit	k5eAaPmNgMnS	pověřit
vedením	vedení	k1gNnSc7	vedení
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
Albert	Alberta	k1gFnPc2	Alberta
Kornuta	Kornut	k2eAgFnSc1d1	Kornuta
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
xxxx	xxxx	k1gInSc1	xxxx
Ing.	ing.	kA	ing.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Opletal	Opletal	k1gMnSc1	Opletal
starosta	starosta	k1gMnSc1	starosta
</s>
</p>
<p>
<s>
==	==	k?	==
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
a	a	k8xC	a
zvolení	zvolený	k2eAgMnPc1d1	zvolený
zastupitelé	zastupitel	k1gMnPc1	zastupitel
města	město	k1gNnSc2	město
Třinec	Třinec	k1gInSc4	Třinec
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc3	část
města	město	k1gNnSc2	město
a	a	k8xC	a
okolní	okolní	k2eAgFnSc2d1	okolní
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Části	část	k1gFnSc3	část
města	město	k1gNnSc2	město
Třinec	Třinec	k1gInSc4	Třinec
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Třinec	Třinec	k1gInSc1	Třinec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
částí	část	k1gFnPc2	část
na	na	k7c6	na
dvanácti	dvanáct	k4xCc6	dvanáct
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
Líštná	Líštný	k2eAgFnSc1d1	Líštná
</s>
</p>
<p>
<s>
Guty	Guta	k1gFnPc1	Guta
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
Líštná	Líštný	k2eAgFnSc1d1	Líštná
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
Karpentná	Karpentnat	k5eAaPmIp3nS	Karpentnat
</s>
</p>
<p>
<s>
Kojkovice	Kojkovice	k1gFnPc1	Kojkovice
</s>
</p>
<p>
<s>
Konská	Konskat	k5eAaPmIp3nS	Konskat
</s>
</p>
<p>
<s>
Lyžbice	Lyžbice	k1gFnSc1	Lyžbice
</s>
</p>
<p>
<s>
Nebory	Nebora	k1gFnPc1	Nebora
</s>
</p>
<p>
<s>
Oldřichovice	Oldřichovice	k1gFnSc1	Oldřichovice
</s>
</p>
<p>
<s>
Osůvky	Osůvek	k1gInPc1	Osůvek
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
Puncov	Puncov	k1gInSc1	Puncov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
samotný	samotný	k2eAgInSc1d1	samotný
Třinec	Třinec	k1gInSc1	Třinec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TyraV	TyraV	k?	TyraV
letech	let	k1gInPc6	let
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
obec	obec	k1gFnSc1	obec
Vendryně	Vendryně	k1gFnSc1	Vendryně
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
také	také	k9	také
Ropice	Ropice	k1gFnPc1	Ropice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
kopců	kopec	k1gInPc2	kopec
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Třinecka	Třinecko	k1gNnSc2	Třinecko
a	a	k8xC	a
Jablunkovska	Jablunkovsko	k1gNnSc2	Jablunkovsko
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Bielsko-Biała	Bielsko-Biała	k1gFnSc1	Bielsko-Biała
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Infocentrum	infocentrum	k1gNnSc1	infocentrum
Třinec	Třinec	k1gInSc1	Třinec
</s>
</p>
<p>
<s>
Třinecké	třinecký	k2eAgFnPc1d1	třinecká
železárny	železárna	k1gFnPc1	železárna
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Oceláři	ocelář	k1gMnSc6	ocelář
Třinec	Třinec	k1gInSc4	Třinec
</s>
</p>
<p>
<s>
FK	FK	kA	FK
Fotbal	fotbal	k1gInSc1	fotbal
Třinec	Třinec	k1gInSc1	Třinec
</s>
</p>
<p>
<s>
Beskydská	beskydský	k2eAgFnSc1d1	Beskydská
laťka	laťka	k1gFnSc1	laťka
</s>
</p>
<p>
<s>
Evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Třinec	Třinec	k1gInSc1	Třinec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Třinec	Třinec	k1gInSc1	Třinec
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
http://www.trinecko.cz	[url]	k1gInSc1	http://www.trinecko.cz
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
Infocentrum	infocentrum	k1gNnSc1	infocentrum
Třinec	Třinec	k1gInSc1	Třinec
–	–	k?	–
Lidická	lidický	k2eAgNnPc1d1	Lidické
541	[number]	k4	541
</s>
</p>
<p>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
Třinec	Třinec	k1gInSc1	Třinec
–	–	k?	–
Lidická	lidický	k2eAgNnPc5d1	Lidické
541	[number]	k4	541
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
města	město	k1gNnSc2	město
Třince	Třinec	k1gInSc2	Třinec
–	–	k?	–
Lidická	lidický	k2eAgFnSc1d1	Lidická
541	[number]	k4	541
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
dům	dům	k1gInSc1	dům
Trisia	Trisia	k1gFnSc1	Trisia
a.s.	a.s.	k?	a.s.
–	–	k?	–
nám.	nám.	k?	nám.
Svobody	svoboda	k1gFnSc2	svoboda
521	[number]	k4	521
</s>
</p>
<p>
<s>
Kino	kino	k1gNnSc1	kino
Kosmos	kosmos	k1gInSc1	kosmos
–	–	k?	–
Dukelská	dukelský	k2eAgNnPc5d1	Dukelské
689	[number]	k4	689
</s>
</p>
<p>
<s>
IFK	IFK	kA	IFK
-	-	kIx~	-
regionální	regionální	k2eAgFnSc2d1	regionální
televize	televize	k1gFnSc2	televize
Třinec	Třinec	k1gInSc4	Třinec
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
z	z	k7c2	z
Třince	Třinec	k1gInSc2	Třinec
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
</s>
</p>
