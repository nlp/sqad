<p>
<s>
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
MTG	MTG	kA	MTG
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sběratelská	sběratelský	k2eAgFnSc1d1	sběratelská
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
ji	on	k3xPp3gFnSc4	on
vydala	vydat	k5eAaPmAgFnS	vydat
firma	firma	k1gFnSc1	firma
Wizards	Wizardsa	k1gFnPc2	Wizardsa
of	of	k?	of
the	the	k?	the
Coast	Coast	k1gMnSc1	Coast
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
španělštiny	španělština	k1gFnSc2	španělština
<g/>
,	,	kIx,	,
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
korejštiny	korejština	k1gFnSc2	korejština
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc2d1	tradiční
i	i	k8xC	i
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
čínštiny	čínština	k1gFnSc2	čínština
<g/>
,	,	kIx,	,
japonštiny	japonština	k1gFnSc2	japonština
a	a	k8xC	a
ruštiny	ruština	k1gFnSc2	ruština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
prodává	prodávat	k5eAaImIp3nS	prodávat
původní	původní	k2eAgFnSc1d1	původní
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
hry	hra	k1gFnSc2	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc4	Gathering
je	být	k5eAaImIp3nS	být
sběratelská	sběratelský	k2eAgFnSc1d1	sběratelská
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
hraje	hrát	k5eAaImIp3nS	hrát
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
balíčkem	balíček	k1gInSc7	balíček
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
každého	každý	k3xTgMnSc4	každý
hráče	hráč	k1gMnSc4	hráč
jiný	jiný	k2eAgInSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgMnSc1	svůj
balíček	balíček	k1gMnSc1	balíček
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
z	z	k7c2	z
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
koupí	koupit	k5eAaPmIp3nP	koupit
nebo	nebo	k8xC	nebo
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Karet	kareta	k1gFnPc2	kareta
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
karty	karta	k1gFnPc1	karta
mají	mít	k5eAaImIp3nP	mít
mezi	mezi	k7c7	mezi
hráči	hráč	k1gMnPc7	hráč
různou	různý	k2eAgFnSc4d1	různá
cenu	cena	k1gFnSc4	cena
–	–	k?	–
karta	karta	k1gFnSc1	karta
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
dražší	drahý	k2eAgFnSc1d2	dražší
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
silnější	silný	k2eAgFnSc1d2	silnější
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
čím	co	k3yInSc7	co
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
těchto	tento	k3xDgFnPc2	tento
karet	kareta	k1gFnPc2	kareta
se	se	k3xPyFc4	se
vytiskl	vytisknout	k5eAaPmAgInS	vytisknout
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
vyhrát	vyhrát	k5eAaPmF	vyhrát
jen	jen	k9	jen
díky	díky	k7c3	díky
nákupu	nákup	k1gInSc2	nákup
těch	ten	k3xDgMnPc2	ten
nejdražších	drahý	k2eAgMnPc2d3	nejdražší
a	a	k8xC	a
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
karet	kareta	k1gFnPc2	kareta
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
karty	karta	k1gFnPc1	karta
v	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
spolu	spolu	k6eAd1	spolu
musejí	muset	k5eAaImIp3nP	muset
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
každý	každý	k3xTgInSc1	každý
balíček	balíček	k1gInSc1	balíček
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
balíčkům	balíček	k1gInPc3	balíček
silný	silný	k2eAgInSc4d1	silný
a	a	k8xC	a
proti	proti	k7c3	proti
jiným	jiný	k2eAgFnPc3d1	jiná
zase	zase	k9	zase
slabý	slabý	k2eAgMnSc1d1	slabý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc4	Gathering
představuje	představovat	k5eAaImIp3nS	představovat
čaroděje-sférochodce	čarodějeférochodce	k1gMnSc1	čaroděje-sférochodce
(	(	kIx(	(
<g/>
planeswalker	planeswalker	k1gMnSc1	planeswalker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
udolat	udolat	k5eAaPmF	udolat
nepřátelského	přátelský	k2eNgMnSc4d1	nepřátelský
čaroděje	čaroděj	k1gMnSc4	čaroděj
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
nestvůr	nestvůra	k1gFnPc2	nestvůra
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
karty	karta	k1gFnPc1	karta
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
balíčku	balíček	k1gInSc6	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
mají	mít	k5eAaImIp3nP	mít
všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc1	hráč
20	[number]	k4	20
životů	život	k1gInPc2	život
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
snížit	snížit	k5eAaPmF	snížit
počet	počet	k1gInSc1	počet
životů	život	k1gInPc2	život
druhého	druhý	k4xOgMnSc4	druhý
hráče	hráč	k1gMnSc4	hráč
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
docílit	docílit	k5eAaPmF	docílit
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
došly	dojít	k5eAaPmAgFnP	dojít
karty	karta	k1gFnPc1	karta
v	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gInSc4	on
otrávit	otrávit	k5eAaPmF	otrávit
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
nabyl	nabýt	k5eAaPmAgMnS	nabýt
10	[number]	k4	10
"	"	kIx"	"
<g/>
poison	poison	k1gNnSc1	poison
counterů	counter	k1gInPc2	counter
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
hry	hra	k1gFnSc2	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Hru	hra	k1gFnSc4	hra
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
americký	americký	k2eAgMnSc1d1	americký
student	student	k1gMnSc1	student
matematiky	matematika	k1gFnSc2	matematika
Richard	Richard	k1gMnSc1	Richard
Garfield	Garfield	k1gMnSc1	Garfield
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
ji	on	k3xPp3gFnSc4	on
firma	firma	k1gFnSc1	firma
Wizards	Wizards	k1gInSc1	Wizards
of	of	k?	of
the	the	k?	the
Coast	Coast	k1gInSc1	Coast
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
systém	systém	k1gInSc1	systém
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
turnajů	turnaj	k1gInPc2	turnaj
DCI	DCI	kA	DCI
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgNnSc2	jenž
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
změřit	změřit	k5eAaPmF	změřit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
online	onlinout	k5eAaPmIp3nS	onlinout
verze	verze	k1gFnSc1	verze
Magic	Magice	k1gFnPc2	Magice
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
Online	Onlin	k1gInSc5	Onlin
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc1d1	umožňující
hrát	hrát	k5eAaImF	hrát
hru	hra	k1gFnSc4	hra
a	a	k8xC	a
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
si	se	k3xPyFc3	se
karty	karta	k1gFnSc2	karta
hráčům	hráč	k1gMnPc3	hráč
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
po	po	k7c6	po
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Nesmíme	smět	k5eNaImIp1nP	smět
zapomenout	zapomenout	k5eAaPmF	zapomenout
na	na	k7c4	na
programy	program	k1gInPc4	program
Magic	Magice	k1gFnPc2	Magice
Workstation	workstation	k1gInSc2	workstation
nebo	nebo	k8xC	nebo
Cockatrice	Cockatrika	k1gFnSc6	Cockatrika
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc4d1	umožňující
hraní	hraní	k1gNnSc4	hraní
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
zadarmo	zadarmo	k6eAd1	zadarmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pravidla	pravidlo	k1gNnPc1	pravidlo
==	==	k?	==
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
Magic	Magice	k1gFnPc2	Magice
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Gathering	Gathering	k1gInSc1	Gathering
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
kompletní	kompletní	k2eAgInSc1d1	kompletní
popis	popis	k1gInSc1	popis
zdaleka	zdaleka	k6eAd1	zdaleka
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
rozsah	rozsah	k1gInSc1	rozsah
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
nově	nově	k6eAd1	nově
vydanou	vydaný	k2eAgFnSc7d1	vydaná
edicí	edice	k1gFnSc7	edice
přicházejí	přicházet	k5eAaImIp3nP	přicházet
karty	karta	k1gFnPc1	karta
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
doplnění	doplnění	k1gNnSc4	doplnění
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
jen	jen	k9	jen
ta	ten	k3xDgFnSc1	ten
základní	základní	k2eAgFnSc1d1	základní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Princip	princip	k1gInSc1	princip
===	===	k?	===
</s>
</p>
<p>
<s>
Magic	Magice	k1gInPc2	Magice
hrají	hrát	k5eAaImIp3nP	hrát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
obvykle	obvykle	k6eAd1	obvykle
dva	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jich	on	k3xPp3gFnPc2	on
však	však	k9	však
být	být	k5eAaImF	být
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
snížit	snížit	k5eAaPmF	snížit
životy	život	k1gInPc4	život
druhého	druhý	k4xOgMnSc4	druhý
hráče	hráč	k1gMnSc4	hráč
na	na	k7c4	na
0	[number]	k4	0
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
útočí	útočit	k5eAaImIp3nS	útočit
různými	různý	k2eAgFnPc7d1	různá
nestvůrami	nestvůra	k1gFnPc7	nestvůra
a	a	k8xC	a
sesílá	sesílat	k5eAaImIp3nS	sesílat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
za	za	k7c4	za
která	který	k3yRgNnPc4	který
platí	platit	k5eAaImIp3nP	platit
magickou	magický	k2eAgFnSc7d1	magická
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
manou	mana	k1gFnSc7	mana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barvy	barva	k1gFnSc2	barva
===	===	k?	===
</s>
</p>
<p>
<s>
Karty	karta	k1gFnPc1	karta
v	v	k7c6	v
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
barev	barva	k1gFnPc2	barva
<g/>
:	:	kIx,	:
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
barva	barva	k1gFnSc1	barva
představuje	představovat	k5eAaImIp3nS	představovat
jinou	jiný	k2eAgFnSc4d1	jiná
sféru	sféra	k1gFnSc4	sféra
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
trochu	trochu	k6eAd1	trochu
jiný	jiný	k2eAgInSc4d1	jiný
styl	styl	k1gInSc4	styl
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
silné	silný	k2eAgFnPc4d1	silná
a	a	k8xC	a
slabé	slabý	k2eAgFnPc4d1	slabá
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
kouzlo	kouzlo	k1gNnSc4	kouzlo
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zaplatit	zaplatit	k5eAaPmF	zaplatit
alespoň	alespoň	k9	alespoň
jednou	jednou	k6eAd1	jednou
manou	manout	k5eAaImIp3nP	manout
jeho	jeho	k3xOp3gFnPc1	jeho
barvy	barva	k1gFnPc1	barva
(	(	kIx(	(
<g/>
silnější	silný	k2eAgFnPc1d2	silnější
karty	karta	k1gFnPc1	karta
obvykle	obvykle	k6eAd1	obvykle
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
více	hodně	k6eAd2	hodně
many	mana	k1gFnSc2	mana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
produkují	produkovat	k5eAaImIp3nP	produkovat
karty	karta	k1gFnPc1	karta
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
základní	základní	k2eAgFnPc4d1	základní
země	zem	k1gFnPc4	zem
<g/>
:	:	kIx,	:
pláně	pláň	k1gFnPc4	pláň
(	(	kIx(	(
<g/>
Plains	Plains	k1gInSc1	Plains
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bažiny	bažina	k1gFnSc2	bažina
(	(	kIx(	(
<g/>
Swamps	Swamps	k1gInSc1	Swamps
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
Mountains	Mountains	k1gInSc1	Mountains
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Islands	Islands	k1gInSc1	Islands
<g/>
)	)	kIx)	)
a	a	k8xC	a
lesy	les	k1gInPc1	les
(	(	kIx(	(
<g/>
Forests	Forests	k1gInSc1	Forests
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
základních	základní	k2eAgFnPc2d1	základní
zemí	zem	k1gFnPc2	zem
může	moct	k5eAaImIp3nS	moct
vyrobit	vyrobit	k5eAaPmF	vyrobit
jednu	jeden	k4xCgFnSc4	jeden
manu	mana	k1gFnSc4	mana
za	za	k7c4	za
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
barva	barva	k1gFnSc1	barva
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
protilehlé	protilehlý	k2eAgFnPc4d1	protilehlá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
brány	brána	k1gFnPc1	brána
jako	jako	k8xC	jako
nepřátelské	přátelský	k2eNgInPc1d1	nepřátelský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
naděje	naděje	k1gFnSc2	naděje
a	a	k8xC	a
léčení	léčení	k1gNnSc2	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
bytosti	bytost	k1gFnPc1	bytost
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
různí	různý	k2eAgMnPc1d1	různý
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
,	,	kIx,	,
paladinové	paladin	k1gMnPc1	paladin
<g/>
,	,	kIx,	,
léčitelé	léčitel	k1gMnPc1	léčitel
<g/>
,	,	kIx,	,
klerici	klerik	k1gMnPc1	klerik
a	a	k8xC	a
andělé	anděl	k1gMnPc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
především	především	k9	především
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
bílá	bílý	k2eAgNnPc4d1	bílé
kouzla	kouzlo	k1gNnPc4	kouzlo
většinou	většinou	k6eAd1	většinou
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
vlastních	vlastní	k2eAgFnPc2d1	vlastní
bytostí	bytost	k1gFnPc2	bytost
nebo	nebo	k8xC	nebo
přidávání	přidávání	k1gNnPc2	přidávání
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Bílou	bílý	k2eAgFnSc4d1	bílá
manu	mana	k1gFnSc4	mana
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
pláně	pláň	k1gFnPc4	pláň
<g/>
,	,	kIx,	,
nepřáteli	nepřítel	k1gMnPc7	nepřítel
bílé	bílý	k2eAgFnPc1d1	bílá
jsou	být	k5eAaImIp3nP	být
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
<g/>
Černá	černý	k2eAgFnSc1d1	černá
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
rozkladu	rozklad	k1gInSc2	rozklad
<g/>
,	,	kIx,	,
záhuby	záhuba	k1gFnSc2	záhuba
a	a	k8xC	a
záhrobí	záhrobí	k1gNnSc2	záhrobí
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
bytosti	bytost	k1gFnPc1	bytost
představují	představovat	k5eAaImIp3nP	představovat
nemrtví	nemrtvý	k1gMnPc1	nemrtvý
<g/>
,	,	kIx,	,
krysy	krysa	k1gFnPc1	krysa
<g/>
,	,	kIx,	,
upíři	upír	k1gMnPc1	upír
<g/>
,	,	kIx,	,
přízraky	přízrak	k1gInPc1	přízrak
<g/>
,	,	kIx,	,
démoni	démon	k1gMnPc1	démon
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hře	hra	k1gFnSc6	hra
přímé	přímý	k2eAgNnSc4d1	přímé
zabíjení	zabíjení	k1gNnSc4	zabíjení
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
potvor	potvora	k1gFnPc2	potvora
a	a	k8xC	a
navracení	navracení	k1gNnSc1	navracení
vlastních	vlastní	k2eAgMnPc2d1	vlastní
zabitých	zabitý	k1gMnPc2	zabitý
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
černá	černá	k1gFnSc1	černá
kouzla	kouzlo	k1gNnSc2	kouzlo
nejčastěji	často	k6eAd3	často
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
soupeřovy	soupeřův	k2eAgFnPc1d1	soupeřova
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
vyhazují	vyhazovat	k5eAaImIp3nP	vyhazovat
mu	on	k3xPp3gMnSc3	on
karty	karta	k1gFnPc4	karta
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
nebo	nebo	k8xC	nebo
oživují	oživovat	k5eAaImIp3nP	oživovat
vlastní	vlastní	k2eAgFnPc1d1	vlastní
zabité	zabitý	k2eAgFnPc1d1	zabitá
bytosti	bytost	k1gFnPc1	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Černou	černý	k2eAgFnSc4d1	černá
manu	mana	k1gFnSc4	mana
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
bažiny	bažina	k1gFnPc1	bažina
<g/>
,	,	kIx,	,
nepřáteli	nepřítel	k1gMnPc7	nepřítel
černé	černá	k1gFnSc2	černá
jsou	být	k5eAaImIp3nP	být
bílá	bílý	k2eAgNnPc1d1	bílé
a	a	k8xC	a
zelená	zelený	k2eAgNnPc1d1	zelené
<g/>
.	.	kIx.	.
<g/>
Červená	červené	k1gNnPc1	červené
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zkázy	zkáza	k1gFnSc2	zkáza
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
bytosti	bytost	k1gFnPc1	bytost
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
goblini	goblin	k2eAgMnPc1d1	goblin
<g/>
,	,	kIx,	,
orkové	orkus	k1gMnPc1	orkus
<g/>
,	,	kIx,	,
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
draci	drak	k1gMnPc1	drak
a	a	k8xC	a
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
edic	edice	k1gFnPc2	edice
ďáblové	ďábel	k1gMnPc1	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
karty	karta	k1gFnPc1	karta
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
především	především	k9	především
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
agresi	agrese	k1gFnSc4	agrese
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
bývají	bývat	k5eAaImIp3nP	bývat
červená	červený	k2eAgNnPc1d1	červené
kouzla	kouzlo	k1gNnPc1	kouzlo
zaměřena	zaměřen	k2eAgNnPc1d1	zaměřeno
na	na	k7c6	na
zraňování	zraňování	k1gNnSc6	zraňování
soupeře	soupeř	k1gMnSc2	soupeř
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc2	jeho
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Červenou	červený	k2eAgFnSc4d1	červená
manu	mana	k1gFnSc4	mana
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
nepřáteli	nepřítel	k1gMnPc7	nepřítel
červené	červená	k1gFnSc2	červená
jsou	být	k5eAaImIp3nP	být
bílá	bílý	k2eAgNnPc1d1	bílé
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
iluzí	iluze	k1gFnPc2	iluze
a	a	k8xC	a
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
modrými	modrý	k2eAgFnPc7d1	modrá
bytostmi	bytost	k1gFnPc7	bytost
najdeme	najít	k5eAaPmIp1nP	najít
různé	různý	k2eAgMnPc4d1	různý
vodní	vodní	k2eAgMnPc4d1	vodní
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
létající	létající	k2eAgMnPc4d1	létající
obludy	obluda	k1gFnSc2	obluda
nebo	nebo	k8xC	nebo
čaroděje	čaroděj	k1gMnSc2	čaroděj
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
manipulace	manipulace	k1gFnSc2	manipulace
a	a	k8xC	a
modrá	modrý	k2eAgNnPc4d1	modré
kouzla	kouzlo	k1gNnPc4	kouzlo
umějí	umět	k5eAaImIp3nP	umět
rušit	rušit	k5eAaImF	rušit
soupeřova	soupeřův	k2eAgNnPc4d1	soupeřovo
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
jeho	jeho	k3xOp3gFnPc4	jeho
karty	karta	k1gFnPc4	karta
nebo	nebo	k8xC	nebo
mu	on	k3xPp3gMnSc3	on
vracet	vracet	k5eAaImF	vracet
vyložené	vyložený	k2eAgFnPc4d1	vyložená
karty	karta	k1gFnPc4	karta
na	na	k7c4	na
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
můžou	můžou	k?	můžou
i	i	k8xC	i
pomoci	pomoc	k1gFnSc2	pomoc
s	s	k7c7	s
lízáním	lízání	k1gNnSc7	lízání
nových	nový	k2eAgFnPc2d1	nová
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
přidáváním	přidávání	k1gNnSc7	přidávání
kol	kola	k1gFnPc2	kola
<g/>
.	.	kIx.	.
</s>
<s>
Modrou	modrý	k2eAgFnSc4d1	modrá
manu	mana	k1gFnSc4	mana
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
nepřáteli	nepřítel	k1gMnPc7	nepřítel
modré	modrý	k2eAgFnSc2d1	modrá
jsou	být	k5eAaImIp3nP	být
zelená	zelený	k2eAgFnSc1d1	zelená
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
<g/>
Zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
bujení	bujení	k1gNnSc2	bujení
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgFnSc2d1	Zelené
bytosti	bytost	k1gFnSc2	bytost
jsou	být	k5eAaImIp3nP	být
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
druidi	druid	k1gMnPc1	druid
<g/>
,	,	kIx,	,
dryády	dryáda	k1gFnPc1	dryáda
a	a	k8xC	a
rozličná	rozličný	k2eAgNnPc1d1	rozličné
stvoření	stvoření	k1gNnPc1	stvoření
z	z	k7c2	z
hloubi	hloub	k1gFnSc2	hloub
hvozdu	hvozd	k1gInSc2	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
zde	zde	k6eAd1	zde
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
čistou	čistý	k2eAgFnSc4d1	čistá
brutální	brutální	k2eAgFnSc4d1	brutální
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
zelená	zelený	k2eAgNnPc4d1	zelené
kouzla	kouzlo	k1gNnPc4	kouzlo
nejčastěji	často	k6eAd3	často
zaměřena	zaměřit	k5eAaPmNgNnP	zaměřit
na	na	k7c6	na
posilování	posilování	k1gNnSc6	posilování
nestvůr	nestvůra	k1gFnPc2	nestvůra
a	a	k8xC	a
na	na	k7c4	na
přidávání	přidávání	k1gNnSc4	přidávání
many	mana	k1gFnSc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
Zelenou	zelený	k2eAgFnSc4d1	zelená
manu	mana	k1gFnSc4	mana
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
nepřáteli	nepřítel	k1gMnPc7	nepřítel
zelené	zelená	k1gFnSc2	zelená
jsou	být	k5eAaImIp3nP	být
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
<g/>
Každé	každý	k3xTgNnSc4	každý
kouzlo	kouzlo	k1gNnSc4	kouzlo
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zaplacení	zaplacení	k1gNnSc6	zaplacení
několika	několik	k4yIc2	několik
man	mana	k1gFnPc2	mana
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
man	mana	k1gFnPc2	mana
libovolné	libovolný	k2eAgFnSc2d1	libovolná
barvy	barva	k1gFnSc2	barva
nebo	nebo	k8xC	nebo
bezbarvých	bezbarvý	k2eAgInPc2d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
vícebarevná	vícebarevný	k2eAgNnPc4d1	vícebarevné
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zaplatit	zaplatit	k5eAaPmF	zaplatit
manami	mana	k1gFnPc7	mana
více	hodně	k6eAd2	hodně
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
artefakty	artefakt	k1gInPc1	artefakt
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgInPc1d1	bezbarvý
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gFnPc4	on
manou	manout	k5eAaImIp3nP	manout
libovolné	libovolný	k2eAgFnPc4d1	libovolná
barvy	barva	k1gFnPc4	barva
nebo	nebo	k8xC	nebo
bezbarvou	bezbarvý	k2eAgFnSc4d1	bezbarvá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
edicích	edice	k1gFnPc6	edice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
za	za	k7c4	za
která	který	k3yIgNnPc4	který
je	on	k3xPp3gNnPc4	on
třeba	třeba	k6eAd1	třeba
zaplatit	zaplatit	k5eAaPmF	zaplatit
manou	mana	k1gFnSc7	mana
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
stanovených	stanovený	k2eAgFnPc2d1	stanovená
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zelenou	zelená	k1gFnSc7	zelená
nebo	nebo	k8xC	nebo
bílou	bílý	k2eAgFnSc7d1	bílá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhy	druh	k1gInPc4	druh
karet	kareta	k1gFnPc2	kareta
===	===	k?	===
</s>
</p>
<p>
<s>
Karty	karta	k1gFnPc1	karta
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
významu	význam	k1gInSc2	význam
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Permanent	permanent	k1gInSc1	permanent
<g/>
:	:	kIx,	:
Souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
po	po	k7c6	po
seslání	seslání	k1gNnSc6	seslání
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
(	(	kIx(	(
<g/>
dokud	dokud	k6eAd1	dokud
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
nebo	nebo	k8xC	nebo
něco	něco	k3yInSc1	něco
neodstraní	odstranit	k5eNaPmIp3nS	odstranit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Land	Land	k1gInSc1	Land
(	(	kIx(	(
<g/>
země	zem	k1gFnPc1	zem
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Země	zem	k1gFnPc1	zem
zpravidla	zpravidla	k6eAd1	zpravidla
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
manu	mana	k1gFnSc4	mana
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
pět	pět	k4xCc4	pět
základních	základní	k2eAgFnPc2d1	základní
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
pláně	pláň	k1gFnPc1	pláň
<g/>
,	,	kIx,	,
bažiny	bažina	k1gFnPc1	bažina
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
lesy	les	k1gInPc1	les
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
jednu	jeden	k4xCgFnSc4	jeden
manu	mana	k1gFnSc4	mana
jedné	jeden	k4xCgFnSc2	jeden
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
nezákladních	základní	k2eNgFnPc2d1	základní
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umějí	umět	k5eAaImIp3nP	umět
vyrábět	vyrábět	k5eAaImF	vyrábět
manu	mana	k1gFnSc4	mana
více	hodně	k6eAd2	hodně
barev	barva	k1gFnPc2	barva
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
další	další	k2eAgFnSc4d1	další
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
země	země	k1gFnSc1	země
City	City	k1gFnSc2	City
of	of	k?	of
Brass	Brass	k1gInSc1	Brass
umí	umět	k5eAaImIp3nS	umět
vyrobit	vyrobit	k5eAaPmF	vyrobit
manu	mana	k1gFnSc4	mana
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
z	z	k7c2	z
pěti	pět	k4xCc2	pět
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
<g/>
,	,	kIx,	,
zraní	zranit	k5eAaPmIp3nS	zranit
svého	svůj	k3xOyFgMnSc4	svůj
majitele	majitel	k1gMnSc4	majitel
za	za	k7c4	za
1	[number]	k4	1
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
smí	smět	k5eAaImIp3nS	smět
zdarma	zdarma	k6eAd1	zdarma
vyložit	vyložit	k5eAaPmF	vyložit
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
kola	kola	k1gFnSc1	kola
jednu	jeden	k4xCgFnSc4	jeden
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Karty	karta	k1gFnPc1	karta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
nazývají	nazývat	k5eAaImIp3nP	nazývat
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Creature	Creatur	k1gMnSc5	Creatur
(	(	kIx(	(
<g/>
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
nestvůry	nestvůra	k1gFnPc1	nestvůra
<g/>
,	,	kIx,	,
příšery	příšera	k1gFnPc1	příšera
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
hráčovu	hráčův	k2eAgFnSc4d1	hráčova
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
bytost	bytost	k1gFnSc1	bytost
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
dolním	dolní	k2eAgInSc6d1	dolní
rohu	roh	k1gInSc6	roh
uvedenu	uveden	k2eAgFnSc4d1	uvedena
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc4	počet
zranění	zranění	k1gNnSc2	zranění
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bytost	bytost	k1gFnSc1	bytost
způsobí	způsobit	k5eAaPmIp3nS	způsobit
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
0	[number]	k4	0
a	a	k8xC	a
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc4	počet
zranění	zranění	k1gNnSc2	zranění
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
sama	sám	k3xTgMnSc4	sám
dostat	dostat	k5eAaPmF	dostat
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
bytostí	bytost	k1gFnPc2	bytost
nebo	nebo	k8xC	nebo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
odolnost	odolnost	k1gFnSc4	odolnost
0	[number]	k4	0
<g/>
,	,	kIx,	,
creatura	creatura	k1gFnSc1	creatura
automaticky	automaticky	k6eAd1	automaticky
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Sílu	síla	k1gFnSc4	síla
i	i	k8xC	i
odolnost	odolnost	k1gFnSc4	odolnost
lze	lze	k6eAd1	lze
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
i	i	k8xC	i
snižovat	snižovat	k5eAaImF	snižovat
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Enchantment	Enchantment	k1gInSc1	Enchantment
(	(	kIx(	(
<g/>
očarování	očarování	k1gNnSc2	očarování
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
vykládají	vykládat	k5eAaImIp3nP	vykládat
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
a	a	k8xC	a
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
pak	pak	k6eAd1	pak
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
očarování	očarování	k1gNnSc6	očarování
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yRnSc4	co
lze	lze	k6eAd1	lze
toto	tento	k3xDgNnSc4	tento
očarování	očarování	k1gNnSc4	očarování
zahrát	zahrát	k5eAaPmF	zahrát
a	a	k8xC	a
co	co	k3yRnSc4	co
toto	tento	k3xDgNnSc1	tento
očarování	očarování	k1gNnSc1	očarování
po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
očarování	očarování	k1gNnPc1	očarování
jsou	být	k5eAaImIp3nP	být
lokální	lokální	k2eAgFnPc1d1	lokální
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
vynášena	vynášet	k5eAaImNgNnP	vynášet
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kartu	karta	k1gFnSc4	karta
na	na	k7c6	na
stole	stol	k1gInSc6	stol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
očarování	očarování	k1gNnSc4	očarování
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
země	zem	k1gFnSc2	zem
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pak	pak	k6eAd1	pak
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
posilují	posilovat	k5eAaImIp3nP	posilovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Giant	Giant	k1gMnSc1	Giant
Strength	Strength	k1gMnSc1	Strength
je	být	k5eAaImIp3nS	být
očarování	očarování	k1gNnSc4	očarování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
sílu	síla	k1gFnSc4	síla
i	i	k8xC	i
odolnost	odolnost	k1gFnSc4	odolnost
očarované	očarovaný	k2eAgFnSc2d1	očarovaná
bytosti	bytost	k1gFnSc2	bytost
o	o	k7c4	o
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
karta	karta	k1gFnSc1	karta
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
i	i	k9	i
její	její	k3xOp3gNnSc1	její
očarování	očarování	k1gNnSc1	očarování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Artifact	Artifact	k1gInSc1	Artifact
(	(	kIx(	(
<g/>
artefakty	artefakt	k1gInPc1	artefakt
<g/>
)	)	kIx)	)
představují	představovat	k5eAaImIp3nP	představovat
magické	magický	k2eAgInPc1d1	magický
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
očarování	očarování	k1gNnPc4	očarování
–	–	k?	–
vykládají	vykládat	k5eAaImIp3nP	vykládat
se	se	k3xPyFc4	se
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
hru	hra	k1gFnSc4	hra
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Artefakty	artefakt	k1gInPc1	artefakt
nestojí	stát	k5eNaImIp3nP	stát
žádnou	žádný	k3yNgFnSc4	žádný
barevnou	barevný	k2eAgFnSc4d1	barevná
manu	mana	k1gFnSc4	mana
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
libovolnou	libovolný	k2eAgFnSc7d1	libovolná
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
i	i	k9	i
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
artefaktů	artefakt	k1gInPc2	artefakt
jsou	být	k5eAaImIp3nP	být
artefaktové	artefaktový	k2eAgFnPc1d1	artefaktová
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
artefaktu	artefakt	k1gInSc2	artefakt
a	a	k8xC	a
bytosti	bytost	k1gFnSc2	bytost
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
jen	jen	k9	jen
manou	mana	k1gFnSc7	mana
libovolné	libovolný	k2eAgFnSc2d1	libovolná
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
druhem	druh	k1gInSc7	druh
artefaktů	artefakt	k1gInPc2	artefakt
je	být	k5eAaImIp3nS	být
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
výbavu	výbava	k1gFnSc4	výbava
(	(	kIx(	(
<g/>
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
brnění	brnění	k1gNnSc1	brnění
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
přidělit	přidělit	k5eAaPmF	přidělit
některé	některý	k3yIgFnPc4	některý
své	svůj	k3xOyFgFnPc4	svůj
bytosti	bytost	k1gFnPc4	bytost
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Planeswalker	Planeswalker	k1gMnSc1	Planeswalker
(	(	kIx(	(
<g/>
sférochodec	sférochodec	k1gMnSc1	sférochodec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
karty	karta	k1gFnSc2	karta
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
permanent	permanent	k1gInSc4	permanent
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc4	ten
subtyp	subtyp	k1gMnSc1	subtyp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
s	s	k7c7	s
několika	několik	k4yIc7	několik
loyalty	loyalt	k1gInPc7	loyalt
countery	counter	k1gInPc1	counter
(	(	kIx(	(
<g/>
body	bod	k1gInPc1	bod
<g/>
/	/	kIx~	/
<g/>
žetony	žeton	k1gInPc1	žeton
loajality	loajalita	k1gFnSc2	loajalita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
až	až	k6eAd1	až
čtyři	čtyři	k4xCgFnPc4	čtyři
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
mu	on	k3xPp3gMnSc3	on
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
nebo	nebo	k8xC	nebo
snižují	snižovat	k5eAaImIp3nP	snižovat
počet	počet	k1gInSc4	počet
loyalt	loyalta	k1gFnPc2	loyalta
counterů	counter	k1gMnPc2	counter
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
nějaký	nějaký	k3yIgInSc4	nějaký
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
zahrát	zahrát	k5eAaPmF	zahrát
vždy	vždy	k6eAd1	vždy
jednu	jeden	k4xCgFnSc4	jeden
schopnost	schopnost	k1gFnSc4	schopnost
Sférochodce	Sférochodce	k1gMnSc2	Sférochodce
za	za	k7c4	za
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
loyalty	loyalt	k1gInPc4	loyalt
counterů	counter	k1gInPc2	counter
0	[number]	k4	0
<g/>
,	,	kIx,	,
Planeswalker	Planeswalker	k1gInSc1	Planeswalker
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
<g/>
Sorcery	Sorcer	k1gInPc1	Sorcer
a	a	k8xC	a
instant	instant	k?	instant
(	(	kIx(	(
<g/>
kouzlo	kouzlo	k1gNnSc1	kouzlo
a	a	k8xC	a
instant	instant	k?	instant
–	–	k?	–
žádný	žádný	k3yNgInSc4	žádný
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
se	se	k3xPyFc4	se
neujal	ujmout	k5eNaPmAgInS	ujmout
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
nevykládají	vykládat	k5eNaImIp3nP	vykládat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
zahrání	zahrání	k1gNnSc6	zahrání
vykonají	vykonat	k5eAaPmIp3nP	vykonat
nějaký	nějaký	k3yIgInSc4	nějaký
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
uveden	uvést	k5eAaPmNgMnS	uvést
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
ihned	ihned	k6eAd1	ihned
odložena	odložit	k5eAaPmNgFnS	odložit
na	na	k7c4	na
hromádku	hromádka	k1gFnSc4	hromádka
použitých	použitý	k2eAgInPc2d1	použitý
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
zničených	zničený	k2eAgFnPc2d1	zničená
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Efektů	efekt	k1gInPc2	efekt
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
:	:	kIx,	:
Například	například	k6eAd1	například
Terror	Terror	k1gInSc1	Terror
umí	umět	k5eAaImIp3nS	umět
nenávratně	návratně	k6eNd1	návratně
zabít	zabít	k5eAaPmF	zabít
jednu	jeden	k4xCgFnSc4	jeden
bytost	bytost	k1gFnSc4	bytost
(	(	kIx(	(
<g/>
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
černá	černý	k2eAgFnSc1d1	černá
nebo	nebo	k8xC	nebo
artefaktová	artefaktový	k2eAgFnSc1d1	artefaktová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shock	Shock	k1gInSc4	Shock
udělí	udělit	k5eAaPmIp3nP	udělit
vybrané	vybraný	k2eAgFnPc1d1	vybraná
bytosti	bytost	k1gFnPc1	bytost
nebo	nebo	k8xC	nebo
hráči	hráč	k1gMnPc1	hráč
2	[number]	k4	2
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
Stone	ston	k1gInSc5	ston
Rain	Rain	k1gNnSc1	Rain
zničí	zničit	k5eAaPmIp3nS	zničit
zvolenou	zvolený	k2eAgFnSc4d1	zvolená
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Sorcery	Sorcera	k1gFnPc4	Sorcera
smí	smět	k5eAaImIp3nS	smět
hráč	hráč	k1gMnSc1	hráč
zahrát	zahrát	k5eAaPmF	zahrát
jen	jen	k9	jen
ve	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
fázi	fáze	k1gFnSc6	fáze
svého	svůj	k3xOyFgNnSc2	svůj
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
instant	instant	k?	instant
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
hrát	hrát	k5eAaImF	hrát
téměř	téměř	k6eAd1	téměř
kdykoli	kdykoli	k6eAd1	kdykoli
<g/>
,	,	kIx,	,
i	i	k9	i
během	během	k7c2	během
soupeřova	soupeřův	k2eAgNnSc2d1	soupeřovo
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
<g/>
Plane	planout	k5eAaImIp3nS	planout
(	(	kIx(	(
<g/>
sféra	sféra	k1gFnSc1	sféra
<g/>
)	)	kIx)	)
Typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Planechase	Planechasa	k1gFnSc6	Planechasa
není	být	k5eNaImIp3nS	být
permanent	permanent	k1gInSc4	permanent
ani	ani	k8xC	ani
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
speciální	speciální	k2eAgFnPc1d1	speciální
karty	karta	k1gFnPc1	karta
podstatně	podstatně	k6eAd1	podstatně
ovlivňující	ovlivňující	k2eAgInSc4d1	ovlivňující
průběh	průběh	k1gInSc4	průběh
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
styl	styl	k1gInSc4	styl
hry	hra	k1gFnSc2	hra
zvaný	zvaný	k2eAgInSc4d1	zvaný
Planar	Planar	k1gInSc4	Planar
Magic	Magice	k1gFnPc2	Magice
a	a	k8xC	a
nedají	dát	k5eNaPmIp3nP	dát
se	se	k3xPyFc4	se
hrát	hrát	k5eAaImF	hrát
bez	bez	k7c2	bez
speciální	speciální	k2eAgFnSc2d1	speciální
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
karty	karta	k1gFnPc1	karta
se	se	k3xPyFc4	se
nelížou	lízat	k5eNaImIp3nP	lízat
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
speciálního	speciální	k2eAgInSc2d1	speciální
sférového	sférový	k2eAgInSc2d1	sférový
balíčku	balíček	k1gInSc2	balíček
<g/>
.	.	kIx.	.
<g/>
Tribal	Tribal	k1gInSc1	Tribal
Každá	každý	k3xTgFnSc1	každý
tribal	tribal	k1gInSc1	tribal
karta	karta	k1gFnSc1	karta
má	mít	k5eAaImIp3nS	mít
další	další	k2eAgInSc4d1	další
karetní	karetní	k2eAgInSc4d1	karetní
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
hraní	hraní	k1gNnSc1	hraní
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
pravidly	pravidlo	k1gNnPc7	pravidlo
pro	pro	k7c4	pro
onen	onen	k3xDgInSc1	onen
druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
<g/>
.	.	kIx.	.
</s>
<s>
Set	set	k1gInSc1	set
tribal	tribal	k1gInSc1	tribal
subtypů	subtyp	k1gInPc2	subtyp
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
subtypů	subtyp	k1gInPc2	subtyp
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Instanty	Instant	k1gInPc1	Instant
<g/>
,	,	kIx,	,
očarování	očarování	k1gNnPc1	očarování
i	i	k8xC	i
sorcery	sorcera	k1gFnPc1	sorcera
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
tribal	tribal	k1gInSc4	tribal
typ	typ	k1gInSc1	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Conspiracy	Conspiraca	k1gFnPc1	Conspiraca
(	(	kIx(	(
<g/>
spiknutí	spiknutí	k1gNnSc1	spiknutí
<g/>
,	,	kIx,	,
conspiracy	conspiracy	k1gInPc1	conspiracy
<g/>
)	)	kIx)	)
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
karty	karta	k1gFnSc2	karta
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Conspiracy	Conspiraca	k1gFnSc2	Conspiraca
<g/>
.	.	kIx.	.
</s>
<s>
Karty	karta	k1gFnPc1	karta
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c4	v
command	command	k1gInSc4	command
zóně	zóna	k1gFnSc3	zóna
a	a	k8xC	a
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Nepočítají	počítat	k5eNaImIp3nP	počítat
se	se	k3xPyFc4	se
do	do	k7c2	do
karetního	karetní	k2eAgInSc2d1	karetní
limitu	limit	k1gInSc2	limit
pro	pro	k7c4	pro
herní	herní	k2eAgInSc4d1	herní
balíček	balíček	k1gInSc4	balíček
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
různé	různý	k2eAgInPc1d1	různý
podtypy	podtyp	k1gInPc1	podtyp
(	(	kIx(	(
<g/>
artifact	artifact	k2eAgInSc1d1	artifact
equipment	equipment	k1gInSc1	equipment
<g/>
)	)	kIx)	)
a	a	k8xC	a
subtypy	subtyp	k1gInPc1	subtyp
(	(	kIx(	(
<g/>
artifact	artifact	k2eAgInSc1d1	artifact
creature	creatur	k1gMnSc5	creatur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podtypy	podtyp	k1gInPc1	podtyp
určují	určovat	k5eAaImIp3nP	určovat
další	další	k2eAgNnSc4d1	další
zařazení	zařazení	k1gNnSc4	zařazení
dané	daný	k2eAgFnSc2d1	daná
karty	karta	k1gFnSc2	karta
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
artifact	artifact	k1gMnSc1	artifact
equipment	equipment	k1gMnSc1	equipment
značí	značit	k5eAaImIp3nS	značit
artefakt	artefakt	k1gInSc4	artefakt
–	–	k?	–
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
veškerá	veškerý	k3xTgNnPc4	veškerý
další	další	k2eAgNnPc4d1	další
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Podtyp	podtyp	k1gInSc1	podtyp
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgInSc7	tento
artefaktem	artefakt	k1gInSc7	artefakt
se	se	k3xPyFc4	se
vybavuje	vybavovat	k5eAaImIp3nS	vybavovat
bytost	bytost	k1gFnSc1	bytost
(	(	kIx(	(
<g/>
artefakt	artefakt	k1gInSc1	artefakt
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přidává	přidávat	k5eAaImIp3nS	přidávat
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
její	její	k3xOp3gFnPc4	její
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
její	její	k3xOp3gInSc4	její
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
oba	dva	k4xCgInPc4	dva
názvy	název	k1gInPc4	název
u	u	k7c2	u
subtypu	subtyp	k1gInSc2	subtyp
se	se	k3xPyFc4	se
rovnají	rovnat	k5eAaImIp3nP	rovnat
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
použitelná	použitelný	k2eAgNnPc1d1	použitelné
pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
artifact	artifact	k1gMnSc1	artifact
creature	creatur	k1gMnSc5	creatur
lze	lze	k6eAd1	lze
zničit	zničit	k5eAaPmF	zničit
jak	jak	k8xS	jak
ničitelem	ničitel	k1gMnSc7	ničitel
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
posílit	posílit	k5eAaPmF	posílit
očarováním	očarování	k1gNnSc7	očarování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
seslat	seslat	k5eAaPmF	seslat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
bytosti	bytost	k1gFnSc6	bytost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzhled	vzhled	k1gInSc4	vzhled
karet	kareta	k1gFnPc2	kareta
===	===	k?	===
</s>
</p>
<p>
<s>
Nahoře	nahoře	k6eAd1	nahoře
vlevo	vlevo	k6eAd1	vlevo
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
kartě	karta	k1gFnSc6	karta
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
pak	pak	k6eAd1	pak
její	její	k3xOp3gFnSc1	její
manová	manový	k2eAgFnSc1d1	manová
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
počet	počet	k1gInSc1	počet
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
třeba	třeba	k6eAd1	třeba
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
číslice	číslice	k1gFnSc2	číslice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
počet	počet	k1gInSc4	počet
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
man	mana	k1gFnPc2	mana
libovolné	libovolný	k2eAgFnSc2d1	libovolná
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
symbolů	symbol	k1gInPc2	symbol
man	mana	k1gFnPc2	mana
určité	určitý	k2eAgFnSc2d1	určitá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
například	například	k6eAd1	například
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
kouzla	kouzlo	k1gNnSc2	kouzlo
uvedena	uveden	k2eAgFnSc1d1	uvedena
číslice	číslice	k1gFnSc1	číslice
3	[number]	k4	3
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
symboly	symbol	k1gInPc4	symbol
zelené	zelený	k2eAgFnSc2d1	zelená
many	mana	k1gFnSc2	mana
(	(	kIx(	(
<g/>
stromečky	stromeček	k1gInPc1	stromeček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouzlo	kouzlo	k1gNnSc4	kouzlo
stojí	stát	k5eAaImIp3nS	stát
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zelené	zelený	k2eAgInPc1d1	zelený
a	a	k8xC	a
zbylé	zbylý	k2eAgInPc1d1	zbylý
tři	tři	k4xCgInPc1	tři
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
barvy	barva	k1gFnPc4	barva
nebo	nebo	k8xC	nebo
bezbarvé	bezbarvý	k2eAgInPc4d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
textless	textless	k1gInSc4	textless
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
základní	základní	k2eAgFnPc4d1	základní
země	zem	k1gFnPc4	zem
z	z	k7c2	z
edice	edice	k1gFnSc2	edice
Zendikar	Zendikara	k1gFnPc2	Zendikara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
karty	karta	k1gFnSc2	karta
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
obrázek	obrázek	k1gInSc1	obrázek
bytosti	bytost	k1gFnSc2	bytost
nebo	nebo	k8xC	nebo
kouzla	kouzlo	k1gNnSc2	kouzlo
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
název	název	k1gInSc1	název
typu	typ	k1gInSc2	typ
karty	karta	k1gFnSc2	karta
(	(	kIx(	(
<g/>
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
a	a	k8xC	a
symbol	symbol	k1gInSc4	symbol
edice	edice	k1gFnSc2	edice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
karta	karta	k1gFnSc1	karta
vyšla	vyjít	k5eAaPmAgFnS	vyjít
(	(	kIx(	(
<g/>
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
textové	textový	k2eAgNnSc1d1	textové
okénko	okénko	k1gNnSc1	okénko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
karta	karta	k1gFnSc1	karta
umí	umět	k5eAaImIp3nS	umět
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
okénko	okénko	k1gNnSc1	okénko
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
flavor	flavor	k1gInSc1	flavor
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
roli	role	k1gFnSc4	role
tohoto	tento	k3xDgNnSc2	tento
kouzla	kouzlo	k1gNnSc2	kouzlo
nebo	nebo	k8xC	nebo
země	zem	k1gFnSc2	zem
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
kartami	karta	k1gFnPc7	karta
spojen	spojit	k5eAaPmNgInS	spojit
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
kurzívou	kurzíva	k1gFnSc7	kurzíva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dole	dole	k6eAd1	dole
na	na	k7c6	na
kartě	karta	k1gFnSc6	karta
je	být	k5eAaImIp3nS	být
vlevo	vlevo	k6eAd1	vlevo
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
jméno	jméno	k1gNnSc1	jméno
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
kolikátá	kolikátý	k4xOyRgFnSc1	kolikátý
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
karta	karta	k1gFnSc1	karta
z	z	k7c2	z
dané	daný	k2eAgFnSc2d1	daná
edice	edice	k1gFnSc2	edice
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kartu	karta	k1gFnSc4	karta
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vpravo	vpravo	k6eAd1	vpravo
dole	dole	k6eAd1	dole
uvedena	uvést	k5eAaPmNgFnS	uvést
její	její	k3xOp3gFnSc1	její
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
odolnost	odolnost	k1gFnSc1	odolnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bytost	bytost	k1gFnSc1	bytost
má	mít	k5eAaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
3	[number]	k4	3
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahájení	zahájení	k1gNnSc1	zahájení
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
bude	být	k5eAaImBp3nS	být
začínat	začínat	k5eAaImF	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
turnajovém	turnajový	k2eAgNnSc6d1	turnajové
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
hře	hra	k1gFnSc6	hra
vylosuje	vylosovat	k5eAaPmIp3nS	vylosovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hodem	hod	k1gInSc7	hod
kostky	kostka	k1gFnSc2	kostka
<g/>
)	)	kIx)	)
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
začne	začít	k5eAaPmIp3nS	začít
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
další	další	k2eAgFnSc6d1	další
hře	hra	k1gFnSc6	hra
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
učiní	učinit	k5eAaPmIp3nS	učinit
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prohrál	prohrát	k5eAaPmAgMnS	prohrát
minulou	minulý	k2eAgFnSc4d1	minulá
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
hráči	hráč	k1gMnPc1	hráč
navzájem	navzájem	k6eAd1	navzájem
zamíchají	zamíchat	k5eAaPmIp3nP	zamíchat
své	svůj	k3xOyFgInPc4	svůj
balíčky	balíček	k1gInPc4	balíček
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zaručí	zaručit	k5eAaPmIp3nS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
neseřadil	seřadit	k5eNaPmAgMnS	seřadit
karty	karta	k1gFnPc4	karta
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
balíčku	balíček	k1gInSc6	balíček
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
hodí	hodit	k5eAaPmIp3nS	hodit
<g/>
)	)	kIx)	)
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
vezme	vzít	k5eAaPmIp3nS	vzít
vrchních	vrchní	k2eAgFnPc2d1	vrchní
sedm	sedm	k4xCc1	sedm
karet	kareta	k1gFnPc2	kareta
svého	svůj	k3xOyFgInSc2	svůj
balíčku	balíček	k1gInSc2	balíček
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
startovní	startovní	k2eAgFnSc1d1	startovní
ruka	ruka	k1gFnSc1	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
svého	svůj	k3xOyFgInSc2	svůj
balíčku	balíček	k1gInSc2	balíček
položí	položit	k5eAaPmIp3nS	položit
lícem	líc	k1gInSc7	líc
dolů	dolů	k6eAd1	dolů
před	před	k7c4	před
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
mírně	mírně	k6eAd1	mírně
doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeho	jeho	k3xOp3gFnSc4	jeho
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
dobírá	dobírat	k5eAaImIp3nS	dobírat
další	další	k2eAgFnSc2d1	další
karty	karta	k1gFnSc2	karta
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
také	také	k9	také
vyhradí	vyhradit	k5eAaPmIp3nS	vyhradit
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
hřbitov	hřbitov	k1gInSc4	hřbitov
(	(	kIx(	(
<g/>
standardně	standardně	k6eAd1	standardně
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
lícem	líc	k1gInSc7	líc
nahoru	nahoru	k6eAd1	nahoru
odkládat	odkládat	k5eAaImF	odkládat
zabité	zabitý	k2eAgFnPc4d1	zabitá
nestvůry	nestvůra	k1gFnPc4	nestvůra
<g/>
,	,	kIx,	,
použitá	použitý	k2eAgNnPc4d1	Použité
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
odložené	odložený	k2eAgFnPc4d1	odložená
či	či	k8xC	či
vyhozené	vyhozený	k2eAgFnPc4d1	vyhozená
karty	karta	k1gFnPc4	karta
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
líznutí	líznutí	k1gNnSc6	líznutí
prvních	první	k4xOgFnPc2	první
sedmi	sedm	k4xCc2	sedm
karet	kareta	k1gFnPc2	kareta
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
využije	využít	k5eAaPmIp3nS	využít
tzv.	tzv.	kA	tzv.
pravidlo	pravidlo	k1gNnSc1	pravidlo
mulligan	mulligan	k1gMnSc1	mulligan
–	–	k?	–
hráč	hráč	k1gMnSc1	hráč
vloží	vložit	k5eAaPmIp3nS	vložit
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
ruku	ruka	k1gFnSc4	ruka
do	do	k7c2	do
balíčku	balíček	k1gInSc2	balíček
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
jej	on	k3xPp3gMnSc4	on
zamíchá	zamíchat	k5eAaPmIp3nS	zamíchat
a	a	k8xC	a
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
nové	nový	k2eAgFnPc4d1	nová
karty	karta	k1gFnPc4	karta
–	–	k?	–
ovšem	ovšem	k9	ovšem
teď	teď	k6eAd1	teď
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
méně	málo	k6eAd2	málo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
šest	šest	k4xCc4	šest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
ani	ani	k9	ani
ty	ten	k3xDgInPc4	ten
nechce	chtít	k5eNaImIp3nS	chtít
nechat	nechat	k5eAaPmF	nechat
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
je	on	k3xPp3gInPc4	on
zamíchá	zamíchat	k5eAaPmIp3nS	zamíchat
a	a	k8xC	a
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
zase	zase	k9	zase
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
méně	málo	k6eAd2	málo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
pět	pět	k4xCc4	pět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
možné	možný	k2eAgNnSc1d1	možné
opakovat	opakovat	k5eAaImF	opakovat
až	až	k9	až
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
nula	nula	k1gFnSc1	nula
karet	kareta	k1gFnPc2	kareta
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průběh	průběh	k1gInSc1	průběh
kola	kolo	k1gNnSc2	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
Hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
v	v	k7c6	v
kolech	kolo	k1gNnPc6	kolo
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kolo	kolo	k1gNnSc1	kolo
každého	každý	k3xTgMnSc2	každý
hráče	hráč	k1gMnSc2	hráč
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
fází	fáze	k1gFnPc2	fáze
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
z	z	k7c2	z
několika	několik	k4yIc2	několik
kroků	krok	k1gInPc2	krok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
<g/>
:	:	kIx,	:
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
odtapovací	odtapovací	k2eAgInSc4d1	odtapovací
krok	krok	k1gInSc4	krok
(	(	kIx(	(
<g/>
untap	untapit	k5eAaPmRp2nS	untapit
step	step	k1gInSc1	step
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hráč	hráč	k1gMnSc1	hráč
"	"	kIx"	"
<g/>
odtapne	odtapnout	k5eAaPmIp3nS	odtapnout
<g/>
"	"	kIx"	"
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
tapnuté	tapnutý	k2eAgFnPc4d1	tapnutá
karty	karta	k1gFnPc4	karta
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Tapnout	Tapnout	k5eAaImF	Tapnout
kartu	karta	k1gFnSc4	karta
znamená	znamenat	k5eAaImIp3nS	znamenat
otočit	otočit	k5eAaPmF	otočit
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
–	–	k?	–
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
karta	karta	k1gFnSc1	karta
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kole	kolo	k1gNnSc6	kolo
udělala	udělat	k5eAaPmAgFnS	udělat
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
může	moct	k5eAaImIp3nS	moct
udělat	udělat	k5eAaPmF	udělat
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
kolo	kolo	k1gNnSc4	kolo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bytost	bytost	k1gFnSc1	bytost
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
manu	mana	k1gFnSc4	mana
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtapnutím	Odtapnutí	k1gNnSc7	Odtapnutí
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
otočit	otočit	k5eAaPmF	otočit
kartu	karta	k1gFnSc4	karta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
svislé	svislý	k2eAgFnSc2d1	svislá
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kroku	krok	k1gInSc6	krok
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
všechny	všechen	k3xTgFnPc1	všechen
tapnuté	tapnutý	k2eAgFnPc1d1	tapnutá
karty	karta	k1gFnPc1	karta
odtapnou	odtapnout	k5eAaPmIp3nP	odtapnout
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kole	kolo	k1gNnSc6	kolo
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
tzv.	tzv.	kA	tzv.
upkeep	upkeep	k1gInSc1	upkeep
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hráč	hráč	k1gMnSc1	hráč
platí	platit	k5eAaImIp3nS	platit
různé	různý	k2eAgFnPc4d1	různá
přídavné	přídavný	k2eAgFnPc4d1	přídavná
ceny	cena	k1gFnPc4	cena
některých	některý	k3yIgFnPc2	některý
zvlášť	zvlášť	k6eAd1	zvlášť
silných	silný	k2eAgFnPc2d1	silná
bytostí	bytost	k1gFnPc2	bytost
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
efekty	efekt	k1gInPc4	efekt
některých	některý	k3yIgNnPc2	některý
kouzel	kouzlo	k1gNnPc2	kouzlo
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
žádné	žádný	k3yNgNnSc4	žádný
takové	takový	k3xDgNnSc4	takový
kouzlo	kouzlo	k1gNnSc4	kouzlo
ani	ani	k8xC	ani
bytost	bytost	k1gFnSc1	bytost
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
přeskočit	přeskočit	k5eAaPmF	přeskočit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
dobere	dobrat	k5eAaPmIp3nS	dobrat
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
jednu	jeden	k4xCgFnSc4	jeden
kartu	karta	k1gFnSc4	karta
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
(	(	kIx(	(
<g/>
hráč	hráč	k1gMnSc1	hráč
začínající	začínající	k2eAgFnSc4d1	začínající
hru	hra	k1gFnSc4	hra
si	se	k3xPyFc3	se
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
kartu	karta	k1gFnSc4	karta
nedobírá	dobírat	k5eNaImIp3nS	dobírat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc1	první
hlavní	hlavní	k2eAgFnSc1d1	hlavní
fáze	fáze	k1gFnSc1	fáze
<g/>
:	:	kIx,	:
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
vyložit	vyložit	k5eAaPmF	vyložit
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
jednu	jeden	k4xCgFnSc4	jeden
za	za	k7c2	za
kolo	kolo	k1gNnSc4	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
sesílat	sesílat	k5eAaImF	sesílat
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
vyvolat	vyvolat	k5eAaPmF	vyvolat
bytost	bytost	k1gFnSc4	bytost
<g/>
...	...	k?	...
Prostě	prostě	k9	prostě
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zamane	zamanout	k5eAaPmIp3nS	zamanout
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc1	pravidlo
to	ten	k3xDgNnSc4	ten
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
<g/>
.	.	kIx.	.
<g/>
Bojová	bojový	k2eAgFnSc1d1	bojová
fáze	fáze	k1gFnSc1	fáze
<g/>
:	:	kIx,	:
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
svými	svůj	k3xOyFgFnPc7	svůj
bytostmi	bytost	k1gFnPc7	bytost
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
nechce	chtít	k5eNaImIp3nS	chtít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kole	kolo	k1gNnSc6	kolo
útočit	útočit	k5eAaImF	útočit
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
fázi	fáze	k1gFnSc4	fáze
přeskočí	přeskočit	k5eAaPmIp3nS	přeskočit
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
fáze	fáze	k1gFnSc1	fáze
<g/>
:	:	kIx,	:
Totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
první	první	k4xOgFnSc7	první
hlavní	hlavní	k2eAgFnSc7d1	hlavní
fází	fáze	k1gFnSc7	fáze
<g/>
,	,	kIx,	,
povoleny	povolen	k2eAgFnPc4d1	povolena
stejné	stejný	k2eAgFnPc4d1	stejná
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
<g/>
Ukončovací	ukončovací	k2eAgFnPc4d1	ukončovací
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
napřed	napřed	k6eAd1	napřed
hráč	hráč	k1gMnSc1	hráč
odhodí	odhodit	k5eAaPmIp3nS	odhodit
karty	karta	k1gFnPc4	karta
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
jich	on	k3xPp3gMnPc2	on
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
zbylo	zbýt	k5eAaPmAgNnS	zbýt
7	[number]	k4	7
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
méně	málo	k6eAd2	málo
karet	kareta	k1gFnPc2	kareta
než	než	k8xS	než
7	[number]	k4	7
<g/>
,	,	kIx,	,
neodhazuje	odhazovat	k5eNaImIp3nS	odhazovat
nic	nic	k3yNnSc1	nic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vyprší	vypršet	k5eAaPmIp3nS	vypršet
různé	různý	k2eAgInPc4d1	různý
efekty	efekt	k1gInPc4	efekt
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
platí	platit	k5eAaImIp3nS	platit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
a	a	k8xC	a
bytostem	bytost	k1gFnPc3	bytost
se	se	k3xPyFc4	se
vyléčí	vyléčit	k5eAaPmIp3nP	vyléčit
všechna	všechen	k3xTgNnPc1	všechen
zranění	zranění	k1gNnPc1	zranění
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
kole	kolo	k1gNnSc6	kolo
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
hráč	hráč	k1gMnSc1	hráč
oznámí	oznámit	k5eAaPmIp3nS	oznámit
soupeři	soupeř	k1gMnSc3	soupeř
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
jeho	jeho	k3xOp3gNnSc4	jeho
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
mohou	moct	k5eAaImIp3nP	moct
ještě	ještě	k9	ještě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kole	kolo	k1gNnSc6	kolo
zahrát	zahrát	k5eAaPmF	zahrát
nějaké	nějaký	k3yIgMnPc4	nějaký
instanty	instant	k1gMnPc4	instant
nebo	nebo	k8xC	nebo
použít	použít	k5eAaPmF	použít
efekty	efekt	k1gInPc4	efekt
svých	svůj	k3xOyFgFnPc2	svůj
karet	kareta	k1gFnPc2	kareta
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sesílání	sesílání	k1gNnSc1	sesílání
kouzel	kouzlo	k1gNnPc2	kouzlo
+	+	kIx~	+
stack	stack	k1gInSc1	stack
(	(	kIx(	(
<g/>
várka	várka	k1gFnSc1	várka
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
hráč	hráč	k1gMnSc1	hráč
seslat	seslat	k5eAaPmF	seslat
kouzlo	kouzlo	k1gNnSc4	kouzlo
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
vyloží	vyložit	k5eAaPmIp3nS	vyložit
kartu	karta	k1gFnSc4	karta
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
kouzlem	kouzlo	k1gNnSc7	kouzlo
před	před	k7c4	před
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
jeho	jeho	k3xOp3gFnSc4	jeho
cenu	cena	k1gFnSc4	cena
tapnutím	tapnutí	k1gNnSc7	tapnutí
příslušných	příslušný	k2eAgFnPc2d1	příslušná
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
manu	mana	k1gFnSc4	mana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
kouzlo	kouzlo	k1gNnSc1	kouzlo
pak	pak	k6eAd1	pak
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
várky	várka	k1gFnSc2	várka
neboli	neboli	k8xC	neboli
stacku	stack	k1gInSc2	stack
a	a	k8xC	a
soupeř	soupeř	k1gMnSc1	soupeř
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
sesláním	seslání	k1gNnSc7	seslání
instantu	instant	k1gMnSc3	instant
nebo	nebo	k8xC	nebo
použitím	použití	k1gNnSc7	použití
nějakého	nějaký	k3yIgInSc2	nějaký
efektu	efekt	k1gInSc2	efekt
své	svůj	k3xOyFgFnSc2	svůj
karty	karta	k1gFnSc2	karta
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tak	tak	k6eAd1	tak
učiní	učinit	k5eAaPmIp3nS	učinit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
kouzlo	kouzlo	k1gNnSc4	kouzlo
nebo	nebo	k8xC	nebo
efekt	efekt	k1gInSc4	efekt
do	do	k7c2	do
stacku	stack	k1gInSc2	stack
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
první	první	k4xOgMnSc1	první
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
zahraná	zahraný	k2eAgNnPc1d1	zahrané
kouzla	kouzlo	k1gNnPc1	kouzlo
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc1	schopnost
"	"	kIx"	"
<g/>
vrší	vršit	k5eAaImIp3nS	vršit
<g/>
"	"	kIx"	"
v	v	k7c6	v
imaginárním	imaginární	k2eAgInSc6d1	imaginární
"	"	kIx"	"
<g/>
balíku	balík	k1gInSc6	balík
<g/>
"	"	kIx"	"
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žádný	žádný	k3yNgMnSc1	žádný
hráč	hráč	k1gMnSc1	hráč
nechce	chtít	k5eNaImIp3nS	chtít
dále	daleko	k6eAd2	daleko
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
kouzla	kouzlo	k1gNnPc1	kouzlo
ve	v	k7c6	v
stacku	stacko	k1gNnSc6	stacko
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
–	–	k?	–
u	u	k7c2	u
sorcery	sorcera	k1gFnSc2	sorcera
(	(	kIx(	(
<g/>
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
instantů	instant	k1gMnPc2	instant
se	se	k3xPyFc4	se
provedou	provést	k5eAaPmIp3nP	provést
jejich	jejich	k3xOp3gInPc1	jejich
efekty	efekt	k1gInPc1	efekt
<g/>
,	,	kIx,	,
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
očarování	očarování	k1gNnPc1	očarování
a	a	k8xC	a
artefakty	artefakt	k1gInPc1	artefakt
se	se	k3xPyFc4	se
vyloží	vyložit	k5eAaPmIp3nP	vyložit
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodnocování	vyhodnocování	k1gNnSc1	vyhodnocování
však	však	k9	však
probíhá	probíhat	k5eAaImIp3nS	probíhat
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
tohoto	tento	k3xDgInSc2	tento
"	"	kIx"	"
<g/>
balíčku	balíček	k1gInSc2	balíček
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
byla	být	k5eAaImAgNnP	být
kouzla	kouzlo	k1gNnPc1	kouzlo
seslána	seslat	k5eAaPmNgNnP	seslat
–	–	k?	–
to	ten	k3xDgNnSc4	ten
naposledy	naposledy	k6eAd1	naposledy
seslané	seslaný	k2eAgFnPc1d1	seslaná
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyhodnotí	vyhodnotit	k5eAaPmIp3nS	vyhodnotit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
přesměrovat	přesměrovat	k5eAaPmF	přesměrovat
nebo	nebo	k8xC	nebo
zrušit	zrušit	k5eAaPmF	zrušit
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
sesláno	seslat	k5eAaPmNgNnS	seslat
dříve	dříve	k6eAd2	dříve
<g/>
!	!	kIx.	!
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
zajímavým	zajímavý	k2eAgFnPc3d1	zajímavá
a	a	k8xC	a
nepředvídaným	předvídaný	k2eNgFnPc3d1	nepředvídaná
situacím	situace	k1gFnPc3	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
seslání	seslání	k1gNnSc1	seslání
kouzla	kouzlo	k1gNnSc2	kouzlo
může	moct	k5eAaImIp3nS	moct
dopadnout	dopadnout	k5eAaPmF	dopadnout
zcela	zcela	k6eAd1	zcela
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
hráč	hráč	k1gMnSc1	hráč
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
kouzla	kouzlo	k1gNnPc1	kouzlo
(	(	kIx(	(
<g/>
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hlavní	hlavní	k2eAgFnSc6d1	hlavní
fázi	fáze	k1gFnSc6	fáze
a	a	k8xC	a
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
stack	stack	k1gInSc1	stack
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
kola	kolo	k1gNnSc2	kolo
zahrát	zahrát	k5eAaPmF	zahrát
libovolný	libovolný	k2eAgInSc4d1	libovolný
počet	počet	k1gInSc4	počet
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
dost	dost	k6eAd1	dost
many	mana	k1gFnPc4	mana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Instant	Instant	k1gInSc1	Instant
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
slangově	slangově	k6eAd1	slangově
"	"	kIx"	"
<g/>
instanty	instant	k1gMnPc4	instant
<g/>
"	"	kIx"	"
může	moct	k5eAaImIp3nS	moct
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
hrát	hrát	k5eAaImF	hrát
téměř	téměř	k6eAd1	téměř
kdykoli	kdykoli	k6eAd1	kdykoli
<g/>
,	,	kIx,	,
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
i	i	k9	i
soupeřova	soupeřův	k2eAgNnPc4d1	soupeřovo
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
využít	využít	k5eAaPmF	využít
pravidlo	pravidlo	k1gNnSc1	pravidlo
o	o	k7c4	o
stacku	stacka	k1gFnSc4	stacka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
tohoto	tento	k3xDgNnSc2	tento
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
složitějšího	složitý	k2eAgInSc2d2	složitější
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
důležitého	důležitý	k2eAgNnSc2d1	důležité
pravidla	pravidlo	k1gNnSc2	pravidlo
si	se	k3xPyFc3	se
uvedeme	uvést	k5eAaPmIp1nP	uvést
pár	pár	k4xCyI	pár
příkladů	příklad	k1gInPc2	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mějme	mít	k5eAaImRp1nP	mít
vyloženou	vyložený	k2eAgFnSc4d1	vyložená
kartu	karta	k1gFnSc4	karta
Grizzly	grizzly	k1gMnPc4	grizzly
Bears	Bears	k1gInSc4	Bears
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potvora	potvora	k1gFnSc1	potvora
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
útočné	útočný	k2eAgNnSc4d1	útočné
i	i	k8xC	i
obranné	obranný	k2eAgNnSc4d1	obranné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nás	my	k3xPp1nPc4	my
nyní	nyní	k6eAd1	nyní
bude	být	k5eAaImBp3nS	být
zajímat	zajímat	k5eAaImF	zajímat
pouze	pouze	k6eAd1	pouze
její	její	k3xOp3gNnSc4	její
obranné	obranný	k2eAgNnSc4d1	obranné
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Dejme	dát	k5eAaPmRp1nP	dát
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
protivník	protivník	k1gMnSc1	protivník
chce	chtít	k5eAaImIp3nS	chtít
tuto	tento	k3xDgFnSc4	tento
naší	náš	k3xOp1gFnSc2	náš
kartu	karta	k1gFnSc4	karta
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
a	a	k8xC	a
použije	použít	k5eAaPmIp3nS	použít
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
kartu	karta	k1gFnSc4	karta
Shock	Shocka	k1gFnPc2	Shocka
(	(	kIx(	(
<g/>
instant	instant	k?	instant
<g/>
,	,	kIx,	,
zraňuje	zraňovat	k5eAaImIp3nS	zraňovat
za	za	k7c4	za
2	[number]	k4	2
životy	život	k1gInPc4	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vidno	vidno	k6eAd1	vidno
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdybychom	kdyby	kYmCp1nP	kdyby
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nic	nic	k3yNnSc1	nic
nedělali	dělat	k5eNaImAgMnP	dělat
<g/>
,	,	kIx,	,
náš	náš	k3xOp1gMnSc1	náš
medvěd	medvěd	k1gMnSc1	medvěd
by	by	kYmCp3nS	by
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
nechceme	chtít	k5eNaImIp1nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
soupeřův	soupeřův	k2eAgInSc4d1	soupeřův
Shock	Shock	k1gInSc4	Shock
zahrát	zahrát	k5eAaPmF	zahrát
Giant	Giant	k1gMnSc1	Giant
Growth	Growth	k1gMnSc1	Growth
(	(	kIx(	(
<g/>
taktéž	taktéž	k?	taktéž
instant	instant	k?	instant
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
nestvůře	nestvůra	k1gFnSc6	nestvůra
+3	+3	k4	+3
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
do	do	k7c2	do
konce	konec	k1gInSc2	konec
kola	kolo	k1gNnSc2	kolo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
instant	instant	k?	instant
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
zahrát	zahrát	k5eAaPmF	zahrát
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
karta	karta	k1gFnSc1	karta
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
na	na	k7c4	na
vrch	vrch	k1gInSc4	vrch
stacku	stack	k1gInSc2	stack
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
<g/>
:	:	kIx,	:
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stacku	stack	k1gInSc6	stack
naše	náš	k3xOp1gFnSc1	náš
karta	karta	k1gFnSc1	karta
Giant	Giant	k1gMnSc1	Giant
Growth	Growth	k1gMnSc1	Growth
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodnotí	vyhodnotit	k5eAaPmIp3nS	vyhodnotit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
náš	náš	k3xOp1gMnSc1	náš
méďa	méďa	k1gMnSc1	méďa
dostane	dostat	k5eAaPmIp3nS	dostat
+3	+3	k4	+3
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
udělá	udělat	k5eAaPmIp3nS	udělat
o	o	k7c4	o
dost	dost	k6eAd1	dost
silnější	silný	k2eAgFnSc4d2	silnější
potvoru	potvora	k1gFnSc4	potvora
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stacku	stack	k1gInSc6	stack
soupeřův	soupeřův	k2eAgInSc4d1	soupeřův
Shock	Shock	k1gInSc4	Shock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
udělí	udělit	k5eAaPmIp3nS	udělit
méďovi	méďa	k1gMnSc3	méďa
2	[number]	k4	2
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
méďa	méďa	k1gMnSc1	méďa
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
v	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
číslo	číslo	k1gNnSc1	číslo
5	[number]	k4	5
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgMnS	muset
udělit	udělit	k5eAaPmF	udělit
ještě	ještě	k9	ještě
3	[number]	k4	3
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
neudělil	udělit	k5eNaPmAgMnS	udělit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nám	my	k3xPp1nPc3	my
naše	náš	k3xOp1gFnSc1	náš
karta	karta	k1gFnSc1	karta
Grizzly	grizzly	k1gMnSc1	grizzly
Bears	Bears	k1gInSc4	Bears
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yQnSc1	co
kdyby	kdyby	kYmCp3nS	kdyby
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
opačná	opačný	k2eAgFnSc1d1	opačná
<g/>
:	:	kIx,	:
my	my	k3xPp1nPc1	my
bychom	by	kYmCp1nP	by
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
chtěli	chtít	k5eAaImAgMnP	chtít
našeho	náš	k3xOp1gMnSc4	náš
medvěda	medvěd	k1gMnSc4	medvěd
trochu	trochu	k6eAd1	trochu
posílit	posílit	k5eAaPmF	posílit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pro	pro	k7c4	pro
nastávající	nastávající	k2eAgInSc4d1	nastávající
útok	útok	k1gInSc4	útok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahrajeme	zahrát	k5eAaPmIp1nP	zahrát
Giant	Giant	k1gMnSc1	Giant
Growth	Growth	k1gMnSc1	Growth
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
stack	stack	k1gInSc4	stack
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soupeř	soupeř	k1gMnSc1	soupeř
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
reagovat	reagovat	k5eAaBmF	reagovat
a	a	k8xC	a
zahraje	zahrát	k5eAaPmIp3nS	zahrát
svůj	svůj	k3xOyFgInSc4	svůj
Shock	Shock	k1gInSc4	Shock
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
<g/>
:	:	kIx,	:
jako	jako	k8xS	jako
první	první	k4xOgFnSc4	první
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
stacku	stacka	k1gFnSc4	stacka
Shock	Shocka	k1gFnPc2	Shocka
našeho	náš	k3xOp1gMnSc2	náš
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Shock	Shock	k6eAd1	Shock
udělí	udělit	k5eAaPmIp3nS	udělit
naší	náš	k3xOp1gFnSc3	náš
kartě	karta	k1gFnSc3	karta
Grizzly	grizzly	k1gMnSc1	grizzly
Bears	Bears	k1gInSc4	Bears
2	[number]	k4	2
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
karta	karta	k1gFnSc1	karta
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
položena	položit	k5eAaPmNgFnS	položit
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stacku	stacko	k1gNnSc6	stacko
náš	náš	k3xOp1gMnSc1	náš
Giant	Giant	k1gMnSc1	Giant
Growth	Growth	k1gMnSc1	Growth
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ovšem	ovšem	k9	ovšem
cíluje	cílovat	k5eAaImIp3nS	cílovat
potvoru	potvora	k1gFnSc4	potvora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
už	už	k9	už
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vyhodnocena	vyhodnotit	k5eAaPmNgFnS	vyhodnotit
jako	jako	k9	jako
neplatná	platný	k2eNgFnSc1d1	neplatná
a	a	k8xC	a
položená	položený	k2eAgFnSc1d1	položená
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
dalších	další	k2eAgInPc2d1	další
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boj	boj	k1gInSc1	boj
===	===	k?	===
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgInSc7d3	nejčastější
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vyhrát	vyhrát	k5eAaPmF	vyhrát
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
bojové	bojový	k2eAgFnSc6d1	bojová
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
svými	svůj	k3xOyFgFnPc7	svůj
bytostmi	bytost	k1gFnPc7	bytost
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
oznámí	oznámit	k5eAaPmIp3nS	oznámit
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
soupeř	soupeř	k1gMnSc1	soupeř
nechce	chtít	k5eNaImIp3nS	chtít
nic	nic	k3yNnSc1	nic
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
samotný	samotný	k2eAgInSc1d1	samotný
boj	boj	k1gInSc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Útočící	útočící	k2eAgMnSc1d1	útočící
hráč	hráč	k1gMnSc1	hráč
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
bytostí	bytost	k1gFnPc2	bytost
budou	být	k5eAaImBp3nP	být
útočit	útočit	k5eAaImF	útočit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tapne	tapnout	k5eAaImIp3nS	tapnout
je	on	k3xPp3gNnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Nemohou	moct	k5eNaImIp3nP	moct
útočit	útočit	k5eAaImF	útočit
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
tapnuté	tapnutý	k2eAgFnPc1d1	tapnutá
<g/>
,	,	kIx,	,
a	a	k8xC	a
bytost	bytost	k1gFnSc1	bytost
nesmí	smět	k5eNaImIp3nS	smět
útočit	útočit	k5eAaImF	útočit
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
seslána	seslán	k2eAgFnSc1d1	seslána
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
únava	únava	k1gFnSc1	únava
z	z	k7c2	z
vyvolání	vyvolání	k1gNnSc2	vyvolání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
soupeř	soupeř	k1gMnSc1	soupeř
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
svými	svůj	k3xOyFgMnPc7	svůj
bytostmi	bytost	k1gFnPc7	bytost
útočící	útočící	k2eAgFnSc2d1	útočící
bytosti	bytost	k1gFnSc2	bytost
zablokuje	zablokovat	k5eAaPmIp3nS	zablokovat
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
útočící	útočící	k2eAgFnSc4d1	útočící
bytost	bytost	k1gFnSc4	bytost
může	moct	k5eAaImIp3nS	moct
zablokovat	zablokovat	k5eAaPmF	zablokovat
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
svými	svůj	k3xOyFgFnPc7	svůj
bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
tapnuté	tapnutý	k2eAgFnPc1d1	tapnutá
(	(	kIx(	(
<g/>
nelze	lze	k6eNd1	lze
tedy	tedy	k8xC	tedy
blokovat	blokovat	k5eAaImF	blokovat
bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
kole	kolo	k1gNnSc6	kolo
bránícího	bránící	k2eAgInSc2d1	bránící
se	se	k3xPyFc4	se
hráče	hráč	k1gMnPc4	hráč
útočily	útočit	k5eAaImAgFnP	útočit
–	–	k?	–
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
blokují	blokovat	k5eAaImIp3nP	blokovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
už	už	k6eAd1	už
netapují	tapovat	k5eNaPmIp3nP	tapovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
hrát	hrát	k5eAaImF	hrát
instanty	instant	k1gMnPc4	instant
na	na	k7c6	na
posilování	posilování	k1gNnSc6	posilování
svých	svůj	k3xOyFgFnPc2	svůj
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
oslabování	oslabování	k1gNnSc3	oslabování
soupeřových	soupeřův	k2eAgFnPc2d1	soupeřova
bytostí	bytost	k1gFnPc2	bytost
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
už	už	k9	už
žádný	žádný	k3yNgMnSc1	žádný
hráč	hráč	k1gMnSc1	hráč
žádné	žádný	k3yNgNnSc4	žádný
kouzlo	kouzlo	k1gNnSc4	kouzlo
nechce	chtít	k5eNaImIp3nS	chtít
nebo	nebo	k8xC	nebo
nemůže	moct	k5eNaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vyhodnocení	vyhodnocení	k1gNnSc3	vyhodnocení
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
útočící	útočící	k2eAgFnSc1d1	útočící
a	a	k8xC	a
blokující	blokující	k2eAgFnSc1d1	blokující
bytost	bytost	k1gFnSc1	bytost
udělí	udělit	k5eAaPmIp3nS	udělit
tolik	tolik	k4xDc4	tolik
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
zablokovány	zablokovat	k5eAaPmNgFnP	zablokovat
<g/>
,	,	kIx,	,
udělí	udělit	k5eAaPmIp3nP	udělit
zranění	zraněný	k2eAgMnPc1d1	zraněný
soupeři	soupeř	k1gMnPc1	soupeř
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc1	jeho
počet	počet	k1gInSc1	počet
životů	život	k1gInPc2	život
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
o	o	k7c4	o
počet	počet	k1gInSc4	počet
udělených	udělený	k2eAgNnPc2d1	udělené
zranění	zranění	k1gNnPc2	zranění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zablokované	zablokovaný	k2eAgFnPc1d1	zablokovaná
bytosti	bytost	k1gFnPc1	bytost
si	se	k3xPyFc3	se
udělí	udělit	k5eAaPmIp3nP	udělit
zranění	zranění	k1gNnSc4	zranění
navzájem	navzájem	k6eAd1	navzájem
současně	současně	k6eAd1	současně
s	s	k7c7	s
bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	on	k3xPp3gNnPc4	on
blokují	blokovat	k5eAaImIp3nP	blokovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nějaká	nějaký	k3yIgFnSc1	nějaký
bytost	bytost	k1gFnSc1	bytost
dostane	dostat	k5eAaPmIp3nS	dostat
stejně	stejně	k6eAd1	stejně
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
zranění	zraněný	k2eAgMnPc1d1	zraněný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc4	její
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odložena	odložit	k5eAaPmNgFnS	odložit
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Schopnosti	schopnost	k1gFnSc3	schopnost
karet	kareta	k1gFnPc2	kareta
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
kartě	karta	k1gFnSc6	karta
vykládané	vykládaný	k2eAgFnSc6d1	vykládaná
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
její	její	k3xOp3gFnPc1	její
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
jsou	být	k5eAaImIp3nP	být
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
platí	platit	k5eAaImIp3nP	platit
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
karta	karta	k1gFnSc1	karta
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
(	(	kIx(	(
<g/>
permanentní	permanentní	k2eAgFnSc4d1	permanentní
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
bílé	bílý	k2eAgNnSc1d1	bílé
očarování	očarování	k1gNnSc1	očarování
jménem	jméno	k1gNnSc7	jméno
Crusade	Crusad	k1gInSc5	Crusad
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
sílu	síla	k1gFnSc4	síla
i	i	k9	i
odolnost	odolnost	k1gFnSc4	odolnost
všech	všecek	k3xTgFnPc2	všecek
bílých	bílý	k2eAgFnPc2d1	bílá
bytostí	bytost	k1gFnPc2	bytost
o	o	k7c4	o
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
jsou	být	k5eAaImIp3nP	být
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
spouštějí	spouštět	k5eAaImIp3nP	spouštět
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
okamžiku	okamžik	k1gInSc6	okamžik
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
upkeepu	upkeep	k1gInSc2	upkeep
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kola	kolo	k1gNnSc2	kolo
nebo	nebo	k8xC	nebo
při	při	k7c6	při
seslání	seslání	k1gNnSc6	seslání
kouzla	kouzlo	k1gNnSc2	kouzlo
(	(	kIx(	(
<g/>
časová	časový	k2eAgFnSc1d1	časová
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc4	takový
schopnost	schopnost	k1gFnSc4	schopnost
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
artefakt	artefakt	k1gInSc1	artefakt
Ankh	Ankh	k1gInSc1	Ankh
of	of	k?	of
Mishra	Mishra	k1gFnSc1	Mishra
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
spustí	spustit	k5eAaPmIp3nS	spustit
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
některý	některý	k3yIgMnSc1	některý
hráč	hráč	k1gMnSc1	hráč
vyloží	vyložit	k5eAaPmIp3nS	vyložit
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
artefakt	artefakt	k1gInSc4	artefakt
pak	pak	k6eAd1	pak
tohoto	tento	k3xDgMnSc4	tento
hráče	hráč	k1gMnSc4	hráč
zraní	zranit	k5eAaPmIp3nS	zranit
o	o	k7c4	o
2	[number]	k4	2
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetím	třetí	k4xOgInSc7	třetí
druhem	druh	k1gInSc7	druh
schopností	schopnost	k1gFnPc2	schopnost
jsou	být	k5eAaImIp3nP	být
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
použití	použití	k1gNnSc4	použití
musí	muset	k5eAaImIp3nS	muset
hráč	hráč	k1gMnSc1	hráč
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
sesílání	sesílání	k1gNnSc4	sesílání
kouzla	kouzlo	k1gNnSc2	kouzlo
(	(	kIx(	(
<g/>
aktivační	aktivační	k2eAgFnSc1d1	aktivační
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
platit	platit	k5eAaImF	platit
například	například	k6eAd1	například
manou	mana	k1gFnSc7	mana
<g/>
,	,	kIx,	,
tapnutím	tapnutí	k1gNnSc7	tapnutí
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
nese	nést	k5eAaImIp3nS	nést
<g/>
,	,	kIx,	,
obětováním	obětování	k1gNnPc3	obětování
některé	některý	k3yIgFnSc2	některý
své	svůj	k3xOyFgFnSc2	svůj
bytosti	bytost	k1gFnSc2	bytost
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
odložením	odložení	k1gNnSc7	odložení
karty	karta	k1gFnSc2	karta
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
zaplacením	zaplacení	k1gNnSc7	zaplacení
stanoveného	stanovený	k2eAgInSc2d1	stanovený
počtu	počet	k1gInSc2	počet
životů	život	k1gInPc2	život
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Shivan	Shivan	k1gMnSc1	Shivan
Dragon	Dragon	k1gMnSc1	Dragon
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
za	za	k7c4	za
zaplacení	zaplacení	k1gNnSc4	zaplacení
jedné	jeden	k4xCgFnSc2	jeden
červené	červený	k2eAgFnSc2d1	červená
many	mana	k1gFnSc2	mana
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
jeho	jeho	k3xOp3gFnSc4	jeho
sílu	síla	k1gFnSc4	síla
o	o	k7c4	o
1	[number]	k4	1
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Prodigal	Prodigal	k1gMnSc1	Prodigal
Sorcerer	Sorcerer	k1gMnSc1	Sorcerer
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
za	za	k7c2	za
jeho	on	k3xPp3gNnSc2	on
tapnutí	tapnutí	k1gNnSc2	tapnutí
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
už	už	k6eAd1	už
tapnut	tapnut	k2eAgMnSc1d1	tapnut
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
schopnost	schopnost	k1gFnSc4	schopnost
použít	použít	k5eAaPmF	použít
<g/>
)	)	kIx)	)
způsobí	způsobit	k5eAaPmIp3nS	způsobit
vybrané	vybraný	k2eAgFnSc3d1	vybraná
bytosti	bytost	k1gFnSc3	bytost
nebo	nebo	k8xC	nebo
hráči	hráč	k1gMnSc3	hráč
1	[number]	k4	1
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Aktivační	aktivační	k2eAgFnPc4d1	aktivační
schopnosti	schopnost	k1gFnPc4	schopnost
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
používat	používat	k5eAaImF	používat
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
instanty	instant	k1gMnPc4	instant
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
během	během	k7c2	během
soupeřova	soupeřův	k2eAgNnSc2d1	soupeřovo
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
během	během	k7c2	během
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
prázdný	prázdný	k2eAgInSc1d1	prázdný
stack	stack	k1gInSc1	stack
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc1	některý
schopnosti	schopnost	k1gFnPc1	schopnost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
nestvůr	nestvůra	k1gFnPc2	nestvůra
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
klíčovým	klíčový	k2eAgNnSc7d1	klíčové
slovem	slovo	k1gNnSc7	slovo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vysvětleny	vysvětlit	k5eAaPmNgFnP	vysvětlit
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Klíčová	klíčový	k2eAgNnPc1d1	klíčové
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
některé	některý	k3yIgInPc1	některý
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
silnější	silný	k2eAgFnPc1d2	silnější
<g/>
)	)	kIx)	)
potvory	potvora	k1gFnPc1	potvora
jich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
a	a	k8xC	a
prostě	prostě	k9	prostě
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
karty	karta	k1gFnSc2	karta
už	už	k6eAd1	už
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
nevešlo	vejít	k5eNaPmAgNnS	vejít
<g/>
.	.	kIx.	.
</s>
<s>
Klíčových	klíčový	k2eAgNnPc2d1	klíčové
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Magicu	Magicus	k1gInSc6	Magicus
trvale	trvale	k6eAd1	trvale
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jen	jen	k9	jen
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vydané	vydaný	k2eAgFnSc6d1	vydaná
edici	edice	k1gFnSc6	edice
či	či	k8xC	či
bloku	blok	k1gInSc6	blok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgFnPc4	ten
nejčastější	častý	k2eAgMnPc1d3	nejčastější
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Banding	Banding	k1gInSc1	Banding
</s>
</p>
<p>
<s>
Flying	Flying	k1gInSc1	Flying
(	(	kIx(	(
<g/>
létání	létání	k1gNnSc2	létání
<g/>
)	)	kIx)	)
–	–	k?	–
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
-li	i	k?	-li
létající	létající	k2eAgFnSc4d1	létající
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
zablokovat	zablokovat	k5eAaPmF	zablokovat
jen	jen	k9	jen
jiná	jiný	k2eAgFnSc1d1	jiná
létající	létající	k2eAgFnSc1d1	létající
bytost	bytost	k1gFnSc1	bytost
(	(	kIx(	(
<g/>
sama	sám	k3xTgFnSc1	sám
létající	létající	k2eAgFnSc1d1	létající
bytost	bytost	k1gFnSc1	bytost
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
zablokovat	zablokovat	k5eAaPmF	zablokovat
i	i	k9	i
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nelétají	létat	k5eNaImIp3nP	létat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bytost	bytost	k1gFnSc1	bytost
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
Reach	Reacha	k1gFnPc2	Reacha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trample	Trample	k?	Trample
(	(	kIx(	(
<g/>
dupání	dupání	k1gNnSc1	dupání
<g/>
)	)	kIx)	)
–	–	k?	–
pokud	pokud	k8xS	pokud
bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
trample	trample	k?	trample
útočí	útočit	k5eAaImIp3nS	útočit
a	a	k8xC	a
zablokuje	zablokovat	k5eAaPmIp3nS	zablokovat
ji	on	k3xPp3gFnSc4	on
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgFnSc4d2	nižší
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
bytosti	bytost	k1gFnSc3	bytost
s	s	k7c7	s
trample	trample	k?	trample
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
trample	trample	k?	trample
udělí	udělit	k5eAaPmIp3nP	udělit
blokující	blokující	k2eAgFnPc1d1	blokující
bytosti	bytost	k1gFnPc1	bytost
jen	jen	k6eAd1	jen
tolik	tolik	k4xDc4	tolik
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc4	její
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatní	ostatní	k2eAgNnSc1d1	ostatní
zranění	zranění	k1gNnSc1	zranění
může	moct	k5eAaImIp3nS	moct
udělit	udělit	k5eAaPmF	udělit
soupeři	soupeř	k1gMnSc3	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
-li	i	k?	-li
bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
trample	trample	k?	trample
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
3	[number]	k4	3
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zablokována	zablokovat	k5eAaPmNgFnS	zablokovat
bytostí	bytost	k1gFnSc7	bytost
s	s	k7c7	s
odolností	odolnost	k1gFnSc7	odolnost
1	[number]	k4	1
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jí	on	k3xPp3gFnSc3	on
udělí	udělit	k5eAaPmIp3nS	udělit
jen	jen	k9	jen
1	[number]	k4	1
zranění	zranění	k1gNnSc1	zranění
a	a	k8xC	a
zbylá	zbylý	k2eAgFnSc1d1	zbylá
2	[number]	k4	2
zranění	zranění	k1gNnPc2	zranění
udělí	udělit	k5eAaPmIp3nP	udělit
soupeři	soupeř	k1gMnPc1	soupeř
nebo	nebo	k8xC	nebo
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
blokovala	blokovat	k5eAaImAgFnS	blokovat
bytost	bytost	k1gFnSc4	bytost
s	s	k7c7	s
trample	trample	k?	trample
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Indestructible	Indestructible	k6eAd1	Indestructible
(	(	kIx(	(
<g/>
nezničitelný	zničitelný	k2eNgMnSc1d1	nezničitelný
<g/>
)	)	kIx)	)
–	–	k?	–
Nepůsobí	působit	k5eNaImIp3nS	působit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
efekty	efekt	k1gInPc1	efekt
znič	zničit	k5eAaPmRp2nS	zničit
a	a	k8xC	a
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
odolnost	odolnost	k1gFnSc1	odolnost
klesne	klesnout	k5eAaPmIp3nS	klesnout
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
nezemře	zemřít	k5eNaPmIp3nS	zemřít
(	(	kIx(	(
<g/>
neplatí	platit	k5eNaImIp3nS	platit
na	na	k7c4	na
schopnosti	schopnost	k1gFnPc4	schopnost
Wither	Withra	k1gFnPc2	Withra
a	a	k8xC	a
Infect	Infecta	k1gFnPc2	Infecta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protection	Protection	k1gInSc1	Protection
from	from	k1gInSc1	from
...	...	k?	...
(	(	kIx(	(
<g/>
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
–	–	k?	–
ochrana	ochrana	k1gFnSc1	ochrana
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
před	před	k7c7	před
určitou	určitý	k2eAgFnSc7d1	určitá
barvou	barva	k1gFnSc7	barva
nebo	nebo	k8xC	nebo
druhem	druh	k1gInSc7	druh
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
bytost	bytost	k1gFnSc1	bytost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zacílena	zacílen	k2eAgFnSc1d1	zacílena
kouzly	kouzlo	k1gNnPc7	kouzlo
oné	onen	k3xDgFnSc2	onen
barvy	barva	k1gFnSc2	barva
nebo	nebo	k8xC	nebo
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
taková	takový	k3xDgNnPc4	takový
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
permanenty	permanent	k1gInPc4	permanent
jí	on	k3xPp3gFnSc3	on
nezpůsobí	způsobit	k5eNaPmIp3nS	způsobit
žádná	žádný	k3yNgNnPc4	žádný
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
ji	on	k3xPp3gFnSc4	on
zablokovat	zablokovat	k5eAaPmF	zablokovat
bytosti	bytost	k1gFnSc2	bytost
uvedené	uvedený	k2eAgFnSc2d1	uvedená
barvy	barva	k1gFnSc2	barva
nebo	nebo	k8xC	nebo
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
očarována	očarovat	k5eAaPmNgFnS	očarovat
enchantmenty	enchantment	k1gMnPc7	enchantment
dané	daný	k2eAgFnSc2d1	daná
barvy	barva	k1gFnSc2	barva
nebo	nebo	k8xC	nebo
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
First	First	k1gFnSc1	First
strike	strike	k1gFnPc2	strike
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
úder	úder	k1gInSc1	úder
<g/>
)	)	kIx)	)
–	–	k?	–
bojuje	bojovat	k5eAaImIp3nS	bojovat
<g/>
-li	i	k?	-li
bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
úderem	úder	k1gInSc7	úder
s	s	k7c7	s
bytostí	bytost	k1gFnSc7	bytost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
první	první	k4xOgInSc4	první
úder	úder	k1gInSc4	úder
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
napřed	napřed	k6eAd1	napřed
uděluje	udělovat	k5eAaImIp3nS	udělovat
zranění	zranění	k1gNnSc1	zranění
bytost	bytost	k1gFnSc4	bytost
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
úderem	úder	k1gInSc7	úder
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pokud	pokud	k8xS	pokud
druhá	druhý	k4xOgFnSc1	druhý
bytost	bytost	k1gFnSc1	bytost
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
,	,	kIx,	,
uděluje	udělovat	k5eAaImIp3nS	udělovat
zranění	zranění	k1gNnSc4	zranění
ona	onen	k3xDgFnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
když	když	k8xS	když
se	se	k3xPyFc4	se
bytost	bytost	k1gFnSc1	bytost
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
3	[number]	k4	3
<g/>
,	,	kIx,	,
odolnosti	odolnost	k1gFnPc1	odolnost
2	[number]	k4	2
a	a	k8xC	a
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
úderem	úder	k1gInSc7	úder
utká	utkat	k5eAaPmIp3nS	utkat
s	s	k7c7	s
bytostí	bytost	k1gFnSc7	bytost
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
5	[number]	k4	5
a	a	k8xC	a
odolnosti	odolnost	k1gFnSc6	odolnost
3	[number]	k4	3
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
tuto	tento	k3xDgFnSc4	tento
bytost	bytost	k1gFnSc4	bytost
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
jí	on	k3xPp3gFnSc3	on
ta	ten	k3xDgFnSc1	ten
druhá	druhý	k4xOgFnSc1	druhý
bytost	bytost	k1gFnSc1	bytost
stihla	stihnout	k5eAaPmAgFnS	stihnout
udělit	udělit	k5eAaPmF	udělit
zranění	zranění	k1gNnSc4	zranění
(	(	kIx(	(
<g/>
kdyby	kdyby	k9	kdyby
první	první	k4xOgFnSc1	první
bytost	bytost	k1gFnSc1	bytost
neměla	mít	k5eNaImAgFnS	mít
první	první	k4xOgInSc4	první
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
zabily	zabít	k5eAaPmAgInP	zabít
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
udělily	udělit	k5eAaPmAgFnP	udělit
zranění	zranění	k1gNnSc4	zranění
současně	současně	k6eAd1	současně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Double	double	k2eAgInSc4d1	double
strike	strike	k1gInSc4	strike
(	(	kIx(	(
<g/>
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
úder	úder	k1gInSc4	úder
<g/>
)	)	kIx)	)
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
bytost	bytost	k1gFnSc1	bytost
má	mít	k5eAaImIp3nS	mít
vlastně	vlastně	k9	vlastně
první	první	k4xOgInSc4	první
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
ještě	ještě	k6eAd1	ještě
navíc	navíc	k6eAd1	navíc
udělí	udělit	k5eAaPmIp3nP	udělit
zranění	zranění	k1gNnSc4	zranění
jako	jako	k8xS	jako
normálně	normálně	k6eAd1	normálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vigilance	Vigilance	k1gFnSc1	Vigilance
(	(	kIx(	(
<g/>
ostražitost	ostražitost	k1gFnSc1	ostražitost
<g/>
)	)	kIx)	)
–	–	k?	–
pokud	pokud	k8xS	pokud
bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
ostražitostí	ostražitost	k1gFnSc7	ostražitost
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
,	,	kIx,	,
netapuje	tapovat	k5eNaImIp3nS	tapovat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
takže	takže	k9	takže
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
kole	kolo	k1gNnSc6	kolo
může	moct	k5eAaImIp3nS	moct
blokovat	blokovat	k5eAaImF	blokovat
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
útočící	útočící	k2eAgFnSc2d1	útočící
bytosti	bytost	k1gFnSc2	bytost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haste	hasit	k5eAaImRp2nP	hasit
(	(	kIx(	(
<g/>
zrychlení	zrychlení	k1gNnSc3	zrychlení
<g/>
)	)	kIx)	)
–	–	k?	–
taková	takový	k3xDgFnSc1	takový
bytost	bytost	k1gFnSc1	bytost
může	moct	k5eAaImIp3nS	moct
útočit	útočit	k5eAaImF	útočit
nebo	nebo	k8xC	nebo
být	být	k5eAaImF	být
tapnuta	tapnut	k2eAgFnSc1d1	tapnut
hned	hned	k6eAd1	hned
v	v	k7c6	v
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
vyložena	vyložen	k2eAgFnSc1d1	vyložena
(	(	kIx(	(
<g/>
ruší	rušit	k5eAaImIp3nS	rušit
únavové	únavový	k2eAgNnSc1d1	únavové
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Flash	Flash	k1gInSc1	Flash
(	(	kIx(	(
<g/>
blesk	blesk	k1gInSc1	blesk
<g/>
)	)	kIx)	)
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
karta	karta	k1gFnSc1	karta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zahrána	zahrát	k5eAaPmNgFnS	zahrát
jako	jako	k9	jako
instant	instant	k?	instant
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
kdykoliv	kdykoliv	k6eAd1	kdykoliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vidno	vidno	k6eAd1	vidno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sorcery	Sorcer	k1gInPc1	Sorcer
s	s	k7c7	s
Flashem	Flash	k1gInSc7	Flash
=	=	kIx~	=
instant	instant	k?	instant
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
jen	jen	k9	jen
u	u	k7c2	u
bytostí	bytost	k1gFnPc2	bytost
nebo	nebo	k8xC	nebo
očarování	očarování	k1gNnPc2	očarování
(	(	kIx(	(
<g/>
Enchantment	Enchantment	k1gInSc1	Enchantment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Regenerate	Regenerat	k1gMnSc5	Regenerat
(	(	kIx(	(
<g/>
regenerace	regenerace	k1gFnSc2	regenerace
<g/>
)	)	kIx)	)
–	–	k?	–
u	u	k7c2	u
regenerace	regenerace	k1gFnSc2	regenerace
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
uvedena	uvést	k5eAaPmNgFnS	uvést
její	její	k3xOp3gFnSc1	její
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
permanent	permanent	k1gInSc1	permanent
s	s	k7c7	s
regenerací	regenerace	k1gFnSc7	regenerace
zničen	zničen	k2eAgInSc4d1	zničen
(	(	kIx(	(
<g/>
destroyed	destroyed	k1gInSc4	destroyed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
majitel	majitel	k1gMnSc1	majitel
může	moct	k5eAaImIp3nS	moct
zaplatit	zaplatit	k5eAaPmF	zaplatit
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
permanent	permanent	k1gInSc4	permanent
namísto	namísto	k7c2	namísto
zničení	zničení	k1gNnSc2	zničení
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
tapne	tapnout	k5eAaPmIp3nS	tapnout
(	(	kIx(	(
<g/>
nebyl	být	k5eNaImAgMnS	být
<g/>
-li	i	k?	-li
tapnutý	tapnutý	k2eAgMnSc1d1	tapnutý
již	již	k9	již
předtím	předtím	k6eAd1	předtím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
daným	daný	k2eAgInSc7d1	daný
permanentem	permanent	k1gInSc7	permanent
bytost	bytost	k1gFnSc1	bytost
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
regenerace	regenerace	k1gFnSc1	regenerace
tuto	tento	k3xDgFnSc4	tento
bytost	bytost	k1gFnSc1	bytost
z	z	k7c2	z
boje	boj	k1gInSc2	boj
odstraní	odstranit	k5eAaPmIp3nS	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fear	Fear	k1gInSc1	Fear
(	(	kIx(	(
<g/>
strach	strach	k1gInSc1	strach
<g/>
)	)	kIx)	)
–	–	k?	–
takovou	takový	k3xDgFnSc4	takový
bytost	bytost	k1gFnSc4	bytost
smějí	smát	k5eAaImIp3nP	smát
zablokovat	zablokovat	k5eAaPmF	zablokovat
jen	jen	k9	jen
černé	černý	k2eAgFnPc1d1	černá
a	a	k8xC	a
artefaktové	artefaktový	k2eAgFnPc1d1	artefaktová
bytosti	bytost	k1gFnPc1	bytost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Defender	Defender	k1gMnSc1	Defender
(	(	kIx(	(
<g/>
obránce	obránce	k1gMnSc1	obránce
<g/>
)	)	kIx)	)
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
bytost	bytost	k1gFnSc1	bytost
nesmí	smět	k5eNaImIp3nS	smět
útočit	útočit	k5eAaImF	útočit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jen	jen	k9	jen
blokovat	blokovat	k5eAaImF	blokovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
bytostí	bytost	k1gFnPc2	bytost
s	s	k7c7	s
typem	typ	k1gInSc7	typ
Wall	Walla	k1gFnPc2	Walla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Changeling	Changeling	k1gInSc1	Changeling
–	–	k?	–
Tato	tento	k3xDgFnSc1	tento
karta	karta	k1gFnSc1	karta
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
typ	typ	k1gInSc4	typ
příšery	příšera	k1gFnSc2	příšera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Storm	Storm	k1gInSc1	Storm
(	(	kIx(	(
<g/>
bouřka	bouřka	k1gFnSc1	bouřka
<g/>
)	)	kIx)	)
–	–	k?	–
Toto	tento	k3xDgNnSc1	tento
kouzlo	kouzlo	k1gNnSc1	kouzlo
se	se	k3xPyFc4	se
zkopíruje	zkopírovat	k5eAaPmIp3nS	zkopírovat
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
jiné	jiný	k2eAgNnSc4d1	jiné
kouzlo	kouzlo	k1gNnSc4	kouzlo
zahrané	zahraný	k2eAgNnSc4d1	zahrané
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
před	před	k7c7	před
zahráním	zahrání	k1gNnSc7	zahrání
tohoto	tento	k3xDgNnSc2	tento
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reach	Reach	k1gInSc1	Reach
(	(	kIx(	(
<g/>
starší	starý	k2eAgInSc1d2	starší
název	název	k1gInSc1	název
Web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
–	–	k?	–
Tato	tento	k3xDgFnSc1	tento
příšera	příšera	k1gFnSc1	příšera
může	moct	k5eAaImIp3nS	moct
blokovat	blokovat	k5eAaImF	blokovat
příšery	příšera	k1gFnPc4	příšera
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
Flying	Flying	k1gInSc4	Flying
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hexproof	Hexproof	k1gInSc1	Hexproof
–	–	k?	–
Příšera	příšera	k1gFnSc1	příšera
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
cílena	cílen	k2eAgFnSc1d1	cílena
nepřátelským	přátelský	k2eNgNnSc7d1	nepřátelské
kouzlem	kouzlo	k1gNnSc7	kouzlo
nebo	nebo	k8xC	nebo
schopností	schopnost	k1gFnSc7	schopnost
(	(	kIx(	(
<g/>
abilitou	abilita	k1gFnSc7	abilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shroud	Shroud	k6eAd1	Shroud
–	–	k?	–
Příšera	příšera	k1gFnSc1	příšera
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
cílená	cílený	k2eAgFnSc1d1	cílená
kouzlem	kouzlo	k1gNnSc7	kouzlo
nebo	nebo	k8xC	nebo
schopností	schopnost	k1gFnSc7	schopnost
(	(	kIx(	(
<g/>
abilitou	abilita	k1gFnSc7	abilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kicker	Kicker	k1gInSc1	Kicker
–	–	k?	–
Schopnost	schopnost	k1gFnSc1	schopnost
Kicker	Kicker	k1gInSc1	Kicker
(	(	kIx(	(
<g/>
Nakopnutí	nakopnutí	k1gNnSc1	nakopnutí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Invasion	Invasion	k1gInSc4	Invasion
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
kouzla	kouzlo	k1gNnSc2	kouzlo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
efekt	efekt	k1gInSc1	efekt
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
připlatíte	připlatit	k5eAaPmIp2nP	připlatit
určitou	určitý	k2eAgFnSc4d1	určitá
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sesíláte	sesílat	k5eAaImIp2nP	sesílat
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
(	(	kIx(	(
<g/>
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
karty	karta	k1gFnPc1	karta
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
Kicker	Kicker	k1gInSc4	Kicker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aktivujete	aktivovat	k5eAaBmIp2nP	aktivovat
tím	ten	k3xDgNnSc7	ten
určitou	určitý	k2eAgFnSc4d1	určitá
speciální	speciální	k2eAgFnSc4d1	speciální
schopnost	schopnost	k1gFnSc4	schopnost
karty	karta	k1gFnSc2	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Multikicker	Multikicker	k1gInSc1	Multikicker
–	–	k?	–
Schopnost	schopnost	k1gFnSc1	schopnost
poprvé	poprvé	k6eAd1	poprvé
použitá	použitý	k2eAgFnSc1d1	použitá
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Worldwake	Worldwak	k1gFnSc2	Worldwak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
Kicker	Kicker	k1gInSc1	Kicker
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
,	,	kIx,	,
kolikrát	kolikrát	k6eAd1	kolikrát
chcete	chtít	k5eAaImIp2nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Efekt	efekt	k1gInSc1	efekt
tím	ten	k3xDgNnSc7	ten
aktivovaný	aktivovaný	k2eAgInSc1d1	aktivovaný
u	u	k7c2	u
vaší	váš	k3xOp2gFnSc2	váš
potvory	potvora	k1gFnSc2	potvora
vám	vy	k3xPp2nPc3	vy
k	k	k7c3	k
ní	on	k3xPp3gFnSc6	on
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
Multikicker	Multikicker	k1gInSc4	Multikicker
obyčejně	obyčejně	k6eAd1	obyčejně
přidá	přidat	k5eAaPmIp3nS	přidat
+1	+1	k4	+1
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
counter	countra	k1gFnPc2	countra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Landfall	Landfall	k1gInSc1	Landfall
–	–	k?	–
Když	když	k8xS	když
země	zem	k1gFnSc2	zem
tvé	tvůj	k3xOp2gFnSc2	tvůj
kontroly	kontrola	k1gFnSc2	kontrola
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
získáš	získat	k5eAaPmIp2nS	získat
určitý	určitý	k2eAgInSc4d1	určitý
bonus	bonus	k1gInSc4	bonus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
kartě	karta	k1gFnSc6	karta
napsán	napsat	k5eAaBmNgInS	napsat
např.	např.	kA	např.
bytost	bytost	k1gFnSc1	bytost
dostane	dostat	k5eAaPmIp3nS	dostat
+2	+2	k4	+2
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
do	do	k7c2	do
konce	konec	k1gInSc2	konec
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Zendikar	Zendikara	k1gFnPc2	Zendikara
<g/>
,	,	kIx,	,
karty	karta	k1gFnPc1	karta
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgInPc1d1	fungující
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
však	však	k9	však
byly	být	k5eAaImAgFnP	být
již	již	k9	již
dávno	dávno	k6eAd1	dávno
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Annihilator	Annihilator	k1gMnSc1	Annihilator
X	X	kA	X
–	–	k?	–
Když	když	k8xS	když
bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
schopností	schopnost	k1gFnSc7	schopnost
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
,	,	kIx,	,
bránící	bránící	k2eAgMnSc1d1	bránící
hráč	hráč	k1gMnSc1	hráč
obětuje	obětovat	k5eAaBmIp3nS	obětovat
X	X	kA	X
permanentů	permanent	k1gInPc2	permanent
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Rise	Ris	k1gInSc2	Ris
of	of	k?	of
the	the	k?	the
Eldrazi	Eldraze	k1gFnSc4	Eldraze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Landwalk	Landwalk	k6eAd1	Landwalk
–	–	k?	–
Bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
tuto	tento	k3xDgFnSc4	tento
schopností	schopnost	k1gFnSc7	schopnost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
blokována	blokován	k2eAgFnSc1d1	blokována
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
blokující	blokující	k2eAgMnSc1d1	blokující
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
zemi	zem	k1gFnSc4	zem
určitého	určitý	k2eAgInSc2d1	určitý
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Split	Split	k1gInSc1	Split
second	second	k1gInSc1	second
–	–	k?	–
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
kouzlo	kouzlo	k1gNnSc4	kouzlo
na	na	k7c4	na
stacku	stacka	k1gFnSc4	stacka
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
nemohou	moct	k5eNaImIp3nP	moct
hrát	hrát	k5eAaImF	hrát
kouzla	kouzlo	k1gNnPc4	kouzlo
(	(	kIx(	(
<g/>
spells	spells	k6eAd1	spells
<g/>
)	)	kIx)	)
a	a	k8xC	a
aktivovat	aktivovat	k5eAaBmF	aktivovat
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shadow	Shadow	k?	Shadow
–	–	k?	–
Bytost	bytost	k1gFnSc1	bytost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
blokována	blokován	k2eAgFnSc1d1	blokována
ničím	nic	k3yNnSc7	nic
jiným	jiný	k2eAgNnSc7d1	jiné
než	než	k8xS	než
Shadow	Shadow	k1gFnSc7	Shadow
kartou	karta	k1gFnSc7	karta
(	(	kIx(	(
<g/>
jako	jako	k9	jako
Flying	Flying	k1gInSc1	Flying
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
blokovat	blokovat	k5eAaImF	blokovat
jen	jen	k9	jen
creatury	creatura	k1gFnPc4	creatura
se	s	k7c7	s
Shadow	Shadow	k1gFnSc7	Shadow
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lifelink	Lifelink	k1gInSc1	Lifelink
–	–	k?	–
Damage	Damage	k1gInSc1	Damage
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnSc1	poškození
<g/>
)	)	kIx)	)
které	který	k3yIgNnSc1	který
dá	dát	k5eAaPmIp3nS	dát
tato	tento	k3xDgFnSc1	tento
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
přidá	přidat	k5eAaPmIp3nS	přidat
útočícímu	útočící	k2eAgMnSc3d1	útočící
hráči	hráč	k1gMnSc3	hráč
stejně	stejně	k6eAd1	stejně
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
damage	damage	k1gFnSc1	damage
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
udělena	udělit	k5eAaPmNgFnS	udělit
jak	jak	k8xS	jak
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
tak	tak	k9	tak
do	do	k7c2	do
bránící	bránící	k2eAgFnSc2d1	bránící
creatury	creatura	k1gFnSc2	creatura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výdrž	výdrž	k1gFnSc1	výdrž
creatury	creatura	k1gFnSc2	creatura
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
lifelink	lifelink	k1gInSc4	lifelink
omezující	omezující	k2eAgFnPc1d1	omezující
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
lifelink	lifelink	k1gInSc1	lifelink
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
brání	bránit	k5eAaImIp3nS	bránit
–	–	k?	–
útočící	útočící	k2eAgMnSc1d1	útočící
hráč	hráč	k1gMnSc1	hráč
získá	získat	k5eAaPmIp3nS	získat
5	[number]	k4	5
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intimidate	Intimidat	k1gMnSc5	Intimidat
(	(	kIx(	(
<g/>
zastrašení	zastrašení	k1gNnSc2	zastrašení
<g/>
)	)	kIx)	)
–	–	k?	–
Bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
blokována	blokován	k2eAgFnSc1d1	blokována
pouze	pouze	k6eAd1	pouze
artifact	artifact	k5eAaPmF	artifact
creaturama	creaturama	k1gNnSc4	creaturama
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sama	sám	k3xTgMnSc4	sám
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Deathtouch	Deathtouch	k1gInSc1	Deathtouch
(	(	kIx(	(
<g/>
Smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
dotek	dotek	k1gInSc1	dotek
<g/>
)	)	kIx)	)
–	–	k?	–
Bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
uděleno	udělen	k2eAgNnSc4d1	uděleno
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pokud	pokud	k8xS	pokud
útočí	útočit	k5eAaImIp3nS	útočit
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
Deathtouchem	Deathtouch	k1gInSc7	Deathtouch
a	a	k8xC	a
zablokuje	zablokovat	k5eAaPmIp3nS	zablokovat
jí	on	k3xPp3gFnSc3	on
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
bytost	bytost	k1gFnSc4	bytost
zabita	zabit	k2eAgFnSc1d1	zabita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haunt	Haunt	k1gInSc1	Haunt
(	(	kIx(	(
<g/>
Strašení	strašení	k1gNnSc1	strašení
<g/>
)	)	kIx)	)
–	–	k?	–
Při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
karty	karta	k1gFnSc2	karta
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
Haunt	Haunta	k1gFnPc2	Haunta
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
vybere	vybrat	k5eAaPmIp3nS	vybrat
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
bytost	bytost	k1gFnSc4	bytost
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
bytost	bytost	k1gFnSc1	bytost
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
strašena	strašen	k2eAgFnSc1d1	strašena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
chvíli	chvíle	k1gFnSc4	chvíle
kdy	kdy	k6eAd1	kdy
tato	tento	k3xDgFnSc1	tento
karta	karta	k1gFnSc1	karta
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
efekt	efekt	k1gInSc1	efekt
karty	karta	k1gFnSc2	karta
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
Haunt	Haunta	k1gFnPc2	Haunta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wither	Withra	k1gFnPc2	Withra
(	(	kIx(	(
<g/>
Vysušení	vysušení	k1gNnSc2	vysušení
<g/>
)	)	kIx)	)
–	–	k?	–
Bytost	bytost	k1gFnSc1	bytost
s	s	k7c7	s
Wither	Withra	k1gFnPc2	Withra
dává	dávat	k5eAaImIp3nS	dávat
damage	damage	k6eAd1	damage
do	do	k7c2	do
bytostí	bytost	k1gFnPc2	bytost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
-1	-1	k4	-1
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
counterů	counter	k1gInPc2	counter
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
bytost	bytost	k1gFnSc1	bytost
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
s	s	k7c7	s
Witherem	Withero	k1gNnSc7	Withero
proti	proti	k7c3	proti
bytosti	bytost	k1gFnSc3	bytost
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
–	–	k?	–
bytost	bytost	k1gFnSc1	bytost
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
s	s	k7c7	s
Witherem	Withero	k1gNnSc7	Withero
sice	sice	k8xC	sice
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
bytosti	bytost	k1gFnSc2	bytost
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
sobě	se	k3xPyFc3	se
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
5	[number]	k4	5
-1	-1	k4	-1
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
counterů	counter	k1gInPc2	counter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Infect	Infect	k1gInSc1	Infect
(	(	kIx(	(
<g/>
Nakazit	nakazit	k5eAaPmF	nakazit
<g/>
)	)	kIx)	)
–	–	k?	–
Bytosti	bytost	k1gFnSc6	bytost
s	s	k7c7	s
Infectem	Infect	k1gInSc7	Infect
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
bytostí	bytost	k1gFnPc2	bytost
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
bytosti	bytost	k1gFnPc1	bytost
s	s	k7c7	s
Witherem	Withero	k1gNnSc7	Withero
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
hráčů	hráč	k1gMnPc2	hráč
dávají	dávat	k5eAaImIp3nP	dávat
damage	damage	k6eAd1	damage
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Poison	Poisona	k1gFnPc2	Poisona
counterů	counter	k1gMnPc2	counter
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
výhoda	výhoda	k1gFnSc1	výhoda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
máte	mít	k5eAaImIp2nP	mít
10	[number]	k4	10
Poison	Poisona	k1gFnPc2	Poisona
counterů	counter	k1gInPc2	counter
prohráli	prohrát	k5eAaPmAgMnP	prohrát
jste	být	k5eAaImIp2nP	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
když	když	k8xS	když
ztratíte	ztratit	k5eAaPmIp2nP	ztratit
všechny	všechen	k3xTgInPc4	všechen
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Echo	echo	k1gNnSc1	echo
(	(	kIx(	(
<g/>
Ozvěna	ozvěna	k1gFnSc1	ozvěna
<g/>
)	)	kIx)	)
–	–	k?	–
Karta	karta	k1gFnSc1	karta
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
abilitou	abilita	k1gFnSc7	abilita
se	se	k3xPyFc4	se
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
vyloženi	vyložen	k2eAgMnPc1d1	vyložen
(	(	kIx(	(
<g/>
zaplacení	zaplacený	k2eAgMnPc1d1	zaplacený
<g/>
)	)	kIx)	)
platí	platit	k5eAaImIp3nP	platit
až	až	k9	až
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
<g/>
)	)	kIx)	)
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
odtapovávací	odtapovávací	k2eAgFnSc6d1	odtapovávací
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
bývá	bývat	k5eAaImIp3nS	bývat
psáno	psát	k5eAaImNgNnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
nezaplatíte	zaplatit	k5eNaPmIp2nP	zaplatit
její	její	k3xOp3gFnSc4	její
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
odtapovávací	odtapovávací	k2eAgFnSc6d1	odtapovávací
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
musíte	muset	k5eAaImIp2nP	muset
ji	on	k3xPp3gFnSc4	on
obětovat	obětovat	k5eAaBmF	obětovat
(	(	kIx(	(
<g/>
odložit	odložit	k5eAaPmF	odložit
do	do	k7c2	do
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostě	prostě	k6eAd1	prostě
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
vydržet	vydržet	k5eAaPmF	vydržet
další	další	k2eAgNnSc4d1	další
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rampage	Rampage	k1gNnSc1	Rampage
(	(	kIx(	(
<g/>
Zuření	zuření	k1gNnSc1	zuření
<g/>
)	)	kIx)	)
–	–	k?	–
Karta	karta	k1gFnSc1	karta
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
abilitou	abilita	k1gFnSc7	abilita
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepřítel	nepřítel	k1gMnSc1	nepřítel
blokuje	blokovat	k5eAaImIp3nS	blokovat
kartu	karta	k1gFnSc4	karta
s	s	k7c7	s
Rampage	Rampage	k1gNnSc7	Rampage
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednou	jeden	k4xCgFnSc7	jeden
blokující	blokující	k2eAgFnSc7d1	blokující
kartou	karta	k1gFnSc7	karta
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
Rampage	Rampage	k1gFnSc1	Rampage
tolik	tolik	k6eAd1	tolik
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
a	a	k8xC	a
k	k	k7c3	k
životům	život	k1gInPc3	život
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
toho	ten	k3xDgNnSc2	ten
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
Chromium	Chromium	k1gNnSc1	Chromium
(	(	kIx(	(
<g/>
drak	drak	k1gInSc1	drak
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
s	s	k7c7	s
Rampage	Rampage	k1gNnSc7	Rampage
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
blokován	blokovat	k5eAaImNgInS	blokovat
dvěma	dva	k4xCgFnPc7	dva
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
jsou	být	k5eAaImIp3nP	být
zničeny	zničen	k2eAgInPc1d1	zničen
a	a	k8xC	a
Chromium	Chromium	k1gNnSc4	Chromium
zůstal	zůstat	k5eAaPmAgMnS	zůstat
s	s	k7c7	s
1	[number]	k4	1
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bushido	Bushida	k1gFnSc5	Bushida
X	X	kA	X
–	–	k?	–
Když	když	k8xS	když
bytost	bytost	k1gFnSc1	bytost
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
Bushido	Bushida	k1gFnSc5	Bushida
blokuje	blokovat	k5eAaImIp3nS	blokovat
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
blokována	blokován	k2eAgFnSc1d1	blokována
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
+	+	kIx~	+
<g/>
X	X	kA	X
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
X	X	kA	X
do	do	k7c2	do
konce	konec	k1gInSc2	konec
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
bloku	blok	k1gInSc6	blok
Kamigawa	Kamigaw	k1gInSc2	Kamigaw
<g/>
,	,	kIx,	,
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Champions	Championsa	k1gFnPc2	Championsa
of	of	k?	of
Kamigawa	Kamigawa	k1gMnSc1	Kamigawa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soulbond	Soulbond	k1gInSc1	Soulbond
(	(	kIx(	(
<g/>
Připoutaný	připoutaný	k2eAgInSc1d1	připoutaný
<g/>
)	)	kIx)	)
–	–	k?	–
Příšera	příšera	k1gFnSc1	příšera
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
schopností	schopnost	k1gFnSc7	schopnost
si	se	k3xPyFc3	se
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
sobě	se	k3xPyFc3	se
připoutat	připoutat	k5eAaPmF	připoutat
jinou	jiný	k2eAgFnSc4d1	jiná
příšeru	příšera	k1gFnSc4	příšera
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
získají	získat	k5eAaPmIp3nP	získat
nějaký	nějaký	k3yIgInSc1	nějaký
bonus	bonus	k1gInSc1	bonus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
+4	+4	k4	+4
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nějakou	nějaký	k3yIgFnSc4	nějaký
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
bloku	blok	k1gInSc6	blok
Innistrad	Innistrada	k1gFnPc2	Innistrada
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Avacyn	Avacyn	k1gMnSc1	Avacyn
Restored	Restored	k1gMnSc1	Restored
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Miracle	Miracle	k1gFnSc1	Miracle
(	(	kIx(	(
<g/>
Zázrak	zázrak	k1gInSc1	zázrak
<g/>
)	)	kIx)	)
–	–	k?	–
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
lízne	líznout	k5eAaPmIp3nS	líznout
tuto	tento	k3xDgFnSc4	tento
kartu	karta	k1gFnSc4	karta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
líznutá	líznutý	k2eAgFnSc1d1	líznutá
karta	karta	k1gFnSc1	karta
toto	tento	k3xDgNnSc4	tento
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
hned	hned	k6eAd1	hned
zahrát	zahrát	k5eAaPmF	zahrát
za	za	k7c4	za
zvýhodněnou	zvýhodněný	k2eAgFnSc4d1	zvýhodněná
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Living	Living	k1gInSc1	Living
weapon	weapon	k1gInSc1	weapon
(	(	kIx(	(
<g/>
Oživlá	oživlý	k2eAgFnSc1d1	oživlá
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
)	)	kIx)	)
–	–	k?	–
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
karet	kareta	k1gFnPc2	kareta
typu	typ	k1gInSc2	typ
Equipment	Equipment	k1gMnSc1	Equipment
(	(	kIx(	(
<g/>
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijde	přijít	k5eAaPmIp3nS	přijít
tato	tento	k3xDgFnSc1	tento
karta	karta	k1gFnSc1	karta
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	s	k7c7	s
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
černý	černý	k2eAgInSc4d1	černý
Germ	Germ	k1gInSc4	Germ
token	tokna	k1gFnPc2	tokna
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
equipne	equipnout	k5eAaPmIp3nS	equipnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gotcha	Gotcha	k1gFnSc1	Gotcha
–	–	k?	–
Když	když	k8xS	když
oponent	oponent	k1gMnSc1	oponent
vykoná	vykonat	k5eAaPmIp3nS	vykonat
jistou	jistý	k2eAgFnSc4d1	jistá
věc	věc	k1gFnSc4	věc
(	(	kIx(	(
<g/>
jakou	jaký	k3yQgFnSc4	jaký
je	být	k5eAaImIp3nS	být
upřesněno	upřesnit	k5eAaPmNgNnS	upřesnit
na	na	k7c6	na
kartě	karta	k1gFnSc6	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
říct	říct	k5eAaPmF	říct
gotcha	gotcha	k1gFnSc1	gotcha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tak	tak	k6eAd1	tak
učiníte	učinit	k5eAaPmIp2nP	učinit
<g/>
,	,	kIx,	,
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
ta	ten	k3xDgFnSc1	ten
karta	karta	k1gFnSc1	karta
ze	z	k7c2	z
hřbitova	hřbitov	k1gInSc2	hřbitov
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Unhinged	Unhinged	k1gInSc1	Unhinged
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
legální	legální	k2eAgFnSc1d1	legální
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cycling	Cycling	k1gInSc1	Cycling
-	-	kIx~	-
Po	po	k7c6	po
zaplacení	zaplacení	k1gNnSc6	zaplacení
určené	určený	k2eAgFnSc2d1	určená
ceny	cena	k1gFnSc2	cena
zahodíte	zahodit	k5eAaPmIp2nP	zahodit
danou	daný	k2eAgFnSc4d1	daná
kartu	karta	k1gFnSc4	karta
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
líznete	líznout	k5eAaPmIp2nP	líznout
si	se	k3xPyFc3	se
novou	nový	k2eAgFnSc4d1	nová
<g/>
.	.	kIx.	.
<g/>
Heroic	Heroic	k1gMnSc1	Heroic
-	-	kIx~	-
když	když	k8xS	když
zahrajete	zahrát	k5eAaPmIp2nP	zahrát
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
<g/>
mířící	mířící	k2eAgFnSc4d1	mířící
na	na	k7c4	na
bytost	bytost	k1gFnSc4	bytost
s	s	k7c7	s
heroicem	heroic	k1gMnSc7	heroic
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
bytost	bytost	k1gFnSc1	bytost
něco	něco	k3yInSc4	něco
udělá	udělat	k5eAaPmIp3nS	udělat
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
dá	dát	k5eAaPmIp3nS	dát
+1	+1	k4	+1
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
counter	countra	k1gFnPc2	countra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Devoid	Devoid	k1gInSc1	Devoid
-Tato	-Tato	k1gNnSc1	-Tato
karta	karta	k1gFnSc1	karta
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ingest	Ingest	k1gInSc1	Ingest
(	(	kIx(	(
<g/>
přijmout	přijmout	k5eAaPmF	přijmout
jako	jako	k8xS	jako
potravu	potrava	k1gFnSc4	potrava
<g/>
)	)	kIx)	)
<g/>
-Když	-Když	k6eAd1	-Když
dá	dát	k5eAaPmIp3nS	dát
tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
svůj	svůj	k3xOyFgInSc4	svůj
útok	útok	k1gInSc4	útok
do	do	k7c2	do
hráče	hráč	k1gMnSc2	hráč
<g/>
,	,	kIx,	,
exilne	exilnout	k5eAaPmIp3nS	exilnout
tento	tento	k3xDgMnSc1	tento
hráč	hráč	k1gMnSc1	hráč
vrchní	vrchní	k2eAgFnSc4d1	vrchní
kartu	karta	k1gFnSc4	karta
své	svůj	k3xOyFgFnSc2	svůj
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Renown	Renown	k1gInSc1	Renown
-	-	kIx~	-
Když	když	k8xS	když
tato	tento	k3xDgFnSc1	tento
bytost	bytost	k1gFnSc1	bytost
dá	dát	k5eAaPmIp3nS	dát
poškození	poškození	k1gNnSc4	poškození
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
hráče	hráč	k1gMnSc2	hráč
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
renowned	renowned	k1gInSc1	renowned
(	(	kIx(	(
<g/>
proslulá	proslulý	k2eAgFnSc1d1	proslulá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
příslušný	příslušný	k2eAgInSc4d1	příslušný
počet	počet	k1gInSc4	počet
counterů	counter	k1gInPc2	counter
(	(	kIx(	(
<g/>
např.	např.	kA	např.
+1	+1	k4	+1
<g/>
/	/	kIx~	/
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
renowned	renowned	k1gMnSc1	renowned
(	(	kIx(	(
<g/>
proslulou	proslulý	k2eAgFnSc7d1	proslulá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dash	Dash	k1gInSc1	Dash
-	-	kIx~	-
Nová	nový	k2eAgFnSc1d1	nová
mechanika	mechanika	k1gFnSc1	mechanika
klanu	klan	k1gInSc2	klan
Mardu	Mard	k1gInSc2	Mard
představuje	představovat	k5eAaImIp3nS	představovat
alternativní	alternativní	k2eAgInSc4d1	alternativní
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
platit	platit	k5eAaImF	platit
za	za	k7c4	za
karty	karta	k1gFnPc4	karta
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bytost	bytost	k1gFnSc1	bytost
zahrajete	zahrát	k5eAaPmIp2nP	zahrát
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
Dash	Dash	k1gInSc4	Dash
cost	cost	k5eAaPmF	cost
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
haste	hasit	k5eAaImRp2nP	hasit
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kola	kolo	k1gNnSc2	kolo
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
vrátíte	vrátit	k5eAaPmIp2nP	vrátit
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
během	během	k7c2	během
vašeho	váš	k3xOp2gInSc2	váš
tahu	tah	k1gInSc2	tah
umře	umřít	k5eAaPmIp3nS	umřít
nebo	nebo	k8xC	nebo
bude	být	k5eAaImBp3nS	být
vypovězena	vypovědět	k5eAaPmNgFnS	vypovědět
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
na	na	k7c4	na
ruku	ruka	k1gFnSc4	ruka
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bestow	Bestow	k?	Bestow
-	-	kIx~	-
Představuje	představovat	k5eAaImIp3nS	představovat
alternativní	alternativní	k2eAgInSc4d1	alternativní
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
danou	daný	k2eAgFnSc4d1	daná
kartu	karta	k1gFnSc4	karta
zahrát	zahrát	k5eAaPmF	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zaplatíte	zaplatit	k5eAaPmIp2nP	zaplatit
mana	man	k1gMnSc4	man
cost	cost	k5eAaPmF	cost
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
bytost	bytost	k1gFnSc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kartu	karta	k1gFnSc4	karta
zahrajete	zahrát	k5eAaPmIp2nP	zahrát
přes	přes	k7c4	přes
Bestow	Bestow	k1gFnSc4	Bestow
cost	costa	k1gFnPc2	costa
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
Aura	aura	k1gFnSc1	aura
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zacílíte	zacílit	k5eAaPmIp2nP	zacílit
nějakou	nějaký	k3yIgFnSc4	nějaký
bytost	bytost	k1gFnSc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
nebude	být	k5eNaImBp3nS	být
karta	karta	k1gFnSc1	karta
s	s	k7c7	s
Bestow	Bestow	k1gFnSc7	Bestow
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
bytosti	bytost	k1gFnSc3	bytost
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
bytost	bytost	k1gFnSc1	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Upozornění	upozornění	k1gNnSc1	upozornění
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
karta	karta	k1gFnSc1	karta
hraná	hraný	k2eAgFnSc1d1	hraná
přes	přes	k7c4	přes
Bestow	Bestow	k1gFnSc4	Bestow
cost	cost	k1gMnSc1	cost
ztratí	ztratit	k5eAaPmIp3nS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
po	po	k7c6	po
zacílení	zacílení	k1gNnSc6	zacílení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
podstřelení	podstřelení	k1gNnSc4	podstřelení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
jako	jako	k8xS	jako
bytost	bytost	k1gFnSc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Neputuje	putovat	k5eNaImIp3nS	putovat
do	do	k7c2	do
hřbitova	hřbitov	k1gInSc2	hřbitov
jako	jako	k8xS	jako
jiné	jiný	k2eAgFnSc2d1	jiná
aury	aura	k1gFnSc2	aura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hexproof	Hexproof	k1gInSc1	Hexproof
-	-	kIx~	-
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
bytost	bytost	k1gFnSc4	bytost
nemůže	moct	k5eNaImIp3nS	moct
soupeř	soupeř	k1gMnSc1	soupeř
cílit	cílit	k5eAaImF	cílit
svoje	svůj	k3xOyFgInPc4	svůj
sorcery	sorcer	k1gInPc4	sorcer
a	a	k8xC	a
instant	instant	k?	instant
karty	karta	k1gFnSc2	karta
ani	ani	k8xC	ani
schopností	schopnost	k1gFnSc7	schopnost
svých	svůj	k3xOyFgFnPc2	svůj
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prohra	prohra	k1gFnSc1	prohra
===	===	k?	===
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
prohrát	prohrát	k5eAaPmF	prohrát
čtyřmi	čtyři	k4xCgInPc7	čtyři
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jeho	jeho	k3xOp3gInSc1	jeho
počet	počet	k1gInSc1	počet
životů	život	k1gInPc2	život
klesne	klesnout	k5eAaPmIp3nS	klesnout
na	na	k7c4	na
nebo	nebo	k8xC	nebo
pod	pod	k7c4	pod
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
si	se	k3xPyFc3	se
dobrat	dobrat	k5eAaPmF	dobrat
karty	karta	k1gFnPc4	karta
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
už	už	k6eAd1	už
mu	on	k3xPp3gMnSc3	on
tolik	tolik	k4xDc1	tolik
karet	kareta	k1gFnPc2	kareta
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
některá	některý	k3yIgFnSc1	některý
karta	karta	k1gFnSc1	karta
mu	on	k3xPp3gMnSc3	on
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jinému	jiný	k2eAgMnSc3d1	jiný
hráči	hráč	k1gMnSc3	hráč
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
bytosti	bytost	k1gFnPc1	bytost
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
(	(	kIx(	(
<g/>
infect	infect	k1gInSc1	infect
<g/>
)	)	kIx)	)
–	–	k?	–
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
zraní	zranit	k5eAaPmIp3nS	zranit
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
udělí	udělit	k5eAaPmIp3nS	udělit
mu	on	k3xPp3gMnSc3	on
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
žetonů	žeton	k1gInPc2	žeton
otravy	otrava	k1gFnSc2	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jich	on	k3xPp3gNnPc2	on
hráč	hráč	k1gMnSc1	hráč
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
deset	deset	k4xCc4	deset
<g/>
,	,	kIx,	,
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Remíza	remíza	k1gFnSc1	remíza
===	===	k?	===
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
končí	končit	k5eAaImIp3nS	končit
remízou	remíza	k1gFnSc7	remíza
spíše	spíše	k9	spíše
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
když	když	k8xS	když
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
ztratí	ztratit	k5eAaPmIp3nP	ztratit
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
současně	současně	k6eAd1	současně
</s>
</p>
<p>
<s>
když	když	k8xS	když
by	by	kYmCp3nP	by
vyhodnocování	vyhodnocování	k1gNnSc1	vyhodnocování
várky	várka	k1gFnSc2	várka
trvalo	trvat	k5eAaImAgNnS	trvat
nekonečně	konečně	k6eNd1	konečně
dlouho	dlouho	k6eAd1	dlouho
(	(	kIx(	(
<g/>
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
zacyklení	zacyklení	k1gNnSc3	zacyklení
efektů	efekt	k1gInPc2	efekt
nějakých	nějaký	k3yIgFnPc2	nějaký
karet	kareta	k1gFnPc2	kareta
např.	např.	kA	např.
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
první	první	k4xOgFnSc1	první
–	–	k?	–
situace	situace	k1gFnSc1	situace
podobná	podobný	k2eAgFnSc1d1	podobná
deadlocku	deadlock	k1gInSc2	deadlock
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
když	když	k8xS	když
nějaká	nějaký	k3yIgFnSc1	nějaký
karta	karta	k1gFnSc1	karta
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hra	hra	k1gFnSc1	hra
skončila	skončit	k5eAaPmAgFnS	skončit
remízou	remíza	k1gFnSc7	remíza
</s>
</p>
<p>
<s>
když	když	k8xS	když
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
hráči	hráč	k1gMnPc1	hráč
nedokončí	dokončit	k5eNaPmIp3nP	dokončit
hru	hra	k1gFnSc4	hra
do	do	k7c2	do
časového	časový	k2eAgInSc2d1	časový
limitu	limit	k1gInSc2	limit
a	a	k8xC	a
pěti	pět	k4xCc2	pět
nastavených	nastavený	k2eAgFnPc2d1	nastavená
kolK	kolK	k?	kolK
remíze	remíza	k1gFnSc6	remíza
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
když	když	k8xS	když
nějaké	nějaký	k3yIgNnSc4	nějaký
kouzlo	kouzlo	k1gNnSc4	kouzlo
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
přijdou	přijít	k5eAaPmIp3nP	přijít
o	o	k7c4	o
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
také	také	k9	také
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
remízovanou	remízovaný	k2eAgFnSc4d1	remízovaný
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
hráči	hráč	k1gMnPc1	hráč
nedokončí	dokončit	k5eNaPmIp3nP	dokončit
do	do	k7c2	do
časového	časový	k2eAgInSc2d1	časový
limitu	limit	k1gInSc2	limit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
sázku	sázka	k1gFnSc4	sázka
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Magic	Magic	k1gMnSc1	Magic
vyšel	vyjít	k5eAaPmAgMnS	vyjít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
zamýšlen	zamýšlet	k5eAaImNgMnS	zamýšlet
jako	jako	k8xS	jako
hra	hra	k1gFnSc1	hra
o	o	k7c4	o
sázku	sázka	k1gFnSc4	sázka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
vsadil	vsadit	k5eAaPmAgMnS	vsadit
náhodně	náhodně	k6eAd1	náhodně
zvolenou	zvolený	k2eAgFnSc4d1	zvolená
kartu	karta	k1gFnSc4	karta
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
balíčku	balíček	k1gInSc2	balíček
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
hry	hra	k1gFnSc2	hra
získal	získat	k5eAaPmAgMnS	získat
obě	dva	k4xCgFnPc4	dva
karty	karta	k1gFnPc4	karta
(	(	kIx(	(
<g/>
ante	ante	k6eAd1	ante
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
karty	karta	k1gFnPc1	karta
dokonce	dokonce	k9	dokonce
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
hry	hra	k1gFnPc1	hra
podporovaly	podporovat	k5eAaImAgFnP	podporovat
svými	svůj	k3xOyFgFnPc7	svůj
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
sázku	sázka	k1gFnSc4	sázka
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
praktiky	praktika	k1gFnSc2	praktika
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Magic	Magic	k1gMnSc1	Magic
nebyl	být	k5eNaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
hazardní	hazardní	k2eAgFnSc4d1	hazardní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Planeswalker	Planeswalkero	k1gNnPc2	Planeswalkero
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
planeswalker	planeswalker	k1gMnSc1	planeswalker
(	(	kIx(	(
<g/>
sférochodec	sférochodec	k1gMnSc1	sférochodec
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Magicu	Magicus	k1gInSc6	Magicus
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
hry	hra	k1gFnSc2	hra
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
planeswalker	planeswalker	k1gInSc1	planeswalker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postava	postava	k1gFnSc1	postava
z	z	k7c2	z
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
cestovat	cestovat	k5eAaImF	cestovat
mezi	mezi	k7c7	mezi
sférami	sféra	k1gFnPc7	sféra
(	(	kIx(	(
<g/>
planes	planes	k1gInSc1	planes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgFnPc4	takový
postavy	postava	k1gFnPc4	postava
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Nicol	Nicol	k1gInSc1	Nicol
Bolas	bolas	k1gInSc1	bolas
<g/>
,	,	kIx,	,
Jace	Jace	k1gNnSc1	Jace
Beleren	Belerna	k1gFnPc2	Belerna
nebo	nebo	k8xC	nebo
Chandra	chandra	k1gFnSc1	chandra
Nalaar	Nalaara	k1gFnPc2	Nalaara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typ	typ	k1gInSc1	typ
karty	karta	k1gFnSc2	karta
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Lorwyn-	Lorwyn-	k1gFnSc1	Lorwyn-
ta	ten	k3xDgFnSc1	ten
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pět	pět	k4xCc4	pět
takových	takový	k3xDgFnPc2	takový
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skoro	skoro	k6eAd1	skoro
každá	každý	k3xTgFnSc1	každý
edice	edice	k1gFnSc1	edice
několik	několik	k4yIc4	několik
karet	kareta	k1gFnPc2	kareta
planeswalkerů	planeswalker	k1gInPc2	planeswalker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírání	sbírání	k1gNnPc1	sbírání
karet	kareta	k1gFnPc2	kareta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rarita	rarita	k1gFnSc1	rarita
===	===	k?	===
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
karta	karta	k1gFnSc1	karta
v	v	k7c6	v
Magicu	Magicus	k1gInSc6	Magicus
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
raritu	rarita	k1gFnSc4	rarita
neboli	neboli	k8xC	neboli
vzácnost	vzácnost	k1gFnSc4	vzácnost
<g/>
:	:	kIx,	:
běžné	běžný	k2eAgFnPc4d1	běžná
(	(	kIx(	(
<g/>
common	common	k1gInSc4	common
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neobvyklé	obvyklý	k2eNgNnSc4d1	neobvyklé
(	(	kIx(	(
<g/>
uncommon	uncommon	k1gNnSc4	uncommon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácné	vzácný	k2eAgInPc1d1	vzácný
(	(	kIx(	(
<g/>
rare	rar	k1gInPc1	rar
<g/>
)	)	kIx)	)
a	a	k8xC	a
nově	nově	k6eAd1	nově
mythic	mythic	k1gMnSc1	mythic
rare	rar	k1gFnSc2	rar
<g/>
.	.	kIx.	.
</s>
<s>
Běžných	běžný	k2eAgFnPc2d1	běžná
karet	kareta	k1gFnPc2	kareta
je	být	k5eAaImIp3nS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
neobvyklých	obvyklý	k2eNgInPc2d1	neobvyklý
a	a	k8xC	a
těch	ten	k3xDgMnPc2	ten
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
vzácných	vzácný	k2eAgInPc2d1	vzácný
(	(	kIx(	(
<g/>
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
dělení	dělení	k1gNnSc2	dělení
se	se	k3xPyFc4	se
nepočítají	počítat	k5eNaImIp3nP	počítat
základní	základní	k2eAgFnPc4d1	základní
země	zem	k1gFnPc4	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
edice	edice	k1gFnSc2	edice
Exodus	Exodus	k1gInSc1	Exodus
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rarita	rarita	k1gFnSc1	rarita
znázorňována	znázorňován	k2eAgFnSc1d1	znázorňována
barvou	barva	k1gFnSc7	barva
symbolu	symbol	k1gInSc2	symbol
edice	edice	k1gFnSc2	edice
vpravo	vpravo	k6eAd1	vpravo
uprostřed	uprostřed	k7c2	uprostřed
karty	karta	k1gFnSc2	karta
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1	běžná
karty	karta	k1gFnPc1	karta
mají	mít	k5eAaImIp3nP	mít
černý	černý	k2eAgInSc4d1	černý
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
neobvyklé	obvyklý	k2eNgNnSc4d1	neobvyklé
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
a	a	k8xC	a
vzácné	vzácný	k2eAgInPc4d1	vzácný
zlatý	zlatý	k1gInSc4	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Edicí	edice	k1gFnSc7	edice
Shards	Shardsa	k1gFnPc2	Shardsa
of	of	k?	of
Alara	Alar	k1gInSc2	Alar
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
symbol	symbol	k1gInSc1	symbol
rarity	rarita	k1gFnSc2	rarita
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
mýtický	mýtický	k2eAgInSc1d1	mýtický
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
rarita	rarita	k1gFnSc1	rarita
common-rare	commonar	k1gMnSc5	common-rar
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
silou	síla	k1gFnSc7	síla
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
rarita	rarita	k1gFnSc1	rarita
mythic	mythice	k1gFnPc2	mythice
rare	rare	k1gFnSc1	rare
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
příběhem	příběh	k1gInSc7	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Masterpieces	Masterpieces	k1gInSc4	Masterpieces
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
edicí	edice	k1gFnSc7	edice
Battle	Battle	k1gFnSc2	Battle
for	forum	k1gNnPc2	forum
Zendikar	Zendikara	k1gFnPc2	Zendikara
navíc	navíc	k6eAd1	navíc
přišly	přijít	k5eAaPmAgFnP	přijít
karty	karta	k1gFnPc1	karta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
Masterpieces	Masterpieces	k1gInSc4	Masterpieces
(	(	kIx(	(
<g/>
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
kus	kus	k1gInSc4	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
karty	karta	k1gFnPc1	karta
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
najít	najít	k5eAaPmF	najít
náhodně	náhodně	k6eAd1	náhodně
v	v	k7c6	v
boosterech	booster	k1gInPc6	booster
určité	určitý	k2eAgFnSc2d1	určitá
sady	sada	k1gFnSc2	sada
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
jiný	jiný	k2eAgInSc4d1	jiný
symbol	symbol	k1gInSc4	symbol
a	a	k8xC	a
grafické	grafický	k2eAgNnSc4d1	grafické
provedení	provedení	k1gNnSc4	provedení
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc4d1	ostatní
karty	karta	k1gFnPc4	karta
v	v	k7c6	v
boosteru	booster	k1gInSc6	booster
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
foilové	foilový	k2eAgFnPc1d1	foilová
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
legální	legální	k2eAgMnPc1d1	legální
na	na	k7c4	na
Limited	limited	k2eAgMnPc4d1	limited
(	(	kIx(	(
<g/>
booster	booster	k1gInSc1	booster
draft	draft	k1gInSc1	draft
<g/>
,	,	kIx,	,
sealed	sealed	k1gMnSc1	sealed
deck	deck	k1gMnSc1	deck
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
formátech	formát	k1gInPc6	formát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
legální	legální	k2eAgFnPc1d1	legální
jejich	jejich	k3xOp3gFnPc1	jejich
původní	původní	k2eAgFnPc1d1	původní
verze	verze	k1gFnPc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
šanci	šance	k1gFnSc3	šance
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
nalezení	nalezení	k1gNnSc4	nalezení
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Vyšly	vyjít	k5eAaPmAgInP	vyjít
s	s	k7c7	s
edicemi	edice	k1gFnPc7	edice
Battle	Battle	k1gFnPc4	Battle
for	forum	k1gNnPc2	forum
Zendikar	Zendikara	k1gFnPc2	Zendikara
<g/>
,	,	kIx,	,
Kaladesh	Kaladesha	k1gFnPc2	Kaladesha
<g/>
,	,	kIx,	,
Amonkhet	Amonkheta	k1gFnPc2	Amonkheta
<g/>
,	,	kIx,	,
Guilds	Guildsa	k1gFnPc2	Guildsa
of	of	k?	of
Ravnica	Ravnicus	k1gMnSc2	Ravnicus
a	a	k8xC	a
Ravnica	Ravnicus	k1gMnSc2	Ravnicus
Allegiance	Allegianec	k1gMnSc2	Allegianec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
kartami	karta	k1gFnPc7	karta
===	===	k?	===
</s>
</p>
<p>
<s>
Magic	Magic	k1gMnSc1	Magic
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
baleních	balení	k1gNnPc6	balení
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
balením	balení	k1gNnSc7	balení
jsou	být	k5eAaImIp3nP	být
booster	booster	k1gInSc4	booster
packy	packa	k1gFnSc2	packa
obsahující	obsahující	k2eAgInSc4d1	obsahující
15	[number]	k4	15
náhodných	náhodný	k2eAgFnPc2d1	náhodná
karet	kareta	k1gFnPc2	kareta
(	(	kIx(	(
<g/>
1	[number]	k4	1
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
nebo	nebo	k8xC	nebo
mythickou	mythický	k2eAgFnSc4d1	mythická
<g/>
,	,	kIx,	,
3	[number]	k4	3
neobvyklé	obvyklý	k2eNgInPc1d1	neobvyklý
<g/>
,	,	kIx,	,
10	[number]	k4	10
běžných	běžný	k2eAgFnPc2d1	běžná
a	a	k8xC	a
1	[number]	k4	1
základní	základní	k2eAgFnSc4d1	základní
zem	zem	k1gFnSc4	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
koupit	koupit	k5eAaPmF	koupit
i	i	k9	i
tzv.	tzv.	kA	tzv.
Intro	Intro	k1gNnSc4	Intro
packy	packa	k1gFnSc2	packa
nebo	nebo	k8xC	nebo
Planeswalker	Planeswalker	k1gMnSc1	Planeswalker
decky	decka	k1gFnPc4	decka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
již	již	k6eAd1	již
sestavené	sestavený	k2eAgInPc4d1	sestavený
balíčky	balíček	k1gInPc4	balíček
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
booster	boostra	k1gFnPc2	boostra
packy	packy	k6eAd1	packy
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zakoupení	zakoupení	k1gNnSc6	zakoupení
začít	začít	k5eAaPmF	začít
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
hráč	hráč	k1gMnSc1	hráč
získal	získat	k5eAaPmAgMnS	získat
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
nejlépe	dobře	k6eAd3	dobře
hodí	hodit	k5eAaImIp3nP	hodit
do	do	k7c2	do
balíčku	balíček	k1gInSc2	balíček
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
je	on	k3xPp3gInPc4	on
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
nebo	nebo	k8xC	nebo
nakupovat	nakupovat	k5eAaBmF	nakupovat
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
hráči	hráč	k1gMnPc1	hráč
navzájem	navzájem	k6eAd1	navzájem
karty	karta	k1gFnPc4	karta
prodávají	prodávat	k5eAaImIp3nP	prodávat
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
kvalitách	kvalita	k1gFnPc6	kvalita
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
raritě	rarita	k1gFnSc6	rarita
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
běžných	běžný	k2eAgFnPc2d1	běžná
karet	kareta	k1gFnPc2	kareta
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
až	až	k9	až
20	[number]	k4	20
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
neobvyklých	obvyklý	k2eNgMnPc2d1	neobvyklý
se	se	k3xPyFc4	se
vyšplhá	vyšplhat	k5eAaPmIp3nS	vyšplhat
cca	cca	kA	cca
na	na	k7c4	na
200	[number]	k4	200
Kč	Kč	kA	Kč
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
vzácných	vzácný	k2eAgFnPc2d1	vzácná
karet	kareta	k1gFnPc2	kareta
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
tisíce	tisíc	k4xCgInPc4	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
karty	karta	k1gFnPc1	karta
ze	z	k7c2	z
starších	starý	k2eAgFnPc2d2	starší
edicí	edice	k1gFnPc2	edice
nebo	nebo	k8xC	nebo
limitovaných	limitovaný	k2eAgFnPc2d1	limitovaná
sérií	série	k1gFnPc2	série
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
vycházejí	vycházet	k5eAaImIp3nP	vycházet
orientační	orientační	k2eAgInPc1d1	orientační
ceníky	ceník	k1gInPc1	ceník
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
obchody	obchod	k1gInPc4	obchod
(	(	kIx(	(
<g/>
i	i	k9	i
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
balení	balení	k1gNnSc2	balení
možné	možný	k2eAgFnSc2d1	možná
zakoupit	zakoupit	k5eAaPmF	zakoupit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
karty	karta	k1gFnPc4	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
boosterech	booster	k1gInPc6	booster
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
narazit	narazit	k5eAaPmF	narazit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
foilové	foilový	k2eAgFnPc4d1	foilová
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
vzhledově	vzhledově	k6eAd1	vzhledově
speciální	speciální	k2eAgFnPc1d1	speciální
<g/>
,	,	kIx,	,
blýskavé	blýskavý	k2eAgFnPc1d1	blýskavá
verze	verze	k1gFnPc1	verze
karet	kareta	k1gFnPc2	kareta
vydaných	vydaný	k2eAgFnPc2d1	vydaná
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
edici	edice	k1gFnSc6	edice
(	(	kIx(	(
<g/>
herní	herní	k2eAgFnSc7d1	herní
funkčností	funkčnost	k1gFnSc7	funkčnost
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
karty	karta	k1gFnPc4	karta
samozřejmě	samozřejmě	k6eAd1	samozřejmě
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k8xC	jako
jejich	jejich	k3xOp3gInPc7	jejich
nefoilové	foilové	k2eNgInPc7d1	foilové
protějšky	protějšek	k1gInPc7	protějšek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
foilových	foilův	k2eAgFnPc2d1	foilův
karet	kareta	k1gFnPc2	kareta
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
nefoilových	foilových	k2eNgFnPc1d1	foilových
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
foilové	foilový	k2eAgFnPc1d1	foilová
karty	karta	k1gFnPc1	karta
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
rarity	rarita	k1gFnPc4	rarita
a	a	k8xC	a
v	v	k7c6	v
boosteru	booster	k1gInSc6	booster
vždy	vždy	k6eAd1	vždy
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
kartu	karta	k1gFnSc4	karta
běžnou	běžný	k2eAgFnSc4d1	běžná
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
neplatí	platit	k5eNaImIp3nS	platit
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
edic	edice	k1gFnPc2	edice
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
foilová	foilový	k2eAgFnSc1d1	foilová
karta	karta	k1gFnSc1	karta
v	v	k7c6	v
boosteru	booster	k1gInSc6	booster
nahrazovala	nahrazovat	k5eAaImAgFnS	nahrazovat
kartu	karta	k1gFnSc4	karta
též	tenž	k3xDgFnSc2	tenž
rarity	rarita	k1gFnSc2	rarita
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
sama	sám	k3xTgFnSc1	sám
měla	mít	k5eAaImAgFnS	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
otevřít	otevřít	k5eAaPmF	otevřít
v	v	k7c6	v
boosteru	booster	k1gInSc6	booster
dvě	dva	k4xCgFnPc4	dva
vzácné	vzácný	k2eAgFnPc4d1	vzácná
karty	karta	k1gFnPc4	karta
(	(	kIx(	(
<g/>
jednu	jeden	k4xCgFnSc4	jeden
standardní	standardní	k2eAgFnSc4d1	standardní
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
foilovou	foilová	k1gFnSc4	foilová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Foilová	Foilový	k2eAgFnSc1d1	Foilový
karta	karta	k1gFnSc1	karta
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
každého	každý	k3xTgInSc2	každý
boosteru	booster	k1gInSc2	booster
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
otevření	otevření	k1gNnSc2	otevření
foilové	foilový	k2eAgFnSc2d1	foilová
karty	karta	k1gFnSc2	karta
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
poměrem	poměr	k1gInSc7	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
teoreticky	teoreticky	k6eAd1	teoreticky
musíte	muset	k5eAaImIp2nP	muset
koupit	koupit	k5eAaPmF	koupit
<g/>
,	,	kIx,	,
než	než	k8xS	než
otevřete	otevřít	k5eAaPmIp2nP	otevřít
jednu	jeden	k4xCgFnSc4	jeden
foilovou	foilový	k2eAgFnSc4d1	foilová
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
edice	edice	k1gFnSc2	edice
Shadowmoor	Shadowmoora	k1gFnPc2	Shadowmoora
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vůbec	vůbec	k9	vůbec
nejdražší	drahý	k2eAgFnSc1d3	nejdražší
karta	karta	k1gFnSc1	karta
v	v	k7c6	v
MTG	MTG	kA	MTG
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Black	Black	k1gInSc1	Black
Lotus	Lotus	kA	Lotus
(	(	kIx(	(
<g/>
cca	cca	kA	cca
529	[number]	k4	529
999	[number]	k4	999
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
Alpha	Alpha	k1gFnSc1	Alpha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
jen	jen	k9	jen
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
edicích	edice	k1gFnPc6	edice
(	(	kIx(	(
<g/>
Alpha	Alpha	k1gFnSc1	Alpha
<g/>
,	,	kIx,	,
Beta	beta	k1gNnSc1	beta
a	a	k8xC	a
Unlimited	Unlimited	k1gInSc1	Unlimited
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
příliš	příliš	k6eAd1	příliš
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
edicích	edice	k1gFnPc6	edice
se	se	k3xPyFc4	se
už	už	k6eAd1	už
neobjevila	objevit	k5eNaPmAgFnS	objevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Foilové	Foilový	k2eAgFnPc1d1	Foilový
karty	karta	k1gFnPc1	karta
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
vycházejí	vycházet	k5eAaImIp3nP	vycházet
tzv.	tzv.	kA	tzv.
foilové	foilový	k2eAgFnPc4d1	foilová
karty	karta	k1gFnPc4	karta
mající	mající	k2eAgInSc4d1	mající
lesklý	lesklý	k2eAgInSc4d1	lesklý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
náhodně	náhodně	k6eAd1	náhodně
mezi	mezi	k7c7	mezi
kartami	karta	k1gFnPc7	karta
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
balení	balení	k1gNnSc6	balení
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
vypadají	vypadat	k5eAaPmIp3nP	vypadat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jiné	jiný	k2eAgFnPc1d1	jiná
karty	karta	k1gFnPc1	karta
a	a	k8xC	a
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
hraje	hrát	k5eAaImIp3nS	hrát
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
s	s	k7c7	s
nefoilovými	nefoilová	k1gFnPc7	nefoilová
(	(	kIx(	(
<g/>
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
dlouhodobého	dlouhodobý	k2eAgMnSc4d1	dlouhodobý
hráče	hráč	k1gMnSc4	hráč
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
máte	mít	k5eAaImIp2nP	mít
na	na	k7c6	na
stole	stol	k1gInSc6	stol
dvě	dva	k4xCgFnPc1	dva
stejné	stejný	k2eAgFnPc1d1	stejná
karty	karta	k1gFnPc1	karta
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
foilová	foilová	k1gFnSc1	foilová
<g/>
,	,	kIx,	,
soupeř	soupeř	k1gMnSc1	soupeř
se	se	k3xPyFc4	se
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
kterou	který	k3yQgFnSc4	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vám	vy	k3xPp2nPc3	vy
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
foilovou	foilová	k1gFnSc4	foilová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
karta	karta	k1gFnSc1	karta
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
foilové	foilový	k2eAgFnSc6d1	foilová
i	i	k8xC	i
nefoilové	foilové	k2eNgFnSc6d1	foilové
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ta	ten	k3xDgFnSc1	ten
foilová	foilová	k1gFnSc1	foilová
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
sběrateli	sběratel	k1gMnPc7	sběratel
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
vyšší	vysoký	k2eAgFnSc4d2	vyšší
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nově	nova	k1gFnSc3	nova
vydávané	vydávaný	k2eAgFnSc2d1	vydávaná
edice	edice	k1gFnSc2	edice
===	===	k?	===
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
vyjde	vyjít	k5eAaPmIp3nS	vyjít
jeden	jeden	k4xCgInSc1	jeden
blok	blok	k1gInSc1	blok
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tvořen	tvořen	k2eAgInSc1d1	tvořen
jednou	jednou	k6eAd1	jednou
velkou	velký	k2eAgFnSc7d1	velká
edicí	edice	k1gFnSc7	edice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
249	[number]	k4	249
karet	kareta	k1gFnPc2	kareta
se	s	k7c7	s
základními	základní	k2eAgFnPc7d1	základní
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
velkou	velký	k2eAgFnSc4d1	velká
edici	edice	k1gFnSc4	edice
"	"	kIx"	"
<g/>
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
<g/>
"	"	kIx"	"
dvě	dva	k4xCgFnPc1	dva
menší	malý	k2eAgFnPc1d2	menší
edice	edice	k1gFnPc1	edice
o	o	k7c6	o
145	[number]	k4	145
kartách	karta	k1gFnPc6	karta
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
blok	blok	k1gInSc1	blok
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něco	něco	k3yInSc4	něco
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
specifické	specifický	k2eAgFnPc4d1	specifická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
blok	blok	k1gInSc1	blok
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
jedním	jeden	k4xCgInSc7	jeden
příběhem	příběh	k1gInSc7	příběh
vydávaným	vydávaný	k2eAgInSc7d1	vydávaný
knižně	knižně	k6eAd1	knižně
a	a	k8xC	a
karty	karta	k1gFnPc1	karta
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
představují	představovat	k5eAaImIp3nP	představovat
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc1	postava
a	a	k8xC	a
události	událost	k1gFnPc1	událost
tohoto	tento	k3xDgInSc2	tento
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
vychází	vycházet	k5eAaImIp3nS	vycházet
nová	nový	k2eAgFnSc1d1	nová
tzv.	tzv.	kA	tzv.
základní	základní	k2eAgFnSc1d1	základní
edice	edice	k1gFnSc1	edice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Magic	Magice	k1gFnPc2	Magice
The	The	k1gFnPc2	The
Gathering	Gathering	k1gInSc1	Gathering
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
co	co	k3yInSc4	co
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
roku	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyjde	vyjít	k5eAaPmIp3nS	vyjít
<g/>
)	)	kIx)	)
CORE	CORE	kA	CORE
SET	set	k1gInSc1	set
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starších	starý	k2eAgFnPc2d2	starší
základních	základní	k2eAgFnPc2d1	základní
edic	edice	k1gFnPc2	edice
mají	mít	k5eAaImIp3nP	mít
ty	ten	k3xDgFnPc1	ten
současné	současný	k2eAgFnPc1d1	současná
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
nových	nový	k2eAgFnPc2d1	nová
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
edice	edice	k1gFnSc1	edice
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
karty	karta	k1gFnPc1	karta
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obsažené	obsažený	k2eAgInPc1d1	obsažený
nemají	mít	k5eNaImIp3nP	mít
tak	tak	k9	tak
složité	složitý	k2eAgFnPc4d1	složitá
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
karty	karta	k1gFnPc1	karta
v	v	k7c6	v
rozšiřujících	rozšiřující	k2eAgFnPc6d1	rozšiřující
edicích	edice	k1gFnPc6	edice
<g/>
.	.	kIx.	.
</s>
<s>
Vydávání	vydávání	k1gNnSc1	vydávání
základních	základní	k2eAgFnPc2d1	základní
edic	edice	k1gFnPc2	edice
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
edicí	edice	k1gFnPc2	edice
Magic	Magice	k1gFnPc2	Magice
Origins	Origins	k1gInSc1	Origins
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
znovu	znovu	k6eAd1	znovu
odstartováno	odstartován	k2eAgNnSc1d1	odstartováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počty	počet	k1gInPc1	počet
karet	kareta	k1gFnPc2	kareta
v	v	k7c6	v
edicích	edice	k1gFnPc6	edice
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různé-	různé-	k?	různé-
například	například	k6eAd1	například
Shadowmoor	Shadowmoora	k1gFnPc2	Shadowmoora
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Lorwyn	Lorwyn	k1gInSc1	Lorwyn
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
301	[number]	k4	301
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blok	blok	k1gInSc1	blok
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jen	jen	k8xS	jen
třemi	tři	k4xCgFnPc7	tři
edicemi	edice	k1gFnPc7	edice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Lorwyn	Lorwyn	k1gNnSc1	Lorwyn
blok	blok	k1gInSc1	blok
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
edice	edice	k1gFnPc4	edice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Ixalan	Ixalan	k1gInSc1	Ixalan
blok	blok	k1gInSc1	blok
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
edice	edice	k1gFnSc2	edice
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
edice	edice	k1gFnSc1	edice
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
velkou	velká	k1gFnSc4	velká
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
malé	malá	k1gFnPc4	malá
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zendikar	Zendikar	k1gInSc1	Zendikar
blok	blok	k1gInSc1	blok
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
velké	velká	k1gFnSc2	velká
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
malou	malý	k2eAgFnSc4d1	malá
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
balíčku	balíček	k1gInSc2	balíček
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
hráč	hráč	k1gMnSc1	hráč
vyhrávat	vyhrávat	k5eAaImF	vyhrávat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
balíček	balíček	k1gInSc4	balíček
postavit	postavit	k5eAaPmF	postavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
karty	karta	k1gFnPc1	karta
dobře	dobře	k6eAd1	dobře
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
s	s	k7c7	s
balíčkem	balíček	k1gMnSc7	balíček
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
karet	kareta	k1gFnPc2	kareta
omezen	omezit	k5eAaPmNgMnS	omezit
druhem	druh	k1gInSc7	druh
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
postavit	postavit	k5eAaPmF	postavit
balíček	balíček	k1gInSc4	balíček
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
nově	nově	k6eAd1	nově
vydanou	vydaný	k2eAgFnSc7d1	vydaná
edicí	edice	k1gFnSc7	edice
přibudou	přibýt	k5eAaPmIp3nP	přibýt
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
balíček	balíček	k1gInSc1	balíček
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
balíčkům	balíček	k1gInPc3	balíček
silný	silný	k2eAgInSc4d1	silný
a	a	k8xC	a
proti	proti	k7c3	proti
jiným	jiný	k2eAgFnPc3d1	jiná
zase	zase	k9	zase
slabý	slabý	k2eAgMnSc1d1	slabý
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
balíčky	balíček	k1gInPc1	balíček
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Agresivní	agresivní	k2eAgFnSc1d1	agresivní
–	–	k?	–
útočná	útočný	k2eAgFnSc1d1	útočná
strategie	strategie	k1gFnSc1	strategie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
snaží	snažit	k5eAaImIp3nS	snažit
rychle	rychle	k6eAd1	rychle
vynést	vynést	k5eAaPmF	vynést
množství	množství	k1gNnSc4	množství
nestvůr	nestvůra	k1gFnPc2	nestvůra
a	a	k8xC	a
porazit	porazit	k5eAaPmF	porazit
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
soupeře	soupeř	k1gMnSc4	soupeř
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
ten	ten	k3xDgInSc1	ten
vyloží	vyložit	k5eAaPmIp3nS	vyložit
silnější	silný	k2eAgNnPc4d2	silnější
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
balíčky	balíček	k1gInPc1	balíček
často	často	k6eAd1	často
využívají	využívat	k5eAaPmIp3nP	využívat
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nepříteli	nepřítel	k1gMnSc3	nepřítel
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
rozvinout	rozvinout	k5eAaPmF	rozvinout
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
odstraňováním	odstraňování	k1gNnSc7	odstraňování
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
nestvůr	nestvůra	k1gFnPc2	nestvůra
<g/>
,	,	kIx,	,
ničením	ničení	k1gNnSc7	ničení
protihráčových	protihráčův	k2eAgFnPc2d1	protihráčova
zemí	zem	k1gFnPc2	zem
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
–	–	k?	–
obranná	obranný	k2eAgFnSc1d1	obranná
strategie	strategie	k1gFnSc1	strategie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
napřed	napřed	k6eAd1	napřed
neprůstřelně	průstřelně	k6eNd1	průstřelně
zajistí	zajistit	k5eAaPmIp3nS	zajistit
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
,	,	kIx,	,
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
hrozby	hrozba	k1gFnPc4	hrozba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
protihráč	protihráč	k1gMnSc1	protihráč
sešle	seslat	k5eAaPmIp3nS	seslat
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
získává	získávat	k5eAaImIp3nS	získávat
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
začne	začít	k5eAaPmIp3nS	začít
vykládat	vykládat	k5eAaImF	vykládat
manově	manově	k6eAd1	manově
drahé	drahý	k2eAgNnSc1d1	drahé
a	a	k8xC	a
nesmírně	smírně	k6eNd1	smírně
silné	silný	k2eAgFnPc1d1	silná
karty	karta	k1gFnPc1	karta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nepříteli	nepřítel	k1gMnSc3	nepřítel
nedávají	dávat	k5eNaImIp3nP	dávat
mnoho	mnoho	k4c4	mnoho
šancí	šance	k1gFnPc2	šance
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
typem	typ	k1gInSc7	typ
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
balíčku	balíček	k1gInSc2	balíček
je	být	k5eAaImIp3nS	být
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
"	"	kIx"	"
<g/>
Mill	Mill	k1gInSc1	Mill
<g/>
"	"	kIx"	"
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
strategie	strategie	k1gFnSc1	strategie
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
postupném	postupný	k2eAgNnSc6d1	postupné
odemílání	odemílání	k1gNnSc6	odemílání
karet	kareta	k1gFnPc2	kareta
z	z	k7c2	z
protivníkova	protivníkův	k2eAgInSc2d1	protivníkův
balíčku	balíček	k1gInSc2	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
už	už	k6eAd1	už
mu	on	k3xPp3gMnSc3	on
nezbude	zbýt	k5eNaPmIp3nS	zbýt
žádná	žádný	k3yNgFnSc1	žádný
karta	karta	k1gFnSc1	karta
v	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
<g/>
,	,	kIx,	,
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kombo	Komba	k1gMnSc5	Komba
–	–	k?	–
protože	protože	k8xS	protože
v	v	k7c6	v
Magicu	Magicus	k1gInSc6	Magicus
je	být	k5eAaImIp3nS	být
vydáváno	vydávat	k5eAaPmNgNnS	vydávat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
karet	kareta	k1gFnPc2	kareta
s	s	k7c7	s
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
kombinace	kombinace	k1gFnSc1	kombinace
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
samy	sám	k3xTgFnPc1	sám
třeba	třeba	k9	třeba
nic	nic	k3yNnSc1	nic
moc	moc	k6eAd1	moc
neumí	umět	k5eNaImIp3nS	umět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
vhodné	vhodný	k2eAgNnSc1d1	vhodné
zkombinování	zkombinování	k1gNnSc1	zkombinování
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
soupeře	soupeř	k1gMnSc4	soupeř
rychlou	rychlý	k2eAgFnSc4d1	rychlá
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Kombo	Komba	k1gMnSc5	Komba
balíček	balíček	k1gMnSc1	balíček
tedy	tedy	k9	tedy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tyto	tento	k3xDgFnPc4	tento
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
balíčku	balíček	k1gInSc2	balíček
jsou	být	k5eAaImIp3nP	být
karty	karta	k1gFnPc1	karta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožní	umožnit	k5eAaPmIp3nP	umožnit
jejich	jejich	k3xOp3gNnSc4	jejich
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vyhledání	vyhledání	k1gNnSc4	vyhledání
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
a	a	k8xC	a
seslání	seslání	k1gNnSc6	seslání
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
typy	typ	k1gInPc1	typ
balíčků	balíček	k1gMnPc2	balíček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgInPc1	tento
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgInPc1d1	základní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
balíčku	balíček	k1gInSc2	balíček
zařadit	zařadit	k5eAaPmF	zařadit
vhodný	vhodný	k2eAgInSc4d1	vhodný
počet	počet	k1gInSc4	počet
zemí	zem	k1gFnPc2	zem
vyrábějících	vyrábějící	k2eAgFnPc2d1	vyrábějící
manu	mana	k1gFnSc4	mana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nestávalo	stávat	k5eNaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
mnoho	mnoho	k4c4	mnoho
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
manu	mana	k1gFnSc4	mana
na	na	k7c4	na
jejich	jejich	k3xOp3gNnPc4	jejich
seslání	seslání	k1gNnPc4	seslání
(	(	kIx(	(
<g/>
mana	mana	k1gFnSc1	mana
screw	screw	k?	screw
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
mnoho	mnoho	k6eAd1	mnoho
many	mana	k1gFnPc4	mana
a	a	k8xC	a
málo	málo	k4c4	málo
kouzel	kouzlo	k1gNnPc2	kouzlo
(	(	kIx(	(
<g/>
mana	mana	k1gFnSc1	mana
flood	flooda	k1gFnPc2	flooda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
v	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
kouzla	kouzlo	k1gNnSc2	kouzlo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc2	jeden
barvy	barva	k1gFnSc2	barva
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
země	zem	k1gFnPc1	zem
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
manu	mana	k1gFnSc4	mana
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc2	jeden
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
kouzla	kouzlo	k1gNnSc2	kouzlo
jiné	jiný	k2eAgFnSc2d1	jiná
barvy	barva	k1gFnSc2	barva
než	než	k8xS	než
země	zem	k1gFnSc2	zem
na	na	k7c6	na
stole	stol	k1gInSc6	stol
(	(	kIx(	(
<g/>
color	color	k1gInSc1	color
screw	screw	k?	screw
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
opravdu	opravdu	k6eAd1	opravdu
dobrých	dobrý	k2eAgInPc2d1	dobrý
balíčků	balíček	k1gInPc2	balíček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
se	se	k3xPyFc4	se
solidně	solidně	k6eAd1	solidně
umístit	umístit	k5eAaPmF	umístit
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
jen	jen	k9	jen
asi	asi	k9	asi
10	[number]	k4	10
<g/>
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
vydaných	vydaný	k2eAgFnPc2d1	vydaná
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
cena	cena	k1gFnSc1	cena
karet	kareta	k1gFnPc2	kareta
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
balíčku	balíček	k1gInSc6	balíček
se	se	k3xPyFc4	se
vyšplhá	vyšplhat	k5eAaPmIp3nS	vyšplhat
na	na	k7c4	na
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Seznamy	seznam	k1gInPc1	seznam
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
balíčků	balíček	k1gMnPc2	balíček
jsou	být	k5eAaImIp3nP	být
zveřejňovány	zveřejňovat	k5eAaImNgInP	zveřejňovat
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
i	i	k8xC	i
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizované	organizovaný	k2eAgNnSc4d1	organizované
hraní	hraní	k1gNnSc4	hraní
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Turnaje	turnaj	k1gInPc1	turnaj
===	===	k?	===
</s>
</p>
<p>
<s>
Turnaje	turnaj	k1gInPc1	turnaj
v	v	k7c6	v
M	M	kA	M
<g/>
:	:	kIx,	:
<g/>
tG	tg	kA	tg
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
formátů	formát	k1gInPc2	formát
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
turnaji	turnaj	k1gInSc6	turnaj
smí	smět	k5eAaImIp3nS	smět
postavit	postavit	k5eAaPmF	postavit
balíček	balíček	k1gInSc1	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
turnaje	turnaj	k1gInPc4	turnaj
konstruované	konstruovaný	k2eAgInPc4d1	konstruovaný
a	a	k8xC	a
limitované	limitovaný	k2eAgInPc4d1	limitovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
konstruovaný	konstruovaný	k2eAgInSc4d1	konstruovaný
turnaj	turnaj	k1gInSc4	turnaj
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
přinese	přinést	k5eAaPmIp3nS	přinést
balíček	balíček	k1gMnSc1	balíček
postavený	postavený	k2eAgMnSc1d1	postavený
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
odehraje	odehrát	k5eAaPmIp3nS	odehrát
celý	celý	k2eAgInSc1d1	celý
turnaj	turnaj	k1gInSc1	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Balíček	balíček	k1gMnSc1	balíček
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
nejméně	málo	k6eAd3	málo
60	[number]	k4	60
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
karta	karta	k1gFnSc1	karta
kromě	kromě	k7c2	kromě
základních	základní	k2eAgFnPc2d1	základní
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nesmí	smět	k5eNaImIp3nS	smět
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
<g/>
krát	krát	k6eAd1	krát
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
karta	karta	k1gFnSc1	karta
Relentless	Relentlessa	k1gFnPc2	Relentlessa
Rats	Ratsa	k1gFnPc2	Ratsa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
můžete	moct	k5eAaImIp2nP	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
balíku	balík	k1gInSc6	balík
kolikrát	kolikrát	k6eAd1	kolikrát
chcete	chtít	k5eAaImIp2nP	chtít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
balíčku	balíček	k1gMnSc3	balíček
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
navíc	navíc	k6eAd1	navíc
15	[number]	k4	15
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sideboard	sideboard	k1gInSc1	sideboard
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
turnajovém	turnajový	k2eAgNnSc6d1	turnajové
kole	kolo	k1gNnSc6	kolo
může	moct	k5eAaImIp3nS	moct
před	před	k7c7	před
každou	každý	k3xTgFnSc7	každý
hrou	hra	k1gFnSc7	hra
kromě	kromě	k7c2	kromě
první	první	k4xOgFnSc2	první
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
karty	karta	k1gFnSc2	karta
mezi	mezi	k7c7	mezi
balíčkem	balíček	k1gMnSc7	balíček
a	a	k8xC	a
sideboardem	sideboard	k1gMnSc7	sideboard
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gInSc1	jeho
balíček	balíček	k1gInSc1	balíček
byl	být	k5eAaImAgInS	být
lépe	dobře	k6eAd2	dobře
vybaven	vybavit	k5eAaPmNgInS	vybavit
proti	proti	k7c3	proti
konkrétnímu	konkrétní	k2eAgMnSc3d1	konkrétní
soupeři	soupeř	k1gMnSc3	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sideboardu	sideboard	k1gInSc6	sideboard
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zaměřené	zaměřený	k2eAgFnPc1d1	zaměřená
proti	proti	k7c3	proti
červeným	červený	k2eAgFnPc3d1	červená
kartám	karta	k1gFnPc3	karta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
jiným	jiný	k1gMnPc3	jiný
jsou	být	k5eAaImIp3nP	být
bezcenné	bezcenný	k2eAgFnPc1d1	bezcenná
<g/>
;	;	kIx,	;
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
-li	i	k?	-li
v	v	k7c6	v
první	první	k4xOgFnSc6	první
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
soupeř	soupeř	k1gMnSc1	soupeř
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
červené	červený	k2eAgFnSc2d1	červená
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
zařadí	zařadit	k5eAaPmIp3nS	zařadit
si	se	k3xPyFc3	se
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
hrou	hra	k1gFnSc7	hra
karty	karta	k1gFnPc4	karta
proti	proti	k7c3	proti
červené	červená	k1gFnSc3	červená
ze	z	k7c2	z
sideboardu	sideboard	k1gInSc2	sideboard
do	do	k7c2	do
základního	základní	k2eAgInSc2d1	základní
balíčku	balíček	k1gInSc2	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Konstruované	konstruovaný	k2eAgInPc1d1	konstruovaný
turnaje	turnaj	k1gInPc1	turnaj
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakých	jaký	k3yIgFnPc2	jaký
karet	kareta	k1gFnPc2	kareta
je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
si	se	k3xPyFc3	se
balíček	balíček	k1gInSc4	balíček
postavit	postavit	k5eAaPmF	postavit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
standard	standard	k1gInSc1	standard
–	–	k?	–
smějí	smát	k5eAaImIp3nP	smát
se	se	k3xPyFc4	se
hrát	hrát	k5eAaImF	hrát
jen	jen	k9	jen
karty	karta	k1gFnPc4	karta
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
dvou	dva	k4xCgInPc2	dva
vydaných	vydaný	k2eAgInPc2d1	vydaný
bloků	blok	k1gInPc2	blok
a	a	k8xC	a
základní	základní	k2eAgFnPc4d1	základní
nové	nový	k2eAgFnPc4d1	nová
edice	edice	k1gFnPc4	edice
+	+	kIx~	+
edice	edice	k1gFnSc1	edice
mezi	mezi	k7c4	mezi
bloky	blok	k1gInPc4	blok
</s>
</p>
<p>
<s>
momentálně	momentálně	k6eAd1	momentálně
povolené	povolený	k2eAgFnPc4d1	povolená
edice	edice	k1gFnPc4	edice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ixalan	Ixalan	k1gMnSc1	Ixalan
</s>
</p>
<p>
<s>
Rivals	Rivals	k1gInSc1	Rivals
of	of	k?	of
Ixalan	Ixalan	k1gInSc1	Ixalan
</s>
</p>
<p>
<s>
Dominaria	Dominarium	k1gNnPc1	Dominarium
</s>
</p>
<p>
<s>
Core	Core	k6eAd1	Core
2019	[number]	k4	2019
</s>
</p>
<p>
<s>
Guilds	Guilds	k6eAd1	Guilds
of	of	k?	of
Ravnica	Ravnica	k1gMnSc1	Ravnica
</s>
</p>
<p>
<s>
Ravnica	Ravnic	k2eAgFnSc1d1	Ravnica
Allegiance	Allegiance	k1gFnSc1	Allegiance
</s>
</p>
<p>
<s>
extended	extended	k1gInSc1	extended
–	–	k?	–
smějí	smát	k5eAaImIp3nP	smát
se	se	k3xPyFc4	se
hrát	hrát	k5eAaImF	hrát
karty	karta	k1gFnPc1	karta
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
čtyř	čtyři	k4xCgInPc2	čtyři
bloků	blok	k1gInPc2	blok
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
edicí	edice	k1gFnPc2	edice
jsou	být	k5eAaImIp3nP	být
povoleny	povolen	k2eAgInPc1d1	povolen
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyšly	vyjít	k5eAaPmAgInP	vyjít
mezi	mezi	k7c7	mezi
povolenými	povolený	k2eAgInPc7d1	povolený
bloky	blok	k1gInPc7	blok
</s>
</p>
<p>
<s>
vintage	vintag	k1gInPc1	vintag
<g/>
,	,	kIx,	,
legacy	legacy	k1gInPc1	legacy
–	–	k?	–
povoleny	povolen	k2eAgInPc1d1	povolen
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
vydané	vydaný	k2eAgFnPc4d1	vydaná
edice	edice	k1gFnPc4	edice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
typu	typ	k1gInSc6	typ
vintage	vintage	k6eAd1	vintage
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc4	seznam
mnoha	mnoho	k4c2	mnoho
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
balík	balík	k1gInSc1	balík
smí	smět	k5eAaImIp3nS	smět
obsahovat	obsahovat	k5eAaImF	obsahovat
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
typu	typ	k1gInSc6	typ
legacy	legaca	k1gFnSc2	legaca
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
karty	karta	k1gFnPc4	karta
zakázány	zakázán	k2eAgFnPc4d1	zakázána
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příliš	příliš	k6eAd1	příliš
silné	silný	k2eAgFnPc4d1	silná
karty	karta	k1gFnPc4	karta
z	z	k7c2	z
počátečních	počáteční	k2eAgFnPc2d1	počáteční
edicí	edice	k1gFnPc2	edice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
prakticky	prakticky	k6eAd1	prakticky
nesehnatelné	sehnatelný	k2eNgNnSc1d1	nesehnatelné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
block	block	k1gMnSc1	block
constructed	constructed	k1gMnSc1	constructed
–	–	k?	–
povoleny	povolen	k2eAgInPc1d1	povolen
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
karty	karta	k1gFnPc1	karta
z	z	k7c2	z
určitého	určitý	k2eAgInSc2d1	určitý
bloku	blok	k1gInSc2	blok
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
toho	ten	k3xDgInSc2	ten
posledního	poslední	k2eAgInSc2d1	poslední
vydaného	vydaný	k2eAgInSc2d1	vydaný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
modern	modern	k1gNnSc1	modern
-	-	kIx~	-
povoleny	povolen	k2eAgFnPc1d1	povolena
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
karty	karta	k1gFnPc1	karta
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
edice	edice	k1gFnSc1	edice
po	po	k7c4	po
poslední	poslední	k2eAgInSc4d1	poslední
současnouNaopak	současnouNaopak	k1gInSc4	současnouNaopak
na	na	k7c6	na
limitovaných	limitovaný	k2eAgInPc6d1	limitovaný
turnajích	turnaj	k1gInPc6	turnaj
si	se	k3xPyFc3	se
hráči	hráč	k1gMnPc1	hráč
postaví	postavit	k5eAaPmIp3nP	postavit
balíček	balíček	k1gInSc4	balíček
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
turnaje	turnaj	k1gInSc2	turnaj
dostanou	dostat	k5eAaPmIp3nP	dostat
(	(	kIx(	(
<g/>
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
turnajích	turnaj	k1gInPc6	turnaj
má	mít	k5eAaImIp3nS	mít
balíček	balíček	k1gInSc1	balíček
nejméně	málo	k6eAd3	málo
40	[number]	k4	40
karet	kareta	k1gFnPc2	kareta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
limitovaných	limitovaný	k2eAgInPc2d1	limitovaný
turnajů	turnaj	k1gInPc2	turnaj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sealed	sealed	k1gMnSc1	sealed
deck	decka	k1gFnPc2	decka
–	–	k?	–
hráči	hráč	k1gMnPc1	hráč
si	se	k3xPyFc3	se
koupí	koupit	k5eAaPmIp3nP	koupit
stanovený	stanovený	k2eAgInSc4d1	stanovený
počet	počet	k1gInSc4	počet
karet	kareta	k1gFnPc2	kareta
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
6	[number]	k4	6
booster	boostrum	k1gNnPc2	boostrum
packů	pacek	k1gMnPc2	pacek
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
jeden	jeden	k4xCgInSc4	jeden
starter	startra	k1gFnPc2	startra
pack	pack	k6eAd1	pack
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc1	tři
booster	booster	k1gInSc1	booster
packy	packa	k1gFnSc2	packa
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
postaví	postavit	k5eAaPmIp3nS	postavit
balíček	balíček	k1gInSc1	balíček
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
karet	kareta	k1gFnPc2	kareta
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
sideboard	sideboard	k1gInSc4	sideboard
</s>
</p>
<p>
<s>
booster	booster	k1gInSc1	booster
draft	draft	k1gInSc1	draft
–	–	k?	–
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
koupí	koupit	k5eAaPmIp3nS	koupit
tři	tři	k4xCgInPc4	tři
booster	boostra	k1gFnPc2	boostra
packy	packy	k6eAd1	packy
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
8	[number]	k4	8
<g/>
)	)	kIx)	)
sedí	sedit	k5eAaImIp3nS	sedit
kolem	kolem	k7c2	kolem
stolu	stol	k1gInSc2	stol
a	a	k8xC	a
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
každý	každý	k3xTgInSc4	každý
otevře	otevřít	k5eAaPmIp3nS	otevřít
jeden	jeden	k4xCgMnSc1	jeden
svůj	svůj	k3xOyFgMnSc1	svůj
booster	booster	k1gMnSc1	booster
<g/>
,	,	kIx,	,
vybere	vybrat	k5eAaPmIp3nS	vybrat
si	se	k3xPyFc3	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
jednu	jeden	k4xCgFnSc4	jeden
kartu	karta	k1gFnSc4	karta
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
pošle	poslat	k5eAaPmIp3nS	poslat
sousedovi	soused	k1gMnSc3	soused
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
převezme	převzít	k5eAaPmIp3nS	převzít
booster	booster	k1gInSc4	booster
bez	bez	k7c2	bez
jedné	jeden	k4xCgFnSc2	jeden
karty	karta	k1gFnSc2	karta
od	od	k7c2	od
druhého	druhý	k4xOgMnSc2	druhý
souseda	soused	k1gMnSc2	soused
<g/>
,	,	kIx,	,
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
si	se	k3xPyFc3	se
také	také	k6eAd1	také
vybere	vybrat	k5eAaPmIp3nS	vybrat
kartu	karta	k1gFnSc4	karta
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
pošle	poslat	k5eAaPmIp3nS	poslat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
až	až	k9	až
do	do	k7c2	do
rozebrání	rozebrání	k1gNnSc2	rozebrání
všech	všecek	k3xTgFnPc2	všecek
karet	kareta	k1gFnPc2	kareta
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
boosteru	booster	k1gInSc6	booster
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
rozebere	rozebrat	k5eAaPmIp3nS	rozebrat
druhý	druhý	k4xOgInSc1	druhý
booster	booster	k1gInSc1	booster
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
posílá	posílat	k5eAaImIp3nS	posílat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
třetí	třetí	k4xOgMnSc1	třetí
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
posílá	posílat	k5eAaImIp3nS	posílat
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
postaví	postavit	k5eAaPmIp3nS	postavit
balíček	balíček	k1gInSc1	balíček
z	z	k7c2	z
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
takto	takto	k6eAd1	takto
získal	získat	k5eAaPmAgMnS	získat
(	(	kIx(	(
<g/>
karty	karta	k1gFnPc1	karta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
nezařadí	zařadit	k5eNaPmIp3nS	zařadit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
sideboard	sideboard	k1gInSc1	sideboard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
zemí	zem	k1gFnPc2	zem
dodaných	dodaný	k2eAgFnPc2d1	dodaná
pořadatelem	pořadatel	k1gMnSc7	pořadatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rochester	rochester	k1gInSc1	rochester
draft	draft	k1gInSc1	draft
–	–	k?	–
první	první	k4xOgMnSc1	první
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
dané	daný	k2eAgNnSc4d1	dané
znamení	znamení	k1gNnSc4	znamení
rozloží	rozložit	k5eAaPmIp3nS	rozložit
karty	karta	k1gFnPc4	karta
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
boosteru	booster	k1gInSc2	booster
lícem	líc	k1gInSc7	líc
nahoru	nahoru	k6eAd1	nahoru
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
,	,	kIx,	,
vybere	vybrat	k5eAaPmIp3nS	vybrat
si	se	k3xPyFc3	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jednu	jeden	k4xCgFnSc4	jeden
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
další	další	k2eAgFnSc4d1	další
kartu	karta	k1gFnSc4	karta
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
dál	daleko	k6eAd2	daleko
dokola	dokola	k6eAd1	dokola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozebrání	rozebrání	k1gNnSc6	rozebrání
jednoho	jeden	k4xCgInSc2	jeden
boosteru	booster	k1gInSc2	booster
rozloží	rozložit	k5eAaPmIp3nS	rozložit
svůj	svůj	k3xOyFgInSc4	svůj
booster	booster	k1gInSc4	booster
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
druhý	druhý	k4xOgMnSc1	druhý
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
opět	opět	k6eAd1	opět
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
rozeberou	rozebrat	k5eAaPmIp3nP	rozebrat
<g/>
.	.	kIx.	.
<g/>
Turnaj	turnaj	k1gInSc1	turnaj
probíhá	probíhat	k5eAaImIp3nS	probíhat
švýcarským	švýcarský	k2eAgInSc7d1	švýcarský
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
spolu	spolu	k6eAd1	spolu
hrají	hrát	k5eAaImIp3nP	hrát
náhodně	náhodně	k6eAd1	náhodně
vylosované	vylosovaný	k2eAgFnPc1d1	vylosovaná
dvojice	dvojice	k1gFnPc1	dvojice
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
dalším	další	k2eAgInSc6d1	další
pak	pak	k6eAd1	pak
dvojice	dvojice	k1gFnSc1	dvojice
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průběžném	průběžný	k2eAgNnSc6d1	průběžné
pořadí	pořadí	k1gNnSc6	pořadí
co	co	k9	co
nejblíže	blízce	k6eAd3	blízce
u	u	k7c2	u
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spolu	spolu	k6eAd1	spolu
ještě	ještě	k6eAd1	ještě
nehráli	hrát	k5eNaImAgMnP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
dvojice	dvojice	k1gFnSc1	dvojice
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
vítězné	vítězný	k2eAgFnPc4d1	vítězná
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
za	za	k7c2	za
remízu	remíz	k1gInSc2	remíz
1	[number]	k4	1
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kolo	kolo	k1gNnSc4	kolo
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někteří	některý	k3yIgMnPc1	některý
hráči	hráč	k1gMnPc1	hráč
nestihnou	stihnout	k5eNaPmIp3nP	stihnout
dohrát	dohrát	k5eAaPmF	dohrát
hru	hra	k1gFnSc4	hra
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
limitu	limit	k1gInSc6	limit
<g/>
,	,	kIx,	,
odehrají	odehrát	k5eAaPmIp3nP	odehrát
ještě	ještě	k9	ještě
5	[number]	k4	5
kol	kolo	k1gNnPc2	kolo
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
pak	pak	k6eAd1	pak
skončí	skončit	k5eAaPmIp3nS	skončit
remízou	remíza	k1gFnSc7	remíza
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
kol	kolo	k1gNnPc2	kolo
(	(	kIx(	(
<g/>
3	[number]	k4	3
až	až	k9	až
7	[number]	k4	7
<g/>
)	)	kIx)	)
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
účastníků	účastník	k1gMnPc2	účastník
a	a	k8xC	a
důležitosti	důležitost	k1gFnSc2	důležitost
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
postoupí	postoupit	k5eAaPmIp3nP	postoupit
8	[number]	k4	8
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
jich	on	k3xPp3gMnPc2	on
být	být	k5eAaImF	být
i	i	k8xC	i
16	[number]	k4	16
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
účastníků	účastník	k1gMnPc2	účastník
<g/>
)	)	kIx)	)
do	do	k7c2	do
finálové	finálový	k2eAgFnSc2d1	finálová
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
vyřazovacím	vyřazovací	k2eAgInSc7d1	vyřazovací
systémem	systém	k1gInSc7	systém
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
vítězné	vítězný	k2eAgFnPc4d1	vítězná
hry	hra	k1gFnPc4	hra
bez	bez	k7c2	bez
časového	časový	k2eAgNnSc2d1	časové
omezení	omezení	k1gNnSc2	omezení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
DCI	DCI	kA	DCI
===	===	k?	===
</s>
</p>
<p>
<s>
Celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnPc4	hodnocení
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
DCI	DCI	kA	DCI
(	(	kIx(	(
<g/>
Duelists	Duelists	k1gInSc1	Duelists
<g/>
'	'	kIx"	'
Convocation	Convocation	k1gInSc1	Convocation
International	International	k1gFnSc2	International
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
pravidla	pravidlo	k1gNnPc4	pravidlo
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
certifikaci	certifikace	k1gFnSc3	certifikace
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
sankcionovaných	sankcionovaný	k2eAgInPc2d1	sankcionovaný
DCI	DCI	kA	DCI
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
zaregistrováni	zaregistrovat	k5eAaPmNgMnP	zaregistrovat
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
do	do	k7c2	do
databáze	databáze	k1gFnSc2	databáze
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
kdykoli	kdykoli	k6eAd1	kdykoli
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
DCI	DCI	kA	DCI
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
chová	chovat	k5eAaImIp3nS	chovat
nesportovně	sportovně	k6eNd1	sportovně
nebo	nebo	k8xC	nebo
podvádí	podvádět	k5eAaImIp3nS	podvádět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velké	velký	k2eAgInPc1d1	velký
turnaje	turnaj	k1gInPc1	turnaj
===	===	k?	===
</s>
</p>
<p>
<s>
Každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
se	se	k3xPyFc4	se
někde	někde	k6eAd1	někde
na	na	k7c6	na
světě	svět	k1gInSc6	svět
pořádá	pořádat	k5eAaImIp3nS	pořádat
turnaj	turnaj	k1gInSc4	turnaj
typu	typ	k1gInSc2	typ
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
kdokoli	kdokoli	k3yInSc1	kdokoli
<g/>
;	;	kIx,	;
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
však	však	k9	však
hrají	hrát	k5eAaImIp3nP	hrát
menší	malý	k2eAgInPc1d2	menší
kvalifikační	kvalifikační	k2eAgInPc1d1	kvalifikační
turnaje	turnaj	k1gInPc1	turnaj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc7	jejichž
vítěz	vítěz	k1gMnSc1	vítěz
získá	získat	k5eAaPmIp3nS	získat
na	na	k7c4	na
GP	GP	kA	GP
automatickou	automatický	k2eAgFnSc4d1	automatická
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
třech	tři	k4xCgNnPc6	tři
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
GP	GP	kA	GP
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
stanovený	stanovený	k2eAgInSc4d1	stanovený
formát	formát	k1gInSc4	formát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
booster	booster	k1gMnSc1	booster
draft	draft	k1gInSc1	draft
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
místech	místo	k1gNnPc6	místo
turnaje	turnaj	k1gInSc2	turnaj
GP	GP	kA	GP
je	být	k5eAaImIp3nS	být
finančně	finančně	k6eAd1	finančně
odměňováno	odměňován	k2eAgNnSc1d1	odměňováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
pořádají	pořádat	k5eAaImIp3nP	pořádat
regionální	regionální	k2eAgInSc4d1	regionální
i	i	k8xC	i
celonárodní	celonárodní	k2eAgNnSc4d1	celonárodní
mistrovství	mistrovství	k1gNnSc4	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
národní	národní	k2eAgNnSc4d1	národní
mistrovství	mistrovství	k1gNnSc4	mistrovství
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
skončit	skončit	k5eAaPmF	skončit
na	na	k7c6	na
předním	přední	k2eAgNnSc6d1	přední
místě	místo	k1gNnSc6	místo
kvalifikačního	kvalifikační	k2eAgInSc2d1	kvalifikační
turnaje	turnaj	k1gInSc2	turnaj
nebo	nebo	k8xC	nebo
na	na	k7c6	na
předním	přední	k2eAgNnSc6d1	přední
místě	místo	k1gNnSc6	místo
DCI	DCI	kA	DCI
žebříčku	žebříček	k1gInSc2	žebříček
hráčů	hráč	k1gMnPc2	hráč
příslušného	příslušný	k2eAgInSc2d1	příslušný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
národního	národní	k2eAgNnSc2d1	národní
mistrovství	mistrovství	k1gNnSc2	mistrovství
pak	pak	k6eAd1	pak
postoupí	postoupit	k5eAaPmIp3nP	postoupit
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
trvá	trvat	k5eAaImIp3nS	trvat
pět	pět	k4xCc1	pět
dní	den	k1gInPc2	den
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
několik	několik	k4yIc1	několik
typů	typ	k1gInPc2	typ
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mistrovství	mistrovství	k1gNnSc3	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
Jakub	Jakub	k1gMnSc1	Jakub
Šlemr	Šlemr	k1gMnSc1	Šlemr
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turnajem	turnaj	k1gInSc7	turnaj
s	s	k7c7	s
největšími	veliký	k2eAgFnPc7d3	veliký
výhrami	výhra	k1gFnPc7	výhra
je	být	k5eAaImIp3nS	být
Pro	pro	k7c4	pro
Tour	Tour	k1gInSc4	Tour
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
několikrát	několikrát	k6eAd1	několikrát
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
pořádán	pořádat	k5eAaImNgInS	pořádat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
se	se	k3xPyFc4	se
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
buď	buď	k8xC	buď
předním	přední	k2eAgNnSc7d1	přední
umístěním	umístění	k1gNnSc7	umístění
na	na	k7c6	na
některém	některý	k3yIgInSc6	některý
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
turnaji	turnaj	k1gInSc6	turnaj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ziskem	zisk	k1gInSc7	zisk
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
počtu	počet	k1gInSc2	počet
DCI	DCI	kA	DCI
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ziskem	zisk	k1gInSc7	zisk
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
počtu	počet	k1gInSc2	počet
Pro	pro	k7c4	pro
Points	Points	k1gInSc4	Points
–	–	k?	–
body	bod	k1gInPc4	bod
za	za	k7c4	za
umístění	umístění	k1gNnSc4	umístění
na	na	k7c4	na
Pro	pro	k7c4	pro
Tourech	Tour	k1gInPc6	Tour
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Prixech	Prix	k1gInPc6	Prix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
FNM	FNM	kA	FNM
==	==	k?	==
</s>
</p>
<p>
<s>
FNM	FNM	kA	FNM
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
Friday	Fridaa	k1gFnPc1	Fridaa
Night	Nighta	k1gFnPc2	Nighta
Magic	Magice	k1gInPc2	Magice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
malého	malý	k2eAgInSc2d1	malý
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
FNM	FNM	kA	FNM
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
větším	veliký	k2eAgNnSc6d2	veliký
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chcete	chtít	k5eAaImIp2nP	chtít
hrát	hrát	k5eAaImF	hrát
takové	takový	k3xDgInPc4	takový
turnaje	turnaj	k1gInPc4	turnaj
musíte	muset	k5eAaImIp2nP	muset
mít	mít	k5eAaImF	mít
DCI	DCI	kA	DCI
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
o	o	k7c4	o
DCI	DCI	kA	DCI
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
bonusové	bonusový	k2eAgFnPc4d1	bonusová
<g/>
,	,	kIx,	,
foilové	foilový	k2eAgFnPc4d1	foilová
karty	karta	k1gFnPc4	karta
a	a	k8xC	a
o	o	k7c4	o
boostery	booster	k1gMnPc4	booster
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tematická	tematický	k2eAgFnSc1d1	tematická
literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Hrou	hra	k1gFnSc7	hra
Magic	Magice	k1gFnPc2	Magice
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
řada	řada	k1gFnSc1	řada
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
úspěšně	úspěšně	k6eAd1	úspěšně
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c6	o
převedení	převedení	k1gNnSc6	převedení
hry	hra	k1gFnSc2	hra
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejzdařilejší	zdařilý	k2eAgNnPc4d3	nejzdařilejší
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nP	patřit
fantasy	fantas	k1gInPc1	fantas
román	román	k1gInSc1	román
Williama	William	k1gMnSc4	William
R.	R.	kA	R.
Forstchena	Forstchen	k1gMnSc4	Forstchen
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
Magic	Magic	k1gMnSc1	Magic
the	the	k?	the
Gathering	Gathering	k1gInSc1	Gathering
<g/>
:	:	kIx,	:
Aréna	aréna	k1gFnSc1	aréna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmy	film	k1gInPc4	film
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
MTG	MTG	kA	MTG
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
český	český	k2eAgInSc1d1	český
nezávislý	závislý	k2eNgInSc1d1	nezávislý
film	film	k1gInSc1	film
TAP	TAP	kA	TAP
<g/>
:	:	kIx,	:
Maxova	Maxův	k2eAgFnSc1d1	Maxova
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
s	s	k7c7	s
titulky	titulek	k1gInPc7	titulek
v	v	k7c6	v
osmi	osm	k4xCc6	osm
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
promítám	promítat	k5eAaImIp1nS	promítat
na	na	k7c6	na
třiceti	třicet	k4xCc6	třicet
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
probíhá	probíhat	k5eAaImIp3nS	probíhat
natáčení	natáčení	k1gNnSc4	natáčení
2	[number]	k4	2
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
TAP	TAP	kA	TAP
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
čest	čest	k1gFnSc4	čest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
Arena	Arena	k1gFnSc1	Arena
==	==	k?	==
</s>
</p>
<p>
<s>
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
Arena	Arena	k1gFnSc1	Arena
je	být	k5eAaImIp3nS	být
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
společností	společnost	k1gFnSc7	společnost
Wizards	Wizardsa	k1gFnPc2	Wizardsa
of	of	k?	of
the	the	k?	the
Coast	Coast	k1gInSc1	Coast
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
karty	karta	k1gFnPc4	karta
z	z	k7c2	z
edic	edice	k1gFnPc2	edice
Ixalan	Ixalana	k1gFnPc2	Ixalana
až	až	k9	až
Ravnica	Ravnic	k2eAgFnSc1d1	Ravnica
Allegiance	Allegiance	k1gFnSc1	Allegiance
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hrát	hrát	k5eAaImF	hrát
Magic	Magic	k1gMnSc1	Magic
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
papírové	papírový	k2eAgFnSc2d1	papírová
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
beta	beta	k1gNnSc7	beta
verze	verze	k1gFnSc2	verze
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
spuštění	spuštění	k1gNnSc4	spuštění
plné	plný	k2eAgFnSc2d1	plná
verze	verze	k1gFnSc2	verze
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Již	jenž	k3xRgFnSc4	jenž
brzy	brzy	k6eAd1	brzy
vyjde	vyjít	k5eAaPmIp3nS	vyjít
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
War	War	k1gFnPc3	War
of	of	k?	of
the	the	k?	the
Spark	Spark	k1gInSc1	Spark
===	===	k?	===
</s>
</p>
<p>
<s>
duben	duben	k1gInSc1	duben
2019	[number]	k4	2019
</s>
</p>
<p>
<s>
===	===	k?	===
Core	Cor	k1gInPc4	Cor
Set	set	k1gInSc1	set
2020	[number]	k4	2020
===	===	k?	===
</s>
</p>
<p>
<s>
červenec	červenec	k1gInSc1	červenec
2019	[number]	k4	2019
</s>
</p>
<p>
<s>
===	===	k?	===
Commander	Commandero	k1gNnPc2	Commandero
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
Edition	Edition	k1gInSc1	Edition
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
srpen	srpen	k1gInSc4	srpen
2019	[number]	k4	2019
<g/>
Přehled	přehled	k1gInSc4	přehled
všech	všecek	k3xTgFnPc2	všecek
edic	edice	k1gFnPc2	edice
a	a	k8xC	a
bloků	blok	k1gInPc2	blok
naleznete	nalézt	k5eAaBmIp2nP	nalézt
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
https://magic.wizards.com/en/products/card-set-archive	[url]	k5eAaPmIp3nS	https://magic.wizards.com/en/products/card-set-archive
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Shards	Shards	k6eAd1	Shards
of	of	k?	of
Alara	Alara	k1gMnSc1	Alara
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Magic	Magice	k1gFnPc2	Magice
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc4	Gathering
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Magic	Magice	k1gFnPc2	Magice
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc4	Gathering
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Magic	Magice	k1gFnPc2	Magice
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc4	Gathering
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CMUS	CMUS	kA	CMUS
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
informační	informační	k2eAgInSc4d1	informační
server	server	k1gInSc4	server
o	o	k7c6	o
karetní	karetní	k2eAgFnSc6d1	karetní
hře	hra	k1gFnSc6	hra
Magic	Magic	k1gMnSc1	Magic
the	the	k?	the
Gathering	Gathering	k1gInSc1	Gathering
přinášející	přinášející	k2eAgInPc1d1	přinášející
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
reporty	report	k1gInPc1	report
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
turnaje	turnaj	k1gInPc1	turnaj
<g/>
,	,	kIx,	,
rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
,	,	kIx,	,
diskuzní	diskuzní	k2eAgNnSc1d1	diskuzní
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
výměnu	výměna	k1gFnSc4	výměna
a	a	k8xC	a
bazar	bazar	k1gInSc4	bazar
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TCG	TCG	kA	TCG
player	player	k1gMnSc1	player
–	–	k?	–
dříve	dříve	k6eAd2	dříve
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
BrainBurst	BrainBurst	k1gInSc1	BrainBurst
<g/>
,	,	kIx,	,
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
web	web	k1gInSc1	web
o	o	k7c6	o
MTG	MTG	kA	MTG
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zejména	zejména	k9	zejména
seznamy	seznam	k1gInPc4	seznam
a	a	k8xC	a
rozbory	rozbor	k1gInPc4	rozbor
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
balíků	balík	k1gInPc2	balík
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Star	Star	kA	Star
City	city	k1gNnSc1	city
Games	Games	k1gInSc1	Games
–	–	k?	–
další	další	k2eAgInSc1d1	další
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
web	web	k1gInSc1	web
o	o	k7c6	o
MTG	MTG	kA	MTG
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MTG	MTG	kA	MTG
News	News	k1gInSc1	News
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
různé	různý	k2eAgFnPc4d1	různá
aktuality	aktualita	k1gFnPc4	aktualita
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
MTG	MTG	kA	MTG
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TAP	TAP	kA	TAP
<g/>
:	:	kIx,	:
Maxova	Maxův	k2eAgFnSc1d1	Maxova
hra	hra	k1gFnSc1	hra
–	–	k?	–
stránka	stránka	k1gFnSc1	stránka
českého	český	k2eAgInSc2d1	český
filmu	film	k1gInSc2	film
TAP	TAP	kA	TAP
<g/>
:	:	kIx,	:
Maxova	Maxův	k2eAgFnSc1d1	Maxova
hra	hra	k1gFnSc1	hra
o	o	k7c4	o
MTG	MTG	kA	MTG
</s>
</p>
