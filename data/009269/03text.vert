<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Indie	Indie	k1gFnSc2	Indie
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
vodorovnýmy	vodorovným	k1gInPc7	vodorovným
pásy	pás	k1gInPc1	pás
šafránové	šafránový	k2eAgFnPc4d1	šafránová
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc4d1	bílá
a	a	k8xC	a
zelené	zelený	k2eAgFnPc4d1	zelená
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
pásu	pás	k1gInSc2	pás
je	být	k5eAaImIp3nS	být
modré	modrý	k2eAgNnSc1d1	modré
kolo	kolo	k1gNnSc1	kolo
-	-	kIx~	-
čakra	čakra	k1gFnSc1	čakra
(	(	kIx(	(
<g/>
slunce	slunce	k1gNnSc1	slunce
s	s	k7c7	s
loučemi	louč	k1gFnPc7	louč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
nigerské	nigerský	k2eAgFnSc3d1	Nigerská
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnSc6d1	historická
souvislosti	souvislost	k1gFnSc6	souvislost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založili	založit	k5eAaPmAgMnP	založit
Britové	Brit	k1gMnPc1	Brit
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
kolonii	kolonie	k1gFnSc4	kolonie
a	a	k8xC	a
spravovali	spravovat	k5eAaImAgMnP	spravovat
ji	on	k3xPp3gFnSc4	on
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Indie	Indie	k1gFnSc1	Indie
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
právě	právě	k6eAd1	právě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vedl	vést	k5eAaImAgInS	vést
Mahátma	Mahátma	k1gNnSc4	Mahátma
Gándhí	Gándhí	k1gFnSc2	Gándhí
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
britská	britský	k2eAgFnSc1d1	britská
kolonie	kolonie	k1gFnSc1	kolonie
se	se	k3xPyFc4	se
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
nezávislé	závislý	k2eNgInPc4d1	nezávislý
státy	stát	k1gInPc4	stát
-	-	kIx~	-
převážně	převážně	k6eAd1	převážně
hinduistickou	hinduistický	k2eAgFnSc4d1	hinduistická
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
muslimský	muslimský	k2eAgInSc4d1	muslimský
Pákistán	Pákistán	k1gInSc4	Pákistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interpretace	interpretace	k1gFnSc2	interpretace
==	==	k?	==
</s>
</p>
<p>
<s>
Šafránově	šafránově	k6eAd1	šafránově
žlutý	žlutý	k2eAgInSc1d1	žlutý
pruh	pruh	k1gInSc1	pruh
vlajky	vlajka	k1gFnSc2	vlajka
představuje	představovat	k5eAaImIp3nS	představovat
hinduistickou	hinduistický	k2eAgFnSc4d1	hinduistická
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc4d1	zelený
pruh	pruh	k1gInSc4	pruh
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
muslimy	muslim	k1gMnPc4	muslim
a	a	k8xC	a
bílá	bílé	k1gNnPc4	bílé
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
pruhu	pruh	k1gInSc6	pruh
je	být	k5eAaImIp3nS	být
barvou	barvý	k2eAgFnSc4d1	barvá
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
interpretace	interpretace	k1gFnSc1	interpretace
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šafránová	šafránový	k2eAgFnSc1d1	šafránová
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
obětování	obětování	k1gNnSc2	obětování
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
míru	mír	k1gInSc2	mír
a	a	k8xC	a
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
zelená	zelenat	k5eAaImIp3nS	zelenat
barvou	barva	k1gFnSc7	barva
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
rytířskosti	rytířskost	k1gFnSc2	rytířskost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolo	kolo	k1gNnSc1	kolo
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
starobylým	starobylý	k2eAgInSc7d1	starobylý
znakem	znak	k1gInSc7	znak
brahmánismu	brahmánismus	k1gInSc2	brahmánismus
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Dharmačakra	Dharmačakra	k1gMnSc1	Dharmačakra
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Ašókačakra	Ašókačakra	k1gFnSc1	Ašókačakra
-	-	kIx~	-
(	(	kIx(	(
<g/>
Kolo	kolo	k1gNnSc1	kolo
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
atribútů	atribúta	k1gMnPc2	atribúta
boha	bůh	k1gMnSc4	bůh
Višnu	Višna	k1gMnSc4	Višna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dvacet	dvacet	k4xCc4	dvacet
čtyři	čtyři	k4xCgMnPc4	čtyři
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
polí	pole	k1gFnPc2	pole
představuje	představovat	k5eAaImIp3nS	představovat
počet	počet	k1gInSc1	počet
hodin	hodina	k1gFnPc2	hodina
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
nekončící	končící	k2eNgInSc4d1	nekončící
chod	chod	k1gInSc4	chod
života	život	k1gInSc2	život
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
kola	kolo	k1gNnSc2	kolo
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Indie	Indie	k1gFnSc2	Indie
</s>
</p>
<p>
<s>
Indická	indický	k2eAgFnSc1d1	indická
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Indie	Indie	k1gFnSc2	Indie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Indická	indický	k2eAgFnSc1d1	indická
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
