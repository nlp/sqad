<s>
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Naděžda	Naděžda	k1gFnSc1	Naděžda
Balabánová	Balabánová	k1gFnSc1	Balabánová
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1939	[number]	k4	1939
Nová	nový	k2eAgFnSc1d1	nová
Paka	Paka	k1gFnSc1	Paka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
Jana	Jana	k1gFnSc1	Jana
Fabiánová	Fabiánová	k1gFnSc1	Fabiánová
je	být	k5eAaImIp3nS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
střední	střední	k2eAgFnSc4d1	střední
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
jako	jako	k8xS	jako
diplomovaná	diplomovaný	k2eAgFnSc1d1	diplomovaná
sestra	sestra	k1gFnSc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
debutovala	debutovat	k5eAaBmAgFnS	debutovat
jako	jako	k9	jako
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
Pardubickém	pardubický	k2eAgNnSc6d1	pardubické
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Laterně	laterna	k1gFnSc6	laterna
magice	magika	k1gFnSc6	magika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
ji	on	k3xPp3gFnSc4	on
režisér	režisér	k1gMnSc1	režisér
Ján	Ján	k1gMnSc1	Ján
Roháč	roháč	k1gMnSc1	roháč
angažoval	angažovat	k5eAaBmAgMnS	angažovat
do	do	k7c2	do
filmu	film	k1gInSc2	film
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
a	a	k8xC	a
do	do	k7c2	do
pražského	pražský	k2eAgNnSc2d1	Pražské
divadla	divadlo	k1gNnSc2	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
také	také	k9	také
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Bobkem	Bobek	k1gMnSc7	Bobek
<g/>
,	,	kIx,	,
Jitkou	Jitka	k1gFnSc7	Jitka
Molavcovou	Molavcův	k2eAgFnSc7d1	Molavcův
<g/>
,	,	kIx,	,
Miluší	Miluše	k1gFnSc7	Miluše
Voborníkovou	Voborníková	k1gFnSc7	Voborníková
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Grossmanem	Grossman	k1gMnSc7	Grossman
a	a	k8xC	a
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Burianovou	Burianová	k1gFnSc7	Burianová
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
bicí	bicí	k2eAgFnSc7d1	bicí
soupravou	souprava	k1gFnSc7	souprava
v	v	k7c6	v
doprovodné	doprovodný	k2eAgFnSc6d1	doprovodná
hudební	hudební	k2eAgFnSc6d1	hudební
skupině	skupina	k1gFnSc6	skupina
tehdy	tehdy	k6eAd1	tehdy
sedával	sedávat	k5eAaImAgMnS	sedávat
bubeník	bubeník	k1gMnSc1	bubeník
František	František	k1gMnSc1	František
Ringo	Ringo	k1gMnSc1	Ringo
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
hitem	hit	k1gInSc7	hit
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
píseň	píseň	k1gFnSc1	píseň
Drahý	drahý	k1gMnSc1	drahý
můj	můj	k1gMnSc1	můj
v	v	k7c6	v
duetu	duet	k1gInSc6	duet
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Grossmanem	Grossman	k1gMnSc7	Grossman
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Grossmanova	Grossmanův	k2eAgFnSc1d1	Grossmanova
nostalgicky	nostalgicky	k6eAd1	nostalgicky
poetická	poetický	k2eAgFnSc1d1	poetická
píseň	píseň	k1gFnSc1	píseň
Závidím	závidět	k5eAaImIp1nS	závidět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
započala	započnout	k5eAaPmAgFnS	započnout
její	její	k3xOp3gFnSc1	její
"	"	kIx"	"
<g/>
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
<g/>
"	"	kIx"	"
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Brabcem	Brabec	k1gMnSc7	Brabec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
skupinou	skupina	k1gFnSc7	skupina
Country	country	k2eAgInSc4d1	country
Beat	beat	k1gInSc4	beat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
počátkem	počátkem	k7c2	počátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vyhoupla	vyhoupnout	k5eAaPmAgFnS	vyhoupnout
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
čtenářské	čtenářský	k2eAgFnSc6d1	čtenářská
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrvala	setrvat	k5eAaPmAgFnS	setrvat
plných	plný	k2eAgNnPc2d1	plné
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Country	country	k2eAgInSc7d1	country
Beatem	beat	k1gInSc7	beat
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
několik	několik	k4yIc4	několik
důležitých	důležitý	k2eAgInPc2d1	důležitý
zájezdů	zájezd	k1gInPc2	zájezd
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
též	též	k9	též
do	do	k7c2	do
amerického	americký	k2eAgNnSc2d1	americké
města	město	k1gNnSc2	město
Nashville	Nashville	k1gFnSc2	Nashville
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
<g/>
,	,	kIx,	,
bezesporu	bezesporu	k9	bezesporu
nejvýznamnějšího	významný	k2eAgNnSc2d3	nejvýznamnější
centra	centrum	k1gNnSc2	centrum
country	country	k2eAgInPc2d1	country
music	musice	k1gInPc2	musice
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
Václava	Václav	k1gMnSc2	Václav
Hybše	Hybš	k1gMnSc2	Hybš
<g/>
,	,	kIx,	,
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Hudebním	hudební	k2eAgNnSc6d1	hudební
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nakrátko	nakrátko	k6eAd1	nakrátko
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Jiřímu	Jiří	k1gMnSc3	Jiří
Brabcovi	Brabec	k1gMnSc3	Brabec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
odmlčela	odmlčet	k5eAaPmAgFnS	odmlčet
se	se	k3xPyFc4	se
na	na	k7c4	na
čas	čas	k1gInSc4	čas
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velkých	velký	k2eAgInPc6d1	velký
problémech	problém	k1gInPc6	problém
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
krachu	krach	k1gInSc2	krach
v	v	k7c6	v
podnikání	podnikání	k1gNnSc6	podnikání
a	a	k8xC	a
nezdarech	nezdar	k1gInPc6	nezdar
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
po	po	k7c6	po
létech	léto	k1gNnPc6	léto
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
vystupování	vystupování	k1gNnSc3	vystupování
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Bokomara	Bokomar	k1gMnSc2	Bokomar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
moderuje	moderovat	k5eAaBmIp3nS	moderovat
pořad	pořad	k1gInSc4	pořad
Písničky	písnička	k1gFnSc2	písnička
pro	pro	k7c4	pro
Vysočinu	vysočina	k1gFnSc4	vysočina
na	na	k7c6	na
Českém	český	k2eAgInSc6d1	český
rozhlase	rozhlas	k1gInSc6	rozhlas
Region	region	k1gInSc1	region
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Želivu	Želiv	k1gInSc3	Želiv
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc4	sedm
žen	žena	k1gFnPc2	žena
Alfonse	Alfons	k1gMnSc4	Alfons
Karáska	Karásek	k1gMnSc4	Karásek
-	-	kIx~	-
1967	[number]	k4	1967
Radůz	Radůza	k1gFnPc2	Radůza
a	a	k8xC	a
Mahulena	Mahulena	k1gFnSc1	Mahulena
-	-	kIx~	-
sestra	sestra	k1gFnSc1	sestra
Mahuleny	Mahulena	k1gFnSc2	Mahulena
-	-	kIx~	-
1970	[number]	k4	1970
Písničky	písnička	k1gFnSc2	písnička
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
Písničky	písnička	k1gFnSc2	písnička
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
Vzlety	vzlet	k1gInPc1	vzlet
a	a	k8xC	a
pády	pád	k1gInPc1	pád
-	-	kIx~	-
2000	[number]	k4	2000
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
-	-	kIx~	-
1964	[number]	k4	1964
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
dívčí	dívčí	k2eAgFnSc6d1	dívčí
škole	škola	k1gFnSc6	škola
-	-	kIx~	-
1965	[number]	k4	1965
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
-	-	kIx~	-
Viktoria	Viktoria	k1gFnSc1	Viktoria
Freie	Freie	k1gFnSc1	Freie
-	-	kIx~	-
1966	[number]	k4	1966
Pension	pension	k1gInSc1	pension
pro	pro	k7c4	pro
svobodné	svobodný	k2eAgMnPc4d1	svobodný
pány	pan	k1gMnPc4	pan
-	-	kIx~	-
dáma	dáma	k1gFnSc1	dáma
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
1967	[number]	k4	1967
Ach	ach	k0	ach
ta	ten	k3xDgFnSc1	ten
vojna	vojna	k1gFnSc1	vojna
-	-	kIx~	-
1968	[number]	k4	1968
Rakev	rakev	k1gFnSc1	rakev
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
viděti	vidět	k5eAaImF	vidět
-	-	kIx~	-
1968	[number]	k4	1968
Čierna	Čierna	k1gFnSc1	Čierna
minúta	minúta	k1gFnSc1	minúta
-	-	kIx~	-
1969	[number]	k4	1969
Dospěláci	Dospěláci	k?	Dospěláci
můžou	můžou	k?	můžou
všechno	všechen	k3xTgNnSc4	všechen
-	-	kIx~	-
1969	[number]	k4	1969
Skřivánci	Skřivánek	k1gMnPc1	Skřivánek
na	na	k7c6	na
niti	nit	k1gFnSc6	nit
-	-	kIx~	-
Lenka	Lenka	k1gFnSc1	Lenka
-	-	kIx~	-
1969	[number]	k4	1969
Nevěsta	nevěsta	k1gFnSc1	nevěsta
-	-	kIx~	-
1970	[number]	k4	1970
Lucie	Lucie	k1gFnPc1	Lucie
a	a	k8xC	a
zázraky	zázrak	k1gInPc1	zázrak
-	-	kIx~	-
1970	[number]	k4	1970
Radúz	Radúz	k1gMnSc1	Radúz
a	a	k8xC	a
Mahulena	Mahulena	k1gFnSc1	Mahulena
-	-	kIx~	-
1970	[number]	k4	1970
Romance	romance	k1gFnSc2	romance
za	za	k7c4	za
korunu	koruna	k1gFnSc4	koruna
-	-	kIx~	-
1975	[number]	k4	1975
Na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
lesa	les	k1gInSc2	les
-	-	kIx~	-
1976	[number]	k4	1976
Hodinářova	hodinářův	k2eAgFnSc1d1	Hodinářova
svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
korálovým	korálový	k2eAgNnSc7d1	korálové
mořem	moře	k1gNnSc7	moře
-	-	kIx~	-
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc4	zpěv
<g/>
)	)	kIx)	)
Fandy	Fanda	k1gMnSc2	Fanda
<g/>
,	,	kIx,	,
ó	ó	k0	ó
Fandy	Fanda	k1gMnSc2	Fanda
-	-	kIx~	-
1983	[number]	k4	1983
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
Nemožná	nemožná	k1gFnSc1	nemožná
-	-	kIx~	-
1987	[number]	k4	1987
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
PERPLEXU	PERPLEXU	kA	PERPLEXU
16	[number]	k4	16
milimetrů	milimetr	k1gInPc2	milimetr
-	-	kIx~	-
1998	[number]	k4	1998
anketa	anketa	k1gFnSc1	anketa
Zlatý	zlatý	k1gInSc4	zlatý
slavík	slavík	k1gInSc1	slavík
1972	[number]	k4	1972
anketa	anketa	k1gFnSc1	anketa
Zlatý	zlatý	k1gInSc4	zlatý
slavík	slavík	k1gInSc1	slavík
1973	[number]	k4	1973
anketa	anketa	k1gFnSc1	anketa
Zlatý	zlatý	k1gInSc4	zlatý
slavík	slavík	k1gInSc1	slavík
1974	[number]	k4	1974
anketa	anketa	k1gFnSc1	anketa
<g />
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
1975	[number]	k4	1975
anketa	anketa	k1gFnSc1	anketa
Zlatý	zlatý	k1gInSc4	zlatý
slavík	slavík	k1gInSc1	slavík
1976	[number]	k4	1976
1974	[number]	k4	1974
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
s	s	k7c7	s
Naďou	Naďa	k1gFnSc7	Naďa
Urbánkovou	Urbánkův	k2eAgFnSc7d1	Urbánkova
-	-	kIx~	-
LP	LP	kA	LP
1979	[number]	k4	1979
Naďa	Naďa	k1gFnSc1	Naďa
-	-	kIx~	-
LP	LP	kA	LP
1995	[number]	k4	1995
20	[number]	k4	20
<g/>
×	×	k?	×
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
-	-	kIx~	-
Sony	Sony	kA	Sony
Music	Musice	k1gInPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
,	,	kIx,	,
CD	CD	kA	CD
(	(	kIx(	(
<g/>
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
Country	country	k2eAgInSc1d1	country
Beat	beat	k1gInSc1	beat
Jiřího	Jiří	k1gMnSc2	Jiří
Brabce	Brabec	k1gMnSc2	Brabec
-	-	kIx~	-
/	/	kIx~	/
<g/>
výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
<g />
.	.	kIx.	.
</s>
<s>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Drahý	drahý	k1gMnSc1	drahý
můj	můj	k1gMnSc1	můj
-	-	kIx~	-
Venkow	Venkow	k1gMnSc1	Venkow
<g/>
,	,	kIx,	,
CD	CD	kA	CD
2001	[number]	k4	2001
Zlaté	zlatý	k2eAgInPc1d1	zlatý
hity	hit	k1gInPc1	hit
-	-	kIx~	-
Popron	Popron	k1gMnSc1	Popron
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
,	,	kIx,	,
CD	CD	kA	CD
2003	[number]	k4	2003
Gold	Gold	k1gInSc4	Gold
-	-	kIx~	-
Popron	Popron	k1gMnSc1	Popron
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
CD	CD	kA	CD
2003	[number]	k4	2003
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
-	-	kIx~	-
Areca	Areca	k1gFnSc1	Areca
Multimedia	multimedium	k1gNnSc2	multimedium
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
,	,	kIx,	,
CD	CD	kA	CD
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Edice	edice	k1gFnSc1	edice
-	-	kIx~	-
Portréty	portrét	k1gInPc1	portrét
českých	český	k2eAgFnPc2d1	Česká
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
Závidím	závidět	k5eAaImIp1nS	závidět
-	-	kIx~	-
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jiřího	Jiří	k1gMnSc4	Jiří
Grossmanna	Grossmann	k1gMnSc4	Grossmann
<g/>
,	,	kIx,	,
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
2007	[number]	k4	2007
Pouť	pouť	k1gFnSc1	pouť
na	na	k7c4	na
Želiv	Želiv	k1gInSc4	Želiv
-	-	kIx~	-
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Bokomara	Bokomara	k1gFnSc1	Bokomara
<g/>
,	,	kIx,	,
ARECA	ARECA	kA	ARECA
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
AM	AM	kA	AM
80450	[number]	k4	80450
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Želiv	Želiv	k1gInSc1	Želiv
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Pop	pop	k1gInSc1	pop
galerie	galerie	k1gFnSc2	galerie
-	-	kIx~	-
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
1988	[number]	k4	1988
Vůně	vůně	k1gFnSc1	vůně
jehličí	jehličí	k1gNnSc2	jehličí
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
LP	LP	kA	LP
-	-	kIx~	-
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vůně	vůně	k1gFnSc1	vůně
jehličí	jehličí	k1gNnSc2	jehličí
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Tiše	tiš	k1gFnPc4	tiš
padá	padat	k5eAaImIp3nS	padat
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInPc1d1	vánoční
zvonky	zvonek	k1gInPc1	zvonek
(	(	kIx(	(
<g/>
Jingle	Jingle	k1gFnSc1	Jingle
Bell	bell	k1gInSc1	bell
Rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Ráda	rád	k2eAgFnSc1d1	ráda
bych	by	kYmCp1nS	by
k	k	k7c3	k
Betlému	betlém	k1gInSc2	betlém
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Veselé	Veselé	k2eAgFnPc1d1	Veselé
vánoce	vánoce	k1gFnPc1	vánoce
-	-	kIx~	-
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Bartoň	Bartoň	k1gMnSc1	Bartoň
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Preiss	Preiss	k1gInSc1	Preiss
<g/>
,	,	kIx,	,
Iveta	Iveta	k1gFnSc1	Iveta
Bartošová	Bartošová	k1gFnSc1	Bartošová
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Munzar	Munzar	k1gMnSc1	Munzar
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
Hlaváčová	Hlaváčová	k1gFnSc1	Hlaváčová
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
Hybš	Hybš	k1gMnSc1	Hybš
hraje	hrát	k5eAaImIp3nS	hrát
písničky	písnička	k1gFnPc4	písnička
Karla	Karel	k1gMnSc2	Karel
Hašlera	Hašler	k1gMnSc2	Hašler
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
To	to	k9	to
byl	být	k5eAaImAgInS	být
váš	váš	k3xOp2gInSc1	váš
hit	hit	k1gInSc1	hit
-	-	kIx~	-
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
-	-	kIx~	-
Levné	levný	k2eAgFnPc1d1	levná
Knihy	kniha	k1gFnPc1	kniha
-	-	kIx~	-
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Půlnoc	půlnoc	k1gFnSc1	půlnoc
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc4	období
zlé	zlá	k1gFnSc2	zlá
2005	[number]	k4	2005
Hit-paráda	Hitaráda	k1gFnSc1	Hit-paráda
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
-	-	kIx~	-
Závidím	závidět	k5eAaImIp1nS	závidět
cd	cd	kA	cd
2	[number]	k4	2
2006	[number]	k4	2006
Už	už	k6eAd1	už
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
zní	znět	k5eAaImIp3nS	znět
zvon	zvon	k1gInSc1	zvon
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Padá	padat	k5eAaImIp3nS	padat
sníh	sníh	k1gInSc1	sníh
-	-	kIx~	-
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pulec	pulec	k1gMnSc1	pulec
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Rolničky	rolnička	k1gFnPc1	rolnička
-	-	kIx~	-
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
38	[number]	k4	38
originálních	originální	k2eAgFnPc2d1	originální
nahrávek	nahrávka	k1gFnPc2	nahrávka
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
-	-	kIx~	-
Supraphon	supraphon	k1gInSc1	supraphon
-	-	kIx~	-
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgInPc1d1	bílý
tulipány	tulipán	k1gInPc1	tulipán
2007	[number]	k4	2007
Hold	hold	k1gInSc4	hold
Johnnymu	Johnnym	k1gInSc2	Johnnym
Cashovi	Cash	k1gMnSc6	Cash
-	-	kIx~	-
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
-	-	kIx~	-
Jiří	Jiří	k1gMnSc1	Jiří
Grossmann	Grossmann	k1gMnSc1	Grossmann
a	a	k8xC	a
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
2007	[number]	k4	2007
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
country	country	k2eAgInSc4d1	country
výběr	výběr	k1gInSc4	výběr
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
-	-	kIx~	-
Universal	Universal	k1gMnSc1	Universal
Music	Music	k1gMnSc1	Music
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Závidím	závidět	k5eAaImIp1nS	závidět
(	(	kIx(	(
<g/>
Il	Il	k1gFnSc1	Il
ragazzo	ragazza	k1gFnSc5	ragazza
della	delnout	k5eAaImAgFnS	delnout
via	via	k7c4	via
gluck	gluck	k1gInSc4	gluck
<g/>
)	)	kIx)	)
-	-	kIx~	-
cd	cd	kA	cd
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Tobě	ty	k3xPp2nSc3	ty
náležel	náležet	k5eAaImAgInS	náležet
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Wanna	Wanna	k1gFnSc1	Wanna
Play	play	k0	play
House	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
-	-	kIx~	-
cd	cd	kA	cd
2	[number]	k4	2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
cz	cz	k?	cz
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
NAĎA	Naďa	k1gFnSc1	Naďa
URBÁNKOVÁ	Urbánková	k1gFnSc1	Urbánková
<g/>
:	:	kIx,	:
Všechny	všechen	k3xTgInPc1	všechen
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsem	být	k5eAaImIp1nS	být
natočila	natočit	k5eAaBmAgFnS	natočit
byly	být	k5eAaImAgFnP	být
zakázané	zakázaný	k2eAgNnSc4d1	zakázané
Video	video	k1gNnSc4	video
<g/>
:	:	kIx,	:
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
-	-	kIx~	-
Vilém	Vilém	k1gMnSc1	Vilém
peče	péct	k5eAaImIp3nS	péct
housky	houska	k1gFnSc2	houska
Video	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Grossmann	Grossmann	k1gMnSc1	Grossmann
-	-	kIx~	-
Drahý	drahý	k1gMnSc1	drahý
můj	můj	k1gMnSc1	můj
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
13	[number]	k4	13
<g/>
.	.	kIx.	.
komnata	komnata	k1gFnSc1	komnata
Hlas	hlas	k1gInSc1	hlas
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dar	dar	k1gInSc1	dar
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
-	-	kIx~	-
rozhovor	rozhovor	k1gInSc4	rozhovor
Katolický	katolický	k2eAgInSc4d1	katolický
týdeník	týdeník	k1gInSc4	týdeník
</s>
