<s>
Charles	Charles	k1gMnSc1	Charles
Edouard	Edouard	k1gMnSc1	Edouard
Guillaume	Guillaum	k1gInSc5	Guillaum
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Fleurier	Fleurier	k1gInSc1	Fleurier
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Sè	Sè	k1gFnSc1	Sè
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzsko-švýcarský	francouzsko-švýcarský	k2eAgMnSc1d1	francouzsko-švýcarský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
objev	objev	k1gInSc4	objev
anomálií	anomálie	k1gFnPc2	anomálie
v	v	k7c6	v
niklové	niklový	k2eAgFnSc6d1	niklová
oceli	ocel	k1gFnSc6	ocel
(	(	kIx(	(
<g/>
invar	invar	k1gInSc1	invar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
přesných	přesný	k2eAgFnPc2d1	přesná
měření	měření	k1gNnPc2	měření
<g/>
.	.	kIx.	.
</s>
