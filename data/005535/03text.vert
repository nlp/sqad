<s>
Komedie	komedie	k1gFnSc1	komedie
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgInSc4d1	literární
nebo	nebo	k8xC	nebo
dramatický	dramatický	k2eAgInSc4d1	dramatický
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vždy	vždy	k6eAd1	vždy
skončí	skončit	k5eAaPmIp3nS	skončit
šťastně	šťastně	k6eAd1	šťastně
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
komedie	komedie	k1gFnSc2	komedie
je	být	k5eAaImIp3nS	být
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
komedie	komedie	k1gFnSc2	komedie
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
humorný	humorný	k2eAgInSc1d1	humorný
nadhled	nadhled	k1gInSc1	nadhled
nad	nad	k7c7	nad
lidskými	lidský	k2eAgFnPc7d1	lidská
slabostmi	slabost	k1gFnPc7	slabost
a	a	k8xC	a
lidskou	lidský	k2eAgFnSc7d1	lidská
nedostatečností	nedostatečnost	k1gFnSc7	nedostatečnost
<g/>
.	.	kIx.	.
</s>
<s>
Humor	humor	k1gInSc1	humor
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vkusnou	vkusný	k2eAgFnSc7d1	vkusná
komikou	komika	k1gFnSc7	komika
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
základními	základní	k2eAgInPc7d1	základní
aspekty	aspekt	k1gInPc7	aspekt
tohoto	tento	k3xDgInSc2	tento
útvaru	útvar	k1gInSc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
hrdiny	hrdina	k1gMnSc2	hrdina
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
nakonec	nakonec	k6eAd1	nakonec
vyřešit	vyřešit	k5eAaPmF	vyřešit
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
v	v	k7c6	v
dobré	dobrá	k1gFnSc6	dobrá
obrátí	obrátit	k5eAaPmIp3nS	obrátit
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
spojuje	spojovat	k5eAaImIp3nS	spojovat
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
s	s	k7c7	s
kómos	kómos	k1gInSc1	kómos
<g/>
,	,	kIx,	,
průvod	průvod	k1gInSc1	průvod
spjatý	spjatý	k2eAgInSc1d1	spjatý
s	s	k7c7	s
Dionýsovým	Dionýsův	k2eAgInSc7d1	Dionýsův
kultem	kult	k1gInSc7	kult
(	(	kIx(	(
<g/>
malé	malý	k2eAgFnPc4d1	malá
dionýsie	dionýsie	k1gFnPc4	dionýsie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
prvky	prvek	k1gInPc4	prvek
patří	patřit	k5eAaImIp3nP	patřit
komické	komický	k2eAgFnPc4d1	komická
scénky	scénka	k1gFnPc4	scénka
ze	z	k7c2	z
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
v	v	k7c6	v
kostýmech	kostým	k1gInPc6	kostým
<g/>
:	:	kIx,	:
vycpaná	vycpaný	k2eAgNnPc1d1	vycpané
břicha	břicho	k1gNnPc1	břicho
a	a	k8xC	a
zadnice	zadnice	k1gFnPc1	zadnice
<g/>
,	,	kIx,	,
přivázaný	přivázaný	k2eAgInSc1d1	přivázaný
kožený	kožený	k2eAgInSc1d1	kožený
falos	falos	k1gInSc1	falos
tj.	tj.	kA	tj.
předimenzovaný	předimenzovaný	k2eAgInSc1d1	předimenzovaný
mužský	mužský	k2eAgInSc1d1	mužský
úd	úd	k1gInSc1	úd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sborové	sborový	k2eAgFnPc1d1	sborová
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
agón	agón	k1gInSc1	agón
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zápas	zápas	k1gInSc1	zápas
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jara	jaro	k1gNnSc2	jaro
se	s	k7c7	s
zimou	zima	k1gFnSc7	zima
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tance	tanec	k1gInPc4	tanec
ve	v	k7c6	v
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
maskách	maska	k1gFnPc6	maska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
byla	být	k5eAaImAgFnS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
nižší	nízký	k2eAgFnSc4d2	nižší
formu	forma	k1gFnSc4	forma
než	než	k8xS	než
tragédie	tragédie	k1gFnSc2	tragédie
a	a	k8xC	a
skládala	skládat	k5eAaImAgFnS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
výstupů	výstup	k1gInPc2	výstup
spojených	spojený	k2eAgInPc2d1	spojený
pouze	pouze	k6eAd1	pouze
postavou	postava	k1gFnSc7	postava
šprýmaře	šprýmař	k1gMnSc2	šprýmař
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
dějové	dějový	k2eAgFnSc2d1	dějová
<g/>
,	,	kIx,	,
časové	časový	k2eAgFnSc2d1	časová
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
souvislosti	souvislost	k1gFnSc2	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Dějovou	dějový	k2eAgFnSc4d1	dějová
souvislost	souvislost	k1gFnSc4	souvislost
začala	začít	k5eAaPmAgFnS	začít
získávat	získávat	k5eAaImF	získávat
již	již	k6eAd1	již
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
až	až	k9	až
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
attická	attická	k1gFnSc1	attická
komedie	komedie	k1gFnSc1	komedie
<g/>
)	)	kIx)	)
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
politická	politický	k2eAgFnSc1d1	politická
<g/>
,	,	kIx,	,
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
životem	život	k1gInSc7	život
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
kritika	kritika	k1gFnSc1	kritika
nedostatků	nedostatek	k1gInPc2	nedostatek
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
i	i	k9	i
přírodní	přírodní	k2eAgMnPc1d1	přírodní
filozofové	filozof	k1gMnPc1	filozof
<g/>
,	,	kIx,	,
sofisté	sofista	k1gMnPc1	sofista
<g/>
,	,	kIx,	,
autoři	autor	k1gMnPc1	autor
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
,	,	kIx,	,
dithyrambů	dithyramb	k1gInPc2	dithyramb
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
sólistů	sólista	k1gMnPc2	sólista
(	(	kIx(	(
<g/>
i	i	k8xC	i
ženské	ženský	k2eAgFnPc4d1	ženská
role	role	k1gFnPc4	role
hrají	hrát	k5eAaImIp3nP	hrát
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
maskovaný	maskovaný	k2eAgMnSc1d1	maskovaný
za	za	k7c4	za
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
oblaka	oblaka	k1gNnPc4	oblaka
<g/>
,	,	kIx,	,
spojenecká	spojenecký	k2eAgNnPc4d1	spojenecké
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
fantastické	fantastický	k2eAgNnSc1d1	fantastické
<g/>
.	.	kIx.	.
</s>
<s>
Kompozice	kompozice	k1gFnSc1	kompozice
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
volná	volný	k2eAgFnSc1d1	volná
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
na	na	k7c4	na
40	[number]	k4	40
jmen	jméno	k1gNnPc2	jméno
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Eupolis	Eupolis	k1gFnPc1	Eupolis
<g/>
,	,	kIx,	,
Kratínos	Kratínos	k1gInSc1	Kratínos
<g/>
,	,	kIx,	,
Ferekratés	Ferekratés	k1gInSc1	Ferekratés
a	a	k8xC	a
Aristofanés	Aristofanés	k1gInSc1	Aristofanés
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInSc1d2	menší
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
vystoupení	vystoupení	k1gNnSc1	vystoupení
(	(	kIx(	(
<g/>
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
<g/>
)	)	kIx)	)
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
přestávky	přestávka	k1gFnPc1	přestávka
<g/>
;	;	kIx,	;
písně	píseň	k1gFnPc1	píseň
(	(	kIx(	(
<g/>
embolima	embolima	k1gFnSc1	embolima
<g/>
)	)	kIx)	)
nesouvisejí	souviset	k5eNaImIp3nP	souviset
s	s	k7c7	s
dějem	děj	k1gInSc7	děj
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
politických	politický	k2eAgInPc2d1	politický
systémů	systém	k1gInPc2	systém
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
útočnost	útočnost	k1gFnSc1	útočnost
<g/>
,	,	kIx,	,
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
se	se	k3xPyFc4	se
adresná	adresný	k2eAgFnSc1d1	adresná
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
přibývají	přibývat	k5eAaImIp3nP	přibývat
směšné	směšný	k2eAgInPc1d1	směšný
typy	typ	k1gInPc1	typ
(	(	kIx(	(
<g/>
parasit	parasit	k1gMnSc1	parasit
<g/>
,	,	kIx,	,
kuchař	kuchař	k1gMnSc1	kuchař
<g/>
,	,	kIx,	,
hetéra	hetéra	k1gFnSc1	hetéra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mizí	mizet	k5eAaImIp3nS	mizet
obscénnost	obscénnost	k1gFnSc1	obscénnost
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
na	na	k7c4	na
50	[number]	k4	50
jmen	jméno	k1gNnPc2	jméno
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnPc1d2	známější
jsou	být	k5eAaImIp3nP	být
Antifanés	Antifanésa	k1gFnPc2	Antifanésa
<g/>
,	,	kIx,	,
Anaxandridés	Anaxandridésa	k1gFnPc2	Anaxandridésa
a	a	k8xC	a
Alexis	Alexis	k1gFnPc2	Alexis
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
mizí	mizet	k5eAaImIp3nS	mizet
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Dionýsovým	Dionýsův	k2eAgInSc7d1	Dionýsův
kultem	kult	k1gInSc7	kult
a	a	k8xC	a
fantastičnost	fantastičnost	k1gFnSc1	fantastičnost
<g/>
,	,	kIx,	,
převládají	převládat	k5eAaImIp3nP	převládat
typy	typ	k1gInPc4	typ
a	a	k8xC	a
náměty	námět	k1gInPc4	námět
ze	z	k7c2	z
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
středních	střední	k2eAgFnPc2d1	střední
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc1d1	častý
typy	typ	k1gInPc1	typ
<g/>
:	:	kIx,	:
lakomý	lakomý	k2eAgMnSc1d1	lakomý
a	a	k8xC	a
přísný	přísný	k2eAgMnSc1d1	přísný
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
hádavá	hádavý	k2eAgFnSc1d1	hádavá
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
lehkomyslný	lehkomyslný	k2eAgMnSc1d1	lehkomyslný
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
mazaný	mazaný	k2eAgMnSc1d1	mazaný
otrok	otrok	k1gMnSc1	otrok
<g/>
,	,	kIx,	,
příživník	příživník	k1gMnSc1	příživník
<g/>
,	,	kIx,	,
vychloubavý	vychloubavý	k2eAgMnSc1d1	vychloubavý
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
hetéra	hetéra	k1gFnSc1	hetéra
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
motivy	motiv	k1gInPc7	motiv
jsou	být	k5eAaImIp3nP	být
láska	láska	k1gFnSc1	láska
s	s	k7c7	s
překážkami	překážka	k1gFnPc7	překážka
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
odložení	odložení	k1gNnSc1	odložení
dítěte	dítě	k1gNnSc2	dítě
(	(	kIx(	(
<g/>
anagnórisis	anagnórisis	k1gFnSc1	anagnórisis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
70	[number]	k4	70
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jména	jméno	k1gNnPc1	jméno
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgInSc4d3	nejznámější
Filemón	Filemón	k1gInSc4	Filemón
<g/>
,	,	kIx,	,
Dífilos	Dífilosa	k1gFnPc2	Dífilosa
a	a	k8xC	a
Menandros	Menandrosa	k1gFnPc2	Menandrosa
<g/>
.	.	kIx.	.
tragédie	tragédie	k1gFnSc1	tragédie
fraška	fraška	k1gFnSc1	fraška
groteska	groteska	k1gFnSc1	groteska
satira	satira	k1gFnSc1	satira
parodie	parodie	k1gFnSc1	parodie
humor	humor	k1gInSc1	humor
komika	komika	k1gFnSc1	komika
</s>
