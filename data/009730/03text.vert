<p>
<s>
Bekhend	bekhend	k1gInSc1	bekhend
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
backhand	backhand	k1gInSc1	backhand
<g/>
)	)	kIx)	)
v	v	k7c6	v
tenisu	tenis	k1gInSc6	tenis
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
úderů	úder	k1gInPc2	úder
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
hráč	hráč	k1gMnSc1	hráč
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
míč	míč	k1gInSc4	míč
po	po	k7c6	po
odrazu	odraz	k1gInSc6	odraz
od	od	k7c2	od
země	zem	k1gFnSc2	zem
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
polovině	polovina	k1gFnSc6	polovina
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
u	u	k7c2	u
praváků	pravák	k1gMnPc2	pravák
<g/>
,	,	kIx,	,
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
polovině	polovina	k1gFnSc6	polovina
u	u	k7c2	u
leváků	levák	k1gMnPc2	levák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bekhend	bekhend	k1gInSc1	bekhend
je	být	k5eAaImIp3nS	být
úderem	úder	k1gInSc7	úder
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
míčových	míčový	k2eAgInPc6d1	míčový
sportech	sport	k1gInPc6	sport
např.	např.	kA	např.
v	v	k7c6	v
badmintonu	badminton	k1gInSc6	badminton
<g/>
,	,	kIx,	,
squashi	squash	k1gInSc6	squash
nebo	nebo	k8xC	nebo
stolním	stolní	k2eAgInSc6d1	stolní
tenisu	tenis	k1gInSc6	tenis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
bekhendu	bekhend	k1gInSc2	bekhend
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
formy	forma	k1gFnSc2	forma
zásahu	zásah	k1gInSc2	zásah
míče	míč	k1gInSc2	míč
raketou	raketa	k1gFnSc7	raketa
lze	lze	k6eAd1	lze
rozlišitbekhend	rozlišitbekhenda	k1gFnPc2	rozlišitbekhenda
přímý	přímý	k2eAgMnSc1d1	přímý
</s>
</p>
<p>
<s>
bekhend	bekhend	k1gInSc1	bekhend
s	s	k7c7	s
rotací	rotace	k1gFnSc7	rotace
–	–	k?	–
horní	horní	k2eAgFnSc1d1	horní
či	či	k8xC	či
spodníPodle	spodníPodle	k6eAd1	spodníPodle
počtu	počet	k1gInSc2	počet
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
drží	držet	k5eAaImIp3nP	držet
při	při	k7c6	při
úderu	úder	k1gInSc6	úder
raketujednoručný	raketujednoručný	k2eAgInSc1d1	raketujednoručný
bekhend	bekhend	k1gInSc1	bekhend
</s>
</p>
<p>
<s>
obouručný	obouručný	k2eAgInSc1d1	obouručný
bekhendPokud	bekhendPokud	k1gInSc1	bekhendPokud
je	být	k5eAaImIp3nS	být
míč	míč	k1gInSc4	míč
zasáhnut	zasáhnut	k2eAgInSc4d1	zasáhnut
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pohybu	pohyb	k1gInSc2	pohyb
vpřed	vpřed	k6eAd1	vpřed
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
nabíhaném	nabíhaný	k2eAgMnSc6d1	nabíhaný
(	(	kIx(	(
<g/>
útočném	útočný	k2eAgInSc6d1	útočný
<g/>
)	)	kIx)	)
bekhendu	bekhend	k1gInSc6	bekhend
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
míč	míč	k1gInSc1	míč
nedopadne	dopadnout	k5eNaPmIp3nS	dopadnout
na	na	k7c4	na
dvorec	dvorec	k1gInSc4	dvorec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
bekhendovém	bekhendový	k2eAgInSc6d1	bekhendový
voleji	volej	k1gInSc6	volej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mechanizmus	mechanizmus	k1gInSc1	mechanizmus
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
bočním	boční	k2eAgNnSc6d1	boční
postavení	postavení	k1gNnSc6	postavení
osa	osa	k1gFnSc1	osa
ramen	rameno	k1gNnPc2	rameno
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
pokrčená	pokrčený	k2eAgNnPc4d1	pokrčené
kolena	koleno	k1gNnPc4	koleno
a	a	k8xC	a
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
vpředu	vpředu	k6eAd1	vpředu
(	(	kIx(	(
<g/>
u	u	k7c2	u
praváků	pravák	k1gMnPc2	pravák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bekhendový	bekhendový	k2eAgInSc1d1	bekhendový
úder	úder	k1gInSc1	úder
je	být	k5eAaImIp3nS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
nápřahovým	nápřahový	k2eAgInSc7d1	nápřahový
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Ruka	ruka	k1gFnSc1	ruka
(	(	kIx(	(
<g/>
či	či	k8xC	či
ruce	ruka	k1gFnPc4	ruka
u	u	k7c2	u
obouruče	obouruč	k1gFnSc2	obouruč
<g/>
)	)	kIx)	)
s	s	k7c7	s
raketou	raketa	k1gFnSc7	raketa
poté	poté	k6eAd1	poté
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
letícímu	letící	k2eAgInSc3d1	letící
míči	míč	k1gInSc3	míč
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ruka	ruka	k1gFnSc1	ruka
protáhne	protáhnout	k5eAaPmIp3nS	protáhnout
úder	úder	k1gInSc4	úder
na	na	k7c4	na
pravou	pravá	k1gFnSc4	pravá
(	(	kIx(	(
<g/>
u	u	k7c2	u
praváka	pravák	k1gMnSc2	pravák
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
levou	levá	k1gFnSc4	levá
(	(	kIx(	(
<g/>
u	u	k7c2	u
leváka	levák	k1gMnSc2	levák
<g/>
)	)	kIx)	)
stranu	strana	k1gFnSc4	strana
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Úder	úder	k1gInSc1	úder
je	být	k5eAaImIp3nS	být
hrán	hrát	k5eAaImNgInS	hrát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
míč	míč	k1gInSc1	míč
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
dvorce	dvorec	k1gInSc2	dvorec
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
hráč	hráč	k1gMnSc1	hráč
nedrží	držet	k5eNaImIp3nS	držet
raketu	raketa	k1gFnSc4	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
míč	míč	k1gInSc1	míč
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
forhendové	forhendový	k2eAgFnSc2d1	forhendová
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
u	u	k7c2	u
hráčů	hráč	k1gMnPc2	hráč
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
lepším	dobrý	k2eAgInSc7d2	lepší
bekhendem	bekhend	k1gInSc7	bekhend
<g/>
)	)	kIx)	)
oběhnout	oběhnout	k5eAaPmF	oběhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
zahrát	zahrát	k5eAaPmF	zahrát
tímto	tento	k3xDgInSc7	tento
úderem	úder	k1gInSc7	úder
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
celá	celý	k2eAgFnSc1d1	celá
plocha	plocha	k1gFnSc1	plocha
dvorce	dvorec	k1gInSc2	dvorec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Držení	držení	k1gNnSc4	držení
rakety	raketa	k1gFnPc1	raketa
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
uchopení	uchopení	k1gNnSc4	uchopení
rakety	raketa	k1gFnSc2	raketa
při	při	k7c6	při
úderech	úder	k1gInPc6	úder
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
držena	držen	k2eAgFnSc1d1	držena
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
sklonu	sklon	k1gInSc2	sklon
(	(	kIx(	(
<g/>
natočení	natočení	k1gNnSc2	natočení
<g/>
)	)	kIx)	)
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
styly	styl	k1gInPc1	styl
držení	držení	k1gNnSc2	držení
rakety	raketa	k1gFnSc2	raketa
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jednotný	jednotný	k2eAgInSc1d1	jednotný
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
<g/>
)	)	kIx)	)
–	–	k?	–
pozice	pozice	k1gFnPc4	pozice
ruky	ruka	k1gFnSc2	ruka
na	na	k7c6	na
držadle	držadlo	k1gNnSc6	držadlo
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
;	;	kIx,	;
vidlice	vidlice	k1gFnSc1	vidlice
mezi	mezi	k7c7	mezi
palcem	palec	k1gInSc7	palec
a	a	k8xC	a
ukazovákem	ukazovák	k1gInSc7	ukazovák
je	být	k5eAaImIp3nS	být
blízko	blízko	k7c2	blízko
levé	levý	k2eAgFnSc2d1	levá
hrany	hrana	k1gFnSc2	hrana
horní	horní	k2eAgFnSc2d1	horní
plošky	ploška	k1gFnSc2	ploška
držadla	držadlo	k1gNnSc2	držadlo
</s>
</p>
<p>
<s>
změněné	změněný	k2eAgNnSc1d1	změněné
pro	pro	k7c4	pro
forhend	forhend	k1gInSc4	forhend
a	a	k8xC	a
bekhend	bekhend	k1gInSc1	bekhend
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
držadlo	držadlo	k1gNnSc4	držadlo
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
mírně	mírně	k6eAd1	mírně
pootočí	pootočit	k5eAaPmIp3nS	pootočit
doprava	doprava	k1gFnSc1	doprava
či	či	k8xC	či
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
forhendové	forhendový	k2eAgNnSc1d1	forhendové
a	a	k8xC	a
bekhendové	bekhendový	k2eAgNnSc1d1	bekhendové
držení	držení	k1gNnSc1	držení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
východním	východní	k2eAgNnSc6d1	východní
držení	držení	k1gNnSc6	držení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rozdíly	rozdíl	k1gInPc1	rozdíl
pro	pro	k7c4	pro
forhendové	forhendový	k2eAgNnSc4d1	forhendové
a	a	k8xC	a
bekhendové	bekhendový	k2eAgNnSc4d1	bekhendové
držení	držení	k1gNnSc4	držení
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
západním	západní	k2eAgNnSc6d1	západní
držení	držení	k1gNnSc1	držení
(	(	kIx(	(
<g/>
kalifornském	kalifornský	k2eAgInSc6d1	kalifornský
či	či	k8xC	či
"	"	kIx"	"
<g/>
přehnaně	přehnaně	k6eAd1	přehnaně
<g/>
"	"	kIx"	"
forhendovém	forhendový	k2eAgInSc6d1	forhendový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
modifikované	modifikovaný	k2eAgInPc1d1	modifikovaný
–	–	k?	–
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
bekhendu	bekhend	k1gInSc2	bekhend
a	a	k8xC	a
forhendu	forhend	k1gInSc2	forhend
jsou	být	k5eAaImIp3nP	být
minimální	minimální	k2eAgFnPc1d1	minimální
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k9	jen
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
polohy	poloha	k1gFnSc2	poloha
prstů	prst	k1gInPc2	prst
na	na	k7c6	na
držadle	držadlo	k1gNnSc6	držadlo
</s>
</p>
<p>
<s>
==	==	k?	==
Obouručný	Obouručný	k2eAgInSc1d1	Obouručný
bekhend	bekhend	k1gInSc1	bekhend
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
bekhend	bekhend	k1gInSc1	bekhend
hrán	hrát	k5eAaImNgInS	hrát
jednoručně	jednoručně	k6eAd1	jednoručně
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
východním	východní	k2eAgNnSc7d1	východní
držením	držení	k1gNnSc7	držení
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
významní	významný	k2eAgMnPc1d1	významný
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
hrát	hrát	k5eAaImF	hrát
obouručným	obouručný	k2eAgInSc7d1	obouručný
bekhendem	bekhend	k1gInSc7	bekhend
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Australané	Australan	k1gMnPc1	Australan
Vivian	Viviana	k1gFnPc2	Viviana
McGrathová	McGrathová	k1gFnSc1	McGrathová
a	a	k8xC	a
John	John	k1gMnSc1	John
Bromwich	Bromwich	k1gMnSc1	Bromwich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
hráči	hráč	k1gMnPc1	hráč
s	s	k7c7	s
obouručným	obouručný	k2eAgInSc7d1	obouručný
bekhendem	bekhend	k1gInSc7	bekhend
(	(	kIx(	(
<g/>
Jimmy	Jimma	k1gFnSc2	Jimma
Connors	Connors	k1gInSc1	Connors
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Evertová	Evertová	k1gFnSc1	Evertová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
úderu	úder	k1gInSc2	úder
významného	významný	k2eAgNnSc2d1	významné
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Obouručný	Obouručný	k2eAgInSc1d1	Obouručný
bekhend	bekhend	k1gInSc1	bekhend
je	být	k5eAaImIp3nS	být
standardně	standardně	k6eAd1	standardně
vyučován	vyučovat	k5eAaImNgInS	vyučovat
již	již	k6eAd1	již
v	v	k7c6	v
tenisové	tenisový	k2eAgFnSc6d1	tenisová
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
profesionálové	profesionál	k1gMnPc1	profesionál
však	však	k9	však
přecházejí	přecházet	k5eAaImIp3nP	přecházet
z	z	k7c2	z
obouručného	obouručný	k2eAgMnSc2d1	obouručný
na	na	k7c4	na
jednoručný	jednoručný	k2eAgInSc4d1	jednoručný
úder	úder	k1gInSc4	úder
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pete	Pete	k1gNnSc1	Pete
Sampras	Sampras	k1gMnSc1	Sampras
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
Edberg	Edberg	k1gMnSc1	Edberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tenisté	tenista	k1gMnPc1	tenista
==	==	k?	==
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
přehled	přehled	k1gInSc4	přehled
některých	některý	k3yIgMnPc2	některý
tenistů	tenista	k1gMnPc2	tenista
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
byl	být	k5eAaImAgInS	být
<g/>
/	/	kIx~	/
<g/>
je	být	k5eAaImIp3nS	být
bekhend	bekhend	k1gInSc1	bekhend
velmi	velmi	k6eAd1	velmi
silným	silný	k2eAgInSc7d1	silný
úderem	úder	k1gInSc7	úder
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Muži	muž	k1gMnPc1	muž
===	===	k?	===
</s>
</p>
<p>
<s>
Gustavo	Gustava	k1gFnSc5	Gustava
Kuerten	Kuerten	k2eAgInSc4d1	Kuerten
</s>
</p>
<p>
<s>
Jimmy	Jimma	k1gFnPc1	Jimma
Connors	Connorsa	k1gFnPc2	Connorsa
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Lendl	Lendl	k1gMnSc1	Lendl
</s>
</p>
<p>
<s>
Stefan	Stefan	k1gMnSc1	Stefan
Edberg	Edberg	k1gMnSc1	Edberg
</s>
</p>
<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Davyděnko	Davyděnka	k1gFnSc5	Davyděnka
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
</s>
</p>
<p>
<s>
Novak	Novak	k1gInSc1	Novak
Djokovic	Djokovice	k1gFnPc2	Djokovice
</s>
</p>
<p>
<s>
Norbert	Norbert	k1gMnSc1	Norbert
Ratiu	Ratius	k1gMnSc3	Ratius
</s>
</p>
<p>
<s>
Andre	Andr	k1gMnSc5	Andr
Agassi	Agass	k1gMnSc5	Agass
</s>
</p>
<p>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS	Jevgenít
Kafelnikov	Kafelnikov	k1gInSc4	Kafelnikov
</s>
</p>
<p>
<s>
Marat	Marat	k2eAgInSc1d1	Marat
Safin	Safin	k1gInSc1	Safin
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Gasquet	Gasquet	k1gMnSc1	Gasquet
</s>
</p>
<p>
<s>
Tommy	Tomma	k1gFnPc1	Tomma
Haas	Haasa	k1gFnPc2	Haasa
</s>
</p>
<p>
<s>
Lleyton	Lleyton	k1gInSc1	Lleyton
Hewitt	Hewitta	k1gFnPc2	Hewitta
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Nalbandian	Nalbandian	k1gMnSc1	Nalbandian
</s>
</p>
<p>
<s>
Boris	Boris	k1gMnSc1	Boris
Becker	Becker	k1gMnSc1	Becker
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
McEnroe	McEnro	k1gFnSc2	McEnro
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Ljubičić	Ljubičić	k1gMnSc1	Ljubičić
</s>
</p>
<p>
<s>
Gastón	Gastón	k1gMnSc1	Gastón
Gaudio	Gaudio	k1gMnSc1	Gaudio
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
McEnroe	McEnro	k1gFnSc2	McEnro
</s>
</p>
<p>
<s>
Björn	Björn	k1gMnSc1	Björn
Borg	Borg	k1gMnSc1	Borg
</s>
</p>
<p>
<s>
Marcelo	Marcela	k1gFnSc5	Marcela
Ríos	Ríos	k1gInSc4	Ríos
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Pavel	Pavel	k1gMnSc1	Pavel
</s>
</p>
<p>
<s>
Stanislas	Stanislas	k1gInSc1	Stanislas
Wawrinka	Wawrinka	k1gFnSc1	Wawrinka
</s>
</p>
<p>
<s>
Andy	Anda	k1gFnPc1	Anda
Murray	Murraa	k1gFnSc2	Murraa
</s>
</p>
<p>
<s>
===	===	k?	===
Ženy	žena	k1gFnPc1	žena
===	===	k?	===
</s>
</p>
<p>
<s>
Martina	Martina	k1gFnSc1	Martina
Hingisová	Hingisový	k2eAgFnSc1d1	Hingisová
</s>
</p>
<p>
<s>
Chris	Chris	k1gFnSc1	Chris
Evertová	Evertová	k1gFnSc1	Evertová
</s>
</p>
<p>
<s>
Monika	Monika	k1gFnSc1	Monika
Selešová	Selešová	k1gFnSc1	Selešová
</s>
</p>
<p>
<s>
Justine	Justin	k1gMnSc5	Justin
Heninová	Heninová	k1gFnSc5	Heninová
</s>
</p>
<p>
<s>
Amélie	Amélie	k1gFnSc5	Amélie
Mauresmo	Mauresma	k1gFnSc5	Mauresma
</s>
</p>
<p>
<s>
Jelena	Jelena	k1gFnSc1	Jelena
Jankovićová	Jankovićová	k1gFnSc1	Jankovićová
</s>
</p>
<p>
<s>
Gabriela	Gabriela	k1gFnSc1	Gabriela
Sabatini	Sabatin	k2eAgMnPc1d1	Sabatin
</s>
</p>
<p>
<s>
Venus	Venus	k1gInSc1	Venus
Williamsová	Williamsová	k1gFnSc1	Williamsová
</s>
</p>
<p>
<s>
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
</s>
</p>
<p>
<s>
Maria	Maria	k1gFnSc1	Maria
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
</s>
</p>
<p>
<s>
Monica	Monica	k6eAd1	Monica
NiculescuováJohn	NiculescuováJohn	k1gInSc1	NiculescuováJohn
McEnroe	McEnroe	k1gFnPc2	McEnroe
označil	označit	k5eAaPmAgInS	označit
jednoručný	jednoručný	k2eAgInSc1d1	jednoručný
bekhend	bekhend	k1gInSc1	bekhend
Justine	Justin	k1gMnSc5	Justin
Heninové	Heninové	k2eAgNnPc1d1	Heninové
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ženského	ženský	k2eAgInSc2d1	ženský
i	i	k8xC	i
mužského	mužský	k2eAgInSc2d1	mužský
tenisu	tenis	k1gInSc2	tenis
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc4d1	hlavní
důvod	důvod	k1gInSc4	důvod
jejího	její	k3xOp3gInSc2	její
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Lichner	Lichner	k1gMnSc1	Lichner
<g/>
,	,	kIx,	,
I.	I.	kA	I.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bekhend	bekhend	k1gInSc1	bekhend
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Technika	technika	k1gFnSc1	technika
bekhendu	bekhend	k1gInSc2	bekhend
</s>
</p>
