<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1887	[number]	k4	1887
Hronov	Hronov	k1gInSc1	Hronov
–	–	k?	–
duben	duben	k1gInSc1	duben
1945	[number]	k4	1945
Bergen-Belsen	Bergen-Belsna	k1gFnPc2	Bergen-Belsna
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
knižní	knižní	k2eAgMnSc1d1	knižní
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
spisovatele	spisovatel	k1gMnSc2	spisovatel
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
lékaře	lékař	k1gMnSc2	lékař
Antonína	Antonín	k1gMnSc2	Antonín
Čapka	Čapek	k1gMnSc2	Čapek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Boženy	Božena	k1gFnSc2	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
lázeňském	lázeňský	k2eAgInSc6d1	lázeňský
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Malých	Malých	k2eAgFnPc6d1	Malých
Svatoňovicích	Svatoňovice	k1gFnPc6	Svatoňovice
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
starší	starý	k2eAgFnSc7d2	starší
sestrou	sestra	k1gFnSc7	sestra
Helenou	Helena	k1gFnSc7	Helena
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Úpice	Úpice	k1gFnSc2	Úpice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
obecnou	obecná	k1gFnSc4	obecná
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
a	a	k8xC	a
měšťanskou	měšťanský	k2eAgFnSc7d1	měšťanská
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
však	však	k9	však
příliš	příliš	k6eAd1	příliš
neprospíval	prospívat	k5eNaImAgMnS	prospívat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
nadání	nadání	k1gNnSc4	nadání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedalo	dát	k5eNaPmAgNnS	dát
upřít	upřít	k5eAaPmF	upřít
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
odbornou	odborný	k2eAgFnSc4d1	odborná
školu	škola	k1gFnSc4	škola
tkalcovskou	tkalcovský	k2eAgFnSc4d1	tkalcovská
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
absolvování	absolvování	k1gNnSc6	absolvování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
pracoval	pracovat	k5eAaImAgInS	pracovat
rok	rok	k1gInSc1	rok
jako	jako	k8xS	jako
dělník	dělník	k1gMnSc1	dělník
v	v	k7c6	v
úpické	úpický	k2eAgFnSc6d1	Úpická
továrně	továrna	k1gFnSc6	továrna
F.	F.	kA	F.
M.	M.	kA	M.
Oberländera	Oberländer	k1gMnSc2	Oberländer
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1904	[number]	k4	1904
začal	začít	k5eAaPmAgInS	začít
žít	žít	k5eAaImF	žít
už	už	k6eAd1	už
natrvalo	natrvalo	k6eAd1	natrvalo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
seznámil	seznámit	k5eAaPmAgInS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
manželkou	manželka	k1gFnSc7	manželka
Jarmilou	Jarmila	k1gFnSc7	Jarmila
Pospíšilovou	Pospíšilová	k1gFnSc7	Pospíšilová
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
na	na	k7c6	na
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
schůzce	schůzka	k1gFnSc6	schůzka
s	s	k7c7	s
budoucí	budoucí	k2eAgFnSc7d1	budoucí
manželkou	manželka	k1gFnSc7	manželka
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1910	[number]	k4	1910
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
přijel	přijet	k5eAaPmAgMnS	přijet
i	i	k9	i
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
J.	J.	kA	J.
Čapek	Čapek	k1gMnSc1	Čapek
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Colarossiho	Colarossi	k1gMnSc4	Colarossi
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
zkrátil	zkrátit	k5eAaPmAgInS	zkrátit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
čtvrti	čtvrt	k1gFnPc4	čtvrt
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
blízko	blízko	k7c2	blízko
své	svůj	k3xOyFgFnSc2	svůj
vyvolené	vyvolená	k1gFnSc2	vyvolená
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Čapkem	Čapek	k1gMnSc7	Čapek
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
rozepsali	rozepsat	k5eAaPmAgMnP	rozepsat
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
dramatu	drama	k1gNnSc2	drama
Loupežník	loupežník	k1gMnSc1	loupežník
<g/>
.	.	kIx.	.
<g/>
Bratři	bratr	k1gMnPc1	bratr
Čapkové	Čapková	k1gFnSc2	Čapková
se	se	k3xPyFc4	se
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
starší	starý	k2eAgFnSc7d2	starší
a	a	k8xC	a
mladou	mladý	k2eAgFnSc7d1	mladá
nastupující	nastupující	k2eAgFnSc7d1	nastupující
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
generací	generace	k1gFnSc7	generace
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
spolku	spolek	k1gInSc6	spolek
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
mladých	mladý	k2eAgMnPc2d1	mladý
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
spolek	spolek	k1gInSc1	spolek
Mánes	Mánes	k1gMnSc1	Mánes
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
novou	nový	k2eAgFnSc4d1	nová
Skupinu	skupina	k1gFnSc4	skupina
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
nemusel	muset	k5eNaImAgMnS	muset
narukovat	narukovat	k5eAaPmF	narukovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
slabého	slabý	k2eAgInSc2d1	slabý
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Manželství	manželství	k1gNnPc4	manželství
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
věk	věk	k1gInSc4	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
devítileté	devítiletý	k2eAgFnSc6d1	devítiletá
známosti	známost	k1gFnSc6	známost
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
láskou	láska	k1gFnSc7	láska
Jarmilou	Jarmila	k1gFnSc7	Jarmila
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
pražského	pražský	k2eAgMnSc2d1	pražský
advokáta	advokát	k1gMnSc2	advokát
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
<g/>
.	.	kIx.	.
</s>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Ludmily	Ludmila	k1gFnSc2	Ludmila
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bydleli	bydlet	k5eAaImAgMnP	bydlet
jak	jak	k6eAd1	jak
novomanželé	novomanžel	k1gMnPc1	novomanžel
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Říční	říční	k2eAgFnSc6d1	říční
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
jediná	jediný	k2eAgFnSc1d1	jediná
dcera	dcera	k1gFnSc1	dcera
Alena	Alena	k1gFnSc1	Alena
Čapková	Čapková	k1gFnSc1	Čapková
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Dostálová	Dostálová	k1gFnSc1	Dostálová
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
se	se	k3xPyFc4	se
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
Jarmily	Jarmila	k1gFnSc2	Jarmila
Čapkové	Čapková	k1gFnSc3	Čapková
<g/>
,	,	kIx,	,
Zdence	Zdenka	k1gFnSc3	Zdenka
Pospíšilové	Pospíšilová	k1gFnSc2	Pospíšilová
<g/>
,	,	kIx,	,
na	na	k7c4	na
Purkyňovo	Purkyňův	k2eAgNnSc4d1	Purkyňovo
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
náměstí	náměstí	k1gNnSc1	náměstí
Míru	mír	k1gInSc2	mír
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
postavil	postavit	k5eAaPmAgMnS	postavit
architekt	architekt	k1gMnSc1	architekt
Ladislav	Ladislav	k1gMnSc1	Ladislav
Machoň	Machoň	k1gMnSc1	Machoň
pro	pro	k7c4	pro
bratry	bratr	k1gMnPc4	bratr
Čapkovy	Čapkův	k2eAgMnPc4d1	Čapkův
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
hraně	hrana	k1gFnSc6	hrana
vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
kolonie	kolonie	k1gFnSc2	kolonie
Spolku	spolek	k1gInSc2	spolek
žurnalistů	žurnalist	k1gMnPc2	žurnalist
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
dvojvilu	dvojvila	k1gFnSc4	dvojvila
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
národním	národní	k2eAgInSc6d1	národní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
žil	žít	k5eAaImAgMnS	žít
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
polovině	polovina	k1gFnSc6	polovina
dvojdomu	dvojdom	k1gInSc2	dvojdom
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Praze	Praha	k1gFnSc6	Praha
10	[number]	k4	10
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Olgou	Olga	k1gFnSc7	Olga
Scheinpflugovou	Scheinpflugův	k2eAgFnSc7d1	Scheinpflugův
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
domu	dům	k1gInSc2	dům
obýval	obývat	k5eAaImAgInS	obývat
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
<g/>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
především	především	k6eAd1	především
dětským	dětský	k2eAgInSc7d1	dětský
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Alena	Alena	k1gFnSc1	Alena
dorůstala	dorůstat	k5eAaImAgFnS	dorůstat
do	do	k7c2	do
školních	školní	k2eAgNnPc2d1	školní
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
knížku	knížka	k1gFnSc4	knížka
Povídání	povídání	k1gNnSc3	povídání
o	o	k7c6	o
pejskovi	pejsek	k1gMnSc6	pejsek
a	a	k8xC	a
kočičce	kočička	k1gFnSc6	kočička
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kresby	kresba	k1gFnPc4	kresba
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Závěr	závěr	k1gInSc1	závěr
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Želivě	Želiv	k1gInSc6	Želiv
u	u	k7c2	u
Humpolce	Humpolec	k1gInSc2	Humpolec
zatčen	zatknout	k5eAaPmNgMnS	zatknout
gestapem	gestapo	k1gNnSc7	gestapo
a	a	k8xC	a
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
vězni	vězeň	k1gMnPc7	vězeň
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Dachau	Dachaus	k1gInSc2	Dachaus
u	u	k7c2	u
Mnichova	Mnichov	k1gInSc2	Mnichov
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
do	do	k7c2	do
Buchenwaldu	Buchenwald	k1gInSc2	Buchenwald
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
vězněn	věznit	k5eAaImNgMnS	věznit
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
do	do	k7c2	do
malířské	malířský	k2eAgFnSc2d1	malířská
a	a	k8xC	a
písmomalířské	písmomalířský	k2eAgFnSc2d1	písmomalířský
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
maloval	malovat	k5eAaImAgMnS	malovat
rodokmeny	rodokmen	k1gInPc4	rodokmen
členů	člen	k1gMnPc2	člen
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
přidělena	přidělen	k2eAgFnSc1d1	přidělena
i	i	k9	i
dalším	další	k2eAgMnPc3d1	další
malířům	malíř	k1gMnPc3	malíř
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Emilu	Emil	k1gMnSc3	Emil
Fillovi	Fill	k1gMnSc3	Fill
<g/>
.26	.26	k4	.26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
v	v	k7c4	v
Sachsenhausenu	Sachsenhausen	k2eAgFnSc4d1	Sachsenhausen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
malířské	malířský	k2eAgFnSc6d1	malířská
dílně	dílna	k1gFnSc6	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Tajně	tajně	k6eAd1	tajně
překládal	překládat	k5eAaImAgMnS	překládat
anglickou	anglický	k2eAgFnSc4d1	anglická
<g/>
,	,	kIx,	,
španělskou	španělský	k2eAgFnSc4d1	španělská
a	a	k8xC	a
norskou	norský	k2eAgFnSc4d1	norská
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1942	[number]	k4	1942
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
báseň	báseň	k1gFnSc4	báseň
Za	za	k7c7	za
bratrem	bratr	k1gMnSc7	bratr
Karlem	Karel	k1gMnSc7	Karel
a	a	k8xC	a
také	také	k6eAd1	také
kreslil	kreslit	k5eAaImAgMnS	kreslit
drobné	drobný	k2eAgFnSc2d1	drobná
črty	črta	k1gFnSc2	črta
tužkou	tužka	k1gFnSc7	tužka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
v	v	k7c6	v
psaní	psaní	k1gNnSc6	psaní
poezie	poezie	k1gFnSc2	poezie
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
básně	báseň	k1gFnPc1	báseň
kolovaly	kolovat	k5eAaImAgFnP	kolovat
mezi	mezi	k7c7	mezi
spoluvězni	spoluvězeň	k1gMnSc3	spoluvězeň
v	v	k7c6	v
opisech	opis	k1gInPc6	opis
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
v	v	k7c4	v
Bergen-Belsenu	Bergen-Belsen	k2eAgFnSc4d1	Bergen-Belsen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
následně	následně	k6eAd1	následně
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
tyfová	tyfový	k2eAgFnSc1d1	tyfová
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
skvrnitý	skvrnitý	k2eAgInSc4d1	skvrnitý
tyfus	tyfus	k1gInSc4	tyfus
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
svědectví	svědectví	k1gNnPc2	svědectví
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
naživu	naživu	k6eAd1	naživu
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všech	všecek	k3xTgInPc2	všecek
náznaků	náznak	k1gInPc2	náznak
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
osvobozením	osvobození	k1gNnSc7	osvobození
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Rudolf	Rudolf	k1gMnSc1	Rudolf
Margolius	Margolius	k1gMnSc1	Margolius
Jarmilu	Jarmila	k1gFnSc4	Jarmila
Čapkovou	Čapková	k1gFnSc4	Čapková
do	do	k7c2	do
Bergen-Belsenu	Bergen-Belsen	k2eAgFnSc4d1	Bergen-Belsen
hledat	hledat	k5eAaImF	hledat
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gInSc1	jeho
skutečný	skutečný	k2eAgInSc1d1	skutečný
hrob	hrob	k1gInSc1	hrob
neznáme	znát	k5eNaImIp1nP	znát
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
symbolický	symbolický	k2eAgInSc1d1	symbolický
hrob	hrob	k1gInSc1	hrob
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tělesné	tělesný	k2eAgInPc1d1	tělesný
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
nikdy	nikdy	k6eAd1	nikdy
nenašly	najít	k5eNaPmAgFnP	najít
<g/>
,	,	kIx,	,
probíhalo	probíhat	k5eAaImAgNnS	probíhat
řízení	řízení	k1gNnSc1	řízení
o	o	k7c4	o
prohlášení	prohlášení	k1gNnSc4	prohlášení
za	za	k7c4	za
mrtvého	mrtvý	k1gMnSc4	mrtvý
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
úřední	úřední	k2eAgNnSc1d1	úřední
datum	datum	k1gNnSc1	datum
úmrtí	úmrtí	k1gNnSc2	úmrtí
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citát	citát	k1gInSc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Novinář	novinář	k1gMnSc1	novinář
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
Uměleckého	umělecký	k2eAgInSc2d1	umělecký
měsíčníku	měsíčník	k1gInSc2	měsíčník
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orgánu	orgán	k1gInSc3	orgán
Skupiny	skupina	k1gFnSc2	skupina
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
se	s	k7c7	s
Skupinou	skupina	k1gFnSc7	skupina
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
výtvarného	výtvarný	k2eAgInSc2d1	výtvarný
spolku	spolek	k1gInSc2	spolek
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1913-1914	[number]	k4	1913-1914
spoluredaktorem	spoluredaktor	k1gMnSc7	spoluredaktor
jeho	on	k3xPp3gInSc2	on
časopisu	časopis	k1gInSc2	časopis
Volné	volný	k2eAgInPc4d1	volný
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
17	[number]	k4	17
<g/>
/	/	kIx~	/
<g/>
1913	[number]	k4	1913
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
svou	svůj	k3xOyFgFnSc4	svůj
nejzávažnější	závažný	k2eAgFnSc4d3	nejzávažnější
esej	esej	k1gFnSc4	esej
Tvořivá	tvořivý	k2eAgFnSc1d1	tvořivá
povaha	povaha	k1gFnSc1	povaha
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
nejen	nejen	k6eAd1	nejen
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
na	na	k7c4	na
moderní	moderní	k2eAgNnSc4d1	moderní
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
samu	sám	k3xTgFnSc4	sám
podstatu	podstata	k1gFnSc4	podstata
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tvořivosti	tvořivost	k1gFnSc2	tvořivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1920	[number]	k4	1918-1920
redigoval	redigovat	k5eAaImAgMnS	redigovat
časopis	časopis	k1gInSc4	časopis
Nebojsa	nebojsa	k1gMnSc1	nebojsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Karlem	Karel	k1gMnSc7	Karel
Národní	národní	k2eAgFnSc2d1	národní
listy	lista	k1gFnSc2	lista
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
na	na	k7c4	na
18	[number]	k4	18
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
zatčení	zatčení	k1gNnSc2	zatčení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
redaktorem	redaktor	k1gMnSc7	redaktor
a	a	k8xC	a
výtvarným	výtvarný	k2eAgMnSc7d1	výtvarný
kritikem	kritik	k1gMnSc7	kritik
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
jako	jako	k9	jako
karikaturista	karikaturista	k1gMnSc1	karikaturista
a	a	k8xC	a
přispíval	přispívat	k5eAaImAgMnS	přispívat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
deníků	deník	k1gInPc2	deník
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výtvarník	výtvarník	k1gMnSc1	výtvarník
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Člen	člen	k1gInSc1	člen
výtvarných	výtvarný	k2eAgFnPc2d1	výtvarná
skupin	skupina	k1gFnPc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
byl	být	k5eAaImAgMnS	být
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
Skupiny	skupina	k1gFnSc2	skupina
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Členy	člen	k1gMnPc7	člen
Skupiny	skupina	k1gFnSc2	skupina
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Václav	Václav	k1gMnSc1	Václav
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Filla	Filla	k1gMnSc1	Filla
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Kubín	Kubín	k1gMnSc1	Kubín
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
bratři	bratr	k1gMnPc1	bratr
Čapkové	Čapkové	k2eAgFnSc4d1	Čapkové
Skupinu	skupina	k1gFnSc4	skupina
opustili	opustit	k5eAaPmAgMnP	opustit
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Mánesa	Mánes	k1gMnSc2	Mánes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Tvrdošíjní	tvrdošíjný	k2eAgMnPc1d1	tvrdošíjný
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Kremlička	Kremlička	k1gFnSc1	Kremlička
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Špála	Špála	k1gMnSc1	Špála
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Díla	dílo	k1gNnPc4	dílo
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
===	===	k?	===
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
výstav	výstava	k1gFnPc2	výstava
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Leden	leden	k1gInSc1	leden
1912	[number]	k4	1912
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
výstava	výstava	k1gFnSc1	výstava
Skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Obecním	obecní	k2eAgInSc6d1	obecní
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
díly	díl	k1gInPc7	díl
Matka	matka	k1gFnSc1	matka
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
(	(	kIx(	(
<g/>
Matka	matka	k1gFnSc1	matka
česající	česající	k2eAgNnSc1d1	česající
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
a	a	k8xC	a
Služka	služka	k1gFnSc1	služka
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
se	se	k3xPyFc4	se
nesetkala	setkat	k5eNaPmAgFnS	setkat
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Čapkova	Čapkův	k2eAgMnSc2d1	Čapkův
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
přirovnána	přirovnat	k5eAaPmNgFnS	přirovnat
k	k	k7c3	k
malbám	malba	k1gFnPc3	malba
neolitickým	neolitický	k2eAgFnPc3d1	neolitická
nebo	nebo	k8xC	nebo
dětským	dětský	k2eAgFnPc3d1	dětská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podzim	podzim	k1gInSc1	podzim
1912	[number]	k4	1912
<g/>
:	:	kIx,	:
druhá	druhý	k4xOgFnSc1	druhý
výstava	výstava	k1gFnSc1	výstava
Skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Čapek	Čapek	k1gMnSc1	Čapek
vystavil	vystavit	k5eAaPmAgMnS	vystavit
kubistické	kubistický	k2eAgInPc4d1	kubistický
Přístav	přístav	k1gInSc4	přístav
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
a	a	k8xC	a
Marseille	Marseille	k1gFnSc6	Marseille
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
březen	březen	k1gInSc1	březen
1914	[number]	k4	1914
<g/>
:	:	kIx,	:
osm	osm	k4xCc4	osm
kubistických	kubistický	k2eAgFnPc2d1	kubistická
olejomaleb	olejomalba	k1gFnPc2	olejomalba
na	na	k7c4	na
45	[number]	k4	45
<g/>
.	.	kIx.	.
výstavě	výstava	k1gFnSc6	výstava
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1917	[number]	k4	1917
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
dvojčíslo	dvojčíslo	k1gNnSc1	dvojčíslo
časopisu	časopis	k1gInSc2	časopis
Die	Die	k1gMnPc1	Die
Aktion	Aktion	k1gInSc4	Aktion
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Josefu	Josef	k1gMnSc3	Josef
Čapkovi	Čapkův	k2eAgMnPc1d1	Čapkův
s	s	k7c7	s
reprodukcemi	reprodukce	k1gFnPc7	reprodukce
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
původními	původní	k2eAgInPc7d1	původní
otisky	otisk	k1gInPc7	otisk
jeho	jeho	k3xOp3gInPc2	jeho
linorytů	linoryt	k1gInPc2	linoryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
březen	březen	k1gInSc1	březen
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
ve	v	k7c6	v
Weinertově	Weinertův	k2eAgFnSc6d1	Weinertův
umělecké	umělecký	k2eAgFnSc6d1	umělecká
a	a	k8xC	a
aukční	aukční	k2eAgFnSc6d1	aukční
síni	síň	k1gFnSc6	síň
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
výstava	výstava	k1gFnSc1	výstava
s	s	k7c7	s
názvem	název	k1gInSc7	název
A	a	k9	a
přece	přece	k9	přece
<g/>
!	!	kIx.	!
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
několika	několik	k4yIc2	několik
tvrdošíjných	tvrdošíjný	k2eAgMnPc2d1	tvrdošíjný
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přispěl	přispět	k5eAaPmAgMnS	přispět
31	[number]	k4	31
obrazy	obraz	k1gInPc7	obraz
<g/>
,	,	kIx,	,
uhlokresbami	uhlokresba	k1gFnPc7	uhlokresba
<g/>
,	,	kIx,	,
linoryty	linoryt	k1gInPc7	linoryt
a	a	k8xC	a
litografiemi	litografie	k1gFnPc7	litografie
i	i	k9	i
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
současně	současně	k6eAd1	současně
organizátorem	organizátor	k1gMnSc7	organizátor
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
<g/>
:	:	kIx,	:
čtyři	čtyři	k4xCgMnPc1	čtyři
Tvrdošíjní	tvrdošíjný	k2eAgMnPc1d1	tvrdošíjný
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Špála	Špála	k1gMnSc1	Špála
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
německých	německý	k2eAgNnPc6d1	německé
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgInS	představit
zde	zde	k6eAd1	zde
své	svůj	k3xOyFgInPc4	svůj
nové	nový	k2eAgInPc4d1	nový
obrazy	obraz	k1gInPc4	obraz
malované	malovaný	k2eAgInPc4d1	malovaný
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
civilismu	civilismus	k1gInSc2	civilismus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Předměstské	předměstský	k2eAgNnSc1d1	předměstské
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jaro	jaro	k1gNnSc1	jaro
1923	[number]	k4	1923
<g/>
:	:	kIx,	:
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
Tvrdošíjných	tvrdošíjný	k2eAgFnPc2d1	tvrdošíjná
v	v	k7c6	v
Krasoumné	krasoumný	k2eAgFnSc6d1	Krasoumná
jednotě	jednota	k1gFnSc6	jednota
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Dům	dům	k1gInSc1	dům
umělců	umělec	k1gMnPc2	umělec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
říjen	říjen	k1gInSc1	říjen
1924	[number]	k4	1924
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
výstava	výstava	k1gFnSc1	výstava
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Krasoumné	krasoumný	k2eAgFnSc6d1	Krasoumná
jednotě	jednota	k1gFnSc6	jednota
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
Dům	dům	k1gInSc1	dům
umělců	umělec	k1gMnPc2	umělec
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
šestá	šestý	k4xOgFnSc1	šestý
výstava	výstava	k1gFnSc1	výstava
Tvrdošíjných	tvrdošíjný	k2eAgNnPc2d1	tvrdošíjné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
106	[number]	k4	106
olejů	olej	k1gInPc2	olej
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1910	[number]	k4	1910
až	až	k6eAd1	až
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
116	[number]	k4	116
temper	tempera	k1gFnPc2	tempera
<g/>
,	,	kIx,	,
akvarelů	akvarel	k1gInPc2	akvarel
<g/>
,	,	kIx,	,
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
grafiky	grafika	k1gFnSc2	grafika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
knižních	knižní	k2eAgFnPc2d1	knižní
obálek	obálka	k1gFnPc2	obálka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
byl	být	k5eAaImAgInS	být
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
pražského	pražský	k2eAgInSc2d1	pražský
souboru	soubor	k1gInSc2	soubor
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
pavilónu	pavilón	k1gInSc6	pavilón
Klubu	klub	k1gInSc2	klub
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Musaionu	Musaion	k1gInSc6	Musaion
V.	V.	kA	V.
monografie	monografie	k1gFnSc2	monografie
o	o	k7c4	o
Josefu	Josefa	k1gFnSc4	Josefa
Čapkovi	Čapek	k1gMnSc3	Čapek
s	s	k7c7	s
úvodním	úvodní	k2eAgInSc7d1	úvodní
textem	text	k1gInSc7	text
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Špály	Špála	k1gMnSc2	Špála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
<g/>
:	:	kIx,	:
souborná	souborný	k2eAgFnSc1d1	souborná
výstava	výstava	k1gFnSc1	výstava
díla	dílo	k1gNnSc2	dílo
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
Jízdárně	jízdárna	k1gFnSc6	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
</s>
</p>
<p>
<s>
říjen	říjen	k1gInSc1	říjen
2009	[number]	k4	2009
-	-	kIx~	-
leden	leden	k1gInSc1	leden
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
souborná	souborný	k2eAgFnSc1d1	souborná
výstava	výstava	k1gFnSc1	výstava
díla	dílo	k1gNnSc2	dílo
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
Jízdárně	jízdárna	k1gFnSc6	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
</s>
</p>
<p>
<s>
===	===	k?	===
Umělecký	umělecký	k2eAgInSc4d1	umělecký
vývoj	vývoj	k1gInSc4	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Rané	raný	k2eAgNnSc4d1	rané
období	období	k1gNnSc4	období
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
před	před	k7c7	před
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
jen	jen	k9	jen
málo	málo	k4c1	málo
děl	dělo	k1gNnPc2	dělo
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Portrét	portrét	k1gInSc1	portrét
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
pastel	pastel	k1gInSc1	pastel
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
Trocadéru	Trocadér	k1gInSc6	Trocadér
objevil	objevit	k5eAaPmAgInS	objevit
během	během	k7c2	během
studijního	studijní	k2eAgInSc2d1	studijní
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
umění	umění	k1gNnSc2	umění
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
domorodé	domorodý	k2eAgNnSc1d1	domorodé
umění	umění	k1gNnSc1	umění
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc4	studium
těchto	tento	k3xDgInPc2	tento
uměleckých	umělecký	k2eAgInPc2d1	umělecký
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
teoretické	teoretický	k2eAgNnSc1d1	teoretické
zpracování	zpracování	k1gNnSc1	zpracování
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
velkou	velká	k1gFnSc7	velká
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sochách	socha	k1gFnPc6	socha
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
objevil	objevit	k5eAaPmAgMnS	objevit
hodně	hodně	k6eAd1	hodně
podstatných	podstatný	k2eAgInPc2d1	podstatný
prvků	prvek	k1gInPc2	prvek
pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
tvorbu	tvorba	k1gFnSc4	tvorba
i	i	k9	i
pro	pro	k7c4	pro
povahu	povaha	k1gFnSc4	povaha
umění	umění	k1gNnSc2	umění
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Od	od	k7c2	od
návratu	návrat	k1gInSc2	návrat
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
do	do	k7c2	do
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dílech	díl	k1gInPc6	díl
inspirovaných	inspirovaný	k2eAgInPc6d1	inspirovaný
fauvismem	fauvismus	k1gInSc7	fauvismus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
(	(	kIx(	(
<g/>
Matka	matka	k1gFnSc1	matka
a	a	k8xC	a
Služka	služka	k1gFnSc1	služka
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
s	s	k7c7	s
kubistickými	kubistický	k2eAgInPc7d1	kubistický
pokusy	pokus	k1gInPc7	pokus
(	(	kIx(	(
<g/>
Přístav	přístav	k1gInSc1	přístav
v	v	k7c4	v
Marseille	Marseille	k1gFnPc4	Marseille
a	a	k8xC	a
Marseille	Marseille	k1gFnPc4	Marseille
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
krajinami	krajina	k1gFnPc7	krajina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
a	a	k8xC	a
kubistickými	kubistický	k2eAgFnPc7d1	kubistická
postavami	postava	k1gFnPc7	postava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
obraz	obraz	k1gInSc4	obraz
Novostavba	novostavba	k1gFnSc1	novostavba
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
městské	městský	k2eAgFnSc2d1	městská
periferie	periferie	k1gFnSc2	periferie
<g/>
,	,	kIx,	,
komponované	komponovaný	k2eAgFnSc6d1	komponovaná
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
s	s	k7c7	s
dominantou	dominanta	k1gFnSc7	dominanta
kandelábru	kandelábr	k1gInSc2	kandelábr
s	s	k7c7	s
obloukovou	obloukový	k2eAgFnSc7d1	oblouková
lampou	lampa	k1gFnSc7	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
dílem	díl	k1gInSc7	díl
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
je	být	k5eAaImIp3nS	být
Ženský	ženský	k2eAgInSc4d1	ženský
akt	akt	k1gInSc4	akt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
ženy	žena	k1gFnSc2	žena
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
ve	v	k7c4	v
strunný	strunný	k2eAgInSc4d1	strunný
nástroj	nástroj	k1gInSc4	nástroj
<g/>
;	;	kIx,	;
obraz	obraz	k1gInSc1	obraz
spojuje	spojovat	k5eAaImIp3nS	spojovat
prvky	prvek	k1gInPc4	prvek
analytického	analytický	k2eAgInSc2d1	analytický
a	a	k8xC	a
syntetického	syntetický	k2eAgInSc2d1	syntetický
kubismu	kubismus	k1gInSc2	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Kubistické	kubistický	k2eAgNnSc4d1	kubistické
tvarosloví	tvarosloví	k1gNnSc4	tvarosloví
využíval	využívat	k5eAaPmAgMnS	využívat
s	s	k7c7	s
volností	volnost	k1gFnSc7	volnost
a	a	k8xC	a
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
nevyhýbal	vyhýbat	k5eNaImAgMnS	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
neobratnostem	neobratnost	k1gFnPc3	neobratnost
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
pro	pro	k7c4	pro
naivní	naivní	k2eAgNnSc4d1	naivní
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
objevuje	objevovat	k5eAaImIp3nS	objevovat
nové	nový	k2eAgInPc4d1	nový
tematické	tematický	k2eAgInPc4d1	tematický
okruhy	okruh	k1gInPc4	okruh
-	-	kIx~	-
námořníky	námořník	k1gMnPc4	námořník
a	a	k8xC	a
nevěstky	nevěstka	k1gFnPc4	nevěstka
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Námořník	námořník	k1gMnSc1	námořník
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nevěstka	nevěstka	k1gFnSc1	nevěstka
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
kloboukem	klobouk	k1gInSc7	klobouk
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Dvacátá	dvacátý	k4xOgNnPc4	dvacátý
léta	léto	k1gNnPc4	léto
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
své	svůj	k3xOyFgInPc4	svůj
obrazy	obraz	k1gInPc4	obraz
Piják	piják	k1gMnSc1	piják
<g/>
,	,	kIx,	,
Kuřák	kuřák	k1gMnSc1	kuřák
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
a	a	k8xC	a
Muž	muž	k1gMnSc1	muž
se	s	k7c7	s
zavázanou	zavázaný	k2eAgFnSc7d1	zavázaná
rukou	ruka	k1gFnSc7	ruka
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
výstavě	výstava	k1gFnSc6	výstava
Tvrdošíjných	tvrdošíjný	k2eAgMnPc2d1	tvrdošíjný
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plastické	plastický	k2eAgFnPc4d1	plastická
zjednodušené	zjednodušený	k2eAgFnPc4d1	zjednodušená
postavy	postava	k1gFnPc4	postava
ostrých	ostrý	k2eAgInPc2d1	ostrý
tvarů	tvar	k1gInPc2	tvar
ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Expresívní	expresívní	k2eAgFnPc1d1	expresívní
špičaté	špičatý	k2eAgFnPc1d1	špičatá
formy	forma	k1gFnPc1	forma
a	a	k8xC	a
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
kolorit	kolorit	k1gInSc1	kolorit
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
povahu	povaha	k1gFnSc4	povaha
figur	figura	k1gFnPc2	figura
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
psychologické	psychologický	k2eAgNnSc4d1	psychologické
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
používal	používat	k5eAaImAgInS	používat
také	také	k9	také
zakulacené	zakulacený	k2eAgFnPc4d1	zakulacená
hladké	hladký	k2eAgFnPc4d1	hladká
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výraznou	výrazný	k2eAgFnSc4d1	výrazná
konturu	kontura	k1gFnSc4	kontura
a	a	k8xC	a
prudké	prudký	k2eAgInPc1d1	prudký
světelné	světelný	k2eAgInPc1d1	světelný
kontrasty	kontrast	k1gInPc1	kontrast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
malířské	malířský	k2eAgFnSc6d1	malířská
tvorbě	tvorba	k1gFnSc6	tvorba
projevovat	projevovat	k5eAaImF	projevovat
období	období	k1gNnSc4	období
"	"	kIx"	"
<g/>
chlapů	chlap	k1gMnPc2	chlap
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc4	obraz
Hadráři	hadrář	k1gMnPc1	hadrář
<g/>
,	,	kIx,	,
ukončila	ukončit	k5eAaPmAgFnS	ukončit
je	on	k3xPp3gNnPc4	on
malba	malba	k1gFnSc1	malba
Dřevěný	dřevěný	k2eAgMnSc1d1	dřevěný
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maloval	malovat	k5eAaImAgMnS	malovat
často	často	k6eAd1	často
žebráky	žebrák	k1gMnPc4	žebrák
<g/>
,	,	kIx,	,
lůzu	lůza	k1gFnSc4	lůza
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc4	postava
ze	z	k7c2	z
společenské	společenský	k2eAgFnSc2d1	společenská
spodiny	spodina	k1gFnSc2	spodina
vzbuzující	vzbuzující	k2eAgNnSc4d1	vzbuzující
podezření	podezření	k1gNnSc4	podezření
a	a	k8xC	a
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
výrazovým	výrazový	k2eAgInSc7d1	výrazový
prostředkem	prostředek	k1gInSc7	prostředek
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
stala	stát	k5eAaPmAgFnS	stát
expresívní	expresívní	k2eAgFnSc1d1	expresívní
obrysová	obrysový	k2eAgFnSc1d1	obrysová
čára	čára	k1gFnSc1	čára
a	a	k8xC	a
magické	magický	k2eAgNnSc1d1	magické
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
atmosféru	atmosféra	k1gFnSc4	atmosféra
tajemnosti	tajemnost	k1gFnSc2	tajemnost
a	a	k8xC	a
dramatického	dramatický	k2eAgNnSc2d1	dramatické
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
svou	svůj	k3xOyFgFnSc7	svůj
symbolikou	symbolika	k1gFnSc7	symbolika
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
obsahové	obsahový	k2eAgNnSc1d1	obsahové
vyznění	vyznění	k1gNnSc1	vyznění
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
psal	psát	k5eAaImAgInS	psát
pro	pro	k7c4	pro
dceru	dcera	k1gFnSc4	dcera
Alenku	Alenka	k1gFnSc4	Alenka
Povídání	povídání	k1gNnSc2	povídání
o	o	k7c6	o
pejskovi	pejsek	k1gMnSc6	pejsek
a	a	k8xC	a
kočičce	kočička	k1gFnSc6	kočička
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
malířská	malířský	k2eAgFnSc1d1	malířská
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
od	od	k7c2	od
"	"	kIx"	"
<g/>
chlapů	chlap	k1gMnPc2	chlap
<g/>
"	"	kIx"	"
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
obrazy	obraz	k1gInPc4	obraz
s	s	k7c7	s
dětskými	dětský	k2eAgNnPc7d1	dětské
tématy	téma	k1gNnPc7	téma
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Dva	dva	k4xCgMnPc1	dva
kluci	kluk	k1gMnPc1	kluk
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Děvčátko	děvčátko	k1gNnSc1	děvčátko
s	s	k7c7	s
jahodami	jahoda	k1gFnPc7	jahoda
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dětská	dětský	k2eAgNnPc4d1	dětské
témata	téma	k1gNnPc4	téma
neopouštěl	opouštět	k5eNaImAgMnS	opouštět
ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
Dětské	dětský	k2eAgFnSc2d1	dětská
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Třicátá	třicátý	k4xOgNnPc4	třicátý
léta	léto	k1gNnPc4	léto
====	====	k?	====
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
obrazy	obraz	k1gInPc7	obraz
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
honitby	honitba	k1gFnSc2	honitba
a	a	k8xC	a
myslivců	myslivec	k1gMnPc2	myslivec
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Myslivci	Myslivec	k1gMnPc1	Myslivec
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Olgy	Olga	k1gFnSc2	Olga
Scheinpflugové	Scheinpflugový	k2eAgNnSc1d1	Scheinpflugové
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
namaloval	namalovat	k5eAaPmAgMnS	namalovat
J.	J.	kA	J.
Čapek	Čapek	k1gMnSc1	Čapek
obraz	obraz	k1gInSc1	obraz
Mrak	mrak	k1gInSc1	mrak
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
otevřel	otevřít	k5eAaPmAgMnS	otevřít
nové	nový	k2eAgNnSc4d1	nové
téma	téma	k1gNnSc4	téma
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obraz	obraz	k1gInSc1	obraz
lze	lze	k6eAd1	lze
také	také	k9	také
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k9	jako
předzvěst	předzvěst	k1gFnSc4	předzvěst
budoucího	budoucí	k2eAgInSc2d1	budoucí
vývoje	vývoj	k1gInSc2	vývoj
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
tématu	téma	k1gNnSc2	téma
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
Krajina	Krajina	k1gFnSc1	Krajina
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
a	a	k8xC	a
holuby	holub	k1gMnPc7	holub
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
cyklus	cyklus	k1gInSc1	cyklus
Nocí	noc	k1gFnPc2	noc
s	s	k7c7	s
objímajícími	objímající	k2eAgFnPc7d1	objímající
se	se	k3xPyFc4	se
milenci	milenec	k1gMnPc1	milenec
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
hvězdného	hvězdný	k2eAgNnSc2d1	Hvězdné
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pět	pět	k4xCc4	pět
kompozic	kompozice	k1gFnPc2	kompozice
v	v	k7c6	v
černé	černá	k1gFnSc6	černá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnSc3d1	červená
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgFnSc3d1	hnědá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnSc3d1	zelená
a	a	k8xC	a
modré	modrý	k2eAgFnSc3d1	modrá
dominantě	dominanta	k1gFnSc3	dominanta
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
naplňoval	naplňovat	k5eAaImAgMnS	naplňovat
erotický	erotický	k2eAgInSc4d1	erotický
půvab	půvab	k1gInSc4	půvab
<g/>
,	,	kIx,	,
lyrická	lyrický	k2eAgFnSc1d1	lyrická
snivost	snivost	k1gFnSc1	snivost
a	a	k8xC	a
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Antifašista	antifašista	k1gMnSc1	antifašista
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
reagoval	reagovat	k5eAaBmAgMnS	reagovat
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jiní	jiný	k2eAgMnPc1d1	jiný
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
na	na	k7c4	na
změněnou	změněný	k2eAgFnSc4d1	změněná
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
kresby	kresba	k1gFnSc2	kresba
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
vyšly	vyjít	k5eAaPmAgInP	vyjít
knižně	knižně	k6eAd1	knižně
s	s	k7c7	s
předmluvou	předmluva	k1gFnSc7	předmluva
Josefa	Josef	k1gMnSc2	Josef
Hory	hora	k1gFnSc2	hora
Diktátorské	diktátorský	k2eAgFnSc2d1	diktátorská
boty	bota	k1gFnSc2	bota
–	–	k?	–
kresby	kresba	k1gFnSc2	kresba
otiskované	otiskovaný	k2eAgFnSc2d1	otiskovaná
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
jako	jako	k8xC	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
cyklus	cyklus	k1gInSc1	cyklus
satirických	satirický	k2eAgFnPc2d1	satirická
kreseb	kresba	k1gFnPc2	kresba
Modern	Modern	k1gMnSc1	Modern
times	times	k1gMnSc1	times
<g/>
.	.	kIx.	.
</s>
<s>
Tíživá	tíživý	k2eAgFnSc1d1	tíživá
atmosféra	atmosféra	k1gFnSc1	atmosféra
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
i	i	k9	i
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
Ohníčky	ohníček	k1gInPc1	ohníček
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Těžko	těžko	k6eAd1	těžko
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Krajině	Krajina	k1gFnSc6	Krajina
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
a	a	k8xC	a
holuby	holub	k1gMnPc7	holub
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
visely	viset	k5eAaImAgInP	viset
jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
československého	československý	k2eAgNnSc2d1	Československé
umění	umění	k1gNnSc2	umění
v	v	k7c4	v
londýnské	londýnský	k2eAgInPc4d1	londýnský
Mayor	Mayor	k1gInSc4	Mayor
Gallery	Galler	k1gInPc4	Galler
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
českých	český	k2eAgMnPc2d1	český
malířů	malíř	k1gMnPc2	malíř
Carnegieho	Carnegie	k1gMnSc4	Carnegie
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Pittsburghu	Pittsburgh	k1gInSc6	Pittsburgh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledními	poslední	k2eAgInPc7d1	poslední
malířskými	malířský	k2eAgInPc7d1	malířský
cykly	cyklus	k1gInPc7	cyklus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
byly	být	k5eAaImAgInP	být
cykly	cyklus	k1gInPc1	cyklus
Oheň	oheň	k1gInSc1	oheň
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
Touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čapek	Čapek	k1gMnSc1	Čapek
tak	tak	k6eAd1	tak
s	s	k7c7	s
bolestným	bolestný	k2eAgInSc7d1	bolestný
smutkem	smutek	k1gInSc7	smutek
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
Mnichovské	mnichovský	k2eAgFnSc3d1	Mnichovská
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc4	název
obou	dva	k4xCgInPc2	dva
cyklů	cyklus	k1gInPc2	cyklus
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
až	až	k9	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Umělecké	umělecký	k2eAgFnSc6d1	umělecká
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
obdivuhodně	obdivuhodně	k6eAd1	obdivuhodně
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
olejomaleb	olejomalba	k1gFnPc2	olejomalba
a	a	k8xC	a
přes	přes	k7c4	přes
300	[number]	k4	300
kreseb	kresba	k1gFnPc2	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zamýšleného	zamýšlený	k2eAgInSc2d1	zamýšlený
třetího	třetí	k4xOgInSc2	třetí
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
oslavovat	oslavovat	k5eAaImF	oslavovat
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
nacismem	nacismus	k1gInSc7	nacismus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
jen	jen	k9	jen
náčrt	náčrt	k1gInSc4	náčrt
tužkou	tužka	k1gFnSc7	tužka
<g/>
:	:	kIx,	:
motiv	motiv	k1gInSc1	motiv
kohouta	kohout	k1gMnSc2	kohout
<g/>
,	,	kIx,	,
vítajícího	vítající	k2eAgMnSc2d1	vítající
nový	nový	k2eAgInSc4d1	nový
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnPc1	práce
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Scénografie	scénografie	k1gFnSc1	scénografie
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
jevištní	jevištní	k2eAgMnSc1d1	jevištní
výtvarník	výtvarník	k1gMnSc1	výtvarník
debutoval	debutovat	k5eAaBmAgMnS	debutovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1921	[number]	k4	1921
kostýmní	kostýmní	k2eAgFnSc6d1	kostýmní
výpravou	výprava	k1gFnSc7	výprava
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
R.	R.	kA	R.
U.	U.	kA	U.
R.	R.	kA	R.
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1921	[number]	k4	1921
potom	potom	k6eAd1	potom
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
scénu	scéna	k1gFnSc4	scéna
k	k	k7c3	k
Heibergově	Heibergův	k2eAgFnSc3d1	Heibergův
hře	hra	k1gFnSc3	hra
Tragédie	tragédie	k1gFnSc2	tragédie
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
k	k	k7c3	k
Lenormandovu	Lenormandův	k2eAgNnSc3d1	Lenormandův
Tournée	Tournée	k1gNnSc3	Tournée
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1921	[number]	k4	1921
k	k	k7c3	k
dramatu	drama	k1gNnSc3	drama
Julia	Julius	k1gMnSc2	Julius
Zeyera	Zeyer	k1gMnSc2	Zeyer
Stará	starý	k2eAgFnSc1d1	stará
historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
čtyři	čtyři	k4xCgFnPc4	čtyři
divadelní	divadelní	k2eAgFnPc4d1	divadelní
výpravy	výprava	k1gFnPc4	výprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
šest	šest	k4xCc4	šest
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svou	svůj	k3xOyFgFnSc4	svůj
scénografickou	scénografický	k2eAgFnSc4d1	scénografická
tvorbu	tvorba	k1gFnSc4	tvorba
ukončil	ukončit	k5eAaPmAgInS	ukončit
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
šestapadesát	šestapadesát	k4xCc1	šestapadesát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jevištní	jevištní	k2eAgMnSc1d1	jevištní
výtvarník	výtvarník	k1gMnSc1	výtvarník
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
Národním	národní	k2eAgNnSc7d1	národní
divadlem	divadlo	k1gNnSc7	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
s	s	k7c7	s
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
Státním	státní	k2eAgNnSc7d1	státní
divadlem	divadlo	k1gNnSc7	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
s	s	k7c7	s
Městským	městský	k2eAgNnSc7d1	Městské
divadlem	divadlo	k1gNnSc7	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pražskou	pražský	k2eAgFnSc4d1	Pražská
premiéru	premiéra	k1gFnSc4	premiéra
hry	hra	k1gFnSc2	hra
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
scénu	scéna	k1gFnSc4	scéna
a	a	k8xC	a
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
své	svůj	k3xOyFgNnSc4	svůj
umění	umění	k1gNnSc4	umění
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
zkratky	zkratka	k1gFnSc2	zkratka
a	a	k8xC	a
stylizace	stylizace	k1gFnSc2	stylizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
motýlí	motýlí	k2eAgFnSc2d1	motýlí
<g/>
"	"	kIx"	"
části	část	k1gFnSc2	část
scénu	scéna	k1gFnSc4	scéna
zaplnil	zaplnit	k5eAaPmAgMnS	zaplnit
motivy	motiv	k1gInPc7	motiv
barevných	barevný	k2eAgFnPc2d1	barevná
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
pestrých	pestrý	k2eAgNnPc2d1	pestré
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mravencích	mravenec	k1gMnPc6	mravenec
a	a	k8xC	a
Kořistnících	kořistník	k1gMnPc6	kořistník
pak	pak	k6eAd1	pak
převažovala	převažovat	k5eAaImAgFnS	převažovat
zelená	zelený	k2eAgFnSc1d1	zelená
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
stylizovaná	stylizovaný	k2eAgFnSc1d1	stylizovaná
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
zde	zde	k6eAd1	zde
také	také	k9	také
použil	použít	k5eAaPmAgMnS	použít
kruhový	kruhový	k2eAgInSc4d1	kruhový
horizont	horizont	k1gInSc4	horizont
s	s	k7c7	s
transparentním	transparentní	k2eAgInSc7d1	transparentní
závěsem	závěs	k1gInSc7	závěs
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
promítal	promítat	k5eAaImAgMnS	promítat
barevné	barevný	k2eAgFnPc4d1	barevná
projekce	projekce	k1gFnPc4	projekce
<g/>
:	:	kIx,	:
blesky	blesk	k1gInPc4	blesk
<g/>
,	,	kIx,	,
duhová	duhový	k2eAgNnPc4d1	duhové
spektra	spektrum	k1gNnPc4	spektrum
<g/>
,	,	kIx,	,
černé	černý	k2eAgInPc1d1	černý
kouřící	kouřící	k2eAgInPc1d1	kouřící
komíny	komín	k1gInPc1	komín
<g/>
...	...	k?	...
Výjevy	výjev	k1gInPc1	výjev
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
tak	tak	k6eAd1	tak
vystihoval	vystihovat	k5eAaImAgMnS	vystihovat
a	a	k8xC	a
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Neusiloval	usilovat	k5eNaImAgMnS	usilovat
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
nových	nový	k2eAgInPc2d1	nový
<g/>
,	,	kIx,	,
specificky	specificky	k6eAd1	specificky
jevištních	jevištní	k2eAgInPc2d1	jevištní
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
moderní	moderní	k2eAgFnSc2d1	moderní
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
problémy	problém	k1gInPc4	problém
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
na	na	k7c4	na
scénografii	scénografie	k1gFnSc4	scénografie
<g/>
.	.	kIx.	.
</s>
<s>
Kostýmními	kostýmní	k2eAgInPc7d1	kostýmní
návrhy	návrh	k1gInPc7	návrh
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
vyhraněné	vyhraněný	k2eAgInPc4d1	vyhraněný
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
přesnou	přesný	k2eAgFnSc4d1	přesná
charakteristiku	charakteristika	k1gFnSc4	charakteristika
vyhrotil	vyhrotit	k5eAaPmAgInS	vyhrotit
až	až	k6eAd1	až
do	do	k7c2	do
lehké	lehký	k2eAgFnSc2d1	lehká
karikatury	karikatura	k1gFnSc2	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
kostýmy	kostým	k1gInPc1	kostým
a	a	k8xC	a
masky	maska	k1gFnPc1	maska
hmyzích	hmyzí	k2eAgFnPc2d1	hmyzí
podob	podoba	k1gFnPc2	podoba
plastickými	plastický	k2eAgFnPc7d1	plastická
úpravami	úprava	k1gFnPc7	úprava
měnily	měnit	k5eAaImAgFnP	měnit
hercovu	hercův	k2eAgFnSc4d1	hercova
tvář	tvář	k1gFnSc4	tvář
a	a	k8xC	a
podobu	podoba	k1gFnSc4	podoba
do	do	k7c2	do
groteskních	groteskní	k2eAgInPc2d1	groteskní
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
divadlem	divadlo	k1gNnSc7	divadlo
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
spíše	spíše	k9	spíše
reálná	reálný	k2eAgNnPc4d1	reálné
prostředí	prostředí	k1gNnPc4	prostředí
<g/>
,	,	kIx,	,
moderně	moderně	k6eAd1	moderně
stylizovaná	stylizovaný	k2eAgFnSc1d1	stylizovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návrzích	návrh	k1gInPc6	návrh
pro	pro	k7c4	pro
sociální	sociální	k2eAgNnPc4d1	sociální
dramata	drama	k1gNnPc4	drama
Henri	Henri	k1gNnSc2	Henri
Ghéon	Ghéona	k1gFnPc2	Ghéona
a	a	k8xC	a
Chléb	chléb	k1gInSc1	chléb
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
expresionistických	expresionistický	k2eAgFnPc2d1	expresionistická
zásad	zásada	k1gFnPc2	zásada
držel	držet	k5eAaImAgMnS	držet
určujícího	určující	k2eAgInSc2d1	určující
barevného	barevný	k2eAgInSc2d1	barevný
tónu	tón	k1gInSc2	tón
<g/>
:	:	kIx,	:
prospekt	prospekt	k1gInSc1	prospekt
<g/>
,	,	kIx,	,
kulisy	kulisa	k1gFnPc4	kulisa
<g/>
,	,	kIx,	,
rekvizity	rekvizit	k1gInPc4	rekvizit
i	i	k8xC	i
kostýmy	kostým	k1gInPc4	kostým
odstupňoval	odstupňovat	k5eAaPmAgMnS	odstupňovat
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
odstínů	odstín	k1gInPc2	odstín
barvy	barva	k1gFnSc2	barva
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
pytloviny	pytlovina	k1gFnSc2	pytlovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Aristofanově	Aristofanův	k2eAgInSc6d1	Aristofanův
Ženském	ženský	k2eAgInSc6d1	ženský
sněmu	sněm	k1gInSc6	sněm
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
použil	použít	k5eAaPmAgInS	použít
jasnou	jasný	k2eAgFnSc4d1	jasná
barevnost	barevnost	k1gFnSc4	barevnost
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
rustikální	rustikální	k2eAgFnSc4d1	rustikální
ornamentiku	ornamentika	k1gFnSc4	ornamentika
kostýmů	kostým	k1gInPc2	kostým
<g/>
,	,	kIx,	,
v	v	k7c6	v
Romainsově	Romainsův	k2eAgMnSc6d1	Romainsův
Prostopášníku	prostopášník	k1gMnSc6	prostopášník
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
zase	zase	k9	zase
vycházel	vycházet	k5eAaImAgMnS	vycházet
ze	z	k7c2	z
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Karlem	Karel	k1gMnSc7	Karel
===	===	k?	===
</s>
</p>
<p>
<s>
1910	[number]	k4	1910
Lásky	láska	k1gFnSc2	láska
hra	hra	k1gFnSc1	hra
osudná	osudný	k2eAgFnSc1d1	osudná
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
–	–	k?	–
Jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
,	,	kIx,	,
psáno	psát	k5eAaImNgNnS	psát
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
tiskem	tisk	k1gInSc7	tisk
Otakar	Otakara	k1gFnPc2	Otakara
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
</s>
</p>
<p>
<s>
1913	[number]	k4	1913
Almanach	almanach	k1gInSc1	almanach
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1914	[number]	k4	1914
-	-	kIx~	-
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Událost	událost	k1gFnSc1	událost
a	a	k8xC	a
Vodní	vodní	k2eAgFnSc1d1	vodní
krajina	krajina	k1gFnSc1	krajina
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
kubistickým	kubistický	k2eAgFnPc3d1	kubistická
prózám	próza	k1gFnPc3	próza
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Vydalo	vydat	k5eAaPmAgNnS	vydat
Družstvo	družstvo	k1gNnSc1	družstvo
Přehled	přehled	k1gInSc1	přehled
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1916	[number]	k4	1916
Zářivé	zářivý	k2eAgFnPc4d1	zářivá
hlubiny	hlubina	k1gFnPc4	hlubina
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
prosy	prosa	k1gFnPc4	prosa
-	-	kIx~	-
Sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1918	[number]	k4	1918
Krakonošova	Krakonošův	k2eAgFnSc1d1	Krakonošova
zahrada	zahrada	k1gFnSc1	zahrada
–	–	k?	–
Sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
–	–	k?	–
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
Brno	Brno	k1gNnSc1	Brno
1922	[number]	k4	1922
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
Adam	Adam	k1gMnSc1	Adam
stvořitel	stvořitel	k1gMnSc1	stvořitel
–	–	k?	–
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
psáno	psát	k5eAaImNgNnS	psát
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
</s>
</p>
<p>
<s>
===	===	k?	===
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
1917	[number]	k4	1917
Lélio	Lélio	k1gMnSc1	Lélio
–	–	k?	–
Sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
kniha	kniha	k1gFnSc1	kniha
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
nad	nad	k7c7	nad
nepoznatelností	nepoznatelnost	k1gFnSc7	nepoznatelnost
smyslu	smysl	k1gInSc2	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
Kamilla	Kamilla	k1gFnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
Nejskromnější	skromný	k2eAgInSc4d3	nejskromnější
umění	umění	k1gNnSc1	umění
-	-	kIx~	-
Eseje	esej	k1gFnPc1	esej
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
shrnul	shrnout	k5eAaPmAgMnS	shrnout
úvahy	úvaha	k1gFnPc4	úvaha
<g/>
,	,	kIx,	,
publikované	publikovaný	k2eAgInPc4d1	publikovaný
1918	[number]	k4	1918
až	až	k8xS	až
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
kombinaci	kombinace	k1gFnSc4	kombinace
poetického	poetický	k2eAgInSc2d1	poetický
fejetonu	fejeton	k1gInSc2	fejeton
s	s	k7c7	s
estetickou	estetický	k2eAgFnSc7d1	estetická
esejí	esej	k1gFnSc7	esej
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
např.	např.	kA	např.
o	o	k7c6	o
vývěsních	vývěsní	k2eAgInPc6d1	vývěsní
štítech	štít	k1gInPc6	štít
drobných	drobný	k2eAgMnPc2d1	drobný
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
o	o	k7c6	o
staré	starý	k2eAgFnSc6d1	stará
pohovce	pohovka	k1gFnSc6	pohovka
<g/>
,	,	kIx,	,
podobizně	podobizna	k1gFnSc6	podobizna
stařenky	stařenka	k1gFnSc2	stařenka
Rafaela	Rafael	k1gMnSc2	Rafael
Jörka	Jörek	k1gMnSc2	Jörek
<g/>
.	.	kIx.	.
</s>
<s>
Vydalo	vydat	k5eAaPmAgNnS	vydat
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
Pro	pro	k7c4	pro
delfína	delfín	k1gMnSc4	delfín
-	-	kIx~	-
Sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Reynek	Reynek	k1gMnSc1	Reynek
<g/>
,	,	kIx,	,
Petrkov	Petrkov	k1gInSc1	Petrkov
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
Země	země	k1gFnSc1	země
mnoha	mnoho	k4c2	mnoho
jmen	jméno	k1gNnPc2	jméno
-	-	kIx~	-
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
–	–	k?	–
přebírá	přebírat	k5eAaImIp3nS	přebírat
pesimismus	pesimismus	k1gInSc4	pesimismus
z	z	k7c2	z
Lélia	Lélium	k1gNnSc2	Lélium
<g/>
;	;	kIx,	;
hlavní	hlavní	k2eAgFnSc7d1	hlavní
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
lze	lze	k6eAd1	lze
člověka	člověk	k1gMnSc4	člověk
mravně	mravně	k6eAd1	mravně
obrodit	obrodit	k5eAaPmF	obrodit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
motivu	motiv	k1gInSc2	motiv
Atlantidy	Atlantida	k1gFnSc2	Atlantida
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
řazeno	řazen	k2eAgNnSc1d1	řazeno
do	do	k7c2	do
fantastické	fantastický	k2eAgFnSc2d1	fantastická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
Málo	málo	k1gNnSc1	málo
o	o	k7c6	o
mnohém	mnohé	k1gNnSc6	mnohé
-	-	kIx~	-
Fejeton	fejeton	k1gInSc1	fejeton
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1924	[number]	k4	1924
Umělý	umělý	k2eAgMnSc1d1	umělý
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
Ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
fejeton	fejeton	k1gInSc1	fejeton
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1928	[number]	k4	1928
Ledacos	ledacos	k3yInSc1	ledacos
-	-	kIx~	-
Fejetony	fejeton	k1gInPc4	fejeton
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1929	[number]	k4	1929
Povídání	povídání	k1gNnSc1	povídání
o	o	k7c6	o
pejskovi	pejsek	k1gMnSc6	pejsek
a	a	k8xC	a
kočičce	kočička	k1gFnSc6	kočička
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
spolu	spolu	k6eAd1	spolu
hospodařili	hospodařit	k5eAaImAgMnP	hospodařit
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
o	o	k7c6	o
všelijakých	všelijaký	k3yIgFnPc6	všelijaký
jiných	jiný	k2eAgFnPc6d1	jiná
věcech	věc	k1gFnPc6	věc
-	-	kIx~	-
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
knížka	knížka	k1gFnSc1	knížka
<g/>
,	,	kIx,	,
příhody	příhoda	k1gFnPc1	příhoda
pejska	pejsek	k1gMnSc2	pejsek
a	a	k8xC	a
kočičky	kočička	k1gFnSc2	kočička
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
spolu	spolu	k6eAd1	spolu
žijí	žít	k5eAaImIp3nP	žít
a	a	k8xC	a
hospodaří	hospodařit	k5eAaImIp3nP	hospodařit
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
Stín	stín	k1gInSc1	stín
kapradiny	kapradina	k1gFnSc2	kapradina
-	-	kIx~	-
Povídka	povídka	k1gFnSc1	povídka
-	-	kIx~	-
balada	balada	k1gFnSc1	balada
o	o	k7c6	o
pronásledování	pronásledování	k1gNnSc6	pronásledování
dvou	dva	k4xCgMnPc2	dva
pytláků	pytlák	k1gMnPc2	pytlák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
lesníka	lesník	k1gMnSc4	lesník
a	a	k8xC	a
schovávají	schovávat	k5eAaImIp3nP	schovávat
se	se	k3xPyFc4	se
v	v	k7c6	v
šumavských	šumavský	k2eAgInPc6d1	šumavský
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
zmocňují	zmocňovat	k5eAaImIp3nP	zmocňovat
výčitky	výčitka	k1gFnPc4	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
,	,	kIx,	,
nezapadají	zapadat	k5eNaImIp3nP	zapadat
ani	ani	k8xC	ani
do	do	k7c2	do
světa	svět	k1gInSc2	svět
spořádaných	spořádaný	k2eAgMnPc2d1	spořádaný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
do	do	k7c2	do
světa	svět	k1gInSc2	svět
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
četníky	četník	k1gMnPc7	četník
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
páchá	páchat	k5eAaImIp3nS	páchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
knížce	knížka	k1gFnSc3	knížka
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Vydalo	vydat	k5eAaPmAgNnS	vydat
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
Dobře	dobře	k6eAd1	dobře
to	ten	k3xDgNnSc1	ten
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
aneb	aneb	k?	aneb
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
pradědeček	pradědeček	k1gMnSc1	pradědeček
<g/>
,	,	kIx,	,
lupiči	lupič	k1gMnPc1	lupič
a	a	k8xC	a
detektivové	detektiv	k1gMnPc1	detektiv
-	-	kIx~	-
Dramatizace	dramatizace	k1gFnSc1	dramatizace
pohádky	pohádka	k1gFnSc2	pohádka
O	o	k7c6	o
tlustém	tlustý	k2eAgMnSc6d1	tlustý
pradědečkovi	pradědeček	k1gMnSc6	pradědeček
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
Devatero	devatero	k1gNnSc1	devatero
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Křička	Křička	k1gMnSc1	Křička
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
Kulhavý	kulhavý	k2eAgMnSc1d1	kulhavý
poutník	poutník	k1gMnSc1	poutník
(	(	kIx(	(
<g/>
co	co	k3yInSc1	co
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
uviděl	uvidět	k5eAaPmAgMnS	uvidět
<g/>
)	)	kIx)	)
Esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
filosofické	filosofický	k2eAgFnPc1d1	filosofická
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
Umění	umění	k1gNnSc1	umění
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
-	-	kIx~	-
Autorova	autorův	k2eAgFnSc1d1	autorova
nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
a	a	k8xC	a
významem	význam	k1gInSc7	význam
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
shrnul	shrnout	k5eAaPmAgMnS	shrnout
své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
a	a	k8xC	a
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c6	o
domorodém	domorodý	k2eAgNnSc6d1	domorodé
umění	umění	k1gNnSc6	umění
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
světadílů	světadíl	k1gInPc2	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Obálka	obálka	k1gFnSc1	obálka
a	a	k8xC	a
vazba	vazba	k1gFnSc1	vazba
František	františek	k1gInSc1	františek
Muzika	muzika	k1gFnSc1	muzika
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
Básně	báseň	k1gFnSc2	báseň
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
–	–	k?	–
Básně	báseň	k1gFnSc2	báseň
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
zážitků	zážitek	k1gInPc2	zážitek
i	i	k8xC	i
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
a	a	k8xC	a
upínající	upínající	k2eAgNnSc4d1	upínající
se	se	k3xPyFc4	se
k	k	k7c3	k
existenciální	existenciální	k2eAgFnSc3d1	existenciální
reflexi	reflexe	k1gFnSc3	reflexe
údělu	úděl	k1gInSc2	úděl
člověka	člověk	k1gMnSc2	člověk
vydaného	vydaný	k2eAgMnSc2d1	vydaný
vládě	vláda	k1gFnSc3	vláda
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Verše	verš	k1gInPc1	verš
jsou	být	k5eAaImIp3nP	být
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
optimistickému	optimistický	k2eAgInSc3d1	optimistický
patosu	patos	k1gInSc3	patos
a	a	k8xC	a
rétorice	rétorika	k1gFnSc3	rétorika
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
Psáno	psát	k5eAaImNgNnS	psát
do	do	k7c2	do
mraků	mrak	k1gInPc2	mrak
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
–	–	k?	–
aforismy	aforismus	k1gInPc7	aforismus
<g/>
.	.	kIx.	.
</s>
<s>
Posmrtně	posmrtně	k6eAd1	posmrtně
vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
Povídejme	povídat	k5eAaImRp1nP	povídat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
–	–	k?	–
z	z	k7c2	z
příspěvků	příspěvek	k1gInPc2	příspěvek
psaných	psaný	k2eAgInPc2d1	psaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1929-1933	[number]	k4	1929-1933
pro	pro	k7c4	pro
Dětský	dětský	k2eAgInSc4d1	dětský
koutek	koutek	k1gInSc4	koutek
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
péčí	péče	k1gFnSc7	péče
Miroslava	Miroslav	k1gMnSc2	Miroslav
Halíka	Halík	k1gMnSc2	Halík
</s>
</p>
<p>
<s>
==	==	k?	==
Knižní	knižní	k2eAgFnSc1d1	knižní
grafika	grafika	k1gFnSc1	grafika
==	==	k?	==
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Červen	červen	k1gInSc1	červen
dílo	dílo	k1gNnSc1	dílo
Guillauma	Guillaum	k1gMnSc2	Guillaum
Apollinaira	Apollinairo	k1gNnSc2	Apollinairo
Pásmo	pásmo	k1gNnSc4	pásmo
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapka	k1gMnSc1	Čapka
s	s	k7c7	s
12	[number]	k4	12
linoryty	linoryt	k1gInPc7	linoryt
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
s	s	k7c7	s
15	[number]	k4	15
linoryty	linoryt	k1gInPc7	linoryt
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
F.	F.	kA	F.
Borového	borový	k2eAgInSc2d1	borový
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
J.	J.	kA	J.
Čapek	Čapek	k1gMnSc1	Čapek
také	také	k6eAd1	také
více	hodně	k6eAd2	hodně
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
knižní	knižní	k2eAgInSc1d1	knižní
grafice	grafika	k1gFnSc3	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Reynkem	Reynek	k1gMnSc7	Reynek
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Florianem	Florian	k1gMnSc7	Florian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
knižní	knižní	k2eAgFnSc4d1	knižní
obálku	obálka	k1gFnSc4	obálka
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
k	k	k7c3	k
Neumannově	Neumannův	k2eAgFnSc3d1	Neumannova
sbírce	sbírka	k1gFnSc3	sbírka
Horký	horký	k2eAgInSc1d1	horký
van	van	k1gInSc1	van
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Josefu	Josef	k1gMnSc3	Josef
Čapkovi	Čapek	k1gMnSc3	Čapek
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
padesát	padesát	k4xCc4	padesát
obálek	obálka	k1gFnPc2	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
linorytem	linoryt	k1gInSc7	linoryt
<g/>
,	,	kIx,	,
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
"	"	kIx"	"
<g/>
jen	jen	k9	jen
velké	velký	k2eAgFnPc1d1	velká
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
zabřednouti	zabřednout	k5eAaPmF	zabřednout
do	do	k7c2	do
podřadných	podřadný	k2eAgFnPc2d1	podřadná
ozdůbek	ozdůbka	k1gFnPc2	ozdůbka
a	a	k8xC	a
detailů	detail	k1gInPc2	detail
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mráz	mráz	k1gInSc1	mráz
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
96	[number]	k4	96
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
požaduje	požadovat	k5eAaImIp3nS	požadovat
to	ten	k3xDgNnSc1	ten
nejzákladnější	základní	k2eAgNnSc1d3	nejzákladnější
a	a	k8xC	a
hrubší	hrubý	k2eAgNnSc1d2	hrubší
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
elementárně	elementárně	k6eAd1	elementárně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
podstatě	podstata	k1gFnSc6	podstata
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Čapkova	Čapkův	k2eAgFnSc1d1	Čapkova
"	"	kIx"	"
<g/>
obálkářská	obálkářský	k2eAgFnSc1d1	obálkářský
<g/>
"	"	kIx"	"
tvořivost	tvořivost	k1gFnSc1	tvořivost
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Karla	Karel	k1gMnSc4	Karel
Čapka	Čapek	k1gMnSc4	Čapek
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
linoryty	linoryt	k1gInPc4	linoryt
na	na	k7c4	na
obálky	obálka	k1gFnPc4	obálka
her	hra	k1gFnPc2	hra
Loupežník	loupežník	k1gMnSc1	loupežník
(	(	kIx(	(
<g/>
Aventinum	Aventinum	k1gInSc1	Aventinum
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
R.	R.	kA	R.
U.	U.	kA	U.
R.	R.	kA	R.
(	(	kIx(	(
<g/>
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
knihám	kniha	k1gFnPc3	kniha
Kritika	kritika	k1gFnSc1	kritika
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
poezie	poezie	k1gFnSc1	poezie
nové	nový	k2eAgFnSc2d1	nová
<g />
.	.	kIx.	.
</s>
<s>
doby	doba	k1gFnPc1	doba
v	v	k7c6	v
překladech	překlad	k1gInPc6	překlad
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
sestru	sestra	k1gFnSc4	sestra
Helenu	Helena	k1gFnSc4	Helena
k	k	k7c3	k
vyprávění	vyprávění	k1gNnSc4	vyprávění
Malé	Malé	k2eAgNnSc1d1	Malé
děvče	děvče	k1gNnSc1	děvče
(	(	kIx(	(
<g/>
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marina	k1gFnPc2	Štorch-Marina
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
S.	S.	kA	S.
K.	K.	kA	K.
Neumanna	Neumann	k1gMnSc2	Neumann
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
umění	umění	k1gNnSc6	umění
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
život	život	k1gInSc4	život
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
k	k	k7c3	k
básním	báseň	k1gFnPc3	báseň
Josefa	Josef	k1gMnSc2	Josef
Nemasty	Nemast	k1gInPc4	Nemast
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Němce	Němec	k1gMnSc4	Němec
a	a	k8xC	a
Hugo	Hugo	k1gMnSc1	Hugo
Sonnenscheina	Sonnenscheina	k1gMnSc1	Sonnenscheina
<g/>
,	,	kIx,	,
prózám	próza	k1gFnPc3	próza
Francise	Francise	k1gFnSc2	Francise
Jammese	Jammese	k1gFnSc2	Jammese
<g/>
,	,	kIx,	,
Marie	Maria	k1gFnSc2	Maria
Pujmanové	Pujmanová	k1gFnSc2	Pujmanová
a	a	k8xC	a
Julese	Julese	k1gFnSc2	Julese
Romainse	Romainse	k1gFnSc2	Romainse
atd.	atd.	kA	atd.
Každá	každý	k3xTgFnSc1	každý
jeho	jeho	k3xOp3gFnSc1	jeho
obálka	obálka	k1gFnSc1	obálka
byla	být	k5eAaImAgFnS	být
tvarově	tvarově	k6eAd1	tvarově
a	a	k8xC	a
barevně	barevně	k6eAd1	barevně
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
originál	originál	k1gInSc4	originál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
příznačným	příznačný	k2eAgInSc7d1	příznačný
způsobem	způsob	k1gInSc7	způsob
uváděl	uvádět	k5eAaImAgMnS	uvádět
danou	daný	k2eAgFnSc4d1	daná
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Aventinum	Aventinum	k1gInSc4	Aventinum
hru	hra	k1gFnSc4	hra
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
obálkou	obálka	k1gFnSc7	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Vyšly	vyjít	k5eAaPmAgFnP	vyjít
i	i	k9	i
další	další	k2eAgFnPc1d1	další
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
zpracování	zpracování	k1gNnSc6	zpracování
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
<g/>
:	:	kIx,	:
nejčastěji	často	k6eAd3	často
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
novely	novela	k1gFnSc2	novela
jeho	jeho	k3xOp3gMnSc2	jeho
bratra	bratr	k1gMnSc2	bratr
Karla	Karel	k1gMnSc2	Karel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
obálky	obálka	k1gFnPc4	obálka
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
autory	autor	k1gMnPc4	autor
Aventina	Aventin	k2eAgFnSc1d1	Aventin
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
knižních	knižní	k2eAgFnPc6d1	knižní
ilustracích	ilustrace	k1gFnPc6	ilustrace
převládat	převládat	k5eAaImF	převládat
kresby	kresba	k1gFnPc4	kresba
nad	nad	k7c7	nad
linoryty	linoryt	k1gInPc7	linoryt
<g/>
.	.	kIx.	.
</s>
<s>
Kresby	kresba	k1gFnPc1	kresba
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
hodily	hodit	k5eAaImAgFnP	hodit
k	k	k7c3	k
fejetonu	fejeton	k1gInSc3	fejeton
<g/>
,	,	kIx,	,
epické	epický	k2eAgFnSc6d1	epická
próze	próza	k1gFnSc6	próza
nebo	nebo	k8xC	nebo
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
<g/>
,	,	kIx,	,
základ	základ	k1gInSc4	základ
tvořila	tvořit	k5eAaImAgFnS	tvořit
kontura	kontura	k1gFnSc1	kontura
<g/>
,	,	kIx,	,
prostý	prostý	k2eAgInSc1d1	prostý
obrys	obrys	k1gInSc1	obrys
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
Čapka	Čapek	k1gMnSc2	Čapek
značnou	značný	k2eAgFnSc4d1	značná
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
<g/>
:	:	kIx,	:
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
rozpačitě	rozpačitě	k6eAd1	rozpačitě
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
,	,	kIx,	,
dynamický	dynamický	k2eAgInSc4d1	dynamický
<g/>
,	,	kIx,	,
prudký	prudký	k2eAgInSc4d1	prudký
<g/>
,	,	kIx,	,
klidný	klidný	k2eAgInSc4d1	klidný
<g/>
,	,	kIx,	,
ostrý	ostrý	k2eAgInSc4d1	ostrý
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příklon	příklon	k1gInSc1	příklon
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
knižní	knižní	k2eAgFnSc6d1	knižní
grafice	grafika	k1gFnSc6	grafika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
nejčastěji	často	k6eAd3	často
knihy	kniha	k1gFnSc2	kniha
pro	pro	k7c4	pro
nejmenší	malý	k2eAgMnPc4d3	nejmenší
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
:	:	kIx,	:
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Žabákova	žabákův	k2eAgNnSc2d1	žabákův
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
–	–	k?	–
Kenneth	Kenneth	k1gMnSc1	Kenneth
Graham	Graham	k1gMnSc1	Graham
</s>
</p>
<p>
<s>
Edudant	Edudant	k1gMnSc1	Edudant
a	a	k8xC	a
Francimor	Francimor	k1gMnSc1	Francimor
–	–	k?	–
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Poláček	Poláček	k1gMnSc1	Poláček
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kluci	kluk	k1gMnPc1	kluk
<g/>
,	,	kIx,	,
hurá	hurá	k0	hurá
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Poplach	poplach	k1gInSc1	poplach
v	v	k7c6	v
Kovářské	kovářský	k2eAgFnSc6d1	Kovářská
uličce	ulička	k1gFnSc6	ulička
–	–	k?	–
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Řezáč	Řezáč	k1gMnSc1	Řezáč
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Adolf	Adolf	k1gMnSc1	Adolf
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
Vtipnými	vtipný	k2eAgFnPc7d1	vtipná
kresbami	kresba	k1gFnPc7	kresba
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
také	také	k9	také
knihy	kniha	k1gFnPc1	kniha
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zahradníkův	Zahradníkův	k2eAgInSc1d1	Zahradníkův
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Minda	Minda	k1gFnSc1	Minda
čili	čili	k8xC	čili
O	o	k7c6	o
chovu	chov	k1gInSc6	chov
psů	pes	k1gMnPc2	pes
(	(	kIx(	(
<g/>
Spolek	spolek	k1gInSc1	spolek
českých	český	k2eAgMnPc2d1	český
bibliofilů	bibliofil	k1gMnPc2	bibliofil
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
náměty	námět	k1gInPc4	námět
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
řada	řada	k1gFnSc1	řada
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc4	tři
televizní	televizní	k2eAgMnPc4d1	televizní
inscenace	inscenace	k1gFnSc2	inscenace
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
<g/>
:	:	kIx,	:
O	o	k7c6	o
pyšné	pyšný	k2eAgFnSc6d1	pyšná
noční	noční	k2eAgFnSc6d1	noční
košilce	košilka	k1gFnSc6	košilka
<g/>
,	,	kIx,	,
Jak	jak	k8xS	jak
pejsek	pejsek	k1gMnSc1	pejsek
s	s	k7c7	s
kočičkou	kočička	k1gFnSc7	kočička
myli	mýt	k5eAaImAgMnP	mýt
podlahu	podlaha	k1gFnSc4	podlaha
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Eduard	Eduard	k1gMnSc1	Eduard
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
<g/>
:	:	kIx,	:
Jak	jak	k8xC	jak
si	se	k3xPyFc3	se
pejsek	pejsek	k1gMnSc1	pejsek
roztrhl	roztrhnout	k5eAaPmAgMnS	roztrhnout
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
si	se	k3xPyFc3	se
pejsek	pejsek	k1gMnSc1	pejsek
s	s	k7c7	s
kočičkou	kočička	k1gFnSc7	kočička
dělali	dělat	k5eAaImAgMnP	dělat
dort	dort	k1gInSc4	dort
,	,	kIx,	,
O	o	k7c6	o
panence	panenka	k1gFnSc6	panenka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tence	tenko	k6eAd1	tenko
plakala	plakat	k5eAaImAgFnS	plakat
<g/>
,	,	kIx,	,
O	o	k7c6	o
pejskovi	pejsek	k1gMnSc6	pejsek
a	a	k8xC	a
kočičce	kočička	k1gFnSc6	kočička
jak	jak	k6eAd1	jak
psali	psát	k5eAaImAgMnP	psát
psaní	psaní	k1gNnSc4	psaní
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Eduard	Eduard	k1gMnSc1	Eduard
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
svět	svět	k1gInSc1	svět
zařízen	zařízen	k2eAgInSc1d1	zařízen
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Eduard	Eduard	k1gMnSc1	Eduard
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
<g/>
:	:	kIx,	:
Hrajeme	hrát	k5eAaImIp1nP	hrát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
Už	už	k9	už
je	být	k5eAaImIp3nS	být
ráno	ráno	k6eAd1	ráno
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Eduard	Eduard	k1gMnSc1	Eduard
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
O	o	k7c6	o
tlustém	tlustý	k2eAgMnSc6d1	tlustý
pradědečkovi	pradědeček	k1gMnSc6	pradědeček
(	(	kIx(	(
<g/>
TV	TV	kA	TV
inscenace	inscenace	k1gFnSc1	inscenace
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Pavel	Pavel	k1gMnSc1	Pavel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pešek	Pešek	k1gMnSc1	Pešek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
TV	TV	kA	TV
inscenace	inscenace	k1gFnSc1	inscenace
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Matějovský	Matějovský	k1gMnSc1	Matějovský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
pejsek	pejsek	k1gMnSc1	pejsek
s	s	k7c7	s
kočičkou	kočička	k1gFnSc7	kočička
myli	mýt	k5eAaImAgMnP	mýt
podlahu	podlaha	k1gFnSc4	podlaha
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Я	Я	k?	Я
п	п	k?	п
і	і	k?	і
к	к	k?	к
м	м	k?	м
п	п	k?	п
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alla	Alla	k1gFnSc1	Alla
Gračovová	Gračovová	k1gFnSc1	Gračovová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
pradědeček	pradědeček	k1gMnSc1	pradědeček
(	(	kIx(	(
<g/>
TV	TV	kA	TV
inscenace	inscenace	k1gFnSc1	inscenace
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Věra	Věra	k1gFnSc1	Věra
Jordánová	Jordánová	k1gFnSc1	Jordánová
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Stín	stín	k1gInSc1	stín
kapradiny	kapradina	k1gFnSc2	kapradina
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
<g/>
)	)	kIx)	)
<g/>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Musil	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Mrkvička	mrkvička	k1gFnSc1	mrkvička
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgInP	použít
dokumentární	dokumentární	k2eAgInPc1d1	dokumentární
záběry	záběr	k1gInPc1	záběr
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Čapka	Čapka	k1gMnSc1	Čapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Člověk	člověk	k1gMnSc1	člověk
proti	proti	k7c3	proti
zkáze	zkáza	k1gFnSc3	zkáza
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Štěpán	Štěpán	k1gMnSc1	Štěpán
Skalský	Skalský	k1gMnSc1	Skalský
<g/>
)	)	kIx)	)
hrál	hrát	k5eAaImAgMnS	hrát
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapka	k1gMnSc1	Čapka
František	František	k1gMnSc1	František
Řehák	Řehák	k1gMnSc1	Řehák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
dosvědčil	dosvědčit	k5eAaPmAgMnS	dosvědčit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
pravým	pravý	k2eAgMnSc7d1	pravý
autorem	autor	k1gMnSc7	autor
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
robot	robot	k1gInSc1	robot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
používaných	používaný	k2eAgInPc2d1	používaný
výrazů	výraz	k1gInPc2	výraz
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
moderních	moderní	k2eAgFnPc2d1	moderní
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
tento	tento	k3xDgInSc4	tento
výraz	výraz	k1gInSc4	výraz
původně	původně	k6eAd1	původně
použil	použít	k5eAaPmAgInS	použít
pro	pro	k7c4	pro
uměle	uměle	k6eAd1	uměle
vytvořeného	vytvořený	k2eAgMnSc4d1	vytvořený
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hře	hra	k1gFnSc6	hra
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
67	[number]	k4	67
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
/	/	kIx~	/
redakce	redakce	k1gFnSc2	redakce
Otakar	Otakar	k1gMnSc1	Otakar
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
57	[number]	k4	57
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
/	/	kIx~	/
hlavní	hlavní	k2eAgMnSc1d1	hlavní
redaktor	redaktor	k1gMnSc1	redaktor
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Victoria	Victorium	k1gNnSc2	Victorium
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85865	[number]	k4	85865
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
612	[number]	k4	612
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OPELÍK	OPELÍK	kA	OPELÍK
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triáda	triáda	k1gFnSc1	triáda
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
344	[number]	k4	344
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
<g/>
–	–	k?	–
<g/>
G.	G.	kA	G.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
900	[number]	k4	900
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
797	[number]	k4	797
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
378	[number]	k4	378
<g/>
–	–	k?	–
<g/>
381	[number]	k4	381
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠEBOROVÁ	Šeborová	k1gFnSc1	Šeborová
<g/>
,	,	kIx,	,
Silvie	Silvie	k1gFnSc1	Silvie
<g/>
.	.	kIx.	.
</s>
<s>
Ostýchavý	ostýchavý	k2eAgMnSc1d1	ostýchavý
ctitel	ctitel	k1gMnSc1	ctitel
<g/>
.	.	kIx.	.
</s>
<s>
Živá	živý	k2eAgFnSc1d1	živá
historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
</s>
<s>
Červenec-srpen	Červenecrpen	k2eAgMnSc1d1	Červenec-srpen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s.	s.	k?	s.
66	[number]	k4	66
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠULCOVÁ	Šulcová	k1gFnSc1	Šulcová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Čapci	Čapek	k1gMnPc1	Čapek
<g/>
,	,	kIx,	,
Ladění	laděný	k2eAgMnPc1d1	laděný
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
Poločas	poločas	k1gInSc4	poločas
nadějí	naděje	k1gFnPc2	naděje
<g/>
,	,	kIx,	,
Brána	brána	k1gFnSc1	brána
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gInSc1	Melantrich
1993-98	[number]	k4	1993-98
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
267	[number]	k4	267
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
279	[number]	k4	279
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
165	[number]	k4	165
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
80-7023--245-5	[number]	k4	80-7023--245-5
</s>
</p>
<p>
<s>
ŠULCOVÁ	Šulcová	k1gFnSc1	Šulcová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužený	prodloužený	k2eAgInSc1d1	prodloužený
čas	čas	k1gInSc1	čas
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	paseka	k1gFnSc1	paseka
2000	[number]	k4	2000
ISBN	ISBN	kA	ISBN
80-7185-332-1	[number]	k4	80-7185-332-1
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
182	[number]	k4	182
<g/>
–	–	k?	–
<g/>
183	[number]	k4	183
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VALTROVÁ	Valtrová	k1gFnSc1	Valtrová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
rodu	rod	k1gInSc2	rod
Hrušínských	Hrušínský	k2eAgFnPc2d1	Hrušínská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
69	[number]	k4	69
<g/>
,	,	kIx,	,
260	[number]	k4	260
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-0485-X	[number]	k4	80-207-0485-X
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
10	[number]	k4	10
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Č	Č	kA	Č
<g/>
–	–	k?	–
<g/>
Čerma	Čerma	k1gFnSc1	Čerma
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
503	[number]	k4	503
<g/>
–	–	k?	–
<g/>
606	[number]	k4	606
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
367	[number]	k4	367
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
524	[number]	k4	524
<g/>
–	–	k?	–
<g/>
528	[number]	k4	528
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
THIELE	THIELE	kA	THIELE
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
a	a	k8xC	a
kniha	kniha	k1gFnSc1	kniha
:	:	kIx,	:
září	září	k1gNnSc1	září
-	-	kIx~	-
říjen	říjen	k1gInSc1	říjen
1950	[number]	k4	1950
/	/	kIx~	/
báseň	báseň	k1gFnSc1	báseň
Nad	nad	k7c7	nad
knižními	knižní	k2eAgFnPc7d1	knižní
obálkami	obálka	k1gFnPc7	obálka
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
napsal	napsat	k5eAaBmAgMnS	napsat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Thiele	Thiel	k1gInSc2	Thiel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
33	[number]	k4	33
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
</s>
</p>
<p>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Bratři	bratr	k1gMnPc1	bratr
Čapkové	Čapková	k1gFnSc2	Čapková
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josef	k1gMnSc1	Josef
Čapek	čapka	k1gFnPc2	čapka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Wikilivres	Wikilivres	k1gMnSc1	Wikilivres
<g/>
:	:	kIx,	:
<g/>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
:	:	kIx,	:
díla	dílo	k1gNnPc4	dílo
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Wikilivres	Wikilivresa	k1gFnPc2	Wikilivresa
</s>
</p>
<p>
<s>
Digitalizovaná	digitalizovaný	k2eAgNnPc1d1	digitalizované
díla	dílo	k1gNnPc1	dílo
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
NK	NK	kA	NK
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
–	–	k?	–
Grafika	grafika	k1gFnSc1	grafika
<g/>
,	,	kIx,	,
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
galerieart	galerieart	k1gInSc1	galerieart
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
</s>
</p>
<p>
</p>
