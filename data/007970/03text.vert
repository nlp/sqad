<s>
Bakteriofág	bakteriofág	k1gInSc1	bakteriofág
(	(	kIx(	(
<g/>
odvozený	odvozený	k2eAgMnSc1d1	odvozený
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
fagein	fagein	k1gInSc1	fagein
–	–	k?	–
řecky	řecky	k6eAd1	řecky
φ	φ	k?	φ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jíst	jíst	k5eAaImF	jíst
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
fág	fág	k1gInSc1	fág
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obecný	obecný	k2eAgInSc4d1	obecný
název	název	k1gInSc4	název
pro	pro	k7c4	pro
virus	virus	k1gInSc4	virus
infikující	infikující	k2eAgFnSc2d1	infikující
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriofágy	bakteriofág	k1gInPc1	bakteriofág
jsou	být	k5eAaImIp3nP	být
nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
biologickým	biologický	k2eAgInSc7d1	biologický
objektem	objekt	k1gInSc7	objekt
v	v	k7c6	v
biosféře	biosféra	k1gFnSc6	biosféra
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1030	[number]	k4	1030
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
osídlených	osídlený	k2eAgFnPc2d1	osídlená
jejich	jejich	k3xOp3gMnPc7	jejich
bakteriálními	bakteriální	k2eAgMnPc7d1	bakteriální
hostiteli	hostitel	k1gMnPc7	hostitel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
půda	půda	k1gFnSc1	půda
nebo	nebo	k8xC	nebo
střeva	střevo	k1gNnPc4	střevo
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejkoncentrovanějších	koncentrovaný	k2eAgNnPc2d3	nejkoncentrovanější
nalezišť	naleziště	k1gNnPc2	naleziště
fágů	fág	k1gInPc2	fág
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
virů	vir	k1gInPc2	vir
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
až	až	k9	až
9	[number]	k4	9
<g/>
×	×	k?	×
<g/>
108	[number]	k4	108
virionů	virion	k1gInPc2	virion
na	na	k7c4	na
mililitr	mililitr	k1gInSc4	mililitr
a	a	k8xC	a
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
mořských	mořský	k2eAgFnPc2d1	mořská
bakterií	bakterie	k1gFnPc2	bakterie
jimi	on	k3xPp3gInPc7	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
infikováno	infikovat	k5eAaBmNgNnS	infikovat
<g/>
.	.	kIx.	.
</s>
<s>
Fágy	fág	k1gInPc1	fág
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
neživé	živý	k2eNgInPc4d1	neživý
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemají	mít	k5eNaImIp3nP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
metabolismus	metabolismus	k1gInSc4	metabolismus
<g/>
,	,	kIx,	,
nereagují	reagovat	k5eNaBmIp3nP	reagovat
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgNnSc4	svůj
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
využívají	využívat	k5eAaPmIp3nP	využívat
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
živé	živá	k1gFnPc4	živá
nebuněčné	buněčný	k2eNgFnSc2d1	nebuněčná
entity	entita	k1gFnSc2	entita
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriofágy	bakteriofág	k1gInPc1	bakteriofág
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
20	[number]	k4	20
a	a	k8xC	a
200	[number]	k4	200
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
stavba	stavba	k1gFnSc1	stavba
fágů	fág	k1gInPc2	fág
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
bílkovinnou	bílkovinný	k2eAgFnSc4d1	bílkovinná
kapsidu	kapsida	k1gFnSc4	kapsida
a	a	k8xC	a
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kapsidu	kapsida	k1gFnSc4	kapsida
krčkem	krček	k1gInSc7	krček
navazuje	navazovat	k5eAaImIp3nS	navazovat
bičík	bičík	k1gInSc1	bičík
s	s	k7c7	s
bičíkovou	bičíkův	k2eAgFnSc7d1	bičíkův
pochvou	pochva	k1gFnSc7	pochva
<g/>
,	,	kIx,	,
bazální	bazální	k2eAgFnSc7d1	bazální
destičkou	destička	k1gFnSc7	destička
a	a	k8xC	a
vlákny	vlákna	k1gFnSc2	vlákna
bičíku	bičík	k1gInSc2	bičík
sloužícími	sloužící	k2eAgInPc7d1	sloužící
k	k	k7c3	k
přichycení	přichycení	k1gNnSc3	přichycení
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Genetický	genetický	k2eAgInSc1d1	genetický
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
DNA	dno	k1gNnSc2	dno
nebo	nebo	k8xC	nebo
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jednovláknová	jednovláknový	k2eAgFnSc1d1	jednovláknová
(	(	kIx(	(
<g/>
ssDNA	ssDNA	k?	ssDNA
-	-	kIx~	-
angl.	angl.	k?	angl.
single-stranded	singletranded	k1gInSc1	single-stranded
<g/>
)	)	kIx)	)
i	i	k9	i
dvouvláknová	dvouvláknový	k2eAgFnSc1d1	dvouvláknová
(	(	kIx(	(
<g/>
dsDNA	dsDNA	k?	dsDNA
-	-	kIx~	-
double-stranded	doubletranded	k1gMnSc1	double-stranded
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kružnicová	kružnicový	k2eAgFnSc1d1	kružnicová
i	i	k8xC	i
lineární	lineární	k2eAgFnSc1d1	lineární
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
5000	[number]	k4	5000
až	až	k9	až
500	[number]	k4	500
000	[number]	k4	000
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
60	[number]	k4	60
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
fágy	fág	k1gInPc1	fág
užívány	užíván	k2eAgInPc1d1	užíván
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
penicilinu	penicilin	k1gInSc2	penicilin
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
zavedení	zavedení	k1gNnSc4	zavedení
do	do	k7c2	do
klinické	klinický	k2eAgFnSc2d1	klinická
praxe	praxe	k1gFnSc2	praxe
se	se	k3xPyFc4	se
však	však	k9	však
pozornost	pozornost	k1gFnSc1	pozornost
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
a	a	k8xC	a
fágy	fág	k1gInPc1	fág
ustoupily	ustoupit	k5eAaPmAgInP	ustoupit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
jedině	jedině	k6eAd1	jedině
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Eliavově	Eliavův	k2eAgInSc6d1	Eliavův
institutu	institut	k1gInSc6	institut
v	v	k7c6	v
gruzínském	gruzínský	k2eAgNnSc6d1	gruzínské
Tbilisi	Tbilisi	k1gNnSc6	Tbilisi
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
bakteriofágové	bakteriofágový	k2eAgInPc4d1	bakteriofágový
preparáty	preparát	k1gInPc4	preparát
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
však	však	k9	však
fágy	fág	k1gInPc4	fág
zažívají	zažívat	k5eAaImIp3nP	zažívat
celosvětový	celosvětový	k2eAgMnSc1d1	celosvětový
"	"	kIx"	"
<g/>
comeback	comeback	k1gMnSc1	comeback
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
neustále	neustále	k6eAd1	neustále
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
počtu	počet	k1gInSc3	počet
multirezistentních	multirezistentní	k2eAgInPc2d1	multirezistentní
kmenů	kmen	k1gInPc2	kmen
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
,	,	kIx,	,
Pseudomonas	Pseudomonas	k1gMnSc1	Pseudomonas
aeruginosa	aeruginosa	k1gFnSc1	aeruginosa
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
si	se	k3xPyFc3	se
chemik	chemik	k1gMnSc1	chemik
E.	E.	kA	E.
H.	H.	kA	H.
Hankin	Hankin	k1gMnSc1	Hankin
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nějaká	nějaký	k3yIgFnSc1	nějaký
látka	látka	k1gFnSc1	látka
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Ganze	Ganga	k1gFnSc6	Ganga
ničí	ničit	k5eAaImIp3nP	ničit
bakterii	bakterie	k1gFnSc4	bakterie
Vibrio	vibrio	k1gNnSc4	vibrio
cholerae	cholerae	k6eAd1	cholerae
způsobující	způsobující	k2eAgFnSc4d1	způsobující
choleru	cholera	k1gFnSc4	cholera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
pak	pak	k6eAd1	pak
britský	britský	k2eAgMnSc1d1	britský
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
Frederick	Frederick	k1gMnSc1	Frederick
W.	W.	kA	W.
Twort	Twort	k1gInSc1	Twort
izoloval	izolovat	k5eAaBmAgInS	izolovat
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
nazvány	nazván	k2eAgInPc1d1	nazván
bakteriofágy	bakteriofág	k1gInPc1	bakteriofág
-	-	kIx~	-
totiž	totiž	k9	totiž
"	"	kIx"	"
<g/>
požírači	požírač	k1gMnPc1	požírač
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Tworta	Twort	k1gMnSc2	Twort
se	se	k3xPyFc4	se
na	na	k7c6	na
objevu	objev	k1gInSc6	objev
bakteriofágů	bakteriofág	k1gInPc2	bakteriofág
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
francouzsko-kanadský	francouzskoanadský	k2eAgMnSc1d1	francouzsko-kanadský
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
Félix	Félix	k1gInSc1	Félix
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Herelle	Herell	k1gMnSc2	Herell
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
fágy	fág	k1gInPc4	fág
izoloval	izolovat	k5eAaBmAgMnS	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
úspěšně	úspěšně	k6eAd1	úspěšně
použil	použít	k5eAaPmAgMnS	použít
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
úplavice	úplavice	k1gFnSc2	úplavice
(	(	kIx(	(
<g/>
právě	právě	k6eAd1	právě
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
termínu	termín	k1gInSc2	termín
bakteriofág	bakteriofág	k1gInSc1	bakteriofág
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriofágy	bakteriofág	k1gInPc1	bakteriofág
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
lyzovat	lyzovat	k5eAaPmF	lyzovat
napadené	napadený	k2eAgFnPc1d1	napadená
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
proto	proto	k8xC	proto
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
účinná	účinný	k2eAgNnPc4d1	účinné
antibakteriální	antibakteriální	k2eAgNnPc4d1	antibakteriální
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
druh	druh	k1gInSc4	druh
bakteriofága	bakteriofága	k1gFnSc1	bakteriofága
napadá	napadat	k5eAaImIp3nS	napadat
jen	jen	k9	jen
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
druh	druh	k1gInSc4	druh
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
však	však	k9	však
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
léčba	léčba	k1gFnSc1	léčba
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
bakteriální	bakteriální	k2eAgInSc4d1	bakteriální
druh	druh	k1gInSc4	druh
a	a	k8xC	a
kmen	kmen	k1gInSc4	kmen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
podat	podat	k5eAaPmF	podat
pacientovi	pacient	k1gMnSc3	pacient
přesně	přesně	k6eAd1	přesně
vybrané	vybraný	k2eAgInPc4d1	vybraný
bakteriofágy	bakteriofág	k1gInPc4	bakteriofág
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
pomocí	pomocí	k7c2	pomocí
bakteriofágů	bakteriofág	k1gInPc2	bakteriofág
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
časově	časově	k6eAd1	časově
náročnější	náročný	k2eAgMnSc1d2	náročnější
než	než	k8xS	než
např.	např.	kA	např.
použití	použití	k1gNnSc1	použití
širokospektrálních	širokospektrální	k2eAgNnPc2d1	širokospektrální
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
již	již	k9	již
mnoho	mnoho	k4c4	mnoho
bakteriálních	bakteriální	k2eAgInPc2d1	bakteriální
kmenů	kmen	k1gInPc2	kmen
rezistentních	rezistentní	k2eAgInPc2d1	rezistentní
vůči	vůči	k7c3	vůči
nejméně	málo	k6eAd3	málo
jednomu	jeden	k4xCgNnSc3	jeden
antibiotiku	antibiotikum	k1gNnSc3	antibiotikum
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc4	některý
nemocniční	nemocniční	k2eAgInPc4d1	nemocniční
kmeny	kmen	k1gInPc4	kmen
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
stafylokok	stafylokok	k1gInSc1	stafylokok
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rezistentní	rezistentní	k2eAgMnPc1d1	rezistentní
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgFnPc3	všecek
běžně	běžně	k6eAd1	běžně
používaným	používaný	k2eAgNnPc3d1	používané
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
včetně	včetně	k7c2	včetně
vankomycinu	vankomycin	k1gInSc6	vankomycin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
antibiotikem	antibiotikum	k1gNnSc7	antibiotikum
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
u	u	k7c2	u
komplikovaných	komplikovaný	k2eAgFnPc2d1	komplikovaná
infekcí	infekce	k1gFnPc2	infekce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
fágy	fág	k1gInPc1	fág
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
jako	jako	k8xS	jako
lepší	dobrý	k2eAgFnSc1d2	lepší
varianta	varianta	k1gFnSc1	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Fágová	Fágový	k2eAgFnSc1d1	Fágový
terapie	terapie	k1gFnSc1	terapie
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
mnoho	mnoho	k4c1	mnoho
velkých	velký	k2eAgFnPc2d1	velká
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
Bakterie	bakterie	k1gFnSc1	bakterie
si	se	k3xPyFc3	se
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
horizontu	horizont	k1gInSc6	horizont
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
rezistenci	rezistence	k1gFnSc4	rezistence
k	k	k7c3	k
bakteriofágům	bakteriofág	k1gInPc3	bakteriofág
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
fágy	fág	k1gInPc1	fág
taktéž	taktéž	k?	taktéž
mutují	mutovat	k5eAaImIp3nP	mutovat
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
mutací	mutace	k1gFnPc2	mutace
pomohou	pomoct	k5eAaPmIp3nP	pomoct
překonat	překonat	k5eAaPmF	překonat
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
rezistenci	rezistence	k1gFnSc4	rezistence
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
nekončící	končící	k2eNgInSc1d1	nekončící
závod	závod	k1gInSc1	závod
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
)	)	kIx)	)
Fágy	fág	k1gInPc1	fág
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
specifické	specifický	k2eAgInPc4d1	specifický
<g/>
,	,	kIx,	,
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
fág	fág	k1gInSc4	fág
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
úzká	úzký	k2eAgFnSc1d1	úzká
skupina	skupina	k1gFnSc1	skupina
fágů	fág	k1gInPc2	fág
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
infikovat	infikovat	k5eAaBmF	infikovat
pouze	pouze	k6eAd1	pouze
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
bakteriální	bakteriální	k2eAgInSc4d1	bakteriální
kmen	kmen	k1gInSc4	kmen
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
úzkou	úzký	k2eAgFnSc4d1	úzká
skupinu	skupina	k1gFnSc4	skupina
kmenů	kmen	k1gInPc2	kmen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
chrání	chránit	k5eAaImIp3nS	chránit
mikroflóru	mikroflóra	k1gFnSc4	mikroflóra
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
Příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
bakteriofágových	bakteriofágový	k2eAgInPc2d1	bakteriofágový
preparátů	preparát	k1gInPc2	preparát
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
než	než	k8xS	než
zavedení	zavedení	k1gNnSc1	zavedení
nového	nový	k2eAgNnSc2d1	nové
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
)	)	kIx)	)
Produkce	produkce	k1gFnSc1	produkce
fágových	fágův	k2eAgInPc2d1	fágův
preparátů	preparát	k1gInPc2	preparát
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
zavádění	zavádění	k1gNnSc1	zavádění
nových	nový	k2eAgNnPc2d1	nové
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
užitečných	užitečný	k2eAgFnPc2d1	užitečná
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
bakteriofágy	bakteriofág	k1gInPc4	bakteriofág
jako	jako	k8xC	jako
ničitelé	ničitel	k1gMnPc1	ničitel
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
negativně	negativně	k6eAd1	negativně
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
průběh	průběh	k1gInSc4	průběh
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
infekcí	infekce	k1gFnPc2	infekce
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
buňky	buňka	k1gFnSc2	buňka
mohou	moct	k5eAaImIp3nP	moct
totiž	totiž	k9	totiž
přejít	přejít	k5eAaPmF	přejít
do	do	k7c2	do
latentního	latentní	k2eAgInSc2d1	latentní
<g/>
,	,	kIx,	,
lyzogenního	lyzogenní	k2eAgInSc2d1	lyzogenní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
bakteriofág	bakteriofág	k1gInSc1	bakteriofág
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gFnSc1	jeho
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
)	)	kIx)	)
začlení	začlenit	k5eAaPmIp3nS	začlenit
do	do	k7c2	do
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Fág	fág	k1gInSc1	fág
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
profág	profág	k1gInSc1	profág
<g/>
.	.	kIx.	.
</s>
<s>
Zpětný	zpětný	k2eAgInSc1d1	zpětný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
vyčlenění	vyčlenění	k1gNnSc1	vyčlenění
(	(	kIx(	(
<g/>
excize	excize	k1gFnSc1	excize
<g/>
)	)	kIx)	)
z	z	k7c2	z
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
DNA	DNA	kA	DNA
a	a	k8xC	a
přechod	přechod	k1gInSc4	přechod
do	do	k7c2	do
lytické	lytický	k2eAgFnSc2d1	lytická
fáze	fáze	k1gFnSc2	fáze
(	(	kIx(	(
<g/>
replikace	replikace	k1gFnSc1	replikace
a	a	k8xC	a
lyze	lyze	k1gFnSc1	lyze
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
při	při	k7c6	při
"	"	kIx"	"
<g/>
stresu	stres	k1gInSc6	stres
<g/>
"	"	kIx"	"
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
buňky	buňka	k1gFnSc2	buňka
vyvolaném	vyvolaný	k2eAgMnSc6d1	vyvolaný
např.	např.	kA	např.
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
UV	UV	kA	UV
zářením	záření	k1gNnPc3	záření
nebo	nebo	k8xC	nebo
některými	některý	k3yIgNnPc7	některý
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Fágy	fág	k1gInPc1	fág
v	v	k7c6	v
lyzogenním	lyzogenní	k2eAgInSc6d1	lyzogenní
stavu	stav	k1gInSc6	stav
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
bakteriální	bakteriální	k2eAgFnSc7d1	bakteriální
DNA	DNA	kA	DNA
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
také	také	k9	také
replikovány	replikován	k2eAgInPc1d1	replikován
při	při	k7c6	při
buněčném	buněčný	k2eAgNnSc6d1	buněčné
dělení	dělení	k1gNnSc6	dělení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
genomu	genom	k1gInSc6	genom
některých	některý	k3yIgInPc2	některý
bakteriofágů	bakteriofág	k1gInPc2	bakteriofág
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
geny	gen	k1gInPc1	gen
pro	pro	k7c4	pro
toxiny	toxin	k1gInPc4	toxin
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tím	ten	k3xDgNnSc7	ten
lyzogenizovaná	lyzogenizovaný	k2eAgFnSc1d1	lyzogenizovaný
bakterie	bakterie	k1gFnSc1	bakterie
(	(	kIx(	(
<g/>
bakterie	bakterie	k1gFnSc1	bakterie
s	s	k7c7	s
profágem	profág	k1gInSc7	profág
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
produkovat	produkovat	k5eAaImF	produkovat
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
i	i	k9	i
z	z	k7c2	z
relativně	relativně	k6eAd1	relativně
neškodné	škodný	k2eNgFnSc2d1	neškodná
bakterie	bakterie	k1gFnSc2	bakterie
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
vysoce	vysoce	k6eAd1	vysoce
patogenní	patogenní	k2eAgMnPc4d1	patogenní
původce	původce	k1gMnPc4	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bakterie	bakterie	k1gFnSc1	bakterie
díky	díky	k7c3	díky
profágu	profág	k1gInSc3	profág
získá	získat	k5eAaPmIp3nS	získat
nové	nový	k2eAgFnSc2d1	nová
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
lyzogenní	lyzogenní	k2eAgFnSc1d1	lyzogenní
konverze	konverze	k1gFnSc1	konverze
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc4	případ
zisku	zisk	k1gInSc2	zisk
genu	gen	k1gInSc2	gen
pro	pro	k7c4	pro
toxin	toxin	k1gInSc4	toxin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
negativní	negativní	k2eAgInPc1d1	negativní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
profág	profág	k1gInSc1	profág
svým	svůj	k3xOyFgNnSc7	svůj
začleněním	začlenění	k1gNnSc7	začlenění
do	do	k7c2	do
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
genů	gen	k1gInPc2	gen
bakterie	bakterie	k1gFnSc2	bakterie
přeruší	přerušit	k5eAaPmIp3nS	přerušit
jeho	jeho	k3xOp3gFnSc4	jeho
sekvenci	sekvence	k1gFnSc4	sekvence
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
gen	gen	k1gInSc1	gen
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
lyzogenní	lyzogenní	k2eAgFnSc2d1	lyzogenní
konverze	konverze	k1gFnSc2	konverze
je	být	k5eAaImIp3nS	být
bakterie	bakterie	k1gFnSc1	bakterie
Corynebacterium	Corynebacterium	k1gNnSc4	Corynebacterium
diphteriae	diphteriaat	k5eAaPmIp3nS	diphteriaat
způsobující	způsobující	k2eAgInSc1d1	způsobující
záškrt	záškrt	k1gInSc1	záškrt
-	-	kIx~	-
při	při	k7c6	při
lyzogenizaci	lyzogenizace	k1gFnSc6	lyzogenizace
bakteriofágem	bakteriofág	k1gInSc7	bakteriofág
B	B	kA	B
začne	začít	k5eAaPmIp3nS	začít
produkovat	produkovat	k5eAaImF	produkovat
toxin	toxin	k1gInSc4	toxin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
kódován	kódovat	k5eAaBmNgInS	kódovat
fágovým	fágův	k2eAgInSc7d1	fágův
genem	gen	k1gInSc7	gen
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
takovými	takový	k3xDgFnPc7	takový
bakteriemi	bakterie	k1gFnPc7	bakterie
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vážnější	vážní	k2eAgFnSc1d2	vážnější
než	než	k8xS	než
"	"	kIx"	"
<g/>
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
<g/>
"	"	kIx"	"
záškrt	záškrt	k1gInSc1	záškrt
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	coli	k1gNnSc2	coli
<g/>
,	,	kIx,	,
neškodné	škodný	k2eNgFnSc2d1	neškodná
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
jako	jako	k9	jako
komenzál	komenzál	k1gInSc1	komenzál
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
vážné	vážný	k2eAgFnPc1d1	vážná
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
potíže	potíž	k1gFnPc1	potíž
(	(	kIx(	(
<g/>
enterohemoragická	enterohemoragický	k2eAgFnSc1d1	enterohemoragická
E.	E.	kA	E.
coli	col	k1gFnSc6	col
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kmeny	kmen	k1gInPc1	kmen
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
také	také	k9	také
infikované	infikovaný	k2eAgFnPc4d1	infikovaná
bakteriofágem	bakteriofág	k1gInSc7	bakteriofág
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
patogenitu	patogenita	k1gFnSc4	patogenita
těchto	tento	k3xDgInPc2	tento
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vazbou	vazba	k1gFnSc7	vazba
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
Bakteriofágy	bakteriofág	k1gInPc4	bakteriofág
jako	jako	k8xC	jako
léky	lék	k1gInPc4	lék
(	(	kIx(	(
<g/>
fágová	fágový	k2eAgFnSc1d1	fágový
terapie	terapie	k1gFnSc1	terapie
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
fágy	fág	k1gInPc1	fág
užívané	užívaný	k2eAgInPc1d1	užívaný
ve	v	k7c6	v
fágové	fágové	k2eAgFnSc6d1	fágové
terapii	terapie	k1gFnSc6	terapie
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
existovat	existovat	k5eAaImF	existovat
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
profága	profága	k1gFnSc1	profága
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nemají	mít	k5eNaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
průběh	průběh	k1gInSc4	průběh
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
infekcí	infekce	k1gFnPc2	infekce
lyzogenní	lyzogenní	k2eAgFnSc7d1	lyzogenní
konverzí	konverze	k1gFnSc7	konverze
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
měly	mít	k5eAaImAgFnP	mít
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
vyléčení	vyléčení	k1gNnSc1	vyléčení
nemocného	nemocný	k1gMnSc2	nemocný
by	by	k9	by
mohly	moct	k5eAaImAgFnP	moct
jeho	on	k3xPp3gInSc4	on
stav	stav	k1gInSc4	stav
naopak	naopak	k6eAd1	naopak
výrazně	výrazně	k6eAd1	výrazně
zhoršit	zhoršit	k5eAaPmF	zhoršit
<g/>
.	.	kIx.	.
</s>
