<s>
Bakteriofág	bakteriofág	k1gInSc1	bakteriofág
(	(	kIx(	(
<g/>
odvozený	odvozený	k2eAgMnSc1d1	odvozený
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
fagein	fagein	k1gInSc1	fagein
–	–	k?	–
řecky	řecky	k6eAd1	řecky
φ	φ	k?	φ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jíst	jíst	k5eAaImF	jíst
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
fág	fág	k1gInSc1	fág
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obecný	obecný	k2eAgInSc4d1	obecný
název	název	k1gInSc4	název
pro	pro	k7c4	pro
virus	virus	k1gInSc4	virus
infikující	infikující	k2eAgFnSc2d1	infikující
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
