<s>
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
výrobce	výrobce	k1gMnSc1	výrobce
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
počítačových	počítačový	k2eAgFnPc2d1	počítačová
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
počítačového	počítačový	k2eAgInSc2d1	počítačový
software	software	k1gInSc1	software
a	a	k8xC	a
poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
informačních	informační	k2eAgFnPc6d1	informační
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1982	[number]	k4	1982
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Santa	Santa	k1gFnSc1	Santa
Clara	Clara	k1gFnSc1	Clara
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
části	část	k1gFnSc6	část
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gMnSc2	Vallea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
středisku	středisko	k1gNnSc6	středisko
Agnews	Agnewsa	k1gFnPc2	Agnewsa
Developmental	Developmental	k1gMnSc1	Developmental
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
firmy	firma	k1gFnSc2	firma
Oracle	Oracle	k1gFnSc2	Oracle
<g/>
.	.	kIx.	.
</s>
<s>
Sun	sun	k1gInSc1	sun
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
tvůrce	tvůrce	k1gMnSc1	tvůrce
inovativních	inovativní	k2eAgFnPc2d1	inovativní
technologií	technologie	k1gFnPc2	technologie
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
platforma	platforma	k1gFnSc1	platforma
Java	Java	k1gFnSc1	Java
a	a	k8xC	a
NFS	NFS	kA	NFS
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k8xS	jako
obhájce	obhájce	k1gMnSc1	obhájce
otevřených	otevřený	k2eAgInPc2d1	otevřený
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Unixu	Unix	k1gInSc3	Unix
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
velký	velký	k2eAgMnSc1d1	velký
zastánce	zastánce	k1gMnSc1	zastánce
i	i	k8xC	i
vydavatel	vydavatel	k1gMnSc1	vydavatel
tzv.	tzv.	kA	tzv.
open	open	k1gMnSc1	open
source	source	k1gMnSc1	source
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
balík	balík	k1gInSc1	balík
OpenOffice	OpenOffice	k1gFnSc1	OpenOffice
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
produkty	produkt	k1gInPc7	produkt
této	tento	k3xDgFnSc2	tento
značky	značka	k1gFnSc2	značka
patřily	patřit	k5eAaImAgInP	patřit
servery	server	k1gInPc1	server
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc1d1	pracovní
stanice	stanice	k1gFnPc1	stanice
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
vlastní	vlastní	k2eAgInSc4d1	vlastní
RISC	RISC	kA	RISC
technologii	technologie	k1gFnSc6	technologie
(	(	kIx(	(
<g/>
SPARC	SPARC	kA	SPARC
<g/>
)	)	kIx)	)
a	a	k8xC	a
procesorech	procesor	k1gInPc6	procesor
Opteron	Opteron	k1gMnSc1	Opteron
společnosti	společnost	k1gFnSc3	společnost
AMD	AMD	kA	AMD
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
i	i	k9	i
procesory	procesor	k1gInPc1	procesor
firmy	firma	k1gFnSc2	firma
Intel	Intel	kA	Intel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
si	se	k3xPyFc3	se
společnost	společnost	k1gFnSc1	společnost
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
softwareové	softwareové	k2eAgInPc4d1	softwareové
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patřil	patřit	k5eAaImAgInS	patřit
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
,	,	kIx,	,
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Solaris	Solaris	k1gInSc1	Solaris
<g/>
,	,	kIx,	,
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
ZFS	ZFS	kA	ZFS
<g/>
,	,	kIx,	,
vývojářské	vývojářský	k2eAgFnPc4d1	vývojářská
utility	utilita	k1gFnPc4	utilita
a	a	k8xC	a
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
NetBeans	NetBeans	k1gInSc4	NetBeans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
software	software	k1gInSc1	software
pro	pro	k7c4	pro
Webovou	webový	k2eAgFnSc4d1	webová
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
,	,	kIx,	,
a	a	k8xC	a
aplikace	aplikace	k1gFnPc1	aplikace
pro	pro	k7c4	pro
identity	identita	k1gFnPc4	identita
management	management	k1gInSc1	management
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgFnPc1d1	výrobní
haly	hala	k1gFnPc1	hala
společnosti	společnost	k1gFnSc2	společnost
Sun	suna	k1gFnPc2	suna
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Hillsboro	Hillsbora	k1gFnSc5	Hillsbora
v	v	k7c6	v
Oregonu	Oregon	k1gInSc2	Oregon
a	a	k8xC	a
Linlithgow	Linlithgow	k1gFnSc2	Linlithgow
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
návrh	návrh	k1gInSc1	návrh
pracovní	pracovní	k2eAgFnSc2d1	pracovní
stanice	stanice	k1gFnSc2	stanice
Sun	Sun	kA	Sun
učinil	učinit	k5eAaImAgMnS	učinit
Andy	Anda	k1gFnPc4	Anda
Bechtolsheim	Bechtolsheimo	k1gNnPc2	Bechtolsheimo
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
Stanfordově	Stanfordův	k2eAgFnSc6d1	Stanfordova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alto	k1gNnSc1	Alto
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
"	"	kIx"	"
<g/>
68000	[number]	k4	68000
Unix	Unix	k1gInSc1	Unix
system	syst	k1gInSc7	syst
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c6	v
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
sestavoval	sestavovat	k5eAaImAgInS	sestavovat
jej	on	k3xPp3gMnSc4	on
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
dodávali	dodávat	k5eAaImAgMnP	dodávat
z	z	k7c2	z
univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
oddělení	oddělení	k1gNnSc2	oddělení
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
ze	z	k7c2	z
skladů	sklad	k1gInPc2	sklad
v	v	k7c6	v
Silicon	Silicon	kA	Silicon
Valley	Valle	k2eAgFnPc4d1	Valle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgFnSc4	první
pracovní	pracovní	k2eAgFnSc4d1	pracovní
stanici	stanice	k1gFnSc4	stanice
Sun	Sun	kA	Sun
běžel	běžet	k5eAaImAgInS	běžet
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Unix	Unix	k1gInSc1	Unix
Version	Version	k1gInSc1	Version
7	[number]	k4	7
portovaný	portovaný	k2eAgInSc1d1	portovaný
UniSoftem	UniSoft	k1gInSc7	UniSoft
na	na	k7c4	na
procesory	procesor	k1gInPc4	procesor
Motorola	Motorola	kA	Motorola
68000	[number]	k4	68000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1982	[number]	k4	1982
se	se	k3xPyFc4	se
Bechtolsheim	Bechtolsheim	k1gMnSc1	Bechtolsheim
spojil	spojit	k5eAaPmAgMnS	spojit
se	s	k7c7	s
studenty	student	k1gMnPc7	student
promujícími	promující	k2eAgMnPc7d1	promující
na	na	k7c6	na
Stanfordově	Stanfordův	k2eAgFnSc6d1	Stanfordova
univerzitě	univerzita	k1gFnSc6	univerzita
Vinodem	Vinod	k1gInSc7	Vinod
Khoslou	Khoslý	k2eAgFnSc4d1	Khoslý
<g/>
,	,	kIx,	,
Scottem	Scott	k1gInSc7	Scott
McNealym	McNealym	k1gInSc1	McNealym
a	a	k8xC	a
Billy	Bill	k1gMnPc4	Bill
Joyem	Joy	k1gMnSc7	Joy
(	(	kIx(	(
<g/>
hlavním	hlavní	k2eAgMnSc7d1	hlavní
vývojářem	vývojář	k1gMnSc7	vývojář
BSD	BSD	kA	BSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
společnost	společnost	k1gFnSc4	společnost
později	pozdě	k6eAd2	pozdě
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xS	jako
Sun	sun	k1gInSc4	sun
Microsystems	Microsystemsa	k1gFnPc2	Microsystemsa
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
ze	z	k7c2	z
začátečních	začáteční	k2eAgNnPc2d1	začáteční
písmen	písmeno	k1gNnPc2	písmeno
Stanford	Stanford	k1gMnSc1	Stanford
University	universita	k1gFnSc2	universita
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
Dalšími	další	k2eAgMnPc7d1	další
známými	známý	k2eAgMnPc7d1	známý
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
Sunu	sun	k1gInSc2	sun
byli	být	k5eAaImAgMnP	být
John	John	k1gMnSc1	John
Gilmore	Gilmor	k1gInSc5	Gilmor
a	a	k8xC	a
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
.	.	kIx.	.
</s>
<s>
Sun	Sun	kA	Sun
byl	být	k5eAaImAgMnS	být
zastánce	zastánce	k1gMnSc1	zastánce
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Unix	Unix	k1gInSc1	Unix
<g/>
,	,	kIx,	,
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
protokol	protokol	k1gInSc4	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
NFS	NFS	kA	NFS
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
odrazem	odraz	k1gInSc7	odraz
motta	motto	k1gNnSc2	motto
společnosti	společnost	k1gFnSc2	společnost
"	"	kIx"	"
<g/>
Síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
počítač	počítač	k1gInSc1	počítač
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc4	Gosling
vedl	vést	k5eAaImAgMnS	vést
tým	tým	k1gInSc4	tým
vyvíjející	vyvíjející	k2eAgInSc4d1	vyvíjející
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
Jon	Jon	k1gMnSc1	Jon
Bosak	Bosak	k1gMnSc1	Bosak
vývojářem	vývojář	k1gMnSc7	vývojář
jazyka	jazyk	k1gInSc2	jazyk
EML	EML	kA	EML
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
specifikovala	specifikovat	k5eAaBmAgFnS	specifikovat
W	W	kA	W
<g/>
3	[number]	k4	3
<g/>
C.	C.	kA	C.
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
Sun	Sun	kA	Sun
Microsystems	Microsystemsa	k1gFnPc2	Microsystemsa
prodána	prodán	k2eAgFnSc1d1	prodána
firmě	firma	k1gFnSc3	firma
Oracle	Oracle	k1gInSc4	Oracle
za	za	k7c4	za
7,4	[number]	k4	7,4
miliardy	miliarda	k4xCgFnSc2	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
společnosti	společnost	k1gFnSc2	společnost
Oracle	Oracl	k1gMnSc2	Oracl
America	Americus	k1gMnSc2	Americus
<g/>
.	.	kIx.	.
</s>
<s>
Unix	Unix	k1gInSc1	Unix
NetBeans	NetBeans	k1gInSc1	NetBeans
Java	Javus	k1gMnSc2	Javus
Oracle	Oracl	k1gMnSc2	Oracl
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc4	Microsystems
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
firmy	firma	k1gFnSc2	firma
Oracle	Oracle	k1gFnSc2	Oracle
+	+	kIx~	+
Sun	Sun	kA	Sun
Weblogy	Webloga	k1gFnSc2	Webloga
firmy	firma	k1gFnSc2	firma
Oracle	Oracle	k1gFnSc2	Oracle
Sun	Sun	kA	Sun
Developers	Developers	k1gInSc1	Developers
Network	network	k1gInSc1	network
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc4	web
české	český	k2eAgFnSc2d1	Česká
dceřiné	dceřiný	k2eAgFnSc2d1	dceřiná
firmy	firma	k1gFnSc2	firma
Sunsec	Sunsec	k1gMnSc1	Sunsec
</s>
