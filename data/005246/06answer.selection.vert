<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1982	[number]	k4	1982
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Santa	Santa	k1gFnSc1	Santa
Clara	Clara	k1gFnSc1	Clara
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
části	část	k1gFnSc6	část
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gMnSc2	Vallea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
středisku	středisko	k1gNnSc6	středisko
Agnews	Agnewsa	k1gFnPc2	Agnewsa
Developmental	Developmental	k1gMnSc1	Developmental
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
