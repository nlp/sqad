<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1368	[number]	k4	1368
Norimberk	Norimberk	k1gInSc1	Norimberk
–	–	k?	–
9.	[number]	k4	9.
prosince	prosinec	k1gInSc2	prosinec
1437	[number]	k4	1437
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
manželky	manželka	k1gFnPc1	manželka
Alžběty	Alžběta	k1gFnSc2	Alžběta
Pomořanské	pomořanský	k2eAgFnPc1d1	Pomořanská
<g/>
,	,	kIx,	,
braniborský	braniborský	k2eAgMnSc1d1	braniborský
markrabě	markrabě	k1gMnSc1	markrabě
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1373	[number]	k4	1373
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
od	od	k7c2	od
31.	[number]	k4	31.
března	březen	k1gInSc2	březen
1387	[number]	k4	1387
<g/>
.	.	kIx.	.
)	)	kIx)	)
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
zvolen	zvolen	k2eAgInSc1d1	zvolen
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
21.	[number]	k4	21.
července	červenec	k1gInSc2	červenec
1411	[number]	k4	1411
<g/>
,	,	kIx,	,
korunován	korunován	k2eAgMnSc1d1	korunován
8.	[number]	k4	8.
listopadu	listopad	k1gInSc2	listopad
1414	[number]	k4	1414
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slezský	slezský	k2eAgMnSc1d1	slezský
vévoda	vévoda	k1gMnSc1	vévoda
a	a	k8xC	a
lužický	lužický	k2eAgMnSc1d1	lužický
markrabě	markrabě	k1gMnSc1	markrabě
(	(	kIx(	(
<g/>
od	od	k7c2	od
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
(	(	kIx(	(
<g/>
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1423	[number]	k4	1423
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
korunován	korunován	k2eAgMnSc1d1	korunován
28.	[number]	k4	28.
července	červenec	k1gInSc2	červenec
1420	[number]	k4	1420
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1436	[number]	k4	1436
<g/>
–	–	k?	–
<g/>
1437	[number]	k4	1437
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lombardský	lombardský	k2eAgMnSc1d1	lombardský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1431	[number]	k4	1431
<g/>
)	)	kIx)	)
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1433	[number]	k4	1433
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
