<s>
Leváda	Leváda	k1gFnSc1
</s>
<s>
Přímý	přímý	k2eAgInSc1d1
úsek	úsek	k1gInSc1
levády	leváda	k1gFnSc2
25	#num#	k4
fontes	fontesa	k1gFnPc2
</s>
<s>
Leváda	Leváda	k1gFnSc1
je	být	k5eAaImIp3nS
název	název	k1gInSc4
pro	pro	k7c4
zavlažovací	zavlažovací	k2eAgInSc4d1
kanál	kanál	k1gInSc4
na	na	k7c6
ostrově	ostrov	k1gInSc6
Madeira	Madeira	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podmínky	podmínka	k1gFnPc1
</s>
<s>
Existence	existence	k1gFnSc1
levád	leváda	k1gFnPc2
je	být	k5eAaImIp3nS
podmíněna	podmínit	k5eAaPmNgFnS
geologickou	geologický	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
ostrova	ostrov	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
vrcholem	vrchol	k1gInSc7
prastaré	prastarý	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgFnPc1d1
geologické	geologický	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
lávy	láva	k1gFnSc2
<g/>
,	,	kIx,
popela	popel	k1gInSc2
a	a	k8xC
tufu	tuf	k1gInSc2
nejsou	být	k5eNaImIp3nP
vodorovné	vodorovný	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
převládá	převládat	k5eAaImIp3nS
sklon	sklon	k1gInSc4
k	k	k7c3
severu	sever	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
i	i	k9
déšť	déšť	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
vsáknul	vsáknout	k5eAaPmAgInS
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
svahu	svah	k1gInSc6
<g/>
,	,	kIx,
protéká	protékat	k5eAaImIp3nS
pod	pod	k7c7
povrchem	povrch	k1gInSc7
k	k	k7c3
severu	sever	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
prameny	pramen	k1gInPc1
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
ve	v	k7c6
výškách	výška	k1gFnPc6
až	až	k9
1600	#num#	k4
m.	m.	k?
Nejintenzivnější	intenzivní	k2eAgInPc1d3
deště	dešť	k1gInPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
kolem	kolem	k7c2
1000	#num#	k4
m	m	kA
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
většina	většina	k1gFnSc1
levád	leváda	k1gFnPc2
začíná	začínat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geologové	geolog	k1gMnPc1
odhadují	odhadovat	k5eAaImIp3nP
zásoby	zásoba	k1gFnPc4
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
zachycené	zachycený	k2eAgFnSc2d1
v	v	k7c6
porézní	porézní	k2eAgFnSc6d1
hornině	hornina	k1gFnSc6
ostrova	ostrov	k1gInSc2
na	na	k7c4
200	#num#	k4
milionů	milion	k4xCgInPc2
m³	m³	k?
<g/>
.	.	kIx.
</s>
<s>
Úzký	úzký	k2eAgInSc1d1
tunel	tunel	k1gInSc1
ve	v	k7c6
skále	skála	k1gFnSc6
s	s	k7c7
levádou	leváda	k1gFnSc7
(	(	kIx(
<g/>
Levada	Levada	k1gFnSc1
Nova	nova	k1gFnSc1
do	do	k7c2
Norte	Nort	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
První	první	k4xOgMnPc1
portugalští	portugalský	k2eAgMnPc1d1
osadníci	osadník	k1gMnPc1
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
brzy	brzy	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jih	jih	k1gInSc1
s	s	k7c7
příjemným	příjemný	k2eAgNnSc7d1
subtropickým	subtropický	k2eAgNnSc7d1
klimatem	klima	k1gNnSc7
nemá	mít	k5eNaImIp3nS
dostatek	dostatek	k1gInSc4
vody	voda	k1gFnSc2
pro	pro	k7c4
zemědělství	zemědělství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
severozápadě	severozápad	k1gInSc6
ostrova	ostrov	k1gInSc2
však	však	k9
prší	pršet	k5eAaImIp3nS
skoro	skoro	k6eAd1
každý	každý	k3xTgInSc1
den	den	k1gInSc1
(	(	kIx(
<g/>
roční	roční	k2eAgInSc1d1
úhrn	úhrn	k1gInSc1
srážek	srážka	k1gFnPc2
činí	činit	k5eAaImIp3nS
až	až	k9
2000	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
na	na	k7c6
náhorní	náhorní	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
a	a	k8xC
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
prakticky	prakticky	k6eAd1
denně	denně	k6eAd1
v	v	k7c6
odpoledních	odpolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
skrytých	skrytý	k2eAgInPc2d1
v	v	k7c6
mlze	mlha	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vody	voda	k1gFnPc4
dostatek	dostatek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
jsou	být	k5eAaImIp3nP
mraky	mrak	k1gInPc1
postupující	postupující	k2eAgInPc1d1
v	v	k7c6
malé	malý	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
nad	nad	k7c7
oceánem	oceán	k1gInSc7
od	od	k7c2
severu	sever	k1gInSc2
a	a	k8xC
narážející	narážející	k2eAgInSc4d1
na	na	k7c4
ostrov	ostrov	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
hory	hora	k1gFnPc1
dosahují	dosahovat	k5eAaImIp3nP
výšky	výška	k1gFnPc4
kolem	kolem	k7c2
1800	#num#	k4
m.	m.	k?
</s>
<s>
Portugalci	Portugalec	k1gMnPc1
proto	proto	k8xC
brzy	brzy	k6eAd1
začali	začít	k5eAaPmAgMnP
budovat	budovat	k5eAaImF
kanály	kanál	k1gInPc4
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
vodu	voda	k1gFnSc4
ze	z	k7c2
severu	sever	k1gInSc2
převádějí	převádět	k5eAaImIp3nP
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budování	budování	k1gNnSc1
levád	leváda	k1gFnPc2
a	a	k8xC
rozdělování	rozdělování	k1gNnSc4
vody	voda	k1gFnSc2
dostalo	dostat	k5eAaPmAgNnS
hned	hned	k6eAd1
zpočátku	zpočátku	k6eAd1
i	i	k9
právní	právní	k2eAgInSc4d1
základ	základ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
roku	rok	k1gInSc2
1461	#num#	k4
byli	být	k5eAaImAgMnP
jmenováni	jmenovat	k5eAaImNgMnP,k5eAaBmNgMnP
dva	dva	k4xCgMnPc1
úředníci	úředník	k1gMnPc1
dohlížející	dohlížející	k2eAgMnPc1d1
na	na	k7c4
rozdělování	rozdělování	k1gNnSc4
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1485	#num#	k4
jim	on	k3xPp3gMnPc3
král	král	k1gMnSc1
Joã	Joã	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
stanovil	stanovit	k5eAaPmAgInS
pravidla	pravidlo	k1gNnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
musejí	muset	k5eAaImIp3nP
uživatelům	uživatel	k1gMnPc3
vodu	voda	k1gFnSc4
poskytnout	poskytnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
pak	pak	k6eAd1
roku	rok	k1gInSc2
1493	#num#	k4
vydal	vydat	k5eAaPmAgInS
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
nějž	jenž	k3xRgInSc2
nesmějí	smát	k5eNaImIp3nP
vlastníci	vlastník	k1gMnPc1
pozemků	pozemek	k1gInPc2
bránit	bránit	k5eAaImF
stavbě	stavba	k1gFnSc3
<g/>
,	,	kIx,
údržbě	údržba	k1gFnSc3
a	a	k8xC
využívání	využívání	k1gNnSc4
levád	leváda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
rozšířený	rozšířený	k2eAgInSc4d1
a	a	k8xC
prohloubený	prohloubený	k2eAgInSc4d1
úsek	úsek	k1gInSc4
levády	leváda	k1gFnSc2
dosud	dosud	k6eAd1
nestačil	stačit	k5eNaBmAgInS
zarůst	zarůst	k5eAaPmF
vegetací	vegetace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sleduje	sledovat	k5eAaImIp3nS
půdorysný	půdorysný	k2eAgInSc4d1
profil	profil	k1gInSc4
skalní	skalní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
levád	leváda	k1gFnPc2
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nP
podle	podle	k7c2
jednotlivých	jednotlivý	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záleží	záležet	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
sečteme	sečíst	k5eAaPmIp1nP
jen	jen	k9
délku	délka	k1gFnSc4
hlavních	hlavní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
započteme	započíst	k5eAaPmIp1nP
i	i	k9
nejužší	úzký	k2eAgFnPc4d3
odbočky	odbočka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
bylo	být	k5eAaImAgNnS
registrováno	registrovat	k5eAaBmNgNnS
200	#num#	k4
levád	leváda	k1gFnPc2
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
1000	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
setkáváme	setkávat	k5eAaImIp1nP
s	s	k7c7
údaji	údaj	k1gInPc7
od	od	k7c2
2400	#num#	k4
km	km	kA
až	až	k6eAd1
po	po	k7c4
5000	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
levádách	leváda	k1gFnPc6
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
40	#num#	k4
km	km	kA
tunelů	tunel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
iniciovala	iniciovat	k5eAaBmAgFnS
portugalská	portugalský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
vypracování	vypracování	k1gNnSc1
studie	studie	k1gFnSc2
o	o	k7c6
využití	využití	k1gNnSc6
a	a	k8xC
budoucnosti	budoucnost	k1gFnSc6
levád	leváda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejím	její	k3xOp3gInSc6
základě	základ	k1gInSc6
vláda	vláda	k1gFnSc1
financovala	financovat	k5eAaBmAgFnS
výstavbu	výstavba	k1gFnSc4
nových	nový	k2eAgFnPc2d1
levád	leváda	k1gFnPc2
využívaných	využívaný	k2eAgFnPc2d1
i	i	k9
k	k	k7c3
pohonu	pohon	k1gInSc3
hydroelektráren	hydroelektrárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
největší	veliký	k2eAgFnPc1d3
z	z	k7c2
nich	on	k3xPp3gFnPc2
byly	být	k5eAaImAgFnP
Levada	Levada	k1gFnSc1
dos	dos	k?
Tornos	Tornos	k1gInSc1
a	a	k8xC
Levada	Levada	k1gFnSc1
do	do	k7c2
Norte	Nort	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
25	#num#	k4
roků	rok	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
vše	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
provést	provést	k5eAaPmF
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracovali	pracovat	k5eAaImAgMnP
na	na	k7c6
nich	on	k3xPp3gFnPc6
i	i	k9
horolezci	horolezec	k1gMnPc1
a	a	k8xC
práce	práce	k1gFnPc1
si	se	k3xPyFc3
vyžádaly	vyžádat	k5eAaPmAgFnP
také	také	k9
lidské	lidský	k2eAgFnPc1d1
oběti	oběť	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Levada	Levada	k1gFnSc1
dos	dos	k?
Tornos	Tornos	k1gInSc1
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
provozu	provoz	k1gInSc2
roku	rok	k1gInSc2
1966	#num#	k4
a	a	k8xC
měří	měřit	k5eAaImIp3nS
106	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
ve	v	k7c6
výšce	výška	k1gFnSc6
1000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
v	v	k7c6
délce	délka	k1gFnSc6
16	#num#	k4
km	km	kA
prochází	procházet	k5eAaImIp3nP
tunely	tunel	k1gInPc1
(	(	kIx(
<g/>
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejdelší	dlouhý	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
5,1	5,1	k4
km	km	kA
dlouhý	dlouhý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pohání	pohánět	k5eAaImIp3nS
hydroelektrárnu	hydroelektrárna	k1gFnSc4
(	(	kIx(
<g/>
ve	v	k7c6
výšce	výška	k1gFnSc6
přibližně	přibližně	k6eAd1
600	#num#	k4
m	m	kA
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
)	)	kIx)
a	a	k8xC
voda	voda	k1gFnSc1
pak	pak	k6eAd1
zavlažuje	zavlažovat	k5eAaImIp3nS
9900	#num#	k4
hektarů	hektar	k1gInPc2
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Schodovité	schodovitý	k2eAgNnSc1d1
klesání	klesání	k1gNnSc1
levády	leváda	k1gFnSc2
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Leváda	Leváda	k1gFnSc1
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
po	po	k7c6
celé	celá	k1gFnSc6
své	svůj	k3xOyFgFnSc6
délce	délka	k1gFnSc6
mírné	mírný	k2eAgNnSc1d1
klesání	klesání	k1gNnSc1
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
například	například	k6eAd1
ve	v	k7c6
svislé	svislý	k2eAgFnSc6d1
skalní	skalní	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
tunelem	tunel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
změnit	změnit	k5eAaPmF
výšku	výška	k1gFnSc4
skokem	skok	k1gInSc7
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
vodopád	vodopád	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
schody	schod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
stavbu	stavba	k1gFnSc4
prvních	první	k4xOgFnPc2
levád	leváda	k1gFnPc2
byli	být	k5eAaImAgMnP
využíváni	využíván	k2eAgMnPc1d1
převážně	převážně	k6eAd1
černí	černý	k2eAgMnPc1d1
otroci	otrok	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
zde	zde	k6eAd1
však	však	k9
i	i	k9
otroci	otrok	k1gMnPc1
arabští	arabský	k2eAgMnPc1d1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
se	s	k7c7
zavlažovacími	zavlažovací	k2eAgInPc7d1
kanály	kanál	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gFnSc7
stavbou	stavba	k1gFnSc7
zkušenosti	zkušenost	k1gFnSc2
z	z	k7c2
domova	domov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
omezení	omezení	k1gNnSc2
odpařování	odpařování	k1gNnSc2
jsou	být	k5eAaImIp3nP
levády	leváda	k1gFnPc1
více	hodně	k6eAd2
hluboké	hluboký	k2eAgFnPc1d1
než	než	k8xS
široké	široký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
šířku	šířka	k1gFnSc4
asi	asi	k9
od	od	k7c2
20	#num#	k4
cm	cm	kA
až	až	k6eAd1
po	po	k7c6
1	#num#	k4
m.	m.	k?
První	první	k4xOgFnPc1
levády	leváda	k1gFnPc1
byly	být	k5eAaImAgFnP
tesány	tesat	k5eAaImNgFnP
do	do	k7c2
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
dozděny	dozděn	k2eAgInPc1d1
kamennou	kamenný	k2eAgFnSc7d1
zídkou	zídka	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
úsecích	úsek	k1gInPc6
měly	mít	k5eAaImAgFnP
dřevěné	dřevěný	k2eAgNnSc4d1
koryto	koryto	k1gNnSc4
(	(	kIx(
<g/>
na	na	k7c6
Madeiře	Madeira	k1gFnSc6
byl	být	k5eAaImAgInS
dostatek	dostatek	k1gInSc1
dřeva	dřevo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začal	začít	k5eAaPmAgMnS
převládat	převládat	k5eAaImF
beton	beton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
jsou	být	k5eAaImIp3nP
postupně	postupně	k6eAd1
opravovány	opravován	k2eAgInPc1d1
úseky	úsek	k1gInPc1
starých	starý	k2eAgFnPc2d1
levád	leváda	k1gFnPc2
(	(	kIx(
<g/>
na	na	k7c6
Madeiře	Madeira	k1gFnSc6
obvykle	obvykle	k6eAd1
nemrzne	mrznout	k5eNaImIp3nS
<g/>
,	,	kIx,
ani	ani	k8xC
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
<g/>
,	,	kIx,
takže	takže	k8xS
nejsou	být	k5eNaImIp3nP
problémy	problém	k1gInPc4
s	s	k7c7
poškozením	poškození	k1gNnSc7
betonu	beton	k1gInSc2
mrazem	mráz	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nespotřebovaná	spotřebovaný	k2eNgFnSc1d1
voda	voda	k1gFnSc1
teče	téct	k5eAaImIp3nS
z	z	k7c2
levády	leváda	k1gFnSc2
do	do	k7c2
moře	moře	k1gNnSc2
</s>
<s>
Levády	Leváda	k1gFnPc1
procházející	procházející	k2eAgFnPc1d1
přes	přes	k7c4
vesnice	vesnice	k1gFnPc4
mají	mít	k5eAaImIp3nP
odbočky	odbočka	k1gFnPc1
k	k	k7c3
odběratelům	odběratel	k1gMnPc3
vody	voda	k1gFnSc2
–	–	k?
zahrazením	zahrazení	k1gNnPc3
toku	tok	k1gInSc2
překážkou	překážka	k1gFnSc7
voda	voda	k1gFnSc1
začne	začít	k5eAaPmIp3nS
proudit	proudit	k5eAaPmF,k5eAaImF
do	do	k7c2
odbočky	odbočka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
napuštění	napuštění	k1gNnSc6
nádrže	nádrž	k1gFnSc2
nebo	nebo	k8xC
zavlažení	zavlažení	k1gNnSc2
políčka	políčko	k1gNnSc2
se	se	k3xPyFc4
překážka	překážka	k1gFnSc1
vyjme	vyjmout	k5eAaPmIp3nS
a	a	k8xC
voda	voda	k1gFnSc1
proudí	proudit	k5eAaPmIp3nS,k5eAaImIp3nS
dál	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespotřebovaná	spotřebovaný	k2eNgFnSc1d1
voda	voda	k1gFnSc1
na	na	k7c6
konci	konec	k1gInSc6
levády	leváda	k1gFnSc2
teče	teč	k1gFnSc2
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levády	Levád	k1gInPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
udržovat	udržovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
za	za	k7c4
vodu	voda	k1gFnSc4
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
nikoliv	nikoliv	k9
podle	podle	k7c2
odebraného	odebraný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
podle	podle	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
od	od	k7c2
zdroje	zdroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Levády	Leváda	k1gFnPc1
dobře	dobře	k6eAd1
zapadají	zapadat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
přirozeného	přirozený	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
stavitelé	stavitel	k1gMnPc1
neupravovali	upravovat	k5eNaImAgMnP
směr	směr	k1gInSc4
toku	tok	k1gInSc2
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
leváda	leváda	k1gFnSc1
obvykle	obvykle	k6eAd1
sleduje	sledovat	k5eAaImIp3nS
půdorysný	půdorysný	k2eAgInSc4d1
profil	profil	k1gInSc4
skalní	skalní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
široké	široký	k2eAgFnPc1d1
levády	leváda	k1gFnPc1
využívaly	využívat	k5eAaPmAgFnP,k5eAaImAgFnP
i	i	k9
k	k	k7c3
plavení	plavení	k1gNnSc3
dřeva	dřevo	k1gNnSc2
a	a	k8xC
chodníky	chodník	k1gInPc1
kolem	kolem	k7c2
nich	on	k3xPp3gInPc2
byly	být	k5eAaImAgFnP
často	často	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
přístupovou	přístupový	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
do	do	k7c2
horských	horský	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Téměř	téměř	k6eAd1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
je	být	k5eAaImIp3nS
souběžně	souběžně	k6eAd1
s	s	k7c7
levádou	leváda	k1gFnSc7
veden	vést	k5eAaImNgInS
chodníček	chodníček	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
těchto	tento	k3xDgInPc6
chodnících	chodník	k1gInPc6
dnes	dnes	k6eAd1
proudí	proudý	k2eAgMnPc1d1
turisté	turist	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Opravený	opravený	k2eAgInSc1d1
úsek	úsek	k1gInSc1
levády	leváda	k1gFnSc2
da	da	k?
Fajă	Fajă	k1gMnSc1
da	da	k?
Ovelha	Ovelha	k1gMnSc1
na	na	k7c6
Madeiře	Madeira	k1gFnSc6
se	se	k3xPyFc4
kříží	křížit	k5eAaImIp3nS
s	s	k7c7
horským	horský	k2eAgInSc7d1
potokem	potok	k1gInSc7
</s>
<s>
Voda	voda	k1gFnSc1
v	v	k7c6
levádách	leváda	k1gFnPc6
je	být	k5eAaImIp3nS
křišťálově	křišťálově	k6eAd1
čistá	čistý	k2eAgFnSc1d1
a	a	k8xC
studená	studený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
prudkém	prudký	k2eAgInSc6d1
dešti	dešť	k1gInSc6
však	však	k9
levády	levád	k1gInPc1
zachytí	zachytit	k5eAaPmIp3nP
i	i	k9
povrchovou	povrchový	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
a	a	k8xC
cesta	cesta	k1gFnSc1
kolem	kolem	k7c2
nich	on	k3xPp3gMnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
značně	značně	k6eAd1
obtížná	obtížný	k2eAgFnSc1d1
až	až	k8xS
nebezpečná	bezpečný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ekologický	ekologický	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Podél	podél	k7c2
levád	leváda	k1gFnPc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
také	také	k9
bohatá	bohatý	k2eAgFnSc1d1
květena	květena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
roku	rok	k1gInSc2
kolem	kolem	k7c2
levád	leváda	k1gFnPc2
hojně	hojně	k6eAd1
kvete	kvést	k5eAaImIp3nS
modře	modro	k6eAd1
a	a	k8xC
bíle	bíle	k6eAd1
zbarvený	zbarvený	k2eAgInSc4d1
kalokvět	kalokvět	k1gInSc4
(	(	kIx(
<g/>
Agapanthus	Agapanthus	k1gInSc1
praecox	praecox	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Turistické	turistický	k2eAgNnSc1d1
využití	využití	k1gNnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vnitrozemí	vnitrozemí	k1gNnSc1
ostrova	ostrov	k1gInSc2
Madeiry	Madeira	k1gFnSc2
je	být	k5eAaImIp3nS
jen	jen	k9
velmi	velmi	k6eAd1
obtížně	obtížně	k6eAd1
prostupné	prostupný	k2eAgNnSc1d1
pro	pro	k7c4
pěšího	pěší	k1gMnSc4
turistu	turista	k1gMnSc4
–	–	k?
strmé	strmý	k2eAgInPc4d1
<g/>
,	,	kIx,
skalnaté	skalnatý	k2eAgInPc4d1
kopce	kopec	k1gInPc4
nebo	nebo	k8xC
prakticky	prakticky	k6eAd1
neprostupná	prostupný	k2eNgFnSc1d1
hustá	hustý	k2eAgFnSc1d1
<g/>
,	,	kIx,
často	často	k6eAd1
trnitá	trnitý	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
uměle	uměle	k6eAd1
vytvořená	vytvořený	k2eAgFnSc1d1
leváda	leváda	k1gFnSc1
přirozenou	přirozený	k2eAgFnSc7d1
turistickou	turistický	k2eAgFnSc7d1
stezkou	stezka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břehy	břeh	k1gInPc4
koryt	koryto	k1gNnPc2
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
betonové	betonový	k2eAgFnPc1d1
nebo	nebo	k8xC
zpevněné	zpevněný	k2eAgFnPc1d1
a	a	k8xC
protože	protože	k8xS
je	být	k5eAaImIp3nS
každou	každý	k3xTgFnSc4
levádu	leváda	k1gFnSc4
nutno	nutno	k6eAd1
udržovat	udržovat	k5eAaImF
a	a	k8xC
opravovat	opravovat	k5eAaImF
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
po	po	k7c6
jejím	její	k3xOp3gInSc6
břehu	břeh	k1gInSc6
cesta	cesta	k1gFnSc1
či	či	k8xC
úzká	úzký	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
původně	původně	k6eAd1
využívali	využívat	k5eAaPmAgMnP,k5eAaImAgMnP
vodohospodáři	vodohospodář	k1gMnPc1
ke	k	k7c3
správě	správa	k1gFnSc3
vodního	vodní	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
posledních	poslední	k2eAgNnPc2d1
let	léto	k1gNnPc2
se	se	k3xPyFc4
velmi	velmi	k6eAd1
silně	silně	k6eAd1
začalo	začít	k5eAaPmAgNnS
využívat	využívat	k5eAaPmF,k5eAaImF
jednotlivých	jednotlivý	k2eAgFnPc2d1
vybraných	vybraný	k2eAgFnPc2d1
levád	leváda	k1gFnPc2
jako	jako	k8xS,k8xC
chodeckých	chodecký	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
pro	pro	k7c4
klasickou	klasický	k2eAgFnSc4d1
pěší	pěší	k2eAgFnSc4d1
turistiku	turistika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
cestovních	cestovní	k2eAgFnPc2d1
kanceláří	kancelář	k1gFnPc2
nabízí	nabízet	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
projít	projít	k5eAaPmF
si	se	k3xPyFc3
některé	některý	k3yIgInPc4
úseky	úsek	k1gInPc4
s	s	k7c7
průvodcem	průvodce	k1gMnSc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
výkladu	výklad	k1gInSc2
i	i	k8xC
dopravy	doprava	k1gFnSc2
na	na	k7c4
začátek	začátek	k1gInSc4
levády	leváda	k1gFnSc2
případně	případně	k6eAd1
odvoz	odvoz	k1gInSc4
turistů	turist	k1gMnPc2
zpět	zpět	k6eAd1
do	do	k7c2
hotelu	hotel	k1gInSc2
po	po	k7c6
absolvování	absolvování	k1gNnSc6
trasy	trasa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levády	Leváda	k1gFnPc1
jsou	být	k5eAaImIp3nP
uváděny	uvádět	k5eAaImNgFnP
i	i	k9
turistických	turistický	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
Madeiry	Madeira	k1gFnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gInPc6
nachází	nacházet	k5eAaImIp3nS
i	i	k9
běžné	běžný	k2eAgNnSc1d1
turistické	turistický	k2eAgNnSc1d1
značení	značení	k1gNnSc1
a	a	k8xC
na	na	k7c6
rozcestích	rozcestí	k1gNnPc6
jsou	být	k5eAaImIp3nP
umístěny	umístěn	k2eAgFnPc1d1
směrovky	směrovka	k1gFnPc1
označující	označující	k2eAgFnSc4d1
správné	správný	k2eAgNnSc4d1
pokračování	pokračování	k1gNnSc4
trasy	trasa	k1gFnSc2
nebo	nebo	k8xC
i	i	k9
stručné	stručný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
o	o	k7c6
levádě	leváda	k1gFnSc6
formou	forma	k1gFnSc7
informačních	informační	k2eAgFnPc2d1
tabulí	tabule	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Leváda	Leváda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Reportáž	reportáž	k1gFnSc1
ze	z	k7c2
Pstruží	pstruží	k2eAgFnSc2d1
levády	leváda	k1gFnSc2
(	(	kIx(
<g/>
směr	směr	k1gInSc1
Furado	Furada	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Zemědělství	zemědělství	k1gNnSc1
</s>
