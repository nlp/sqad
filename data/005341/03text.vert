<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
mexické	mexický	k2eAgInPc4d1	mexický
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
Estados	Estados	k1gMnSc1	Estados
Unidos	Unidos	k1gMnSc1	Unidos
Mexicanos	Mexicanos	k1gMnSc1	Mexicanos
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Geograficky	geograficky	k6eAd1	geograficky
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
drtivou	drtivý	k2eAgFnSc7d1	drtivá
většinou	většina	k1gFnSc7	většina
území	území	k1gNnPc2	území
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
geopoliticky	geopoliticky	k6eAd1	geopoliticky
bývá	bývat	k5eAaImIp3nS	bývat
ale	ale	k9	ale
často	často	k6eAd1	často
řazeno	řazen	k2eAgNnSc1d1	řazeno
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc4	stát
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předkolumbovském	předkolumbovský	k2eAgNnSc6d1	předkolumbovské
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
středního	střední	k2eAgNnSc2d1	střední
a	a	k8xC	a
jižního	jižní	k2eAgNnSc2d1	jižní
Mexika	Mexiko	k1gNnSc2	Mexiko
rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
mezoamerické	mezoamerický	k2eAgFnPc1d1	mezoamerická
civilizace	civilizace	k1gFnPc1	civilizace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Teotihuacan	Teotihuacan	k1gMnSc1	Teotihuacan
<g/>
,	,	kIx,	,
Toltékové	Toltéek	k1gMnPc1	Toltéek
<g/>
,	,	kIx,	,
Mayové	May	k1gMnPc1	May
nebo	nebo	k8xC	nebo
Aztékové	Azték	k1gMnPc1	Azték
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prosluli	proslout	k5eAaPmAgMnP	proslout
krvavými	krvavý	k2eAgInPc7d1	krvavý
rituály	rituál	k1gInPc7	rituál
s	s	k7c7	s
lidskými	lidský	k2eAgFnPc7d1	lidská
oběťmi	oběť	k1gFnPc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
do	do	k7c2	do
"	"	kIx"	"
<g/>
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
dobyl	dobýt	k5eAaPmAgInS	dobýt
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1519	[number]	k4	1519
a	a	k8xC	a
1521	[number]	k4	1521
conquistador	conquistador	k1gMnSc1	conquistador
Hernán	Hernán	k2eAgInSc4d1	Hernán
Cortés	Cortés	k1gInSc4	Cortés
Aztéckou	aztécký	k2eAgFnSc4d1	aztécká
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
poté	poté	k6eAd1	poté
obrátili	obrátit	k5eAaPmAgMnP	obrátit
Španělé	Španěl	k1gMnPc1	Španěl
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
indiánské	indiánský	k2eAgInPc4d1	indiánský
státy	stát	k1gInPc4	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgFnSc4d1	poslední
dobyli	dobýt	k5eAaPmAgMnP	dobýt
roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
založili	založit	k5eAaPmAgMnP	založit
Španělé	Španěl	k1gMnPc1	Španěl
svoji	svůj	k3xOyFgFnSc4	svůj
kolonii	kolonie	k1gFnSc4	kolonie
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
aztéckého	aztécký	k2eAgNnSc2d1	aztécké
města	město	k1gNnSc2	město
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1	Tenochtitlán
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
území	území	k1gNnSc6	území
vysušeného	vysušený	k2eAgNnSc2d1	vysušené
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sice	sice	k8xC	sice
získalo	získat	k5eAaPmAgNnS	získat
Mexiko	Mexiko	k1gNnSc1	Mexiko
nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odtržení	odtržení	k1gNnSc1	odtržení
Texaské	texaský	k2eAgFnSc2d1	texaská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
Mexicko-americké	mexickomerický	k2eAgFnSc6d1	mexicko-americká
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Mexiko	Mexiko	k1gNnSc1	Mexiko
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
území	území	k1gNnSc1	území
na	na	k7c6	na
severu	sever	k1gInSc6	sever
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
Horní	horní	k2eAgFnSc1d1	horní
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
a	a	k8xC	a
Santa	Santa	k1gMnSc1	Santa
Fe	Fe	k1gFnSc2	Fe
de	de	k?	de
Nuevo	Nuevo	k1gNnSc1	Nuevo
México	México	k1gMnSc1	México
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
čas	čas	k1gInSc4	čas
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Francouzi	Francouz	k1gMnPc1	Francouz
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderního	moderní	k2eAgInSc2d1	moderní
mexického	mexický	k2eAgInSc2d1	mexický
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
Benito	Benit	k2eAgNnSc1d1	Benito
Juárez	Juárez	k1gInSc4	Juárez
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
indiánského	indiánský	k2eAgInSc2d1	indiánský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Revoluce	revoluce	k1gFnSc1	revoluce
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
pozemkovou	pozemkový	k2eAgFnSc7d1	pozemková
reformou	reforma	k1gFnSc7	reforma
a	a	k8xC	a
znárodňováním	znárodňování	k1gNnSc7	znárodňování
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Cárdenase	Cárdenasa	k1gFnSc6	Cárdenasa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
objevu	objev	k1gInSc3	objev
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
Mexickém	mexický	k2eAgInSc6d1	mexický
zálivu	záliv	k1gInSc6	záliv
je	být	k5eAaImIp3nS	být
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
relativně	relativně	k6eAd1	relativně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
latinskoamerických	latinskoamerický	k2eAgFnPc6d1	latinskoamerická
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
má	mít	k5eAaImIp3nS	mít
činné	činný	k2eAgFnPc4d1	činná
sopky	sopka	k1gFnPc4	sopka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ohrožováno	ohrožovat	k5eAaImNgNnS	ohrožovat
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
jedno	jeden	k4xCgNnSc1	jeden
takové	takový	k3xDgFnPc4	takový
napáchalo	napáchat	k5eAaBmAgNnS	napáchat
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
chudých	chudý	k2eAgFnPc6d1	chudá
částech	část	k1gFnPc6	část
Mexika	Mexiko	k1gNnSc2	Mexiko
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
Chiapasu	Chiapas	k1gInSc2	Chiapas
<g/>
)	)	kIx)	)
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
hnutí	hnutí	k1gNnSc1	hnutí
EZLN	EZLN	kA	EZLN
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgInPc4d1	vedený
osobou	osoba	k1gFnSc7	osoba
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
jako	jako	k8xS	jako
Subcomandante	Subcomandant	k1gMnSc5	Subcomandant
Marcos	Marcosa	k1gFnPc2	Marcosa
<g/>
,	,	kIx,	,
<g/>
které	který	k3yQgFnPc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
revoltuje	revoltovat	k5eAaImIp3nS	revoltovat
proti	proti	k7c3	proti
mexické	mexický	k2eAgFnSc3d1	mexická
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
autonomní	autonomní	k2eAgFnSc4d1	autonomní
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
mexickou	mexický	k2eAgFnSc7d1	mexická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
dramaticky	dramaticky	k6eAd1	dramaticky
zintenzívnila	zintenzívnit	k5eAaPmAgFnS	zintenzívnit
drogová	drogový	k2eAgFnSc1d1	drogová
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
státem	stát	k1gInSc7	stát
a	a	k8xC	a
drogovými	drogový	k2eAgInPc7d1	drogový
kartely	kartel	k1gInPc7	kartel
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
už	už	k6eAd1	už
téměř	téměř	k6eAd1	téměř
140	[number]	k4	140
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mexické	mexický	k2eAgInPc1d1	mexický
jsou	být	k5eAaImIp3nP	být
americká	americký	k2eAgFnSc1d1	americká
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Guatemalou	Guatemala	k1gFnSc7	Guatemala
a	a	k8xC	a
Belize	Belize	k1gFnSc1	Belize
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Mexiku	Mexiko	k1gNnSc3	Mexiko
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
několik	několik	k4yIc1	několik
ostrovů	ostrov	k1gInPc2	ostrov
ležících	ležící	k2eAgInPc2d1	ležící
na	na	k7c6	na
západě	západ	k1gInSc6	západ
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
okolo	okolo	k7c2	okolo
pobřeží	pobřeží	k1gNnSc2	pobřeží
v	v	k7c6	v
Mexickém	mexický	k2eAgInSc6d1	mexický
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
či	či	k8xC	či
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
proláklina	proláklina	k1gFnSc1	proláklina
nedaleko	nedaleko	k7c2	nedaleko
Mexicali	Mexicali	k1gFnSc2	Mexicali
(	(	kIx(	(
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
2597	[number]	k4	2597
km	km	kA	km
tvoří	tvořit	k5eAaImIp3nS	tvořit
částečně	částečně	k6eAd1	částečně
řeka	řeka	k1gFnSc1	řeka
Rio	Rio	k1gMnSc5	Rio
Grande	grand	k1gMnSc5	grand
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
hraničí	hraničit	k5eAaImIp3nS	hraničit
Mexiko	Mexiko	k1gNnSc1	Mexiko
s	s	k7c7	s
Guatemalou	Guatemala	k1gFnSc7	Guatemala
(	(	kIx(	(
<g/>
962	[number]	k4	962
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Belize	Belize	k1gFnSc1	Belize
(	(	kIx(	(
<g/>
176	[number]	k4	176
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
mexického	mexický	k2eAgNnSc2d1	mexické
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
9219	[number]	k4	9219
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tichomořské	tichomořský	k2eAgNnSc1d1	Tichomořské
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
atlantské	atlantský	k2eAgNnSc4d1	Atlantské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
oblévá	oblévat	k5eAaImIp3nS	oblévat
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
a	a	k8xC	a
část	část	k1gFnSc1	část
Karibské	karibský	k2eAgNnSc4d1	Karibské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
táhne	táhnout	k5eAaImIp3nS	táhnout
úzký	úzký	k2eAgInSc1d1	úzký
Středoamerický	středoamerický	k2eAgInSc1d1	středoamerický
příkop	příkop	k1gInSc1	příkop
dosahující	dosahující	k2eAgInSc4d1	dosahující
hloubek	hloubek	k1gInSc4	hloubek
přes	přes	k7c4	přes
6000	[number]	k4	6000
m.	m.	k?	m.
Rozloha	rozloha	k1gFnSc1	rozloha
mexického	mexický	k2eAgNnSc2d1	mexické
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
milióny	milión	k4xCgInPc4	milión
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
po	po	k7c6	po
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
Argentině	Argentina	k1gFnSc6	Argentina
je	být	k5eAaImIp3nS	být
Mexiko	Mexiko	k1gNnSc1	Mexiko
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
mexické	mexický	k2eAgInPc4d1	mexický
ostrovy	ostrov	k1gInPc4	ostrov
patří	patřit	k5eAaImIp3nS	patřit
Tiburón	Tiburón	k1gInSc1	Tiburón
<g/>
,	,	kIx,	,
Ángel	Ángel	k1gInSc1	Ángel
de	de	k?	de
la	la	k1gNnSc1	la
Guarda	Guardo	k1gNnSc2	Guardo
<g/>
,	,	kIx,	,
Cozumel	Cozumela	k1gFnPc2	Cozumela
<g/>
,	,	kIx,	,
Cedros	Cedrosa	k1gFnPc2	Cedrosa
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mexické	mexický	k2eAgNnSc1d1	mexické
území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
horopisně	horopisně	k6eAd1	horopisně
z	z	k7c2	z
největší	veliký	k2eAgFnSc2d3	veliký
části	část	k1gFnSc2	část
přimyká	přimykat	k5eAaImIp3nS	přimykat
k	k	k7c3	k
severoamerické	severoamerický	k2eAgFnSc3d1	severoamerická
kordillerské	kordillerský	k2eAgFnSc3d1	kordillerský
soustavě	soustava	k1gFnSc3	soustava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zdvihá	zdvihat	k5eAaImIp3nS	zdvihat
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
"	"	kIx"	"
<g/>
Mesa	Mesa	k1gMnSc1	Mesa
central	centrat	k5eAaPmAgMnS	centrat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
horské	horský	k2eAgInPc4d1	horský
hřbety	hřbet	k1gInPc4	hřbet
a	a	k8xC	a
kotliny	kotlina	k1gFnPc4	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
vysočina	vysočina	k1gFnSc1	vysočina
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
obou	dva	k4xCgInPc6	dva
oceánech	oceán	k1gInPc6	oceán
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
horskými	horský	k2eAgFnPc7d1	horská
pásmy	pásmo	k1gNnPc7	pásmo
pohoří	pohoří	k1gNnSc3	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
Oriental	Oriental	k1gMnSc1	Oriental
a	a	k8xC	a
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
Occidental	Occidental	k1gMnSc1	Occidental
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
mexická	mexický	k2eAgFnSc1d1	mexická
vysočina	vysočina	k1gFnSc1	vysočina
nazývá	nazývat	k5eAaImIp3nS	nazývat
Anahuácká	Anahuácký	k2eAgFnSc1d1	Anahuácký
vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
zvedá	zvedat	k5eAaImIp3nS	zvedat
se	se	k3xPyFc4	se
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
2000	[number]	k4	2000
m.	m.	k?	m.
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
částí	část	k1gFnSc7	část
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
kotliny	kotlina	k1gFnPc4	kotlina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
koncentrace	koncentrace	k1gFnSc1	koncentrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
–	–	k?	–
Mexické	mexický	k2eAgFnSc2d1	mexická
a	a	k8xC	a
Tolucké	Tolucké	k2eAgNnSc2d1	Tolucké
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
úrodné	úrodný	k2eAgFnPc1d1	úrodná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Mexické	mexický	k2eAgFnSc2d1	mexická
vysočiny	vysočina	k1gFnSc2	vysočina
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
významné	významný	k2eAgNnSc1d1	významné
pásmo	pásmo	k1gNnSc1	pásmo
s	s	k7c7	s
několika	několik	k4yIc7	několik
menšími	malý	k2eAgNnPc7d2	menší
pohořími	pohoří	k1gNnPc7	pohoří
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
jihomexické	jihomexický	k2eAgNnSc4d1	jihomexický
vulkanické	vulkanický	k2eAgNnSc4d1	vulkanické
pásmo	pásmo	k1gNnSc4	pásmo
Cordillera	Cordiller	k1gMnSc2	Cordiller
Neovolcánica	Neovolcánicus	k1gMnSc2	Neovolcánicus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
horami	hora	k1gFnPc7	hora
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Pico	Pico	k6eAd1	Pico
de	de	k?	de
Orizaba	Orizaba	k1gFnSc1	Orizaba
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
Citlaltépetl	Citlaltépetl	k1gFnSc1	Citlaltépetl
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
5636	[number]	k4	5636
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Popocatépetl	Popocatépetl	k1gMnSc1	Popocatépetl
(	(	kIx(	(
<g/>
5452	[number]	k4	5452
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iztaccíhuatl	Iztaccíhuatl	k1gMnSc1	Iztaccíhuatl
(	(	kIx(	(
<g/>
5280	[number]	k4	5280
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Colima	Colima	k1gFnSc1	Colima
(	(	kIx(	(
<g/>
4340	[number]	k4	4340
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
bývá	bývat	k5eAaImIp3nS	bývat
postihována	postihovat	k5eAaImNgFnS	postihovat
zemětřeseními	zemětřesení	k1gNnPc7	zemětřesení
tektonického	tektonický	k2eAgInSc2d1	tektonický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
sopky	sopka	k1gFnPc1	sopka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
projevují	projevovat	k5eAaImIp3nP	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
jižněji	jižně	k6eAd2	jižně
leží	ležet	k5eAaImIp3nS	ležet
pohoří	pohoří	k1gNnSc1	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
del	del	k?	del
Sur	Sur	k1gMnSc5	Sur
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Tichým	Tichý	k1gMnSc7	Tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Tehuantepeckou	Tehuantepecká	k1gFnSc4	Tehuantepecká
šíjí	šíj	k1gFnPc2	šíj
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc2	sever
Neovulcanickým	Neovulcanický	k2eAgNnPc3d1	Neovulcanický
pohořím	pohoří	k1gNnPc3	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Tehuantepecké	Tehuantepecký	k2eAgFnSc2d1	Tehuantepecký
šíje	šíj	k1gFnSc2	šíj
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
pohoří	pohoří	k1gNnSc1	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
de	de	k?	de
Chiapas	Chiapasa	k1gFnPc2	Chiapasa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přechází	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
Tabaské	Tabaský	k2eAgFnSc2d1	Tabaský
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
suchou	suchý	k2eAgFnSc4d1	suchá
a	a	k8xC	a
zkrasovělou	zkrasovělý	k2eAgFnSc4d1	zkrasovělá
vápencovou	vápencový	k2eAgFnSc4d1	vápencová
tabuli	tabule	k1gFnSc4	tabule
Yucatánského	Yucatánský	k2eAgInSc2d1	Yucatánský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
tvoří	tvořit	k5eAaImIp3nP	tvořit
část	část	k1gFnSc4	část
Mexika	Mexiko	k1gNnSc2	Mexiko
Kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
poloostrov	poloostrov	k1gInSc1	poloostrov
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
1250	[number]	k4	1250
km	km	kA	km
a	a	k8xC	a
Kalifornský	kalifornský	k2eAgInSc4d1	kalifornský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
horou	hora	k1gFnSc7	hora
poloostrova	poloostrov	k1gInSc2	poloostrov
je	být	k5eAaImIp3nS	být
El	Ela	k1gFnPc2	Ela
Picacho	Picacha	k1gFnSc5	Picacha
del	del	k?	del
Diablo	Diablo	k1gFnSc4	Diablo
–	–	k?	–
3095	[number]	k4	3095
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
500	[number]	k4	500
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
na	na	k7c4	na
západ	západ	k1gInSc4	západ
leží	ležet	k5eAaImIp3nP	ležet
vulkanické	vulkanický	k2eAgInPc1d1	vulkanický
ostrovy	ostrov	k1gInPc1	ostrov
Revillagigedo	Revillagigedo	k1gNnSc1	Revillagigedo
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k4c4	málo
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
Río	Río	k1gFnSc1	Río
Bravo	bravo	k1gMnSc1	bravo
del	del	k?	del
Norte	Nort	k1gInSc5	Nort
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Rio	Rio	k1gFnSc1	Rio
Grande	grand	k1gMnSc5	grand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
3023	[number]	k4	3023
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přítoků	přítok	k1gInPc2	přítok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
významnější	významný	k2eAgFnSc1d2	významnější
jen	jen	k9	jen
řeka	řeka	k1gFnSc1	řeka
Conchos	Conchosa	k1gFnPc2	Conchosa
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
větších	veliký	k2eAgFnPc2d2	veliký
řek	řeka	k1gFnPc2	řeka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
atlantském	atlantský	k2eAgNnSc6d1	Atlantské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
bohatší	bohatý	k2eAgFnPc1d2	bohatší
na	na	k7c4	na
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Panuco	Panuco	k6eAd1	Panuco
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
má	mít	k5eAaImIp3nS	mít
četné	četný	k2eAgFnPc4d1	četná
kratší	krátký	k2eAgFnPc4d2	kratší
řeky	řeka	k1gFnPc4	řeka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
prudce	prudko	k6eAd1	prudko
spadají	spadat	k5eAaImIp3nP	spadat
z	z	k7c2	z
pobřežních	pobřežní	k2eAgNnPc2d1	pobřežní
horských	horský	k2eAgNnPc2d1	horské
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
proto	proto	k8xC	proto
značný	značný	k2eAgInSc4d1	značný
energetický	energetický	k2eAgInSc4d1	energetický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Yaqui	Yaqu	k1gFnPc1	Yaqu
<g/>
,	,	kIx,	,
Fuerte	Fuert	k1gMnSc5	Fuert
<g/>
,	,	kIx,	,
Río	Río	k1gMnSc5	Río
Grande	grand	k1gMnSc5	grand
de	de	k?	de
Santiago	Santiago	k1gNnSc1	Santiago
(	(	kIx(	(
<g/>
800	[number]	k4	800
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
největším	veliký	k2eAgNnSc7d3	veliký
mexickým	mexický	k2eAgNnSc7d1	mexické
jezerem	jezero	k1gNnSc7	jezero
Lago	Lago	k1gMnSc1	Lago
de	de	k?	de
Chapala	Chapal	k1gMnSc4	Chapal
(	(	kIx(	(
<g/>
1	[number]	k4	1
116	[number]	k4	116
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
řeka	řeka	k1gFnSc1	řeka
Balsas	Balsas	k1gInSc1	Balsas
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
řekami	řeka	k1gFnPc7	řeka
mexického	mexický	k2eAgInSc2d1	mexický
jihu	jih	k1gInSc2	jih
jsou	být	k5eAaImIp3nP	být
Usumacinta	Usumacinto	k1gNnPc4	Usumacinto
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Grijalva	Grijalva	k1gFnSc1	Grijalva
(	(	kIx(	(
<g/>
velké	velký	k2eAgFnSc2d1	velká
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Usumacinta	Usumacinta	k1gFnSc1	Usumacinta
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc4	část
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Guatemalou	Guatemala	k1gFnSc7	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnSc1d1	východní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Yucatánského	Yucatánský	k2eAgInSc2d1	Yucatánský
poloostrova	poloostrov	k1gInSc2	poloostrov
lemuje	lemovat	k5eAaImIp3nS	lemovat
Mezoamerický	Mezoamerický	k2eAgInSc4d1	Mezoamerický
korálový	korálový	k2eAgInSc4d1	korálový
útes	útes	k1gInSc4	útes
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
korálový	korálový	k2eAgInSc4d1	korálový
útes	útes	k1gInSc4	útes
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
Mexika	Mexiko	k1gNnSc2	Mexiko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
subtropickém	subtropický	k2eAgInSc6d1	subtropický
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
týká	týkat	k5eAaImIp3nS	týkat
jen	jen	k6eAd1	jen
nížin	nížina	k1gFnPc2	nížina
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnebí	podnebí	k1gNnSc1	podnebí
jinak	jinak	k6eAd1	jinak
závisí	záviset	k5eAaImIp3nS	záviset
zejména	zejména	k9	zejména
na	na	k7c6	na
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc4	čtyři
výšková	výškový	k2eAgNnPc4d1	výškové
podnební	podnební	k2eAgNnPc4d1	podnební
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Horká	horký	k2eAgFnSc1d1	horká
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
tierra	tierra	k1gFnSc1	tierra
caliente	calient	k1gInSc5	calient
<g/>
)	)	kIx)	)
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
800	[number]	k4	800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
má	mít	k5eAaImIp3nS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
roční	roční	k2eAgFnSc4d1	roční
teplotu	teplota	k1gFnSc4	teplota
přes	přes	k7c4	přes
23	[number]	k4	23
°	°	k?	°
<g/>
C.	C.	kA	C.
Mírná	mírný	k2eAgFnSc1d1	mírná
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
tierra	tierra	k1gFnSc1	tierra
templada	templada	k1gFnSc1	templada
<g/>
)	)	kIx)	)
do	do	k7c2	do
1700	[number]	k4	1700
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
má	mít	k5eAaImIp3nS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
teplotu	teplota	k1gFnSc4	teplota
od	od	k7c2	od
17	[number]	k4	17
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
23	[number]	k4	23
°	°	k?	°
<g/>
C.	C.	kA	C.
Chladnou	chladný	k2eAgFnSc4d1	chladná
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
tierra	tierra	k6eAd1	tierra
fría	fría	k6eAd1	fría
<g/>
)	)	kIx)	)
do	do	k7c2	do
2500	[number]	k4	2500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
méně	málo	k6eAd2	málo
než	než	k8xS	než
17	[number]	k4	17
°	°	k?	°
<g/>
C	C	kA	C
střídá	střídat	k5eAaImIp3nS	střídat
pásmo	pásmo	k1gNnSc4	pásmo
věčného	věčný	k2eAgInSc2d1	věčný
sněhu	sníh	k1gInSc2	sníh
(	(	kIx(	(
<g/>
tierra	tierra	k1gFnSc1	tierra
helada	helada	k1gFnSc1	helada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
rok	rok	k1gInSc1	rok
na	na	k7c4	na
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
dešťů	dešť	k1gInPc2	dešť
–	–	k?	–
ty	ten	k3xDgInPc1	ten
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
maxima	maximum	k1gNnPc4	maximum
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
závisí	záviset	k5eAaImIp3nS	záviset
srážková	srážkový	k2eAgFnSc1d1	srážková
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
na	na	k7c6	na
pasátu	pasát	k1gInSc6	pasát
vanoucím	vanoucí	k2eAgInSc6d1	vanoucí
z	z	k7c2	z
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
na	na	k7c6	na
větru	vítr	k1gInSc6	vítr
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Pacifické	pacifický	k2eAgNnSc1d1	pacifické
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
sušší	suchý	k2eAgNnSc1d2	sušší
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
Kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
poloostrov	poloostrov	k1gInSc1	poloostrov
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
západního	západní	k2eAgNnSc2d1	západní
Mexika	Mexiko	k1gNnSc2	Mexiko
má	mít	k5eAaImIp3nS	mít
srážek	srážka	k1gFnPc2	srážka
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
tropickou	tropický	k2eAgFnSc7d1	tropická
flórou	flóra	k1gFnSc7	flóra
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
deštném	deštný	k2eAgInSc6d1	deštný
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
palem	palma	k1gFnPc2	palma
propletených	propletený	k2eAgFnPc2d1	propletená
liánami	liána	k1gFnPc7	liána
<g/>
,	,	kIx,	,
oživeny	oživen	k2eAgInPc1d1	oživen
pestře	pestro	k6eAd1	pestro
zbarvenými	zbarvený	k2eAgFnPc7d1	zbarvená
orchidejemi	orchidea	k1gFnPc7	orchidea
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
palma	palma	k1gFnSc1	palma
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
sisal	sisal	k1gInSc1	sisal
<g/>
,	,	kIx,	,
kokosovník	kokosovník	k1gInSc1	kokosovník
<g/>
,	,	kIx,	,
kakaovník	kakaovník	k1gInSc1	kakaovník
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
věčného	věčný	k2eAgInSc2d1	věčný
ledu	led	k1gInSc2	led
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
hor	hora	k1gFnPc2	hora
nalézáme	nalézat	k5eAaImIp1nP	nalézat
naopak	naopak	k6eAd1	naopak
severoamerickou	severoamerický	k2eAgFnSc4d1	severoamerická
holoarktickou	holoarktický	k2eAgFnSc4d1	holoarktický
flóru	flóra	k1gFnSc4	flóra
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
povrch	povrch	k1gInSc1	povrch
Mexika	Mexiko	k1gNnSc2	Mexiko
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
zalesněn	zalesněn	k2eAgMnSc1d1	zalesněn
<g/>
,	,	kIx,	,
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
zde	zde	k6eAd1	zde
roste	růst	k5eAaImIp3nS	růst
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
četně	četně	k6eAd1	četně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
cedr	cedr	k1gInSc1	cedr
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
a	a	k8xC	a
borovice	borovice	k1gFnSc1	borovice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
jedle	jedle	k1gFnSc2	jedle
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
dřevo	dřevo	k1gNnSc1	dřevo
tropických	tropický	k2eAgInPc2d1	tropický
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgInPc1d1	užívaný
v	v	k7c6	v
nábytkářském	nábytkářský	k2eAgInSc6d1	nábytkářský
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
mahagon	mahagon	k1gInSc1	mahagon
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
kampešek	kampeška	k1gFnPc2	kampeška
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
rostlinou	rostlina	k1gFnSc7	rostlina
celého	celý	k2eAgNnSc2d1	celé
Mexika	Mexiko	k1gNnSc2	Mexiko
je	být	k5eAaImIp3nS	být
kaktus	kaktus	k1gInSc1	kaktus
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
ve	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
Mexika	Mexiko	k1gNnSc2	Mexiko
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
a	a	k8xC	a
proso	proso	k1gNnSc1	proso
<g/>
.	.	kIx.	.
</s>
<s>
Severněji	severně	k6eAd2	severně
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pastviny	pastvina	k1gFnPc1	pastvina
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
tropického	tropický	k2eAgNnSc2d1	tropické
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
různé	různý	k2eAgFnSc2d1	různá
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
chilli	chilli	k1gNnSc4	chilli
a	a	k8xC	a
rajčata	rajče	k1gNnPc4	rajče
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
bavlníku	bavlník	k1gInSc2	bavlník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řek	řeka	k1gFnPc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
a	a	k8xC	a
Rio	Rio	k1gFnSc1	Rio
Grande	grand	k1gMnSc5	grand
<g/>
.	.	kIx.	.
</s>
<s>
Zvířena	zvířena	k1gFnSc1	zvířena
čítá	čítat	k5eAaImIp3nS	čítat
tisíce	tisíc	k4xCgInPc4	tisíc
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
nepříjemní	příjemný	k2eNgMnPc1d1	nepříjemný
jsou	být	k5eAaImIp3nP	být
moskyti	moskyt	k1gMnPc1	moskyt
<g/>
,	,	kIx,	,
přenášející	přenášející	k2eAgFnSc4d1	přenášející
malárii	malárie	k1gFnSc4	malárie
a	a	k8xC	a
žlutou	žlutý	k2eAgFnSc4d1	žlutá
zimnici	zimnice	k1gFnSc4	zimnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
nacházíme	nacházet	k5eAaImIp1nP	nacházet
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc4d1	žijící
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
pocházejí	pocházet	k5eAaImIp3nP	pocházet
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
vydry	vydra	k1gFnPc1	vydra
<g/>
,	,	kIx,	,
jeleni	jelen	k1gMnPc1	jelen
a	a	k8xC	a
jelencii	jelencie	k1gFnSc4	jelencie
<g/>
,	,	kIx,	,
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
pak	pak	k6eAd1	pak
jaguár	jaguár	k1gMnSc1	jaguár
<g/>
,	,	kIx,	,
pásovec	pásovec	k1gMnSc1	pásovec
<g/>
,	,	kIx,	,
mravenečník	mravenečník	k1gMnSc1	mravenečník
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
opic	opice	k1gFnPc2	opice
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
i	i	k9	i
dikobrazové	dikobraz	k1gMnPc1	dikobraz
<g/>
,	,	kIx,	,
různí	různý	k2eAgMnPc1d1	různý
hlodavci	hlodavec	k1gMnPc1	hlodavec
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
savců	savec	k1gMnPc2	savec
jako	jako	k8xS	jako
například	například	k6eAd1	například
tapír	tapír	k1gMnSc1	tapír
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
dovezli	dovézt	k5eAaPmAgMnP	dovézt
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
až	až	k9	až
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
divoké	divoký	k2eAgMnPc4d1	divoký
potomky	potomek	k1gMnPc4	potomek
španělských	španělský	k2eAgInPc2d1	španělský
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mnoho	mnoho	k4c1	mnoho
obojživelníků	obojživelník	k1gMnPc2	obojživelník
a	a	k8xC	a
plazů	plaz	k1gMnPc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc4d3	veliký
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
představují	představovat	k5eAaImIp3nP	představovat
četní	četný	k2eAgMnPc1d1	četný
a	a	k8xC	a
prudce	prudko	k6eAd1	prudko
jedovatí	jedovatý	k2eAgMnPc1d1	jedovatý
chřestýši	chřestýš	k1gMnPc1	chřestýš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ptačí	ptačí	k2eAgFnSc2d1	ptačí
říše	říš	k1gFnSc2	říš
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaImF	jmenovat
zase	zase	k9	zase
řadu	řada	k1gFnSc4	řada
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
od	od	k7c2	od
maličkých	maličký	k2eAgMnPc2d1	maličký
kolibříků	kolibřík	k1gMnPc2	kolibřík
až	až	k9	až
po	po	k7c4	po
supy	sup	k1gMnPc4	sup
a	a	k8xC	a
orly	orel	k1gMnPc4	orel
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
mexickým	mexický	k2eAgMnSc7d1	mexický
ptákem	pták	k1gMnSc7	pták
je	být	k5eAaImIp3nS	být
však	však	k9	však
patrně	patrně	k6eAd1	patrně
krocan	krocan	k1gMnSc1	krocan
<g/>
,	,	kIx,	,
divoký	divoký	k2eAgMnSc1d1	divoký
předek	předek	k1gMnSc1	předek
dnes	dnes	k6eAd1	dnes
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
rozšířeného	rozšířený	k2eAgMnSc2d1	rozšířený
krocana	krocan	k1gMnSc2	krocan
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
zejména	zejména	k9	zejména
chovem	chov	k1gInSc7	chov
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
s	s	k7c7	s
Mexickým	mexický	k2eAgInSc7d1	mexický
zálivem	záliv	k1gInSc7	záliv
se	se	k3xPyFc4	se
loví	lovit	k5eAaImIp3nP	lovit
zejména	zejména	k9	zejména
krabi	krab	k1gMnPc1	krab
a	a	k8xC	a
langusty	langusta	k1gFnPc1	langusta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
krabi	krab	k1gMnPc1	krab
a	a	k8xC	a
ústřice	ústřice	k1gFnPc1	ústřice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Kalifornského	kalifornský	k2eAgInSc2d1	kalifornský
zálivu	záliv	k1gInSc2	záliv
žije	žít	k5eAaImIp3nS	žít
kriticky	kriticky	k6eAd1	kriticky
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
sviňucha	sviňucha	k1gFnSc1	sviňucha
kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
funguje	fungovat	k5eAaImIp3nS	fungovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
různých	různý	k2eAgNnPc2d1	různé
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
státní	státní	k2eAgFnSc1d1	státní
organizace	organizace	k1gFnSc1	organizace
Národní	národní	k2eAgFnSc2d1	národní
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
přírodní	přírodní	k2eAgNnPc4d1	přírodní
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Comisión	Comisión	k1gMnSc1	Comisión
nacional	nacionat	k5eAaImAgMnS	nacionat
de	de	k?	de
áreas	áreas	k1gMnSc1	áreas
naturales	naturales	k1gMnSc1	naturales
protegidas	protegidas	k1gMnSc1	protegidas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
existovalo	existovat	k5eAaImAgNnS	existovat
celkem	celek	k1gInSc7	celek
174	[number]	k4	174
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
pod	pod	k7c4	pod
jistý	jistý	k2eAgInSc4d1	jistý
stupeň	stupeň	k1gInSc4	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
41	[number]	k4	41
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
,	,	kIx,	,
67	[number]	k4	67
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
5	[number]	k4	5
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
8	[number]	k4	8
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
pro	pro	k7c4	pro
ochrana	ochrana	k1gFnSc1	ochrana
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
35	[number]	k4	35
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flory	flora	k1gFnSc2	flora
a	a	k8xC	a
18	[number]	k4	18
útočišť	útočiště	k1gNnPc2	útočiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
výměra	výměra	k1gFnSc1	výměra
všech	všecek	k3xTgNnPc2	všecek
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
253	[number]	k4	253
848	[number]	k4	848
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
figuruje	figurovat	k5eAaImIp3nS	figurovat
zároveň	zároveň	k6eAd1	zároveň
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	Unesco	k1gNnSc1	Unesco
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
Vizcaíno	Vizcaína	k1gFnSc5	Vizcaína
<g/>
,	,	kIx,	,
Sian	Sian	k1gMnSc1	Sian
Ka	Ka	k1gMnSc1	Ka
<g/>
'	'	kIx"	'
<g/>
an	an	k?	an
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
Kalifornského	kalifornský	k2eAgInSc2d1	kalifornský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
biosférická	biosférický	k2eAgFnSc1d1	biosférická
rezervace	rezervace	k1gFnSc1	rezervace
monarchy	monarcha	k1gMnSc2	monarcha
stěhovavého	stěhovavý	k2eAgMnSc2d1	stěhovavý
a	a	k8xC	a
biosférická	biosférický	k2eAgFnSc1d1	biosférická
rezervace	rezervace	k1gFnSc1	rezervace
El	Ela	k1gFnPc2	Ela
Pinacate	Pinacat	k1gInSc5	Pinacat
a	a	k8xC	a
Gran	Grana	k1gFnPc2	Grana
Desierto	Desierta	k1gFnSc5	Desierta
de	de	k?	de
Altar	Altar	k1gInSc1	Altar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
mají	mít	k5eAaImIp3nP	mít
všichni	všechen	k3xTgMnPc1	všechen
dospělí	dospělí	k1gMnPc1	dospělí
starší	starý	k2eAgMnPc1d2	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
i	i	k8xC	i
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
se	se	k3xPyFc4	se
na	na	k7c4	na
jediné	jediný	k2eAgNnSc4d1	jediné
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
Národní	národní	k2eAgInSc1d1	národní
kongres	kongres	k1gInSc1	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
500	[number]	k4	500
členů	člen	k1gMnPc2	člen
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
voleny	volen	k2eAgMnPc4d1	volen
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
hlasů	hlas	k1gInPc2	hlas
získaných	získaný	k2eAgInPc2d1	získaný
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
na	na	k7c6	na
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
64	[number]	k4	64
senátorů	senátor	k1gMnPc2	senátor
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
si	se	k3xPyFc3	se
volí	volit	k5eAaImIp3nS	volit
vlastního	vlastní	k2eAgMnSc4d1	vlastní
guvernéra	guvernér	k1gMnSc4	guvernér
a	a	k8xC	a
zákonodárný	zákonodárný	k2eAgInSc4d1	zákonodárný
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
guvernéra	guvernér	k1gMnSc4	guvernér
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
celosvětových	celosvětový	k2eAgFnPc2d1	celosvětová
organizací	organizace	k1gFnPc2	organizace
jako	jako	k9	jako
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
APEC	APEC	kA	APEC
<g/>
,	,	kIx,	,
Interpol	interpol	k1gInSc1	interpol
<g/>
,	,	kIx,	,
UNESCO	UNESCO	kA	UNESCO
atd.	atd.	kA	atd.
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zapojené	zapojený	k2eAgNnSc1d1	zapojené
do	do	k7c2	do
několika	několik	k4yIc2	několik
organizací	organizace	k1gFnPc2	organizace
působících	působící	k2eAgFnPc2d1	působící
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Latinskoamerické	latinskoamerický	k2eAgNnSc1d1	latinskoamerické
integrační	integrační	k2eAgNnSc1d1	integrační
sdružení	sdružení	k1gNnSc1	sdružení
<g/>
,	,	kIx,	,
Pacifická	pacifický	k2eAgFnSc1d1	Pacifická
aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česko-mexické	českoexický	k2eAgInPc4d1	česko-mexický
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
navázalo	navázat	k5eAaPmAgNnS	navázat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
vztahy	vztah	k1gInPc1	vztah
byly	být	k5eAaImAgInP	být
nakrátko	nakrátko	k6eAd1	nakrátko
přerušeny	přerušit	k5eAaPmNgInP	přerušit
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1939	[number]	k4	1939
a	a	k8xC	a
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
prezident	prezident	k1gMnSc1	prezident
Lázaro	Lázara	k1gFnSc5	Lázara
Cárdenas	Cárdenas	k1gMnSc1	Cárdenas
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
neznámou	známý	k2eNgFnSc4d1	neznámá
ulici	ulice	k1gFnSc4	ulice
jménem	jméno	k1gNnSc7	jméno
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
pomník	pomník	k1gInSc4	pomník
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Avenida	Avenida	k1gFnSc1	Avenida
Presidente	president	k1gMnSc5	president
Masaryk	Masaryk	k1gMnSc1	Masaryk
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
ulic	ulice	k1gFnPc2	ulice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
nejdražších	drahý	k2eAgFnPc2d3	nejdražší
obchodních	obchodní	k2eAgFnPc2d1	obchodní
tříd	třída	k1gFnPc2	třída
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c4	v
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k6eAd1	México
a	a	k8xC	a
honorátní	honorátní	k2eAgInPc1d1	honorátní
konzuláty	konzulát	k1gInPc1	konzulát
v	v	k7c6	v
Guadalajaře	Guadalajara	k1gFnSc6	Guadalajara
<g/>
,	,	kIx,	,
Tijuaně	Tijuaň	k1gFnPc1	Tijuaň
<g/>
,	,	kIx,	,
Monterrey	Monterrea	k1gFnPc1	Monterrea
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc1	Mexiko
svou	svůj	k3xOyFgFnSc4	svůj
ambasádu	ambasáda	k1gFnSc4	ambasáda
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Státy	stát	k1gInPc1	stát
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
31	[number]	k4	31
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
a	a	k8xC	a
distrikt	distrikt	k1gInSc1	distrikt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
názvy	název	k1gInPc1	název
států	stát	k1gInPc2	stát
a	a	k8xC	a
měst	město	k1gNnPc2	město
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnPc4	jméno
historických	historický	k2eAgFnPc2d1	historická
osobností	osobnost	k1gFnPc2	osobnost
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
jsou	být	k5eAaImIp3nP	být
indiánského	indiánský	k2eAgMnSc4d1	indiánský
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Zleva	zleva	k6eAd1	zleva
zeleným	zelené	k1gNnSc7	zelené
(	(	kIx(	(
<g/>
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bílým	bílý	k1gMnSc7	bílý
(	(	kIx(	(
<g/>
čistota	čistota	k1gFnSc1	čistota
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
a	a	k8xC	a
červeným	červená	k1gFnPc3	červená
(	(	kIx(	(
<g/>
jednota	jednota	k1gFnSc1	jednota
spojení	spojení	k1gNnSc2	spojení
míšenecké	míšenecký	k2eAgFnSc2d1	míšenecký
<g/>
,	,	kIx,	,
indiánské	indiánský	k2eAgFnSc2d1	indiánská
a	a	k8xC	a
španělské	španělský	k2eAgFnSc2d1	španělská
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
tvoří	tvořit	k5eAaImIp3nS	tvořit
hnědě	hnědě	k6eAd1	hnědě
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
orel	orel	k1gMnSc1	orel
stojící	stojící	k2eAgMnSc1d1	stojící
na	na	k7c6	na
nopálovém	nopálový	k2eAgInSc6d1	nopálový
kaktusu	kaktus	k1gInSc6	kaktus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
roste	růst	k5eAaImIp3nS	růst
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
jezera	jezero	k1gNnSc2	jezero
<g/>
;	;	kIx,	;
orel	orel	k1gMnSc1	orel
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
zeleného	zelený	k2eAgMnSc2d1	zelený
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
půlkruhu	půlkruh	k1gInSc2	půlkruh
prohnuté	prohnutý	k2eAgFnSc2d1	prohnutá
ratolesti	ratolest	k1gFnSc2	ratolest
dubu	dub	k1gInSc2	dub
a	a	k8xC	a
vavřínu	vavřín	k1gInSc2	vavřín
svázané	svázaný	k2eAgNnSc4d1	svázané
stužkou	stužka	k1gFnSc7	stužka
v	v	k7c6	v
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
starou	starý	k2eAgFnSc4d1	stará
aztéckou	aztécký	k2eAgFnSc4d1	aztécká
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
putující	putující	k2eAgInSc1d1	putující
indiánský	indiánský	k2eAgInSc1d1	indiánský
lid	lid	k1gInSc1	lid
měl	mít	k5eAaImAgInS	mít
nalézt	nalézt	k5eAaBmF	nalézt
nový	nový	k2eAgInSc1d1	nový
domov	domov	k1gInSc1	domov
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
uprostřed	uprostřed	k7c2	uprostřed
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvidí	uvidět	k5eAaPmIp3nS	uvidět
tuto	tento	k3xDgFnSc4	tento
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
Texcoco	Texcoco	k6eAd1	Texcoco
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
založen	založen	k2eAgInSc1d1	založen
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
troskách	troska	k1gFnPc6	troska
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
dnešní	dnešní	k2eAgNnSc1d1	dnešní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
Méxiko	Méxika	k1gFnSc5	Méxika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
ekonomikou	ekonomika	k1gFnSc7	ekonomika
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
po	po	k7c6	po
USA	USA	kA	USA
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kombinaci	kombinace	k1gFnSc4	kombinace
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
založená	založený	k2eAgFnSc1d1	založená
především	především	k9	především
na	na	k7c6	na
exportu	export	k1gInSc6	export
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
Mexika	Mexiko	k1gNnSc2	Mexiko
umožnily	umožnit	k5eAaPmAgFnP	umožnit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyšší	vysoký	k2eAgFnSc4d2	vyšší
konkurenci	konkurence	k1gFnSc4	konkurence
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mořských	mořský	k2eAgInPc2d1	mořský
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
telekomunikačních	telekomunikační	k2eAgNnPc2d1	telekomunikační
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc3	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
distribuci	distribuce	k1gFnSc4	distribuce
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
modernizace	modernizace	k1gFnSc2	modernizace
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
Mexika	Mexiko	k1gNnSc2	Mexiko
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
dohod	dohoda	k1gFnPc2	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
NAFTA	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
dohody	dohoda	k1gFnPc1	dohoda
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
několika	několik	k4yIc7	několik
státy	stát	k1gInPc7	stát
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
směřovalo	směřovat	k5eAaImAgNnS	směřovat
např.	např.	kA	např.
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
čtvrtletí	čtvrtletí	k1gNnSc4	čtvrtletí
2014	[number]	k4	2014
celých	celý	k2eAgInPc2d1	celý
80	[number]	k4	80
<g/>
%	%	kIx~	%
mexického	mexický	k2eAgInSc2d1	mexický
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
15	[number]	k4	15
600	[number]	k4	600
$	$	kIx~	$
za	za	k7c4	za
rok	rok	k1gInSc4	rok
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
pracovalo	pracovat	k5eAaImAgNnS	pracovat
13	[number]	k4	13
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
24	[number]	k4	24
<g/>
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
služeb	služba	k1gFnPc2	služba
62	[number]	k4	62
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
exportní	exportní	k2eAgNnSc1d1	exportní
zboží	zboží	k1gNnSc1	zboží
je	být	k5eAaImIp3nS	být
surová	surový	k2eAgFnSc1d1	surová
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
součásti	součást	k1gFnPc1	součást
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
zboží	zboží	k1gNnSc1	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
oplývá	oplývat	k5eAaImIp3nS	oplývat
značným	značný	k2eAgNnSc7d1	značné
přírodním	přírodní	k2eAgNnSc7d1	přírodní
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koloniální	koloniální	k2eAgFnSc6d1	koloniální
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
ceněno	cenit	k5eAaImNgNnS	cenit
zvlášť	zvlášť	k6eAd1	zvlášť
stříbro	stříbro	k1gNnSc1	stříbro
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
těžilo	těžit	k5eAaImAgNnS	těžit
se	se	k3xPyFc4	se
i	i	k9	i
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
a	a	k8xC	a
železo	železo	k1gNnSc1	železo
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrná	k1gFnSc2	stříbrná
rudy	ruda	k1gFnPc1	ruda
našli	najít	k5eAaPmAgMnP	najít
geologové	geolog	k1gMnPc1	geolog
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
bohatými	bohatý	k2eAgInPc7d1	bohatý
doly	dol	k1gInPc7	dol
vynikají	vynikat	k5eAaImIp3nP	vynikat
zejména	zejména	k9	zejména
státy	stát	k1gInPc1	stát
Zacatecas	Zacatecasa	k1gFnPc2	Zacatecasa
<g/>
,	,	kIx,	,
Durango	Durango	k1gMnSc1	Durango
<g/>
,	,	kIx,	,
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
<g/>
,	,	kIx,	,
Guanajuato	Guanajuat	k2eAgNnSc1d1	Guanajuato
a	a	k8xC	a
Chihuahua	Chihuahua	k1gFnSc1	Chihuahua
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
mexický	mexický	k2eAgInSc1d1	mexický
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
produkci	produkce	k1gFnSc6	produkce
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
%	%	kIx~	%
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc1	Mexiko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
naleziště	naleziště	k1gNnSc4	naleziště
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
částečně	částečně	k6eAd1	částečně
vyčerpána	vyčerpán	k2eAgFnSc1d1	vyčerpána
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Durango	Durango	k6eAd1	Durango
<g/>
,	,	kIx,	,
Sonora	sonora	k1gFnSc1	sonora
<g/>
,	,	kIx,	,
Chihuahua	Chihuahua	k1gFnSc1	Chihuahua
a	a	k8xC	a
Querétaro	Querétara	k1gFnSc5	Querétara
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
kovových	kovový	k2eAgFnPc2d1	kovová
rud	ruda	k1gFnPc2	ruda
má	mít	k5eAaImIp3nS	mít
značnou	značný	k2eAgFnSc4d1	značná
důležitost	důležitost	k1gFnSc4	důležitost
ruda	rudo	k1gNnSc2	rudo
zinková	zinkový	k2eAgFnSc1d1	zinková
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
největší	veliký	k2eAgFnSc2d3	veliký
pánve	pánev	k1gFnSc2	pánev
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
stejných	stejný	k2eAgInPc6d1	stejný
státech	stát	k1gInPc6	stát
jako	jako	k8xC	jako
doly	dol	k1gInPc4	dol
na	na	k7c4	na
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
olovnaté	olovnatý	k2eAgFnPc4d1	olovnatá
a	a	k8xC	a
měděné	měděný	k2eAgFnPc4d1	měděná
rudy	ruda	k1gFnPc4	ruda
<g/>
,	,	kIx,	,
známé	známý	k2eAgMnPc4d1	známý
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
intenzivnímu	intenzivní	k2eAgInSc3d1	intenzivní
rozvoji	rozvoj	k1gInSc3	rozvoj
těžby	těžba	k1gFnSc2	těžba
železných	železný	k2eAgFnPc2d1	železná
rud	ruda	k1gFnPc2	ruda
v	v	k7c6	v
nově	nově	k6eAd1	nově
objevených	objevený	k2eAgNnPc6d1	objevené
ložiscích	ložisko	k1gNnPc6	ložisko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Cerro	Cerro	k1gNnSc1	Cerro
del	del	k?	del
Mercado	Mercada	k1gFnSc5	Mercada
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Mamen	Mamna	k1gFnPc2	Mamna
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Perla	perla	k1gFnSc1	perla
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Joven	Jovna	k1gFnPc2	Jovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ruda	ruda	k1gFnSc1	ruda
tu	tu	k6eAd1	tu
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
60	[number]	k4	60
%	%	kIx~	%
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
i	i	k8xC	i
bohatá	bohatý	k2eAgNnPc4d1	bohaté
naleziště	naleziště	k1gNnSc4	naleziště
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
těžba	těžba	k1gFnSc1	těžba
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
grafitu	grafit	k1gInSc2	grafit
<g/>
,	,	kIx,	,
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
<g/>
,	,	kIx,	,
wolfram	wolfram	k1gInSc1	wolfram
<g/>
,	,	kIx,	,
kadmium	kadmium	k1gNnSc1	kadmium
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
nerostů	nerost	k1gInPc2	nerost
a	a	k8xC	a
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
nerostné	nerostný	k2eAgNnSc1d1	nerostné
bohatství	bohatství	k1gNnSc1	bohatství
představuje	představovat	k5eAaImIp3nS	představovat
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
těžbě	těžba	k1gFnSc6	těžba
závisí	záviset	k5eAaImIp3nS	záviset
příjem	příjem	k1gInSc4	příjem
deviz	deviza	k1gFnPc2	deviza
i	i	k8xC	i
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgInSc1d1	rozvíjející
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
všechny	všechen	k3xTgInPc1	všechen
vrty	vrt	k1gInPc1	vrt
<g/>
,	,	kIx,	,
soustředěné	soustředěný	k2eAgInPc1d1	soustředěný
zejména	zejména	k9	zejména
na	na	k7c6	na
atlantském	atlantský	k2eAgNnSc6d1	Atlantské
pobřeží	pobřeží	k1gNnSc6	pobřeží
včetně	včetně	k7c2	včetně
pobřežního	pobřežní	k2eAgInSc2d1	pobřežní
šelfu	šelf	k1gInSc2	šelf
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
společnost	společnost	k1gFnSc1	společnost
PEMEX	PEMEX	kA	PEMEX
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
Mexika	Mexiko	k1gNnSc2	Mexiko
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
představuje	představovat	k5eAaImIp3nS	představovat
asi	asi	k9	asi
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc1d2	veliký
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
produkci	produkce	k1gFnSc4	produkce
odebírají	odebírat	k5eAaImIp3nP	odebírat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	nejvíc	k6eAd1	nejvíc
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
kolem	kolem	k7c2	kolem
měst	město	k1gNnPc2	město
Reynosa	Reynosa	k1gFnSc1	Reynosa
a	a	k8xC	a
Poza	Poza	k?	Poza
Rica	Rica	k1gMnSc1	Rica
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Tabasco	Tabasco	k1gNnSc1	Tabasco
<g/>
.	.	kIx.	.
</s>
<s>
Převažuje	převažovat	k5eAaImIp3nS	převažovat
průmysl	průmysl	k1gInSc4	průmysl
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
a	a	k8xC	a
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
.	.	kIx.	.
</s>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
sídlí	sídlet	k5eAaImIp3nS	sídlet
zejména	zejména	k9	zejména
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Coatzacoalcos	Coatzacoalcosa	k1gFnPc2	Coatzacoalcosa
a	a	k8xC	a
Tampico	Tampico	k6eAd1	Tampico
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Salina	salina	k1gFnSc1	salina
Cruz	Cruza	k1gFnPc2	Cruza
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
Salamanca	Salamanca	k1gMnSc1	Salamanca
ve	v	k7c6	v
středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Strojírenským	strojírenský	k2eAgInSc7d1	strojírenský
průmyslem	průmysl	k1gInSc7	průmysl
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
Veracruz	Veracruz	k1gMnSc1	Veracruz
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Puebla	Puebla	k1gFnSc6	Puebla
ve	v	k7c6	v
středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnSc2	město
jako	jako	k8xS	jako
Mexicali	Mexicali	k1gFnSc2	Mexicali
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc4	Ciudad
Juárez	Juáreza	k1gFnPc2	Juáreza
<g/>
,	,	kIx,	,
Chihuahua	Chihuahua	k1gMnSc1	Chihuahua
<g/>
,	,	kIx,	,
Torreón	Torreón	k1gMnSc1	Torreón
<g/>
,	,	kIx,	,
León	León	k1gMnSc1	León
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
potravinářstvím	potravinářství	k1gNnSc7	potravinářství
<g/>
,	,	kIx,	,
textilním	textilní	k2eAgInSc7d1	textilní
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
hutnictvím	hutnictví	k1gNnSc7	hutnictví
<g/>
.	.	kIx.	.
</s>
<s>
Monclova	Monclův	k2eAgFnSc1d1	Monclova
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Veracruz	Veracruz	k1gInSc4	Veracruz
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
hutní	hutní	k2eAgNnPc1d1	hutní
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgNnSc7	takový
textilním	textilní	k2eAgNnSc7d1	textilní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Mérida	Mérida	k1gFnSc1	Mérida
na	na	k7c6	na
severu	sever	k1gInSc6	sever
poloostrova	poloostrov	k1gInSc2	poloostrov
Yucatán	Yucatán	k1gMnSc1	Yucatán
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Mexika	Mexiko	k1gNnSc2	Mexiko
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
ze	z	k7c2	z
14,3	[number]	k4	14,3
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
123	[number]	k4	123
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
venkovského	venkovský	k2eAgNnSc2d1	venkovské
a	a	k8xC	a
městského	městský	k2eAgNnSc2d1	Městské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
činí	činit	k5eAaImIp3nS	činit
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
57	[number]	k4	57
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
1	[number]	k4	1
km2	km2	k4	km2
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
hustota	hustota	k1gFnSc1	hustota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgMnSc1d3	nejmenší
pak	pak	k6eAd1	pak
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Baja	Baj	k1gInSc2	Baj
California	Californium	k1gNnSc2	Californium
Sur	Sur	k1gFnSc2	Sur
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Valle	Valle	k1gFnSc2	Valle
de	de	k?	de
México	México	k1gMnSc1	México
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
centrem	centrum	k1gNnSc7	centrum
je	být	k5eAaImIp3nS	být
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mesticové	mestic	k1gMnPc1	mestic
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
míšenci	míšenec	k1gMnPc1	míšenec
Indiánů	Indián	k1gMnPc2	Indián
a	a	k8xC	a
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zastoupeni	zastoupit	k5eAaPmNgMnP	zastoupit
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
indiáni	indián	k1gMnPc1	indián
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
potomci	potomek	k1gMnPc1	potomek
Španělů	Španěl	k1gMnPc2	Španěl
zvaní	zvaný	k2eAgMnPc1d1	zvaný
kreolové	kreol	k1gMnPc1	kreol
<g/>
)	)	kIx)	)
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Indiánů	Indián	k1gMnPc2	Indián
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
<g/>
:	:	kIx,	:
Nahuů	Nahu	k1gMnPc2	Nahu
(	(	kIx(	(
<g/>
potomci	potomek	k1gMnPc1	potomek
Aztéků	Azték	k1gMnPc2	Azték
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mayů	May	k1gMnPc2	May
a	a	k8xC	a
Zapoteců	Zapotece	k1gMnPc2	Zapotece
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
<g/>
:	:	kIx,	:
Ixcateců	Ixcatece	k1gMnPc2	Ixcatece
a	a	k8xC	a
Kiliwaů	Kiliwa	k1gMnPc2	Kiliwa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vymření	vymření	k1gNnSc2	vymření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
89	[number]	k4	89
%	%	kIx~	%
katolíků	katolík	k1gMnPc2	katolík
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
východních	východní	k2eAgInPc2d1	východní
<g/>
)	)	kIx)	)
a	a	k8xC	a
6	[number]	k4	6
%	%	kIx~	%
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
přicházejí	přicházet	k5eAaImIp3nP	přicházet
lidé	člověk	k1gMnPc1	člověk
především	především	k6eAd1	především
z	z	k7c2	z
Guatemaly	Guatemala	k1gFnSc2	Guatemala
a	a	k8xC	a
Hondurasu	Honduras	k1gInSc2	Honduras
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
lepšími	dobrý	k2eAgFnPc7d2	lepší
životními	životní	k2eAgFnPc7d1	životní
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Mexičané	Mexičan	k1gMnPc1	Mexičan
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
obyvatelé	obyvatel	k1gMnPc1	obyvatel
celé	celý	k2eAgFnSc2d1	celá
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
mohutně	mohutně	k6eAd1	mohutně
migrují	migrovat	k5eAaImIp3nP	migrovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
20	[number]	k4	20
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
:	:	kIx,	:
Úřední	úřední	k2eAgInSc1d1	úřední
jazyk	jazyk	k1gInSc1	jazyk
Mexika	Mexiko	k1gNnSc2	Mexiko
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
s	s	k7c7	s
totožnou	totožný	k2eAgFnSc7d1	totožná
gramatikou	gramatika	k1gFnSc7	gramatika
jako	jako	k8xS	jako
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Latinskoamerické	latinskoamerický	k2eAgFnPc1d1	latinskoamerická
španělštiny	španělština	k1gFnPc1	španělština
ovšem	ovšem	k9	ovšem
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
specifika	specifikon	k1gNnPc4	specifikon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c4	mezi
nejvýraznější	výrazný	k2eAgNnSc4d3	nejvýraznější
patří	patřit	k5eAaImIp3nS	patřit
nepoužívání	nepoužívání	k1gNnSc4	nepoužívání
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
osoby	osoba	k1gFnPc1	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
španělština	španělština	k1gFnSc1	španělština
má	mít	k5eAaImIp3nS	mít
slovník	slovník	k1gInSc4	slovník
obohacen	obohacen	k2eAgInSc4d1	obohacen
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
slov	slovo	k1gNnPc2	slovo
převzatých	převzatý	k2eAgNnPc2d1	převzaté
z	z	k7c2	z
indiánských	indiánský	k2eAgInPc2d1	indiánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
celkem	celkem	k6eAd1	celkem
62	[number]	k4	62
viz	vidět	k5eAaImRp2nS	vidět
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
aztéčtina	aztéčtina	k1gFnSc1	aztéčtina
–	–	k?	–
nahuatl	nahuatnout	k5eAaPmAgMnS	nahuatnout
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pak	pak	k6eAd1	pak
mayština	mayština	k1gFnSc1	mayština
<g/>
,	,	kIx,	,
mixtéčtina	mixtéčtina	k1gFnSc1	mixtéčtina
a	a	k8xC	a
zapotečtina	zapotečtina	k1gFnSc1	zapotečtina
<g/>
.	.	kIx.	.
</s>
<s>
Aztécký	aztécký	k2eAgMnSc1d1	aztécký
nahuatl	nahuatnout	k5eAaPmAgMnS	nahuatnout
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
rozeznatelný	rozeznatelný	k2eAgInSc1d1	rozeznatelný
příponami	přípona	k1gFnPc7	přípona
-tl	l	k?	-tl
(	(	kIx(	(
<g/>
známá	známý	k2eAgFnSc1d1	známá
sopka	sopka	k1gFnSc1	sopka
Popocatépetl	Popocatépetl	k1gFnSc2	Popocatépetl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
aztéčtiny	aztéčtina	k1gFnSc2	aztéčtina
byla	být	k5eAaImAgFnS	být
převzata	převzat	k2eAgFnSc1d1	převzata
i	i	k9	i
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
šokolatl	šokolatnout	k5eAaPmAgInS	šokolatnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
naše	náš	k3xOp1gFnSc1	náš
známá	známý	k2eAgFnSc1d1	známá
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
,	,	kIx,	,
papričky	paprička	k1gFnPc1	paprička
"	"	kIx"	"
<g/>
chilli	chilli	k1gNnSc1	chilli
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
řeknou	říct	k5eAaPmIp3nP	říct
také	také	k9	také
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
sousloví	sousloví	k1gNnSc1	sousloví
"	"	kIx"	"
<g/>
tomatová	tomatový	k2eAgFnSc1d1	tomatová
<g/>
"	"	kIx"	"
omáčka	omáčka	k1gFnSc1	omáčka
(	(	kIx(	(
<g/>
z	z	k7c2	z
aztéckého	aztécký	k2eAgInSc2d1	aztécký
"	"	kIx"	"
<g/>
tomatl	tomatnout	k5eAaPmAgInS	tomatnout
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
rajče	rajče	k1gNnSc4	rajče
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
Češi	Čech	k1gMnPc1	Čech
používají	používat	k5eAaImIp3nP	používat
méně	málo	k6eAd2	málo
častěji	často	k6eAd2	často
než	než	k8xS	než
Němci	Němec	k1gMnPc1	Němec
nebo	nebo	k8xC	nebo
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
indiánských	indiánský	k2eAgMnPc2d1	indiánský
obyvatel	obyvatel	k1gMnPc2	obyvatel
Mexika	Mexiko	k1gNnSc2	Mexiko
v	v	k7c6	v
předkolumbovském	předkolumbovský	k2eAgNnSc6d1	předkolumbovské
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
prostoupen	prostoupit	k5eAaPmNgInS	prostoupit
uctíváním	uctívání	k1gNnSc7	uctívání
obrovského	obrovský	k2eAgInSc2d1	obrovský
panteonu	panteon	k1gInSc2	panteon
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
počest	počest	k1gFnSc4	počest
se	se	k3xPyFc4	se
budovaly	budovat	k5eAaImAgFnP	budovat
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
stavby	stavba	k1gFnPc1	stavba
a	a	k8xC	a
kultovní	kultovní	k2eAgNnPc1d1	kultovní
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
vydržovalo	vydržovat	k5eAaImAgNnS	vydržovat
početnou	početný	k2eAgFnSc4d1	početná
vrstvu	vrstva	k1gFnSc4	vrstva
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
prostředníky	prostředník	k1gInPc4	prostředník
mezi	mezi	k7c7	mezi
prostým	prostý	k2eAgInSc7d1	prostý
lidem	lid	k1gInSc7	lid
a	a	k8xC	a
bohy	bůh	k1gMnPc4	bůh
<g/>
,	,	kIx,	,
putovalo	putovat	k5eAaImAgNnS	putovat
do	do	k7c2	do
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
posvátných	posvátný	k2eAgNnPc2d1	posvátné
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
pořádalo	pořádat	k5eAaImAgNnS	pořádat
náboženské	náboženský	k2eAgFnPc4d1	náboženská
slavnosti	slavnost	k1gFnPc4	slavnost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc2	jejichž
součástí	součást	k1gFnPc2	součást
bývaly	bývat	k5eAaImAgFnP	bývat
i	i	k9	i
lidské	lidský	k2eAgFnPc1d1	lidská
oběti	oběť	k1gFnPc1	oběť
na	na	k7c4	na
usmíření	usmíření	k1gNnPc4	usmíření
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
conquisty	conquista	k1gFnPc4	conquista
a	a	k8xC	a
v	v	k7c6	v
koloniálním	koloniální	k2eAgNnSc6d1	koloniální
období	období	k1gNnSc6	období
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
násilím	násilí	k1gNnSc7	násilí
obraceli	obracet	k5eAaImAgMnP	obracet
Indiány	Indián	k1gMnPc4	Indián
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
sem	sem	k6eAd1	sem
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
přicházeli	přicházet	k5eAaImAgMnP	přicházet
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
mniši	mnich	k1gMnPc1	mnich
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
náboženských	náboženský	k2eAgInPc2d1	náboženský
řádů	řád	k1gInPc2	řád
–	–	k?	–
františkáni	františkán	k1gMnPc1	františkán
<g/>
,	,	kIx,	,
dominikáni	dominikán	k1gMnPc1	dominikán
<g/>
,	,	kIx,	,
augustiniáni	augustinián	k1gMnPc1	augustinián
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
jezuité	jezuita	k1gMnPc1	jezuita
–	–	k?	–
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
Indiány	Indián	k1gMnPc4	Indián
vzdělat	vzdělat	k5eAaPmF	vzdělat
v	v	k7c6	v
základech	základ	k1gInPc6	základ
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
oslavy	oslava	k1gFnPc1	oslava
kultu	kult	k1gInSc2	kult
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Guadalupské	Guadalupský	k2eAgFnSc2d1	Guadalupská
<g/>
,	,	kIx,	,
patronky	patronka	k1gFnPc4	patronka
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
jejího	její	k3xOp3gNnSc2	její
prvního	první	k4xOgNnSc2	první
údajného	údajný	k2eAgNnSc2d1	údajné
zjevení	zjevení	k1gNnSc2	zjevení
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Mexiko	Mexiko	k1gNnSc4	Mexiko
zemí	zem	k1gFnSc7	zem
katolickou	katolický	k2eAgFnSc7d1	katolická
–	–	k?	–
převažují	převažovat	k5eAaImIp3nP	převažovat
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
východní	východní	k2eAgFnSc2d1	východní
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
arménští	arménský	k2eAgMnPc1d1	arménský
katolíci	katolík	k1gMnPc1	katolík
–	–	k?	–
Apoštolský	apoštolský	k2eAgInSc4d1	apoštolský
exarchát	exarchát	k1gInSc4	exarchát
pro	pro	k7c4	pro
Latin	latina	k1gFnPc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Argentinu	Argentina	k1gFnSc4	Argentina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
farnosti	farnost	k1gFnPc1	farnost
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
stagnuje	stagnovat	k5eAaImIp3nS	stagnovat
<g/>
:	:	kIx,	:
2015	[number]	k4	2015
–	–	k?	–
12	[number]	k4	12
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
r.	r.	kA	r.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
*	*	kIx~	*
2	[number]	k4	2
<g/>
)	)	kIx)	)
maronité	maronitý	k2eAgFnSc2d1	maronitý
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc2	eparchie
Nuestra	Nuestr	k1gMnSc2	Nuestr
Señ	Señ	k1gMnSc2	Señ
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Naší	náš	k3xOp1gFnSc7	náš
Paní	paní	k1gFnSc7	paní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
)	)	kIx)	)
de	de	k?	de
los	los	k1gInSc1	los
Martires	Martires	k1gInSc1	Martires
de	de	k?	de
Libano	Libana	k1gFnSc5	Libana
(	(	kIx(	(
<g/>
libanonských	libanonský	k2eAgMnPc2d1	libanonský
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
r.	r.	kA	r.
2015	[number]	k4	2015
–	–	k?	–
156	[number]	k4	156
000	[number]	k4	000
věřících	věřící	k1gFnPc2	věřící
–	–	k?	–
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
:	:	kIx,	:
2014	[number]	k4	2014
–	–	k?	–
154	[number]	k4	154
400	[number]	k4	400
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
–	–	k?	–
150	[number]	k4	150
800	[number]	k4	800
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
150	[number]	k4	150
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
je	být	k5eAaImIp3nS	být
i	i	k9	i
Carlos	Carlos	k1gMnSc1	Carlos
Slim	Slim	k1gMnSc1	Slim
Helú	Helú	k1gMnSc1	Helú
(	(	kIx(	(
<g/>
VIZ	vidět	k5eAaImRp2nS	vidět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
*	*	kIx~	*
3	[number]	k4	3
<g/>
)	)	kIx)	)
řeckokatolíci-melchité	řeckokatolícielchitý	k2eAgFnSc2d1	řeckokatolíci-melchitý
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc2	eparchie
Nuestra	Nuestr	k1gMnSc2	Nuestr
Señ	Señ	k1gFnSc2	Señ
(	(	kIx(	(
<g/>
Naší	náš	k3xOp1gFnSc7	náš
Paní	paní	k1gFnSc7	paní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
P.	P.	kA	P.
Marie	Marie	k1gFnSc1	Marie
<g/>
)	)	kIx)	)
del	del	k?	del
Paraíso	Paraísa	k1gFnSc5	Paraísa
<g/>
:	:	kIx,	:
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
–	–	k?	–
4	[number]	k4	4
700	[number]	k4	700
věřících	věřící	k2eAgMnPc2d1	věřící
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
2	[number]	k4	2
500	[number]	k4	500
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
–	–	k?	–
2	[number]	k4	2
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
89	[number]	k4	89
%	%	kIx~	%
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k8xC	i
komunity	komunita	k1gFnSc2	komunita
jiných	jiný	k2eAgNnPc2d1	jiné
náboženství	náboženství	k1gNnPc2	náboženství
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
protestanti	protestant	k1gMnPc1	protestant
–	–	k?	–
asi	asi	k9	asi
6	[number]	k4	6
%	%	kIx~	%
<g/>
,	,	kIx,	,
židé	žid	k1gMnPc1	žid
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
domorodého	domorodý	k2eAgNnSc2d1	domorodé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
ze	z	k7c2	z
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
kombinaci	kombinace	k1gFnSc4	kombinace
svého	svůj	k3xOyFgNnSc2	svůj
původního	původní	k2eAgNnSc2d1	původní
náboženství	náboženství	k1gNnSc2	náboženství
s	s	k7c7	s
katolicismem	katolicismus	k1gInSc7	katolicismus
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc4	Mexiko
několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
zesnulý	zesnulý	k1gMnSc1	zesnulý
římský	římský	k2eAgMnSc1d1	římský
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
<g/>
katolický	katolický	k2eAgMnSc1d1	katolický
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
stalo	stát	k5eAaPmAgNnS	stát
hostitelskou	hostitelský	k2eAgFnSc7d1	hostitelská
zemí	zem	k1gFnSc7	zem
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1970	[number]	k4	1970
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnSc1d1	zdejší
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Mexika	Mexiko	k1gNnSc2	Mexiko
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
16	[number]	k4	16
let	léto	k1gNnPc2	léto
součástí	součást	k1gFnSc7	součást
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ročníku	ročník	k1gInSc2	ročník
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgFnSc1d1	zdejší
Mexická	mexický	k2eAgFnSc1d1	mexická
rallye	rallye	k1gFnSc1	rallye
součástí	součást	k1gFnSc7	součást
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rallye	rallye	k1gNnSc6	rallye
<g/>
.	.	kIx.	.
</s>
<s>
Mexičtí	mexický	k2eAgMnPc1d1	mexický
sportovci	sportovec	k1gMnPc1	sportovec
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
svoji	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
regionálních	regionální	k2eAgFnPc6d1	regionální
Středoamerický	středoamerický	k2eAgInSc1d1	středoamerický
a	a	k8xC	a
karibských	karibský	k2eAgFnPc6d1	karibská
hrách	hra	k1gFnPc6	hra
či	či	k8xC	či
Panamerických	panamerický	k2eAgFnPc6d1	Panamerická
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
<g/>
)	)	kIx)	)
Zlatého	zlatý	k2eAgInSc2d1	zlatý
poháru	pohár	k1gInSc2	pohár
CONCACAF	CONCACAF	kA	CONCACAF
<g/>
.	.	kIx.	.
</s>
<s>
Školní	školní	k2eAgNnSc1d1	školní
vzdělání	vzdělání	k1gNnSc1	vzdělání
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
v	v	k7c6	v
základním	základní	k2eAgMnSc6d1	základní
<g/>
,	,	kIx,	,
středním	střední	k2eAgInSc6d1	střední
(	(	kIx(	(
<g/>
nižším	nízký	k2eAgInSc6d2	nižší
a	a	k8xC	a
vyšším	vysoký	k2eAgMnSc6d2	vyšší
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysokém	vysoký	k2eAgInSc6d1	vysoký
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
šestileté	šestiletý	k2eAgNnSc1d1	šestileté
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
povinné	povinný	k2eAgNnSc1d1	povinné
a	a	k8xC	a
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgNnSc4d2	nižší
střední	střední	k2eAgNnSc4d1	střední
vzdělání	vzdělání	k1gNnSc4	vzdělání
představují	představovat	k5eAaImIp3nP	představovat
tříleté	tříletý	k2eAgFnPc4d1	tříletá
školy	škola	k1gFnPc4	škola
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
tohoto	tento	k3xDgNnSc2	tento
vzdělání	vzdělání	k1gNnSc2	vzdělání
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
střední	střední	k2eAgNnSc4d1	střední
vzdělání	vzdělání	k1gNnSc4	vzdělání
na	na	k7c6	na
tříletých	tříletý	k2eAgFnPc6d1	tříletá
nebo	nebo	k8xC	nebo
čtyřletých	čtyřletý	k2eAgFnPc6d1	čtyřletá
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
formou	forma	k1gFnSc7	forma
vzdělání	vzdělání	k1gNnSc2	vzdělání
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
nebo	nebo	k8xC	nebo
technického	technický	k2eAgInSc2d1	technický
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
zemím	zem	k1gFnPc3	zem
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
gramotností	gramotnost	k1gFnSc7	gramotnost
a	a	k8xC	a
vyspělým	vyspělý	k2eAgInSc7d1	vyspělý
školským	školský	k2eAgInSc7d1	školský
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Mexické	mexický	k2eAgNnSc1d1	mexické
školství	školství	k1gNnSc1	školství
má	mít	k5eAaImIp3nS	mít
širokou	široký	k2eAgFnSc4d1	široká
síť	síť	k1gFnSc4	síť
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
světově	světově	k6eAd1	světově
uznávané	uznávaný	k2eAgNnSc4d1	uznávané
vysoké	vysoký	k2eAgNnSc4d1	vysoké
školství	školství	k1gNnSc4	školství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
autonomní	autonomní	k2eAgFnSc6d1	autonomní
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
UNAM	UNAM	kA	UNAM
<g/>
)	)	kIx)	)
studuje	studovat	k5eAaImIp3nS	studovat
přes	přes	k7c4	přes
400	[number]	k4	400
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybět	k5eAaImIp3nP	chybět
učitelé	učitel	k1gMnPc1	učitel
se	s	k7c7	s
znalostí	znalost	k1gFnSc7	znalost
indiánských	indiánský	k2eAgInPc2d1	indiánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
2	[number]	k4	2
milióny	milión	k4xCgInPc1	milión
indiánů	indián	k1gMnPc2	indián
ovládá	ovládat	k5eAaImIp3nS	ovládat
jen	jen	k9	jen
svoji	svůj	k3xOyFgFnSc4	svůj
tradiční	tradiční	k2eAgFnSc4d1	tradiční
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc4d1	další
2	[number]	k4	2
milióny	milión	k4xCgInPc7	milión
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
hovoří	hovořit	k5eAaImIp3nS	hovořit
částečně	částečně	k6eAd1	částečně
španělsky	španělsky	k6eAd1	španělsky
<g/>
.	.	kIx.	.
</s>
<s>
Šanci	šance	k1gFnSc4	šance
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
nějaké	nějaký	k3yIgFnSc6	nějaký
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
má	mít	k5eAaImIp3nS	mít
ročně	ročně	k6eAd1	ročně
asi	asi	k9	asi
1,5	[number]	k4	1,5
miliónů	milión	k4xCgInPc2	milión
mladých	mladý	k2eAgMnPc2d1	mladý
Mexičanů	Mexičan	k1gMnPc2	Mexičan
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
točit	točit	k5eAaImF	točit
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
i	i	k8xC	i
zvukový	zvukový	k2eAgInSc1d1	zvukový
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc1d1	natočen
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Que	Que	k1gMnSc1	Que
viva	viva	k1gMnSc1	viva
México	México	k1gMnSc1	México
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
povahu	povaha	k1gFnSc4	povaha
Mexičanů	Mexičan	k1gMnPc2	Mexičan
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
jsou	být	k5eAaImIp3nP	být
významnými	významný	k2eAgMnPc7d1	významný
režiséry	režisér	k1gMnPc7	režisér
zejména	zejména	k9	zejména
E.	E.	kA	E.
Fernandez	Fernandez	k1gMnSc1	Fernandez
a	a	k8xC	a
kameraman	kameraman	k1gMnSc1	kameraman
G.	G.	kA	G.
Figneroa	Figneroa	k1gMnSc1	Figneroa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
M.	M.	kA	M.
Alzraki	Alzrak	k1gFnPc4	Alzrak
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc4	film
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
kvalitnějšími	kvalitní	k2eAgFnPc7d2	kvalitnější
a	a	k8xC	a
získávají	získávat	k5eAaImIp3nP	získávat
i	i	k9	i
různá	různý	k2eAgNnPc4d1	různé
ocenění	ocenění	k1gNnPc4	ocenění
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
režisérem	režisér	k1gMnSc7	režisér
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
Arturo	Artura	k1gFnSc5	Artura
Ripstein	Ripstein	k1gMnSc1	Ripstein
(	(	kIx(	(
<g/>
Mentiras	Mentiras	k1gMnSc1	Mentiras
piadosas	piadosas	k1gMnSc1	piadosas
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
;	;	kIx,	;
La	la	k1gNnSc2	la
mujer	mujero	k1gNnPc2	mujero
del	del	k?	del
puerto	puerta	k1gFnSc5	puerta
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
Principio	Principio	k6eAd1	Principio
y	y	k?	y
fin	fin	k?	fin
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
;	;	kIx,	;
Profundo	Profundo	k1gNnSc1	Profundo
carmesí	carmese	k1gFnPc2	carmese
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
slavili	slavit	k5eAaImAgMnP	slavit
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
jsou	být	k5eAaImIp3nP	být
Alfonso	Alfonso	k1gMnSc1	Alfonso
Arau	Araa	k1gFnSc4	Araa
(	(	kIx(	(
<g/>
Como	Como	k6eAd1	Como
agua	agua	k6eAd1	agua
para	para	k2eAgMnSc5d1	para
chocolate	chocolat	k1gMnSc5	chocolat
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
;	;	kIx,	;
Un	Un	k1gMnSc1	Un
paseo	paseo	k1gMnSc1	paseo
por	por	k?	por
las	laso	k1gNnPc2	laso
nubes	nubesa	k1gFnPc2	nubesa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Rodríguez	Rodríguez	k1gMnSc1	Rodríguez
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
mariachi	mariachi	k1gNnPc2	mariachi
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alberto	Alberta	k1gFnSc5	Alberta
Isaac	Isaac	k1gFnSc4	Isaac
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Mujeres	Mujeres	k1gMnSc1	Mujeres
insumisas	insumisas	k1gMnSc1	insumisas
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jorge	Jorge	k1gNnSc1	Jorge
Fons	Fonsa	k1gFnPc2	Fonsa
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
callejón	callejón	k1gInSc1	callejón
de	de	k?	de
los	los	k1gInSc1	los
milagros	milagrosa	k1gFnPc2	milagrosa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
María	María	k1gMnSc1	María
Novaro	Novara	k1gFnSc5	Novara
(	(	kIx(	(
<g/>
Lola	Lola	k1gMnSc1	Lola
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
;	;	kIx,	;
Danzón	Danzón	k1gMnSc1	Danzón
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
;	;	kIx,	;
El	Ela	k1gFnPc2	Ela
jardín	jardína	k1gFnPc2	jardína
del	del	k?	del
Edén	Edéna	k1gFnPc2	Edéna
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Guillermo	Guillerma	k1gFnSc5	Guillerma
del	del	k?	del
Toro	Toro	k?	Toro
(	(	kIx(	(
<g/>
Cronos	Cronos	k1gMnSc1	Cronos
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nicolás	Nicolás	k1gInSc1	Nicolás
Echevarría	Echevarrí	k1gInSc2	Echevarrí
(	(	kIx(	(
<g/>
Cabeza	Cabeza	k1gFnSc1	Cabeza
de	de	k?	de
Vaca	Vaca	k1gMnSc1	Vaca
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
především	především	k9	především
hrnčířství	hrnčířství	k1gNnSc1	hrnčířství
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
textilní	textilní	k2eAgFnSc1d1	textilní
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
vlna	vlna	k1gFnSc1	vlna
a	a	k8xC	a
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
henequenu	henequen	k1gInSc2	henequen
a	a	k8xC	a
ixtle	ixtle	k1gFnSc2	ixtle
(	(	kIx(	(
<g/>
agáve	agáve	k1gFnSc1	agáve
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
brašny	brašna	k1gFnSc2	brašna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rákosí	rákosí	k1gNnSc2	rákosí
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
různé	různý	k2eAgFnPc4d1	různá
krytiny	krytina	k1gFnPc4	krytina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
charros	charrosa	k1gFnPc2	charrosa
(	(	kIx(	(
<g/>
klobouky	klobouk	k1gInPc1	klobouk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orozco	Orozca	k1gMnSc5	Orozca
<g/>
,	,	kIx,	,
Jose	Josus	k1gMnSc5	Josus
Clemente	Clement	k1gMnSc5	Clement
<g/>
,1883	,1883	k4	,1883
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
;	;	kIx,	;
<g/>
Rivera	Rivera	k1gFnSc1	Rivera
<g/>
,	,	kIx,	,
Diego	Diego	k1gNnSc1	Diego
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
;	;	kIx,	;
Siqueiros	Siqueirosa	k1gFnPc2	Siqueirosa
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Alfaro	Alfara	k1gFnSc5	Alfara
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
;	;	kIx,	;
Kahlo	Kahlo	k1gNnSc1	Kahlo
<g/>
,	,	kIx,	,
Frida	Frida	k1gFnSc1	Frida
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Mario	Mario	k1gMnSc1	Mario
J.	J.	kA	J.
Molina	molino	k1gNnSc2	molino
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
běžné	běžný	k2eAgFnPc4d1	běžná
lidové	lidový	k2eAgFnPc4d1	lidová
slavnosti	slavnost	k1gFnPc4	slavnost
patří	patřit	k5eAaImIp3nP	patřit
býčí	býčí	k2eAgInPc1d1	býčí
a	a	k8xC	a
kohoutí	kohoutí	k2eAgInPc1d1	kohoutí
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
udržení	udržení	k1gNnSc1	udržení
se	se	k3xPyFc4	se
na	na	k7c6	na
nezkrocených	zkrocený	k2eNgMnPc6d1	nezkrocený
koních	kůň	k1gMnPc6	kůň
tzv.	tzv.	kA	tzv.
charros	charrosa	k1gFnPc2	charrosa
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
převažují	převažovat	k5eAaImIp3nP	převažovat
různé	různý	k2eAgFnPc1d1	různá
indiánské	indiánský	k2eAgFnPc1d1	indiánská
a	a	k8xC	a
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Mexičané	Mexičan	k1gMnPc1	Mexičan
rádi	rád	k2eAgMnPc1d1	rád
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
<g/>
.	.	kIx.	.
</s>
<s>
Oslava	oslava	k1gFnSc1	oslava
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
fiesta	fiesta	k1gFnSc1	fiesta
<g/>
.	.	kIx.	.
</s>
<s>
Sejde	sejít	k5eAaPmIp3nS	sejít
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
dvacet	dvacet	k4xCc4	dvacet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
přátel	přítel	k1gMnPc2	přítel
nebo	nebo	k8xC	nebo
známých	známý	k1gMnPc2	známý
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
fiesty	fiesta	k1gFnPc1	fiesta
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
často	často	k6eAd1	často
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
na	na	k7c6	na
dvorku	dvorek	k1gInSc6	dvorek
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
večer	večer	k6eAd1	večer
pak	pak	k6eAd1	pak
při	při	k7c6	při
světle	světlo	k1gNnSc6	světlo
lampionů	lampion	k1gInPc2	lampion
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
je	být	k5eAaImIp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
piňata	piňata	k1gFnSc1	piňata
<g/>
,	,	kIx,	,
keramická	keramický	k2eAgFnSc1d1	keramická
nádoba	nádoba	k1gFnSc1	nádoba
obalená	obalený	k2eAgFnSc1d1	obalená
staniolem	staniol	k1gInSc7	staniol
<g/>
,	,	kIx,	,
barevným	barevný	k2eAgInSc7d1	barevný
papírem	papír	k1gInSc7	papír
a	a	k8xC	a
papírovými	papírový	k2eAgFnPc7d1	papírová
třásněmi	třáseň	k1gFnPc7	třáseň
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
bonbóny	bonbón	k1gInPc4	bonbón
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgNnSc4d1	drobné
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
hračky	hračka	k1gFnPc1	hračka
a	a	k8xC	a
papírové	papírový	k2eAgInPc1d1	papírový
vločky	vloček	k1gInPc1	vloček
<g/>
.	.	kIx.	.
</s>
<s>
Piñ	Piñ	k?	Piñ
visí	viset	k5eAaImIp3nS	viset
na	na	k7c6	na
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
provazu	provaz	k1gInSc6	provaz
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
účastníků	účastník	k1gMnPc2	účastník
oslavy	oslava	k1gFnSc2	oslava
tahají	tahat	k5eAaImIp3nP	tahat
za	za	k7c2	za
konce	konec	k1gInSc2	konec
provazu	provaz	k1gInSc2	provaz
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
piňata	piňata	k1gFnSc1	piňata
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
stálém	stálý	k2eAgInSc6d1	stálý
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
dostanou	dostat	k5eAaPmIp3nP	dostat
hůl	hůl	k1gFnSc4	hůl
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
nádobu	nádoba	k1gFnSc4	nádoba
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
přítomní	přítomný	k1gMnPc1	přítomný
sledují	sledovat	k5eAaImIp3nP	sledovat
s	s	k7c7	s
povyražením	povyražení	k1gNnSc7	povyražení
marné	marný	k2eAgFnSc2d1	marná
i	i	k8xC	i
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
pokusy	pokus	k1gInPc4	pokus
a	a	k8xC	a
bouřlivě	bouřlivě	k6eAd1	bouřlivě
povzbuzují	povzbuzovat	k5eAaImIp3nP	povzbuzovat
tradičním	tradiční	k2eAgInSc7d1	tradiční
popěvkem	popěvek	k1gInSc7	popěvek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
dospělí	dospělý	k2eAgMnPc1d1	dospělý
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
převázané	převázaný	k2eAgFnPc4d1	převázaná
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
snaha	snaha	k1gFnSc1	snaha
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
piňatu	piňata	k1gFnSc4	piňata
je	být	k5eAaImIp3nS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
všeobecným	všeobecný	k2eAgNnSc7d1	všeobecné
veselím	veselí	k1gNnSc7	veselí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
fiestě	fiesta	k1gFnSc3	fiesta
patří	patřit	k5eAaImIp3nS	patřit
hodně	hodně	k6eAd1	hodně
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
fiesta	fiesta	k1gFnSc1	fiesta
oslavou	oslava	k1gFnSc7	oslava
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
feria	feria	k1gFnSc1	feria
bývá	bývat	k5eAaImIp3nS	bývat
výroční	výroční	k2eAgFnSc7d1	výroční
slavností	slavnost	k1gFnSc7	slavnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
výroční	výroční	k2eAgInSc1d1	výroční
trh	trh	k1gInSc1	trh
a	a	k8xC	a
výstava	výstava	k1gFnSc1	výstava
té	ten	k3xDgFnSc2	ten
výrobní	výrobní	k2eAgFnSc2d1	výrobní
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
převažuje	převažovat	k5eAaImIp3nS	převažovat
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Taxco	Taxco	k6eAd1	Taxco
<g/>
,	,	kIx,	,
známém	známý	k2eAgNnSc6d1	známé
výrobou	výroba	k1gFnSc7	výroba
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
feria	ferium	k1gNnSc2	ferium
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
je	být	k5eAaImIp3nS	být
feria	ferius	k1gMnSc4	ferius
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
feria	feria	k1gFnSc1	feria
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
dílny	dílna	k1gFnPc4	dílna
zručných	zručný	k2eAgMnPc2d1	zručný
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
pozorovat	pozorovat	k5eAaImF	pozorovat
jejich	jejich	k3xOp3gFnSc4	jejich
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
hudebníci	hudebník	k1gMnPc1	hudebník
a	a	k8xC	a
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
Zvuky	zvuk	k1gInPc1	zvuk
mariachis	mariachis	k1gFnSc2	mariachis
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
temperamentní	temperamentní	k2eAgInSc4d1	temperamentní
děj	děj	k1gInSc4	děj
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
rozehřeje	rozehřát	k5eAaPmIp3nS	rozehřát
postávající	postávající	k2eAgMnPc4d1	postávající
diváky	divák	k1gMnPc4	divák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podupávají	podupávat	k5eAaImIp3nP	podupávat
a	a	k8xC	a
přizvukují	přizvukovat	k5eAaImIp3nP	přizvukovat
do	do	k7c2	do
taktu	takt	k1gInSc2	takt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poledních	polední	k2eAgFnPc6d1	polední
hodinách	hodina	k1gFnPc6	hodina
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
velké	velký	k2eAgNnSc1d1	velké
vedro	vedro	k1gNnSc1	vedro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
odpoledne	odpoledne	k6eAd1	odpoledne
stává	stávat	k5eAaImIp3nS	stávat
úmorným	úmorný	k2eAgMnSc7d1	úmorný
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Mexičané	Mexičan	k1gMnPc1	Mexičan
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
tráví	trávit	k5eAaImIp3nP	trávit
siestu	siesta	k1gFnSc4	siesta
<g/>
.	.	kIx.	.
</s>
<s>
Siesta	siesta	k1gFnSc1	siesta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
těžkých	těžký	k2eAgFnPc2d1	těžká
ručních	ruční	k2eAgFnPc2d1	ruční
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
nebo	nebo	k8xC	nebo
stavbách	stavba	k1gFnPc6	stavba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
práce	práce	k1gFnSc1	práce
pod	pod	k7c7	pod
žhavým	žhavý	k2eAgNnSc7d1	žhavé
sluncem	slunce	k1gNnSc7	slunce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vyčerpávající	vyčerpávající	k2eAgInSc1d1	vyčerpávající
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
odpolední	odpolední	k2eAgNnSc1d1	odpolední
spánek	spánek	k1gInSc4	spánek
a	a	k8xC	a
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
siesta	siesta	k1gFnSc1	siesta
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
znakem	znak	k1gInSc7	znak
lenosti	lenost	k1gFnSc2	lenost
nebo	nebo	k8xC	nebo
přejedení	přejedení	k1gNnSc2	přejedení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
nevyhnutelností	nevyhnutelnost	k1gFnPc2	nevyhnutelnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mexická	mexický	k2eAgFnSc1d1	mexická
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
