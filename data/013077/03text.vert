<p>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
řecké	řecký	k2eAgFnPc1d1	řecká
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámější	známý	k2eAgFnSc1d3	nejznámější
kniha	kniha	k1gFnSc1	kniha
Eduarda	Eduard	k1gMnSc2	Eduard
Petišky	Petiška	k1gFnSc2	Petiška
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
příběhy	příběh	k1gInPc4	příběh
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
převyprávěné	převyprávěný	k2eAgFnPc1d1	převyprávěná
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
příběhy	příběh	k1gInPc1	příběh
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
řeckých	řecký	k2eAgMnPc6d1	řecký
bozích	bůh	k1gMnPc6	bůh
<g/>
,	,	kIx,	,
hrdinech	hrdina	k1gMnPc6	hrdina
<g/>
,	,	kIx,	,
mýtických	mýtický	k2eAgFnPc6d1	mýtická
postavách	postava	k1gFnPc6	postava
a	a	k8xC	a
nestvůrách	nestvůra	k1gFnPc6	nestvůra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Václava	Václav	k1gMnSc2	Václav
Fialy	Fiala	k1gMnSc2	Fiala
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
dětské	dětský	k2eAgFnSc2d1	dětská
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
jen	jen	k9	jen
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
vydána	vydán	k2eAgFnSc1d1	vydána
celkem	celkem	k6eAd1	celkem
patnáctkrát	patnáctkrát	k6eAd1	patnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
264	[number]	k4	264
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Petiška	Petišek	k1gInSc2	Petišek
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
komunistické	komunistický	k2eAgFnSc2d1	komunistická
totality	totalita	k1gFnSc2	totalita
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
životě	život	k1gInSc6	život
podaří	podařit	k5eAaPmIp3nS	podařit
vydat	vydat	k5eAaPmF	vydat
knihu	kniha	k1gFnSc4	kniha
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
pojal	pojmout	k5eAaPmAgMnS	pojmout
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
později	pozdě	k6eAd2	pozdě
sděloval	sdělovat	k5eAaImAgMnS	sdělovat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
román	román	k1gInSc1	román
o	o	k7c6	o
životě	život	k1gInSc6	život
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c6	o
českých	český	k2eAgNnPc6d1	české
zoufalstvích	zoufalství	k1gNnPc6	zoufalství
a	a	k8xC	a
nadějích	naděje	k1gFnPc6	naděje
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
klíčový	klíčový	k2eAgInSc4d1	klíčový
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
bozi	bůh	k1gMnPc1	bůh
představovali	představovat	k5eAaImAgMnP	představovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
politiky	politika	k1gFnPc4	politika
a	a	k8xC	a
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
archetypickém	archetypický	k2eAgNnSc6d1	archetypický
pojetí	pojetí	k1gNnSc6	pojetí
antické	antický	k2eAgFnSc2d1	antická
látky	látka	k1gFnSc2	látka
zřejmě	zřejmě	k6eAd1	zřejmě
spočívá	spočívat	k5eAaImIp3nS	spočívat
její	její	k3xOp3gInSc4	její
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
převyprávění	převyprávění	k1gNnSc1	převyprávění
antických	antický	k2eAgInPc2d1	antický
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednolitá	jednolitý	k2eAgFnSc1d1	jednolitá
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
životě	život	k1gInSc6	život
a	a	k8xC	a
charakteru	charakter	k1gInSc6	charakter
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
je	být	k5eAaImIp3nS	být
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
a	a	k8xC	a
vydávána	vydávat	k5eAaPmNgFnS	vydávat
(	(	kIx(	(
<g/>
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
nizozemštině	nizozemština	k1gFnSc6	nizozemština
<g/>
,	,	kIx,	,
finštině	finština	k1gFnSc6	finština
<g/>
,	,	kIx,	,
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
maďarštině	maďarština	k1gFnSc6	maďarština
<g/>
,	,	kIx,	,
estonštině	estonština	k1gFnSc6	estonština
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
pětadvaceti	pětadvacet	k4xCc2	pětadvacet
vydání	vydání	k1gNnPc2	vydání
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
školní	školní	k2eAgFnSc7d1	školní
četbou	četba	k1gFnSc7	četba
<g/>
.	.	kIx.	.
<g/>
Autorovo	autorův	k2eAgNnSc1d1	autorovo
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
již	již	k6eAd1	již
před	před	k7c7	před
knihou	kniha	k1gFnSc7	kniha
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
větví	větev	k1gFnPc2	větev
<g/>
:	:	kIx,	:
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
a	a	k8xC	a
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
odvětví	odvětví	k1gNnSc4	odvětví
<g/>
,	,	kIx,	,
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
se	se	k3xPyFc4	se
tak	tak	k9	tak
celý	celý	k2eAgInSc1d1	celý
úsek	úsek	k1gInSc1	úsek
čtenářského	čtenářský	k2eAgInSc2d1	čtenářský
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
čtenářských	čtenářský	k2eAgInPc2d1	čtenářský
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Staré	Staré	k2eAgFnPc1d1	Staré
řecké	řecký	k2eAgFnPc1d1	řecká
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
jsou	být	k5eAaImIp3nP	být
úhelným	úhelný	k2eAgInSc7d1	úhelný
kamenem	kámen	k1gInSc7	kámen
Petiškovy	Petiškův	k2eAgFnSc2d1	Petiškův
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
;	;	kIx,	;
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yQgInSc4	který
jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
neprojevili	projevit	k5eNaPmAgMnP	projevit
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
zdál	zdát	k5eAaImAgInS	zdát
příliš	příliš	k6eAd1	příliš
pracný	pracný	k2eAgMnSc1d1	pracný
a	a	k8xC	a
bezvýznamný	bezvýznamný	k2eAgMnSc1d1	bezvýznamný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základní	základní	k2eAgInSc1d1	základní
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
autor	autor	k1gMnSc1	autor
vyšel	vyjít	k5eAaPmAgMnS	vyjít
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
vypravěčem	vypravěč	k1gMnSc7	vypravěč
jak	jak	k8xS	jak
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
o	o	k7c6	o
tísni	tíseň	k1gFnSc6	tíseň
i	i	k8xC	i
radosti	radost	k1gFnSc6	radost
<g/>
,	,	kIx,	,
věrnosti	věrnost	k1gFnSc6	věrnost
i	i	k8xC	i
zradách	zrada	k1gFnPc6	zrada
<g/>
,	,	kIx,	,
o	o	k7c6	o
marnosti	marnost	k1gFnSc6	marnost
i	i	k8xC	i
možném	možný	k2eAgNnSc6d1	možné
naplnění	naplnění	k1gNnSc6	naplnění
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
životní	životní	k2eAgFnSc6d1	životní
pouti	pouť	k1gFnSc6	pouť
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
zdánlivě	zdánlivě	k6eAd1	zdánlivě
krátké	krátký	k2eAgFnSc2d1	krátká
<g/>
,	,	kIx,	,
ve	v	k7c4	v
skutečnosti	skutečnost	k1gFnPc4	skutečnost
plné	plný	k2eAgFnPc4d1	plná
možností	možnost	k1gFnSc7	možnost
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
přisouzen	přisouzen	k2eAgMnSc1d1	přisouzen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
