<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
karibské	karibský	k2eAgFnSc2d1	karibská
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
země	země	k1gFnSc1	země
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
u	u	k7c2	u
obou	dva	k4xCgInPc2	dva
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Karibiku	Karibik	k1gInSc2	Karibik
u	u	k7c2	u
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
pobřeží	pobřeží	k1gNnSc6	pobřeží
leží	ležet	k5eAaImIp3nS	ležet
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Venezuelou	Venezuela	k1gFnSc7	Venezuela
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc3	Brazílie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc7	Peru
<g/>
,	,	kIx,	,
Ekvádorem	Ekvádor	k1gInSc7	Ekvádor
a	a	k8xC	a
Panamou	Panama	k1gFnSc7	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
48	[number]	k4	48
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Bogotá	Bogotá	k1gFnSc1	Bogotá
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
mluví	mluvit	k5eAaImIp3nP	mluvit
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
hlásí	hlásit	k5eAaImIp3nP	hlásit
se	se	k3xPyFc4	se
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
donedávna	donedávna	k6eAd1	donedávna
probíhala	probíhat	k5eAaImAgFnS	probíhat
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
drogovými	drogový	k2eAgInPc7d1	drogový
kartely	kartel	k1gInPc7	kartel
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
bylo	být	k5eAaImAgNnS	být
Španěly	Španěl	k1gMnPc4	Španěl
kolonizováno	kolonizován	k2eAgNnSc1d1	kolonizováno
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
generální	generální	k2eAgInSc1d1	generální
kapitanát	kapitanát	k1gInSc1	kapitanát
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
místokrálovství	místokrálovství	k1gNnSc4	místokrálovství
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
<g/>
)	)	kIx)	)
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Bogotou	Bogota	k1gFnSc7	Bogota
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odehrávala	odehrávat	k5eAaImAgNnP	odehrávat
častá	častý	k2eAgNnPc1d1	časté
povstání	povstání	k1gNnPc1	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
byla	být	k5eAaImAgFnS	být
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Boyacá	Boyacá	k1gFnSc2	Boyacá
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Simón	Simón	k1gInSc4	Simón
Bolívar	Bolívar	k1gInSc1	Bolívar
dokázal	dokázat	k5eAaPmAgInS	dokázat
porazit	porazit	k5eAaPmF	porazit
španělské	španělský	k2eAgInPc4d1	španělský
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
Panama	Panama	k1gFnSc1	Panama
<g/>
,	,	kIx,	,
od	od	k7c2	od
1822	[number]	k4	1822
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
opět	opět	k6eAd1	opět
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
Granadu	Granada	k1gFnSc4	Granada
(	(	kIx(	(
<g/>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Panama	Panama	k1gFnSc1	Panama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Venezuelu	Venezuela	k1gFnSc4	Venezuela
a	a	k8xC	a
Ekvádor	Ekvádor	k1gInSc4	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
popud	popud	k1gInSc4	popud
USA	USA	kA	USA
odtržena	odtržen	k2eAgFnSc1d1	odtržena
Panama	Panama	k1gFnSc1	Panama
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
stavbě	stavba	k1gFnSc3	stavba
průplavu	průplav	k1gInSc2	průplav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
četné	četný	k2eAgInPc4d1	četný
boje	boj	k1gInPc4	boj
mezi	mezi	k7c7	mezi
konzervativci	konzervativec	k1gMnPc7	konzervativec
a	a	k8xC	a
liberály	liberál	k1gMnPc7	liberál
nebo	nebo	k8xC	nebo
levicovými	levicový	k2eAgMnPc7d1	levicový
radikály	radikál	k1gMnPc7	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
současnou	současný	k2eAgFnSc4d1	současná
guerillovou	guerillový	k2eAgFnSc4d1	guerillová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikají	vznikat	k5eAaImIp3nP	vznikat
levicově	levicově	k6eAd1	levicově
orientované	orientovaný	k2eAgInPc4d1	orientovaný
gardy	gard	k1gInPc4	gard
<g/>
:	:	kIx,	:
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
FARC	FARC	kA	FARC
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
umírněnější	umírněný	k2eAgFnSc1d2	umírněnější
Národní	národní	k2eAgFnSc1d1	národní
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
armáda	armáda	k1gFnSc1	armáda
-	-	kIx~	-
ELN	ELN	kA	ELN
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
stojí	stát	k5eAaImIp3nS	stát
vedle	vedle	k7c2	vedle
kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
armády	armáda	k1gFnSc2	armáda
pravicová	pravicový	k2eAgFnSc1d1	pravicová
paramilitantní	paramilitantní	k2eAgFnSc1d1	paramilitantní
organizace	organizace	k1gFnSc1	organizace
Spojená	spojený	k2eAgFnSc1d1	spojená
sebeobrana	sebeobrana	k1gFnSc1	sebeobrana
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
-	-	kIx~	-
AUC	AUC	kA	AUC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gFnPc4	jejich
praktiky	praktika	k1gFnPc4	praktika
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
únosy	únos	k1gInPc4	únos
civilistů	civilista	k1gMnPc2	civilista
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jejich	jejich	k3xOp3gNnSc2	jejich
zabíjení	zabíjení	k1gNnSc2	zabíjení
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
otevřeně	otevřeně	k6eAd1	otevřeně
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
rebelům	rebel	k1gMnPc3	rebel
(	(	kIx(	(
<g/>
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
úsilí	úsilí	k1gNnSc4	úsilí
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
začíná	začínat	k5eAaImIp3nS	začínat
mít	mít	k5eAaImF	mít
první	první	k4xOgInPc4	první
výsledky	výsledek	k1gInPc4	výsledek
<g/>
)	)	kIx)	)
a	a	k8xC	a
též	též	k9	též
k	k	k7c3	k
boji	boj	k1gInSc3	boj
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
(	(	kIx(	(
<g/>
drogové	drogový	k2eAgInPc1d1	drogový
kartely	kartel	k1gInPc1	kartel
z	z	k7c2	z
Cali	Cal	k1gFnSc2	Cal
a	a	k8xC	a
Mendelínský	Mendelínský	k2eAgInSc1d1	Mendelínský
kartel	kartel	k1gInSc1	kartel
nechvalně	chvalně	k6eNd1	chvalně
známého	známý	k2eAgNnSc2d1	známé
Pablo	Pablo	k1gNnSc1	Pablo
Escobara	Escobar	k1gMnSc2	Escobar
vzkvétaly	vzkvétat	k5eAaImAgInP	vzkvétat
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
rozprášeny	rozprášit	k5eAaPmNgInP	rozprášit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
si	se	k3xPyFc3	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
na	na	k7c4	na
220	[number]	k4	220
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
Kolumbijců	Kolumbijec	k1gMnPc2	Kolumbijec
muselo	muset	k5eAaImAgNnS	muset
opustit	opustit	k5eAaPmF	opustit
své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbijské	kolumbijský	k2eAgInPc1d1	kolumbijský
břehy	břeh	k1gInPc1	břeh
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
jak	jak	k6eAd1	jak
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
asi	asi	k9	asi
1800	[number]	k4	1800
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Pacifiku	Pacifik	k1gInSc3	Pacifik
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1500	[number]	k4	1500
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pevninské	pevninský	k2eAgFnPc4d1	pevninská
hranice	hranice	k1gFnPc4	hranice
sdílí	sdílet	k5eAaImIp3nS	sdílet
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
s	s	k7c7	s
Panamou	Panama	k1gFnSc7	Panama
(	(	kIx(	(
<g/>
225	[number]	k4	225
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ekvádorem	Ekvádor	k1gInSc7	Ekvádor
(	(	kIx(	(
<g/>
590	[number]	k4	590
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
(	(	kIx(	(
<g/>
1496	[number]	k4	1496
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brazílií	Brazílie	k1gFnSc7	Brazílie
(	(	kIx(	(
<g/>
1643	[number]	k4	1643
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Venezuelou	Venezuela	k1gFnSc7	Venezuela
(	(	kIx(	(
<g/>
2050	[number]	k4	2050
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
jsou	být	k5eAaImIp3nP	být
Panama	Panama	k1gFnSc1	Panama
<g/>
,	,	kIx,	,
Kostarika	Kostarika	k1gFnSc1	Kostarika
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
,	,	kIx,	,
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
a	a	k8xC	a
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
má	mít	k5eAaImIp3nS	mít
celkovou	celkový	k2eAgFnSc4d1	celková
rozlohu	rozloha	k1gFnSc4	rozloha
1	[number]	k4	1
138	[number]	k4	138
910	[number]	k4	910
km2	km2	k4	km2
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k8xS	tak
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
po	po	k7c6	po
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Argentině	Argentina	k1gFnSc6	Argentina
a	a	k8xC	a
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sedmou	sedma	k1gFnSc7	sedma
největší	veliký	k2eAgFnSc7d3	veliký
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1	[number]	k4	1
038	[number]	k4	038
700	[number]	k4	700
km2	km2	k4	km2
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
100	[number]	k4	100
210	[number]	k4	210
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
geografických	geografický	k2eAgInPc2d1	geografický
regionů	region	k1gInPc2	region
<g/>
:	:	kIx,	:
Andská	andský	k2eAgFnSc1d1	andská
vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
pohoří	pohoří	k1gNnPc4	pohoří
a	a	k8xC	a
údolní	údolní	k2eAgFnPc4d1	údolní
nížiny	nížina	k1gFnPc4	nížina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karibské	karibský	k2eAgFnSc2d1	karibská
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
Pacifické	pacifický	k2eAgFnPc4d1	Pacifická
nížiny	nížina	k1gFnPc4	nížina
a	a	k8xC	a
Los	los	k1gInSc4	los
Llanos	Llanos	k1gInPc2	Llanos
a	a	k8xC	a
tropický	tropický	k2eAgInSc1d1	tropický
deštný	deštný	k2eAgInSc1d1	deštný
les	les	k1gInSc1	les
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
několik	několik	k4yIc4	několik
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
a	a	k8xC	a
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc4	souostroví
San	San	k1gFnSc2	San
Andrés	Andrésa	k1gFnPc2	Andrésa
a	a	k8xC	a
Providencia	Providencium	k1gNnSc2	Providencium
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Karibiku	Karibikum	k1gNnSc6	Karibikum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
775	[number]	k4	775
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
severního	severní	k2eAgNnSc2d1	severní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
a	a	k8xC	a
220	[number]	k4	220
km	km	kA	km
od	od	k7c2	od
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Nikaraguy	Nikaragua	k1gFnSc2	Nikaragua
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
32	[number]	k4	32
kolumbijských	kolumbijský	k2eAgInPc2d1	kolumbijský
departamentů	departament	k1gInPc2	departament
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
San	San	k1gFnSc4	San
Andrés	Andrésa	k1gFnPc2	Andrésa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
leží	ležet	k5eAaImIp3nS	ležet
ostrov	ostrov	k1gInSc1	ostrov
Malpelo	Malpela	k1gFnSc5	Malpela
<g/>
.	.	kIx.	.
</s>
<s>
Ostrůvek	ostrůvek	k1gInSc1	ostrůvek
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
del	del	k?	del
Islote	Islot	k1gInSc5	Islot
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejhustěji	husto	k6eAd3	husto
obydlené	obydlený	k2eAgInPc4d1	obydlený
ostrovy	ostrov	k1gInPc4	ostrov
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozdílnostem	rozdílnost	k1gFnPc3	rozdílnost
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
má	mít	k5eAaImIp3nS	mít
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
v	v	k7c6	v
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
zde	zde	k6eAd1	zde
však	však	k9	však
panují	panovat	k5eAaImIp3nP	panovat
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgInPc4d1	malý
teplotní	teplotní	k2eAgInPc4d1	teplotní
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelné	obyvatelný	k2eAgFnPc1d1	obyvatelná
oblasti	oblast	k1gFnPc1	oblast
leží	ležet	k5eAaImIp3nP	ležet
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
teplých	teplé	k1gNnPc6	teplé
-	-	kIx~	-
pod	pod	k7c4	pod
900	[number]	k4	900
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
středních	střední	k2eAgInPc2d1	střední
-	-	kIx~	-
mezi	mezi	k7c7	mezi
900	[number]	k4	900
a	a	k8xC	a
1980	[number]	k4	1980
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
studených	studený	k2eAgInPc2d1	studený
-	-	kIx~	-
mezi	mezi	k7c7	mezi
1980	[number]	k4	1980
a	a	k8xC	a
2950	[number]	k4	2950
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
klimatických	klimatický	k2eAgFnPc6d1	klimatická
zónách	zóna	k1gFnPc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
těžké	těžký	k2eAgInPc1d1	těžký
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
oblastech	oblast	k1gFnPc6	oblast
Pacifiku	Pacifik	k1gInSc2	Pacifik
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
jižní	jižní	k2eAgFnSc2d1	jižní
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Andské	andský	k2eAgNnSc1d1	Andské
pohoří	pohoří	k1gNnSc1	pohoří
se	se	k3xPyFc4	se
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
(	(	kIx(	(
<g/>
ekvádorské	ekvádorský	k2eAgFnPc1d1	ekvádorská
hranice	hranice	k1gFnPc1	hranice
<g/>
)	)	kIx)	)
až	až	k9	až
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
(	(	kIx(	(
<g/>
venezuelské	venezuelský	k2eAgFnPc4d1	venezuelská
hranice	hranice	k1gFnPc4	hranice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc1	Anda
se	se	k3xPyFc4	se
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Kolumbijský	kolumbijský	k2eAgInSc4d1	kolumbijský
masiv	masiv	k1gInSc4	masiv
(	(	kIx(	(
<g/>
Macizo	Maciza	k1gFnSc5	Maciza
Colombiano	Colombiana	k1gFnSc5	Colombiana
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
další	další	k2eAgNnPc4d1	další
pohoří	pohoří	k1gNnPc4	pohoří
(	(	kIx(	(
<g/>
Východní	východní	k2eAgNnSc4d1	východní
<g/>
,	,	kIx,	,
Centrální	centrální	k2eAgNnSc4d1	centrální
a	a	k8xC	a
Západní	západní	k2eAgNnSc4d1	západní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
dvě	dva	k4xCgNnPc4	dva
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
údolí	údolí	k1gNnPc4	údolí
-	-	kIx~	-
Magdalena	Magdalena	k1gFnSc1	Magdalena
a	a	k8xC	a
Cauca	Cauca	k1gFnSc1	Cauca
-	-	kIx~	-
jimi	on	k3xPp3gFnPc7	on
protékají	protékat	k5eAaImIp3nP	protékat
stejnojmenné	stejnojmenný	k2eAgFnPc1d1	stejnojmenná
řeky	řeka	k1gFnPc1	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
není	být	k5eNaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hory	hora	k1gFnPc1	hora
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
de	de	k?	de
Santa	Santa	k1gFnSc1	Santa
Marta	Marta	k1gFnSc1	Marta
-	-	kIx~	-
Pico	Pico	k6eAd1	Pico
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
Colón	colón	k1gInSc1	colón
vysoký	vysoký	k2eAgInSc1d1	vysoký
5775	[number]	k4	5775
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
Pico	Pico	k1gMnSc1	Pico
Simon	Simon	k1gMnSc1	Simon
Bolivar	Bolivar	k1gInSc4	Bolivar
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
savanou	savana	k1gFnSc7	savana
a	a	k8xC	a
deštným	deštný	k2eAgInSc7d1	deštný
pralesem	prales	k1gInSc7	prales
<g/>
.	.	kIx.	.
</s>
<s>
Protékají	protékat	k5eAaImIp3nP	protékat
tudy	tudy	k6eAd1	tudy
řeky	řeka	k1gFnPc1	řeka
Amazonka	Amazonka	k1gFnSc1	Amazonka
a	a	k8xC	a
Orinoco	Orinoco	k1gNnSc1	Orinoco
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Los	los	k1gInSc4	los
Llanos	Llanos	k1gInPc7	Llanos
je	být	k5eAaImIp3nS	být
region	region	k1gInSc1	region
savan	savana	k1gFnPc2	savana
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
právě	právě	k9	právě
u	u	k7c2	u
Orinocského	Orinocský	k2eAgNnSc2d1	Orinocský
povodí	povodí	k1gNnSc2	povodí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Amazonie	Amazonie	k1gFnSc1	Amazonie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
Amazonským	amazonský	k2eAgInSc7d1	amazonský
deštným	deštný	k2eAgInSc7d1	deštný
pralesem	prales	k1gInSc7	prales
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
Amazonského	amazonský	k2eAgNnSc2d1	amazonské
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
And	Anda	k1gFnPc2	Anda
jsou	být	k5eAaImIp3nP	být
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
planiny	planina	k1gFnPc1	planina
<g/>
,	,	kIx,	,
Karibská	karibský	k2eAgFnSc1d1	karibská
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Pacifická	pacifický	k2eAgFnSc1d1	Pacifická
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
tradičních	tradiční	k2eAgInPc2d1	tradiční
krajinných	krajinný	k2eAgInPc2d1	krajinný
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Andský	andský	k2eAgInSc1d1	andský
region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
Karibský	karibský	k2eAgInSc1d1	karibský
region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
Pacifický	pacifický	k2eAgInSc1d1	pacifický
region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
Orinoquijský	Orinoquijský	k2eAgInSc1d1	Orinoquijský
region	region	k1gInSc1	region
a	a	k8xC	a
Amazonský	amazonský	k2eAgInSc1d1	amazonský
region	region	k1gInSc1	region
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
označují	označovat	k5eAaImIp3nP	označovat
ještě	ještě	k9	ještě
za	za	k7c4	za
region	region	k1gInSc4	region
Insularní	Insularní	k2eAgInSc4d1	Insularní
region	region	k1gInSc4	region
separovaný	separovaný	k2eAgInSc4d1	separovaný
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
je	být	k5eAaImIp3nS	být
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
unitárním	unitární	k2eAgInSc7d1	unitární
státem	stát	k1gInSc7	stát
s	s	k7c7	s
decentralizovaným	decentralizovaný	k2eAgNnSc7d1	decentralizované
řízením	řízení	k1gNnSc7	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
procesu	proces	k1gInSc6	proces
modernizace	modernizace	k1gFnSc2	modernizace
zahájené	zahájený	k2eAgNnSc1d1	zahájené
přijetím	přijetí	k1gNnSc7	přijetí
Ústavy	ústava	k1gFnSc2	ústava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
nové	nový	k2eAgFnSc2d1	nová
Ústavy	ústava	k1gFnSc2	ústava
zvolen	zvolen	k2eAgMnSc1d1	zvolen
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
Kongres	kongres	k1gInSc1	kongres
a	a	k8xC	a
regionální	regionální	k2eAgNnPc1d1	regionální
zastupitelstva	zastupitelstvo	k1gNnPc1	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
konstituce	konstituce	k1gFnSc1	konstituce
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
zemi	zem	k1gFnSc4	zem
jako	jako	k8xS	jako
sociální	sociální	k2eAgInSc4d1	sociální
právní	právní	k2eAgInSc4d1	právní
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
unitární	unitární	k2eAgFnSc4d1	unitární
a	a	k8xC	a
decentralizovanou	decentralizovaný	k2eAgFnSc4d1	decentralizovaná
republiku	republika	k1gFnSc4	republika
s	s	k7c7	s
částečnou	částečný	k2eAgFnSc7d1	částečná
autonomií	autonomie	k1gFnSc7	autonomie
regionálních	regionální	k2eAgInPc2d1	regionální
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
participativní	participativní	k2eAgFnSc6d1	participativní
a	a	k8xC	a
pluralistické	pluralistický	k2eAgFnSc6d1	pluralistická
demokracii	demokracie	k1gFnSc6	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1	vnitropolitická
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
zejména	zejména	k9	zejména
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
konsolidaci	konsolidace	k1gFnSc4	konsolidace
reforem	reforma	k1gFnPc2	reforma
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
prezidenta	prezident	k1gMnSc2	prezident
Uribeho	Uribe	k1gMnSc2	Uribe
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
postavení	postavení	k1gNnSc2	postavení
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
převládala	převládat	k5eAaImAgFnS	převládat
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
širší	široký	k2eAgFnSc6d2	širší
projekci	projekce	k1gFnSc6	projekce
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
výsledků	výsledek	k1gInPc2	výsledek
do	do	k7c2	do
sociální	sociální	k2eAgFnSc2d1	sociální
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
problematika	problematika	k1gFnSc1	problematika
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
narkomafii	narkomafie	k1gFnSc3	narkomafie
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojeným	spojený	k2eAgInSc7d1	spojený
terorismem	terorismus	k1gInSc7	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
Uribeho	Uribe	k1gMnSc2	Uribe
administrativy	administrativa	k1gFnSc2	administrativa
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nekompromisním	kompromisní	k2eNgInSc7d1	nekompromisní
vojenským	vojenský	k2eAgInSc7d1	vojenský
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
vnitropolitické	vnitropolitický	k2eAgFnSc2d1	vnitropolitická
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
otevřeným	otevřený	k2eAgInSc7d1	otevřený
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
gerile	gerila	k1gFnSc3	gerila
FARC	FARC	kA	FARC
a	a	k8xC	a
ELN	ELN	kA	ELN
<g/>
,	,	kIx,	,
tlakem	tlak	k1gInSc7	tlak
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Řídící	řídící	k2eAgFnSc1d1	řídící
moc	moc	k1gFnSc1	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
sfér	sféra	k1gFnPc2	sféra
<g/>
:	:	kIx,	:
výkonné	výkonný	k2eAgFnSc2d1	výkonná
<g/>
,	,	kIx,	,
legislativní	legislativní	k2eAgFnSc2d1	legislativní
a	a	k8xC	a
justiční	justiční	k2eAgFnSc2d1	justiční
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgMnPc1d1	soudní
moc	moc	k6eAd1	moc
představují	představovat	k5eAaImIp3nP	představovat
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
generálního	generální	k2eAgMnSc2d1	generální
prokurátora	prokurátor	k1gMnSc2	prokurátor
a	a	k8xC	a
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
administrace	administrace	k1gFnSc1	administrace
Rada	rada	k1gFnSc1	rada
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
13	[number]	k4	13
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
32	[number]	k4	32
departementů	departement	k1gInPc2	departement
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
španělsky	španělsky	k6eAd1	španělsky
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
departament	departament	k1gInSc1	departament
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
je	být	k5eAaImIp3nS	být
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
pluralitou	pluralita	k1gFnSc7	pluralita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
také	také	k9	také
představitelé	představitel	k1gMnPc1	představitel
opoziční	opoziční	k2eAgFnSc2d1	opoziční
liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
rozdělené	rozdělená	k1gFnPc1	rozdělená
do	do	k7c2	do
8	[number]	k4	8
stálých	stálý	k2eAgFnPc2d1	stálá
komisí	komise	k1gFnPc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
má	mít	k5eAaImIp3nS	mít
162	[number]	k4	162
členů	člen	k1gInPc2	člen
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
102	[number]	k4	102
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
je	být	k5eAaImIp3nS	být
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
několika	několik	k4yIc2	několik
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
latinskoamerické	latinskoamerický	k2eAgFnSc2d1	latinskoamerická
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
např.	např.	kA	např.
organizací	organizace	k1gFnSc7	organizace
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Unie	unie	k1gFnSc1	unie
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Pacifická	pacifický	k2eAgFnSc1d1	Pacifická
aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Latinskoamerické	latinskoamerický	k2eAgNnSc1d1	latinskoamerické
integrační	integrační	k2eAgNnSc1d1	integrační
sdružení	sdružení	k1gNnSc1	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Departementy	departement	k1gInPc4	departement
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
distrikt	distrikt	k1gInSc1	distrikt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
distrito	distrita	k1gFnSc5	distrita
capital	capitat	k5eAaPmAgInS	capitat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bogotá	Bogotá	k1gMnSc1	Bogotá
D.C.	D.C.	k1gFnSc2	D.C.
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
odvětví	odvětví	k1gNnSc4	odvětví
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
těžba	těžba	k1gFnSc1	těžba
-	-	kIx~	-
antimon	antimon	k1gInSc1	antimon
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
bizmut	bizmut	k1gInSc1	bizmut
zemědělství	zemědělství	k1gNnSc2	zemědělství
-	-	kIx~	-
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
maniok	maniok	k1gInSc1	maniok
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
sójová	sójový	k2eAgFnSc1d1	sójová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
chov	chov	k1gInSc1	chov
-	-	kIx~	-
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnPc1	ovce
<g/>
,	,	kIx,	,
lamy	lama	k1gFnPc1	lama
<g/>
,	,	kIx,	,
alpaky	alpaka	k1gFnPc1	alpaka
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc1	drůbež
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
pracovalo	pracovat	k5eAaImAgNnS	pracovat
11	[number]	k4	11
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
Export	export	k1gInSc1	export
<g/>
:	:	kIx,	:
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
rudy	ruda	k1gFnPc1	ruda
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
obchodní	obchodní	k2eAgMnPc1d1	obchodní
partneři	partner	k1gMnPc1	partner
<g/>
:	:	kIx,	:
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Běloši	běloch	k1gMnPc1	běloch
španělského	španělský	k2eAgInSc2d1	španělský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
míšenci	míšenec	k1gMnPc1	míšenec
bělochů	běloch	k1gMnPc2	běloch
a	a	k8xC	a
Indiánů	Indián	k1gMnPc2	Indián
(	(	kIx(	(
<g/>
mesticové	mestic	k1gMnPc1	mestic
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
84.2	[number]	k4	84.2
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
černoši	černoch	k1gMnPc1	černoch
10.4	[number]	k4	10.4
<g/>
%	%	kIx~	%
a	a	k8xC	a
příslušníci	příslušník	k1gMnPc1	příslušník
indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
3.4	[number]	k4	3.4
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Latinskoamerická	latinskoamerický	k2eAgFnSc1d1	latinskoamerická
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
literatura	literatura	k1gFnSc1	literatura
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
koloniální	koloniální	k2eAgFnSc6d1	koloniální
době	doba	k1gFnSc6	doba
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
literatuře	literatura	k1gFnSc6	literatura
španělské	španělský	k2eAgFnSc6d1	španělská
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
utvářet	utvářet	k5eAaImF	utvářet
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
proudu	proud	k1gInSc6	proud
(	(	kIx(	(
<g/>
především	především	k9	především
Rafael	Rafael	k1gMnSc1	Rafael
Pombo	Pomba	k1gFnSc5	Pomba
a	a	k8xC	a
Jorge	Jorg	k1gMnSc2	Jorg
Isaacs	Isaacsa	k1gFnPc2	Isaacsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
realistické	realistický	k2eAgFnSc6d1	realistická
próze	próza	k1gFnSc6	próza
(	(	kIx(	(
<g/>
Tomás	Tomás	k1gInSc1	Tomás
Carrasquilla	Carrasquillo	k1gNnSc2	Carrasquillo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poezie	poezie	k1gFnSc1	poezie
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
modernistické	modernistický	k2eAgFnSc6d1	modernistická
lyrice	lyrika	k1gFnSc6	lyrika
J.	J.	kA	J.
A.	A.	kA	A.
Silvy	Silva	k1gFnSc2	Silva
a	a	k8xC	a
v	v	k7c6	v
avantgardní	avantgardní	k2eAgFnSc6d1	avantgardní
tvorbě	tvorba	k1gFnSc6	tvorba
L.	L.	kA	L.
Greiffa	Greiff	k1gMnSc4	Greiff
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
navázala	navázat	k5eAaPmAgFnS	navázat
skupina	skupina	k1gFnSc1	skupina
Kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
nebe	nebe	k1gNnSc1	nebe
(	(	kIx(	(
<g/>
Jorge	Jorge	k1gFnSc1	Jorge
Rojas	Rojas	k1gMnSc1	Rojas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
romanopiscům	romanopisec	k1gMnPc3	romanopisec
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
patřil	patřit	k5eAaImAgInS	patřit
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
J.	J.	kA	J.
E.	E.	kA	E.
Rivera	Rivero	k1gNnPc4	Rivero
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Gabriel	Gabriel	k1gMnSc1	Gabriel
García	García	k1gMnSc1	García
Márquez	Márquez	k1gMnSc1	Márquez
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovou	Nobelová	k1gFnSc4	Nobelová
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc4d1	místní
význam	význam	k1gInSc4	význam
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
i	i	k9	i
tvorba	tvorba	k1gFnSc1	tvorba
dalších	další	k2eAgMnPc2d1	další
romanopisců	romanopisec	k1gMnPc2	romanopisec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Eduardo	Eduardo	k1gNnSc4	Eduardo
Zalamea	Zalame	k2eAgFnSc1d1	Zalame
Borda	Borda	k1gFnSc1	Borda
<g/>
,	,	kIx,	,
Jorge	Jorge	k1gFnSc1	Jorge
Zalamea	Zalamea	k1gFnSc1	Zalamea
<g/>
,	,	kIx,	,
Eduardo	Eduardo	k1gNnSc1	Eduardo
Caballero	caballero	k1gMnSc1	caballero
Calderón	Calderón	k1gMnSc1	Calderón
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgNnPc2d1	různé
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
:	:	kIx,	:
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
archeologie	archeologie	k1gFnSc2	archeologie
<g/>
,	,	kIx,	,
etnografie	etnografie	k1gFnSc2	etnografie
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
muzeum	muzeum	k1gNnSc1	muzeum
na	na	k7c6	na
světě	svět	k1gInSc6	svět
zlata	zlato	k1gNnSc2	zlato
-	-	kIx~	-
24,000	[number]	k4	24,000
starověké	starověký	k2eAgInPc4d1	starověký
indické	indický	k2eAgInPc4d1	indický
zlaté	zlatý	k2eAgInPc4d1	zlatý
šperky	šperk	k1gInPc4	šperk
a	a	k8xC	a
smaragdy	smaragd	k1gInPc4	smaragd
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Dům-muzeum	Důmuzeum	k1gNnSc1	Dům-muzeum
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
španělských	španělský	k2eAgFnPc2d1	španělská
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
S.	S.	kA	S.
Bolivar	Bolivar	k1gInSc1	Bolivar
-	-	kIx~	-
Bogota	Bogota	k1gFnSc1	Bogota
<g/>
.	.	kIx.	.
</s>
