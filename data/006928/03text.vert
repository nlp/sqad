<s>
Poluce	poluce	k1gFnSc1	poluce
je	být	k5eAaImIp3nS	být
mimovolný	mimovolný	k2eAgInSc4d1	mimovolný
výron	výron	k1gInSc4	výron
ejakulátu	ejakulát	k1gInSc2	ejakulát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
v	v	k7c6	v
bdělém	bdělý	k2eAgInSc6d1	bdělý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
mokré	mokrý	k2eAgInPc4d1	mokrý
sny	sen	k1gInPc4	sen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mimovolný	mimovolný	k2eAgInSc1d1	mimovolný
orgasmus	orgasmus	k1gInSc1	orgasmus
nebo	nebo	k8xC	nebo
orgasmus	orgasmus	k1gInSc1	orgasmus
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Poluce	poluce	k1gFnSc1	poluce
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
v	v	k7c6	v
období	období	k1gNnSc6	období
puberty	puberta	k1gFnSc2	puberta
a	a	k8xC	a
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
docházet	docházet	k5eAaImF	docházet
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
provázeny	provázen	k2eAgInPc4d1	provázen
erotickými	erotický	k2eAgInPc7d1	erotický
sny	sen	k1gInPc7	sen
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
někteří	některý	k3yIgMnPc1	některý
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
během	během	k7c2	během
ejakulace	ejakulace	k1gFnSc2	ejakulace
vzbudí	vzbudit	k5eAaPmIp3nP	vzbudit
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
spí	spát	k5eAaImIp3nS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
poluce	poluce	k1gFnSc2	poluce
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
přijímanou	přijímaný	k2eAgFnSc7d1	přijímaná
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přímý	přímý	k2eAgInSc4d1	přímý
důsledek	důsledek	k1gInSc4	důsledek
stimulace	stimulace	k1gFnSc2	stimulace
způsobené	způsobený	k2eAgInPc1d1	způsobený
erotickými	erotický	k2eAgInPc7d1	erotický
sny	sen	k1gInPc7	sen
nebo	nebo	k8xC	nebo
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
sexuální	sexuální	k2eAgFnSc4d1	sexuální
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
muži	muž	k1gMnPc1	muž
však	však	k9	však
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
erotické	erotický	k2eAgInPc4d1	erotický
sny	sen	k1gInPc4	sen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
k	k	k7c3	k
poluci	poluce	k1gFnSc3	poluce
nevedly	vést	k5eNaImAgInP	vést
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiná	k1gFnSc7	jiná
všeobecně	všeobecně	k6eAd1	všeobecně
přijímanou	přijímaný	k2eAgFnSc7d1	přijímaná
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
nahromaděného	nahromaděný	k2eAgNnSc2d1	nahromaděné
spermatu	sperma	k1gNnSc2	sperma
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tělo	tělo	k1gNnSc1	tělo
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nemá	mít	k5eNaImIp3nS	mít
tuto	tento	k3xDgFnSc4	tento
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ejakulují	ejakulovat	k5eAaImIp3nP	ejakulovat
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
a	a	k8xC	a
poluce	poluce	k1gFnPc1	poluce
nezažívají	zažívat	k5eNaImIp3nP	zažívat
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
polucí	poluce	k1gFnPc2	poluce
je	být	k5eAaImIp3nS	být
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
individuální	individuální	k2eAgFnSc1d1	individuální
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
někteří	některý	k3yIgMnPc1	některý
muži	muž	k1gMnPc1	muž
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
pubertě	puberta	k1gFnSc6	puberta
časté	častý	k2eAgFnSc2d1	častá
poluce	poluce	k1gFnSc2	poluce
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
nezažili	zažít	k5eNaPmAgMnP	zažít
žádnou	žádný	k3yNgFnSc4	žádný
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
muži	muž	k1gMnPc1	muž
mají	mít	k5eAaImIp3nP	mít
poluce	poluce	k1gFnPc4	poluce
víceméně	víceméně	k9	víceméně
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
se	se	k3xPyFc4	se
poluce	poluce	k1gFnSc1	poluce
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
věkovém	věkový	k2eAgNnSc6d1	věkové
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k1gMnPc2	jiný
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
od	od	k7c2	od
puberty	puberta	k1gFnSc2	puberta
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
obecnému	obecný	k2eAgInSc3d1	obecný
názoru	názor	k1gInSc3	názor
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
četností	četnost	k1gFnSc7	četnost
polucí	poluce	k1gFnSc7	poluce
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
často	často	k6eAd1	často
daný	daný	k2eAgMnSc1d1	daný
člověk	člověk	k1gMnSc1	člověk
masturbuje	masturbovat	k5eAaImIp3nS	masturbovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
četnost	četnost	k1gFnSc4	četnost
polucí	poluce	k1gFnPc2	poluce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
testosteronu	testosteron	k1gInSc2	testosteron
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
experimentu	experiment	k1gInSc6	experiment
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
významně	významně	k6eAd1	významně
rostl	růst	k5eAaImAgInS	růst
počet	počet	k1gInSc1	počet
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgMnPc2	který
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
polucím	poluce	k1gFnPc3	poluce
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
úměrně	úměrně	k7c3	úměrně
jejich	jejich	k3xOp3gFnSc3	jejich
dávce	dávka	k1gFnSc3	dávka
testosteronu	testosteron	k1gInSc2	testosteron
–	–	k?	–
od	od	k7c2	od
17	[number]	k4	17
%	%	kIx~	%
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
bez	bez	k7c2	bez
podávání	podávání	k1gNnSc2	podávání
hormonu	hormon	k1gInSc2	hormon
až	až	k9	až
po	po	k7c4	po
90	[number]	k4	90
%	%	kIx~	%
u	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
dávkou	dávka	k1gFnSc7	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
vede	vést	k5eAaImIp3nS	vést
ejakulace	ejakulace	k1gFnPc4	ejakulace
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
erekce	erekce	k1gFnSc2	erekce
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
poluce	poluce	k1gFnSc2	poluce
je	být	k5eAaImIp3nS	být
erekce	erekce	k1gFnSc1	erekce
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgMnPc2	který
se	se	k3xPyFc4	se
mimovolná	mimovolný	k2eAgFnSc1d1	mimovolná
ejakulace	ejakulace	k1gFnSc1	ejakulace
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
často	často	k6eAd1	často
nebo	nebo	k8xC	nebo
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
výronu	výron	k1gInSc3	výron
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
spermatu	sperma	k1gNnSc2	sperma
<g/>
,	,	kIx,	,
diagnostikováni	diagnostikován	k2eAgMnPc1d1	diagnostikován
jako	jako	k9	jako
pacienti	pacient	k1gMnPc1	pacient
postižen	postihnout	k5eAaPmNgMnS	postihnout
nemocí	nemoc	k1gFnSc7	nemoc
spermatorea	spermatoreum	k1gNnSc2	spermatoreum
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
této	tento	k3xDgFnSc2	tento
"	"	kIx"	"
<g/>
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
"	"	kIx"	"
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
obřezávání	obřezávání	k1gNnSc6	obřezávání
nebo	nebo	k8xC	nebo
kastraci	kastrace	k1gFnSc6	kastrace
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
moderní	moderní	k2eAgMnPc1d1	moderní
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
léčitelé	léčitel	k1gMnPc1	léčitel
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
jako	jako	k8xS	jako
spermatoreu	spermatoreu	k6eAd1	spermatoreu
diagnostikují	diagnostikovat	k5eAaBmIp3nP	diagnostikovat
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
léčebné	léčebný	k2eAgFnPc1d1	léčebná
metody	metoda	k1gFnPc1	metoda
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
oficiálně	oficiálně	k6eAd1	oficiálně
schváleny	schválit	k5eAaPmNgFnP	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
noční	noční	k2eAgFnSc2d1	noční
poluce	poluce	k1gFnSc2	poluce
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
masturbace	masturbace	k1gFnSc2	masturbace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
většina	většina	k1gFnSc1	většina
ortodoxních	ortodoxní	k2eAgMnPc2d1	ortodoxní
křesťanů	křesťan	k1gMnPc2	křesťan
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
nešpiní	špinit	k5eNaImIp3nP	špinit
čistotu	čistota	k1gFnSc4	čistota
svědomí	svědomí	k1gNnSc2	svědomí
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nedobrovolný	dobrovolný	k2eNgInSc4d1	nedobrovolný
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
akt	akt	k1gInSc4	akt
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Augustin	Augustin	k1gMnSc1	Augustin
modlil	modlit	k5eAaImAgMnS	modlit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
"	"	kIx"	"
<g/>
lepidla	lepidlo	k1gNnSc2	lepidlo
chtíče	chtíč	k1gInSc2	chtíč
<g/>
"	"	kIx"	"
a	a	k8xC	a
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
prosit	prosit	k5eAaImF	prosit
Boha	bůh	k1gMnSc4	bůh
o	o	k7c4	o
očištění	očištění	k1gNnSc4	očištění
duše	duše	k1gFnSc2	duše
od	od	k7c2	od
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
neduhů	neduh	k1gInPc2	neduh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
Bible	bible	k1gFnSc2	bible
přímo	přímo	k6eAd1	přímo
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
poluci	poluce	k1gFnSc4	poluce
v	v	k7c6	v
negativním	negativní	k2eAgNnSc6d1	negativní
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
označují	označovat	k5eAaImIp3nP	označovat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
"	"	kIx"	"
<g/>
nečistou	čistý	k2eNgFnSc4d1	nečistá
<g/>
"	"	kIx"	"
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
nakažlivou	nakažlivý	k2eAgFnSc4d1	nakažlivá
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
léčena	léčit	k5eAaImNgFnS	léčit
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
obřadů	obřad	k1gInPc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Deuteronomium	Deuteronomium	k1gNnSc4	Deuteronomium
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
(	(	kIx(	(
<g/>
1613	[number]	k4	1613
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bys	by	kYmCp2nS	by
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
vojensky	vojensky	k6eAd1	vojensky
proti	proti	k7c3	proti
nepřátelům	nepřítel	k1gMnPc3	nepřítel
svým	svůj	k3xOyFgInSc7	svůj
<g/>
,	,	kIx,	,
vystříhej	vystříhat	k5eAaBmRp2nS	vystříhat
se	se	k3xPyFc4	se
od	od	k7c2	od
všeliké	všeliký	k3yIgFnSc2	všeliký
zlé	zlý	k2eAgFnSc2d1	zlá
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
mezi	mezi	k7c7	mezi
vámi	vy	k3xPp2nPc7	vy
kdo	kdo	k3yQnSc1	kdo
<g/>
,	,	kIx,	,
ješto	ješta	k1gMnSc5	ješta
by	by	kYmCp3nP	by
poškvrněn	poškvrněn	k2eAgMnSc1d1	poškvrněn
byl	být	k5eAaImAgInS	být
příhodou	příhoda	k1gFnSc7	příhoda
noční	noční	k2eAgFnSc7d1	noční
<g/>
,	,	kIx,	,
vyjde	vyjít	k5eAaPmIp3nS	vyjít
ven	ven	k6eAd1	ven
z	z	k7c2	z
stanů	stan	k1gInPc2	stan
<g/>
,	,	kIx,	,
a	a	k8xC	a
nevejde	vejít	k5eNaPmIp3nS	vejít
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
když	když	k8xS	když
bude	být	k5eAaImBp3nS	být
k	k	k7c3	k
večerou	večerý	k2eAgFnSc4d1	večerý
<g/>
,	,	kIx,	,
umyje	umýt	k5eAaPmIp3nS	umýt
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc2	slunce
vejde	vejít	k5eAaPmIp3nS	vejít
do	do	k7c2	do
stanů	stan	k1gInPc2	stan
<g/>
.	.	kIx.	.
</s>
<s>
Leviticus	Leviticus	k1gInSc1	Leviticus
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
(	(	kIx(	(
<g/>
1613	[number]	k4	1613
<g/>
)	)	kIx)	)
Mluvil	mluvit	k5eAaImAgMnS	mluvit
pak	pak	k6eAd1	pak
Hospodin	Hospodin	k1gMnSc1	Hospodin
Mojžíšovi	Mojžíšův	k2eAgMnPc1d1	Mojžíšův
a	a	k8xC	a
Aronovi	Aronův	k2eAgMnPc1d1	Aronův
<g/>
,	,	kIx,	,
řka	říct	k5eAaPmSgMnS	říct
<g/>
:	:	kIx,	:
Mluvte	mluvit	k5eAaImRp2nP	mluvit
synům	syn	k1gMnPc3	syn
Izraelským	izraelský	k2eAgFnPc3d1	izraelská
a	a	k8xC	a
rcete	říct	k5eAaPmRp2nPwK	říct
jim	on	k3xPp3gMnPc3	on
<g/>
:	:	kIx,	:
Když	když	k8xS	když
by	by	kYmCp3nS	by
který	který	k3yQgMnSc1	který
muž	muž	k1gMnSc1	muž
trpěl	trpět	k5eAaImAgMnS	trpět
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
svého	svůj	k3xOyFgNnSc2	svůj
<g/>
,	,	kIx,	,
nečistý	čistý	k2eNgMnSc1d1	nečistý
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
nečistota	nečistota	k1gFnSc1	nečistota
jeho	jeho	k3xOp3gFnSc1	jeho
v	v	k7c4	v
toku	toka	k1gFnSc4	toka
jeho	jeho	k3xOp3gMnSc2	jeho
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
Jestliže	jestliže	k8xS	jestliže
<g/>
]	]	kIx)	]
vypěňuje	vypěňovat	k5eAaImIp3nS	vypěňovat
tělo	tělo	k1gNnSc4	tělo
jeho	jeho	k3xOp3gFnPc2	jeho
tok	toka	k1gFnPc2	toka
svůj	svůj	k3xOyFgInSc4	svůj
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
tok	tok	k1gInSc1	tok
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
jeho	jeho	k3xOp3gNnSc2	jeho
<g/>
,	,	kIx,	,
nečistota	nečistota	k1gFnSc1	nečistota
jeho	jeho	k3xOp3gMnSc3	jeho
jest	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
lůže	lůže	k?	lůže
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
by	by	kYmCp3nP	by
ležel	ležet	k5eAaImAgMnS	ležet
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
má	mít	k5eAaImIp3nS	mít
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
nečisté	čistý	k2eNgFnSc2d1	nečistá
bude	být	k5eAaImBp3nS	být
<g/>
;	;	kIx,	;
a	a	k8xC	a
všecko	všecek	k3xTgNnSc4	všecek
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yRnSc6	což
by	by	kYmCp3nS	by
seděl	sedět	k5eAaImAgMnS	sedět
<g/>
,	,	kIx,	,
nečisté	čistý	k2eNgNnSc1d1	nečisté
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
kdož	kdož	k3yRnSc1	kdož
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dotekl	dotéct	k5eAaPmAgInS	dotéct
lůže	lůže	k?	lůže
jeho	jeho	k3xOp3gFnSc1	jeho
<g/>
,	,	kIx,	,
zpéře	zpéřít	k5eAaPmIp3nS	zpéřít
roucha	roucho	k1gNnPc4	roucho
svá	svůj	k3xOyFgFnSc1	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
umyje	umýt	k5eAaPmIp3nS	umýt
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
nečistý	čistý	k2eNgMnSc1d1	nečistý
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
sedl	sednout	k5eAaPmAgMnS	sednout
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yRnSc6	což
seděl	sedět	k5eAaImAgMnS	sedět
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
má	mít	k5eAaImIp3nS	mít
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
zpéře	zpéř	k1gInSc2	zpéř
roucha	roucho	k1gNnSc2	roucho
svá	svůj	k3xOyFgFnSc1	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
umyje	umýt	k5eAaPmIp3nS	umýt
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
i	i	k8xC	i
bude	být	k5eAaImBp3nS	být
nečistý	čistý	k2eNgMnSc1d1	nečistý
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
kdo	kdo	k3yInSc1	kdo
dotekl	dotéct	k5eAaPmAgMnS	dotéct
těla	tělo	k1gNnSc2	tělo
trpícího	trpící	k2eAgNnSc2d1	trpící
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
zpéře	zpéř	k1gInSc2	zpéř
roucha	roucho	k1gNnSc2	roucho
svá	svůj	k3xOyFgFnSc1	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
umyje	umýt	k5eAaPmIp3nS	umýt
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
i	i	k8xC	i
bude	být	k5eAaImBp3nS	být
nečistý	čistý	k2eNgMnSc1d1	nečistý
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
trpí	trpět	k5eAaImIp3nS	trpět
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
plinul	plinout	k5eAaPmAgMnS	plinout
na	na	k7c4	na
čistého	čistý	k2eAgMnSc4d1	čistý
<g/>
,	,	kIx,	,
zpéře	zpéřít	k5eAaPmIp3nS	zpéřít
roucha	roucho	k1gNnPc4	roucho
svá	svůj	k3xOyFgFnSc1	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
umyje	umýt	k5eAaPmIp3nS	umýt
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
i	i	k8xC	i
bude	být	k5eAaImBp3nS	být
nečistý	čistý	k2eNgMnSc1d1	nečistý
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
by	by	kYmCp3nS	by
seděl	sedět	k5eAaImAgMnS	sedět
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
má	mít	k5eAaImIp3nS	mít
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
nečisté	čistý	k2eNgNnSc1d1	nečisté
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
kdož	kdož	k3yRnSc1	kdož
by	by	kYmCp3nS	by
koli	koli	k6eAd1	koli
dotekl	dotéct	k5eAaPmAgMnS	dotéct
se	se	k3xPyFc4	se
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
nečistý	čistý	k2eNgMnSc1d1	nečistý
bude	být	k5eAaImBp3nS	být
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
;	;	kIx,	;
a	a	k8xC	a
kdož	kdož	k3yRnSc1	kdož
by	by	kYmCp3nS	by
co	co	k9	co
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nesl	nést	k5eAaImAgMnS	nést
<g/>
,	,	kIx,	,
zpéře	zpéřít	k5eAaPmIp3nS	zpéřít
roucha	roucho	k1gNnPc4	roucho
svá	svůj	k3xOyFgFnSc1	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
umyje	umýt	k5eAaPmIp3nS	umýt
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
nečistý	čistý	k2eNgMnSc1d1	nečistý
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Kohož	kdož	k3yRnSc4	kdož
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dotekl	dotéct	k5eAaPmAgMnS	dotéct
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
má	mít	k5eAaImIp3nS	mít
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
a	a	k8xC	a
neumyl	umýt	k5eNaPmAgInS	umýt
rukou	ruka	k1gFnSc7	ruka
svých	svůj	k3xOyFgMnPc2	svůj
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
zpéře	zpéra	k1gFnSc3	zpéra
roucha	roucho	k1gNnSc2	roucho
svá	svůj	k3xOyFgFnSc1	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
umyje	umýt	k5eAaPmIp3nS	umýt
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
i	i	k8xC	i
bude	být	k5eAaImBp3nS	být
nečistý	čistý	k2eNgMnSc1d1	nečistý
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Nádoba	nádoba	k1gFnSc1	nádoba
hliněná	hliněný	k2eAgFnSc1d1	hliněná
<g/>
,	,	kIx,	,
kteréž	kteréž	k?	kteréž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dotekl	dotéct	k5eAaPmAgMnS	dotéct
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
má	mít	k5eAaImIp3nS	mít
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
rozbita	rozbít	k5eAaPmNgFnS	rozbít
<g/>
,	,	kIx,	,
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
nádoba	nádoba	k1gFnSc1	nádoba
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
vodou	voda	k1gFnSc7	voda
vymyta	vymyt	k2eAgFnSc1d1	vymyta
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
očištěn	očistit	k5eAaPmNgMnS	očistit
byl	být	k5eAaImAgMnS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
tok	tok	k1gInSc4	tok
semene	semeno	k1gNnSc2	semeno
trpěl	trpět	k5eAaImAgMnS	trpět
<g/>
,	,	kIx,	,
od	od	k7c2	od
toku	tok	k1gInSc2	tok
svého	své	k1gNnSc2	své
<g/>
,	,	kIx,	,
sečte	sečíst	k5eAaPmIp3nS	sečíst
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
očištění	očištění	k1gNnSc6	očištění
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpéře	zpéř	k1gFnPc1	zpéř
roucha	roucho	k1gNnSc2	roucho
svá	svůj	k3xOyFgFnSc1	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
živou	živý	k2eAgFnSc7d1	živá
zmyje	zmyj	k1gFnPc4	zmyj
tělo	tělo	k1gNnSc4	tělo
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
i	i	k8xC	i
bude	být	k5eAaImBp3nS	být
čist	čist	k2eAgInSc1d1	čist
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
dne	den	k1gInSc2	den
osmého	osmý	k4xOgNnSc2	osmý
vezme	vzít	k5eAaPmIp3nS	vzít
sobě	se	k3xPyFc3	se
dvě	dva	k4xCgFnPc4	dva
hrdličky	hrdlička	k1gFnPc4	hrdlička
aneb	aneb	k?	aneb
dvé	dvé	k?	dvé
holoubátek	holoubátko	k1gNnPc2	holoubátko
<g/>
,	,	kIx,	,
a	a	k8xC	a
přijda	přijda	k1gFnSc1	přijda
před	před	k7c4	před
Hospodina	Hospodin	k1gMnSc4	Hospodin
ke	k	k7c3	k
dveřím	dveře	k1gFnPc3	dveře
stánku	stánek	k1gInSc2	stánek
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
je	být	k5eAaImIp3nS	být
knězi	kněz	k1gMnSc3	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Kterýž	Kterýž	k?	Kterýž
obětovati	obětovat	k5eAaBmF	obětovat
je	on	k3xPp3gInPc4	on
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
za	za	k7c4	za
hřích	hřích	k1gInSc4	hřích
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
obět	obět	k?	obět
zápalnou	zápalný	k2eAgFnSc4d1	zápalná
<g/>
;	;	kIx,	;
a	a	k8xC	a
očistí	očistit	k5eAaPmIp3nP	očistit
jej	on	k3xPp3gMnSc4	on
kněz	kněz	k1gMnSc1	kněz
před	před	k7c7	před
Hospodinem	Hospodin	k1gMnSc7	Hospodin
od	od	k7c2	od
toku	tok	k1gInSc2	tok
jeho	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nS	by
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
símě	símě	k1gNnSc1	símě
scházení	scházení	k1gNnSc2	scházení
<g/>
,	,	kIx,	,
zmyje	zmyj	k1gFnSc2	zmyj
vodou	voda	k1gFnSc7	voda
všecko	všecek	k3xTgNnSc4	všecek
tělo	tělo	k1gNnSc4	tělo
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
nečistý	čistý	k2eNgMnSc1d1	nečistý
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
roucho	roucho	k1gNnSc1	roucho
i	i	k8xC	i
každá	každý	k3xTgFnSc1	každý
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
símě	símě	k1gNnSc1	símě
scházení	scházení	k1gNnSc2	scházení
<g/>
,	,	kIx,	,
zeprána	zeprán	k2eAgFnSc1d1	zeprán
bude	být	k5eAaImBp3nS	být
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
nečistá	čistý	k2eNgFnSc1d1	nečistá
bude	být	k5eAaImBp3nS	být
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
také	také	k9	také
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterouž	kterouž	k?	kterouž
by	by	kYmCp3nS	by
obýval	obývat	k5eAaImAgMnS	obývat
muž	muž	k1gMnSc1	muž
scházením	scházení	k1gNnSc7	scházení
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
oba	dva	k4xCgMnPc1	dva
<g/>
]	]	kIx)	]
zmyjí	zmyjet	k5eAaPmIp3nS	zmyjet
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
nečistí	nečistý	k1gMnPc1	nečistý
budou	být	k5eAaImBp3nP	být
až	až	k6eAd1	až
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
křesťané	křesťan	k1gMnPc1	křesťan
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc4d1	uvedená
citace	citace	k1gFnPc4	citace
vnímají	vnímat	k5eAaImIp3nP	vnímat
jako	jako	k9	jako
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
důkaz	důkaz	k1gInSc4	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
hřích	hřích	k1gInSc4	hřích
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mimovolní	mimovolní	k2eAgFnSc4d1	mimovolní
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
však	však	k9	však
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
citace	citace	k1gFnSc1	citace
Deuteronomia	Deuteronomium	k1gNnSc2	Deuteronomium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úplně	úplně	k6eAd1	úplně
jiném	jiný	k2eAgInSc6d1	jiný
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
Leviticus	Leviticus	k1gInSc1	Leviticus
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
i	i	k9	i
o	o	k7c6	o
menstruaci	menstruace	k1gFnSc6	menstruace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
křesťanství	křesťanství	k1gNnSc6	křesťanství
opomíjena	opomíjet	k5eAaImNgFnS	opomíjet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současné	současný	k2eAgFnSc2d1	současná
teorie	teorie	k1gFnSc2	teorie
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc4	tento
pasáže	pasáž	k1gFnPc4	pasáž
Bible	bible	k1gFnPc4	bible
vést	vést	k5eAaImF	vést
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
správné	správný	k2eAgFnSc2d1	správná
hygieny	hygiena	k1gFnSc2	hygiena
a	a	k8xC	a
k	k	k7c3	k
prevenci	prevence	k1gFnSc3	prevence
reálných	reálný	k2eAgNnPc2d1	reálné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pokud	pokud	k8xS	pokud
osoba	osoba	k1gFnSc1	osoba
trpící	trpící	k2eAgFnSc1d1	trpící
výtoky	výtok	k1gInPc1	výtok
byla	být	k5eAaImAgFnS	být
přenašečem	přenašeč	k1gInSc7	přenašeč
závažného	závažný	k2eAgNnSc2d1	závažné
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc1d1	uvedená
citace	citace	k1gFnPc1	citace
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
vyřazení	vyřazení	k1gNnSc3	vyřazení
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc1d1	uvedená
pasáže	pasáž	k1gFnPc1	pasáž
se	se	k3xPyFc4	se
netýkají	týkat	k5eNaImIp3nP	týkat
spermatu	sperma	k1gNnSc3	sperma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krve	krev	k1gFnSc2	krev
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
substancí	substance	k1gFnPc2	substance
indikujících	indikující	k2eAgNnPc2d1	indikující
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
