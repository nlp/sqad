<s>
Schumi	Schu	k1gFnPc7
(	(	kIx(
<g/>
planetka	planetka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
SchumiIdentifikátory	SchumiIdentifikátor	k1gInPc1
Označení	označení	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
15761	#num#	k4
<g/>
)	)	kIx)
Schumi	Schu	k1gFnPc7
Předběžné	předběžný	k2eAgInPc1d1
označení	označení	k1gNnSc1
</s>
<s>
1992	#num#	k4
SM16	SM16	k1gFnPc2
Katalogové	katalogový	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
15761	#num#	k4
Objevena	objeven	k2eAgFnSc1d1
Datum	datum	k1gNnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1992	#num#	k4
Místo	místo	k7c2
</s>
<s>
Hvězdárna	hvězdárna	k1gFnSc1
Tautenburg	Tautenburg	k1gMnSc1
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Lutz	Lutz	k1gMnSc1
D.	D.	kA
Schmadel	Schmadlo	k1gNnPc2
<g/>
,	,	kIx,
<g/>
Freimut	Freimut	k2eAgInSc1d1
Börngen	Börngen	k1gInSc1
Jméno	jméno	k1gNnSc1
po	po	k7c4
</s>
<s>
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
Elementy	element	k1gInPc1
dráhy	dráha	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Perioda	perioda	k1gFnSc1
(	(	kIx(
<g/>
oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4,21	4,21	k4
roku	rok	k1gInSc2
Fyzikální	fyzikální	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
15761	#num#	k4
<g/>
)	)	kIx)
Schumi	Schumi	k1gFnSc1
je	být	k5eAaImIp3nS
planetka	planetka	k1gFnSc1
nacházející	nacházející	k2eAgFnSc1d1
se	se	k3xPyFc4
v	v	k7c6
hlavním	hlavní	k2eAgInSc6d1
pásu	pás	k1gInSc6
asteroidů	asteroid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevili	objevit	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
Lutz	Lutz	k1gInSc1
D.	D.	kA
Schmadel	Schmadlo	k1gNnPc2
a	a	k8xC
Freimut	Freimut	k2eAgInSc1d1
Börngen	Börngen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
pilotovi	pilot	k1gMnSc6
formule	formule	k1gFnSc2
1	#num#	k4
a	a	k8xC
pozdějším	pozdní	k2eAgNnSc6d2
sedminásobném	sedminásobný	k2eAgNnSc6d1
mistru	mistr	k1gMnSc3
světa	svět	k1gInSc2
Michaelu	Michael	k1gMnSc3
Schumacherovi	Schumacher	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc4
planetek	planetka	k1gFnPc2
15751-16000	15751-16000	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
15761	#num#	k4
<g/>
)	)	kIx)
Schumi	Schu	k1gFnPc7
na	na	k7c6
webu	web	k1gInSc6
České	český	k2eAgFnSc2d1
astronomické	astronomický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
JPL	JPL	kA
Small-Body	Small-Boda	k1gFnPc1
Database	Databasa	k1gFnSc3
Browser	Browser	k1gMnSc1
on	on	k3xPp3gMnSc1
15761	#num#	k4
Schumi	Schu	k1gFnPc7
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
