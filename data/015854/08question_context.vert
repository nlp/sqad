<s>
Zdravá	zdravý	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
též	též	k9
Škola	škola	k1gFnSc1
podporující	podporující	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rámcový	rámcový	k2eAgInSc1d1
program	program	k1gInSc1
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
jsou	být	k5eAaImIp3nP
zapojeny	zapojen	k2eAgFnPc4d1
mateřské	mateřský	k2eAgFnPc4d1
<g/>
,	,	kIx,
základní	základní	k2eAgFnPc4d1
a	a	k8xC
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>