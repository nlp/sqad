<s>
Zdravá	zdravý	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Logo	logo	k1gNnSc4
programu	program	k1gInSc2
Zdravá	zdravý	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Zdravá	zdravý	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
též	též	k9
Škola	škola	k1gFnSc1
podporující	podporující	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rámcový	rámcový	k2eAgInSc1d1
program	program	k1gInSc1
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
jsou	být	k5eAaImIp3nP
zapojeny	zapojen	k2eAgFnPc4d1
mateřské	mateřský	k2eAgFnPc4d1
<g/>
,	,	kIx,
základní	základní	k2eAgFnPc4d1
a	a	k8xC
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
programu	program	k1gInSc2
je	být	k5eAaImIp3nS
zoptimalizovat	zoptimalizovat	k5eAaPmF
vývoj	vývoj	k1gInSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
každého	každý	k3xTgMnSc2
jednotlivce	jednotlivec	k1gMnSc2
<g/>
,	,	kIx,
žáka	žák	k1gMnSc2
a	a	k8xC
učitele	učitel	k1gMnSc2
v	v	k7c6
součinnosti	součinnost	k1gFnSc6
s	s	k7c7
rodiči	rodič	k1gMnPc7
a	a	k8xC
školou	škola	k1gFnSc7
samotnou	samotný	k2eAgFnSc7d1
<g/>
,	,	kIx,
po	po	k7c6
stránce	stránka	k1gFnSc6
tělesné	tělesný	k2eAgFnSc2d1
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc2d1
i	i	k8xC
duševní	duševní	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
projekt	projekt	k1gInSc1
realizován	realizovat	k5eAaBmNgInS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Skotsko	Skotsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
se	se	k3xPyFc4
záštity	záštita	k1gFnSc2
chopila	chopit	k5eAaPmAgFnS
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
roku	rok	k1gInSc3
2013	#num#	k4
bylo	být	k5eAaImAgNnS
do	do	k7c2
programu	program	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Evropy	Evropa	k1gFnSc2
zapojeno	zapojit	k5eAaPmNgNnS
na	na	k7c4
35	#num#	k4
000	#num#	k4
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
tohoto	tento	k3xDgInSc2
projektu	projekt	k1gInSc2
bylo	být	k5eAaImAgNnS
zlepšit	zlepšit	k5eAaPmF
zdraví	zdraví	k1gNnSc4
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
učitelů	učitel	k1gMnPc2
<g/>
,	,	kIx,
rodin	rodina	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
členů	člen	k1gInPc2
komunity	komunita	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
školní	školní	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
byl	být	k5eAaImAgInS
inspirován	inspirovat	k5eAaBmNgInS
Ottawskou	ottawský	k2eAgFnSc7d1
chartou	charta	k1gFnSc7
pro	pro	k7c4
podporu	podpora	k1gFnSc4
zdraví	zdraví	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
také	také	k9
Jakartskou	Jakartský	k2eAgFnSc7d1
deklarací	deklarace	k1gFnSc7
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
podpoře	podpora	k1gFnSc6
zdraví	zdraví	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
cíle	cíl	k1gInPc1
a	a	k8xC
zásady	zásada	k1gFnPc1
zdravé	zdravý	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
rámcový	rámcový	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
,	,	kIx,
škola	škola	k1gFnSc1
si	se	k3xPyFc3
stanovuje	stanovovat	k5eAaImIp3nS
své	své	k1gNnSc4
cíle	cíl	k1gInSc2
sama	sám	k3xTgMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
se	se	k3xPyFc4
do	do	k7c2
projektu	projekt	k1gInSc2
také	také	k9
přihlásí	přihlásit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
tudíž	tudíž	k8xC
obecně	obecně	k6eAd1
definovat	definovat	k5eAaBmF
cíle	cíl	k1gInPc4
a	a	k8xC
zásady	zásada	k1gFnPc4
všech	všecek	k3xTgFnPc2
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
každá	každý	k3xTgFnSc1
instituce	instituce	k1gFnSc1
může	moct	k5eAaImIp3nS
opřít	opřít	k5eAaPmF
o	o	k7c4
tři	tři	k4xCgInPc4
pilíře	pilíř	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mohou	moct	k5eAaImIp3nP
sloužit	sloužit	k5eAaImF
jako	jako	k9
návod	návod	k1gInSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohoda	pohoda	k1gFnSc1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
věcného	věcný	k2eAgMnSc2d1
<g/>
,	,	kIx,
sociálního	sociální	k2eAgInSc2d1
<g/>
,	,	kIx,
organizačního	organizační	k2eAgInSc2d1
<g/>
)	)	kIx)
</s>
<s>
Zdravé	zdravý	k2eAgNnSc1d1
učení	učení	k1gNnSc1
(	(	kIx(
<g/>
smysluplnost	smysluplnost	k1gFnSc1
<g/>
,	,	kIx,
možnost	možnost	k1gFnSc1
výběru	výběr	k1gInSc2
<g/>
,	,	kIx,
přiměřenost	přiměřenost	k1gFnSc1
<g/>
,	,	kIx,
spoluúčast	spoluúčast	k1gFnSc1
<g/>
,	,	kIx,
spolupráce	spolupráce	k1gFnSc1
<g/>
,	,	kIx,
motivující	motivující	k2eAgNnSc1d1
hodnocení	hodnocení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Otevřené	otevřený	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
(	(	kIx(
<g/>
škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
modelem	model	k1gInSc7
demokratického	demokratický	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
<g/>
,	,	kIx,
pořádá	pořádat	k5eAaImIp3nS
veřejné	veřejný	k2eAgFnPc4d1
akce	akce	k1gFnPc4
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Program	program	k1gInSc1
Škola	škola	k1gFnSc1
podporující	podporující	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
(	(	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
ŠPZ	ŠPZ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
od	od	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
také	také	k9
školám	škola	k1gFnPc3
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
se	se	k3xPyFc4
postupně	postupně	k6eAd1
mění	měnit	k5eAaImIp3nS
koordinace	koordinace	k1gFnSc1
celé	celý	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
z	z	k7c2
celostátní	celostátní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
na	na	k7c4
krajskou	krajský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
projektu	projekt	k1gInSc2
ŠPZ	ŠPZ	kA
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
zapojit	zapojit	k5eAaPmF
každá	každý	k3xTgFnSc1
mateřská	mateřský	k2eAgFnSc1d1
<g/>
,	,	kIx,
základní	základní	k2eAgFnSc1d1
i	i	k8xC
střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vytvářejí	vytvářet	k5eAaImIp3nP
dohromady	dohromady	k6eAd1
tzv.	tzv.	kA
Národní	národní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
škol	škola	k1gFnPc2
podporujících	podporující	k2eAgFnPc2d1
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návod	návod	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
přihlásit	přihlásit	k5eAaPmF
do	do	k7c2
programu	program	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
na	na	k7c6
internetových	internetový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
Státního	státní	k2eAgInSc2d1
zdravotního	zdravotní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
stát	stát	k1gInSc1
ŠPZ	ŠPZ	kA
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
škola	škola	k1gFnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
stát	stát	k5eAaImF,k5eAaPmF
se	s	k7c7
školou	škola	k1gFnSc7
podporující	podporující	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
si	se	k3xPyFc3
nejdříve	dříve	k6eAd3
prostudovat	prostudovat	k5eAaPmF
metodiky	metodika	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
filozofii	filozofie	k1gFnSc4
ŠPZ	ŠPZ	kA
<g/>
,	,	kIx,
její	její	k3xOp3gInPc1
principy	princip	k1gInPc1
a	a	k8xC
zásady	zásada	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
krokem	krok	k1gInSc7
je	být	k5eAaImIp3nS
zaslání	zaslání	k1gNnSc1
přihlášky	přihláška	k1gFnSc2
garantovi	garant	k1gMnSc3
ŠPZ	ŠPZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Garantem	garant	k1gMnSc7
je	být	k5eAaImIp3nS
Státní	státní	k2eAgInSc1d1
zdravotní	zdravotní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1
přihláška	přihláška	k1gFnSc1
musí	muset	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
úplný	úplný	k2eAgInSc4d1
název	název	k1gInSc4
a	a	k8xC
adresu	adresa	k1gFnSc4
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
kontakty	kontakt	k1gInPc1
a	a	k8xC
jméno	jméno	k1gNnSc1
ředitele	ředitel	k1gMnSc2
<g/>
/	/	kIx~
<g/>
ky	ky	k?
</s>
<s>
prezentaci	prezentace	k1gFnSc4
školy	škola	k1gFnSc2
(	(	kIx(
<g/>
základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
vč.	vč.	k?
názvu	název	k1gInSc2
přijatých	přijatý	k2eAgInPc2d1
vzdělávacích	vzdělávací	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
odpověď	odpověď	k1gFnSc4
na	na	k7c4
tři	tři	k4xCgFnPc4
otázky	otázka	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Proč	proč	k6eAd1
školu	škola	k1gFnSc4
program	program	k1gInSc1
Škola	škola	k1gFnSc1
podporující	podporující	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
zajímá	zajímat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
všechno	všechen	k3xTgNnSc1
projevuje	projevovat	k5eAaImIp3nS
o	o	k7c4
program	program	k1gInSc4
ŠPZ	ŠPZ	kA
zájem	zájem	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
učitele	učitel	k1gMnSc2
<g/>
,	,	kIx,
rodiče	rodič	k1gMnPc1
<g/>
,	,	kIx,
žáci	žák	k1gMnPc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc4
<g/>
,	,	kIx,
jiný	jiný	k2eAgMnSc1d1
partner	partner	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
od	od	k7c2
programu	program	k1gInSc2
ŠPZ	ŠPZ	kA
pro	pro	k7c4
sebe	sebe	k3xPyFc4
očekává	očekávat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
Po	po	k7c6
zaslání	zaslání	k1gNnSc6
přihlášky	přihláška	k1gFnSc2
je	být	k5eAaImIp3nS
škola	škola	k1gFnSc1
automaticky	automaticky	k6eAd1
zařazena	zařadit	k5eAaPmNgFnS
do	do	k7c2
projektu	projekt	k1gInSc2
a	a	k8xC
stanovuje	stanovovat	k5eAaImIp3nS
si	se	k3xPyFc3
vlastní	vlastní	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
bude	být	k5eAaImBp3nS
připravovat	připravovat	k5eAaImF
projekt	projekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
využít	využít	k5eAaPmF
nabídky	nabídka	k1gFnPc4
Státního	státní	k2eAgInSc2d1
zdravotního	zdravotní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
či	či	k8xC
k	k	k7c3
tomu	ten	k3xDgMnSc3
přistoupit	přistoupit	k5eAaPmF
dle	dle	k7c2
svého	svůj	k3xOyFgNnSc2
vlastního	vlastní	k2eAgNnSc2d1
uvážení	uvážení	k1gNnSc2
podle	podle	k7c2
předepsaných	předepsaný	k2eAgFnPc2d1
metodik	metodika	k1gFnPc2
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
krokem	krok	k1gInSc7
je	být	k5eAaImIp3nS
vypracovat	vypracovat	k5eAaPmF
předběžný	předběžný	k2eAgInSc4d1
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
bude	být	k5eAaImBp3nS
prezentovat	prezentovat	k5eAaBmF
reálné	reálný	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
chce	chtít	k5eAaImIp3nS
škola	škola	k1gFnSc1
dosáhnout	dosáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vypracování	vypracování	k1gNnSc6
školního	školní	k2eAgInSc2d1
projektu	projekt	k1gInSc2
jej	on	k3xPp3gMnSc4
zástupci	zástupce	k1gMnPc7
jednotlivých	jednotlivý	k2eAgMnPc6d1
školou	škola	k1gFnSc7
zašlou	zaslat	k5eAaPmIp3nP
na	na	k7c4
adresu	adresa	k1gFnSc4
SZÚ	SZÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
SZÚ	SZÚ	kA
projekt	projekt	k1gInSc4
posoudí	posoudit	k5eAaPmIp3nP
a	a	k8xC
zašlou	zaslat	k5eAaPmIp3nP
škole	škola	k1gFnSc3
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
návštěva	návštěva	k1gFnSc1
školy	škola	k1gFnSc2
a	a	k8xC
přátelské	přátelský	k2eAgNnSc4d1
setkání	setkání	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
škola	škola	k1gFnSc1
seznámí	seznámit	k5eAaPmIp3nS
se	s	k7c7
všemi	všecek	k3xTgFnPc7
podmínkami	podmínka	k1gFnPc7
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
projekt	projekt	k1gInSc1
realizovat	realizovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koordinační	koordinační	k2eAgInSc1d1
tým	tým	k1gInSc1
SZÚ	SZÚ	kA
následně	následně	k6eAd1
pozve	pozvat	k5eAaPmIp3nS
zástupce	zástupce	k1gMnSc1
školy	škola	k1gFnSc2
k	k	k7c3
tzv.	tzv.	kA
kulatému	kulatý	k2eAgInSc3d1
stolu	stol	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
prezentaci	prezentace	k1gFnSc3
školního	školní	k2eAgInSc2d1
projektu	projekt	k1gInSc2
a	a	k8xC
následně	následně	k6eAd1
k	k	k7c3
smluvnímu	smluvní	k2eAgNnSc3d1
podepsání	podepsání	k1gNnSc3
o	o	k7c6
vstupu	vstup	k1gInSc6
do	do	k7c2
programu	program	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pokud	pokud	k8xS
škola	škola	k1gFnSc1
uspěje	uspět	k5eAaPmIp3nS
a	a	k8xC
stane	stanout	k5eAaPmIp3nS
se	se	k3xPyFc4
součástí	součást	k1gFnSc7
projektu	projekt	k1gInSc2
ŠPZ	ŠPZ	kA
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
si	se	k3xPyFc3
podporu	podpora	k1gFnSc4
zdraví	zdraví	k1gNnSc2
ve	v	k7c6
škole	škola	k1gFnSc6
zařadit	zařadit	k5eAaPmF
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
kurikula	kurikulum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
LEE	Lea	k1gFnSc3
<g/>
,	,	kIx,
A.	A.	kA
TSANG	TSANG	kA
<g/>
,	,	kIx,
C.	C.	kA
LEE	Lea	k1gFnSc6
<g/>
,	,	kIx,
S	s	k7c7
H.	H.	kA
T	T	kA
<g/>
,	,	kIx,
C	C	kA
Y.	Y.	kA
A	a	k8xC
comprehensive	comprehensiev	k1gFnSc2
“	“	k?
<g/>
Healthy	Healtha	k1gFnSc2
Schools	Schoolsa	k1gFnPc2
Programme	Programme	k1gMnSc1
<g/>
”	”	k?
to	ten	k3xDgNnSc1
promote	promot	k1gInSc5
school	school	k1gInSc4
health	healtha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
J	J	kA
Epidemiol	Epidemiol	k1gInSc4
Community	Communita	k1gFnSc2
Health	Health	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
KOHOUTEK	Kohoutek	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZDRAVÁ	zdravý	k2eAgFnSc1d1
ŠKOLA	škola	k1gFnSc1
ZORNÝM	zorný	k2eAgInSc7d1
ÚHLEM	úhel	k1gInSc7
STUDENTŮ	student	k1gMnPc2
PEDAGOGICKÉ	pedagogický	k2eAgFnSc2d1
FAKULTY	fakulta	k1gFnSc2
MU	MU	kA
<g/>
.	.	kIx.
www.ped.muni.cz	www.ped.muni.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NEJEDLÁ	Nejedlá	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
škola	škola	k1gFnSc1
podporující	podporující	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
406	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
104	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WORLD	WORLD	kA
TRADE	TRADE	kA
ORGANIZATION	ORGANIZATION	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Global	globat	k5eAaImAgMnS
school	school	k1gInSc4
health	health	k1gMnSc1
initiative	initiativ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
WHO	WHO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KRUNCLOVÁ	KRUNCLOVÁ	kA
<g/>
,	,	kIx,
Marcela	Marcela	k1gFnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
programu	program	k1gInSc2
ŠPZ	ŠPZ	kA
v	v	k7c6
současnosti	současnost	k1gFnSc6
a	a	k8xC
historickém	historický	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
www.vychovakezdravi.cz	www.vychovakezdravi.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výchova	výchova	k1gFnSc1
ke	k	k7c3
zdraví	zdraví	k1gNnSc3
<g/>
.	.	kIx.
www.vychovakezdravi.cz	www.vychovakezdravi.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jak	jak	k6eAd1
se	se	k3xPyFc4
stát	stát	k1gInSc1
ŠPZ	ŠPZ	kA
<g/>
,	,	kIx,
SZÚ	SZÚ	kA
<g/>
.	.	kIx.
www.szu.cz	www.szu.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ANSCHLAGOVÁ	ANSCHLAGOVÁ	kA
<g/>
,	,	kIx,
Dagmar	Dagmar	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PROJEKT	projekt	k1gInSc1
ŠKOLA	škola	k1gFnSc1
PODPORUJÍCÍ	podporující	k2eAgNnSc1d1
ZDRAVÍ	zdraví	k1gNnSc1
<g/>
.	.	kIx.
www.kr-kralovehradecky.cz	www.kr-kralovehradecky.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.kr-kralovehradecky.cz	www.kr-kralovehradecky.cz	k1gMnSc1
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POLIAKOVÁ	POLIAKOVÁ	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srovnání	srovnání	k1gNnSc1
výskytu	výskyt	k1gInSc2
stresorů	stresor	k1gInPc2
percipovaných	percipovaný	k2eAgInPc2d1
žáky	žák	k1gMnPc7
běžných	běžný	k2eAgFnPc2d1
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
žáky	žák	k1gMnPc7
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
zapojených	zapojený	k2eAgFnPc2d1
do	do	k7c2
projektu	projekt	k1gInSc2
Zdravá	zdravý	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
is	is	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NEJEDLÁ	Nejedlá	k1gFnSc1
Marie	Maria	k1gFnSc2
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
Škola	škola	k1gFnSc1
podporující	podporující	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Klinika	klinika	k1gFnSc1
adiktologie	adiktologie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Všeobecná	všeobecný	k2eAgFnSc1d1
fakultní	fakultní	k2eAgFnSc1d1
nemocnice	nemocnice	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
NLN	NLN	kA
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
406	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KRUNCLOVÁ	KRUNCLOVÁ	kA
,	,	kIx,
Marcela	Marcela	k1gFnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
programu	program	k1gInSc2
ŠPZ	ŠPZ	kA
v	v	k7c6
současnosti	současnost	k1gFnSc6
a	a	k8xC
historickém	historický	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GEOPRINT	GEOPRINT	kA
<g/>
.	.	kIx.
</s>
<s>
POLIAKOVÁ	POLIAKOVÁ	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srovnání	srovnání	k1gNnSc1
výskytu	výskyt	k1gInSc2
stresorů	stresor	k1gInPc2
percipovaných	percipovaný	k2eAgInPc2d1
žáky	žák	k1gMnPc7
běžných	běžný	k2eAgFnPc2d1
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
žáky	žák	k1gMnPc7
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
zapojených	zapojený	k2eAgFnPc2d1
do	do	k7c2
projektu	projekt	k1gInSc2
Zdravá	zdravý	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Psychologický	psychologický	k2eAgInSc4d1
ústav	ústav	k1gInSc1
</s>
<s>
KOHOUTEK	Kohoutek	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdravá	zdravý	k2eAgFnSc1d1
škola	škola	k1gFnSc1
zorným	zorný	k2eAgInSc7d1
pohledem	pohled	k1gInSc7
studentů	student	k1gMnPc2
pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
MU	MU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
diskurs	diskurs	k1gInSc1
zkoumání	zkoumání	k1gNnSc2
školy	škola	k1gFnSc2
a	a	k8xC
zdraví	zdraví	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Státního	státní	k2eAgInSc2d1
zdravotního	zdravotní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
</s>
