<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
nebo	nebo	k8xC	nebo
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
parlamentním	parlamentní	k2eAgInSc6d1	parlamentní
systému	systém	k1gInSc6	systém
druhá	druhý	k4xOgFnSc1	druhý
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
monarchiích	monarchie	k1gFnPc6	monarchie
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
modelovým	modelový	k2eAgInSc7d1	modelový
příkladem	příklad	k1gInSc7	příklad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
slučitelná	slučitelný	k2eAgFnSc1d1	slučitelná
výhradně	výhradně	k6eAd1	výhradně
s	s	k7c7	s
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
parlamentnímu	parlamentní	k2eAgInSc3d1	parlamentní
systému	systém	k1gInSc3	systém
má	mít	k5eAaImIp3nS	mít
prezident	prezident	k1gMnSc1	prezident
více	hodně	k6eAd2	hodně
pravomocí	pravomoc	k1gFnSc7	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
kritiků	kritik	k1gMnPc2	kritik
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
nastolena	nastolen	k2eAgFnSc1d1	nastolena
tendence	tendence	k1gFnSc1	tendence
k	k	k7c3	k
autoritářským	autoritářský	k2eAgFnPc3d1	autoritářská
formám	forma	k1gFnPc3	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpodstatnější	podstatný	k2eAgInPc4d3	nejpodstatnější
rysy	rys	k1gInPc4	rys
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
republiky	republika	k1gFnSc2	republika
patří	patřit	k5eAaImIp3nS	patřit
to	ten	k3xDgNnSc1	ten
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
že	že	k8xS	že
legitimita	legitimita	k1gFnSc1	legitimita
výkonné	výkonný	k2eAgFnSc2d1	výkonná
i	i	k8xC	i
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
moci	moc	k1gFnSc2	moc
je	být	k5eAaImIp3nS	být
odvozena	odvozen	k2eAgFnSc1d1	odvozena
od	od	k7c2	od
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
silné	silný	k2eAgNnSc4d1	silné
postavení	postavení	k1gNnSc4	postavení
obou	dva	k4xCgInPc2	dva
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
<g/>
[	[	kIx(	[
<g/>
kde	kde	k6eAd1	kde
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
prezident	prezident	k1gMnSc1	prezident
i	i	k8xC	i
zákonodárci	zákonodárce	k1gMnPc1	zákonodárce
voleni	volit	k5eAaImNgMnP	volit
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
sborem	sbor	k1gInSc7	sbor
volitelů	volitel	k1gMnPc2	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
základních	základní	k2eAgFnPc6d1	základní
formách	forma	k1gFnPc6	forma
prezidencialismu	prezidencialismus	k1gInSc2	prezidencialismus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ovšem	ovšem	k9	ovšem
nejsou	být	k5eNaImIp3nP	být
geograficky	geograficky	k6eAd1	geograficky
ohraničeny	ohraničit	k5eAaPmNgInP	ohraničit
na	na	k7c4	na
kontinent	kontinent	k1gInSc4	kontinent
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
severoamerický	severoamerický	k2eAgInSc4d1	severoamerický
prezidencialismus	prezidencialismus	k1gInSc4	prezidencialismus
–	–	k?	–
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgFnSc2d1	americká
jihoamerický	jihoamerický	k2eAgInSc4d1	jihoamerický
prezidencialismus	prezidencialismus	k1gInSc4	prezidencialismus
–	–	k?	–
u	u	k7c2	u
režimů	režim	k1gInPc2	režim
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
strukturálně	strukturálně	k6eAd1	strukturálně
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
první	první	k4xOgFnSc2	první
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavním	hlavní	k2eAgInSc7d1	hlavní
rozdílem	rozdíl	k1gInSc7	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
multipartismu	multipartismus	k1gInSc2	multipartismus
(	(	kIx(	(
<g/>
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
roli	role	k1gFnSc4	role
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
obchází	obcházet	k5eAaImIp3nS	obcházet
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
je	být	k5eAaImIp3nS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
od	od	k7c2	od
dalších	další	k2eAgFnPc2d1	další
politických	politický	k2eAgFnPc2d1	politická
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
<g/>
,	,	kIx,	,
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
a	a	k8xC	a
soudní	soudní	k2eAgMnSc1d1	soudní
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
naprosto	naprosto	k6eAd1	naprosto
oddělené	oddělený	k2eAgFnPc1d1	oddělená
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
orgán	orgán	k1gInSc1	orgán
je	být	k5eAaImIp3nS	být
všemocný	všemocný	k2eAgMnSc1d1	všemocný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
žádné	žádný	k3yNgInPc4	žádný
exekutivní	exekutivní	k2eAgInPc4d1	exekutivní
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgMnS	vybavit
úplnou	úplný	k2eAgFnSc7d1	úplná
mocí	moc	k1gFnSc7	moc
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
zákonodárných	zákonodárný	k2eAgFnPc2d1	zákonodárná
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
ústavou	ústava	k1gFnSc7	ústava
daným	daný	k2eAgMnSc7d1	daný
držitelem	držitel	k1gMnSc7	držitel
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předsedá	předsedat	k5eAaImIp3nS	předsedat
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
osoby	osoba	k1gFnPc4	osoba
exekutivy	exekutiva	k1gFnSc2	exekutiva
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
také	také	k9	také
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
není	být	k5eNaImIp3nS	být
odpovědný	odpovědný	k2eAgInSc1d1	odpovědný
legislativnímu	legislativní	k2eAgInSc3d1	legislativní
orgánu	orgán	k1gInSc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
republice	republika	k1gFnSc6	republika
neexistuje	existovat	k5eNaImIp3nS	existovat
institut	institut	k1gInSc4	institut
vota	votum	k1gNnSc2	votum
důvěry	důvěra	k1gFnSc2	důvěra
či	či	k8xC	či
vota	votum	k1gNnSc2	votum
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
a	a	k8xC	a
zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
orgán	orgán	k1gInSc1	orgán
nemůže	moct	k5eNaImIp3nS	moct
politickou	politický	k2eAgFnSc7d1	politická
cestou	cesta	k1gFnSc7	cesta
odstranit	odstranit	k5eAaPmF	odstranit
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
ani	ani	k8xC	ani
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
politickou	politický	k2eAgFnSc4d1	politická
neodpovědnost	neodpovědnost	k1gFnSc4	neodpovědnost
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
vůči	vůči	k7c3	vůči
parlamentu	parlament	k1gInSc3	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
zase	zase	k9	zase
nemůže	moct	k5eNaImIp3nS	moct
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
,	,	kIx,	,
iniciovat	iniciovat	k5eAaBmF	iniciovat
jeho	jeho	k3xOp3gNnSc4	jeho
zasedání	zasedání	k1gNnSc4	zasedání
či	či	k8xC	či
disponovat	disponovat	k5eAaBmF	disponovat
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
,,	,,	k?	,,
</s>
