<s>
Aleramovci	Aleramovec	k1gMnPc1
</s>
<s>
Aleramové	Aleramový	k2eAgFnPc1d1
či	či	k8xC
Aleramovci	Aleramovec	k1gMnPc1
/	/	kIx~
Aleramici	Aleramik	k1gMnPc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Aleramidi	Aleramid	k1gMnPc1
<g/>
)	)	kIx)
Erb	erb	k1gInSc1
rodu	rod	k1gInSc2
Aleramů	Aleram	k1gInPc2
<g/>
:	:	kIx,
červené	červený	k2eAgNnSc4d1
břevno	břevno	k1gNnSc4
ve	v	k7c6
stříbrném	stříbrný	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
.	.	kIx.
<g/>
Země	země	k1gFnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Tituly	titul	k1gInPc1
</s>
<s>
svobodní	svobodný	k2eAgMnPc1d1
páni	pan	k1gMnPc1
<g/>
,	,	kIx,
hrabata	hrabě	k1gNnPc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Aleramo	Aleramo	k6eAd1
z	z	k7c2
Monferrata	Monferrat	k1gMnSc2
Rok	rok	k1gInSc4
založení	založení	k1gNnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Aleramové	Aleramový	k2eAgFnPc1d1
či	či	k8xC
Aleramovci	Aleramovec	k1gInSc6
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
Aleramici	Aleramik	k1gMnPc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
spíše	spíše	k9
jako	jako	k8xS,k8xC
Aleramidi	Aleramid	k1gMnPc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
někdejší	někdejší	k2eAgInSc4d1
významný	významný	k2eAgInSc4d1
italský	italský	k2eAgInSc4d1
panský	panský	k2eAgInSc4d1
rod	rod	k1gInSc4
franského	franský	k2eAgInSc2d1
či	či	k8xC
franko-salického	franko-salický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
usídlil	usídlit	k5eAaPmAgMnS
v	v	k7c6
několika	několik	k4yIc6
větvích	větev	k1gFnPc6
v	v	k7c6
italských	italský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Piemontsko	Piemontsko	k1gNnSc1
a	a	k8xC
Ligurie	Ligurie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
spravoval	spravovat	k5eAaImAgInS
panství	panství	k1gNnSc4
Monferrato	Monferrat	k2eAgNnSc4d1
<g/>
,	,	kIx,
Saluzzo	Saluzza	k1gFnSc5
<g/>
,	,	kIx,
Savona	Savona	k1gFnSc1
a	a	k8xC
další	další	k2eAgNnSc1d1
území	území	k1gNnSc1
mezi	mezi	k7c7
západní	západní	k2eAgFnSc7d1
Ligurií	Ligurie	k1gFnSc7
a	a	k8xC
Dolního	dolní	k2eAgNnSc2d1
Piemontska	Piemontsko	k1gNnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1305	#num#	k4
<g/>
,	,	kIx,
respektive	respektive	k9
1543	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
význační	význačný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
rodu	rod	k1gInSc2
byli	být	k5eAaImAgMnP
mj.	mj.	kA
představiteli	představitel	k1gMnPc7
jeruzalémského	jeruzalémský	k2eAgNnSc2d1
a	a	k8xC
soluňského	soluňský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
sicilského	sicilský	k2eAgNnSc2d1
hrabství	hrabství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Genealogický	genealogický	k2eAgInSc1d1
strom	strom	k1gInSc1
monferratských	monferratský	k2eAgNnPc2d1
markrabat	markrabě	k1gNnPc2
z	z	k7c2
"	"	kIx"
<g/>
Compendi	Compend	k1gMnPc1
Historici	historik	k1gMnPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
1668	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aleramský	Aleramský	k2eAgInSc1d1
rod	rod	k1gInSc1
z	z	k7c2
Monferrata	Monferrat	k1gMnSc2
vymřel	vymřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1305	#num#	k4
a	a	k8xC
k	k	k7c3
moci	moc	k1gFnSc3
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
Paleologové	Paleolog	k1gMnPc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
z	z	k7c2
tohoto	tento	k3xDgNnSc2
schématu	schéma	k1gNnSc2
</s>
<s>
Původ	původ	k1gInSc1
ani	ani	k8xC
genealogie	genealogie	k1gFnSc1
rodu	rod	k1gInSc2
nejsou	být	k5eNaImIp3nP
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
malé	malý	k2eAgFnSc2d1
spolehlivosti	spolehlivost	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
úplné	úplný	k2eAgFnSc2d1
absence	absence	k1gFnSc2
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
anebo	anebo	k8xC
rozporů	rozpor	k1gInPc2
vyvolaných	vyvolaný	k2eAgFnPc2d1
falšovanými	falšovaný	k2eAgInPc7d1
dokumenty	dokument	k1gInPc7
vytvořenými	vytvořený	k2eAgInPc7d1
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
podepření	podepření	k1gNnSc3
teorie	teorie	k1gFnSc2
heraldických	heraldický	k2eAgInPc2d1
nároků	nárok	k1gInPc2
<g/>
,	,	kIx,
zhusta	zhusta	k6eAd1
používané	používaný	k2eAgFnPc4d1
historiky	historik	k1gMnPc4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulých	minulý	k2eAgNnPc6d1
staletích	staletí	k1gNnPc6
se	se	k3xPyFc4
mnoho	mnoho	k6eAd1
historiků	historik	k1gMnPc2
snažilo	snažit	k5eAaImAgNnS
vystopovat	vystopovat	k5eAaPmF
předky	předek	k1gInPc4
Alerama	Aleram	k1gMnSc2
<g/>
,	,	kIx,
zakladatele	zakladatel	k1gMnSc2
dynastie	dynastie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
podle	podle	k7c2
smyšlených	smyšlený	k2eAgInPc2d1
středověkých	středověký	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
měli	mít	k5eAaImAgMnP
pocházet	pocházet	k5eAaImF
od	od	k7c2
Teodorika	Teodorik	k1gMnSc2
Fríského	fríský	k2eAgMnSc2d1
nebo	nebo	k8xC
od	od	k7c2
pánů	pan	k1gMnPc2
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
v	v	k7c6
16	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
marně	marně	k6eAd1
snažili	snažit	k5eAaImAgMnP
najít	najít	k5eAaPmF
listiny	listina	k1gFnPc4
potvrzující	potvrzující	k2eAgFnSc4d1
legendu	legenda	k1gFnSc4
o	o	k7c6
lásce	láska	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
spojila	spojit	k5eAaPmAgFnS
Alerama	Alerama	k1gFnSc1
s	s	k7c7
Adelasií	Adelasie	k1gFnSc7
<g/>
,	,	kIx,
mytickou	mytický	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
německého	německý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Oty	Ota	k1gMnSc2
I.	I.	kA
Velkého	velký	k2eAgInSc2d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dokument	dokument	k1gInSc1
z	z	k7c2
grazzanského	grazzanský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
961	#num#	k4
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Aleramo	Alerama	k1gFnSc5
byl	být	k5eAaImAgInS
syn	syn	k1gMnSc1
jakéhosi	jakýsi	k3yIgMnSc2
hraběte	hrabě	k1gMnSc2
Viléma	Vilém	k1gMnSc2
(	(	kIx(
<g/>
Guglielma	Guglielm	k1gMnSc2
<g/>
)	)	kIx)
podle	podle	k7c2
salického	salický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
být	být	k5eAaImF
přijatelným	přijatelný	k2eAgNnSc7d1
identifikovat	identifikovat	k5eAaBmF
Viléma	Vilém	k1gMnSc4
jako	jako	k8xC,k8xS
osobu	osoba	k1gFnSc4
zřejmě	zřejmě	k6eAd1
z	z	k7c2
franského	franský	k2eAgInSc2d1
kmene	kmen	k1gInSc2
<g/>
,	,	kIx,
vstoupivšího	vstoupivší	k2eAgInSc2d1
na	na	k7c6
území	území	k1gNnSc6
do	do	k7c2
Itálie	Itálie	k1gFnSc2
se	s	k7c7
třemi	tři	k4xCgNnPc7
sty	sto	k4xCgNnPc7
bojovníky	bojovník	k1gMnPc4
následně	následně	k6eAd1
po	po	k7c6
Kvido	Kvido	k1gMnSc1
ze	z	k7c2
Spoleta	Spolet	k2eAgFnSc1d1
roku	rok	k1gInSc2
888	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
924	#num#	k4
činného	činný	k2eAgMnSc4d1
u	u	k7c2
dvora	dvůr	k1gInSc2
italského	italský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Rudolfa	Rudolf	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burgundského	burgundské	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aleramův	Aleramův	k2eAgInSc4d1
původ	původ	k1gInSc4
je	být	k5eAaImIp3nS
proto	proto	k8xC
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2
na	na	k7c6
území	území	k1gNnSc6
starého	starý	k2eAgNnSc2d1
království	království	k1gNnSc2
Lotara	Lotar	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čemuž	což	k3yQnSc3,k3yRnSc3
nasvědčuje	nasvědčovat	k5eAaImIp3nS
několik	několik	k4yIc4
hypotéz	hypotéza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rodový	rodový	k2eAgInSc4d1
původ	původ	k1gInSc4
Adelasie	Adelasie	k1gFnSc2
<g/>
,	,	kIx,
Aleramovy	Aleramův	k2eAgFnSc2d1
první	první	k4xOgFnSc2
manželky	manželka	k1gFnSc2
a	a	k8xC
pramáti	pramáti	k1gFnSc2
Aleramovců	Aleramovec	k1gMnPc2
<g/>
,	,	kIx,
zůstává	zůstávat	k5eAaImIp3nS
neznámý	známý	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jisté	jistý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Aleramo	Alerama	k1gFnSc5
později	pozdě	k6eAd2
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Gerbergou	Gerberga	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
italského	italský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Berengara	Berengar	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
toto	tento	k3xDgNnSc1
manželství	manželství	k1gNnSc1
mu	on	k3xPp3gMnSc3
přineslo	přinést	k5eAaPmAgNnS
nabytí	nabytí	k1gNnSc4
titulu	titul	k1gInSc2
markraběte	markrabě	k1gMnSc2
v	v	k7c6
letech	léto	k1gNnPc6
958	#num#	k4
až	až	k9
961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císařský	císařský	k2eAgInSc1d1
původ	původ	k1gInSc1
Adelasie	Adelasie	k1gFnSc2
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
být	být	k5eAaImF
spíše	spíše	k9
legendou	legenda	k1gFnSc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
vytvořenou	vytvořený	k2eAgFnSc4d1
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
slávy	sláva	k1gFnSc2
jeho	jeho	k3xOp3gInPc2
předků	předek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Alerama	Alerama	k1gFnSc1
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
za	za	k7c2
skutečného	skutečný	k2eAgMnSc2d1
zakladatele	zakladatel	k1gMnSc2
aleramovských	aleramovský	k2eAgFnPc2d1
dynastií	dynastie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těšil	těšit	k5eAaImAgMnS
se	se	k3xPyFc4
velké	velký	k2eAgFnSc3d1
úctě	úcta	k1gFnSc3
jak	jak	k8xS,k8xC
u	u	k7c2
italského	italský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Huga	Hugo	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
Lotara	Lotar	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
či	či	k8xC
Berengara	Berengara	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
tak	tak	k6eAd1
u	u	k7c2
dvora	dvůr	k1gInSc2
císaře	císař	k1gMnSc2
Oty	Ota	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
svědčí	svědčit	k5eAaImIp3nP
rozličné	rozličný	k2eAgInPc1d1
územní	územní	k2eAgInPc1d1
dary	dar	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
připojeny	připojit	k5eAaPmNgFnP
k	k	k7c3
panstvím	panství	k1gNnPc3
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
Aleramo	Alerama	k1gFnSc5
již	již	k6eAd1
vlastnil	vlastnit	k5eAaImAgInS
ve	v	k7c6
Vercellese	Vercellesa	k1gFnSc6
a	a	k8xC
v	v	k7c6
Lombardsku	Lombardsku	k?
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
z	z	k7c2
markraběcího	markraběcí	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
mu	on	k3xPp3gMnSc3
udělil	udělit	k5eAaPmAgInS
Berengar	Berengar	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
967	#num#	k4
mu	on	k3xPp3gNnSc3
Ota	Ota	k1gMnSc1
I.	I.	kA
Daroval	darovat	k5eAaPmAgMnS
rozlehlé	rozlehlý	k2eAgNnSc4d1
území	území	k1gNnSc4
mezi	mezi	k7c7
l	l	kA
<g/>
'	'	kIx"
<g/>
Orba	orba	k1gFnSc1
a	a	k8xC
il	il	k?
Tanaro	Tanara	k1gFnSc5
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
na	na	k7c6
jihu	jih	k1gInSc6
dosahovalo	dosahovat	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
blízkosti	blízkost	k1gFnSc2
Savony	Savona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
<g/>
,	,	kIx,
lesnaté	lesnatý	k2eAgNnSc1d1
a	a	k8xC
zanedbané	zanedbaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
předcházejícího	předcházející	k2eAgNnSc2d1
století	století	k1gNnSc2
zpustošeno	zpustošit	k5eAaPmNgNnS
loupeživými	loupeživý	k2eAgInPc7d1
nájezdy	nájezd	k1gInPc7
<g/>
,	,	kIx,
ze	z	k7c2
strany	strana	k1gFnSc2
tzv.	tzv.	kA
"	"	kIx"
<g/>
saracénů	saracén	k1gInPc2
<g/>
"	"	kIx"
z	z	k7c2
Fraxineta	Fraxineto	k1gNnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
jimi	on	k3xPp3gInPc7
alespoň	alespoň	k9
schvalovanými	schvalovaný	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
se	se	k3xPyFc4
nazývalo	nazývat	k5eAaImAgNnS
"	"	kIx"
<g/>
Vasto	Vasto	k1gNnSc1
<g/>
"	"	kIx"
nebo	nebo	k8xC
"	"	kIx"
<g/>
Guasto	Guasta	k1gMnSc5
<g/>
"	"	kIx"
(	(	kIx(
<g/>
tzn.	tzn.	kA
„	„	k?
<g/>
rozlehlé	rozlehlý	k2eAgFnSc6d1
<g/>
,	,	kIx,
široké	široký	k2eAgFnSc6d1
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
mnoho	mnoho	k4c1
Aleramových	Aleramový	k2eAgMnPc2d1
následníků	následník	k1gMnPc2
se	se	k3xPyFc4
proto	proto	k8xC
nazývalo	nazývat	k5eAaImAgNnS
"	"	kIx"
<g/>
markrabaty	markrabě	k1gNnPc7
z	z	k7c2
Vasta	Vast	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Riccarda	Riccard	k1gMnSc2
Mussa	Muss	k1gMnSc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
místní	místní	k2eAgInSc1d1
název	název	k1gInSc1
udržel	udržet	k5eAaPmAgInS
po	po	k7c4
několik	několik	k4yIc4
staletí	staletí	k1gNnPc2
a	a	k8xC
zahrnoval	zahrnovat	k5eAaImAgInS
hornaté	hornatý	k2eAgNnSc4d1
území	území	k1gNnSc4
včetně	včetně	k7c2
panství	panství	k1gNnSc2
Dego	Dego	k6eAd1
<g/>
,	,	kIx,
Montenotte	Montenott	k1gMnSc5
<g/>
,	,	kIx,
Carcare	Carcar	k1gMnSc5
a	a	k8xC
Cairo	Caira	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dalších	další	k2eAgNnPc6d1
územích	území	k1gNnPc6
se	se	k3xPyFc4
však	však	k9
vžilo	vžít	k5eAaPmAgNnS
shodné	shodný	k2eAgNnSc1d1
toponymum	toponymum	k1gNnSc1
"	"	kIx"
<g/>
Langhe	Langhe	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
vulgariter	vulgariter	k1gMnSc1
enim	enim	k1gMnSc1
loca	loca	k1gMnSc1
deserta	deserta	k1gFnSc1
Langae	Langa	k1gFnSc2
dicuntur	dicuntura	k1gFnPc2
podle	podle	k7c2
Luniga	Lunig	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejednalo	jednat	k5eNaImAgNnS
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
homogenní	homogenní	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
spíše	spíše	k9
o	o	k7c4
několik	několik	k4yIc4
odlehlých	odlehlý	k2eAgInPc2d1
dvorů	dvůr	k1gInPc2
v	v	k7c6
lesnatých	lesnatý	k2eAgFnPc6d1
a	a	k8xC
divokých	divoký	k2eAgNnPc6d1
údolích	údolí	k1gNnPc6
jižního	jižní	k2eAgNnSc2d1
Piemontska	Piemontsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Příbuzná	příbuzný	k2eAgNnPc1d1
hesla	heslo	k1gNnPc1
</s>
<s>
Carrettové	Carrettové	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Nejznámějšími	známý	k2eAgMnPc7d3
falsifikátory	falsifikátor	k1gMnPc7
významnými	významný	k2eAgMnPc7d1
pro	pro	k7c4
dějiny	dějiny	k1gFnPc4
Aleramiů	Alerami	k1gMnPc2
z	z	k7c2
Vasta	Vasto	k1gNnSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
G.	G.	kA
F.	F.	kA
Meyranesio	Meyranesio	k1gMnSc1
a	a	k8xC
G.	G.	kA
Sclavo	Sclava	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
markrabata	markrabě	k1gNnPc4
z	z	k7c2
Incisy	Incis	k1gInPc1
je	být	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
případ	případ	k1gInSc1
G.	G.	kA
Molinariho	Molinari	k1gMnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hlavním	hlavní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
legendárních	legendární	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
o	o	k7c6
Aleramovi	Aleram	k1gMnSc3
jsou	být	k5eAaImIp3nP
kroniky	kronika	k1gFnPc1
sepsané	sepsaný	k2eAgFnSc2d1
mnichy	mnich	k1gMnPc7
Jakubem	Jakub	k1gMnSc7
d	d	k?
<g/>
'	'	kIx"
<g/>
Acqui	Acqu	k1gFnSc2
a	a	k8xC
Galvanem	Galvan	k1gMnSc7
Fiammou	Fiamma	k1gMnSc7
<g/>
,	,	kIx,
che	che	k0
attinsero	attinsero	k1gNnSc4
na	na	k7c4
literární	literární	k2eAgInPc4d1
prameny	pramen	k1gInPc4
dnes	dnes	k6eAd1
ztracené	ztracený	k2eAgNnSc4d1
a	a	k8xC
na	na	k7c4
lidové	lidový	k2eAgFnPc4d1
tradice	tradice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
to	ten	k3xDgNnSc1
od	od	k7c2
bratra	bratr	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
<g/>
,	,	kIx,
ovlivnilo	ovlivnit	k5eAaPmAgNnS
další	další	k2eAgMnPc4d1
autory	autor	k1gMnPc4
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
až	až	k9
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Aleramovci	Aleramovec	k1gInPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Legenda	legenda	k1gFnSc1
o	o	k7c6
Aleramovi	Aleram	k1gMnSc6
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Markýzové	markýz	k1gMnPc1
z	z	k7c2
Monferrata	Monferrat	k1gMnSc2
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Accademia	Accademia	k1gFnSc1
Aleramica	Aleramic	k2eAgFnSc1d1
di	di	k?
Alba	alba	k1gFnSc1
<g/>
,	,	kIx,
místní	místní	k2eAgFnSc1d1
propagační	propagační	k2eAgFnSc1d1
kulturní	kulturní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Gruppo	Gruppa	k1gFnSc5
Storico	Storica	k1gFnSc5
La	la	k1gNnSc4
Medioevale	Medioevala	k1gFnSc3
di	di	k?
Savona	Savona	k1gFnSc1
<g/>
,	,	kIx,
historické	historický	k2eAgNnSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgNnSc4d1
uskupení	uskupení	k1gNnSc4
asociace	asociace	k1gFnSc2
"	"	kIx"
<g/>
Aleramovci	Aleramovec	k1gMnPc1
<g/>
"	"	kIx"
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Aleramici	Aleramik	k1gMnPc1
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
</s>
