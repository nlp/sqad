<s>
Aleramové	Aleramový	k2eAgFnPc1d1
či	či	k8xC
Aleramovci	Aleramovec	k1gInSc6
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
Aleramici	Aleramik	k1gMnPc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
spíše	spíše	k9
jako	jako	k8xS,k8xC
Aleramidi	Aleramid	k1gMnPc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
někdejší	někdejší	k2eAgInSc4d1
významný	významný	k2eAgInSc4d1
italský	italský	k2eAgInSc4d1
panský	panský	k2eAgInSc4d1
rod	rod	k1gInSc4
franského	franský	k2eAgInSc2d1
či	či	k8xC
franko-salického	franko-salický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>