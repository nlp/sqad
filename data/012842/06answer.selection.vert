<s>
Kyselina	kyselina	k1gFnSc1	kyselina
fosforečná	fosforečný	k2eAgFnSc1d1	fosforečná
a	a	k8xC	a
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
odrezovacích	odrezovací	k2eAgInPc2d1	odrezovací
roztoků	roztok	k1gInPc2	roztok
pro	pro	k7c4	pro
odstraňování	odstraňování	k1gNnSc4	odstraňování
korozních	korozní	k2eAgInPc2d1	korozní
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
železných	železný	k2eAgFnPc2d1	železná
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
železitým	železitý	k2eAgInSc7d1	železitý
<g/>
.	.	kIx.	.
</s>
