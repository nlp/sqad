<s>
Město	město	k1gNnSc1	město
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
osídleno	osídlit	k5eAaPmNgNnS	osídlit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
klanem	klan	k1gInSc7	klan
Al-Sabah	Al-Sabaha	k1gFnPc2	Al-Sabaha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vládnoucí	vládnoucí	k2eAgInSc1d1	vládnoucí
rod	rod	k1gInSc1	rod
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
,	,	kIx,	,
větev	větev	k1gFnSc1	větev
kmene	kmen	k1gInSc2	kmen
Al-Utū	Al-Utū	k1gMnSc2	Al-Utū
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
rodem	rod	k1gInSc7	rod
Al-Chálífa	Al-Chálíf	k1gMnSc2	Al-Chálíf
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
v	v	k7c6	v
Bahrajnu	Bahrajn	k1gInSc6	Bahrajn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
