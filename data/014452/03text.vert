<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
fotbalovém	fotbalový	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
historickém	historický	k2eAgInSc6d1
všestranném	všestranný	k2eAgInSc6d1
atletickém	atletický	k2eAgInSc6d1
klubu	klub	k1gInSc6
AC	AC	kA
Sparta	Sparta	k1gFnSc1
a	a	k8xC
dnešní	dnešní	k2eAgFnSc6d1
asociaci	asociace	k1gFnSc6
sportovních	sportovní	k2eAgInPc2d1
klubů	klub	k1gInPc2
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
a	a	k8xC
jejích	její	k3xOp3gInPc6
dalších	další	k2eAgInPc6d1
členských	členský	k2eAgInPc6d1
klubech	klub	k1gInPc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Asociace	asociace	k1gFnSc2
sportovních	sportovní	k2eAgInPc2d1
klubů	klub	k1gInPc2
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
Název	název	k1gInSc1
</s>
<s>
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc4
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
Železná	železný	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Letenští	letenský	k2eAgMnPc1d1
<g/>
“	“	k?
Země	zem	k1gFnPc4
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Bubeneč	Bubeneč	k1gInSc1
<g/>
)	)	kIx)
Založen	založen	k2eAgInSc1d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1893	#num#	k4
Asociace	asociace	k1gFnSc2
</s>
<s>
FAČR	FAČR	kA
Barvy	barva	k1gFnPc1
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
žlutá	žlutý	k2eAgFnSc1d1
a	a	k8xC
rudá	rudý	k2eAgFnSc1d1
(	(	kIx(
)	)	kIx)
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_spraga	_spraga	k1gFnSc1
<g/>
1920	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Chyba	chyba	k1gFnSc1
v	v	k7c6
šabloně	šablona	k1gFnSc6
{{	{{	k?
<g/>
Fotbalový	fotbalový	k2eAgInSc4d1
dres	dres	k1gInSc4
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
:	:	kIx,
Hodnota	hodnota	k1gFnSc1
parametru	parametr	k1gInSc2
"	"	kIx"
<g/>
vzor_trenýrek	vzor_trenýrka	k1gFnPc2
<g/>
"	"	kIx"
nerozpoznána	rozpoznán	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_spraga	_spraga	k1gFnSc1
<g/>
1718	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_spraga	_spraga	k1gFnSc1
<g/>
1718	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Generali	Generat	k5eAaImAgMnP,k5eAaPmAgMnP
Česká	český	k2eAgFnSc1d1
pojišťovna	pojišťovna	k1gFnSc1
Arena	Arena	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
18	#num#	k4
887	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vedení	vedení	k1gNnSc1
Vlastník	vlastník	k1gMnSc1
</s>
<s>
J	J	kA
<g/>
&	&	k?
<g/>
T	T	kA
Credit	Credit	k1gFnPc2
Investments	Investmentsa	k1gFnPc2
(	(	kIx(
<g/>
60	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
Daniel	Daniel	k1gMnSc1
Křetínský	Křetínský	k1gMnSc1
(	(	kIx(
<g/>
40	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Křetínský	Křetínský	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Vrba	Vrba	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
22	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
,	,	kIx,
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
38	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Protektorátu	protektorát	k1gInSc2
Č.	Č.	kA
a	a	k8xC
M.	M.	kA
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
)	)	kIx)
12	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
98	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
Československý	československý	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
20	#num#	k4
<g/>
×	×	k?
Český	český	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
2	#num#	k4
<g/>
×	×	k?
Český	český	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
elitní	elitní	k2eAgFnSc1d1
éra	éra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
[[	[[	k?
<g/>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
liga	liga	k1gFnSc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
]]	]]	k?
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
profesionální	profesionální	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
sídlí	sídlet	k5eAaImIp3nS
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
Letné	Letná	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddíl	oddíl	k1gInSc1
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
asociace	asociace	k1gFnSc2
Association	Association	k1gInSc1
Club	club	k1gInSc1
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
je	být	k5eAaImIp3nS
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
českým	český	k2eAgInSc7d1
fotbalovým	fotbalový	k2eAgInSc7d1
týmem	tým	k1gInSc7
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
36	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
československé	československý	k2eAgFnSc2d1
a	a	k8xC
české	český	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
několikrát	několikrát	k6eAd1
účinkovala	účinkovat	k5eAaImAgFnS
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
a	a	k8xC
během	během	k7c2
více	hodně	k6eAd2
než	než	k8xS
120	#num#	k4
<g/>
leté	letý	k2eAgFnSc2d1
historie	historie	k1gFnSc2
klubu	klub	k1gInSc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
dresu	dres	k1gInSc6
působila	působit	k5eAaImAgFnS
řada	řada	k1gFnSc1
reprezentantů	reprezentant	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současným	současný	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
klubu	klub	k1gInSc2
je	být	k5eAaImIp3nS
J	J	kA
<g/>
&	&	k?
<g/>
T	T	kA
Credit	Credit	k1gMnPc2
Investments	Investmentsa	k1gFnPc2
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
jejímiž	jejíž	k3xOyRp3gMnPc7
akcionáři	akcionář	k1gMnPc7
jsou	být	k5eAaImIp3nP
Daniel	Daniel	k1gMnSc1
Křetínský	Křetínský	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
kyperská	kyperský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ACSP	ACSP	kA
Holding	holding	k1gInSc1
Limited	limited	k2eAgInSc2d1
(	(	kIx(
<g/>
90	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
vlastněna	vlastnit	k5eAaImNgFnS
neprůhlednou	průhledný	k2eNgFnSc7d1
sítí	síť	k1gFnSc7
společností	společnost	k1gFnPc2
z	z	k7c2
daňových	daňový	k2eAgInPc2d1
rájů	ráj	k1gInPc2
<g/>
;	;	kIx,
podle	podle	k7c2
vyjádření	vyjádření	k1gNnSc2
firmy	firma	k1gFnSc2
však	však	k9
probíhají	probíhat	k5eAaImIp3nP
transakce	transakce	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
má	mít	k5eAaImIp3nS
Daniel	Daniel	k1gMnSc1
Křetínský	Křetínský	k1gMnSc1
získat	získat	k5eAaPmF
až	až	k9
55	#num#	k4
<g/>
%	%	kIx~
vlastnický	vlastnický	k2eAgInSc4d1
podíl	podíl	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
Generali	Generali	k1gFnSc6
Areně	Aréna	k1gFnSc6
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
18	#num#	k4
944	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
žlutá	žlutý	k2eAgFnSc1d1
a	a	k8xC
rudá	rudý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Symboly	symbol	k1gInPc1
</s>
<s>
Sparťanská	sparťanský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Sparty	Sparta	k1gFnSc2
</s>
<s>
Barvy	barva	k1gFnPc1
klubu	klub	k1gInSc2
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
žlutá	žlutý	k2eAgFnSc1d1
a	a	k8xC
červená	červený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivář	archivář	k1gMnSc1
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
J.	J.	kA
Forbelský	Forbelský	k2eAgMnSc1d1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
:	:	kIx,
Podle	podle	k7c2
stanov	stanova	k1gFnPc2
klubu	klub	k1gInSc2
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
z	z	k7c2
r.	r.	kA
1894	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
při	při	k7c6
požáru	požár	k1gInSc6
tribuny	tribuna	k1gFnSc2
v	v	k7c6
r.	r.	kA
1934	#num#	k4
zachovaly	zachovat	k5eAaPmAgFnP
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
v	v	k7c6
§	§	k?
20	#num#	k4
stanoven	stanoven	k2eAgInSc1d1
„	„	k?
<g/>
praporec	praporec	k1gInSc1
klubový	klubový	k2eAgInSc1d1
modrý	modrý	k2eAgInSc1d1
<g/>
,	,	kIx,
červeně	červeně	k6eAd1
ozubený	ozubený	k2eAgInSc1d1
<g/>
,	,	kIx,
nese	nést	k5eAaImIp3nS
zlatý	zlatý	k2eAgInSc1d1
kruh	kruh	k1gInSc1
palmový	palmový	k2eAgInSc1d1
a	a	k8xC
jméno	jméno	k1gNnSc4
A.C.	A.C.	k1gFnSc2
Sparta	Sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
V	v	k7c6
poznámce	poznámka	k1gFnSc6
k	k	k7c3
opravě	oprava	k1gFnSc3
stanov	stanova	k1gFnPc2
je	být	k5eAaImIp3nS
u	u	k7c2
palmového	palmový	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
a	a	k8xC
jména	jméno	k1gNnSc2
klubu	klub	k1gInSc2
místo	místo	k7c2
zlaté	zlatý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
uvedena	uvést	k5eAaPmNgFnS
žlutá	žlutat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
tradice	tradice	k1gFnSc2
(	(	kIx(
<g/>
Rudolf	Rudolf	k1gMnSc1
Richter	Richter	k1gMnSc1
a	a	k8xC
Vladimír	Vladimír	k1gMnSc1
Víšek	Víšek	k1gMnSc1
<g/>
)	)	kIx)
autoři	autor	k1gMnPc1
barvu	barva	k1gFnSc4
nově	nova	k1gFnSc3
založeného	založený	k2eAgInSc2d1
klubu	klub	k1gInSc2
(	(	kIx(
<g/>
modrou	modrý	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
doplnili	doplnit	k5eAaPmAgMnP
barvami	barva	k1gFnPc7
královského	královský	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
modrá	modrý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
barva	barva	k1gFnSc1
rychlosti	rychlost	k1gFnSc2
<g/>
,	,	kIx,
lehké	lehký	k2eAgFnSc2d1
atletiky	atletika	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
výklad	výklad	k1gInSc1
významu	význam	k1gInSc2
modré	modrý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
je	být	k5eAaImIp3nS
kombinace	kombinace	k1gFnSc1
žluté	žlutý	k2eAgFnSc2d1
a	a	k8xC
červené	červený	k2eAgFnSc2d1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
barvy	barva	k1gFnPc1
Prahy	Praha	k1gFnSc2
jako	jako	k8xS,k8xC
královského	královský	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dotvoření	dotvoření	k1gNnSc3
trikolory	trikolora	k1gFnSc2
<g/>
,	,	kIx,
přidali	přidat	k5eAaPmAgMnP
zakladatelé	zakladatel	k1gMnPc1
Sparty	Sparta	k1gFnSc2
nad	nad	k7c4
ně	on	k3xPp3gMnPc4
barvu	barva	k1gFnSc4
modrou	modrý	k2eAgFnSc4d1
jako	jako	k8xS,k8xC
symbol	symbol	k1gInSc4
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
Sparta	Sparta	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
starověkého	starověký	k2eAgNnSc2d1
města	město	k1gNnSc2
Sparta	Sparta	k1gFnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
vyznačovali	vyznačovat	k5eAaImAgMnP
bojovností	bojovnost	k1gFnSc7
a	a	k8xC
vůlí	vůle	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
klubu	klub	k1gInSc2
je	být	k5eAaImIp3nS
písmeno	písmeno	k1gNnSc4
S	s	k7c7
(	(	kIx(
<g/>
počáteční	počáteční	k2eAgNnSc4d1
písmeno	písmeno	k1gNnSc4
slova	slovo	k1gNnSc2
Sparta	Sparta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dresy	dres	k1gInPc1
</s>
<s>
Rudý	rudý	k2eAgInSc1d1
sparťanský	sparťanský	k2eAgInSc1d1
dres	dres	k1gInSc1
<g/>
,	,	kIx,
verze	verze	k1gFnSc1
pro	pro	k7c4
sezonu	sezona	k1gFnSc4
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Zpočátku	zpočátku	k6eAd1
sparťanští	sparťanský	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
nosili	nosit	k5eAaImAgMnP
černé	černý	k2eAgFnPc4d1
košile	košile	k1gFnPc4
s	s	k7c7
vyšitým	vyšitý	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
„	„	k?
<g/>
S	s	k7c7
<g/>
“	“	k?
na	na	k7c6
hrudi	hruď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
2	#num#	k4
roky	rok	k1gInPc4
používali	používat	k5eAaImAgMnP
kombinaci	kombinace	k1gFnSc4
v	v	k7c6
černé	černý	k2eAgFnSc6d1
a	a	k8xC
bílé	bílý	k2eAgFnSc6d1
pruhované	pruhovaný	k2eAgFnSc6d1
košili	košile	k1gFnSc6
(	(	kIx(
<g/>
později	pozdě	k6eAd2
použila	použít	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
tento	tento	k3xDgInSc4
vzor	vzor	k1gInSc4
jako	jako	k8xS,k8xC
rezervní	rezervní	k2eAgFnSc4d1
v	v	k7c6
letech	let	k1gInPc6
1996	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
byl	být	k5eAaImAgMnS
představitel	představitel	k1gMnSc1
klubu	klub	k1gInSc2
JUDr.	JUDr.	kA
Petřík	Petřík	k1gMnSc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
viděl	vidět	k5eAaImAgMnS
hru	hra	k1gFnSc4
Arsenalu	Arsenal	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
rudé	rudý	k2eAgInPc4d1
dresy	dres	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petřík	Petřík	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
vzít	vzít	k5eAaPmF
jednu	jeden	k4xCgFnSc4
sadu	sada	k1gFnSc4
dresů	dres	k1gInPc2
s	s	k7c7
sebou	se	k3xPyFc7
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
patří	patřit	k5eAaImIp3nS
rudá	rudý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
na	na	k7c4
dres	dres	k1gInSc4
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
červená	červená	k1gFnSc1
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kombinaci	kombinace	k1gFnSc6
dotváří	dotvářet	k5eAaImIp3nP
bílé	bílý	k2eAgFnPc1d1
trenýrky	trenýrky	k1gFnPc1
a	a	k8xC
černé	černý	k2eAgFnPc1d1
štulpny	štulpna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
Sparta	Sparta	k1gFnSc1
používá	používat	k5eAaImIp3nS
kombinaci	kombinace	k1gFnSc4
barev	barva	k1gFnPc2
rudé	rudý	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
domácí	domácí	k2eAgFnSc2d1
a	a	k8xC
žluté	žlutý	k2eAgFnSc2d1
pro	pro	k7c4
hostující	hostující	k2eAgInSc4d1
dres	dres	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Sponzor	sponzor	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
??	??	k?
<g/>
–	–	k?
<g/>
1990	#num#	k4
</s>
<s>
Adidas	Adidas	k1gMnSc1
</s>
<s>
–	–	k?
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
</s>
<s>
Opel	opel	k1gInSc1
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
</s>
<s>
Lotto	Lotto	k1gNnSc1
</s>
<s>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
</s>
<s>
Nike	Nike	k1gFnSc1
</s>
<s>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
</s>
<s>
Eurotel	Eurotel	kA
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
2015	#num#	k4
</s>
<s>
Blesk	blesk	k1gInSc1
energie	energie	k1gFnSc2
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
</s>
<s>
Synot	Synot	k1gInSc1
tip	tip	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
</s>
<s>
Sazka	Sazka	k1gFnSc1
Bet	Bet	k1gFnSc2
</s>
<s>
2019	#num#	k4
<g/>
–	–	k?
</s>
<s>
Tipsport	Tipsport	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
stadionů	stadion	k1gInPc2
Sparty	Sparta	k1gFnSc2
Praha	Praha	k1gFnSc1
na	na	k7c6
místě	místo	k1gNnSc6
stávajícího	stávající	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
</s>
<s>
1921	#num#	k4
-	-	kIx~
první	první	k4xOgInSc1
stadion	stadion	k1gInSc1
-	-	kIx~
dřevěná	dřevěný	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
</s>
<s>
1936	#num#	k4
-	-	kIx~
železobetonová	železobetonový	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
tribuna	tribuna	k1gFnSc1
(	(	kIx(
<g/>
základ	základ	k1gInSc1
dnešní	dnešní	k2eAgFnSc2d1
hlavní	hlavní	k2eAgFnSc2d1
tribuny	tribuna	k1gFnSc2
a	a	k8xC
zázemí	zázemí	k1gNnSc2
stadionu	stadion	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
1969	#num#	k4
-	-	kIx~
nová	nový	k2eAgFnSc1d1
aréna	aréna	k1gFnSc1
pro	pro	k7c4
35.880	35.880	k4
diváků	divák	k1gMnPc2
</s>
<s>
1994	#num#	k4
-	-	kIx~
celková	celkový	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
stadionu	stadion	k1gInSc2
do	do	k7c2
současné	současný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
</s>
<s>
2003	#num#	k4
-	-	kIx~
nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
stadionu	stadion	k1gInSc2
-	-	kIx~
Toyota	toyota	k1gFnSc1
Arena	Arena	k1gFnSc1
</s>
<s>
2007	#num#	k4
-	-	kIx~
nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
stadionu	stadion	k1gInSc2
-	-	kIx~
AXA	AXA	kA
Arena	Arena	k1gFnSc1
</s>
<s>
2009	#num#	k4
-	-	kIx~
nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
stadionu	stadion	k1gInSc2
-	-	kIx~
Generali	Generali	k1gFnSc1
Česká	český	k2eAgFnSc1d1
pojišťovna	pojišťovna	k1gFnSc1
Arena	Aren	k1gInSc2
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
1893	#num#	k4
–	–	k?
AC	AC	kA
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
(	(	kIx(
<g/>
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1894	#num#	k4
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
(	(	kIx(
<g/>
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
Sparta	Sparta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1948	#num#	k4
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Bubeneč	Bubeneč	k1gFnSc1
(	(	kIx(
<g/>
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
Sparta	Sparta	k1gFnSc1
Bubeneč	Bubeneč	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1949	#num#	k4
–	–	k?
Sokol	Sokol	k1gMnSc1
Bratrství	bratrství	k1gNnSc2
Sparta	Sparta	k1gFnSc1
</s>
<s>
1951	#num#	k4
–	–	k?
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Sparta	Sparta	k1gFnSc1
Českomoravská	českomoravský	k2eAgFnSc1d1
Kolben	Kolben	k2eAgMnSc1d1
Daněk	Daněk	k1gMnSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1953	#num#	k4
–	–	k?
TJ	tj	kA
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
1965	#num#	k4
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Sparta	Sparta	k1gFnSc1
Českomoravská	českomoravský	k2eAgFnSc1d1
Kolben	Kolben	k2eAgMnSc1d1
Daněk	Daněk	k1gMnSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
(	(	kIx(
<g/>
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc1
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Historie	historie	k1gFnSc1
fotbalové	fotbalový	k2eAgFnSc2d1
Sparty	Sparta	k1gFnSc2
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
několik	několik	k4yIc1
nadšenců	nadšenec	k1gMnPc2
založilo	založit	k5eAaPmAgNnS
na	na	k7c6
pražských	pražský	k2eAgFnPc6d1
Maninách	manina	k1gFnPc6
klub	klub	k1gInSc4
se	s	k7c7
jménem	jméno	k1gNnSc7
AC	AC	kA
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěstovala	pěstovat	k5eAaImAgNnP
se	se	k3xPyFc4
zde	zde	k6eAd1
mimo	mimo	k7c4
jiné	jiný	k2eAgMnPc4d1
i	i	k8xC
cyklistika	cyklistika	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
byla	být	k5eAaImAgFnS
Vltava	Vltava	k1gFnSc1
zamrzlá	zamrzlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
bruslení	bruslení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgInPc6
letech	léto	k1gNnPc6
činnosti	činnost	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
klub	klub	k1gInSc4
rozhádal	rozhádat	k5eAaPmAgMnS
a	a	k8xC
odešla	odejít	k5eAaPmAgFnS
z	z	k7c2
něj	on	k3xPp3gMnSc2
skupina	skupina	k1gFnSc1
odbojníků	odbojník	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Karlem	Karel	k1gMnSc7
<g/>
,	,	kIx,
Otou	Ota	k1gMnSc7
a	a	k8xC
Josefem	Josef	k1gMnSc7
Malečkovými	Malečkovi	k1gRnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
založili	založit	k5eAaPmAgMnP
Athletic	Athletice	k1gFnPc2
Club	club	k1gInSc4
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
Maxmilián	Maxmilián	k1gMnSc1
Švagrovský	švagrovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
původem	původ	k1gInSc7
z	z	k7c2
Roudnice	Roudnice	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinohrady	Vinohrady	k1gInPc4
byly	být	k5eAaImAgInP
tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
samostatné	samostatný	k2eAgNnSc1d1
bohaté	bohatý	k2eAgNnSc1d1
město	město	k1gNnSc1
za	za	k7c7
hranicemi	hranice	k1gFnPc7
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
nový	nový	k2eAgInSc1d1
klub	klub	k1gInSc1
očekával	očekávat	k5eAaImAgInS
od	od	k7c2
radnice	radnice	k1gFnSc2
štědrou	štědrý	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
ustanoven	ustanovit	k5eAaPmNgInS
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1893	#num#	k4
a	a	k8xC
tento	tento	k3xDgInSc1
den	den	k1gInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
pár	pár	k4xCyI
týdnů	týden	k1gInPc2
později	pozdě	k6eAd2
uspořádal	uspořádat	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
klub	klub	k1gInSc1
bruslařskou	bruslařský	k2eAgFnSc4d1
exhibici	exhibice	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
i	i	k9
Bohumil	Bohumil	k1gMnSc1
Rudl	rudnout	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
exhibice	exhibice	k1gFnSc1
líbila	líbit	k5eAaImAgFnS
a	a	k8xC
přivedl	přivést	k5eAaPmAgInS
i	i	k9
své	svůj	k3xOyFgMnPc4
dva	dva	k4xCgMnPc4
bratry	bratr	k1gMnPc4
Václava	Václav	k1gMnSc4
a	a	k8xC
Rudolfa	Rudolf	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
hráli	hrát	k5eAaImAgMnP
především	především	k6eAd1
fotbal	fotbal	k1gInSc4
a	a	k8xC
brzy	brzy	k6eAd1
přivedli	přivést	k5eAaPmAgMnP
celý	celý	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
kroužek	kroužek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
působil	působit	k5eAaImAgMnS
na	na	k7c6
Letenské	letenský	k2eAgFnSc6d1
pláni	pláň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
vinohradské	vinohradský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
byla	být	k5eAaImAgFnS
nulová	nulový	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
jim	on	k3xPp3gMnPc3
nenabídla	nabídnout	k5eNaPmAgFnS
ani	ani	k9
pozemek	pozemek	k1gInSc4
pro	pro	k7c4
nový	nový	k2eAgInSc4d1
stadion	stadion	k1gInSc4
<g/>
,	,	kIx,
klub	klub	k1gInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
změnit	změnit	k5eAaPmF
působiště	působiště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
schváleno	schválit	k5eAaPmNgNnS
na	na	k7c6
valné	valný	k2eAgFnSc6d1
hromadě	hromada	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1894	#num#	k4
v	v	k7c6
kavárně	kavárna	k1gFnSc6
Demínka	Demínko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jednání	jednání	k1gNnSc2
povstal	povstat	k5eAaPmAgMnS
Vladimír	Vladimír	k1gMnSc1
Horejc	Horejc	k1gInSc1
a	a	k8xC
pronesl	pronést	k5eAaPmAgInS
slavná	slavný	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Nechť	nechť	k9
se	se	k3xPyFc4
náš	náš	k3xOp1gInSc1
klub	klub	k1gInSc1
napříště	napříště	k6eAd1
zove	zovat	k5eAaPmIp3nS
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
V	v	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
antická	antický	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
v	v	k7c6
módě	móda	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
návrh	návrh	k1gInSc1
schválen	schválit	k5eAaPmNgInS
s	s	k7c7
bouřlivým	bouřlivý	k2eAgNnSc7d1
nadšením	nadšení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
ani	ani	k8xC
nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
nedokázal	dokázat	k5eNaPmAgInS
zajistit	zajistit	k5eAaPmF
nový	nový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparťani	sparťan	k1gMnPc1
tedy	tedy	k9
trénovali	trénovat	k5eAaImAgMnP
na	na	k7c6
vojenském	vojenský	k2eAgNnSc6d1
cvičišti	cvičiště	k1gNnSc6
na	na	k7c6
Invalidovně	invalidovna	k1gFnSc6
a	a	k8xC
jako	jako	k8xC,k8xS
většina	většina	k1gFnSc1
tehdejších	tehdejší	k2eAgMnPc2d1
klubů	klub	k1gInPc2
své	svůj	k3xOyFgInPc4
zápasy	zápas	k1gInPc4
hráli	hrát	k5eAaImAgMnP
nejčastěji	často	k6eAd3
na	na	k7c6
Císařské	císařský	k2eAgFnSc6d1
louce	louka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
tam	tam	k6eAd1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1896	#num#	k4
turnaj	turnaj	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Národní	národní	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
mužstev	mužstvo	k1gNnPc2
<g/>
,	,	kIx,
kopaný	kopaný	k2eAgInSc1d1
míč	míč	k1gInSc1
cvičících	cvičící	k2eAgFnPc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgInSc2
turnaje	turnaj	k1gInSc2
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
všechna	všechen	k3xTgNnPc4
slavná	slavný	k2eAgNnPc4d1
derby	derby	k1gNnPc4
Sparty	Sparta	k1gFnSc2
se	s	k7c7
Slavií	slavie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hřišti	hřiště	k1gNnSc6
vyhrála	vyhrát	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
první	první	k4xOgInSc1
zápas	zápas	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
slavný	slavný	k2eAgMnSc1d1
rozhodčí	rozhodčí	k1gMnSc1
a	a	k8xC
praotec	praotec	k1gMnSc1
českého	český	k2eAgInSc2d1
sportu	sport	k1gInSc2
Josef	Josef	k1gMnSc1
Rössler-Ořovský	Rössler-Ořovský	k1gMnSc1
po	po	k7c6
zápase	zápas	k1gInSc6
sparťanský	sparťanský	k2eAgInSc4d1
gól	gól	k1gInSc4
odvolal	odvolat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyla	být	k5eNaImAgFnS
to	ten	k3xDgNnSc1
svévole	svévole	k1gFnSc1
rozhodčího	rozhodčí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
odpovídalo	odpovídat	k5eAaImAgNnS
tehdejšímu	tehdejší	k2eAgInSc3d1
výkladu	výklad	k1gInSc3
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
museli	muset	k5eAaImAgMnP
dosaženou	dosažený	k2eAgFnSc4d1
branku	branka	k1gFnSc4
schválit	schválit	k5eAaPmF
kapitáni	kapitán	k1gMnPc1
obou	dva	k4xCgInPc2
mužstev	mužstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
ochlazení	ochlazení	k1gNnSc3
vztahů	vztah	k1gInPc2
se	s	k7c7
Slavií	slavie	k1gFnSc7
se	se	k3xPyFc4
další	další	k2eAgInSc1d1
zápas	zápas	k1gInSc1
konal	konat	k5eAaImAgInS
až	až	k9
za	za	k7c4
jedenáct	jedenáct	k4xCc4
let	léto	k1gNnPc2
a	a	k8xC
skončil	skončit	k5eAaPmAgMnS
výsledkem	výsledek	k1gInSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
střelcem	střelec	k1gMnSc7
Sparty	Sparta	k1gFnSc2
v	v	k7c6
derby	derby	k1gNnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
autor	autor	k1gMnSc1
obou	dva	k4xCgInPc2
gólů	gól	k1gInPc2
Karel	Karel	k1gMnSc1
Hradecký	Hradecký	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
století	století	k1gNnSc2
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
1908	#num#	k4
</s>
<s>
Sparta	Sparta	k1gFnSc1
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
velmi	velmi	k6eAd1
chudý	chudý	k2eAgInSc1d1
klub	klub	k1gInSc1
a	a	k8xC
barvy	barva	k1gFnPc1
dresů	dres	k1gInPc2
určovala	určovat	k5eAaImAgFnS
spíše	spíše	k9
náhoda	náhoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
sparťanské	sparťanský	k2eAgInPc4d1
dresy	dres	k1gInPc4
byly	být	k5eAaImAgFnP
černé	černá	k1gFnPc1
s	s	k7c7
bílým	bílý	k2eAgNnSc7d1
„	„	k?
<g/>
S	s	k7c7
<g/>
“	“	k?
na	na	k7c6
prsou	prsa	k1gNnPc6
svetru	svetr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubu	klub	k1gInSc2
hrozil	hrozit	k5eAaImAgInS
několikrát	několikrát	k6eAd1
rozpad	rozpad	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
chtěl	chtít	k5eAaImAgMnS
novou	nový	k2eAgFnSc4d1
výstroj	výstroj	k1gFnSc4
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
vzít	vzít	k5eAaPmF
zavděk	zavděk	k6eAd1
i	i	k8xC
silně	silně	k6eAd1
obnošenými	obnošený	k2eAgFnPc7d1
červenočernými	červenočerný	k2eAgFnPc7d1
košilemi	košile	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
původně	původně	k6eAd1
patřily	patřit	k5eAaImAgFnP
smíchovskému	smíchovský	k2eAgMnSc3d1
ČFK	ČFK	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
je	on	k3xPp3gMnPc4
nosil	nosit	k5eAaImAgInS
vinohradský	vinohradský	k2eAgInSc1d1
ČAFC	ČAFC	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
změnil	změnit	k5eAaPmAgMnS
barvy	barva	k1gFnSc2
a	a	k8xC
své	svůj	k3xOyFgInPc4
dresy	dres	k1gInPc4
přenechal	přenechat	k5eAaPmAgMnS
chudým	chudý	k2eAgMnSc7d1
sparťanům	sparťan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
získal	získat	k5eAaPmAgInS
klub	klub	k1gInSc1
ze	z	k7c2
zaniklého	zaniklý	k2eAgInSc2d1
Unionu	union	k1gInSc2
Praha	Praha	k1gFnSc1
dresy	dres	k1gInPc4
s	s	k7c7
červenými	červený	k2eAgInPc7d1
a	a	k8xC
bílými	bílý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
týmu	tým	k1gInSc6
působil	působit	k5eAaImAgMnS
i	i	k9
střelec	střelec	k1gMnSc1
Jan	Jan	k1gMnSc1
Košek	Košek	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
k	k	k7c3
nelibosti	nelibost	k1gFnSc3
sparťanů	sparťan	k1gMnPc2
přetáhla	přetáhnout	k5eAaPmAgFnS
Slavie	slavie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
1904	#num#	k4
však	však	k8xC
ve	v	k7c6
Slavii	slavie	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozkolu	rozkol	k1gInSc3
a	a	k8xC
do	do	k7c2
Sparty	Sparta	k1gFnSc2
přišel	přijít	k5eAaPmAgMnS
nejen	nejen	k6eAd1
Košek	Košek	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
slavné	slavný	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
Jindřich	Jindřich	k1gMnSc1
Baumruk	Baumruk	k1gMnSc1
a	a	k8xC
Rudolf	Rudolf	k1gMnSc1
Krummer	Krummer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
se	se	k3xPyFc4
rázem	rázem	k6eAd1
stala	stát	k5eAaPmAgFnS
nejlepším	dobrý	k2eAgInSc7d3
týmem	tým	k1gInSc7
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
potvrdila	potvrdit	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
o	o	k7c6
Vánocích	Vánoce	k1gFnPc6
porazila	porazit	k5eAaPmAgFnS
First	First	k1gInSc4
Viennu	Vienn	k1gInSc2
7	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
budapešťský	budapešťský	k2eAgInSc1d1
BTK	BTK	kA
porazila	porazit	k5eAaPmAgFnS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
senzací	senzace	k1gFnSc7
však	však	k9
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
když	když	k8xS
Sparta	Sparta	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
s	s	k7c7
vítězem	vítěz	k1gMnSc7
anglické	anglický	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
Newcastlem	Newcastl	k1gInSc7
United	United	k1gInSc1
jen	jen	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalí	bývalý	k2eAgMnPc1d1
slávisté	slávista	k1gMnPc1
se	se	k3xPyFc4
však	však	k9
vrátili	vrátit	k5eAaPmAgMnP
zpět	zpět	k6eAd1
do	do	k7c2
sešívaného	sešívaný	k2eAgInSc2d1
dresu	dres	k1gInSc2
a	a	k8xC
sláva	sláva	k1gFnSc1
Sparty	Sparta	k1gFnSc2
vzala	vzít	k5eAaPmAgFnS
prozatím	prozatím	k6eAd1
za	za	k7c4
své	svůj	k3xOyFgNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
stadion	stadion	k1gInSc4
získal	získat	k5eAaPmAgMnS
klub	klub	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
naproti	naproti	k7c3
holešovickému	holešovický	k2eAgInSc3d1
pivovaru	pivovar	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
za	za	k7c4
rok	rok	k1gInSc4
se	se	k3xPyFc4
stěhovali	stěhovat	k5eAaImAgMnP
na	na	k7c4
Letnou	Letná	k1gFnSc4
do	do	k7c2
prostoru	prostor	k1gInSc2
dnešního	dnešní	k2eAgNnSc2d1
Národního	národní	k2eAgNnSc2d1
technického	technický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
přivezl	přivézt	k5eAaPmAgMnS
z	z	k7c2
Londýna	Londýn	k1gInSc2
pan	pan	k1gMnSc1
Petřík	Petřík	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
sadu	sada	k1gFnSc4
rudých	rudý	k2eAgInPc2d1
svetrů	svetr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
takové	takový	k3xDgInPc4
dresy	dres	k1gInPc4
oblékal	oblékat	k5eAaImAgMnS
londýnský	londýnský	k2eAgMnSc1d1
Arsenal	Arsenal	k1gMnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Petřík	Petřík	k1gMnSc1
je	být	k5eAaImIp3nS
daroval	darovat	k5eAaPmAgMnS
sparťanům	sparťan	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
hráli	hrát	k5eAaImAgMnP
stejně	stejně	k6eAd1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
rudé	rudý	k2eAgInPc1d1
dresy	dres	k1gInPc1
staly	stát	k5eAaPmAgInP
pevným	pevný	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
století	století	k1gNnSc2
byla	být	k5eAaImAgFnS
dominujícím	dominující	k2eAgInSc7d1
českým	český	k2eAgInSc7d1
klubem	klub	k1gInSc7
Slavia	Slavia	k1gFnSc1
a	a	k8xC
i	i	k9
její	její	k3xOp3gInSc1
druhý	druhý	k4xOgInSc1
tým	tým	k1gInSc1
dokázal	dokázat	k5eAaPmAgInS
porážet	porážet	k5eAaImF
domácí	domácí	k2eAgMnPc4d1
soupeře	soupeř	k1gMnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
však	však	k9
krok	krok	k1gInSc4
za	za	k7c7
krokem	krok	k1gInSc7
také	také	k6eAd1
budovala	budovat	k5eAaImAgFnS
silný	silný	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
získala	získat	k5eAaPmAgFnS
několik	několik	k4yIc4
skvělých	skvělý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
včetně	včetně	k7c2
Václava	Václav	k1gMnSc2
Piláta	Pilát	k1gMnSc2
<g/>
,	,	kIx,
výborného	výborný	k2eAgMnSc4d1
technika	technik	k1gMnSc4
a	a	k8xC
mistra	mistr	k1gMnSc4
nahrávek	nahrávka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1911	#num#	k4
porazila	porazit	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
Slavii	slavie	k1gFnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
začala	začít	k5eAaPmAgFnS
tím	ten	k3xDgNnSc7
období	období	k1gNnPc1
dvojvládí	dvojvládí	k1gNnPc1
pražských	pražský	k2eAgMnPc2d1
„	„	k?
<g/>
S	s	k7c7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
1913	#num#	k4
získala	získat	k5eAaPmAgFnS
záložníka	záložník	k1gMnSc4
ČAFC	ČAFC	kA
Karla	Karel	k1gMnSc4
Peška-Káďu	Peška-Káďa	k1gMnSc4
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
českým	český	k2eAgMnSc7d1
suverénem	suverén	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
první	první	k4xOgNnSc1
řádné	řádný	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
Českého	český	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparťané	Sparťan	k1gMnPc1
vyhráli	vyhrát	k5eAaPmAgMnP
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
semifinále	semifinále	k1gNnSc6
porazili	porazit	k5eAaPmAgMnP
Moravskou	moravský	k2eAgFnSc4d1
Slavii	slavie	k1gFnSc4
Brno	Brno	k1gNnSc1
a	a	k8xC
po	po	k7c6
finálové	finálový	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
Kolína	Kolín	k1gInSc2
získali	získat	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
Světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
narukovali	narukovat	k5eAaPmAgMnP
do	do	k7c2
bojů	boj	k1gInPc2
i	i	k8xC
fotbalisté	fotbalista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
přinesly	přinést	k5eAaPmAgInP
v	v	k7c4
první	první	k4xOgFnPc4
zářijové	zářijový	k2eAgFnPc4d1
dny	dna	k1gFnPc4
roku	rok	k1gInSc2
1914	#num#	k4
zprávu	zpráva	k1gFnSc4
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
Václava	Václav	k1gMnSc2
Piláta	Pilát	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
však	však	k9
nepravdivá	pravdivý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilát	pilát	k1gInSc1
byl	být	k5eAaImAgInS
sice	sice	k8xC
zasažen	zasáhnout	k5eAaPmNgInS
střepinou	střepina	k1gFnSc7
a	a	k8xC
zůstal	zůstat	k5eAaPmAgInS
nehybně	hybně	k6eNd1
ležet	ležet	k5eAaImF
na	na	k7c6
bitevním	bitevní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
procházející	procházející	k2eAgMnSc1d1
kaprál	kaprál	k1gMnSc1
Štěpánovský	Štěpánovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
civilu	civil	k1gMnSc3
známý	známý	k2eAgMnSc1d1
rozhodčí	rozhodčí	k1gMnSc1
<g/>
,	,	kIx,
však	však	k8xC
Piláta	Pilát	k1gMnSc4
poznal	poznat	k5eAaPmAgMnS
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
odnést	odnést	k5eAaPmF
na	na	k7c4
ošetřovnu	ošetřovna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilát	pilát	k1gInSc1
dlouho	dlouho	k6eAd1
zápasil	zápasit	k5eAaImAgInS
o	o	k7c4
život	život	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
zázračně	zázračně	k6eAd1
uzdravil	uzdravit	k5eAaPmAgMnS
a	a	k8xC
brzy	brzy	k6eAd1
začal	začít	k5eAaPmAgInS
dokonce	dokonce	k9
znovu	znovu	k6eAd1
hrát	hrát	k5eAaImF
fotbal	fotbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Během	během	k7c2
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
války	válka	k1gFnSc2
na	na	k7c6
tom	ten	k3xDgInSc6
byl	být	k5eAaImAgInS
český	český	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
špatně	špatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
všech	všecek	k3xTgInPc2
týmů	tým	k1gInPc2
zbyly	zbýt	k5eAaPmAgFnP
jen	jen	k9
trosky	troska	k1gFnPc1
a	a	k8xC
na	na	k7c4
nějakou	nějaký	k3yIgFnSc4
vážnější	vážní	k2eAgFnSc4d2
soutěž	soutěž	k1gFnSc4
nebylo	být	k5eNaImAgNnS
v	v	k7c6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
sestavy	sestava	k1gFnSc2
týmů	tým	k1gInPc2
měnily	měnit	k5eAaImAgInP
zápas	zápas	k1gInSc4
od	od	k7c2
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
ani	ani	k8xC
pomyšlení	pomyšlení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
různými	různý	k2eAgFnPc7d1
posilami	posila	k1gFnPc7
Sparty	Sparta	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
objevil	objevit	k5eAaPmAgMnS
i	i	k9
jeden	jeden	k4xCgMnSc1
skvělý	skvělý	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
–	–	k?
Antonín	Antonín	k1gMnSc1
Janda	Janda	k1gMnSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc4d1
Očko	očko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišel	přijít	k5eAaPmAgMnS
z	z	k7c2
Olympie	Olympia	k1gFnSc2
Praha	Praha	k1gFnSc1
VII	VII	kA
<g/>
,	,	kIx,
přivedl	přivést	k5eAaPmAgInS
i	i	k9
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
pilířem	pilíř	k1gInSc7
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
války	válka	k1gFnSc2
působil	působit	k5eAaImAgMnS
v	v	k7c6
týmu	tým	k1gInSc6
také	také	k9
brankář	brankář	k1gMnSc1
Vlasta	Vlasta	k1gMnSc1
Burian	Burian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
šanci	šance	k1gFnSc4
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
i	i	k9
reprezentačním	reprezentační	k2eAgMnSc7d1
brankářem	brankář	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
dal	dát	k5eAaPmAgInS
přednost	přednost	k1gFnSc4
divadelní	divadelní	k2eAgFnSc4d1
a	a	k8xC
filmové	filmový	k2eAgFnSc3d1
kariéře	kariéra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedostatek	nedostatek	k1gInSc1
hráčů	hráč	k1gMnPc2
přivedl	přivést	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
Václava	Václava	k1gFnSc1
Rudla	rudnout	k5eAaImAgFnS
<g/>
,	,	kIx,
prvního	první	k4xOgMnSc4
fotbalistu	fotbalista	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
pravých	pravý	k2eAgFnPc6d1
kopačkách	kopačka	k1gFnPc6
<g/>
,	,	kIx,
objednaných	objednaný	k2eAgFnPc2d1
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
myšlenku	myšlenka	k1gFnSc4
založit	založit	k5eAaPmF
první	první	k4xOgInSc4
dorost	dorost	k1gInSc4
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gMnPc3
svěřencům	svěřenec	k1gMnPc3
patřili	patřit	k5eAaImAgMnP
i	i	k9
Josef	Josef	k1gMnSc1
Maleček	Maleček	k1gMnSc1
a	a	k8xC
Jiří	Jiří	k1gMnSc1
Tožička	Tožička	k1gMnSc1
<g/>
,	,	kIx,
budoucí	budoucí	k2eAgMnPc1d1
skvělí	skvělý	k2eAgMnPc1d1
fotbalisté	fotbalista	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
hokejisté	hokejista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Rudl	rudnout	k5eAaImAgMnS
se	se	k3xPyFc4
také	také	k9
zapsal	zapsat	k5eAaPmAgMnS
jako	jako	k9
první	první	k4xOgMnSc1
sparťanský	sparťanský	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
a	a	k8xC
první	první	k4xOgMnSc1
trenér	trenér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Železná	železný	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
</s>
<s>
Válka	válka	k1gFnSc1
však	však	k9
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
skončila	skončit	k5eAaPmAgFnS
a	a	k8xC
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
se	se	k3xPyFc4
sešlo	sejít	k5eAaPmAgNnS
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgNnSc1d1
mužstvo	mužstvo	k1gNnSc1
<g/>
,	,	kIx,
zvané	zvaný	k2eAgNnSc1d1
proto	proto	k8xC
„	„	k?
<g/>
Železná	železný	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
největší	veliký	k2eAgNnSc4d3
překvapení	překvapení	k1gNnSc4
patřil	patřit	k5eAaImAgMnS
návrat	návrat	k1gInSc4
ve	v	k7c6
válce	válka	k1gFnSc6
těžce	těžce	k6eAd1
raněného	raněný	k2eAgMnSc4d1
Václava	Václav	k1gMnSc4
Piláta	Pilát	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
branky	branka	k1gFnSc2
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgMnS
vysoký	vysoký	k2eAgMnSc1d1
František	František	k1gMnSc1
Peyr	Peyra	k1gFnPc2
<g/>
,	,	kIx,
povoláním	povolání	k1gNnSc7
zlatník	zlatník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obraně	obrana	k1gFnSc6
hráli	hrát	k5eAaImAgMnP
Antonín	Antonín	k1gMnSc1
Hojer	Hojer	k1gMnSc1
a	a	k8xC
Miroslav	Miroslav	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdnou	hvězdný	k2eAgFnSc4d1
zálohu	záloha	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
neměla	mít	k5eNaImAgFnS
na	na	k7c6
pevninské	pevninský	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
obdoby	obdoba	k1gFnSc2
<g/>
,	,	kIx,
tvořili	tvořit	k5eAaImAgMnP
František	František	k1gMnSc1
Kolenatý	kolenatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Fivébr	Fivébr	k1gMnSc1
a	a	k8xC
Káďa	Káďa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
nahradil	nahradit	k5eAaPmAgInS
Káďa	Káďum	k1gNnPc4
ve	v	k7c4
středu	středa	k1gFnSc4
hřiště	hřiště	k1gNnSc2
svého	svůj	k3xOyFgMnSc2
učitele	učitel	k1gMnSc2
Fivébra	Fivébr	k1gMnSc2
a	a	k8xC
nalevo	nalevo	k6eAd1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
Antonín	Antonín	k1gMnSc1
Perner	Perner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
útoku	útok	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vedl	vést	k5eAaImAgInS
Pilát	pilát	k1gInSc4
<g/>
,	,	kIx,
působil	působit	k5eAaImAgMnS
vpravo	vpravo	k6eAd1
Josef	Josef	k1gMnSc1
Sedláček	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
Šmrdlous	Šmrdlous	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
Janda-Očko	Janda-Očko	k6eAd1
<g/>
,	,	kIx,
vlevo	vlevo	k6eAd1
byli	být	k5eAaImAgMnP
Josef	Josef	k1gMnSc1
„	„	k?
<g/>
Boban	Boban	k1gMnSc1
<g/>
“	“	k?
Šroubek	Šroubek	k1gMnSc1
a	a	k8xC
Otto	Otto	k1gMnSc1
Škvajn	Škvajn	k1gMnSc1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
též	též	k9
Mazal	Mazal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1920	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Antverpách	Antverpy	k1gFnPc6
konaly	konat	k5eAaImAgFnP
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
a	a	k8xC
československá	československý	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nastoupila	nastoupit	k5eAaPmAgFnS
proti	proti	k7c3
domácím	domácí	k2eAgMnPc3d1
Belgičanům	Belgičan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
Káďa	Káďa	k1gMnSc1
však	však	k9
své	svůj	k3xOyFgNnSc4
mužstvo	mužstvo	k1gNnSc4
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
rozhodování	rozhodování	k1gNnSc3
sudího	sudí	k1gMnSc2
odvolal	odvolat	k5eAaPmAgMnS
ze	z	k7c2
hřiště	hřiště	k1gNnSc2
a	a	k8xC
reprezentace	reprezentace	k1gFnSc1
byla	být	k5eAaImAgFnS
vyloučena	vyloučit	k5eAaPmNgFnS
z	z	k7c2
celého	celý	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
nastoupilo	nastoupit	k5eAaPmAgNnS
v	v	k7c6
československém	československý	k2eAgInSc6d1
týmu	tým	k1gInSc2
deset	deset	k4xCc1
hráčů	hráč	k1gMnPc2
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
A.C.	A.C.	k?
Sparta	Sparta	k1gFnSc1
1921	#num#	k4
–	–	k?
Káďa	Káďa	k1gMnSc1
<g/>
,	,	kIx,
Mazal	Mazal	k1gMnSc1
<g/>
,	,	kIx,
Pilát	Pilát	k1gMnSc1
<g/>
,	,	kIx,
Perner	Perner	k1gMnSc1
<g/>
,	,	kIx,
Peyr	Peyr	k1gMnSc1
<g/>
,	,	kIx,
Sedláček	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
Šroubek	Šroubek	k1gMnSc1
<g/>
,	,	kIx,
Pospíšil	Pospíšil	k1gMnSc1
<g/>
,	,	kIx,
Červený	Červený	k1gMnSc1
<g/>
,	,	kIx,
Kolenatý	kolenatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Hoyer	Hoyer	k1gMnSc1
</s>
<s>
Celostátní	celostátní	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ještě	ještě	k9
neexistovalo	existovat	k5eNaImAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c4
vrcholnou	vrcholný	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
se	se	k3xPyFc4
všeobecně	všeobecně	k6eAd1
považovala	považovat	k5eAaImAgFnS
početná	početný	k2eAgFnSc1d1
a	a	k8xC
mocná	mocný	k2eAgFnSc1d1
Středočeská	středočeský	k2eAgFnSc1d1
župa	župa	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnSc1
vítěz	vítěz	k1gMnSc1
byl	být	k5eAaImAgMnS
uznáván	uznávat	k5eAaImNgMnS
jako	jako	k8xS,k8xC
domácí	domácí	k2eAgMnSc1d1
šampion	šampion	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
vítězila	vítězit	k5eAaImAgFnS
nepřetržitě	přetržitě	k6eNd1
od	od	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěž	soutěž	k1gFnSc1
se	se	k3xPyFc4
hrála	hrát	k5eAaImAgFnS
jednokolově	jednokolově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
za	za	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
odehrála	odehrát	k5eAaPmAgFnS
59	#num#	k4
zápasů	zápas	k1gInPc2
a	a	k8xC
kromě	kromě	k7c2
jednoho	jeden	k4xCgInSc2
všechny	všechen	k3xTgInPc4
vyhrála	vyhrát	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
235	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc1d1
prohraný	prohraný	k2eAgInSc1d1
zápas	zápas	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
druhého	druhý	k4xOgNnSc2
kola	kolo	k1gNnSc2
prvního	první	k4xOgMnSc2
ročníku	ročník	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Sparta	Sparta	k1gFnSc1
nečekaně	nečekaně	k6eAd1
prohrála	prohrát	k5eAaPmAgFnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
s	s	k7c7
Unionem	union	k1gInSc7
Žižkov	Žižkov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgNnSc3
vítězství	vítězství	k1gNnSc3
se	se	k3xPyFc4
pak	pak	k6eAd1
novináři	novinář	k1gMnPc1
a	a	k8xC
fanoušci	fanoušek	k1gMnPc1
vraceli	vracet	k5eAaImAgMnP
ještě	ještě	k6eAd1
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrdinové	Hrdinová	k1gFnPc1
tohoto	tento	k3xDgInSc2
zápasu	zápas	k1gInSc2
byli	být	k5eAaImAgMnP
brankář	brankář	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Kaliba	Kaliba	k1gMnSc1
a	a	k8xC
skórující	skórující	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Dvořáček	Dvořáček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc4
hráči	hráč	k1gMnPc1
po	po	k7c6
nějaké	nějaký	k3yIgFnSc6
době	doba	k1gFnSc6
přestoupili	přestoupit	k5eAaPmAgMnP
do	do	k7c2
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1919	#num#	k4
a	a	k8xC
1922	#num#	k4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
i	i	k9
turnaj	turnaj	k1gInSc1
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgMnPc7d1
župními	župní	k2eAgMnPc7d1
mistry	mistr	k1gMnPc7
o	o	k7c4
mistra	mistr	k1gMnSc4
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
ligovým	ligový	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
Sparta	Sparta	k1gFnSc1
porazila	porazit	k5eAaPmAgFnS
všechny	všechen	k3xTgMnPc4
čtyři	čtyři	k4xCgMnPc4
soupeře	soupeř	k1gMnPc4
a	a	k8xC
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
získala	získat	k5eAaPmAgFnS
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podruhé	podruhé	k6eAd1
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
vyřazovacím	vyřazovací	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
a	a	k8xC
Sparta	Sparta	k1gFnSc1
byla	být	k5eAaImAgFnS
nasazena	nasadit	k5eAaPmNgFnS
až	až	k6eAd1
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
přehrála	přehrát	k5eAaPmAgFnS
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tituly	titul	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
1912	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
a	a	k8xC
1922	#num#	k4
byly	být	k5eAaImAgInP
Českomoravským	českomoravský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
uznány	uznat	k5eAaPmNgFnP
jako	jako	k9
mistrovské	mistrovský	k2eAgFnPc1d1
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
fotbalových	fotbalový	k2eAgMnPc2d1
znalců	znalec	k1gMnPc2
evropského	evropský	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
považovala	považovat	k5eAaImAgFnS
za	za	k7c4
nejlepší	dobrý	k2eAgInPc4d3
týmy	tým	k1gInPc4
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Spartu	Sparta	k1gFnSc4
<g/>
,	,	kIx,
FC	FC	kA
Barcelonu	Barcelona	k1gFnSc4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Nürnberg	Nürnberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
prestižním	prestižní	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
o	o	k7c4
titul	titul	k1gInSc4
neoficiálního	neoficiální	k2eAgMnSc2d1,k2eNgMnSc2d1
mistra	mistr	k1gMnSc2
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
sparťani	sparťan	k1gMnPc1
přehráli	přehrát	k5eAaPmAgMnP
oba	dva	k4xCgMnPc4
soupeře	soupeř	k1gMnPc4
na	na	k7c4
jejich	jejich	k3xOp3gFnSc1
půdě	půda	k1gFnSc3
–	–	k?
Norimberk	Norimberk	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
Barcelonu	Barcelona	k1gFnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Konkurence	konkurence	k1gFnSc1
doma	doma	k6eAd1
i	i	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
začala	začít	k5eAaPmAgFnS
s	s	k7c7
popularizací	popularizace	k1gFnSc7
fotbalu	fotbal	k1gInSc2
stoupat	stoupat	k5eAaImF
a	a	k8xC
domácí	domácí	k2eAgMnPc1d1
senzacechtiví	senzacechtivý	k2eAgMnPc1d1
novináři	novinář	k1gMnPc1
začali	začít	k5eAaPmAgMnP
poté	poté	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
odešel	odejít	k5eAaPmAgMnS
Pilát	pilát	k1gInSc4
<g/>
,	,	kIx,
psát	psát	k5eAaImF
o	o	k7c4
rezavění	rezavění	k1gNnSc4
železné	železný	k2eAgFnSc2d1
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zahraničí	zahraničí	k1gNnSc6
však	však	k9
mělo	mít	k5eAaImAgNnS
klubové	klubový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
stále	stále	k6eAd1
vynikající	vynikající	k2eAgInSc1d1
zvuk	zvuk	k1gInSc1
a	a	k8xC
Sparta	Sparta	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
pozvána	pozvat	k5eAaPmNgFnS
na	na	k7c4
americký	americký	k2eAgInSc4d1
zájezd	zájezd	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužstvo	mužstvo	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
vynikajícího	vynikající	k2eAgMnSc4d1
střelce	střelec	k1gMnPc4
Josefa	Josef	k1gMnSc4
Silného	silný	k2eAgMnSc4d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
dosáhl	dosáhnout	k5eAaPmAgMnS
sta	sto	k4xCgNnPc4
vstřelených	vstřelený	k2eAgMnPc2d1
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
však	však	k9
musel	muset	k5eAaImAgInS
zaplatit	zaplatit	k5eAaPmF
Slavii	slavie	k1gFnSc4
tehdy	tehdy	k6eAd1
ohromných	ohromný	k2eAgNnPc2d1
60	#num#	k4
000	#num#	k4
Kčs	Kčs	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
Janda-Očko	Janda-Očko	k6eAd1
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
si	se	k3xPyFc3
zaoceánští	zaoceánský	k2eAgMnPc1d1
Češi	Čech	k1gMnPc1
vynutili	vynutit	k5eAaPmAgMnP
kvůli	kvůli	k7c3
skvělým	skvělý	k2eAgInPc3d1
ohlasům	ohlas	k1gInPc3
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
výkony	výkon	k1gInPc1
na	na	k7c4
Pershingově	Pershingově	k1gMnSc4
olympiádě	olympiáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestování	cestování	k1gNnPc1
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
po	po	k7c6
ní	on	k3xPp3gFnSc6
bylo	být	k5eAaImAgNnS
tehdy	tehdy	k6eAd1
velmi	velmi	k6eAd1
únavné	únavný	k2eAgFnPc1d1
a	a	k8xC
po	po	k7c6
namáhavé	namáhavý	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
navíc	navíc	k6eAd1
čekal	čekat	k5eAaImAgMnS
sparťany	sparťan	k1gMnPc4
souboj	souboj	k1gInSc1
většinou	většinou	k6eAd1
s	s	k7c7
anglickým	anglický	k2eAgInSc7d1
či	či	k8xC
skotským	skotský	k2eAgInSc7d1
profesionálním	profesionální	k2eAgInSc7d1
klubem	klub	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
z	z	k7c2
dvanácti	dvanáct	k4xCc2
utkání	utkání	k1gNnPc2
sedm	sedm	k4xCc1
vyhráli	vyhrát	k5eAaPmAgMnP
a	a	k8xC
poražení	poražený	k1gMnPc1
byli	být	k5eAaImAgMnP
jen	jen	k9
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
fotbalový	fotbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
stanovil	stanovit	k5eAaPmAgInS
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
profesionalizaci	profesionalizace	k1gFnSc4
fotbalu	fotbal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
prvními	první	k4xOgInPc7
17	#num#	k4
profesionálními	profesionální	k2eAgInPc7d1
kluby	klub	k1gInPc7
byla	být	k5eAaImAgFnS
i	i	k9
Sparta	Sparta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotbal	fotbal	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
rozdělil	rozdělit	k5eAaPmAgInS
do	do	k7c2
dvou	dva	k4xCgInPc2
proudů	proud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionální	profesionální	k2eAgInPc1d1
týmy	tým	k1gInPc1
vytvořily	vytvořit	k5eAaPmAgInP
první	první	k4xOgFnSc4
a	a	k8xC
druhou	druhý	k4xOgFnSc4
ligu	liga	k1gFnSc4
a	a	k8xC
ty	ten	k3xDgFnPc1
zbylé	zbylý	k2eAgFnPc1d1
dál	daleko	k6eAd2
hrály	hrát	k5eAaImAgFnP
po	po	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
župách	župa	k1gFnPc6
o	o	k7c4
titul	titul	k1gInSc4
amatérského	amatérský	k2eAgMnSc2d1
mistra	mistr	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
ligový	ligový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
odehrála	odehrát	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1926	#num#	k4
s	s	k7c7
Vršovicemi	Vršovice	k1gFnPc7
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Bohemians	Bohemians	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
zvítězila	zvítězit	k5eAaPmAgFnS
7	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
rozhodčí	rozhodčí	k1gMnSc1
nakonec	nakonec	k6eAd1
neuznal	uznat	k5eNaPmAgMnS
výsledek	výsledek	k1gInSc4
zápasu	zápas	k1gInSc2
kvůli	kvůli	k7c3
nevyhovujícímu	vyhovující	k2eNgInSc3d1
terénu	terén	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
mnoho	mnoho	k4c1
různých	různý	k2eAgInPc2d1
protestů	protest	k1gInPc2
a	a	k8xC
tahanic	tahanice	k1gFnPc2
u	u	k7c2
zeleného	zelený	k2eAgInSc2d1
stolu	stol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
po	po	k7c6
mnoha	mnoho	k4c6
měsících	měsíc	k1gInPc6
byl	být	k5eAaImAgMnS
po	po	k7c6
odehrání	odehrání	k1gNnSc6
soutěže	soutěž	k1gFnSc2
nařízen	nařízen	k2eAgInSc4d1
nový	nový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
Sparta	Sparta	k1gFnSc1
opět	opět	k6eAd1
vyhrála	vyhrát	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
by	by	kYmCp3nS
jí	on	k3xPp3gFnSc7
zajistil	zajistit	k5eAaPmAgMnS
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
ale	ale	k8xC
skončila	skončit	k5eAaPmAgFnS
o	o	k7c6
skóre	skóre	k1gNnSc6
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
ročník	ročník	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
původně	původně	k6eAd1
dvoukolový	dvoukolový	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
jen	jen	k9
jarní	jarní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
na	na	k7c4
podzim	podzim	k1gInSc4
začít	začít	k5eAaPmF
s	s	k7c7
anglickým	anglický	k2eAgInSc7d1
ligovým	ligový	k2eAgInSc7d1
vzorem	vzor	k1gInSc7
podzim	podzim	k1gInSc1
–	–	k?
jaro	jaro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
1925	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
už	už	k6eAd1
sparťané	sparťan	k1gMnPc1
vyhráli	vyhrát	k5eAaPmAgMnP
a	a	k8xC
během	během	k7c2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
získali	získat	k5eAaPmAgMnP
dohromady	dohromady	k6eAd1
šest	šest	k4xCc4
titulů	titul	k1gInPc2
mistra	mistr	k1gMnSc4
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1923	#num#	k4
až	až	k9
1928	#num#	k4
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
i	i	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
mužů	muž	k1gMnPc2
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
Jiří	Jiří	k1gMnSc1
Stříbrný	stříbrný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Spartě	Sparta	k1gFnSc3
se	se	k3xPyFc4
však	však	k9
nedařilo	dařit	k5eNaImAgNnS
jen	jen	k9
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
té	ten	k3xDgFnSc6
mezinárodní	mezinárodní	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1927	#num#	k4
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgInS
první	první	k4xOgInSc1
ročník	ročník	k1gInSc1
Středoevropského	středoevropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
jakýsi	jakýsi	k3yIgInSc1
předobraz	předobraz	k1gInSc1
pozdějších	pozdní	k2eAgFnPc2d2
evropských	evropský	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
sice	sice	k8xC
pocházeli	pocházet	k5eAaImAgMnP
jen	jen	k9
ze	z	k7c2
středoevropského	středoevropský	k2eAgInSc2d1
regionu	region	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgMnS
spolu	spolu	k6eAd1
s	s	k7c7
Pyrenejským	pyrenejský	k2eAgInSc7d1
poloostrovem	poloostrov	k1gInSc7
považován	považován	k2eAgInSc1d1
za	za	k7c4
nejsilnější	silný	k2eAgFnSc4d3
fotbalovou	fotbalový	k2eAgFnSc4d1
část	část	k1gFnSc4
pevninské	pevninský	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
první	první	k4xOgInSc4
ročník	ročník	k1gInSc4
sparťané	sparťan	k1gMnPc1
vyhráli	vyhrát	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrál	hrát	k5eAaImAgInS
se	se	k3xPyFc4
dvoukolový	dvoukolový	k2eAgInSc1d1
systém	systém	k1gInSc1
známý	známý	k2eAgInSc1d1
z	z	k7c2
dnešních	dnešní	k2eAgInPc2d1
fotbalových	fotbalový	k2eAgInPc2d1
pohárů	pohár	k1gInPc2
s	s	k7c7
tím	ten	k3xDgInSc7
rozdílem	rozdíl	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
rovnosti	rovnost	k1gFnSc2
skóre	skóre	k1gNnSc4
nerozhodovaly	rozhodovat	k5eNaImAgInP
góly	gól	k1gInPc1
vstřelené	vstřelený	k2eAgInPc1d1
na	na	k7c6
soupeřově	soupeřův	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
rozhodující	rozhodující	k2eAgInSc4d1
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
těsně	těsně	k6eAd1
o	o	k7c6
skóre	skóre	k1gNnSc6
vyřadili	vyřadit	k5eAaPmAgMnP
vídeňskou	vídeňský	k2eAgFnSc4d1
Admiru	Admira	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
semifinále	semifinále	k1gNnSc6
dvakrát	dvakrát	k6eAd1
remizovali	remizovat	k5eAaPmAgMnP
s	s	k7c7
budapešťskou	budapešťský	k2eAgFnSc7d1
Hungarií	Hungarie	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
rozhodující	rozhodující	k2eAgInSc4d1
zápas	zápas	k1gInSc4
se	se	k3xPyFc4
kvůli	kvůli	k7c3
neoprávněnému	oprávněný	k2eNgInSc3d1
startu	start	k1gInSc3
budapešťského	budapešťský	k2eAgMnSc2d1
Kalmána	Kalmán	k1gMnSc2
Konráda	Konrád	k1gMnSc2
nehrál	hrát	k5eNaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
Sparta	Sparta	k1gFnSc1
postoupila	postoupit	k5eAaPmAgFnS
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
se	se	k3xPyFc4
střetla	střetnout	k5eAaPmAgFnS
s	s	k7c7
vídeňským	vídeňský	k2eAgInSc7d1
Rapidem	rapid	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
semifinále	semifinále	k1gNnSc6
vyřadil	vyřadit	k5eAaPmAgInS
Slavii	slavie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finálový	finálový	k2eAgInSc1d1
dvojzápas	dvojzápas	k1gInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
už	už	k6eAd1
v	v	k7c6
prvním	první	k4xOgNnSc6
utkání	utkání	k1gNnSc6
na	na	k7c6
Letné	Letná	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káďa	Káďa	k1gMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
gól	gól	k1gInSc4
hned	hned	k6eAd1
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
a	a	k8xC
Sparta	Sparta	k1gFnSc1
zvítězila	zvítězit	k5eAaPmAgFnS
6	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rapid	rapid	k1gInSc4
sice	sice	k8xC
doma	doma	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
prvním	první	k4xOgMnSc6
držitelem	držitel	k1gMnSc7
prestižní	prestižní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
musela	muset	k5eAaImAgFnS
kvůli	kvůli	k7c3
divokým	divoký	k2eAgFnPc3d1
scénám	scéna	k1gFnPc3
zklamaných	zklamaný	k2eAgFnPc2d1
domácích	domácí	k1gFnPc2
fanoušků	fanoušek	k1gMnPc2
rychle	rychle	k6eAd1
utéct	utéct	k5eAaPmF
do	do	k7c2
šaten	šatna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
získala	získat	k5eAaPmAgFnS
pohár	pohár	k1gInSc4
podruhé	podruhé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
původního	původní	k2eAgInSc2d1
vítězného	vítězný	k2eAgInSc2d1
týmu	tým	k1gInSc2
zbyl	zbýt	k5eAaPmAgMnS
pouze	pouze	k6eAd1
obránce	obránce	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Burgr	Burgr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
debutoval	debutovat	k5eAaBmAgMnS
v	v	k7c6
rudém	rudý	k2eAgInSc6d1
dresu	dres	k1gInSc6
právě	právě	k9
v	v	k7c6
pražském	pražský	k2eAgNnSc6d1
finále	finále	k1gNnSc6
s	s	k7c7
Rapidem	rapid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěž	soutěž	k1gFnSc1
měla	mít	k5eAaImAgFnS
16	#num#	k4
účastníků	účastník	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
Sparta	Sparta	k1gFnSc1
tak	tak	k6eAd1
musela	muset	k5eAaImAgFnS
k	k	k7c3
zisku	zisk	k1gInSc3
poháru	pohár	k1gInSc2
vyřadit	vyřadit	k5eAaPmF
4	#num#	k4
soupeře	soupeř	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgInPc6
dvou	dva	k4xCgInPc6
kolech	kolo	k1gNnPc6
si	se	k3xPyFc3
poradila	poradit	k5eAaPmAgFnS
s	s	k7c7
First	First	k1gFnSc4
Viennou	Vienný	k2eAgFnSc7d1
i	i	k8xC
Fiorentinou	Fiorentina	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
dostala	dostat	k5eAaPmAgFnS
7	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
narazila	narazit	k5eAaPmAgFnS
na	na	k7c4
slavný	slavný	k2eAgInSc4d1
Juventus	Juventus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doma	doma	k6eAd1
zvítězila	zvítězit	k5eAaPmAgFnS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
v	v	k7c6
Turíně	Turín	k1gInSc6
prohrála	prohrát	k5eAaPmAgFnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
podle	podle	k7c2
dnešních	dnešní	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
k	k	k7c3
postupu	postup	k1gInSc3
stačilo	stačit	k5eAaBmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
však	však	k9
museli	muset	k5eAaImAgMnP
sparťané	sparťan	k1gMnPc1
sehrát	sehrát	k5eAaPmF
ještě	ještě	k9
rozhodující	rozhodující	k2eAgInSc4d1
duel	duel	k1gInSc4
v	v	k7c6
Basileji	Basilej	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slavném	slavný	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
vedla	vést	k5eAaImAgFnS
Sparta	Sparta	k1gFnSc1
po	po	k7c6
poločase	poločas	k1gInSc6
již	již	k6eAd1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italové	Ital	k1gMnPc1
sice	sice	k8xC
20	#num#	k4
minut	minuta	k1gFnPc2
před	před	k7c7
koncem	konec	k1gInSc7
z	z	k7c2
penalty	penalta	k1gFnSc2
snížili	snížit	k5eAaPmAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
závěr	závěr	k1gInSc4
patřil	patřit	k5eAaImAgMnS
sparťanům	sparťan	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
dali	dát	k5eAaPmAgMnP
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
a	a	k8xC
vítězstvím	vítězství	k1gNnSc7
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
postoupili	postoupit	k5eAaPmAgMnP
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgInSc1d1
gól	gól	k1gInSc1
dal	dát	k5eAaPmAgInS
střelou	střela	k1gFnSc7
přes	přes	k7c4
hlavu	hlava	k1gFnSc4
Raymond	Raymonda	k1gFnPc2
Braine	Brain	k1gInSc5
<g/>
,	,	kIx,
belgická	belgický	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
za	za	k7c4
klub	klub	k1gInSc4
ve	v	k7c6
281	#num#	k4
zápasech	zápas	k1gInPc6
vstřelil	vstřelit	k5eAaPmAgMnS
300	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
na	na	k7c4
Spartu	Sparta	k1gFnSc4
narazil	narazit	k5eAaPmAgMnS
budapešťský	budapešťský	k2eAgMnSc1d1
Ferencváros	Ferencvárosa	k1gFnPc2
<g/>
,	,	kIx,
kterému	který	k3yIgNnSc3,k3yRgNnSc3,k3yQgNnSc3
sparťané	sparťan	k1gMnPc1
venku	venku	k6eAd1
podlehli	podlehnout	k5eAaPmAgMnP
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgInSc1d1
zápas	zápas	k1gInSc1
byl	být	k5eAaImAgInS
křtem	křest	k1gInSc7
pro	pro	k7c4
nový	nový	k2eAgInSc4d1
Strahovský	strahovský	k2eAgInSc4d1
stadion	stadion	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
vyprodán	vyprodat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
duelu	duel	k1gInSc6
lepší	dobrý	k2eAgMnSc1d2
a	a	k8xC
díky	díky	k7c3
dvěma	dva	k4xCgInPc3
gólům	gól	k1gInPc3
Brainea	Braine	k1gInSc2
a	a	k8xC
trefě	trefa	k1gFnSc6
Faczinka	Faczinka	k1gFnSc1
zvítězila	zvítězit	k5eAaPmAgFnS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
podruhé	podruhé	k6eAd1
získala	získat	k5eAaPmAgFnS
Středoevropský	středoevropský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
Sparta	Sparta	k1gFnSc1
pohár	pohár	k1gInSc4
téměř	téměř	k6eAd1
obhájila	obhájit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
finále	finále	k1gNnSc6
doma	doma	k6eAd1
překvapivě	překvapivě	k6eAd1
podlehla	podlehnout	k5eAaPmAgFnS
vídeňské	vídeňský	k2eAgFnPc4d1
Austrii	Austrie	k1gFnSc4
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
venku	venku	k6eAd1
uhrála	uhrát	k5eAaPmAgFnS
pouze	pouze	k6eAd1
bezbrankovou	bezbrankový	k2eAgFnSc4d1
remízu	remíza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
však	však	k9
Sparta	Sparta	k1gFnSc1
zařadila	zařadit	k5eAaPmAgFnS
se	s	k7c7
dvěma	dva	k4xCgNnPc7
vítězstvími	vítězství	k1gNnPc7
mezi	mezi	k7c4
nejlepší	dobrý	k2eAgInPc4d3
týmy	tým	k1gInPc4
předválečné	předválečný	k2eAgFnSc2d1
historie	historie	k1gFnSc2
Středoevropského	středoevropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
a	a	k8xC
poválečná	poválečný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
</s>
<s>
Vlasta	Vlasta	k1gMnSc1
Burian	Burian	k1gMnSc1
</s>
<s>
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
sezóny	sezóna	k1gFnSc2
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
titul	titul	k1gInSc4
vyhrála	vyhrát	k5eAaPmAgFnS
Viktoria	Viktoria	k1gFnSc1
Žižkov	Žižkov	k1gInSc1
<g/>
,	,	kIx,
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
lize	liga	k1gFnSc6
Sparta	Sparta	k1gFnSc1
vždy	vždy	k6eAd1
nejhůře	zle	k6eAd3
druhá	druhý	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
války	válka	k1gFnSc2
však	však	k9
začal	začít	k5eAaPmAgMnS
sparťanský	sparťanský	k2eAgInSc4d1
kádr	kádr	k1gInSc4
stárnout	stárnout	k5eAaImF
a	a	k8xC
výsledkem	výsledek	k1gInSc7
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
až	až	k9
čtvrté	čtvrtý	k4xOgNnSc1
místo	místo	k1gNnSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
byl	být	k5eAaImAgInS
jen	jen	k9
začátek	začátek	k1gInSc1
déletrvající	déletrvající	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
špatně	špatně	k6eAd1
a	a	k8xC
po	po	k7c6
podzimní	podzimní	k2eAgFnSc6d1
části	část	k1gFnSc6
soutěže	soutěž	k1gFnSc2
měla	mít	k5eAaImAgFnS
z	z	k7c2
jedenácti	jedenáct	k4xCc2
zápasů	zápas	k1gInPc2
jen	jen	k6eAd1
tři	tři	k4xCgInPc4
body	bod	k1gInPc4
a	a	k8xC
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
navíc	navíc	k6eAd1
završila	završit	k5eAaPmAgFnS
potupná	potupný	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
v	v	k7c6
derby	derby	k1gNnSc6
se	s	k7c7
Slavií	slavie	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
Sparta	Sparta	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
záchraně	záchrana	k1gFnSc3
se	se	k3xPyFc4
musely	muset	k5eAaImAgFnP
udělat	udělat	k5eAaPmF
změny	změna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužstva	mužstvo	k1gNnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
Vlasta	Vlasta	k1gMnSc1
Burian	Burian	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejen	nejen	k6eAd1
trenérem	trenér	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
na	na	k7c4
oko	oko	k1gNnSc4
zaměstnal	zaměstnat	k5eAaPmAgMnS
několik	několik	k4yIc4
hráčů	hráč	k1gMnPc2
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
divadle	divadlo	k1gNnSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
obešel	obejít	k5eAaPmAgMnS
nacistický	nacistický	k2eAgInSc4d1
zákaz	zákaz	k1gInSc4
profesionalismu	profesionalismus	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
praxe	praxe	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
běžná	běžný	k2eAgFnSc1d1
u	u	k7c2
mnoha	mnoho	k4c2
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burian	Burian	k1gMnSc1
pomáhal	pomáhat	k5eAaImAgMnS
také	také	k9
shánět	shánět	k5eAaImF
nové	nový	k2eAgFnPc4d1
posily	posila	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
staly	stát	k5eAaPmAgFnP
novými	nový	k2eAgFnPc7d1
oporami	opora	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejzásadnější	zásadní	k2eAgFnSc7d3
byla	být	k5eAaImAgFnS
však	však	k9
pomoc	pomoc	k1gFnSc4
od	od	k7c2
Slavie	slavie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
svému	svůj	k3xOyFgMnSc3
největšímu	veliký	k2eAgMnSc3d3
protivníkovi	protivník	k1gMnSc3
půjčila	půjčit	k5eAaPmAgFnS
na	na	k7c4
jarní	jarní	k2eAgFnSc4d1
část	část	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
kanonýra	kanonýr	k1gMnSc2
Vojtěcha	Vojtěch	k1gMnSc2
Bradáče	Bradáč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
jarní	jarní	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
opět	opět	k6eAd1
nepovedl	povést	k5eNaPmAgMnS
a	a	k8xC
Sparta	Sparta	k1gFnSc1
v	v	k7c6
dalším	další	k2eAgNnSc6d1
derby	derby	k1gNnSc6
podlehla	podlehnout	k5eAaPmAgNnP
opět	opět	k6eAd1
potupně	potupně	k6eAd1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
však	však	k9
začalo	začít	k5eAaPmAgNnS
dařit	dařit	k5eAaImF
a	a	k8xC
až	až	k9
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
se	se	k3xPyFc4
výhrou	výhra	k1gFnSc7
nad	nad	k7c7
Zlínem	Zlín	k1gInSc7
definitivně	definitivně	k6eAd1
zachránila	zachránit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgFnPc1d1
posily	posila	k1gFnPc1
se	se	k3xPyFc4
začínaly	začínat	k5eAaImAgFnP
prosazovat	prosazovat	k5eAaImF
v	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Sparta	Sparta	k1gFnSc1
opět	opět	k6eAd1
hrála	hrát	k5eAaImAgFnS
na	na	k7c6
vyšších	vysoký	k2eAgFnPc6d2
pozicích	pozice	k1gFnPc6
a	a	k8xC
získala	získat	k5eAaPmAgFnS
titul	titul	k1gInSc4
vicemistra	vicemistr	k1gMnSc2
za	za	k7c7
Slavií	slavie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
čtvrtý	čtvrtý	k4xOgInSc4
titul	titul	k1gInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
byl	být	k5eAaImAgInS
posledním	poslední	k2eAgInSc7d1
hraným	hraný	k2eAgInSc7d1
za	za	k7c4
války	válek	k1gInPc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
sezóna	sezóna	k1gFnSc1
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
se	se	k3xPyFc4
kvůli	kvůli	k7c3
blížícím	blížící	k2eAgInPc3d1
se	se	k3xPyFc4
frontám	fronta	k1gFnPc3
ani	ani	k8xC
nerozehrála	rozehrát	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
obě	dva	k4xCgNnPc4
derby	derby	k1gNnPc4
a	a	k8xC
v	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
ztratila	ztratit	k5eAaPmAgFnS
pouze	pouze	k6eAd1
4	#num#	k4
body	bod	k1gInPc4
a	a	k8xC
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
době	doba	k1gFnSc6
opět	opět	k6eAd1
získala	získat	k5eAaPmAgFnS
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
osvobození	osvobození	k1gNnSc6
se	se	k3xPyFc4
většina	většina	k1gFnSc1
hráčů	hráč	k1gMnPc2
týmu	tým	k1gInSc2
dostala	dostat	k5eAaPmAgNnP
i	i	k9
do	do	k7c2
reprezentace	reprezentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
fotbalový	fotbalový	k2eAgInSc1d1
život	život	k1gInSc1
rychle	rychle	k6eAd1
obnovil	obnovit	k5eAaPmAgInS
a	a	k8xC
zdálo	zdát	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vše	všechen	k3xTgNnSc1
vrací	vracet	k5eAaImIp3nS
do	do	k7c2
starých	starý	k2eAgFnPc2d1
kolejí	kolej	k1gFnPc2
–	–	k?
Sparta	Sparta	k1gFnSc1
i	i	k8xC
Slavia	Slavia	k1gFnSc1
byly	být	k5eAaImAgFnP
stále	stále	k6eAd1
nejlepšími	dobrý	k2eAgInPc7d3
týmy	tým	k1gInPc7
a	a	k8xC
čtyři	čtyři	k4xCgInPc1
tituly	titul	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
do	do	k7c2
přelomového	přelomový	k2eAgInSc2d1
roku	rok	k1gInSc2
1948	#num#	k4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
si	se	k3xPyFc3
rozdělily	rozdělit	k5eAaPmAgFnP
půl	půl	k1xP
na	na	k7c6
půl	půl	k1xP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dvou	dva	k4xCgFnPc6
poválečných	poválečný	k2eAgFnPc2d1
sparťanských	sparťanský	k2eAgFnPc2d1
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
jednak	jednak	k8xC
veteráni	veterán	k1gMnPc1
Karel	Karel	k1gMnSc1
Kolský	Kolský	k1gMnSc1
a	a	k8xC
Karel	Karel	k1gMnSc1
Senecký	Senecký	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
–	–	k?
k	k	k7c3
jejím	její	k3xOp3gInPc3
největším	veliký	k2eAgInPc3d3
hvězdám	hvězda	k1gFnPc3
patřili	patřit	k5eAaImAgMnP
útočníci	útočník	k1gMnPc1
Jan	Jan	k1gMnSc1
Říha	Říha	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Cejp	Cejp	k1gMnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Ludl	Ludl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trochu	trochu	k6eAd1
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
stínu	stín	k1gInSc6
stáli	stát	k5eAaImAgMnP
další	další	k2eAgMnPc1d1
ofenzivní	ofenzivní	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Zmatlík	Zmatlík	k1gInSc4
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Kokštejn	Kokštejn	k1gMnSc1
či	či	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Vejvoda	Vejvoda	k1gMnSc1
<g/>
,	,	kIx,
budoucí	budoucí	k2eAgFnSc1d1
trenérská	trenérský	k2eAgFnSc1d1
legenda	legenda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brankářem	brankář	k1gMnSc7
tehdejšího	tehdejší	k2eAgInSc2d1
týmu	tým	k1gInSc2
byl	být	k5eAaImAgMnS
Karel	Karel	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
klíčovým	klíčový	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
obrany	obrana	k1gFnSc2
Jiří	Jiří	k1gMnSc1
Zástěra	Zástěra	k1gMnSc1
<g/>
,	,	kIx,
zálohu	záloha	k1gFnSc4
tvořili	tvořit	k5eAaImAgMnP
Ladislav	Ladislav	k1gMnSc1
Koubek	Koubek	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Hronek	Hronek	k1gMnSc1
a	a	k8xC
Rudolf	Rudolf	k1gMnSc1
Šmejkal	Šmejkal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
50	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
obě	dva	k4xCgFnPc1
pražská	pražský	k2eAgFnSc1d1
„	„	k?
<g/>
S	s	k7c7
<g/>
“	“	k?
na	na	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
ztratila	ztratit	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
výsadní	výsadní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
v	v	k7c6
československém	československý	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
sice	sice	k8xC
nevyklidila	vyklidit	k5eNaPmAgFnS
pozice	pozice	k1gFnPc4
tolik	tolik	k6eAd1
jako	jako	k8xC,k8xS
Slavia	Slavia	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nové	nový	k2eAgFnSc3d1
konkurenci	konkurence	k1gFnSc3
čelila	čelit	k5eAaImAgFnS
obtížně	obtížně	k6eAd1
–	–	k?
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
se	se	k3xPyFc4
zformovaly	zformovat	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
nová	nový	k2eAgNnPc1d1
silná	silný	k2eAgNnPc1d1
mužstva	mužstvo	k1gNnPc1
Sokol	Sokol	k1gInSc1
NV	NV	kA
(	(	kIx(
<g/>
Slovan	Slovan	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Červená	červený	k2eAgFnSc1d1
hvezda	hvezda	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
přibyl	přibýt	k5eAaPmAgInS
silný	silný	k2eAgInSc1d1
armádní	armádní	k2eAgInSc1d1
tým	tým	k1gInSc1
ATK	ATK	kA
(	(	kIx(
<g/>
ÚDA	úd	k1gMnSc2
<g/>
,	,	kIx,
Dukla	Dukla	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
právě	právě	k6eAd1
tyto	tento	k3xDgInPc4
celky	celek	k1gInPc4
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
získaly	získat	k5eAaPmAgInP
většinu	většina	k1gFnSc4
mistrovských	mistrovský	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
končila	končit	k5eAaImAgFnS
většinou	většina	k1gFnSc7
druhá	druhý	k4xOgFnSc1
až	až	k9
třetí	třetí	k4xOgFnPc1
<g/>
,	,	kIx,
přesto	přesto	k8xC
dvakrát	dvakrát	k6eAd1
dobyla	dobýt	k5eAaPmAgFnS
ligový	ligový	k2eAgInSc4d1
trůn	trůn	k1gInSc4
–	–	k?
v	v	k7c6
letech	léto	k1gNnPc6
1952	#num#	k4
a	a	k8xC
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
mistrovských	mistrovský	k2eAgInPc6d1
týmech	tým	k1gInPc6
byli	být	k5eAaImAgMnP
ještě	ještě	k9
„	„	k?
<g/>
tři	tři	k4xCgMnPc1
veteráni	veterán	k1gMnPc1
<g/>
“	“	k?
ze	z	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Karel	Karel	k1gMnSc1
Senecký	Senecký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Vlastimil	Vlastimil	k1gMnSc1
Preis	Preis	k1gFnSc2
a	a	k8xC
Ladislav	Ladislav	k1gMnSc1
Koubek	Koubek	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k6eAd1
přišla	přijít	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
–	–	k?
v	v	k7c6
bráně	brána	k1gFnSc6
stál	stát	k5eAaImAgMnS
André	André	k1gMnSc1
Houška	Houška	k1gMnSc1
<g/>
,	,	kIx,
pilířem	pilíř	k1gInSc7
obrany	obrana	k1gFnSc2
byl	být	k5eAaImAgMnS
Miroslav	Miroslav	k1gMnSc1
Zuzánek	Zuzánek	k1gMnSc1
<g/>
,	,	kIx,
zálohu	záloha	k1gFnSc4
většinou	většinou	k6eAd1
tvořil	tvořit	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Hejský	Hejský	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Procházka	Procházka	k1gMnSc1
a	a	k8xC
Oldřich	Oldřich	k1gMnSc1
Menclík	Menclík	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
útoku	útok	k1gInSc6
nastupovali	nastupovat	k5eAaImAgMnP
Josef	Josef	k1gMnSc1
Crha	Crha	k1gMnSc1
<g/>
,	,	kIx,
Arnošt	Arnošt	k1gMnSc1
Pazdera	Pazdera	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Pešek	Pešek	k1gMnSc1
či	či	k8xC
Antonín	Antonín	k1gMnSc1
Rýgr	Rýgr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
však	však	k9
neměl	mít	k5eNaImAgInS
příliš	příliš	k6eAd1
možnost	možnost	k1gFnSc4
mezinárodního	mezinárodní	k2eAgNnSc2d1
srovnání	srovnání	k1gNnSc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
kvůli	kvůli	k7c3
uzavřenosti	uzavřenost	k1gFnSc3
tehdejšího	tehdejší	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
neexistovala	existovat	k5eNaImAgFnS
celoevropská	celoevropský	k2eAgFnSc1d1
klubová	klubový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
–	–	k?
Středoevropský	středoevropský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
plnohodnotně	plnohodnotně	k6eAd1
resuscitovat	resuscitovat	k5eAaImF
a	a	k8xC
UEFA	UEFA	kA
založila	založit	k5eAaPmAgFnS
své	svůj	k3xOyFgInPc4
poháry	pohár	k1gInPc4
až	až	k9
ve	v	k7c4
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
však	však	k9
již	již	k6eAd1
výkonnost	výkonnost	k1gFnSc1
Sparty	Sparta	k1gFnSc2
poklesla	poklesnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
přišla	přijít	k5eAaPmAgFnS
větší	veliký	k2eAgFnSc1d2
krize	krize	k1gFnSc1
a	a	k8xC
poprvé	poprvé	k6eAd1
v	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
začal	začít	k5eAaPmAgInS
reálně	reálně	k6eAd1
hrozit	hrozit	k5eAaImF
sestup	sestup	k1gInSc4
–	–	k?
v	v	k7c6
sezónách	sezóna	k1gFnPc6
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
a	a	k8xC
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
skončila	skončit	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
v	v	k7c6
lize	liga	k1gFnSc6
desátá	desátá	k1gFnSc1
a	a	k8xC
dvanáctá	dvanáctý	k4xOgFnSc1
(	(	kIx(
<g/>
ve	v	k7c6
čtrnáctičlenné	čtrnáctičlenný	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ježkova	Ježkův	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
</s>
<s>
Začátek	začátek	k1gInSc1
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byl	být	k5eAaImAgInS
nadále	nadále	k6eAd1
ve	v	k7c6
znamení	znamení	k1gNnSc6
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
Sparta	Sparta	k1gFnSc1
znovu	znovu	k6eAd1
tak	tak	k6eAd1
tak	tak	k6eAd1
uhájila	uhájit	k5eAaPmAgFnS
ligovou	ligový	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
lize	liga	k1gFnSc6
jedenáctá	jedenáctý	k4xOgFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
již	již	k6eAd1
přicházelo	přicházet	k5eAaImAgNnS
postupně	postupně	k6eAd1
zlepšení	zlepšení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
na	na	k7c6
něm	on	k3xPp3gNnSc6
měl	mít	k5eAaImAgMnS
jistě	jistě	k6eAd1
trenér	trenér	k1gMnSc1
Karel	Karel	k1gMnSc1
Kolský	Kolský	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
pak	pak	k6eAd1
zažil	zažít	k5eAaPmAgInS
slavná	slavný	k2eAgNnPc4d1
léta	léto	k1gNnPc4
na	na	k7c6
lavičce	lavička	k1gFnSc6
pražské	pražský	k2eAgFnSc2d1
Dukly	Dukla	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
zlom	zlom	k1gInSc1
nastal	nastat	k5eAaPmAgInS
zejména	zejména	k9
s	s	k7c7
příchodem	příchod	k1gInSc7
trenéra	trenér	k1gMnSc2
Václava	Václav	k1gMnSc2
Ježka	Ježek	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
později	pozdě	k6eAd2
přivedl	přivést	k5eAaPmAgInS
Československo	Československo	k1gNnSc4
k	k	k7c3
největšímu	veliký	k2eAgInSc3d3
reprezentačnímu	reprezentační	k2eAgInSc3d1
úspěchu	úspěch	k1gInSc3
v	v	k7c6
historii	historie	k1gFnSc6
–	–	k?
zlatým	zlatý	k2eAgFnPc3d1
medailím	medaile	k1gFnPc3
mistrů	mistr	k1gMnPc2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
zformoval	zformovat	k5eAaPmAgInS
tým	tým	k1gInSc4
silný	silný	k2eAgInSc4d1
i	i	k8xC
produkující	produkující	k2eAgInSc4d1
atraktivní	atraktivní	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přinesl	přinést	k5eAaPmAgMnS
Spartě	Sparta	k1gFnSc6
dva	dva	k4xCgInPc4
mistrovské	mistrovský	k2eAgInPc4d1
tituly	titul	k1gInPc4
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
staré	starý	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
50	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
v	v	k7c6
týmu	tým	k1gInSc6
zůstal	zůstat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
jediný	jediný	k2eAgMnSc1d1
Arnošt	Arnošt	k1gMnSc1
Pazdera	Pazdera	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
hlavní	hlavní	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
byl	být	k5eAaImAgInS
pilíř	pilíř	k1gInSc1
zálohy	záloha	k1gFnSc2
Andrej	Andrej	k1gMnSc1
Kvašňák	Kvašňák	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
získal	získat	k5eAaPmAgMnS
velkou	velký	k2eAgFnSc4d1
popularitu	popularita	k1gFnSc4
i	i	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
uměl	umět	k5eAaImAgMnS
tribuny	tribuna	k1gFnPc4
pobavit	pobavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
zjevem	zjev	k1gInSc7
i	i	k8xC
pojetím	pojetí	k1gNnSc7
hry	hra	k1gFnSc2
jakoby	jakoby	k8xS
na	na	k7c4
Jandu-Očka	Jandu-Očko	k1gNnPc4
navazoval	navazovat	k5eAaImAgMnS
nepřehlédnutelný	přehlédnutelný	k2eNgMnSc1d1
Tadek	Tadek	k1gMnSc1
Kraus	Kraus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bránu	brán	k2eAgFnSc4d1
hájil	hájit	k5eAaImAgMnS
mohutný	mohutný	k2eAgMnSc1d1
Antonín	Antonín	k1gMnSc1
Kramerius	Kramerius	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
náhradníkem	náhradník	k1gMnSc7
byl	být	k5eAaImAgMnS
Pavel	Pavel	k1gMnSc1
Kouba	Kouba	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
budoucí	budoucí	k2eAgFnSc2d1
brankářské	brankářský	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
klubu	klub	k1gInSc2
Petra	Petr	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obranu	obrana	k1gFnSc4
vyztužil	vyztužit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Tichý	Tichý	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
přišel	přijít	k5eAaPmAgMnS
z	z	k7c2
ČH	ČH	kA
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
pověstným	pověstný	k2eAgInSc7d1
brouskem	brousek	k1gInSc7
byl	být	k5eAaImAgMnS
Václav	Václav	k1gMnSc1
Migas	Migas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
záloze	záloha	k1gFnSc6
hru	hra	k1gFnSc4
organizoval	organizovat	k5eAaBmAgMnS
vedle	vedle	k7c2
Kvašňáka	Kvašňák	k1gMnSc2
i	i	k9
Pavel	Pavel	k1gMnSc1
Dyba	Dyba	k1gMnSc1
či	či	k8xC
Josef	Josef	k1gMnSc1
Vojta	Vojta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimořádnou	mimořádný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
měl	mít	k5eAaImAgInS
útok	útok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
křídlech	křídlo	k1gNnPc6
běhali	běhat	k5eAaImAgMnP
reprezentanti	reprezentant	k1gMnPc1
Václav	Václav	k1gMnSc1
Vrána	Vrána	k1gMnSc1
a	a	k8xC
Tomáš	Tomáš	k1gMnSc1
Pospíchal	Pospíchal	k1gMnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
i	i	k9
Bohumil	Bohumil	k1gMnSc1
Veselý	Veselý	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
miláčkem	miláček	k1gMnSc7
tribun	tribuna	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
mladý	mladý	k2eAgInSc1d1
talent	talent	k1gInSc1
Josef	Josef	k1gMnSc1
Jurkanin	Jurkanina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
útočná	útočný	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc1
(	(	kIx(
<g/>
Václav	Václav	k1gMnSc1
Mašek	Mašek	k1gMnSc1
–	–	k?
Ivan	Ivan	k1gMnSc1
Mráz	Mráz	k1gMnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
MM	mm	kA
kanonýři	kanonýr	k1gMnPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
budila	budit	k5eAaImAgFnS
u	u	k7c2
soupeřů	soupeř	k1gMnPc2
úctu	úcta	k1gFnSc4
<g/>
,	,	kIx,
až	až	k8xS
hrůzu	hrůza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
měla	mít	k5eAaImAgFnS
mezinárodní	mezinárodní	k2eAgInPc4d1
parametry	parametr	k1gInPc4
prokázala	prokázat	k5eAaPmAgNnP
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
,	,	kIx,
do	do	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
Sparta	Sparta	k1gFnSc1
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
prvně	prvně	k?
podívala	podívat	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohárová	pohárový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
Mráze	Mráz	k1gMnSc2
s	s	k7c7
Maškem	Mašek	k1gMnSc7
je	být	k5eAaImIp3nS
úctyhodná	úctyhodný	k2eAgFnSc1d1
–	–	k?
dodnes	dodnes	k6eAd1
kralují	kralovat	k5eAaImIp3nP
klubovému	klubový	k2eAgInSc3d1
žebříčku	žebříček	k1gInSc3
pohárových	pohárový	k2eAgMnPc2d1
střelců	střelec	k1gMnPc2
–	–	k?
Mráz	mráz	k1gInSc1
se	s	k7c7
14	#num#	k4
góly	gól	k1gInPc7
a	a	k8xC
Mašek	Mašek	k1gMnSc1
se	s	k7c7
13	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
gólů	gól	k1gInPc2
Anderlechtu	Anderlecht	k1gInSc2
(	(	kIx(
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
))	))	k?
Mašek	Mašek	k1gMnSc1
navíc	navíc	k6eAd1
vybojoval	vybojovat	k5eAaPmAgMnS
stříbro	stříbro	k1gNnSc4
na	na	k7c4
MS	MS	kA
v	v	k7c6
Chile	Chile	k1gNnSc6
roku	rok	k1gInSc2
1962	#num#	k4
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k9
Kvašňák	Kvašňák	k1gMnSc1
<g/>
,	,	kIx,
Tichý	Tichý	k1gMnSc1
a	a	k8xC
Pospíchal	Pospíchal	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
start	start	k1gInSc4
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
Sparta	Sparta	k1gFnSc1
zažila	zažít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Poháru	pohár	k1gInSc6
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
vypadla	vypadnout	k5eAaPmAgFnS
ve	v	k7c6
druhém	druhý	k4xOgInSc6
kole	kolo	k1gNnSc6
s	s	k7c7
West	West	k1gMnSc1
Ham	ham	k0
United	United	k1gInSc4
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
dva	dva	k4xCgInPc1
mistrovské	mistrovský	k2eAgInPc1d1
tituly	titul	k1gInPc1
ji	on	k3xPp3gFnSc4
zaručily	zaručit	k5eAaPmAgInP
možnost	možnost	k1gFnSc4
dvakrát	dvakrát	k6eAd1
si	se	k3xPyFc3
zahrát	zahrát	k5eAaPmF
i	i	k9
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
uspěla	uspět	k5eAaPmAgFnS
–	–	k?
dvakrát	dvakrát	k6eAd1
vybojovala	vybojovat	k5eAaPmAgFnS
účast	účast	k1gFnSc4
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byť	byť	k8xS
v	v	k7c6
prvním	první	k4xOgInSc6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
jaře	jaro	k1gNnSc6
1966	#num#	k4
měla	mít	k5eAaImAgFnS
k	k	k7c3
postupu	postup	k1gInSc3
do	do	k7c2
semifinále	semifinále	k1gNnSc2
opravdu	opravdu	k6eAd1
blízko	blízko	k7c2
–	–	k?
doma	doma	k6eAd1
porazila	porazit	k5eAaPmAgFnS
Partizan	Partizana	k1gFnPc2
Bělehrad	Bělehrad	k1gInSc1
vysoko	vysoko	k6eAd1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
zdálo	zdát	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
postup	postup	k1gInSc1
jí	on	k3xPp3gFnSc2
nemůže	moct	k5eNaImIp3nS
uniknout	uniknout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bělehradě	Bělehrad	k1gInSc6
však	však	k9
přišel	přijít	k5eAaPmAgMnS
neuvěřitelný	uvěřitelný	k2eNgInSc4d1
kolaps	kolaps	k1gInSc4
<g/>
,	,	kIx,
tým	tým	k1gInSc1
prohrál	prohrát	k5eAaPmAgInS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
a	a	k8xC
vypadl	vypadnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
prohra	prohra	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
pověstnou	pověstný	k2eAgFnSc7d1
a	a	k8xC
je	být	k5eAaImIp3nS
dokonce	dokonce	k9
opředena	opříst	k5eAaPmNgFnS
mnoha	mnoho	k4c7
legendami	legenda	k1gFnPc7
<g/>
,	,	kIx,
ba	ba	k9
konspiračními	konspirační	k2eAgFnPc7d1
teoriemi	teorie	k1gFnPc7
(	(	kIx(
<g/>
například	například	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
pořadatelé	pořadatel	k1gMnPc1
přimíchali	přimíchat	k5eAaPmAgMnP
Sparťanům	Sparťan	k1gMnPc3
do	do	k7c2
občerstvení	občerstvení	k1gNnSc2
uspávací	uspávací	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
před	před	k7c7
bělehradským	bělehradský	k2eAgInSc7d1
výbuchem	výbuch	k1gInSc7
vyřadila	vyřadit	k5eAaPmAgFnS
FC	FC	kA
Lausanne-Sport	Lausanne-Sport	k1gInSc4
a	a	k8xC
Górnik	Górnik	k1gInSc4
Zabrze	Zabrze	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
v	v	k7c6
Poháru	pohár	k1gInSc6
mistrů	mistr	k1gMnPc2
pak	pak	k8xC
Skeid	Skeida	k1gFnPc2
Oslo	Oslo	k1gNnSc1
a	a	k8xC
RSC	RSC	kA
Anderlecht	Anderlecht	k1gInSc1
(	(	kIx(
<g/>
ztroskotala	ztroskotat	k5eAaPmAgFnS
na	na	k7c6
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Krize	krize	k1gFnSc1
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
Sparta	Sparta	k1gFnSc1
začala	začít	k5eAaPmAgFnS
sunout	sunout	k5eAaImF
do	do	k7c2
ligového	ligový	k2eAgInSc2d1
průměru	průměr	k1gInSc2
a	a	k8xC
brzy	brzy	k6eAd1
přišla	přijít	k5eAaPmAgFnS
největší	veliký	k2eAgFnSc1d3
krize	krize	k1gFnSc1
v	v	k7c6
její	její	k3xOp3gFnSc6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedenácté	jedenáctý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
ročníku	ročník	k1gInSc6
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
bylo	být	k5eAaImAgNnS
varování	varování	k1gNnSc1
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
však	však	k9
krize	krize	k1gFnSc1
dostoupila	dostoupit	k5eAaPmAgFnS
vrcholu	vrchol	k1gInSc3
a	a	k8xC
Sparta	Sparta	k1gFnSc1
poprvé	poprvé	k6eAd1
(	(	kIx(
<g/>
a	a	k8xC
dosud	dosud	k6eAd1
naposledy	naposledy	k6eAd1
<g/>
)	)	kIx)
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
sestoupila	sestoupit	k5eAaPmAgFnS
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
15	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
rok	rok	k1gInSc4
se	se	k3xPyFc4
sice	sice	k8xC
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
vrátila	vrátit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
poté	poté	k6eAd1
hrála	hrát	k5eAaImAgFnS
povětšinou	povětšinou	k6eAd1
na	na	k7c6
chvostu	chvost	k1gInSc6
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paradoxem	paradoxon	k1gNnSc7
však	však	k9
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
v	v	k7c6
této	tento	k3xDgFnSc6
krizové	krizový	k2eAgFnSc6d1
době	doba	k1gFnSc6
zaznamenala	zaznamenat	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
možná	možná	k9
největší	veliký	k2eAgInSc4d3
mezinárodní	mezinárodní	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
své	svůj	k3xOyFgFnSc2
novodobé	novodobý	k2eAgFnSc2d1
historie	historie	k1gFnSc2
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
postoupila	postoupit	k5eAaPmAgFnS
přes	přes	k7c4
Standard	standard	k1gInSc4
Lutych	Lutycha	k1gFnPc2
<g/>
,	,	kIx,
Ferencváros	Ferencvárosa	k1gFnPc2
Budapešť	Budapešť	k1gFnSc1
a	a	k8xC
Schalke	Schalke	k1gFnSc1
04	#num#	k4
až	až	k9
do	do	k7c2
semifinále	semifinále	k1gNnSc7
druhého	druhý	k4xOgInSc2
nejprestižnějšího	prestižní	k2eAgInSc2d3
evropského	evropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
–	–	k?
Poháru	pohár	k1gInSc2
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
nestačila	stačit	k5eNaBmAgFnS
na	na	k7c4
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
sveřepé	sveřepý	k2eAgInPc4d1
mezinárodní	mezinárodní	k2eAgInPc4d1
boje	boj	k1gInPc4
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
šel	jít	k5eAaImAgInS
doma	doma	k6eAd1
od	od	k7c2
porážky	porážka	k1gFnSc2
k	k	k7c3
porážce	porážka	k1gFnSc3
<g/>
,	,	kIx,
budily	budit	k5eAaImAgFnP
údiv	údiv	k1gInSc4
<g/>
,	,	kIx,
ohlas	ohlas	k1gInSc4
i	i	k8xC
úctu	úcta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgNnSc6
semifinále	semifinále	k1gNnSc6
Sparta	Sparta	k1gFnSc1
nastoupila	nastoupit	k5eAaPmAgFnS
v	v	k7c6
sestavě	sestava	k1gFnSc6
Brabec	Brabec	k1gMnSc1
–	–	k?
Tenner	Tenner	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
Chovanec	Chovanec	k1gMnSc1
<g/>
,	,	kIx,
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
Táborský	Táborský	k1gMnSc1
–	–	k?
Bouška	Bouška	k1gMnSc1
<g/>
,	,	kIx,
Stránský	Stránský	k1gMnSc1
<g/>
,	,	kIx,
Mašek	Mašek	k1gMnSc1
–	–	k?
B.	B.	kA
Veselý	Veselý	k1gMnSc1
<g/>
,	,	kIx,
Kára	kára	k1gFnSc1
(	(	kIx(
<g/>
Hladík	Hladík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
J.	J.	kA
<g/>
Veselý	Veselý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odvetě	odveta	k1gFnSc6
Tennera	Tenner	k1gMnSc2
nahradil	nahradit	k5eAaPmAgMnS
Princ	princ	k1gMnSc1
a	a	k8xC
J.	J.	kA
Veselého	Veselého	k2eAgNnSc2d1
Kára	káro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
Vladimír	Vladimír	k1gMnSc1
Kára	káro	k1gNnSc2
s	s	k7c7
Jaroslavem	Jaroslav	k1gMnSc7
Bartoněm	Bartoň	k1gMnSc7
byli	být	k5eAaImAgMnP
klíčovými	klíčový	k2eAgMnPc7d1
střelci	střelec	k1gMnPc7
týmu	tým	k1gInSc2
–	–	k?
Bartoň	Bartoň	k1gMnSc1
dal	dát	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
Ferencvárosu	Ferencváros	k1gInSc2
i	i	k8xC
Schalke	Schalke	k1gNnSc2
<g/>
,	,	kIx,
Kára	káro	k1gNnSc2
tři	tři	k4xCgFnPc4
Lutychu	Lutych	k1gMnSc3
a	a	k8xC
jeden	jeden	k4xCgInSc1
Schalke	Schalke	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
éře	éra	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
zrodila	zrodit	k5eAaPmAgFnS
pověst	pověst	k1gFnSc1
Sparty	Sparta	k1gFnSc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
pohárového	pohárový	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
v	v	k7c6
pohárově	pohárově	k6eAd1
hraných	hraný	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
dařilo	dařit	k5eAaImAgNnS
i	i	k9
při	při	k7c6
ligové	ligový	k2eAgFnSc6d1
agonii	agonie	k1gFnSc6
–	–	k?
krom	krom	k7c2
zmíněného	zmíněný	k2eAgNnSc2d1
semifinále	semifinále	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
Spartě	Sparta	k1gFnSc6
podařilo	podařit	k5eAaPmAgNnS
proniknout	proniknout	k5eAaPmF
třeba	třeba	k6eAd1
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
Veletržního	veletržní	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
přes	přes	k7c4
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
a	a	k8xC
Dundee	Dundee	k1gNnSc1
United	United	k1gMnSc1
<g/>
)	)	kIx)
či	či	k8xC
třikrát	třikrát	k6eAd1
vyhrát	vyhrát	k5eAaPmF
Československý	československý	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
populárním	populární	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
patřil	patřit	k5eAaImAgMnS
třeba	třeba	k6eAd1
Milan	Milan	k1gMnSc1
Čermák	Čermák	k1gMnSc1
a	a	k8xC
brankář	brankář	k1gMnSc1
Jan	Jan	k1gMnSc1
Poštulka	Poštulka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Renesance	renesance	k1gFnSc1
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
:	:	kIx,
Ježek	Ježek	k1gMnSc1
znovu	znovu	k6eAd1
u	u	k7c2
kormidla	kormidlo	k1gNnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
skončila	skončit	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
v	v	k7c6
lize	liga	k1gFnSc6
ještě	ještě	k9
desátá	desátý	k4xOgFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k9
rok	rok	k1gInSc4
poté	poté	k6eAd1
přišlo	přijít	k5eAaPmAgNnS
viditelné	viditelný	k2eAgNnSc1d1
zlepšení	zlepšení	k1gNnSc1
a	a	k8xC
přesun	přesun	k1gInSc1
do	do	k7c2
horní	horní	k2eAgFnSc2d1
poloviny	polovina	k1gFnSc2
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišli	přijít	k5eAaPmAgMnP
noví	nový	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
–	–	k?
například	například	k6eAd1
velmi	velmi	k6eAd1
populární	populární	k2eAgMnSc1d1
Jaroslav	Jaroslav	k1gMnSc1
„	„	k?
<g/>
Bobby	Bobba	k1gFnSc2
<g/>
“	“	k?
Pollák	Pollák	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Slaný	Slaný	k1gInSc1
z	z	k7c2
Baníku	Baník	k1gInSc2
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Jarolím	Jarole	k1gFnPc3
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Vdovjak	Vdovjak	k1gMnSc1
<g/>
,	,	kIx,
Vlastimil	Vlastimil	k1gMnSc1
Calta	calta	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Berger	Berger	k1gMnSc1
či	či	k8xC
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definitivní	definitivní	k2eAgInSc1d1
zlom	zlom	k1gInSc1
však	však	k9
přišel	přijít	k5eAaPmAgInS
až	až	k6eAd1
s	s	k7c7
návratem	návrat	k1gInSc7
Václava	Václav	k1gMnSc2
Ježka	Ježek	k1gMnSc2
na	na	k7c4
lavičku	lavička	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ježek	Ježek	k1gMnSc1
začal	začít	k5eAaPmAgMnS
budovat	budovat	k5eAaImF
nový	nový	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
zkušených	zkušená	k1gFnPc2
Bergra	Berger	k1gMnSc2
s	s	k7c7
Chovancem	Chovanec	k1gMnSc7
rozestavěl	rozestavět	k5eAaPmAgMnS
nadějné	nadějný	k2eAgInPc4d1
mladíky	mladík	k1gInPc4
–	–	k?
Jana	Jan	k1gMnSc2
Stejskala	Stejskal	k1gMnSc2
do	do	k7c2
brány	brána	k1gFnSc2
<g/>
,	,	kIx,
Františka	František	k1gMnSc4
Straku	Straka	k1gMnSc4
a	a	k8xC
Julia	Julius	k1gMnSc4
Bielika	Bielik	k1gMnSc4
do	do	k7c2
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
Ivana	Ivan	k1gMnSc2
Haška	Hašek	k1gMnSc2
a	a	k8xC
Michala	Michal	k1gMnSc2
Bílka	Bílek	k1gMnSc2
do	do	k7c2
zálohy	záloha	k1gFnSc2
<g/>
,	,	kIx,
Stanislava	Stanislav	k1gMnSc2
Grigu	Grig	k1gInSc2
a	a	k8xC
Tomáše	Tomáš	k1gMnSc4
Skuhravého	Skuhravý	k1gMnSc4
do	do	k7c2
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vybudoval	vybudovat	k5eAaPmAgInS
(	(	kIx(
<g/>
a	a	k8xC
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
postupně	postupně	k6eAd1
doplňován	doplňovat	k5eAaImNgInS
jmény	jméno	k1gNnPc7
jako	jako	k9
Ivan	Ivan	k1gMnSc1
Čabala	Čabala	k1gFnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Vrabec	Vrabec	k1gMnSc1
či	či	k8xC
Václav	Václav	k1gMnSc1
Němeček	Němeček	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naprosto	naprosto	k6eAd1
ovládl	ovládnout	k5eAaPmAgInS
domácí	domácí	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
hegemonie	hegemonie	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
až	až	k9
absolutní	absolutní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
titul	titul	k1gInSc1
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
Sparta	Sparta	k1gFnSc1
vybojovala	vybojovat	k5eAaPmAgFnS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
a	a	k8xC
do	do	k7c2
konce	konec	k1gInSc2
dekády	dekáda	k1gFnSc2
jí	jíst	k5eAaImIp3nS
ligovou	ligový	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
dokázal	dokázat	k5eAaPmAgMnS
uzmout	uzmout	k5eAaPmF
jen	jen	k9
jediný	jediný	k2eAgInSc1d1
tým	tým	k1gInSc1
–	–	k?
TJ	tj	kA
Vítkovice	Vítkovice	k1gInPc1
(	(	kIx(
<g/>
v	v	k7c6
sezóně	sezóna	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Spartou	Sparta	k1gFnSc7
zmítaly	zmítat	k5eAaImAgInP
vnitřní	vnitřní	k2eAgInPc1d1
spory	spor	k1gInPc1
pod	pod	k7c7
kontroverzním	kontroverzní	k2eAgInSc7d1
a	a	k8xC
náročným	náročný	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Jánem	Ján	k1gMnSc7
Zacharem	Zachar	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
se	se	k3xPyFc4
Ježkovu	Ježkův	k2eAgFnSc4d1
„	„	k?
<g/>
team-worku	team-worka	k1gFnSc4
<g/>
“	“	k?
dařilo	dařit	k5eAaImAgNnS
i	i	k9
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1983	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
se	se	k3xPyFc4
Sparta	Sparta	k1gFnSc1
probojovala	probojovat	k5eAaPmAgFnS
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
i	i	k8xC
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvém	prvý	k4xOgInSc6
případě	případ	k1gInSc6
ztroskotala	ztroskotat	k5eAaPmAgFnS
na	na	k7c6
Hajduku	hajduk	k1gMnSc6
Split	Split	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
druhém	druhý	k4xOgMnSc6
na	na	k7c6
tehdy	tehdy	k6eAd1
famózním	famózní	k2eAgInSc6d1
Juventusu	Juventus	k1gInSc6
Turín	Turín	k1gInSc1
s	s	k7c7
Michelem	Michel	k1gInSc7
Platinim	Platinima	k1gFnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
však	však	k9
na	na	k7c4
tyto	tento	k3xDgInPc4
úspěchy	úspěch	k1gInPc4
navázat	navázat	k5eAaPmF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oč	oč	k6eAd1
více	hodně	k6eAd2
Sparta	Sparta	k1gFnSc1
dominovala	dominovat	k5eAaImAgFnS
doma	doma	k6eAd1
<g/>
,	,	kIx,
o	o	k7c4
to	ten	k3xDgNnSc4
více	hodně	k6eAd2
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
nedařilo	dařit	k5eNaImAgNnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
–	–	k?
potupnou	potupný	k2eAgFnSc7d1
byla	být	k5eAaImAgFnS
především	především	k9
porážka	porážka	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
doma	doma	k6eAd1
se	s	k7c7
Steauou	Steaua	k1gFnSc7
Bukurešť	Bukurešť	k1gFnSc1
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
měl	mít	k5eAaImAgMnS
rumunský	rumunský	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
tehdy	tehdy	k6eAd1
úctyhodný	úctyhodný	k2eAgInSc4d1
tým	tým	k1gInSc4
s	s	k7c7
Gheorghem	Gheorgh	k1gInSc7
Hagim	Hagima	k1gFnPc2
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
řadách	řada	k1gFnPc6
<g/>
,	,	kIx,
debakl	debakl	k1gInSc1
se	s	k7c7
Steauou	Steaua	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
symbolem	symbol	k1gInSc7
pohárové	pohárový	k2eAgFnPc4d1
mizérie	mizérie	k1gFnPc4
těch	ten	k3xDgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
Uhrinova	Uhrinův	k2eAgFnSc1d1
</s>
<s>
Po	po	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
Československo	Československo	k1gNnSc4
reprezentovalo	reprezentovat	k5eAaImAgNnS
devět	devět	k4xCc1
Sparťanů	Sparťan	k1gMnPc2
(	(	kIx(
<g/>
Stejskal	Stejskal	k1gMnSc1
<g/>
,	,	kIx,
Straka	Straka	k1gMnSc1
<g/>
,	,	kIx,
Hašek	Hašek	k1gMnSc1
<g/>
,	,	kIx,
Chovanec	Chovanec	k1gMnSc1
<g/>
,	,	kIx,
Bílek	Bílek	k1gMnSc1
<g/>
,	,	kIx,
Skuhravý	Skuhravý	k1gMnSc1
<g/>
,	,	kIx,
Němeček	Němeček	k1gMnSc1
<g/>
,	,	kIx,
Bielik	Bielik	k1gMnSc1
<g/>
,	,	kIx,
Griga	Griga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
pilíře	pilíř	k1gInPc1
letenského	letenský	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
rozprchly	rozprchnout	k5eAaPmAgFnP
do	do	k7c2
světa	svět	k1gInSc2
–	–	k?
jednak	jednak	k8xC
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
šampionát	šampionát	k1gInSc1
byl	být	k5eAaImAgInS
pro	pro	k7c4
československé	československý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
poměrně	poměrně	k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
změněných	změněný	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdálo	zdát	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
nadvládě	nadvláda	k1gFnSc3
Sparty	Sparta	k1gFnSc2
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
je	být	k5eAaImIp3nS
tak	tak	k9
konec	konec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opak	opak	k1gInSc1
byl	být	k5eAaImAgInS
pravdou	pravda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
Sparta	Sparta	k1gFnSc1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
dominovala	dominovat	k5eAaImAgFnS
domácí	domácí	k1gFnSc1
lize	liga	k1gFnSc3
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
kdy	kdy	k6eAd1
předtím	předtím	k6eAd1
<g/>
:	:	kIx,
za	za	k7c4
celou	celý	k2eAgFnSc4d1
dekádu	dekáda	k1gFnSc4
jí	on	k3xPp3gFnSc2
jen	jen	k9
dvě	dva	k4xCgNnPc1
mužstva	mužstvo	k1gNnPc1
odňala	odnít	k5eAaPmAgNnP
na	na	k7c4
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
–	–	k?
Slovan	Slovan	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
a	a	k8xC
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodů	důvod	k1gInPc2
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
sehrálo	sehrát	k5eAaPmAgNnS
jistě	jistě	k6eAd1
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
Sparta	Sparta	k1gFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c6
lavičce	lavička	k1gFnSc6
Dušana	Dušan	k1gMnSc2
Uhrina	Uhrin	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
i	i	k9
z	z	k7c2
dosud	dosud	k6eAd1
nepříliš	příliš	k6eNd1
známých	známý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
vytvořil	vytvořit	k5eAaPmAgInS
mimořádný	mimořádný	k2eAgInSc1d1
tým	tým	k1gInSc1
–	–	k?
podobně	podobně	k6eAd1
jako	jako	k9
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
podařilo	podařit	k5eAaPmAgNnS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
roku	rok	k1gInSc2
1996	#num#	k4
s	s	k7c7
českou	český	k2eAgFnSc7d1
reprezentací	reprezentace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tehdy	tehdy	k6eAd1
získala	získat	k5eAaPmAgFnS
nečekané	čekaný	k2eNgNnSc4d1
stříbro	stříbro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádro	jádro	k1gNnSc1
Sparty	Sparta	k1gFnSc2
počátku	počátek	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vytvořili	vytvořit	k5eAaPmAgMnP
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nakoukli	nakouknout	k5eAaPmAgMnP
do	do	k7c2
prvního	první	k4xOgNnSc2
mužstva	mužstvo	k1gNnSc2
již	již	k6eAd1
v	v	k7c6
minulém	minulý	k2eAgNnSc6d1
desetiletí	desetiletí	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
konkurenci	konkurence	k1gFnSc6
slavných	slavný	k2eAgMnPc2d1
reprezentantů	reprezentant	k1gMnPc2
neměli	mít	k5eNaImAgMnP
moc	moc	k6eAd1
šance	šance	k1gFnSc2
–	–	k?
k	k	k7c3
takovým	takový	k3xDgInPc3
hráčům	hráč	k1gMnPc3
patřili	patřit	k5eAaImAgMnP
tři	tři	k4xCgMnPc1
obránci	obránce	k1gMnPc1
a	a	k8xC
jeden	jeden	k4xCgMnSc1
útočník	útočník	k1gMnSc1
<g/>
:	:	kIx,
věrný	věrný	k2eAgMnSc1d1
Jiří	Jiří	k1gMnSc1
Novotný	Novotný	k1gMnSc1
(	(	kIx(
<g/>
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
stal	stát	k5eAaPmAgMnS
absolutním	absolutní	k2eAgMnSc7d1
českým	český	k2eAgMnSc7d1
rekordmanem	rekordman	k1gMnSc7
v	v	k7c6
počtu	počet	k1gInSc6
získaných	získaný	k2eAgInPc2d1
titulů	titul	k1gInPc2
–	–	k?
14	#num#	k4
a	a	k8xC
všechny	všechen	k3xTgInPc1
se	se	k3xPyFc4
Spartou	Sparta	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výborný	výborný	k2eAgMnSc1d1
exekutor	exekutor	k1gMnSc1
trestných	trestný	k2eAgInPc2d1
kopů	kop	k1gInPc2
Petr	Petr	k1gMnSc1
Vrabec	Vrabec	k1gMnSc1
<g/>
,	,	kIx,
neúnavný	únavný	k2eNgMnSc1d1
pracant	pracant	k1gMnSc1
Michal	Michal	k1gMnSc1
Horňák	Horňák	k1gMnSc1
a	a	k8xC
Horst	Horst	k1gMnSc1
Siegl	Siegl	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
kanonýrů	kanonýr	k1gMnPc2
<g/>
,	,	kIx,
jakého	jaký	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
Sparta	Sparta	k1gFnSc1
kdy	kdy	k6eAd1
měla	mít	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uhrin	Uhrin	k1gInSc1
k	k	k7c3
nim	on	k3xPp3gFnPc3
doplnil	doplnit	k5eAaPmAgMnS
nové	nový	k2eAgInPc4d1
objevy	objev	k1gInPc4
<g/>
:	:	kIx,
brankáře	brankář	k1gMnSc4
Petra	Petr	k1gMnSc4
Koubu	Kouba	k1gMnSc4
<g/>
,	,	kIx,
záložníky	záložník	k1gMnPc4
Jiřího	Jiří	k1gMnSc4
Němce	Němec	k1gMnSc4
<g/>
,	,	kIx,
Martina	Martina	k1gFnSc1
Frýdka	frýdka	k1gFnSc1
<g/>
,	,	kIx,
Romana	Roman	k1gMnSc2
Vonáška	Vonášek	k1gMnSc2
či	či	k8xC
Lumíra	Lumír	k1gMnSc2
Mistra	mistr	k1gMnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
připojili	připojit	k5eAaPmAgMnP
i	i	k9
Zdeněk	Zdeněk	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
či	či	k8xC
Tomáš	Tomáš	k1gMnSc1
Votava	Votava	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tým	tým	k1gInSc1
bez	bez	k7c2
hvězd	hvězda	k1gFnPc2
šlapal	šlapat	k5eAaImAgMnS
–	–	k?
k	k	k7c3
překvapení	překvapení	k1gNnSc3
mnohých	mnohé	k1gNnPc2
i	i	k8xC
na	na	k7c6
evropské	evropský	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
létech	léto	k1gNnPc6
půstu	půst	k1gInSc6
Sparta	Sparta	k1gFnSc1
zaznamenala	zaznamenat	k5eAaPmAgFnS
výrazné	výrazný	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
<g/>
:	:	kIx,
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
prožila	prožít	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
nejúspěšnější	úspěšný	k2eAgNnSc4d3
tažení	tažení	k1gNnSc4
Pohárem	pohár	k1gInSc7
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřadila	vyřadit	k5eAaPmAgFnS
skotský	skotský	k2eAgInSc4d1
Glasgow	Glasgow	k1gInSc4
Rangers	Rangersa	k1gFnPc2
i	i	k8xC
obhájce	obhájce	k1gMnSc1
trofeje	trofej	k1gFnSc2
Olympique	Olympique	k1gInSc1
de	de	k?
Marseille	Marseille	k1gFnSc1
s	s	k7c7
Jean-Pierrem	Jean-Pierr	k1gMnSc7
Papinem	Papin	k1gMnSc7
a	a	k8xC
ve	v	k7c6
skupině	skupina	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
prala	prát	k5eAaImAgFnS
o	o	k7c4
přímý	přímý	k2eAgInSc4d1
postup	postup	k1gInSc4
do	do	k7c2
finále	finále	k1gNnSc2
s	s	k7c7
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Dynamem	dynamo	k1gNnSc7
Kyjev	Kyjev	k1gInSc1
a	a	k8xC
Benficou	Benfica	k1gFnSc7
Lisabon	Lisabon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelonu	Barcelona	k1gFnSc4
i	i	k8xC
Kyjev	Kyjev	k1gInSc4
doma	doma	k6eAd1
porazila	porazit	k5eAaPmAgFnS
<g/>
,	,	kIx,
s	s	k7c7
Benficou	Benfica	k1gFnSc7
dvakrát	dvakrát	k6eAd1
remizovala	remizovat	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
to	ten	k3xDgNnSc1
nestačilo	stačit	k5eNaBmAgNnS
–	–	k?
ze	z	k7c2
skupiny	skupina	k1gFnSc2
postupoval	postupovat	k5eAaImAgMnS
jen	jen	k9
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
celkový	celkový	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
o	o	k7c4
zvláštní	zvláštní	k2eAgInSc4d1
model	model	k1gInSc4
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
již	již	k6eAd1
nikdy	nikdy	k6eAd1
později	pozdě	k6eAd2
neopakoval	opakovat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
ročník	ročník	k1gInSc1
prohlášen	prohlásit	k5eAaPmNgInS
za	za	k7c4
nultý	nultý	k4xOgInSc4
ročník	ročník	k1gInSc4
tzv.	tzv.	kA
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
totiž	totiž	k9
v	v	k7c6
Poháru	pohár	k1gInSc6
mistrů	mistr	k1gMnPc2
užit	užit	k2eAgInSc4d1
princip	princip	k1gInSc4
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
Sparta	Sparta	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
uspěla	uspět	k5eAaPmAgFnS
<g/>
,	,	kIx,
postoupila	postoupit	k5eAaPmAgFnS
v	v	k7c6
Poháru	pohár	k1gInSc6
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
přes	přes	k7c4
obhájce	obhájce	k1gMnSc4
trofeje	trofej	k1gFnSc2
Werder	Werder	k1gMnSc1
Brémy	Brémy	k1gFnPc4
až	až	k9
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ovšem	ovšem	k9
vypadla	vypadnout	k5eAaPmAgFnS
s	s	k7c7
AC	AC	kA
Parma	Parma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
Nedvěda	Nedvěd	k1gMnSc2
i	i	k8xC
Rosického	rosický	k2eAgMnSc2d1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
–	–	k?
sparťanská	sparťanský	k2eAgFnSc1d1
osobnost	osobnost	k1gFnSc1
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
</s>
<s>
Devadesátá	devadesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
byla	být	k5eAaImAgFnS
charakteristická	charakteristický	k2eAgFnSc1d1
také	také	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nastala	nastat	k5eAaPmAgFnS
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc1d2
fluktuace	fluktuace	k1gFnSc1
hráčů	hráč	k1gMnPc2
než	než	k8xS
bývalo	bývat	k5eAaImAgNnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
,	,	kIx,
mužstvo	mužstvo	k1gNnSc1
se	se	k3xPyFc4
obměňovalo	obměňovat	k5eAaImAgNnS
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
zejména	zejména	k9
výborní	výborný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
nevydrželi	vydržet	k5eNaPmAgMnP
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
přicházeli	přicházet	k5eAaImAgMnP
a	a	k8xC
často	často	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
legendy	legenda	k1gFnPc4
–	–	k?
patřil	patřit	k5eAaImAgInS
k	k	k7c3
nim	on	k3xPp3gInPc3
zejména	zejména	k9
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
<g/>
,	,	kIx,
budoucí	budoucí	k2eAgMnSc1d1
nejlepší	dobrý	k2eAgMnSc1d3
fotbalista	fotbalista	k1gMnSc1
kontinentu	kontinent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
Spartou	Sparta	k1gFnSc7
získal	získat	k5eAaPmAgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
tři	tři	k4xCgInPc4
tituly	titul	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
v	v	k7c6
rudém	rudý	k2eAgInSc6d1
dresu	dres	k1gInSc6
pobyli	pobýt	k5eAaPmAgMnP
gólman	gólman	k1gMnSc1
Jaromír	Jaromír	k1gMnSc1
Blažek	Blažek	k1gMnSc1
a	a	k8xC
útočník	útočník	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
Lokvenc	Lokvenc	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vytvořil	vytvořit	k5eAaPmAgInS
se	s	k7c7
Sieglem	Siegl	k1gInSc7
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejnebezpečnějších	bezpečný	k2eNgFnPc2d3
útočných	útočný	k2eAgFnPc2d1
dvojic	dvojice	k1gFnPc2
sparťanské	sparťanský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
připomínala	připomínat	k5eAaImAgFnS
dvojici	dvojice	k1gFnSc4
Mráz	mráz	k1gInSc1
<g/>
–	–	k?
<g/>
Mašek	Mašek	k1gMnSc1
–	–	k?
i	i	k8xC
bilancí	bilance	k1gFnPc2
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Siegl	Siegl	k1gMnSc1
vyrovnal	vyrovnat	k5eAaPmAgMnS,k5eAaBmAgMnS
v	v	k7c6
počtu	počet	k1gInSc6
vstřelených	vstřelený	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
branek	branka	k1gFnPc2
Maška	Mašek	k1gMnSc2
(	(	kIx(
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lokvenc	Lokvenc	k1gFnSc4
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgInS
11	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
na	na	k7c6
Letné	Letná	k1gFnSc6
objevila	objevit	k5eAaPmAgFnS
i	i	k9
další	další	k2eAgFnSc1d1
legenda	legenda	k1gFnSc1
–	–	k?
Jan	Jan	k1gMnSc1
Koller	Koller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
nezkušeným	zkušený	k2eNgMnSc7d1
začátečníkem	začátečník	k1gMnSc7
a	a	k8xC
ze	z	k7c2
Sparty	Sparta	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
zakrátko	zakrátko	k6eAd1
jako	jako	k8xC,k8xS
neperspektivní	perspektivní	k2eNgMnSc1d1
–	–	k?
až	až	k9
později	pozdě	k6eAd2
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gMnSc2
vyklubal	vyklubat	k5eAaPmAgMnS
nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
české	český	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
(	(	kIx(
<g/>
55	#num#	k4
branek	branka	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
vyhazov	vyhazov	k1gInSc1
je	být	k5eAaImIp3nS
tak	tak	k9
zřejmě	zřejmě	k6eAd1
největší	veliký	k2eAgFnSc7d3
chybou	chyba	k1gFnSc7
v	v	k7c6
dějinách	dějiny	k1gFnPc6
sparťanského	sparťanský	k2eAgInSc2d1
managementu	management	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedvědova	Nedvědův	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
sice	sice	k8xC
nadále	nadále	k6eAd1
sbírala	sbírat	k5eAaImAgFnS
domácí	domácí	k2eAgInPc4d1
tituly	titul	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
už	už	k6eAd1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
ovšem	ovšem	k9
tolik	tolik	k6eAd1
nedařilo	dařit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
výjimeční	výjimeční	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
jako	jako	k8xC,k8xS
Nedvěd	Nedvěd	k1gMnSc1
rychle	rychle	k6eAd1
odcházeli	odcházet	k5eAaImAgMnP
do	do	k7c2
zahraničních	zahraniční	k2eAgInPc2d1
klubů	klub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Přesto	přesto	k8xC
se	se	k3xPyFc4
stále	stále	k6eAd1
dařilo	dařit	k5eAaImAgNnS
mužstvo	mužstvo	k1gNnSc1
doplňovat	doplňovat	k5eAaImF
mimořádnými	mimořádný	k2eAgNnPc7d1
fotbalisty	fotbalista	k1gMnPc7
–	–	k?
na	na	k7c6
konci	konec	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
rodit	rodit	k5eAaImF
nová	nový	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
<g/>
,	,	kIx,
přišly	přijít	k5eAaPmAgFnP
nové	nový	k2eAgFnPc4d1
opory	opora	k1gFnPc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgMnPc7
nejtalentovanější	talentovaný	k2eAgMnSc1d3
byl	být	k5eAaImAgMnS
bezpochyby	bezpochyby	k6eAd1
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
podobně	podobně	k6eAd1
jako	jako	k9
Nedvěd	Nedvěd	k1gMnSc1
stihl	stihnout	k5eAaPmAgMnS
na	na	k7c6
Letné	Letná	k1gFnSc6
oslavit	oslavit	k5eAaPmF
tři	tři	k4xCgInPc4
tituly	titul	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Borussie	Borussie	k1gFnSc2
Dortmund	Dortmund	k1gInSc1
jako	jako	k8xS,k8xC
nejdražší	drahý	k2eAgMnSc1d3
český	český	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Fotbalový	fotbalový	k2eAgMnSc1d1
Mozart	Mozart	k1gMnSc1
<g/>
“	“	k?
již	již	k6eAd1
tehdy	tehdy	k6eAd1
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
kouzlil	kouzlit	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
taktovku	taktovka	k1gFnSc4
ještě	ještě	k6eAd1
drželi	držet	k5eAaImAgMnP
jiní	jiný	k1gMnPc1
–	–	k?
v	v	k7c6
obraně	obrana	k1gFnSc6
Ostraváci	Ostravák	k1gMnPc1
Tomáš	Tomáš	k1gMnSc1
Řepka	řepka	k1gFnSc1
a	a	k8xC
René	René	k1gMnSc1
Bolf	Bolf	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
Milan	Milan	k1gMnSc1
Fukal	Fukal	k1gMnSc1
či	či	k8xC
Vladimír	Vladimír	k1gMnSc1
Labant	Labant	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
záloze	záloha	k1gFnSc6
pak	pak	k6eAd1
Jiří	Jiří	k1gMnSc1
Jarošík	Jarošík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Obajdin	Obajdin	k2eAgMnSc1d1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Hašek	Hašek	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Baranek	Baranka	k1gFnPc2
a	a	k8xC
Libor	Libora	k1gFnPc2
Sionko	Sionko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
toto	tento	k3xDgNnSc1
mužstvo	mužstvo	k1gNnSc1
se	se	k3xPyFc4
jako	jako	k9
první	první	k4xOgInSc1
český	český	k2eAgInSc1d1
celek	celek	k1gInSc1
podívalo	podívat	k5eAaImAgNnS,k5eAaPmAgNnS
do	do	k7c2
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jako	jako	k9
první	první	k4xOgInSc1
český	český	k2eAgInSc1d1
celek	celek	k1gInSc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
i	i	k9
postoupilo	postoupit	k5eAaPmAgNnS
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
–	–	k?
pod	pod	k7c7
vedením	vedení	k1gNnSc7
trenéra	trenér	k1gMnSc2
Ivana	Ivan	k1gMnSc2
Haška	Hašek	k1gMnSc2
prošlo	projít	k5eAaPmAgNnS
ze	z	k7c2
skupiny	skupina	k1gFnSc2
s	s	k7c7
Girondins	Girondinsa	k1gFnPc2
Bordeaux	Bordeaux	k1gNnSc1
<g/>
,	,	kIx,
Spartakem	Spartak	k1gInSc7
Moskva	Moskva	k1gFnSc1
a	a	k8xC
Willem	Will	k1gInSc7
II	II	kA
Tilburg	Tilburg	k1gInSc1
a	a	k8xC
v	v	k7c6
osmifinálové	osmifinálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
skončilo	skončit	k5eAaPmAgNnS
třetí	třetí	k4xOgNnSc1
za	za	k7c2
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
FC	FC	kA
Porto	porto	k1gNnSc1
a	a	k8xC
před	před	k7c7
Herthou	Hertha	k1gFnSc7
Berlín	Berlín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Éra	éra	k1gFnSc1
navrátilců	navrátilec	k1gMnPc2
<g/>
:	:	kIx,
Sparta	Sparta	k1gFnSc1
Poborského	Poborský	k2eAgNnSc2d1
a	a	k8xC
Řepky	řepka	k1gFnSc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Řepka	řepka	k1gFnSc1
v	v	k7c6
předkole	předkolo	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
s	s	k7c7
Lech	Lech	k1gMnSc1
Poznań	Poznań	k1gMnSc7
roku	rok	k1gInSc2
2010	#num#	k4
</s>
<s>
Zatímco	zatímco	k8xS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Sparta	Sparta	k1gFnSc1
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
jednoznačně	jednoznačně	k6eAd1
dominovala	dominovat	k5eAaImAgFnS
<g/>
,	,	kIx,
v	v	k7c6
nultých	nultý	k4xOgNnPc6
létech	léto	k1gNnPc6
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
změnilo	změnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěchy	úspěch	k1gInPc4
v	v	k7c6
lize	liga	k1gFnSc6
sice	sice	k8xC
přicházely	přicházet	k5eAaImAgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
Spartě	Sparta	k1gFnSc3
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nepodařilo	podařit	k5eNaPmAgNnS
titul	titul	k1gInSc4
obhájit	obhájit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvakrát	dvakrát	k6eAd1
jí	on	k3xPp3gFnSc3
v	v	k7c6
dekádě	dekáda	k1gFnSc6
přehrál	přehrát	k5eAaPmAgInS
Slovan	Slovan	k1gInSc1
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
jednou	jednou	k6eAd1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
a	a	k8xC
dvakrát	dvakrát	k6eAd1
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
si	se	k3xPyFc3
v	v	k7c6
nultém	nultý	k4xOgNnSc6
desetiletí	desetiletí	k1gNnSc6
připsala	připsat	k5eAaPmAgFnS
5	#num#	k4
mistrovských	mistrovský	k2eAgInPc2d1
titulů	titul	k1gInPc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
po	po	k7c6
dvaceti	dvacet	k4xCc6
letech	léto	k1gNnPc6
hegemonie	hegemonie	k1gFnSc2
jevilo	jevit	k5eAaImAgNnS
jako	jako	k9
ústup	ústup	k1gInSc4
ze	z	k7c2
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvody	důvod	k1gInPc1
byly	být	k5eAaImAgInP
zřejmé	zřejmý	k2eAgInPc1d1
–	–	k?
zatímco	zatímco	k8xS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
nejlepší	dobrý	k2eAgMnPc1d3
čeští	český	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
Spartou	Sparta	k1gFnSc7
aspoň	aspoň	k9
prošli	projít	k5eAaPmAgMnP
<g/>
,	,	kIx,
byť	byť	k8xS
krátce	krátce	k6eAd1
<g/>
,	,	kIx,
ekonomická	ekonomický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
domácí	domácí	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
nultých	nultý	k4xOgNnPc6
letech	léto	k1gNnPc6
zhoršila	zhoršit	k5eAaPmAgFnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
výborní	výborný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
začali	začít	k5eAaPmAgMnP
odcházet	odcházet	k5eAaImF
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
již	již	k6eAd1
po	po	k7c6
pár	pár	k4xCyI
úspěšných	úspěšný	k2eAgInPc6d1
ligových	ligový	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
z	z	k7c2
dorosteneckých	dorostenecký	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezbývalo	zbývat	k5eNaImAgNnS
než	než	k8xS
jít	jít	k5eAaImF
alternativními	alternativní	k2eAgInPc7d1
cestami	cesta	k1gFnPc7
–	–	k?
dávat	dávat	k5eAaImF
větší	veliký	k2eAgFnSc4d2
šanci	šance	k1gFnSc4
talentovaným	talentovaný	k2eAgMnPc3d1
odchovancům	odchovanec	k1gMnPc3
<g/>
,	,	kIx,
hledat	hledat	k5eAaImF
velmi	velmi	k6eAd1
mladé	mladý	k2eAgInPc4d1
talenty	talent	k1gInPc4
<g/>
,	,	kIx,
motivovat	motivovat	k5eAaBmF
hráče	hráč	k1gMnPc4
středních	střední	k2eAgFnPc2d1
kvalit	kvalita	k1gFnPc2
a	a	k8xC
dát	dát	k5eAaPmF
prostor	prostor	k1gInSc4
veteránům	veterán	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
vraceli	vracet	k5eAaImAgMnP
do	do	k7c2
české	český	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
dosloužit	dosloužit	k5eAaPmF
po	po	k7c6
úspěšných	úspěšný	k2eAgFnPc6d1
kariérách	kariéra	k1gFnPc6
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
tato	tento	k3xDgFnSc1
kombinace	kombinace	k1gFnSc1
zafungovala	zafungovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
mladým	mladý	k2eAgMnPc3d1
objevům	objev	k1gInPc3
bezpochyby	bezpochyby	k6eAd1
patřili	patřit	k5eAaImAgMnP
například	například	k6eAd1
Zdeněk	Zdeněk	k1gMnSc1
Grygera	Grygero	k1gNnSc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Hübschman	Hübschman	k1gMnSc1
či	či	k8xC
Tomáš	Tomáš	k1gMnSc1
Sivok	Sivok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
mimořádné	mimořádný	k2eAgInPc1d1
talenty	talent	k1gInPc1
se	se	k3xPyFc4
neprosadily	prosadit	k5eNaPmAgInP
tolik	tolik	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
čekalo	čekat	k5eAaImAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
Spartu	Sparta	k1gFnSc4
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
udělali	udělat	k5eAaPmAgMnP
mnoho	mnoho	k6eAd1
–	–	k?
k	k	k7c3
takovým	takový	k3xDgMnPc3
patřili	patřit	k5eAaImAgMnP
například	například	k6eAd1
Tomáš	Tomáš	k1gMnSc1
Jun	jun	k1gMnSc1
či	či	k8xC
Lukáš	Lukáš	k1gMnSc1
Zelenka	Zelenka	k1gMnSc1
<g/>
,	,	kIx,
mistři	mistr	k1gMnPc1
Evropy	Evropa	k1gFnSc2
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
z	z	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
průměrní	průměrný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
pod	pod	k7c7
dobrým	dobrý	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
zazářit	zazářit	k5eAaPmF
a	a	k8xC
prožít	prožít	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
hvězdnou	hvězdný	k2eAgFnSc4d1
chvilku	chvilka	k1gFnSc4
–	–	k?
k	k	k7c3
takovým	takový	k3xDgFnPc3
lze	lze	k6eAd1
počítat	počítat	k5eAaImF
například	například	k6eAd1
Marka	Marek	k1gMnSc2
Kincla	Kincl	k1gMnSc2
či	či	k8xC
Jana	Jan	k1gMnSc2
Holendu	Holend	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
navrátivším	navrátivší	k2eAgMnPc3d1
se	se	k3xPyFc4
legendám	legenda	k1gFnPc3
pak	pak	k9
především	především	k6eAd1
a	a	k8xC
hlavně	hlavně	k9
vicemistra	vicemistr	k1gMnSc4
Evropy	Evropa	k1gFnSc2
Karla	Karel	k1gMnSc4
Poborského	Poborský	k2eAgMnSc4d1
<g/>
,	,	kIx,
českého	český	k2eAgMnSc4d1
rekordmana	rekordman	k1gMnSc4
v	v	k7c6
počtu	počet	k1gInSc6
reprezentačních	reprezentační	k2eAgInPc2d1
startů	start	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
podobný	podobný	k2eAgInSc4d1
comeback	comeback	k1gInSc4
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
Pavel	Pavel	k1gMnSc1
Horváth	Horváth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stabilitu	stabilita	k1gFnSc4
vnesl	vnést	k5eAaPmAgInS
mezi	mezi	k7c4
tři	tři	k4xCgFnPc4
tyče	tyč	k1gFnPc4
znovu	znovu	k6eAd1
Jaromír	Jaromír	k1gMnSc1
Blažek	Blažek	k1gMnSc1
<g/>
,	,	kIx,
do	do	k7c2
zadních	zadní	k2eAgFnPc2d1
řad	řada	k1gFnPc2
další	další	k2eAgMnSc1d1
navrátilec	navrátilec	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Řepka	řepka	k1gFnSc1
<g/>
,	,	kIx,
pomohl	pomoct	k5eAaPmAgInS
i	i	k9
návrat	návrat	k1gInSc1
Libora	Libor	k1gMnSc2
Sionka	Sionek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
prvého	prvý	k4xOgNnSc2
desetiletí	desetiletí	k1gNnPc4
nového	nový	k2eAgNnSc2d1
století	století	k1gNnSc2
dvakrát	dvakrát	k6eAd1
postoupila	postoupit	k5eAaPmAgFnS
ze	z	k7c2
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
trenéra	trenér	k1gMnSc2
Jaroslava	Jaroslav	k1gMnSc2
Hřebíka	Hřebík	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
šel	jít	k5eAaImAgMnS
spíše	spíše	k9
„	„	k?
<g/>
uhrinovskou	uhrinovský	k2eAgFnSc7d1
<g/>
“	“	k?
cestou	cesta	k1gFnSc7
bez	bez	k7c2
velkých	velký	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
podruhé	podruhé	k6eAd1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Jiřího	Jiří	k1gMnSc2
Kotrby	kotrba	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
postavil	postavit	k5eAaPmAgMnS
mužstvo	mužstvo	k1gNnSc4
kolem	kolem	k7c2
matadorů	matador	k1gMnPc2
Poborského	Poborský	k2eAgMnSc2d1
<g/>
,	,	kIx,
Sionka	Sionek	k1gMnSc2
a	a	k8xC
Zelenky	Zelenka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřebíkova	Hřebíkův	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
ztroskotala	ztroskotat	k5eAaPmAgFnS
v	v	k7c6
osmifinálové	osmifinálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
na	na	k7c6
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc4
a	a	k8xC
Panathinaikosu	Panathinaikosa	k1gFnSc4
Athény	Athéna	k1gFnSc2
<g/>
,	,	kIx,
Kotrbova	Kotrbův	k2eAgFnSc1d1
v	v	k7c6
osmifinále	osmifinále	k1gNnSc6
nestačila	stačit	k5eNaBmAgFnS
na	na	k7c6
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
nultých	nultý	k4xOgNnPc2
let	léto	k1gNnPc2
však	však	k9
přišel	přijít	k5eAaPmAgMnS
kvalitativní	kvalitativní	k2eAgInSc4d1
zlom	zlom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neustálé	neustálý	k2eAgInPc1d1
rozprodeje	rozprodej	k1gInPc1
hráčů	hráč	k1gMnPc2
už	už	k6eAd1
nebylo	být	k5eNaImAgNnS
vedení	vedení	k1gNnSc1
schopno	schopen	k2eAgNnSc1d1
kompenzovat	kompenzovat	k5eAaBmF
dostatečně	dostatečně	k6eAd1
kvalitními	kvalitní	k2eAgMnPc7d1
náhradníky	náhradník	k1gMnPc7
a	a	k8xC
Sparta	Sparta	k1gFnSc1
vyklidila	vyklidit	k5eAaPmAgFnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
pozice	pozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
dvakrát	dvakrát	k6eAd1
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
základní	základní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
zjevné	zjevný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
není	být	k5eNaImIp3nS
schopná	schopný	k2eAgFnSc1d1
konkurovat	konkurovat	k5eAaImF
–	–	k?
v	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
skončila	skončit	k5eAaPmAgFnS
na	na	k7c6
posledním	poslední	k2eAgMnSc6d1
čtvrtém	čtvrtý	k4xOgNnSc6
místě	místo	k1gNnSc6
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
bušila	bušit	k5eAaImAgFnS
na	na	k7c4
brány	brána	k1gFnPc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
marně	marně	k6eAd1
(	(	kIx(
<g/>
neprošla	projít	k5eNaPmAgFnS
přes	přes	k7c4
Arsenal	Arsenal	k1gFnSc4
FC	FC	kA
a	a	k8xC
dvakrát	dvakrát	k6eAd1
přes	přes	k7c4
Panathinaikos	Panathinaikos	k1gInSc4
Atény	Atény	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ba	ba	k9
ani	ani	k8xC
v	v	k7c6
Poháru	pohár	k1gInSc6
UEFA	UEFA	kA
nebyla	být	k5eNaImAgFnS
schopná	schopný	k2eAgFnSc1d1
postupovat	postupovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
desetiletí	desetiletí	k1gNnSc2
proto	proto	k8xC
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
Sparty	Sparta	k1gFnSc2
trenér	trenér	k1gMnSc1
a	a	k8xC
manažer	manažer	k1gMnSc1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
osobě	osoba	k1gFnSc3
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
začít	začít	k5eAaPmF
budovat	budovat	k5eAaImF
nové	nový	k2eAgNnSc4d1
mužstvo	mužstvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
by	by	kYmCp3nS
rostlo	růst	k5eAaImAgNnS
postupnými	postupný	k2eAgInPc7d1
kroky	krok	k1gInPc7
–	–	k?
šanci	šance	k1gFnSc4
dostali	dostat	k5eAaPmAgMnP
noví	nový	k2eAgMnPc1d1
muži	muž	k1gMnPc1
<g/>
:	:	kIx,
Juraj	Juraj	k1gMnSc1
Kucka	Kucka	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Slepička	Slepička	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Kladrubský	kladrubský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Kamil	Kamil	k1gMnSc1
Vacek	Vacek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
<g/>
,	,	kIx,
objevily	objevit	k5eAaPmAgFnP
se	se	k3xPyFc4
populární	populární	k2eAgFnPc1d1
zahraniční	zahraniční	k2eAgFnPc1d1
posily	posila	k1gFnPc1
Wilfried	Wilfried	k1gInSc4
Bony	bona	k1gFnSc2
(	(	kIx(
<g/>
11	#num#	k4
branek	branka	k1gFnPc2
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
)	)	kIx)
či	či	k8xC
Léonard	Léonard	k1gInSc4
Kweuke	Kweuk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
znakem	znak	k1gInSc7
úspěšné	úspěšný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
byl	být	k5eAaImAgInS
ročník	ročník	k1gInSc1
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Spartě	Sparta	k1gFnSc3
poprvé	poprvé	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
postoupit	postoupit	k5eAaPmF
ze	z	k7c2
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
z	z	k7c2
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
minulosti	minulost	k1gFnSc6
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
z	z	k7c2
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
o	o	k7c4
rok	rok	k1gInSc4
dříve	dříve	k6eAd2
v	v	k7c6
prvním	první	k4xOgInSc6
ročníku	ročník	k1gInSc6
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
mladé	mladý	k2eAgNnSc4d1
sparťanské	sparťanský	k2eAgNnSc4d1
mužstvo	mužstvo	k1gNnSc4
přes	přes	k7c4
nadějné	nadějný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
ještě	ještě	k9
vyhořelo	vyhořet	k5eAaPmAgNnS
v	v	k7c6
klíčovém	klíčový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
o	o	k7c4
postup	postup	k1gInSc4
s	s	k7c7
FC	FC	kA
Kodaň	Kodaň	k1gFnSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
nechalo	nechat	k5eAaPmAgNnS
FC	FC	kA
Lausanne-Sport	Lausanne-Sport	k1gInSc4
i	i	k8xC
US	US	kA
Palermo	Palermo	k1gNnSc1
<g/>
,	,	kIx,
byť	byť	k8xS
přes	přes	k7c4
FC	FC	kA
Liverpool	Liverpool	k1gInSc4
se	se	k3xPyFc4
letenským	letenský	k2eAgInPc3d1
nakonec	nakonec	k6eAd1
do	do	k7c2
osmifinále	osmifinále	k1gNnSc2
probojovat	probojovat	k5eAaPmF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
roce	rok	k1gInSc6
se	se	k3xPyFc4
však	však	k9
Sparta	Sparta	k1gFnSc1
do	do	k7c2
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
neprobojovala	probojovat	k5eNaPmAgFnS
a	a	k8xC
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
musel	muset	k5eAaImAgMnS
velitelský	velitelský	k2eAgInSc4d1
můstek	můstek	k1gInSc4
v	v	k7c6
zimě	zima	k1gFnSc6
2011	#num#	k4
opustit	opustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
trenéra	trenér	k1gMnSc2
Lavičky	Lavička	k1gMnSc2
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
se	se	k3xPyFc4
Sparta	Sparta	k1gFnSc1
úspěšně	úspěšně	k6eAd1
kvalifikovala	kvalifikovat	k5eAaBmAgFnS
do	do	k7c2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
přes	přes	k7c4
týmy	tým	k1gInPc4
Admiry	Admira	k1gFnSc2
Wacker	Wackra	k1gFnPc2
a	a	k8xC
nizozemského	nizozemský	k2eAgInSc2d1
Feyenoordu	Feyenoord	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
základní	základní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
byla	být	k5eAaImAgFnS
nalosována	nalosován	k2eAgFnSc1d1
k	k	k7c3
Olympique	Olympique	k1gFnSc3
Lyon	Lyon	k1gInSc1
<g/>
,	,	kIx,
izraelskému	izraelský	k2eAgInSc3d1
Hapoelu	Hapoel	k1gInSc3
Kirjat	Kirjat	k1gInSc4
Šmona	Šmon	k1gMnSc2
a	a	k8xC
finalistovi	finalista	k1gMnSc3
předešlého	předešlý	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
Athleticu	Athletica	k1gFnSc4
Bilbao	Bilbao	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužstvu	mužstvo	k1gNnSc3
okolo	okolo	k7c2
zkušených	zkušený	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
jako	jako	k8xC,k8xS
Marka	Marek	k1gMnSc2
Matějovského	Matějovský	k1gMnSc2
nebo	nebo	k8xC
Jiřího	Jiří	k1gMnSc2
Jarošíka	Jarošík	k1gMnSc2
doplněnému	doplněný	k2eAgMnSc3d1
o	o	k7c4
mladé	mladý	k2eAgMnPc4d1
talentované	talentovaný	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
jako	jako	k8xS,k8xC
Václav	Václav	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
nebo	nebo	k8xC
Ladislav	Ladislav	k1gMnSc1
Krejčí	Krejčí	k1gFnSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
postoupit	postoupit	k5eAaPmF
do	do	k7c2
vyřazovací	vyřazovací	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
narazilo	narazit	k5eAaPmAgNnS
na	na	k7c4
londýnskou	londýnský	k2eAgFnSc4d1
Chelsea	Chelsea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
výsledcích	výsledek	k1gInPc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
doma	doma	k6eAd1
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
venku	venek	k1gInSc2
se	se	k3xPyFc4
muselo	muset	k5eAaImAgNnS
s	s	k7c7
touto	tento	k3xDgFnSc7
soutěží	soutěž	k1gFnSc7
rozloučit	rozloučit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
zároveň	zároveň	k6eAd1
bojovala	bojovat	k5eAaImAgFnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
trenéra	trenér	k1gMnSc2
Vítězslava	Vítězslav	k1gMnSc2
Lavičky	Lavička	k1gMnSc2
o	o	k7c4
titul	titul	k1gInSc4
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
až	až	k9
do	do	k7c2
posledního	poslední	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
však	však	k9
získala	získat	k5eAaPmAgFnS
silná	silný	k2eAgFnSc1d1
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
se	se	k3xPyFc4
ACS	ACS	kA
opět	opět	k6eAd1
kvalifikovala	kvalifikovat	k5eAaBmAgNnP
do	do	k7c2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
zde	zde	k6eAd1
ale	ale	k8xC
vypadla	vypadnout	k5eAaPmAgFnS
již	již	k6eAd1
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
předkole	předkolo	k1gNnSc6
s	s	k7c7
málo	málo	k6eAd1
známým	známý	k2eAgInSc7d1
švédským	švédský	k2eAgInSc7d1
týmem	tým	k1gInSc7
BK	BK	kA
Häcken	Häcken	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Gambrinus	gambrinus	k1gInSc4
lize	liga	k1gFnSc3
během	během	k7c2
podzimní	podzimní	k2eAgFnSc2d1
části	část	k1gFnSc2
dominovala	dominovat	k5eAaImAgFnS
<g/>
,	,	kIx,
nepoznala	poznat	k5eNaPmAgFnS
hořkost	hořkost	k1gFnSc1
porážky	porážka	k1gFnSc2
a	a	k8xC
před	před	k7c4
druhou	druhý	k4xOgFnSc4
Plzní	Plzeň	k1gFnPc2
si	se	k3xPyFc3
vypracovala	vypracovat	k5eAaPmAgFnS
pětibodový	pětibodový	k2eAgInSc4d1
náskok	náskok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
získala	získat	k5eAaPmAgFnS
po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčovými	klíčový	k2eAgFnPc7d1
oporami	opora	k1gFnPc7
mistrovského	mistrovský	k2eAgInSc2d1
mužstva	mužstvo	k1gNnSc2
byli	být	k5eAaImAgMnP
Josef	Josef	k1gMnSc1
Hušbauer	Hušbauer	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Lafata	Lafata	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Kadeřábek	Kadeřábek	k1gMnSc1
<g/>
,	,	kIx,
Bořek	Bořek	k1gMnSc1
Dočkal	Dočkal	k1gMnSc1
<g/>
,	,	kIx,
Costa	Costa	k1gMnSc1
Nhamoinesu	Nhamoines	k1gInSc2
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
Matějovský	Matějovský	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Vaclík	Vaclík	k1gMnSc1
či	či	k8xC
Ladislav	Ladislav	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezoně	sezona	k1gFnSc6
získala	získat	k5eAaPmAgFnS
nakonec	nakonec	k6eAd1
double	double	k2eAgFnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
opanovala	opanovat	k5eAaPmAgFnS
i	i	k9
Pohár	pohár	k1gInSc4
České	český	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návrat	návrat	k1gInSc1
trenéra	trenér	k1gMnSc2
Ščasného	Ščasný	k2eAgMnSc2d1
</s>
<s>
Během	během	k7c2
jarní	jarní	k2eAgFnSc2d1
části	část	k1gFnSc2
sezony	sezona	k1gFnSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
byl	být	k5eAaImAgMnS
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
kvůli	kvůli	k7c3
špatným	špatný	k2eAgInPc3d1
výsledkům	výsledek	k1gInPc3
i	i	k8xC
výkonům	výkon	k1gInPc3
týmu	tým	k1gInSc3
v	v	k7c6
lize	liga	k1gFnSc6
i	i	k8xC
pohárových	pohárový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
odvolán	odvolán	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradil	nahradit	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
na	na	k7c4
pozici	pozice	k1gFnSc4
hlavního	hlavní	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
Sparty	Sparta	k1gFnSc2
vrátil	vrátit	k5eAaPmAgInS
po	po	k7c6
šestnácti	šestnáct	k4xCc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Ščasný	Ščasný	k2eAgMnSc1d1
převzal	převzít	k5eAaPmAgMnS
tým	tým	k1gInSc4
na	na	k7c6
druhé	druhý	k4xOgFnSc6
příčce	příčka	k1gFnSc6
ligové	ligový	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
také	také	k6eAd1
sezonu	sezona	k1gFnSc4
zakončil	zakončit	k5eAaPmAgMnS
a	a	k8xC
zajistil	zajistit	k5eAaPmAgMnS
tak	tak	k6eAd1
kvalifikaci	kvalifikace	k1gFnSc4
do	do	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
předkole	předkolo	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
však	však	k9
Sparta	Sparta	k1gFnSc1
nestačila	stačit	k5eNaBmAgFnS
na	na	k7c4
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
a	a	k8xC
po	po	k7c4
zbytek	zbytek	k1gInSc4
sezóny	sezóna	k1gFnSc2
hrála	hrát	k5eAaImAgFnS
Evropskou	evropský	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
české	český	k2eAgFnSc6d1
lize	liga	k1gFnSc6
Sparta	Sparta	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
ročníku	ročník	k1gInSc6
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
opět	opět	k6eAd1
druhá	druhý	k4xOgFnSc1
<g/>
,	,	kIx,
významného	významný	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
však	však	k9
dosáhla	dosáhnout	k5eAaPmAgFnS
na	na	k7c6
evropské	evropský	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
dokázala	dokázat	k5eAaPmAgNnP
postoupit	postoupit	k5eAaPmF
ze	z	k7c2
skupiny	skupina	k1gFnSc2
do	do	k7c2
jarní	jarní	k2eAgFnSc2d1
vyřazovací	vyřazovací	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
ní	on	k3xPp3gFnSc6
nejprve	nejprve	k6eAd1
vyřadila	vyřadit	k5eAaPmAgFnS
FK	FK	kA
Krasnodar	Krasnodar	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
osmifinále	osmifinále	k1gNnSc6
poté	poté	k6eAd1
SS	SS	kA
Lazio	Lazio	k1gNnSc1
a	a	k8xC
vypadla	vypadnout	k5eAaPmAgFnS
až	až	k9
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
nestačila	stačit	k5eNaBmAgFnS
na	na	k7c4
španělský	španělský	k2eAgInSc4d1
Villarreal	Villarreal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
největší	veliký	k2eAgInSc4d3
úspěch	úspěch	k1gInSc4
českého	český	k2eAgInSc2d1
klubu	klub	k1gInSc2
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
stejné	stejný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
postoupil	postoupit	k5eAaPmAgMnS
Slovan	Slovan	k1gMnSc1
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezona	sezona	k1gFnSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
ale	ale	k8xC
nezačala	začít	k5eNaPmAgFnS
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
málem	málem	k6eAd1
nepostoupila	postoupit	k5eNaPmAgFnS
do	do	k7c2
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
EL.	EL.	k1gFnSc2
Ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
předkole	předkolo	k1gNnSc6
si	se	k3xPyFc3
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
zápasu	zápas	k1gInSc2
proti	proti	k7c3
Sø	Sø	k1gFnSc3
odvezla	odvézt	k5eAaPmAgFnS
remízu	remíza	k1gFnSc4
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
domácí	domácí	k2eAgFnSc6d1
odvetě	odveta	k1gFnSc6
prohrávala	prohrávat	k5eAaImAgFnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
musela	muset	k5eAaImAgFnS
zápas	zápas	k1gInSc4
otáčet	otáčet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
zápasu	zápas	k1gInSc2
Jakub	Jakub	k1gMnSc1
Brabec	Brabec	k1gMnSc1
otočil	otočit	k5eAaPmAgMnS
na	na	k7c4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
zajistil	zajistit	k5eAaPmAgInS
Spartě	Sparta	k1gFnSc3
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
základní	základní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
EL	Ela	k1gFnPc2
ale	ale	k8xC
přišla	přijít	k5eAaPmAgFnS
porážka	porážka	k1gFnSc1
od	od	k7c2
anglického	anglický	k2eAgInSc2d1
Southamptonu	Southampton	k1gInSc2
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
a	a	k8xC
v	v	k7c6
lize	liga	k1gFnSc6
porážka	porážka	k1gFnSc1
od	od	k7c2
Slavie	slavie	k1gFnSc2
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
skončit	skončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
přechodných	přechodný	k2eAgMnPc2d1
trenérů	trenér	k1gMnPc2
</s>
<s>
Po	po	k7c6
odchodu	odchod	k1gInSc6
Ščasného	Ščasný	k2eAgInSc2d1
Sparta	Sparta	k1gFnSc1
musela	muset	k5eAaImAgFnS
ještě	ještě	k6eAd1
před	před	k7c7
začátkem	začátek	k1gInSc7
základních	základní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
najít	najít	k5eAaPmF
nového	nový	k2eAgMnSc4d1
trenéra	trenér	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
nepovedlo	povést	k5eNaPmAgNnS
a	a	k8xC
tak	tak	k6eAd1
na	na	k7c4
lavičku	lavička	k1gFnSc4
usedl	usednout	k5eAaPmAgMnS
trenér	trenér	k1gMnSc1
juniorky	juniorka	k1gFnSc2
David	David	k1gMnSc1
Holoubek	Holoubek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holoubek	Holoubek	k1gMnSc1
ale	ale	k9
neměl	mít	k5eNaImAgInS
licenci	licence	k1gFnSc4
UEFA	UEFA	kA
Pro	pro	k7c4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
trénování	trénování	k1gNnSc4
prvoligového	prvoligový	k2eAgInSc2d1
týmu	tým	k1gInSc2
potřebná	potřebný	k2eAgFnSc1d1
a	a	k8xC
tak	tak	k6eAd1
mohl	moct	k5eAaImAgInS
trénovat	trénovat	k5eAaImF
pouze	pouze	k6eAd1
po	po	k7c4
omezenou	omezený	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
Holoubkovou	Holoubkův	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
bylo	být	k5eAaImAgNnS
sestavit	sestavit	k5eAaPmF
základní	základní	k2eAgFnSc4d1
jedenáctku	jedenáctka	k1gFnSc4
pro	pro	k7c4
zápas	zápas	k1gInSc4
se	s	k7c7
slavným	slavný	k2eAgInSc7d1
Interem	Inter	k1gInSc7
<g/>
,	,	kIx,
protože	protože	k8xS
mužstvo	mužstvo	k1gNnSc1
decimovala	decimovat	k5eAaBmAgFnS
četná	četný	k2eAgNnPc4d1
zranění	zranění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
jednu	jeden	k4xCgFnSc4
dobu	doba	k1gFnSc4
bylo	být	k5eAaImAgNnS
na	na	k7c6
marodce	marodka	k1gFnSc6
až	až	k9
12	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holoubek	Holoubek	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
experimentální	experimentální	k2eAgFnSc4d1
sestavu	sestava	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
z	z	k7c2
části	část	k1gFnSc2
z	z	k7c2
původních	původní	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
části	část	k1gFnSc2
z	z	k7c2
náhradníků	náhradník	k1gMnPc2
a	a	k8xC
zbytek	zbytek	k1gInSc1
doplnil	doplnit	k5eAaPmAgInS
juniory	junior	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
nedávného	dávný	k2eNgNnSc2d1
působení	působení	k1gNnSc2
znal	znát	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
senzací	senzace	k1gFnSc7
<g/>
,	,	kIx,
Sparta	Sparta	k1gFnSc1
na	na	k7c6
Letné	Letná	k1gFnSc6
porazila	porazit	k5eAaPmAgFnS
Inter	Inter	k1gInSc4
Milán	Milán	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvěma	dva	k4xCgFnPc7
góly	gól	k1gInPc4
se	se	k3xPyFc4
na	na	k7c6
vítězství	vítězství	k1gNnSc6
podílel	podílet	k5eAaImAgMnS
Václav	Václav	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
posledním	poslední	k2eAgInSc7d1
zápasem	zápas	k1gInSc7
EL	Ela	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
Sparta	Sparta	k1gFnSc1
hrála	hrát	k5eAaImAgFnS
na	na	k7c6
stadionu	stadion	k1gInSc6
Interu	Inter	k1gInSc2
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
Holoubek	Holoubek	k1gMnSc1
kvůli	kvůli	k7c3
licenci	licence	k1gFnSc3
skončit	skončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtrénoval	odtrénovat	k5eAaPmAgMnS
14	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
10	#num#	k4
vítězně	vítězně	k6eAd1
<g/>
,	,	kIx,
2	#num#	k4
skončily	skončit	k5eAaPmAgInP
remízou	remíza	k1gFnSc7
a	a	k8xC
2	#num#	k4
porážkou	porážka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
lavičku	lavička	k1gFnSc4
byl	být	k5eAaImAgMnS
dosazen	dosazen	k2eAgMnSc1d1
dosavadní	dosavadní	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
měl	mít	k5eAaImAgMnS
mužstvo	mužstvo	k1gNnSc4
vést	vést	k5eAaImF
pouze	pouze	k6eAd1
do	do	k7c2
zimní	zimní	k2eAgFnSc2d1
přestávky	přestávka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svobodovi	Svoboda	k1gMnSc3
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
navázat	navázat	k5eAaPmF
na	na	k7c4
Holoubkovy	Holoubkův	k2eAgInPc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
s	s	k7c7
Interem	Intero	k1gNnSc7
prohrála	prohrát	k5eAaPmAgFnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
2	#num#	k4
porážky	porážka	k1gFnSc2
Sparta	Sparta	k1gFnSc1
postoupila	postoupit	k5eAaPmAgFnS
do	do	k7c2
jarní	jarní	k2eAgFnSc1d1
vyřazovací	vyřazovací	k2eAgFnSc1d1
fázi	fáze	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zimní	zimní	k2eAgFnSc6d1
pauze	pauza	k1gFnSc6
pokračovalo	pokračovat	k5eAaImAgNnS
hledání	hledání	k1gNnSc1
trenéra	trenér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
toto	tento	k3xDgNnSc1
hledání	hledání	k1gNnSc1
nebylo	být	k5eNaImAgNnS
úspěšné	úspěšný	k2eAgNnSc1d1
a	a	k8xC
trenérem	trenér	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
generální	generální	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Požár	požár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšné	úspěšný	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
Sparta	Sparta	k1gFnSc1
porazila	porazit	k5eAaPmAgFnS
slavný	slavný	k2eAgInSc4d1
Zenit	zenit	k1gInSc4
Petrohrad	Petrohrad	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
přišla	přijít	k5eAaPmAgFnS
ledová	ledový	k2eAgFnSc1d1
sprcha	sprcha	k1gFnSc1
od	od	k7c2
ruského	ruský	k2eAgInSc2d1
Rostovu	Rostův	k2eAgFnSc4d1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zopakování	zopakování	k1gNnSc1
evropské	evropský	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
bylo	být	k5eAaImAgNnS
nemožné	možný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odveta	odveta	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
remízou	remíza	k1gFnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požárovo	Požárův	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
nebylo	být	k5eNaImAgNnS
úspěšné	úspěšný	k2eAgNnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
po	po	k7c6
ligové	ligový	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
s	s	k7c7
Boleslaví	Boleslavý	k2eAgMnPc1d1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
odvolání	odvolání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledání	hledání	k1gNnSc1
trenéra	trenér	k1gMnSc2
tedy	tedy	k9
pokračovalo	pokračovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
posledních	poslední	k2eAgNnPc2d1
10	#num#	k4
zápasů	zápas	k1gInPc2
se	s	k7c7
trenérem	trenér	k1gMnSc7
stal	stát	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Rada	Rada	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
10	#num#	k4
zápasů	zápas	k1gInPc2
Sparta	Sparta	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
jediný	jediný	k2eAgInSc4d1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
s	s	k7c7
Jihlavou	Jihlava	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Remizovala	remizovat	k5eAaPmAgFnS
s	s	k7c7
úhlavním	úhlavní	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
Slavií	slavie	k1gFnPc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
porazila	porazit	k5eAaPmAgFnS
Plzeň	Plzeň	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radovi	Radův	k2eAgMnPc1d1
ale	ale	k8xC
smlouva	smlouva	k1gFnSc1
prodloužena	prodloužit	k5eAaPmNgFnS
nebyla	být	k5eNaImAgFnS
a	a	k8xC
po	po	k7c6
sezoně	sezona	k1gFnSc6
tak	tak	k6eAd1
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Stramaccioniho	Stramaccionize	k6eAd1
Sparta	Sparta	k1gFnSc1
</s>
<s>
Vedení	vedení	k1gNnSc1
nehledalo	hledat	k5eNaImAgNnS
nového	nový	k2eAgMnSc4d1
trenéra	trenér	k1gMnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
hledání	hledání	k1gNnSc2
byl	být	k5eAaImAgMnS
Ital	Ital	k1gMnSc1
Andrea	Andrea	k1gFnSc1
Stramaccioni	Stramaccioň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gInSc6
příchodu	příchod	k1gInSc6
začaly	začít	k5eAaPmAgFnP
velké	velký	k2eAgFnPc1d1
změny	změna	k1gFnPc1
v	v	k7c6
kádru	kádr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odešli	odejít	k5eAaPmAgMnP
Ondřej	Ondřej	k1gMnSc1
Mazuch	Mazuch	k1gMnSc1
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
Holek	holka	k1gFnPc2
<g/>
,	,	kIx,
Matěj	Matěj	k1gMnSc1
Hybš	Hybš	k1gMnSc1
<g/>
,	,	kIx,
Tiémoko	Tiémoko	k1gNnSc1
Konaté	Konatá	k1gFnSc2
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
Čermák	Čermák	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
Štěch	Štěch	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Jiráček	Jiráček	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Holzer	Holzer	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgFnPc1d1
posily	posila	k1gFnPc1
byly	být	k5eAaImAgFnP
pouze	pouze	k6eAd1
cizinci	cizinec	k1gMnPc7
–	–	k?
Vukadin	Vukadin	k2eAgMnSc1d1
Vukadinović	Vukadinović	k1gMnSc1
a	a	k8xC
Srđan	Srđan	k1gMnSc1
Plavšić	Plavšić	k1gMnSc1
(	(	kIx(
<g/>
Srbsko	Srbsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Eldar	Eldar	k1gMnSc1
Ćivić	Ćivić	k1gMnSc1
(	(	kIx(
<g/>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
Štetina	Štetina	k1gFnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Dúbravka	Dúbravka	k1gFnSc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Marc	Marc	k1gFnSc1
Janko	Janko	k1gMnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tal	Tal	k1gFnSc1
Ben	Ben	k1gInSc1
Chajim	Chajim	k1gInSc1
(	(	kIx(
<g/>
Izrael	Izrael	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Semih	Semih	k1gMnSc1
Kaya	Kaya	k1gMnSc1
(	(	kIx(
<g/>
Turecko	Turecko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Georges	Georges	k1gMnSc1
Mandjeck	Mandjeck	k1gMnSc1
(	(	kIx(
<g/>
Kamerun	Kamerun	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Rio	Rio	k1gMnSc1
Mavuba	Mavuba	k1gMnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posily	posít	k5eAaPmAgInP
stály	stát	k5eAaImAgInP
celkem	celek	k1gInSc7
267,3	267,3	k4
milionu	milion	k4xCgInSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Sparta	Sparta	k1gFnSc1
nerozjela	rozjet	k5eNaPmAgFnS
sezonu	sezona	k1gFnSc4
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
ostrém	ostrý	k2eAgInSc6d1
zápase	zápas	k1gInSc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
)	)	kIx)
prohrála	prohrát	k5eAaPmAgFnS
na	na	k7c6
hřišti	hřiště	k1gNnSc6
CZ	CZ	kA
Bělehrad	Bělehrad	k1gInSc4
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
odvetě	odveta	k1gFnSc6
prohrála	prohrát	k5eAaPmAgFnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
vypadla	vypadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
domácím	domácí	k2eAgInSc6d1
poháru	pohár	k1gInSc6
se	se	k3xPyFc4
Spartě	Sparta	k1gFnSc3
také	také	k9
nedařilo	dařit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
v	v	k7c6
osmifinále	osmifinále	k1gNnSc6
vypadla	vypadnout	k5eAaPmAgFnS
s	s	k7c7
Ostravou	Ostrava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
se	se	k3xPyFc4
také	také	k6eAd1
Spartě	Sparta	k1gFnSc3
nedařilo	dařit	k5eNaImAgNnS
podle	podle	k7c2
představ	představa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
podzimní	podzimní	k2eAgFnSc6d1
části	část	k1gFnSc6
byla	být	k5eAaImAgFnS
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
tabulky	tabulka	k1gFnSc2
s	s	k7c7
18	#num#	k4
bodovou	bodový	k2eAgFnSc7d1
ztrátou	ztráta	k1gFnSc7
na	na	k7c4
suverénní	suverénní	k2eAgFnSc4d1
Plzeň	Plzeň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
zimní	zimní	k2eAgFnSc2d1
přestávky	přestávka	k1gFnSc2
přišly	přijít	k5eAaPmAgFnP
další	další	k2eAgFnPc1d1
posily	posila	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rumuni	Rumun	k1gMnPc1
Nicolae	Nicolae	k1gFnPc2
Stanciu	Stancius	k1gMnSc6
z	z	k7c2
Anderlechtu	Anderlecht	k1gInSc2
a	a	k8xC
Florin	Florin	k1gInSc4
Niț	Niț	k1gFnPc2
z	z	k7c2
FCSB	FCSB	kA
<g/>
,	,	kIx,
Gaboňan	Gaboňan	k1gMnSc1
Guélor	Guélor	k1gMnSc1
Kanga	Kanga	k1gFnSc1
z	z	k7c2
Crvene	Crven	k1gInSc5
Zvezdy	Zvezda	k1gFnSc2
a	a	k8xC
na	na	k7c6
hostování	hostování	k1gNnSc6
přišel	přijít	k5eAaPmAgInS
Mihajlo	Mihajlo	k1gNnSc4
Ristić	Ristić	k1gFnSc2
z	z	k7c2
Krasnodaru	Krasnodar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
s	s	k7c7
novými	nový	k2eAgFnPc7d1
posilami	posila	k1gFnPc7
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
jarní	jarní	k2eAgFnSc2d1
části	část	k1gFnSc2
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
porazila	porazit	k5eAaPmAgFnS
Liberec	Liberec	k1gInSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
utkáních	utkání	k1gNnPc6
ale	ale	k8xC
přišly	přijít	k5eAaPmAgFnP
remízy	remíza	k1gFnPc1
se	s	k7c7
Slováckem	Slovácko	k1gNnSc7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
Brnem	Brno	k1gNnSc7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
den	den	k1gInSc4
italská	italský	k2eAgNnPc1d1
média	médium	k1gNnPc1
psala	psát	k5eAaImAgNnP
o	o	k7c6
odvolání	odvolání	k1gNnSc6
trenéra	trenér	k1gMnSc2
Stramaccioniho	Stramaccioni	k1gMnSc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andrea	Andrea	k1gFnSc1
Stramaccioni	Stramaccioň	k1gFnSc3
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
odvolán	odvolat	k5eAaPmNgInS
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
po	po	k7c4
Stramaccionim	Stramaccionim	k1gInSc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
#	#	kIx~
<g/>
Sezona	sezona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pár	pár	k4xCyI
hodin	hodina	k1gFnPc2
po	po	k7c4
odvolání	odvolání	k1gNnSc4
Stramaccioniho	Stramaccioni	k1gMnSc2
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Sparty	Sparta	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Pavel	Pavel	k1gMnSc1
Hapal	Hapal	k1gMnSc1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
utkání	utkání	k1gNnSc6
pod	pod	k7c7
Hapalem	Hapal	k1gMnSc7
Sparta	Sparta	k1gFnSc1
remizovala	remizovat	k5eAaPmAgFnS
s	s	k7c7
Karvinou	Karviná	k1gFnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
Hapalovým	Hapalův	k2eAgNnSc7d1
utkáním	utkání	k1gNnSc7
bylo	být	k5eAaImAgNnS
derby	derby	k1gNnSc1
se	s	k7c7
Slavií	slavie	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
po	po	k7c6
poločase	poločas	k1gInSc6
Sparta	Sparta	k1gFnSc1
vedla	vést	k5eAaImAgFnS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc1
ale	ale	k8xC
neudržela	udržet	k5eNaPmAgFnS
a	a	k8xC
zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezonu	sezona	k1gFnSc4
Sparta	Sparta	k1gFnSc1
zakončila	zakončit	k5eAaPmAgFnS
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
dostala	dostat	k5eAaPmAgFnS
se	se	k3xPyFc4
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
předkola	předkolo	k1gNnSc2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letní	letní	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
se	se	k3xPyFc4
Spartě	Sparta	k1gFnSc3
nedařilo	dařit	k5eNaImAgNnS
<g/>
,	,	kIx,
ze	z	k7c2
sedmi	sedm	k4xCc2
zápasů	zápas	k1gInPc2
dokázala	dokázat	k5eAaPmAgFnS
vyhrát	vyhrát	k5eAaPmF
dva	dva	k4xCgInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátek	začátek	k1gInSc1
ligy	liga	k1gFnSc2
Spartě	Sparta	k1gFnSc3
vyšel	vyjít	k5eAaPmAgInS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
porazila	porazit	k5eAaPmAgFnS
nováčka	nováček	k1gMnSc4
z	z	k7c2
Opavy	Opava	k1gFnSc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
vstup	vstup	k1gInSc4
do	do	k7c2
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
ale	ale	k8xC
nevyšel	vyjít	k5eNaPmAgMnS
a	a	k8xC
Sparta	Sparta	k1gFnSc1
senzačně	senzačně	k6eAd1
prohrála	prohrát	k5eAaPmAgFnS
se	s	k7c7
srbským	srbský	k2eAgMnSc7d1
Spartakem	Spartakus	k1gMnSc7
Subotica	Subotic	k1gInSc2
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
osudným	osudný	k2eAgInSc7d1
trenéru	trenér	k1gMnSc3
Hapalovi	Hapal	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
po	po	k7c6
utkání	utkání	k1gNnSc6
odvolán	odvolán	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
byl	být	k5eAaImAgMnS
dočasně	dočasně	k6eAd1
jmenován	jmenován	k2eAgMnSc1d1
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgMnSc1d1
sportovní	sportovní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
staronovým	staronový	k2eAgMnSc7d1
koučem	kouč	k1gMnSc7
Ščasným	Ščasný	k2eAgFnPc3d1
Sparta	Sparta	k1gFnSc1
uspěla	uspět	k5eAaPmAgFnS
v	v	k7c6
lize	liga	k1gFnSc6
v	v	k7c6
Jablonci	Jablonec	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
zápase	zápas	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
Sparta	Sparta	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
klubový	klubový	k2eAgInSc4d1
a	a	k8xC
ligový	ligový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
–	–	k?
nejmenší	malý	k2eAgInSc4d3
počet	počet	k1gInSc4
hráčů	hráč	k1gMnPc2
české	český	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
v	v	k7c6
základní	základní	k2eAgFnSc6d1
sestavě	sestava	k1gFnSc6
–	–	k?
1	#num#	k4
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odvetě	odveta	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
předkola	předkolo	k1gNnSc2
dokonce	dokonce	k9
porazila	porazit	k5eAaPmAgFnS
Suboticu	Subotica	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c4
postup	postup	k1gInSc4
nestačilo	stačit	k5eNaBmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
podruhé	podruhé	k6eAd1
v	v	k7c6
řadě	řada	k1gFnSc6
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
na	na	k7c6
prvním	první	k4xOgMnSc6
soupeři	soupeř	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
utkání	utkání	k1gNnSc2
(	(	kIx(
<g/>
když	když	k8xS
Sparta	Sparta	k1gFnSc1
po	po	k7c6
penaltě	penalta	k1gFnSc6
inkasovala	inkasovat	k5eAaBmAgFnS
<g/>
)	)	kIx)
vniklo	vniknout	k5eAaPmAgNnS
na	na	k7c4
hřiště	hřiště	k1gNnSc4
několik	několik	k4yIc1
fanoušků	fanoušek	k1gMnPc2
z	z	k7c2
„	„	k?
<g/>
kotle	kotel	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
zápas	zápas	k1gInSc1
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
na	na	k7c4
několik	několik	k4yIc4
minut	minuta	k1gFnPc2
přerušen	přerušit	k5eAaPmNgInS
<g/>
,	,	kIx,
fanoušky	fanoušek	k1gMnPc7
uklidňoval	uklidňovat	k5eAaImAgMnS
i	i	k9
kapitán	kapitán	k1gMnSc1
Josef	Josef	k1gMnSc1
Šural	Šural	k1gMnSc1
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
fanoušci	fanoušek	k1gMnPc1
byli	být	k5eAaImAgMnP
obviněni	obvinit	k5eAaPmNgMnP
z	z	k7c2
výtržnictví	výtržnictví	k1gNnSc2
a	a	k8xC
hledá	hledat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
policie	policie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
strany	strana	k1gFnSc2
Sparty	Sparta	k1gFnSc2
jim	on	k3xPp3gMnPc3
hrozí	hrozit	k5eAaImIp3nP
zákaz	zákaz	k1gInSc4
vstupu	vstup	k1gInSc2
na	na	k7c4
stadion	stadion	k1gInSc4
a	a	k8xC
peněžní	peněžní	k2eAgFnPc4d1
pokuty	pokuta	k1gFnPc4
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
Sparta	Sparta	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
s	s	k7c7
Plzní	Plzeň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgInP
další	další	k2eAgInPc1d1
dvě	dva	k4xCgFnPc1
venkovní	venkovní	k2eAgFnPc1d1
prohry	prohra	k1gFnPc1
a	a	k8xC
Sparta	Sparta	k1gFnSc1
klesla	klesnout	k5eAaPmAgFnS
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pohárovém	pohárový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
proti	proti	k7c3
diviznímu	divizní	k2eAgInSc3d1
Slavoji	Slavoj	k1gInSc3
Polná	Polná	k1gFnSc1
do	do	k7c2
hry	hra	k1gFnSc2
zasáhl	zasáhnout	k5eAaPmAgInS
a	a	k8xC
v	v	k7c6
závěru	závěr	k1gInSc6
skóroval	skórovat	k5eAaBmAgMnS
16	#num#	k4
<g/>
letý	letý	k2eAgInSc4d1
talent	talent	k1gInSc4
Adam	Adam	k1gMnSc1
Hložek	Hložek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hložek	hložek	k1gInSc1
nastoupil	nastoupit	k5eAaPmAgInS
i	i	k9
v	v	k7c6
ligovém	ligový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
s	s	k7c7
Karvinou	Karviná	k1gFnSc7
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
nejmladším	mladý	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
kdy	kdy	k6eAd1
zasáhl	zasáhnout	k5eAaPmAgMnS
do	do	k7c2
ligového	ligový	k2eAgNnSc2d1
utkání	utkání	k1gNnSc2
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
polovině	polovina	k1gFnSc6
základní	základní	k2eAgFnSc2d1
části	část	k1gFnSc2
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
se	se	k3xPyFc4
ztrátou	ztráta	k1gFnSc7
5	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
druhou	druhý	k4xOgFnSc4
Plzeň	Plzeň	k1gFnSc4
a	a	k8xC
7	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
vedoucí	vedoucí	k2eAgFnSc4d1
Slavii	slavie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovala	následovat	k5eAaImAgNnP
další	další	k2eAgNnPc1d1
nezvládnutá	zvládnutý	k2eNgNnPc1d1
utkání	utkání	k1gNnPc1
(	(	kIx(
<g/>
remíza	remíza	k1gFnSc1
s	s	k7c7
Jabloncem	Jablonec	k1gInSc7
<g/>
,	,	kIx,
prohry	prohra	k1gFnPc1
se	s	k7c7
Slováckem	Slovácko	k1gNnSc7
a	a	k8xC
s	s	k7c7
Teplicemi	Teplice	k1gFnPc7
<g/>
)	)	kIx)
a	a	k8xC
ztráta	ztráta	k1gFnSc1
na	na	k7c6
vedoucí	vedoucí	k2eAgFnSc6d1
Slavii	slavie	k1gFnSc6
se	se	k3xPyFc4
zvětšila	zvětšit	k5eAaPmAgFnS
na	na	k7c4
propastných	propastný	k2eAgInPc2d1
15	#num#	k4
bodů	bod	k1gInPc2
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
sportovním	sportovní	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
#	#	kIx~
<g/>
Sezona	sezona	k1gFnSc1
a	a	k8xC
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
#	#	kIx~
<g/>
Sezona	sezona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
na	na	k7c6
pozici	pozice	k1gFnSc6
sportovního	sportovní	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
skončil	skončit	k5eAaPmAgMnS
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
zůstala	zůstat	k5eAaPmAgFnS
pozice	pozice	k1gFnPc4
trenéra	trenér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
sportovním	sportovní	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
zimní	zimní	k2eAgFnSc2d1
přestávky	přestávka	k1gFnSc2
odešlo	odejít	k5eAaPmAgNnS
z	z	k7c2
týmu	tým	k1gInSc2
několik	několik	k4yIc1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
mj.	mj.	kA
kapitán	kapitán	k1gMnSc1
Josef	Josef	k1gMnSc1
Šural	Šural	k1gMnSc1
a	a	k8xC
i	i	k9
zástupce	zástupce	k1gMnSc4
kapitána	kapitán	k1gMnSc4
Nicolae	Nicola	k1gMnSc4
Stanciu	Stancium	k1gNnSc6
<g/>
;	;	kIx,
kapitánskou	kapitánský	k2eAgFnSc4d1
pásku	páska	k1gFnSc4
tak	tak	k6eAd1
získal	získat	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
Frýdek	frýdka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2019	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
týmu	tým	k1gInSc2
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
vrátil	vrátit	k5eAaPmAgMnS
reprezentační	reprezentační	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
Bořek	Bořek	k1gMnSc1
Dočkal	Dočkal	k1gMnSc1
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
se	s	k7c7
16	#num#	k4
<g/>
letý	letý	k2eAgMnSc1d1
Adam	Adam	k1gMnSc1
Hložek	Hložek	k1gMnSc1
postaral	postarat	k5eAaPmAgMnS
o	o	k7c4
historický	historický	k2eAgInSc4d1
zápis	zápis	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
utkání	utkání	k1gNnSc4
proti	proti	k7c3
Plzni	Plzeň	k1gFnSc3
vstřelil	vstřelit	k5eAaPmAgMnS
gól	gól	k1gInSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
nejmladším	mladý	k2eAgMnSc7d3
ligovým	ligový	k2eAgMnSc7d1
střelcem	střelec	k1gMnSc7
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pondělí	pondělí	k1gNnSc6
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Letné	Letná	k1gFnSc6
začala	začít	k5eAaPmAgFnS
šířit	šířit	k5eAaImF
smutná	smutný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
–	–	k?
při	při	k7c6
autonehodě	autonehoda	k1gFnSc6
v	v	k7c6
Turecku	Turecko	k1gNnSc6
tragicky	tragicky	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Šural	Šural	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
Spartu	Sparta	k1gFnSc4
odehrál	odehrát	k5eAaPmAgMnS
80	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
vstřelil	vstřelit	k5eAaPmAgMnS
25	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podzimní	podzimní	k2eAgFnSc6d1
části	část	k1gFnSc6
sezony	sezona	k1gFnSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
nastupoval	nastupovat	k5eAaImAgMnS
jako	jako	k9
kapitán	kapitán	k1gMnSc1
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
utkání	utkání	k1gNnSc6
byla	být	k5eAaImAgNnP
věnována	věnovat	k5eAaImNgNnP,k5eAaPmNgNnP
právě	právě	k6eAd1
jemu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
3	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
nadstavby	nadstavba	k1gFnSc2
Sparta	Sparta	k1gFnSc1
inkasovala	inkasovat	k5eAaBmAgFnS
od	od	k7c2
Plzně	Plzeň	k1gFnSc2
čtyři	čtyři	k4xCgInPc4
góly	gól	k1gInPc4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nejtěžší	těžký	k2eAgFnSc1d3
porážka	porážka	k1gFnSc1
Sparty	Sparta	k1gFnSc2
v	v	k7c6
samostatné	samostatný	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
kolo	kolo	k1gNnSc1
bylo	být	k5eAaImAgNnS
provázeno	provázet	k5eAaImNgNnS
protesty	protest	k1gInPc4
fanoušků	fanoušek	k1gMnPc2
proti	proti	k7c3
vedení	vedení	k1gNnSc3
klubu	klub	k1gInSc2
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
zakončila	zakončit	k5eAaPmAgFnS
ročník	ročník	k1gInSc4
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
lize	liga	k1gFnSc6
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
17	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
mistrovskou	mistrovský	k2eAgFnSc4d1
Slavii	slavie	k1gFnSc4
<g/>
;	;	kIx,
v	v	k7c6
poháru	pohár	k1gInSc6
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
;	;	kIx,
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
vypadla	vypadnout	k5eAaPmAgFnS
už	už	k6eAd1
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
předkole	předkolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
před	před	k7c7
sezonou	sezona	k1gFnSc7
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
převzal	převzít	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Jílek	Jílek	k1gMnSc1
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jílkovo	Jílkův	k2eAgNnSc1d1
angažmá	angažmá	k1gNnSc1
ale	ale	k9
nezačalo	začít	k5eNaPmAgNnS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
prvním	první	k4xOgInSc6
utkání	utkání	k1gNnSc6
se	s	k7c7
Slováckem	Slovácko	k1gNnSc7
Sparta	Sparta	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
zahajovala	zahajovat	k5eAaImAgFnS
tak	tak	k6eAd1
ročník	ročník	k1gInSc4
na	na	k7c6
posledním	poslední	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spartě	Sparta	k1gFnSc6
se	se	k3xPyFc4
nezdařilo	zdařit	k5eNaPmAgNnS
ani	ani	k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
třetím	třetí	k4xOgInSc6
předkole	předkolo	k1gNnSc6
neprošla	projít	k5eNaPmAgFnS
přes	přes	k7c4
Trabzonspor	Trabzonspor	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
potřetí	potřetí	k4xO
v	v	k7c6
řadě	řada	k1gFnSc6
vypadla	vypadnout	k5eAaPmAgFnS
hned	hned	k6eAd1
na	na	k7c6
prvním	první	k4xOgMnSc6
soupeři	soupeř	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
zimní	zimní	k2eAgFnSc2d1
přestávky	přestávka	k1gFnSc2
pak	pak	k6eAd1
Sparta	Sparta	k1gFnSc1
šla	jít	k5eAaImAgFnS
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
se	se	k3xPyFc4
ztrátou	ztráta	k1gFnSc7
propastných	propastný	k2eAgInPc2d1
21	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
vedoucí	vedoucí	k2eAgFnSc4d1
Slavii	slavie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
jarní	jarní	k2eAgNnSc1d1
utkání	utkání	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
porážkou	porážka	k1gFnSc7
s	s	k7c7
Libercem	Liberec	k1gInSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
následovalo	následovat	k5eAaImAgNnS
propuštění	propuštění	k1gNnSc1
trenéra	trenér	k1gMnSc2
Jílka	Jílek	k1gMnSc2
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
Václava	Václav	k1gMnSc2
Kotala	Kotal	k1gMnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
#	#	kIx~
<g/>
Sezona	sezona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Kotal	Kotal	k1gMnSc1
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Soupiska	soupiska	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
25	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2021	#num#	k4
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Florin	Florin	k1gInSc1
Niț	Niț	k1gFnSc2
</s>
<s>
3	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Čelůstka	Čelůstka	k1gFnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Dominik	Dominik	k1gMnSc1
Plechatý	plechatý	k2eAgMnSc1d1
</s>
<s>
6	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Filip	Filip	k1gMnSc1
Souček	Souček	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
David	David	k1gMnSc1
Moberg	Moberg	k1gMnSc1
Karlsson	Karlsson	k1gMnSc1
</s>
<s>
8	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
David	David	k1gMnSc1
Pavelka	Pavelka	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Bořek	Bořek	k1gMnSc1
Dočkal	dočkat	k5eAaPmAgMnS
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Martin	Martin	k1gMnSc1
Minčev	Minčev	k1gMnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
David	David	k1gMnSc1
Lischka	Lischka	k1gMnSc1
</s>
<s>
15	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Matěj	Matěj	k1gMnSc1
Hanousek	Hanousek	k1gMnSc1
</s>
<s>
16	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Michal	Michal	k1gMnSc1
Sáček	sáček	k1gInSc4
</s>
<s>
18	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Libor	Libor	k1gMnSc1
Kozák	Kozák	k1gMnSc1
</s>
<s>
19	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Štetina	Štetin	k2eAgInSc2d1
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gMnSc1
</s>
<s>
20	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Adam	Adam	k1gMnSc1
Hložek	Hložek	k1gMnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Srđan	Srđan	k1gMnSc1
Plavšić	Plavšić	k1gMnSc1
</s>
<s>
24	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Matěj	Matěj	k1gMnSc1
Polidar	Polidar	k1gMnSc1
</s>
<s>
25	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Michal	Michal	k1gMnSc1
Trávník	trávník	k1gInSc4
</s>
<s>
27	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Filip	Filip	k1gMnSc1
Panák	panák	k1gMnSc1
</s>
<s>
29	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Milan	Milan	k1gMnSc1
Heča	Heča	k1gMnSc1
</s>
<s>
32	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Andreas	Andreas	k1gMnSc1
Vindheim	Vindheim	k1gMnSc1
</s>
<s>
33	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Dávid	Dávid	k1gInSc1
Hancko	Hancko	k1gNnSc1
</s>
<s>
36	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Adam	Adam	k1gMnSc1
Karabec	Karabec	k1gMnSc1
</s>
<s>
37	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
39	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Juliš	Juliš	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Zahustel	Zahustel	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Matyáš	Matyáš	k1gMnSc1
Kozák	Kozák	k1gMnSc1
</s>
<s>
Změny	změna	k1gFnPc1
v	v	k7c6
kádru	kádr	k1gInSc6
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
přestupovém	přestupový	k2eAgNnSc6d1
období	období	k1gNnSc6
2021	#num#	k4
</s>
<s>
Příchody	příchod	k1gInPc1
</s>
<s>
Poz	Poz	k?
<g/>
.	.	kIx.
</s>
<s>
Nár	Nár	k?
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
Odkud	odkud	k6eAd1
přišel	přijít	k5eAaPmAgMnS
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
ZJan	ZJan	k1gInSc1
Fortelný	Fortelný	k?
<g/>
21	#num#	k4
FK	FK	kA
Teplicenávrat	Teplicenávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
OOndřej	OOndřát	k5eAaPmRp2nS,k5eAaImRp2nS
Zahustel	Zahustel	k1gInSc4
<g/>
28	#num#	k4
FK	FK	kA
Mladá	mladá	k1gFnSc1
Boleslavnávrat	Boleslavnávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
OUroš	OUroš	k1gMnSc1
Radaković	Radaković	k1gMnSc1
<g/>
26	#num#	k4
FC	FC	kA
Astananávrat	Astananávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
ÚMatyáš	ÚMatyat	k5eAaBmIp2nS,k5eAaImIp2nS,k5eAaPmIp2nS
Kozák	kozák	k1gInSc4
<g/>
19	#num#	k4
FK	FK	kA
Teplicenávrat	Teplicenávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
ZJiří	ZJiří	k2eAgMnSc1d1
Kulhánek	Kulhánek	k1gMnSc1
<g/>
24	#num#	k4
FK	FK	kA
Mladá	mladá	k1gFnSc1
Boleslavnávrat	Boleslavnávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
OLukáš	OLukat	k5eAaPmIp2nS,k5eAaImIp2nS,k5eAaBmIp2nS
Hušek	Hušek	k1gMnSc1
<g/>
20	#num#	k4
FK	FK	kA
Pardubicenávrat	Pardubicenávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
Odchody	odchod	k1gInPc4
</s>
<s>
Poz	Poz	k?
<g/>
.	.	kIx.
</s>
<s>
Nár	Nár	k?
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
Kam	kam	k6eAd1
odešel	odejít	k5eAaPmAgMnS
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
ZJiří	ZJiří	k2eAgMnSc1d1
Kulhánek	Kulhánek	k1gMnSc1
<g/>
24	#num#	k4
SFC	SFC	kA
Opavahostování	Opavahostování	k1gNnSc2
</s>
<s>
BJan	BJan	k1gInSc1
Čtvrtečka	čtvrtečka	k1gFnSc1
<g/>
22	#num#	k4
FK	FK	kA
Teplicehostování	Teplicehostování	k1gNnSc2
</s>
<s>
BDominik	BDominik	k1gMnSc1
Holec	holec	k1gMnSc1
<g/>
26	#num#	k4
Raków	Raków	k1gFnSc2
Częstochowahostování	Częstochowahostování	k1gNnSc2
</s>
<s>
OLukáš	OLukat	k5eAaBmIp2nS,k5eAaImIp2nS,k5eAaPmIp2nS
Hušek	Hušek	k1gMnSc1
<g/>
20	#num#	k4
FK	FK	kA
Viktoria	Viktoria	k1gFnSc1
Žižkovhostování	Žižkovhostování	k1gNnSc6
</s>
<s>
ZJan	ZJan	k1gInSc1
Fortelný	Fortelný	k?
<g/>
22	#num#	k4
FK	FK	kA
Teplicepokračující	Teplicepokračující	k2eAgNnSc1d1
hostování	hostování	k1gNnSc1
</s>
<s>
OUroš	OUroš	k1gMnSc1
Radaković	Radaković	k1gFnSc2
<g/>
26	#num#	k4
Wisla	Wislo	k1gNnSc2
Krakovhostování	Krakovhostování	k1gNnSc2
</s>
<s>
Sparta	Sparta	k1gFnSc1
v	v	k7c6
popkultuře	popkultuře	k?
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
stává	stávat	k5eAaImIp3nS
i	i	k9
tématem	téma	k1gNnSc7
pop-kulturních	pop-kulturní	k2eAgInPc2d1
obsahů	obsah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
skladatel	skladatel	k1gMnSc1
Karel	Karla	k1gFnPc2
Baling	Baling	k1gInSc4
písničku	písnička	k1gFnSc4
Dneska	Dneska	k?
hraje	hrát	k5eAaImIp3nS
Káďa	Káďa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pojednávala	pojednávat	k5eAaImAgFnS
o	o	k7c6
populárním	populární	k2eAgMnSc6d1
sparťanském	sparťanský	k2eAgMnSc6d1
středopolaři	středopolař	k1gMnSc6
Karlu	Karel	k1gMnSc6
Peškovi	Pešek	k1gMnSc6
zvanému	zvaný	k2eAgInSc3d1
Káďa	Káď	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hitem	hit	k1gInSc7
v	v	k7c6
kabaretu	kabaret	k1gInSc6
Rokoko	rokoko	k1gNnSc1
Karla	Karel	k1gMnSc2
Hašlera	Hašler	k1gMnSc2
<g/>
,	,	kIx,
zpívalo	zpívat	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
„	„	k?
<g/>
Dneska	Dneska	k?
hraje	hrát	k5eAaImIp3nS
Káďa	Káď	k2eAgFnSc1d1
<g/>
,	,	kIx,
zlatovlasá	zlatovlasý	k2eAgFnSc1d1
primadona	primadona	k1gFnSc1
<g/>
,	,	kIx,
dneska	dneska	k?
hraje	hrát	k5eAaImIp3nS
Káďa	Káďa	k1gMnSc1
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
tají	tajit	k5eAaImIp3nS
dech	dech	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
když	když	k8xS
kopne	kopnout	k5eAaPmIp3nS
do	do	k7c2
meruny	meruna	k1gFnSc2
<g/>
,	,	kIx,
rozehraje	rozehrát	k5eAaPmIp3nS
srdce	srdce	k1gNnSc1
struny	struna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dneska	Dneska	k?
hraje	hrát	k5eAaImIp3nS
Káďa	Káďa	k1gFnSc1
<g/>
,	,	kIx,
chlouba	chlouba	k1gFnSc1
celých	celý	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Roku	rok	k1gInSc2
1936	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
filmová	filmový	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
Naše	náš	k3xOp1gFnSc1
jedenáctka	jedenáctka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
režíroval	režírovat	k5eAaImAgMnS
Václav	Václav	k1gMnSc1
Binovec	Binovec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenoknihovní	Červenoknihovní	k2eAgFnSc1d1
zápletka	zápletka	k1gFnSc1
je	být	k5eAaImIp3nS
implantována	implantovat	k5eAaBmNgFnS
do	do	k7c2
fotbalového	fotbalový	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
šéf	šéf	k1gMnSc1
klubu	klub	k1gInSc2
AC	AC	kA
Sparta	Sparta	k1gFnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
přetáhnout	přetáhnout	k5eAaPmF
do	do	k7c2
Prahy	Praha	k1gFnSc2
nadějného	nadějný	k2eAgMnSc2d1
útočníka	útočník	k1gMnSc2
SK	Sk	kA
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
podání	podání	k1gNnSc6
Antonína	Antonín	k1gMnSc4
Novotného	Novotný	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
snaží	snažit	k5eAaImIp3nP
se	se	k3xPyFc4
k	k	k7c3
tomu	ten	k3xDgMnSc3
využít	využít	k5eAaPmF
i	i	k9
svou	svůj	k3xOyFgFnSc4
půvabnou	půvabný	k2eAgFnSc4d1
dceru	dcera	k1gFnSc4
(	(	kIx(
<g/>
Nataša	Nataša	k1gFnSc1
Gollová	Gollová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmu	film	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
autentické	autentický	k2eAgInPc1d1
záběry	záběr	k1gInPc1
z	z	k7c2
finálového	finálový	k2eAgNnSc2d1
utkání	utkání	k1gNnSc2
Středoevropského	středoevropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
Sparta	Sparta	k1gFnSc1
–	–	k?
Ferencváros	Ferencvárosa	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1935	#num#	k4
a	a	k8xC
i	i	k9
v	v	k7c6
hraných	hraný	k2eAgFnPc6d1
scénách	scéna	k1gFnPc6
vystupují	vystupovat	k5eAaImIp3nP
tehdejší	tehdejší	k2eAgMnPc1d1
reální	reální	k2eAgMnPc1d1
fotbalisté	fotbalista	k1gMnPc1
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
Bohumil	Bohumil	k1gMnSc1
Klenovec	Klenovec	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Burgr	Burgr	k1gMnSc1
<g/>
,	,	kIx,
Ferdinand	Ferdinand	k1gMnSc1
Faczinek	Faczinek	k1gInSc1
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
Srbek	Srbka	k1gFnPc2
či	či	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Bouček	Bouček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferenc	Ferenc	k1gMnSc1
Futurista	futurista	k1gMnSc1
hraje	hrát	k5eAaImIp3nS
ve	v	k7c6
snímku	snímek	k1gInSc6
fanatického	fanatický	k2eAgMnSc2d1
sparťanského	sparťanský	k2eAgMnSc2d1
fanouška	fanoušek	k1gMnSc2
<g/>
,	,	kIx,
kancelářského	kancelářský	k1gMnSc2
sluhu	sluha	k1gMnSc4
Krátkého	Krátký	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmu	film	k1gInSc6
Muži	muž	k1gMnPc1
v	v	k7c6
offsidu	offsid	k1gInSc6
si	se	k3xPyFc3
podobně	podobně	k6eAd1
zavilého	zavilý	k2eAgMnSc2d1
fanouška	fanoušek	k1gMnSc2
letenských	letenský	k2eAgMnPc2d1
zase	zase	k9
zahrál	zahrát	k5eAaPmAgMnS
Jaroslav	Jaroslav	k1gMnSc1
Vojta	Vojta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1938	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
film	film	k1gInSc1
Klapzubova	Klapzubův	k2eAgFnSc1d1
jedenáctka	jedenáctka	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
motivy	motiv	k1gInPc4
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
předlohy	předloha	k1gFnSc2
Eduarda	Eduard	k1gMnSc2
Basse	Bass	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klapzubovo	Klapzubův	k2eAgNnSc1d1
mužstvo	mužstvo	k1gNnSc1
(	(	kIx(
<g/>
navzdory	navzdory	k7c3
předloze	předloha	k1gFnSc3
<g/>
)	)	kIx)
na	na	k7c6
začátku	začátek	k1gInSc6
filmu	film	k1gInSc2
dostává	dostávat	k5eAaImIp3nS
dresy	dres	k1gInPc4
různých	různý	k2eAgInPc2d1
českých	český	k2eAgInPc2d1
týmů	tým	k1gInPc2
–	–	k?
ten	ten	k3xDgInSc4
sparťanský	sparťanský	k2eAgInSc4d1
prý	prý	k9
měl	mít	k5eAaImAgInS
patřit	patřit	k5eAaImF
Václavu	Václav	k1gMnSc3
Pilátovi	Pilát	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
snímku	snímek	k1gInSc6
Cesta	cesta	k1gFnSc1
do	do	k7c2
hlubin	hlubina	k1gFnPc2
študákovy	študákův	k2eAgFnSc2d1
duše	duše	k1gFnSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
bitce	bitka	k1gFnSc3
mezi	mezi	k7c7
studenty	student	k1gMnPc7
Peterkou	Peterka	k1gMnSc7
(	(	kIx(
<g/>
R.	R.	kA
A.	A.	kA
Strejka	Strejka	k?
<g/>
)	)	kIx)
a	a	k8xC
Kulíkem	kulík	k1gMnSc7
(	(	kIx(
<g/>
Ladislav	Ladislav	k1gMnSc1
Pešek	Pešek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
Peterka	Peterka	k1gMnSc1
fandí	fandit	k5eAaImIp3nS
„	„	k?
<g/>
proletářskému	proletářský	k2eAgInSc3d1
klubu	klub	k1gInSc2
<g/>
“	“	k?
Spartě	Sparta	k1gFnSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Kulík	kulík	k1gMnSc1
Slavii	slavie	k1gFnSc4
(	(	kIx(
<g/>
Slavia	Slavia	k1gFnSc1
vyrostla	vyrůst	k5eAaPmAgFnS
ze	z	k7c2
studentského	studentský	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
Sparta	Sparta	k1gFnSc1
z	z	k7c2
dělnického	dělnický	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Proslulým	proslulý	k2eAgMnSc7d1
filmovým	filmový	k2eAgMnSc7d1
fanouškem	fanoušek	k1gMnSc7
Sparty	Sparta	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
také	také	k9
táta	táta	k1gMnSc1
Homolka	Homolka	k1gMnSc1
v	v	k7c6
komediích	komedie	k1gFnPc6
Jaroslava	Jaroslav	k1gMnSc2
Papouška	Papoušek	k1gMnSc2
Ecce	Ecc	k1gMnSc2
homo	homo	k1gMnSc1
Homolka	Homolka	k1gMnSc1
a	a	k8xC
Hogo	Hogo	k1gMnSc1
fogo	fogo	k1gMnSc1
Homolka	Homolka	k1gMnSc1
v	v	k7c6
podání	podání	k1gNnSc6
Josefa	Josef	k1gMnSc2
Šebánka	Šebánek	k1gMnSc2
z	z	k7c2
konce	konec	k1gInSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c4
jehož	jehož	k3xOyRp3gInSc4
portrét	portrét	k1gInSc4
lze	lze	k6eAd1
v	v	k7c6
hledišti	hlediště	k1gNnSc6
narazit	narazit	k5eAaPmF
prakticky	prakticky	k6eAd1
na	na	k7c6
každém	každý	k3xTgInSc6
domácím	domácí	k2eAgInSc6d1
zápasu	zápas	k1gInSc6
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
filmu	film	k1gInSc6
táta	táta	k1gMnSc1
Homolka	Homolka	k1gMnSc1
nadšeně	nadšeně	k6eAd1
líčí	líčit	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
zážitek	zážitek	k1gInSc4
ze	z	k7c2
zápasu	zápas	k1gInSc2
Sparty	Sparta	k1gFnSc2
a	a	k8xC
zmiňuje	zmiňovat	k5eAaImIp3nS
některé	některý	k3yIgMnPc4
hráče	hráč	k1gMnPc4
tehdejší	tehdejší	k2eAgFnSc2d1
doby	doba	k1gFnSc2
–	–	k?
Andreje	Andrej	k1gMnSc4
Kvašňáka	Kvašňák	k1gMnSc4
<g/>
,	,	kIx,
Václava	Václav	k1gMnSc4
Vránu	Vrána	k1gMnSc4
či	či	k8xC
tehdy	tehdy	k6eAd1
populárního	populární	k2eAgMnSc2d1
Josefa	Josef	k1gMnSc2
Jurkanina	Jurkanin	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
výroky	výrok	k1gInPc1
přitom	přitom	k6eAd1
zlidověly	zlidovět	k5eAaPmAgInP
(	(	kIx(
<g/>
„	„	k?
<g/>
Sparta	Sparta	k1gFnSc1
má	mít	k5eAaImIp3nS
takovej	takovej	k?
útok	útok	k1gInSc1
teďka	teďka	k?
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
rozkopala	rozkopat	k5eAaPmAgFnS
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
snímku	snímek	k1gInSc6
táta	táta	k1gMnSc1
Homolka	Homolka	k1gMnSc1
bojuje	bojovat	k5eAaImIp3nS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
jet	jet	k2eAgInSc1d1
„	„	k?
<g/>
se	s	k7c7
Spartou	Sparta	k1gFnSc7
do	do	k7c2
Teplic	Teplice	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
však	však	k9
místo	místo	k7c2
toho	ten	k3xDgNnSc2
jet	jet	k5eAaImF
za	za	k7c4
údajně	údajně	k6eAd1
umírajícím	umírající	k2eAgMnSc7d1
tchánem	tchán	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papoušek	Papoušek	k1gMnSc1
využil	využít	k5eAaPmAgMnS
fanouškovství	fanouškovství	k?
jako	jako	k8xC,k8xS
symbolu	symbol	k1gInSc3
maloměšťáctví	maloměšťáctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Andrej	Andrej	k1gMnSc1
Kvašňák	Kvašňák	k1gMnSc1
byl	být	k5eAaImAgMnS
populární	populární	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
ještě	ještě	k6eAd1
dlouho	dlouho	k6eAd1
po	po	k7c6
konci	konec	k1gInSc6
své	svůj	k3xOyFgFnSc2
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
dokladem	doklad	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1998	#num#	k4
ve	v	k7c6
filmu	film	k1gInSc6
Oskara	Oskar	k1gMnSc2
Reifa	Reif	k1gMnSc2
Postel	postel	k1gFnSc1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
symbol	symbol	k1gInSc1
mužství	mužství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Stadión	stadión	k1gInSc1
na	na	k7c6
Letné	Letná	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
dějištěm	dějiště	k1gNnSc7
závěrečné	závěrečný	k2eAgFnSc2d1
přestřelky	přestřelka	k1gFnSc2
v	v	k7c6
krimi	krimi	k1gNnSc6
snímku	snímek	k1gInSc2
Past	pasta	k1gFnPc2
na	na	k7c4
kachnu	kachna	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
zde	zde	k6eAd1
natáčel	natáčet	k5eAaImAgMnS
i	i	k9
jeden	jeden	k4xCgInSc4
díl	díl	k1gInSc4
populárního	populární	k2eAgInSc2d1
televizního	televizní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
Dva	dva	k4xCgInPc4
z	z	k7c2
jednoho	jeden	k4xCgNnSc2
města	město	k1gNnSc2
–	–	k?
v	v	k7c6
hledišti	hlediště	k1gNnSc6
bylo	být	k5eAaImAgNnS
tehdy	tehdy	k6eAd1
20	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spíše	spíše	k9
epizodní	epizodní	k2eAgFnSc4d1
roli	role	k1gFnSc4
pak	pak	k6eAd1
měl	mít	k5eAaImAgMnS
stadion	stadion	k1gInSc4
i	i	k9
v	v	k7c6
pilotním	pilotní	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
seriálu	seriál	k1gInSc2
Cirkus	cirkus	k1gInSc1
Bukowsky	Bukowska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Problematickými	problematický	k2eAgInPc7d1
jevy	jev	k1gInPc7
spojenými	spojený	k2eAgInPc7d1
s	s	k7c7
fotbalovým	fotbalový	k2eAgInSc7d1
fanouškovstvím	fanouškovstvím	k?
se	se	k3xPyFc4
věnovaly	věnovat	k5eAaImAgInP,k5eAaPmAgInP
snímky	snímek	k1gInPc1
Proč	proč	k6eAd1
(	(	kIx(
<g/>
inspirován	inspirovat	k5eAaBmNgInS
skutečnými	skutečný	k2eAgFnPc7d1
výtržnostmi	výtržnost	k1gFnPc7
sparťanských	sparťanský	k2eAgMnPc2d1
fanoušků	fanoušek	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Horem	horem	k6eAd1
pádem	pád	k1gInSc7
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jan	Jan	k1gMnSc1
Hřebejk	Hřebejk	k1gMnSc1
<g/>
)	)	kIx)
či	či	k8xC
Non	Non	k1gMnSc1
plus	plus	k1gNnSc1
ultras	ultras	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jakub	Jakub	k1gMnSc1
Sluka	Sluka	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2009	#num#	k4
televize	televize	k1gFnSc1
Prima	prima	k1gFnSc1
natočila	natočit	k5eAaBmAgFnS
komediální	komediální	k2eAgInSc4d1
seriál	seriál	k1gInSc4
Přešlapy	přešlap	k1gInPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
tři	tři	k4xCgMnPc1
hlavní	hlavní	k2eAgMnPc1d1
mužští	mužský	k2eAgMnPc1d1
hrdinové	hrdina	k1gMnPc1
(	(	kIx(
<g/>
v	v	k7c6
podání	podání	k1gNnSc6
Filipa	Filip	k1gMnSc2
Blažka	Blažek	k1gMnSc2
<g/>
,	,	kIx,
Davida	David	k1gMnSc2
Novotného	Novotný	k1gMnSc2
a	a	k8xC
Davida	David	k1gMnSc2
Matáska	Matásek	k1gMnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
fanoušky	fanoušek	k1gMnPc4
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
David	David	k1gMnSc1
Novotný	Novotný	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
osobním	osobní	k2eAgInSc6d1
životě	život	k1gInSc6
velký	velký	k2eAgMnSc1d1
fanoušek	fanoušek	k1gMnSc1
Sparty	Sparta	k1gFnSc2
a	a	k8xC
čestný	čestný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
fanouškovského	fanouškovský	k2eAgNnSc2d1
občanského	občanský	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
„	„	k?
<g/>
DVANÁCTÝM	dvanáctý	k4xOgFnPc3
HRÁČEM	hráč	k1gMnSc7
JSME	být	k5eAaImIp1nP
MY	my	k3xPp1nPc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
v	v	k7c6
seriálu	seriál	k1gInSc6
komentuje	komentovat	k5eAaBmIp3nS
i	i	k9
dění	dění	k1gNnSc4
ve	v	k7c6
fotbalovém	fotbalový	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
,	,	kIx,
zmiňuje	zmiňovat	k5eAaImIp3nS
například	například	k6eAd1
útočníka	útočník	k1gMnSc2
Václava	Václav	k1gMnSc2
Kadlece	Kadlec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novotný	Novotný	k1gMnSc1
hrál	hrát	k5eAaImAgMnS
fanouška	fanoušek	k1gMnSc4
Sparty	Sparta	k1gFnSc2
i	i	k9
v	v	k7c6
seriálech	seriál	k1gInPc6
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
a	a	k8xC
Čtvrtá	čtvrtý	k4xOgFnSc1
hvězda	hvězda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmové	filmový	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
Okresního	okresní	k2eAgInSc2d1
přeboru	přebor	k1gInSc2
<g/>
,	,	kIx,
snímku	snímek	k1gInSc2
Okresní	okresní	k2eAgInSc1d1
přebor	přebor	k1gInSc1
<g/>
:	:	kIx,
Poslední	poslední	k2eAgInSc1d1
zápas	zápas	k1gInSc1
Pepika	Pepika	k?
Hnátka	Hnátka	k1gFnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
fanatický	fanatický	k2eAgMnSc1d1
fanoušek	fanoušek	k1gMnSc1
Sparty	Sparta	k1gFnSc2
představen	představen	k2eAgMnSc1d1
i	i	k8xC
trenér	trenér	k1gMnSc1
Pepik	Pepik	k?
Hnátek	hnátek	k1gInSc1
v	v	k7c6
podání	podání	k1gNnSc6
Miroslava	Miroslav	k1gMnSc2
Krobota	Krobota	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
ve	v	k7c6
filmu	film	k1gInSc6
odmítne	odmítnout	k5eAaPmIp3nS
transplantaci	transplantace	k1gFnSc4
srdce	srdce	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
ukáže	ukázat	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dárce	dárce	k1gMnSc1
orgánu	orgán	k1gInSc2
byl	být	k5eAaImAgMnS
fanoušek	fanoušek	k1gMnSc1
Slavie	slavie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cena	cena	k1gFnSc1
kabiny	kabina	k1gFnSc2
</s>
<s>
Po	po	k7c6
každé	každý	k3xTgFnSc6
sezoně	sezona	k1gFnSc6
hráči	hráč	k1gMnSc3
A	A	kA
<g/>
–	–	k?
<g/>
týmu	tým	k1gInSc2
Sparty	Sparta	k1gFnSc2
hlasují	hlasovat	k5eAaImIp3nP
o	o	k7c6
nejlepším	dobrý	k2eAgMnSc6d3
spoluhráči	spoluhráč	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
se	se	k3xPyFc4
předává	předávat	k5eAaImIp3nS
od	od	k7c2
sezony	sezona	k1gFnSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sezona	sezona	k1gFnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
David	David	k1gMnSc1
Lafata	Lafe	k1gNnPc4
<g/>
9	#num#	k4
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
David	David	k1gMnSc1
Bičík	bičík	k1gInSc4
<g/>
10	#num#	k4
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
Vjačeslav	Vjačeslav	k1gMnSc1
Karavajev	Karavajev	k1gMnSc1
<g/>
15	#num#	k4
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
Lukáš	Lukáš	k1gMnSc1
Štetina	Štetin	k2eAgNnSc2d1
<g/>
12	#num#	k4
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
Adam	Adam	k1gMnSc1
Hložek	Hložek	k1gMnSc1
<g/>
13	#num#	k4
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Dávid	Dávid	k1gInSc1
Hancko	Hancko	k1gNnSc1
<g/>
8	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
v	v	k7c6
rudém	rudý	k2eAgInSc6d1
dresu	dres	k1gInSc6
</s>
<s>
Stadion	stadion	k1gInSc1
na	na	k7c6
Letné	Letná	k1gFnSc6
</s>
<s>
Stadion	stadion	k1gInSc1
na	na	k7c6
Letné	Letná	k1gFnSc6
</s>
<s>
Počet	počet	k1gInSc1
titulůHráčMistrovské	titulůHráčMistrovský	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
</s>
<s>
14	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Novotný	Novotný	k1gMnSc1
</s>
<s>
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
03	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Horst	Horst	k1gMnSc1
Siegl	Siegl	k1gMnSc1
</s>
<s>
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
01	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Michal	Michal	k1gMnSc1
Horňák	Horňák	k1gMnSc1
</s>
<s>
89	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
01	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Michal	Michal	k1gMnSc1
Bílek	Bílek	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
</s>
<s>
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
</s>
<s>
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
01	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Senecký	Senecký	k2eAgMnSc1d1
</s>
<s>
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
<g/>
,	,	kIx,
44	#num#	k4
<g/>
,	,	kIx,
46	#num#	k4
<g/>
,	,	kIx,
48	#num#	k4
<g/>
,	,	kIx,
52	#num#	k4
<g/>
,	,	kIx,
54	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Stejskal	Stejskal	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
</s>
<s>
Július	Július	k1gMnSc1
Bielik	Bielik	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Pešek	Pešek	k1gMnSc1
</s>
<s>
12	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
<g/>
,	,	kIx,
32	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Vrabec	Vrabec	k1gMnSc1
</s>
<s>
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
</s>
<s>
Václav	Václav	k1gMnSc1
Němeček	Němeček	k1gMnSc1
</s>
<s>
97	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Votava	Votava	k1gMnSc1
</s>
<s>
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Blažek	Blažek	k1gMnSc1
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Poštulka	Poštulka	k1gFnSc1
</s>
<s>
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
07	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Obajdin	Obajdina	k1gFnPc2
</s>
<s>
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
03	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Antonín	Antonín	k1gMnSc1
Perner	Perner	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
<g/>
,	,	kIx,
32	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Burgr	Burgr	k1gMnSc1
</s>
<s>
32	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
,	,	kIx,
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
<g/>
,	,	kIx,
44	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Košťálek	košťálek	k1gInSc4
</s>
<s>
32	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
,	,	kIx,
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
<g/>
,	,	kIx,
44	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Kolský	Kolský	k1gMnSc1
</s>
<s>
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
<g/>
,	,	kIx,
44	#num#	k4
<g/>
,	,	kIx,
46	#num#	k4
<g/>
,	,	kIx,
48	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Říha	Říha	k1gMnSc1
</s>
<s>
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
<g/>
,	,	kIx,
44	#num#	k4
<g/>
,	,	kIx,
46	#num#	k4
<g/>
,	,	kIx,
48	#num#	k4
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Koubek	Koubek	k1gMnSc1
</s>
<s>
44	#num#	k4
<g/>
,	,	kIx,
46	#num#	k4
<g/>
,	,	kIx,
48	#num#	k4
<g/>
,	,	kIx,
52	#num#	k4
<g/>
,	,	kIx,
54	#num#	k4
</s>
<s>
Petar	Petar	k1gMnSc1
Novák	Novák	k1gMnSc1
</s>
<s>
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Griga	Grig	k1gMnSc2
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Skuhravý	Skuhravý	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Drahokoupil	Drahokoupil	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Čabala	Čabal	k1gMnSc2
</s>
<s>
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
91	#num#	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Frýdek	frýdka	k1gFnPc2
</s>
<s>
91	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
</s>
<s>
Lumír	Lumír	k1gMnSc1
Mistr	mistr	k1gMnSc1
</s>
<s>
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
</s>
<s>
Vratislav	Vratislav	k1gFnSc1
Lokvenc	Lokvenc	k1gFnSc1
</s>
<s>
95	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
00	#num#	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Hašek	Hašek	k1gMnSc1
</s>
<s>
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
05	#num#	k4
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Baranek	Baranka	k1gFnPc2
</s>
<s>
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
05	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Jun	jun	k1gMnSc1
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
07	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
František	František	k1gMnSc1
Kolenatý	kolenatý	k2eAgMnSc1d1
</s>
<s>
19	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
</s>
<s>
Bohumil	Bohumil	k1gMnSc1
Klenovec	Klenovec	k1gMnSc1
</s>
<s>
32	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
,	,	kIx,
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Čtyřoký	čtyřoký	k2eAgMnSc1d1
</s>
<s>
32	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
,	,	kIx,
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Nejedlý	Nejedlý	k1gMnSc1
</s>
<s>
32	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
,	,	kIx,
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Bouček	Bouček	k1gMnSc1
</s>
<s>
32	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
,	,	kIx,
38	#num#	k4
<g/>
,	,	kIx,
39	#num#	k4
</s>
<s>
František	František	k1gMnSc1
Straka	Straka	k1gMnSc1
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Jarolím	Jarolí	k1gNnPc3
</s>
<s>
84	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
87	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Kouba	Kouba	k1gMnSc1
</s>
<s>
91	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
<g/>
,	,	kIx,
95	#num#	k4
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
</s>
<s>
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
00	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
</s>
<s>
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
,	,	kIx,
00	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Řepka	řepka	k1gFnSc1
</s>
<s>
97	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Jarošík	Jarošík	k1gMnSc1
</s>
<s>
97	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
03	#num#	k4
</s>
<s>
Libor	Libor	k1gMnSc1
Sionko	Sionko	k1gNnSc4
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Labant	Labant	k1gMnSc1
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
05	#num#	k4
</s>
<s>
Další	další	k2eAgMnPc1d1
známí	známý	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Pilát	Pilát	k1gMnSc1
</s>
<s>
Raymond	Raymond	k1gMnSc1
Braine	Brain	k1gInSc5
</s>
<s>
Václav	Václav	k1gMnSc1
Mašek	Mašek	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Vojta	Vojta	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Vrána	Vrána	k1gMnSc1
</s>
<s>
Andrej	Andrej	k1gMnSc1
Kvašňák	Kvašňák	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Němec	Němec	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
</s>
<s>
Petr	Petr	k1gMnSc1
Čech	Čech	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Poborský	Poborský	k2eAgMnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Koller	Koller	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Berger	Berger	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Grygera	Grygero	k1gNnSc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Hübschman	Hübschman	k1gMnSc1
</s>
<s>
Wilfried	Wilfried	k1gInSc1
Bony	bona	k1gFnSc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Ujfaluši	Ujfaluch	k1gMnPc1
</s>
<s>
David	David	k1gMnSc1
Lafata	Lafat	k1gMnSc2
</s>
<s>
Horst	Horst	k1gMnSc1
Siegl	Siegl	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kadeřábek	Kadeřábek	k1gMnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
</s>
<s>
Guelor	Guelor	k1gMnSc1
Kanga	Kang	k1gMnSc2
</s>
<s>
Adam	Adam	k1gMnSc1
Hložek	Hložek	k1gMnSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
trenéři	trenér	k1gMnPc1
</s>
<s>
Zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
uvedeni	uveden	k2eAgMnPc1d1
trenéři	trenér	k1gMnPc1
Sparty	Sparta	k1gFnSc2
od	od	k7c2
r.	r.	kA
1993	#num#	k4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vyhráli	vyhrát	k5eAaPmAgMnP
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
nebo	nebo	k8xC
dosáhli	dosáhnout	k5eAaPmAgMnP
většího	veliký	k2eAgInSc2d2
úspěchu	úspěch	k1gInSc2
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1
seznam	seznam	k1gInSc1
trenérů	trenér	k1gMnPc2
najdete	najít	k5eAaPmIp2nP
na	na	k7c6
stránce	stránka	k1gFnSc6
Seznam	seznam	k1gInSc4
trenérů	trenér	k1gMnPc2
fotbalové	fotbalový	k2eAgFnSc2d1
Sparty	Sparta	k1gFnSc2
Praha	Praha	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Roky	rok	k1gInPc1
působení	působení	k1gNnSc2
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Karol	Karol	k1gInSc1
Dobiaš	Dobiaš	k1gInSc1
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
94	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jozef	Jozef	k1gMnSc1
Jarabinský	Jarabinský	k2eAgMnSc1d1
</s>
<s>
1995	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
Superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čtvrtfinále	čtvrtfinále	k1gNnSc6
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osmifinále	osmifinále	k1gNnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Hřebík	hřebík	k1gInSc4
</s>
<s>
2001	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osmifinále	osmifinále	k1gNnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kotrba	kotrba	k1gFnSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osmifinále	osmifinále	k1gNnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Michal	Michal	k1gMnSc1
Bílek	Bílek	k1gMnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
Superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Úspěchy	úspěch	k1gInPc1
mužů	muž	k1gMnPc2
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Mitropa	Mitropa	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
elitní	elitní	k2eAgFnSc1d1
meziválečná	meziválečný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
letech	let	k1gInPc6
1927	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
významnou	významný	k2eAgFnSc4d1
evropskou	evropský	k2eAgFnSc4d1
fotbalovou	fotbalový	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
prioritně	prioritně	k6eAd1
pro	pro	k7c4
nejsilnější	silný	k2eAgFnPc4d3
kontinentální	kontinentální	k2eAgFnPc4d1
země	zem	k1gFnPc4
dvacátých	dvacátý	k4xOgNnPc2
a	a	k8xC
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
a	a	k8xC
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgFnSc1d1
prestiž	prestiž	k1gFnSc1
soutěže	soutěž	k1gFnSc2
v	v	k7c6
těchto	tento	k3xDgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
srovnávat	srovnávat	k5eAaImF
s	s	k7c7
PMEZ	PMEZ	kA
a	a	k8xC
nebo	nebo	k8xC
Ligou	liga	k1gFnSc7
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celosvětově	celosvětově	k6eAd1
uznávanější	uznávaný	k2eAgInSc4d2
název	název	k1gInSc4
soutěže	soutěž	k1gFnSc2
Mitropa	Mitropa	k1gFnSc1
Cup	cup	k1gInSc1
pak	pak	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
německého	německý	k2eAgInSc2d1
akronymu	akronym	k1gInSc2
pro	pro	k7c4
slovo	slovo	k1gNnSc4
Mitteleuropa	Mitteleuropa	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
jako	jako	k8xC,k8xS
Střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
začal	začít	k5eAaPmAgMnS
její	její	k3xOp3gInSc4
význam	význam	k1gInSc4
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
upadat	upadat	k5eAaImF,k5eAaPmF
a	a	k8xC
pozdější	pozdní	k2eAgNnSc4d2
založení	založení	k1gNnSc4
vlastních	vlastní	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
Unie	unie	k1gFnSc2
evropských	evropský	k2eAgFnPc2d1
fotbalových	fotbalový	k2eAgFnPc2d1
asociací	asociace	k1gFnPc2
úpadek	úpadek	k1gInSc1
pouze	pouze	k6eAd1
prohloubil	prohloubit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
se	se	k3xPyFc4
poté	poté	k6eAd1
soutěže	soutěž	k1gFnPc1
zúčastňovaly	zúčastňovat	k5eAaImAgFnP
také	také	k9
druholigové	druholigový	k2eAgInPc4d1
kluby	klub	k1gInPc4
a	a	k8xC
po	po	k7c6
pádu	pád	k1gInSc6
Jugoslávie	Jugoslávie	k1gFnSc2
byla	být	k5eAaImAgFnS
soutěž	soutěž	k1gFnSc1
definitivně	definitivně	k6eAd1
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1927	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1930	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
ČSF	ČSF	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1912	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
,	,	kIx,
1922	#num#	k4
</s>
<s>
Československá	československý	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
,	,	kIx,
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
;	;	kIx,
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1952	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
<g/>
:	:	kIx,
1954	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
;	;	kIx,
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1925	#num#	k4
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
;	;	kIx,
Sokol	Sokol	k1gInSc1
Bratrství	bratrství	k1gNnSc2
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1949	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
;	;	kIx,
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1951	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
<g/>
:	:	kIx,
1953	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
;	;	kIx,
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
<g/>
:	:	kIx,
1955	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Protektorátní	protektorátní	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Československý	československý	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
<g/>
:	:	kIx,
1964	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1972	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
;	;	kIx,
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1992	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
;	;	kIx,
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1991	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
Menší	malý	k2eAgInPc1d2
úspěchy	úspěch	k1gInPc1
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Mitropa	Mitropa	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
poválečná	poválečný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
viz	vidět	k5eAaImRp2nS
poznámka	poznámka	k1gFnSc1
u	u	k7c2
kontinentálních	kontinentální	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
<g/>
:	:	kIx,
1964	#num#	k4
</s>
<s>
Interpohár	Interpohár	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1980	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1985	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1989	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
ČSF	ČSF	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1909	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Středočeské	středočeský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1920	#num#	k4
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
,	,	kIx,
1923	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
dobročinnosti	dobročinnost	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1909	#num#	k4
<g/>
,	,	kIx,
1915	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1918	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
,	,	kIx,
1931	#num#	k4
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
</s>
<s>
Velikonoční	velikonoční	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
<g/>
:	:	kIx,
1932	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Spartak	Spartak	k1gInSc1
Praha	Praha	k1gFnSc1
Sokolovo	Sokolovo	k1gNnSc4
<g/>
:	:	kIx,
1961	#num#	k4
</s>
<s>
Tournoi	Tournoi	k6eAd1
de	de	k?
Paris	Paris	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1965	#num#	k4
</s>
<s>
Zimní	zimní	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
Tatry	Tatra	k1gFnSc2
Smíchov	Smíchov	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
</s>
<s>
Perleťový	perleťový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
;	;	kIx,
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2003	#num#	k4
</s>
<s>
Úspěchy	úspěch	k1gInPc1
žen	žena	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1991	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
ČSR	ČSR	kA
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ČR	ČR	kA
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
92	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ČR	ČR	kA
(	(	kIx(
<g/>
od	od	k7c2
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
32	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
;	;	kIx,
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
11	#num#	k4
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Menší	malý	k2eAgInPc1d2
úspěchy	úspěch	k1gInPc1
</s>
<s>
Tournoi	Tournoi	k6eAd1
de	de	k?
Menton	menton	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1984	#num#	k4
</s>
<s>
Turbine	Turbinout	k5eAaPmIp3nS
Hallencup	Hallencup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
21	#num#	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Dorostenecká	dorostenecký	k2eAgFnSc1d1
liga	liga	k1gFnSc1
ČR	ČR	kA
(	(	kIx(
<g/>
od	od	k7c2
1993	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
8	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
–	–	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1925	#num#	k4
<g/>
:	:	kIx,
Asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
:	:	kIx,
Středočeská	středočeský	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1927	#num#	k4
<g/>
:	:	kIx,
Kvalifikační	kvalifikační	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
Středočeské	středočeský	k2eAgFnSc2d1
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
</s>
<s>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
:	:	kIx,
Středočeská	středočeský	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1929	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
:	:	kIx,
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
:	:	kIx,
Celostátní	celostátní	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
</s>
<s>
1951	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
:	:	kIx,
Mistrovství	mistrovství	k1gNnSc4
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
<g/>
:	:	kIx,
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1956	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1975	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
ČSR	ČSR	kA
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
–	–	k?
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
výhry	výhra	k1gFnPc1
<g/>
,	,	kIx,
R	R	kA
–	–	k?
remízy	remíza	k1gFnPc4
<g/>
,	,	kIx,
P	P	kA
–	–	k?
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
–	–	k?
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
–	–	k?
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
–	–	k?
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1925	#num#	k4
–	–	k?
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1925	#num#	k4
</s>
<s>
Asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
+20	+20	k4
</s>
<s>
15	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
</s>
<s>
Středočeská	středočeský	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
97	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+73	+73	k4
</s>
<s>
39	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
</s>
<s>
Kvalifikační	kvalifikační	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
+27	+27	k4
</s>
<s>
13	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
</s>
<s>
Středočeská	středočeský	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
+18	+18	k4
</s>
<s>
14	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
</s>
<s>
Středočeská	středočeský	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
+11	+11	k4
</s>
<s>
13	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+21	+21	k4
</s>
<s>
18	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
59	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
+37	+37	k4
</s>
<s>
21	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
54	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
+33	+33	k4
</s>
<s>
27	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
+22	+22	k4
</s>
<s>
24	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
asociační	asociační	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
59	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
+27	+27	k4
</s>
<s>
25	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
74	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
+48	+48	k4
</s>
<s>
35	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
+73	+73	k4
</s>
<s>
41	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
74	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
+45	+45	k4
</s>
<s>
31	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
66	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
+32	+32	k4
</s>
<s>
36	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
85	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
+55	+55	k4
</s>
<s>
32	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
1939	#num#	k4
–	–	k?
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
71	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
+37	+37	k4
</s>
<s>
35	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
+13	+13	k4
</s>
<s>
24	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
–	–	k?
<g/>
8	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
78	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
+31	+31	k4
</s>
<s>
29	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
112	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
+85	+85	k4
</s>
<s>
48	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1945	#num#	k4
–	–	k?
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
1	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
80	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
+46	+46	k4
</s>
<s>
32	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
110	#num#	k4
</s>
<s>
54	#num#	k4
</s>
<s>
+56	+56	k4
</s>
<s>
40	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
62	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+27	+27	k4
</s>
<s>
27	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
</s>
<s>
Celostátní	celostátní	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
89	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
+47	+47	k4
</s>
<s>
37	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
</s>
<s>
Celostátní	celostátní	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
+27	+27	k4
</s>
<s>
35	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
+21	+21	k4
</s>
<s>
33	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
+41	+41	k4
</s>
<s>
41	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
</s>
<s>
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
+8	+8	k4
</s>
<s>
19	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
</s>
<s>
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
+24	+24	k4
</s>
<s>
30	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
</s>
<s>
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
+21	+21	k4
</s>
<s>
27	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
+17	+17	k4
</s>
<s>
26	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
+20	+20	k4
</s>
<s>
40	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
+10	+10	k4
</s>
<s>
23	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
–	–	k?
<g/>
9	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
+1	+1	k4
</s>
<s>
29	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
+1	+1	k4
</s>
<s>
24	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
+8	+8	k4
</s>
<s>
29	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
59	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
+37	+37	k4
</s>
<s>
40	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
+17	+17	k4
</s>
<s>
33	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
+32	+32	k4
</s>
<s>
39	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
+5	+5	k4
</s>
<s>
28	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
+9	+9	k4
</s>
<s>
29	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+15	+15	k4
</s>
<s>
38	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
+6	+6	k4
</s>
<s>
35	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
52	#num#	k4
</s>
<s>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
–	–	k?
<g/>
6	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
54	#num#	k4
</s>
<s>
–	–	k?
<g/>
10	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
52	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
+24	+24	k4
</s>
<s>
37	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
–	–	k?
<g/>
20	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
+6	+6	k4
</s>
<s>
31	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
+14	+14	k4
</s>
<s>
36	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+5	+5	k4
</s>
<s>
31	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
</s>
<s>
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+15	+15	k4
</s>
<s>
36	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
58	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+34	+34	k4
</s>
<s>
46	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+40	+40	k4
</s>
<s>
43	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
75	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
+45	+45	k4
</s>
<s>
37	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
+46	+46	k4
</s>
<s>
42	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
82	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
+60	+60	k4
</s>
<s>
49	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
73	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
+47	+47	k4
</s>
<s>
45	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
+50	+50	k4
</s>
<s>
46	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
58	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
+30	+30	k4
</s>
<s>
39	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
68	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
+48	+48	k4
</s>
<s>
48	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
66	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+42	+42	k4
</s>
<s>
48	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
62	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
+41	+41	k4
</s>
<s>
45	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
+47	+47	k4
</s>
<s>
70	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
56	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+21	+21	k4
</s>
<s>
49	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
61	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
+41	+41	k4
</s>
<s>
65	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
+34	+34	k4
</s>
<s>
71	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
62	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
+39	+39	k4
</s>
<s>
60	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
81	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
+58	+58	k4
</s>
<s>
76	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
71	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
+40	+40	k4
</s>
<s>
68	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
55	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
+36	+36	k4
</s>
<s>
63	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
51	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
+34	+34	k4
</s>
<s>
65	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+24	+24	k4
</s>
<s>
58	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
+25	+25	k4
</s>
<s>
64	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
+4	+4	k4
</s>
<s>
45	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
+24	+24	k4
</s>
<s>
62	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
+27	+27	k4
</s>
<s>
57	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+23	+23	k4
</s>
<s>
56	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
+28	+28	k4
</s>
<s>
62	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
54	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
+33	+33	k4
</s>
<s>
68	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
51	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+26	+26	k4
</s>
<s>
64	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
55	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
+27	+27	k4
</s>
<s>
63	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
78	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
+59	+59	k4
</s>
<s>
79	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Synot	Synot	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
57	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
+37	+37	k4
</s>
<s>
67	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Synot	Synot	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
61	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+37	+37	k4
</s>
<s>
64	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
ePojisteni	ePojisten	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
+21	+21	k4
</s>
<s>
57	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
HET	HET	kA
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+18	+18	k4
</s>
<s>
53	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
liga	liga	k1gFnSc1
</s>
<s>
13520695933	#num#	k4
<g/>
+	+	kIx~
<g/>
2666	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
liga	liga	k1gFnSc1
</s>
<s>
13517996640	#num#	k4
<g/>
+	+	kIx~
<g/>
2660	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
</s>
<s>
130	#num#	k4
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
:	:	kIx,
Sparta	Sparta	k1gFnSc1
(	(	kIx(
<g/>
vítěz	vítěz	k1gMnSc1
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
<g/>
)	)	kIx)
ve	v	k7c6
finále	finále	k1gNnSc6
ligového	ligový	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
vyhrála	vyhrát	k5eAaPmAgFnS
celkově	celkově	k6eAd1
9	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
z	z	k7c2
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
<g/>
z	z	k7c2
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
nad	nad	k7c7
pražskou	pražský	k2eAgFnSc7d1
Slavií	slavie	k1gFnSc7
(	(	kIx(
<g/>
vítěz	vítěz	k1gMnSc1
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Evropské	evropský	k2eAgInPc1d1
poháry	pohár	k1gInPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Výsledky	výsledek	k1gInPc1
Sparty	Sparta	k1gFnSc2
Praha	Praha	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
fotbalových	fotbalový	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
FK	FK	kA
Partizan	Partizany	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
RSC	RSC	kA
Anderlecht	Anderlecht	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Steaua	Steaua	k1gFnSc1
Bucureș	Bucureș	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
semifinálové	semifinálový	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
B	B	kA
(	(	kIx(
<g/>
sdílené	sdílený	k2eAgFnSc2d1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
PMEZ	PMEZ	kA
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
RSC	RSC	kA
Anderlecht	Anderlecht	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
–	–	k?
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
IFK	IFK	kA
Göteborg	Göteborg	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
,	,	kIx,
Parma	Parma	k1gFnSc1
<g/>
,	,	kIx,
Galatasaray	Galatasaraa	k1gFnPc1
SK	Sk	kA
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
–	–	k?
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
(	(	kIx(
<g/>
vítěz	vítěz	k1gMnSc1
<g/>
:	:	kIx,
Bordeaux	Bordeaux	k1gNnSc7
<g/>
,	,	kIx,
Willem	Will	k1gInSc7
II	II	kA
<g/>
,	,	kIx,
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Porto	porto	k1gNnSc1
<g/>
,	,	kIx,
Hertha	Hertha	k1gFnSc1
BSC	BSC	kA
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
:	:	kIx,
Arsenal	Arsenal	k1gFnSc2
FC	FC	kA
<g/>
,	,	kIx,
SS	SS	kA
Lazio	Lazio	k1gNnSc1
<g/>
,	,	kIx,
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
:	:	kIx,
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Feyenoord	Feyenoord	k1gInSc1
<g/>
,	,	kIx,
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
:	:	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Panathinaikos	Panathinaikos	k1gInSc1
<g/>
,	,	kIx,
Porto	porto	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Racing	Racing	k1gInSc1
Genk	Genk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
:	:	kIx,
Chelsea	Chelsea	k1gMnSc1
<g/>
,	,	kIx,
SS	SS	kA
Lazio	Lazio	k1gNnSc1
<g/>
,	,	kIx,
Beşiktaş	Beşiktaş	k1gFnPc1
JK	JK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
osmifinále	osmifinále	k1gNnSc1
(	(	kIx(
<g/>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
:	:	kIx,
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
<g/>
,	,	kIx,
Olympique	Olympique	k1gInSc1
Lyon	Lyon	k1gInSc1
<g/>
,	,	kIx,
Fenerbahçe	Fenerbahçe	k1gInSc1
SK	Sk	kA
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
:	:	kIx,
Arsenal	Arsenal	k1gFnSc2
FC	FC	kA
<g/>
,	,	kIx,
Ajax	Ajax	k1gInSc1
<g/>
,	,	kIx,
Thun	Thun	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Arsenal	Arsenal	k1gFnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Panathinaikos	Panathinaikos	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Panathinaikos	Panathinaikos	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
MŠK	MŠK	kA
Žilina	Žilina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Malmö	Malmö	k1gFnSc1
FF	ff	kA
<g/>
)	)	kIx)
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Steaua	Steau	k2eAgFnSc1d1
Bukurešť	Bukurešť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
)	)	kIx)
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Neuchâtel	Neuchâtel	k1gInSc1
Xamax	Xamax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
HNK	HNK	kA
Hajduk	hajduk	k1gMnSc1
Split	Split	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
(	(	kIx(
<g/>
Vitória	Vitórium	k1gNnSc2
SC	SC	kA
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Denizlispor	Denizlispor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
GNK	GNK	kA
Dinamo	Dinama	k1gFnSc5
Zagreb	Zagreb	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
K	k	k7c3
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
FC	FC	kA
Vaslui	Vaslu	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
I	I	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Chelsea	Chelse	k2eAgFnSc1d1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
BK	BK	kA
Häcken	Häcken	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
I	I	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
K	k	k7c3
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čtvrtfinále	čtvrtfinále	k1gNnSc1
(	(	kIx(
<g/>
Villarreal	Villarreal	k1gInSc1
CF	CF	kA
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
–	–	k?
základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
K	k	k7c3
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
FK	FK	kA
Rostov	Rostov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
FK	FK	kA
Crvena	Crvena	k1gFnSc1
zvezda	zvezda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
FK	FK	kA
Spartak	Spartak	k1gInSc1
Subotica	Subotica	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
(	(	kIx(
<g/>
Trabzonspor	Trabzonspor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
–	–	k?
finále	finále	k1gNnSc6
<g/>
,	,	kIx,
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
SK	Sk	kA
Rapid	rapid	k1gInSc1
Wien	Wien	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1929	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
Újpest	Újpest	k1gInSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
1930	#num#	k4
–	–	k?
finále	finále	k1gNnSc4
(	(	kIx(
<g/>
SK	Sk	kA
Rapid	rapid	k1gInSc1
Wien	Wien	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1931	#num#	k4
–	–	k?
semifinále	semifinále	k1gNnSc1
(	(	kIx(
<g/>
Wiener	Wiener	k1gInSc1
AC	AC	kA
<g/>
)	)	kIx)
</s>
<s>
1932	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
AGC	AGC	kA
Bologna	Bologna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1933	#num#	k4
–	–	k?
semifinále	semifinále	k1gNnSc6
(	(	kIx(
<g/>
AS	as	k9
Ambrosiana	Ambrosiana	k1gFnSc1
Inter	Intra	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
1934	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
SK	Sk	kA
Admira	Admira	k1gMnSc1
Wien	Wien	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1935	#num#	k4
–	–	k?
finále	finále	k1gNnSc6
<g/>
,	,	kIx,
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
poražený	poražený	k2eAgMnSc1d1
finalista	finalista	k1gMnSc1
Ferencvárosi	Ferencvárose	k1gFnSc4
TC	tc	k0
<g/>
)	)	kIx)
</s>
<s>
1936	#num#	k4
–	–	k?
finále	finále	k1gNnSc4
(	(	kIx(
<g/>
FK	FK	kA
Austria	Austrium	k1gNnPc4
Wien	Wiena	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
1937	#num#	k4
–	–	k?
osmifinále	osmifinále	k1gNnSc6
(	(	kIx(
<g/>
SK	Sk	kA
Admira	Admira	k1gMnSc1
Wien	Wien	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1938	#num#	k4
–	–	k?
osmifinále	osmifinále	k1gNnSc6
(	(	kIx(
<g/>
Genova	Genov	k1gInSc2
1893	#num#	k4
FBC	FBC	kA
<g/>
)	)	kIx)
</s>
<s>
1939	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc6
(	(	kIx(
<g/>
Ferencvárosi	Ferencvárose	k1gFnSc6
TC	tc	k0
<g/>
)	)	kIx)
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
FC	FC	kA
Bologna	Bologna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
Leeds	Leeds	k1gInSc1
United	United	k1gInSc1
AFC	AFC	kA
<g/>
)	)	kIx)
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
V	v	k7c6
závorce	závorka	k1gFnSc6
u	u	k7c2
daného	daný	k2eAgNnSc2d1
ročníkového	ročníkový	k2eAgNnSc2d1
maxima	maximum	k1gNnSc2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgMnSc1d1
přemožitel	přemožitel	k1gMnSc1
daného	daný	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
–	–	k?
<g/>
li	li	k8xS
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
West	West	k1gInSc1
Ham	ham	k0
United	United	k1gMnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
–	–	k?
semifinále	semifinále	k1gNnSc6
(	(	kIx(
<g/>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
MTK	MTK	kA
Budapešť	Budapešť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
PFK	PFK	kA
Slavia	Slavia	k1gFnSc1
Sofia	Sofia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
–	–	k?
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
Parma	Parma	k1gFnSc1
FC	FC	kA
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
(	(	kIx(
<g/>
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
střelci	střelec	k1gMnPc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
gólů	gól	k1gInPc2
</s>
<s>
Střelec	Střelec	k1gMnSc1
</s>
<s>
Komu	kdo	k3yInSc3,k3yQnSc3,k3yRnSc3
dal	dát	k5eAaPmAgInS
gól	gól	k1gInSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Lafata	Lafat	k1gMnSc2
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
FC	FC	kA
Levadia	Levadium	k1gNnSc2
Tallinn	Tallinna	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
Malmö	Malmö	k1gMnPc2
FF	ff	kA
<g/>
,	,	kIx,
3	#num#	k4
ŠK	ŠK	kA
Slovan	Slovan	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
BSC	BSC	kA
Young	Young	k1gInSc1
Boys	boy	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
BK	BK	kA
Häcken	Häckna	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
FC	FC	kA
Schalke	Schalke	k1gFnSc1
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
APOEL	APOEL	kA
FC	FC	kA
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
<g/>
,	,	kIx,
Asteras	Asteras	k1gInSc1
Tripolis	Tripolis	k1gInSc1
<g/>
,	,	kIx,
SönderjyskE	SönderjyskE	k1gMnSc1
Fodbold	Fodbold	k1gMnSc1
<g/>
,	,	kIx,
Hapoel	Hapoel	k1gMnSc1
Beerševa	Beerševo	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Mráz	Mráz	k1gMnSc1
</s>
<s>
5	#num#	k4
Anorthosis	Anorthosis	k1gFnSc1
Famagusta	Famagusta	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
FC	FC	kA
Lausanne-Sport	Lausanne-Sport	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
Górnik	Górnik	k1gInSc1
Zabrze	Zabrze	k1gFnSc2
<g/>
,	,	kIx,
2	#num#	k4
Skeid	Skeida	k1gFnPc2
Oslo	Oslo	k1gNnSc2
<g/>
,	,	kIx,
West	West	k1gMnSc1
Ham	ham	k0
United	United	k1gInSc4
FC	FC	kA
<g/>
,	,	kIx,
RSC	RSC	kA
Anderlecht	Anderlecht	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
</s>
<s>
Václav	Václav	k1gMnSc1
Mašek	Mašek	k1gMnSc1
</s>
<s>
5	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
<g/>
,	,	kIx,
5	#num#	k4
Anorthosis	Anorthosis	k1gFnSc1
Famagusta	Famagusta	k1gFnSc1
<g/>
,	,	kIx,
West	West	k1gInSc1
Ham	ham	k0
United	United	k1gInSc4
FC	FC	kA
<g/>
,	,	kIx,
Bologna	Bologna	k1gFnSc1
FC	FC	kA
<g/>
,	,	kIx,
Partizan	Partizan	k1gInSc1
Bělehrad	Bělehrad	k1gInSc1
</s>
<s>
Horst	Horst	k1gMnSc1
Siegl	Siegl	k1gMnSc1
</s>
<s>
4	#num#	k4
Glentoran	Glentoran	k1gInSc1
Belfast	Belfast	k1gInSc4
<g/>
,	,	kIx,
2	#num#	k4
AIK	AIK	kA
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Olympique	Olympique	k1gInSc1
de	de	k?
Marseille	Marseille	k1gFnSc2
<g/>
,	,	kIx,
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
SV	sv	kA
Werder	Werder	k1gInSc1
Bremen	Bremen	k1gInSc1
<g/>
,	,	kIx,
Fiorentina	Fiorentina	k1gFnSc1
<g/>
,	,	kIx,
Dortmund	Dortmund	k1gInSc1
<g/>
,	,	kIx,
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
,	,	kIx,
Hertha	Hertha	k1gMnSc1
BSC	BSC	kA
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
</s>
<s>
Vratislav	Vratislav	k1gFnSc1
Lokvenc	Lokvenc	k1gFnSc1
</s>
<s>
3	#num#	k4
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
,	,	kIx,
Galatasaray	Galatasaraa	k1gFnPc1
SK	Sk	kA
<g/>
,	,	kIx,
Fiorentina	Fiorentina	k1gFnSc1
<g/>
,	,	kIx,
Real	Real	k1gInSc1
San	San	k1gMnSc1
Sebastian	Sebastian	k1gMnSc1
<g/>
,	,	kIx,
Salzburg	Salzburg	k1gInSc1
<g/>
,	,	kIx,
Sturm	Sturm	k1gInSc1
Graz	Graz	k1gInSc1
<g/>
,	,	kIx,
Silkeborg	Silkeborg	k1gInSc1
IF	IF	kA
<g/>
,	,	kIx,
Glentoran	Glentoran	k1gInSc1
Belfast	Belfast	k1gInSc1
</s>
<s>
Wilfried	Wilfried	k1gInSc1
Bony	bona	k1gFnSc2
</s>
<s>
4	#num#	k4
FC	FC	kA
Lausanne-Sport	Lausanne-Sport	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
NK	NK	kA
Maribor	Maribora	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
Liepā	Liepā	k1gFnPc2
<g/>
,	,	kIx,
Kluž	Kluž	k1gFnSc1
<g/>
,	,	kIx,
US	US	kA
Palermo	Palermo	k1gNnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
</s>
<s>
2	#num#	k4
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
ŠK	ŠK	kA
Slovan	Slovan	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
PEC	Pec	k1gFnSc1
Zwolle	Zwolle	k1gFnSc1
<g/>
,	,	kIx,
Olympique	Olympique	k1gFnSc1
Lyon	Lyon	k1gInSc1
<g/>
,	,	kIx,
Hapoel	Hapoel	k1gMnSc1
Ironi	Iroň	k1gMnSc3
Kirjat	Kirjat	k1gInSc1
Šmona	Šmono	k1gNnSc2
<g/>
,	,	kIx,
SS	SS	kA
Lazio	Lazio	k1gNnSc1
<g/>
,	,	kIx,
Villareal	Villareal	k1gInSc1
CF	CF	kA
<g/>
,	,	kIx,
Celtic	Celtice	k1gFnPc2
Glasgow	Glasgow	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Jarošík	Jarošík	k1gMnSc1
</s>
<s>
2	#num#	k4
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
Genk	Genka	k1gFnPc2
<g/>
,	,	kIx,
Tilburg	Tilburg	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
Feyenoord	Feyenoorda	k1gFnPc2
<g/>
,	,	kIx,
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
,	,	kIx,
Široki	Široki	k1gNnSc1
Brijeg	Brijega	k1gFnPc2
<g/>
,	,	kIx,
Denizlispor	Denizlispora	k1gFnPc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
</s>
<s>
Léonard	Léonard	k1gInSc1
Kweuke	Kweuk	k1gFnSc2
</s>
<s>
4	#num#	k4
FK	FK	kA
Sarajevo	Sarajevo	k1gNnSc1
<g/>
,	,	kIx,
3	#num#	k4
Admira	Admiro	k1gNnSc2
Mödling	Mödling	k1gInSc1
<g/>
,	,	kIx,
Hapoel	Hapoel	k1gMnSc1
Ironi	Iroň	k1gMnSc3
Kirjat	Kirjat	k1gInSc1
Šmona	Šmono	k1gNnSc2
<g/>
,	,	kIx,
FC	FC	kA
Lausanne-Sport	Lausanne-Sport	k1gInSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
</s>
<s>
3	#num#	k4
Feyenoord	Feyenoorda	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
Inter	Intra	k1gFnPc2
Milano	Milana	k1gFnSc5
<g/>
,	,	kIx,
Liepajas	Liepajas	k1gMnSc1
Metalurgs	Metalurgs	k1gInSc1
<g/>
,	,	kIx,
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
US	US	kA
Palermo	Palermo	k1gNnSc1
<g/>
,	,	kIx,
Hapoel	Hapoel	k1gMnSc1
Ironi	Iroň	k1gMnSc3
Kirjat	Kirjat	k1gInSc1
Šmona	Šmono	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Griga	Grig	k1gMnSc2
</s>
<s>
2	#num#	k4
Neuchâtel	Neuchâtel	k1gInSc1
Xamax	Xamax	k1gInSc4
<g/>
,	,	kIx,
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Widzew	Widzew	k1gFnSc1
Lodž	Lodž	k1gFnSc1
<g/>
,	,	kIx,
Watford	Watford	k1gInSc1
<g/>
,	,	kIx,
Lyngby	Lyngb	k1gInPc1
BK	BK	kA
<g/>
,	,	kIx,
Fram	Fram	k1gMnSc1
Reykjavík	Reykjavík	k1gMnSc1
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Juliš	Juliš	k1gMnSc1
</s>
<s>
5	#num#	k4
Celtic	Celtice	k1gFnPc2
Glasqow	Glasqow	k1gFnPc2
<g/>
,	,	kIx,
APOEL	APOEL	kA
FC	FC	kA
<g/>
,	,	kIx,
FK	FK	kA
Krasnodar	Krasnodar	k1gInSc1
<g/>
,	,	kIx,
SS	SS	kA
Lazio	Lazio	k1gNnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Poborský	Poborský	k2eAgMnSc1d1
</s>
<s>
3	#num#	k4
Vardar	Vardar	k1gInSc1
Skopje	Skopje	k1gFnSc2
<g/>
,	,	kIx,
SS	SS	kA
Lazio	Lazio	k1gNnSc1
<g/>
,	,	kIx,
Beşiktaş	Beşiktaş	k1gFnSc1
JK	JK	kA
<g/>
,	,	kIx,
Genk	Genk	k1gInSc1
<g/>
,	,	kIx,
Široki	Široki	k1gNnSc1
Brijeg	Brijega	k1gFnPc2
</s>
<s>
Libor	Libor	k1gMnSc1
Sionko	Sionko	k1gNnSc1
</s>
<s>
2	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
,	,	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
SS	SS	kA
Lazio	Lazio	k1gNnSc1
<g/>
,	,	kIx,
Vardar	Vardar	k1gInSc1
Skopje	Skopje	k1gFnSc2
<g/>
,	,	kIx,
FK	FK	kA
Sarajevo	Sarajevo	k1gNnSc1
</s>
<s>
Bořek	Bořek	k1gMnSc1
Dočkal	dočkat	k5eAaPmAgMnS
</s>
<s>
3	#num#	k4
FC	FC	kA
Thun	Thun	k1gMnSc1
<g/>
,	,	kIx,
PEC	Pec	k1gFnSc1
Zwolle	Zwolle	k1gFnSc1
<g/>
,	,	kIx,
SS	SS	kA
Lazio	Lazio	k1gNnSc1
<g/>
,	,	kIx,
Villareal	Villareal	k1gInSc1
CF	CF	kA
<g/>
,	,	kIx,
Lille	Lille	k1gFnPc1
OSC	OSC	kA
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Skuhravý	Skuhravý	k1gMnSc1
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Widzew	Widzew	k1gFnSc1
Lodž	Lodž	k1gFnSc1
<g/>
,	,	kIx,
Watford	Watforda	k1gFnPc2
<g/>
,	,	kIx,
Vitória	Vitórium	k1gNnSc2
SC	SC	kA
<g/>
,	,	kIx,
Fram	Fram	k1gMnSc1
Reykjavík	Reykjavík	k1gMnSc1
<g/>
,	,	kIx,
Sredec	Sredec	k1gMnSc1
Sofia	Sofia	k1gFnSc1
</s>
<s>
Petar	Petar	k1gMnSc1
Novák	Novák	k1gMnSc1
</s>
<s>
4	#num#	k4
Fram	Fram	k1gMnSc1
Reykjavík	Reykjavík	k1gMnSc1
<g/>
,	,	kIx,
Vitória	Vitórium	k1gNnSc2
SC	SC	kA
<g/>
,	,	kIx,
Fenerbahçe	Fenerbahç	k1gFnPc1
SK	Sk	kA
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Baranek	Baranka	k1gFnPc2
</s>
<s>
2	#num#	k4
Kutaisi	Kutaise	k1gFnSc3
<g/>
,	,	kIx,
Salzburg	Salzburg	k1gInSc1
<g/>
,	,	kIx,
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
Tilburg	Tilburg	k1gInSc1
<g/>
,	,	kIx,
Široki	Široki	k1gNnSc1
Brijeg	Brijega	k1gFnPc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kladrubský	kladrubský	k2eAgMnSc1d1
</s>
<s>
2	#num#	k4
US	US	kA
Palermo	Palermo	k1gNnSc1
<g/>
,	,	kIx,
2	#num#	k4
GNK	GNK	kA
Dinamo	Dinama	k1gFnSc5
Zagreb	Zagreb	k1gInSc1
<g/>
,	,	kIx,
Lech	Lech	k1gMnSc1
Poznań	Poznań	k1gMnSc1
<g/>
,	,	kIx,
Tiraspol	Tiraspol	k1gInSc1
</s>
<s>
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
26.11	26.11	k4
<g/>
.	.	kIx.
2020	#num#	k4
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Chování	chování	k1gNnSc1
fanoušků	fanoušek	k1gMnPc2
AC	AC	kA
Sparty	Sparta	k1gFnSc2
Praha	Praha	k1gFnSc1
je	být	k5eAaImIp3nS
provázeno	provázet	k5eAaImNgNnS
řadou	řada	k1gFnSc7
skandálů	skandál	k1gInPc2
v	v	k7c6
historii	historie	k1gFnSc6
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
běžné	běžný	k2eAgNnSc4d1
výtržnictví	výtržnictví	k1gNnSc4
pod	pod	k7c7
vlivem	vliv	k1gInSc7
alkoholu	alkohol	k1gInSc2
nebo	nebo	k8xC
otevřený	otevřený	k2eAgInSc4d1
projev	projev	k1gInSc4
rasismu	rasismus	k1gInSc2
v	v	k7c6
areálu	areál	k1gInSc6
stadionu	stadion	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
výkřiky	výkřik	k1gInPc4
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
i	i	k9
slogany	slogan	k1gInPc1
antisemitské	antisemitský	k2eAgInPc1d1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
mířené	mířený	k2eAgInPc1d1
převážně	převážně	k6eAd1
na	na	k7c4
oponenty	oponent	k1gMnPc4
z	z	k7c2
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Františka	František	k1gMnSc2
Banyaiho	Banyai	k1gMnSc2
<g/>
,	,	kIx,
mluvčího	mluvčí	k1gMnSc2
pražské	pražský	k2eAgFnSc2d1
židovské	židovský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
<g/>
,	,	kIx,
„	„	k?
<g/>
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
problém	problém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
řešit	řešit	k5eAaImF
<g/>
“	“	k?
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
Leo	Leo	k1gMnSc1
Pavlátem	Pavlát	k1gInSc7
napsali	napsat	k5eAaPmAgMnP,k5eAaBmAgMnP
Spartě	Sparta	k1gFnSc3
stížnostní	stížnostní	k2eAgInSc4d1
dopis	dopis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
již	již	k6eAd1
Spartu	Sparta	k1gFnSc4
potrestalo	potrestat	k5eAaPmAgNnS
pokutou	pokuta	k1gFnSc7
zhruba	zhruba	k6eAd1
43	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
–	–	k?
oddíl	oddíl	k1gInSc4
ženského	ženský	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
–	–	k?
rezervní	rezervní	k2eAgInSc4d1
tým	tým	k1gInSc4
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
</s>
<s>
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Niț	Niț	k?
</s>
<s>
Chipciu	Chipcius	k1gMnSc3
</s>
<s>
Costa	Costa	k1gFnSc1
</s>
<s>
Kaya	Kaya	k6eAd1
</s>
<s>
Radaković	Radaković	k?
</s>
<s>
Plavšić	Plavšić	k?
</s>
<s>
Kanga	Kanga	k1gFnSc1
</s>
<s>
Frýdek	Frýdek	k1gInSc1
</s>
<s>
Stanciu	Stancius	k1gMnSc3
</s>
<s>
Ben	Ben	k1gInSc1
Chaim	Chaima	k1gFnPc2
</s>
<s>
Tetteh	Tetteh	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DIGITAL	Digital	kA
<g/>
,	,	kIx,
Apploud	Apploud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GENERALI	GENERALI	kA
Arena	Arena	k1gFnSc1
|	|	kIx~
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HRADILKOVÁ	HRADILKOVÁ	kA
BÁRTOVÁ	Bártová	k1gFnSc1
<g/>
,	,	kIx,
Eliška	Eliška	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotbalová	fotbalový	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
jako	jako	k8xC,k8xS
český	český	k2eAgInSc1d1
klenot	klenot	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
vlastní	vlastnit	k5eAaImIp3nS
spletitá	spletitý	k2eAgFnSc1d1
síť	síť	k1gFnSc1
společností	společnost	k1gFnPc2
z	z	k7c2
Kypru	Kypr	k1gInSc2
a	a	k8xC
Seychel	Seychely	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-26	2019-06-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DIGITAL	Digital	kA
<g/>
,	,	kIx,
Apploud	Apploud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GENERALI	GENERALI	kA
Arena	Areno	k1gNnSc2
|	|	kIx~
</s>
<s>
www.sparta.cz	www.sparta.cz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnPc4
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-26	2019-12-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nový	nový	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
partner	partner	k1gMnSc1
<g/>
:	:	kIx,
Tipsport	Tipsport	k1gInSc1
|	|	kIx~
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Doklad	doklad	k1gInSc1
o	o	k7c4
užíváni	užívat	k5eAaImNgMnP
rudých	rudý	k2eAgInPc2d1
dresů	dres	k1gInPc2
týmem	tým	k1gInSc7
Woolwich	Woolwich	k1gMnSc1
Arsenal	Arsenal	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1895	#num#	k4
<g/>
↑	↑	k?
PLZ	PLZ	kA
-	-	kIx~
SPA	SPA	kA
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
pp	pp	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
:	:	kIx,
<g/>
8	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.sparta.cz	www.sparta.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zázrak	zázrak	k1gInSc1
se	se	k3xPyFc4
nekonal	konat	k5eNaImAgInS
<g/>
,	,	kIx,
oslabená	oslabený	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
na	na	k7c4
Villarreal	Villarreal	k1gInSc4
nestačila	stačit	k5eNaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FELT	FELT	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ligu	liga	k1gFnSc4
roztočila	roztočit	k5eAaPmAgFnS
ruleta	ruleta	k1gFnSc1
miliónů	milión	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PRÁVO	právo	k1gNnSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2017	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
27	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
137	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
2119	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stramaccioni	Stramaccion	k1gMnPc1
končí	končit	k5eAaImIp3nP
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Stramaccioni	Stramaccioň	k1gFnSc6
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-06	2018-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Pavel	Pavel	k1gMnSc1
Hapal	hapat	k5eAaPmAgMnS
novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-07	2018-03-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
První	první	k4xOgInSc1
zápas	zápas	k1gInSc1
se	se	k3xPyFc4
Spartakem	Spartak	k1gInSc7
Subotica	Subotic	k1gInSc2
skončil	skončit	k5eAaPmAgInS
prohrou	prohra	k1gFnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Pavel	Pavel	k1gMnSc1
Hapal	Hapal	k1gMnSc1
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
dočasně	dočasně	k6eAd1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
A-tým	A-tý	k1gMnSc7
<g/>
,	,	kIx,
sparta	sparta	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Sestava	sestava	k1gFnSc1
Sparty	Sparta	k1gFnSc2
proti	proti	k7c3
Jablonci	Jablonec	k1gInSc3
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Sparta	Sparta	k1gFnSc1
-	-	kIx~
Subotica	Subotica	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
zkraty	zkrat	k1gInPc1
Radakoviče	Radakovič	k1gMnSc2
i	i	k8xC
fandů	fandů	k?
přinesly	přinést	k5eAaPmAgInP
konec	konec	k1gInSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Policie	policie	k1gFnSc1
hledá	hledat	k5eAaImIp3nS
chuligány	chuligán	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
vběhli	vběhnout	k5eAaPmAgMnP
na	na	k7c4
hřiště	hřiště	k1gNnSc4
Sparty	Sparta	k1gFnSc2
a	a	k8xC
házeli	házet	k5eAaImAgMnP
praporky	praporek	k1gInPc4
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Zákazy	zákaz	k1gInPc1
vstupu	vstup	k1gInSc2
<g/>
,	,	kIx,
uložení	uložení	k1gNnSc2
pokut	pokuta	k1gFnPc2
<g/>
,	,	kIx,
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Sparta	Sparta	k1gFnSc1
otočila	otočit	k5eAaPmAgFnS
zápas	zápas	k1gInSc4
v	v	k7c6
Karviné	Karviná	k1gFnSc6
a	a	k8xC
vyhrála	vyhrát	k5eAaPmAgFnS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-11-10	2018-11-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
i	i	k9
s	s	k7c7
Teplicemi	Teplice	k1gFnPc7
a	a	k8xC
na	na	k7c6
Slavii	slavie	k1gFnSc6
ztrácí	ztrácet	k5eAaImIp3nS
už	už	k6eAd1
patnáct	patnáct	k4xCc4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-09	2018-12-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sportovní	sportovní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-17	2018-12-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dočkal	Dočkal	k1gMnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
do	do	k7c2
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
Rosický	rosický	k2eAgInSc1d1
ale	ale	k8xC
připomíná	připomínat	k5eAaImIp3nS
<g/>
:	:	kIx,
Bořek	Bořek	k1gMnSc1
nevyřeší	vyřešit	k5eNaPmIp3nS
všechno	všechen	k3xTgNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-22	2019-02-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1
Hložek	Hložek	k1gMnSc1
zlomil	zlomit	k5eAaPmAgMnS
Plzeň	Plzeň	k1gFnSc4
i	i	k8xC
rekord	rekord	k1gInSc4
<g/>
:	:	kIx,
Šel	jít	k5eAaImAgMnS
mi	já	k3xPp1nSc3
mráz	mráz	k1gInSc1
po	po	k7c6
zádech	záda	k1gNnPc6
<g/>
.	.	kIx.
iSport	iSport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-09	2019-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POVEJŠIL	POVEJŠIL	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
;	;	kIx,
ČIHÁK	Čihák	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tragédie	tragédie	k1gFnSc1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nehodě	nehoda	k1gFnSc6
mikrobusu	mikrobus	k1gInSc2
zemřel	zemřít	k5eAaPmAgMnS
fotbalista	fotbalista	k1gMnSc1
Josef	Josef	k1gMnSc1
Šural	Šural	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-29	2019-04-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
zůstává	zůstávat	k5eAaImIp3nS
ve	v	k7c6
hře	hra	k1gFnSc6
o	o	k7c4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
Spartě	Sparta	k1gFnSc3
na	na	k7c6
domácím	domácí	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
nastřílela	nastřílet	k5eAaPmAgFnS
čtyři	čtyři	k4xCgInPc4
góly	gól	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-15	2019-05-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
porazila	porazit	k5eAaPmAgFnS
Jablonec	Jablonec	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Severočeši	Severočech	k1gMnPc1
dohrávali	dohrávat	k5eAaImAgMnP
bez	bez	k1gInSc4
vyloučeného	vyloučený	k2eAgMnSc2d1
Hovorky	Hovorka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-19	2019-05-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novým	nový	k2eAgMnSc7d1
koučem	kouč	k1gMnSc7
Sparty	Sparta	k1gFnSc2
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
Václav	Václav	k1gMnSc1
Jílek	Jílek	k1gMnSc1
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-30	2019-05-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slovácko	Slovácko	k1gNnSc4
dvakrát	dvakrát	k6eAd1
udeřilo	udeřit	k5eAaPmAgNnS
z	z	k7c2
brejků	brejk	k1gInPc2
a	a	k8xC
veze	vézt	k5eAaImIp3nS
ze	z	k7c2
Sparty	Sparta	k1gFnSc2
překvapivé	překvapivý	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-07-14	2019-07-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Václav	Václav	k1gMnSc1
Jílek	Jílek	k1gMnSc1
odvolán	odvolat	k5eAaPmNgMnS
z	z	k7c2
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-18	2020-02-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CENA	cena	k1gFnSc1
KABINY	kabina	k1gFnSc2
2020	#num#	k4
|	|	kIx~
Hlasování	hlasování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
YouTube	YouTub	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
Mitropa	Mitropa	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
"	"	kIx"
<g/>
Czechoslovakia	Czechoslovakia	k1gFnSc1
/	/	kIx~
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
-	-	kIx~
List	list	k1gInSc1
of	of	k?
League	League	k1gInSc1
Tables	Tables	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Czechoslovakia	Czechoslovakia	k1gFnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Super	super	k2eAgInSc1d1
Cup	cup	k1gInSc1
Finals	Finalsa	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
UEFA	UEFA	kA
Intertoto	Intertota	k1gFnSc5
Cup	cup	k0
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
History	Histor	k1gMnPc4
of	of	k?
the	the	k?
Intertoto	Intertota	k1gFnSc5
Cup	cup	k0
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mogiel	mogiel	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
International	International	k1gFnSc1
Tournaments	Tournaments	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
1924	#num#	k4
<g/>
-	-	kIx~
<g/>
1957	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Velikonocní	Velikonocní	k2eAgInSc1d1
Turnaj	turnaj	k1gInSc1
in	in	k?
1961	#num#	k4
in	in	k?
Praha	Praha	k1gFnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Tournoi	Tourno	k1gFnSc2
International	International	k1gMnSc1
de	de	k?
Paris	Paris	k1gMnSc1
1957-1993	1957-1993	k4
(	(	kIx(
<g/>
Paris-France	Paris-France	k1gFnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
32	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
Zimního	zimní	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
<g/>
,	,	kIx,
hřiště	hřiště	k1gNnSc2
u	u	k7c2
Železničního	železniční	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
18	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
,	,	kIx,
Vydala	vydat	k5eAaPmAgFnS
<g/>
:	:	kIx,
Tatra	Tatra	k1gFnSc1
Smíchov	Smíchov	k1gInSc1
oddíl	oddíl	k1gInSc1
kopané	kopaná	k1gFnSc2
-	-	kIx~
leden	leden	k1gInSc1
1990	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Vytiskly	vytisknout	k5eAaPmAgInP
<g/>
:	:	kIx,
Tiskařské	tiskařský	k2eAgInPc1d1
závody	závod	k1gInPc1
<g/>
,	,	kIx,
provoz	provoz	k1gInSc1
32	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Myslíkova	Myslíkův	k2eAgFnSc1d1
15	#num#	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Perleťové	perleťový	k2eAgInPc4d1
poháry	pohár	k1gInPc4
konané	konaný	k2eAgInPc4d1
od	od	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcslavojzirovnice	fcslavojzirovnice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Jeřábek	Jeřábek	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc4d1
a	a	k8xC
československý	československý	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
(	(	kIx(
<g/>
1	#num#	k4
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
228	#num#	k4
<g/>
,	,	kIx,
239	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1656	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Women	Women	k2eAgInSc1d1
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
Výsledkový	výsledkový	k2eAgInSc1d1
servis	servis	k1gInSc1
serveru	server	k1gInSc2
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
Fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Sparťanky	Sparťanka	k1gFnPc1
získaly	získat	k5eAaPmAgFnP
double	double	k2eAgFnPc1d1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
21.06	21.06	k4
<g/>
.2008	.2008	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Finále	finále	k1gNnSc1
Poháru	pohár	k1gInSc2
ČMFS	ČMFS	kA
<g/>
:	:	kIx,
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
double	double	k2eAgInSc4d1
pro	pro	k7c4
sparťanky	sparťanka	k1gFnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
21.06	21.06	k4
<g/>
.2009	.2009	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Sparťanky	Sparťanka	k1gFnPc1
získaly	získat	k5eAaPmAgFnP
double	double	k2eAgFnPc1d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26.06	26.06	k4
<g/>
.2009	.2009	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Sparťanky	Sparťanka	k1gFnPc1
ovládly	ovládnout	k5eAaPmAgFnP
ženský	ženský	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
<g/>
,	,	kIx,
titul	titul	k1gInSc4
slaví	slavit	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
věkové	věkový	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sparta	sparta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
15.06	15.06	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparťanky	Sparťanka	k1gFnSc2
zničily	zničit	k5eAaPmAgInP
Pardubice	Pardubice	k1gInPc1
a	a	k8xC
mají	mít	k5eAaImIp3nP
fotbalový	fotbalový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3.05	3.05	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Penaltové	penaltový	k2eAgNnSc1d1
drama	drama	k1gNnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
poháru	pohár	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zfotbal	zfotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
17.05	17.05	k4
<g/>
.2013	.2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Sparta	Sparta	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
na	na	k7c4
penalty	penalta	k1gFnPc4
dramatické	dramatický	k2eAgNnSc4d1
finále	finále	k1gNnSc4
poháru	pohár	k1gInSc2
žen	žena	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zfotbal	zfotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25.05	25.05	k4
<g/>
.2015	.2015	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Finále	finále	k1gNnSc1
poháru	pohár	k1gInSc2
vyhrály	vyhrát	k5eAaPmAgFnP
Sparťanky	Sparťanka	k1gFnPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zeny	zeny	k?
<g/>
.	.	kIx.
<g/>
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
20.05	20.05	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Sparťanky	Sparťanka	k1gFnPc1
obhájily	obhájit	k5eAaPmAgFnP
výhru	výhra	k1gFnSc4
v	v	k7c6
poháru	pohár	k1gInSc6
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zeny	zeny	k?
<g/>
.	.	kIx.
<g/>
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
19.05	19.05	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Sparťanky	Sparťanka	k1gFnPc1
porazily	porazit	k5eAaPmAgFnP
ve	v	k7c6
finále	finále	k1gNnSc6
poháru	pohár	k1gInSc2
Slavii	slavie	k1gFnSc4
na	na	k7c4
penalty	penalta	k1gFnPc4
a	a	k8xC
slaví	slavit	k5eAaImIp3nP
double	double	k2eAgNnSc4d1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zeny	zeny	k?
<g/>
.	.	kIx.
<g/>
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25.05	25.05	k4
<g/>
.2019	.2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Finále	finála	k1gFnSc3
KFŽ	KFŽ	kA
<g/>
:	:	kIx,
Slavia	Slavia	k1gFnSc1
zdolala	zdolat	k5eAaPmAgFnS
Spartu	Sparta	k1gFnSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
nv	nv	k?
<g/>
.	.	kIx.
<g/>
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
květen	květen	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Slávistky	slávistka	k1gFnPc1
vítězem	vítěz	k1gMnSc7
poháru	pohár	k1gInSc2
KFŽ	KFŽ	kA
<g/>
,	,	kIx,
přípravky	přípravek	k1gInPc1
ovládly	ovládnout	k5eAaPmAgInP
Zbrojovačky	Zbrojovačka	k1gFnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zeny	zeny	k?
<g/>
.	.	kIx.
<g/>
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
21.05	21.05	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Menton	menton	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Tournament	Tournament	k1gInSc1
(	(	kIx(
<g/>
Women	Women	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Turbine	Turbin	k1gInSc5
Potsdam	Potsdam	k1gInSc4
unterliegt	unterliegt	k2eAgMnSc1d1
Prag	Prag	k1gMnSc1
erst	erst	k1gMnSc1
im	im	k?
Finale	Finala	k1gFnSc6
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
morgenpost	morgenpost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
,	,	kIx,
27.01	27.01	k4
<g/>
.2019	.2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Mistři	mistr	k1gMnPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
staršího	starý	k2eAgInSc2d2
dorostu	dorost	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
archiv	archiv	k1gInSc1
<g/>
.	.	kIx.
<g/>
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12.12	12.12	k4
<g/>
.2005	.2005	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Archiv	archiv	k1gInSc1
Rudého	rudý	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
1965-19891	1965-19891	k4
2	#num#	k4
Nižší	nízký	k2eAgFnSc2d2
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
(	(	kIx(
<g/>
jfk-fotbal	jfk-fotbal	k1gInSc1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
(	(	kIx(
<g/>
1896	#num#	k4
–	–	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
97	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Libri	Libre	k1gFnSc4
1997.1	1997.1	k4
2	#num#	k4
Archiv	archiv	k1gInSc1
soutěží	soutěžit	k5eAaImIp3nS
<g/>
,	,	kIx,
výsledkový	výsledkový	k2eAgInSc1d1
servis	servis	k1gInSc1
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
"	"	kIx"
<g/>
European	European	k1gInSc1
Cups	Cupsa	k1gFnPc2
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.sparta.cz	www.sparta.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Czech	Czech	k1gMnSc1
Jewish	Jewish	k1gMnSc1
leader	leader	k1gMnSc1
slams	slamsa	k1gFnPc2
anti-Semitic	anti-Semitice	k1gFnPc2
chants	chants	k6eAd1
by	by	kYmCp3nS
Sparta	Sparta	k1gFnSc1
FC	FC	kA
fans	fans	k6eAd1
Haaretz	Haaretz	k1gMnSc1
Daily	Daila	k1gFnSc2
Newspaper	Newspaper	k1gMnSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.sparta.cz	www.sparta.cz	k1gInSc1
–	–	k?
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Sparty	Sparta	k1gFnSc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sparta	Sparta	k1gFnSc1
Forever	Forevero	k1gNnPc2
–	–	k?
Stránky	stránka	k1gFnSc2
sparťanských	sparťanský	k2eAgMnPc2d1
fanoušků	fanoušek	k1gMnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Klubové	klubový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sparťanky	Sparťanka	k1gFnPc1
–	–	k?
Neoficiální	neoficiální	k2eAgFnPc1d1,k2eNgFnPc1d1
stránky	stránka	k1gFnPc1
sparťanských	sparťanský	k2eAgFnPc2d1
fotbalistek	fotbalistka	k1gFnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
E-Sparta	E-Spart	k1gMnSc4
Neoficiální	oficiální	k2eNgInSc1d1,k2eAgInSc1d1
web	web	k1gInSc1
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
fotbal	fotbal	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
fotbalových	fotbalový	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
Domácí	domácí	k1gFnSc1
stadion	stadion	k1gInSc4
</s>
<s>
Generali	Generat	k5eAaImAgMnP,k5eAaPmAgMnP
Arena	Aren	k1gMnSc4
(	(	kIx(
<g/>
Letná	Letná	k1gFnSc1
<g/>
)	)	kIx)
Další	další	k2eAgInPc1d1
týmy	tým	k1gInPc1
</s>
<s>
Rezervní	rezervní	k2eAgInSc1d1
tým	tým	k1gInSc1
•	•	k?
Ženský	ženský	k2eAgInSc1d1
tým	tým	k1gInSc1
Rivalita	rivalita	k1gFnSc1
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1
derby	derby	k1gNnSc1
•	•	k?
Klíč	klíč	k1gInSc1
primátora	primátor	k1gMnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
Evropské	evropský	k2eAgFnSc2d1
poháry	pohár	k1gInPc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
Sparty	Sparta	k1gFnSc2
Praha	Praha	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
fotbalových	fotbalový	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
Další	další	k2eAgInPc4d1
sporty	sport	k1gInPc4
</s>
<s>
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
•	•	k?
Atletika	atletika	k1gFnSc1
•	•	k?
Ragby	ragby	k1gNnSc2
•	•	k?
Tenis	tenis	k1gInSc1
•	•	k?
Basketbal	basketbal	k1gInSc1
•	•	k?
Florbal	florbal	k1gInSc4
Jednotlivé	jednotlivý	k2eAgFnSc2d1
sezony	sezona	k1gFnSc2
</s>
<s>
A	a	k9
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
B	B	kA
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Trenéři	trenér	k1gMnPc1
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
polovina	polovina	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
1907	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Maleček	Malečka	k1gFnPc2
•	•	k?
1911	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Malý	Malý	k1gMnSc1
•	•	k?
1919	#num#	k4
<g/>
–	–	k?
<g/>
1923	#num#	k4
<g/>
:	:	kIx,
Johny	John	k1gMnPc4
Dick	Dicka	k1gFnPc2
•	•	k?
1924	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Špindler	Špindler	k1gMnSc1
•	•	k?
1928	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
:	:	kIx,
Johny	John	k1gMnPc4
Dick	Dicka	k1gFnPc2
•	•	k?
1933	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
:	:	kIx,
Ferenc	Ferenc	k1gMnSc1
Szedlacsek	Szedlacska	k1gFnPc2
•	•	k?
1939	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Kuchynka	Kuchynka	k1gMnSc1
•	•	k?
1945	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
:	:	kIx,
Ferenc	Ferenc	k1gMnSc1
Szedlacsek	Szedlacska	k1gFnPc2
•	•	k?
1948	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
:	:	kIx,
Erich	Erich	k1gMnSc1
Srbek	Srbka	k1gFnPc2
2	#num#	k4
<g/>
.	.	kIx.
polovina	polovina	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
1957	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
<g/>
:	:	kIx,
Erich	Erich	k1gMnSc1
Srbek	Srbka	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Vlastimil	Vlastimil	k1gMnSc1
Preis	Preis	k1gFnSc2
•	•	k?
1958	#num#	k4
<g/>
–	–	k?
<g/>
1959	#num#	k4
<g/>
:	:	kIx,
Vlastimil	Vlastimil	k1gMnSc1
Preis	Preis	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Karel	Karel	k1gMnSc1
Senecký	Senecký	k2eAgMnSc1d1
<g/>
/	/	kIx~
<g/>
Jaroslav	Jaroslav	k1gMnSc1
Šimonek	Šimonka	k1gFnPc2
•	•	k?
1959	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Kolský	Kolský	k1gMnSc1
•	•	k?
1962	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Karel	Karel	k1gMnSc1
Kolský	Kolský	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Jaroslav	Jaroslav	k1gMnSc1
Štumpf	Štumpf	k1gMnSc1
•	•	k?
1963	#num#	k4
<g/>
–	–	k?
<g/>
1964	#num#	k4
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Štumpf	Štumpf	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Václav	Václav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
•	•	k?
1964	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
•	•	k?
1969	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g/>
:	:	kIx,
Milan	Milan	k1gMnSc1
Navara	Navara	k1gFnSc1
•	•	k?
1970	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Karel	Karel	k1gMnSc1
Kolský	Kolský	k1gMnSc1
•	•	k?
1971	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
:	:	kIx,
Tadeáš	Tadeáš	k1gMnSc1
Kraus	Kraus	k1gMnSc1
•	•	k?
1974	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Mráz	Mráz	k1gMnSc1
•	•	k?
1975	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeňka	k1gFnPc2
Roček	roček	k1gInSc1
•	•	k?
1975	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
:	:	kIx,
Štefan	Štefan	k1gMnSc1
Čambal	Čambal	k1gInSc1
<g/>
/	/	kIx~
<g/>
Zdeněk	Zdeněk	k1gMnSc1
Roček	Roček	k1gMnSc1
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Dušan	Dušan	k1gMnSc1
Uhrin	Uhrin	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1977	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
:	:	kIx,
Arnošt	Arnošt	k1gMnSc1
Hložek	hložka	k1gFnPc2
•	•	k?
1978	#num#	k4
<g/>
:	:	kIx,
Antonín	Antonín	k1gMnSc1
Rýgr	Rýgra	k1gFnPc2
•	•	k?
1978	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Rubáš	rubat	k5eAaImIp2nS
•	•	k?
1981	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
:	:	kIx,
Dušan	Dušan	k1gMnSc1
Uhrin	Uhrin	k1gMnSc1
•	•	k?
1983	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
•	•	k?
1984	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1985	#num#	k4
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Táborský	Táborský	k1gMnSc1
•	•	k?
1985	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
:	:	kIx,
Ján	Ján	k1gMnSc1
Zachar	Zachara	k1gFnPc2
•	•	k?
1986	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
•	•	k?
1988	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gInSc4
Jarabinský	Jarabinský	k2eAgInSc4d1
•	•	k?
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
•	•	k?
1991	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1993	#num#	k4
<g/>
:	:	kIx,
Dušan	Dušan	k1gMnSc1
Uhrin	Uhrin	k1gMnSc1
•	•	k?
1993	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
:	:	kIx,
Karol	Karol	k1gInSc1
Dobiaš	Dobiaš	k1gInSc1
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Vladimír	Vladimír	k1gMnSc1
Borovička	Borovička	k1gMnSc1
•	•	k?
1994	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
:	:	kIx,
Jürgen	Jürgen	k1gInSc1
Sundermann	Sundermann	k1gInSc1
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gInSc4
Jarabinský	Jarabinský	k2eAgInSc4d1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Vlastimil	Vlastimil	k1gMnSc1
Petržela	Petržela	k1gMnSc1
•	•	k?
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
•	•	k?
1998	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
•	•	k?
1999	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
:	:	kIx,
Jaroslav	Jaroslava	k1gFnPc2
Hřebík	hřebík	k1gInSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gInSc4
Jarabinský	Jarabinský	k2eAgInSc4d1
•	•	k?
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Kotrba	kotrba	k1gFnSc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Straka	Straka	k1gMnSc1
•	•	k?
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
:	:	kIx,
Jaroslav	Jaroslava	k1gFnPc2
Hřebík	hřebík	k1gInSc1
•	•	k?
2005	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
Stanislav	Stanislav	k1gMnSc1
Griga	Griga	k1gFnSc1
•	•	k?
2006	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
:	:	kIx,
Michal	Michal	k1gMnSc1
Bílek	Bílek	k1gMnSc1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
•	•	k?
2008	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
•	•	k?
2011	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Hašek	Hašek	k1gMnSc1
•	•	k?
2012	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
:	:	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Lavička	Lavička	k1gMnSc1
•	•	k?
2015	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
David	David	k1gMnSc1
Holoubek	Holoubek	k1gMnSc1
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Požár	požár	k1gInSc1
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Rada	rada	k1gFnSc1
•	•	k?
2017	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2018	#num#	k4
<g/>
:	:	kIx,
Andrea	Andrea	k1gFnSc1
Stramaccioni	Stramaccioň	k1gFnSc6
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Hapal	Hapal	k1gMnSc1
•	•	k?
2018	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Ščasný	Ščasný	k2eAgMnSc1d1
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Michal	Michal	k1gMnSc1
Horňák	Horňák	k1gMnSc1
•	•	k?
2019	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Jílek	Jílek	k1gMnSc1
•	•	k?
2020	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Kotal	Kotal	k1gMnSc1
•	•	k?
2021	#num#	k4
<g/>
–	–	k?
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Vrba	Vrba	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
historie	historie	k1gFnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
klubů	klub	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
SK	Sk	kA
Dynamo	dynamo	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
•	•	k?
MFK	MFK	kA
Karviná	Karviná	k1gFnSc1
•	•	k?
FC	FC	kA
Slovan	Slovan	k1gInSc1
Liberec	Liberec	k1gInSc1
•	•	k?
FK	FK	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
SK	Sk	kA
Sigma	sigma	k1gNnSc2
Olomouc	Olomouc	k1gFnSc1
•	•	k?
SFC	SFC	kA
Opava	Opava	k1gFnSc1
•	•	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
•	•	k?
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
1905	#num#	k4
•	•	k?
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Příbram	Příbram	k1gFnSc1
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
•	•	k?
FK	FK	kA
Teplice	teplice	k1gFnSc2
•	•	k?
FC	FC	kA
Fastav	Fastav	k1gFnSc2
Zlín	Zlín	k1gInSc1
•	•	k?
FK	FK	kA
Pardubice	Pardubice	k1gInPc1
•	•	k?
FC	FC	kA
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
Stadiony	stadion	k1gInPc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Jablonec	Jablonec	k1gInSc1
•	•	k?
Karviná	Karviná	k1gFnSc1
•	•	k?
Liberec	Liberec	k1gInSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
•	•	k?
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
Příbram	Příbram	k1gFnSc1
•	•	k?
Slovácko	Slovácko	k1gNnSc1
•	•	k?
Teplice	teplice	k1gFnSc2
•	•	k?
Zlín	Zlín	k1gInSc1
•	•	k?
Pardubice	Pardubice	k1gInPc1
•	•	k?
Brno	Brno	k1gNnSc1
Sezóny	sezóna	k1gFnSc2
(	(	kIx(
<g/>
historický	historický	k2eAgInSc4d1
přehled	přehled	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Československo	Československo	k1gNnSc1
<g/>
:	:	kIx,
1925	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
•	•	k?
1927	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
:	:	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
Československo	Československo	k1gNnSc1
<g/>
:	:	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1949	#num#	k4
•	•	k?
1950	#num#	k4
•	•	k?
1951	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1954	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
Česko	Česko	k1gNnSc1
<g/>
:	:	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
v	v	k7c6
ČSR	ČSR	kA
éře	éra	k1gFnSc6
pouze	pouze	k6eAd1
české	český	k2eAgInPc1d1
kluby	klub	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
SK	Sk	kA
Čechie	Čechie	k1gFnSc1
Praha	Praha	k1gFnSc1
VIII	VIII	kA
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slavoj	Slavoj	k1gInSc1
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nuselský	nuselský	k2eAgInSc4d1
SK	Sk	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
ČAFC	ČAFC	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Meteor	meteor	k1gInSc1
Praha	Praha	k1gFnSc1
VIII	VIII	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFK	AFK	kA
Kolín	Kolín	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DFC	DFC	kA
Prag	Prag	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DSV	DSV	kA
Saaz	Saaz	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Teplitzer	Teplitzer	k1gInSc1
FK	FK	kA
(	(	kIx(
<g/>
1935	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Moravská	moravský	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Rusj	Rusj	k1gInSc1
Užhorod	Užhorod	k1gInSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Náchod	Náchod	k1gInSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Nusle	Nusle	k1gFnPc1
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Polaban	Polaban	k1gMnSc1
Nymburk	Nymburk	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SK	Sk	kA
Prostějov	Prostějov	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Rakovník	Rakovník	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Libeň	Libeň	k1gFnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Olomouc	Olomouc	k1gFnSc1
ASO	ASO	kA
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Čechie	Čechie	k1gFnSc1
Karlín	Karlín	k1gInSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
•	•	k?
ČAFC	ČAFC	kA
Židenice	Židenice	k1gFnSc2
2011	#num#	k4
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slavoj	Slavoj	k1gInSc1
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VTJ	VTJ	kA
Dukla	Dukla	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jiskra	jiskra	k1gFnSc1
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Dukla	Dukla	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
)	)	kIx)
•	•	k?
TJ	tj	kA
Rudá	rudý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
LeRK	LeRK	k1gFnSc7
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Motorlet	motorlet	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
SK	Sk	kA
Baťov	Baťov	k1gInSc4
1930	#num#	k4
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Pardubice	Pardubice	k1gInPc1
1899	#num#	k4
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinec	Třinec	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MFK	MFK	kA
Frýdek-Místek	Frýdek-Místek	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
MFK	MFK	kA
Vítkovice	Vítkovice	k1gInPc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Benešov	Benešov	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Union	union	k1gInSc1
Cheb	Cheb	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
SYNOT	SYNOT	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFK	AFK	kA
Atlantic	Atlantice	k1gFnPc2
Lázně	lázeň	k1gFnSc2
Bohdaneč	Bohdaneč	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Karviná	Karviná	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Drnovice	Drnovice	k1gInPc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Chmel	chmel	k1gInSc4
Blšany	Blšana	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Baník	Baník	k1gInSc1
Most	most	k1gInSc1
1909	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Kladno	Kladno	k1gNnSc4
(	(	kIx(
<g/>
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Ústí	ústit	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Viktoria	Viktoria	k1gFnSc1
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SC	SC	kA
Znojmo	Znojmo	k1gNnSc1
FK	FK	kA
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Vysočina	vysočina	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
a	a	k8xC
trenér	trenér	k1gMnSc1
měsíce	měsíc	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
•	•	k?
Přihraj	přihrát	k5eAaPmRp2nS
<g/>
:	:	kIx,
<g/>
Král	Král	k1gMnSc1
asistencí	asistence	k1gFnPc2
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
•	•	k?
Česko-slovenský	česko-slovenský	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
•	•	k?
Fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Československa	Československo	k1gNnSc2
•	•	k?
Kluby	klub	k1gInPc7
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
•	•	k?
Pražské	pražský	k2eAgNnSc1d1
derby	derby	k1gNnSc1
Fotbal	fotbal	k1gInSc4
ženy	žena	k1gFnSc2
<g/>
:	:	kIx,
I.	I.	kA
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
•	•	k?
Česká	český	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
olak	olak	k1gInSc1
<g/>
2002158693	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131848648	#num#	k4
</s>
