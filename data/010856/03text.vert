<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Paul	Paul	k1gMnSc1	Paul
Labor	Labor	k1gMnSc1	Labor
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1842	[number]	k4	1842
Hořovice	Hořovice	k1gFnPc4	Hořovice
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1924	[number]	k4	1924
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
amatérským	amatérský	k2eAgMnSc7d1	amatérský
muzikantem	muzikant	k1gMnSc7	muzikant
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
domácnost	domácnost	k1gFnSc1	domácnost
byla	být	k5eAaImAgFnS	být
centrem	centrum	k1gNnSc7	centrum
hudebního	hudební	k2eAgNnSc2d1	hudební
dění	dění	k1gNnSc2	dění
Hořovic	Hořovice	k1gFnPc2	Hořovice
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
slavný	slavný	k2eAgMnSc1d1	slavný
český	český	k2eAgMnSc1d1	český
houslista	houslista	k1gMnSc1	houslista
Josef	Josef	k1gMnSc1	Josef
Slavík	Slavík	k1gMnSc1	Slavík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
letech	let	k1gInPc6	let
následkem	následkem	k7c2	následkem
neštovic	neštovice	k1gFnPc2	neštovice
oslepl	oslepnout	k5eAaPmAgInS	oslepnout
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
chlapec	chlapec	k1gMnSc1	chlapec
tam	tam	k6eAd1	tam
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
c.	c.	k?	c.
k.	k.	k?	k.
Slepecký	slepecký	k2eAgInSc1d1	slepecký
výchovný	výchovný	k2eAgInSc1d1	výchovný
ústav	ústav	k1gInSc1	ústav
Matthiase	Matthiasa	k1gFnSc3	Matthiasa
Schlechtera	Schlechter	k1gMnSc2	Schlechter
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
studoval	studovat	k5eAaImAgMnS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
kompozici	kompozice	k1gFnSc4	kompozice
na	na	k7c4	na
Konservatorium	Konservatorium	k1gNnSc4	Konservatorium
der	drát	k5eAaImRp2nS	drát
Gesellschaft	Gesellschaft	k1gInSc1	Gesellschaft
der	drát	k5eAaImRp2nS	drát
Musikfreunde	Musikfreund	k1gMnSc5	Musikfreund
(	(	kIx(	(
<g/>
Konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
společnosti	společnost	k1gFnSc2	společnost
přátel	přítel	k1gMnPc2	přítel
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
jako	jako	k8xS	jako
klavírista	klavírista	k1gMnSc1	klavírista
i	i	k9	i
jako	jako	k9	jako
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
sérii	série	k1gFnSc4	série
velmi	velmi	k6eAd1	velmi
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
hlavních	hlavní	k2eAgNnPc6d1	hlavní
evropských	evropský	k2eAgNnPc6d1	Evropské
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
hannoverským	hannoverský	k2eAgMnSc7d1	hannoverský
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
V.	V.	kA	V.
Hannoverským	hannoverský	k2eAgMnSc7d1	hannoverský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
ho	on	k3xPp3gMnSc4	on
Královským	královský	k2eAgMnSc7d1	královský
komorním	komorní	k2eAgMnSc7d1	komorní
klavíristou	klavírista	k1gMnSc7	klavírista
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgInS	svěřit
mu	on	k3xPp3gMnSc3	on
výuku	výuka	k1gFnSc4	výuka
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
se	se	k3xPyFc4	se
Labor	Labor	k1gMnSc1	Labor
usadil	usadit	k5eAaPmAgMnS	usadit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
učitelem	učitel	k1gMnSc7	učitel
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
studoval	studovat	k5eAaImAgMnS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zanedlouho	zanedlouho	k6eAd1	zanedlouho
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
rovněž	rovněž	k9	rovněž
vynikajících	vynikající	k2eAgInPc2d1	vynikající
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
obdržel	obdržet	k5eAaPmAgInS	obdržet
titul	titul	k1gInSc1	titul
Císařský	císařský	k2eAgInSc1d1	císařský
a	a	k8xC	a
královský	královský	k2eAgMnSc1d1	královský
dvorní	dvorní	k2eAgMnSc1d1	dvorní
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
varhanní	varhanní	k2eAgFnPc1d1	varhanní
skladby	skladba	k1gFnPc1	skladba
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejhranější	hraný	k2eAgNnSc4d3	nejhranější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc2d1	založená
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
německé	německý	k2eAgFnSc2d1	německá
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
redakce	redakce	k1gFnSc2	redakce
edic	edice	k1gFnPc2	edice
Deutsche	Deutsche	k1gNnSc1	Deutsche
Tonkunst	Tonkunst	k1gMnSc1	Tonkunst
in	in	k?	in
Österreich	Österreich	k1gMnSc1	Österreich
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
i	i	k9	i
na	na	k7c6	na
vydávání	vydávání	k1gNnSc6	vydávání
skladeb	skladba	k1gFnPc2	skladba
rakouských	rakouský	k2eAgMnPc2d1	rakouský
císařů	císař	k1gMnPc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kaiserwerke	Kaiserwerk	k1gFnSc2	Kaiserwerk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Labor	Labor	k1gInSc1	Labor
měl	mít	k5eAaImAgInS	mít
hluboký	hluboký	k2eAgInSc1d1	hluboký
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
starou	starý	k2eAgFnSc4d1	stará
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
generální	generální	k2eAgInSc1d1	generální
bas	bas	k1gInSc1	bas
k	k	k7c3	k
houslovým	houslový	k2eAgFnPc3d1	houslová
sonátám	sonáta	k1gFnPc3	sonáta
Heinricha	Heinrich	k1gMnSc2	Heinrich
Ignáce	Ignác	k1gMnSc2	Ignác
Bibera	Biber	k1gMnSc2	Biber
a	a	k8xC	a
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Pomo	Pomo	k6eAd1	Pomo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
oro	oro	k?	oro
Antonia	Antonio	k1gMnSc4	Antonio
Cesti	Cesť	k1gFnSc2	Cesť
<g/>
.	.	kIx.	.
</s>
<s>
Slavil	slavit	k5eAaImAgInS	slavit
i	i	k9	i
velké	velký	k2eAgInPc4d1	velký
úspěchy	úspěch	k1gInPc4	úspěch
jako	jako	k9	jako
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
učitelem	učitel	k1gMnSc7	učitel
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
jako	jako	k8xC	jako
např.	např.	kA	např.
Almy	alma	k1gFnSc2	alma
Mahlerové	Mahlerová	k1gFnSc2	Mahlerová
<g/>
,	,	kIx,	,
Paula	Paul	k1gMnSc2	Paul
Wittgensteina	Wittgenstein	k1gMnSc2	Wittgenstein
<g/>
,	,	kIx,	,
či	či	k8xC	či
Arnolda	Arnold	k1gMnSc2	Arnold
Schoenberga	Schoenberg	k1gMnSc2	Schoenberg
<g/>
.	.	kIx.	.
</s>
<s>
Alma	alma	k1gFnSc1	alma
Mahlerová	Mahlerový	k2eAgFnSc1d1	Mahlerový
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
denících	deník	k1gInPc6	deník
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
mnoho	mnoho	k4c4	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
oblíbeném	oblíbený	k2eAgMnSc6d1	oblíbený
učiteli	učitel	k1gMnSc6	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
notové	notový	k2eAgNnSc4d1	notové
písmo	písmo	k1gNnSc4	písmo
pro	pro	k7c4	pro
slepce	slepec	k1gMnPc4	slepec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
rozšíření	rozšíření	k1gNnSc1	rozšíření
se	se	k3xPyFc4	se
však	však	k9	však
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Paula	Paul	k1gMnSc2	Paul
Wittgensteina	Wittgenstein	k1gMnSc2	Wittgenstein
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hudebních	hudební	k2eAgInPc6d1	hudební
večírcích	večírek	k1gInPc6	večírek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
pořádal	pořádat	k5eAaImAgMnS	pořádat
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
předními	přední	k2eAgFnPc7d1	přední
vídeňskými	vídeňský	k2eAgFnPc7d1	Vídeňská
hudebníky	hudebník	k1gMnPc4	hudebník
(	(	kIx(	(
<g/>
Johannes	Johannes	k1gMnSc1	Johannes
Brahms	Brahms	k1gMnSc1	Brahms
<g/>
,	,	kIx,	,
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
Walter	Walter	k1gMnSc1	Walter
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
o	o	k7c4	o
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Labor	Labor	k1gInSc1	Labor
první	první	k4xOgInSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
skladby	skladba	k1gFnPc4	skladba
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Paula	Paul	k1gMnSc2	Paul
Wittgensteina	Wittgensteino	k1gNnSc2	Wittgensteino
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
Ludwig	Ludwig	k1gMnSc1	Ludwig
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
<g/>
,	,	kIx,	,
řadil	řadit	k5eAaImAgMnS	řadit
Labora	Labor	k1gMnSc4	Labor
mezi	mezi	k7c4	mezi
šest	šest	k4xCc4	šest
skutečně	skutečně	k6eAd1	skutečně
velkých	velký	k2eAgMnPc2d1	velký
skladatelů	skladatel	k1gMnPc2	skladatel
vedle	vedle	k7c2	vedle
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
,	,	kIx,	,
Haydna	Haydna	k1gFnSc1	Haydna
<g/>
,	,	kIx,	,
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
,	,	kIx,	,
Schuberta	Schubert	k1gMnSc2	Schubert
a	a	k8xC	a
Brahmse	Brahms	k1gMnSc2	Brahms
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
před	před	k7c7	před
skladatelovou	skladatelův	k2eAgFnSc7d1	skladatelova
smrtí	smrt	k1gFnSc7	smrt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
založen	založen	k2eAgInSc1d1	založen
Svaz	svaz	k1gInSc1	svaz
Josefa	Josef	k1gMnSc2	Josef
Labora	Labor	k1gMnSc2	Labor
(	(	kIx(	(
<g/>
Josef-Labor-Bund	Josef-Labor-Bund	k1gInSc1	Josef-Labor-Bund
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
propagovat	propagovat	k5eAaImF	propagovat
skladatelova	skladatelův	k2eAgNnPc4d1	skladatelovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
přes	přes	k7c4	přes
80	[number]	k4	80
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Komponoval	komponovat	k5eAaImAgMnS	komponovat
především	především	k9	především
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
a	a	k8xC	a
komorní	komorní	k2eAgFnSc4d1	komorní
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
děl	dělo	k1gNnPc2	dělo
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
německé	německý	k2eAgFnSc2d1	německá
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
podporou	podpora	k1gFnSc7	podpora
vydána	vydat	k5eAaPmNgFnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
ve	v	k7c4	v
Wienbibliothek	Wienbibliothek	k1gInSc4	Wienbibliothek
im	im	k?	im
Rathaus	rathaus	k1gInSc1	rathaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Instrumentální	instrumentální	k2eAgInPc4d1	instrumentální
koncerty	koncert	k1gInPc4	koncert
===	===	k?	===
</s>
</p>
<p>
<s>
Koncert	koncert	k1gInSc1	koncert
e-moll	eolnout	k5eAaPmAgInS	e-molnout
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncertní	koncertní	k2eAgInSc1d1	koncertní
kus	kus	k1gInSc1	kus
h-moll	holnout	k5eAaPmAgInS	h-molnout
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
(	(	kIx(	(
<g/>
před	před	k7c7	před
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncertní	koncertní	k2eAgInSc1d1	koncertní
kus	kus	k1gInSc1	kus
D-dur	Dura	k1gFnPc2	D-dura
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncertní	koncertní	k2eAgInSc1d1	koncertní
kus	kus	k1gInSc1	kus
f-moll	folnout	k5eAaPmAgInS	f-molnout
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncert	koncert	k1gInSc1	koncert
G-dur	Gura	k1gFnPc2	G-dura
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Kvintet	kvintet	k1gInSc1	kvintet
e	e	k0	e
moll	moll	k1gNnSc6	moll
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
kontrabas	kontrabas	k1gInSc1	kontrabas
a	a	k8xC	a
klavír	klavír	k1gInSc1	klavír
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
3	[number]	k4	3
(	(	kIx(	(
<g/>
před	před	k7c7	před
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
(	(	kIx(	(
<g/>
před	před	k7c7	před
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
A-dur	Aura	k1gFnPc2	A-dura
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
7	[number]	k4	7
(	(	kIx(	(
<g/>
před	před	k7c7	před
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgInSc1d1	klavírní
kvintet	kvintet	k1gInSc1	kvintet
C-dur	Cur	k1gMnSc1	C-dur
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
6	[number]	k4	6
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kvintet	kvintet	k1gInSc1	kvintet
in	in	k?	in
D-dur	Dura	k1gFnPc2	D-dura
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc1	klavír
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
11	[number]	k4	11
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trio	trio	k1gNnSc1	trio
e	e	k0	e
moll	moll	k1gNnSc6	moll
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
jednou	jednou	k6eAd1	jednou
rukou	ruka	k1gFnPc2	ruka
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trio	trio	k1gNnSc4	trio
g	g	kA	g
moll	moll	k1gNnPc2	moll
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
F-dur	Fura	k1gFnPc2	F-dura
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
d-moll	doll	k1gMnSc1	d-moll
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
5	[number]	k4	5
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
před	před	k7c7	před
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
A-dur	Aura	k1gFnPc2	A-dura
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
E-dur	Eura	k1gFnPc2	E-dura
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
jednou	jednou	k6eAd1	jednou
rukou	ruka	k1gFnPc2	ruka
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kvintet	kvintet	k1gInSc1	kvintet
c-moll	colla	k1gFnPc2	c-molla
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
jednou	jednou	k6eAd1	jednou
rukou	ruka	k1gFnPc2	ruka
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kvintet	kvintet	k1gInSc1	kvintet
D-dur	Dura	k1gFnPc2	D-dura
pro	pro	k7c4	pro
hoboj	hoboj	k1gFnSc4	hoboj
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
fagot	fagot	k1gInSc4	fagot
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Klavírní	klavírní	k2eAgFnPc4d1	klavírní
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Fantasie	fantasie	k1gFnSc1	fantasie
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
1	[number]	k4	1
na	na	k7c4	na
původní	původní	k2eAgNnSc4d1	původní
téma	téma	k1gNnSc4	téma
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
(	(	kIx(	(
<g/>
před	před	k7c7	před
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Carla	Carlo	k1gNnSc2	Carlo
Czerného	Czerný	k2eAgMnSc4d1	Czerný
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
4	[number]	k4	4
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
5	[number]	k4	5
klavírních	klavírní	k2eAgInPc2d1	klavírní
kusů	kus	k1gInPc2	kus
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
8	[number]	k4	8
(	(	kIx(	(
<g/>
před	před	k7c7	před
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Capriccio	capriccio	k1gNnSc4	capriccio
Big	Big	k1gFnSc2	Big
Ben	Ben	k1gInSc4	Ben
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
Chopinovo	Chopinův	k2eAgNnSc4d1	Chopinovo
téma	téma	k1gNnSc4	téma
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Varhanní	varhanní	k2eAgFnPc4d1	varhanní
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fantasie	fantasie	k1gFnSc1	fantasie
na	na	k7c4	na
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
hymnu	hymna	k1gFnSc4	hymna
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
panování	panování	k1gNnSc2	panování
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
18	[number]	k4	18
gregoriánských	gregoriánský	k2eAgFnPc2d1	gregoriánská
preludií	preludie	k1gFnPc2	preludie
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
h	h	k?	h
moll	moll	k1gNnSc1	moll
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
15	[number]	k4	15
(	(	kIx(	(
<g/>
před	před	k7c7	před
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
c	c	k0	c
moll	moll	k1gNnSc6	moll
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
BACH	BACH	kA	BACH
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vokální	vokální	k2eAgFnPc4d1	vokální
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Liederkranz	Liederkranz	k1gInSc1	Liederkranz
(	(	kIx(	(
<g/>
Kruh	kruh	k1gInSc1	kruh
písní	píseň	k1gFnPc2	píseň
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zpěv	zpěv	k1gInSc4	zpěv
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
texty	text	k1gInPc4	text
Robert	Robert	k1gMnSc1	Robert
Raab	Raab	k1gMnSc1	Raab
<g/>
,	,	kIx,	,
před	před	k7c7	před
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Justus	Justus	k1gInSc1	Justus
ut	ut	k?	ut
palma	palma	k1gFnSc1	palma
florebit	florebit	k5eAaPmF	florebit
pro	pro	k7c4	pro
zpěv	zpěv	k1gInSc4	zpěv
sólo	sólo	k1gNnSc4	sólo
<g/>
,	,	kIx,	,
smíšený	smíšený	k2eAgInSc1d1	smíšený
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
varhany	varhany	k1gInPc1	varhany
(	(	kIx(	(
<g/>
texty	text	k1gInPc1	text
žalmů	žalm	k1gInPc2	žalm
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sechs	Sechs	k1gInSc1	Sechs
Kanons	Kanons	k1gInSc1	Kanons
(	(	kIx(	(
<g/>
Šest	šest	k4xCc4	šest
kánonů	kánon	k1gInPc2	kánon
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
ženské	ženský	k2eAgInPc4d1	ženský
hlasy	hlas	k1gInPc4	hlas
(	(	kIx(	(
<g/>
texty	text	k1gInPc4	text
hrabě	hrabě	k1gMnSc1	hrabě
von	von	k1gInSc4	von
Platen	Platen	k2eAgInSc4d1	Platen
<g/>
,	,	kIx,	,
Goethe	Goethe	k1gInSc1	Goethe
<g/>
,	,	kIx,	,
K.	K.	kA	K.
R.	R.	kA	R.
Tauner	Taunra	k1gFnPc2	Taunra
<g/>
,	,	kIx,	,
Ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Chamisso	Chamissa	k1gFnSc5	Chamissa
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ein	Ein	k?	Ein
Lied	Lied	k1gInSc1	Lied
der	drát	k5eAaImRp2nS	drát
Trauer	Trauer	k1gInSc4	Trauer
(	(	kIx(	(
<g/>
Smuteční	smuteční	k2eAgFnSc4d1	smuteční
píseň	píseň	k1gFnSc4	píseň
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
sóla	sólo	k1gNnPc4	sólo
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
varhany	varhany	k1gInPc1	varhany
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pater	patro	k1gNnPc2	patro
noster	nostra	k1gFnPc2	nostra
pro	pro	k7c4	pro
mužský	mužský	k2eAgInSc4d1	mužský
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
varhany	varhany	k1gInPc4	varhany
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
16	[number]	k4	16
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ave	ave	k1gNnSc1	ave
Maria	Mario	k1gMnSc2	Mario
in	in	k?	in
F	F	kA	F
pro	pro	k7c4	pro
3	[number]	k4	3
ženské	ženský	k2eAgInPc4d1	ženský
hlasy	hlas	k1gInPc4	hlas
a	a	k8xC	a
varhany	varhany	k1gInPc4	varhany
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Missa	Missa	k1gFnSc1	Missa
in	in	k?	in
A	a	k9	a
pro	pro	k7c4	pro
sóla	sólo	k1gNnPc4	sólo
<g/>
,	,	kIx,	,
smíšený	smíšený	k2eAgInSc1d1	smíšený
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
varhany	varhany	k1gInPc1	varhany
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Begrabe	Begrabat	k5eAaPmIp3nS	Begrabat
deine	deinout	k5eAaPmIp3nS	deinout
Toten	toten	k1gInSc1	toten
(	(	kIx(	(
<g/>
Pohřbi	pohřbít	k5eAaPmRp2nS	pohřbít
své	svůj	k3xOyFgNnSc4	svůj
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
mezzosoprán	mezzosoprán	k1gInSc4	mezzosoprán
a	a	k8xC	a
varhany	varhany	k1gInPc4	varhany
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
hudební	hudební	k2eAgInSc1d1	hudební
slovník	slovník	k1gInSc1	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
I.	I.	kA	I.
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
–	–	k?	–
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
hudební	hudební	k2eAgNnSc1d1	hudební
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Österreichisches	Österreichisches	k1gInSc1	Österreichisches
Biographisches	Biographischesa	k1gFnPc2	Biographischesa
Lexikon	lexikon	k1gNnSc1	lexikon
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
4	[number]	k4	4
(	(	kIx(	(
<g/>
Wien	Wien	k1gInSc1	Wien
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neue	Neue	k6eAd1	Neue
deutsche	deutschat	k5eAaPmIp3nS	deutschat
Biographie	Biographie	k1gFnSc1	Biographie
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
13	[number]	k4	13
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Czeike	Czeike	k1gFnSc1	Czeike
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
<g/>
:	:	kIx,	:
Historisches	Historisches	k1gInSc1	Historisches
Lexikon	lexikon	k1gNnSc1	lexikon
Wien	Wien	k1gInSc1	Wien
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
3	[number]	k4	3
(	(	kIx(	(
<g/>
Wien	Wien	k1gInSc1	Wien
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lexikon	lexikon	k1gNnSc1	lexikon
zeitgenössischer	zeitgenössischra	k1gFnPc2	zeitgenössischra
Musik	musika	k1gFnPc2	musika
aus	aus	k?	aus
Österreich	Österreich	k1gMnSc1	Österreich
(	(	kIx(	(
<g/>
Wien	Wien	k1gInSc1	Wien
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
s.	s.	k?	s.
51	[number]	k4	51
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Musik	musika	k1gFnPc2	musika
in	in	k?	in
Geschichte	Geschicht	k1gInSc5	Geschicht
und	und	k?	und
Gegenwart	Gegenwart	k1gInSc1	Gegenwart
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
Finscher	Finschra	k1gFnPc2	Finschra
<g/>
,	,	kIx,	,
Kassel	Kassel	k1gInSc1	Kassel
<g/>
–	–	k?	–
<g/>
Basel	Basel	k1gInSc1	Basel
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grove	Grovat	k5eAaPmIp3nS	Grovat
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dictionary	Dictionar	k1gInPc7	Dictionar
of	of	k?	of
Music	Music	k1gMnSc1	Music
and	and	k?	and
Musicians	Musicians	k1gInSc1	Musicians
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k1gFnSc1	Eric
Blom	Bloma	k1gFnPc2	Bloma
<g/>
,	,	kIx,	,
9	[number]	k4	9
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
5	[number]	k4	5
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oesterreichisches	Oesterreichisches	k1gMnSc1	Oesterreichisches
Musiklexikon	Musiklexikon	k1gMnSc1	Musiklexikon
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
3	[number]	k4	3
(	(	kIx(	(
<g/>
Wien	Wien	k1gInSc1	Wien
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moser	Moser	k1gMnSc1	Moser
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Joachim	Joachim	k1gMnSc1	Joachim
<g/>
:	:	kIx,	:
Blinde	Blind	k1gInSc5	Blind
Musiker	Musiker	k1gInSc4	Musiker
aus	aus	k?	aus
7	[number]	k4	7
Jahrhunderten	Jahrhundertno	k1gNnPc2	Jahrhundertno
(	(	kIx(	(
<g/>
Hamburg	Hamburg	k1gInSc1	Hamburg
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kundi	Kund	k1gMnPc1	Kund
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Labor	Labor	k1gMnSc1	Labor
<g/>
.	.	kIx.	.
</s>
<s>
Sein	Seina	k1gFnPc2	Seina
Leben	Lebna	k1gFnPc2	Lebna
<g/>
,	,	kIx,	,
sein	sein	k1gMnSc1	sein
Klavier-	Klavier-	k1gFnSc2	Klavier-
und	und	k?	und
Orgelwerk	Orgelwerk	k1gInSc1	Orgelwerk
nebst	nebst	k1gFnSc4	nebst
themat	thema	k1gNnPc2	thema
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
sämtlicher	sämtlichra	k1gFnPc2	sämtlichra
Kompositionen	Kompositionna	k1gFnPc2	Kompositionna
(	(	kIx(	(
<g/>
Disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
Universität	Universität	k1gMnSc1	Universität
Wien	Wien	k1gMnSc1	Wien
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Labor	Labor	k1gMnSc1	Labor
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaBmAgMnS	dít
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Paula	Paul	k1gMnSc2	Paul
Labora	Labor	k1gMnSc2	Labor
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Labor	Labor	k1gMnSc1	Labor
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
hudebním	hudební	k2eAgInSc6d1	hudební
slovníku	slovník	k1gInSc6	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
</s>
</p>
<p>
<s>
Varhanní	varhanní	k2eAgFnSc1d1	varhanní
sonáta	sonáta	k1gFnSc1	sonáta
h	h	k?	h
moll	moll	k1gNnPc2	moll
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.15	.15	k4	.15
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ciacona	ciacona	k1gFnSc1	ciacona
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fantasie	fantasie	k1gFnSc1	fantasie
pro	pro	k7c4	pro
dva	dva	k4xCgMnPc4	dva
hráče	hráč	k1gMnPc4	hráč
(	(	kIx(	(
<g/>
Youtube	Youtub	k1gInSc5	Youtub
<g/>
)	)	kIx)	)
</s>
</p>
