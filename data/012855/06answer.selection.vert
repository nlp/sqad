<s>
Ornitologie	ornitologie	k1gFnSc1	ornitologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckých	řecký	k2eAgFnPc2d1	řecká
slov	slovo	k1gNnPc2	slovo
ornis	ornis	k1gFnSc2	ornis
–	–	k?	–
pták	pták	k1gMnSc1	pták
a	a	k8xC	a
logos	logos	k1gInSc1	logos
–	–	k?	–
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zoologická	zoologický	k2eAgFnSc1d1	zoologická
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
