<p>
<s>
Ornitologie	ornitologie	k1gFnSc1	ornitologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckých	řecký	k2eAgFnPc2d1	řecká
slov	slovo	k1gNnPc2	slovo
ornis	ornis	k1gFnSc2	ornis
–	–	k?	–
pták	pták	k1gMnSc1	pták
a	a	k8xC	a
logos	logos	k1gInSc1	logos
–	–	k?	–
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zoologická	zoologický	k2eAgFnSc1d1	zoologická
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
ornitologie	ornitologie	k1gFnSc2	ornitologie
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
trendy	trend	k1gInPc7	trend
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
pouhého	pouhý	k2eAgInSc2d1	pouhý
popisu	popis	k1gInSc2	popis
ptáků	pták	k1gMnPc2	pták
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
vzorců	vzorec	k1gInPc2	vzorec
a	a	k8xC	a
pochopení	pochopení	k1gNnSc4	pochopení
procesů	proces	k1gInPc2	proces
s	s	k7c7	s
vytvářením	vytváření	k1gNnSc7	vytváření
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Člověk	člověk	k1gMnSc1	člověk
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
ptáky	pták	k1gMnPc4	pták
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
a	a	k8xC	a
kamenné	kamenný	k2eAgFnSc2d1	kamenná
kresby	kresba	k1gFnSc2	kresba
v	v	k7c6	v
jeskynních	jeskynní	k2eAgFnPc6d1	jeskynní
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
záznamů	záznam	k1gInPc2	záznam
dokazujících	dokazující	k2eAgInPc2d1	dokazující
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
ptactvo	ptactvo	k1gNnSc4	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
byli	být	k5eAaImAgMnP	být
důležití	důležitý	k2eAgMnPc1d1	důležitý
také	také	k9	také
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vykopávkách	vykopávka	k1gFnPc6	vykopávka
osady	osada	k1gFnSc2	osada
z	z	k7c2	z
rané	raný	k2eAgFnSc2d1	raná
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
až	až	k9	až
80	[number]	k4	80
druhů	druh	k1gInPc2	druh
kostí	kost	k1gFnPc2	kost
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
kulturách	kultura	k1gFnPc6	kultura
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
mají	mít	k5eAaImIp3nP	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
vztahující	vztahující	k2eAgNnPc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnPc1d1	tradiční
ptačí	ptačí	k2eAgNnPc1d1	ptačí
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
podrobné	podrobný	k2eAgFnPc4d1	podrobná
znalosti	znalost	k1gFnPc4	znalost
jejich	jejich	k3xOp3gNnSc4	jejich
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
jmen	jméno	k1gNnPc2	jméno
je	být	k5eAaImIp3nS	být
zvukomalebných	zvukomalebný	k2eAgInPc2d1	zvukomalebný
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
používána	používán	k2eAgNnPc1d1	používáno
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
tradic	tradice	k1gFnPc2	tradice
je	být	k5eAaImIp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jejich	jejich	k3xOp3gInSc2	jejich
významu	význam	k1gInSc2	význam
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgFnPc2	tento
informací	informace	k1gFnPc2	informace
prošla	projít	k5eAaPmAgFnS	projít
ústní	ústní	k2eAgFnSc7d1	ústní
tradicí	tradice	k1gFnSc7	tradice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
ethno-ornitologie	ethnornitologie	k1gFnSc2	ethno-ornitologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
ptáků	pták	k1gMnPc2	pták
rovněž	rovněž	k9	rovněž
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
značné	značný	k2eAgFnPc4d1	značná
znalosti	znalost	k1gFnPc4	znalost
jejich	jejich	k3xOp3gInPc2	jejich
zvyků	zvyk	k1gInPc2	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
sokolnictví	sokolnictví	k1gNnSc2	sokolnictví
se	se	k3xPyFc4	se
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgNnSc1d1	umělé
líhnutí	líhnutí	k1gNnSc1	líhnutí
drůbeže	drůbež	k1gFnSc2	drůbež
bylo	být	k5eAaImAgNnS	být
praktikováno	praktikovat	k5eAaImNgNnS	praktikovat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
246	[number]	k4	246
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
také	také	k9	také
ukázali	ukázat	k5eAaPmAgMnP	ukázat
velkou	velký	k2eAgFnSc4d1	velká
znalost	znalost	k1gFnSc4	znalost
ptáků	pták	k1gMnPc2	pták
využíváním	využívání	k1gNnSc7	využívání
ptačích	ptačí	k2eAgInPc2d1	ptačí
symbolů	symbol	k1gInPc2	symbol
v	v	k7c6	v
hieroglyfech	hieroglyf	k1gInPc6	hieroglyf
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
stylizované	stylizovaný	k2eAgInPc1d1	stylizovaný
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
rozpoznatelné	rozpoznatelný	k2eAgFnPc1d1	rozpoznatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
písemných	písemný	k2eAgInPc2d1	písemný
záznamů	záznam	k1gInPc2	záznam
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
cenné	cenný	k2eAgFnPc1d1	cenná
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
minulosti	minulost	k1gFnSc6	minulost
rozdělení	rozdělení	k1gNnSc2	rozdělení
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Xenofónovy	Xenofónův	k2eAgInPc1d1	Xenofónův
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
pštrosa	pštros	k1gMnSc2	pštros
v	v	k7c6	v
Asýrii	Asýrie	k1gFnSc6	Asýrie
(	(	kIx(	(
<g/>
Anabasis	Anabasis	k1gInSc1	Anabasis
<g/>
,	,	kIx,	,
i.	i.	k?	i.
5	[number]	k4	5
<g/>
)	)	kIx)	)
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	on	k3xPp3gMnSc4	on
vyhynulého	vyhynulý	k2eAgMnSc4d1	vyhynulý
druha	druh	k1gMnSc4	druh
našli	najít	k5eAaPmAgMnP	najít
archeologové	archeolog	k1gMnPc1	archeolog
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
pštrosí	pštrosí	k2eAgInPc1d1	pštrosí
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
Vedas	Vedas	k1gInSc1	Vedas
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
hnízdním	hnízdní	k2eAgInSc6d1	hnízdní
parazitismu	parazitismus	k1gInSc6	parazitismus
u	u	k7c2	u
kukačky	kukačka	k1gFnSc2	kukačka
koel	koel	k1gInSc1	koel
(	(	kIx(	(
<g/>
Eudynamys	Eudynamys	k1gInSc1	Eudynamys
scolopacea	scolopace	k1gInSc2	scolopace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prastarém	prastarý	k2eAgNnSc6d1	prastaré
umění	umění	k1gNnSc6	umění
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc2	Persie
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
ilustrací	ilustrace	k1gFnPc2	ilustrace
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
zpodobněny	zpodobnit	k5eAaPmNgInP	zpodobnit
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
roku	rok	k1gInSc2	rok
350	[number]	k4	350
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Historia	Historium	k1gNnSc2	Historium
Animalium	Animalium	k1gNnSc4	Animalium
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
mají	mít	k5eAaImIp3nP	mít
ptáci	pták	k1gMnPc1	pták
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
migrují	migrovat	k5eAaImIp3nP	migrovat
<g/>
,	,	kIx,	,
pelichá	pelichat	k5eAaImIp3nS	pelichat
jim	on	k3xPp3gInPc3	on
peří	peřit	k5eAaImIp3nP	peřit
a	a	k8xC	a
kladou	klást	k5eAaImIp3nP	klást
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Dopustil	dopustit	k5eAaPmAgInS	dopustit
se	se	k3xPyFc4	se
však	však	k9	však
některých	některý	k3yIgInPc2	některý
omylů	omyl	k1gInPc2	omyl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
že	že	k8xS	že
vlaštovky	vlaštovka	k1gFnPc1	vlaštovka
hibernují	hibernovat	k5eAaImIp3nP	hibernovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeřábi	jeřáb	k1gMnPc1	jeřáb
cestují	cestovat	k5eAaImIp3nP	cestovat
ze	z	k7c2	z
stepí	step	k1gFnPc2	step
na	na	k7c4	na
Scythii	Scythie	k1gFnSc4	Scythie
do	do	k7c2	do
bažin	bažina	k1gFnPc2	bažina
u	u	k7c2	u
pramene	pramen	k1gInSc2	pramen
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
hibernuje	hibernovat	k5eAaImIp3nS	hibernovat
<g/>
,	,	kIx,	,
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
kruzích	kruh	k1gInPc6	kruh
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Elliott	Elliott	k2eAgInSc4d1	Elliott
Coues	Coues	k1gInSc4	Coues
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
182	[number]	k4	182
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
publikací	publikace	k1gFnPc2	publikace
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
zimnímu	zimní	k2eAgInSc3d1	zimní
spánku	spánek	k1gInSc2	spánek
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgNnSc1	žádný
důkazy	důkaz	k1gInPc1	důkaz
a	a	k8xC	a
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
známou	známý	k2eAgFnSc7d1	známá
teorií	teorie	k1gFnSc7	teorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
mylné	mylný	k2eAgFnPc1d1	mylná
představy	představa	k1gFnPc1	představa
existovaly	existovat	k5eAaImAgFnP	existovat
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
chov	chov	k1gInSc4	chov
husy	husa	k1gFnSc2	husa
bernešky	berneška	k1gFnSc2	berneška
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
hnízda	hnízdo	k1gNnPc1	hnízdo
nebyla	být	k5eNaImAgNnP	být
vidět	vidět	k5eAaImF	vidět
a	a	k8xC	a
tak	tak	k6eAd1	tak
lidé	člověk	k1gMnPc1	člověk
snadno	snadno	k6eAd1	snadno
uvěřili	uvěřit	k5eAaPmAgMnP	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
berneška	berneška	k1gFnSc1	berneška
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
transformací	transformace	k1gFnSc7	transformace
z	z	k7c2	z
mušle	mušle	k1gFnSc2	mušle
Pollicipes	Pollicipesa	k1gFnPc2	Pollicipesa
cornucopia	cornucopium	k1gNnSc2	cornucopium
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
převládala	převládat	k5eAaImAgFnS	převládat
celé	celý	k2eAgNnSc4d1	celé
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
biskupem	biskup	k1gMnSc7	biskup
Geraldem	Gerald	k1gMnSc7	Gerald
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Giraldus	Giraldus	k1gInSc1	Giraldus
Cambrensis	Cambrensis	k1gInSc1	Cambrensis
<g/>
)	)	kIx)	)
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
Topographia	Topographia	k1gFnSc1	Topographia
Hiberniae	Hiberniae	k1gFnSc1	Hiberniae
(	(	kIx(	(
<g/>
1187	[number]	k4	1187
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Počátky	počátek	k1gInPc1	počátek
sokolnictví	sokolnictví	k1gNnSc2	sokolnictví
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
a	a	k8xC	a
nejstarší	starý	k2eAgInSc1d3	nejstarší
záznam	záznam	k1gInSc1	záznam
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
panování	panování	k1gNnSc2	panování
Sargona	Sargon	k1gMnSc2	Sargon
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
722	[number]	k4	722
<g/>
–	–	k?	–
<g/>
705	[number]	k4	705
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sokolnictví	sokolnictví	k1gNnPc4	sokolnictví
přinesli	přinést	k5eAaPmAgMnP	přinést
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
400	[number]	k4	400
z	z	k7c2	z
východu	východ	k1gInSc2	východ
po	po	k7c6	po
invazí	invaze	k1gFnPc2	invaze
Hunové	Hun	k1gMnPc1	Hun
a	a	k8xC	a
Alané	Alaná	k1gFnPc1	Alaná
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štaufský	Štaufský	k2eAgMnSc1d1	Štaufský
(	(	kIx(	(
<g/>
1194	[number]	k4	1194
<g/>
–	–	k?	–
<g/>
1250	[number]	k4	1250
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
sokolnictví	sokolnictví	k1gNnSc4	sokolnictví
od	od	k7c2	od
Arabů	Arab	k1gMnPc2	Arab
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
v	v	k7c6	v
regionu	region	k1gInSc6	region
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
arabskou	arabský	k2eAgFnSc4d1	arabská
Moamynovu	Moamynův	k2eAgFnSc4d1	Moamynův
monografii	monografie	k1gFnSc4	monografie
o	o	k7c6	o
sokolnictví	sokolnictví	k1gNnSc6	sokolnictví
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
prováděl	provádět	k5eAaImAgInS	provádět
pokusy	pokus	k1gInPc4	pokus
s	s	k7c7	s
ptáky	pták	k1gMnPc7	pták
ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
zvěřinci	zvěřinec	k1gMnSc6	zvěřinec
<g/>
.	.	kIx.	.
</s>
<s>
Zakrýval	zakrývat	k5eAaImAgMnS	zakrývat
supům	sup	k1gMnPc3	sup
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
posouval	posouvat	k5eAaImAgMnS	posouvat
jim	on	k3xPp3gMnPc3	on
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ptáci	pták	k1gMnPc1	pták
hledají	hledat	k5eAaImIp3nP	hledat
potravu	potrava	k1gFnSc4	potrava
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
čichem	čich	k1gInSc7	čich
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
metody	metoda	k1gFnPc4	metoda
chovu	chov	k1gInSc2	chov
a	a	k8xC	a
tréninku	trénink	k1gInSc2	trénink
sokolů	sokol	k1gMnPc2	sokol
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
pracoval	pracovat	k5eAaImAgInS	pracovat
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
jako	jako	k8xS	jako
De	De	k?	De
Arte	Arte	k1gNnSc1	Arte
Venandi	Venand	k1gMnPc1	Venand
cum	cum	k?	cum
Avibus	Avibus	k1gInSc1	Avibus
(	(	kIx(	(
<g/>
Umění	umění	k1gNnSc1	umění
lovu	lov	k1gInSc2	lov
s	s	k7c7	s
ptáky	pták	k1gMnPc7	pták
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
studií	studie	k1gFnPc2	studie
o	o	k7c6	o
chovu	chov	k1gInSc6	chov
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
poznatků	poznatek	k1gInPc2	poznatek
ornitologie	ornitologie	k1gFnSc2	ornitologie
==	==	k?	==
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
lidských	lidský	k2eAgFnPc2d1	lidská
činností	činnost	k1gFnPc2	činnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc2	peří
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
ornitologie	ornitologie	k1gFnSc1	ornitologie
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
snížit	snížit	k5eAaPmF	snížit
škodlivé	škodlivý	k2eAgInPc4d1	škodlivý
účinky	účinek	k1gInPc4	účinek
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
zvýšit	zvýšit	k5eAaPmF	zvýšit
zisky	zisk	k1gInPc4	zisk
z	z	k7c2	z
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Role	role	k1gFnSc1	role
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
jako	jako	k8xS	jako
škůdce	škůdce	k1gMnSc4	škůdce
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
zrním	zrní	k1gNnSc7	zrní
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
snovač	snovač	k1gMnSc1	snovač
rudozobý	rudozobý	k2eAgMnSc1d1	rudozobý
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
nejpočetnějsí	jpočetnějsit	k5eNaPmIp3nS	jpočetnějsit
ptáky	pták	k1gMnPc4	pták
na	na	k7c6	na
světě	svět	k1gInSc6	svět
ve	v	k7c6	v
společném	společný	k2eAgNnSc6d1	společné
shánění	shánění	k1gNnSc6	shánění
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
hejna	hejno	k1gNnPc1	hejno
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
devastaci	devastace	k1gFnSc4	devastace
úrody	úroda	k1gFnSc2	úroda
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k6eAd1	mnoho
hmyzožravých	hmyzožravý	k2eAgMnPc2d1	hmyzožravý
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
velmi	velmi	k6eAd1	velmi
užitečných	užitečný	k2eAgInPc2d1	užitečný
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
historických	historický	k2eAgNnPc2d1	historické
studií	studio	k1gNnPc2	studio
o	o	k7c6	o
přínosu	přínos	k1gInSc6	přínos
nebo	nebo	k8xC	nebo
škodách	škoda	k1gFnPc6	škoda
způsobených	způsobený	k2eAgFnPc2d1	způsobená
ptáky	pták	k1gMnPc7	pták
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
analýzou	analýza	k1gFnSc7	analýza
obsahu	obsah	k1gInSc2	obsah
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
pozorováním	pozorování	k1gNnSc7	pozorování
jejich	jejich	k3xOp3gNnSc2	jejich
chování	chování	k1gNnSc2	chování
při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
studie	studie	k1gFnPc1	studie
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
na	na	k7c6	na
regulaci	regulace	k1gFnSc6	regulace
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
využívá	využívat	k5eAaPmIp3nS	využívat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
principů	princip	k1gInPc2	princip
z	z	k7c2	z
ekologie	ekologie	k1gFnSc2	ekologie
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
"	"	kIx"	"
<g/>
aquakultura	aquakultura	k1gFnSc1	aquakultura
<g/>
"	"	kIx"	"
přinesla	přinést	k5eAaPmAgFnS	přinést
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
rybami	ryba	k1gFnPc7	ryba
živí	živit	k5eAaImIp3nP	živit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
kormoráni	kormorán	k1gMnPc1	kormorán
<g/>
.	.	kIx.	.
<g/>
Velká	velká	k1gFnSc1	velká
hejna	hejno	k1gNnSc2	hejno
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
špačků	špaček	k1gMnPc2	špaček
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
obtěžující	obtěžující	k2eAgMnPc4d1	obtěžující
a	a	k8xC	a
techniky	technik	k1gMnPc4	technik
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
jejich	jejich	k3xOp3gFnSc2	jejich
populace	populace	k1gFnSc2	populace
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gInPc4	jejich
dopady	dopad	k1gInPc4	dopad
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
inovovují	inovovovat	k5eAaImIp3nP	inovovovat
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
významní	významný	k2eAgMnPc1d1	významný
také	také	k9	také
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
přenašeči	přenašeč	k1gMnPc1	přenašeč
lidských	lidský	k2eAgNnPc2d1	lidské
onemocnění	onemocnění	k1gNnPc2	onemocnění
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
japonská	japonský	k2eAgFnSc1d1	japonská
Encefalitida	encefalitida	k1gFnSc1	encefalitida
<g/>
,	,	kIx,	,
západonilská	západonilský	k2eAgFnSc1d1	západonilská
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
Obzvláště	obzvláště	k6eAd1	obzvláště
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
škody	škoda	k1gFnPc1	škoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
napáchají	napáchat	k5eAaBmIp3nP	napáchat
ptáci	pták	k1gMnPc1	pták
v	v	k7c6	v
letectví	letectví	k1gNnSc6	letectví
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
fatálním	fatální	k2eAgInPc3d1	fatální
důsledkům	důsledek	k1gInPc3	důsledek
a	a	k8xC	a
způsobeným	způsobený	k2eAgFnPc3d1	způsobená
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
odhadnuto	odhadnut	k2eAgNnSc1d1	odhadnuto
<g/>
,	,	kIx,	,
že	že	k8xS	že
leteckému	letecký	k2eAgInSc3d1	letecký
průmyslu	průmysl	k1gInSc3	průmysl
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
vznikají	vznikat	k5eAaImIp3nP	vznikat
škody	škoda	k1gFnPc4	škoda
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gMnPc2	druh
ptáků	pták	k1gMnPc2	pták
přivedl	přivést	k5eAaPmAgMnS	přivést
člověk	člověk	k1gMnSc1	člověk
svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
vyhubení	vyhubení	k1gNnSc2	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
ptactva	ptactvo	k1gNnSc2	ptactvo
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
znalosti	znalost	k1gFnPc4	znalost
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
a	a	k8xC	a
ekologii	ekologie	k1gFnSc6	ekologie
<g/>
.	.	kIx.	.
</s>
<s>
Ornitologové	ornitolog	k1gMnPc1	ornitolog
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
zachování	zachování	k1gNnSc3	zachování
ptactva	ptactvo	k1gNnSc2	ptactvo
studiem	studio	k1gNnSc7	studio
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
určují	určovat	k5eAaImIp3nP	určovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
hrozby	hrozba	k1gFnPc4	hrozba
a	a	k8xC	a
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zvýšit	zvýšit	k5eAaPmF	zvýšit
přežití	přežití	k1gNnSc4	přežití
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
kondor	kondor	k1gMnSc1	kondor
kalifornský	kalifornský	k2eAgMnSc1d1	kalifornský
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
odchyceny	odchytit	k5eAaPmNgFnP	odchytit
a	a	k8xC	a
chováni	chovat	k5eAaImNgMnP	chovat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
opatření	opatření	k1gNnPc1	opatření
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
opětnému	opětný	k2eAgNnSc3d1	opětné
vysazení	vysazení	k1gNnSc3	vysazení
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nezodpovězené	zodpovězený	k2eNgFnPc1d1	nezodpovězená
otázky	otázka	k1gFnPc1	otázka
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
ptáků	pták	k1gMnPc2	pták
==	==	k?	==
</s>
</p>
<p>
<s>
Stěhovaví	stěhovavý	k2eAgMnPc1d1	stěhovavý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
obdivuhodnou	obdivuhodný	k2eAgFnSc4d1	obdivuhodná
orientaci	orientace	k1gFnSc4	orientace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
záhadou	záhada	k1gFnSc7	záhada
pro	pro	k7c4	pro
ornitology	ornitolog	k1gMnPc4	ornitolog
<g/>
.	.	kIx.	.
</s>
<s>
Rupert	Rupert	k1gMnSc1	Rupert
Sheldrake	Sheldrak	k1gFnSc2	Sheldrak
popsal	popsat	k5eAaPmAgMnS	popsat
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
poštovními	poštovní	k2eAgFnPc7d1	poštovní
holubicemi	holubice	k1gFnPc7	holubice
<g/>
:	:	kIx,	:
zavazoval	zavazovat	k5eAaImAgInS	zavazovat
jim	on	k3xPp3gMnPc3	on
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
vyoperovával	vyoperovávat	k5eAaPmAgMnS	vyoperovávat
jim	on	k3xPp3gMnPc3	on
různé	různý	k2eAgInPc4d1	různý
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
dezorientoval	dezorientovat	k5eAaBmAgMnS	dezorientovat
v	v	k7c6	v
centrifuze	centrifuga	k1gFnSc6	centrifuga
<g/>
,	,	kIx,	,
odvážel	odvážet	k5eAaImAgMnS	odvážet
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
vzdálená	vzdálený	k2eAgNnPc4d1	vzdálené
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
přemísťoval	přemísťovat	k5eAaImAgMnS	přemísťovat
jejich	jejich	k3xOp3gMnPc4	jejich
domácí	domácí	k1gMnPc4	domácí
holubníky	holubník	k1gInPc1	holubník
o	o	k7c4	o
stovky	stovka	k1gFnPc4	stovka
kilometrů	kilometr	k1gInPc2	kilometr
jinam	jinam	k6eAd1	jinam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
holubice	holubice	k1gFnSc1	holubice
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
vždy	vždy	k6eAd1	vždy
bezpečně	bezpečně	k6eAd1	bezpečně
trefily	trefit	k5eAaPmAgFnP	trefit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ornitologie	ornitologie	k1gFnSc2	ornitologie
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Významným	významný	k2eAgMnSc7d1	významný
českým	český	k2eAgMnSc7d1	český
ornitologem	ornitolog	k1gMnSc7	ornitolog
a	a	k8xC	a
zoologem	zoolog	k1gMnSc7	zoolog
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Janda	Janda	k1gMnSc1	Janda
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
pražské	pražský	k2eAgFnSc2d1	Pražská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
ornitologem	ornitolog	k1gMnSc7	ornitolog
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
ZOO	zoo	k1gNnSc2	zoo
Praha	Praha	k1gFnSc1	Praha
vedl	vést	k5eAaImAgInS	vést
a	a	k8xC	a
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
neexistující	existující	k2eNgFnSc4d1	neexistující
unikátní	unikátní	k2eAgFnSc4d1	unikátní
sbírku	sbírka	k1gFnSc4	sbírka
hrabavých	hrabavý	k2eAgMnPc2d1	hrabavý
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
české	český	k2eAgFnSc2d1	Česká
etologie	etologie	k1gFnSc2	etologie
a	a	k8xC	a
autorem	autor	k1gMnSc7	autor
mnoha	mnoho	k4c2	mnoho
odborných	odborný	k2eAgInPc2d1	odborný
článků	článek	k1gInPc2	článek
a	a	k8xC	a
populárně	populárně	k6eAd1	populárně
naučných	naučný	k2eAgFnPc2d1	naučná
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
etologie	etologie	k1gFnSc2	etologie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
žákem	žák	k1gMnSc7	žák
nositele	nositel	k1gMnSc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
zakladatele	zakladatel	k1gMnSc2	zakladatel
etologie	etologie	k1gFnSc2	etologie
Konráda	Konrád	k1gMnSc2	Konrád
Lorenze	Lorenz	k1gMnSc2	Lorenz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
světoví	světový	k2eAgMnPc1d1	světový
ornitologové	ornitolog	k1gMnPc1	ornitolog
==	==	k?	==
</s>
</p>
<p>
<s>
Ulisse	Ulisse	k6eAd1	Ulisse
Aldrovandi	Aldrovand	k1gMnPc1	Aldrovand
(	(	kIx(	(
<g/>
1522	[number]	k4	1522
<g/>
–	–	k?	–
<g/>
1605	[number]	k4	1605
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgInS	napsat
dílo	dílo	k1gNnSc4	dílo
Ornithologiae	Ornithologiae	k1gNnSc2	Ornithologiae
libri	libri	k1gNnSc2	libri
XII	XII	kA	XII
(	(	kIx(	(
<g/>
Bologna	Bologna	k1gFnSc1	Bologna
1599	[number]	k4	1599
<g/>
–	–	k?	–
<g/>
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Matthäus	Matthäus	k1gMnSc1	Matthäus
Bechstein	Bechstein	k1gMnSc1	Bechstein
(	(	kIx(	(
<g/>
1757	[number]	k4	1757
<g/>
–	–	k?	–
<g/>
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Otec	otec	k1gMnSc1	otec
německé	německý	k2eAgFnSc2d1	německá
ornitologie	ornitologie	k1gFnSc2	ornitologie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Bond	bond	k1gInSc1	bond
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
Birds	Birds	k1gInSc1	Birds
of	of	k?	of
the	the	k?	the
West	West	k2eAgInSc4d1	West
Indies	Indies	k1gInSc4	Indies
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
odborným	odborný	k2eAgInSc7d1	odborný
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Edwards	Edwardsa	k1gFnPc2	Edwardsa
(	(	kIx(	(
<g/>
1694	[number]	k4	1694
<g/>
–	–	k?	–
<g/>
1773	[number]	k4	1773
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Otec	otec	k1gMnSc1	otec
britské	britský	k2eAgFnSc2d1	britská
ornitologie	ornitologie	k1gFnSc2	ornitologie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
<g/>
:	:	kIx,	:
A	a	k9	a
natural	natural	k?	natural
history	histor	k1gInPc1	histor
of	of	k?	of
Birds	Birds	k1gInSc1	Birds
</s>
</p>
<p>
<s>
Konrad	Konrad	k1gInSc1	Konrad
Lorenz	Lorenz	k1gMnSc1	Lorenz
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
etologie	etologie	k1gFnSc2	etologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Coenraad	Coenraad	k1gInSc1	Coenraad
Jacob	Jacoba	k1gFnPc2	Jacoba
Temminck	Temmincka	k1gFnPc2	Temmincka
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
holandský	holandský	k2eAgMnSc1d1	holandský
aristokrat	aristokrat	k1gMnSc1	aristokrat
a	a	k8xC	a
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
Manuel	Manuel	k1gMnSc1	Manuel
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
ornithologie	ornithologie	k1gFnSc1	ornithologie
<g/>
,	,	kIx,	,
ou	ou	k0	ou
Tableau	Tableaus	k1gInSc2	Tableaus
systematique	systematique	k1gNnSc2	systematique
des	des	k1gNnSc1	des
oiseaux	oiseaux	k1gInSc1	oiseaux
qui	qui	k?	qui
se	se	k3xPyFc4	se
trouvent	trouvent	k1gInSc1	trouvent
en	en	k?	en
Europe	Europ	k1gInSc5	Europ
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
standardem	standard	k1gInSc7	standard
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zdědil	zdědit	k5eAaPmAgInS	zdědit
velkou	velký	k2eAgFnSc4d1	velká
sbírku	sbírka	k1gFnSc4	sbírka
ptactva	ptactvo	k1gNnSc2	ptactvo
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KNĚŽOUREK	kněžourek	k1gMnSc1	kněžourek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
atlas	atlas	k1gInSc1	atlas
ptáků	pták	k1gMnPc2	pták
ku	k	k7c3	k
Kněžourkovu	kněžourkův	k2eAgInSc3d1	kněžourkův
Velkému	velký	k2eAgInSc3d1	velký
přírodopisu	přírodopis	k1gInSc3	přírodopis
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
4	[number]	k4	4
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
<g/>
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KNĚŽOUREK	kněžourek	k1gMnSc1	kněžourek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
přírodopis	přírodopis	k1gInSc1	přírodopis
ptáků	pták	k1gMnPc2	pták
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zřetelem	zřetel	k1gInSc7	zřetel
ku	k	k7c3	k
ptactvu	ptactvo	k1gNnSc3	ptactvo
zemí	zem	k1gFnPc2	zem
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
rakouských	rakouský	k2eAgMnPc2d1	rakouský
<g/>
.	.	kIx.	.
2	[number]	k4	2
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
<g/>
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRSÍK	JIRSÍK	kA	JIRSÍK
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
poznám	poznat	k5eAaPmIp1nS	poznat
ptáky	pták	k1gMnPc4	pták
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
:	:	kIx,	:
úplný	úplný	k2eAgInSc4d1	úplný
klíč	klíč	k1gInSc4	klíč
k	k	k7c3	k
poznávání	poznávání	k1gNnSc3	poznávání
všeho	všecek	k3xTgNnSc2	všecek
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
hnízdícího	hnízdící	k2eAgInSc2d1	hnízdící
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
se	se	k3xPyFc4	se
zatoulávajícího	zatoulávající	k2eAgNnSc2d1	zatoulávající
ptactva	ptactvo	k1gNnSc2	ptactvo
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Entomologie	entomologie	k1gFnSc1	entomologie
</s>
</p>
<p>
<s>
Ichtyologie	ichtyologie	k1gFnSc1	ichtyologie
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ornitologických	ornitologický	k2eAgInPc2d1	ornitologický
časopisů	časopis	k1gInPc2	časopis
</s>
</p>
<p>
<s>
Zoologie	zoologie	k1gFnSc1	zoologie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ornitologie	ornitologie	k1gFnSc2	ornitologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ornitologie	ornitologie	k1gFnSc2	ornitologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
