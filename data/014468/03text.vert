<s>
Antoine	Antoinout	k5eAaPmIp3nS
Lasalle	Lasalle	k1gFnSc1
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
Lasalle	Lasalle	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1775	#num#	k4
<g/>
Mety	meta	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1809	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
34	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Deutsch-Wagram	Deutsch-Wagram	k1gInSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
střelná	střelný	k2eAgFnSc1d1
rána	rána	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
krypta	krypta	k1gFnSc1
Saint-Louis-des-Invalides	Saint-Louis-des-Invalidesa	k1gFnPc2
Povolání	povolání	k1gNnPc4
</s>
<s>
důstojník	důstojník	k1gMnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
velkodůstojník	velkodůstojník	k1gInSc1
Řádu	řád	k1gInSc2
čestné	čestný	k2eAgNnSc1d1
legieJména	legieJména	k6eAd1
vepsaná	vepsaný	k2eAgNnPc4d1
pod	pod	k7c7
Vítězným	vítězný	k2eAgNnSc7d1
obloukemKmotr	obloukemKmotr	k1gMnSc1
podpory	podpora	k1gFnSc2
zvláštní	zvláštní	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Saint-Cyr	Saint-Cyr	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
Charles	Charles	k1gMnSc1
Louis	Louis	k1gMnSc1
Lasalle	Lasalle	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1775	#num#	k4
<g/>
,	,	kIx,
Mety	met	k1gInPc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1809	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
z	z	k7c2
doby	doba	k1gFnSc2
velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
a	a	k8xC
prvního	první	k4xOgInSc2
císařství	císařství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
Lasalle	Lasalle	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1775	#num#	k4
Pierremu	Pierrem	k1gInSc2
Nicolasi	Nicolas	k1gMnPc1
de	de	k?
la	la	k1gNnSc2
Lasalle	Lasalle	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
ženě	žena	k1gFnSc3
Suzanně	Suzanně	k1gFnSc2
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1786	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
náhradním	náhradní	k2eAgMnSc7d1
poručíkem	poručík	k1gMnSc7
v	v	k7c6
královském	královský	k2eAgInSc6d1
pluku	pluk	k1gInSc6
Alsace	Alsace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1791	#num#	k4
byl	být	k5eAaImAgMnS
přidělen	přidělit	k5eAaPmNgMnS
k	k	k7c3
24	#num#	k4
<g/>
.	.	kIx.
jezdeckému	jezdecký	k2eAgInSc3d1
pluku	pluk	k1gInSc3
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgMnSc2,k3yQgMnSc2,k3yIgMnSc2
brzy	brzy	k6eAd1
vystoupil	vystoupit	k5eAaPmAgMnS
a	a	k8xC
jako	jako	k8xS,k8xC
prostý	prostý	k2eAgMnSc1d1
občan	občan	k1gMnSc1
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgMnS
do	do	k7c2
lidové	lidový	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
Pikenýrů	Pikenýr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
projevoval	projevovat	k5eAaImAgMnS
jako	jako	k9
dobrý	dobrý	k2eAgMnSc1d1
vlastenec	vlastenec	k1gMnSc1
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgMnS
jako	jako	k9
dobrovolník	dobrovolník	k1gMnSc1
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
první	první	k4xOgFnSc3
koalici	koalice	k1gFnSc3
a	a	k8xC
za	za	k7c4
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
po	po	k7c6
vstupu	vstup	k1gInSc6
mezi	mezi	k7c7
dobrovolníky	dobrovolník	k1gMnPc7
byl	být	k5eAaImAgMnS
zařazen	zařazen	k2eAgMnSc1d1
k	k	k7c3
nové	nový	k2eAgFnSc3d1
regulérní	regulérní	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
k	k	k7c3
23	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
jízdních	jízdní	k2eAgMnPc2d1
myslivců	myslivec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1793	#num#	k4
u	u	k7c2
Landrecies	Landreciesa	k1gFnPc2
vjel	vjet	k5eAaPmAgInS
mezi	mezi	k7c4
děla	dělo	k1gNnPc4
anglické	anglický	k2eAgFnSc2d1
baterie	baterie	k1gFnSc2
a	a	k8xC
pobil	pobít	k5eAaPmAgMnS
obsluhu	obsluha	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
důstojníka	důstojník	k1gMnSc4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
ale	ale	k9
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
další	další	k2eAgInSc4d1
takový	takový	k3xDgInSc4
kousek	kousek	k1gInSc4
byl	být	k5eAaImAgMnS
Lasalle	Lasalle	k1gInSc4
zraněn	zraněn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
vojenského	vojenský	k2eAgInSc2d1
špitálu	špitál	k1gInSc2
přešel	přejít	k5eAaPmAgInS
k	k	k7c3
Italské	italský	k2eAgFnSc3d1
a	a	k8xC
Alpské	alpský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
spojené	spojený	k2eAgFnSc3d1
v	v	k7c6
jednu	jeden	k4xCgFnSc4
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
brzy	brzy	k6eAd1
rozdělena	rozdělit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lasalle	Lasalle	k1gInSc1
šel	jít	k5eAaImAgInS
s	s	k7c7
Alpskou	alpský	k2eAgFnSc7d1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc2
velel	velet	k5eAaImAgMnS
generál	generál	k1gMnSc1
Kellerman	Kellerman	k1gMnSc1
<g/>
,	,	kIx,
Italské	italský	k2eAgFnSc2d1
velel	velet	k5eAaImAgMnS
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lasalle	Lasalle	k1gNnSc7
roku	rok	k1gInSc2
1796	#num#	k4
přešel	přejít	k5eAaPmAgMnS
k	k	k7c3
Bonapartovi	Bonapartův	k2eAgMnPc1d1
<g/>
,	,	kIx,
u	u	k7c2
nějž	jenž	k3xRgMnSc2
ukázal	ukázat	k5eAaPmAgMnS
velkou	velký	k2eAgFnSc4d1
odvahu	odvaha	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
během	během	k7c2
ústupu	ústup	k1gInSc2
na	na	k7c4
Mantovu	Mantova	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
nesl	nést	k5eAaImAgMnS
zprávu	zpráva	k1gFnSc4
veliteli	velitel	k1gMnPc7
Brescie	Brescie	k1gFnSc2
byl	být	k5eAaImAgInS
Rakušany	Rakušan	k1gMnPc4
zajat	zajat	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zajetí	zajetí	k1gNnSc6
zaujal	zaujmout	k5eAaPmAgMnS
generála	generál	k1gMnSc4
Wurmstera	Wurmster	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
jej	on	k3xPp3gMnSc4
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Castiglione	Castiglion	k1gInSc5
vyměnil	vyměnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1796	#num#	k4
byl	být	k5eAaImAgInS
Lasalle	Lasalle	k1gInSc1
povýšen	povýšen	k2eAgInSc1d1
na	na	k7c4
kapitána	kapitán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
propuštění	propuštění	k1gNnSc6
ze	z	k7c2
zajetí	zajetí	k1gNnSc2
byl	být	k5eAaImAgInS
Lasalle	Lasall	k1gMnPc4
ve	v	k7c6
Vicenze	Vicenza	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
začal	začít	k5eAaPmAgInS
milostný	milostný	k2eAgInSc4d1
románek	románek	k1gInSc4
se	s	k7c7
vdanou	vdaný	k2eAgFnSc7d1
markýzou	markýza	k1gFnSc7
De	De	k?
Sali	Sal	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
neskončil	skončit	k5eNaPmAgInS
ani	ani	k9
ústupem	ústup	k1gInSc7
Francouzů	Francouz	k1gMnPc2
z	z	k7c2
Vicenzy	Vicenza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
po	po	k7c6
odchodu	odchod	k1gInSc6
z	z	k7c2
Vicenzy	Vicenza	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
generál	generál	k1gMnSc1
Masséna	Masséno	k1gNnSc2
potřeboval	potřebovat	k5eAaImAgMnS
nějaké	nějaký	k3yIgFnPc4
zprávy	zpráva	k1gFnPc4
o	o	k7c6
plánech	plán	k1gInPc6
a	a	k8xC
pohybech	pohyb	k1gInPc6
Rakušanů	Rakušan	k1gMnPc2
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
Vicenze	Vicenza	k1gFnSc6
mu	on	k3xPp3gMnSc3
je	on	k3xPp3gNnSc4
obstará	obstarat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
úkol	úkol	k1gInSc4
dostal	dostat	k5eAaPmAgInS
nevelký	velký	k2eNgInSc1d1
oddíl	oddíl	k1gInSc1
jezdců	jezdec	k1gMnPc2
(	(	kIx(
<g/>
18	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
nebyli	být	k5eNaImAgMnP
od	od	k7c2
Rakušanů	Rakušan	k1gMnPc2
k	k	k7c3
rozeznání	rozeznání	k1gNnSc3
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1796	#num#	k4
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
Vicenzy	Vicenza	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Lasalle	Lasalle	k1gInSc1
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
markýzou	markýza	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
níž	jenž	k3xRgFnSc2
dostal	dostat	k5eAaPmAgMnS
informace	informace	k1gFnPc4
o	o	k7c6
Rakušanech	Rakušan	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
setkání	setkání	k1gNnSc1
nakonec	nakonec	k6eAd1
přerušilo	přerušit	k5eAaPmAgNnS
odhalení	odhalení	k1gNnSc1
Francouzů	Francouz	k1gMnPc2
a	a	k8xC
Lasalle	Lasalle	k1gInSc1
se	se	k3xPyFc4
musel	muset	k5eAaImAgInS
z	z	k7c2
Vicenzy	Vicenza	k1gFnSc2
probít	probít	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
jezdců	jezdec	k1gMnPc2
zůstala	zůstat	k5eAaPmAgFnS
před	před	k7c7
Vicenzou	Vicenza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
nájezdu	nájezd	k1gInSc3
na	na	k7c6
Vicenzu	Vicenz	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
velitelem	velitel	k1gMnSc7
eskadrony	eskadrona	k1gFnSc2
7	#num#	k4
<g/>
.	.	kIx.
zdvojeného	zdvojený	k2eAgInSc2d1
husarského	husarský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Lasalle	Lasalle	k1gNnPc6
</s>
<s>
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
začali	začít	k5eAaPmAgMnP
Rakušané	Rakušan	k1gMnPc1
všemi	všecek	k3xTgFnPc7
silami	síla	k1gFnPc7
v	v	k7c6
Itálii	Itálie	k1gFnSc6
silný	silný	k2eAgInSc4d1
nápor	nápor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	Napoleon	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
neustoupit	ustoupit	k5eNaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
čelit	čelit	k5eAaImF
mu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1797	#num#	k4
se	se	k3xPyFc4
Lasalle	Lasalle	k1gNnSc2
proslavil	proslavit	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Rivoli	Rivole	k1gFnSc3
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
začal	začít	k5eAaPmAgInS
rakouský	rakouský	k2eAgInSc1d1
ústup	ústup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
začít	začít	k5eAaPmF
postup	postup	k1gInSc4
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
markýza	markýza	k1gFnSc1
De	De	k?
Sali	Sal	k1gFnSc2
žádala	žádat	k5eAaImAgFnS
Lasalla	Lasalla	k1gFnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
neodcházel	odcházet	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nemohl	moct	k5eNaImAgMnS
splnit	splnit	k5eAaPmF
a	a	k8xC
markýza	markýza	k1gFnSc1
spáchala	spáchat	k5eAaPmAgFnS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
(	(	kIx(
<g/>
když	když	k8xS
francouzi	francouh	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
Lubna	Lubn	k1gMnSc4
<g/>
)	)	kIx)
začal	začít	k5eAaPmAgMnS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
další	další	k2eAgInSc4d1
románek	románek	k1gInSc4
s	s	k7c7
Josephine	Josephin	k1gInSc5
Joanne	Joann	k1gInSc5
d	d	k?
<g/>
´	´	k?
<g/>
Aguilon	Aguilon	k1gInSc1
<g/>
,	,	kIx,
manželkou	manželka	k1gFnSc7
Victora	Victor	k1gMnSc2
Léopolda	Léopold	k1gMnSc2
Berthiera	Berthier	k1gMnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
mu	on	k3xPp3gMnSc3
brzy	brzy	k6eAd1
porodila	porodit	k5eAaPmAgFnS
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lasalle	Lasalle	k1gInSc1
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgInS
tažení	tažení	k1gNnSc4
proti	proti	k7c3
papežskému	papežský	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
Davoutem	Davout	k1gMnSc7
sloužil	sloužit	k5eAaImAgInS
na	na	k7c6
egyptském	egyptský	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
znova	znova	k6eAd1
v	v	k7c6
nejedné	nejeden	k4xCyIgFnSc6
bitvě	bitva	k1gFnSc6
ukázal	ukázat	k5eAaPmAgMnS
velkou	velký	k2eAgFnSc4d1
odvahu	odvaha	k1gFnSc4
a	a	k8xC
dosáhl	dosáhnout	k5eAaPmAgInS
zde	zde	k6eAd1
povýšení	povýšení	k1gNnSc4
na	na	k7c4
plukovníka	plukovník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gInPc2
dopisů	dopis	k1gInPc2
paní	paní	k1gFnSc2
Berthierové	Berthierové	k2eAgInSc2d1
se	se	k3xPyFc4
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
ní	on	k3xPp3gFnSc7
má	mít	k5eAaImIp3nS
poměr	poměr	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
jejímu	její	k3xOp3gInSc3
rozvodu	rozvod	k1gInSc3
s	s	k7c7
Berthierem	Berthiero	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
Egypta	Egypt	k1gInSc2
Lasalle	Lasalle	k1gNnSc2
musel	muset	k5eAaImAgMnS
do	do	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gMnSc6
šířily	šířit	k5eAaImAgFnP
skandální	skandální	k2eAgFnPc4d1
historky	historka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1803	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Josephinou	Josephina	k1gMnSc7
d	d	k?
<g/>
'	'	kIx"
<g/>
Auguillon	Auguillon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1804	#num#	k4
se	se	k3xPyFc4
z	z	k7c2
Napoleona	Napoleon	k1gMnSc2
stal	stát	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
a	a	k8xC
z	z	k7c2
Lasalla	Lasallo	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
komandér	komandér	k1gMnSc1
Řádu	řád	k1gInSc2
čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1805	#num#	k4
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
brigádního	brigádní	k2eAgMnSc4d1
generála	generál	k1gMnSc4
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
pod	pod	k7c4
vedení	vedení	k1gNnSc4
brigádu	brigáda	k1gFnSc4
dragounů	dragoun	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
brigádou	brigáda	k1gFnSc7
se	se	k3xPyFc4
nijak	nijak	k6eAd1
neproslavil	proslavit	k5eNaPmAgMnS
<g/>
,	,	kIx,
proto	proto	k8xC
dostal	dostat	k5eAaPmAgMnS
brigádu	brigáda	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
husarského	husarský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
války	válka	k1gFnSc2
s	s	k7c7
Pruskem	Prusko	k1gNnSc7
zažil	zažít	k5eAaPmAgInS
Lasalle	Lasalle	k1gInSc1
potupu	potupa	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
nechal	nechat	k5eAaPmAgMnS
uniknout	uniknout	k5eAaPmF
generála	generál	k1gMnSc4
Blüchera	Blücher	k1gMnSc4
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgMnS
císařem	císař	k1gMnSc7
pokárán	pokárat	k5eAaPmNgMnS
a	a	k8xC
dokonce	dokonce	k9
kvůli	kvůli	k7c3
tomu	ten	k3xDgMnSc3
chtěl	chtít	k5eAaImAgMnS
spáchat	spáchat	k5eAaPmF
sebevraždu	sebevražda	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
mu	on	k3xPp3gMnSc3
zabránil	zabránit	k5eAaPmAgMnS
plukovník	plukovník	k1gMnSc1
Schwartz	Schwartz	k1gMnSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1806	#num#	k4
stanul	stanout	k5eAaPmAgInS
před	před	k7c7
Štětínem	Štětín	k1gInSc7
a	a	k8xC
při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
dobývání	dobývání	k1gNnSc6
se	se	k3xPyFc4
znovu	znovu	k6eAd1
proslavil	proslavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1806	#num#	k4
u	u	k7c2
Golimyna	Golimyn	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
chtěl	chtít	k5eAaImAgMnS
zaútočit	zaútočit	k5eAaPmF
<g/>
,	,	kIx,
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnPc2
muži	muž	k1gMnPc1
rozutekli	rozutéct	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
je	být	k5eAaImIp3nS
poslal	poslat	k5eAaPmAgMnS
do	do	k7c2
dělostřelecké	dělostřelecký	k2eAgFnSc2d1
palby	palba	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
nechal	nechat	k5eAaPmAgMnS
po	po	k7c4
zbytek	zbytek	k1gInSc4
boje	boj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
polského	polský	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
byl	být	k5eAaImAgInS
Lasalle	Lasalle	k1gInSc4
už	už	k6eAd1
divizním	divizní	k2eAgMnSc7d1
generálem	generál	k1gMnSc7
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
pod	pod	k7c4
vedení	vedení	k1gNnSc4
lehkou	lehký	k2eAgFnSc4d1
divizi	divize	k1gFnSc4
a	a	k8xC
znovu	znovu	k6eAd1
se	se	k3xPyFc4
jako	jako	k9
její	její	k3xOp3gMnSc1
velitel	velitel	k1gMnSc1
proslavil	proslavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgInS
poslán	poslat	k5eAaPmNgInS
do	do	k7c2
bouřlivého	bouřlivý	k2eAgNnSc2d1
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
uštědřil	uštědřit	k5eAaPmAgMnS
povstalcům	povstalec	k1gMnPc3
těžké	těžký	k2eAgFnSc2d1
porážky	porážka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
převzal	převzít	k5eAaPmAgMnS
divizi	divize	k1gFnSc4
lehkého	lehký	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
začala	začít	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
válka	válka	k1gFnSc1
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
převzetí	převzetí	k1gNnSc6
velení	velení	k1gNnSc2
nad	nad	k7c7
divizí	divize	k1gFnSc7
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
Napoleonovou	Napoleonův	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
před	před	k7c7
bitvou	bitva	k1gFnSc7
u	u	k7c2
Aspern-Esslingu	Aspern-Essling	k1gInSc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1809	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
během	během	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
opět	opět	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
bitvu	bitva	k1gFnSc4
Napoleon	napoleon	k1gInSc1
prohrál	prohrát	k5eAaPmAgInS
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
s	s	k7c7
Lauristonem	Lauriston	k1gInSc7
Lasalle	Lasalle	k1gFnSc2
dobyl	dobýt	k5eAaPmAgInS
uherskou	uherský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
Ráb	Ráb	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
sepsal	sepsat	k5eAaPmAgMnS
Lasalle	Lasalle	k1gInSc4
závěť	závěť	k1gFnSc1
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
ten	ten	k3xDgInSc4
den	den	k1gInSc4
večer	večer	k6eAd1
začala	začít	k5eAaPmAgFnS
bitva	bitva	k1gFnSc1
u	u	k7c2
Wagramu	Wagram	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
Lasalle	Lasalla	k1gFnSc6
zahynul	zahynout	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
vedl	vést	k5eAaImAgInS
kyrysníky	kyrysník	k1gMnPc4
do	do	k7c2
útoku	útok	k1gInSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
zasažen	zasáhnout	k5eAaPmNgMnS
kulí	kule	k1gFnSc7
do	do	k7c2
čela	čelo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Antoine	Antoin	k1gInSc5
Lasalle	Lasalle	k1gNnSc3
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
129465615	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7984	#num#	k4
2286	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2005000501	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
14915967	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2005000501	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
