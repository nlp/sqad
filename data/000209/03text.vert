<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
Portugal	portugal	k1gInSc1	portugal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
República	Repúblic	k2eAgFnSc1d1	República
Portuguesa	Portuguesa	k1gFnSc1	Portuguesa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
evropský	evropský	k2eAgInSc4d1	evropský
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
světadílu	světadíl	k1gInSc2	světadíl
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
jeho	jeho	k3xOp3gMnSc7	jeho
pevninským	pevninský	k2eAgMnSc7d1	pevninský
sousedem	soused	k1gMnSc7	soused
je	být	k5eAaImIp3nS	být
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
západní	západní	k2eAgInPc1d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgInPc1d1	jižní
břehy	břeh	k1gInPc1	břeh
země	zem	k1gFnSc2	zem
omývají	omývat	k5eAaImIp3nP	omývat
vody	voda	k1gFnPc1	voda
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsku	Portugalsko	k1gNnSc3	Portugalsko
dále	daleko	k6eAd2	daleko
náleží	náležet	k5eAaImIp3nS	náležet
souostroví	souostroví	k1gNnSc4	souostroví
Azory	azor	k1gInPc4	azor
a	a	k8xC	a
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
vzešel	vzejít	k5eAaPmAgInS	vzejít
z	z	k7c2	z
římského	římský	k2eAgNnSc2d1	římské
označení	označení	k1gNnSc2	označení
Portus	Portus	k1gInSc1	Portus
Cale	Cale	k1gFnSc4	Cale
<g/>
.	.	kIx.	.
</s>
<s>
Cale	Cale	k6eAd1	Cale
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
původní	původní	k2eAgFnSc2d1	původní
osady	osada	k1gFnSc2	osada
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Douro	Douro	k1gNnSc4	Douro
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
získali	získat	k5eAaPmAgMnP	získat
kolem	kolem	k6eAd1	kolem
roku	rok	k1gInSc2	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Římané	Říman	k1gMnPc1	Říman
od	od	k7c2	od
Kartága	Kartágo	k1gNnSc2	Kartágo
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Druhé	druhý	k4xOgFnSc2	druhý
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vizigóti	Vizigót	k1gMnPc1	Vizigót
pak	pak	k6eAd1	pak
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
upravili	upravit	k5eAaPmAgMnP	upravit
název	název	k1gInSc4	název
území	území	k1gNnSc2	území
na	na	k7c6	na
Portucale	Portucala	k1gFnSc6	Portucala
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
později	pozdě	k6eAd2	pozdě
dalo	dát	k5eAaPmAgNnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
jménu	jméno	k1gNnSc6	jméno
Portugale	portugal	k1gInSc5	portugal
čili	čili	k8xC	čili
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pojem	pojem	k1gInSc1	pojem
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
území	území	k1gNnSc6	území
cca	cca	kA	cca
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Minho	Min	k1gMnSc2	Min
a	a	k8xC	a
Douro	Douro	k1gNnSc1	Douro
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
názvu	název	k1gInSc2	název
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc7	jeho
zkrácením	zkrácení	k1gNnSc7	zkrácení
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
taktéž	taktéž	k?	taktéž
dnešní	dnešní	k2eAgNnSc4d1	dnešní
jméno	jméno	k1gNnSc4	jméno
města	město	k1gNnSc2	město
nacházejícího	nacházející	k2eAgInSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
osady	osada	k1gFnSc2	osada
Cale	Cal	k1gFnSc2	Cal
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Porta	porta	k1gFnSc1	porta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
gastronomickým	gastronomický	k2eAgInSc7d1	gastronomický
produktem	produkt	k1gInSc7	produkt
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
je	být	k5eAaImIp3nS	být
portské	portský	k2eAgNnSc4d1	portské
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
dolihované	dolihovaný	k2eAgNnSc4d1	dolihované
víno	víno	k1gNnSc4	víno
z	z	k7c2	z
vinic	vinice	k1gFnPc2	vinice
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Douro	Douro	k1gNnSc1	Douro
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
se	se	k3xPyFc4	se
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
mnoho	mnoho	k6eAd1	mnoho
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
<g/>
,	,	kIx,	,
Kelti	Kelt	k1gMnPc1	Kelt
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
i	i	k8xC	i
Arabové	Arab	k1gMnPc1	Arab
pozměnili	pozměnit	k5eAaPmAgMnP	pozměnit
a	a	k8xC	a
poznamenali	poznamenat	k5eAaPmAgMnP	poznamenat
vývoj	vývoj	k1gInSc4	vývoj
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
naopak	naopak	k6eAd1	naopak
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
svými	svůj	k3xOyFgInPc7	svůj
zámořskými	zámořský	k2eAgInPc7d1	zámořský
objevy	objev	k1gInPc7	objev
změnili	změnit	k5eAaPmAgMnP	změnit
tvář	tvář	k1gFnSc4	tvář
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Raní	raný	k2eAgMnPc1d1	raný
řečtí	řecký	k2eAgMnPc1d1	řecký
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
nazvali	nazvat	k5eAaBmAgMnP	nazvat
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
Ophiussa	Ophiussa	k1gFnSc1	Ophiussa
(	(	kIx(	(
<g/>
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
Země	zem	k1gFnSc2	zem
hadů	had	k1gMnPc2	had
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
uctívali	uctívat	k5eAaImAgMnP	uctívat
hady	had	k1gMnPc4	had
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
břehům	břeh	k1gInPc3	břeh
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
Féničané	Féničan	k1gMnPc1	Féničan
-	-	kIx~	-
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1104	[number]	k4	1104
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
zanechali	zanechat	k5eAaPmAgMnP	zanechat
rovněž	rovněž	k9	rovněž
četné	četný	k2eAgFnPc4d1	četná
stopy	stopa	k1gFnPc4	stopa
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
území	území	k1gNnSc6	území
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
Kelti	Kelt	k1gMnPc1	Kelt
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
smísili	smísit	k5eAaPmAgMnP	smísit
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
238	[number]	k4	238
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
břehy	břeh	k1gInPc7	břeh
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pouhých	pouhý	k2eAgNnPc2d1	pouhé
devatenáct	devatenáct	k4xCc4	devatenáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
však	však	k9	však
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
též	též	k9	též
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
za	za	k7c2	za
Punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
Kartágince	Kartáginec	k1gInSc2	Kartáginec
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
provincie	provincie	k1gFnSc2	provincie
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
,	,	kIx,	,
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
dělení	dělení	k1gNnSc6	dělení
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
octla	octnout	k5eAaPmAgFnS	octnout
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Lusitánie	Lusitánie	k1gFnSc2	Lusitánie
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
podle	podle	k7c2	podle
kmene	kmen	k1gInSc2	kmen
Lusitánů	Lusitán	k1gInPc2	Lusitán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
poloostrova	poloostrov	k1gInSc2	poloostrov
Svébská	Svébský	k2eAgFnSc1d1	Svébský
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Vizigóti	Vizigót	k1gMnPc1	Vizigót
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgNnPc4	tři
století	století	k1gNnPc2	století
později	pozdě	k6eAd2	pozdě
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
reconquisty	reconquista	k1gMnSc2	reconquista
Portugalské	portugalský	k2eAgNnSc4d1	portugalské
hrabství	hrabství	k1gNnSc4	hrabství
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1139	[number]	k4	1139
hrabě	hrabě	k1gMnSc1	hrabě
Alfons	Alfons	k1gMnSc1	Alfons
I.	I.	kA	I.
Portugalský	portugalský	k2eAgMnSc1d1	portugalský
již	již	k6eAd1	již
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
portugalský	portugalský	k2eAgMnSc1d1	portugalský
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
reconquista	reconquist	k1gMnSc4	reconquist
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
historického	historický	k2eAgMnSc2d1	historický
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
vojenský	vojenský	k2eAgInSc4d1	vojenský
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
civilizační	civilizační	k2eAgInSc4d1	civilizační
proces	proces	k1gInSc4	proces
znovudobytí	znovudobytí	k1gNnSc2	znovudobytí
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
evropskou	evropský	k2eAgFnSc7d1	Evropská
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postupně	postupně	k6eAd1	postupně
rozložila	rozložit	k5eAaPmAgFnS	rozložit
tzv.	tzv.	kA	tzv.
Córdobský	Córdobský	k2eAgInSc4d1	Córdobský
chalífát	chalífát	k1gInSc4	chalífát
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
právě	právě	k6eAd1	právě
vznikem	vznik	k1gInSc7	vznik
dvou	dva	k4xCgNnPc2	dva
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
království	království	k1gNnPc2	království
-	-	kIx~	-
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
křižáků	křižák	k1gInPc2	křižák
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Alfons	Alfons	k1gMnSc1	Alfons
I.	I.	kA	I.
Lisabon	Lisabon	k1gInSc1	Lisabon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1147	[number]	k4	1147
<g/>
;	;	kIx,	;
reconquista	reconquista	k1gMnSc1	reconquista
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
dovršena	dovršen	k2eAgFnSc1d1	dovršena
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Španělsku	Španělsko	k1gNnSc6	Španělsko
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
prince	princ	k1gMnSc2	princ
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
průkopníkem	průkopník	k1gMnSc7	průkopník
zámořské	zámořský	k2eAgFnSc2d1	zámořská
expanze	expanze	k1gFnSc2	expanze
a	a	k8xC	a
získávalo	získávat	k5eAaImAgNnS	získávat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
koloniální	koloniální	k2eAgFnPc4d1	koloniální
državy	država	k1gFnPc4	država
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Ceuty	Ceuta	k1gFnSc2	Ceuta
(	(	kIx(	(
<g/>
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
postupovali	postupovat	k5eAaImAgMnP	postupovat
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
afrického	africký	k2eAgNnSc2d1	africké
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
až	až	k9	až
roku	rok	k1gInSc2	rok
1488	[number]	k4	1488
Bartolomeu	Bartolomeus	k1gInSc2	Bartolomeus
Dias	Dias	k1gInSc1	Dias
obeplul	obeplout	k5eAaPmAgInS	obeplout
Mys	mys	k1gInSc4	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
přistál	přistát	k5eAaPmAgInS	přistát
Vasco	Vasco	k1gNnSc4	Vasco
da	da	k?	da
Gama	gama	k1gNnSc2	gama
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikají	vznikat	k5eAaImIp3nP	vznikat
další	další	k2eAgFnPc4d1	další
portugalské	portugalský	k2eAgFnPc4d1	portugalská
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1505	[number]	k4	1505
tzv.	tzv.	kA	tzv.
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
pronikají	pronikat	k5eAaImIp3nP	pronikat
Portugalci	Portugalec	k1gMnPc1	Portugalec
také	také	k9	také
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jejich	jejich	k3xOp3gFnSc1	jejich
největší	veliký	k2eAgFnSc1d3	veliký
kolonie	kolonie	k1gFnSc1	kolonie
-	-	kIx~	-
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
koloniální	koloniální	k2eAgFnSc1d1	koloniální
říše	říše	k1gFnSc1	říše
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
nejdelší	dlouhý	k2eAgNnPc1d3	nejdelší
trvání	trvání	k1gNnPc1	trvání
<g/>
:	:	kIx,	:
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
se	se	k3xPyFc4	se
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Macao	Macao	k1gNnSc1	Macao
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
Číně	Čína	k1gFnSc6	Čína
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
někdejších	někdejší	k2eAgFnPc6d1	někdejší
koloniích	kolonie	k1gFnPc6	kolonie
používána	používán	k2eAgFnSc1d1	používána
portugalština	portugalština	k1gFnSc1	portugalština
a	a	k8xC	a
vyznáváno	vyznáván	k2eAgNnSc1d1	vyznáváno
katolictví	katolictví	k1gNnSc1	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
Globální	globální	k2eAgFnSc2d1	globální
expanze	expanze	k1gFnSc2	expanze
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
vyčerpala	vyčerpat	k5eAaPmAgFnS	vyčerpat
a	a	k8xC	a
při	při	k7c6	při
nástupnické	nástupnický	k2eAgFnSc6d1	nástupnická
krizi	krize	k1gFnSc6	krize
roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
bylo	být	k5eAaImAgNnS	být
přinuceno	přinutit	k5eAaPmNgNnS	přinutit
k	k	k7c3	k
personální	personální	k2eAgFnSc3d1	personální
unii	unie	k1gFnSc3	unie
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
roku	rok	k1gInSc2	rok
1640	[number]	k4	1640
za	za	k7c2	za
Jana	Jan	k1gMnSc2	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
uznali	uznat	k5eAaPmAgMnP	uznat
až	až	k9	až
po	po	k7c6	po
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1668	[number]	k4	1668
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
postihlo	postihnout	k5eAaPmAgNnS	postihnout
Lisabon	Lisabon	k1gInSc4	Lisabon
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
markýz	markýz	k1gMnSc1	markýz
de	de	k?	de
Pombal	Pombal	k1gInSc1	Pombal
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
obnovu	obnova	k1gFnSc4	obnova
města	město	k1gNnSc2	město
v	v	k7c6	v
imperiálním	imperiální	k2eAgInSc6d1	imperiální
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgInS	prosadit
merkantilistické	merkantilistický	k2eAgFnPc4d1	merkantilistická
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
osvícenské	osvícenský	k2eAgFnPc4d1	osvícenská
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Coby	Coby	k?	Coby
tradiční	tradiční	k2eAgMnSc1d1	tradiční
spojenec	spojenec	k1gMnSc1	spojenec
Anglie	Anglie	k1gFnSc2	Anglie
bylo	být	k5eAaImAgNnS	být
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
napadeno	napadnout	k5eAaPmNgNnS	napadnout
Napoleonem	napoleon	k1gInSc7	napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Ria	Ria	k1gFnSc2	Ria
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
a	a	k8xC	a
ovládal	ovládat	k5eAaImAgMnS	ovládat
své	svůj	k3xOyFgFnPc4	svůj
državy	država	k1gFnPc4	država
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1815	[number]	k4	1815
Spojené	spojený	k2eAgInPc1d1	spojený
království	království	k1gNnSc2	království
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
Algarves	Algarvesa	k1gFnPc2	Algarvesa
<g/>
)	)	kIx)	)
odtud	odtud	k6eAd1	odtud
až	až	k9	až
do	do	k7c2	do
návratu	návrat	k1gInSc2	návrat
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
se	se	k3xPyFc4	se
však	však	k9	však
Brazílie	Brazílie	k1gFnSc1	Brazílie
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1828	[number]	k4	1828
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
probíhaly	probíhat	k5eAaImAgInP	probíhat
tzv.	tzv.	kA	tzv.
liberální	liberální	k2eAgInPc1d1	liberální
války	válek	k1gInPc1	válek
mezi	mezi	k7c7	mezi
absolutisty	absolutista	k1gMnPc7	absolutista
a	a	k8xC	a
konstitucionalisty	konstitucionalista	k1gMnPc7	konstitucionalista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
spojenectví	spojenectví	k1gNnSc1	spojenectví
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
portugalskými	portugalský	k2eAgInPc7d1	portugalský
koloniálními	koloniální	k2eAgInPc7d1	koloniální
zájmy	zájem	k1gInPc7	zájem
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
republikou	republika	k1gFnSc7	republika
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
stálo	stát	k5eAaImAgNnS	stát
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
portugalská	portugalský	k2eAgFnSc1d1	portugalská
republika	republika	k1gFnSc1	republika
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zmítána	zmítán	k2eAgFnSc1d1	zmítána
nepokoji	nepokoj	k1gInPc7	nepokoj
a	a	k8xC	a
neměla	mít	k5eNaImAgNnP	mít
tak	tak	k6eAd1	tak
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vojenskému	vojenský	k2eAgInSc3d1	vojenský
puči	puč	k1gInSc3	puč
<g/>
.	.	kIx.	.
</s>
<s>
António	António	k1gMnSc1	António
de	de	k?	de
Oliveira	Oliveira	k1gMnSc1	Oliveira
Salazar	Salazar	k1gMnSc1	Salazar
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zmítané	zmítaný	k2eAgFnSc6d1	zmítaná
krizí	krize	k1gFnSc7	krize
diktaturu	diktatura	k1gFnSc4	diktatura
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
autoritářském	autoritářský	k2eAgInSc6d1	autoritářský
konzervatismu	konzervatismus	k1gInSc6	konzervatismus
<g/>
,	,	kIx,	,
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
a	a	k8xC	a
imperialismu	imperialismus	k1gInSc2	imperialismus
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
<g/>
;	;	kIx,	;
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Estado	Estada	k1gFnSc5	Estada
Novo	nova	k1gFnSc5	nova
<g/>
,	,	kIx,	,
vyhlášený	vyhlášený	k2eAgInSc1d1	vyhlášený
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
trval	trvat	k5eAaImAgInS	trvat
41	[number]	k4	41
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
neutrální	neutrální	k2eAgNnSc1d1	neutrální
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
zakládajícím	zakládající	k2eAgInSc6d1	zakládající
členem	člen	k1gInSc7	člen
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
svoje	svůj	k3xOyFgNnSc4	svůj
koloniální	koloniální	k2eAgNnSc4d1	koloniální
panství	panství	k1gNnSc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rozklad	rozklad	k1gInSc4	rozklad
započal	započnout	k5eAaPmAgMnS	započnout
rokem	rok	k1gInSc7	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Indie	Indie	k1gFnSc1	Indie
připojila	připojit	k5eAaPmAgFnS	připojit
četná	četný	k2eAgNnPc4d1	četné
území	území	k1gNnPc4	území
včetně	včetně	k7c2	včetně
Goy	Goa	k1gFnSc2	Goa
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
ozývaly	ozývat	k5eAaImAgInP	ozývat
hlasy	hlas	k1gInPc1	hlas
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
afrických	africký	k2eAgFnPc2d1	africká
kolonií	kolonie	k1gFnPc2	kolonie
(	(	kIx(	(
<g/>
Angola	Angola	k1gFnSc1	Angola
<g/>
,	,	kIx,	,
Mozambik	Mozambik	k1gInSc1	Mozambik
<g/>
,	,	kIx,	,
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc1d1	princův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Kapverdy	Kapverd	k1gInPc1	Kapverd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nákladné	nákladný	k2eAgFnPc1d1	nákladná
a	a	k8xC	a
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
koloniální	koloniální	k2eAgFnPc1d1	koloniální
války	válka	k1gFnPc1	válka
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
salazarovského	salazarovský	k2eAgInSc2d1	salazarovský
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Marcelo	Marcela	k1gFnSc5	Marcela
Caetano	Caetana	k1gFnSc5	Caetana
<g/>
,	,	kIx,	,
nástupce	nástupce	k1gMnSc1	nástupce
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
vládnoucího	vládnoucí	k2eAgNnSc2d1	vládnoucí
Antónia	Antónium	k1gNnSc2	Antónium
de	de	k?	de
Oliveiry	Oliveira	k1gMnSc2	Oliveira
Salazara	Salazar	k1gMnSc2	Salazar
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
armádou	armáda	k1gFnSc7	armáda
během	během	k7c2	během
tzv.	tzv.	kA	tzv.
<g/>
Karafiátové	Karafiátové	k2eAgFnSc2d1	Karafiátové
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
25.4	[number]	k4	25.4
<g/>
.1974	.1974	k4	.1974
<g/>
,	,	kIx,	,
a	a	k8xC	a
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
pak	pak	k6eAd1	pak
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
cestu	cesta	k1gFnSc4	cesta
pluralitní	pluralitní	k2eAgFnSc2d1	pluralitní
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
portugalština	portugalština	k1gFnSc1	portugalština
úředním	úřední	k2eAgMnSc7d1	úřední
jazykem	jazyk	k1gMnSc7	jazyk
/	/	kIx~	/
<g/>
země	země	k1gFnSc1	země
PALOP	PALOP	kA	PALOP
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
z	z	k7c2	z
Guineje-Bissau	Guineje-Bissaus	k1gInSc2	Guineje-Bissaus
<g/>
,	,	kIx,	,
Angoly	Angola	k1gFnSc2	Angola
<g/>
,	,	kIx,	,
Mosambiku	Mosambik	k1gInSc2	Mosambik
<g/>
,	,	kIx,	,
Sao	Sao	k1gFnSc2	Sao
Tomé	Tomá	k1gFnSc2	Tomá
a	a	k8xC	a
Principe	princip	k1gInSc5	princip
a	a	k8xC	a
z	z	k7c2	z
Kapverdských	kapverdský	k2eAgInPc2d1	kapverdský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rekrutovala	rekrutovat	k5eAaImAgFnS	rekrutovat
většina	většina	k1gFnSc1	většina
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
cizinců	cizinec	k1gMnPc2	cizinec
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
nelegálních	legální	k2eNgMnPc2d1	nelegální
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
země	země	k1gFnSc1	země
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
společně	společně	k6eAd1	společně
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
do	do	k7c2	do
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Eurozóny	Eurozóna	k1gFnSc2	Eurozóna
a	a	k8xC	a
předala	předat	k5eAaPmAgFnS	předat
Číně	Čína	k1gFnSc6	Čína
Macao	Macao	k1gNnSc1	Macao
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
během	během	k7c2	během
třetího	třetí	k4xOgNnSc2	třetí
portugalského	portugalský	k2eAgNnSc2d1	portugalské
předsednictví	předsednictví	k1gNnSc2	předsednictví
EU	EU	kA	EU
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
formulována	formulovat	k5eAaImNgFnS	formulovat
a	a	k8xC	a
podepsána	podepsán	k2eAgFnSc1d1	podepsána
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc4d1	severní
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc4d1	střední
a	a	k8xC	a
východní	východní	k2eAgNnSc4d1	východní
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
horská	horský	k2eAgNnPc1d1	horské
pásma	pásmo	k1gNnPc1	pásmo
Serra	Serr	k1gMnSc2	Serr
de	de	k?	de
Nogueira	Nogueir	k1gMnSc4	Nogueir
<g/>
,	,	kIx,	,
Serra	Serr	k1gMnSc4	Serr
do	do	k7c2	do
Gerez	Gereza	k1gFnPc2	Gereza
<g/>
,	,	kIx,	,
Serra	Serra	k1gMnSc1	Serra
de	de	k?	de
Marã	Marã	k1gMnSc1	Marã
<g/>
,	,	kIx,	,
Serra	Serra	k1gMnSc1	Serra
da	da	k?	da
Estrela	Estrela	k1gFnSc1	Estrela
(	(	kIx(	(
<g/>
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Serra	Serra	k1gMnSc1	Serra
de	de	k?	de
Sã	Sã	k1gMnSc5	Sã
Mamede	Mamed	k1gMnSc5	Mamed
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
Alentejo	Alentejo	k6eAd1	Alentejo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
cípu	cíp	k1gInSc6	cíp
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Serra	Serra	k1gFnSc1	Serra
de	de	k?	de
Monchique	Monchique	k1gInSc1	Monchique
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
nížiny	nížina	k1gFnPc4	nížina
se	s	k7c7	s
strmým	strmý	k2eAgNnSc7d1	strmé
skalnatým	skalnatý	k2eAgNnSc7d1	skalnaté
pobřežím	pobřeží	k1gNnSc7	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc4	souostroví
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
Azorské	azorský	k2eAgInPc1d1	azorský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
riftové	riftový	k2eAgFnSc6d1	riftový
zóně	zóna	k1gFnSc6	zóna
na	na	k7c6	na
styku	styk	k1gInSc6	styk
dvou	dva	k4xCgFnPc2	dva
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sopečného	sopečný	k2eAgMnSc4d1	sopečný
původu	původa	k1gMnSc4	původa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
větší	veliký	k2eAgNnSc4d2	veliký
riziko	riziko	k1gNnSc4	riziko
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
seizmicky	seizmicky	k6eAd1	seizmicky
aktivní	aktivní	k2eAgFnSc7d1	aktivní
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
i	i	k9	i
Lisabonský	lisabonský	k2eAgInSc1d1	lisabonský
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Tejo	Tejo	k1gNnSc1	Tejo
<g/>
,	,	kIx,	,
Douro	Douro	k1gNnSc1	Douro
a	a	k8xC	a
Guadiana	Guadiana	k1gFnSc1	Guadiana
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kaskádových	kaskádový	k2eAgFnPc2d1	kaskádová
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
západě	západ	k1gInSc6	západ
mírné	mírný	k2eAgFnSc2d1	mírná
oceánské	oceánský	k2eAgFnSc2d1	oceánská
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
subtropické	subtropický	k2eAgNnSc1d1	subtropické
středomořské	středomořský	k2eAgNnSc1d1	středomořské
(	(	kIx(	(
<g/>
suché	suchý	k2eAgNnSc1d1	suché
horké	horký	k2eAgNnSc1d1	horké
léto	léto	k1gNnSc1	léto
a	a	k8xC	a
mírná	mírný	k2eAgFnSc1d1	mírná
vlhká	vlhký	k2eAgFnSc1d1	vlhká
zima	zima	k1gFnSc1	zima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
červencová	červencový	k2eAgFnSc1d1	červencová
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
lednová	lednový	k2eAgFnSc1d1	lednová
mezi	mezi	k7c4	mezi
9	[number]	k4	9
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
12	[number]	k4	12
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
klesá	klesat	k5eAaImIp3nS	klesat
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
od	od	k7c2	od
1200	[number]	k4	1200
mm	mm	kA	mm
až	až	k6eAd1	až
po	po	k7c4	po
500	[number]	k4	500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
je	být	k5eAaImIp3nS	být
18,5	[number]	k4	18,5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
průměrné	průměrný	k2eAgFnPc1d1	průměrná
srážky	srážka	k1gFnPc1	srážka
650	[number]	k4	650
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
dělení	dělení	k1gNnSc2	dělení
na	na	k7c4	na
obce	obec	k1gFnPc4	obec
nemá	mít	k5eNaImIp3nS	mít
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
město	město	k1gNnSc1	město
nad	nad	k7c7	nad
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
fakticky	fakticky	k6eAd1	fakticky
srostly	srůst	k5eAaPmAgFnP	srůst
s	s	k7c7	s
centry	centr	k1gInPc7	centr
aglomerací	aglomerace	k1gFnPc2	aglomerace
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
samostatné	samostatný	k2eAgFnPc1d1	samostatná
<g/>
.	.	kIx.	.
</s>
<s>
Lisabon	Lisabon	k1gInSc1	Lisabon
(	(	kIx(	(
<g/>
499	[number]	k4	499
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
třímiliónové	třímiliónový	k2eAgFnSc2d1	třímiliónová
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Regiã	Regiã	k1gMnSc2	Regiã
de	de	k?	de
Lisboa	Lisbous	k1gMnSc2	Lisbous
a	a	k8xC	a
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kulturním	kulturní	k2eAgNnSc7d1	kulturní
<g/>
,	,	kIx,	,
dopravním	dopravní	k2eAgNnSc7d1	dopravní
a	a	k8xC	a
především	především	k6eAd1	především
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
centrem	centrum	k1gNnSc7	centrum
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
oblastí	oblast	k1gFnSc7	oblast
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
s	s	k7c7	s
HDP	HDP	kA	HDP
nad	nad	k7c7	nad
průměrem	průměr	k1gInSc7	průměr
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střediskem	středisko	k1gNnSc7	středisko
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
1,5	[number]	k4	1,5
milióny	milión	k4xCgInPc1	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Porto	porto	k1gNnSc1	porto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
čítá	čítat	k5eAaImIp3nS	čítat
240	[number]	k4	240
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
severoportugalským	severoportugalský	k2eAgInSc7d1	severoportugalský
přístavem	přístav	k1gInSc7	přístav
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Douro	Douro	k1gNnSc4	Douro
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
velká	velký	k2eAgNnPc4d1	velké
města	město	k1gNnPc4	město
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
zalidněném	zalidněný	k2eAgNnSc6d1	zalidněné
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
patří	patřit	k5eAaImIp3nS	patřit
Braga	Braga	k1gFnSc1	Braga
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
a	a	k8xC	a
náboženské	náboženský	k2eAgNnSc4d1	náboženské
středisko	středisko	k1gNnSc4	středisko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Guimarã	Guimarã	k1gFnSc1	Guimarã
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
pak	pak	k6eAd1	pak
Coimbra	Coimbra	k1gMnSc1	Coimbra
(	(	kIx(	(
<g/>
157	[number]	k4	157
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
nejstarší	starý	k2eAgFnSc2d3	nejstarší
portugalské	portugalský	k2eAgFnSc2d1	portugalská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Alenteju	Alenteju	k1gFnSc6	Alenteju
historická	historický	k2eAgFnSc1d1	historická
Évora	Évora	k1gFnSc1	Évora
a	a	k8xC	a
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Faro	fara	k1gFnSc5	fara
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Madeiry	Madeira	k1gFnSc2	Madeira
je	být	k5eAaImIp3nS	být
Funchal	Funchal	k1gFnSc1	Funchal
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
města	město	k1gNnPc1	město
na	na	k7c6	na
Azorech	azor	k1gInPc6	azor
jsou	být	k5eAaImIp3nP	být
Ponta	Ponta	k1gFnSc1	Ponta
Delgada	Delgada	k1gFnSc1	Delgada
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
Sã	Sã	k1gMnSc1	Sã
Miguel	Miguel	k1gMnSc1	Miguel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Angra	Angra	k1gMnSc1	Angra
do	do	k7c2	do
Heroísmo	Heroísma	k1gFnSc5	Heroísma
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
Terceira	Terceir	k1gInSc2	Terceir
<g/>
)	)	kIx)	)
a	a	k8xC	a
Horta	Hort	k1gMnSc2	Hort
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
Faial	Faial	k1gInSc1	Faial
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
navrženou	navržený	k2eAgFnSc4d1	navržená
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
pravomoc	pravomoc	k1gFnSc4	pravomoc
rozpustit	rozpustit	k5eAaPmF	rozpustit
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
odvolat	odvolat	k5eAaPmF	odvolat
vládu	vláda	k1gFnSc4	vláda
či	či	k8xC	či
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
omezeny	omezit	k5eAaPmNgInP	omezit
řadou	řada	k1gFnSc7	řada
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
než	než	k8xS	než
patří	patřit	k5eAaImIp3nS	patřit
kupříkladu	kupříkladu	k6eAd1	kupříkladu
povinnost	povinnost	k1gFnSc4	povinnost
konzultací	konzultace	k1gFnPc2	konzultace
se	s	k7c7	s
sedmnáctičlennou	sedmnáctičlenný	k2eAgFnSc7d1	sedmnáctičlenná
Státní	státní	k2eAgFnSc7d1	státní
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
poradní	poradní	k2eAgInSc1d1	poradní
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
Republikového	republikový	k2eAgNnSc2d1	republikové
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
předsedy	předseda	k1gMnSc2	předseda
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Ochránce	ochránce	k1gMnSc4	ochránce
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
předsedů	předseda	k1gMnPc2	předseda
regionálních	regionální	k2eAgFnPc2d1	regionální
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
z	z	k7c2	z
pěti	pět	k4xCc2	pět
občanů	občan	k1gMnPc2	občan
vybraných	vybraný	k2eAgMnPc2d1	vybraný
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
z	z	k7c2	z
pěti	pět	k4xCc2	pět
občanů	občan	k1gMnPc2	občan
volených	volený	k2eAgInPc2d1	volený
republikovým	republikový	k2eAgNnSc7d1	republikové
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
využívanou	využívaný	k2eAgFnSc7d1	využívaná
pravomocí	pravomoc	k1gFnSc7	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
je	být	k5eAaImIp3nS	být
vetování	vetování	k1gNnSc4	vetování
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Republikové	republikový	k2eAgNnSc1d1	republikové
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
Assembleia	Assembleia	k1gFnSc1	Assembleia
da	da	k?	da
República	República	k1gFnSc1	República
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
portugalského	portugalský	k2eAgInSc2d1	portugalský
jednokomorového	jednokomorový	k2eAgInSc2d1	jednokomorový
parlamentu	parlament	k1gInSc2	parlament
složeného	složený	k2eAgNnSc2d1	složené
z	z	k7c2	z
230	[number]	k4	230
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
na	na	k7c6	na
základě	základ	k1gInSc6	základ
principu	princip	k1gInSc2	princip
poměrného	poměrný	k2eAgNnSc2d1	poměrné
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Republikového	republikový	k2eAgNnSc2d1	republikové
shromáždění	shromáždění	k1gNnSc2	shromáždění
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
prezidenta	prezident	k1gMnSc4	prezident
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
musí	muset	k5eAaImIp3nS	muset
parlamentu	parlament	k1gInSc2	parlament
předložit	předložit	k5eAaPmF	předložit
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
zamítnut	zamítnout	k5eAaPmNgMnS	zamítnout
většinou	většina	k1gFnSc7	většina
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
potvrzení	potvrzení	k1gNnSc1	potvrzení
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
vede	vést	k5eAaImIp3nS	vést
dlouholetý	dlouholetý	k2eAgInSc4d1	dlouholetý
spor	spor	k1gInSc4	spor
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
Olivença	Olivenç	k1gInSc2	Olivenç
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Olivenza	Olivenz	k1gMnSc2	Olivenz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
součástí	součást	k1gFnSc7	součást
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
po	po	k7c6	po
drtivé	drtivý	k2eAgFnSc6d1	drtivá
porážce	porážka	k1gFnSc6	porážka
od	od	k7c2	od
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
spojence	spojenec	k1gMnSc2	spojenec
Španělska	Španělsko	k1gNnSc2	Španělsko
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
muselo	muset	k5eAaImAgNnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
právě	právě	k9	právě
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
si	se	k3xPyFc3	se
nárokuje	nárokovat	k5eAaImIp3nS	nárokovat
jeho	on	k3xPp3gNnSc2	on
navrácení	navrácení	k1gNnSc2	navrácení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
čl	čl	kA	čl
<g/>
.	.	kIx.	.
105	[number]	k4	105
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ustavila	ustavit	k5eAaPmAgFnS	ustavit
evropské	evropský	k2eAgInPc4d1	evropský
poměry	poměr	k1gInPc4	poměr
po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgFnSc6d1	Napoleonova
porážce	porážka	k1gFnSc6	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
18	[number]	k4	18
distriktů	distrikt	k1gInPc2	distrikt
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
části	část	k1gFnSc6	část
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
autonomních	autonomní	k2eAgInPc2d1	autonomní
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
Azory	azor	k1gInPc1	azor
a	a	k8xC	a
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
děleny	dělit	k5eAaImNgInP	dělit
do	do	k7c2	do
308	[number]	k4	308
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
"	"	kIx"	"
<g/>
Município	Município	k6eAd1	Município
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Concelho	Concel	k1gMnSc4	Concel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
3092	[number]	k4	3092
farností	farnost	k1gFnPc2	farnost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Freguesia	Freguesia	k1gFnSc1	Freguesia
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
územně-správního	územněprávní	k2eAgNnSc2d1	územně-správní
dělení	dělení	k1gNnSc2	dělení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
význam	význam	k1gInSc4	význam
distriktů	distrikt	k1gInPc2	distrikt
upadá	upadat	k5eAaImIp3nS	upadat
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
meziokresních	meziokresní	k2eAgNnPc2d1	meziokresní
společenství	společenství	k1gNnPc2	společenství
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontinentálním	kontinentální	k2eAgNnSc6d1	kontinentální
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
existuje	existovat	k5eAaImIp3nS	existovat
21	[number]	k4	21
meziokresních	meziokresní	k2eAgNnPc2d1	meziokresní
společenství	společenství	k1gNnPc2	společenství
a	a	k8xC	a
2	[number]	k4	2
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
:	:	kIx,	:
Alentejo	Alenteja	k1gMnSc5	Alenteja
Central	Central	k1gMnSc5	Central
<g/>
,	,	kIx,	,
Alentejo	Alenteja	k1gMnSc5	Alenteja
Litoral	Litoral	k1gMnSc5	Litoral
<g/>
,	,	kIx,	,
Algarve	Algarv	k1gMnSc5	Algarv
<g/>
,	,	kIx,	,
Alto	Alta	k1gMnSc5	Alta
Alentejo	Alenteja	k1gMnSc5	Alenteja
<g/>
,	,	kIx,	,
Alto	Alta	k1gMnSc5	Alta
Minho	Minha	k1gMnSc5	Minha
<g/>
,	,	kIx,	,
Alto	Alto	k6eAd1	Alto
Tâmega	Tâmega	k1gFnSc1	Tâmega
<g/>
,	,	kIx,	,
Ave	ave	k1gNnSc1	ave
<g/>
,	,	kIx,	,
Baixo	Baixo	k1gMnSc1	Baixo
Alentejo	Alentejo	k1gMnSc1	Alentejo
<g/>
,	,	kIx,	,
Beira	Beira	k1gMnSc1	Beira
Baixa	Baixa	k1gMnSc1	Baixa
<g/>
,	,	kIx,	,
Beiras	Beiras	k1gMnSc1	Beiras
e	e	k0	e
Serra	Serro	k1gNnPc4	Serro
<g />
.	.	kIx.	.
</s>
<s>
da	da	k?	da
Estrela	Estrela	k1gFnSc1	Estrela
<g/>
,	,	kIx,	,
Cávado	Cávada	k1gFnSc5	Cávada
<g/>
,	,	kIx,	,
Douro	Doura	k1gFnSc5	Doura
<g/>
,	,	kIx,	,
Lezíria	Lezírium	k1gNnSc2	Lezírium
do	do	k7c2	do
Tejo	Tejo	k6eAd1	Tejo
<g/>
,	,	kIx,	,
Médio	Média	k1gMnSc5	Média
Tejo	Teja	k1gMnSc5	Teja
<g/>
,	,	kIx,	,
Oeste	Oest	k1gMnSc5	Oest
<g/>
,	,	kIx,	,
Regiã	Regiã	k1gFnPc2	Regiã
de	de	k?	de
Aveiro	Aveiro	k1gNnSc4	Aveiro
<g/>
,	,	kIx,	,
Regiã	Regiã	k1gMnSc1	Regiã
de	de	k?	de
Coimbra	Coimbra	k1gMnSc1	Coimbra
<g/>
,	,	kIx,	,
Regiã	Regiã	k1gMnSc1	Regiã
de	de	k?	de
Leiria	Leirium	k1gNnSc2	Leirium
<g/>
,	,	kIx,	,
Tâmega	Tâmega	k1gFnSc1	Tâmega
e	e	k0	e
Sousa	Sousa	k1gFnSc1	Sousa
<g/>
,	,	kIx,	,
Terras	Terras	k1gMnSc1	Terras
de	de	k?	de
Trás-os-Montes	Tráss-Montes	k1gMnSc1	Trás-os-Montes
<g/>
,	,	kIx,	,
Viseu	Vise	k2eAgFnSc4d1	Vise
Dã	Dã	k1gFnSc4	Dã
Lafõ	Lafõ	k1gFnSc2	Lafõ
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
Lisabonu	Lisabon	k1gInSc2	Lisabon
<g/>
,	,	kIx,	,
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Porta	porto	k1gNnSc2	porto
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
je	být	k5eAaImIp3nS	být
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
a	a	k8xC	a
také	také	k9	také
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
stát	stát	k1gInSc1	stát
se	s	k7c7	s
zásobami	zásoba	k1gFnPc7	zásoba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
stát	stát	k1gInSc1	stát
značného	značný	k2eAgInSc2d1	značný
rozmachu	rozmach	k1gInSc2	rozmach
dříve	dříve	k6eAd2	dříve
stangujícího	stangující	k2eAgNnSc2d1	stangující
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
přílivu	příliv	k1gInSc2	příliv
investic	investice	k1gFnPc2	investice
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Varující	varující	k2eAgFnSc1d1	varující
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
asi	asi	k9	asi
5	[number]	k4	5
<g/>
%	%	kIx~	%
negramotnost	negramotnost	k1gFnSc1	negramotnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
hutnictví	hutnictví	k1gNnPc4	hutnictví
<g/>
,	,	kIx,	,
těžební	těžební	k2eAgInPc4d1	těžební
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInPc4d1	textilní
<g/>
,	,	kIx,	,
obuvnický	obuvnický	k2eAgInSc4d1	obuvnický
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
wolfram	wolfram	k1gInSc1	wolfram
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
černé	černý	k2eAgNnSc1d1	černé
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
kaolin	kaolin	k1gInSc1	kaolin
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
mramor	mramor	k1gInSc1	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
75	[number]	k4	75
%	%	kIx~	%
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
výměny	výměna	k1gFnSc2	výměna
zboží	zboží	k1gNnSc2	zboží
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c6	na
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
střediska	středisko	k1gNnPc1	středisko
jsou	být	k5eAaImIp3nP	být
Lisabon	Lisabon	k1gInSc4	Lisabon
a	a	k8xC	a
Porto	porto	k1gNnSc1	porto
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgMnSc7d1	důležitý
producentem	producent	k1gMnSc7	producent
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP	být
vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
převažuje	převažovat	k5eAaImIp3nS	převažovat
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
produkce	produkce	k1gFnSc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
zabírá	zabírat	k5eAaImIp3nS	zabírat
32	[number]	k4	32
%	%	kIx~	%
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
36	[number]	k4	36
%	%	kIx~	%
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
11	[number]	k4	11
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
korku	korek	k1gInSc2	korek
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
korku	korek	k1gInSc2	korek
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
180	[number]	k4	180
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
a	a	k8xC	a
subtropické	subtropický	k2eAgNnSc1d1	subtropické
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
)	)	kIx)	)
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
oslů	osel	k1gMnPc2	osel
a	a	k8xC	a
mul	mula	k1gFnPc2	mula
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
produkce	produkce	k1gFnSc1	produkce
rybích	rybí	k2eAgInPc2d1	rybí
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
rybí	rybí	k2eAgFnPc4d1	rybí
konzervy	konzerva	k1gFnPc4	konzerva
a	a	k8xC	a
rybí	rybí	k2eAgFnSc1d1	rybí
moučka	moučka	k1gFnSc1	moučka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgInPc1d1	výrazný
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Nejatraktivnější	atraktivní	k2eAgNnPc1d3	nejatraktivnější
turistická	turistický	k2eAgNnPc1d1	turistické
střediska	středisko	k1gNnPc1	středisko
jsou	být	k5eAaImIp3nP	být
Lisabon	Lisabon	k1gInSc4	Lisabon
<g/>
,	,	kIx,	,
Porto	porto	k1gNnSc1	porto
<g/>
,	,	kIx,	,
Coimbra	Coimbra	k1gFnSc1	Coimbra
<g/>
,	,	kIx,	,
pobřeží	pobřeží	k1gNnSc1	pobřeží
Algarve	Algarev	k1gFnSc2	Algarev
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc4	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
Azory	Azory	k1gFnPc1	Azory
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
navštíví	navštívit	k5eAaPmIp3nS	navštívit
ročně	ročně	k6eAd1	ročně
cca	cca	kA	cca
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
používalo	používat	k5eAaImAgNnS	používat
ve	v	k7c4	v
4	[number]	k4	4
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgInPc6d1	jdoucí
dnech	den	k1gInPc6	den
(	(	kIx(	(
<g/>
7.5	[number]	k4	7.5
<g/>
.2016	.2016	k4	.2016
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
h	h	k?	h
-	-	kIx~	-
11.5	[number]	k4	11.5
<g/>
.2016	.2016	k4	.2016
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
elektřinu	elektřina	k1gFnSc4	elektřina
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
nefosilních	fosilní	k2eNgInPc2d1	fosilní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ze	z	k7c2	z
solárních	solární	k2eAgFnPc2d1	solární
<g/>
,	,	kIx,	,
vodních	vodní	k2eAgFnPc2d1	vodní
a	a	k8xC	a
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
periferní	periferní	k2eAgFnSc4d1	periferní
a	a	k8xC	a
přímořskou	přímořský	k2eAgFnSc4d1	přímořská
polohu	poloha	k1gFnSc4	poloha
se	se	k3xPyFc4	se
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
na	na	k7c4	na
námořní	námořní	k2eAgFnSc4d1	námořní
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
méně	málo	k6eAd2	málo
na	na	k7c4	na
pozemní	pozemní	k2eAgFnSc4d1	pozemní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
přístavy	přístav	k1gInPc1	přístav
fungují	fungovat	k5eAaImIp3nP	fungovat
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
a	a	k8xC	a
Leixõ	Leixõ	k1gFnSc6	Leixõ
<g/>
;	;	kIx,	;
námořní	námořní	k2eAgFnSc1d1	námořní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
také	také	k9	také
životně	životně	k6eAd1	životně
důležitá	důležitý	k2eAgNnPc1d1	důležité
pro	pro	k7c4	pro
Madeiru	Madeira	k1gFnSc4	Madeira
a	a	k8xC	a
Azory	Azory	k1gFnPc4	Azory
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnPc1d1	velká
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
silniční	silniční	k2eAgFnSc2d1	silniční
a	a	k8xC	a
dálniční	dálniční	k2eAgFnSc2d1	dálniční
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
většině	většina	k1gFnSc6	většina
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vybírá	vybírat	k5eAaImIp3nS	vybírat
mýtné	mýtné	k1gNnSc1	mýtné
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
těchto	tento	k3xDgFnPc2	tento
investic	investice	k1gFnPc2	investice
byl	být	k5eAaImAgInS	být
i	i	k9	i
Most	most	k1gInSc1	most
Vasco	Vasco	k1gMnSc1	Vasco
da	da	k?	da
Gama	gama	k1gNnSc4	gama
nebo	nebo	k8xC	nebo
lisabonské	lisabonský	k2eAgNnSc4d1	Lisabonské
nádraží	nádraží	k1gNnSc4	nádraží
Oriente	Orient	k1gInSc5	Orient
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dodaly	dodat	k5eAaPmAgFnP	dodat
lesk	lesk	k1gInSc4	lesk
výstavě	výstava	k1gFnSc6	výstava
Expo	Expo	k1gNnSc1	Expo
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
zrychlena	zrychlen	k2eAgFnSc1d1	zrychlena
železniční	železniční	k2eAgFnSc1d1	železniční
magistrála	magistrála	k1gFnSc1	magistrála
Lisabon	Lisabon	k1gInSc1	Lisabon
-	-	kIx~	-
Porto	porto	k1gNnSc1	porto
<g/>
.	.	kIx.	.
</s>
<s>
Velikášské	velikášský	k2eAgInPc1d1	velikášský
plány	plán	k1gInPc1	plán
na	na	k7c4	na
vysokorychlostní	vysokorychlostní	k2eAgNnSc4d1	vysokorychlostní
propojení	propojení	k1gNnSc4	propojení
se	s	k7c7	s
španělskou	španělský	k2eAgFnSc7d1	španělská
sítí	síť	k1gFnSc7	síť
AVE	ave	k1gNnSc2	ave
se	se	k3xPyFc4	se
zpožďují	zpožďovat	k5eAaImIp3nP	zpožďovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
následkem	následkem	k7c2	následkem
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Správcem	správce	k1gMnSc7	správce
železniční	železniční	k2eAgFnSc2d1	železniční
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
je	být	k5eAaImIp3nS	být
REFER	REFER	kA	REFER
<g/>
;	;	kIx,	;
aktuálně	aktuálně	k6eAd1	aktuálně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
2843	[number]	k4	2843
km	km	kA	km
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
železničním	železniční	k2eAgInSc7d1	železniční
podnikem	podnik	k1gInSc7	podnik
jsou	být	k5eAaImIp3nP	být
Comboios	Comboios	k1gInSc4	Comboios
de	de	k?	de
Portugal	portugal	k1gInSc1	portugal
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ročně	ročně	k6eAd1	ročně
přepraví	přepravit	k5eAaPmIp3nP	přepravit
okolo	okolo	k7c2	okolo
130	[number]	k4	130
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
méně	málo	k6eAd2	málo
než	než	k8xS	než
ČD	ČD	kA	ČD
ve	v	k7c6	v
srovnatelně	srovnatelně	k6eAd1	srovnatelně
lidnatém	lidnatý	k2eAgNnSc6d1	lidnaté
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hojně	hojně	k6eAd1	hojně
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
zejména	zejména	k9	zejména
příměstské	příměstský	k2eAgInPc1d1	příměstský
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
aglomeracích	aglomerace	k1gFnPc6	aglomerace
Porta	porto	k1gNnSc2	porto
a	a	k8xC	a
Lisabonu	Lisabon	k1gInSc2	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
města	město	k1gNnPc1	město
provozují	provozovat	k5eAaImIp3nP	provozovat
také	také	k9	také
metro	metro	k1gNnSc1	metro
<g/>
;	;	kIx,	;
lisabonské	lisabonský	k2eAgNnSc1d1	Lisabonské
je	být	k5eAaImIp3nS	být
klasické	klasický	k2eAgNnSc1d1	klasické
<g/>
,	,	kIx,	,
portské	portský	k2eAgNnSc1d1	portské
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
rychlodráhu	rychlodráha	k1gFnSc4	rychlodráha
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
také	také	k9	také
malebná	malebný	k2eAgFnSc1d1	malebná
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
a	a	k8xC	a
lanovky	lanovka	k1gFnPc1	lanovka
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
,	,	kIx,	,
Braze	Braza	k1gFnSc6	Braza
<g/>
,	,	kIx,	,
Coimbře	Coimbra	k1gFnSc6	Coimbra
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
mělo	mít	k5eAaImAgNnS	mít
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
10	[number]	k4	10
562	[number]	k4	562
178	[number]	k4	178
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc4d1	velká
potíže	potíž	k1gFnPc4	potíž
činila	činit	k5eAaImAgFnS	činit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
negramotnost	negramotnost	k1gFnSc4	negramotnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
i	i	k9	i
dnes	dnes	k6eAd1	dnes
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
čísel	číslo	k1gNnPc2	číslo
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc7d1	nízká
porodností	porodnost	k1gFnSc7	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
Valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
Portugalců	Portugalec	k1gMnPc2	Portugalec
asi	asi	k9	asi
81	[number]	k4	81
<g/>
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
římskokatolíci	římskokatolík	k1gMnPc1	římskokatolík
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
poměrně	poměrně	k6eAd1	poměrně
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
a	a	k8xC	a
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
významné	významný	k2eAgFnPc4d1	významná
katolické	katolický	k2eAgFnPc4d1	katolická
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
světoznámé	světoznámý	k2eAgNnSc4d1	světoznámé
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
Fatima	Fatimum	k1gNnSc2	Fatimum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
křesťanským	křesťanský	k2eAgFnPc3d1	křesťanská
církvím	církev	k1gFnPc3	církev
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
asi	asi	k9	asi
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ostatním	ostatní	k2eAgNnPc3d1	ostatní
náboženstvím	náboženství	k1gNnPc3	náboženství
asi	asi	k9	asi
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
hl.	hl.	k?	hl.
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
hl.	hl.	k?	hl.
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
hinduisté	hinduista	k1gMnPc1	hinduista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
3,9	[number]	k4	3,9
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
asi	asi	k9	asi
kolem	kolem	k7c2	kolem
9	[number]	k4	9
<g/>
%	%	kIx~	%
se	se	k3xPyFc4	se
k	k	k7c3	k
náboženské	náboženský	k2eAgFnSc3d1	náboženská
otázce	otázka	k1gFnSc3	otázka
nevyjádřilo	vyjádřit	k5eNaPmAgNnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
mnoho	mnoho	k6eAd1	mnoho
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
stopy	stopa	k1gFnPc4	stopa
zde	zde	k6eAd1	zde
zanechala	zanechat	k5eAaPmAgFnS	zanechat
fénická	fénický	k2eAgFnSc1d1	fénická
<g/>
,	,	kIx,	,
řecká	řecký	k2eAgFnSc1d1	řecká
<g/>
,	,	kIx,	,
keltská	keltský	k2eAgFnSc1d1	keltská
<g/>
,	,	kIx,	,
kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
i	i	k8xC	i
arabská	arabský	k2eAgFnSc1d1	arabská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
"	"	kIx"	"
<g/>
portugalský	portugalský	k2eAgInSc1d1	portugalský
architektonický	architektonický	k2eAgInSc1d1	architektonický
styl	styl	k1gInSc1	styl
<g/>
"	"	kIx"	"
pozdní	pozdní	k2eAgFnSc2d1	pozdní
gotiky	gotika	k1gFnSc2	gotika
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
námořními	námořní	k2eAgInPc7d1	námořní
objevy	objev	k1gInPc7	objev
a	a	k8xC	a
s	s	k7c7	s
bohatstvím	bohatství	k1gNnSc7	bohatství
plynoucím	plynoucí	k2eAgFnPc3d1	plynoucí
z	z	k7c2	z
dovozu	dovoz	k1gInSc2	dovoz
zámořských	zámořský	k2eAgInPc2d1	zámořský
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
manuelino	manuelin	k2eAgNnSc1d1	manuelin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nazván	nazvat	k5eAaPmNgInS	nazvat
národním	národní	k2eAgInSc7d1	národní
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
manuelské	manuelský	k2eAgFnPc1d1	manuelská
stavby	stavba	k1gFnPc1	stavba
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
<g/>
,	,	kIx,	,
na	na	k7c6	na
Azorech	azor	k1gInPc6	azor
<g/>
,	,	kIx,	,
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mosambiku	Mosambik	k1gInSc6	Mosambik
<g/>
,	,	kIx,	,
v	v	k7c6	v
Hormuzu	Hormuz	k1gInSc6	Hormuz
či	či	k8xC	či
v	v	k7c6	v
indické	indický	k2eAgFnSc6d1	indická
Goe	Goe	k1gFnSc6	Goe
<g/>
.	.	kIx.	.
</s>
<s>
Slavní	slavný	k2eAgMnPc1d1	slavný
portugalští	portugalský	k2eAgMnPc1d1	portugalský
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
díla	dílo	k1gNnPc1	dílo
byla	být	k5eAaImAgNnP	být
přeložena	přeložit	k5eAaPmNgNnP	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
Luís	Luís	k1gInSc4	Luís
Vaz	vaz	k1gInSc1	vaz
de	de	k?	de
Camõ	Camõ	k1gMnSc5	Camõ
<g/>
,	,	kIx,	,
Gil	Gil	k1gMnSc5	Gil
Vincente	Vincent	k1gMnSc5	Vincent
<g/>
,	,	kIx,	,
José	Josí	k1gMnPc4	Josí
Maria	Maria	k1gFnSc1	Maria
Eça	Eça	k1gFnSc2	Eça
de	de	k?	de
Queiros	Queirosa	k1gFnPc2	Queirosa
<g/>
,	,	kIx,	,
Almeida	Almeida	k1gFnSc1	Almeida
Garett	Garett	k1gInSc1	Garett
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Pessoa	Pesso	k2eAgNnPc1d1	Pesso
<g/>
,	,	kIx,	,
José	Josý	k2eAgNnSc1d1	José
Saramago	Saramago	k1gNnSc1	Saramago
<g/>
,	,	kIx,	,
António	António	k1gMnSc1	António
Lobo	Lobo	k1gMnSc1	Lobo
Antunes	Antunes	k1gMnSc1	Antunes
<g/>
,	,	kIx,	,
Eduardo	Eduarda	k1gMnSc5	Eduarda
Lourenço	Lourença	k1gMnSc5	Lourença
<g/>
,	,	kIx,	,
Bernardim	Bernardim	k1gInSc4	Bernardim
Ribeiro	Ribeiro	k1gNnSc4	Ribeiro
<g/>
,	,	kIx,	,
Miguel	Miguel	k1gMnSc1	Miguel
Sousa	Sous	k1gMnSc2	Sous
Tavares	Tavares	k1gInSc4	Tavares
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
také	také	k9	také
působí	působit	k5eAaImIp3nS	působit
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
František	František	k1gMnSc1	František
Listopad	listopad	k1gInSc1	listopad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Portugalsku	Portugalsko	k1gNnSc3	Portugalsko
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nS	patřit
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
hostila	hostit	k5eAaImAgFnS	hostit
Euro	euro	k1gNnSc4	euro
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
obsadila	obsadit	k5eAaPmAgFnS	obsadit
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
s	s	k7c7	s
5	[number]	k4	5
góly	gól	k1gInPc7	gól
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
šampionátu	šampionát	k1gInSc3	šampionát
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavěn	k2eAgNnSc1d1	přestavěno
či	či	k8xC	či
nově	nově	k6eAd1	nově
postaveno	postavit	k5eAaPmNgNnS	postavit
10	[number]	k4	10
moderních	moderní	k2eAgInPc2d1	moderní
stadionů	stadion	k1gInPc2	stadion
<g/>
.	.	kIx.	.
</s>
