<s>
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
česky	česky	k6eAd1
psaná	psaný	k2eAgFnSc1d1
veršovaná	veršovaný	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
ze	z	k7c2
stěžejních	stěžejní	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
českého	český	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>