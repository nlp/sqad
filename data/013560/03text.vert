<s>
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
</s>
<s>
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
,	,	kIx,
iluminace	iluminace	k1gFnSc1
z	z	k7c2
tzv.	tzv.	kA
Pařížského	pařížský	k2eAgInSc2d1
zlomku	zlomek	k1gInSc2
<g/>
,	,	kIx,
jediného	jediný	k2eAgInSc2d1
dochovaného	dochovaný	k2eAgInSc2d1
latinského	latinský	k2eAgInSc2d1
překladu	překlad	k1gInSc2
kronikyAutor	kronikyAutor	k1gMnSc1
</s>
<s>
Dalimil	Dalimil	k1gMnSc1
a	a	k8xC
anonym	anonym	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Rýmovaná	rýmovaný	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
česká	český	k2eAgFnSc1d1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
Jazyk	jazyk	k1gMnSc1
</s>
<s>
čeština	čeština	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
česky	česky	k6eAd1
psaná	psaný	k2eAgFnSc1d1
veršovaná	veršovaný	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
ze	z	k7c2
stěžejních	stěžejní	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
českého	český	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
počátku	počátek	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
záznamy	záznam	k1gInPc1
končí	končit	k5eAaImIp3nS
rokem	rok	k1gInSc7
1314	#num#	k4
<g/>
,	,	kIx,
respektive	respektive	k9
s	s	k7c7
dodatky	dodatek	k1gInPc7
(	(	kIx(
<g/>
nejspíše	nejspíše	k9
jiného	jiný	k2eAgMnSc2d1
autora	autor	k1gMnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
dovedeny	doveden	k2eAgFnPc1d1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1325	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
svého	svůj	k3xOyFgInSc2
nejdůležitějšího	důležitý	k2eAgInSc2d3
zdroje	zdroj	k1gInSc2
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
též	též	k9
jako	jako	k9
Kronika	kronika	k1gFnSc1
boleslavská	boleslavský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
je	být	k5eAaImIp3nS
anonymní	anonymní	k2eAgNnSc4d1
a	a	k8xC
označení	označení	k1gNnSc4
přívlastkem	přívlastek	k1gInSc7
„	„	k?
<g/>
Dalimilova	Dalimilův	k2eAgMnSc4d1
<g/>
“	“	k?
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
až	až	k9
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
mnoho	mnoho	k4c4
jejích	její	k3xOp3gInPc2
opisů	opis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
silně	silně	k6eAd1
vlastenecké	vlastenecký	k2eAgNnSc4d1
ladění	ladění	k1gNnSc4
byla	být	k5eAaImAgFnS
předmětem	předmět	k1gInSc7
zvýšeného	zvýšený	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
vždy	vždy	k6eAd1
v	v	k7c6
dobách	doba	k1gFnPc6
národního	národní	k2eAgInSc2d1
útisku	útisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Autor	autor	k1gMnSc1
a	a	k8xC
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
postoje	postoj	k1gInSc2
</s>
<s>
Ani	ani	k8xC
ve	v	k7c6
vlastní	vlastní	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
<g/>
,	,	kIx,
ani	ani	k8xC
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
k	k	k7c3
autorství	autorství	k1gNnSc3
tohoto	tento	k3xDgNnSc2
díla	dílo	k1gNnSc2
nikdo	nikdo	k3yNnSc1
nehlásil	hlásit	k5eNaImAgMnS
a	a	k8xC
dodnes	dodnes	k6eAd1
se	se	k3xPyFc4
zcela	zcela	k6eAd1
uspokojivě	uspokojivě	k6eAd1
nepodařilo	podařit	k5eNaPmAgNnS
zpětně	zpětně	k6eAd1
jednoznačně	jednoznačně	k6eAd1
spojit	spojit	k5eAaPmF
dílo	dílo	k1gNnSc4
s	s	k7c7
reálným	reálný	k2eAgMnSc7d1
člověkem	člověk	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozborem	rozbor	k1gInSc7
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
autor	autor	k1gMnSc1
v	v	k7c6
díle	díl	k1gInSc6
podal	podat	k5eAaPmAgMnS
<g/>
,	,	kIx,
lze	lze	k6eAd1
dovozovat	dovozovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
na	na	k7c4
svoji	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
vysoce	vysoce	k6eAd1
vzdělaný	vzdělaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
šlechtic	šlechtic	k1gMnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
duchovní	duchovní	k2eAgFnSc1d1
(	(	kIx(
<g/>
řada	řada	k1gFnSc1
údajů	údaj	k1gInPc2
svědčí	svědčit	k5eAaImIp3nS
spíše	spíše	k9
o	o	k7c6
duchovním	duchovní	k2eAgNnSc6d1
s	s	k7c7
nižším	nízký	k2eAgNnSc7d2
svěcením	svěcení	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
nejnovější	nový	k2eAgFnSc1d3
studie	studie	k1gFnSc1
Dalimila	Dalimil	k1gMnSc2
ztotožnila	ztotožnit	k5eAaPmAgFnS
přímo	přímo	k6eAd1
s	s	k7c7
knězem	kněz	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
přesvědčen	přesvědčit	k5eAaPmNgMnS
o	o	k7c6
odpovědnosti	odpovědnost	k1gFnSc6
panovníka	panovník	k1gMnSc2
za	za	k7c4
osud	osud	k1gInSc4
celé	celý	k2eAgFnSc2d1
země	zem	k1gFnSc2
a	a	k8xC
národa	národ	k1gInSc2
<g/>
,	,	kIx,
vyjadřuje	vyjadřovat	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
despektem	despekt	k1gInSc7
k	k	k7c3
měšťanům	měšťan	k1gMnPc3
a	a	k8xC
zejména	zejména	k9
Němcům	Němec	k1gMnPc3
(	(	kIx(
<g/>
cení	cenit	k5eAaImIp3nS
si	se	k3xPyFc3
těch	ten	k3xDgMnPc2
českých	český	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
stavěli	stavět	k5eAaImAgMnP
negativně	negativně	k6eAd1
německému	německý	k2eAgInSc3d1
živlu	živel	k1gInSc3
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
bádání	bádání	k1gNnSc2
po	po	k7c6
autorovi	autor	k1gMnSc6
výrazně	výrazně	k6eAd1
zasáhla	zasáhnout	k5eAaPmAgFnS
česká	český	k2eAgFnSc1d1
barokní	barokní	k2eAgFnSc1d1
historiografie	historiografie	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
s	s	k7c7
tímto	tento	k3xDgInSc7
dílem	díl	k1gInSc7
spojila	spojit	k5eAaPmAgFnS
jakéhosi	jakýsi	k3yIgMnSc4
boleslavského	boleslavský	k2eAgMnSc4d1
kanovníka	kanovník	k1gMnSc4
Dalimila	Dalimil	k1gMnSc4
Meziříčského	meziříčský	k2eAgInSc2d1
uváděného	uváděný	k2eAgInSc2d1
v	v	k7c6
Hájkově	Hájkův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
jako	jako	k8xS,k8xC
pramen	pramen	k1gInSc4
(	(	kIx(
<g/>
nejprve	nejprve	k6eAd1
Tomáš	Tomáš	k1gMnSc1
Pešina	Pešina	k1gMnSc1
z	z	k7c2
Čechorodu	Čechorod	k1gInSc2
<g/>
,	,	kIx,
pozdě	pozdě	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
souhlasil	souhlasit	k5eAaImAgMnS
i	i	k9
Bohuslav	Bohuslav	k1gMnSc1
Balbín	Balbín	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
dnes	dnes	k6eAd1
panují	panovat	k5eAaImIp3nP
pochybnosti	pochybnost	k1gFnPc4
<g/>
,	,	kIx,
zdali	zdali	k8xS
někdo	někdo	k3yInSc1
takový	takový	k3xDgInSc1
vůbec	vůbec	k9
existoval	existovat	k5eAaImAgInS
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
toto	tento	k3xDgNnSc1
jméno	jméno	k1gNnSc1
v	v	k7c6
názvu	název	k1gInSc6
jako	jako	k8xC,k8xS
obecně	obecně	k6eAd1
známé	známý	k2eAgInPc1d1
a	a	k8xC
užívané	užívaný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrzení	tvrzení	k1gNnSc1
o	o	k7c6
Dalimilovi	Dalimil	k1gMnSc6
přejal	přejmout	k5eAaPmAgMnS
Gelasius	Gelasius	k1gMnSc1
Dobner	Dobner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
první	první	k4xOgNnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
Dalimilovo	Dalimilův	k2eAgNnSc4d1
autorství	autorství	k1gNnPc4
zpochybnil	zpochybnit	k5eAaPmAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
František	František	k1gMnSc1
Faustin	Faustin	k1gMnSc1
Procházka	Procházka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Jireček	Jireček	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
nějaký	nějaký	k3yIgMnSc1
johanitský	johanitský	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
(	(	kIx(
<g/>
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
autor	autor	k1gMnSc1
prohlašuje	prohlašovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
boje	boj	k1gInPc1
o	o	k7c4
království	království	k1gNnSc4
v	v	k7c6
letech	let	k1gInPc6
1309	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
sledoval	sledovat	k5eAaImAgMnS
z	z	k7c2
Malé	Malé	k2eAgFnSc2d1
Strany	strana	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
existovala	existovat	k5eAaImAgFnS
johanitská	johanitský	k2eAgFnSc1d1
komenda	komenda	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
že	že	k8xS
pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
rodu	rod	k1gInSc2
Ronovců	Ronovec	k1gMnPc2
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Vítězslav	Vítězslav	k1gMnSc1
Šimák	Šimák	k1gMnSc1
ztotožnil	ztotožnit	k5eAaPmAgMnS
autora	autor	k1gMnSc4
s	s	k7c7
Hynkem	Hynek	k1gMnSc7
Žákem	Žák	k1gMnSc7
z	z	k7c2
Dubé	Dubé	k1gNnSc2
<g/>
,	,	kIx,
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Beran	Beran	k1gMnSc1
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jím	jíst	k5eAaImIp1nS
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
Havel	Havel	k1gMnSc1
z	z	k7c2
Lemberka	Lemberka	k1gFnSc1
<g/>
,	,	kIx,
vnuk	vnuk	k1gMnSc1
sv.	sv.	kA
Zdislavy	Zdislava	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Michálek	Michálek	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
johanitu	johanita	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gNnSc4
poslední	poslední	k2eAgNnSc4d1
místo	místo	k1gNnSc4
pobytu	pobyt	k1gInSc2
přesunul	přesunout	k5eAaPmAgMnS
do	do	k7c2
Mladé	mladá	k1gFnSc2
Boleslavi	Boleslaev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
M.	M.	kA
Očadlík	Očadlík	k1gInSc1
identifikoval	identifikovat	k5eAaBmAgMnS
Dalimila	Dalimil	k1gMnSc4
jako	jako	k8xC,k8xS
Jana	Jan	k1gMnSc4
IV	IV	kA
<g/>
.	.	kIx.
z	z	k7c2
Dražic	Dražice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdeněk	Zdeňka	k1gFnPc2
Kristen	Kristen	k2eAgMnSc1d1
vyvrátil	vyvrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
kněze	kněz	k1gMnPc4
<g/>
,	,	kIx,
na	na	k7c4
něho	on	k3xPp3gMnSc4
navázal	navázat	k5eAaPmAgMnS
Zdeněk	Zdeněk	k1gMnSc1
Fiala	Fiala	k1gMnSc1
a	a	k8xC
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
buď	buď	k8xC
vzdělaný	vzdělaný	k2eAgMnSc1d1
laický	laický	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
ještě	ještě	k9
spíše	spíše	k9
duchovní	duchovní	k1gMnSc1
nižšího	nízký	k2eAgNnSc2d2
svěcení	svěcení	k1gNnSc2
v	v	k7c6
šlechtických	šlechtický	k2eAgFnPc6d1
službách	služba	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zatím	zatím	k6eAd1
poslední	poslední	k2eAgFnSc4d1
hypotézu	hypotéza	k1gFnSc4
vyslovil	vyslovit	k5eAaPmAgMnS
Tomáš	Tomáš	k1gMnSc1
Edel	Edel	k1gMnSc1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
autorem	autor	k1gMnSc7
kroniky	kronika	k1gFnSc2
je	být	k5eAaImIp3nS
johanitský	johanitský	k2eAgMnSc1d1
komtur	komtur	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
z	z	k7c2
Varnsdorfu	Varnsdorf	k1gInSc2
v	v	k7c6
žitavské	žitavský	k2eAgFnSc6d1
komendě	komenda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ovšem	ovšem	k9
i	i	k9
tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
napadnutelných	napadnutelný	k2eAgNnPc2d1
tvrzení	tvrzení	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hned	hned	k6eAd1
v	v	k7c6
úvodu	úvod	k1gInSc6
si	se	k3xPyFc3
klade	klást	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
zpracovávat	zpracovávat	k5eAaImF
domácí	domácí	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
a	a	k8xC
omezit	omezit	k5eAaPmF
tak	tak	k6eAd1
tehdy	tehdy	k6eAd1
oblíbená	oblíbený	k2eAgNnPc4d1
vyprávění	vyprávění	k1gNnPc4
cizích	cizí	k2eAgInPc2d1
rytířských	rytířský	k2eAgInPc2d1
osudů	osud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mnoha	mnoho	k4c2
místech	místo	k1gNnPc6
vybízí	vybízet	k5eAaImIp3nS
k	k	k7c3
odporu	odpor	k1gInSc3
k	k	k7c3
cizím	cizí	k2eAgInPc3d1
škodlivým	škodlivý	k2eAgInPc3d1
vlivům	vliv	k1gInPc3
a	a	k8xC
módám	móda	k1gFnPc3
(	(	kIx(
<g/>
turnajům	turnaj	k1gInPc3
<g/>
,	,	kIx,
dvorským	dvorský	k2eAgFnPc3d1
slavnostem	slavnost	k1gFnPc3
<g/>
,	,	kIx,
odívání	odívání	k1gNnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zamítá	zamítat	k5eAaImIp3nS
sňatky	sňatek	k1gInPc4
českých	český	k2eAgMnPc2d1
feudálů	feudál	k1gMnPc2
s	s	k7c7
cizinkami	cizinka	k1gFnPc7
(	(	kIx(
<g/>
jmenovitě	jmenovitě	k6eAd1
Němkami	Němka	k1gFnPc7
<g/>
)	)	kIx)
a	a	k8xC
výchovu	výchova	k1gFnSc4
jejich	jejich	k3xOp3gFnPc2
dětí	dítě	k1gFnPc2
podle	podle	k7c2
cizích	cizí	k2eAgInPc2d1
vzorů	vzor	k1gInPc2
nebo	nebo	k8xC
v	v	k7c6
cizině	cizina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
je	být	k5eAaImIp3nS
kronika	kronika	k1gFnSc1
předkládána	předkládán	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
důkaz	důkaz	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
dějiny	dějiny	k1gFnPc1
nacionalismu	nacionalismus	k1gInSc2
nepočínají	počínat	k5eNaImIp3nP
až	až	k9
18	#num#	k4
<g/>
.	.	kIx.
stoletím	století	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
a	a	k8xC
obsah	obsah	k1gInSc1
</s>
<s>
V	v	k7c6
úvodu	úvod	k1gInSc6
autor	autor	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
ze	z	k7c2
všech	všecek	k3xTgInPc2
pramenů	pramen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
používal	používat	k5eAaImAgInS
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
pět	pět	k4xCc1
kronik	kronika	k1gFnPc2
<g/>
:	:	kIx,
kroniku	kronika	k1gFnSc4
boleslavskou	boleslavský	k2eAgFnSc4d1
<g/>
,	,	kIx,
pražskou	pražský	k2eAgFnSc4d1
<g/>
,	,	kIx,
břevnovskou	břevnovský	k2eAgFnSc4d1
<g/>
,	,	kIx,
opatovickou	opatovický	k2eAgFnSc4d1
a	a	k8xC
vyšehradskou	vyšehradský	k2eAgFnSc4d1
(	(	kIx(
<g/>
k	k	k7c3
tomu	ten	k3xDgNnSc3
v	v	k7c6
textu	text	k1gInSc6
ještě	ještě	k6eAd1
přidává	přidávat	k5eAaImIp3nS
kroniku	kronika	k1gFnSc4
moravskou	moravský	k2eAgFnSc7d1
a	a	k8xC
německou	německý	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
učiněna	učiněn	k2eAgFnSc1d1
řada	řada	k1gFnSc1
pokusů	pokus	k1gInPc2
směřujících	směřující	k2eAgFnPc2d1
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
těchto	tento	k3xDgInPc2
pramenů	pramen	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
všechny	všechen	k3xTgFnPc1
jsou	být	k5eAaImIp3nP
zcela	zcela	k6eAd1
jednoznačné	jednoznačný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
pražská	pražský	k2eAgFnSc1d1
a	a	k8xC
břevnovská	břevnovský	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
dva	dva	k4xCgInPc1
rukopisy	rukopis	k1gInPc1
Kosmovy	Kosmův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
označení	označení	k1gNnSc1
dvou	dva	k4xCgFnPc2
knih	kniha	k1gFnPc2
této	tento	k3xDgFnSc2
kroniky	kronika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opatovická	opatovický	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
jsou	být	k5eAaImIp3nP
nejspíš	nejspíš	k9
tzv.	tzv.	kA
Anály	anály	k1gInPc4
hradištsko-opatovické	hradištsko-opatovický	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vyšehradské	vyšehradský	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
určitě	určitě	k6eAd1
to	ten	k3xDgNnSc4
není	být	k5eNaImIp3nS
tzv.	tzv.	kA
Kanovník	kanovník	k1gMnSc1
vyšehradský	vyšehradský	k2eAgMnSc1d1
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yRgMnSc4,k3yIgMnSc4
autor	autor	k1gMnSc1
přímo	přímo	k6eAd1
neznal	neznat	k5eAaImAgMnS,k5eNaImAgMnS
<g/>
)	)	kIx)
existuje	existovat	k5eAaImIp3nS
také	také	k9
domněnka	domněnka	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
to	ten	k3xDgNnSc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
anály	anály	k1gInPc4
vedené	vedený	k2eAgNnSc1d1
u	u	k7c2
vyšehradského	vyšehradský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Tomáš	Tomáš	k1gMnSc1
Edel	Edel	k1gMnSc1
upozornil	upozornit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vzhledem	vzhledem	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
by	by	kYmCp3nS
to	ten	k3xDgNnSc4
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
také	také	k9
Jarlochova	Jarlochův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
,	,	kIx,
čemuž	což	k3yQnSc3,k3yRnSc3
by	by	kYmCp3nS
napovídal	napovídat	k5eAaBmAgInS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
tzv.	tzv.	kA
Dalimil	Dalimil	k1gMnSc1
si	se	k3xPyFc3
této	tento	k3xDgFnSc2
kroniky	kronika	k1gFnSc2
nejméně	málo	k6eAd3
vážil	vážit	k5eAaImAgInS
(	(	kIx(
<g/>
Jarloch	Jarloch	k1gInSc1
zastával	zastávat	k5eAaImAgInS
proněmecké	proněmecký	k2eAgInPc4d1
postoje	postoj	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Prvně	Prvně	k?
uvedená	uvedený	k2eAgFnSc1d1
boleslavská	boleslavský	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
se	se	k3xPyFc4
Dalimilovi	Dalimilův	k2eAgMnPc1d1
nejvíce	nejvíce	k6eAd1,k6eAd3
líbila	líbit	k5eAaImAgFnS
a	a	k8xC
také	také	k9
z	z	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
ní	on	k3xPp3gFnSc7
nejvíce	nejvíce	k6eAd1,k6eAd3
vycházel	vycházet	k5eAaImAgMnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
již	již	k6eAd1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
ke	k	k7c3
konstatování	konstatování	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
muselo	muset	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
Kosmovu	Kosmův	k2eAgFnSc4d1
kroniku	kronika	k1gFnSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
názory	názor	k1gInPc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
dnes	dnes	k6eAd1
neznámý	známý	k2eNgInSc4d1
opis	opis	k1gInSc4
Kosmovy	Kosmův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
s	s	k7c7
dodatky	dodatek	k1gInPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Edel	Edel	k1gMnSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
spíše	spíše	k9
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
falešnou	falešný	k2eAgFnSc4d1
autoritu	autorita	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
Dalimil	Dalimil	k1gMnSc1
snažil	snažit	k5eAaImAgMnS
podpořit	podpořit	k5eAaPmF
kontroverzní	kontroverzní	k2eAgInPc4d1
výroky	výrok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Moravská	moravský	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
nebyla	být	k5eNaImAgFnS
identifikována	identifikován	k2eAgFnSc1d1
a	a	k8xC
německá	německý	k2eAgFnSc1d1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
rytířská	rytířský	k2eAgFnSc1d1
báseň	báseň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tzv.	tzv.	kA
Dalimil	Dalimil	k1gMnSc1
nepochybně	pochybně	k6eNd1
vycházel	vycházet	k5eAaImAgMnS
také	také	k9
z	z	k7c2
pramenů	pramen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
nikde	nikde	k6eAd1
neuvádí	uvádět	k5eNaImIp3nP
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
době	doba	k1gFnSc6
běžné	běžný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
zmíněných	zmíněný	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
nepochybně	pochybně	k6eNd1
ústní	ústní	k2eAgNnSc1d1
podání	podání	k1gNnSc1
kmenových	kmenový	k2eAgFnPc2d1
<g/>
,	,	kIx,
místních	místní	k2eAgFnPc2d1
a	a	k8xC
erbovních	erbovní	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
popis	popis	k1gInSc4
života	život	k1gInSc2
sv.	sv.	kA
Václava	Václav	k1gMnSc4
znal	znát	k5eAaImAgMnS
legendu	legenda	k1gFnSc4
Oriente	Orient	k1gInSc5
iam	iam	k?
sole	sol	k1gInSc5
I	i	k9
<g/>
,	,	kIx,
Edel	Edel	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
znal	znát	k5eAaImAgInS
Voraginovu	Voraginův	k2eAgFnSc4d1
Legendu	legenda	k1gFnSc4
aureu	aureus	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
nedochovala	dochovat	k5eNaPmAgFnS
a	a	k8xC
obsahovala	obsahovat	k5eAaImAgFnS
články	článek	k1gInPc4
o	o	k7c6
třech	tři	k4xCgFnPc6
českých	český	k2eAgFnPc6d1
světcích	světec	k1gMnPc6
<g/>
,	,	kIx,
o	o	k7c6
Václavovi	Václav	k1gMnSc6
<g/>
,	,	kIx,
Ludmile	Ludmila	k1gFnSc6
a	a	k8xC
Vojtěchovi	Vojtěchův	k2eAgMnPc1d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
možná	možná	k9
znal	znát	k5eAaImAgInS
kroniku	kronika	k1gFnSc4
Jindřicha	Jindřich	k1gMnSc2
Heimburského	Heimburský	k2eAgMnSc2d1
a	a	k8xC
Oty	Ota	k1gMnSc2
Durynského	durynský	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kronika	kronika	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
106	#num#	k4
kapitol	kapitola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
líčí	líčit	k5eAaImIp3nS
stavbu	stavba	k1gFnSc4
babylónské	babylónský	k2eAgFnSc2d1
věže	věž	k1gFnSc2
<g/>
,	,	kIx,
následující	následující	k2eAgInSc1d1
boží	boží	k2eAgInSc1d1
trest	trest	k1gInSc1
<g/>
,	,	kIx,
zmatení	zmatení	k1gNnSc1
jazyků	jazyk	k1gInPc2
a	a	k8xC
rozchod	rozchod	k1gInSc1
lidí	člověk	k1gMnPc2
do	do	k7c2
různých	různý	k2eAgFnPc2d1
částí	část	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračuje	pokračovat	k5eAaImIp3nS
chronologicky	chronologicky	k6eAd1
řazenými	řazený	k2eAgInPc7d1
příběhy	příběh	k1gInPc7
z	z	k7c2
historie	historie	k1gFnSc2
Čech	Čechy	k1gFnPc2
počínaje	počínaje	k7c7
osídlením	osídlení	k1gNnSc7
našeho	náš	k3xOp1gNnSc2
území	území	k1gNnSc2
praotcem	praotec	k1gMnSc7
Čechem	Čech	k1gMnSc7
<g/>
,	,	kIx,
pověstmi	pověst	k1gFnPc7
o	o	k7c6
Libušině	Libušin	k2eAgNnSc6d1
proroctví	proroctví	k1gNnSc6
<g/>
,	,	kIx,
o	o	k7c6
Přemyslu	Přemysl	k1gMnSc6
Oráčovi	oráč	k1gMnSc6
a	a	k8xC
o	o	k7c6
Dívčí	dívčí	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Končí	končit	k5eAaImIp3nS
v	v	k7c6
období	období	k1gNnSc6
vlády	vláda	k1gFnSc2
Jana	Jan	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
svého	svůj	k3xOyFgNnSc2
dopsání	dopsání	k1gNnSc2
(	(	kIx(
<g/>
1314	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
ale	ale	k9
i	i	k9
několik	několik	k4yIc4
pozdějších	pozdní	k2eAgInPc2d2
dodatků	dodatek	k1gInPc2
(	(	kIx(
<g/>
zejména	zejména	k9
z	z	k7c2
let	léto	k1gNnPc2
1315	#num#	k4
a	a	k8xC
1316	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
však	však	k9
patrně	patrně	k6eAd1
nenapsal	napsat	k5eNaPmAgMnS
původní	původní	k2eAgMnSc1d1
autor	autor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Styl	styl	k1gInSc1
a	a	k8xC
jazyk	jazyk	k1gInSc1
</s>
<s>
Zpracování	zpracování	k1gNnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
je	být	k5eAaImIp3nS
soustředěné	soustředěný	k2eAgNnSc1d1
<g/>
,	,	kIx,
charakteristika	charakteristika	k1gFnSc1
postav	postava	k1gFnPc2
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
jejich	jejich	k3xOp3gNnSc2
jednání	jednání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvahy	úvaha	k1gFnPc1
<g/>
,	,	kIx,
rady	rada	k1gFnPc1
a	a	k8xC
kritiky	kritika	k1gFnPc1
autorovy	autorův	k2eAgFnPc1d1
nejsou	být	k5eNaImIp3nP
samoúčelné	samoúčelný	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
vyplývají	vyplývat	k5eAaImIp3nP
a	a	k8xC
prolínají	prolínat	k5eAaImIp3nP
se	se	k3xPyFc4
s	s	k7c7
vyprávěným	vyprávěný	k2eAgInSc7d1
dějem	děj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvůrce	tvůrce	k1gMnSc1
stále	stále	k6eAd1
myslí	myslet	k5eAaImIp3nS
na	na	k7c4
čtenáře	čtenář	k1gMnPc4
<g/>
,	,	kIx,
promlouvá	promlouvat	k5eAaImIp3nS
prostě	prostě	k6eAd1
<g/>
,	,	kIx,
volí	volit	k5eAaImIp3nP
přiléhavá	přiléhavý	k2eAgNnPc1d1
přirovnání	přirovnání	k1gNnPc1
<g/>
,	,	kIx,
nevyhýbá	vyhýbat	k5eNaImIp3nS
se	se	k3xPyFc4
lidovým	lidový	k2eAgInSc7d1
výrazům	výraz	k1gInPc3
a	a	k8xC
rčením	rčení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
bezrozměrového	bezrozměrový	k2eAgInSc2d1
verše	verš	k1gInSc2
o	o	k7c6
proměnlivém	proměnlivý	k2eAgInSc6d1
počtu	počet	k1gInSc6
slabik	slabika	k1gFnPc2
a	a	k8xC
blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
hovorové	hovorový	k2eAgFnSc3d1
řeči	řeč	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgFnPc4
vlastnosti	vlastnost	k1gFnPc4
se	se	k3xPyFc4
první	první	k4xOgFnSc1
česky	česky	k6eAd1
psaná	psaný	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
stala	stát	k5eAaPmAgFnS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
dobách	doba	k1gFnPc6
velmi	velmi	k6eAd1
populární	populární	k2eAgFnPc1d1
a	a	k8xC
její	její	k3xOp3gInSc1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
trvale	trvale	k6eAd1
aktuální	aktuální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vypravování	vypravování	k1gNnSc1
o	o	k7c6
Oldřichovi	Oldřich	k1gMnSc6
a	a	k8xC
Boženě	Božena	k1gFnSc6
</s>
<s>
Ukázka	ukázka	k1gFnSc1
(	(	kIx(
<g/>
přebásněno	přebásněn	k2eAgNnSc1d1
do	do	k7c2
moderní	moderní	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
41	#num#	k4
<g/>
.	.	kIx.
kapitola	kapitola	k1gFnSc1
<g/>
:	:	kIx,
O	o	k7c6
selské	selský	k2eAgFnSc6d1
kněžně	kněžna	k1gFnSc6
Boženě	Božena	k1gFnSc3
</s>
<s>
Když	když	k8xS
v	v	k7c6
postoloprtské	postoloprtský	k2eAgFnSc6d1
stráni	stráň	k1gFnSc6
</s>
<s>
lovil	lovit	k5eAaImAgMnS
Oldřich	Oldřich	k1gMnSc1
<g/>
,	,	kIx,
znenadání	znenadání	k6eAd1
</s>
<s>
za	za	k7c7
jednou	jeden	k4xCgFnSc7
vsí	ves	k1gFnSc7
nad	nad	k7c7
říčkou	říčka	k1gFnSc7
</s>
<s>
spatřil	spatřit	k5eAaPmAgMnS
selku	selka	k1gFnSc4
mladičkou	mladičký	k2eAgFnSc4d1
<g/>
,	,	kIx,
</s>
<s>
jak	jak	k8xS,k8xC
v	v	k7c6
košili	košile	k1gFnSc6
bez	bez	k7c2
oplecka	oplecko	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
bosa	bos	k2eAgFnSc1d1
<g/>
,	,	kIx,
umáčená	umáčený	k2eAgFnSc1d1
všecka	všecek	k3xTgNnPc1
<g/>
,	,	kIx,
</s>
<s>
pere	prát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
krásná	krásný	k2eAgFnSc1d1
<g/>
,	,	kIx,
milá	milý	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
Oldřichu	Oldřich	k1gMnSc3
se	se	k3xPyFc4
zalíbila	zalíbit	k5eAaPmAgNnP
<g/>
,	,	kIx,
</s>
<s>
že	že	k8xS
tvář	tvář	k1gFnSc1
měla	mít	k5eAaImAgFnS
čistou	čistá	k1gFnSc4
<g/>
,	,	kIx,
něžnou	něžný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
A	a	k9
tak	tak	k6eAd1
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
kněžnou	kněžna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
kněžna	kněžna	k1gFnSc1
dokonalá	dokonalý	k2eAgFnSc1d1
</s>
<s>
Božena	Božena	k1gFnSc1
se	se	k3xPyFc4
jmenovala	jmenovat	k5eAaBmAgFnS,k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
nedbal	nedbat	k5eAaImAgMnS,k5eNaImAgMnS
nic	nic	k3yNnSc4
<g/>
,	,	kIx,
že	že	k8xS
páni	pan	k1gMnPc1
</s>
<s>
to	ten	k3xDgNnSc4
manželství	manželství	k1gNnSc4
jemu	on	k3xPp3gMnSc3
haní	hanit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Řek	Řek	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Tak	tak	k9
už	už	k6eAd1
to	ten	k3xDgNnSc1
v	v	k7c6
světě	svět	k1gInSc6
chodí	chodit	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
chlapa	chlap	k1gMnSc2
šlechtic	šlechtic	k1gMnSc1
rodí	rodit	k5eAaImIp3nS
</s>
<s>
a	a	k8xC
že	že	k8xS
sedlák	sedlák	k1gMnSc1
nezřídce	nezřídka	k1gFnSc3
</s>
<s>
za	za	k7c2
otce	otec	k1gMnSc2
má	mít	k5eAaImIp3nS
šlechtice	šlechtic	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Stříbro	stříbro	k1gNnSc1
poctu	pocta	k1gFnSc4
rodu	rod	k1gInSc2
vrací	vracet	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
chudý	chudý	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
jméno	jméno	k1gNnSc4
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pán	pán	k1gMnSc1
i	i	k8xC
kmán	kmán	k?
jsou	být	k5eAaImIp3nP
jedna	jeden	k4xCgFnSc1
krev	krev	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
proto	proto	k8xC
potlačte	potlačit	k5eAaPmRp2nP
svůj	svůj	k3xOyFgInSc4
hněv	hněv	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
že	že	k8xS
jsem	být	k5eAaImIp1nS
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
za	za	k7c4
ženu	žena	k1gFnSc4
</s>
<s>
prostou	prostý	k2eAgFnSc4d1
dívku	dívka	k1gFnSc4
Boženu	Božena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
mně	já	k3xPp1nSc6
každý	každý	k3xTgMnSc1
pána	pán	k1gMnSc4
hledá	hledat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
Přemysla	Přemysl	k1gMnSc4
na	na	k7c4
praděda	praděd	k1gMnSc4
</s>
<s>
nejdříve	dříve	k6eAd3
však	však	k9
pomyslete	pomyslet	k5eAaPmRp2nP
<g/>
,	,	kIx,
</s>
<s>
jenž	jenž	k3xRgMnSc1
z	z	k7c2
oráče	oráč	k1gMnSc2
na	na	k7c4
knížete	kníže	k1gMnSc2
</s>
<s>
povýšen	povýšen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
stejně	stejně	k6eAd1
</s>
<s>
počati	počat	k2eAgMnPc1d1
jsme	být	k5eAaImIp1nP
byli	být	k5eAaImAgMnP
v	v	k7c6
ženě	žena	k1gFnSc6
<g/>
,	,	kIx,
</s>
<s>
ač	ač	k8xS
pokládán	pokládat	k5eAaImNgInS
za	za	k7c4
šlechtice	šlechtic	k1gMnPc4
</s>
<s>
je	být	k5eAaImIp3nS
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
stříbra	stříbro	k1gNnPc1
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Chci	chtít	k5eAaImIp1nS
žít	žít	k5eAaImF
s	s	k7c7
českou	český	k2eAgFnSc7d1
selkou	selka	k1gFnSc7
skrovnou	skrovný	k2eAgFnSc7d1
</s>
<s>
spíše	spíše	k9
než	než	k8xS
s	s	k7c7
Němkou	Němka	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
císařovnou	císařovna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pouto	pouto	k1gNnSc1
krve	krev	k1gFnSc2
vždy	vždy	k6eAd1
je	být	k5eAaImIp3nS
silné	silný	k2eAgNnSc1d1
<g/>
,	,	kIx,
</s>
<s>
Němka	Němka	k1gFnSc1
k	k	k7c3
Čechům	Čech	k1gMnPc3
sotva	sotva	k6eAd1
přilne	přilnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Vaše	váš	k3xOp2gFnPc1
utrhačné	utrhačný	k2eAgFnPc1d1
řeči	řeč	k1gFnPc1
</s>
<s>
o	o	k7c6
hlouposti	hloupost	k1gFnSc6
jenom	jenom	k9
svědčí	svědčit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
u	u	k7c2
Němky	Němka	k1gFnSc2
byste	by	kYmCp2nP
<g/>
,	,	kIx,
páni	pan	k1gMnPc1
<g/>
,	,	kIx,
</s>
<s>
chtěli	chtít	k5eAaImAgMnP
prosit	prosit	k5eAaImF
o	o	k7c6
zastání	zastání	k1gNnSc6
<g/>
?	?	kIx.
</s>
<s>
Čeleď	čeleď	k1gFnSc1
z	z	k7c2
Němec	Němec	k1gMnSc1
<g/>
,	,	kIx,
z	z	k7c2
Němec	Němec	k1gMnSc1
mistři	mistr	k1gMnPc1
<g/>
,	,	kIx,
</s>
<s>
Němkyně	Němkyně	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
koni	kůň	k1gMnSc6
</s>
<s>
a	a	k8xC
mé	můj	k3xOp1gFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
synci	synek	k1gMnPc1
bystří	bystřet	k5eAaImIp3nP
<g/>
,	,	kIx,
</s>
<s>
německy	německy	k6eAd1
jen	jen	k9
švadroní	švadronit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Vidím	vidět	k5eAaImIp1nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zem	zem	k1gFnSc1
vadne	vadnout	k5eAaImIp3nS
<g/>
,	,	kIx,
chřadne	chřadnout	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
jak	jak	k8xC,k8xS
Čech	Čech	k1gMnSc1
právo	právo	k1gNnSc4
nemá	mít	k5eNaImIp3nS
řádné	řádný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Božena	Božena	k1gFnSc1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
přála	přát	k5eAaImAgFnS
právu	právo	k1gNnSc3
<g/>
,	,	kIx,
</s>
<s>
ctila	ctít	k5eAaImAgFnS
lidi	člověk	k1gMnPc4
všeho	všecek	k3xTgInSc2
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Oldřichu	Oldřich	k1gMnSc3
pak	pak	k6eAd1
selka	selka	k1gFnSc1
zdravá	zdravá	k1gFnSc1
</s>
<s>
dala	dát	k5eAaPmAgFnS
syna	syn	k1gMnSc4
Břetislava	Břetislav	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Dochování	dochování	k1gNnSc1
</s>
<s>
České	český	k2eAgInPc1d1
veršované	veršovaný	k2eAgInPc1d1
rukopisy	rukopis	k1gInPc1
</s>
<s>
Protože	protože	k8xS
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
oblíbeným	oblíbený	k2eAgNnSc7d1
čtením	čtení	k1gNnSc7
<g/>
,	,	kIx,
zachovala	zachovat	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
řadě	řada	k1gFnSc6
opisů	opis	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
pouze	pouze	k6eAd1
ve	v	k7c6
zlomcích	zlomek	k1gInPc6
<g/>
,	,	kIx,
od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
již	již	k9
v	v	k7c6
úplnosti	úplnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
souvisí	souviset	k5eAaImIp3nS
existence	existence	k1gFnSc1
několika	několik	k4yIc2
recenzí	recenze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Hanušovy	Hanušův	k2eAgInPc4d1
zlomky	zlomek	k1gInPc4
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
dva	dva	k4xCgInPc1
pergamenové	pergamenový	k2eAgInPc1d1
dvojlisty	dvojlist	k1gInPc1
obsahující	obsahující	k2eAgFnSc2d1
kapitoly	kapitola	k1gFnSc2
30	#num#	k4
<g/>
–	–	k?
<g/>
32	#num#	k4
a	a	k8xC
39	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezeny	nalezen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1861	#num#	k4
Konstantinem	Konstantin	k1gMnSc7
Höflerem	Höfler	k1gMnSc7
a	a	k8xC
popsány	popsat	k5eAaPmNgInP
I.	I.	kA
J.	J.	kA
Hanušem	Hanuš	k1gMnSc7
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
chovány	chovat	k5eAaImNgFnP
v	v	k7c6
rukopisném	rukopisný	k2eAgNnSc6d1
oddělení	oddělení	k1gNnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Hradecké	Hradecké	k2eAgInPc4d1
zlomky	zlomek	k1gInPc4
ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
dvou	dva	k4xCgInPc6
pergamenových	pergamenový	k2eAgInPc6d1
dvojlistech	dvojlist	k1gInPc6
s	s	k7c7
částmi	část	k1gFnPc7
kapitol	kapitola	k1gFnPc2
3	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chovány	chován	k2eAgFnPc4d1
v	v	k7c6
knihovně	knihovna	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
poslední	poslední	k2eAgInSc4d1
rukopis	rukopis	k1gInSc4
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
je	být	k5eAaImIp3nS
dochován	dochovat	k5eAaPmNgInS
na	na	k7c6
Křižovnických	Křižovnický	k2eAgFnPc6d1
(	(	kIx(
<g/>
Národní	národní	k2eAgFnSc1d1
archiv	archiv	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Klementinských	klementinský	k2eAgFnPc6d1
(	(	kIx(
<g/>
Národní	národní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
)	)	kIx)
zlomcích	zlomek	k1gInPc6
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
kapitoly	kapitola	k1gFnSc2
67	#num#	k4
<g/>
,	,	kIx,
68	#num#	k4
<g/>
,	,	kIx,
69	#num#	k4
<g/>
,	,	kIx,
75	#num#	k4
<g/>
,	,	kIx,
83	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
<g/>
,	,	kIx,
94	#num#	k4
</s>
<s>
Cambridgeský	cambridgeský	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
je	být	k5eAaImIp3nS
první	první	k4xOgInSc1
dochovaný	dochovaný	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
na	na	k7c6
pergamenu	pergamen	k1gInSc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
není	být	k5eNaImIp3nS
kompletní	kompletní	k2eAgFnSc1d1
a	a	k8xC
zcela	zcela	k6eAd1
končí	končit	k5eAaImIp3nS
v	v	k7c6
kapitole	kapitola	k1gFnSc6
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
chován	chovat	k5eAaImNgInS
v	v	k7c6
knihovně	knihovna	k1gFnSc6
Trinity	Trinita	k1gFnSc2
College	Colleg	k1gFnSc2
v	v	k7c6
Cambridge	Cambridge	k1gFnSc1
</s>
<s>
Františkánský	františkánský	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
je	být	k5eAaImIp3nS
papírový	papírový	k2eAgInSc4d1
rukopis	rukopis	k1gInSc4
asi	asi	k9
z	z	k7c2
roku	rok	k1gInSc2
1440	#num#	k4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
</s>
<s>
Vídeňský	vídeňský	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
konce	konec	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
výrazné	výrazný	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
a	a	k8xC
doplňky	doplněk	k1gInPc4
<g/>
,	,	kIx,
chován	chovat	k5eAaImNgInS
v	v	k7c6
Rakouské	rakouský	k2eAgFnSc6d1
národní	národní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
</s>
<s>
Strahovský	strahovský	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
Vídeňskému	vídeňský	k2eAgInSc3d1
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
asi	asi	k9
z	z	k7c2
30	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
dochován	dochován	k2eAgInSc1d1
fragmentárně	fragmentárně	k6eAd1
<g/>
,	,	kIx,
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
knihovně	knihovna	k1gFnSc6
Památníku	památník	k1gInSc2
národního	národní	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
na	na	k7c6
Strahově	Strahov	k1gInSc6
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
zlomek	zlomek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
konec	konec	k1gInSc4
10	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
začátek	začátek	k1gInSc1
11	#num#	k4
<g/>
.	.	kIx.
kapitoly	kapitola	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
knihovně	knihovna	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
</s>
<s>
Lobkovický	lobkovický	k2eAgInSc4d1
rukopis	rukopis	k1gInSc4
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
obsahuje	obsahovat	k5eAaImIp3nS
další	další	k2eAgInPc4d1
přídavky	přídavek	k1gInPc4
oproti	oproti	k7c3
Vídeňskému	vídeňský	k2eAgInSc3d1
rukopisu	rukopis	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
zlomky	zlomek	k1gInPc4
v	v	k7c6
Mnichovské	mnichovský	k2eAgFnSc6d1
univerzitní	univerzitní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
konce	konec	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
podobné	podobný	k2eAgFnPc4d1
Lobkovickému	lobkovický	k2eAgMnSc3d1
a	a	k8xC
Vídeňskému	vídeňský	k2eAgInSc3d1
rukopisu	rukopis	k1gInSc3
</s>
<s>
Cerronský	Cerronský	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
představuje	představovat	k5eAaImIp3nS
rukopis	rukopis	k1gInSc4
nejširšího	široký	k2eAgInSc2d3
obsahu	obsah	k1gInSc2
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1443	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
Moravském	moravský	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
archivu	archiv	k1gInSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Pelclův	Pelclův	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
z	z	k7c2
konce	konec	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Fürstenberský	Fürstenberský	k2eAgInSc4d1
rukopis	rukopis	k1gInSc4
také	také	k9
z	z	k7c2
konce	konec	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
knihovně	knihovna	k1gFnSc6
na	na	k7c6
Křivoklátě	Křivoklát	k1gInSc6
</s>
<s>
Zebererův	Zebererův	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
text	text	k1gInSc4
Dalimilovy	Dalimilův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Pivnička	pivnička	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1459	#num#	k4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
chován	chovat	k5eAaImNgInS
v	v	k7c6
knihovně	knihovna	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Překlady	překlad	k1gInPc4
</s>
<s>
Je	být	k5eAaImIp3nS
znám	znám	k2eAgInSc1d1
veršovaný	veršovaný	k2eAgInSc1d1
německý	německý	k2eAgInSc1d1
překlad	překlad	k1gInSc1
z	z	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
Veršované	veršovaný	k2eAgInPc1d1
letopisy	letopis	k1gInPc1
<g/>
)	)	kIx)
dochovaný	dochovaný	k2eAgInSc1d1
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
rukopise	rukopis	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1389	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Archivu	archiv	k1gInSc6
Pražského	pražský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Edel	Edel	k1gMnSc1
se	se	k3xPyFc4
dokonce	dokonce	k9
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
autor	autor	k1gMnSc1
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
také	také	k9
Jindřich	Jindřich	k1gMnSc1
z	z	k7c2
Varnsdorfu	Varnsdorf	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německý	německý	k2eAgInSc1d1
překlad	překlad	k1gInSc1
do	do	k7c2
prózy	próza	k1gFnSc2
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
ze	z	k7c2
tří	tři	k4xCgFnPc2
rukopisů	rukopis	k1gInPc2
<g/>
,	,	kIx,
nejstarší	starý	k2eAgInSc1d3
z	z	k7c2
roku	rok	k1gInSc2
1444	#num#	k4
a	a	k8xC
nejnovější	nový	k2eAgInPc1d3
z	z	k7c2
konce	konec	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Státní	státní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
a	a	k8xC
z	z	k7c2
konce	konec	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Univerzitní	univerzitní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
v	v	k7c6
Lipsku	Lipsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byla	být	k5eAaImAgFnS
jediná	jediný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
překladu	překlad	k1gInSc6
Dalimilovy	Dalimilův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
do	do	k7c2
latiny	latina	k1gFnSc2
pouze	pouze	k6eAd1
pro	pro	k7c4
dnes	dnes	k6eAd1
nedochovaný	dochovaný	k2eNgInSc4d1
rukopis	rukopis	k1gInSc4
piaristy	piarista	k1gMnSc2
Remigia	Remigius	k1gMnSc2
Maschata	Maschat	k1gMnSc2
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
také	také	k9
nález	nález	k1gInSc4
Pařížského	pařížský	k2eAgInSc2d1
zlomku	zlomek	k1gInSc2
<g/>
,	,	kIx,
středověkého	středověký	k2eAgInSc2d1
zlomku	zlomek	k1gInSc2
latinského	latinský	k2eAgInSc2d1
překladu	překlad	k1gInSc2
Dalimilovy	Dalimilův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
<g/>
,	,	kIx,
vzbudil	vzbudit	k5eAaPmAgInS
značnou	značný	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2005	#num#	k4
na	na	k7c6
aukci	aukce	k1gFnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
získala	získat	k5eAaPmAgFnS
česká	český	k2eAgFnSc1d1
Národní	národní	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
tento	tento	k3xDgInSc4
zlomek	zlomek	k1gInSc4
za	za	k7c4
339	#num#	k4
000	#num#	k4
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakoupený	zakoupený	k2eAgInSc1d1
spisek	spisek	k1gInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
odborníků	odborník	k1gMnPc2
nejvzácnějším	vzácný	k2eAgNnSc7d3
bohemikem	bohemikum	k1gNnSc7
<g/>
,	,	kIx,
jaké	jaký	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
na	na	k7c6
trhu	trh	k1gInSc6
objevilo	objevit	k5eAaPmAgNnS
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
nejméně	málo	k6eAd3
osmdesáti	osmdesát	k4xCc7
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Obsahuje	obsahovat	k5eAaImIp3nS
24	#num#	k4
bohatě	bohatě	k6eAd1
ilustrovaných	ilustrovaný	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
o	o	k7c6
formátu	formát	k1gInSc6
přibližně	přibližně	k6eAd1
A	a	k9
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
ze	z	k7c2
třicátých	třicátý	k4xOgNnPc2
nebo	nebo	k8xC
čtyřicátých	čtyřicátý	k4xOgNnPc2
let	léto	k1gNnPc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
knižní	knižní	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
</s>
<s>
První	první	k4xOgNnSc4
vydání	vydání	k1gNnSc4
tiskem	tisk	k1gInSc7
pořídil	pořídit	k5eAaPmAgInS
roku	rok	k1gInSc2
1620	#num#	k4
Pavel	Pavel	k1gMnSc1
Ješín	Ješín	k1gMnSc1
z	z	k7c2
Bezdězce	Bezdězka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
porážce	porážka	k1gFnSc6
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
výtisků	výtisk	k1gInPc2
zničena	zničit	k5eAaPmNgFnS
a	a	k8xC
kronika	kronika	k1gFnSc1
upadla	upadnout	k5eAaPmAgFnS
na	na	k7c4
čas	čas	k1gInSc4
v	v	k7c4
zapomnění	zapomnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovu	znovu	k6eAd1
vyšla	vyjít	k5eAaPmAgFnS
až	až	k6eAd1
roku	rok	k1gInSc2
1786	#num#	k4
nákladem	náklad	k1gInSc7
Františka	František	k1gMnSc2
Faustina	Faustin	k1gMnSc2
Procházky	Procházka	k1gMnSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
prvních	první	k4xOgMnPc2
obrozeneckých	obrozenecký	k2eAgMnPc2d1
buditelů	buditel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k9
znovu	znovu	k6eAd1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
širšího	široký	k2eAgInSc2d2
oběhu	oběh	k1gInSc2
a	a	k8xC
jistě	jistě	k6eAd1
zapůsobila	zapůsobit	k5eAaPmAgFnS
na	na	k7c4
upevňující	upevňující	k2eAgNnSc4d1
se	se	k3xPyFc4
národní	národní	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
kroniku	kronika	k1gFnSc4
podle	podle	k7c2
rukopisu	rukopis	k1gInSc2
Cambridgeského	cambridgeský	k2eAgInSc2d1
vydala	vydat	k5eAaPmAgFnS
Česká	český	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
byla	být	k5eAaImAgFnS
kronika	kronika	k1gFnSc1
přepsána	přepsat	k5eAaPmNgFnS
v	v	k7c6
novém	nový	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
Václavem	Václav	k1gMnSc7
Flajšhansem	Flajšhans	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
kronika	kronika	k1gFnSc1
v	v	k7c6
prozaické	prozaický	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
ji	on	k3xPp3gFnSc4
převedl	převést	k5eAaPmAgMnS
a	a	k8xC
upravil	upravit	k5eAaPmAgMnS
Milan	Milan	k1gMnSc1
Maralík	Maralík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
další	další	k2eAgNnSc1d1
novočeské	novočeský	k2eAgNnSc1d1
přebásnění	přebásnění	k1gNnSc1
od	od	k7c2
Marie	Maria	k1gFnSc2
Krčmové	krčmový	k2eAgFnSc2d1
a	a	k8xC
Hany	Hana	k1gFnSc2
Vrbové	Vrbová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1
novočeský	novočeský	k2eAgInSc1d1
překlad	překlad	k1gInSc1
(	(	kIx(
<g/>
přebásnění	přebásnění	k1gNnSc2
<g/>
)	)	kIx)
vydala	vydat	k5eAaPmAgFnS
Marie	Marie	k1gFnSc1
Bláhová	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
<g/>
,	,	kIx,
originál	originál	k1gInSc4
přeložila	přeložit	k5eAaPmAgFnS
Marie	Marie	k1gFnSc1
Krčmová	krčmový	k2eAgFnSc1d1
<g/>
,	,	kIx,
přebásnila	přebásnit	k5eAaPmAgFnS
<g/>
,	,	kIx,
doslov	doslov	k1gInSc1
k	k	k7c3
překladu	překlad	k1gInSc3
napsala	napsat	k5eAaPmAgFnS,k5eAaBmAgFnS
a	a	k8xC
slovníček	slovníček	k1gInSc1
starších	starý	k2eAgNnPc2d2
jmen	jméno	k1gNnPc2
a	a	k8xC
výrazů	výraz	k1gInPc2
sestavila	sestavit	k5eAaPmAgFnS
Hana	Hana	k1gFnSc1
Vrbová	Vrbová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
Národní	národní	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
barevná	barevný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
nově	nově	k6eAd1
nalezeného	nalezený	k2eAgInSc2d1
iluminovaného	iluminovaný	k2eAgInSc2d1
zlomku	zlomek	k1gInSc2
Dalimilovy	Dalimilův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
v	v	k7c6
latině	latina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BLÁHOVÁ	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
rýmovaná	rýmovaný	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
191	#num#	k4
<g/>
–	–	k?
<g/>
192	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
EDEL	EDEL	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
johanitského	johanitský	k2eAgMnSc2d1
komtura	komtur	k1gMnSc2
řečeného	řečený	k2eAgInSc2d1
Dalimil	Dalimil	k1gMnSc1
:	:	kIx,
kapitola	kapitola	k1gFnSc1
z	z	k7c2
dějin	dějiny	k1gFnPc2
české	český	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ISV	ISV	kA
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
195	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85866	#num#	k4
<g/>
-	-	kIx~
<g/>
61	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomáš	Tomáš	k1gMnSc1
Edel	Edel	k1gMnSc1
<g/>
:	:	kIx,
Příběh	příběh	k1gInSc1
johanitského	johanitský	k2eAgMnSc2d1
komtura	komtur	k1gMnSc2
řečeného	řečený	k2eAgInSc2d1
Dalimil	Dalimil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
z	z	k7c2
dějin	dějiny	k1gFnPc2
české	český	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
195	#num#	k4
s.	s.	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Fontes	Fontes	k1gInSc1
Nisae	Nisa	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7083	#num#	k4
<g/>
-	-	kIx~
<g/>
545	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
206	#num#	k4
<g/>
–	–	k?
<g/>
212	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bláhová	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
194	#num#	k4
<g/>
↑	↑	k?
Edel	Edel	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
135	#num#	k4
<g/>
↑	↑	k?
Bláhová	bláhový	k2eAgFnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
194	#num#	k4
<g/>
↑	↑	k?
Edel	Edel	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
–	–	k?
<g/>
138	#num#	k4
<g/>
↑	↑	k?
Edel	Edel	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
133	#num#	k4
<g/>
–	–	k?
<g/>
138	#num#	k4
<g/>
↑	↑	k?
Edel	Edel	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
158	#num#	k4
<g/>
–	–	k?
<g/>
160	#num#	k4
<g/>
↑	↑	k?
Dalimil	Dalimil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
Dalimilova	Dalimilův	k2eAgNnSc2d1
<g/>
:	:	kIx,
podle	podle	k7c2
rukopisu	rukopis	k1gInSc2
Cambridgeského	cambridgeský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Václav	Václav	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
Mourek	mourek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Čes.	Čes.	k1gMnSc1
akad	akad	k1gMnSc1
<g/>
.	.	kIx.
pro	pro	k7c4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
.	.	kIx.
175	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Bláhová	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staročeská	staročeský	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
v	v	k7c6
kontextu	kontext	k1gInSc6
středověké	středověký	k2eAgFnSc2d1
historiografie	historiografie	k1gFnSc2
latinského	latinský	k2eAgInSc2d1
kulturního	kulturní	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
a	a	k8xC
její	její	k3xOp3gFnSc1
pramenná	pramenný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Díl	díl	k1gInSc1
<g/>
]	]	kIx)
3	#num#	k4
<g/>
,	,	kIx,
Historický	historický	k2eAgInSc1d1
komentář	komentář	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalimil	Dalimil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Brom	brom	k1gInSc1
<g/>
,	,	kIx,
Vlastimil	Vlastimil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panovnické	panovnický	k2eAgInPc1d1
tituly	titul	k1gInPc1
v	v	k7c6
Dalimilově	Dalimilův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
využití	využití	k1gNnSc3
textové	textový	k2eAgFnSc2d1
lingvistiky	lingvistika	k1gFnSc2
pro	pro	k7c4
historickou	historický	k2eAgFnSc4d1
interpretaci	interpretace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Stát	stát	k1gInSc1
<g/>
,	,	kIx,
státnost	státnost	k1gFnSc1
a	a	k8xC
rituály	rituál	k1gInPc4
přemyslovského	přemyslovský	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémy	problém	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
<g/>
,	,	kIx,
otázky	otázka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc4
příspěvků	příspěvek	k1gInPc2
z	z	k7c2
konference	konference	k1gFnSc2
konané	konaný	k2eAgFnSc2d1
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vydání	vydání	k1gNnSc3
připravil	připravit	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
Wihoda	Wihoda	k1gMnSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Demeterem	Demeter	k1gMnSc7
Malaťákem	Malaťák	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
217	#num#	k4
<g/>
–	–	k?
<g/>
234	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Starší	starý	k2eAgFnSc1d2
česká	český	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
/	/	kIx~
Redaktor	redaktor	k1gMnSc1
svazku	svazek	k1gInSc2
Josef	Josef	k1gMnSc1
Hrabák	Hrabák	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československá	československý	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
.	.	kIx.
531	#num#	k4
s.	s.	k?
S.	S.	kA
114	#num#	k4
<g/>
–	–	k?
<g/>
121	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FORST	FORST	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gNnSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
:	:	kIx,
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
díla	dílo	k1gNnPc1
<g/>
,	,	kIx,
instituce	instituce	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
<g/>
–	–	k?
<g/>
G.	G.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
900	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
797	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
510	#num#	k4
<g/>
–	–	k?
<g/>
512	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
-	-	kIx~
Česko	Česko	k1gNnSc1
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
823	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
796	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
114	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Staročeská	staročeský	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
textu	text	k1gInSc2
a	a	k8xC
veškerého	veškerý	k3xTgInSc2
textového	textový	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vydání	vydání	k1gNnSc3
připravili	připravit	k5eAaPmAgMnP
Jiří	Jiří	k1gMnPc1
Daňhelka	Daňhelka	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Hádek	hádek	k1gMnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
Havránek	Havránek	k1gMnSc1
<g/>
(	(	kIx(
<g/>
†	†	k?
<g/>
)	)	kIx)
a	a	k8xC
Naděžda	Naděžda	k1gFnSc1
Kvítková	kvítkový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Šťastný	Šťastný	k1gMnSc1
<g/>
,	,	kIx,
Radko	Radka	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tajemství	tajemství	k1gNnSc2
jména	jméno	k1gNnSc2
Dalimil	Dalimil	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
345	#num#	k4
s.	s.	k?
:	:	kIx,
fot	fot	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
bibliografie	bibliografie	k1gFnSc1
<g/>
,	,	kIx,
frontispis	frontispis	k1gInSc1
<g/>
,	,	kIx,
angl.	angl.	k?
a	a	k8xC
něm.	něm.	k?
resumé	resumé	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7023-072-X	80-7023-072-X	k4
</s>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1
<g/>
,	,	kIx,
Pavla	Pavla	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
:	:	kIx,
12	#num#	k4
<g/>
.	.	kIx.
sešit	sešit	k1gInSc1
:	:	kIx,
D	D	kA
<g/>
–	–	k?
<g/>
Die	Die	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
105	#num#	k4
<g/>
–	–	k?
<g/>
215	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
415	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
113	#num#	k4
<g/>
–	–	k?
<g/>
114	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kosmova	Kosmův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc4
Rýmovaná	rýmovaný	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
česká	český	k2eAgFnSc1d1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Dalimil	Dalimil	k1gMnSc1
ve	v	k7c6
Vlastenském	vlastenský	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
historickém	historický	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
díla	dílo	k1gNnSc2
Dalimilova	Dalimilův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Kramerius	Kramerius	k1gMnSc1
NK	NK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Text	text	k1gInSc1
kroniky	kronika	k1gFnSc2
na	na	k7c6
webu	web	k1gInSc6
Archive	archiv	k1gInSc5
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
-	-	kIx~
fragment	fragment	k1gInSc1
latinského	latinský	k2eAgInSc2d1
překladu	překlad	k1gInSc2
na	na	k7c6
Manuscriptoriu	Manuscriptorium	k1gNnSc6
</s>
<s>
Dalimil	Dalimil	k1gMnSc1
–	–	k?
článek	článek	k1gInSc1
o	o	k7c6
pravděpodobném	pravděpodobný	k2eAgNnSc6d1
autorovi	autor	k1gMnSc3
na	na	k7c4
www.e-stredovek.cz	www.e-stredovek.cz	k1gInSc4
</s>
<s>
Kronika	kronika	k1gFnSc1
tak	tak	k6eAd1
řečeného	řečený	k2eAgMnSc2d1
Dalimila	Dalimil	k1gMnSc2
–	–	k?
článek	článek	k1gInSc1
na	na	k7c4
www.e-stredovek.cz	www.e-stredovek.cz	k1gInSc4
</s>
<s>
Výňatky	výňatek	k1gInPc1
z	z	k7c2
Dalimilovy	Dalimilův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Moravia	Moravius	k1gMnSc2
Magna	Magn	k1gMnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
unn	unn	k?
<g/>
2006374858	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4245528-5	4245528-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82125664	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
180638171	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82125664	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
