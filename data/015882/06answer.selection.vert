<s>
Úkolem	úkol	k1gInSc7
facilitátora	facilitátor	k1gMnSc2
je	být	k5eAaImIp3nS
usnadnit	usnadnit	k5eAaPmF
komunikaci	komunikace	k1gFnSc4
a	a	k8xC
dát	dát	k5eAaPmF
účastníkům	účastník	k1gMnPc3
diskuse	diskuse	k1gFnPc4
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
soustředit	soustředit	k5eAaPmF
na	na	k7c4
věcnou	věcný	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
problému	problém	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
řešení	řešení	k1gNnSc2
<g/>
.	.	kIx.
</s>