<s>
Facilitátor	Facilitátor	k1gMnSc1
</s>
<s>
Facilitátoři	Facilitátor	k1gMnPc1
</s>
<s>
Facilitátor	Facilitátor	k1gMnSc1
je	být	k5eAaImIp3nS
odborník	odborník	k1gMnSc1
na	na	k7c4
vedení	vedení	k1gNnSc4
diskuse	diskuse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
moderátora	moderátor	k1gMnSc2
aktivně	aktivně	k6eAd1
řídí	řídit	k5eAaImIp3nS
diskusi	diskuse	k1gFnSc4
po	po	k7c6
stránce	stránka	k1gFnSc6
procesní	procesní	k2eAgInSc1d1
a	a	k8xC
odpovídá	odpovídat	k5eAaImIp3nS
za	za	k7c4
její	její	k3xOp3gInSc4
průběh	průběh	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nikoliv	nikoliv	k9
za	za	k7c4
její	její	k3xOp3gInSc4
obsah	obsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Náplň	náplň	k1gFnSc1
činnosti	činnost	k1gFnSc2
facilitátora	facilitátor	k1gMnSc2
</s>
<s>
Úkolem	úkol	k1gInSc7
facilitátora	facilitátor	k1gMnSc2
je	být	k5eAaImIp3nS
usnadnit	usnadnit	k5eAaPmF
komunikaci	komunikace	k1gFnSc4
a	a	k8xC
dát	dát	k5eAaPmF
účastníkům	účastník	k1gMnPc3
diskuse	diskuse	k1gFnPc4
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
soustředit	soustředit	k5eAaPmF
na	na	k7c4
věcnou	věcný	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
problému	problém	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
řešení	řešení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Role	role	k1gFnSc1
facilitátora	facilitátor	k1gMnSc2
bývá	bývat	k5eAaImIp3nS
přirovnávána	přirovnávat	k5eAaImNgFnS
k	k	k7c3
porodní	porodní	k2eAgFnSc3d1
asistentce	asistentka	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sice	sice	k8xC
pomáhá	pomáhat	k5eAaImIp3nS
při	při	k7c6
narození	narození	k1gNnSc6
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
jeho	jeho	k3xOp3gMnSc7
rodičem	rodič	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facilitátor	Facilitátor	k1gMnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vždy	vždy	k6eAd1
nestranný	nestranný	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
diskusi	diskuse	k1gFnSc6
vyskytne	vyskytnout	k5eAaPmIp3nS
spor	spor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Facilitátor	Facilitátor	k1gMnSc1
dbá	dbát	k5eAaImIp3nS
na	na	k7c4
dodržování	dodržování	k1gNnSc4
dohodnutých	dohodnutý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
diskuse	diskuse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sleduje	sledovat	k5eAaImIp3nS
například	například	k6eAd1
<g/>
,	,	kIx,
zda	zda	k8xS
diskutující	diskutující	k1gMnSc1
nepřesáhl	přesáhnout	k5eNaPmAgMnS
dobu	doba	k1gFnSc4
vymezenou	vymezený	k2eAgFnSc4d1
pro	pro	k7c4
diskuzní	diskuzní	k2eAgInSc4d1
příspěvek	příspěvek	k1gInSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
byla	být	k5eAaImAgFnS
dohodnuta	dohodnout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
povzbuzuje	povzbuzovat	k5eAaImIp3nS
některé	některý	k3yIgMnPc4
zdrženlivější	zdrženlivý	k2eAgMnPc4d2
účastníky	účastník	k1gMnPc4
diskuse	diskuse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facilitátor	Facilitátor	k1gMnSc1
vždy	vždy	k6eAd1
musí	muset	k5eAaImIp3nS
vést	vést	k5eAaImF
diskusi	diskuse	k1gFnSc4
směrem	směr	k1gInSc7
k	k	k7c3
dosažení	dosažení	k1gNnSc3
shody	shoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
shoda	shoda	k1gFnSc1
není	být	k5eNaImIp3nS
možná	možný	k2eAgFnSc1d1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
by	by	kYmCp3nS
se	se	k3xPyFc4
pokusit	pokusit	k5eAaPmF
alespoň	alespoň	k9
objasnit	objasnit	k5eAaPmF
jádro	jádro	k1gNnSc4
problému	problém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facilitátor	Facilitátor	k1gMnSc1
obvykle	obvykle	k6eAd1
též	též	k9
dělá	dělat	k5eAaImIp3nS
během	během	k7c2
diskuse	diskuse	k1gFnSc2
shrnující	shrnující	k2eAgFnSc2d1
poznámky	poznámka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Požadavky	požadavek	k1gInPc1
na	na	k7c4
facilitátora	facilitátor	k1gMnSc4
</s>
<s>
Na	na	k7c4
facilitátora	facilitátor	k1gMnSc4
je	být	k5eAaImIp3nS
kladena	kladen	k2eAgFnSc1d1
řada	řada	k1gFnSc1
požadavků	požadavek	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
schopnost	schopnost	k1gFnSc1
pozorně	pozorně	k6eAd1
naslouchat	naslouchat	k5eAaImF
diskutujícím	diskutující	k1gMnSc7
<g/>
,	,	kIx,
respektovat	respektovat	k5eAaImF
diskutující	diskutující	k2eAgMnPc4d1
<g/>
,	,	kIx,
snažit	snažit	k5eAaImF
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
pochopit	pochopit	k5eAaPmF
<g/>
,	,	kIx,
v	v	k7c6
potřebné	potřebný	k2eAgFnSc6d1
míře	míra	k1gFnSc6
parafrázovat	parafrázovat	k5eAaBmF
výroky	výrok	k1gInPc4
diskutujících	diskutující	k1gMnPc2
<g/>
,	,	kIx,
za	za	k7c2
každé	každý	k3xTgFnSc2
situace	situace	k1gFnSc2
zachovat	zachovat	k5eAaPmF
nestrannost	nestrannost	k1gFnSc4
a	a	k8xC
umět	umět	k5eAaImF
zabránit	zabránit	k5eAaPmF
případným	případný	k2eAgInPc3d1
konfliktům	konflikt	k1gInPc3
mezi	mezi	k7c7
diskutujícími	diskutující	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facilitátor	Facilitátor	k1gMnSc1
ale	ale	k9
ve	v	k7c6
výsledku	výsledek	k1gInSc6
vždy	vždy	k6eAd1
musí	muset	k5eAaImIp3nS
jednat	jednat	k5eAaImF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
tvořivosti	tvořivost	k1gFnSc3
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
stručné	stručný	k2eAgFnPc1d1
definice	definice	k1gFnPc1
facilitátora	facilitátor	k1gMnSc2
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
skupinám	skupina	k1gFnPc3
a	a	k8xC
institucím	instituce	k1gFnPc3
pracovat	pracovat	k5eAaImF
efektivněji	efektivně	k6eAd2
a	a	k8xC
dosáhnout	dosáhnout	k5eAaPmF
lepší	dobrý	k2eAgFnPc4d2
součinnosti	součinnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
chování	chování	k1gNnSc4
skupiny	skupina	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
skupina	skupina	k1gFnSc1
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
efektivně	efektivně	k6eAd1
fungovat	fungovat	k5eAaImF
a	a	k8xC
dělat	dělat	k5eAaImF
kvalitní	kvalitní	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Úkolem	úkol	k1gInSc7
facilitátora	facilitátor	k1gMnSc2
je	být	k5eAaImIp3nS
podporovat	podporovat	k5eAaImF
nejlepší	dobrý	k2eAgFnPc4d3
myšlenky	myšlenka	k1gFnPc4
a	a	k8xC
postupy	postup	k1gInPc1
všech	všecek	k3xTgMnPc2
účastníků	účastník	k1gMnPc2
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
facilitátorů	facilitátor	k1gMnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
sdružující	sdružující	k2eAgMnPc4d1
facilitátory	facilitátor	k1gMnPc4
(	(	kIx(
<g/>
The	The	k1gMnPc7
International	International	k1gFnSc2
Association	Association	k1gInSc1
of	of	k?
Facilitators	Facilitators	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Praxe	praxe	k1gFnSc1
</s>
<s>
Během	během	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
byly	být	k5eAaImAgFnP
v	v	k7c6
Indii	Indie	k1gFnSc6
nainstalovány	nainstalován	k2eAgInPc4d1
automaty	automat	k1gInPc4
na	na	k7c4
prodej	prodej	k1gInSc4
jízdenek	jízdenka	k1gFnPc2
vybavené	vybavený	k2eAgNnSc1d1
jednoduchým	jednoduchý	k2eAgInSc7d1
dotykovým	dotykový	k2eAgInSc7d1
displejem	displej	k1gInSc7
a	a	k8xC
vyžadující	vyžadující	k2eAgNnSc1d1
použití	použití	k1gNnSc1
předplacených	předplacený	k2eAgFnPc2d1
čipových	čipový	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
využití	využití	k1gNnSc1
bylo	být	k5eAaImAgNnS
nízké	nízký	k2eAgNnSc1d1
a	a	k8xC
cestující	cestující	k1gMnPc1
raději	rád	k6eAd2
čekali	čekat	k5eAaImAgMnP
20	#num#	k4
až	až	k8xS
30	#num#	k4
minut	minuta	k1gFnPc2
u	u	k7c2
běžných	běžný	k2eAgFnPc2d1
pokladen	pokladna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
železnice	železnice	k1gFnSc1
umožnila	umožnit	k5eAaPmAgFnS
zaměstnancům	zaměstnanec	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
mimopracovní	mimopracovní	k2eAgFnSc6d1
době	doba	k1gFnSc6
nebo	nebo	k8xC
o	o	k7c6
dovolené	dovolená	k1gFnSc6
pracovali	pracovat	k5eAaImAgMnP
jako	jako	k9
„	„	k?
<g/>
facilitátoři	facilitátor	k1gMnPc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jednoho	jeden	k4xCgInSc2
měsíce	měsíc	k1gInSc2
se	se	k3xPyFc4
použití	použití	k1gNnSc1
automatů	automat	k1gInPc2
zvýšilo	zvýšit	k5eAaPmAgNnS
o	o	k7c4
20	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Facilitator	Facilitator	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
International	International	k1gFnSc2
Association	Association	k1gInSc1
of	of	k?
Facilitators	Facilitators	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Facilitace	Facilitace	k1gFnSc1
v	v	k7c6
podnikání	podnikání	k1gNnSc6
</s>
<s>
Zážitkové	zážitkový	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
</s>
<s>
Metoda	metoda	k1gFnSc1
šesti	šest	k4xCc2
klobouků	klobouk	k1gInPc2
</s>
