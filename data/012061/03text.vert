<p>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
888	[number]	k4	888
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
921	[number]	k4	921
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
915	[number]	k4	915
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
921	[number]	k4	921
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
knížete	kníže	k1gMnSc2	kníže
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
a	a	k8xC	a
kněžny	kněžna	k1gFnSc2	kněžna
Ludmily	Ludmila	k1gFnSc2	Ludmila
<g/>
,	,	kIx,	,
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Spytihněva	Spytihněvo	k1gNnSc2	Spytihněvo
I.	I.	kA	I.
<g/>
,	,	kIx,	,
otcem	otec	k1gMnSc7	otec
knížat	kníže	k1gMnPc2wR	kníže
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Knížetem	kníže	k1gNnSc7wR	kníže
===	===	k?	===
</s>
</p>
<p>
<s>
Legendy	legenda	k1gFnPc1	legenda
popisují	popisovat	k5eAaImIp3nP	popisovat
Vratislava	Vratislav	k1gMnSc4	Vratislav
jako	jako	k8xC	jako
schopného	schopný	k2eAgMnSc4d1	schopný
panovníka	panovník	k1gMnSc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
expandoval	expandovat	k5eAaImAgMnS	expandovat
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
české	český	k2eAgFnSc2d1	Česká
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
916	[number]	k4	916
založil	založit	k5eAaPmAgInS	založit
baziliku	bazilika	k1gFnSc4	bazilika
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
pochován	pochován	k2eAgMnSc1d1	pochován
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
charakterizovaly	charakterizovat	k5eAaBmAgInP	charakterizovat
boje	boj	k1gInPc1	boj
s	s	k7c7	s
Maďary	Maďar	k1gMnPc7	Maďar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Drahomírou	Drahomíra	k1gFnSc7	Drahomíra
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Stodoranů	Stodoran	k1gInPc2	Stodoran
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
906	[number]	k4	906
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
sedm	sedm	k4xCc4	sedm
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
syny	syn	k1gMnPc7	syn
–	–	k?	–
Václava	Václav	k1gMnSc4	Václav
<g/>
,	,	kIx,	,
Boleslava	Boleslav	k1gMnSc4	Boleslav
a	a	k8xC	a
Spytihněva	Spytihněv	k1gMnSc4	Spytihněv
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
jako	jako	k9	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
nikdy	nikdy	k6eAd1	nikdy
nestal	stát	k5eNaPmAgInS	stát
panujícím	panující	k2eAgMnSc7d1	panující
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
dcer	dcera	k1gFnPc2	dcera
možná	možná	k9	možná
byla	být	k5eAaImAgFnS	být
Střezislava	Střezislava	k1gFnSc1	Střezislava
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Slavníka	Slavník	k1gMnSc2	Slavník
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
příbuzným	příbuzný	k1gMnPc3	příbuzný
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
byl	být	k5eAaImAgMnS	být
Slavník	Slavník	k1gMnSc1	Slavník
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
rod	rod	k1gInSc1	rod
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kronika	kronika	k1gFnSc1	kronika
Gesta	gesto	k1gNnSc2	gesto
Hungarorum	Hungarorum	k1gNnSc1	Hungarorum
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vratislav	Vratislav	k1gMnSc1	Vratislav
I.	I.	kA	I.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
po	po	k7c6	po
zranění	zranění	k1gNnSc6	zranění
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
921	[number]	k4	921
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
uherského	uherský	k2eAgInSc2d1	uherský
nájezdu	nájezd	k1gInSc2	nájezd
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
pod	pod	k7c7	pod
podlahou	podlaha	k1gFnSc7	podlaha
před	před	k7c7	před
kněžištěm	kněžiště	k1gNnSc7	kněžiště
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc4	jeho
ostatky	ostatek	k1gInPc4	ostatek
v	v	k7c6	v
náhrobní	náhrobní	k2eAgFnSc6d1	náhrobní
tumbě	tumba	k1gFnSc6	tumba
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
lodi	loď	k1gFnSc6	loď
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
epištolní	epištolní	k2eAgFnSc6d1	epištolní
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
manželky	manželka	k1gFnSc2	manželka
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Vratislavově	Vratislavův	k2eAgFnSc6d1	Vratislavova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
tchyní	tchyně	k1gFnSc7	tchyně
Ludmilou	Ludmila	k1gFnSc7	Ludmila
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ještě	ještě	k9	ještě
za	za	k7c2	za
života	život	k1gInSc2	život
Vratislava	Vratislav	k1gMnSc2	Vratislav
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
jeho	jeho	k3xOp3gMnPc4	jeho
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
knížete	kníže	k1gNnSc2wR	kníže
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kmenové	kmenový	k2eAgNnSc1d1	kmenové
shromáždění	shromáždění	k1gNnSc1	shromáždění
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
moci	moc	k1gFnSc2	moc
mezi	mezi	k7c4	mezi
Ludmilu	Ludmila	k1gFnSc4	Ludmila
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
snachu	snacha	k1gFnSc4	snacha
Drahomíru	Drahomíra	k1gFnSc4	Drahomíra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ludmiliny	Ludmilin	k2eAgFnSc2d1	Ludmilina
péče	péče	k1gFnSc2	péče
byla	být	k5eAaImAgFnS	být
nadále	nadále	k6eAd1	nadále
svěřena	svěřen	k2eAgFnSc1d1	svěřena
výchova	výchova	k1gFnSc1	výchova
budoucího	budoucí	k2eAgNnSc2d1	budoucí
knížete	kníže	k1gNnSc2wR	kníže
<g/>
,	,	kIx,	,
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
jej	on	k3xPp3gInSc4	on
naopak	naopak	k6eAd1	naopak
měla	mít	k5eAaImAgFnS	mít
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gInSc2	jeho
nástupu	nástup	k1gInSc2	nástup
zastupovat	zastupovat	k5eAaImF	zastupovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
ženami	žena	k1gFnPc7	žena
snad	snad	k9	snad
souvisel	souviset	k5eAaImAgInS	souviset
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
přesouvala	přesouvat	k5eAaImAgFnS	přesouvat
z	z	k7c2	z
bavorského	bavorský	k2eAgNnSc2d1	bavorské
vévodství	vévodství	k1gNnSc2	vévodství
na	na	k7c4	na
Sasko	Sasko	k1gNnSc4	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
zasazovala	zasazovat	k5eAaImAgFnS	zasazovat
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
Saskem	Sasko	k1gNnSc7	Sasko
i	i	k8xC	i
přes	přes	k7c4	přes
riziko	riziko	k1gNnSc4	riziko
omezení	omezení	k1gNnSc2	omezení
relativně	relativně	k6eAd1	relativně
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
postavení	postavení	k1gNnSc2	postavení
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
snad	snad	k9	snad
věřila	věřit	k5eAaImAgFnS	věřit
spíše	spíše	k9	spíše
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
orientaci	orientace	k1gFnSc6	orientace
na	na	k7c4	na
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
(	(	kIx(	(
<g/>
Řezenská	řezenský	k2eAgFnSc1d1	řezenská
diecéze	diecéze	k1gFnSc1	diecéze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
konflikt	konflikt	k1gInSc1	konflikt
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
završen	završen	k2eAgMnSc1d1	završen
vraždou	vražda	k1gFnSc7	vražda
Ludmily	Ludmila	k1gFnSc2	Ludmila
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
921	[number]	k4	921
na	na	k7c6	na
hradišti	hradiště	k1gNnSc6	hradiště
Tetín	Tetín	k1gInSc1	Tetín
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ludmila	Ludmila	k1gFnSc1	Ludmila
byla	být	k5eAaImAgFnS	být
uškrcena	uškrtit	k5eAaPmNgFnS	uškrtit
Tunnou	tunný	k2eAgFnSc7d1	tunný
a	a	k8xC	a
Gommonem	Gommon	k1gInSc7	Gommon
<g/>
,	,	kIx,	,
svojí	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
šálou	šála	k1gFnSc7	šála
(	(	kIx(	(
<g/>
závojem	závoj	k1gInSc7	závoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
Varjagy	Varjaga	k1gFnSc2	Varjaga
(	(	kIx(	(
<g/>
ruský	ruský	k2eAgInSc1d1	ruský
název	název	k1gInSc1	název
severských	severský	k2eAgMnPc2d1	severský
Vikingů	Viking	k1gMnPc2	Viking
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
družiníky	družiník	k1gMnPc7	družiník
kněžny	kněžna	k1gFnSc2	kněžna
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Vratislav	Vratislav	k1gMnSc1	Vratislav
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
někdy	někdy	k6eAd1	někdy
nazýván	nazýván	k2eAgInSc1d1	nazýván
blahoslaveným	blahoslavený	k2eAgInSc7d1	blahoslavený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
I.	I.	kA	I.
Zrození	zrození	k1gNnSc1	zrození
státu	stát	k1gInSc2	stát
872	[number]	k4	872
<g/>
-	-	kIx~	-
<g/>
972	[number]	k4	972
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
po	po	k7c4	po
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
:	:	kIx,	:
vstup	vstup	k1gInSc1	vstup
Čechů	Čech	k1gMnPc2	Čech
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
530	[number]	k4	530
<g/>
–	–	k?	–
<g/>
935	[number]	k4	935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
658	[number]	k4	658
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vratislav	Vratislava	k1gFnPc2	Vratislava
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
