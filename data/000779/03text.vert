<s>
Adin	Adin	k1gInSc1	Adin
je	být	k5eAaImIp3nS	být
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
ádin	ádin	k1gInSc1	ádin
-	-	kIx~	-
citlivý	citlivý	k2eAgInSc1d1	citlivý
<g/>
,	,	kIx,	,
smyslný	smyslný	k2eAgInSc1d1	smyslný
<g/>
,	,	kIx,	,
něžný	něžný	k2eAgInSc1d1	něžný
<g/>
,	,	kIx,	,
jemný	jemný	k2eAgInSc1d1	jemný
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
má	mít	k5eAaImIp3nS	mít
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Adínek	Adínek	k1gMnSc1	Adínek
<g/>
,	,	kIx,	,
Ada	Ada	kA	Ada
<g/>
,	,	kIx,	,
Adouš	Adouš	k1gInSc4	Adouš
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Adin	Adin	k1gInSc1	Adin
</s>
