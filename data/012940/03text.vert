<p>
<s>
Kulometná	kulometný	k2eAgFnSc1d1	kulometná
pistole	pistole	k1gFnSc1	pistole
ZK-383	ZK-383	k1gFnSc2	ZK-383
byla	být	k5eAaImAgFnS	být
zbraň	zbraň	k1gFnSc1	zbraň
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
konstruktérem	konstruktér	k1gMnSc7	konstruktér
Josefem	Josef	k1gMnSc7	Josef
Kouckým	Koucký	k1gMnSc7	Koucký
a	a	k8xC	a
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
Československou	československý	k2eAgFnSc7d1	Československá
zbrojovkou	zbrojovka	k1gFnSc7	zbrojovka
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
čekoslovenským	čekoslovenský	k2eAgInSc7d1	čekoslovenský
sériově	sériově	k6eAd1	sériově
vyráběným	vyráběný	k2eAgInSc7d1	vyráběný
samopalem	samopal	k1gInSc7	samopal
<g/>
.	.	kIx.	.
</s>
<s>
Zbraň	zbraň	k1gFnSc1	zbraň
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
precizní	precizní	k2eAgNnPc1d1	precizní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc7	její
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
dílů	díl	k1gInPc2	díl
byla	být	k5eAaImAgFnS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
třískovým	třískový	k2eAgNnSc7d1	třískové
obráběním	obrábění	k1gNnSc7	obrábění
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
zbraň	zbraň	k1gFnSc1	zbraň
poměrně	poměrně	k6eAd1	poměrně
drahá	drahý	k2eAgFnSc1d1	drahá
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nevhodná	vhodný	k2eNgNnPc1d1	nevhodné
k	k	k7c3	k
masové	masový	k2eAgFnSc3d1	masová
válečné	válečný	k2eAgFnSc3d1	válečná
výrobě	výroba	k1gFnSc3	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
odběratelem	odběratel	k1gMnSc7	odběratel
zbraně	zbraň	k1gFnSc2	zbraň
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
mohly	moct	k5eAaImAgFnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
dražší	drahý	k2eAgFnPc1d2	dražší
ale	ale	k8xC	ale
kvalitnější	kvalitní	k2eAgFnPc1d2	kvalitnější
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
technický	technický	k2eAgInSc1d1	technický
a	a	k8xC	a
letecký	letecký	k2eAgInSc1d1	letecký
ústav	ústav	k1gInSc1	ústav
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
zbrojní	zbrojní	k2eAgInPc4d1	zbrojní
závody	závod	k1gInPc4	závod
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
automatické	automatický	k2eAgFnPc1d1	automatická
zbraně	zbraň	k1gFnPc1	zbraň
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
balistickým	balistický	k2eAgInSc7d1	balistický
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
Samopalu	samopal	k1gInSc2	samopal
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Josef	Josef	k1gMnSc1	Josef
Koucký	Koucký	k1gMnSc1	Koucký
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
zbrojovce	zbrojovka	k1gFnSc6	zbrojovka
Brno	Brno	k1gNnSc1	Brno
zabýval	zabývat	k5eAaImAgInS	zabývat
konstrukcí	konstrukce	k1gFnSc7	konstrukce
samopalu	samopal	k1gInSc2	samopal
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
už	už	k6eAd1	už
několik	několik	k4yIc4	několik
patentů	patent	k1gInPc2	patent
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
samopalů	samopal	k1gInPc2	samopal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zkoušky	zkouška	k1gFnPc1	zkouška
zbraně	zbraň	k1gFnSc2	zbraň
v	v	k7c6	v
ráži	ráže	k1gFnSc6	ráže
9	[number]	k4	9
<g/>
mm	mm	kA	mm
browning	browning	k1gInSc1	browning
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
na	na	k7c6	na
střelnici	střelnice	k1gFnSc6	střelnice
pobočného	pobočný	k2eAgInSc2d1	pobočný
závodu	závod	k1gInSc2	závod
ve	v	k7c6	v
Vsetíně	Vsetín	k1gInSc6	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
neprojevila	projevit	k5eNaPmAgFnS	projevit
o	o	k7c4	o
samopal	samopal	k1gInSc4	samopal
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
kulometnou	kulometný	k2eAgFnSc4d1	kulometná
pistoli	pistole	k1gFnSc4	pistole
vz	vz	k?	vz
38	[number]	k4	38
strakonické	strakonický	k2eAgFnPc1d1	Strakonická
zbrojovky	zbrojovka	k1gFnPc1	zbrojovka
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc1	Brno
dále	daleko	k6eAd2	daleko
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
samopal	samopal	k1gInSc4	samopal
na	na	k7c4	na
export	export	k1gInSc4	export
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
předělal	předělat	k5eAaPmAgMnS	předělat
Josef	Josef	k1gMnSc1	Josef
Koucký	Koucký	k1gMnSc1	Koucký
zbraň	zbraň	k1gFnSc4	zbraň
na	na	k7c4	na
náboje	náboj	k1gInPc4	náboj
ráže	ráže	k1gFnSc2	ráže
9	[number]	k4	9
mm	mm	kA	mm
Parabelum	parabelum	k1gNnSc2	parabelum
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
vzorky	vzorek	k1gInPc4	vzorek
v	v	k7c6	v
ráži	ráže	k1gFnSc6	ráže
.45	.45	k4	.45
ACP	ACP	kA	ACP
<g/>
.	.	kIx.	.
<g/>
ZK	ZK	kA	ZK
383	[number]	k4	383
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
verzi	verze	k1gFnSc6	verze
byla	být	k5eAaImAgFnS	být
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
jako	jako	k9	jako
zbraň	zbraň	k1gFnSc1	zbraň
pro	pro	k7c4	pro
palebnou	palebný	k2eAgFnSc4d1	palebná
podporu	podpora	k1gFnSc4	podpora
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
dvojnožkou	dvojnožka	k1gFnSc7	dvojnožka
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
vyměnitelnou	vyměnitelný	k2eAgFnSc4d1	vyměnitelná
hlavní	hlavní	k2eAgFnSc4d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
využití	využití	k1gNnSc2	využití
samopalu	samopal	k1gInSc2	samopal
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahrazoval	nahrazovat	k5eAaImAgInS	nahrazovat
lehký	lehký	k2eAgInSc1d1	lehký
kulomet	kulomet	k1gInSc1	kulomet
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Zbraň	zbraň	k1gFnSc1	zbraň
fungovala	fungovat	k5eAaImAgFnS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
dynamického	dynamický	k2eAgInSc2d1	dynamický
závěru	závěr	k1gInSc2	závěr
střílejícícho	střílejícícho	k6eAd1	střílejícícho
ze	z	k7c2	z
zadní	zadní	k2eAgFnSc2d1	zadní
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Hlaveň	hlaveň	k1gFnSc1	hlaveň
byla	být	k5eAaImAgFnS	být
kryta	krýt	k5eAaImNgFnS	krýt
perforovaným	perforovaný	k2eAgInSc7d1	perforovaný
pláštěm	plášť	k1gInSc7	plášť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
kromě	kromě	k7c2	kromě
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
popálením	popálení	k1gNnSc7	popálení
střelce	střelec	k1gMnSc2	střelec
plnil	plnit	k5eAaImAgMnS	plnit
také	také	k6eAd1	také
funkci	funkce	k1gFnSc4	funkce
uchycení	uchycení	k1gNnSc3	uchycení
hlavně	hlavně	k6eAd1	hlavně
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
samopalu	samopal	k1gInSc2	samopal
se	se	k3xPyFc4	se
zasouval	zasouvat	k5eAaImAgInS	zasouvat
segmentový	segmentový	k2eAgInSc1d1	segmentový
zásobník	zásobník	k1gInSc1	zásobník
zleva	zleva	k6eAd1	zleva
(	(	kIx(	(
<g/>
dodatková	dodatkový	k2eAgFnSc1d1	dodatková
pismena	pismen	k2eAgFnSc1d1	pismena
sz	sz	k?	sz
za	za	k7c7	za
názvem	název	k1gInSc7	název
zbraně	zbraň	k1gFnSc2	zbraň
znamenají	znamenat	k5eAaImIp3nP	znamenat
stranový	stranový	k2eAgInSc4d1	stranový
zásobník	zásobník	k1gInSc4	zásobník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
verze	verze	k1gFnSc1	verze
samopalu	samopal	k1gInSc2	samopal
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
lehkou	lehký	k2eAgFnSc7d1	lehká
dvojnožkou	dvojnožka	k1gFnSc7	dvojnožka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
sklápěla	sklápět	k5eAaImAgFnS	sklápět
dozadu	dozadu	k6eAd1	dozadu
do	do	k7c2	do
předpažbí	předpažbí	k1gNnSc2	předpažbí
<g/>
.	.	kIx.	.
</s>
<s>
Kadenci	kadence	k1gFnSc4	kadence
zbraně	zbraň	k1gFnSc2	zbraň
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zvýšit	zvýšit	k5eAaPmF	zvýšit
odebráním	odebrání	k1gNnSc7	odebrání
závaží	závaží	k1gNnSc4	závaží
ze	z	k7c2	z
závěru	závěr	k1gInSc2	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Zbraň	zbraň	k1gFnSc1	zbraň
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ovladatelná	ovladatelný	k2eAgFnSc1d1	ovladatelná
i	i	k9	i
při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
dávkou	dávka	k1gFnSc7	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
spouští	spoušť	k1gFnSc7	spoušť
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
přepínač	přepínač	k1gInSc1	přepínač
střelby	střelba	k1gFnSc2	střelba
<g/>
,	,	kIx,	,
při	při	k7c6	při
páčce	páčka	k1gFnSc6	páčka
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
krajní	krajní	k2eAgFnSc6d1	krajní
poloze	poloha	k1gFnSc6	poloha
zbraň	zbraň	k1gFnSc1	zbraň
střílela	střílet	k5eAaImAgFnS	střílet
dávkou	dávka	k1gFnSc7	dávka
a	a	k8xC	a
při	při	k7c6	při
poloze	poloha	k1gFnSc6	poloha
vpravo	vpravo	k6eAd1	vpravo
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
výstřely	výstřel	k1gInPc7	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
přepínače	přepínač	k1gInSc2	přepínač
střelby	střelba	k1gFnSc2	střelba
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
pojistka	pojistka	k1gFnSc1	pojistka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tlačítka	tlačítko	k1gNnSc2	tlačítko
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
při	při	k7c6	při
zamáčknutí	zamáčknutí	k1gNnSc6	zamáčknutí
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
zbraně	zbraň	k1gFnSc2	zbraň
zablokovalo	zablokovat	k5eAaPmAgNnS	zablokovat
spoušť	spoušť	k1gFnSc4	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
Pružina	pružina	k1gFnSc1	pružina
zbraně	zbraň	k1gFnSc2	zbraň
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
pažbě	pažba	k1gFnSc6	pažba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
i	i	k9	i
vytěrák	vytěrák	k1gInSc4	vytěrák
hlavně	hlavně	k6eAd1	hlavně
a	a	k8xC	a
olejnička	olejnička	k1gFnSc1	olejnička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc4	variant
==	==	k?	==
</s>
</p>
<p>
<s>
ZK	ZK	kA	ZK
383	[number]	k4	383
<g/>
:	:	kIx,	:
klasická	klasický	k2eAgFnSc1d1	klasická
varianta	varianta	k1gFnSc1	varianta
</s>
</p>
<p>
<s>
ZK	ZK	kA	ZK
383	[number]	k4	383
<g/>
-P	-P	k?	-P
<g/>
:	:	kIx,	:
policejní	policejní	k2eAgFnSc2d1	policejní
verze	verze	k1gFnSc2	verze
bez	bez	k7c2	bez
dvojnožky	dvojnožka	k1gFnSc2	dvojnožka
a	a	k8xC	a
vyměnitelné	vyměnitelný	k2eAgNnSc1d1	vyměnitelné
hlavně	hlavně	k6eAd1	hlavně
nabízena	nabízen	k2eAgFnSc1d1	nabízena
Venezuele	Venezuela	k1gFnSc3	Venezuela
a	a	k8xC	a
Bolívii	Bolívie	k1gFnSc3	Bolívie
</s>
</p>
<p>
<s>
ZK	ZK	kA	ZK
383	[number]	k4	383
H	H	kA	H
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
<g/>
)	)	kIx)	)
poválečná	poválečný	k2eAgFnSc1d1	poválečná
varianta	varianta	k1gFnSc1	varianta
se	s	k7c7	s
sklopným	sklopný	k2eAgInSc7d1	sklopný
zásobníkem	zásobník	k1gInSc7	zásobník
</s>
</p>
<p>
<s>
==	==	k?	==
Ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
používajících	používající	k2eAgInPc2d1	používající
samopaly	samopal	k1gInPc4	samopal
ZK-383	ZK-383	k1gFnPc6	ZK-383
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
říše	říše	k1gFnSc1	říše
jednotky	jednotka	k1gFnSc2	jednotka
SS	SS	kA	SS
<g/>
,	,	kIx,	,
neznámý	známý	k2eNgInSc1d1	neznámý
počet	počet	k1gInSc1	počet
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
190	[number]	k4	190
ks	ks	kA	ks
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
4000	[number]	k4	4000
ks	ks	kA	ks
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
</s>
</p>
<p>
<s>
Bolívie	Bolívie	k1gFnSc1	Bolívie
1000	[number]	k4	1000
ks	ks	kA	ks
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
</s>
</p>
<p>
<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
1200	[number]	k4	1200
ks	ks	kA	ks
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
12	[number]	k4	12
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
na	na	k7c4	na
Gunstribune	Gunstribun	k1gInSc5	Gunstribun
s	s	k7c7	s
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
</s>
</p>
