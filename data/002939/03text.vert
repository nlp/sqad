<s>
Logická	logický	k2eAgFnSc1d1	logická
operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
taková	takový	k3xDgFnSc1	takový
operace	operace	k1gFnSc1	operace
s	s	k7c7	s
výroky	výrok	k1gInPc7	výrok
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
pravdivostní	pravdivostní	k2eAgFnSc1d1	pravdivostní
hodnota	hodnota	k1gFnSc1	hodnota
(	(	kIx(	(
<g/>
PRAVDA	pravda	k1gFnSc1	pravda
nebo	nebo	k8xC	nebo
NEPRAVDA	nepravda	k1gFnSc1	nepravda
<g/>
)	)	kIx)	)
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
pravdivosti	pravdivost	k1gFnSc6	pravdivost
výroků	výrok	k1gInPc2	výrok
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Unární	unární	k2eAgFnSc7d1	unární
logickou	logický	k2eAgFnSc7d1	logická
operací	operace	k1gFnSc7	operace
je	být	k5eAaImIp3nS	být
negace	negace	k1gFnSc1	negace
<g/>
.	.	kIx.	.
</s>
<s>
Binárními	binární	k2eAgFnPc7d1	binární
operacemi	operace	k1gFnPc7	operace
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
konjunkce	konjunkce	k1gFnSc1	konjunkce
<g/>
,	,	kIx,	,
disjunkce	disjunkce	k1gFnSc1	disjunkce
<g/>
,	,	kIx,	,
implikace	implikace	k1gFnSc1	implikace
a	a	k8xC	a
ekvivalence	ekvivalence	k1gFnSc1	ekvivalence
<g/>
.	.	kIx.	.
logický	logický	k2eAgInSc1d1	logický
člen	člen	k1gInSc1	člen
(	(	kIx(	(
<g/>
hradlo	hradlo	k1gNnSc1	hradlo
<g/>
)	)	kIx)	)
konjunkce	konjunkce	k1gFnSc1	konjunkce
disjunkce	disjunkce	k1gFnSc1	disjunkce
implikace	implikace	k1gFnSc1	implikace
ekvivalence	ekvivalence	k1gFnSc1	ekvivalence
negace	negace	k1gFnSc1	negace
</s>
