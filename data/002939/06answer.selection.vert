<s>
Logická	logický	k2eAgFnSc1d1	logická
operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
taková	takový	k3xDgFnSc1	takový
operace	operace	k1gFnSc1	operace
s	s	k7c7	s
výroky	výrok	k1gInPc7	výrok
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
pravdivostní	pravdivostní	k2eAgFnSc1d1	pravdivostní
hodnota	hodnota	k1gFnSc1	hodnota
(	(	kIx(	(
<g/>
PRAVDA	pravda	k1gFnSc1	pravda
nebo	nebo	k8xC	nebo
NEPRAVDA	nepravda	k1gFnSc1	nepravda
<g/>
)	)	kIx)	)
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
pravdivosti	pravdivost	k1gFnSc6	pravdivost
výroků	výrok	k1gInPc2	výrok
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
