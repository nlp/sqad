<s>
Montgomery	Montgomera	k1gFnPc4	Montgomera
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Alabama	Alabamum	k1gNnSc2	Alabamum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Montgomery	Montgomera	k1gFnSc2	Montgomera
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Alabamy	Alabam	k1gInPc1	Alabam
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
censu	census	k1gInSc2	census
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
201	[number]	k4	201
568	[number]	k4	568
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
sloučením	sloučení	k1gNnSc7	sloučení
dvou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
naproti	naproti	k6eAd1	naproti
sobě	se	k3xPyFc3	se
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Alabama	Alabamum	k1gNnSc2	Alabamum
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1861	[number]	k4	1861
bylo	být	k5eAaImAgNnS	být
Montgomery	Montgomer	k1gMnPc4	Montgomer
vybráno	vybrán	k2eAgNnSc1d1	vybráno
jako	jako	k8xS	jako
první	první	k4xOgNnSc1	první
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Konfederovaných	konfederovaný	k2eAgInPc2d1	konfederovaný
spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Richmondu	Richmond	k1gInSc2	Richmond
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Montgomery	Montgomer	k1gMnPc4	Montgomer
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgFnSc1d1	velká
vojenská	vojenský	k2eAgFnSc1d1	vojenská
přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blízkosti	blízkost	k1gFnSc3	blízkost
letecké	letecký	k2eAgFnSc2d1	letecká
základny	základna	k1gFnSc2	základna
Maxwell	maxwell	k1gInSc1	maxwell
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc3	force
Base	basa	k1gFnSc3	basa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Alabama	Alabama	k1gFnSc1	Alabama
State	status	k1gInSc5	status
University	universita	k1gFnPc4	universita
a	a	k8xC	a
Auburn	Auburn	k1gInSc4	Auburn
University-Mongomery	University-Mongomera	k1gFnSc2	University-Mongomera
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
205	[number]	k4	205
764	[number]	k4	764
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
37,3	[number]	k4	37,3
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
56,6	[number]	k4	56,6
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
3,9	[number]	k4	3,9
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Pietrasanta	Pietrasanta	k1gFnSc1	Pietrasanta
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Zelda	Zelda	k1gFnSc1	Zelda
Fitzgeraldová	Fitzgeraldová	k1gFnSc1	Fitzgeraldová
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
-	-	kIx~	-
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
spisovatele	spisovatel	k1gMnSc2	spisovatel
F.	F.	kA	F.
Scotta	Scott	k1gMnSc2	Scott
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
Nat	Nat	k1gMnSc2	Nat
King	King	k1gInSc1	King
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
-	-	kIx~	-
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
pianista	pianista	k1gMnSc1	pianista
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
