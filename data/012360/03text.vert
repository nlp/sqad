<p>
<s>
Užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatá	k1gFnSc1	podplamatá
(	(	kIx(	(
<g/>
Natrix	Natrix	k1gInSc1	Natrix
tessellata	tesselle	k1gNnPc1	tesselle
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
evropský	evropský	k2eAgMnSc1d1	evropský
nejedovatý	jedovatý	k2eNgMnSc1d1	nejedovatý
had	had	k1gMnSc1	had
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
užovkovitých	užovkovitý	k2eAgFnPc2d1	užovkovitý
(	(	kIx(	(
<g/>
Colubridae	Colubridae	k1gFnPc2	Colubridae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podčeledi	podčeleď	k1gFnPc4	podčeleď
Natricinae	Natricina	k1gFnSc2	Natricina
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Natrix	Natrix	k1gInSc4	Natrix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
až	až	k9	až
o	o	k7c4	o
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
80-100	[number]	k4	80-100
cm	cm	kA	cm
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
až	až	k9	až
1,5	[number]	k4	1,5
m.	m.	k?	m.
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
změřený	změřený	k2eAgInSc1d1	změřený
exemplář	exemplář	k1gInSc1	exemplář
měřil	měřit	k5eAaImAgInS	měřit
150	[number]	k4	150
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nepotvrzené	potvrzený	k2eNgFnPc1d1	nepotvrzená
zprávy	zpráva	k1gFnPc1	zpráva
hovoří	hovořit	k5eAaImIp3nP	hovořit
až	až	k9	až
o	o	k7c6	o
dvoumetrových	dvoumetrový	k2eAgInPc6d1	dvoumetrový
exemplářích	exemplář	k1gInPc6	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
protáhlá	protáhlý	k2eAgFnSc1d1	protáhlá
hlava	hlava	k1gFnSc1	hlava
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
zřetelně	zřetelně	k6eAd1	zřetelně
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
šupiny	šupina	k1gFnPc1	šupina
v	v	k7c6	v
devatenácti	devatenáct	k4xCc6	devatenáct
řadách	řada	k1gFnPc6	řada
kolem	kolem	k7c2	kolem
těla	tělo	k1gNnSc2	tělo
mají	mít	k5eAaImIp3nP	mít
výrazně	výrazně	k6eAd1	výrazně
kýlnaté	kýlnatý	k2eAgFnPc1d1	kýlnatá
<g/>
.	.	kIx.	.
</s>
<s>
Barvu	barva	k1gFnSc4	barva
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
od	od	k7c2	od
šedozelené	šedozelený	k2eAgFnSc2d1	šedozelená
až	až	k9	až
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
černou	černý	k2eAgFnSc4d1	černá
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
mají	mít	k5eAaImIp3nP	mít
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
řad	řada	k1gFnPc2	řada
tmavých	tmavý	k2eAgFnPc2d1	tmavá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
příčné	příčný	k2eAgInPc4d1	příčný
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
je	být	k5eAaImIp3nS	být
nažloutlé	nažloutlý	k2eAgNnSc1d1	nažloutlé
až	až	k6eAd1	až
červené	červený	k2eAgNnSc1d1	červené
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
ostře	ostro	k6eAd1	ostro
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
žlutě	žlutě	k6eAd1	žlutě
nebo	nebo	k8xC	nebo
oranžově	oranžově	k6eAd1	oranžově
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc1d1	podobná
kostkám	kostka	k1gFnPc3	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
skvrn	skvrna	k1gFnPc2	skvrna
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
i	i	k9	i
jejich	jejich	k3xOp3gNnSc1	jejich
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
plama	plama	k1gFnSc1	plama
<g/>
"	"	kIx"	"
dříve	dříve	k6eAd2	dříve
vyjadřovalo	vyjadřovat	k5eAaImAgNnS	vyjadřovat
pojem	pojem	k1gInSc4	pojem
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatá	k1gFnSc1	podplamatá
znamená	znamenat	k5eAaImIp3nS	znamenat
něco	něco	k3yInSc1	něco
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dole	dole	k6eAd1	dole
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
užovky	užovka	k1gFnSc2	užovka
obojkové	obojkový	k2eAgFnSc2d1	obojková
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
zbarvení	zbarvení	k1gNnPc2	zbarvení
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
šupin	šupina	k1gFnPc2	šupina
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatá	k1gFnSc1	podplamatá
má	mít	k5eAaImIp3nS	mít
8	[number]	k4	8
retních	retní	k2eAgInPc2d1	retní
štítků	štítek	k1gInPc2	štítek
(	(	kIx(	(
<g/>
užovka	užovka	k1gFnSc1	užovka
obojková	obojkový	k2eAgFnSc1d1	obojková
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
předoční	předoční	k2eAgInPc1d1	předoční
štítky	štítek	k1gInPc1	štítek
(	(	kIx(	(
<g/>
užovka	užovka	k1gFnSc1	užovka
obojková	obojkový	k2eAgFnSc1d1	obojková
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
záočních	záoční	k2eAgInPc2d1	záoční
(	(	kIx(	(
<g/>
užovka	užovka	k1gFnSc1	užovka
obojková	obojkový	k2eAgFnSc1d1	obojková
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
potoků	potok	k1gInPc2	potok
nebo	nebo	k8xC	nebo
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
bahnitým	bahnitý	k2eAgInPc3d1	bahnitý
tokům	tok	k1gInPc3	tok
se	se	k3xPyFc4	se
však	však	k9	však
většinou	většinou	k6eAd1	většinou
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
většinou	většinou	k6eAd1	většinou
opouští	opouštět	k5eAaImIp3nS	opouštět
až	až	k9	až
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Žere	žrát	k5eAaImIp3nS	žrát
především	především	k9	především
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ale	ale	k9	ale
také	také	k9	také
obojživelníky	obojživelník	k1gMnPc4	obojživelník
jako	jako	k8xS	jako
žáby	žába	k1gFnPc4	žába
<g/>
,	,	kIx,	,
ropuchy	ropucha	k1gFnPc4	ropucha
<g/>
,	,	kIx,	,
čolky	čolek	k1gMnPc4	čolek
a	a	k8xC	a
pulce	pulec	k1gMnPc4	pulec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
had	had	k1gMnSc1	had
není	být	k5eNaImIp3nS	být
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
kousavější	kousavý	k2eAgFnSc1d2	kousavější
než	než	k8xS	než
užovka	užovka	k1gFnSc1	užovka
obojková	obojkový	k2eAgFnSc1d1	obojková
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
šíří	šířit	k5eAaImIp3nS	šířit
zapáchající	zapáchající	k2eAgInSc4d1	zapáchající
sekret	sekret	k1gInSc4	sekret
z	z	k7c2	z
kloaky	kloaka	k1gFnSc2	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
obranným	obranný	k2eAgInSc7d1	obranný
mechanismem	mechanismus	k1gInSc7	mechanismus
je	být	k5eAaImIp3nS	být
thanatoza	thanatoza	k1gFnSc1	thanatoza
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
had	had	k1gMnSc1	had
předstírá	předstírat	k5eAaImIp3nS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
hned	hned	k6eAd1	hned
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
hibernace	hibernace	k1gFnSc2	hibernace
v	v	k7c6	v
(	(	kIx(	(
<g/>
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
samice	samice	k1gFnSc2	samice
obvykle	obvykle	k6eAd1	obvykle
klade	klást	k5eAaImIp3nS	klást
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
od	od	k7c2	od
páření	páření	k1gNnSc2	páření
obvykle	obvykle	k6eAd1	obvykle
45	[number]	k4	45
až	až	k9	až
60	[number]	k4	60
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
hnízdě	hnízdo	k1gNnSc6	hnízdo
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Háďata	hádě	k1gNnPc1	hádě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
od	od	k7c2	od
nakladení	nakladení	k1gNnSc2	nakladení
vajec	vejce	k1gNnPc2	vejce
8	[number]	k4	8
až	až	k6eAd1	až
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
měří	měřit	k5eAaImIp3nP	měřit
kolem	kolem	k7c2	kolem
20	[number]	k4	20
cm	cm	kA	cm
a	a	k8xC	a
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
svlečou	svléct	k5eAaPmIp3nP	svléct
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
začnou	začít	k5eAaPmIp3nP	začít
přijímat	přijímat	k5eAaImF	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
drobné	drobný	k2eAgFnPc1d1	drobná
rybky	rybka	k1gFnPc1	rybka
a	a	k8xC	a
pulci	pulec	k1gMnPc1	pulec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
pak	pak	k6eAd1	pak
užovky	užovka	k1gFnPc1	užovka
podplamaté	podplamatá	k1gFnSc2	podplamatá
zahajují	zahajovat	k5eAaImIp3nP	zahajovat
zimní	zimní	k2eAgInSc4d1	zimní
spánek	spánek	k1gInSc4	spánek
v	v	k7c6	v
dírách	díra	k1gFnPc6	díra
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatá	k1gFnSc1	podplamatá
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
:	:	kIx,	:
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
,	,	kIx,	,
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
,	,	kIx,	,
Jemen	Jemen	k1gInSc1	Jemen
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
spíše	spíše	k9	spíše
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
větších	veliký	k2eAgInPc2d2	veliký
toků	tok	k1gInPc2	tok
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nad	nad	k7c7	nad
400	[number]	k4	400
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Chráněna	chráněn	k2eAgFnSc1d1	chráněna
jsou	být	k5eAaImIp3nP	být
jí	on	k3xPp3gFnSc3	on
užívaná	užívaný	k2eAgNnPc4d1	užívané
přirozená	přirozený	k2eAgNnPc4d1	přirozené
i	i	k8xC	i
umělá	umělý	k2eAgNnPc4d1	umělé
sídla	sídlo	k1gNnPc4	sídlo
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
biotop	biotop	k1gInSc4	biotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dice	Dic	k1gMnSc2	Dic
snake	snak	k1gMnSc2	snak
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MORAVEC	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Plazi	plaz	k1gMnPc1	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
531	[number]	k4	531
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2416	[number]	k4	2416
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Natrix	Natrix	k1gInSc1	Natrix
tessellata	tesselle	k1gNnPc1	tesselle
–	–	k?	–
užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatá	k1gFnSc1	podplamatá
<g/>
,	,	kIx,	,
s.	s.	k?	s.
364	[number]	k4	364
<g/>
–	–	k?	–
<g/>
395	[number]	k4	395
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatat	k5eAaPmIp3nS	podplamatat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatat	k5eAaPmIp3nS	podplamatat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Natrix	Natrix	k1gInSc1	Natrix
tessellata	tesselle	k1gNnPc1	tesselle
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Natrix	Natrix	k1gInSc1	Natrix
tessellata	tesselle	k1gNnPc1	tesselle
(	(	kIx(	(
<g/>
užovka	užovka	k1gFnSc1	užovka
podplamatá	podplamatá	k1gFnSc1	podplamatá
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
