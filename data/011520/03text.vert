<p>
<s>
Pat	pat	k1gInSc1	pat
Torpey	Torpea	k1gFnSc2	Torpea
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1953	[number]	k4	1953
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
hardrockový	hardrockový	k2eAgMnSc1d1	hardrockový
hudebník	hudebník	k1gMnSc1	hudebník
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
bubeník	bubeník	k1gMnSc1	bubeník
a	a	k8xC	a
doprovodný	doprovodný	k2eAgMnSc1d1	doprovodný
zpěvák	zpěvák	k1gMnSc1	zpěvák
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
Big	Big	k1gFnPc2	Big
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
pro	pro	k7c4	pro
další	další	k2eAgMnPc4d1	další
interprety	interpret	k1gMnPc4	interpret
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
John	John	k1gMnSc1	John
Parr	Parr	k1gMnSc1	Parr
<g/>
,	,	kIx,	,
Belinda	Belinda	k1gFnSc1	Belinda
Carlisle	Carlisle	k1gMnSc1	Carlisle
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
Montrose	Montrosa	k1gFnSc6	Montrosa
<g/>
,	,	kIx,	,
Richie	Richie	k1gFnSc1	Richie
Kotzen	Kotzna	k1gFnPc2	Kotzna
či	či	k8xC	či
kapela	kapela	k1gFnSc1	kapela
The	The	k1gFnSc2	The
Knack	Knacka	k1gFnPc2	Knacka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
Torpey	Torpea	k1gFnSc2	Torpea
trpěl	trpět	k5eAaImAgMnS	trpět
Parkinsonovovou	Parkinsonovová	k1gFnSc4	Parkinsonovová
chorobou	choroba	k1gFnSc7	choroba
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
nemohl	moct	k5eNaImAgMnS	moct
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
bicí	bicí	k2eAgMnPc4d1	bicí
na	na	k7c6	na
albech	album	k1gNnPc6	album
...	...	k?	...
<g/>
The	The	k1gFnSc1	The
Stories	Stories	k1gMnSc1	Stories
We	We	k1gMnSc1	We
Could	Could	k1gMnSc1	Could
Tell	Tell	k1gMnSc1	Tell
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
Defying	Defying	k1gInSc1	Defying
Gravity	Gravita	k1gFnSc2	Gravita
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Big	Big	k?	Big
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
však	však	k9	však
zůstával	zůstávat	k5eAaImAgInS	zůstávat
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
zmiňované	zmiňovaný	k2eAgFnSc6d1	zmiňovaná
desce	deska	k1gFnSc6	deska
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
bicí	bicí	k2eAgMnSc1d1	bicí
na	na	k7c6	na
automatu	automat	k1gInSc6	automat
<g/>
,	,	kIx,	,
u	u	k7c2	u
Defying	Defying	k1gInSc4	Defying
Gravity	Gravita	k1gFnSc2	Gravita
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
producent	producent	k1gMnSc1	producent
bicích	bicí	k2eAgFnPc2d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
Torpey	Torpea	k1gFnSc2	Torpea
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
64	[number]	k4	64
let	léto	k1gNnPc2	léto
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
komplikací	komplikace	k1gFnPc2	komplikace
způsobených	způsobený	k2eAgFnPc2d1	způsobená
chorobou	choroba	k1gFnSc7	choroba
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pat	pat	k1gInSc1	pat
Torpey	Torpea	k1gFnSc2	Torpea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pat	pata	k1gFnPc2	pata
Torpey	Torpea	k1gFnSc2	Torpea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
