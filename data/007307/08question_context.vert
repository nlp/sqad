<s>
Je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
druhem	druh	k1gInSc7
rodu	rod	k1gInSc2
jinan	jinan	k1gInSc4
z	z	k7c2
monotypické	monotypický	k2eAgFnSc2d1
čeledě	čeleď	k1gFnSc2
jinanovitých	jinanovitý	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Jinan	jinan	k1gInSc1
dvoulaločný	dvoulaločný	k2eAgInSc1d1
(	(	kIx(
<g/>
Ginkgo	Ginkgo	k1gNnSc1
biloba	biloba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
také	také	k9
ginkgo	ginkgo	k6eAd1
biloba	biloba	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dvoudomý	dvoudomý	k2eAgInSc1d1
opadavý	opadavý	k2eAgInSc1d1
strom	strom	k1gInSc1
<g/>
.	.	kIx.
</s>