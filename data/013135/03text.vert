<p>
<s>
Buzuki	Buzuki	k1gNnSc1	Buzuki
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
τ	τ	k?	τ
Μ	Μ	k?	Μ
<g/>
;	;	kIx,	;
pl.	pl.	k?	pl.
τ	τ	k?	τ
μ	μ	k?	μ
<g/>
,	,	kIx,	,
Buzúki	Buzúke	k1gFnSc4	Buzúke
<g/>
,	,	kIx,	,
přepis	přepis	k1gInSc4	přepis
i	i	k8xC	i
Bouzouki	Bouzouke	k1gFnSc4	Bouzouke
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řecký	řecký	k2eAgInSc1d1	řecký
strunný	strunný	k2eAgInSc1d1	strunný
drnkací	drnkací	k2eAgInSc1d1	drnkací
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
z	z	k7c2	z
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
nástroji	nástroj	k1gInSc6	nástroj
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
drnkací	drnkací	k2eAgInSc4d1	drnkací
nástroj	nástroj	k1gInSc4	nástroj
podobný	podobný	k2eAgInSc4d1	podobný
Buzuki	Buzuk	k1gInPc7	Buzuk
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
Řeků	Řek	k1gMnPc2	Řek
populární	populární	k2eAgMnSc1d1	populární
v	v	k7c6	v
Byzantské	byzantský	k2eAgFnSc6d1	byzantská
době	doba	k1gFnSc6	doba
a	a	k8xC	a
nazýval	nazývat	k5eAaImAgMnS	nazývat
se	se	k3xPyFc4	se
tambura	tambura	k1gFnSc1	tambura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
tento	tento	k3xDgInSc4	tento
nástroj	nástroj	k1gInSc4	nástroj
převzali	převzít	k5eAaPmAgMnP	převzít
Turci	Turek	k1gMnPc1	Turek
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
tureckým	turecký	k2eAgInSc7d1	turecký
lidovým	lidový	k2eAgInSc7d1	lidový
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
řecké	řecký	k2eAgNnSc1d1	řecké
jméno	jméno	k1gNnSc1	jméno
Buzuki	Buzuk	k1gFnSc2	Buzuk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
turečtiny	turečtina	k1gFnSc2	turečtina
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
buzuk	buzuk	k1gInSc4	buzuk
(	(	kIx(	(
<g/>
zlomený	zlomený	k2eAgInSc4d1	zlomený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
výstižně	výstižně	k6eAd1	výstižně
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
zalomený	zalomený	k2eAgInSc4d1	zalomený
konec	konec	k1gInSc4	konec
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
nástroj	nástroj	k1gInSc1	nástroj
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
u	u	k7c2	u
Řeků	Řek	k1gMnPc2	Řek
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Anatolie	Anatolie	k1gFnSc2	Anatolie
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
trvaní	trvaný	k2eAgMnPc1d1	trvaný
tureckého	turecký	k2eAgInSc2d1	turecký
sultanátu	sultanát	k1gInSc2	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mateřském	mateřský	k2eAgNnSc6d1	mateřské
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
drnkací	drnkací	k2eAgInPc1d1	drnkací
nástroje	nástroj	k1gInPc1	nástroj
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Buzuki	Buzuk	k1gFnSc2	Buzuk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
