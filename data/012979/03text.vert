<p>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Formula	Formula	k1gFnSc1	Formula
1	[number]	k4	1
Nagydíj	Nagydíj	k1gFnSc1	Nagydíj
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Hungarian	Hungarian	k1gInSc1	Hungarian
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
závodů	závod	k1gInPc2	závod
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
vozů	vůz	k1gInPc2	vůz
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
pořádané	pořádaný	k2eAgNnSc1d1	pořádané
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgNnSc7d1	tradiční
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
trať	trať	k1gFnSc1	trať
Hungaroring	Hungaroring	k1gInSc4	Hungaroring
nedaleko	nedaleko	k7c2	nedaleko
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vítězové	vítěz	k1gMnPc1	vítěz
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Opakovaná	opakovaný	k2eAgNnPc4d1	opakované
vítězství	vítězství	k1gNnPc4	vítězství
(	(	kIx(	(
<g/>
jezdci	jezdec	k1gInSc6	jezdec
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
označeni	označen	k2eAgMnPc1d1	označen
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
aktuální	aktuální	k2eAgFnSc6d1	aktuální
sezóně	sezóna	k1gFnSc6	sezóna
účastní	účastnit	k5eAaImIp3nS	účastnit
šampionátu	šampionát	k1gInSc2	šampionát
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opakovaná	opakovaný	k2eAgNnPc1d1	opakované
vítězství	vítězství	k1gNnPc1	vítězství
(	(	kIx(	(
<g/>
týmy	tým	k1gInPc1	tým
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
označeny	označen	k2eAgInPc1d1	označen
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
aktuální	aktuální	k2eAgFnSc6d1	aktuální
sezóně	sezóna	k1gFnSc6	sezóna
účastní	účastnit	k5eAaImIp3nS	účastnit
šampionátu	šampionát	k1gInSc2	šampionát
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vítězové	vítěz	k1gMnPc1	vítěz
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
letech	léto	k1gNnPc6	léto
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
