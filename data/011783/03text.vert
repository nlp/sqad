<p>
<s>
Kjell	Kjellit	k5eAaPmRp2nS	Kjellit
Magne	Magn	k1gMnSc5	Magn
Bondevik	Bondevik	k1gInSc4	Bondevik
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1947	[number]	k4	1947
v	v	k7c6	v
Molde	Mold	k1gMnSc5	Mold
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
norský	norský	k2eAgMnSc1d1	norský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
luterský	luterský	k2eAgMnSc1d1	luterský
pastor	pastor	k1gMnSc1	pastor
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
byl	být	k5eAaImAgInS	být
norským	norský	k2eAgMnSc7d1	norský
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Molde	Mold	k1gInSc5	Mold
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Johannes	Johannes	k1gMnSc1	Johannes
Bondevik	Bondevik	k1gMnSc1	Bondevik
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Kjell	Kjellit	k5eAaPmRp2nS	Kjellit
Magne	Magn	k1gMnSc5	Magn
Bondevik	Bondevik	k1gMnSc1	Bondevik
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
teologii	teologie	k1gFnSc4	teologie
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pastorem	pastor	k1gMnSc7	pastor
norské	norský	k2eAgFnSc2d1	norská
státní	státní	k2eAgFnSc2d1	státní
luterské	luterský	k2eAgFnSc2d1	luterská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Bjø	Bjø	k1gFnSc2	Bjø
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Rasmussen	Rasmussen	k1gInSc1	Rasmussen
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
(	(	kIx(	(
<g/>
Stortingu	Storting	k1gInSc2	Storting
<g/>
)	)	kIx)	)
zasedá	zasedat	k5eAaImIp3nS	zasedat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
za	za	k7c4	za
Křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
lidovou	lidový	k2eAgFnSc4d1	lidová
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
Kristelig	Kristelig	k1gInSc4	Kristelig
Folkeparti	Folkepart	k1gMnPc1	Folkepart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
ministrem	ministr	k1gMnSc7	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
norského	norský	k2eAgMnSc2d1	norský
premiéra	premiér	k1gMnSc2	premiér
vykonával	vykonávat	k5eAaImAgInS	vykonávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
a	a	k8xC	a
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
Bondevik	Bondevik	k1gMnSc1	Bondevik
nevykonával	vykonávat	k5eNaImAgMnS	vykonávat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
úřad	úřad	k1gInSc1	úřad
premiéra	premiér	k1gMnSc2	premiér
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
depresivní	depresivní	k2eAgFnSc2d1	depresivní
epizody	epizoda	k1gFnSc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
vybudovat	vybudovat	k5eAaPmF	vybudovat
na	na	k7c4	na
území	území	k1gNnSc4	území
Norska	Norsko	k1gNnSc2	Norsko
novou	nový	k2eAgFnSc4d1	nová
plynovou	plynový	k2eAgFnSc4d1	plynová
elektrárnu	elektrárna	k1gFnSc4	elektrárna
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
stavba	stavba	k1gFnSc1	stavba
přispěla	přispět	k5eAaPmAgFnS	přispět
ke	k	k7c3	k
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
navštívil	navštívit	k5eAaPmAgMnS	navštívit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
premiéra	premiér	k1gMnSc2	premiér
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K.	K.	kA	K.
M.	M.	kA	M.
Bondevik	Bondevik	k1gMnSc1	Bondevik
je	být	k5eAaImIp3nS	být
aktivním	aktivní	k2eAgMnSc7d1	aktivní
křesťanem	křesťan	k1gMnSc7	křesťan
a	a	k8xC	a
abstinentem	abstinent	k1gMnSc7	abstinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kjell	Kjella	k1gFnPc2	Kjella
Magne	Magn	k1gInSc5	Magn
Bondevik	Bondevik	k1gInSc4	Bondevik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Kjell	Kjella	k1gFnPc2	Kjella
Magne	Magn	k1gInSc5	Magn
Bondevik	Bondevika	k1gFnPc2	Bondevika
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
