<p>
<s>
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
islandsky	islandsky	k6eAd1	islandsky
Ísland	Ísland	k1gInSc1	Ísland
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
a	a	k8xC	a
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
350 000	[number]	k4	350 000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
103 125	[number]	k4	103 125
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejřidčeji	řídce	k6eAd3	řídce
zalidněný	zalidněný	k2eAgInSc1d1	zalidněný
evropský	evropský	k2eAgInSc1d1	evropský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Reykjavík	Reykjavík	k1gInSc1	Reykjavík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
aglomeraci	aglomerace	k1gFnSc6	aglomerace
žijí	žít	k5eAaImIp3nP	žít
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
islandské	islandský	k2eAgFnSc2d1	islandská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
lávová	lávový	k2eAgNnPc1d1	lávové
pole	pole	k1gNnPc1	pole
<g/>
,	,	kIx,	,
pohoří	pohoří	k1gNnPc1	pohoří
<g/>
,	,	kIx,	,
ledovce	ledovec	k1gInPc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Břehy	břeh	k1gInPc1	břeh
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
omývány	omývat	k5eAaImNgFnP	omývat
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
významně	významně	k6eAd1	významně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
zdejší	zdejší	k2eAgNnSc4d1	zdejší
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
810	[number]	k4	810
km	km	kA	km
od	od	k7c2	od
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
970	[number]	k4	970
km	km	kA	km
od	od	k7c2	od
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
287	[number]	k4	287
km	km	kA	km
od	od	k7c2	od
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Prvními	první	k4xOgFnPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
Islandu	Island	k1gInSc2	Island
byli	být	k5eAaImAgMnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Norové	Nor	k1gMnPc1	Nor
a	a	k8xC	a
Keltové	Kelt	k1gMnPc1	Kelt
(	(	kIx(	(
<g/>
irští	irský	k2eAgMnPc1d1	irský
a	a	k8xC	a
skotští	skotský	k2eAgMnPc1d1	skotský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
koncem	koncem	k7c2	koncem
9.	[number]	k4	9.
a	a	k8xC	a
v	v	k7c6	v
10.	[number]	k4	10.
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Vikinští	vikinský	k2eAgMnPc1d1	vikinský
náčelníci	náčelník	k1gMnPc1	náčelník
zakládali	zakládat	k5eAaImAgMnP	zakládat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
thingy	thing	k1gInPc1	thing
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgInP	být
místní	místní	k2eAgInPc1d1	místní
sněmy	sněm	k1gInPc1	sněm
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
veřejně	veřejně	k6eAd1	veřejně
urovnávaly	urovnávat	k5eAaImAgInP	urovnávat
spory	spor	k1gInPc1	spor
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
mezi	mezi	k7c4	mezi
osadníky	osadník	k1gMnPc4	osadník
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
930	[number]	k4	930
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Althing	Althing	k1gInSc1	Althing
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
národní	národní	k2eAgInSc1d1	národní
sněm	sněm	k1gInSc1	sněm
můžeme	moct	k5eAaImIp1nP	moct
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
nejstarší	starý	k2eAgInSc4d3	nejstarší
evropský	evropský	k2eAgInSc4d1	evropský
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgMnSc3	jenž
měl	mít	k5eAaImAgInS	mít
Island	Island	k1gInSc1	Island
kulturně	kulturně	k6eAd1	kulturně
i	i	k9	i
geograficky	geograficky	k6eAd1	geograficky
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Olaf	Olaf	k1gMnSc1	Olaf
Trygvason	Trygvason	k1gMnSc1	Trygvason
(	(	kIx(	(
<g/>
norský	norský	k2eAgMnSc1d1	norský
král	král	k1gMnSc1	král
v	v	k7c6	v
letech	let	k1gInPc6	let
995	[number]	k4	995
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
šíření	šíření	k1gNnSc3	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
tou	ten	k3xDgFnSc7	ten
nejtvrdší	tvrdý	k2eAgFnSc7d3	nejtvrdší
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
misionáři	misionář	k1gMnPc1	misionář
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
postupovat	postupovat	k5eAaImF	postupovat
stejně	stejně	k6eAd1	stejně
a	a	k8xC	a
ničili	ničit	k5eAaImAgMnP	ničit
především	především	k6eAd1	především
pohanské	pohanský	k2eAgFnPc4d1	pohanská
svatyně	svatyně	k1gFnPc4	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Sněm	sněm	k1gInSc1	sněm
Althing	Althing	k1gInSc4	Althing
je	být	k5eAaImIp3nS	být
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
psance	psanec	k1gMnSc4	psanec
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
rouhat	rouhat	k5eAaImF	rouhat
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vypovězen	vypovědět	k5eAaPmNgInS	vypovědět
<g/>
.	.	kIx.	.
</s>
<s>
Olaf	Olaf	k1gInSc1	Olaf
dal	dát	k5eAaPmAgInS	dát
zatknout	zatknout	k5eAaPmF	zatknout
Islanďany	Islanďan	k1gMnPc4	Islanďan
přebývající	přebývající	k2eAgMnPc4d1	přebývající
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
hrozil	hrozit	k5eAaImAgMnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
Island	Island	k1gInSc1	Island
nepříjme	příjit	k5eNaPmRp1nP	příjit
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
rukojmí	rukojmí	k1gNnPc2	rukojmí
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
přijal	přijmout	k5eAaPmAgInS	přijmout
Althing	Althing	k1gInSc1	Althing
v	v	k7c6	v
letech	let	k1gInPc6	let
999	[number]	k4	999
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
kompromisní	kompromisní	k2eAgInSc1d1	kompromisní
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
Islanďané	Islanďan	k1gMnPc1	Islanďan
dají	dát	k5eAaPmIp3nP	dát
pokřtít	pokřtít	k5eAaPmF	pokřtít
<g/>
,	,	kIx,	,
že	že	k8xS	že
však	však	k9	však
bude	být	k5eAaImBp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
obětovat	obětovat	k5eAaBmF	obětovat
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13.	[number]	k4	13.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
islandští	islandský	k2eAgMnPc1d1	islandský
náčelníci	náčelník	k1gMnPc1	náčelník
podrobili	podrobit	k5eAaPmAgMnP	podrobit
norské	norský	k2eAgFnSc3d1	norská
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1397	[number]	k4	1397
se	se	k3xPyFc4	se
Island	Island	k1gInSc1	Island
společně	společně	k6eAd1	společně
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
období	období	k1gNnSc1	období
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
úpadku	úpadek	k1gInSc2	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
byla	být	k5eAaImAgFnS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
k	k	k7c3	k
erupci	erupce	k1gFnSc3	erupce
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
menších	malý	k2eAgFnPc2d2	menší
sopek	sopka	k1gFnPc2	sopka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
sopky	sopka	k1gFnSc2	sopka
Laki	Lak	k1gFnSc2	Lak
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
hladomor	hladomor	k1gMnSc1	hladomor
a	a	k8xC	a
vymření	vymření	k1gNnPc1	vymření
až	až	k6eAd1	až
25	[number]	k4	25
%	%	kIx~	%
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
populace	populace	k1gFnSc2	populace
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Následky	následek	k1gInPc1	následek
výbuchu	výbuch	k1gInSc2	výbuch
sopky	sopka	k1gFnSc2	sopka
Askja	Askj	k1gInSc2	Askj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
zničily	zničit	k5eAaPmAgFnP	zničit
islandskou	islandský	k2eAgFnSc4d1	islandská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
způsobily	způsobit	k5eAaPmAgFnP	způsobit
velký	velký	k2eAgInSc4d1	velký
hladomor	hladomor	k1gInSc4	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
25	[number]	k4	25
let	léto	k1gNnPc2	léto
20	[number]	k4	20
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Islandu	Island	k1gInSc2	Island
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
země	zem	k1gFnSc2	zem
začala	začít	k5eAaPmAgFnS	začít
pomalu	pomalu	k6eAd1	pomalu
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Samostatnost	samostatnost	k1gFnSc4	samostatnost
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
Islandu	Island	k1gInSc2	Island
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
osamostatňování	osamostatňování	k1gNnSc1	osamostatňování
Islandu	Island	k1gInSc2	Island
vytvořením	vytvoření	k1gNnSc7	vytvoření
společné	společný	k2eAgFnSc2d1	společná
Dánsko-Islandské	dánskoslandský	k2eAgFnSc2d1	dánsko-islandský
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
shodla	shodnout	k5eAaBmAgFnS	shodnout
na	na	k7c6	na
úplné	úplný	k2eAgFnSc6d1	úplná
autonomii	autonomie	k1gFnSc6	autonomie
pro	pro	k7c4	pro
Island	Island	k1gInSc4	Island
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
po	po	k7c4	po
schválení	schválení	k1gNnSc4	schválení
dánským	dánský	k2eAgMnSc7d1	dánský
a	a	k8xC	a
islandským	islandský	k2eAgInSc7d1	islandský
parlamentem	parlament	k1gInSc7	parlament
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
návrh	návrh	k1gInSc1	návrh
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
se	se	k3xPyFc4	se
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnSc1	obyvatel
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Přijetím	přijetí	k1gNnSc7	přijetí
tohoto	tento	k3xDgInSc2	tento
návrhu	návrh	k1gInSc2	návrh
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
1.	[number]	k4	1.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
Island	Island	k1gInSc4	Island
suverénním	suverénní	k2eAgInSc7d1	suverénní
a	a	k8xC	a
samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
oslavy	oslava	k1gFnPc4	oslava
ale	ale	k8xC	ale
zhatila	zhatit	k5eAaPmAgFnS	zhatit
krutá	krutý	k2eAgFnSc1d1	krutá
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Island	Island	k1gInSc1	Island
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
společného	společný	k2eAgMnSc2d1	společný
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
odvolání	odvolání	k1gNnSc2	odvolání
staralo	starat	k5eAaImAgNnS	starat
o	o	k7c4	o
obranu	obrana	k1gFnSc4	obrana
islandských	islandský	k2eAgFnPc2d1	islandská
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
vyplatilo	vyplatit	k5eAaPmAgNnS	vyplatit
Islanďanům	Islanďan	k1gMnPc3	Islanďan
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
korun	koruna	k1gFnPc2	koruna
do	do	k7c2	do
fondu	fond	k1gInSc2	fond
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
a	a	k8xC	a
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
byla	být	k5eAaImAgFnS	být
platná	platný	k2eAgFnSc1d1	platná
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
a	a	k8xC	a
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
mohl	moct	k5eAaImAgMnS	moct
kterýkoli	kterýkoli	k3yIgMnSc1	kterýkoli
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
úpravu	úprava	k1gFnSc4	úprava
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
k	k	k7c3	k
revizi	revize	k1gFnSc3	revize
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
jakýkoliv	jakýkoliv	k3yIgMnSc1	jakýkoliv
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
smlouvu	smlouva	k1gFnSc4	smlouva
jednostranně	jednostranně	k6eAd1	jednostranně
vypovědět	vypovědět	k5eAaPmF	vypovědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
a	a	k8xC	a
okupace	okupace	k1gFnSc1	okupace
Islandu	Island	k1gInSc2	Island
===	===	k?	===
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Islandu	Island	k1gInSc2	Island
žili	žít	k5eAaImAgMnP	žít
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
unii	unie	k1gFnSc6	unie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
v	v	k7c6	v
neutrální	neutrální	k2eAgFnSc6d1	neutrální
zemi	zem	k1gFnSc6	zem
bez	bez	k7c2	bez
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
neutralitu	neutralita	k1gFnSc4	neutralita
si	se	k3xPyFc3	se
hodlali	hodlat	k5eAaImAgMnP	hodlat
udržet	udržet	k5eAaPmF	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
válečné	válečný	k2eAgNnSc4d1	válečné
období	období	k1gNnSc4	období
byla	být	k5eAaImAgFnS	být
utvořena	utvořen	k2eAgFnSc1d1	utvořena
tzv.	tzv.	kA	tzv.
válečná	válečná	k1gFnSc1	válečná
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ani	ani	k8xC	ani
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Dánska	Dánsko	k1gNnSc2	Dánsko
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
britskou	britský	k2eAgFnSc4d1	britská
nabídku	nabídka	k1gFnSc4	nabídka
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Němci	Němec	k1gMnPc1	Němec
neobsadili	obsadit	k5eNaPmAgMnP	obsadit
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
podnikli	podniknout	k5eAaPmAgMnP	podniknout
bleskovou	bleskový	k2eAgFnSc4d1	blesková
operaci	operace	k1gFnSc4	operace
"	"	kIx"	"
<g/>
Fork	Fork	k1gInSc4	Fork
<g/>
"	"	kIx"	"
a	a	k8xC	a
10.	[number]	k4	10.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
Island	Island	k1gInSc4	Island
obsadili	obsadit	k5eAaPmAgMnP	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
převzaly	převzít	k5eAaPmAgFnP	převzít
obranu	obrana	k1gFnSc4	obrana
Islandu	Island	k1gInSc2	Island
od	od	k7c2	od
Britů	Brit	k1gMnPc2	Brit
zatím	zatím	k6eAd1	zatím
neutrální	neutrální	k2eAgInPc1d1	neutrální
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
přistoupili	přistoupit	k5eAaPmAgMnP	přistoupit
Islanďané	Islanďan	k1gMnPc1	Islanďan
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
vyklizení	vyklizení	k1gNnSc2	vyklizení
Islandu	Island	k1gInSc2	Island
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
respektování	respektování	k1gNnSc1	respektování
suverenity	suverenita	k1gFnSc2	suverenita
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
především	především	k6eAd1	především
letiště	letiště	k1gNnSc4	letiště
u	u	k7c2	u
města	město	k1gNnSc2	město
Keflavík	Keflavík	k1gInSc1	Keflavík
a	a	k8xC	a
rozmístili	rozmístit	k5eAaPmAgMnP	rozmístit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
radarové	radarový	k2eAgFnSc2d1	radarová
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Američanům	Američan	k1gMnPc3	Američan
se	se	k3xPyFc4	se
rapidně	rapidně	k6eAd1	rapidně
snížila	snížit	k5eAaPmAgFnS	snížit
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
stavu	stav	k1gInSc2	stav
měly	mít	k5eAaImAgFnP	mít
USA	USA	kA	USA
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
47 000	[number]	k4	47 000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
poválečné	poválečný	k2eAgNnSc4d1	poválečné
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
30.	[number]	k4	30.
let	let	k1gInSc4	let
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
zrušení	zrušení	k1gNnSc4	zrušení
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
obrana	obrana	k1gFnSc1	obrana
islandských	islandský	k2eAgFnPc2d1	islandská
vod	voda	k1gFnPc2	voda
již	již	k6eAd1	již
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Dánska	Dánsko	k1gNnSc2	Dánsko
Německem	Německo	k1gNnSc7	Německo
nebyla	být	k5eNaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
probíhaly	probíhat	k5eAaImAgFnP	probíhat
přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
byla	být	k5eAaImAgFnS	být
vypověditelná	vypověditelný	k2eAgFnSc1d1	vypověditelná
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
dánských	dánský	k2eAgMnPc2d1	dánský
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
a	a	k8xC	a
Američanů	Američan	k1gMnPc2	Američan
byl	být	k5eAaImAgInS	být
až	až	k9	až
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
Altingem	Alting	k1gInSc7	Alting
schválen	schválen	k2eAgInSc4d1	schválen
návrh	návrh	k1gInSc4	návrh
zrušení	zrušení	k1gNnSc4	zrušení
unie	unie	k1gFnSc2	unie
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
a	a	k8xC	a
vyhlášeno	vyhlášen	k2eAgNnSc4d1	vyhlášeno
celonárodní	celonárodní	k2eAgNnSc4d1	celonárodní
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
98	[number]	k4	98
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
97	[number]	k4	97
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dalo	dát	k5eAaPmAgNnS	dát
hlas	hlas	k1gInSc4	hlas
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
95	[number]	k4	95
%	%	kIx~	%
jich	on	k3xPp3gFnPc2	on
dalo	dát	k5eAaPmAgNnS	dát
hlas	hlas	k1gInSc1	hlas
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
17.	[number]	k4	17.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
Island	Island	k1gInSc1	Island
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Alting	Alting	k1gInSc1	Alting
zvolil	zvolit	k5eAaPmAgInS	zvolit
za	za	k7c4	za
1.	[number]	k4	1.
prezidenta	prezident	k1gMnSc4	prezident
Sveinna	Sveinn	k1gMnSc4	Sveinn
Björnssona	Björnsson	k1gMnSc4	Björnsson
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Kristián	Kristián	k1gMnSc1	Kristián
X.	X.	kA	X.
poslal	poslat	k5eAaPmAgMnS	poslat
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
dni	den	k1gInSc3	den
telegram	telegram	k1gInSc1	telegram
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
politování	politování	k1gNnSc4	politování
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
islandský	islandský	k2eAgInSc1d1	islandský
lid	lid	k1gInSc1	lid
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
za	za	k7c2	za
těchto	tento	k3xDgInPc2	tento
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
popřál	popřát	k5eAaPmAgMnS	popřát
mu	on	k3xPp3gMnSc3	on
vše	všechen	k3xTgNnSc1	všechen
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
ústavy	ústava	k1gFnSc2	ústava
spočívala	spočívat	k5eAaImAgFnS	spočívat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
nahrazení	nahrazení	k1gNnSc6	nahrazení
slova	slovo	k1gNnSc2	slovo
král	král	k1gMnSc1	král
slovem	slovo	k1gNnSc7	slovo
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
odešly	odejít	k5eAaPmAgInP	odejít
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
1947	[number]	k4	1947
všechny	všechen	k3xTgFnPc1	všechen
cizí	cizí	k2eAgFnPc1d1	cizí
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tu	tu	k6eAd1	tu
samou	samý	k3xTgFnSc4	samý
dobu	doba	k1gFnSc4	doba
bylo	být	k5eAaImAgNnS	být
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
USA	USA	kA	USA
o	o	k7c6	o
předání	předání	k1gNnSc6	předání
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Keflavíku	Keflavík	k1gInSc6	Keflavík
a	a	k8xC	a
možnosti	možnost	k1gFnSc3	možnost
mezipřistání	mezipřistání	k1gNnSc2	mezipřistání
amerických	americký	k2eAgNnPc2d1	americké
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
Islandu	Island	k1gInSc2	Island
plynuly	plynout	k5eAaImAgInP	plynout
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Island	Island	k1gInSc1	Island
stále	stále	k6eAd1	stále
zůstával	zůstávat	k5eAaImAgInS	zůstávat
neutrální	neutrální	k2eAgMnSc1d1	neutrální
a	a	k8xC	a
bez	bez	k7c2	bez
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
bylo	být	k5eAaImAgNnS	být
Islandu	Island	k1gInSc6	Island
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
NATO	NATO	kA	NATO
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
nabídku	nabídka	k1gFnSc4	nabídka
přijala	přijmout	k5eAaPmAgFnS	přijmout
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
nebudou	být	k5eNaImBp3nP	být
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
rozmístěna	rozmístěn	k2eAgNnPc4d1	rozmístěno
cizí	cizí	k2eAgNnPc4d1	cizí
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
již	již	k6eAd1	již
vláda	vláda	k1gFnSc1	vláda
podepsala	podepsat	k5eAaPmAgFnS	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
obraně	obrana	k1gFnSc6	obrana
s	s	k7c7	s
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Keflavíku	Keflavík	k1gInSc6	Keflavík
přistálo	přistát	k5eAaPmAgNnS	přistát
prvních	první	k4xOgNnPc6	první
15	[number]	k4	15
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
získal	získat	k5eAaPmAgInS	získat
Island	Island	k1gInSc1	Island
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
až	až	k9	až
1960	[number]	k4	1960
kolem	kolem	k7c2	kolem
70	[number]	k4	70
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
postupně	postupně	k6eAd1	postupně
navráceny	navrácen	k2eAgInPc4d1	navrácen
islandské	islandský	k2eAgInPc4d1	islandský
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
21.	[number]	k4	21.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
postihla	postihnout	k5eAaPmAgFnS	postihnout
zemi	zem	k1gFnSc4	zem
vážná	vážný	k2eAgFnSc1d1	vážná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
pád	pád	k1gInSc4	pád
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
premiér	premiér	k1gMnSc1	premiér
Geir	Geir	k1gMnSc1	Geir
Haarde	Haard	k1gInSc5	Haard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pád	pád	k1gInSc4	pád
vlády	vláda	k1gFnPc1	vláda
měly	mít	k5eAaImAgFnP	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
i	i	k8xC	i
poměrně	poměrně	k6eAd1	poměrně
masivní	masivní	k2eAgFnPc4d1	masivní
demonstrace	demonstrace	k1gFnPc4	demonstrace
a	a	k8xC	a
protesty	protest	k1gInPc4	protest
<g/>
.	.	kIx.	.
25.	[number]	k4	25.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
předčasné	předčasný	k2eAgFnPc1d1	předčasná
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
levicové	levicový	k2eAgFnPc1d1	levicová
strany	strana	k1gFnPc1	strana
(	(	kIx(	(
<g/>
premiérkou	premiérka	k1gFnSc7	premiérka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Jóhanna	Jóhanen	k2eAgFnSc1d1	Jóhanna
Sigurð	Sigurð	k1gFnSc1	Sigurð
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
velké	velký	k2eAgFnPc1d1	velká
banky	banka	k1gFnPc1	banka
byly	být	k5eAaImAgFnP	být
znárodněny	znárodněn	k2eAgInPc1d1	znárodněn
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vypracována	vypracován	k2eAgFnSc1d1	vypracována
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
i	i	k9	i
občanská	občanský	k2eAgNnPc4d1	občanské
shromáždění	shromáždění	k1gNnPc4	shromáždění
<g/>
.	.	kIx.	.
16.	[number]	k4	16.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
podal	podat	k5eAaPmAgInS	podat
Island	Island	k1gInSc1	Island
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
následně	následně	k6eAd1	následně
11.	[number]	k4	11.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
a	a	k8xC	a
přístupové	přístupový	k2eAgInPc1d1	přístupový
rozhovory	rozhovor	k1gInPc1	rozhovor
byly	být	k5eAaImAgInP	být
zastaveny	zastavit	k5eAaPmNgInP	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Islandský	islandský	k2eAgInSc1d1	islandský
parlament	parlament	k1gInSc1	parlament
Althing	Althing	k1gInSc1	Althing
má	mít	k5eAaImIp3nS	mít
63	[number]	k4	63
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
jednou	jednou	k9	jednou
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
volen	volit	k5eAaImNgInS	volit
přímo	přímo	k6eAd1	přímo
občany	občan	k1gMnPc4	občan
a	a	k8xC	a
plní	plnit	k5eAaImIp3nP	plnit
spíše	spíše	k9	spíše
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
zatím	zatím	k6eAd1	zatím
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
republiky	republika	k1gFnSc2	republika
tvořena	tvořit	k5eAaImNgFnS	tvořit
zástupci	zástupce	k1gMnPc7	zástupce
dvou	dva	k4xCgFnPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žádná	žádný	k3yNgFnSc1	žádný
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
dosud	dosud	k6eAd1	dosud
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
republiky	republika	k1gFnSc2	republika
nezískala	získat	k5eNaPmAgFnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
Islandu	Island	k1gInSc2	Island
je	být	k5eAaImIp3nS	být
Gudni	Gudeň	k1gFnSc3	Gudeň
Jóhannesson	Jóhannessona	k1gFnPc2	Jóhannessona
a	a	k8xC	a
předsedkyní	předsedkyně	k1gFnPc2	předsedkyně
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
Katrín	Katrín	k1gMnSc1	Katrín
Jakobsdóttir	Jakobsdóttir	k1gMnSc1	Jakobsdóttir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
islandských	islandský	k2eAgMnPc2d1	islandský
prezidentů	prezident	k1gMnPc2	prezident
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Sveinn	Sveinn	k1gMnSc1	Sveinn
Björnsson	Björnsson	k1gMnSc1	Björnsson
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1881	[number]	k4	1881
-	-	kIx~	-
25.	[number]	k4	25.
ledna	leden	k1gInSc2	leden
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
vládl	vládnout	k5eAaImAgInS	vládnout
od	od	k7c2	od
17.	[number]	k4	17.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
do	do	k7c2	do
25.	[number]	k4	25.
ledna	leden	k1gInSc2	leden
1952.	[number]	k4	1952.
</s>
</p>
<p>
<s>
Ásgeir	Ásgeir	k1gMnSc1	Ásgeir
Ásgeirsson	Ásgeirsson	k1gMnSc1	Ásgeirsson
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1894	[number]	k4	1894
-	-	kIx~	-
15.	[number]	k4	15.
září	září	k1gNnSc2	září
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
vládl	vládnout	k5eAaImAgInS	vládnout
od	od	k7c2	od
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
do	do	k7c2	do
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1968.	[number]	k4	1968.
</s>
</p>
<p>
<s>
Kristján	Kristján	k1gMnSc1	Kristján
Eldjárn	Eldjárn	k1gMnSc1	Eldjárn
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1916	[number]	k4	1916
-	-	kIx~	-
14.	[number]	k4	14.
září	září	k1gNnSc2	září
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
vládl	vládnout	k5eAaImAgInS	vládnout
od	od	k7c2	od
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
do	do	k7c2	do
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1980.	[number]	k4	1980.
</s>
</p>
<p>
<s>
Vigdís	Vigdís	k1gInSc1	Vigdís
Finnbogadóttir	Finnbogadóttir	k1gInSc1	Finnbogadóttir
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
vládla	vládnout	k5eAaImAgFnS	vládnout
od	od	k7c2	od
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
do	do	k7c2	do
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1996.	[number]	k4	1996.
</s>
</p>
<p>
<s>
Ólafur	Ólafur	k1gMnSc1	Ólafur
Ragnar	Ragnar	k1gMnSc1	Ragnar
Grímsson	Grímsson	k1gMnSc1	Grímsson
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
vládl	vládnout	k5eAaImAgInS	vládnout
od	od	k7c2	od
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
do	do	k7c2	do
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
Gudni	Gudeň	k1gFnSc3	Gudeň
Jóhannesson	Jóhannesson	k1gMnSc1	Jóhannesson
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
vládne	vládnout	k5eAaImIp3nS	vládnout
od	od	k7c2	od
1.	[number]	k4	1.
srpna	srpen	k1gInSc2	srpen
2016.	[number]	k4	2016.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Topografie	topografie	k1gFnSc2	topografie
===	===	k?	===
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
103 125	[number]	k4	103 125
km2	km2	k4	km2
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
Skotska	Skotsko	k1gNnSc2	Skotsko
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
kolem	kolem	k7c2	kolem
800	[number]	k4	800
km	km	kA	km
<g/>
,	,	kIx,	,
od	od	k7c2	od
Norska	Norsko	k1gNnSc2	Norsko
970	[number]	k4	970
km	km	kA	km
<g/>
,	,	kIx,	,
od	od	k7c2	od
Grónska	Grónsko	k1gNnSc2	Grónsko
287	[number]	k4	287
km	km	kA	km
a	a	k8xC	a
od	od	k7c2	od
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
420	[number]	k4	420
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
ostrov	ostrov	k1gInSc1	ostrov
Grímsey	Grímsea	k1gFnSc2	Grímsea
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
skutečně	skutečně	k6eAd1	skutečně
protnut	protnut	k2eAgInSc1d1	protnut
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
střed	střed	k1gInSc1	střed
a	a	k8xC	a
východ	východ	k1gInSc1	východ
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
dost	dost	k6eAd1	dost
velké	velký	k2eAgFnSc6d1	velká
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severnímu	severní	k2eAgNnSc3d1	severní
pobřeží	pobřeží	k1gNnSc3	pobřeží
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
řekami	řeka	k1gFnPc7	řeka
oddělené	oddělený	k2eAgInPc4d1	oddělený
hřbety	hřbet	k1gInPc4	hřbet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Hvannadalshnjúkur	Hvannadalshnjúkur	k1gMnSc1	Hvannadalshnjúkur
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ledovce	ledovec	k1gInSc2	ledovec
Vatnajökull	Vatnajökull	k1gInSc1	Vatnajökull
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
sopky	sopka	k1gFnSc2	sopka
Herð	Herð	k1gFnSc2	Herð
(	(	kIx(	(
<g/>
1682	[number]	k4	1682
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Askja	Askja	k1gMnSc1	Askja
(	(	kIx(	(
<g/>
1510	[number]	k4	1510
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středoatlantského	Středoatlantský	k2eAgInSc2d1	Středoatlantský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
,	,	kIx,	,
místě	místo	k1gNnSc6	místo
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nové	nový	k2eAgFnSc2d1	nová
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
následnému	následný	k2eAgNnSc3d1	následné
rozpínání	rozpínání	k1gNnSc3	rozpínání
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vyjma	vyjma	k7c2	vyjma
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
Islandem	Island	k1gInSc7	Island
nachází	nacházet	k5eAaImIp3nS	nacházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
horká	horký	k2eAgFnSc1d1	horká
skvrna	skvrna	k1gFnSc1	skvrna
způsobující	způsobující	k2eAgFnSc4d1	způsobující
enormní	enormní	k2eAgFnSc4d1	enormní
produkci	produkce	k1gFnSc4	produkce
lávy	láva	k1gFnSc2	láva
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
unikátního	unikátní	k2eAgNnSc2d1	unikátní
podloží	podloží	k1gNnSc2	podloží
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
riftové	riftový	k2eAgFnSc2d1	riftový
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
velmi	velmi	k6eAd1	velmi
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
sérií	série	k1gFnSc7	série
sopečný	sopečný	k2eAgInSc1d1	sopečný
erupcí	erupce	k1gFnPc2	erupce
ze	z	k7c2	z
Středoatlanstského	Středoatlanstský	k2eAgInSc2d1	Středoatlanstský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
muselo	muset	k5eAaImAgNnS	muset
byt	byt	k1gInSc4	byt
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
erupci	erupce	k1gFnSc4	erupce
vulkánu	vulkán	k1gInSc2	vulkán
Helgafell	Helgafella	k1gFnPc2	Helgafella
evakuováno	evakuován	k2eAgNnSc1d1	evakuováno
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
ostrova	ostrov	k1gInSc2	ostrov
Heimaney	Heimanea	k1gFnSc2	Heimanea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
poloostrova	poloostrov	k1gInSc2	poloostrov
Melrakkaslétta	Melrakkaslétt	k1gInSc2	Melrakkaslétt
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mění	měnit	k5eAaImIp3nS	měnit
směr	směr	k1gInSc4	směr
na	na	k7c4	na
jihozápadní	jihozápadní	k2eAgNnSc4d1	jihozápadní
a	a	k8xC	a
rozdvojuje	rozdvojovat	k5eAaImIp3nS	rozdvojovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Sopečný	sopečný	k2eAgInSc1d1	sopečný
materiál	materiál	k1gInSc1	materiál
budující	budující	k2eAgInSc1d1	budující
vlastně	vlastně	k9	vlastně
celý	celý	k2eAgInSc1d1	celý
Island	Island	k1gInSc1	Island
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Geologicky	geologicky	k6eAd1	geologicky
nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
starší	starší	k1gMnSc1	starší
bazaltové	bazaltový	k2eAgNnSc1d1	bazaltové
plató	plató	k1gNnSc1	plató
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
třetihorních	třetihorní	k2eAgInPc2d1	třetihorní
čedičů	čedič	k1gInPc2	čedič
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
až	až	k9	až
16	[number]	k4	16
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
formace	formace	k1gFnSc1	formace
buduje	budovat	k5eAaImIp3nS	budovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
zejména	zejména	k9	zejména
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
severozápadě	severozápad	k1gInSc6	severozápad
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgNnSc1d2	mladší
bazaltové	bazaltový	k2eAgNnSc1d1	bazaltové
pásmo	pásmo	k1gNnSc1	pásmo
je	být	k5eAaImIp3nS	být
šedě	šedě	k6eAd1	šedě
zbarvené	zbarvený	k2eAgNnSc1d1	zbarvené
a	a	k8xC	a
lemuje	lemovat	k5eAaImIp3nS	lemovat
v	v	k7c6	v
úzkých	úzký	k2eAgInPc6d1	úzký
pruzích	pruh	k1gInPc6	pruh
současnou	současný	k2eAgFnSc4d1	současná
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Prokazatelné	prokazatelný	k2eAgNnSc1d1	prokazatelné
stáří	stáří	k1gNnSc1	stáří
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
0,2	[number]	k4	0,2
až	až	k8xS	až
3	[number]	k4	3
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Plošiny	plošina	k1gFnPc1	plošina
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
převážně	převážně	k6eAd1	převážně
čedičem	čedič	k1gInSc7	čedič
a	a	k8xC	a
doleritem	dolerit	k1gInSc7	dolerit
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
horniny	hornina	k1gFnPc1	hornina
kyselejší	kyselý	k2eAgFnPc1d2	kyselejší
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ryolity	ryolit	k2eAgFnPc1d1	ryolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
mladá	mladý	k2eAgFnSc1d1	mladá
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
zóna	zóna	k1gFnSc1	zóna
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
horninami	hornina	k1gFnPc7	hornina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
stáří	stáří	k1gNnSc1	stáří
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0,7	[number]	k4	0,7
miliónu	milión	k4xCgInSc2	milión
let	léto	k1gNnPc2	léto
až	až	k6eAd1	až
po	po	k7c6	po
nedávné	dávný	k2eNgFnSc6d1	nedávná
výlevy	výlev	k1gInPc7	výlev
čedičů	čedič	k1gInPc2	čedič
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejaktivnějších	aktivní	k2eAgFnPc2d3	nejaktivnější
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2 000	[number]	k4	2 000
erupcemi	erupce	k1gFnPc7	erupce
v	v	k7c6	v
poledové	poledový	k2eAgFnSc6d1	poledová
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
za	za	k7c4	za
10 000	[number]	k4	10 000
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgInPc1d1	unikátní
jsou	být	k5eAaImIp3nP	být
tvary	tvar	k1gInPc1	tvar
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
pod	pod	k7c7	pod
ledovcovými	ledovcový	k2eAgInPc7d1	ledovcový
výbuchy	výbuch	k1gInPc7	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Láva	láva	k1gFnSc1	láva
se	se	k3xPyFc4	se
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
ledem	led	k1gInSc7	led
nebo	nebo	k8xC	nebo
tavnou	tavný	k2eAgFnSc7d1	tavná
vodou	voda	k1gFnSc7	voda
rychle	rychle	k6eAd1	rychle
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
a	a	k8xC	a
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
přitom	přitom	k6eAd1	přitom
pronikne	proniknout	k5eAaPmIp3nS	proniknout
ledovcovou	ledovcový	k2eAgFnSc7d1	ledovcová
hmotou	hmota	k1gFnSc7	hmota
nebo	nebo	k8xC	nebo
hladinou	hladina	k1gFnSc7	hladina
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
útvar	útvar	k1gInSc1	útvar
podobný	podobný	k2eAgInSc1d1	podobný
stolové	stolový	k2eAgFnSc3d1	stolová
hoře	hora	k1gFnSc3	hora
(	(	kIx(	(
<g/>
v	v	k7c6	v
islandštině	islandština	k1gFnSc6	islandština
stapi	stap	k1gFnSc2	stap
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgInPc2d3	nejmladší
ostrovů	ostrov	k1gInPc2	ostrov
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
Surtsey	Surtsea	k1gMnSc2	Surtsea
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2,7	[number]	k4	2,7
km2	km2	k4	km2
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
jen	jen	k9	jen
1,4	[number]	k4	1,4
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
centra	centrum	k1gNnSc2	centrum
tlakové	tlakový	k2eAgFnSc2d1	tlaková
níže	níže	k1gFnSc2	níže
a	a	k8xC	a
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
severní	severní	k2eAgFnSc4d1	severní
polohu	poloha	k1gFnSc4	poloha
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
teplým	teplý	k2eAgFnPc3d1	teplá
vodám	voda	k1gFnPc3	voda
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
mírné	mírný	k2eAgNnSc1d1	mírné
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
</s>
<s>
Míšení	míšení	k1gNnSc1	míšení
teplých	teplý	k2eAgFnPc2d1	teplá
a	a	k8xC	a
chladných	chladný	k2eAgFnPc2d1	chladná
mořských	mořský	k2eAgFnPc2d1	mořská
vod	voda	k1gFnPc2	voda
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
vydatných	vydatný	k2eAgInPc2d1	vydatný
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
klesá	klesat	k5eAaImIp3nS	klesat
přibližně	přibližně	k6eAd1	přibližně
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
1 000	[number]	k4	1 000
až	až	k9	až
3 000	[number]	k4	3 000
mm	mm	kA	mm
podle	podle	k7c2	podle
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
východě	východ	k1gInSc6	východ
pouze	pouze	k6eAd1	pouze
300	[number]	k4	300
-	-	kIx~	-
500	[number]	k4	500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
proměnlivost	proměnlivost	k1gFnSc1	proměnlivost
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
počasí	počasí	k1gNnSc4	počasí
celého	celý	k2eAgInSc2d1	celý
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
mírná	mírný	k2eAgFnSc1d1	mírná
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
od	od	k7c2	od
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
-	-	kIx~	-
<g/>
1	[number]	k4	1
°	°	k?	°
<g/>
C.	C.	kA	C.
Letní	letní	k2eAgFnSc2d1	letní
teploty	teplota	k1gFnSc2	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
15	[number]	k4	15
i	i	k9	i
více	hodně	k6eAd2	hodně
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
ne	ne	k9	ne
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
polohách	poloha	k1gFnPc6	poloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ledovce	ledovec	k1gInPc1	ledovec
===	===	k?	===
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
jazyce	jazyk	k1gInSc6	jazyk
"	"	kIx"	"
<g/>
ledová	ledový	k2eAgFnSc1d1	ledová
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
nese	nést	k5eAaImIp3nS	nést
stopy	stopa	k1gFnPc4	stopa
po	po	k7c6	po
činnosti	činnost	k1gFnSc6	činnost
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
velká	velký	k2eAgNnPc4d1	velké
ledovcová	ledovcový	k2eAgNnPc4d1	ledovcové
údolí	údolí	k1gNnPc4	údolí
s	s	k7c7	s
čedičovými	čedičový	k2eAgFnPc7d1	čedičová
vrstvami	vrstva	k1gFnPc7	vrstva
vyhlazenými	vyhlazený	k2eAgFnPc7d1	vyhlazená
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
ledové	ledový	k2eAgFnSc2d1	ledová
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
rozbrázděnými	rozbrázděný	k2eAgInPc7d1	rozbrázděný
úlomky	úlomek	k1gInPc7	úlomek
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
uvízl	uvíznout	k5eAaPmAgMnS	uvíznout
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
ledovcové	ledovcový	k2eAgFnSc6d1	ledovcová
aktivitě	aktivita	k1gFnSc6	aktivita
i	i	k9	i
množství	množství	k1gNnSc1	množství
morén	moréna	k1gFnPc2	moréna
a	a	k8xC	a
obrovské	obrovský	k2eAgFnSc2d1	obrovská
výplavové	výplavový	k2eAgFnSc2d1	výplavový
plošiny	plošina	k1gFnSc2	plošina
–	–	k?	–
sandry	sandr	k1gInPc4	sandr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
prodělal	prodělat	k5eAaPmAgMnS	prodělat
ostrov	ostrov	k1gInSc4	ostrov
klimatické	klimatický	k2eAgFnSc2d1	klimatická
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
postup	postup	k1gInSc1	postup
a	a	k8xC	a
ústup	ústup	k1gInSc1	ústup
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celek	k1gInSc7	celek
ledovce	ledovec	k1gInSc2	ledovec
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
11	[number]	k4	11
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
ledovcová	ledovcový	k2eAgFnSc1d1	ledovcová
oblast	oblast	k1gFnSc1	oblast
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Vatnajökull	Vatnajökull	k1gInSc4	Vatnajökull
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
měří	měřit	k5eAaImIp3nS	měřit
8 100	[number]	k4	8 100
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Vatnajökull	Vatnajökull	k1gInSc1	Vatnajökull
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
ledovcová	ledovcový	k2eAgFnSc1d1	ledovcová
čapka	čapka	k1gFnSc1	čapka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
ledových	ledový	k2eAgNnPc2d1	ledové
těles	těleso	k1gNnPc2	těleso
umístěných	umístěný	k2eAgNnPc2d1	umístěné
na	na	k7c6	na
podledovcových	podledovcův	k2eAgInPc6d1	podledovcův
sopečných	sopečný	k2eAgInPc6d1	sopečný
vrcholech	vrchol	k1gInPc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Sopky	sopka	k1gFnPc1	sopka
překryté	překrytý	k2eAgFnPc1d1	překrytá
ledovou	ledový	k2eAgFnSc7d1	ledová
hmotou	hmota	k1gFnSc7	hmota
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
aktivní	aktivní	k2eAgFnPc1d1	aktivní
(	(	kIx(	(
<g/>
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
r.	r.	kA	r.
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
katastrofické	katastrofický	k2eAgFnPc4d1	katastrofická
povodně	povodeň	k1gFnPc4	povodeň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
valily	valit	k5eAaImAgInP	valit
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Jökulsá	Jökulsý	k2eAgFnSc1d1	Jökulsá
á	á	k0	á
Fjöllum	Fjöllum	k1gInSc4	Fjöllum
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
unikátní	unikátní	k2eAgInPc1d1	unikátní
tvary	tvar	k1gInPc1	tvar
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
2 750	[number]	k4	2 750
km2	km2	k4	km2
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
2,67	[number]	k4	2,67
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
pramení	pramenit	k5eAaImIp3nS	pramenit
největší	veliký	k2eAgFnSc2d3	veliký
zdejší	zdejší	k2eAgFnSc2d1	zdejší
řeky	řeka	k1gFnSc2	řeka
napájené	napájený	k2eAgInPc4d1	napájený
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnPc4	on
Thjórsá	Thjórsý	k2eAgNnPc4d1	Thjórsý
<g/>
,	,	kIx,	,
Hvítá	Hvítý	k2eAgNnPc1d1	Hvítý
a	a	k8xC	a
nejvodnější	vodný	k2eAgNnPc1d3	vodný
Jökulsá	Jökulsý	k2eAgNnPc1d1	Jökulsý
á	á	k0	á
Fjöllum	Fjöllum	k1gNnSc4	Fjöllum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
mladých	mladý	k2eAgInPc2d1	mladý
tektonických	tektonický	k2eAgInPc2d1	tektonický
pohybů	pohyb	k1gInPc2	pohyb
se	se	k3xPyFc4	se
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
množstvím	množství	k1gNnSc7	množství
vodopádů	vodopád	k1gInPc2	vodopád
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
puklinách	puklina	k1gFnPc6	puklina
a	a	k8xC	a
zlomech	zlom	k1gInPc6	zlom
(	(	kIx(	(
<g/>
Dettifoss	Dettifoss	k1gInSc1	Dettifoss
<g/>
,	,	kIx,	,
Skógafoss	Skógafoss	k1gInSc1	Skógafoss
<g/>
,	,	kIx,	,
Gullfoss	Gullfoss	k1gInSc1	Gullfoss
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
rysem	rys	k1gInSc7	rys
krajiny	krajina	k1gFnSc2	krajina
je	být	k5eAaImIp3nS	být
také	také	k9	také
množství	množství	k1gNnSc1	množství
jezer	jezero	k1gNnPc2	jezero
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
činností	činnost	k1gFnPc2	činnost
ledovců	ledovec	k1gInPc2	ledovec
(	(	kIx(	(
<g/>
jezero	jezero	k1gNnSc1	jezero
Lagarfljót	Lagarfljóta	k1gFnPc2	Lagarfljóta
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tektonických	tektonický	k2eAgInPc2d1	tektonický
poklesů	pokles	k1gInPc2	pokles
(	(	kIx(	(
<g/>
jezero	jezero	k1gNnSc1	jezero
Thingvallavatn	Thingvallavatna	k1gFnPc2	Thingvallavatna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
jezer	jezero	k1gNnPc2	jezero
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
kráterech	kráter	k1gInPc6	kráter
sopek	sopka	k1gFnPc2	sopka
(	(	kIx(	(
<g/>
maar	maar	k1gMnSc1	maar
Vítí	Vítí	k1gMnSc1	Vítí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
hrazena	hradit	k5eAaImNgFnS	hradit
lávovými	lávový	k2eAgInPc7d1	lávový
proudy	proud	k1gInPc7	proud
(	(	kIx(	(
<g/>
jezero	jezero	k1gNnSc1	jezero
Mývatn	Mývatna	k1gFnPc2	Mývatna
<g/>
)	)	kIx)	)
či	či	k8xC	či
ledovcovými	ledovcový	k2eAgFnPc7d1	ledovcová
morénami	moréna	k1gFnPc7	moréna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Termální	termální	k2eAgInPc1d1	termální
prameny	pramen	k1gInPc1	pramen
a	a	k8xC	a
gejzíry	gejzír	k1gInPc1	gejzír
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
ostrovem	ostrov	k1gInSc7	ostrov
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
totiž	totiž	k9	totiž
výhradně	výhradně	k6eAd1	výhradně
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
chrlí	chrlit	k5eAaImIp3nP	chrlit
žhavou	žhavý	k2eAgFnSc4d1	žhavá
lávu	láva	k1gFnSc4	láva
a	a	k8xC	a
horký	horký	k2eAgInSc4d1	horký
popel	popel	k1gInSc4	popel
<g/>
.	.	kIx.	.
</s>
<s>
Islanďané	Islanďan	k1gMnPc1	Islanďan
sopky	sopka	k1gFnSc2	sopka
využívají	využívat	k5eAaImIp3nP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Vrtají	vrtat	k5eAaImIp3nP	vrtat
do	do	k7c2	do
Země	zem	k1gFnSc2	zem
díry	díra	k1gFnSc2	díra
a	a	k8xC	a
prohánějí	prohánět	k5eAaImIp3nP	prohánět
jimi	on	k3xPp3gNnPc7	on
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
vytápět	vytápět	k5eAaImF	vytápět
obytné	obytný	k2eAgInPc4d1	obytný
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
termální	termální	k2eAgInPc4d1	termální
prameny	pramen	k1gInPc4	pramen
a	a	k8xC	a
gejzíry	gejzír	k1gInPc4	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
znakem	znak	k1gInSc7	znak
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
přítomnost	přítomnost	k1gFnSc1	přítomnost
bahenních	bahenní	k2eAgFnPc2d1	bahenní
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
sirovodíkem	sirovodík	k1gInSc7	sirovodík
páchnoucích	páchnoucí	k2eAgFnPc2d1	páchnoucí
solfatar	solfatara	k1gFnPc2	solfatara
a	a	k8xC	a
fumarol	fumarola	k1gFnPc2	fumarola
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgNnSc7d3	nejznámější
místem	místo	k1gNnSc7	místo
výskytu	výskyt	k1gInSc2	výskyt
těchto	tento	k3xDgInPc2	tento
úkazů	úkaz	k1gInPc2	úkaz
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
Námaskard	Námaskard	k1gMnSc1	Námaskard
(	(	kIx(	(
<g/>
Námafjall	Námafjall	k1gMnSc1	Námafjall
<g/>
)	)	kIx)	)
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Mývatn	Mývatna	k1gFnPc2	Mývatna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
gejzírů	gejzír	k1gInPc2	gejzír
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Haukadalur	Haukadalura	k1gFnPc2	Haukadalura
<g/>
,	,	kIx,	,
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
místního	místní	k2eAgInSc2d1	místní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Velkého	velký	k2eAgInSc2d1	velký
gejzíru	gejzír	k1gInSc2	gejzír
(	(	kIx(	(
<g/>
Stóri	Stór	k1gFnSc2	Stór
gejzír	gejzír	k1gInSc1	gejzír
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
ono	onen	k3xDgNnSc1	onen
mezinárodně	mezinárodně	k6eAd1	mezinárodně
užívané	užívaný	k2eAgNnSc1d1	užívané
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
tento	tento	k3xDgMnSc1	tento
otec	otec	k1gMnSc1	otec
gejzírů	gejzír	k1gInPc2	gejzír
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
chrlil	chrlit	k5eAaImAgMnS	chrlit
vodu	voda	k1gFnSc4	voda
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
70	[number]	k4	70
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nečinný	činný	k2eNgMnSc1d1	nečinný
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc7d1	hlavní
atrakcí	atrakce	k1gFnSc7	atrakce
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
menší	malý	k2eAgFnSc1d2	menší
obdoba	obdoba	k1gFnSc1	obdoba
Strokkur	Strokkura	k1gFnPc2	Strokkura
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
chrlící	chrlící	k2eAgFnSc4d1	chrlící
vodu	voda	k1gFnSc4	voda
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
20	[number]	k4	20
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
setkáme	setkat	k5eAaPmIp1nP	setkat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
trpasličí	trpasličí	k2eAgFnPc1d1	trpasličí
vrby	vrba	k1gFnPc1	vrba
<g/>
,	,	kIx,	,
zakrslé	zakrslý	k2eAgFnPc1d1	zakrslá
břízy	bříza	k1gFnPc1	bříza
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
tundrovou	tundrový	k2eAgFnSc4d1	tundrová
vegetaci	vegetace	k1gFnSc4	vegetace
a	a	k8xC	a
jalovce	jalovec	k1gInPc4	jalovec
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
tu	tu	k6eAd1	tu
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
brusnicovitých	brusnicovitý	k2eAgFnPc2d1	brusnicovitý
a	a	k8xC	a
šíchovitých	šíchovitý	k2eAgFnPc2d1	šíchovitý
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Hojné	hojný	k2eAgInPc1d1	hojný
jsou	být	k5eAaImIp3nP	být
lišejníky	lišejník	k1gInPc1	lišejník
a	a	k8xC	a
ostřicové	ostřicový	k2eAgFnPc1d1	ostřicová
trávy	tráva	k1gFnPc1	tráva
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
bažiny	bažina	k1gFnPc4	bažina
a	a	k8xC	a
mokřiny	mokřina	k1gFnPc4	mokřina
<g/>
.	.	kIx.	.
</s>
<s>
Nápadnějšími	nápadní	k2eAgInPc7d2	nápadní
květy	květ	k1gInPc7	květ
upoutají	upoutat	k5eAaPmIp3nP	upoutat
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
létě	léto	k1gNnSc6	léto
např.	např.	kA	např.
kakost	kakost	k1gInSc1	kakost
<g/>
,	,	kIx,	,
endemitní	endemitní	k2eAgInSc1d1	endemitní
mák	mák	k1gInSc1	mák
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
turan	turan	k1gInSc1	turan
<g/>
,	,	kIx,	,
smetánka	smetánka	k1gFnSc1	smetánka
či	či	k8xC	či
zvonek	zvonek	k1gInSc1	zvonek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nízkých	nízký	k2eAgFnPc2d1	nízká
orchidejí	orchidea	k1gFnPc2	orchidea
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
Platanthera	Platanthero	k1gNnPc4	Platanthero
hyperborea	hyperbore	k2eAgNnPc4d1	hyperbore
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
tu	tu	k6eAd1	tu
také	také	k9	také
protěž	protěž	k1gFnSc1	protěž
alpská	alpský	k2eAgFnSc1d1	alpská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotvárná	jednotvárný	k2eAgFnSc1d1	jednotvárná
vegetace	vegetace	k1gFnSc1	vegetace
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
dostatek	dostatek	k1gInSc1	dostatek
potravy	potrava	k1gFnSc2	potrava
zvěři	zvěř	k1gFnSc3	zvěř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
druhově	druhově	k6eAd1	druhově
velmi	velmi	k6eAd1	velmi
chudá	chudý	k2eAgFnSc1d1	chudá
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
tu	tu	k6eAd1	tu
liška	liška	k1gFnSc1	liška
polární	polární	k2eAgFnSc1d1	polární
<g/>
,	,	kIx,	,
na	na	k7c6	na
krách	kra	k1gFnPc6	kra
sem	sem	k6eAd1	sem
občas	občas	k6eAd1	občas
zabloudí	zabloudit	k5eAaPmIp3nS	zabloudit
medvěd	medvěd	k1gMnSc1	medvěd
lední	lední	k2eAgMnSc1d1	lední
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
faunou	fauna	k1gFnSc7	fauna
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
také	také	k6eAd1	také
ovce	ovce	k1gFnSc1	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Kolonie	kolonie	k1gFnSc1	kolonie
alek	alka	k1gFnPc2	alka
<g/>
,	,	kIx,	,
chaluh	chaluha	k1gFnPc2	chaluha
a	a	k8xC	a
racků	racek	k1gMnPc2	racek
v	v	k7c6	v
pobřežních	pobřežní	k2eAgInPc6d1	pobřežní
fjordech	fjord	k1gInPc6	fjord
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vyhledávaným	vyhledávaný	k2eAgFnPc3d1	vyhledávaná
lokalitám	lokalita	k1gFnPc3	lokalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
žije	žít	k5eAaImIp3nS	žít
na	na	k7c4	na
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
papuchalků	papuchalka	k1gMnPc2	papuchalka
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
zbarveným	zbarvený	k2eAgInSc7d1	zbarvený
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
černobílým	černobílý	k2eAgNnSc7d1	černobílé
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Obratně	obratně	k6eAd1	obratně
se	se	k3xPyFc4	se
potápějí	potápět	k5eAaImIp3nP	potápět
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
loví	lovit	k5eAaImIp3nP	lovit
rybky	rybka	k1gFnPc4	rybka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
3	[number]	k4	3
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Snæ	Snæ	k1gFnSc2	Snæ
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc2d1	národní
park	park	k1gInSc4	park
Þ	Þ	k?	Þ
a	a	k8xC	a
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Vatnajökull	Vatnajökulla	k1gFnPc2	Vatnajökulla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
79	[number]	k4	79
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
sveitarfélög	sveitarfélög	k1gInSc1	sveitarfélög
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
23	[number]	k4	23
hrabství	hrabství	k1gNnSc2	hrabství
(	(	kIx(	(
<g/>
isl	isl	k?	isl
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
sýslur	sýslur	k1gMnSc1	sýslur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
8	[number]	k4	8
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
hluti	hlut	k5eAaImF	hlut
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
6	[number]	k4	6
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
žilo	žít	k5eAaImAgNnS	žít
ke	k	k7c3	k
dni	den	k1gInSc3	den
1.	[number]	k4	1.
lednu	leden	k1gInSc6	leden
2018 348 450	[number]	k4	2018 348 450
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
3,22	[number]	k4	3,22
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
60	[number]	k4	60
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Islandu	Island	k1gInSc2	Island
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Reykjavíku	Reykjavík	k1gInSc2	Reykjavík
<g/>
.	.	kIx.	.
92	[number]	k4	92
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
8	[number]	k4	8
%	%	kIx~	%
na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
žebříčku	žebříček	k1gInSc2	žebříček
Rozvojového	rozvojový	k2eAgInSc2d1	rozvojový
programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
Island	Island	k1gInSc1	Island
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
život	život	k1gInSc4	život
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
177	[number]	k4	177
hodnocených	hodnocený	k2eAgFnPc2d1	hodnocená
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
jsou	být	k5eAaImIp3nP	být
islandština	islandština	k1gFnSc1	islandština
a	a	k8xC	a
od	od	k7c2	od
2.	[number]	k4	2.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
také	také	k6eAd1	také
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
<g/>
.	.	kIx.	.
</s>
<s>
Islandština	islandština	k1gFnSc1	islandština
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc4d1	severogermánský
jazyk	jazyk	k1gInSc4	jazyk
pocházející	pocházející	k2eAgInSc4d1	pocházející
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
.	.	kIx.	.
</s>
<s>
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
schválen	schválit	k5eAaPmNgInS	schválit
jako	jako	k8xC	jako
oficiální	oficiální	k2eAgInSc1d1	oficiální
státní	státní	k2eAgInSc1d1	státní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Islanďané	Islanďan	k1gMnPc1	Islanďan
jsou	být	k5eAaImIp3nP	být
hrdí	hrdý	k2eAgMnPc1d1	hrdý
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
vikinské	vikinský	k2eAgFnPc1d1	vikinská
kořeny	kořen	k2eAgFnPc1d1	Kořena
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
vysledovat	vysledovat	k5eAaPmF	vysledovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
až	až	k9	až
k	k	k7c3	k
prvním	první	k4xOgMnPc3	první
osadníkům	osadník	k1gMnPc3	osadník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
zapsána	zapsat	k5eAaPmNgNnP	zapsat
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
osadníků	osadník	k1gMnPc2	osadník
(	(	kIx(	(
<g/>
Landnámabók	Landnámabók	k1gMnSc1	Landnámabók
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
současná	současný	k2eAgFnSc1d1	současná
islandština	islandština	k1gFnSc1	islandština
vychází	vycházet	k5eAaImIp3nS	vycházet
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
jazyk	jazyk	k1gInSc4	jazyk
Vikingů	Viking	k1gMnPc2	Viking
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
středověké	středověký	k2eAgFnPc1d1	středověká
vikinské	vikinský	k2eAgFnPc1d1	vikinská
ságy	sága	k1gFnPc1	sága
snadno	snadno	k6eAd1	snadno
číst	číst	k5eAaImF	číst
i	i	k9	i
dnešní	dnešní	k2eAgMnPc1d1	dnešní
Islanďané	Islanďan	k1gMnPc1	Islanďan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
nemají	mít	k5eNaImIp3nP	mít
klasická	klasický	k2eAgNnPc1d1	klasické
jména	jméno	k1gNnPc1	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc1	příjmení
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
řazeny	řazen	k2eAgInPc4d1	řazen
i	i	k8xC	i
telefonní	telefonní	k2eAgInPc4d1	telefonní
seznamy	seznam	k1gInPc4	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
dostává	dostávat	k5eAaImIp3nS	dostávat
místo	místo	k7c2	místo
příjmení	příjmení	k1gNnSc2	příjmení
jméno	jméno	k1gNnSc1	jméno
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
s	s	k7c7	s
příponou	přípona	k1gFnSc7	přípona
-	-	kIx~	-
<g/>
dóttir	dóttir	k1gInSc1	dóttir
(	(	kIx(	(
<g/>
dcera	dcera	k1gFnSc1	dcera
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
-	-	kIx~	-
<g/>
son	son	k1gInSc1	son
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Ólafur	Ólafur	k1gMnSc1	Ólafur
Ragnar	Ragnar	k1gMnSc1	Ragnar
Grímsson	Grímsson	k1gMnSc1	Grímsson
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Ólaf	Ólaf	k1gMnSc1	Ólaf
Ragnar	Ragnar	k1gMnSc1	Ragnar
<g/>
,	,	kIx,	,
Grímův	Grímův	k2eAgMnSc1d1	Grímův
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dvě	dva	k4xCgNnPc4	dva
křestní	křestní	k2eAgNnPc4d1	křestní
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Vezměme	vzít	k5eAaPmRp1nP	vzít
si	se	k3xPyFc3	se
příklad	příklad	k1gInSc4	příklad
<g/>
:	:	kIx,	:
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Anna	Anna	k1gFnSc1	Anna
Kristín	Kristín	k1gMnSc1	Kristín
Guð	Guð	k1gMnSc1	Guð
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Kristján	Kristján	k1gInSc1	Kristján
Atli	Atl	k1gFnSc2	Atl
Hilmarsson	Hilmarssona	k1gFnPc2	Hilmarssona
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
nepřebírá	přebírat	k5eNaImIp3nS	přebírat
příjmení	příjmení	k1gNnSc1	příjmení
po	po	k7c6	po
manželovi	manžel	k1gMnSc6	manžel
jako	jako	k8xS	jako
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
přebírají	přebírat	k5eAaImIp3nP	přebírat
příjmení	příjmení	k1gNnSc2	příjmení
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
křestního	křestní	k2eAgNnSc2d1	křestní
jména	jméno	k1gNnSc2	jméno
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Čili	čili	k1gNnSc1	čili
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaImF	jmenovat
např.	např.	kA	např.
Ásta	Ásta	k1gFnSc1	Ásta
Björg	Björg	k1gMnSc1	Björg
Kristjánsdóttir	Kristjánsdóttir	k1gMnSc1	Kristjánsdóttir
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Svavar	Svavar	k1gInSc1	Svavar
Þ	Þ	k?	Þ
Kristjánsson	Kristjánsson	k1gInSc1	Kristjánsson
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přechází	přecházet	k5eAaImIp3nS	přecházet
příjmení	příjmení	k1gNnSc4	příjmení
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
na	na	k7c4	na
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
takových	takový	k3xDgFnPc2	takový
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
ho	on	k3xPp3gInSc4	on
většinou	většinou	k6eAd1	většinou
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
předkové	předek	k1gMnPc1	předek
byli	být	k5eAaImAgMnP	být
kdysi	kdysi	k6eAd1	kdysi
z	z	k7c2	z
bohatého	bohatý	k2eAgInSc2d1	bohatý
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
"	"	kIx"	"
<g/>
normálních	normální	k2eAgInPc2d1	normální
<g/>
"	"	kIx"	"
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc7d1	státní
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
luteránská	luteránský	k2eAgFnSc1d1	luteránská
Islandská	islandský	k2eAgFnSc1d1	islandská
státní	státní	k2eAgFnSc1d1	státní
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
rozdělení	rozdělení	k1gNnSc1	rozdělení
islandské	islandský	k2eAgFnSc2d1	islandská
společnosti	společnost	k1gFnSc2	společnost
podle	podle	k7c2	podle
náboženské	náboženský	k2eAgFnSc2d1	náboženská
příslušnosti	příslušnost	k1gFnSc2	příslušnost
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Islandská	islandský	k2eAgFnSc1d1	islandská
státní	státní	k2eAgFnSc1d1	státní
církev	církev	k1gFnSc1	církev
73,8	[number]	k4	73,8
%	%	kIx~	%
</s>
</p>
<p>
<s>
jiní	jiný	k2eAgMnPc1d1	jiný
křesťané	křesťan	k1gMnPc1	křesťan
11,7	[number]	k4	11,7
%	%	kIx~	%
</s>
</p>
<p>
<s>
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
3,6	[number]	k4	3,6
%	%	kIx~	%
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
</s>
</p>
<p>
<s>
jiní	jiný	k2eAgMnPc1d1	jiný
a	a	k8xC	a
nespecifikovaní	specifikovaný	k2eNgMnPc1d1	nespecifikovaný
věřící	věřící	k1gMnPc1	věřící
7,1	[number]	k4	7,1
%	%	kIx~	%
</s>
</p>
<p>
<s>
nehlásící	hlásící	k2eNgFnSc1d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
víře	víra	k1gFnSc3	víra
5,6	[number]	k4	5,6
%	%	kIx~	%
</s>
</p>
<p>
<s>
nekřesťanská	křesťanský	k2eNgNnPc4d1	nekřesťanské
náboženství	náboženství	k1gNnPc4	náboženství
1,5	[number]	k4	1,5
%	%	kIx~	%
</s>
</p>
<p>
<s>
členové	člen	k1gMnPc1	člen
Islandské	islandský	k2eAgFnSc2d1	islandská
etické	etický	k2eAgFnSc2d1	etická
humanistické	humanistický	k2eAgFnSc2d1	humanistická
společnosti	společnost	k1gFnSc2	společnost
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vykonal	vykonat	k5eAaPmAgInS	vykonat
Gallupův	Gallupův	k2eAgInSc1d1	Gallupův
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
57	[number]	k4	57
%	%	kIx~	%
Islanďanů	Islanďan	k1gMnPc2	Islanďan
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
náboženské	náboženský	k2eAgFnPc4d1	náboženská
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
31	[number]	k4	31
%	%	kIx~	%
za	za	k7c4	za
nenáboženské	náboženský	k2eNgFnPc4d1	nenáboženská
osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
za	za	k7c4	za
přesvědčené	přesvědčený	k2eAgMnPc4d1	přesvědčený
ateisty	ateista	k1gMnPc4	ateista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
7	[number]	k4	7
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
podle	podle	k7c2	podle
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Reykjavík	Reykjavík	k1gInSc1	Reykjavík
117 607	[number]	k4	117 607
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kópavogur	Kópavogur	k1gMnSc1	Kópavogur
27 421	[number]	k4	27 421
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hafnarfjörð	Hafnarfjörð	k?	Hafnarfjörð
24 712	[number]	k4	24 712
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Akureyri	Akureyri	k6eAd1	Akureyri
17 158	[number]	k4	17 158
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Keflavík	Keflavík	k1gInSc1	Keflavík
10 200	[number]	k4	10 200
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Garð	Garð	k?	Garð
9 877	[number]	k4	9 877
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Mosfellsbæ	Mosfellsbæ	k?	Mosfellsbæ
7 808	[number]	k4	7 808
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Dle	dle	k7c2	dle
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
má	mít	k5eAaImIp3nS	mít
Island	Island	k1gInSc1	Island
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
39 168	[number]	k4	39 168
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Island	Island	k1gInSc1	Island
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
stal	stát	k5eAaPmAgMnS	stát
státem	stát	k1gInSc7	stát
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
HDI	HDI	kA	HDI
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krize	krize	k1gFnSc2	krize
finančního	finanční	k2eAgInSc2d1	finanční
sektoru	sektor	k1gInSc2	sektor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
si	se	k3xPyFc3	se
Island	Island	k1gInSc1	Island
vysloužil	vysloužit	k5eAaPmAgInS	vysloužit
1.	[number]	k4	1.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
bank	banka	k1gFnPc2	banka
držených	držený	k2eAgFnPc2d1	držená
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
stát	stát	k1gInSc1	stát
přebral	přebrat	k5eAaPmAgInS	přebrat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
třemi	tři	k4xCgFnPc7	tři
největšími	veliký	k2eAgFnPc7d3	veliký
bankami	banka	k1gFnPc7	banka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
vážných	vážný	k2eAgInPc2d1	vážný
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
však	však	k9	však
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
enormní	enormní	k2eAgFnSc4d1	enormní
zátěž	zátěž	k1gFnSc4	zátěž
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
nachází	nacházet	k5eAaImIp3nS	nacházet
krok	krok	k1gInSc4	krok
před	před	k7c7	před
státním	státní	k2eAgInSc7d1	státní
bankrotem	bankrot	k1gInSc7	bankrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Finanční	finanční	k2eAgFnSc1d1	finanční
krize	krize	k1gFnSc1	krize
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Bankovní	bankovní	k2eAgInSc1d1	bankovní
systém	systém	k1gInSc1	systém
v	v	k7c6	v
Islandu	Island	k1gInSc6	Island
prošel	projít	k5eAaPmAgInS	projít
velmi	velmi	k6eAd1	velmi
překotným	překotný	k2eAgInSc7d1	překotný
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
zdejší	zdejší	k2eAgInSc4d1	zdejší
nejen	nejen	k6eAd1	nejen
bankovní	bankovní	k2eAgInSc4d1	bankovní
trh	trh	k1gInSc4	trh
mohutně	mohutně	k6eAd1	mohutně
sílil	sílit	k5eAaImAgMnS	sílit
a	a	k8xC	a
expandoval	expandovat	k5eAaImAgMnS	expandovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přineslo	přinést	k5eAaPmAgNnS	přinést
růst	růst	k1gInSc4	růst
celému	celý	k2eAgInSc3d1	celý
finančnímu	finanční	k2eAgInSc3d1	finanční
sektoru	sektor	k1gInSc3	sektor
a	a	k8xC	a
Island	Island	k1gInSc1	Island
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
rychle	rychle	k6eAd1	rychle
rozvíjejíce	rozvíjet	k5eAaImSgFnP	rozvíjet
se	se	k3xPyFc4	se
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInSc1d1	finanční
sektor	sektor	k1gInSc1	sektor
rostl	růst	k5eAaImAgInS	růst
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
před	před	k7c7	před
krizí	krize	k1gFnSc7	krize
dokonce	dokonce	k9	dokonce
devětkrát	devětkrát	k6eAd1	devětkrát
převýšil	převýšit	k5eAaPmAgInS	převýšit
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
globální	globální	k2eAgFnSc2d1	globální
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
islandské	islandský	k2eAgInPc1d1	islandský
banky	bank	k1gInPc1	bank
rázem	rázem	k6eAd1	rázem
propadly	propadnout	k5eAaPmAgInP	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
tři	tři	k4xCgFnPc1	tři
největší	veliký	k2eAgFnPc1d3	veliký
banky	banka	k1gFnPc1	banka
zestátněny	zestátněn	k2eAgFnPc1d1	zestátněna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
islandskou	islandský	k2eAgFnSc4d1	islandská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rychle	rychle	k6eAd1	rychle
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svoji	svůj	k3xOyFgFnSc4	svůj
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Dopad	dopad	k1gInSc1	dopad
této	tento	k3xDgFnSc2	tento
krize	krize	k1gFnSc2	krize
pocítili	pocítit	k5eAaPmAgMnP	pocítit
i	i	k9	i
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tento	tento	k3xDgInSc4	tento
ostrov	ostrov	k1gInSc4	ostrov
daleko	daleko	k6eAd1	daleko
levnější	levný	k2eAgFnSc7d2	levnější
destinací	destinace	k1gFnSc7	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Političtí	politický	k2eAgMnPc1d1	politický
představitelé	představitel	k1gMnPc1	představitel
Islandu	Island	k1gInSc2	Island
pak	pak	k6eAd1	pak
jednali	jednat	k5eAaImAgMnP	jednat
o	o	k7c6	o
půjčce	půjčka	k1gFnSc6	půjčka
(	(	kIx(	(
<g/>
neúspěšně	úspěšně	k6eNd1	úspěšně
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
také	také	k9	také
znovu	znovu	k6eAd1	znovu
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
diskuzi	diskuze	k1gFnSc3	diskuze
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
Island	Island	k1gInSc1	Island
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Islanďané	Islanďan	k1gMnPc1	Islanďan
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
do	do	k7c2	do
unie	unie	k1gFnSc2	unie
nechtěli	chtít	k5eNaImAgMnP	chtít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
byly	být	k5eAaImAgFnP	být
rybolovné	rybolovný	k2eAgFnPc1d1	rybolovná
kvóty	kvóta	k1gFnPc1	kvóta
<g/>
,	,	kIx,	,
rozdělované	rozdělovaný	k2eAgFnPc1d1	rozdělovaná
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
mezi	mezi	k7c4	mezi
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
bez	bez	k7c2	bez
výrazných	výrazný	k2eAgInPc2d1	výrazný
surovinových	surovinový	k2eAgInPc2d1	surovinový
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
hydroenergetickými	hydroenergetický	k2eAgFnPc7d1	hydroenergetická
a	a	k8xC	a
geotermálními	geotermální	k2eAgFnPc7d1	geotermální
zásobami	zásoba	k1gFnPc7	zásoba
<g/>
,	,	kIx,	,
využívanými	využívaný	k2eAgFnPc7d1	využívaná
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
k	k	k7c3	k
vytápění	vytápění	k1gNnSc3	vytápění
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
geotermální	geotermální	k2eAgFnSc1d1	geotermální
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
až	až	k9	až
z	z	k7c2	z
85	[number]	k4	85
%	%	kIx~	%
na	na	k7c6	na
vyhřívání	vyhřívání	k1gNnSc6	vyhřívání
islandských	islandský	k2eAgInPc2d1	islandský
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
prioritou	priorita	k1gFnSc7	priorita
islandského	islandský	k2eAgNnSc2d1	islandské
hospodářství	hospodářství	k1gNnSc2	hospodářství
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgInSc4d1	námořní
rybolov	rybolov	k1gInSc4	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
zejména	zejména	k9	zejména
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
rybích	rybí	k2eAgFnPc2d1	rybí
konzerv	konzerva	k1gFnPc2	konzerva
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
hutnictví	hutnictví	k1gNnSc1	hutnictví
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rybolov	rybolov	k1gInSc1	rybolov
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
75	[number]	k4	75
%	%	kIx~	%
vývozu	vývoz	k1gInSc2	vývoz
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
celých	celý	k2eAgNnPc2d1	celé
12	[number]	k4	12
%	%	kIx~	%
práceschopné	práceschopný	k2eAgFnSc2d1	práceschopná
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Rybolov	rybolov	k1gInSc1	rybolov
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
bouřlivého	bouřlivý	k2eAgInSc2d1	bouřlivý
rozkvětu	rozkvět	k1gInSc2	rozkvět
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
rodinného	rodinný	k2eAgInSc2d1	rodinný
rybaření	rybařený	k2eAgMnPc1d1	rybařený
stal	stát	k5eAaPmAgInS	stát
celý	celý	k2eAgInSc4d1	celý
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
rybolov	rybolov	k1gInSc4	rybolov
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc4	zpracování
a	a	k8xC	a
vývoz	vývoz	k1gInSc4	vývoz
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Islandská	islandský	k2eAgFnSc1d1	islandská
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
snaží	snažit	k5eAaImIp3nP	snažit
snížit	snížit	k5eAaPmF	snížit
závislosti	závislost	k1gFnSc2	závislost
ekonomiky	ekonomika	k1gFnSc2	ekonomika
(	(	kIx(	(
<g/>
70	[number]	k4	70
%	%	kIx~	%
financí	finance	k1gFnPc2	finance
z	z	k7c2	z
vývozu	vývoz	k1gInSc2	vývoz
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
rybolovu	rybolov	k1gInSc2	rybolov
a	a	k8xC	a
polovina	polovina	k1gFnSc1	polovina
hrubého	hrubý	k2eAgInSc2d1	hrubý
národního	národní	k2eAgInSc2d1	národní
důchodu	důchod	k1gInSc2	důchod
čerpá	čerpat	k5eAaImIp3nS	čerpat
finance	finance	k1gFnPc4	finance
také	také	k9	také
z	z	k7c2	z
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
)	)	kIx)	)
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
klesající	klesající	k2eAgFnSc1d1	klesající
cena	cena	k1gFnSc1	cena
rybích	rybí	k2eAgInPc2d1	rybí
produktů	produkt	k1gInPc2	produkt
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
rybích	rybí	k2eAgFnPc2d1	rybí
populací	populace	k1gFnPc2	populace
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
chov	chov	k1gInSc4	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
produkce	produkce	k1gFnSc1	produkce
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
brambor	brambora	k1gFnPc2	brambora
a	a	k8xC	a
zejména	zejména	k9	zejména
pícnin	pícnina	k1gFnPc2	pícnina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
%	%	kIx~	%
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
úkazem	úkaz	k1gInSc7	úkaz
je	být	k5eAaImIp3nS	být
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
úroda	úroda	k1gFnSc1	úroda
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
jižního	jižní	k2eAgNnSc2d1	jižní
ovoce	ovoce	k1gNnSc2	ovoce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
při	při	k7c6	při
polárním	polární	k2eAgInSc6d1	polární
kruhu	kruh	k1gInSc6	kruh
<g/>
,	,	kIx,	,
realizovaná	realizovaný	k2eAgFnSc1d1	realizovaná
ve	v	k7c6	v
velkokapacitních	velkokapacitní	k2eAgInPc6d1	velkokapacitní
sklenících	skleník	k1gInPc6	skleník
vytápěných	vytápěný	k2eAgInPc2d1	vytápěný
geotermální	geotermální	k2eAgFnSc7d1	geotermální
energií	energie	k1gFnSc7	energie
<g/>
;	;	kIx,	;
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
evropským	evropský	k2eAgMnSc7d1	evropský
producentem	producent	k1gMnSc7	producent
banánů	banán	k1gInPc2	banán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dovážejí	dovážet	k5eAaImIp3nP	dovážet
se	se	k3xPyFc4	se
paliva	palivo	k1gNnPc1	palivo
<g/>
,	,	kIx,	,
strojírenské	strojírenský	k2eAgInPc4d1	strojírenský
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Doprava	doprava	k1gFnSc1	doprava
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
je	být	k5eAaImIp3nS	být
řízena	řízen	k2eAgFnSc1d1	řízena
podle	podle	k7c2	podle
drsného	drsný	k2eAgInSc2d1	drsný
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
nepříznivými	příznivý	k2eNgFnPc7d1	nepříznivá
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
typ	typ	k1gInSc1	typ
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
auto	auto	k1gNnSc1	auto
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
silnice	silnice	k1gFnPc1	silnice
jsou	být	k5eAaImIp3nP	být
sjízdné	sjízdný	k2eAgFnPc1d1	sjízdná
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
zde	zde	k6eAd1	zde
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
a	a	k8xC	a
kromě	kromě	k7c2	kromě
měst	město	k1gNnPc2	město
ani	ani	k8xC	ani
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
<g/>
Aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
překonat	překonat	k5eAaPmF	překonat
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
taxíky-malá	taxíkyalý	k2eAgNnPc1d1	taxíky-malý
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
najmout	najmout	k5eAaPmF	najmout
i	i	k9	i
s	s	k7c7	s
pilotem	pilot	k1gMnSc7	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc1	dva
větší	veliký	k2eAgNnPc1d2	veliký
města	město	k1gNnPc1	město
Reykjavík	Reykjavík	k1gInSc1	Reykjavík
a	a	k8xC	a
Akureyri	Akureyri	k1gNnSc1	Akureyri
spojuje	spojovat	k5eAaImIp3nS	spojovat
hlavní	hlavní	k2eAgFnSc1d1	hlavní
okružní	okružní	k2eAgFnSc1d1	okružní
islandská	islandský	k2eAgFnSc1d1	islandská
silnice	silnice	k1gFnSc1	silnice
Hringvegur	Hringvegura	k1gFnPc2	Hringvegura
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
Islanďané	Islanďan	k1gMnPc1	Islanďan
dopravovat	dopravovat	k5eAaImF	dopravovat
mimo	mimo	k7c4	mimo
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
letadlo	letadlo	k1gNnSc4	letadlo
a	a	k8xC	a
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Island	Island	k1gInSc4	Island
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
cestovat	cestovat	k5eAaImF	cestovat
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
,	,	kIx,	,
především	především	k9	především
se	s	k7c7	s
společnostmi	společnost	k1gFnPc7	společnost
IcelandAir	IcelandAira	k1gFnPc2	IcelandAira
a	a	k8xC	a
nízkonákladovou	nízkonákladový	k2eAgFnSc4d1	nízkonákladová
Iceland	Icelanda	k1gFnPc2	Icelanda
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
levněji	levně	k6eAd2	levně
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
norského	norský	k2eAgInSc2d1	norský
Bergenu	Bergen	k1gInSc2	Bergen
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
potrvá	potrvat	k5eAaPmIp3nS	potrvat
2	[number]	k4	2
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
Islandu	Island	k1gInSc6	Island
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
nejsnáze	snadno	k6eAd3	snadno
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
opět	opět	k6eAd1	opět
letecky	letecky	k6eAd1	letecky
na	na	k7c6	na
vnitrostátních	vnitrostátní	k2eAgFnPc6d1	vnitrostátní
aerolinkách	aerolinka	k1gFnPc6	aerolinka
Air	Air	k1gFnSc2	Air
Iceland	Iceland	k1gInSc1	Iceland
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
svými	svůj	k3xOyFgFnPc7	svůj
cenami	cena	k1gFnPc7	cena
konkurují	konkurovat	k5eAaImIp3nP	konkurovat
dálkovým	dálkový	k2eAgInPc3d1	dálkový
autobusům	autobus	k1gInPc3	autobus
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
rychlejší	rychlý	k2eAgFnPc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
hlavní	hlavní	k2eAgFnSc2d1	hlavní
okružní	okružní	k2eAgFnSc2d1	okružní
silnice	silnice	k1gFnSc2	silnice
sjízdné	sjízdný	k2eAgFnSc2d1	sjízdná
jen	jen	k9	jen
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
pěší	pěší	k2eAgInPc1d1	pěší
přechody	přechod	k1gInPc1	přechod
jsou	být	k5eAaImIp3nP	být
dost	dost	k6eAd1	dost
náročné	náročný	k2eAgInPc1d1	náročný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
neexistovala	existovat	k5eNaImAgFnS	existovat
a	a	k8xC	a
neexistuje	existovat	k5eNaImIp3nS	existovat
veřejná	veřejný	k2eAgFnSc1d1	veřejná
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letech	let	k1gInPc6	let
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
koleje	kolej	k1gFnPc1	kolej
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
úzkorochodná	úzkorochodný	k2eAgFnSc1d1	úzkorochodný
o	o	k7c6	o
rozchodu	rozchod	k1gInSc6	rozchod
900	[number]	k4	900
mm	mm	kA	mm
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
pro	pro	k7c4	pro
návoz	návoz	k1gInSc4	návoz
a	a	k8xC	a
odvoz	odvoz	k1gInSc4	odvoz
zeminy	zemina	k1gFnSc2	zemina
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
existují	existovat	k5eAaImIp3nP	existovat
2	[number]	k4	2
parní	parní	k2eAgFnPc1d1	parní
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
sloužily	sloužit	k5eAaImAgFnP	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
lokomotivkou	lokomotivka	k1gFnSc7	lokomotivka
Jung	Jung	k1gMnSc1	Jung
engine	enginout	k5eAaPmIp3nS	enginout
company	compana	k1gFnPc4	compana
v	v	k7c6	v
Kirchenu	Kircheno	k1gNnSc6	Kircheno
<g/>
,	,	kIx,	,
jmenují	jmenovat	k5eAaImIp3nP	jmenovat
se	se	k3xPyFc4	se
Pioner	Pioner	k1gMnSc1	Pioner
(	(	kIx(	(
<g/>
vystaven	vystaven	k2eAgMnSc1d1	vystaven
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
historie	historie	k1gFnSc2	historie
Reykjavíku	Reykjavík	k1gInSc2	Reykjavík
<g/>
)	)	kIx)	)
a	a	k8xC	a
Minø	Minø	k1gMnSc1	Minø
(	(	kIx(	(
<g/>
na	na	k7c6	na
pomníku	pomník	k1gInSc6	pomník
u	u	k7c2	u
kulturního	kulturní	k2eAgNnSc2d1	kulturní
centra	centrum	k1gNnSc2	centrum
Harpa	Harpa	k1gFnSc1	Harpa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
vláda	vláda	k1gFnSc1	vláda
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
mezi	mezi	k7c7	mezi
Reykjavíkem	Reykjavík	k1gInSc7	Reykjavík
a	a	k8xC	a
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
letištěm	letiště	k1gNnSc7	letiště
v	v	k7c6	v
Keflavíku	Keflavík	k1gInSc6	Keflavík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
chtějí	chtít	k5eAaImIp3nP	chtít
radní	radní	k1gMnPc1	radní
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
vybudovat	vybudovat	k5eAaPmF	vybudovat
systém	systém	k1gInSc4	systém
městské	městský	k2eAgFnSc2d1	městská
kolejové	kolejový	k2eAgFnSc2d1	kolejová
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
německého	německý	k2eAgInSc2d1	německý
S-Bahn	S-Bahno	k1gNnPc2	S-Bahno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
vystavět	vystavět	k5eAaPmF	vystavět
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2040.	[number]	k4	2040.
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
má	mít	k5eAaImIp3nS	mít
13 034	[number]	k4	13 034
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4 617	[number]	k4	4 617
km	km	kA	km
zpevněných	zpevněný	k2eAgInPc2d1	zpevněný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
86	[number]	k4	86
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
12	[number]	k4	12
asfaltovaných	asfaltovaný	k2eAgFnPc2d1	asfaltovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Měna	měna	k1gFnSc1	měna
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
islandskou	islandský	k2eAgFnSc7d1	islandská
korunou	koruna	k1gFnSc7	koruna
(	(	kIx(	(
<g/>
isl	isl	k?	isl
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Íslensk	Íslensk	k1gInSc1	Íslensk
króna	krón	k1gInSc2	krón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
100	[number]	k4	100
aurar	aurara	k1gFnPc2	aurara
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
měny	měna	k1gFnSc2	měna
je	být	k5eAaImIp3nS	být
ISK	ISK	kA	ISK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Mince	mince	k1gFnSc1	mince
islandské	islandský	k2eAgFnSc2d1	islandská
koruny	koruna	k1gFnSc2	koruna
mají	mít	k5eAaImIp3nP	mít
nominální	nominální	k2eAgFnPc4d1	nominální
hodnoty	hodnota	k1gFnPc4	hodnota
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
50	[number]	k4	50
a	a	k8xC	a
100	[number]	k4	100
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
mají	mít	k5eAaImIp3nP	mít
nominální	nominální	k2eAgFnPc4d1	nominální
hodnoty	hodnota	k1gFnPc4	hodnota
500	[number]	k4	500
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
<g/>
,	,	kIx,	,
10000	[number]	k4	10000
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
bankovky	bankovka	k1gFnSc2	bankovka
v	v	k7c6	v
hodnotách	hodnota	k1gFnPc6	hodnota
10	[number]	k4	10
<g/>
,	,	kIx,	,
50	[number]	k4	50
a	a	k8xC	a
100	[number]	k4	100
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
stahují	stahovat	k5eAaImIp3nP	stahovat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
platit	platit	k5eAaImF	platit
přestanou	přestat	k5eAaPmIp3nP	přestat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
v	v	k7c6	v
mincích	mince	k1gFnPc6	mince
s	s	k7c7	s
nominální	nominální	k2eAgFnSc7d1	nominální
hodnotou	hodnota	k1gFnSc7	hodnota
1	[number]	k4	1
a	a	k8xC	a
5	[number]	k4	5
korun	koruna	k1gFnPc2	koruna
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
jejich	jejich	k3xOp3gFnSc1	jejich
peněžní	peněžní	k2eAgFnSc1d1	peněžní
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Islanďané	Islanďan	k1gMnPc1	Islanďan
poslali	poslat	k5eAaPmAgMnP	poslat
žádost	žádost	k1gFnSc4	žádost
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
15.	[number]	k4	15.
3.	[number]	k4	3.
2015	[number]	k4	2015
žádost	žádost	k1gFnSc4	žádost
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Pro	pro	k7c4	pro
islandskou	islandský	k2eAgFnSc4d1	islandská
literaturu	literatura	k1gFnSc4	literatura
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
ságy	sága	k1gFnPc1	sága
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
vznikaly	vznikat	k5eAaImAgInP	vznikat
již	již	k6eAd1	již
od	od	k7c2	od
12.	[number]	k4	12.
století	století	k1gNnSc2	století
tj.	tj.	kA	tj.
náboženské	náboženský	k2eAgFnPc1d1	náboženská
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
historické	historický	k2eAgFnPc1d1	historická
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnPc1d1	poslední
byly	být	k5eAaImAgFnP	být
napsané	napsaný	k2eAgFnPc1d1	napsaná
v	v	k7c4	v
14.	[number]	k4	14.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
se	se	k3xPyFc4	se
literatura	literatura	k1gFnSc1	literatura
odvíjela	odvíjet	k5eAaImAgFnS	odvíjet
od	od	k7c2	od
germánské	germánský	k2eAgFnSc2d1	germánská
a	a	k8xC	a
norské	norský	k2eAgFnSc2d1	norská
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
od	od	k7c2	od
zdejších	zdejší	k2eAgInPc2d1	zdejší
mýtů	mýtus	k1gInPc2	mýtus
a	a	k8xC	a
hrdinských	hrdinský	k2eAgFnPc2d1	hrdinská
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
středověkým	středověký	k2eAgMnSc7d1	středověký
islandským	islandský	k2eAgMnSc7d1	islandský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
a	a	k8xC	a
učencem	učenec	k1gMnSc7	učenec
byl	být	k5eAaImAgMnS	být
Snorri	Snorre	k1gFnSc4	Snorre
Sturluson	Sturluson	k1gMnSc1	Sturluson
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Prozaické	prozaický	k2eAgFnSc2d1	prozaická
Eddy	Edda	k1gFnSc2	Edda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
autora	autor	k1gMnSc2	autor
Poetické	poetický	k2eAgFnSc2d1	poetická
Eddy	Edda	k1gFnSc2	Edda
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
považován	považován	k2eAgMnSc1d1	považován
učenec	učenec	k1gMnSc1	učenec
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
slavné	slavný	k2eAgFnSc2d1	slavná
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Oddi	Odd	k1gFnSc6	Odd
Sæ	Sæ	k1gMnSc1	Sæ
Sigfússon	Sigfússon	k1gMnSc1	Sigfússon
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
vědci	vědec	k1gMnPc1	vědec
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
pravděpodobné	pravděpodobný	k2eAgNnSc4d1	pravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgMnSc7d1	klíčový
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
skaldské	skaldský	k2eAgFnSc2d1	Skaldská
poezie	poezie	k1gFnSc2	poezie
byl	být	k5eAaImAgMnS	být
Egill	Egill	k1gMnSc1	Egill
Skallagrímsson	Skallagrímsson	k1gMnSc1	Skallagrímsson
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
středověkým	středověký	k2eAgMnSc7d1	středověký
kronikářem	kronikář	k1gMnSc7	kronikář
byl	být	k5eAaImAgMnS	být
Ari	Ari	k1gMnSc1	Ari
Thorgilsson	Thorgilsson	k1gMnSc1	Thorgilsson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
16.	[number]	k4	16.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
dějiny	dějiny	k1gFnPc4	dějiny
a	a	k8xC	a
zeměpis	zeměpis	k1gInSc4	zeměpis
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
v	v	k7c6	v
17.	[number]	k4	17.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
vědeckému	vědecký	k2eAgInSc3d1	vědecký
oboru	obor	k1gInSc3	obor
-	-	kIx~	-
nordistice	nordistika	k1gFnSc3	nordistika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18.	[number]	k4	18.
století	století	k1gNnSc2	století
vznikají	vznikat	k5eAaImIp3nP	vznikat
první	první	k4xOgFnPc1	první
světské	světský	k2eAgFnPc1d1	světská
tiskárny	tiskárna	k1gFnPc1	tiskárna
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19.	[number]	k4	19.
století	století	k1gNnSc6	století
nastává	nastávat	k5eAaImIp3nS	nastávat
obrovský	obrovský	k2eAgInSc1d1	obrovský
rozvoj	rozvoj	k1gInSc1	rozvoj
poezie	poezie	k1gFnSc2	poezie
následován	následován	k2eAgInSc1d1	následován
počátky	počátek	k1gInPc7	počátek
umělecké	umělecký	k2eAgFnSc2d1	umělecká
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otcem	otec	k1gMnSc7	otec
moderní	moderní	k2eAgFnSc2d1	moderní
islandské	islandský	k2eAgFnSc2d1	islandská
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
byl	být	k5eAaImAgMnS	být
Árni	Árne	k1gFnSc4	Árne
Magnússon	Magnússon	k1gMnSc1	Magnússon
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
velkým	velký	k2eAgMnSc7d1	velký
moderním	moderní	k2eAgMnSc7d1	moderní
spisovatelem	spisovatel	k1gMnSc7	spisovatel
byl	být	k5eAaImAgMnS	být
Matthías	Matthías	k1gMnSc1	Matthías
Jochumsson	Jochumsson	k1gMnSc1	Jochumsson
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
autor	autor	k1gMnSc1	autor
textu	text	k1gInSc2	text
islandské	islandský	k2eAgFnSc2d1	islandská
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
nových	nový	k2eAgInPc2d1	nový
směrů	směr	k1gInPc2	směr
ale	ale	k8xC	ale
kladen	klást	k5eAaImNgInS	klást
je	být	k5eAaImIp3nS	být
důraz	důraz	k1gInSc1	důraz
i	i	k9	i
na	na	k7c4	na
svébytnost	svébytnost	k1gFnSc4	svébytnost
islandského	islandský	k2eAgInSc2d1	islandský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Experimentuje	experimentovat	k5eAaImIp3nS	experimentovat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c4	v
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
nastává	nastávat	k5eAaImIp3nS	nastávat
až	až	k9	až
v	v	k7c6	v
2.	[number]	k4	2.
polovině	polovina	k1gFnSc6	polovina
století	století	k1gNnSc1	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
získal	získat	k5eAaPmAgInS	získat
Halldór	Halldór	k1gInSc1	Halldór
Laxness	Laxness	k1gInSc1	Laxness
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
světový	světový	k2eAgInSc1d1	světový
ohlas	ohlas	k1gInSc1	ohlas
i	i	k9	i
spisovatel	spisovatel	k1gMnSc1	spisovatel
Gunnar	Gunnar	k1gMnSc1	Gunnar
Gunnarsson	Gunnarsson	k1gMnSc1	Gunnarsson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
nejznámější	známý	k2eAgMnSc1d3	nejznámější
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Sjón	Sjóno	k1gNnPc2	Sjóno
<g/>
,	,	kIx,	,
Hallgrímur	Hallgrímur	k1gMnSc1	Hallgrímur
Helgason	Helgason	k1gMnSc1	Helgason
a	a	k8xC	a
Matthías	Matthías	k1gMnSc1	Matthías
Jochumsson	Jochumsson	k1gMnSc1	Jochumsson
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rozmachu	rozmach	k1gInSc2	rozmach
severské	severský	k2eAgFnSc2d1	severská
detektivky	detektivka	k1gFnSc2	detektivka
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
např.	např.	kA	např.
Arnaldur	Arnaldura	k1gFnPc2	Arnaldura
Indrið	Indrið	k1gFnPc2	Indrið
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
konceptuální	konceptuální	k2eAgMnSc1d1	konceptuální
umělec	umělec	k1gMnSc1	umělec
Olafur	Olafur	k1gMnSc1	Olafur
Eliasson	Eliasson	k1gMnSc1	Eliasson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
má	mít	k5eAaImIp3nS	mít
význačný	význačný	k2eAgInSc1d1	význačný
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejpopulárnější	populární	k2eAgInSc1d3	nejpopulárnější
islandský	islandský	k2eAgInSc1d1	islandský
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
komedie	komedie	k1gFnSc2	komedie
101	[number]	k4	101
Reykjavík	Reykjavík	k1gMnSc1	Reykjavík
(	(	kIx(	(
<g/>
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
uváděný	uváděný	k2eAgInSc4d1	uváděný
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
S	s	k7c7	s
milenkou	milenka	k1gFnSc7	milenka
mé	můj	k3xOp1gFnSc2	můj
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
režírovaný	režírovaný	k2eAgInSc1d1	režírovaný
Baltasarem	Baltasar	k1gMnSc7	Baltasar
Kormákurem	Kormákur	k1gMnSc7	Kormákur
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc4	jeho
film	film	k1gInSc4	film
Severní	severní	k2eAgNnPc4d1	severní
blata	blata	k1gNnPc4	blata
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
předlohy	předloha	k1gFnSc2	předloha
Arnaldura	Arnaldur	k1gMnSc2	Arnaldur
Indrið	Indrið	k1gMnSc2	Indrið
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007.	[number]	k4	2007.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
televizní	televizní	k2eAgFnSc7d1	televizní
stanicí	stanice	k1gFnSc7	stanice
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
televize	televize	k1gFnSc1	televize
RÚV	RÚV	kA	RÚV
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
nejsledovanější	sledovaný	k2eAgInSc1d3	nejsledovanější
pořad	pořad	k1gInSc1	pořad
Sjónvarpið	Sjónvarpið	k1gFnPc1	Sjónvarpið
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
sport	sport	k1gInSc1	sport
a	a	k8xC	a
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
kanálem	kanál	k1gInSc7	kanál
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
televize	televize	k1gFnSc1	televize
Stöð	Stöð	k1gFnSc2	Stöð
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
SkjárEinn	SkjárEinn	k1gInSc4	SkjárEinn
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnSc1d1	ostatní
menší	malý	k2eAgFnSc1d2	menší
stanice	stanice	k1gFnSc1	stanice
jsou	být	k5eAaImIp3nP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
pouze	pouze	k6eAd1	pouze
lokálně	lokálně	k6eAd1	lokálně
<g/>
.	.	kIx.	.
</s>
<s>
Rádia	rádio	k1gNnPc1	rádio
jsou	být	k5eAaImIp3nP	být
vysílána	vysílán	k2eAgNnPc1d1	vysíláno
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Islandu	Island	k1gInSc6	Island
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgNnPc1d3	nejpopulárnější
rádia	rádio	k1gNnPc1	rádio
jsou	být	k5eAaImIp3nP	být
Rás	Rás	k1gFnSc7	Rás
1	[number]	k4	1
<g/>
,	,	kIx,	,
Rás	Rás	k1gFnSc1	Rás
2	[number]	k4	2
<g/>
,	,	kIx,	,
Bylgjan	Bylgjan	k1gInSc1	Bylgjan
a	a	k8xC	a
FM	FM	kA	FM
95,7	[number]	k4	95,7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
vycházejí	vycházet	k5eAaImIp3nP	vycházet
troje	troje	k4xRgFnPc4	troje
denní	denní	k2eAgFnPc4d1	denní
noviny	novina	k1gFnPc4	novina
<g/>
:	:	kIx,	:
Fréttablað	Fréttablað	k1gMnPc2	Fréttablað
<g/>
,	,	kIx,	,
Morgunblað	Morgunblað	k1gMnPc2	Morgunblað
a	a	k8xC	a
24	[number]	k4	24
stundir	stundira	k1gFnPc2	stundira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Islandská	islandský	k2eAgFnSc1d1	islandská
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
nordickou	nordický	k2eAgFnSc7d1	nordická
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
folkovou	folkový	k2eAgFnSc4d1	folková
a	a	k8xC	a
popovou	popový	k2eAgFnSc4d1	popová
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
islandské	islandský	k2eAgMnPc4d1	islandský
interprety	interpret	k1gMnPc4	interpret
patří	patřit	k5eAaImIp3nS	patřit
skupina	skupina	k1gFnSc1	skupina
Sigur	Sigur	k1gMnSc1	Sigur
Rós	Rós	k1gMnSc1	Rós
(	(	kIx(	(
<g/>
s	s	k7c7	s
frontmanem	frontman	k1gInSc7	frontman
Jón	Jón	k1gMnSc1	Jón
Þ	Þ	k?	Þ
Birgissonem	Birgisson	k1gInSc7	Birgisson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Björk	Björk	k1gInSc1	Björk
a	a	k8xC	a
Emilíana	Emilíana	k1gFnSc1	Emilíana
Torrini	Torrin	k1gMnPc1	Torrin
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
filmové	filmový	k2eAgFnSc2d1	filmová
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
skupiny	skupina	k1gFnPc1	skupina
Múm	Múm	k1gMnSc1	Múm
<g/>
,	,	kIx,	,
Of	Of	k1gMnSc1	Of
Monsters	Monstersa	k1gFnPc2	Monstersa
and	and	k?	and
Men	Men	k1gMnSc1	Men
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Sugarcubes	Sugarcubes	k1gMnSc1	Sugarcubes
<g/>
,	,	kIx,	,
Amiina	Amiina	k1gMnSc1	Amiina
<g/>
,	,	kIx,	,
Sólstafir	Sólstafir	k1gMnSc1	Sólstafir
<g/>
,	,	kIx,	,
GusGus	GusGus	k1gMnSc1	GusGus
<g/>
,	,	kIx,	,
Kaleo	Kaleo	k1gMnSc1	Kaleo
<g/>
,	,	kIx,	,
Skálmöld	Skálmöld	k1gMnSc1	Skálmöld
či	či	k8xC	či
FM	FM	kA	FM
Belfast	Belfast	k1gInSc1	Belfast
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc2	zpěvák
Páll	Pálla	k1gFnPc2	Pálla
Óskara	Óskar	k1gMnSc2	Óskar
Hjálmtýssona	Hjálmtýsson	k1gMnSc2	Hjálmtýsson
<g/>
.	.	kIx.	.
</s>
<s>
Originálním	originální	k2eAgMnSc7d1	originální
multiinstrumentalistou	multiinstrumentalista	k1gMnSc7	multiinstrumentalista
je	být	k5eAaImIp3nS	být
Ólafur	Ólafur	k1gMnSc1	Ólafur
Arnalds	Arnalds	k1gInSc1	Arnalds
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
lze	lze	k6eAd1	lze
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
dirigenta	dirigent	k1gMnSc4	dirigent
a	a	k8xC	a
klavíristu	klavírista	k1gMnSc4	klavírista
Vladimira	Vladimir	k1gMnSc4	Vladimir
Ashkenazyho	Ashkenazy	k1gMnSc4	Ashkenazy
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
mj.	mj.	kA	mj.
islandské	islandský	k2eAgNnSc1d1	islandské
občanství	občanství	k1gNnSc1	občanství
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vedl	vést	k5eAaImAgMnS	vést
i	i	k9	i
Českou	český	k2eAgFnSc4d1	Česká
filharmonii	filharmonie	k1gFnSc4	filharmonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc7d1	státní
hymnou	hymna	k1gFnSc7	hymna
Islandu	Island	k1gInSc2	Island
je	být	k5eAaImIp3nS	být
Lofsöngur	Lofsöngur	k1gMnSc1	Lofsöngur
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
je	být	k5eAaImIp3nS	být
také	také	k9	také
známa	znám	k2eAgFnSc1d1	známa
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ó	Ó	kA	Ó
Guð	Guð	k1gMnPc2	Guð
vors	vorsa	k1gFnPc2	vorsa
lands	lands	k6eAd1	lands
(	(	kIx(	(
<g/>
Bože	Boža	k1gFnSc3	Boža
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc4	slovo
napsal	napsat	k5eAaPmAgInS	napsat
Matthías	Matthías	k1gInSc1	Matthías
Jochumsson	Jochumssona	k1gFnPc2	Jochumssona
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Sveinbjörn	Sveinbjörn	k1gMnSc1	Sveinbjörn
Sveinbjörnsson	Sveinbjörnsson	k1gMnSc1	Sveinbjörnsson
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oslav	oslava	k1gFnPc2	oslava
tisíciletého	tisíciletý	k2eAgNnSc2d1	tisícileté
výročí	výročí	k1gNnSc3	výročí
osídlení	osídlení	k1gNnSc1	osídlení
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hymnu	hymna	k1gFnSc4	hymna
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
sloky	sloka	k1gFnPc4	sloka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
hymna	hymna	k1gFnSc1	hymna
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc1	první
sloka	sloka	k1gFnSc1	sloka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Školství	školství	k1gNnSc2	školství
===	===	k?	===
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
školství	školství	k1gNnSc2	školství
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Islandské	islandský	k2eAgNnSc1d1	islandské
školství	školství	k1gNnSc1	školství
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školky	školka	k1gFnSc2	školka
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
<g/>
.	.	kIx.	.
</s>
<s>
Povinné	povinný	k2eAgNnSc1d1	povinné
základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
16	[number]	k4	16
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
institucí	instituce	k1gFnPc2	instituce
požaduje	požadovat	k5eAaImIp3nS	požadovat
od	od	k7c2	od
státu	stát	k1gInSc2	stát
více	hodně	k6eAd2	hodně
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
islandštiny	islandština	k1gFnSc2	islandština
učí	učit	k5eAaImIp3nS	učit
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
dánsky	dánsky	k6eAd1	dánsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Islandská	islandský	k2eAgFnSc1d1	islandská
univerzita	univerzita	k1gFnSc1	univerzita
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Islandská	islandský	k2eAgFnSc1d1	islandská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
isl	isl	k?	isl
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Háskóli	Háskóle	k1gFnSc4	Háskóle
Íslands	Íslandsa	k1gFnPc2	Íslandsa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
islandská	islandský	k2eAgFnSc1d1	islandská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
jedenáct	jedenáct	k4xCc1	jedenáct
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
studuje	studovat	k5eAaImIp3nS	studovat
ji	on	k3xPp3gFnSc4	on
okolo	okolo	k6eAd1	okolo
8 000	[number]	k4	8 000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
pokrmem	pokrm	k1gInSc7	pokrm
islandské	islandský	k2eAgFnSc2d1	islandská
kuchyně	kuchyně	k1gFnSc2	kuchyně
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
jehněčí	jehněčí	k2eAgInPc1d1	jehněčí
a	a	k8xC	a
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
Þ	Þ	k?	Þ
(	(	kIx(	(
<g/>
Thorramatur	Thorramatura	k1gFnPc2	Thorramatura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgNnSc1d1	tradiční
islandské	islandský	k2eAgNnSc1d1	islandské
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Jí	jíst	k5eAaImIp3nS	jíst
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
Þ	Þ	k?	Þ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
leden	leden	k1gInSc4	leden
a	a	k8xC	a
únor	únor	k1gInSc4	únor
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgInPc7d1	tradiční
pokrmy	pokrm	k1gInPc7	pokrm
jsou	být	k5eAaImIp3nP	být
Skyr	Skyr	k1gMnSc1	Skyr
<g/>
,	,	kIx,	,
Scrota	Scrota	k1gFnSc1	Scrota
<g/>
,	,	kIx,	,
žralok	žralok	k1gMnSc1	žralok
a	a	k8xC	a
ovce	ovce	k1gFnSc1	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Skyr	Skyr	k1gInSc1	Skyr
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc4d1	tradiční
islandský	islandský	k2eAgInSc4d1	islandský
mléčný	mléčný	k2eAgInSc4d1	mléčný
výrobek	výrobek	k1gInSc4	výrobek
podobný	podobný	k2eAgInSc4d1	podobný
jogurtu	jogurt	k1gInSc3	jogurt
<g/>
.	.	kIx.	.
</s>
<s>
Skyr	Skyr	k1gInSc1	Skyr
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
12	[number]	k4	12
%	%	kIx~	%
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
3	[number]	k4	3
%	%	kIx~	%
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
0,5	[number]	k4	0,5
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
islandská	islandský	k2eAgFnSc1d1	islandská
strava	strava	k1gFnSc1	strava
velmi	velmi	k6eAd1	velmi
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kuchyně	kuchyně	k1gFnSc1	kuchyně
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
normální	normální	k2eAgFnSc2d1	normální
islandské	islandský	k2eAgFnSc2d1	islandská
snídaně	snídaně	k1gFnSc2	snídaně
patří	patřit	k5eAaImIp3nS	patřit
chléb	chléb	k1gInSc4	chléb
<g/>
,	,	kIx,	,
cereálie	cereálie	k1gFnPc4	cereálie
a	a	k8xC	a
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Večeře	večeře	k1gFnSc1	večeře
je	být	k5eAaImIp3nS	být
odlišnější	odlišný	k2eAgFnSc1d2	odlišnější
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
fast	fast	k2eAgInSc1d1	fast
food	food	k1gInSc1	food
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Sport	sport	k1gInSc1	sport
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyměnilo	vyměnit	k5eAaPmAgNnS	vyměnit
mnoho	mnoho	k4c1	mnoho
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
Islanďané	Islanďan	k1gMnPc1	Islanďan
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
velmi	velmi	k6eAd1	velmi
zdravý	zdravý	k2eAgInSc4d1	zdravý
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
nejpopulárnější	populární	k2eAgInSc1d3	nejpopulárnější
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
házená	házená	k1gFnSc1	házená
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc1	basketbal
a	a	k8xC	a
atletika	atletika	k1gFnSc1	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Golf	golf	k1gInSc1	golf
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
šachy	šach	k1gInPc1	šach
a	a	k8xC	a
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
populární	populární	k2eAgFnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
sport	sport	k1gInSc1	sport
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
je	být	k5eAaImIp3nS	být
Glíma	Glíma	k1gFnSc1	Glíma
<g/>
.	.	kIx.	.
</s>
<s>
Glíma	Glíma	k1gFnSc1	Glíma
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgInPc4d1	podobný
wrestlingu	wrestling	k1gInSc5	wrestling
<g/>
.	.	kIx.	.
</s>
<s>
Islanďané	Islanďan	k1gMnPc1	Islanďan
jsou	být	k5eAaImIp3nP	být
výborní	výborný	k2eAgMnPc1d1	výborný
na	na	k7c6	na
ledu	led	k1gInSc6	led
a	a	k8xC	a
šplhání	šplhání	k1gNnSc1	šplhání
po	po	k7c6	po
skalách	skála	k1gFnPc6	skála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Házená	házená	k1gFnSc1	házená
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xS	jako
národní	národní	k2eAgInSc4d1	národní
sport	sport	k1gInSc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Islandští	islandský	k2eAgMnPc1d1	islandský
házenkáři	házenkář	k1gMnPc1	házenkář
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
mužů	muž	k1gMnPc2	muž
získal	získat	k5eAaPmAgMnS	získat
stříbro	stříbro	k1gNnSc4	stříbro
na	na	k7c4	na
OH	OH	kA	OH
2008	[number]	k4	2008
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
na	na	k7c4	na
ME	ME	kA	ME
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Islanďanky	Islanďanka	k1gFnPc1	Islanďanka
jsou	být	k5eAaImIp3nP	být
překvapivě	překvapivě	k6eAd1	překvapivě
dobré	dobrý	k2eAgFnPc1d1	dobrá
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
i	i	k9	i
mužům	muž	k1gMnPc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
slavné	slavný	k2eAgNnSc1d1	slavné
tažení	tažení	k1gNnSc1	tažení
národního	národní	k2eAgInSc2d1	národní
týmu	tým	k1gInSc2	tým
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
celonárodním	celonárodní	k2eAgInSc7d1	celonárodní
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
islandským	islandský	k2eAgMnPc3d1	islandský
fotbalistům	fotbalista	k1gMnPc3	fotbalista
patří	patřit	k5eAaImIp3nP	patřit
Eið	Eið	k1gMnSc1	Eið
Guð	Guð	k1gMnSc1	Guð
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
šachistou	šachista	k1gMnSc7	šachista
byl	být	k5eAaImAgMnS	být
Frið	Frið	k1gMnSc1	Frið
Ólafsson	Ólafsson	k1gMnSc1	Ólafsson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
===	===	k?	===
</s>
</p>
<p>
<s>
1.	[number]	k4	1.
leden	leden	k1gInSc1	leden
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
</s>
</p>
<p>
<s>
6.	[number]	k4	6.
leden	leden	k1gInSc1	leden
-	-	kIx~	-
Epifanie	Epifanie	k1gFnSc1	Epifanie
</s>
</p>
<p>
<s>
19.	[number]	k4	19.
leden	leden	k1gInSc1	leden
-	-	kIx~	-
den	den	k1gInSc1	den
manželů	manžel	k1gMnPc2	manžel
</s>
</p>
<p>
<s>
18.	[number]	k4	18.
únor	únor	k1gInSc1	únor
-	-	kIx~	-
den	den	k1gInSc1	den
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
1.	[number]	k4	1.
květen	květen	k1gInSc4	květen
-	-	kIx~	-
Svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
</s>
</p>
<p>
<s>
13.	[number]	k4	13.
květen	květen	k1gInSc4	květen
-	-	kIx~	-
den	den	k1gInSc4	den
matek	matka	k1gFnPc2	matka
</s>
</p>
<p>
<s>
3.	[number]	k4	3.
červen	červen	k1gInSc1	červen
-	-	kIx~	-
den	den	k1gInSc1	den
námořníků	námořník	k1gMnPc2	námořník
</s>
</p>
<p>
<s>
17.	[number]	k4	17.
červen	červen	k1gInSc1	červen
-	-	kIx~	-
den	den	k1gInSc1	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
</s>
</p>
<p>
<s>
1.	[number]	k4	1.
srpen	srpen	k1gInSc1	srpen
-	-	kIx~	-
první	první	k4xOgNnPc4	první
pondělí	pondělí	k1gNnPc4	pondělí
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
-	-	kIx~	-
Den	den	k1gInSc1	den
řemeslníků	řemeslník	k1gMnPc2	řemeslník
</s>
</p>
<p>
<s>
16.	[number]	k4	16.
listopad	listopad	k1gInSc1	listopad
-	-	kIx~	-
den	den	k1gInSc1	den
islandštiny	islandština	k1gFnSc2	islandština
</s>
</p>
<p>
<s>
1.	[number]	k4	1.
prosinec	prosinec	k1gInSc1	prosinec
-	-	kIx~	-
vznik	vznik	k1gInSc1	vznik
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
24.	[number]	k4	24.
až	až	k8xS	až
26.	[number]	k4	26.
prosinec	prosinec	k1gInSc1	prosinec
-	-	kIx~	-
Vánoce	Vánoce	k1gFnPc1	Vánoce
</s>
</p>
<p>
<s>
31.	[number]	k4	31.
prosinec	prosinec	k1gInSc1	prosinec
-	-	kIx~	-
Silvestrovské	silvestrovský	k2eAgFnPc1d1	silvestrovská
oslavy	oslava	k1gFnPc1	oslava
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KADEČKOVÁ	KADEČKOVÁ	kA	KADEČKOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7106-343-8	[number]	k4	978-80-7106-343-8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Island	Island	k1gInSc1	Island
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Island	Island	k1gInSc1	Island
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
-	-	kIx~	-
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
zemi	zem	k1gFnSc6	zem
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
-	-	kIx~	-
ledová	ledový	k2eAgFnSc1d1	ledová
tvář	tvář	k1gFnSc1	tvář
s	s	k7c7	s
ohnivým	ohnivý	k2eAgNnSc7d1	ohnivé
srdcem	srdce	k1gNnSc7	srdce
text	text	k1gInSc1	text
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Iceland	Iceland	k1gInSc1	Iceland
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.S.	U.S.	k?	U.S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-03-21	[number]	k4	2011-03-21
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Iceland	Iceland	k1gInSc1	Iceland
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-07	[number]	k4	2011-07-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Oslo	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Island	Island	k1gInSc1	Island
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo.cz	Businessinfo.cz	k1gMnSc1	Businessinfo.cz
<g/>
,	,	kIx,	,
2010-10-01	[number]	k4	2010-10-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011-08-12	[number]	k4	2011-08-12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KARLSSON	KARLSSON	kA	KARLSSON
<g/>
,	,	kIx,	,
Gunnar	Gunnar	k1gInSc1	Gunnar
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Iceland	Iceland	k1gInSc1	Iceland
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
