<s>
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
islandsky	islandsky	k6eAd1	islandsky
Ísland	Ísland	k1gInSc1	Ísland
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
a	a	k8xC	a
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
350 000	[number]	k4	350 000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
103 125	[number]	k4	103 125
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
nejřidčeji	řídce	k6eAd3	řídce
zalidněný	zalidněný	k2eAgInSc1d1	zalidněný
evropský	evropský	k2eAgInSc1d1	evropský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Reykjavík	Reykjavík	k1gInSc1	Reykjavík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
