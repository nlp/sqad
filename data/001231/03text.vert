<s>
Richard	Richard	k1gMnSc1	Richard
Milhous	Milhous	k1gMnSc1	Milhous
Nixon	Nixon	k1gMnSc1	Nixon
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1913	[number]	k4	1913
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
37	[number]	k4	37
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
36	[number]	k4	36
<g/>
.	.	kIx.	.
viceprezident	viceprezident	k1gMnSc1	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
prezident	prezident	k1gMnSc1	prezident
Dwight	Dwight	k1gMnSc1	Dwight
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
.	.	kIx.	.
</s>
<s>
Nixon	Nixon	k1gMnSc1	Nixon
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zvolen	zvolit	k5eAaPmNgMnS	zvolit
americkým	americký	k2eAgMnSc7d1	americký
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
i	i	k8xC	i
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
o	o	k7c4	o
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
ucházel	ucházet	k5eAaImAgMnS	ucházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
těsně	těsně	k6eAd1	těsně
jej	on	k3xPp3gMnSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
mladým	mladý	k2eAgInSc7d1	mladý
televizním	televizní	k2eAgInSc7d1	televizní
a	a	k8xC	a
divadelním	divadelní	k2eAgMnSc7d1	divadelní
producentem	producent	k1gMnSc7	producent
Rogerem	Roger	k1gMnSc7	Roger
Ailesem	Ailes	k1gMnSc7	Ailes
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
mediálního	mediální	k2eAgMnSc2d1	mediální
expertana	expertan	k1gMnSc2	expertan
jeho	jeho	k3xOp3gFnPc4	jeho
volební	volební	k2eAgFnSc1d1	volební
kampaň	kampaň	k1gFnSc1	kampaň
<g/>
,	,	kIx,	,
kouče	kouč	k1gMnSc2	kouč
vystupování	vystupování	k1gNnSc4	vystupování
před	před	k7c7	před
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
šéfa	šéf	k1gMnSc4	šéf
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nixon	Nixon	k1gInSc1	Nixon
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
porazil	porazit	k5eAaPmAgMnS	porazit
kandidáta	kandidát	k1gMnSc4	kandidát
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
Huberta	Hubert	k1gMnSc2	Hubert
Humphreye	Humphrey	k1gMnSc2	Humphrey
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
drtivě	drtivě	k6eAd1	drtivě
porazil	porazit	k5eAaPmAgInS	porazit
George	George	k1gInSc1	George
McGoverna	McGoverna	k1gFnSc1	McGoverna
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Nixonovým	Nixonův	k2eAgMnSc7d1	Nixonův
osobním	osobní	k2eAgMnSc7d1	osobní
dvorním	dvorní	k2eAgMnSc7d1	dvorní
fotografem	fotograf	k1gMnSc7	fotograf
byl	být	k5eAaImAgMnS	být
Oliver	Oliver	k1gMnSc1	Oliver
F.	F.	kA	F.
Atkins	Atkins	k1gInSc1	Atkins
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnPc2	jeho
prezidentství	prezidentství	k1gNnPc2	prezidentství
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
normalizaci	normalizace	k1gFnSc3	normalizace
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
rovněž	rovněž	k9	rovněž
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c4	o
snížení	snížení	k1gNnSc4	snížení
počtu	počet	k1gInSc2	počet
strategických	strategický	k2eAgFnPc2d1	strategická
atomových	atomový	k2eAgFnPc2d1	atomová
zbraní	zbraň	k1gFnPc2	zbraň
SALT	salto	k1gNnPc2	salto
I.	I.	kA	I.
Zahájil	zahájit	k5eAaPmAgInS	zahájit
také	také	k9	také
stahování	stahování	k1gNnSc4	stahování
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
počátek	počátek	k1gInSc4	počátek
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vietnamizace	vietnamizace	k1gFnSc2	vietnamizace
<g/>
"	"	kIx"	"
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
bombardování	bombardování	k1gNnSc4	bombardování
i	i	k8xC	i
na	na	k7c4	na
sousední	sousední	k2eAgInPc4d1	sousední
státy	stát	k1gInPc4	stát
Kambodžu	Kambodža	k1gFnSc4	Kambodža
a	a	k8xC	a
Laos	Laos	k1gInSc4	Laos
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
krok	krok	k1gInSc1	krok
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
jeho	jeho	k3xOp3gNnSc1	jeho
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
opustit	opustit	k5eAaPmF	opustit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zavedený	zavedený	k2eAgInSc4d1	zavedený
Brettonwoodský	Brettonwoodský	k2eAgInSc4d1	Brettonwoodský
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
měnový	měnový	k2eAgInSc4d1	měnový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
směnitelnost	směnitelnost	k1gFnSc1	směnitelnost
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
za	za	k7c4	za
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Henryho	Henry	k1gMnSc4	Henry
Kissingera	Kissinger	k1gMnSc4	Kissinger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
válkou	válka	k1gFnSc7	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
použít	použít	k5eAaPmF	použít
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
na	na	k7c4	na
severovietnamské	severovietnamský	k2eAgMnPc4d1	severovietnamský
vojáky	voják	k1gMnPc4	voják
i	i	k8xC	i
civilisty	civilista	k1gMnPc4	civilista
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Kissinger	Kissinger	k1gInSc1	Kissinger
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
rozmluvil	rozmluvit	k5eAaPmAgMnS	rozmluvit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aféra	aféra	k1gFnSc1	aféra
Watergate	Watergat	k1gInSc5	Watergat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
politice	politika	k1gFnSc6	politika
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
působení	působení	k1gNnSc1	působení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
aférou	aféra	k1gFnSc7	aféra
Watergate	Watergat	k1gInSc5	Watergat
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
policie	policie	k1gFnSc1	policie
zadržela	zadržet	k5eAaPmAgFnS	zadržet
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
Watergate	Watergat	k1gMnSc5	Watergat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlila	sídlit	k5eAaImAgFnS	sídlit
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
pět	pět	k4xCc1	pět
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tam	tam	k6eAd1	tam
instalovali	instalovat	k5eAaBmAgMnP	instalovat
odposlouchávací	odposlouchávací	k2eAgNnSc4d1	odposlouchávací
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
popíral	popírat	k5eAaImAgInS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
s	s	k7c7	s
vloupáním	vloupání	k1gNnSc7	vloupání
měl	mít	k5eAaImAgMnS	mít
cokoliv	cokoliv	k3yInSc1	cokoliv
společného	společný	k2eAgNnSc2d1	společné
až	až	k9	až
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
aféře	aféra	k1gFnSc6	aféra
Watergate	Watergat	k1gInSc5	Watergat
věděl	vědět	k5eAaImAgMnS	vědět
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
a	a	k8xC	a
úmyslně	úmyslně	k6eAd1	úmyslně
lhal	lhát	k5eAaImAgMnS	lhát
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1974	[number]	k4	1974
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
místo	místo	k6eAd1	místo
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
Gerald	Gerald	k1gInSc1	Gerald
Ford	ford	k1gInSc1	ford
<g/>
.	.	kIx.	.
</s>
<s>
Nixon	Nixon	k1gMnSc1	Nixon
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
aféry	aféra	k1gFnSc2	aféra
Watergate	Watergat	k1gMnSc5	Watergat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
poté	poté	k6eAd1	poté
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
viceprezident	viceprezident	k1gMnSc1	viceprezident
Gerald	Geralda	k1gFnPc2	Geralda
Ford	ford	k1gInSc1	ford
<g/>
.	.	kIx.	.
</s>
<s>
Nixon	Nixon	k1gMnSc1	Nixon
zemřel	zemřít	k5eAaPmAgMnS	zemřít
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
na	na	k7c4	na
následky	následek	k1gInPc4	následek
mozkové	mozkový	k2eAgFnSc2d1	mozková
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Pinkpop	Pinkpop	k1gInSc4	Pinkpop
Festivalu	festival	k1gInSc2	festival
poprvé	poprvé	k6eAd1	poprvé
zahrála	zahrát	k5eAaPmAgFnS	zahrát
skupina	skupina	k1gFnSc1	skupina
Rage	Rage	k1gFnSc1	Rage
Against	Against	k1gFnSc1	Against
the	the	k?	the
Machine	Machin	k1gInSc5	Machin
píseň	píseň	k1gFnSc4	píseň
Tire	Tir	k1gMnSc4	Tir
Me	Me	k1gFnSc4	Me
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
Nixonovu	Nixonův	k2eAgFnSc4d1	Nixonova
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gNnSc4	Nixon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc2	galerie
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
Marek	Marek	k1gMnSc1	Marek
Konopko	konopka	k1gFnSc5	konopka
<g/>
:	:	kIx,	:
Nixonův	Nixonův	k2eAgInSc4d1	Nixonův
levý	levý	k2eAgInSc4d1	levý
profil	profil	k1gInSc4	profil
Roman	Roman	k1gMnSc1	Roman
Joch	Joch	k?	Joch
<g/>
:	:	kIx,	:
Američtí	americký	k2eAgMnPc1d1	americký
státníci	státník	k1gMnPc1	státník
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
</s>
