<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(
<g/>
finsky	finsky	k6eAd1
Suomen	Suomen	k2eAgInSc1d1	Suomen
tasavallan	tasavallan	k1gInSc1	tasavallan
presidentti	presidentť	k1gFnSc2	presidentť
<g/>
;	;	kIx,
švédsky	švédsky	k6eAd1
Republiken	Republiken	k2eAgInSc1d1	Republiken
Finlands	Finlands	k1gInSc1	Finlands
president	president	k1gMnSc1	president
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hlavou	hlava	k1gFnSc7	hlava
Finska	Finsko	k1gNnSc2	Finsko
od	od	k7c2
roku	rok	k1gInSc2	rok
1919	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2	rok
1988	#num#	k4
byl	být	k5eAaImAgInS
volen	volit	k5eAaImNgInS
Parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
přešlo	přejít	k5eAaPmAgNnS
na	na	k7c4
přímou	přímý	k2eAgFnSc4d1	přímá
dvoukolovou	dvoukolový	k2eAgFnSc4d1	dvoukolová
volbu	volba	k1gFnSc4	volba
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4
prezidenta	prezident	k1gMnSc4	prezident
může	moct	k5eAaImIp3nS
navrhnout	navrhnout	k5eAaPmF
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
posledních	poslední	k2eAgFnPc6d1	poslední
volbách	volba	k1gFnPc6	volba
získala	získat	k5eAaPmAgFnS
alespoň	alespoň	k9
jedno	jeden	k4xCgNnSc4
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6
parlamentu	parlament	k1gInSc6	parlament
nebo	nebo	k8xC
skupina	skupina	k1gFnSc1	skupina
20	#num#	k4
000	#num#	k4
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
mají	mít	k5eAaImIp3nP
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4
funkce	funkce	k1gFnSc2	funkce
ujímá	ujímat	k5eAaImIp3nS
první	první	k4xOgInSc4
den	den	k1gInSc4	den
v	v	k7c6
měsíci	měsíc	k1gInSc6	měsíc
po	po	k7c6
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
veřejně	veřejně	k6eAd1
přednese	přednést	k5eAaPmIp3nS
slib	slib	k1gInSc4	slib
ve	v	k7c6
finštině	finština	k1gFnSc6	finština
a	a	k8xC
švédštině	švédština	k1gFnSc6	švédština
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
slib	slib	k1gInSc4	slib
dle	dle	k7c2
56	#num#	k4
<g/>
.	.	kIx.
článku	článek	k1gInSc2	článek
finské	finský	k2eAgFnSc2d1	finská
ústavy	ústava	k1gFnSc2	ústava
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
XY	XY	kA
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
si	se	k3xPyFc3
finský	finský	k2eAgInSc4d1	finský
lid	lid	k1gInSc4	lid
zvolil	zvolit	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,
slibuji	slibovat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
funkci	funkce	k1gFnSc6	funkce
budu	být	k5eAaImBp1nS
upřímně	upřímně	k6eAd1
a	a	k8xC
věrně	věrně	k6eAd1
dodržovat	dodržovat	k5eAaImF
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC
zákony	zákon	k1gInPc4	zákon
republiky	republika	k1gFnSc2	republika
a	a	k8xC
podle	podle	k7c2
svých	svůj	k3xOyFgFnPc2
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
schopností	schopnost	k1gFnPc2	schopnost
podporovat	podporovat	k5eAaImF
blaho	blaho	k1gNnSc4	blaho
finského	finský	k2eAgInSc2d1	finský
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Od	od	k7c2
roku	rok	k1gInSc2	rok
1991	#num#	k4
může	moct	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
osoba	osoba	k1gFnSc1	osoba
zastávat	zastávat	k5eAaImF
tento	tento	k3xDgInSc4
úřad	úřad	k1gInSc4	úřad
pouze	pouze	k6eAd1
po	po	k7c4
dvě	dva	k4xCgFnPc4
šestileté	šestiletý	k2eAgFnPc4d1	šestiletá
funkční	funkční	k2eAgFnPc4d1	funkční
období	období	k1gNnPc4	období
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finská	finský	k2eAgFnSc1d1	finská
ústava	ústava	k1gFnSc1	ústava
stanoví	stanovit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
pouze	pouze	k6eAd1
osoba	osoba	k1gFnSc1	osoba
narozená	narozený	k2eAgFnSc1d1	narozená
ve	v	k7c6
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zastává	zastávat	k5eAaImIp3nS
úřad	úřad	k1gInSc1	úřad
finského	finský	k2eAgMnSc2d1	finský
prezidenta	prezident	k1gMnSc2	prezident
Sauli	Saule	k1gFnSc4	Saule
Niinistö	Niinistö	k1gFnSc2	Niinistö
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2	březen
2012	#num#	k4
nahradil	nahradit	k5eAaPmAgInS
první	první	k4xOgFnSc4
ženu	žena	k1gFnSc4	žena
v	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,
Tarju	Tarju	k1gFnSc3	Tarju
Halonenovou	Halonenová	k1gFnSc4	Halonenová
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Seznam	seznam	k1gInSc1	seznam
finských	finský	k2eAgMnPc2d1	finský
prezidentů	prezident	k1gMnPc2	prezident
==	==	k?
</s>
</p>
<p>
<s>
==	==	k?
Reference	reference	k1gFnPc1	reference
==	==	k?
</s>
</p>
