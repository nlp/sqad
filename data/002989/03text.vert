<s>
Zelená	Zelená	k1gFnSc1	Zelená
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
barva	barva	k1gFnSc1	barva
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
světla	světlo	k1gNnSc2	světlo
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
520	[number]	k4	520
až	až	k9	až
570	[number]	k4	570
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Zelenou	Zelená	k1gFnSc4	Zelená
lze	lze	k6eAd1	lze
při	při	k7c6	při
subtraktivním	subtraktivní	k2eAgNnSc6d1	subtraktivní
míchání	míchání	k1gNnSc6	míchání
barev	barva	k1gFnPc2	barva
získat	získat	k5eAaPmF	získat
jako	jako	k9	jako
kombinaci	kombinace	k1gFnSc4	kombinace
žluté	žlutý	k2eAgFnSc2d1	žlutá
a	a	k8xC	a
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
při	při	k7c6	při
aditivním	aditivní	k2eAgNnSc6d1	aditivní
míchání	míchání	k1gNnSc6	míchání
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
RGB	RGB	kA	RGB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
doplňkem	doplněk	k1gInSc7	doplněk
je	být	k5eAaImIp3nS	být
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
uznávají	uznávat	k5eAaImIp3nP	uznávat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
teorii	teorie	k1gFnSc4	teorie
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
doplňkem	doplněk	k1gInSc7	doplněk
zelené	zelený	k2eAgFnSc2d1	zelená
barvy	barva	k1gFnSc2	barva
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kultury	kultura	k1gFnPc1	kultura
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
)	)	kIx)	)
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
mezi	mezi	k7c7	mezi
zelenou	zelená	k1gFnSc7	zelená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
nazývá	nazývat	k5eAaImIp3nS	nazývat
grue	grue	k6eAd1	grue
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
rostlin	rostlina	k1gFnPc2	rostlina
má	mít	k5eAaImIp3nS	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
přítomností	přítomnost	k1gFnSc7	přítomnost
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
silně	silně	k6eAd1	silně
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
odpovídajících	odpovídající	k2eAgFnPc6d1	odpovídající
modré	modrý	k2eAgFnSc3d1	modrá
a	a	k8xC	a
červené	červený	k2eAgFnSc3d1	červená
barvě	barva	k1gFnSc3	barva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odražené	odražený	k2eAgNnSc1d1	odražené
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
zelené	zelená	k1gFnSc2	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
ekologická	ekologický	k2eAgFnSc1d1	ekologická
a	a	k8xC	a
environmentalistická	environmentalistický	k2eAgNnPc1d1	environmentalistické
hnutí	hnutí	k1gNnPc1	hnutí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Greenpeace	Greenpeace	k1gFnSc1	Greenpeace
(	(	kIx(	(
<g/>
zelený	zelený	k2eAgInSc1d1	zelený
mír	mír	k1gInSc1	mír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Zelení	Zelený	k1gMnPc1	Zelený
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
různé	různý	k2eAgFnPc1d1	různá
strany	strana	k1gFnPc1	strana
s	s	k7c7	s
ekologickým	ekologický	k2eAgInSc7d1	ekologický
programem	program	k1gInSc7	program
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
členové	člen	k1gMnPc1	člen
Strany	strana	k1gFnSc2	strana
zelených	zelený	k2eAgMnPc2d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Ideologii	ideologie	k1gFnSc4	ideologie
těchto	tento	k3xDgFnPc2	tento
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
zelená	zelený	k2eAgFnSc1d1	zelená
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
politice	politika	k1gFnSc6	politika
označení	označení	k1gNnSc1	označení
jako	jako	k8xS	jako
zelený	zelený	k2eAgInSc1d1	zelený
postoj	postoj	k1gInSc1	postoj
znamená	znamenat	k5eAaImIp3nS	znamenat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
ideologických	ideologický	k2eAgFnPc2d1	ideologická
zásad	zásada	k1gFnPc2	zásada
-	-	kIx~	-
mj.	mj.	kA	mj.
zohledňování	zohledňování	k1gNnSc1	zohledňování
ekologických	ekologický	k2eAgInPc2d1	ekologický
nebo	nebo	k8xC	nebo
trvale	trvale	k6eAd1	trvale
udržitelných	udržitelný	k2eAgInPc2d1	udržitelný
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
menšinám	menšina	k1gFnPc3	menšina
(	(	kIx(	(
<g/>
náboženským	náboženský	k2eAgInSc7d1	náboženský
<g/>
,	,	kIx,	,
národnostním	národnostní	k2eAgInSc7d1	národnostní
<g/>
,	,	kIx,	,
sexuálním	sexuální	k2eAgInSc7d1	sexuální
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
respekt	respekt	k1gInSc4	respekt
k	k	k7c3	k
odlišnostem	odlišnost	k1gFnPc3	odlišnost
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgNnSc4d1	maximální
zapojení	zapojení	k1gNnSc4	zapojení
občanů	občan	k1gMnPc2	občan
do	do	k7c2	do
procesů	proces	k1gInPc2	proces
zasahujících	zasahující	k2eAgFnPc2d1	zasahující
jejich	jejich	k3xOp3gInSc4	jejich
život	život	k1gInSc1	život
a	a	k8xC	a
decentralizace	decentralizace	k1gFnSc1	decentralizace
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
účastnická	účastnický	k2eAgFnSc1d1	účastnická
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nenásilí	nenásilí	k1gNnSc1	nenásilí
a	a	k8xC	a
pacifismus	pacifismus	k1gInSc1	pacifismus
<g/>
,	,	kIx,	,
přirozená	přirozený	k2eAgFnSc1d1	přirozená
skromnost	skromnost	k1gFnSc1	skromnost
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
udržitelného	udržitelný	k2eAgInSc2d1	udržitelný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
právě	právě	k9	právě
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc4	výsledek
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
díky	dík	k1gInPc7	dík
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
nejvíce	nejvíce	k6eAd1	nejvíce
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Zrakový	zrakový	k2eAgInSc1d1	zrakový
systém	systém	k1gInSc1	systém
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
vyšších	vysoký	k2eAgMnPc2d2	vyšší
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
těch	ten	k3xDgFnPc2	ten
živících	živící	k2eAgFnPc2d1	živící
se	se	k3xPyFc4	se
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
stravou	strava	k1gFnSc7	strava
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgMnPc3d1	jiný
živočichům	živočich	k1gMnPc3	živočich
třetí	třetí	k4xOgInSc4	třetí
typ	typ	k1gInSc4	typ
zrakových	zrakový	k2eAgInPc2d1	zrakový
čípků	čípek	k1gInPc2	čípek
<g/>
,	,	kIx,	,
citlivých	citlivý	k2eAgFnPc2d1	citlivá
právě	právě	k9	právě
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jihoamerické	jihoamerický	k2eAgInPc1d1	jihoamerický
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
desítky	desítka	k1gFnSc2	desítka
(	(	kIx(	(
<g/>
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
až	až	k9	až
200	[number]	k4	200
<g/>
)	)	kIx)	)
výrazů	výraz	k1gInPc2	výraz
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
zelené	zelená	k1gFnSc2	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	Zelená	k1gFnSc1	Zelená
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
barva	barva	k1gFnSc1	barva
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
barva	barva	k1gFnSc1	barva
rajských	rajský	k2eAgFnPc2d1	rajská
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
všeobecně	všeobecně	k6eAd1	všeobecně
označuje	označovat	k5eAaImIp3nS	označovat
volno	volno	k1gNnSc4	volno
<g/>
,	,	kIx,	,
bezpečí	bezpečí	k1gNnSc4	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c6	na
semaforech	semafor	k1gInPc6	semafor
jako	jako	k8xC	jako
signál	signál	k1gInSc4	signál
volno	volno	k6eAd1	volno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
automobilových	automobilový	k2eAgInPc6d1	automobilový
závodech	závod	k1gInPc6	závod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
)	)	kIx)	)
značí	značit	k5eAaImIp3nS	značit
zelená	zelený	k2eAgFnSc1d1	zelená
vlajka	vlajka	k1gFnSc1	vlajka
konec	konec	k1gInSc1	konec
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
znovuspuštění	znovuspuštění	k1gNnSc4	znovuspuštění
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
pozičního	poziční	k2eAgNnSc2d1	poziční
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
letadlech	letadlo	k1gNnPc6	letadlo
a	a	k8xC	a
lodích	loď	k1gFnPc6	loď
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
starboard	starboard	k1gMnSc1	starboard
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
silnicích	silnice	k1gFnPc6	silnice
označuje	označovat	k5eAaImIp3nS	označovat
zelená	zelený	k2eAgFnSc1d1	zelená
dopravní	dopravní	k2eAgFnSc1d1	dopravní
značka	značka	k1gFnSc1	značka
dálniční	dálniční	k2eAgNnSc1d1	dálniční
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
schopnosti	schopnost	k1gFnSc3	schopnost
kamufláže	kamufláž	k1gFnSc2	kamufláž
je	být	k5eAaImIp3nS	být
zelená	zelená	k1gFnSc1	zelená
tradiční	tradiční	k2eAgFnSc1d1	tradiční
barvou	barva	k1gFnSc7	barva
vojenských	vojenský	k2eAgFnPc2d1	vojenská
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
zelený	zelený	k2eAgInSc1d1	zelený
mužíček	mužíček	k1gInSc1	mužíček
je	být	k5eAaImIp3nS	být
stereotyp	stereotyp	k1gInSc4	stereotyp
mimozemšťana	mimozemšťan	k1gMnSc2	mimozemšťan
(	(	kIx(	(
<g/>
humanoidní	humanoidní	k2eAgInSc1d1	humanoidní
vzhled	vzhled	k1gInSc1	vzhled
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
anténky	anténka	k1gFnPc1	anténka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
asociována	asociovat	k5eAaBmNgFnS	asociovat
se	s	k7c7	s
závistí	závist	k1gFnSc7	závist
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c4	v
rčení	rčení	k1gNnSc4	rčení
zezelenat	zezelenat	k5eAaPmF	zezelenat
závistí	závist	k1gFnSc7	závist
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
nezkušený	zkušený	k2eNgMnSc1d1	nezkušený
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgInSc1d1	označován
za	za	k7c4	za
zelenáče	zelenáč	k1gMnSc4	zelenáč
(	(	kIx(	(
<g/>
greenhorn	greenhorn	k1gMnSc1	greenhorn
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
přirovnání	přirovnání	k1gNnSc4	přirovnání
k	k	k7c3	k
zelené	zelený	k2eAgFnSc3d1	zelená
barvě	barva	k1gFnSc3	barva
nezralého	zralý	k2eNgNnSc2d1	nezralé
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
rezistorů	rezistor	k1gInPc2	rezistor
znamená	znamenat	k5eAaImIp3nS	znamenat
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
5	[number]	k4	5
nebo	nebo	k8xC	nebo
toleranci	tolerance	k1gFnSc4	tolerance
±	±	k?	±
<g/>
0.5	[number]	k4	0.5
<g/>
%	%	kIx~	%
</s>
<s>
Označení	označení	k1gNnSc1	označení
zelená	zelená	k1gFnSc1	zelená
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgInSc4d1	tradiční
mátový	mátový	k2eAgInSc4d1	mátový
likér	likér	k1gInSc4	likér
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
mimochodem	mimochodem	k9	mimochodem
o	o	k7c4	o
název	název	k1gInSc4	název
písně	píseň	k1gFnPc1	píseň
skupiny	skupina	k1gFnSc2	skupina
Tři	tři	k4xCgFnPc4	tři
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
z	z	k7c2	z
alba	album	k1gNnSc2	album
Alkáč	Alkáč	k1gInSc1	Alkáč
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
kocour	kocour	k1gInSc1	kocour
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
A	a	k9	a
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
má	mít	k5eAaImIp3nS	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
na	na	k7c6	na
mariášových	mariášový	k2eAgFnPc6d1	mariášová
kartách	karta	k1gFnPc6	karta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
zelenými	zelený	k2eAgInPc7d1	zelený
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zelená	zelenat	k5eAaImIp3nS	zelenat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
zelený	zelený	k2eAgInSc1d1	zelený
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
