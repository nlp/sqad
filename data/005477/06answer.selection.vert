<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Μ	Μ	k?	Μ
<g/>
,	,	kIx,	,
Mesopotamia	Mesopotamia	k1gFnSc1	Mesopotamia
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
meziříčí	meziříčí	k1gNnSc1	meziříčí
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
ب	ب	k?	ب
ا	ا	k?	ا
bilā	bilā	k?	bilā
al-rā	alā	k?	al-rā
<g/>
,	,	kIx,	,
syrsky	syrsky	k6eAd1	syrsky
ܒ	ܒ	k?	ܒ
ܢ	ܢ	k?	ܢ
beth	beth	k1gMnSc1	beth
nahrain	nahrain	k1gMnSc1	nahrain
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
řek	řeka	k1gFnPc2	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Eufrat	Eufrat	k1gInSc1	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc1	Tigris
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
jádro	jádro	k1gNnSc1	jádro
tvoří	tvořit	k5eAaImIp3nS	tvořit
povodí	povodí	k1gNnSc4	povodí
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
obou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
