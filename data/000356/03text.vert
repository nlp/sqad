<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
sedmičky	sedmička	k1gFnPc1	sedmička
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
desktopová	desktopový	k2eAgFnSc1d1	desktopová
verze	verze	k1gFnSc1	verze
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
podpory	podpora	k1gFnSc2	podpora
je	být	k5eAaImIp3nS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
aktualizace	aktualizace	k1gFnSc2	aktualizace
budou	být	k5eAaImBp3nP	být
vydávány	vydávat	k5eAaPmNgFnP	vydávat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
podpory	podpora	k1gFnSc2	podpora
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
32	[number]	k4	32
<g/>
bitová	bitový	k2eAgFnSc1d1	bitová
a	a	k8xC	a
64	[number]	k4	64
<g/>
bitová	bitový	k2eAgFnSc1d1	bitová
varianta	varianta	k1gFnSc1	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
Windows	Windows	kA	Windows
7	[number]	k4	7
byly	být	k5eAaImAgInP	být
Windows	Windows	kA	Windows
Vista	vista	k2eAgInPc1d1	vista
a	a	k8xC	a
nástupcem	nástupce	k1gMnSc7	nástupce
jsou	být	k5eAaImIp3nP	být
Windows	Windows	kA	Windows
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
svému	svůj	k3xOyFgMnSc3	svůj
předchůdci	předchůdce	k1gMnSc3	předchůdce
je	být	k5eAaImIp3nS	být
Windows	Windows	kA	Windows
7	[number]	k4	7
výrazně	výrazně	k6eAd1	výrazně
modernizován	modernizovat	k5eAaBmNgInS	modernizovat
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
plná	plný	k2eAgFnSc1d1	plná
kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
s	s	k7c7	s
existujícími	existující	k2eAgInPc7d1	existující
ovladači	ovladač	k1gInPc7	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
hardwaru	hardware	k1gInSc2	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Microsoft	Microsoft	kA	Microsoft
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
uvedla	uvést	k5eAaPmAgFnS	uvést
prezentaci	prezentace	k1gFnSc4	prezentace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
multidotykové	multidotykový	k2eAgNnSc4d1	multidotykový
ovládání	ovládání	k1gNnSc4	ovládání
<g/>
,	,	kIx,	,
přestavěné	přestavěný	k2eAgInPc4d1	přestavěný
Windows	Windows	kA	Windows
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
hlavním	hlavní	k2eAgInSc7d1	hlavní
panelem	panel	k1gInSc7	panel
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgFnSc1d1	domácí
síť	síť	k1gFnSc1	síť
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
HomeGroup	HomeGroup	k1gInSc4	HomeGroup
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
aplikace	aplikace	k1gFnPc1	aplikace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
verzích	verze	k1gFnPc6	verze
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Windows	Windows	kA	Windows
Mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Movie	Movie	k1gFnSc1	Movie
Maker	makro	k1gNnPc2	makro
nebo	nebo	k8xC	nebo
Windows	Windows	kA	Windows
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nebudou	být	k5eNaImBp3nP	být
dále	daleko	k6eAd2	daleko
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stanou	stanout	k5eAaPmIp3nP	stanout
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
balíčku	balíček	k1gInSc2	balíček
programů	program	k1gInPc2	program
služby	služba	k1gFnSc2	služba
Windows	Windows	kA	Windows
Live	Liv	k1gMnSc2	Liv
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
o	o	k7c4	o
Windows	Windows	kA	Windows
7	[number]	k4	7
začalo	začít	k5eAaPmAgNnS	začít
mluvit	mluvit	k5eAaImF	mluvit
ještě	ještě	k9	ještě
pod	pod	k7c7	pod
kódovým	kódový	k2eAgNnSc7d1	kódové
jménem	jméno	k1gNnSc7	jméno
Blackcomb	Blackcomba	k1gFnPc2	Blackcomba
jako	jako	k8xS	jako
o	o	k7c4	o
nástupci	nástupce	k1gMnSc3	nástupce
Windows	Windows	kA	Windows
XP	XP	kA	XP
(	(	kIx(	(
<g/>
kódové	kódový	k2eAgNnSc1d1	kódové
jméno	jméno	k1gNnSc1	jméno
Whistler	Whistler	k1gInSc1	Whistler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
však	však	k9	však
byla	být	k5eAaImAgFnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
meziverze	meziverze	k1gFnSc1	meziverze
s	s	k7c7	s
kódovým	kódový	k2eAgNnSc7d1	kódové
označením	označení	k1gNnSc7	označení
Longhorn	Longhorn	k1gNnSc1	Longhorn
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Windows	Windows	kA	Windows
Vista	vista	k2eAgInPc7d1	vista
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
pozdržen	pozdržet	k5eAaPmNgMnS	pozdržet
po	po	k7c6	po
několika	několik	k4yIc6	několik
větších	veliký	k2eAgInPc6d2	veliký
problémech	problém	k1gInPc6	problém
s	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
virů	vir	k1gInPc2	vir
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
XP	XP	kA	XP
a	a	k8xC	a
Server	server	k1gInSc1	server
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
kódové	kódový	k2eAgNnSc1d1	kódové
jméno	jméno	k1gNnSc1	jméno
Blackcomb	Blackcomba	k1gFnPc2	Blackcomba
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c4	na
Vienna	Vienn	k1gMnSc4	Vienn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
konečné	konečný	k2eAgFnSc3d1	konečná
změně	změna	k1gFnSc3	změna
na	na	k7c4	na
název	název	k1gInSc4	název
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Plná	plný	k2eAgFnSc1d1	plná
verze	verze	k1gFnSc1	verze
Windows	Windows	kA	Windows
7	[number]	k4	7
vyšla	vyjít	k5eAaPmAgFnS	vyjít
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
vyjít	vyjít	k5eAaPmF	vyjít
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
jazykové	jazykový	k2eAgFnPc1d1	jazyková
verze	verze	k1gFnPc1	verze
vydány	vydat	k5eAaPmNgFnP	vydat
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
betaverze	betaverze	k1gFnSc1	betaverze
SP1	SP1	k1gFnSc1	SP1
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Service	Service	k1gFnSc1	Service
pack	packa	k1gFnPc2	packa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
aktualizace	aktualizace	k1gFnSc1	aktualizace
Service	Service	k1gFnSc1	Service
Pack	Pack	k1gInSc1	Pack
1	[number]	k4	1
oficiálně	oficiálně	k6eAd1	oficiálně
uvedena	uvést	k5eAaPmNgFnS	uvést
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
aplikací	aplikace	k1gFnSc7	aplikace
Internet	Internet	k1gInSc4	Internet
Explorer	Explorer	k1gInSc1	Explorer
verze	verze	k1gFnSc1	verze
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
standardně	standardně	k6eAd1	standardně
dostupné	dostupný	k2eAgInPc1d1	dostupný
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
třech	tři	k4xCgFnPc6	tři
verzích	verze	k1gFnPc6	verze
<g/>
:	:	kIx,	:
Windows	Windows	kA	Windows
7	[number]	k4	7
Starter	Startra	k1gFnPc2	Startra
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
netbooky	netbook	k1gInPc4	netbook
<g/>
,	,	kIx,	,
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
OEM	OEM	kA	OEM
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
Home	Hom	k1gFnSc2	Hom
Basic	Basic	kA	Basic
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
<g/>
,	,	kIx,	,
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rozvojových	rozvojový	k2eAgInPc6d1	rozvojový
trzích	trh	k1gInPc6	trh
Windows	Windows	kA	Windows
7	[number]	k4	7
Home	Home	k1gFnPc2	Home
Premium	Premium	k1gNnSc4	Premium
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
Professional	Professional	k1gFnSc2	Professional
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
určená	určený	k2eAgFnSc1d1	určená
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
firemní	firemní	k2eAgMnPc4d1	firemní
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zejména	zejména	k9	zejména
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
funkci	funkce	k1gFnSc4	funkce
připojení	připojení	k1gNnSc2	připojení
do	do	k7c2	do
domény	doména	k1gFnSc2	doména
(	(	kIx(	(
<g/>
Domain	Domain	k2eAgInSc1d1	Domain
Join	Join	k1gInSc1	Join
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
pokročilé	pokročilý	k2eAgFnPc4d1	pokročilá
možnosti	možnost	k1gFnPc4	možnost
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
především	především	k9	především
pro	pro	k7c4	pro
firemní	firemní	k2eAgMnPc4d1	firemní
zákazníky	zákazník	k1gMnPc4	zákazník
a	a	k8xC	a
některé	některý	k3yIgMnPc4	některý
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
Ultimate	Ultimat	k1gInSc5	Ultimat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
edice	edice	k1gFnPc1	edice
obsahující	obsahující	k2eAgFnPc1d1	obsahující
veškeré	veškerý	k3xTgFnPc1	veškerý
dostupné	dostupný	k2eAgFnPc1d1	dostupná
funkce	funkce	k1gFnPc1	funkce
edicí	edice	k1gFnPc2	edice
Professional	Professional	k1gFnSc2	Professional
a	a	k8xC	a
Enterprise	Enterprise	k1gFnSc2	Enterprise
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
Enterprise	Enterprise	k1gFnSc2	Enterprise
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
firemním	firemní	k2eAgMnPc3d1	firemní
zákazníkům	zákazník	k1gMnPc3	zákazník
s	s	k7c7	s
uzavřenou	uzavřený	k2eAgFnSc7d1	uzavřená
multilicenční	multilicenční	k2eAgFnSc7d1	multilicenční
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Enterprise	Enterprise	k1gFnSc1	Enterprise
edice	edice	k1gFnSc1	edice
nabízí	nabízet	k5eAaImIp3nS	nabízet
stejnou	stejný	k2eAgFnSc4d1	stejná
funkčnost	funkčnost	k1gFnSc4	funkčnost
jako	jako	k8xC	jako
Windows	Windows	kA	Windows
Pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
různé	různý	k2eAgFnPc4d1	různá
multilicenční	multilicenční	k2eAgFnPc4d1	multilicenční
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
Embedded	Embedded	k1gInSc4	Embedded
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
vyhrazená	vyhrazený	k2eAgFnSc1d1	vyhrazená
výrobcům	výrobce	k1gMnPc3	výrobce
hardware	hardware	k1gInSc4	hardware
pro	pro	k7c4	pro
bankomaty	bankomat	k1gInPc4	bankomat
<g/>
,	,	kIx,	,
pokladní	pokladní	k2eAgInPc4d1	pokladní
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc4	stroj
řídící	řídící	k2eAgFnSc4d1	řídící
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
rozdělení	rozdělení	k1gNnSc2	rozdělení
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
odvozené	odvozený	k2eAgFnSc2d1	odvozená
edice	edice	k1gFnSc2	edice
vynucené	vynucený	k2eAgFnSc2d1	vynucená
na	na	k7c6	na
Microsoftu	Microsoft	k1gInSc6	Microsoft
antimonopolními	antimonopolní	k2eAgNnPc7d1	antimonopolní
řízeními	řízení	k1gNnPc7	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
edicí	edice	k1gFnPc2	edice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
písmeno	písmeno	k1gNnSc4	písmeno
N	N	kA	N
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Windows	Windows	kA	Windows
7	[number]	k4	7
Ultimate	Ultimat	k1gInSc5	Ultimat
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
značí	značit	k5eAaImIp3nS	značit
verzi	verze	k1gFnSc4	verze
bez	bez	k7c2	bez
Windows	Windows	kA	Windows
Media	medium	k1gNnPc1	medium
Playeru	Player	k1gInSc2	Player
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
7	[number]	k4	7
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
proti	proti	k7c3	proti
předchozím	předchozí	k2eAgFnPc3d1	předchozí
verzím	verze	k1gFnPc3	verze
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
<g/>
,	,	kIx,	,
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
rychlejší	rychlý	k2eAgNnSc4d2	rychlejší
nastartování	nastartování	k1gNnSc4	nastartování
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
vícejádrových	vícejádrův	k2eAgInPc2d1	vícejádrův
procesorů	procesor	k1gInPc2	procesor
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
virtuální	virtuální	k2eAgInPc4d1	virtuální
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
<g/>
,	,	kIx,	,
zmenšené	zmenšený	k2eAgNnSc1d1	zmenšené
jádro	jádro	k1gNnSc1	jádro
MinWin	MinWina	k1gFnPc2	MinWina
nebo	nebo	k8xC	nebo
rozpoznávání	rozpoznávání	k1gNnSc2	rozpoznávání
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoznávání	rozpoznávání	k1gNnSc1	rozpoznávání
rukopisu	rukopis	k1gInSc2	rukopis
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
rozpoznávání	rozpoznávání	k1gNnSc2	rozpoznávání
řeči	řeč	k1gFnSc2	řeč
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
češtinu	čeština	k1gFnSc4	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
zaslané	zaslaný	k2eAgFnSc2d1	zaslaná
TG	tg	kA	tg
Daily	Daila	k1gFnSc2	Daila
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
sestavení	sestavení	k1gNnSc1	sestavení
Milestone	Mileston	k1gInSc5	Mileston
1	[number]	k4	1
Windows	Windows	kA	Windows
Seven	Seven	k1gInSc4	Seven
podporu	podpor	k1gInSc2	podpor
pro	pro	k7c4	pro
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
více	hodně	k6eAd2	hodně
heterogenních	heterogenní	k2eAgFnPc2d1	heterogenní
grafických	grafický	k2eAgFnPc2d1	grafická
karet	kareta	k1gFnPc2	kareta
od	od	k7c2	od
různých	různý	k2eAgMnPc2d1	různý
dodavatelů	dodavatel	k1gMnPc2	dodavatel
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
Windows	Windows	kA	Windows
Media	medium	k1gNnPc1	medium
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nové	nový	k2eAgFnPc4d1	nová
funkce	funkce	k1gFnPc4	funkce
Milestone	Mileston	k1gInSc5	Mileston
1	[number]	k4	1
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
gadgety	gadget	k1gInPc4	gadget
integrované	integrovaný	k2eAgInPc4d1	integrovaný
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Průzkumníka	průzkumník	k1gMnSc2	průzkumník
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
gadget	gadget	k1gInSc1	gadget
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
Media	medium	k1gNnSc2	medium
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
vizuálního	vizuální	k2eAgInSc2d1	vizuální
PINu	pin	k1gInSc2	pin
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
spouštění	spouštění	k1gNnSc2	spouštění
položek	položka	k1gFnPc2	položka
jak	jak	k8xC	jak
z	z	k7c2	z
nabídky	nabídka	k1gFnSc2	nabídka
Start	start	k1gInSc1	start
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
hlavního	hlavní	k2eAgInSc2d1	hlavní
panelu	panel	k1gInSc2	panel
a	a	k8xC	a
lepší	dobrý	k2eAgFnPc4d2	lepší
mediální	mediální	k2eAgFnPc4d1	mediální
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
integrován	integrován	k2eAgMnSc1d1	integrován
XPS	XPS	kA	XPS
Essentials	Essentials	k1gInSc4	Essentials
Pack	Packa	k1gFnPc2	Packa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Windows	Windows	kA	Windows
PowerShell	PowerShell	k1gInSc1	PowerShell
Integrated	Integrated	k1gInSc1	Integrated
Scripting	Scripting	k1gInSc1	Scripting
Environment	Environment	k1gInSc1	Environment
(	(	kIx(	(
<g/>
ISE	ISE	kA	ISE
<g/>
)	)	kIx)	)
a	a	k8xC	a
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
kalkulačka	kalkulačka	k1gFnSc1	kalkulačka
s	s	k7c7	s
víceřádkovými	víceřádkový	k2eAgFnPc7d1	víceřádková
možnostmi	možnost	k1gFnPc7	možnost
vkládání	vkládání	k1gNnSc2	vkládání
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
programových	programový	k2eAgInPc2d1	programový
a	a	k8xC	a
statistických	statistický	k2eAgInPc2d1	statistický
režimů	režim	k1gInPc2	režim
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
převodníkem	převodník	k1gInSc7	převodník
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ovládacích	ovládací	k2eAgInPc2d1	ovládací
panelů	panel	k1gInPc2	panel
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgFnPc2d1	nová
položek	položka	k1gFnPc2	položka
<g/>
:	:	kIx,	:
Akcelerátory	akcelerátor	k1gInPc1	akcelerátor
<g/>
,	,	kIx,	,
ClearType	ClearTyp	k1gInSc5	ClearTyp
Text	text	k1gInSc1	text
Tuner	tuner	k1gInSc1	tuner
<g/>
,	,	kIx,	,
Displej	displej	k1gInSc1	displej
kalibrace	kalibrace	k1gFnSc2	kalibrace
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
Průvodce	průvodka	k1gFnSc3	průvodka
miniaplikacemi	miniaplikace	k1gFnPc7	miniaplikace
<g/>
,	,	kIx,	,
Infračervený	infračervený	k2eAgInSc1d1	infračervený
port	port	k1gInSc1	port
<g/>
,	,	kIx,	,
Uzdravení	uzdravení	k1gNnSc1	uzdravení
<g/>
,	,	kIx,	,
Odstraňování	odstraňování	k1gNnSc1	odstraňování
závad	závada	k1gFnPc2	závada
<g/>
,	,	kIx,	,
Pracovní	pracovní	k2eAgNnPc1d1	pracovní
Centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
Zaměřovací	zaměřovací	k2eAgInPc1d1	zaměřovací
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
senzory	senzor	k1gInPc1	senzor
<g/>
,	,	kIx,	,
Manažer	manažer	k1gMnSc1	manažer
doporučení	doporučení	k1gNnSc2	doporučení
<g/>
,	,	kIx,	,
Biometrická	Biometrický	k2eAgNnPc4d1	Biometrické
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
Systémové	systémový	k2eAgFnPc4d1	systémová
ikony	ikona	k1gFnPc4	ikona
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Solution	Solution	k1gInSc1	Solution
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Zobrazení	zobrazení	k1gNnSc1	zobrazení
(	(	kIx(	(
<g/>
Displeje	displej	k1gFnPc1	displej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenován	k2eAgNnSc1d1	přejmenováno
na	na	k7c6	na
Windows	Windows	kA	Windows
Solution	Solution	k1gInSc1	Solution
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
Health	Health	k1gInSc1	Health
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Windows	Windows	kA	Windows
Zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
středisko	středisko	k1gNnSc1	středisko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
verzích	verze	k1gFnPc6	verze
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nyní	nyní	k6eAd1	nyní
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
a	a	k8xC	a
údržbu	údržba	k1gFnSc4	údržba
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
