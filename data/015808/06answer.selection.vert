<s desamb="1">
Roku	rok	k1gInSc2
1419	#num#	k4
byl	být	k5eAaImAgMnS
Rudolf	Rudolf	k1gMnSc1
císařem	císař	k1gMnSc7
vyslán	vyslat	k5eAaPmNgInS
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tam	tam	k6eAd1
ukončil	ukončit	k5eAaPmAgMnS
povstání	povstání	k1gNnPc4
husitů	husita	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
začalo	začít	k5eAaPmAgNnS
pražskou	pražský	k2eAgFnSc7d1
defenestrací	defenestrace	k1gFnSc7
<g/>
.	.	kIx.
</s>