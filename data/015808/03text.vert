<s>
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgInSc1d1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgInSc1d1
</s>
<s>
sasko-wittenberský	sasko-wittenberský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
<g/>
,	,	kIx,
saský	saský	k2eAgMnSc1d1
kurfiřt	kurfiřt	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgInSc1d1
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1388	#num#	k4
<g/>
–	–	k?
<g/>
1419	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
asi	asi	k9
1373	#num#	k4
</s>
<s>
Lutherstadt	Lutherstadt	k2eAgInSc1d1
Wittenberg	Wittenberg	k1gInSc1
<g/>
,	,	kIx,
Saské	saský	k2eAgNnSc1d1
kurfiřtství	kurfiřtství	k1gNnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1419	#num#	k4
</s>
<s>
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
františkánský	františkánský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
ve	v	k7c6
Wittenbergu	Wittenberg	k1gInSc6
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
I.	I.	kA
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Míšeňská	míšeňský	k2eAgFnSc1d1
</s>
<s>
Barbara	Barbara	k1gFnSc1
Lehnická	Lehnický	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
ScholasticaRudolfVáclavZikmundBarbora	ScholasticaRudolfVáclavZikmundBarbora	k1gFnSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Askánci	Askánek	k1gMnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
I.	I.	kA
Saský	saský	k2eAgInSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Cecílie	Cecílie	k1gFnSc1
z	z	k7c2
Carrara	Carrar	k1gMnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1373	#num#	k4
<g/>
,	,	kIx,
Wittenberg	Wittenberg	k1gInSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1419	#num#	k4
<g/>
,	,	kIx,
Čechy	Čechy	k1gFnPc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
vévoda	vévoda	k1gMnSc1
sasko-wittenberský	sasko-wittenberský	k2eAgMnSc1d1
a	a	k8xC
kurfiřt	kurfiřt	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
rodu	rod	k1gInSc2
Askánců	Askánec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládl	vládnout	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1388	#num#	k4
až	až	k9
1419	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Jako	jako	k8xS,k8xC
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Václava	Václav	k1gMnSc2
I.	I.	kA
Saského	saský	k2eAgInSc2d1
převzal	převzít	k5eAaPmAgInS
po	po	k7c6
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1388	#num#	k4
vladařské	vladařský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
Sasko-wittenberského	Sasko-wittenberský	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c4
let	léto	k1gNnPc2
byl	být	k5eAaImAgMnS
ve	v	k7c6
sporu	spor	k1gInSc6
s	s	k7c7
arcibiskupem	arcibiskup	k1gMnSc7
magdeburským	magdeburský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1419	#num#	k4
byl	být	k5eAaImAgMnS
Rudolf	Rudolf	k1gMnSc1
císařem	císař	k1gMnSc7
vyslán	vyslat	k5eAaPmNgInS
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tam	tam	k6eAd1
ukončil	ukončit	k5eAaPmAgMnS
povstání	povstání	k1gNnPc4
husitů	husita	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
začalo	začít	k5eAaPmAgNnS
pražskou	pražský	k2eAgFnSc7d1
defenestrací	defenestrace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
již	již	k9
na	na	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
mu	on	k3xPp3gNnSc3
byl	být	k5eAaImAgInS
podán	podat	k5eAaPmNgInS
jed	jed	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
ve	v	k7c6
františkánském	františkánský	k2eAgInSc6d1
klášteře	klášter	k1gInSc6
ve	v	k7c6
Wittenbergu	Wittenberg	k1gInSc6
<g/>
,	,	kIx,
během	během	k7c2
vykopávek	vykopávka	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
byly	být	k5eAaImAgInP
nalezené	nalezený	k2eAgInPc1d1
kosterní	kosterní	k2eAgInPc1d1
pozůstatky	pozůstatek	k1gInPc4
příslušníků	příslušník	k1gMnPc2
askánské	askánský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
přesunuty	přesunout	k5eAaPmNgFnP
do	do	k7c2
místního	místní	k2eAgInSc2d1
zámeckého	zámecký	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1387	#num#	k4
nebo	nebo	k8xC
1389	#num#	k4
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
Annou	Anna	k1gFnSc7
Míšeňskou	míšeňský	k2eAgFnSc7d1
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
markraběte	markrabě	k1gMnSc2
a	a	k8xC
zemského	zemský	k2eAgMnSc2d1
pána	pán	k1gMnSc2
Baltazara	Baltazar	k1gMnSc2
Míšeňského	míšeňský	k2eAgMnSc2d1
a	a	k8xC
Durynského	durynský	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podruhé	podruhé	k6eAd1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1396	#num#	k4
s	s	k7c7
Barborou	Barbora	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
vévody	vévoda	k1gMnSc2
Ruprechta	Ruprechto	k1gNnSc2
I.	I.	kA
Lehnického	Lehnický	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
těchto	tento	k3xDgNnPc2
manželství	manželství	k1gNnPc2
se	se	k3xPyFc4
narodily	narodit	k5eAaPmAgFnP
tyto	tento	k3xDgFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Scholastica	Scholastica	k1gFnSc1
(	(	kIx(
<g/>
1393	#num#	k4
<g/>
–	–	k?
<g/>
1463	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1406	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1407	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Zikmund	Zikmund	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1407	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Barbora	Barbora	k1gFnSc1
(	(	kIx(
<g/>
1405	#num#	k4
<g/>
–	–	k?
<g/>
1465	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Předkové	předek	k1gMnPc1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgInSc1d1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
I.	I.	kA
Saský	saský	k2eAgInSc1d1
</s>
<s>
Anežka	Anežka	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
</s>
<s>
Václav	Václav	k1gMnSc1
I.	I.	kA
Saský	saský	k2eAgInSc5d1
</s>
<s>
Ulrich	Ulrich	k1gMnSc1
z	z	k7c2
Lindowa-Ruppinu	Lindowa-Ruppin	k1gInSc2
</s>
<s>
Anežka	Anežka	k1gFnSc1
z	z	k7c2
Lindowa-Ruppinu	Lindowa-Ruppin	k1gInSc2
</s>
<s>
Adelaida	Adelaida	k1gFnSc1
ze	z	k7c2
Schladenu	Schladen	k2eAgFnSc4d1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgInSc1d1
</s>
<s>
Jakub	Jakub	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
Carrary	Carrara	k1gFnSc2
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
z	z	k7c2
Carrary	Carrara	k1gFnSc2
</s>
<s>
Costanza	Costanza	k1gFnSc1
da	da	k?
Polenta	polenta	k1gFnSc1
dei	dei	k?
Signori	Signor	k1gFnSc2
di	di	k?
Ravenna	Ravenna	k1gFnSc1
</s>
<s>
Cecilie	Cecilie	k1gFnSc1
z	z	k7c2
Carrary	Carrara	k1gFnSc2
</s>
<s>
</s>
<s>
Fina	Fin	k1gMnSc4
Buzzacarini	Buzzacarin	k1gMnPc1
</s>
<s>
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Sachsen-Wittenberg	Sachsen-Wittenberg	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rudolf	Rudolfa	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Václav	Václav	k1gMnSc1
I.	I.	kA
Saský	saský	k2eAgInSc1d1
</s>
<s>
Sasko-wittenberský	Sasko-wittenberský	k2eAgMnSc1d1
vévodaSaský	vévodaSaský	k1gMnSc1
kurfiřt	kurfiřt	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.1388	.1388	k4
<g/>
–	–	k?
<g/>
1419	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Albrecht	Albrecht	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saský	saský	k2eAgInSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
137797958	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85980231	#num#	k4
</s>
