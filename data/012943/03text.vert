<p>
<s>
Ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgMnSc1d1	říční
(	(	kIx(	(
<g/>
Alcedo	Alcedo	k1gNnSc1	Alcedo
atthis	atthis	k1gFnSc2	atthis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
16,5	[number]	k4	16,5
cm	cm	kA	cm
velký	velký	k2eAgMnSc1d1	velký
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ledňáčkovitých	ledňáčkovitý	k2eAgFnPc2d1	ledňáčkovitý
(	(	kIx(	(
<g/>
Alcedinidae	Alcedinidae	k1gFnPc2	Alcedinidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
s	s	k7c7	s
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
spodinou	spodina	k1gFnSc7	spodina
a	a	k8xC	a
modrým	modrý	k2eAgInSc7d1	modrý
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
,	,	kIx,	,
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
temenem	temeno	k1gNnSc7	temeno
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeho	on	k3xPp3gInSc4	on
nápadně	nápadně	k6eAd1	nápadně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
zašpičatělý	zašpičatělý	k2eAgInSc4d1	zašpičatělý
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
krásné	krásný	k2eAgNnSc4d1	krásné
zbarvení	zbarvení	k1gNnSc4	zbarvení
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgInSc1d1	nazýván
Létající	létající	k2eAgInSc1d1	létající
drahokam	drahokam	k1gInSc1	drahokam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
pomalu	pomalu	k6eAd1	pomalu
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
čistých	čistý	k2eAgFnPc2d1	čistá
vod	voda	k1gFnPc2	voda
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jediného	jediný	k2eAgMnSc4d1	jediný
zástupce	zástupce	k1gMnSc4	zástupce
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgMnSc1d1	říční
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
samotářsky	samotářsky	k6eAd1	samotářsky
žijící	žijící	k2eAgMnSc1d1	žijící
a	a	k8xC	a
přísně	přísně	k6eAd1	přísně
teritoriální	teritoriální	k2eAgMnSc1d1	teritoriální
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
menšími	malý	k2eAgFnPc7d2	menší
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
střemhlavým	střemhlavý	k2eAgInSc7d1	střemhlavý
útokem	útok	k1gInSc7	útok
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
potravě	potrava	k1gFnSc6	potrava
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
hloubí	hloubit	k5eAaImIp3nS	hloubit
ve	v	k7c6	v
strmých	strmý	k2eAgInPc6d1	strmý
březích	břeh	k1gInPc6	břeh
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snůšce	snůška	k1gFnSc6	snůška
přitom	přitom	k6eAd1	přitom
bývá	bývat	k5eAaImIp3nS	bývat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
světlých	světlý	k2eAgNnPc2d1	světlé
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgMnSc1d1	říční
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
vrabec	vrabec	k1gMnSc1	vrabec
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
cm	cm	kA	cm
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
mezi	mezi	k7c7	mezi
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
g.	g.	k?	g.
Má	mít	k5eAaImIp3nS	mít
zavalité	zavalitý	k2eAgNnSc4d1	zavalité
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
široká	široký	k2eAgNnPc4d1	široké
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
rovný	rovný	k2eAgInSc1d1	rovný
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
zobák	zobák	k1gInSc1	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
svým	svůj	k3xOyFgNnSc7	svůj
zářivým	zářivý	k2eAgNnSc7d1	zářivé
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
poddruzích	poddruh	k1gInPc6	poddruh
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Taxonomie	taxonomie	k1gFnSc2	taxonomie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1	středoevropský
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
A.	A.	kA	A.
a.	a.	k?	a.
ispida	ispida	k1gFnSc1	ispida
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
vnější	vnější	k2eAgFnSc1d1	vnější
část	část	k1gFnSc1	část
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
hlavu	hlava	k1gFnSc4	hlava
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc4	hruď
a	a	k8xC	a
končetiny	končetina	k1gFnPc4	končetina
jsou	být	k5eAaImIp3nP	být
jasně	jasně	k6eAd1	jasně
oranžové	oranžový	k2eAgFnPc1d1	oranžová
<g/>
,	,	kIx,	,
zobák	zobák	k1gInSc1	zobák
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
,	,	kIx,	,
skvrna	skvrna	k1gFnSc1	skvrna
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
krku	krk	k1gInSc2	krk
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zbarvena	zbarvit	k5eAaPmNgFnS	zbarvit
bíle	bíle	k6eAd1	bíle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	s	k7c7	s
přitom	přitom	k6eAd1	přitom
zbarvením	zbarvení	k1gNnSc7	zbarvení
nijak	nijak	k6eAd1	nijak
výrazně	výrazně	k6eAd1	výrazně
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
však	však	k9	však
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
samců	samec	k1gMnPc2	samec
mají	mít	k5eAaImIp3nP	mít
červeně	červeně	k6eAd1	červeně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
kořen	kořen	k1gInSc4	kořen
spodní	spodní	k2eAgFnSc2d1	spodní
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
jednotvárněji	jednotvárně	k6eAd2	jednotvárně
zbarveni	zbarven	k2eAgMnPc1d1	zbarven
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
opeření	opeření	k1gNnPc2	opeření
má	mít	k5eAaImIp3nS	mít
zelenavější	zelenavý	k2eAgInSc1d2	zelenavý
odstín	odstín	k1gInSc1	odstín
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Asie	Asie	k1gFnSc2	Asie
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
naprosto	naprosto	k6eAd1	naprosto
nezaměnitelný	zaměnitelný	k2eNgInSc4d1	nezaměnitelný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
zaměněn	zaměnit	k5eAaPmNgInS	zaměnit
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
6	[number]	k4	6
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
ledňáčků	ledňáček	k1gMnPc2	ledňáček
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
s	s	k7c7	s
ledňáčkem	ledňáček	k1gMnSc7	ledňáček
modrohřbetým	modrohřbetý	k2eAgNnSc7d1	modrohřbetý
(	(	kIx(	(
<g/>
Alcedo	Alcedo	k1gNnSc1	Alcedo
meninting	meninting	k1gInSc1	meninting
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gInSc1	jeho
let	let	k1gInSc1	let
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
(	(	kIx(	(
<g/>
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
rychlosti	rychlost	k1gFnPc4	rychlost
vyšší	vysoký	k2eAgFnPc4d2	vyšší
jak	jak	k8xC	jak
45	[number]	k4	45
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
přímý	přímý	k2eAgInSc1d1	přímý
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
určit	určit	k5eAaPmF	určit
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gNnSc3	jeho
jasně	jasně	k6eAd1	jasně
modře	modro	k6eAd1	modro
zbarvenému	zbarvený	k2eAgMnSc3d1	zbarvený
kostřci	kostřce	k1gMnSc3	kostřce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlasový	hlasový	k2eAgInSc4d1	hlasový
projev	projev	k1gInSc4	projev
==	==	k?	==
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
a	a	k8xC	a
nápadně	nápadně	k6eAd1	nápadně
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
krátkým	krátký	k2eAgInSc7d1	krátký
<g/>
,	,	kIx,	,
ostrým	ostrý	k2eAgInSc7d1	ostrý
a	a	k8xC	a
při	při	k7c6	při
vzrušení	vzrušení	k1gNnSc6	vzrušení
často	často	k6eAd1	často
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
tyt-tyt	tytyt	k1gInSc4	tyt-tyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgMnSc1d1	říční
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
severního	severní	k2eAgInSc2d1	severní
cípu	cíp	k1gInSc2	cíp
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
na	na	k7c4	na
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
území	území	k1gNnSc4	území
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
západní	západní	k2eAgFnSc2d1	západní
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaně	izolovaně	k6eAd1	izolovaně
obývá	obývat	k5eAaImIp3nS	obývat
také	také	k9	také
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
globální	globální	k2eAgInSc1d1	globální
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
přitom	přitom	k6eAd1	přitom
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
plochu	plocha	k1gFnSc4	plocha
velkou	velký	k2eAgFnSc4d1	velká
zhruba	zhruba	k6eAd1	zhruba
24	[number]	k4	24
900	[number]	k4	900
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Severské	severský	k2eAgFnPc1d1	severská
populace	populace	k1gFnPc1	populace
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
tažné	tažný	k2eAgFnPc1d1	tažná
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
jižních	jižní	k2eAgFnPc2d1	jižní
populací	populace	k1gFnPc2	populace
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
hnízdištích	hnízdiště	k1gNnPc6	hnízdiště
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tažní	tažní	k2eAgMnPc1d1	tažní
jedinci	jedinec	k1gMnPc1	jedinec
zimují	zimovat	k5eAaImIp3nP	zimovat
zejména	zejména	k9	zejména
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Středomoří	středomoří	k1gNnSc4	středomoří
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
migrují	migrovat	k5eAaImIp3nP	migrovat
až	až	k9	až
na	na	k7c4	na
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
africký	africký	k2eAgInSc4d1	africký
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Asijští	asijský	k2eAgMnPc1d1	asijský
ptáci	pták	k1gMnPc1	pták
pak	pak	k6eAd1	pak
zimují	zimovat	k5eAaImIp3nP	zimovat
zejména	zejména	k9	zejména
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Migruje	migrovat	k5eAaImIp3nS	migrovat
především	především	k9	především
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sibiřští	sibiřský	k2eAgMnPc1d1	sibiřský
ptáci	pták	k1gMnPc1	pták
musí	muset	k5eAaImIp3nP	muset
ročně	ročně	k6eAd1	ročně
zdolat	zdolat	k5eAaPmF	zdolat
i	i	k9	i
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
3000	[number]	k4	3000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Početnost	početnost	k1gFnSc4	početnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
nebyl	být	k5eNaImAgInS	být
u	u	k7c2	u
jeho	jeho	k3xOp3gFnSc2	jeho
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čítá	čítat	k5eAaImIp3nS	čítat
zhruba	zhruba	k6eAd1	zhruba
600	[number]	k4	600
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
zaznamenán	zaznamenán	k2eAgInSc4d1	zaznamenán
pokles	pokles	k1gInSc4	pokles
vyšší	vysoký	k2eAgInSc4d2	vyšší
jak	jak	k6eAd1	jak
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
jako	jako	k8xC	jako
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc4d1	dotčený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
pak	pak	k6eAd1	pak
čítá	čítat	k5eAaImIp3nS	čítat
67	[number]	k4	67
000	[number]	k4	000
–	–	k?	–
135	[number]	k4	135
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
početnost	početnost	k1gFnSc1	početnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
většinou	většinou	k6eAd1	většinou
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
odlišná	odlišný	k2eAgFnSc1d1	odlišná
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
teplotách	teplota	k1gFnPc6	teplota
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
zimě	zima	k1gFnSc6	zima
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkými	nízký	k2eAgFnPc7d1	nízká
teplotami	teplota	k1gFnPc7	teplota
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
jen	jen	k9	jen
osm	osm	k4xCc1	osm
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
během	během	k7c2	během
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
početnost	početnost	k1gFnSc1	početnost
na	na	k7c4	na
území	území	k1gNnSc4	území
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
až	až	k9	až
na	na	k7c4	na
45	[number]	k4	45
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
nepříznivou	příznivý	k2eNgFnSc4d1	nepříznivá
zimu	zima	k1gFnSc4	zima
přežilo	přežít	k5eAaPmAgNnS	přežít
pouze	pouze	k6eAd1	pouze
25	[number]	k4	25
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Vážnou	vážný	k2eAgFnSc4d1	vážná
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
představuje	představovat	k5eAaImIp3nS	představovat
i	i	k9	i
ničení	ničení	k1gNnSc4	ničení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biotopu	biotop	k1gInSc2	biotop
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
hnízdních	hnízdní	k2eAgNnPc2d1	hnízdní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Biotop	biotop	k1gInSc4	biotop
===	===	k?	===
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pomalu	pomalu	k6eAd1	pomalu
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
čistých	čistý	k2eAgFnPc2d1	čistá
vod	voda	k1gFnPc2	voda
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
mohou	moct	k5eAaImIp3nP	moct
ledňáčci	ledňáček	k1gMnPc1	ledňáček
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
jako	jako	k9	jako
bioindikátor	bioindikátor	k1gInSc4	bioindikátor
čisté	čistý	k2eAgFnSc2d1	čistá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
tak	tak	k6eAd1	tak
obývá	obývat	k5eAaImIp3nS	obývat
řeky	řeka	k1gFnPc4	řeka
<g/>
,	,	kIx,	,
potoky	potok	k1gInPc4	potok
<g/>
,	,	kIx,	,
rybníky	rybník	k1gInPc4	rybník
<g/>
,	,	kIx,	,
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
přehrady	přehrada	k1gFnSc2	přehrada
a	a	k8xC	a
mokřiny	mokřina	k1gFnSc2	mokřina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
často	často	k6eAd1	často
zalétává	zalétávat	k5eAaImIp3nS	zalétávat
i	i	k9	i
k	k	k7c3	k
poloslaným	poloslaný	k2eAgFnPc3d1	poloslaná
vodám	voda	k1gFnPc3	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgMnSc1d1	říční
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
populační	populační	k2eAgInSc1d1	populační
trend	trend	k1gInSc1	trend
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
mírném	mírný	k2eAgInSc6d1	mírný
vzestupu	vzestup	k1gInSc6	vzestup
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
jej	on	k3xPp3gMnSc4	on
řadíme	řadit	k5eAaImIp1nP	řadit
mezi	mezi	k7c4	mezi
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
přísně	přísně	k6eAd1	přísně
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
totiž	totiž	k9	totiž
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
700	[number]	k4	700
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
maximálně	maximálně	k6eAd1	maximálně
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
900	[number]	k4	900
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgMnSc1d1	říční
je	být	k5eAaImIp3nS	být
plachý	plachý	k2eAgInSc1d1	plachý
a	a	k8xC	a
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
samotářsky	samotářsky	k6eAd1	samotářsky
žijící	žijící	k2eAgMnSc1d1	žijící
a	a	k8xC	a
přísně	přísně	k6eAd1	přísně
teritoriální	teritoriální	k2eAgMnSc1d1	teritoriální
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
vnikne	vniknout	k5eAaPmIp3nS	vniknout
konkurent	konkurent	k1gMnSc1	konkurent
<g/>
,	,	kIx,	,
skloní	sklonit	k5eAaPmIp3nS	sklonit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
pomalu	pomalu	k6eAd1	pomalu
kývat	kývat	k5eAaImF	kývat
hlavou	hlava	k1gFnSc7	hlava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
roztahuje	roztahovat	k5eAaImIp3nS	roztahovat
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
soubojům	souboj	k1gInPc3	souboj
dochází	docházet	k5eAaImIp3nS	docházet
jen	jen	k6eAd1	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
však	však	k9	však
shození	shození	k1gNnSc2	shození
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
účastníků	účastník	k1gMnPc2	účastník
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
přidržení	přidržení	k1gNnSc1	přidržení
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
souboje	souboj	k1gInPc1	souboj
se	se	k3xPyFc4	se
však	však	k9	však
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
obcházejí	obcházet	k5eAaImIp3nP	obcházet
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgNnPc2	jakýkoli
vážných	vážný	k2eAgNnPc2d1	vážné
zranění	zranění	k1gNnPc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
ekologií	ekologie	k1gFnSc7	ekologie
a	a	k8xC	a
ochranou	ochrana	k1gFnSc7	ochrana
ledňáčka	ledňáček	k1gMnSc2	ledňáček
říčního	říční	k2eAgMnSc2d1	říční
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zabývá	zabývat	k5eAaImIp3nS	zabývat
program	program	k1gInSc1	program
ALCEDO	ALCEDO	kA	ALCEDO
(	(	kIx(	(
<g/>
celorepublikový	celorepublikový	k2eAgMnSc1d1	celorepublikový
garant	garant	k1gMnSc1	garant
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
ZO	ZO	kA	ZO
ČSOP	ČSOP	kA	ČSOP
Alcedo	Alcedo	k1gNnSc4	Alcedo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
vypracována	vypracován	k2eAgFnSc1d1	vypracována
podrobná	podrobný	k2eAgFnSc1d1	podrobná
metodika	metodika	k1gFnSc1	metodika
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
Metodika	metodika	k1gFnSc1	metodika
ČSOP	ČSOP	kA	ČSOP
č.	č.	k?	č.
34	[number]	k4	34
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
lov	lov	k1gInSc1	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
menšími	malý	k2eAgFnPc7d2	menší
rybami	ryba	k1gFnPc7	ryba
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
12	[number]	k4	12
cm	cm	kA	cm
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
potravě	potrava	k1gFnSc6	potrava
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
67	[number]	k4	67
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
kořist	kořist	k1gFnSc4	kořist
však	však	k9	však
představují	představovat	k5eAaImIp3nP	představovat
ryby	ryba	k1gFnPc1	ryba
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
cm	cm	kA	cm
a	a	k8xC	a
váze	váha	k1gFnSc6	váha
3	[number]	k4	3
gramy	gram	k1gInPc4	gram
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
vranky	vranka	k1gFnPc1	vranka
<g/>
,	,	kIx,	,
pstruzi	pstruh	k1gMnPc1	pstruh
<g/>
,	,	kIx,	,
štiky	štika	k1gFnPc1	štika
<g/>
,	,	kIx,	,
tloušti	tloušť	k1gMnPc1	tloušť
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
kaprovitých	kaprovitý	k2eAgMnPc2d1	kaprovitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
dominují	dominovat	k5eAaImIp3nP	dominovat
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
na	na	k7c6	na
tekoucích	tekoucí	k2eAgFnPc6d1	tekoucí
vodách	voda	k1gFnPc6	voda
zejména	zejména	k9	zejména
hrouzci	hrouzek	k1gMnPc1	hrouzek
<g/>
,	,	kIx,	,
jelci	jelec	k1gMnPc1	jelec
<g/>
,	,	kIx,	,
plotice	plotice	k1gFnPc1	plotice
a	a	k8xC	a
oukleje	ouklej	k1gFnPc1	ouklej
<g/>
,	,	kIx,	,
na	na	k7c6	na
nádržích	nádrž	k1gFnPc6	nádrž
okoun	okoun	k1gMnSc1	okoun
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
ryb	ryba	k1gFnPc2	ryba
požírá	požírat	k5eAaImIp3nS	požírat
i	i	k9	i
obojživelníky	obojživelník	k1gMnPc4	obojživelník
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
korýše	korýš	k1gMnPc4	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
monitoringu	monitoring	k1gInSc2	monitoring
provedeného	provedený	k2eAgInSc2d1	provedený
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1999	[number]	k4	1999
až	až	k8xS	až
2013	[number]	k4	2013
na	na	k7c6	na
patnácti	patnáct	k4xCc6	patnáct
hnízdních	hnízdní	k2eAgFnPc6d1	hnízdní
lokalitách	lokalita	k1gFnPc6	lokalita
zahrnujících	zahrnující	k2eAgFnPc6d1	zahrnující
vodní	vodní	k2eAgNnSc4d1	vodní
nádrže	nádrž	k1gFnPc4	nádrž
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnPc4	řeka
a	a	k8xC	a
potoky	potok	k1gInPc4	potok
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
nerybí	rybí	k2eNgFnSc4d1	rybí
kořist	kořist	k1gFnSc4	kořist
jen	jen	k6eAd1	jen
cca	cca	kA	cca
0,1	[number]	k4	0,1
%	%	kIx~	%
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
analyzováno	analyzovat	k5eAaImNgNnS	analyzovat
30	[number]	k4	30
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
pozměněné	pozměněný	k2eAgInPc1d1	pozměněný
vodní	vodní	k2eAgInPc1d1	vodní
ekosystémy	ekosystém	k1gInPc1	ekosystém
<g/>
,	,	kIx,	,
16	[number]	k4	16
933	[number]	k4	933
diagnostikovaných	diagnostikovaný	k2eAgMnPc2d1	diagnostikovaný
jedinců	jedinec	k1gMnPc2	jedinec
kořisti	kořist	k1gFnSc2	kořist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1	ledňáček
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
lovit	lovit	k5eAaImF	lovit
i	i	k9	i
mihule	mihule	k1gFnPc4	mihule
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInPc1d3	nejnovější
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
snůška	snůška	k1gFnSc1	snůška
ledňáčků	ledňáček	k1gMnPc2	ledňáček
<g/>
,	,	kIx,	,
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
opustí	opustit	k5eAaPmIp3nS	opustit
hnízdní	hnízdní	k2eAgFnSc4d1	hnízdní
kotlinku	kotlinka	k1gFnSc4	kotlinka
<g/>
,	,	kIx,	,
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
mláďat	mládě	k1gNnPc2	mládě
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
ks	ks	kA	ks
<g/>
)	)	kIx)	)
ryby	ryba	k1gFnPc1	ryba
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
váze	váha	k1gFnSc6	váha
1498	[number]	k4	1498
až	až	k9	až
2968	[number]	k4	2968
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
334	[number]	k4	334
gramů	gram	k1gInPc2	gram
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
kJ	kJ	k?	kJ
<g/>
)	)	kIx)	)
ryb	ryba	k1gFnPc2	ryba
na	na	k7c4	na
mládě	mládě	k1gNnSc4	mládě
za	za	k7c4	za
25	[number]	k4	25
dní	den	k1gInPc2	den
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgInSc1d1	denní
příjem	příjem	k1gInSc1	příjem
ryb	ryba	k1gFnPc2	ryba
jednoho	jeden	k4xCgNnSc2	jeden
mláděte	mládě	k1gNnSc2	mládě
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
37	[number]	k4	37
%	%	kIx~	%
váhy	váha	k1gFnSc2	váha
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ledňáček	ledňáček	k1gMnSc1	ledňáček
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
rychlým	rychlý	k2eAgNnSc7d1	rychlé
létáním	létání	k1gNnSc7	létání
hodně	hodně	k6eAd1	hodně
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nP	muset
spořádat	spořádat	k5eAaPmF	spořádat
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
celým	celý	k2eAgInSc7d1	celý
60	[number]	k4	60
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
většinou	většinou	k6eAd1	většinou
číhá	číhat	k5eAaImIp3nS	číhat
vsedě	vsedě	k6eAd1	vsedě
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
visící	visící	k2eAgMnSc1d1	visící
obvykle	obvykle	k6eAd1	obvykle
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
m	m	kA	m
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
žádné	žádný	k3yNgFnSc2	žádný
vhodné	vhodný	k2eAgFnSc2d1	vhodná
větve	větev	k1gFnSc2	větev
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
<g/>
,	,	kIx,	,
vyhlíží	vyhlížet	k5eAaImIp3nP	vyhlížet
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
za	za	k7c2	za
třepotavého	třepotavý	k2eAgInSc2d1	třepotavý
letu	let	k1gInSc2	let
(	(	kIx(	(
<g/>
takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
však	však	k9	však
stává	stávat	k5eAaImIp3nS	stávat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
3	[number]	k4	3
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kořist	kořist	k1gFnSc1	kořist
spatří	spatřit	k5eAaPmIp3nS	spatřit
<g/>
,	,	kIx,	,
vrhá	vrhat	k5eAaImIp3nS	vrhat
se	se	k3xPyFc4	se
rychlým	rychlý	k2eAgInSc7d1	rychlý
střemhlavým	střemhlavý	k2eAgInSc7d1	střemhlavý
letem	let	k1gInSc7	let
pod	pod	k7c4	pod
vodní	vodní	k2eAgFnSc4d1	vodní
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
uchopí	uchopit	k5eAaPmIp3nP	uchopit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
zobáku	zobák	k1gInSc2	zobák
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
pozorovatelnu	pozorovatelna	k1gFnSc4	pozorovatelna
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
má	mít	k5eAaImIp3nS	mít
oči	oko	k1gNnPc4	oko
otevřené	otevřený	k2eAgNnSc1d1	otevřené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chráněné	chráněný	k2eAgNnSc1d1	chráněné
jemnou	jemný	k2eAgFnSc7d1	jemná
blankou	blanka	k1gFnSc7	blanka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yIgFnSc3	který
nevidí	vidět	k5eNaImIp3nS	vidět
a	a	k8xC	a
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
se	se	k3xPyFc4	se
tak	tak	k9	tak
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
<g/>
.	.	kIx.	.
<g/>
Ulovenou	ulovený	k2eAgFnSc4d1	ulovená
potravu	potrava	k1gFnSc4	potrava
následně	následně	k6eAd1	následně
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
údery	úder	k1gInPc1	úder
o	o	k7c4	o
větev	větev	k1gFnSc4	větev
a	a	k8xC	a
polyká	polykat	k5eAaImIp3nS	polykat
ji	on	k3xPp3gFnSc4	on
hlavou	hlavý	k2eAgFnSc4d1	hlavá
napřed	napřed	k6eAd1	napřed
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
neporanil	poranit	k5eNaPmAgInS	poranit
o	o	k7c4	o
rybí	rybí	k2eAgFnPc4d1	rybí
šupiny	šupina	k1gFnPc4	šupina
a	a	k8xC	a
ploutve	ploutev	k1gFnPc4	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lovu	lov	k1gInSc6	lov
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
potopit	potopit	k5eAaPmF	potopit
i	i	k9	i
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
pod	pod	k7c4	pod
vodní	vodní	k2eAgFnSc4d1	vodní
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
hloubku	hloubka	k1gFnSc4	hloubka
25	[number]	k4	25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
lovu	lov	k1gInSc2	lov
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
jen	jen	k9	jen
totiž	totiž	k9	totiž
přibližně	přibližně	k6eAd1	přibližně
každý	každý	k3xTgInSc1	každý
desátý	desátý	k4xOgInSc1	desátý
pokus	pokus	k1gInSc1	pokus
je	být	k5eAaImIp3nS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
<g/>
Nestravitelných	stravitelný	k2eNgInPc2d1	nestravitelný
zbytků	zbytek	k1gInPc2	zbytek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malých	malý	k2eAgInPc2d1	malý
šedých	šedý	k2eAgInPc2d1	šedý
vývržků	vývržek	k1gInPc2	vývržek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
potravě	potrava	k1gFnSc3	potrava
ledňáčka	ledňáček	k1gMnSc2	ledňáček
říčního	říční	k2eAgMnSc2d1	říční
díky	díky	k7c3	díky
programu	program	k1gInSc3	program
ALCEDO	ALCEDO	kA	ALCEDO
věnována	věnován	k2eAgFnSc1d1	věnována
maximální	maximální	k2eAgFnSc1d1	maximální
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sledována	sledovat	k5eAaImNgFnS	sledovat
na	na	k7c6	na
údolních	údolní	k2eAgFnPc6d1	údolní
nádržích	nádrž	k1gFnPc6	nádrž
<g/>
,	,	kIx,	,
řekách	řeka	k1gFnPc6	řeka
a	a	k8xC	a
speciální	speciální	k2eAgFnSc1d1	speciální
pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
potravě	potrava	k1gFnSc6	potrava
ledňáčků	ledňáček	k1gMnPc2	ledňáček
na	na	k7c6	na
pstruhových	pstruhový	k2eAgInPc6d1	pstruhový
potocích	potok	k1gInPc6	potok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
ledňáčka	ledňáček	k1gMnSc2	ledňáček
říčního	říční	k2eAgMnSc2d1	říční
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
stáří	stáří	k1gNnSc3	stáří
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdění	hnízdění	k1gNnSc1	hnízdění
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
jediné	jediný	k2eAgNnSc1d1	jediné
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nezdržuje	zdržovat	k5eNaImIp3nS	zdržovat
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
pár	pár	k1gInSc1	pár
přitom	přitom	k6eAd1	přitom
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
teritorium	teritorium	k1gNnSc4	teritorium
velké	velká	k1gFnSc2	velká
až	až	k9	až
1	[number]	k4	1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
svými	svůj	k3xOyFgFnPc7	svůj
námluvami	námluva	k1gFnPc7	námluva
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgInPc6	který
samec	samec	k1gInSc1	samec
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
samičku	samička	k1gFnSc4	samička
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
střemhlav	střemhlav	k6eAd1	střemhlav
vrhají	vrhat	k5eAaImIp3nP	vrhat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
změní	změnit	k5eAaPmIp3nS	změnit
směr	směr	k1gInSc4	směr
a	a	k8xC	a
letí	letět	k5eAaImIp3nS	letět
vysoko	vysoko	k6eAd1	vysoko
ke	k	k7c3	k
korunám	koruna	k1gFnPc3	koruna
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ledňáččí	ledňáččí	k2eAgFnPc1d1	ledňáččí
zásnuby	zásnuba	k1gFnPc1	zásnuba
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
zásnubnímu	zásnubní	k2eAgInSc3d1	zásnubní
krmení	krmení	k1gNnSc1	krmení
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
přináší	přinášet	k5eAaImIp3nS	přinášet
samici	samice	k1gFnSc3	samice
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
uklání	uklánět	k5eAaImIp3nS	uklánět
se	se	k3xPyFc4	se
a	a	k8xC	a
předává	předávat	k5eAaImIp3nS	předávat
ji	on	k3xPp3gFnSc4	on
přímo	přímo	k6eAd1	přímo
samičce	samičko	k6eAd1	samičko
do	do	k7c2	do
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hloubí	hloubit	k5eAaImIp3nS	hloubit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
noru	nora	k1gFnSc4	nora
na	na	k7c6	na
příkrých	příkrý	k2eAgInPc6d1	příkrý
březích	břeh	k1gInPc6	břeh
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
bývá	bývat	k5eAaImIp3nS	bývat
umístěna	umístit	k5eAaPmNgFnS	umístit
vysoko	vysoko	k6eAd1	vysoko
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
až	až	k9	až
37	[number]	k4	37
m	m	kA	m
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
predátory	predátor	k1gMnPc7	predátor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
lasičky	lasička	k1gFnPc1	lasička
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gMnPc4	její
vybudování	vybudování	k1gNnSc1	vybudování
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
měkkou	měkký	k2eAgFnSc4d1	měkká
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
zobákem	zobák	k1gInSc7	zobák
naráží	narážet	k5eAaPmIp3nS	narážet
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
neobjeví	objevit	k5eNaPmIp3nS	objevit
důlek	důlek	k1gInSc1	důlek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečném	konečný	k2eAgNnSc6d1	konečné
stádiu	stádium	k1gNnSc6	stádium
nora	nora	k1gFnSc1	nora
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
měřit	měřit	k5eAaImF	měřit
až	až	k9	až
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
komůrka	komůrka	k1gFnSc1	komůrka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
hnízda	hnízdo	k1gNnSc2	hnízdo
trus	trus	k1gInSc1	trus
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
noru	nora	k1gFnSc4	nora
obývají	obývat	k5eAaImIp3nP	obývat
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
od	od	k7c2	od
května	květen	k1gInSc2	květen
až	až	k6eAd1	až
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
má	mít	k5eAaImIp3nS	mít
přitom	přitom	k6eAd1	přitom
obvykle	obvykle	k6eAd1	obvykle
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
snůšky	snůška	k1gFnSc2	snůška
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
,	,	kIx,	,
22	[number]	k4	22
×	×	k?	×
19	[number]	k4	19
mm	mm	kA	mm
velkých	velká	k1gFnPc2	velká
a	a	k8xC	a
4,3	[number]	k4	4,3
g	g	kA	g
těžkých	těžký	k2eAgNnPc2d1	těžké
vajec	vejce	k1gNnPc2	vejce
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5	[number]	k4	5
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
skořápka	skořápka	k1gFnSc1	skořápka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
sezení	sezení	k1gNnSc6	sezení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
střídavě	střídavě	k6eAd1	střídavě
podílí	podílet	k5eAaImIp3nS	podílet
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
noci	noc	k1gFnPc4	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
inkubaci	inkubace	k1gFnSc4	inkubace
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
holá	holý	k2eAgFnSc1d1	holá
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
měří	měřit	k5eAaImIp3nS	měřit
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
je	on	k3xPp3gNnPc4	on
rodiče	rodič	k1gMnPc1	rodič
krmí	krmit	k5eAaImIp3nP	krmit
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
již	již	k6eAd1	již
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
opouští	opouštět	k5eAaImIp3nS	opouštět
po	po	k7c6	po
23	[number]	k4	23
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
náchylná	náchylný	k2eAgFnSc1d1	náchylná
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
promoknutí	promoknutí	k1gNnSc3	promoknutí
peří	peří	k1gNnSc2	peří
se	se	k3xPyFc4	se
často	často	k6eAd1	často
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
lov	lov	k1gInSc4	lov
kořisti	kořist	k1gFnSc2	kořist
utopí	utopit	k5eAaPmIp3nP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Dospělosti	dospělost	k1gFnPc1	dospělost
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
jen	jen	k9	jen
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
nejstarší	starý	k2eAgMnSc1d3	nejstarší
zaznamenaný	zaznamenaný	k2eAgMnSc1d1	zaznamenaný
jedinec	jedinec	k1gMnSc1	jedinec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Ledňáčci	ledňáček	k1gMnPc1	ledňáček
jsou	být	k5eAaImIp3nP	být
malí	malý	k2eAgMnPc1d1	malý
ptáci	pták	k1gMnPc1	pták
tvořící	tvořící	k2eAgFnSc4d1	tvořící
čeleď	čeleď	k1gFnSc4	čeleď
ledňáčkovitých	ledňáčkovitý	k2eAgFnPc2d1	ledňáčkovitý
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
samostatné	samostatný	k2eAgFnPc4d1	samostatná
podčeledi	podčeleď	k1gFnPc4	podčeleď
–	–	k?	–
Halcyoninae	Halcyonina	k1gFnSc2	Halcyonina
<g/>
,	,	kIx,	,
Cerylinae	Cerylina	k1gFnSc2	Cerylina
a	a	k8xC	a
Alcedininae	Alcedinina	k1gFnSc2	Alcedinina
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgInSc4d3	nejpočetnější
rod	rod	k1gInSc4	rod
třetí	třetí	k4xOgFnSc2	třetí
zmiňované	zmiňovaný	k2eAgFnSc2d1	zmiňovaná
podčeledi	podčeleď	k1gFnSc2	podčeleď
<g/>
,	,	kIx,	,
Alcedo	Alcedo	k1gNnSc1	Alcedo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
řadíme	řadit	k5eAaImIp1nP	řadit
i	i	k9	i
ledňáčka	ledňáček	k1gMnSc2	ledňáček
říčního	říční	k2eAgMnSc2d1	říční
<g/>
,	,	kIx,	,
čítá	čítat	k5eAaImIp3nS	čítat
celkem	celkem	k6eAd1	celkem
17	[number]	k4	17
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Alcedo	Alcedo	k1gNnSc1	Alcedo
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
latinský	latinský	k2eAgInSc1d1	latinský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
ledňáčka	ledňáček	k1gMnSc4	ledňáček
<g/>
,	,	kIx,	,
Atthis	Atthis	k1gFnSc4	Atthis
zase	zase	k9	zase
jméno	jméno	k1gNnSc4	jméno
krásné	krásný	k2eAgFnSc2d1	krásná
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Lesbos	Lesbos	k1gInSc1	Lesbos
a	a	k8xC	a
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
známé	známý	k2eAgFnSc2d1	známá
básníkářky	básníkářka	k1gFnSc2	básníkářka
Sapfó	Sapfó	k1gFnSc2	Sapfó
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nejblíže	blízce	k6eAd3	blízce
příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
druhy	druh	k1gInPc7	druh
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
ledňáček	ledňáček	k1gMnSc1	ledňáček
modrohřbetý	modrohřbetý	k2eAgMnSc1d1	modrohřbetý
<g/>
,	,	kIx,	,
límcový	límcový	k2eAgMnSc1d1	límcový
a	a	k8xC	a
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
.	.	kIx.	.
<g/>
Druh	druh	k1gMnSc1	druh
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
švédský	švédský	k2eAgMnSc1d1	švédský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc3	Linná
pod	pod	k7c7	pod
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Alcedo	Alcedo	k1gNnSc1	Alcedo
ispida	ispida	k1gFnSc1	ispida
<g/>
.	.	kIx.	.
<g/>
Většinou	většina	k1gFnSc7	většina
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
sedm	sedm	k4xCc4	sedm
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
přehled	přehled	k1gInSc4	přehled
níže	níže	k1gFnPc4	níže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nP	uvádět
poddruhů	poddruh	k1gInPc2	poddruh
i	i	k8xC	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
athis	athis	k1gInSc1	athis
(	(	kIx(	(
<g/>
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc1d1	široký
pás	pás	k1gInSc1	pás
od	od	k7c2	od
Španělska	Španělsko	k1gNnSc2	Španělsko
po	po	k7c4	po
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
,	,	kIx,	,
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
,	,	kIx,	,
Ománu	Omán	k1gInSc6	Omán
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
bengalensis	bengalensis	k1gInSc1	bengalensis
Gmelin	Gmelina	k1gFnPc2	Gmelina
<g/>
,	,	kIx,	,
1788	[number]	k4	1788
<g/>
:	:	kIx,	:
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
Sundských	Sundský	k2eAgInPc6d1	Sundský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Molukách	Moluky	k1gFnPc6	Moluky
a	a	k8xC	a
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
floresiana	floresiana	k1gFnSc1	floresiana
Sharpe	sharp	k1gInSc5	sharp
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
:	:	kIx,	:
Bali	Bal	k1gFnPc1	Bal
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgInPc1d1	Malé
Sundy	sund	k1gInPc1	sund
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Wetar	Wetara	k1gFnPc2	Wetara
a	a	k8xC	a
Timor	Timora	k1gFnPc2	Timora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
hispidoides	hispidoides	k1gMnSc1	hispidoides
Lesson	Lesson	k1gMnSc1	Lesson
<g/>
,	,	kIx,	,
1837	[number]	k4	1837
<g/>
:	:	kIx,	:
Sulawesi	Sulawesi	k1gNnSc1	Sulawesi
<g/>
,	,	kIx,	,
Moluky	Moluky	k1gFnPc1	Moluky
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Bismarckovo	Bismarckův	k2eAgNnSc1d1	Bismarckovo
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
ispida	ispida	k1gFnSc1	ispida
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
:	:	kIx,	:
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
severněji	severně	k6eAd2	severně
než	než	k8xS	než
A.	A.	kA	A.
a.	a.	k?	a.
athis	athis	k1gInSc1	athis
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc1d1	britský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc6	Kypr
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
salomonensis	salomonensis	k1gInSc1	salomonensis
Rothschild	Rothschild	k1gMnSc1	Rothschild
&	&	k?	&
Hartert	Hartert	k1gMnSc1	Hartert
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
:	:	kIx,	:
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A.	A.	kA	A.
a.	a.	k?	a.
taprobana	taproban	k1gMnSc2	taproban
O.	O.	kA	O.
Kleinschmidt	Kleinschmidt	k1gMnSc1	Kleinschmidt
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
:	:	kIx,	:
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Common	Commona	k1gFnPc2	Commona
Kingfisher	Kingfishra	k1gFnPc2	Kingfishra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Eisvogel	Eisvogel	k1gInSc4	Eisvogel
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ledňáčkovití	Ledňáčkovitý	k2eAgMnPc1d1	Ledňáčkovitý
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
přísně	přísně	k6eAd1	přísně
chráněných	chráněný	k2eAgInPc2d1	chráněný
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
podle	podle	k7c2	podle
Bernské	bernský	k2eAgFnSc2d1	Bernská
úmluvy	úmluva	k1gFnSc2	úmluva
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ledňáček	ledňáček	k1gMnSc1	ledňáček
říční	říční	k2eAgMnSc1d1	říční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Alcedo	Alcedo	k1gNnSc1	Alcedo
atthis	atthis	k1gInSc1	atthis
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
http://www.biolib.cz/cz/taxon/id8804/	[url]	k4	http://www.biolib.cz/cz/taxon/id8804/
</s>
</p>
<p>
<s>
http://www.naturfoto.cz/lednacek-ricni-fotografie-93.html	[url]	k1gMnSc1	http://www.naturfoto.cz/lednacek-ricni-fotografie-93.html
</s>
</p>
<p>
<s>
http://www.vesmir.cz/clanek/lednacek-ricni-ptakem-roku-2000	[url]	k4	http://www.vesmir.cz/clanek/lednacek-ricni-ptakem-roku-2000
</s>
</p>
