<s>
Bopomofo	Bopomofo	k1gNnSc1	Bopomofo
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
注	注	k?	注
<g/>
,	,	kIx,	,
ču-jin	čuin	k1gMnSc1	ču-jin
fu-chao	fuhao	k1gMnSc1	fu-chao
<g/>
,	,	kIx,	,
zhuyin	zhuyin	k2eAgMnSc1d1	zhuyin
fuhao	fuhao	k1gMnSc1	fuhao
–	–	k?	–
"	"	kIx"	"
<g/>
fonetické	fonetický	k2eAgInPc1d1	fonetický
znaky	znak	k1gInPc1	znak
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
fonetická	fonetický	k2eAgFnSc1d1	fonetická
alternativa	alternativa	k1gFnSc1	alternativa
znakového	znakový	k2eAgNnSc2d1	znakové
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
Výborem	výbor	k1gInSc7	výbor
pro	pro	k7c4	pro
sjednocení	sjednocení	k1gNnSc4	sjednocení
výslovnosti	výslovnost	k1gFnSc2	výslovnost
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
pevninské	pevninský	k2eAgFnSc6d1	pevninská
Číně	Čína	k1gFnSc6	Čína
vytlačováno	vytlačovat	k5eAaImNgNnS	vytlačovat
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
(	(	kIx(	(
<g/>
pinyinem	pinyino	k1gNnSc7	pinyino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
upravenou	upravený	k2eAgFnSc7d1	upravená
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
se	se	k3xPyFc4	se
však	však	k9	však
bopomofo	bopomofo	k6eAd1	bopomofo
stále	stále	k6eAd1	stále
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výuce	výuka	k1gFnSc3	výuka
znaků	znak	k1gInPc2	znak
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
způsob	způsob	k1gInSc1	způsob
zadávání	zadávání	k1gNnSc2	zadávání
čínského	čínský	k2eAgInSc2d1	čínský
textu	text	k1gInSc2	text
do	do	k7c2	do
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
bopomofa	bopomof	k1gMnSc2	bopomof
upouští	upouštět	k5eAaImIp3nS	upouštět
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
latinky	latinka	k1gFnSc2	latinka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
tchajwanský	tchajwanský	k2eAgInSc1d1	tchajwanský
pchin-jin	pchinin	k1gInSc1	pchin-jin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
primární	primární	k2eAgNnSc1d1	primární
písmo	písmo	k1gNnSc1	písmo
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
domorodé	domorodý	k2eAgInPc4d1	domorodý
tchaj-wanské	tchajanský	k2eAgInPc4d1	tchaj-wanský
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
také	také	k9	také
založena	založen	k2eAgFnSc1d1	založena
čínská	čínský	k2eAgFnSc1d1	čínská
varianta	varianta	k1gFnSc1	varianta
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bopomofa	bopomof	k1gMnSc2	bopomof
nebylo	být	k5eNaImAgNnS	být
nahradit	nahradit	k5eAaPmF	nahradit
znakové	znakový	k2eAgNnSc4d1	znakové
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožnit	umožnit	k5eAaPmF	umožnit
pomocný	pomocný	k2eAgInSc4d1	pomocný
fonetický	fonetický	k2eAgInSc4d1	fonetický
přepis	přepis	k1gInSc4	přepis
čínštiny	čínština	k1gFnSc2	čínština
pro	pro	k7c4	pro
vědecké	vědecký	k2eAgInPc4d1	vědecký
a	a	k8xC	a
výukové	výukový	k2eAgInPc4d1	výukový
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc4	znak
písma	písmo	k1gNnSc2	písmo
bopomofo	bopomofo	k6eAd1	bopomofo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
kaligraf	kaligraf	k1gMnSc1	kaligraf
Čang	Čang	k1gMnSc1	Čang
Ping-lin	Pingin	k1gInSc4	Ping-lin
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
čínských	čínský	k2eAgMnPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
různých	různý	k2eAgFnPc2d1	různá
starobylých	starobylý	k2eAgFnPc2d1	starobylá
a	a	k8xC	a
zaniklých	zaniklý	k2eAgFnPc2d1	zaniklá
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
třicet	třicet	k4xCc4	třicet
sedm	sedm	k4xCc4	sedm
<g/>
,	,	kIx,	,
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
hlásky	hláska	k1gFnPc1	hláska
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
hláskové	hláskový	k2eAgFnPc1d1	hlásková
skupiny	skupina	k1gFnPc1	skupina
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zapsat	zapsat	k5eAaPmF	zapsat
všechny	všechen	k3xTgFnPc4	všechen
čínské	čínský	k2eAgFnPc4d1	čínská
slabiky	slabika	k1gFnPc4	slabika
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
znaků	znak	k1gInPc2	znak
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
nářečních	nářeční	k2eAgFnPc2d1	nářeční
hlásek	hláska	k1gFnPc2	hláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tóny	Tóny	k1gMnSc1	Tóny
se	se	k3xPyFc4	se
v	v	k7c4	v
bopomofo	bopomofo	k1gNnSc4	bopomofo
značí	značit	k5eAaImIp3nS	značit
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
