<s>
Divadlo	divadlo	k1gNnSc1	divadlo
je	být	k5eAaImIp3nS	být
výkonné	výkonný	k2eAgNnSc1d1	výkonné
čili	čili	k8xC	čili
múzické	múzický	k2eAgNnSc1d1	múzické
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
herci	herc	k1gInSc6	herc
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
zpěváci	zpěvák	k1gMnPc1	zpěvák
a	a	k8xC	a
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
)	)	kIx)	)
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
předvádějí	předvádět	k5eAaImIp3nP	předvádět
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
