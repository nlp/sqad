<s>
Kometa	kometa	k1gFnSc1	kometa
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
vlasatice	vlasatice	k1gFnSc1	vlasatice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
těleso	těleso	k1gNnSc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
složené	složený	k2eAgNnSc1d1	složené
především	především	k9	především
z	z	k7c2	z
ledu	led	k1gInSc2	led
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
obíhající	obíhající	k2eAgFnSc7d1	obíhající
většinou	většina	k1gFnSc7	většina
po	po	k7c6	po
velice	velice	k6eAd1	velice
výstředné	výstředný	k2eAgFnSc6d1	výstředná
(	(	kIx(	(
<g/>
excentrické	excentrický	k2eAgFnSc6d1	excentrická
<g/>
)	)	kIx)	)
eliptické	eliptický	k2eAgFnSc6d1	eliptická
trajektorii	trajektorie	k1gFnSc6	trajektorie
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Komety	kometa	k1gFnPc1	kometa
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
nápadné	nápadný	k2eAgInPc4d1	nápadný
ohony	ohon	k1gInPc4	ohon
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
komet	kometa	k1gFnPc2	kometa
se	se	k3xPyFc4	se
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
občas	občas	k6eAd1	občas
nějaká	nějaký	k3yIgFnSc1	nějaký
přilétne	přilétnout	k5eAaPmIp3nS	přilétnout
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
částí	část	k1gFnPc2	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
popisované	popisovaný	k2eAgInPc1d1	popisovaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
špinavé	špinavý	k2eAgFnPc1d1	špinavá
sněhové	sněhový	k2eAgFnPc1d1	sněhová
koule	koule	k1gFnPc1	koule
<g/>
"	"	kIx"	"
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
je	on	k3xPp3gFnPc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
zmrzlý	zmrzlý	k2eAgInSc4d1	zmrzlý
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
metan	metan	k1gInSc1	metan
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
smíchaná	smíchaný	k2eAgFnSc1d1	smíchaná
s	s	k7c7	s
prachem	prach	k1gInSc7	prach
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
nerostnými	nerostný	k2eAgFnPc7d1	nerostná
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
gravitační	gravitační	k2eAgFnSc6d1	gravitační
interakci	interakce	k1gFnSc6	interakce
komety	kometa	k1gFnSc2	kometa
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
se	se	k3xPyFc4	se
dráha	dráha	k1gFnSc1	dráha
komet	kometa	k1gFnPc2	kometa
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
na	na	k7c4	na
hyperbolickou	hyperbolický	k2eAgFnSc4d1	hyperbolická
(	(	kIx(	(
<g/>
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
opustit	opustit	k5eAaPmF	opustit
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c4	na
méně	málo	k6eAd2	málo
výstřednou	výstředný	k2eAgFnSc4d1	výstředná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mění	měnit	k5eAaImIp3nP	měnit
dráhy	dráha	k1gFnPc1	dráha
komet	kometa	k1gFnPc2	kometa
a	a	k8xC	a
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
oběžných	oběžný	k2eAgFnPc6d1	oběžná
dráhách	dráha	k1gFnPc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
komety	kometa	k1gFnPc1	kometa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
vrací	vracet	k5eAaImIp3nS	vracet
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Halleyova	Halleyův	k2eAgFnSc1d1	Halleyova
<g/>
,	,	kIx,	,
Hale-Bopp	Hale-Bopp	k1gInSc1	Hale-Bopp
nebo	nebo	k8xC	nebo
Kohoutkova	Kohoutkův	k2eAgFnSc1d1	Kohoutkova
kometa	kometa	k1gFnSc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
znamená	znamenat	k5eAaImIp3nS	znamenat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
až	až	k8xS	až
staletí	staletí	k1gNnPc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
–	–	k?	–
pevná	pevný	k2eAgFnSc1d1	pevná
část	část	k1gFnSc1	část
komety	kometa	k1gFnSc2	kometa
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
kilometrů	kilometr	k1gInPc2	kilometr
až	až	k8xS	až
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Koma	koma	k1gFnSc1	koma
–	–	k?	–
kulová	kulový	k2eAgFnSc1d1	kulová
obálka	obálka	k1gFnSc1	obálka
kolem	kolem	k7c2	kolem
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
složena	složen	k2eAgFnSc1d1	složena
především	především	k9	především
z	z	k7c2	z
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ohon	ohon	k1gInSc1	ohon
–	–	k?	–
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
směřující	směřující	k2eAgFnSc1d1	směřující
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
též	též	k9	též
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
chvost	chvost	k1gInSc1	chvost
nebo	nebo	k8xC	nebo
ocas	ocas	k1gInSc1	ocas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
tuhého	tuhý	k2eAgInSc2d1	tuhý
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
zmrzlých	zmrzlý	k2eAgInPc2d1	zmrzlý
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Koma	koma	k1gFnSc1	koma
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
různé	různý	k2eAgFnPc4d1	různá
nedisociované	disociovaný	k2eNgFnPc4d1	nedisociovaná
i	i	k8xC	i
disociované	disociovaný	k2eAgFnPc4d1	disociovaná
molekuly	molekula	k1gFnPc4	molekula
<g/>
,	,	kIx,	,
radikály	radikál	k1gInPc4	radikál
a	a	k8xC	a
ionty	ion	k1gInPc4	ion
<g/>
,	,	kIx,	,
např.	např.	kA	např.
OH-	OH-	k1gFnSc4	OH-
<g/>
,	,	kIx,	,
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
CO	co	k3yQnSc1	co
<g/>
,	,	kIx,	,
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
CN	CN	kA	CN
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
aj.	aj.	kA	aj.
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kometární	kometární	k2eAgInSc4d1	kometární
materiál	materiál	k1gInSc4	materiál
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
udělat	udělat	k5eAaPmF	udělat
i	i	k9	i
doma	doma	k6eAd1	doma
<g/>
:	:	kIx,	:
vezměte	vzít	k5eAaPmRp2nP	vzít
trochu	trochu	k6eAd1	trochu
vody	voda	k1gFnPc4	voda
<g/>
,	,	kIx,	,
smíchejte	smíchat	k5eAaPmRp2nP	smíchat
s	s	k7c7	s
tonerem	toner	k1gInSc7	toner
z	z	k7c2	z
tiskárny	tiskárna	k1gFnSc2	tiskárna
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
přidejte	přidat	k5eAaPmRp2nP	přidat
trochu	trocha	k1gFnSc4	trocha
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
slin	slina	k1gFnPc2	slina
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
směs	směs	k1gFnSc4	směs
promíchejte	promíchat	k5eAaPmRp2nP	promíchat
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
(	(	kIx(	(
<g/>
suchým	suchý	k2eAgInSc7d1	suchý
ledem	led	k1gInSc7	led
<g/>
)	)	kIx)	)
a	a	k8xC	a
nechte	nechat	k5eAaPmRp2nP	nechat
zmrznout	zmrznout	k5eAaPmF	zmrznout
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
Oortově	Oortův	k2eAgNnSc6d1	Oortovo
mračnu	mračno	k1gNnSc6	mračno
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
spojováním	spojování	k1gNnSc7	spojování
zbytků	zbytek	k1gInPc2	zbytek
po	po	k7c6	po
kondenzaci	kondenzace	k1gFnSc6	kondenzace
sluneční	sluneční	k2eAgFnSc2d1	sluneční
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc1	okraj
takovýchto	takovýto	k3xDgFnPc2	takovýto
mlhovin	mlhovina	k1gFnPc2	mlhovina
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
chladné	chladný	k2eAgFnPc1d1	chladná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
mohla	moct	k5eAaImAgFnS	moct
existovat	existovat	k5eAaImF	existovat
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
plynném	plynný	k2eAgNnSc6d1	plynné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Planetky	planetka	k1gFnPc1	planetka
vznikají	vznikat	k5eAaImIp3nP	vznikat
jiným	jiný	k2eAgInSc7d1	jiný
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgFnPc1d1	stará
komety	kometa	k1gFnPc1	kometa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ztratily	ztratit	k5eAaPmAgFnP	ztratit
všechnu	všechen	k3xTgFnSc4	všechen
svoji	svůj	k3xOyFgFnSc4	svůj
těkavou	těkavý	k2eAgFnSc4d1	těkavá
hmotu	hmota	k1gFnSc4	hmota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
mohou	moct	k5eAaImIp3nP	moct
podobat	podobat	k5eAaImF	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
–	–	k?	–
přesněji	přesně	k6eAd2	přesně
kometární	kometární	k2eAgNnPc4d1	kometární
jádra	jádro	k1gNnPc4	jádro
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
ve	v	k7c6	v
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
oblaku	oblak	k1gInSc6	oblak
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xC	jako
Oortův	Oortův	k2eAgInSc1d1	Oortův
oblak	oblak	k1gInSc1	oblak
(	(	kIx(	(
<g/>
pojmenovaném	pojmenovaný	k2eAgInSc6d1	pojmenovaný
podle	podle	k7c2	podle
holandského	holandský	k2eAgMnSc2d1	holandský
astronoma	astronom	k1gMnSc2	astronom
Jana	Jan	k1gMnSc2	Jan
Hendrika	Hendrik	k1gMnSc2	Hendrik
Oorta	Oort	k1gMnSc2	Oort
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
kolem	kolem	k7c2	kolem
50	[number]	k4	50
000	[number]	k4	000
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgNnSc1d1	gravitační
působení	působení	k1gNnSc1	působení
Slunce	slunce	k1gNnSc2	slunce
již	již	k9	již
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgNnSc1d1	slabé
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c4	na
komety	kometa	k1gFnPc4	kometa
významně	významně	k6eAd1	významně
působí	působit	k5eAaImIp3nP	působit
i	i	k9	i
jiná	jiný	k2eAgNnPc1d1	jiné
vesmírná	vesmírný	k2eAgNnPc1d1	vesmírné
tělesa	těleso	k1gNnPc1	těleso
–	–	k?	–
především	především	k6eAd1	především
okolní	okolní	k2eAgFnPc1d1	okolní
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
přiblíží	přiblížit	k5eAaPmIp3nP	přiblížit
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vymrští	vymrštit	k5eAaPmIp3nS	vymrštit
množství	množství	k1gNnSc1	množství
komet	kometa	k1gFnPc2	kometa
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
dostanou	dostat	k5eAaPmIp3nP	dostat
na	na	k7c4	na
extrémně	extrémně	k6eAd1	extrémně
protáhlou	protáhlý	k2eAgFnSc4d1	protáhlá
eliptickou	eliptický	k2eAgFnSc4d1	eliptická
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
perihel	perihel	k1gInSc4	perihel
(	(	kIx(	(
<g/>
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
bod	bod	k1gInSc4	bod
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k6eAd1	blízko
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
kometa	kometa	k1gFnSc1	kometa
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
části	část	k1gFnSc3	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
zahřívání	zahřívání	k1gNnSc4	zahřívání
jejího	její	k3xOp3gNnSc2	její
jádra	jádro	k1gNnSc2	jádro
Sluncem	slunce	k1gNnSc7	slunce
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
vnější	vnější	k2eAgFnPc1d1	vnější
ledové	ledový	k2eAgFnPc1d1	ledová
vrstvy	vrstva	k1gFnPc1	vrstva
začnou	začít	k5eAaPmIp3nP	začít
vypařovat	vypařovat	k5eAaImF	vypařovat
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
uvolněné	uvolněný	k2eAgInPc1d1	uvolněný
proudy	proud	k1gInPc1	proud
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
extrémně	extrémně	k6eAd1	extrémně
řídkou	řídký	k2eAgFnSc4d1	řídká
atmosféru	atmosféra	k1gFnSc4	atmosféra
okolo	okolo	k7c2	okolo
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
koma	koma	k1gNnSc1	koma
<g/>
,	,	kIx,	,
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c6	na
komu	kdo	k3yRnSc3	kdo
působí	působit	k5eAaImIp3nP	působit
sluneční	sluneční	k2eAgInSc4d1	sluneční
vítr	vítr	k1gInSc4	vítr
<g/>
,	,	kIx,	,
způsobí	způsobit	k5eAaPmIp3nS	způsobit
vytvoření	vytvoření	k1gNnSc1	vytvoření
ohonu	ohon	k1gInSc2	ohon
mířícího	mířící	k2eAgInSc2d1	mířící
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Prach	prach	k1gInSc4	prach
a	a	k8xC	a
plyn	plyn	k1gInSc4	plyn
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
samostatné	samostatný	k2eAgInPc1d1	samostatný
ohony	ohon	k1gInPc1	ohon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
míří	mířit	k5eAaImIp3nP	mířit
do	do	k7c2	do
mírně	mírně	k6eAd1	mírně
odlišných	odlišný	k2eAgInPc2d1	odlišný
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prach	prach	k1gInSc1	prach
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vzadu	vzadu	k6eAd1	vzadu
za	za	k7c7	za
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
komety	kometa	k1gFnSc2	kometa
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
takto	takto	k6eAd1	takto
vzniká	vznikat	k5eAaImIp3nS	vznikat
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
ohon	ohon	k1gInSc1	ohon
<g/>
)	)	kIx)	)
a	a	k8xC	a
ohon	ohon	k1gInSc1	ohon
z	z	k7c2	z
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
vždy	vždy	k6eAd1	vždy
míří	mířit	k5eAaImIp3nS	mířit
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
silněji	silně	k6eAd2	silně
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
než	než	k8xS	než
prach	prach	k1gInSc1	prach
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
čáry	čára	k1gFnPc4	čára
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
ne	ne	k9	ne
trajektorii	trajektorie	k1gFnSc3	trajektorie
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
pevné	pevný	k2eAgNnSc1d1	pevné
těleso	těleso	k1gNnSc1	těleso
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
50	[number]	k4	50
km	km	kA	km
<g/>
,	,	kIx,	,
koma	koma	k1gFnSc1	koma
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
ohony	ohon	k1gInPc1	ohon
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
délky	délka	k1gFnPc4	délka
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Komu	kdo	k3yRnSc3	kdo
i	i	k9	i
ohon	ohon	k1gInSc4	ohon
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pozorovatelné	pozorovatelný	k2eAgFnPc4d1	pozorovatelná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
když	když	k8xS	když
kometa	kometa	k1gFnSc1	kometa
prolétá	prolétat	k5eAaImIp3nS	prolétat
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
částí	část	k1gFnSc7	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
prach	prach	k1gInSc1	prach
odráží	odrážet	k5eAaImIp3nS	odrážet
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
plyny	plyn	k1gInPc1	plyn
září	září	k1gNnSc2	září
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ionizace	ionizace	k1gFnSc2	ionizace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
komet	kometa	k1gFnPc2	kometa
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
dalekohledu	dalekohled	k1gInSc2	dalekohled
příliš	příliš	k6eAd1	příliš
slabě	slabě	k6eAd1	slabě
viditelná	viditelný	k2eAgFnSc1d1	viditelná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
jasných	jasný	k2eAgFnPc2d1	jasná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
viditelné	viditelný	k2eAgFnSc2d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vynálezem	vynález	k1gInSc7	vynález
dalekohledu	dalekohled	k1gInSc2	dalekohled
se	se	k3xPyFc4	se
komety	kometa	k1gFnPc1	kometa
zdánlivě	zdánlivě	k6eAd1	zdánlivě
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
nic	nic	k3yNnSc1	nic
zjevovaly	zjevovat	k5eAaImAgFnP	zjevovat
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
mizely	mizet	k5eAaImAgFnP	mizet
z	z	k7c2	z
dohledu	dohled	k1gInSc2	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
zlé	zlý	k2eAgNnSc4d1	zlé
znamení	znamení	k1gNnSc4	znamení
smrti	smrt	k1gFnSc2	smrt
králů	král	k1gMnPc2	král
a	a	k8xC	a
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
blížících	blížící	k2eAgFnPc2d1	blížící
se	se	k3xPyFc4	se
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starověkých	starověký	k2eAgInPc2d1	starověký
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
čínských	čínský	k2eAgFnPc2d1	čínská
kostí	kost	k1gFnPc2	kost
pro	pro	k7c4	pro
předpovídání	předpovídání	k1gNnSc4	předpovídání
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInPc1	jejich
výskyty	výskyt	k1gInPc1	výskyt
byly	být	k5eAaImAgInP	být
pozorované	pozorovaný	k2eAgMnPc4d1	pozorovaný
lidmi	člověk	k1gMnPc7	člověk
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
starých	starý	k2eAgInPc2d1	starý
záznamů	záznam	k1gInPc2	záznam
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
na	na	k7c6	na
Bayeuxském	Bayeuxský	k2eAgInSc6d1	Bayeuxský
gobelínu	gobelín	k1gInSc6	gobelín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
normanský	normanský	k2eAgInSc1d1	normanský
tábor	tábor	k1gInSc1	tábor
při	při	k7c6	při
dobytí	dobytí	k1gNnSc6	dobytí
Anglie	Anglie	k1gFnSc2	Anglie
roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
<g/>
.	.	kIx.	.
</s>
<s>
Překvapením	překvapení	k1gNnSc7	překvapení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
kometární	kometární	k2eAgNnPc1d1	kometární
jádra	jádro	k1gNnPc1	jádro
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejčernější	černý	k2eAgInPc4d3	nejčernější
známé	známý	k2eAgInPc4d1	známý
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgInPc6	který
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Giotto	Giotto	k1gNnSc4	Giotto
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
odráží	odrážet	k5eAaImIp3nS	odrážet
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
%	%	kIx~	%
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dopadá	dopadat	k5eAaImIp3nS	dopadat
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gFnSc1	Space
1	[number]	k4	1
podobně	podobně	k6eAd1	podobně
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
komety	kometa	k1gFnSc2	kometa
Borrelly	Borrella	k1gFnSc2	Borrella
odráží	odrážet	k5eAaImIp3nS	odrážet
jen	jen	k9	jen
2,4	[number]	k4	2,4
%	%	kIx~	%
až	až	k9	až
3,0	[number]	k4	3,0
%	%	kIx~	%
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
asfalt	asfalt	k1gInSc4	asfalt
odráží	odrážet	k5eAaImIp3nS	odrážet
7	[number]	k4	7
%	%	kIx~	%
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
odpařilo	odpařit	k5eAaPmAgNnS	odpařit
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
vrstvě	vrstva	k1gFnSc6	vrstva
komety	kometa	k1gFnSc2	kometa
těkavější	těkavý	k2eAgFnSc2d2	těkavější
složky	složka	k1gFnSc2	složka
a	a	k8xC	a
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
s	s	k7c7	s
delším	dlouhý	k2eAgInSc7d2	delší
řetězcem	řetězec	k1gInSc7	řetězec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
tmavší	tmavý	k2eAgFnPc1d2	tmavší
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
jádra	jádro	k1gNnSc2	jádro
komety	kometa	k1gFnSc2	kometa
73	[number]	k4	73
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Schwassmann-Wachmann	Schwassmann-Wachmann	k1gNnSc1	Schwassmann-Wachmann
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
však	však	k9	však
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
složení	složení	k1gNnSc1	složení
svrchních	svrchní	k2eAgFnPc2d1	svrchní
a	a	k8xC	a
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
vrstev	vrstva	k1gFnPc2	vrstva
komety	kometa	k1gFnSc2	kometa
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
totožné	totožný	k2eAgNnSc1d1	totožné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
překvapivě	překvapivě	k6eAd1	překvapivě
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
i	i	k9	i
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
generované	generovaný	k2eAgNnSc1d1	generované
interakcí	interakce	k1gFnSc7	interakce
komet	kometa	k1gFnPc2	kometa
se	s	k7c7	s
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
:	:	kIx,	:
když	když	k8xS	když
vysokoenergetické	vysokoenergetický	k2eAgInPc4d1	vysokoenergetický
ionty	ion	k1gInPc4	ion
vletí	vletět	k5eAaPmIp3nS	vletět
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
srážejí	srážet	k5eAaImIp3nP	srážet
se	se	k3xPyFc4	se
s	s	k7c7	s
kometárními	kometární	k2eAgInPc7d1	kometární
atomy	atom	k1gInPc7	atom
a	a	k8xC	a
molekulami	molekula	k1gFnPc7	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
takovéto	takovýto	k3xDgFnSc6	takovýto
srážce	srážka	k1gFnSc6	srážka
ionty	ion	k1gInPc1	ion
zachytí	zachytit	k5eAaPmIp3nS	zachytit
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
emisi	emise	k1gFnSc3	emise
rentgenového	rentgenový	k2eAgMnSc2d1	rentgenový
nebo	nebo	k8xC	nebo
ultrafialového	ultrafialový	k2eAgInSc2d1	ultrafialový
fotonu	foton	k1gInSc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
Komety	kometa	k1gFnPc1	kometa
jsou	být	k5eAaImIp3nP	být
klasifikovány	klasifikován	k2eAgFnPc1d1	klasifikována
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
oběžných	oběžný	k2eAgFnPc2d1	oběžná
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
period	perioda	k1gFnPc2	perioda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátkoperiodické	Krátkoperiodický	k2eAgFnPc1d1	Krátkoperiodická
komety	kometa	k1gFnPc1	kometa
mají	mít	k5eAaImIp3nP	mít
oběžné	oběžný	k2eAgFnPc1d1	oběžná
doby	doba	k1gFnPc1	doba
kratší	krátký	k2eAgFnPc1d2	kratší
než	než	k8xS	než
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dlouhoperiodické	dlouhoperiodický	k2eAgFnPc1d1	dlouhoperiodická
komety	kometa	k1gFnPc1	kometa
mají	mít	k5eAaImIp3nP	mít
oběžné	oběžný	k2eAgFnPc4d1	oběžná
doby	doba	k1gFnPc4	doba
delší	dlouhý	k2eAgFnPc4d2	delší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
gravitačně	gravitačně	k6eAd1	gravitačně
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jednonávratové	jednonávratový	k2eAgFnPc1d1	jednonávratový
komety	kometa	k1gFnPc1	kometa
mají	mít	k5eAaImIp3nP	mít
parabolické	parabolický	k2eAgFnPc1d1	parabolická
či	či	k8xC	či
hyperbolické	hyperbolický	k2eAgFnPc1d1	hyperbolická
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
je	on	k3xPp3gNnPc4	on
vynesou	vynést	k5eAaPmIp3nP	vynést
navždy	navždy	k6eAd1	navždy
mimo	mimo	k7c4	mimo
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
po	po	k7c6	po
jediném	jediný	k2eAgInSc6d1	jediný
průletu	průlet	k1gInSc6	průlet
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Opačným	opačný	k2eAgInSc7d1	opačný
extrémem	extrém	k1gInSc7	extrém
je	být	k5eAaImIp3nS	být
krátkoperiodická	krátkoperiodický	k2eAgFnSc1d1	krátkoperiodická
Enckeova	Enckeův	k2eAgFnSc1d1	Enckeova
kometa	kometa	k1gFnSc1	kometa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jí	on	k3xPp3gFnSc3	on
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
se	se	k3xPyFc4	se
vzdálit	vzdálit	k5eAaPmF	vzdálit
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
dál	daleko	k6eAd2	daleko
než	než	k8xS	než
k	k	k7c3	k
oběžné	oběžný	k2eAgFnSc3d1	oběžná
dráze	dráha	k1gFnSc3	dráha
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
místo	místo	k1gNnSc4	místo
vzniku	vznik	k1gInSc2	vznik
krátkoperiodických	krátkoperiodický	k2eAgFnPc2d1	krátkoperiodická
komet	kometa	k1gFnPc2	kometa
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
považuje	považovat	k5eAaImIp3nS	považovat
Kuiperův	Kuiperův	k2eAgInSc1d1	Kuiperův
pás	pás	k1gInSc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhoperiodické	Dlouhoperiodický	k2eAgFnPc1d1	Dlouhoperiodická
komety	kometa	k1gFnPc1	kometa
zřejmě	zřejmě	k6eAd1	zřejmě
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
Oortově	Oortův	k2eAgInSc6d1	Oortův
oblaku	oblak	k1gInSc6	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
modelů	model	k1gInPc2	model
vysvětlujících	vysvětlující	k2eAgInPc2d1	vysvětlující
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
komety	kometa	k1gFnPc1	kometa
odkloněny	odklonit	k5eAaPmNgFnP	odklonit
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
excentrických	excentrický	k2eAgFnPc2d1	excentrická
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
přiblížení	přiblížení	k1gNnSc4	přiblížení
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
hvězdám	hvězda	k1gFnPc3	hvězda
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
Slunce	slunce	k1gNnSc2	slunce
naší	náš	k3xOp1gFnSc7	náš
Galaxií	galaxie	k1gFnSc7	galaxie
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc4	působení
hypotetického	hypotetický	k2eAgMnSc2d1	hypotetický
průvodce	průvodce	k1gMnSc2	průvodce
Slunce	slunce	k1gNnSc2	slunce
Nemesis	Nemesis	k1gFnSc2	Nemesis
a	a	k8xC	a
nebo	nebo	k8xC	nebo
působení	působení	k1gNnSc1	působení
zatím	zatím	k6eAd1	zatím
neznámých	známý	k2eNgNnPc2d1	neznámé
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hypotetické	hypotetický	k2eAgFnPc1d1	hypotetická
Planety	planeta	k1gFnPc1	planeta
X.	X.	kA	X.
Nejpřijímanější	přijímaný	k2eAgFnSc6d3	přijímaný
je	být	k5eAaImIp3nS	být
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
poruchám	porucha	k1gFnPc3	porucha
drah	draha	k1gFnPc2	draha
dochází	docházet	k5eAaImIp3nS	docházet
náhodně	náhodně	k6eAd1	náhodně
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
ovlivňováním	ovlivňování	k1gNnSc7	ovlivňování
se	se	k3xPyFc4	se
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
Oortově	Oortův	k2eAgNnSc6d1	Oortovo
mračnu	mračno	k1gNnSc6	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
malým	malý	k2eAgFnPc3d1	malá
hmotnostem	hmotnost	k1gFnPc3	hmotnost
a	a	k8xC	a
excentrickým	excentrický	k2eAgFnPc3d1	excentrická
oběžným	oběžný	k2eAgFnPc3d1	oběžná
drahám	draha	k1gFnPc3	draha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
je	on	k3xPp3gFnPc4	on
přivádějí	přivádět	k5eAaImIp3nP	přivádět
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
velkých	velký	k2eAgFnPc2d1	velká
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
často	často	k6eAd1	často
rušené	rušený	k2eAgNnSc1d1	rušené
(	(	kIx(	(
<g/>
perturbované	perturbovaný	k2eAgNnSc1d1	perturbovaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
všimnout	všimnout	k5eAaPmF	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
dráhy	dráha	k1gFnPc1	dráha
krátkoperiodických	krátkoperiodický	k2eAgFnPc2d1	krátkoperiodická
komet	kometa	k1gFnPc2	kometa
mívají	mívat	k5eAaImIp3nP	mívat
často	často	k6eAd1	často
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
afelu	afel	k1gInSc2	afel
souměřitelné	souměřitelný	k2eAgInPc1d1	souměřitelný
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
poloosami	poloosa	k1gFnPc7	poloosa
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
pak	pak	k6eAd1	pak
obvykle	obvykle	k6eAd1	obvykle
nazýváme	nazývat	k5eAaImIp1nP	nazývat
rodinami	rodina	k1gFnPc7	rodina
příslušné	příslušný	k2eAgFnSc2d1	příslušná
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jupiterova	Jupiterův	k2eAgFnSc1d1	Jupiterova
rodina	rodina	k1gFnSc1	rodina
komet	kometa	k1gFnPc2	kometa
má	mít	k5eAaImIp3nS	mít
přitom	přitom	k6eAd1	přitom
nejvíce	nejvíce	k6eAd1	nejvíce
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
komet	kometa	k1gFnPc2	kometa
přicházejících	přicházející	k2eAgFnPc2d1	přicházející
z	z	k7c2	z
Oortova	Oortův	k2eAgNnSc2d1	Oortovo
mračna	mračno	k1gNnSc2	mračno
často	často	k6eAd1	často
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
gravitace	gravitace	k1gFnSc1	gravitace
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
komety	kometa	k1gFnPc4	kometa
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
zdrojem	zdroj	k1gInSc7	zdroj
těchto	tento	k3xDgFnPc2	tento
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
nejhmotnější	hmotný	k2eAgFnSc7d3	nejhmotnější
planetou	planeta	k1gFnSc7	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
perturbacím	perturbace	k1gFnPc3	perturbace
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
mnoho	mnoho	k4c1	mnoho
periodických	periodický	k2eAgFnPc2d1	periodická
komet	kometa	k1gFnPc2	kometa
objevených	objevený	k2eAgFnPc2d1	objevená
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
desetiletích	desetiletí	k1gNnPc6	desetiletí
a	a	k8xC	a
stoletích	století	k1gNnPc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
dostatečně	dostatečně	k6eAd1	dostatečně
přesně	přesně	k6eAd1	přesně
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
kde	kde	k6eAd1	kde
čekat	čekat	k5eAaImF	čekat
jejich	jejich	k3xOp3gNnSc4	jejich
budoucí	budoucí	k2eAgNnSc4d1	budoucí
přiblížení	přiblížení	k1gNnSc4	přiblížení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
po	po	k7c6	po
zpětném	zpětný	k2eAgNnSc6d1	zpětné
vypočítání	vypočítání	k1gNnSc6	vypočítání
dráhy	dráha	k1gFnSc2	dráha
nově	nova	k1gFnSc3	nova
objevené	objevený	k2eAgFnSc2d1	objevená
komety	kometa	k1gFnSc2	kometa
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
kometu	kometa	k1gFnSc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
osud	osud	k1gInSc1	osud
sdílí	sdílet	k5eAaImIp3nS	sdílet
například	například	k6eAd1	například
Tempel-Swift-LINEAR	Tempel-Swift-LINEAR	k1gFnSc1	Tempel-Swift-LINEAR
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
objevená	objevený	k2eAgFnSc1d1	objevená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poruchy	porucha	k1gFnSc2	porucha
způsobené	způsobený	k2eAgNnSc1d1	způsobené
Jupiterem	Jupiter	k1gMnSc7	Jupiter
ztracena	ztraceno	k1gNnSc2	ztraceno
<g/>
.	.	kIx.	.
</s>
<s>
Náhodou	náhodou	k6eAd1	náhodou
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
objevena	objevit	k5eAaPmNgFnS	objevit
až	až	k9	až
v	v	k7c6	v
pozorovacím	pozorovací	k2eAgInSc6d1	pozorovací
programu	program	k1gInSc6	program
LINEAR	LINEAR	kA	LINEAR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
nová	nový	k2eAgFnSc1d1	nová
kometa	kometa	k1gFnSc1	kometa
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
z	z	k7c2	z
krátkého	krátký	k2eAgNnSc2d1	krátké
pozorování	pozorování	k1gNnSc2	pozorování
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
úsek	úsek	k1gInSc1	úsek
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
počítá	počítat	k5eAaImIp3nS	počítat
její	její	k3xOp3gFnSc1	její
parabolická	parabolický	k2eAgFnSc1d1	parabolická
aproximace	aproximace	k1gFnSc1	aproximace
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
delším	dlouhý	k2eAgNnSc6d2	delší
pozorování	pozorování	k1gNnSc6	pozorování
lze	lze	k6eAd1	lze
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
eliptická	eliptický	k2eAgFnSc1d1	eliptická
nebo	nebo	k8xC	nebo
hyperbolická	hyperbolický	k2eAgFnSc1d1	hyperbolická
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
3400	[number]	k4	3400
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
známe	znát	k5eAaImIp1nP	znát
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
40	[number]	k4	40
%	%	kIx~	%
komet	kometa	k1gFnPc2	kometa
na	na	k7c6	na
eliptických	eliptický	k2eAgFnPc6d1	eliptická
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
16	[number]	k4	16
%	%	kIx~	%
krátkoperiodických	krátkoperiodický	k2eAgInPc2d1	krátkoperiodický
(	(	kIx(	(
<g/>
perioda	perioda	k1gFnSc1	perioda
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
24	[number]	k4	24
%	%	kIx~	%
dlouhoperiodických	dlouhoperiodický	k2eAgInPc2d1	dlouhoperiodický
(	(	kIx(	(
<g/>
perioda	perioda	k1gFnSc1	perioda
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
49	[number]	k4	49
%	%	kIx~	%
na	na	k7c6	na
parabolických	parabolický	k2eAgFnPc6d1	parabolická
drahách	draha	k1gFnPc6	draha
11	[number]	k4	11
%	%	kIx~	%
na	na	k7c6	na
hyperbolických	hyperbolický	k2eAgFnPc6d1	hyperbolická
drahách	draha	k1gFnPc6	draha
Velké	velký	k2eAgNnSc1d1	velké
procento	procento	k1gNnSc1	procento
parabolických	parabolický	k2eAgFnPc2d1	parabolická
drah	draha	k1gFnPc2	draha
uvedené	uvedený	k2eAgFnPc4d1	uvedená
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
výčtu	výčet	k1gInSc6	výčet
je	být	k5eAaImIp3nS	být
zkreslující	zkreslující	k2eAgNnSc1d1	zkreslující
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
i	i	k9	i
o	o	k7c4	o
komety	kometa	k1gFnPc4	kometa
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
doba	doba	k1gFnSc1	doba
pozorování	pozorování	k1gNnPc2	pozorování
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
krátká	krátký	k2eAgFnSc1d1	krátká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
hyperbole	hyperbola	k1gFnSc6	hyperbola
nebo	nebo	k8xC	nebo
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
protáhlé	protáhlý	k2eAgFnSc6d1	protáhlá
elipse	elipsa	k1gFnSc6	elipsa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
alespoň	alespoň	k9	alespoň
240	[number]	k4	240
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
jen	jen	k9	jen
3	[number]	k4	3
%	%	kIx~	%
má	mít	k5eAaImIp3nS	mít
parabolické	parabolický	k2eAgFnPc4d1	parabolická
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
komety	kometa	k1gFnPc1	kometa
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
znamení	znamení	k1gNnSc4	znamení
zmaru	zmar	k1gInSc2	zmar
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
byly	být	k5eAaImAgFnP	být
dokonce	dokonce	k9	dokonce
znázorňovány	znázorňovat	k5eAaImNgFnP	znázorňovat
jako	jako	k8xS	jako
útok	útok	k1gInSc1	útok
nebeských	nebeský	k2eAgFnPc2d1	nebeská
bytostí	bytost	k1gFnPc2	bytost
proti	proti	k7c3	proti
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
"	"	kIx"	"
<g/>
padajících	padající	k2eAgFnPc6d1	padající
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
"	"	kIx"	"
v	v	k7c6	v
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
,	,	kIx,	,
Janově	Janův	k2eAgFnSc3d1	Janova
Apokalypse	apokalypsa	k1gFnSc3	apokalypsa
a	a	k8xC	a
Knize	kniha	k1gFnSc3	kniha
Enoch	Enocha	k1gFnPc2	Enocha
jako	jako	k8xS	jako
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
kometách	kometa	k1gFnPc6	kometa
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
o	o	k7c6	o
bolidech	bolid	k1gInPc6	bolid
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
předložil	předložit	k5eAaPmAgInS	předložit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Meteorologica	Meteorologic	k1gInSc2	Meteorologic
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
komety	kometa	k1gFnPc4	kometa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
západní	západní	k2eAgNnPc4d1	západní
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
názory	názor	k1gInPc1	názor
několika	několik	k4yIc2	několik
dřívějších	dřívější	k2eAgMnPc2d1	dřívější
filozofů	filozof	k1gMnPc2	filozof
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
jsou	být	k5eAaImIp3nP	být
planety	planeta	k1gFnPc1	planeta
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
jevy	jev	k1gInPc1	jev
planetám	planeta	k1gFnPc3	planeta
podobné	podobný	k2eAgFnPc4d1	podobná
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
planety	planeta	k1gFnPc1	planeta
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
jen	jen	k9	jen
okolo	okolo	k7c2	okolo
zvířetníku	zvířetník	k1gInSc2	zvířetník
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
komety	kometa	k1gFnPc1	kometa
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
kterékoliv	kterýkoliv	k3yIgFnSc6	kterýkoliv
části	část	k1gFnSc6	část
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
popsal	popsat	k5eAaPmAgMnS	popsat
komety	kometa	k1gFnPc4	kometa
jako	jako	k8xS	jako
jevy	jev	k1gInPc4	jev
z	z	k7c2	z
vrchní	vrchní	k2eAgFnSc2d1	vrchní
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
příležitostně	příležitostně	k6eAd1	příležitostně
vybuchují	vybuchovat	k5eAaImIp3nP	vybuchovat
horké	horký	k2eAgInPc1d1	horký
a	a	k8xC	a
suché	suchý	k2eAgInPc1d1	suchý
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
považoval	považovat	k5eAaImAgInS	považovat
tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
za	za	k7c4	za
zodpovědný	zodpovědný	k2eAgInSc4d1	zodpovědný
nejen	nejen	k6eAd1	nejen
za	za	k7c4	za
komety	kometa	k1gFnPc4	kometa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
za	za	k7c4	za
meteory	meteor	k1gInPc4	meteor
<g/>
,	,	kIx,	,
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
za	za	k7c4	za
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
několik	několik	k4yIc4	několik
klasických	klasický	k2eAgMnPc2d1	klasický
filozofů	filozof	k1gMnPc2	filozof
jeho	jeho	k3xOp3gFnSc2	jeho
názor	názor	k1gInSc4	názor
na	na	k7c4	na
komety	kometa	k1gFnPc4	kometa
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Seneca	Seneca	k1gMnSc1	Seneca
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Přírodovědeckých	přírodovědecký	k2eAgFnPc6d1	Přírodovědecká
otázkách	otázka	k1gFnPc6	otázka
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
rušené	rušený	k2eAgInPc1d1	rušený
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
chování	chování	k1gNnSc3	chování
typickému	typický	k2eAgNnSc3d1	typické
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
nebeská	nebeský	k2eAgNnPc4d1	nebeské
tělesa	těleso	k1gNnPc4	těleso
než	než	k8xS	než
pro	pro	k7c4	pro
atmosférické	atmosférický	k2eAgInPc4d1	atmosférický
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
planety	planeta	k1gFnPc1	planeta
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
zvířetník	zvířetník	k1gInSc4	zvířetník
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
,	,	kIx,	,
neviděl	vidět	k5eNaImAgInS	vidět
však	však	k9	však
žádný	žádný	k3yNgInSc1	žádný
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
planetám	planeta	k1gFnPc3	planeta
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
objekty	objekt	k1gInPc1	objekt
nemohly	moct	k5eNaImAgInP	moct
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
kterékoliv	kterýkoliv	k3yIgFnSc6	kterýkoliv
části	část	k1gFnSc6	část
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
vážnou	vážný	k2eAgFnSc4d1	vážná
výtku	výtka	k1gFnSc4	výtka
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
Aristotelovský	aristotelovský	k2eAgInSc1d1	aristotelovský
názor	názor	k1gInSc1	názor
a	a	k8xC	a
udržel	udržet	k5eAaPmAgInS	udržet
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
musí	muset	k5eAaImIp3nP	muset
existovat	existovat	k5eAaImF	existovat
mimo	mimo	k7c4	mimo
atmosféru	atmosféra	k1gFnSc4	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1577	[number]	k4	1577
byla	být	k5eAaImAgFnS	být
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
viditelná	viditelný	k2eAgFnSc1d1	viditelná
jasná	jasný	k2eAgFnSc1d1	jasná
kometa	kometa	k1gFnSc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1	dánský
astronom	astronom	k1gMnSc1	astronom
Tycho	Tyc	k1gMnSc2	Tyc
Brahe	Brah	k1gInSc2	Brah
využil	využít	k5eAaPmAgInS	využít
měření	měření	k1gNnSc4	měření
polohy	poloha	k1gFnSc2	poloha
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provedl	provést	k5eAaPmAgMnS	provést
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgMnPc2d1	další
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kometa	kometa	k1gFnSc1	kometa
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
měřitelnou	měřitelný	k2eAgFnSc4d1	měřitelná
paralaxu	paralaxa	k1gFnSc4	paralaxa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přesností	přesnost	k1gFnSc7	přesnost
těchto	tento	k3xDgInPc6	tento
měření	měření	k1gNnSc4	měření
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kometa	kometa	k1gFnSc1	kometa
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
než	než	k8xS	než
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
patří	patřit	k5eAaImIp3nP	patřit
na	na	k7c4	na
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
o	o	k7c6	o
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
debatovalo	debatovat	k5eAaImAgNnS	debatovat
většinu	většina	k1gFnSc4	většina
následujícího	následující	k2eAgNnSc2d1	následující
staletí	staletí	k1gNnSc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
zjistil	zjistit	k5eAaPmAgMnS	zjistit
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
planety	planeta	k1gFnPc1	planeta
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
eliptických	eliptický	k2eAgFnPc6d1	eliptická
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
zdráhal	zdráhat	k5eAaImAgMnS	zdráhat
se	se	k3xPyFc4	se
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc4	jeho
vlastní	vlastní	k2eAgInPc4d1	vlastní
Keplerovy	Keplerův	k2eAgInPc4d1	Keplerův
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
pohyb	pohyb	k1gInSc1	pohyb
planet	planeta	k1gFnPc2	planeta
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
,	,	kIx,	,
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
i	i	k9	i
pohyb	pohyb	k1gInSc4	pohyb
ostatních	ostatní	k2eAgInPc2d1	ostatní
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
planetami	planeta	k1gFnPc7	planeta
po	po	k7c6	po
přímých	přímý	k2eAgFnPc6d1	přímá
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnSc5	Galile
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
oddaným	oddaný	k2eAgMnSc7d1	oddaný
stoupencem	stoupenec	k1gMnSc7	stoupenec
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Tychonovo	Tychonův	k2eAgNnSc4d1	Tychonův
paralaktické	paralaktický	k2eAgNnSc4d1	paralaktické
pozorování	pozorování	k1gNnSc4	pozorování
a	a	k8xC	a
držel	držet	k5eAaImAgMnS	držet
se	se	k3xPyFc4	se
aristotelovské	aristotelovský	k2eAgFnSc2d1	aristotelovská
představy	představa	k1gFnSc2	představa
pohybu	pohyb	k1gInSc2	pohyb
po	po	k7c6	po
přímkách	přímka	k1gFnPc6	přímka
přes	přes	k7c4	přes
vrchní	vrchní	k2eAgFnSc4d1	vrchní
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
že	že	k8xS	že
Keplerovy	Keplerův	k2eAgInPc4d1	Keplerův
zákony	zákon	k1gInPc4	zákon
planetárních	planetární	k2eAgInPc2d1	planetární
pohybů	pohyb	k1gInPc2	pohyb
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
platit	platit	k5eAaImF	platit
i	i	k9	i
pro	pro	k7c4	pro
komety	kometa	k1gFnPc4	kometa
<g/>
,	,	kIx,	,
předložil	předložit	k5eAaPmAgMnS	předložit
William	William	k1gInSc4	William
Lower	Lower	k1gMnSc1	Lower
roku	rok	k1gInSc2	rok
1610	[number]	k4	1610
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
další	další	k2eAgMnPc1d1	další
astronomové	astronom	k1gMnPc1	astronom
včetně	včetně	k7c2	včetně
Pierra	Pierr	k1gMnSc2	Pierr
Petita	Petit	k1gMnSc2	Petit
<g/>
,	,	kIx,	,
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Borelliho	Borelli	k1gMnSc2	Borelli
<g/>
,	,	kIx,	,
Adriena	Adriena	k1gFnSc1	Adriena
Auzouta	Auzout	k1gMnSc2	Auzout
<g/>
,	,	kIx,	,
Roberta	Robert	k1gMnSc2	Robert
Hooka	Hooek	k1gMnSc2	Hooek
a	a	k8xC	a
Giovanni	Giovann	k1gMnPc1	Giovann
Domenico	Domenico	k6eAd1	Domenico
Cassiniho	Cassini	k1gMnSc4	Cassini
předkládali	předkládat	k5eAaImAgMnP	předkládat
argumenty	argument	k1gInPc4	argument
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
komety	kometa	k1gFnPc1	kometa
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
eliptických	eliptický	k2eAgFnPc6d1	eliptická
nebo	nebo	k8xC	nebo
parabolických	parabolický	k2eAgFnPc6d1	parabolická
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Christiaan	Christiaan	k1gInSc1	Christiaan
Huygens	Huygensa	k1gFnPc2	Huygensa
a	a	k8xC	a
Johannes	Johannes	k1gMnSc1	Johannes
Hevelius	Hevelius	k1gMnSc1	Hevelius
<g/>
,	,	kIx,	,
podporovali	podporovat	k5eAaImAgMnP	podporovat
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
přímém	přímý	k2eAgInSc6d1	přímý
pohybu	pohyb	k1gInSc6	pohyb
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Záležitost	záležitost	k1gFnSc1	záležitost
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
jasná	jasný	k2eAgFnSc1d1	jasná
kometa	kometa	k1gFnSc1	kometa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
objevil	objevit	k5eAaPmAgMnS	objevit
Gottfried	Gottfried	k1gMnSc1	Gottfried
Kirch	Kirch	k1gMnSc1	Kirch
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1680	[number]	k4	1680
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
sledovali	sledovat	k5eAaImAgMnP	sledovat
její	její	k3xOp3gInSc4	její
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
po	po	k7c4	po
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Principiích	principie	k1gFnPc6	principie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
Isaac	Isaac	k1gInSc1	Isaac
Newton	newton	k1gInSc4	newton
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekt	objekt	k1gInSc1	objekt
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
poklesu	pokles	k1gInSc6	pokles
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
čtvercem	čtverec	k1gInSc7	čtverec
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
musí	muset	k5eAaImIp3nS	muset
letět	letět	k5eAaImF	letět
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
kuželoseček	kuželosečka	k1gFnPc2	kuželosečka
<g/>
,	,	kIx,	,
a	a	k8xC	a
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
dráhu	dráha	k1gFnSc4	dráha
komety	kometa	k1gFnSc2	kometa
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
s	s	k7c7	s
parabolickou	parabolický	k2eAgFnSc7d1	parabolická
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
použil	použít	k5eAaPmAgMnS	použít
kometu	kometa	k1gFnSc4	kometa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
Edmond	Edmond	k1gMnSc1	Edmond
Halley	Halley	k1gMnSc1	Halley
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
Newtonovu	Newtonův	k2eAgFnSc4d1	Newtonova
metodu	metoda	k1gFnSc4	metoda
na	na	k7c4	na
24	[number]	k4	24
pozorování	pozorování	k1gNnPc2	pozorování
komet	kometa	k1gFnPc2	kometa
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1337	[number]	k4	1337
a	a	k8xC	a
1698	[number]	k4	1698
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tři	tři	k4xCgMnPc4	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
–	–	k?	–
komety	kometa	k1gFnPc4	kometa
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1531	[number]	k4	1531
<g/>
,	,	kIx,	,
1607	[number]	k4	1607
a	a	k8xC	a
1682	[number]	k4	1682
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc4d1	podobný
dráhové	dráhový	k2eAgInPc4d1	dráhový
elementy	element	k1gInPc4	element
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
dále	daleko	k6eAd2	daleko
schopný	schopný	k2eAgMnSc1d1	schopný
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
malé	malý	k2eAgInPc4d1	malý
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
na	na	k7c6	na
základě	základ	k1gInSc6	základ
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
Jupiterem	Jupiter	k1gInSc7	Jupiter
a	a	k8xC	a
Saturnem	Saturn	k1gInSc7	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Nabyl	nabýt	k5eAaPmAgMnS	nabýt
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
úkazy	úkaz	k1gInPc1	úkaz
byly	být	k5eAaImAgInP	být
výskyty	výskyt	k1gInPc4	výskyt
téže	týž	k3xTgFnSc2	týž
komety	kometa	k1gFnSc2	kometa
a	a	k8xC	a
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
znovu	znovu	k6eAd1	znovu
někdy	někdy	k6eAd1	někdy
roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
nebo	nebo	k8xC	nebo
1759	[number]	k4	1759
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Halleyem	Halley	k1gMnSc7	Halley
Robert	Robert	k1gMnSc1	Robert
Hooke	Hook	k1gMnSc4	Hook
ztotožnil	ztotožnit	k5eAaPmAgMnS	ztotožnit
kometu	kometa	k1gFnSc4	kometa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
a	a	k8xC	a
Giovanni	Giovann	k1gMnPc1	Giovann
Domenico	Domenico	k6eAd1	Domenico
Cassini	Cassin	k2eAgMnPc1d1	Cassin
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
podezření	podezření	k1gNnSc3	podezření
o	o	k7c6	o
totožnosti	totožnost	k1gFnSc6	totožnost
komet	kometa	k1gFnPc2	kometa
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1577	[number]	k4	1577
<g/>
,	,	kIx,	,
1665	[number]	k4	1665
a	a	k8xC	a
1680	[number]	k4	1680
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
však	však	k9	však
mýlili	mýlit	k5eAaImAgMnP	mýlit
<g/>
.	.	kIx.	.
</s>
<s>
Halleyova	Halleyův	k2eAgFnSc1d1	Halleyova
předpověď	předpověď	k1gFnSc1	předpověď
data	datum	k1gNnSc2	datum
návratu	návrat	k1gInSc6	návrat
byla	být	k5eAaImAgFnS	být
brzo	brzo	k6eAd1	brzo
upřesněná	upřesněný	k2eAgNnPc4d1	upřesněné
týmem	tým	k1gInSc7	tým
tří	tři	k4xCgMnPc2	tři
francouzských	francouzský	k2eAgMnPc2d1	francouzský
matematiků	matematik	k1gMnPc2	matematik
<g/>
.	.	kIx.	.
</s>
<s>
Alexis	Alexis	k1gFnSc3	Alexis
Clairaut	Clairaut	k1gMnSc1	Clairaut
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Lalande	Laland	k1gInSc5	Laland
a	a	k8xC	a
Nicole-Reine	Nicole-Rein	k1gInSc5	Nicole-Rein
Lepaute	Lepaut	k1gInSc5	Lepaut
předpověděli	předpovědět	k5eAaPmAgMnP	předpovědět
datum	datum	k1gNnSc4	datum
průchodu	průchod	k1gInSc2	průchod
komety	kometa	k1gFnSc2	kometa
perihelem	perihel	k1gInSc7	perihel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1759	[number]	k4	1759
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
kometa	kometa	k1gFnSc1	kometa
objevila	objevit	k5eAaPmAgFnS	objevit
podle	podle	k7c2	podle
předpovědi	předpověď	k1gFnSc2	předpověď
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
známou	známá	k1gFnSc7	známá
jako	jako	k8xS	jako
Halleyova	Halleyův	k2eAgFnSc1d1	Halleyova
kometa	kometa	k1gFnSc1	kometa
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgNnSc1d1	oficiální
označení	označení	k1gNnSc1	označení
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Halley	Halley	k1gMnSc1	Halley
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
částí	část	k1gFnPc2	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
zavítala	zavítat	k5eAaPmAgFnS	zavítat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
další	další	k2eAgInSc1d1	další
návrat	návrat	k1gInSc1	návrat
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2061	[number]	k4	2061
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kometami	kometa	k1gFnPc7	kometa
s	s	k7c7	s
natolik	natolik	k6eAd1	natolik
krátkými	krátký	k2eAgFnPc7d1	krátká
periodami	perioda	k1gFnPc7	perioda
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
historických	historický	k2eAgInPc2d1	historický
záznamů	záznam	k1gInPc2	záznam
několikrát	několikrát	k6eAd1	několikrát
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Halleyova	Halleyův	k2eAgFnSc1d1	Halleyova
kometa	kometa	k1gFnSc1	kometa
unikátní	unikátní	k2eAgFnSc1d1	unikátní
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
dostatečně	dostatečně	k6eAd1	dostatečně
jasná	jasný	k2eAgFnSc1d1	jasná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
potvrzení	potvrzení	k1gNnSc2	potvrzení
periodicity	periodicita	k1gFnSc2	periodicita
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
bylo	být	k5eAaImAgNnS	být
pomocí	pomocí	k7c2	pomocí
dalekohledů	dalekohled	k1gInPc2	dalekohled
objeveno	objevit	k5eAaPmNgNnS	objevit
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
periodických	periodický	k2eAgFnPc2d1	periodická
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
kometa	kometa	k1gFnSc1	kometa
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
periodická	periodický	k2eAgFnSc1d1	periodická
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Enckeova	Enckeův	k2eAgFnSc1d1	Enckeova
kometa	kometa	k1gFnSc1	kometa
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
označená	označený	k2eAgFnSc1d1	označená
2	[number]	k4	2
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Encke	Enck	k1gMnSc2	Enck
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1819	[number]	k4	1819
až	až	k9	až
1821	[number]	k4	1821
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
Johann	Johanna	k1gFnPc2	Johanna
Franz	Franz	k1gMnSc1	Franz
Encke	Enck	k1gFnSc2	Enck
vypočítal	vypočítat	k5eAaPmAgMnS	vypočítat
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
série	série	k1gFnSc2	série
kometárních	kometární	k2eAgInPc2d1	kometární
výskytů	výskyt	k1gInPc2	výskyt
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1786	[number]	k4	1786
<g/>
,	,	kIx,	,
1795	[number]	k4	1795
<g/>
,	,	kIx,	,
1805	[number]	k4	1805
a	a	k8xC	a
1818	[number]	k4	1818
a	a	k8xC	a
vyvodil	vyvodit	k5eAaBmAgMnS	vyvodit
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tutéž	týž	k3xTgFnSc4	týž
kometu	kometa	k1gFnSc4	kometa
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
její	její	k3xOp3gInSc4	její
návrat	návrat	k1gInSc4	návrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
17	[number]	k4	17
komet	kometa	k1gFnPc2	kometa
s	s	k7c7	s
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
průchodem	průchod	k1gInSc7	průchod
perihelem	perihel	k1gInSc7	perihel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
uznány	uznat	k5eAaPmNgFnP	uznat
za	za	k7c4	za
periodické	periodický	k2eAgNnSc4d1	periodické
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
status	status	k1gInSc1	status
přiznán	přiznán	k2eAgInSc1d1	přiznán
164	[number]	k4	164
kometám	kometa	k1gFnPc3	kometa
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mezitím	mezitím	k6eAd1	mezitím
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
správné	správný	k2eAgFnPc4d1	správná
hypotézy	hypotéza	k1gFnPc4	hypotéza
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
složení	složení	k1gNnSc2	složení
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgInPc4d1	složený
z	z	k7c2	z
nějaké	nějaký	k3yIgFnSc2	nějaký
těkavé	těkavý	k2eAgFnSc2d1	těkavá
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
vypařování	vypařování	k1gNnPc4	vypařování
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jejich	jejich	k3xOp3gInSc4	jejich
zářivý	zářivý	k2eAgInSc4d1	zářivý
vzhled	vzhled	k1gInSc4	vzhled
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
perihelu	perihel	k1gInSc2	perihel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Bessel	Bessel	k1gMnSc1	Bessel
po	po	k7c4	po
pozorování	pozorování	k1gNnSc4	pozorování
proudů	proud	k1gInPc2	proud
vypařování	vypařování	k1gNnPc2	vypařování
během	během	k7c2	během
návratu	návrat	k1gInSc2	návrat
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
síly	síla	k1gFnPc1	síla
vypařující	vypařující	k2eAgFnPc1d1	vypařující
se	se	k3xPyFc4	se
látky	látka	k1gFnPc1	látka
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgFnPc1d1	velká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podstatně	podstatně	k6eAd1	podstatně
změnily	změnit	k5eAaPmAgInP	změnit
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
negravitační	gravitační	k2eNgFnPc1d1	gravitační
poruchy	porucha	k1gFnPc1	porucha
dráhy	dráha	k1gFnSc2	dráha
Enckeovy	Enckeův	k2eAgFnPc1d1	Enckeova
komety	kometa	k1gFnPc1	kometa
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
objev	objev	k1gInSc1	objev
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
komet	kometa	k1gFnPc2	kometa
však	však	k8xC	však
zastínil	zastínit	k5eAaPmAgMnS	zastínit
tyto	tento	k3xDgFnPc4	tento
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
jedno	jeden	k4xCgNnSc4	jeden
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1864	[number]	k4	1864
až	až	k9	až
1866	[number]	k4	1866
italský	italský	k2eAgMnSc1d1	italský
astronom	astronom	k1gMnSc1	astronom
Giovanni	Giovanň	k1gMnSc3	Giovanň
Schiaparelli	Schiaparell	k1gMnSc3	Schiaparell
vypočítal	vypočítat	k5eAaPmAgMnS	vypočítat
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
meteoritického	meteoritický	k2eAgInSc2d1	meteoritický
roje	roj	k1gInSc2	roj
Perseid	perseida	k1gFnPc2	perseida
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobnosti	podobnost	k1gFnSc2	podobnost
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
správnou	správný	k2eAgFnSc4d1	správná
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
Perseidy	perseida	k1gFnPc4	perseida
jsou	být	k5eAaImIp3nP	být
fragmenty	fragment	k1gInPc1	fragment
komety	kometa	k1gFnSc2	kometa
Swift-Tuttle	Swift-Tuttle	k1gFnSc2	Swift-Tuttle
<g/>
.	.	kIx.	.
</s>
<s>
Souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
kometami	kometa	k1gFnPc7	kometa
a	a	k8xC	a
meteorickými	meteorický	k2eAgInPc7d1	meteorický
roji	roj	k1gInPc7	roj
dramaticky	dramaticky	k6eAd1	dramaticky
podtrhl	podtrhnout	k5eAaPmAgInS	podtrhnout
výskyt	výskyt	k1gInSc4	výskyt
velmi	velmi	k6eAd1	velmi
silného	silný	k2eAgInSc2d1	silný
meteorického	meteorický	k2eAgInSc2d1	meteorický
roje	roj	k1gInSc2	roj
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
Bielovy	Bielův	k2eAgFnSc2d1	Bielova
komety	kometa	k1gFnSc2	kometa
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
rozpad	rozpad	k1gInSc1	rozpad
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
během	během	k7c2	během
jejího	její	k3xOp3gInSc2	její
návratu	návrat	k1gInSc2	návrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yQgFnSc1	který
už	už	k9	už
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
model	model	k1gInSc1	model
"	"	kIx"	"
<g/>
štěrkového	štěrkový	k2eAgInSc2d1	štěrkový
náspu	násep	k1gInSc2	násep
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
gravel	gravel	k1gInSc1	gravel
bank	banka	k1gFnPc2	banka
<g/>
)	)	kIx)	)
kometární	kometární	k2eAgFnSc2d1	kometární
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
komety	kometa	k1gFnPc1	kometa
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
sypkých	sypký	k2eAgFnPc2d1	sypká
hromad	hromada	k1gFnPc2	hromada
malých	malý	k2eAgInPc2d1	malý
kamenných	kamenný	k2eAgInPc2d1	kamenný
objektů	objekt	k1gInPc2	objekt
obalených	obalený	k2eAgInPc2d1	obalený
ledovou	ledový	k2eAgFnSc7d1	ledová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
měl	mít	k5eAaImAgInS	mít
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc4	model
několik	několik	k4yIc4	několik
nedostatků	nedostatek	k1gInPc2	nedostatek
<g/>
:	:	kIx,	:
především	především	k6eAd1	především
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
jen	jen	k9	jen
nevelké	velký	k2eNgNnSc1d1	nevelké
množství	množství	k1gNnSc1	množství
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
mít	mít	k5eAaImF	mít
zářivé	zářivý	k2eAgInPc4d1	zářivý
projevy	projev	k1gInPc4	projev
vypařující	vypařující	k2eAgInPc4d1	vypařující
se	se	k3xPyFc4	se
páry	pár	k1gInPc1	pár
po	po	k7c6	po
několika	několik	k4yIc6	několik
průchodech	průchod	k1gInPc6	průchod
perihelem	perihel	k1gInSc7	perihel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Fred	Fred	k1gMnSc1	Fred
Lawrence	Lawrenec	k1gInSc2	Lawrenec
Whipple	Whipple	k1gMnSc1	Whipple
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
namísto	namísto	k7c2	namísto
skalnatých	skalnatý	k2eAgInPc2d1	skalnatý
objektů	objekt	k1gInPc2	objekt
obsahujících	obsahující	k2eAgInPc2d1	obsahující
málo	málo	k4c4	málo
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
komety	kometa	k1gFnPc1	kometa
převážně	převážně	k6eAd1	převážně
ledové	ledový	k2eAgInPc4d1	ledový
objekty	objekt	k1gInPc4	objekt
obsahující	obsahující	k2eAgFnSc2d1	obsahující
malé	malá	k1gFnSc2	malá
množství	množství	k1gNnSc1	množství
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
úlomků	úlomek	k1gInPc2	úlomek
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
"	"	kIx"	"
<g/>
špinavé	špinavý	k2eAgFnSc2d1	špinavá
sněhové	sněhový	k2eAgFnSc2d1	sněhová
koule	koule	k1gFnSc2	koule
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
rychle	rychle	k6eAd1	rychle
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
sněhové	sněhový	k2eAgFnSc2d1	sněhová
koule	koule	k1gFnSc2	koule
se	se	k3xPyFc4	se
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
když	když	k8xS	když
"	"	kIx"	"
<g/>
armáda	armáda	k1gFnSc1	armáda
<g/>
"	"	kIx"	"
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
sond	sonda	k1gFnPc2	sonda
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
sondy	sonda	k1gFnSc2	sonda
ESA	eso	k1gNnSc2	eso
Giotto	Giotto	k1gNnSc4	Giotto
a	a	k8xC	a
sovětské	sovětský	k2eAgFnPc4d1	sovětská
sondy	sonda	k1gFnPc4	sonda
Vega	Veg	k1gInSc2	Veg
1	[number]	k4	1
a	a	k8xC	a
Vega	Vega	k1gMnSc1	Vega
2	[number]	k4	2
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
proletěla	proletět	k5eAaPmAgFnS	proletět
komou	koma	k1gFnSc7	koma
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
fotografovala	fotografovat	k5eAaImAgFnS	fotografovat
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
proudy	proud	k1gInPc4	proud
vypařujícího	vypařující	k2eAgMnSc2d1	vypařující
se	se	k3xPyFc4	se
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Deep	Deep	k1gInSc1	Deep
Space	Space	k1gFnSc1	Space
1	[number]	k4	1
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
okolo	okolo	k7c2	okolo
jádra	jádro	k1gNnSc2	jádro
Borrellyovy	Borrellyův	k2eAgFnSc2d1	Borrellyův
komety	kometa	k1gFnSc2	kometa
<g/>
)	)	kIx)	)
a	a	k8xC	a
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastnosti	vlastnost	k1gFnPc1	vlastnost
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
komety	kometa	k1gFnPc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Stardust	Stardust	k1gFnSc1	Stardust
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
sesbírala	sesbírat	k5eAaPmAgFnS	sesbírat
částečky	částečka	k1gFnPc4	částečka
komy	koma	k1gFnSc2	koma
komety	kometa	k1gFnSc2	kometa
Wild	Wild	k1gInSc1	Wild
2	[number]	k4	2
a	a	k8xC	a
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
je	být	k5eAaImIp3nS	být
dopravila	dopravit	k5eAaPmAgFnS	dopravit
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
projektil	projektil	k1gInSc1	projektil
sondy	sonda	k1gFnSc2	sonda
Deep	Deep	k1gMnSc1	Deep
Impact	Impact	k1gMnSc1	Impact
(	(	kIx(	(
<g/>
sonda	sonda	k1gFnSc1	sonda
<g/>
)	)	kIx)	)
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
komety	kometa	k1gFnSc2	kometa
Tempel	Tempel	k1gInSc1	Tempel
1	[number]	k4	1
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
kráter	kráter	k1gInSc1	kráter
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
prostudovat	prostudovat	k5eAaPmF	prostudovat
její	její	k3xOp3gNnSc4	její
nitro	nitro	k1gNnSc4	nitro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
komet	kometa	k1gFnPc2	kometa
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
voda	voda	k1gFnSc1	voda
i	i	k9	i
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
přivezených	přivezený	k2eAgFnPc2d1	přivezená
sondou	sonda	k1gFnSc7	sonda
Stardust	Stardust	k1gInSc1	Stardust
od	od	k7c2	od
komety	kometa	k1gFnSc2	kometa
Wild	Wild	k1gInSc4	Wild
2	[number]	k4	2
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jen	jen	k9	jen
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
teplot	teplota	k1gFnPc2	teplota
od	od	k7c2	od
50	[number]	k4	50
do	do	k7c2	do
200	[number]	k4	200
°	°	k?	°
<g/>
C.	C.	kA	C.
Jde	jít	k5eAaImIp3nS	jít
konkrétně	konkrétně	k6eAd1	konkrétně
o	o	k7c4	o
minerál	minerál	k1gInSc4	minerál
cubanit	cubanit	k1gInSc1	cubanit
<g/>
,	,	kIx,	,
sulfid	sulfid	k1gInSc1	sulfid
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
CuFe	CuF	k1gInSc2	CuF
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
horkých	horký	k2eAgFnPc2d1	horká
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgFnPc1d1	budoucí
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
mise	mise	k1gFnPc1	mise
přidají	přidat	k5eAaPmIp3nP	přidat
další	další	k2eAgInPc1d1	další
detaily	detail	k1gInPc1	detail
k	k	k7c3	k
naší	náš	k3xOp1gFnSc3	náš
představě	představa	k1gFnSc3	představa
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
evropská	evropský	k2eAgFnSc1d1	Evropská
sonda	sonda	k1gFnSc1	sonda
Rosetta	Rosetta	k1gFnSc1	Rosetta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
komety	kometa	k1gFnSc2	kometa
67	[number]	k4	67
<g/>
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Churyumov-Gerasimenko	Churyumov-Gerasimenka	k1gFnSc5	Churyumov-Gerasimenka
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
miniaturní	miniaturní	k2eAgInSc1d1	miniaturní
přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
Philae	Phila	k1gInSc2	Phila
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
částmi	část	k1gFnPc7	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
prolétnou	prolétnout	k5eAaPmIp3nP	prolétnout
ročně	ročně	k6eAd1	ročně
stovky	stovka	k1gFnPc1	stovka
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pár	pár	k4xCyI	pár
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zapůsobí	zapůsobit	k5eAaPmIp3nS	zapůsobit
i	i	k9	i
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
kometa	kometa	k1gFnSc1	kometa
jasná	jasný	k2eAgFnSc1d1	jasná
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
komety	kometa	k1gFnPc1	kometa
jsou	být	k5eAaImIp3nP	být
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
velké	velký	k2eAgFnPc1d1	velká
komety	kometa	k1gFnPc1	kometa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jasné	jasný	k2eAgFnPc1d1	jasná
komety	kometa	k1gFnPc1	kometa
způsobovaly	způsobovat	k5eAaImAgFnP	způsobovat
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
paniku	panika	k1gFnSc4	panika
a	a	k8xC	a
hysterii	hysterie	k1gFnSc4	hysterie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zjevení	zjevení	k1gNnSc1	zjevení
bývalo	bývat	k5eAaImAgNnS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
zlé	zlý	k2eAgNnSc4d1	zlé
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
během	během	k7c2	během
přechodu	přechod	k1gInSc2	přechod
Halleyovy	Halleyův	k2eAgFnSc2d1	Halleyova
komety	kometa	k1gFnSc2	kometa
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
procházela	procházet	k5eAaImAgFnS	procházet
ohonem	ohon	k1gInSc7	ohon
komety	kometa	k1gFnSc2	kometa
a	a	k8xC	a
noviny	novina	k1gFnSc2	novina
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mylně	mylně	k6eAd1	mylně
způsobily	způsobit	k5eAaPmAgFnP	způsobit
paniku	panika	k1gFnSc4	panika
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ohonu	ohon	k1gInSc6	ohon
obsažený	obsažený	k2eAgInSc4d1	obsažený
dikyan	dikyan	k1gInSc4	dikyan
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
otrávit	otrávit	k5eAaPmF	otrávit
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
spustil	spustit	k5eAaPmAgInS	spustit
příchod	příchod	k1gInSc1	příchod
Hale-Boppovy	Hale-Boppův	k2eAgFnSc2d1	Hale-Boppova
komety	kometa	k1gFnSc2	kometa
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
kultu	kult	k1gInSc2	kult
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
velké	velký	k2eAgFnPc4d1	velká
komety	kometa	k1gFnPc4	kometa
za	za	k7c4	za
jev	jev	k1gInSc4	jev
velmi	velmi	k6eAd1	velmi
krásný	krásný	k2eAgInSc4d1	krásný
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
poměrně	poměrně	k6eAd1	poměrně
neškodný	škodný	k2eNgMnSc1d1	neškodný
<g/>
.	.	kIx.	.
</s>
<s>
Předpovědět	předpovědět	k5eAaPmF	předpovědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nějaká	nějaký	k3yIgFnSc1	nějaký
kometa	kometa	k1gFnSc1	kometa
stane	stanout	k5eAaPmIp3nS	stanout
velkou	velký	k2eAgFnSc7d1	velká
kometou	kometa	k1gFnSc7	kometa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
jasnost	jasnost	k1gFnSc4	jasnost
komety	kometa	k1gFnSc2	kometa
působí	působit	k5eAaImIp3nS	působit
mnoho	mnoho	k4c1	mnoho
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
kometa	kometa	k1gFnSc1	kometa
velké	velká	k1gFnSc2	velká
a	a	k8xC	a
aktivní	aktivní	k2eAgNnSc1d1	aktivní
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
procházet	procházet	k5eAaImF	procházet
blízko	blízko	k7c2	blízko
povrchu	povrch	k1gInSc2	povrch
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
jasnosti	jasnost	k1gFnSc2	jasnost
v	v	k7c6	v
zákrytu	zákryt	k1gInSc6	zákryt
za	za	k7c7	za
Sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
šanci	šance	k1gFnSc4	šance
se	se	k3xPyFc4	se
zařadit	zařadit	k5eAaPmF	zařadit
mezi	mezi	k7c4	mezi
velké	velký	k2eAgFnPc4d1	velká
komety	kometa	k1gFnPc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Kohoutkova	Kohoutkův	k2eAgFnSc1d1	Kohoutkova
kometa	kometa	k1gFnSc1	kometa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
všechna	všechen	k3xTgNnPc4	všechen
tato	tento	k3xDgNnPc1	tento
kritéria	kritérion	k1gNnPc1	kritérion
splňovala	splňovat	k5eAaImAgNnP	splňovat
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
velké	velký	k2eAgNnSc1d1	velké
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
opak	opak	k1gInSc1	opak
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
pravdou	pravda	k1gFnSc7	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
kometa	kometa	k1gFnSc1	kometa
West	Westum	k1gNnPc2	Westum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
kometou	kometa	k1gFnSc7	kometa
stát	stát	k5eAaPmF	stát
neměla	mít	k5eNaImAgFnS	mít
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
působivá	působivý	k2eAgFnSc1d1	působivá
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zažilo	zažít	k5eAaPmAgNnS	zažít
lidstvo	lidstvo	k1gNnSc1	lidstvo
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
přestávkou	přestávka	k1gFnSc7	přestávka
mezi	mezi	k7c7	mezi
objevením	objevení	k1gNnSc7	objevení
se	se	k3xPyFc4	se
velkých	velký	k2eAgFnPc2d1	velká
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
hned	hned	k6eAd1	hned
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
komety	kometa	k1gFnPc4	kometa
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
–	–	k?	–
kometa	kometa	k1gFnSc1	kometa
Hyakutake	Hyakutake	k1gFnSc1	Hyakutake
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
následovaná	následovaný	k2eAgFnSc1d1	následovaná
Hale-Boppovou	Hale-Boppový	k2eAgFnSc7d1	Hale-Boppový
kometou	kometa	k1gFnSc7	kometa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
maxima	maximum	k1gNnSc2	maximum
jasnosti	jasnost	k1gFnSc2	jasnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
objevená	objevený	k2eAgFnSc1d1	objevená
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tisíců	tisíc	k4xCgInPc2	tisíc
známých	známý	k2eAgFnPc2d1	známá
komet	kometa	k1gFnPc2	kometa
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
neobvyklé	obvyklý	k2eNgFnPc1d1	neobvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Enckeova	Enckeův	k2eAgFnSc1d1	Enckeova
kometa	kometa	k1gFnSc1	kometa
má	mít	k5eAaImIp3nS	mít
dráhu	dráha	k1gFnSc4	dráha
ležící	ležící	k2eAgFnSc1d1	ležící
mezi	mezi	k7c7	mezi
oběžnými	oběžný	k2eAgFnPc7d1	oběžná
dráhami	dráha	k1gFnPc7	dráha
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
kometa	kometa	k1gFnSc1	kometa
Schwassmann-Wachmannova	Schwassmann-Wachmannov	k1gInSc2	Schwassmann-Wachmannov
má	mít	k5eAaImIp3nS	mít
nestabilní	stabilní	k2eNgFnSc4d1	nestabilní
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
celá	celá	k1gFnSc1	celá
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
Saturnem	Saturn	k1gMnSc7	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Kometa	kometa	k1gFnSc1	kometa
Chiron	Chiron	k1gInSc1	Chiron
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nestabilní	stabilní	k2eNgFnSc4d1	nestabilní
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
mezi	mezi	k7c7	mezi
Saturnem	Saturn	k1gMnSc7	Saturn
a	a	k8xC	a
Uranem	Uran	k1gMnSc7	Uran
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
klasifikovaná	klasifikovaný	k2eAgFnSc1d1	klasifikovaná
jako	jako	k8xS	jako
asteroid	asteroid	k1gInSc1	asteroid
(	(	kIx(	(
<g/>
dostala	dostat	k5eAaPmAgFnS	dostat
dokonce	dokonce	k9	dokonce
katalogové	katalogový	k2eAgNnSc4d1	Katalogové
číslo	číslo	k1gNnSc4	číslo
2060	[number]	k4	2060
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
její	její	k3xOp3gNnSc4	její
slabé	slabý	k2eAgNnSc4d1	slabé
koma	koma	k1gNnSc4	koma
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
za	za	k7c4	za
asteroid	asteroid	k1gInSc4	asteroid
považována	považován	k2eAgFnSc1d1	považována
kometa	kometa	k1gFnSc1	kometa
Shoemaker-Levy	Shoemaker-Leva	k1gFnSc2	Shoemaker-Leva
2	[number]	k4	2
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
označení	označení	k1gNnSc3	označení
1990	[number]	k4	1990
UL	ul	kA	ul
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
blízkozemní	blízkozemní	k2eAgFnPc1d1	blízkozemní
planetky	planetka	k1gFnPc1	planetka
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgMnPc4d1	považován
za	za	k7c4	za
vyhaslá	vyhaslý	k2eAgNnPc4d1	vyhaslé
jádra	jádro	k1gNnPc4	jádro
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
neuvolňují	uvolňovat	k5eNaImIp3nP	uvolňovat
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
rozpad	rozpad	k1gInSc1	rozpad
jádra	jádro	k1gNnSc2	jádro
komety	kometa	k1gFnSc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
příkladem	příklad	k1gInSc7	příklad
byla	být	k5eAaImAgFnS	být
kometa	kometa	k1gFnSc1	kometa
Biela	Biela	k1gFnSc1	Biela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozlomila	rozlomit	k5eAaPmAgFnS	rozlomit
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
perihelem	perihel	k1gInSc7	perihel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
komety	kometa	k1gFnSc2	kometa
potom	potom	k6eAd1	potom
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nepozorovaly	pozorovat	k5eNaImAgFnP	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgMnSc2	ten
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1872	[number]	k4	1872
a	a	k8xC	a
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měla	mít	k5eAaImAgFnS	mít
kometa	kometa	k1gFnSc1	kometa
být	být	k5eAaImF	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
<g/>
,	,	kIx,	,
pozorovány	pozorován	k2eAgInPc1d1	pozorován
velkolepé	velkolepý	k2eAgInPc1d1	velkolepý
meteoritické	meteoritický	k2eAgInPc1d1	meteoritický
roje	roj	k1gInPc1	roj
<g/>
.	.	kIx.	.
</s>
<s>
Slabý	slabý	k2eAgInSc1d1	slabý
meteoritický	meteoritický	k2eAgInSc1d1	meteoritický
roj	roj	k1gInSc1	roj
Andromedidy	Andromedida	k1gFnSc2	Andromedida
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
způsobený	způsobený	k2eAgInSc1d1	způsobený
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
přechází	přecházet	k5eAaImIp3nS	přecházet
původní	původní	k2eAgFnSc7d1	původní
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
komety	kometa	k1gFnSc2	kometa
Biela	Bielo	k1gNnSc2	Bielo
<g/>
.	.	kIx.	.
</s>
<s>
Rozpad	rozpad	k1gInSc1	rozpad
v	v	k7c6	v
perihelu	perihel	k1gInSc6	perihel
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
i	i	k9	i
u	u	k7c2	u
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
velké	velký	k2eAgFnSc2d1	velká
komety	kometa	k1gFnSc2	kometa
West	Westa	k1gFnPc2	Westa
a	a	k8xC	a
komety	kometa	k1gFnSc2	kometa
Ikeya-Seki	Ikeya-Sek	k1gFnSc2	Ikeya-Sek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
komety	kometa	k1gFnPc1	kometa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
části	část	k1gFnPc4	část
jednoho	jeden	k4xCgInSc2	jeden
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
významné	významný	k2eAgFnPc1d1	významná
pozorovaní	pozorovaný	k2eAgMnPc1d1	pozorovaný
kometárního	kometární	k2eAgInSc2d1	kometární
rozpadu	rozpad	k1gInSc2	rozpad
byl	být	k5eAaImAgInS	být
dopad	dopad	k1gInSc1	dopad
komety	kometa	k1gFnSc2	kometa
Shoemaker-Levy	Shoemaker-Leva	k1gFnSc2	Shoemaker-Leva
9	[number]	k4	9
<g/>
,	,	kIx,	,
pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
objevu	objev	k1gInSc2	objev
procházela	procházet	k5eAaImAgFnS	procházet
dráha	dráha	k1gFnSc1	dráha
komety	kometa	k1gFnSc2	kometa
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
gravitace	gravitace	k1gFnSc1	gravitace
kometu	kometa	k1gFnSc4	kometa
při	při	k7c6	při
blízkém	blízký	k2eAgInSc6d1	blízký
průletu	průlet	k1gInSc6	průlet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
zachytila	zachytit	k5eAaPmAgFnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
průlet	průlet	k1gInSc1	průlet
roztrhal	roztrhat	k5eAaPmAgInS	roztrhat
kometu	kometa	k1gFnSc4	kometa
na	na	k7c4	na
stovky	stovka	k1gFnPc4	stovka
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šestí	šestý	k4xOgMnPc1	šestý
dní	den	k1gInPc2	den
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1994	[number]	k4	1994
pak	pak	k6eAd1	pak
tyto	tento	k3xDgInPc1	tento
kusy	kus	k1gInPc1	kus
někdejší	někdejší	k2eAgFnSc2d1	někdejší
komety	kometa	k1gFnSc2	kometa
spadly	spadnout	k5eAaPmAgFnP	spadnout
na	na	k7c4	na
Jupiter	Jupiter	k1gInSc4	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
tak	tak	k9	tak
astronomové	astronom	k1gMnPc1	astronom
mohli	moct	k5eAaImAgMnP	moct
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
pozorovat	pozorovat	k5eAaImF	pozorovat
srážku	srážka	k1gFnSc4	srážka
dvou	dva	k4xCgInPc2	dva
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
objekt	objekt	k1gInSc1	objekt
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
za	za	k7c4	za
Tunguskou	Tunguský	k2eAgFnSc4d1	Tunguská
katastrofu	katastrofa	k1gFnSc4	katastrofa
nebyl	být	k5eNaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
fragmentů	fragment	k1gInPc2	fragment
Enckeovy	Enckeův	k2eAgFnSc2d1	Enckeova
komety	kometa	k1gFnSc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
díky	dík	k1gInPc7	dík
stále	stále	k6eAd1	stále
zlepšující	zlepšující	k2eAgFnSc6d1	zlepšující
se	se	k3xPyFc4	se
pozorovací	pozorovací	k2eAgFnSc3d1	pozorovací
technice	technika	k1gFnSc3	technika
objevují	objevovat	k5eAaImIp3nP	objevovat
nové	nový	k2eAgFnPc4d1	nová
a	a	k8xC	a
nové	nový	k2eAgFnPc4d1	nová
rozpadlé	rozpadlý	k2eAgFnPc4d1	rozpadlá
komety	kometa	k1gFnPc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
již	již	k9	již
i	i	k9	i
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
komety	kometa	k1gFnPc1	kometa
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
prakticky	prakticky	k6eAd1	prakticky
kdekoliv	kdekoliv	k6eAd1	kdekoliv
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
poutích	pouť	k1gFnPc6	pouť
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komety	kometa	k1gFnPc1	kometa
byly	být	k5eAaImAgFnP	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
námětem	námět	k1gInSc7	námět
pro	pro	k7c4	pro
autory	autor	k1gMnPc4	autor
literatury	literatura	k1gFnSc2	literatura
i	i	k8xC	i
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úplném	úplný	k2eAgInSc6d1	úplný
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
byly	být	k5eAaImAgFnP	být
mnohdy	mnohdy	k6eAd1	mnohdy
vykreslovány	vykreslován	k2eAgInPc1d1	vykreslován
jako	jako	k9	jako
tělesa	těleso	k1gNnPc1	těleso
nikoliv	nikoliv	k9	nikoliv
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hořlavá	hořlavý	k2eAgFnSc1d1	hořlavá
<g/>
.	.	kIx.	.
</s>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
Hector	Hector	k1gMnSc1	Hector
Servadac	Servadac	k1gInSc1	Servadac
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Na	na	k7c6	na
kometě	kometa	k1gFnSc6	kometa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
vysoce	vysoce	k6eAd1	vysoce
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
vize	vize	k1gFnSc1	vize
cestování	cestování	k1gNnSc2	cestování
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
na	na	k7c6	na
kometě	kometa	k1gFnSc6	kometa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výborné	výborný	k2eAgNnSc4d1	výborné
populární	populární	k2eAgNnSc4d1	populární
shrnutí	shrnutí	k1gNnSc4	shrnutí
astronomických	astronomický	k2eAgFnPc2d1	astronomická
znalostí	znalost	k1gFnPc2	znalost
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wellsa	k1gFnPc2	Wellsa
In	In	k1gMnSc1	In
the	the	k?	the
Days	Days	k1gInSc1	Days
of	of	k?	of
the	the	k?	the
Comet	Comet	k1gInSc1	Comet
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
plyny	plyn	k1gInPc1	plyn
z	z	k7c2	z
ohonu	ohon	k1gInSc2	ohon
komety	kometa	k1gFnSc2	kometa
způsobí	způsobit	k5eAaPmIp3nS	způsobit
vznik	vznik	k1gInSc1	vznik
utopie	utopie	k1gFnSc2	utopie
František	František	k1gMnSc1	František
Běhounek	běhounek	k1gMnSc1	běhounek
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Robinsoni	Robinson	k1gMnPc1	Robinson
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zabránit	zabránit	k5eAaPmF	zabránit
srážce	srážka	k1gFnSc3	srážka
komety	kometa	k1gFnSc2	kometa
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tove	Tove	k6eAd1	Tove
Jansson	Jansson	k1gMnSc1	Jansson
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Kometa	kometa	k1gFnSc1	kometa
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
svět	svět	k1gInSc4	svět
Mumínků	Mumínek	k1gInPc2	Mumínek
ohrožovaný	ohrožovaný	k2eAgInSc4d1	ohrožovaný
planoucí	planoucí	k2eAgInSc4d1	planoucí
kometou	kometa	k1gFnSc7	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
C.	C.	kA	C.
Clarke	Clarke	k1gFnSc1	Clarke
v	v	k7c6	v
románu	román	k1gInSc6	román
2061	[number]	k4	2061
<g/>
:	:	kIx,	:
Odyssey	Odyssea	k1gFnSc2	Odyssea
Three	Thre	k1gFnSc2	Thre
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
2061	[number]	k4	2061
<g/>
:	:	kIx,	:
Třetí	třetí	k4xOgFnSc1	třetí
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
výpravu	výprava	k1gFnSc4	výprava
na	na	k7c4	na
Halleyovu	Halleyův	k2eAgFnSc4d1	Halleyova
kometu	kometa	k1gFnSc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Heart	Hearta	k1gFnPc2	Hearta
of	of	k?	of
the	the	k?	the
Comet	Comet	k1gInSc1	Comet
od	od	k7c2	od
Gregoryho	Gregory	k1gMnSc2	Gregory
Benforda	Benford	k1gMnSc2	Benford
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Brina	Brin	k1gMnSc2	Brin
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
kolonizuje	kolonizovat	k5eAaBmIp3nS	kolonizovat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
tým	tým	k1gInSc1	tým
Halleyovu	Halleyův	k2eAgFnSc4d1	Halleyova
kometu	kometa	k1gFnSc4	kometa
stavbou	stavba	k1gFnSc7	stavba
příbytků	příbytek	k1gInPc2	příbytek
pod	pod	k7c7	pod
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Lucifer	Lucifer	k1gMnSc1	Lucifer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammra	k1gFnPc2	Hammra
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Luciferovo	Luciferův	k2eAgNnSc1d1	Luciferův
kladivo	kladivo	k1gNnSc1	kladivo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
od	od	k7c2	od
Larryho	Larry	k1gMnSc2	Larry
Nivena	Niven	k1gMnSc2	Niven
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
popsán	popsán	k2eAgInSc1d1	popsán
apokalyptický	apokalyptický	k2eAgInSc1d1	apokalyptický
příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
komety	kometa	k1gFnSc2	kometa
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
periodických	periodický	k2eAgFnPc2d1	periodická
komet	kometa	k1gFnPc2	kometa
Seznam	seznam	k1gInSc4	seznam
neperiodických	periodický	k2eNgFnPc2d1	neperiodická
komet	kometa	k1gFnPc2	kometa
Astrofyzika	astrofyzik	k1gMnSc2	astrofyzik
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Komety	kometa	k1gFnSc2	kometa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kometa	kometa	k1gFnSc1	kometa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
kometách	kometa	k1gFnPc6	kometa
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
kometách	kometa	k1gFnPc6	kometa
z	z	k7c2	z
Observatoře	observatoř	k1gFnSc2	observatoř
Kleť	Kleť	k1gFnSc1	Kleť
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cometography	Cometograph	k1gInPc1	Cometograph
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Přehled	přehled	k1gInSc1	přehled
od	od	k7c2	od
Davida	David	k1gMnSc2	David
Jewitta	Jewitt	k1gMnSc2	Jewitt
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Harvard	Harvard	k1gInSc1	Harvard
<g/>
:	:	kIx,	:
Lists	Lists	k1gInSc1	Lists
and	and	k?	and
Plots	Plots	k1gInSc1	Plots
<g/>
:	:	kIx,	:
Comets	Comets	k1gInSc1	Comets
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Open	Open	k1gInSc1	Open
Directory	Director	k1gInPc4	Director
Project	Project	k2eAgMnSc1d1	Project
<g/>
:	:	kIx,	:
Comets	Comets	k1gInSc1	Comets
</s>
