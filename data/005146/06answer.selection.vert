<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
komety	kometa	k1gFnPc1	kometa
–	–	k?	–
přesněji	přesně	k6eAd2	přesně
kometární	kometární	k2eAgNnPc4d1	kometární
jádra	jádro	k1gNnPc4	jádro
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
ve	v	k7c6	v
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
oblaku	oblak	k1gInSc6	oblak
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xS	jako
Oortův	Oortův	k2eAgInSc1d1	Oortův
oblak	oblak	k1gInSc1	oblak
(	(	kIx(	(
<g/>
pojmenovaném	pojmenovaný	k2eAgInSc6d1	pojmenovaný
podle	podle	k7c2	podle
holandského	holandský	k2eAgMnSc2d1	holandský
astronoma	astronom	k1gMnSc2	astronom
Jana	Jan	k1gMnSc2	Jan
Hendrika	Hendrik	k1gMnSc2	Hendrik
Oorta	Oort	k1gMnSc2	Oort
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
kolem	kolem	k7c2	kolem
50	[number]	k4	50
000	[number]	k4	000
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
