<s>
Deltora	Deltor	k1gMnSc2	Deltor
Quest	Quest	k1gInSc1	Quest
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
dětských	dětský	k2eAgInPc2d1	dětský
fantasy	fantas	k1gInPc1	fantas
knih	kniha	k1gFnPc2	kniha
australské	australský	k2eAgFnSc2d1	australská
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Emily	Emil	k1gMnPc4	Emil
Rodda	Roddo	k1gNnSc2	Roddo
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
byly	být	k5eAaImAgInP	být
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
také	také	k9	také
jako	jako	k9	jako
anime	animat	k5eAaPmIp3nS	animat
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
uvedený	uvedený	k2eAgInSc1d1	uvedený
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
jménem	jméno	k1gNnSc7	jméno
Deltora	Deltor	k1gMnSc2	Deltor
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
kdysi	kdysi	k6eAd1	kdysi
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
rod	rod	k1gInSc1	rod
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
jeden	jeden	k4xCgInSc4	jeden
obrovský	obrovský	k2eAgInSc4d1	obrovský
drahokam	drahokam	k1gInSc4	drahokam
s	s	k7c7	s
kouzelnou	kouzelný	k2eAgFnSc7d1	kouzelná
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rody	rod	k1gInPc4	rod
ale	ale	k8xC	ale
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
zlý	zlý	k2eAgMnSc1d1	zlý
Pán	pán	k1gMnSc1	pán
stínů	stín	k1gInPc2	stín
a	a	k8xC	a
vypadalo	vypadat	k5eAaImAgNnS	vypadat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nad	nad	k7c7	nad
nimi	on	k3xPp3gInPc7	on
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Deltoře	Deltora	k1gFnSc6	Deltora
se	se	k3xPyFc4	se
však	však	k9	však
našel	najít	k5eAaPmAgMnS	najít
hrdina	hrdina	k1gMnSc1	hrdina
-	-	kIx~	-
kovář	kovář	k1gMnSc1	kovář
Adin	Adin	k1gMnSc1	Adin
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
Del	Del	k1gFnSc2	Del
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
měl	mít	k5eAaImAgMnS	mít
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
viděl	vidět	k5eAaImAgInS	vidět
ocelový	ocelový	k2eAgInSc1d1	ocelový
pás	pás	k1gInSc1	pás
tvořený	tvořený	k2eAgInSc1d1	tvořený
medailony	medailon	k1gInPc4	medailon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
byly	být	k5eAaImAgFnP	být
zasazeny	zasadit	k5eAaPmNgInP	zasadit
talismany	talisman	k1gInPc1	talisman
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Adin	Adin	k1gMnSc1	Adin
takový	takový	k3xDgInSc4	takový
pás	pás	k1gInSc4	pás
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
a	a	k8xC	a
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
rody	rod	k1gInPc4	rod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gNnSc3	on
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
daly	dát	k5eAaPmAgInP	dát
své	svůj	k3xOyFgInPc4	svůj
drahokamy	drahokam	k1gInPc4	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
pás	pás	k1gInSc1	pás
Deltory	Deltor	k1gMnPc4	Deltor
kompletní	kompletní	k2eAgMnSc1d1	kompletní
<g/>
,	,	kIx,	,
Adin	Adin	k1gMnSc1	Adin
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
nasadil	nasadit	k5eAaPmAgMnS	nasadit
<g/>
.	.	kIx.	.
</s>
<s>
Pás	pás	k1gInSc1	pás
se	se	k3xPyFc4	se
rozzářil	rozzářit	k5eAaPmAgInS	rozzářit
a	a	k8xC	a
vyhnal	vyhnat	k5eAaPmAgInS	vyhnat
magii	magie	k1gFnSc3	magie
Pána	pán	k1gMnSc4	pán
stínu	stín	k1gInSc2	stín
z	z	k7c2	z
Deltory	Deltor	k1gInPc4	Deltor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gNnPc1	jeho
vojska	vojsko	k1gNnPc1	vojsko
byla	být	k5eAaImAgNnP	být
poražena	porazit	k5eAaPmNgNnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Deltorské	Deltorský	k2eAgInPc4d1	Deltorský
rody	rod	k1gInPc4	rod
pak	pak	k6eAd1	pak
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
sjednocené	sjednocený	k2eAgNnSc4d1	sjednocené
královstí	královstět	k5eAaPmIp3nS	královstět
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Adin	Adin	k1gInSc1	Adin
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
jeho	jeho	k3xOp3gNnSc4	jeho
potomci	potomek	k1gMnPc1	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
Adinův	Adinův	k2eAgMnSc1d1	Adinův
následník	následník	k1gMnSc1	následník
na	na	k7c6	na
sobě	se	k3xPyFc3	se
Pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
stínů	stín	k1gInPc2	stín
nemůže	moct	k5eNaImIp3nS	moct
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
Deltory	Deltor	k1gInPc5	Deltor
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
stínů	stín	k1gInPc2	stín
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
Deltory	Deltor	k1gMnPc4	Deltor
špehy	špeh	k1gMnPc4	špeh
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
vrchními	vrchní	k1gFnPc7	vrchní
rádci	rádce	k1gMnSc3	rádce
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
zavedli	zavést	k5eAaPmAgMnP	zavést
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
soubor	soubor	k1gInSc1	soubor
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgNnSc3	který
přestali	přestat	k5eAaPmAgMnP	přestat
králové	král	k1gMnPc1	král
nosit	nosit	k5eAaImF	nosit
Pás	pás	k1gInSc4	pás
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
začali	začít	k5eAaPmAgMnP	začít
vrchní	vrchní	k2eAgMnPc1d1	vrchní
rádci	rádce	k1gMnPc1	rádce
fakticky	fakticky	k6eAd1	fakticky
vládnout	vládnout	k5eAaImF	vládnout
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Deltora	Deltor	k1gMnSc4	Deltor
zchudla	zchudnout	k5eAaPmAgFnS	zchudnout
a	a	k8xC	a
nastal	nastat	k5eAaPmAgInS	nastat
hlad	hlad	k1gInSc1	hlad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
králové	král	k1gMnPc1	král
nezasáhli	zasáhnout	k5eNaPmAgMnP	zasáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
oklamáni	oklamat	k5eAaPmNgMnP	oklamat
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
ztratili	ztratit	k5eAaPmAgMnP	ztratit
důvěru	důvěra	k1gFnSc4	důvěra
národa	národ	k1gInSc2	národ
a	a	k8xC	a
Pás	pás	k1gInSc1	pás
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Služebníci	služebník	k1gMnPc1	služebník
Pána	pán	k1gMnSc2	pán
stínů	stín	k1gInPc2	stín
vytrhali	vytrhat	k5eAaPmAgMnP	vytrhat
z	z	k7c2	z
Pásu	pás	k1gInSc2	pás
drahokamy	drahokam	k1gInPc4	drahokam
a	a	k8xC	a
ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
je	on	k3xPp3gMnPc4	on
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nikdo	nikdo	k3yNnSc1	nikdo
nikdo	nikdo	k3yNnSc1	nikdo
neměl	mít	k5eNaImAgMnS	mít
najít	najít	k5eAaPmF	najít
(	(	kIx(	(
<g/>
drahokamy	drahokam	k1gInPc4	drahokam
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
nezničitelné	zničitelný	k2eNgInPc1d1	nezničitelný
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vynést	vynést	k5eAaPmF	vynést
je	on	k3xPp3gInPc4	on
z	z	k7c2	z
Deltory	Deltor	k1gMnPc4	Deltor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
stínů	stín	k1gInPc2	stín
si	se	k3xPyFc3	se
podmanil	podmanit	k5eAaPmAgMnS	podmanit
Deltoru	Deltor	k1gInSc2	Deltor
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
král	král	k1gMnSc1	král
Endon	Endon	k1gMnSc1	Endon
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
těhotnou	těhotný	k2eAgFnSc7d1	těhotná
manželkou	manželka	k1gFnSc7	manželka
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
díky	díky	k7c3	díky
Endonově	Endonův	k2eAgFnSc3d1	Endonův
příteli	přítel	k1gMnSc3	přítel
Jarredovi	Jarred	k1gMnSc3	Jarred
unikli	uniknout	k5eAaPmAgMnP	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Adinův	Adinův	k2eAgMnSc1d1	Adinův
následník	následník	k1gMnSc1	následník
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
<g/>
...	...	k?	...
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
knižní	knižní	k2eAgFnSc2d1	knižní
série	série	k1gFnSc2	série
je	být	k5eAaImIp3nS	být
Lief	Lief	k1gMnSc1	Lief
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jarreda	Jarreda	k1gMnSc1	Jarreda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgFnPc2	svůj
šestnáctých	šestnáctý	k4xOgFnPc2	šestnáctý
narozenin	narozeniny	k1gFnPc2	narozeniny
se	se	k3xPyFc4	se
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
vydává	vydávat	k5eAaPmIp3nS	vydávat
hledat	hledat	k5eAaImF	hledat
drahokamy	drahokam	k1gInPc4	drahokam
z	z	k7c2	z
Kouzelného	kouzelný	k2eAgInSc2d1	kouzelný
pásu	pás	k1gInSc2	pás
Deltory	Deltor	k1gInPc1	Deltor
<g/>
.	.	kIx.	.
</s>
<s>
Společníky	společník	k1gMnPc4	společník
mu	on	k3xPp3gMnSc3	on
dělají	dělat	k5eAaImIp3nP	dělat
Barda	bard	k1gMnSc4	bard
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
strážný	strážný	k1gMnSc1	strážný
z	z	k7c2	z
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jasmína	Jasmína	k1gFnSc1	Jasmína
<g/>
,	,	kIx,	,
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
bez	bez	k7c2	bez
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c6	v
Lesích	lese	k1gFnPc6	lese
hrůzovlády	hrůzovláda	k1gFnSc2	hrůzovláda
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
knižní	knižní	k2eAgFnPc1d1	knižní
série	série	k1gFnPc1	série
Deltory	Deltor	k1gInPc4	Deltor
-	-	kIx~	-
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sérii	série	k1gFnSc6	série
si	se	k3xPyFc3	se
přátelé	přítel	k1gMnPc1	přítel
vezmou	vzít	k5eAaPmIp3nP	vzít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
osvobodit	osvobodit	k5eAaPmF	osvobodit
Deltorany	Deltoran	k1gInPc4	Deltoran
zotročené	zotročený	k2eAgInPc4d1	zotročený
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
stínů	stín	k1gInPc2	stín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pán	pán	k1gMnSc1	pán
stínů	stín	k1gInPc2	stín
nechal	nechat	k5eAaPmAgMnS	nechat
rozmístit	rozmístit	k5eAaPmF	rozmístit
po	po	k7c6	po
Deltoře	Deltora	k1gFnSc6	Deltora
čtyři	čtyři	k4xCgFnPc4	čtyři
Sestry	sestra	k1gFnPc1	sestra
-	-	kIx~	-
zlé	zlý	k2eAgInPc1d1	zlý
drahokamy	drahokam	k1gInPc1	drahokam
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
podle	podle	k7c2	podle
deltorské	deltorský	k2eAgFnSc2d1	deltorský
pověsti	pověst	k1gFnSc2	pověst
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tráví	trávit	k5eAaImIp3nP	trávit
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
hladomor	hladomor	k1gInSc4	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Vydají	vydat	k5eAaPmIp3nP	vydat
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
deltorským	deltorský	k2eAgInSc7d1	deltorský
rodem	rod	k1gInSc7	rod
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Del	Del	k1gFnSc2	Del
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
talismanem	talisman	k1gInSc7	talisman
je	být	k5eAaImIp3nS	být
zlatý	zlatý	k2eAgInSc1d1	zlatý
topas	topas	k1gInSc1	topas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Pročisťuje	pročisťovat	k5eAaImIp3nS	pročisťovat
a	a	k8xC	a
vyjasňuje	vyjasňovat	k5eAaImIp3nS	vyjasňovat
mysl	mysl	k1gFnSc1	mysl
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
spojit	spojit	k5eAaPmF	spojit
živé	živá	k1gFnPc4	živá
se	s	k7c7	s
světem	svět	k1gInSc7	svět
duchů	duch	k1gMnPc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
nositele	nositel	k1gMnPc4	nositel
před	před	k7c7	před
hrůzami	hrůza	k1gFnPc7	hrůza
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
a	a	k8xC	a
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
během	během	k7c2	během
měsíčního	měsíční	k2eAgInSc2d1	měsíční
cyklu	cyklus	k1gInSc2	cyklus
(	(	kIx(	(
<g/>
největší	veliký	k2eAgMnSc1d3	veliký
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
úplňku	úplněk	k1gInSc2	úplněk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
rodem	rod	k1gInSc7	rod
jsou	být	k5eAaImIp3nP	být
Raladové	Raladový	k2eAgFnPc1d1	Raladový
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
bujné	bujný	k2eAgInPc4d1	bujný
zrzavé	zrzavý	k2eAgInPc4d1	zrzavý
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
černá	černý	k2eAgNnPc4d1	černé
očka	očko	k1gNnPc4	očko
jako	jako	k8xS	jako
korálky	korálek	k1gInPc4	korálek
a	a	k8xC	a
černomodrou	černomodrý	k2eAgFnSc4d1	černomodrá
vrásčitou	vrásčitý	k2eAgFnSc4d1	vrásčitá
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
drobní	drobný	k2eAgMnPc1d1	drobný
a	a	k8xC	a
útlí	útlý	k2eAgMnPc1d1	útlý
<g/>
.	.	kIx.	.
</s>
<s>
Umějí	umět	k5eAaImIp3nP	umět
hrát	hrát	k5eAaImF	hrát
krásně	krásně	k6eAd1	krásně
na	na	k7c4	na
flétny	flétna	k1gFnPc4	flétna
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
stavby	stavba	k1gFnPc1	stavba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Deltoře	Deltora	k1gFnSc6	Deltora
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
(	(	kIx(	(
<g/>
postavili	postavit	k5eAaPmAgMnP	postavit
také	také	k9	také
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
v	v	k7c6	v
Del	Del	k1gFnSc6	Del
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
talisman	talisman	k1gInSc1	talisman
je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc4d1	veliký
rubín	rubín	k1gInSc4	rubín
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
protilátkou	protilátka	k1gFnSc7	protilátka
hadího	hadí	k2eAgInSc2d1	hadí
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
zahání	zahánět	k5eAaImIp3nS	zahánět
zlé	zlý	k2eAgMnPc4d1	zlý
duchy	duch	k1gMnPc4	duch
a	a	k8xC	a
bledne	blednout	k5eAaImIp3nS	blednout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
zlo	zlo	k1gNnSc4	zlo
nebo	nebo	k8xC	nebo
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
-li	i	k?	-li
nějaké	nějaký	k3yIgNnSc4	nějaký
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rod	rod	k1gInSc1	rod
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Plání	pláň	k1gFnPc2	pláň
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
prapodivné	prapodivný	k2eAgFnPc1d1	prapodivná
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
talismanem	talisman	k1gInSc7	talisman
je	být	k5eAaImIp3nS	být
opál	opál	k1gInSc1	opál
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgFnSc1d1	hrající
všemi	všecek	k3xTgFnPc7	všecek
barvami	barva	k1gFnPc7	barva
duhy	duha	k1gFnSc2	duha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
a	a	k8xC	a
posiluje	posilovat	k5eAaImIp3nS	posilovat
slabý	slabý	k2eAgInSc4d1	slabý
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
rody	rod	k1gInPc7	rod
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Merové	Merové	k2eAgNnSc1d1	Merové
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
talismanem	talisman	k1gInSc7	talisman
je	být	k5eAaImIp3nS	být
lazurit	lazurit	k1gInSc1	lazurit
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tmavomodrou	tmavomodrý	k2eAgFnSc4d1	tmavomodrá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
posázen	posázet	k5eAaPmNgInS	posázet
bílými	bílý	k2eAgFnPc7d1	bílá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
noční	noční	k2eAgFnSc1d1	noční
obloha	obloha	k1gFnSc1	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Přináší	přinášet	k5eAaImIp3nS	přinášet
nositeli	nositel	k1gMnSc3	nositel
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rod	rod	k1gInSc1	rod
jsou	být	k5eAaImIp3nP	být
Gnómové	gnóm	k1gMnPc1	gnóm
z	z	k7c2	z
Hory	hora	k1gFnSc2	hora
děsu	děs	k1gInSc2	děs
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
drobní	drobný	k2eAgMnPc1d1	drobný
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
snědou	snědý	k2eAgFnSc4d1	snědá
pleť	pleť	k1gFnSc4	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
jeskyních	jeskyně	k1gFnPc6	jeskyně
a	a	k8xC	a
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
poklady	poklad	k1gInPc1	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Rádi	rád	k2eAgMnPc1d1	rád
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
talisman	talisman	k1gInSc1	talisman
je	být	k5eAaImIp3nS	být
zelený	zelený	k2eAgInSc1d1	zelený
elbait	elbait	k1gInSc1	elbait
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
cti	čest	k1gFnSc2	čest
<g/>
,	,	kIx,	,
ztmavne	ztmavnout	k5eAaPmIp3nS	ztmavnout
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
porušena	porušen	k2eAgFnSc1d1	porušena
přísaha	přísaha	k1gFnSc1	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
léčí	léčit	k5eAaImIp3nP	léčit
rány	rána	k1gFnPc1	rána
a	a	k8xC	a
vředy	vřed	k1gInPc1	vřed
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
protilátkou	protilátka	k1gFnSc7	protilátka
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mezi	mezi	k7c7	mezi
rody	rod	k1gInPc7	rod
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
Tory	tory	k1gMnSc1	tory
<g/>
,	,	kIx,	,
Torané	Toran	k1gMnPc1	Toran
<g/>
.	.	kIx.	.
</s>
<s>
Vládnou	vládnout	k5eAaImIp3nP	vládnout
velmi	velmi	k6eAd1	velmi
mocnými	mocný	k2eAgNnPc7d1	mocné
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
očarované	očarovaný	k2eAgNnSc1d1	očarované
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
vždy	vždy	k6eAd1	vždy
hojnost	hojnost	k1gFnSc1	hojnost
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
tam	tam	k6eAd1	tam
žít	žít	k5eAaImF	žít
žádné	žádný	k3yNgNnSc4	žádný
zlo	zlo	k1gNnSc4	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
talismanem	talisman	k1gInSc7	talisman
je	být	k5eAaImIp3nS	být
mocný	mocný	k2eAgInSc4d1	mocný
ametyst	ametyst	k1gInSc4	ametyst
<g/>
.	.	kIx.	.
</s>
<s>
Symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
uklidňuje	uklidňovat	k5eAaImIp3nS	uklidňovat
a	a	k8xC	a
konejší	konejšit	k5eAaImIp3nS	konejšit
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
barvu	barva	k1gFnSc4	barva
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
barvu	barva	k1gFnSc4	barva
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
otráveného	otrávený	k2eAgNnSc2d1	otrávené
jídla	jídlo	k1gNnSc2	jídlo
nebo	nebo	k8xC	nebo
pití	pití	k1gNnSc2	pití
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
rodem	rod	k1gInSc7	rod
jsou	být	k5eAaImIp3nP	být
Jalisové	Jalisový	k2eAgFnPc1d1	Jalisový
<g/>
.	.	kIx.	.
</s>
<s>
Jalisové	Jalisový	k2eAgNnSc1d1	Jalisový
byli	být	k5eAaImAgMnP	být
vždy	vždy	k6eAd1	vždy
velcí	velký	k2eAgMnPc1d1	velký
a	a	k8xC	a
silní	silný	k2eAgMnPc1d1	silný
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jen	jen	k9	jen
oni	onen	k3xDgMnPc1	onen
jediní	jediný	k2eAgMnPc1d1	jediný
pokusili	pokusit	k5eAaPmAgMnP	pokusit
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
Pánem	pán	k1gMnSc7	pán
stínů	stín	k1gInPc2	stín
<g/>
,	,	kIx,	,
když	když	k8xS	když
napadl	napadnout	k5eAaPmAgMnS	napadnout
Deltoru	Deltor	k1gMnSc3	Deltor
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
poraženi	poražen	k2eAgMnPc1d1	poražen
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zahynula	zahynout	k5eAaPmAgFnS	zahynout
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
odvlečena	odvléct	k5eAaPmNgFnS	odvléct
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
talismanem	talisman	k1gInSc7	talisman
je	být	k5eAaImIp3nS	být
diamant	diamant	k1gInSc1	diamant
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
nevinnosti	nevinnost	k1gFnSc2	nevinnost
<g/>
,	,	kIx,	,
čistoty	čistota	k1gFnSc2	čistota
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodává	dodávat	k5eAaImIp3nS	dodávat
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
morem	mor	k1gInSc7	mor
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
skutečné	skutečný	k2eAgFnSc3d1	skutečná
lásce	láska	k1gFnSc3	láska
<g/>
.	.	kIx.	.
</s>
<s>
Autorkou	autorka	k1gFnSc7	autorka
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
Emily	Emil	k1gMnPc4	Emil
Rodda	Rodd	k1gMnSc4	Rodd
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jennifer	Jennifra	k1gFnPc2	Jennifra
Rowe	Rowe	k1gFnSc7	Rowe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vydává	vydávat	k5eAaPmIp3nS	vydávat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Fragment	fragment	k1gInSc1	fragment
(	(	kIx(	(
<g/>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překládá	překládat	k5eAaImIp3nS	překládat
Olga	Olga	k1gFnSc1	Olga
Machútová	Machútová	k1gFnSc1	Machútová
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgInP	vydat
4	[number]	k4	4
díly	díl	k1gInPc1	díl
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
další	další	k2eAgFnSc1d1	další
4	[number]	k4	4
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
skončila	skončit	k5eAaPmAgFnS	skončit
základní	základní	k2eAgFnPc4d1	základní
série	série	k1gFnPc4	série
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vychází	vycházet	k5eAaImIp3nS	vycházet
3	[number]	k4	3
pokračování	pokračování	k1gNnSc2	pokračování
-	-	kIx~	-
druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
(	(	kIx(	(
<g/>
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Pána	pán	k1gMnSc2	pán
stínů	stín	k1gInPc2	stín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zatím	zatím	k6eAd1	zatím
vyšly	vyjít	k5eAaPmAgInP	vyjít
další	další	k2eAgInPc1d1	další
4	[number]	k4	4
(	(	kIx(	(
<g/>
série	série	k1gFnSc1	série
3	[number]	k4	3
<g/>
)	)	kIx)	)
Deltora	Deltor	k1gMnSc2	Deltor
1	[number]	k4	1
<g/>
:	:	kIx,	:
Lesy	les	k1gInPc1	les
hrůzovlády	hrůzovláda	k1gFnSc2	hrůzovláda
ISBN	ISBN	kA	ISBN
80-7200-888-9	[number]	k4	80-7200-888-9
Deltora	Deltor	k1gMnSc2	Deltor
2	[number]	k4	2
<g/>
:	:	kIx,	:
Jezero	jezero	k1gNnSc1	jezero
prokletí	prokletí	k1gNnSc2	prokletí
ISBN	ISBN	kA	ISBN
80-7200-889-7	[number]	k4	80-7200-889-7
Deltora	Deltor	k1gMnSc2	Deltor
3	[number]	k4	3
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
krys	krysa	k1gFnPc2	krysa
ISBN	ISBN	kA	ISBN
80-7200-922-2	[number]	k4	80-7200-922-2
Deltora	Deltor	k1gMnSc2	Deltor
4	[number]	k4	4
<g/>
:	:	kIx,	:
Poušť	poušť	k1gFnSc1	poušť
zkázy	zkáza	k1gFnSc2	zkáza
ISBN	ISBN	kA	ISBN
80-7200-921-4	[number]	k4	80-7200-921-4
Deltora	Deltor	k1gMnSc2	Deltor
5	[number]	k4	5
<g/>
:	:	kIx,	:
Hora	hora	k1gFnSc1	hora
<g />
.	.	kIx.	.
</s>
<s>
děsu	děs	k1gInSc3	děs
ISBN	ISBN	kA	ISBN
80-253-0018-8	[number]	k4	80-253-0018-8
Deltora	Deltor	k1gMnSc2	Deltor
6	[number]	k4	6
<g/>
:	:	kIx,	:
Labyrint	labyrint	k1gInSc1	labyrint
bestie	bestie	k1gFnSc2	bestie
ISBN	ISBN	kA	ISBN
80-7200-768-8	[number]	k4	80-7200-768-8
Deltora	Deltor	k1gMnSc2	Deltor
7	[number]	k4	7
<g/>
:	:	kIx,	:
Údolí	údolí	k1gNnSc2	údolí
ztracených	ztracený	k2eAgInPc2d1	ztracený
ISBN	ISBN	kA	ISBN
80-253-0019-6	[number]	k4	80-253-0019-6
Deltora	Deltor	k1gMnSc2	Deltor
8	[number]	k4	8
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Del	Del	k1gFnSc2	Del
ISBN	ISBN	kA	ISBN
80-253-0020-X	[number]	k4	80-253-0020-X
Deltora	Deltor	k1gMnSc2	Deltor
II	II	kA	II
1	[number]	k4	1
<g/>
:	:	kIx,	:
Jeskyně	jeskyně	k1gFnSc2	jeskyně
strachu	strach	k1gInSc2	strach
ISBN	ISBN	kA	ISBN
80-253-0291-1	[number]	k4	80-253-0291-1
Deltora	Deltor	k1gMnSc2	Deltor
II	II	kA	II
2	[number]	k4	2
<g/>
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
přeludů	přelud	k1gInPc2	přelud
ISBN	ISBN	kA	ISBN
80-253-0292-X	[number]	k4	80-253-0292-X
Deltora	Deltor	k1gMnSc2	Deltor
II	II	kA	II
3	[number]	k4	3
<g/>
:	:	kIx,	:
Země	země	k1gFnSc1	země
stínů	stín	k1gInPc2	stín
ISBN	ISBN	kA	ISBN
80-253-0293-8	[number]	k4	80-253-0293-8
Deltora	Deltor	k1gMnSc2	Deltor
III	III	kA	III
1	[number]	k4	1
<g/>
:	:	kIx,	:
Dračí	dračí	k2eAgNnSc1d1	dračí
hnízdo	hnízdo	k1gNnSc1	hnízdo
ISBN	ISBN	kA	ISBN
978-80-253-0397-9	[number]	k4	978-80-253-0397-9
Deltora	Deltor	k1gMnSc2	Deltor
III	III	kA	III
2	[number]	k4	2
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
stínů	stín	k1gInPc2	stín
ISBN	ISBN	kA	ISBN
978-80-253-0398-6	[number]	k4	978-80-253-0398-6
Deltora	Deltor	k1gMnSc2	Deltor
III	III	kA	III
3	[number]	k4	3
<g/>
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
smrti	smrt	k1gFnSc2	smrt
Deltora	Deltor	k1gMnSc2	Deltor
III	III	kA	III
4	[number]	k4	4
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
sestra	sestra	k1gFnSc1	sestra
Režisérem	režisér	k1gMnSc7	režisér
je	být	k5eAaImIp3nS	být
Micuru	Micur	k1gMnSc3	Micur
Hongo	Hongo	k6eAd1	Hongo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
seriál	seriál	k1gInSc1	seriál
má	mít	k5eAaImIp3nS	mít
65	[number]	k4	65
dílů	díl	k1gInPc2	díl
cca	cca	kA	cca
po	po	k7c6	po
20	[number]	k4	20
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
díly	díl	k1gInPc1	díl
jsou	být	k5eAaImIp3nP	být
spjaty	spjat	k2eAgInPc1d1	spjat
s	s	k7c7	s
první	první	k4xOgFnSc7	první
sérií	série	k1gFnSc7	série
knižních	knižní	k2eAgInPc2d1	knižní
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Autorčin	autorčin	k2eAgInSc1d1	autorčin
web	web	k1gInSc1	web
</s>
