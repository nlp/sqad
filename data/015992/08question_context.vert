<s>
Gaia	Gaia	k1gFnSc1
je	být	k5eAaImIp3nS
vesmírná	vesmírný	k2eAgFnSc1d1
astrometrická	astrometrický	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
kosmické	kosmický	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
(	(	kIx(
<g/>
ESA	eso	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
vynesena	vynést	k5eAaPmNgFnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
pomocí	pomocí	k7c2
rakety	raketa	k1gFnSc2
Sojuz	Sojuz	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>