<s>
Gaia	Gaia	k1gFnSc1
(	(	kIx(
<g/>
sonda	sonda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
probíhající	probíhající	k2eAgFnSc7d1
vesmírnou	vesmírný	k2eAgFnSc7d1
misí	mise	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
zde	zde	k6eAd1
uvedené	uvedený	k2eAgInPc4d1
se	s	k7c7
vzhledem	vzhled	k1gInSc7
k	k	k7c3
neustálému	neustálý	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
mohou	moct	k5eAaImIp3nP
průběžně	průběžně	k6eAd1
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
je	být	k5eAaImIp3nS
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
aktualizovat	aktualizovat	k5eAaBmF
a	a	k8xC
doplňovat	doplňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Gaia	Gaia	k6eAd1
vzhled	vzhled	k1gInSc4
sondy	sonda	k1gFnSc2
Pojmenováno	pojmenovat	k5eAaPmNgNnS
po	po	k7c6
</s>
<s>
Gaia	Gaia	k1gFnSc1
a	a	k8xC
interferometr	interferometr	k1gInSc1
Start	start	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc4
2013	#num#	k4
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Guyanské	Guyanský	k2eAgNnSc4d1
kosmické	kosmický	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
Nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
Sojuz	Sojuz	k1gInSc1
ST-B	ST-B	k1gMnSc1
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
ESA	eso	k1gNnPc4
Druh	druh	k1gInSc1
</s>
<s>
kosmická	kosmický	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
2	#num#	k4
029	#num#	k4
kg	kg	kA
Parametry	parametr	k1gInPc1
dráhy	dráha	k1gFnSc2
Perigeum	perigeum	k1gNnSc5
</s>
<s>
370	#num#	k4
000	#num#	k4
km	km	kA
Doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
</s>
<s>
180	#num#	k4
d	d	k?
Přístroje	přístroj	k1gInPc1
Nese	nést	k5eAaImIp3nS
přístroje	přístroj	k1gInPc1
</s>
<s>
Astro	astra	k1gFnSc5
<g/>
,	,	kIx,
BP	BP	kA
<g/>
/	/	kIx~
<g/>
RP	RP	kA
a	a	k8xC
RVS	RVS	kA
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gaia	Gaia	k1gFnSc1
je	být	k5eAaImIp3nS
vesmírná	vesmírný	k2eAgFnSc1d1
astrometrická	astrometrický	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
kosmické	kosmický	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
(	(	kIx(
<g/>
ESA	eso	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
vynesena	vynést	k5eAaPmNgFnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
pomocí	pomocí	k7c2
rakety	raketa	k1gFnSc2
Sojuz	Sojuz	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Podle	podle	k7c2
původních	původní	k2eAgInPc2d1
plánů	plán	k1gInPc2
měla	mít	k5eAaImAgFnS
sonda	sonda	k1gFnSc1
obsahovat	obsahovat	k5eAaImF
interferometr	interferometr	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
odráželo	odrážet	k5eAaImAgNnS
i	i	k9
v	v	k7c6
názvu	název	k1gInSc6
se	s	k7c7
zkratkou	zkratka	k1gFnSc7
GAIA	GAIA	kA
(	(	kIx(
<g/>
Global	globat	k5eAaImAgMnS
astrometric	astrometric	k1gMnSc1
interferometer	interferometer	k1gMnSc1
for	forum	k1gNnPc2
astrophysics	astrophysics	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přehodnocení	přehodnocení	k1gNnSc6
vybavení	vybavení	k1gNnSc2
bylo	být	k5eAaImAgNnS
označení	označení	k1gNnSc1
sondy	sonda	k1gFnSc2
upraveno	upravit	k5eAaPmNgNnS
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
jej	on	k3xPp3gNnSc4
pouze	pouze	k6eAd1
název	název	k1gInSc1
Gaia	Gaium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plán	plán	k1gInSc1
mise	mise	k1gFnSc2
</s>
<s>
Jejím	její	k3xOp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
posláním	poslání	k1gNnSc7
je	být	k5eAaImIp3nS
sestavit	sestavit	k5eAaPmF
třírozměrnou	třírozměrný	k2eAgFnSc4d1
mapu	mapa	k1gFnSc4
nejbližšího	blízký	k2eAgNnSc2d3
okolí	okolí	k1gNnSc2
v	v	k7c6
naší	náš	k3xOp1gFnSc6
galaxii	galaxie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgNnP
by	by	kYmCp3nP
zpřesnit	zpřesnit	k5eAaPmF
polohu	poloha	k1gFnSc4
přibližně	přibližně	k6eAd1
miliardy	miliarda	k4xCgFnPc4
hvězd	hvězda	k1gFnPc2
(	(	kIx(
<g/>
odhadem	odhad	k1gInSc7
1	#num#	k4
%	%	kIx~
hvězd	hvězda	k1gFnPc2
v	v	k7c6
naší	náš	k3xOp1gFnSc6
galaxii	galaxie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
150	#num#	k4
milionů	milion	k4xCgInPc2
nejjasnějších	jasný	k2eAgInPc2d3
z	z	k7c2
nich	on	k3xPp3gInPc2
pak	pak	k6eAd1
také	také	k9
jejich	jejich	k3xOp3gFnSc1
radiální	radiální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Údaje	údaj	k1gInPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
mnohem	mnohem	k6eAd1
přesnější	přesný	k2eAgMnSc1d2
a	a	k8xC
také	také	k9
rozsáhlejší	rozsáhlý	k2eAgFnPc1d2
<g/>
,	,	kIx,
než	než	k8xS
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
u	u	k7c2
dřívější	dřívější	k2eAgFnSc2d1
sondy	sonda	k1gFnSc2
Hipparcos	Hipparcos	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vybavení	vybavení	k1gNnSc1
sondy	sonda	k1gFnSc2
</s>
<s>
Součástí	součást	k1gFnSc7
sondy	sonda	k1gFnSc2
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc1
teleskopy	teleskop	k1gInPc1
a	a	k8xC
kamera	kamera	k1gFnSc1
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
rozlišením	rozlišení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
sondy	sonda	k1gFnSc2
je	být	k5eAaImIp3nS
740	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
mise	mise	k1gFnSc2
</s>
<s>
Sonda	sonda	k1gFnSc1
byla	být	k5eAaImAgFnS
vynesena	vynést	k5eAaPmNgFnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
raketou	raketa	k1gFnSc7
Sojuz	Sojuz	k1gInSc1
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Kourou	Kourou	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
měsíc	měsíc	k1gInSc4
sonda	sonda	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
cílového	cílový	k2eAgNnSc2d1
umístění	umístění	k1gNnSc2
v	v	k7c6
libračnímu	librační	k2eAgInSc3d1
bodu	bod	k1gInSc2
L	L	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
nastalo	nastat	k5eAaPmAgNnS
období	období	k1gNnSc3
testování	testování	k1gNnSc2
přístrojů	přístroj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
komplikacím	komplikace	k1gFnPc3
prodloužilo	prodloužit	k5eAaPmAgNnS
ze	z	k7c2
čtyř	čtyři	k4xCgNnPc2
na	na	k7c4
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
sonda	sonda	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
plánovaná	plánovaný	k2eAgNnPc4d1
pozorování	pozorování	k1gNnPc4
v	v	k7c6
plném	plný	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
<g/>
,	,	kIx,
předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
přitom	přitom	k6eAd1
její	její	k3xOp3gFnSc1
celková	celkový	k2eAgFnSc1d1
životnost	životnost	k1gFnSc1
5	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vědecké	vědecký	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
základě	základ	k1gInSc6
měření	měření	k1gNnSc2
sondy	sonda	k1gFnSc2
upřesněna	upřesněn	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
Slunce	slunce	k1gNnSc2
od	od	k7c2
galaktického	galaktický	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
na	na	k7c4
7,9	7,9	k4
kiloparseků	kiloparsek	k1gInPc2
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
26	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgInPc2d1
roků	rok	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
rychlost	rychlost	k1gFnSc1
oběhu	oběh	k1gInSc2
Slunce	slunce	k1gNnSc2
kolem	kolem	k7c2
středu	střed	k1gInSc2
naší	náš	k3xOp1gFnSc2
Galaxie	galaxie	k1gFnSc2
na	na	k7c4
přibližně	přibližně	k6eAd1
240	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
rei	rei	k?
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historická	historický	k2eAgFnSc1d1
mise	mise	k1gFnSc1
<g/>
:	:	kIx,
Sonda	sonda	k1gFnSc1
letí	letět	k5eAaImIp3nS
zmapovat	zmapovat	k5eAaPmF
miliardu	miliarda	k4xCgFnSc4
hvězd	hvězda	k1gFnPc2
v	v	k7c6
Mléčné	mléčný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-12-19	2013-12-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VOPLATKA	VOPLATKA	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strasti	strast	k1gFnSc3
družice	družice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
studuje	studovat	k5eAaImIp3nS
Galaxii	galaxie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tajemství	tajemství	k1gNnSc1
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
50	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1805	#num#	k4
<g/>
-	-	kIx~
<g/>
5249	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Photos	Photos	k1gMnSc1
<g/>
:	:	kIx,
Gaia	Gaia	k1gMnSc1
Spacecraft	Spacecraft	k1gMnSc1
to	ten	k3xDgNnSc4
Map	mapa	k1gFnPc2
Milky	Milka	k1gFnSc2
Way	Way	k1gFnSc2
Galaxy	Galax	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2013-12-12	2013-12-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gaia	Gaia	k1gFnSc1
startuje	startovat	k5eAaBmIp3nS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
přinese	přinést	k5eAaPmIp3nS
exoplanetám	exoplaneta	k1gFnPc3
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2013-12-16	2013-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
MARTINEK	Martinek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GAIA	GAIA	kA
upřesnila	upřesnit	k5eAaPmAgFnS
rychlost	rychlost	k1gFnSc1
oběhu	oběh	k1gInSc2
Slunce	slunce	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
vzdálenost	vzdálenost	k1gFnSc4
od	od	k7c2
středu	střed	k1gInSc2
Galaxie	galaxie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astro	astra	k1gFnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-02-23	2017-02-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gaia	Gai	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
mise	mise	k1gFnSc2
Gaia	Gaia	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2001009764	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
133881563	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2001009764	#num#	k4
</s>
