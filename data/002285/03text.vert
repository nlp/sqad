<s>
Jakarta	Jakarta	k1gFnSc1	Jakarta
(	(	kIx(	(
<g/>
Džakarta	Džakarta	k1gFnSc1	Džakarta
<g/>
,	,	kIx,	,
indonésky	indonésky	k6eAd1	indonésky
Jakarta	Jakarta	k1gFnSc1	Jakarta
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Batavia	Batavia	k1gFnSc1	Batavia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
bez	bez	k7c2	bez
aglomerací	aglomerace	k1gFnPc2	aglomerace
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
zvané	zvaný	k2eAgNnSc1d1	zvané
Jabotabek	Jabotabek	k1gInSc4	Jabotabek
více	hodně	k6eAd2	hodně
než	než	k8xS	než
26	[number]	k4	26
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
ostrova	ostrov	k1gInSc2	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
osídlením	osídlení	k1gNnSc7	osídlení
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
byl	být	k5eAaImAgInS	být
přístav	přístav	k1gInSc1	přístav
Kalapa	Kalap	k1gMnSc2	Kalap
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
přístavem	přístav	k1gInSc7	přístav
království	království	k1gNnSc2	království
Sundy	sund	k1gInPc1	sund
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
území	území	k1gNnSc4	území
tohoto	tento	k3xDgInSc2	tento
přístavu	přístav	k1gInSc2	přístav
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
od	od	k7c2	od
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
krále	král	k1gMnSc2	král
povolení	povolení	k1gNnSc2	povolení
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Jakartě	Jakarta	k1gFnSc6	Jakarta
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Sunda	Sunda	k1gMnSc1	Sunda
Kalapa	Kalap	k1gMnSc2	Kalap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1527	[number]	k4	1527
se	se	k3xPyFc4	se
přístav	přístav	k1gInSc1	přístav
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
vládce	vládce	k1gMnSc1	vládce
sultánátu	sultánát	k1gInSc2	sultánát
Bantam	bantam	k1gInSc1	bantam
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1527	[number]	k4	1527
Bantamané	Bantamaný	k2eAgFnSc2d1	Bantamaný
změnili	změnit	k5eAaPmAgMnP	změnit
jméno	jméno	k1gNnSc4	jméno
města	město	k1gNnSc2	město
z	z	k7c2	z
Kalapy	kalap	k1gInPc4	kalap
na	na	k7c4	na
Jayakarta	Jayakart	k1gMnSc4	Jayakart
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Jakarta	Jakarta	k1gFnSc1	Jakarta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
javánštině	javánština	k1gFnSc6	javánština
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vítězný	vítězný	k2eAgMnSc1d1	vítězný
a	a	k8xC	a
prosperující	prosperující	k2eAgMnSc1d1	prosperující
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
oficiálně	oficiálně	k6eAd1	oficiálně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
den	den	k1gInSc4	den
vzniku	vznik	k1gInSc2	vznik
města	město	k1gNnSc2	město
Jakarty	Jakarta	k1gFnSc2	Jakarta
<g/>
.	.	kIx.	.
</s>
<s>
Holanďané	Holanďan	k1gMnPc1	Holanďan
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Jayakarty	Jayakarta	k1gFnSc2	Jayakarta
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1619	[number]	k4	1619
holandské	holandský	k2eAgFnSc2d1	holandská
jednotky	jednotka	k1gFnSc2	jednotka
holandské	holandský	k2eAgFnSc2d1	holandská
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
Janem	Jan	k1gMnSc7	Jan
Pieterszoonem	Pieterszoon	k1gMnSc7	Pieterszoon
Coenem	Coen	k1gMnSc7	Coen
<g/>
,	,	kIx,	,
obsadily	obsadit	k5eAaPmAgFnP	obsadit
město	město	k1gNnSc4	město
a	a	k8xC	a
přejmenovaly	přejmenovat	k5eAaPmAgFnP	přejmenovat
ho	on	k3xPp3gNnSc4	on
z	z	k7c2	z
Jayakarty	Jayakarta	k1gFnSc2	Jayakarta
na	na	k7c4	na
Batavia	Batavius	k1gMnSc4	Batavius
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
latinské	latinský	k2eAgNnSc4d1	latinské
označení	označení	k1gNnSc4	označení
domorodých	domorodý	k2eAgInPc2d1	domorodý
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
žily	žít	k5eAaImAgFnP	žít
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
za	za	k7c2	za
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1621	[number]	k4	1621
se	se	k3xPyFc4	se
Batavie	Batavie	k1gFnSc1	Batavie
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Holandské	holandský	k2eAgFnSc2d1	holandská
Východní	východní	k2eAgFnSc2d1	východní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
Jávu	Jáva	k1gFnSc4	Jáva
a	a	k8xC	a
okupovali	okupovat	k5eAaBmAgMnP	okupovat
ji	on	k3xPp3gFnSc4	on
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
měli	mít	k5eAaImAgMnP	mít
plno	plno	k6eAd1	plno
starostí	starost	k1gFnPc2	starost
s	s	k7c7	s
napoleonskými	napoleonský	k2eAgFnPc7d1	napoleonská
válkami	válka	k1gFnPc7	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
nemohli	moct	k5eNaImAgMnP	moct
tak	tak	k6eAd1	tak
účinně	účinně	k6eAd1	účinně
bránit	bránit	k5eAaImF	bránit
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
převzali	převzít	k5eAaPmAgMnP	převzít
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Japonci	Japonec	k1gMnPc1	Japonec
a	a	k8xC	a
změnili	změnit	k5eAaPmAgMnP	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
dnešní	dnešní	k2eAgFnSc4d1	dnešní
Jakarta	Jakarta	k1gFnSc1	Jakarta
<g/>
.	.	kIx.	.
</s>
<s>
Jakarta	Jakarta	k1gFnSc1	Jakarta
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Indonésie	Indonésie	k1gFnSc2	Indonésie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
v	v	k7c6	v
rušné	rušný	k2eAgFnSc6d1	rušná
obchodní	obchodní	k2eAgFnSc6d1	obchodní
čtvrti	čtvrt	k1gFnSc6	čtvrt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Indonésie	Indonésie	k1gFnSc2	Indonésie
sérií	série	k1gFnPc2	série
bombových	bombový	k2eAgInPc2d1	bombový
útoků	útok	k1gInPc2	útok
teroristé	terorista	k1gMnPc1	terorista
tzv.	tzv.	kA	tzv.
islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
cizí	cizí	k2eAgMnPc1d1	cizí
státní	státní	k2eAgMnPc1d1	státní
příslušníci	příslušník	k1gMnPc1	příslušník
a	a	k8xC	a
tamní	tamní	k2eAgFnPc1d1	tamní
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
složky	složka	k1gFnPc1	složka
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
zabila	zabít	k5eAaPmAgFnS	zabít
5	[number]	k4	5
teroristů	terorista	k1gMnPc2	terorista
<g/>
,	,	kIx,	,
útok	útok	k1gInSc4	útok
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
dvě	dva	k4xCgFnPc4	dva
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1778	[number]	k4	1778
datuje	datovat	k5eAaImIp3nS	datovat
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
Indonéské	indonéský	k2eAgNnSc1d1	indonéské
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Museum	museum	k1gNnSc1	museum
Nasional	Nasional	k1gFnSc2	Nasional
Indonesia	Indonesium	k1gNnSc2	Indonesium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
péčí	péče	k1gFnSc7	péče
holandských	holandský	k2eAgMnPc2d1	holandský
učenců	učenec	k1gMnPc2	učenec
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xC	jako
učená	učený	k2eAgFnSc1d1	učená
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
koloniálních	koloniální	k2eAgInPc2d1	koloniální
úřadů	úřad	k1gInPc2	úřad
plnohodnotná	plnohodnotný	k2eAgFnSc1d1	plnohodnotná
muzejní	muzejní	k2eAgFnSc1d1	muzejní
instituce	instituce	k1gFnSc1	instituce
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
sochy	socha	k1gFnSc2	socha
stojící	stojící	k2eAgFnPc4d1	stojící
před	před	k7c7	před
hlavní	hlavní	k2eAgFnSc7d1	hlavní
budovou	budova	k1gFnSc7	budova
zvané	zvaný	k2eAgInPc1d1	zvaný
též	též	k6eAd1	též
Gedung	Gedung	k1gInSc4	Gedung
Gajah	Gajaha	k1gFnPc2	Gajaha
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sloní	sloní	k2eAgInSc1d1	sloní
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
Museum	museum	k1gNnSc1	museum
Gajah	Gajaha	k1gFnPc2	Gajaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jakartě	Jakarta	k1gFnSc6	Jakarta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rovněž	rovněž	k9	rovněž
největší	veliký	k2eAgFnSc1d3	veliký
mešita	mešita	k1gFnSc1	mešita
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
-	-	kIx~	-
mešita	mešita	k1gFnSc1	mešita
Istiqlal	Istiqlal	k1gFnSc1	Istiqlal
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
indonéského	indonéský	k2eAgInSc2d1	indonéský
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
je	být	k5eAaImIp3nS	být
Monas	Monas	k1gInSc1	Monas
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc1d1	národní
monument	monument	k1gInSc1	monument
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokončený	dokončený	k2eAgInSc1d1	dokončený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Jakarta	Jakarta	k1gFnSc1	Jakarta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jakarta	Jakarta	k1gFnSc1	Jakarta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
