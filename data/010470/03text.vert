<p>
<s>
Plešivka	Plešivka	k1gFnSc1	Plešivka
dlabaná	dlabaný	k2eAgFnSc1d1	dlabaná
(	(	kIx(	(
<g/>
Calvatia	Calvatia	k1gFnSc1	Calvatia
utriformis	utriformis	k1gFnSc2	utriformis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
pýchavka	pýchavka	k1gFnSc1	pýchavka
dlabaná	dlabaný	k2eAgFnSc1d1	dlabaná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedlá	jedlý	k2eAgFnSc1d1	jedlá
a	a	k8xC	a
chutná	chutný	k2eAgFnSc1d1	chutná
houba	houba	k1gFnSc1	houba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
pýchavkovitých	pýchavkovitý	k2eAgMnPc2d1	pýchavkovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Plodnice	plodnice	k1gFnSc1	plodnice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
5-20	[number]	k4	5-20
cm	cm	kA	cm
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
5-15	[number]	k4	5-15
cm	cm	kA	cm
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hruškovitý	hruškovitý	k2eAgInSc4d1	hruškovitý
až	až	k8xS	až
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
na	na	k7c6	na
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
stlačená	stlačený	k2eAgFnSc1d1	stlačená
až	až	k6eAd1	až
uťatá	uťatý	k2eAgFnSc1d1	uťatá
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
i	i	k9	i
obráceně	obráceně	k6eAd1	obráceně
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
nebo	nebo	k8xC	nebo
skoro	skoro	k6eAd1	skoro
válcovitá	válcovitý	k2eAgFnSc1d1	válcovitá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
drobně	drobně	k6eAd1	drobně
políčkovitě	políčkovitě	k6eAd1	políčkovitě
rozpukaná	rozpukaný	k2eAgFnSc1d1	rozpukaná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
špičatými	špičatý	k2eAgFnPc7d1	špičatá
bradavkami	bradavka	k1gFnPc7	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
okrovatí	okrovatět	k5eAaImIp3nS	okrovatět
a	a	k8xC	a
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
dostane	dostat	k5eAaPmIp3nS	dostat
šedohnědou	šedohnědý	k2eAgFnSc4d1	šedohnědá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
bázi	báze	k1gFnSc3	báze
řasnatí	řasnatět	k5eAaImIp3nS	řasnatět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opadání	opadání	k1gNnSc6	opadání
bradavek	bradavka	k1gFnPc2	bradavka
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
rozpadává	rozpadávat	k5eAaImIp3nS	rozpadávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teřich	Teřich	k?	Teřich
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
bílý	bílý	k2eAgMnSc1d1	bílý
a	a	k8xC	a
měkký	měkký	k2eAgMnSc1d1	měkký
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jemnou	jemný	k2eAgFnSc4d1	jemná
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
houbovou	houbový	k2eAgFnSc4d1	houbová
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
olivově	olivově	k6eAd1	olivově
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
a	a	k8xC	a
vodnatě	vodnatě	k6eAd1	vodnatě
měkne	měknout	k5eAaImIp3nS	měknout
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
suchý	suchý	k2eAgInSc1d1	suchý
a	a	k8xC	a
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pouze	pouze	k6eAd1	pouze
miskovitá	miskovitý	k2eAgFnSc1d1	miskovitá
spodní	spodní	k2eAgFnSc1d1	spodní
sterilní	sterilní	k2eAgFnSc1d1	sterilní
báze	báze	k1gFnSc1	báze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výtrusový	výtrusový	k2eAgInSc1d1	výtrusový
prach	prach	k1gInSc1	prach
je	být	k5eAaImIp3nS	být
černohnědý	černohnědý	k2eAgInSc1d1	černohnědý
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusy	výtrus	k1gInPc1	výtrus
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgInPc1d1	hladký
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
kulaté	kulatý	k2eAgFnPc4d1	kulatá
<g/>
,	,	kIx,	,
silnostěnné	silnostěnný	k2eAgFnPc4d1	silnostěnná
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
olejovou	olejový	k2eAgFnSc7d1	olejová
kapkou	kapka	k1gFnSc7	kapka
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
4,5	[number]	k4	4,5
<g/>
-	-	kIx~	-
<g/>
5,5	[number]	k4	5,5
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
houba	houba	k1gFnSc1	houba
roste	růst	k5eAaImIp3nS	růst
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
jednotlivě	jednotlivě	k6eAd1	jednotlivě
ale	ale	k9	ale
i	i	k9	i
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
skalních	skalní	k2eAgFnPc6d1	skalní
stepích	step	k1gFnPc6	step
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
jejich	jejich	k3xOp3gNnPc6	jejich
suchých	suchý	k2eAgNnPc6d1	suché
travnatých	travnatý	k2eAgNnPc6d1	travnaté
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
sušších	suchý	k2eAgNnPc6d2	sušší
otevřených	otevřený	k2eAgNnPc6d1	otevřené
travnatých	travnatý	k2eAgNnPc6d1	travnaté
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesních	lesní	k2eAgInPc6d1	lesní
lemech	lem	k1gInPc6	lem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
akátových	akátový	k2eAgInPc6d1	akátový
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
a	a	k8xC	a
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
pahorkatiny	pahorkatina	k1gFnPc4	pahorkatina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vyšší	vysoký	k2eAgFnPc4d2	vyšší
polohy	poloha	k1gFnPc4	poloha
a	a	k8xC	a
nehnojenou	hnojený	k2eNgFnSc4d1	nehnojená
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
teřich	teřich	k?	teřich
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
houba	houba	k1gFnSc1	houba
výborná	výborný	k2eAgFnSc1d1	výborná
například	například	k6eAd1	například
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
řízků	řízek	k1gInPc2	řízek
nebo	nebo	k8xC	nebo
mozečku	mozeček	k1gInSc2	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
je	být	k5eAaImIp3nS	být
i	i	k9	i
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
jako	jako	k9	jako
houbový	houbový	k2eAgInSc4d1	houbový
prášek	prášek	k1gInSc4	prášek
nebo	nebo	k8xC	nebo
do	do	k7c2	do
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
na	na	k7c6	na
zastavení	zastavení	k1gNnSc6	zastavení
krvácení	krvácení	k1gNnSc4	krvácení
přímým	přímý	k2eAgNnSc7d1	přímé
přiložením	přiložení	k1gNnSc7	přiložení
dužniny	dužnina	k1gFnSc2	dužnina
na	na	k7c4	na
ránu	rána	k1gFnSc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
začíná	začínat	k5eAaImIp3nS	začínat
dužnina	dužnina	k1gFnSc1	dužnina
žloutnout	žloutnout	k5eAaImF	žloutnout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
vhodná	vhodný	k2eAgFnSc1d1	vhodná
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
Bovista	Bovista	k1gMnSc1	Bovista
utriformis	utriformis	k1gFnSc2	utriformis
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Fr.	Fr.	k1gMnSc1	Fr.
<g/>
,	,	kIx,	,
Syst	Syst	k1gMnSc1	Syst
<g/>
.	.	kIx.	.
mycol	mycol	k1gInSc1	mycol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Lundae	Lundae	k1gInSc1	Lundae
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
25	[number]	k4	25
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Calvatia	Calvatia	k1gFnSc1	Calvatia
caelata	caele	k1gNnPc1	caele
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Cincinnati	Cincinnati	k1gFnSc2	Cincinnati
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
Nat	Nat	k1gMnSc1	Nat
<g/>
.	.	kIx.	.
</s>
<s>
Hist.	Hist.	k?	Hist.
12	[number]	k4	12
<g/>
:	:	kIx,	:
169	[number]	k4	169
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Calvatia	Calvatia	k1gFnSc1	Calvatia
caelata	caele	k1gNnPc1	caele
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Cincinnati	Cincinnati	k1gFnSc2	Cincinnati
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
Nat	Nat	k1gMnSc1	Nat
<g/>
.	.	kIx.	.
</s>
<s>
Hist.	Hist.	k?	Hist.
12	[number]	k4	12
<g/>
:	:	kIx,	:
169	[number]	k4	169
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
f.	f.	k?	f.
caelata	caele	k1gNnPc1	caele
</s>
</p>
<p>
<s>
Calvatia	Calvatia	k1gFnSc1	Calvatia
caelata	caele	k1gNnPc1	caele
f.	f.	k?	f.
exigua	exiguus	k1gMnSc2	exiguus
Hruby	Hruby	k?	Hruby
<g/>
,	,	kIx,	,
Hedwigia	Hedwigia	k1gFnSc1	Hedwigia
70	[number]	k4	70
<g/>
:	:	kIx,	:
346	[number]	k4	346
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Calvatia	Calvatia	k1gFnSc1	Calvatia
caelata	caele	k1gNnPc1	caele
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Cincinnati	Cincinnati	k1gFnSc2	Cincinnati
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
Nat	Nat	k1gMnSc1	Nat
<g/>
.	.	kIx.	.
</s>
<s>
Hist.	Hist.	k?	Hist.
12	[number]	k4	12
<g/>
:	:	kIx,	:
169	[number]	k4	169
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
var.	var.	k?	var.
caelata	caele	k1gNnPc1	caele
</s>
</p>
<p>
<s>
Calvatia	Calvatia	k1gFnSc1	Calvatia
caelata	caele	k1gNnPc1	caele
var.	var.	k?	var.
hungarica	hungaricus	k1gMnSc2	hungaricus
(	(	kIx(	(
<g/>
Hollós	Hollós	k1gInSc1	Hollós
<g/>
)	)	kIx)	)
F.	F.	kA	F.
Šmarda	Šmarda	k1gMnSc1	Šmarda
<g/>
,	,	kIx,	,
Fl	Fl	k1gMnSc1	Fl
<g/>
.	.	kIx.	.
</s>
<s>
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
B-	B-	k1gFnSc1	B-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Gasteromycetes	Gasteromycetes	k1gInSc1	Gasteromycetes
<g/>
:	:	kIx,	:
285	[number]	k4	285
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Calvatia	Calvatia	k1gFnSc1	Calvatia
hungarica	hungaricum	k1gNnSc2	hungaricum
Hollós	Hollósa	k1gFnPc2	Hollósa
<g/>
,	,	kIx,	,
Mathem	Math	k1gInSc7	Math
<g/>
.	.	kIx.	.
</s>
<s>
Természettud	Természettud	k6eAd1	Természettud
<g/>
.	.	kIx.	.
</s>
<s>
Ertes	Ertes	k1gInSc1	Ertes
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
:	:	kIx,	:
84	[number]	k4	84
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Calvatia	Calvatia	k1gFnSc1	Calvatia
utriformis	utriformis	k1gFnPc2	utriformis
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jaap	Jaap	k1gMnSc1	Jaap
<g/>
,	,	kIx,	,
Verh	Verh	k1gMnSc1	Verh
<g/>
.	.	kIx.	.
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
Ver	Ver	k?	Ver
<g/>
.	.	kIx.	.
</s>
<s>
Prov	Prov	k1gMnSc1	Prov
<g/>
.	.	kIx.	.
</s>
<s>
Brandenb	Brandenb	k1gInSc1	Brandenb
<g/>
.	.	kIx.	.
59	[number]	k4	59
<g/>
:	:	kIx,	:
37	[number]	k4	37
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Handkea	Handke	k2eAgFnSc1d1	Handke
utriformis	utriformis	k1gFnSc1	utriformis
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Kreisel	Kreisel	k1gInSc1	Kreisel
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Hedwigia	Hedwigia	k1gFnSc1	Hedwigia
48	[number]	k4	48
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
288	[number]	k4	288
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Handkea	Handke	k2eAgFnSc1d1	Handke
utriformis	utriformis	k1gFnSc1	utriformis
var.	var.	k?	var.
hungarica	hungarica	k1gFnSc1	hungarica
(	(	kIx(	(
<g/>
Hollós	Hollós	k1gInSc1	Hollós
<g/>
)	)	kIx)	)
Kreisel	Kreisel	k1gInSc1	Kreisel
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Hedwigia	Hedwigia	k1gFnSc1	Hedwigia
48	[number]	k4	48
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
289	[number]	k4	289
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Handkea	Handke	k2eAgFnSc1d1	Handke
utriformis	utriformis	k1gFnSc1	utriformis
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Kreisel	Kreisel	k1gInSc1	Kreisel
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
Hedwigia	Hedwigia	k1gFnSc1	Hedwigia
48	[number]	k4	48
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
288	[number]	k4	288
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
var.	var.	k?	var.
utriformis	utriformis	k1gInSc1	utriformis
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovista	k1gMnSc1	bovista
Pers.	Pers.	k1gMnSc1	Pers.
<g/>
,	,	kIx,	,
Observ	Observ	k1gMnSc1	Observ
<g/>
.	.	kIx.	.
mycol	mycol	k1gInSc1	mycol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Lipsiae	Lipsiae	k1gInSc1	Lipsiae
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
:	:	kIx,	:
4	[number]	k4	4
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovist	k1gMnSc2	bovist
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Sp	Sp	k1gFnSc1	Sp
<g/>
.	.	kIx.	.
pl.	pl.	k?	pl.
2	[number]	k4	2
<g/>
:	:	kIx,	:
1183	[number]	k4	1183
(	(	kIx(	(
<g/>
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
var.	var.	k?	var.
bovista	bovista	k1gMnSc1	bovista
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovista	k1gMnSc1	bovista
var.	var.	k?	var.
echinatum	echinatum	k1gNnSc4	echinatum
(	(	kIx(	(
<g/>
Schaeff	Schaeff	k1gInSc1	Schaeff
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Huds	Huds	k1gInSc1	Huds
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc4	Fl
<g/>
.	.	kIx.	.
</s>
<s>
Angl.	Angl.	k?	Angl.
<g/>
,	,	kIx,	,
Edn	Edn	k1gFnSc1	Edn
2	[number]	k4	2
2	[number]	k4	2
<g/>
:	:	kIx,	:
642	[number]	k4	642
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gInSc1	Lycoperdon
bovista	bovist	k1gMnSc2	bovist
var.	var.	k?	var.
echinatum	echinatum	k1gNnSc1	echinatum
Lightf	Lightf	k1gMnSc1	Lightf
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc4	Fl
<g/>
.	.	kIx.	.
</s>
<s>
Scot	Scot	k1gInSc1	Scot
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
1067	[number]	k4	1067
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovista	k1gMnSc1	bovista
var.	var.	k?	var.
glabrum	glabrum	k1gInSc1	glabrum
Lightf	Lightf	k1gInSc1	Lightf
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc4	Fl
<g/>
.	.	kIx.	.
</s>
<s>
Scot	Scot	k1gInSc1	Scot
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
1067	[number]	k4	1067
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gInSc1	Lycoperdon
bovista	bovist	k1gMnSc2	bovist
var.	var.	k?	var.
granulatum	granulatum	k1gNnSc1	granulatum
Lightf	Lightf	k1gMnSc1	Lightf
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc4	Fl
<g/>
.	.	kIx.	.
</s>
<s>
Scot	Scot	k1gInSc1	Scot
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
1067	[number]	k4	1067
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovista	k1gMnSc1	bovista
var.	var.	k?	var.
hispidum	hispidum	k1gInSc1	hispidum
Leers	Leers	k1gInSc1	Leers
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc1	Fl
<g/>
.	.	kIx.	.
herborn	herborn	k1gInSc1	herborn
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Edn	Edn	k1gFnSc1	Edn
2	[number]	k4	2
<g/>
:	:	kIx,	:
285	[number]	k4	285
(	(	kIx(	(
<g/>
no	no	k9	no
1114	[number]	k4	1114
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovista	k1gMnSc1	bovista
var.	var.	k?	var.
laeve	laevat	k5eAaPmIp3nS	laevat
Leers	Leers	k1gInSc1	Leers
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc1	Fl
<g/>
.	.	kIx.	.
herborn	herborn	k1gInSc1	herborn
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Edn	Edn	k1gFnSc1	Edn
2	[number]	k4	2
<g/>
:	:	kIx,	:
285	[number]	k4	285
(	(	kIx(	(
<g/>
no	no	k9	no
1114	[number]	k4	1114
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovista	k1gMnSc1	bovista
var.	var.	k?	var.
laeve	laeev	k1gFnSc2	laeev
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Hist.	Hist.	k1gMnSc1	Hist.
Champ	Champ	k1gMnSc1	Champ
<g/>
.	.	kIx.	.
</s>
<s>
Fr.	Fr.	k?	Fr.
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
:	:	kIx,	:
154	[number]	k4	154
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gInSc1	Lycoperdon
bovista	bovist	k1gMnSc2	bovist
var.	var.	k?	var.
maculatum	maculatum	k1gNnSc1	maculatum
Lightf	Lightf	k1gMnSc1	Lightf
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc4	Fl
<g/>
.	.	kIx.	.
</s>
<s>
Scot	Scot	k1gInSc1	Scot
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
1067	[number]	k4	1067
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
bovista	bovista	k1gMnSc1	bovista
var.	var.	k?	var.
vulgare	vulgar	k1gMnSc5	vulgar
Huds	Hudsa	k1gFnPc2	Hudsa
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Fl	Fl	k1gFnSc4	Fl
<g/>
.	.	kIx.	.
</s>
<s>
Angl.	Angl.	k?	Angl.
<g/>
,	,	kIx,	,
Edn	Edn	k1gFnSc1	Edn
2	[number]	k4	2
2	[number]	k4	2
<g/>
:	:	kIx,	:
642	[number]	k4	642
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gInSc1	Lycoperdon
caelatum	caelatum	k1gNnSc1	caelatum
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Herb	Herb	k1gInSc1	Herb
<g/>
.	.	kIx.	.
</s>
<s>
Fr.	Fr.	k?	Fr.
9	[number]	k4	9
<g/>
:	:	kIx,	:
tab	tab	kA	tab
<g/>
.	.	kIx.	.
430	[number]	k4	430
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
cepiforme	cepiform	k1gInSc5	cepiform
var.	var.	k?	var.
hungaricum	hungaricum	k1gInSc1	hungaricum
(	(	kIx(	(
<g/>
Hollós	Hollós	k1gInSc1	Hollós
<g/>
)	)	kIx)	)
Rick	Rick	k1gInSc1	Rick
<g/>
,	,	kIx,	,
in	in	k?	in
Rambo	Ramba	k1gMnSc5	Ramba
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iheringia	Iheringium	k1gNnSc2	Iheringium
<g/>
,	,	kIx,	,
Sér	sérum	k1gNnPc2	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
:	:	kIx,	:
464	[number]	k4	464
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gInSc1	Lycoperdon
echinatum	echinatum	k1gNnSc1	echinatum
Schaeff	Schaeff	k1gInSc1	Schaeff
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Fung	Fung	k1gMnSc1	Fung
<g/>
.	.	kIx.	.
bavar	bavar	k1gMnSc1	bavar
<g/>
.	.	kIx.	.
palat	palata	k1gFnPc2	palata
<g/>
.	.	kIx.	.
nasc	nasc	k1gFnSc1	nasc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ratisbonae	Ratisbonae	k1gInSc1	Ratisbonae
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
:	:	kIx,	:
128	[number]	k4	128
<g/>
,	,	kIx,	,
tab	tab	kA	tab
<g/>
.	.	kIx.	.
186	[number]	k4	186
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
sinclairii	sinclairie	k1gFnSc4	sinclairie
Berk	Berka	k1gFnPc2	Berka
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
as	as	k1gNnSc1	as
'	'	kIx"	'
<g/>
Sinclairi	Sinclairi	k1gNnSc1	Sinclairi
<g/>
'	'	kIx"	'
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
in	in	k?	in
Massee	Massee	k1gInSc1	Massee
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Roy	Roy	k1gMnSc1	Roy
<g/>
.	.	kIx.	.
</s>
<s>
Microscop	Microscop	k1gInSc1	Microscop
<g/>
.	.	kIx.	.
</s>
<s>
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
716	[number]	k4	716
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
utriforme	utriform	k1gInSc5	utriform
var.	var.	k?	var.
hungaricum	hungaricum	k1gInSc1	hungaricum
(	(	kIx(	(
<g/>
Hollós	Hollós	k1gInSc1	Hollós
<g/>
)	)	kIx)	)
Jalink	Jalink	k1gInSc1	Jalink
<g/>
,	,	kIx,	,
N.	N.	kA	N.
Amer.	Amer.	k1gMnSc1	Amer.
Fung	Fung	k1gMnSc1	Fung
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
176	[number]	k4	176
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
utriforme	utriform	k1gInSc5	utriform
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Hist.	Hist.	k1gMnSc1	Hist.
Champ	Champ	k1gMnSc1	Champ
<g/>
.	.	kIx.	.
</s>
<s>
Fr.	Fr.	k?	Fr.
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
153	[number]	k4	153
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
var.	var.	k?	var.
utriforme	utriform	k1gInSc5	utriform
</s>
</p>
<p>
<s>
Utraria	Utrarium	k1gNnPc1	Utrarium
caelata	caele	k1gNnPc1	caele
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Quél	Quél	k1gInSc1	Quél
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Mém	můj	k3xOp1gMnSc6	můj
<g/>
.	.	kIx.	.
</s>
<s>
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
Émul	Émul	k1gMnSc1	Émul
<g/>
.	.	kIx.	.
</s>
<s>
Montbéliard	Montbéliarda	k1gFnPc2	Montbéliarda
<g/>
,	,	kIx,	,
Sér	sérum	k1gNnPc2	sérum
<g/>
.	.	kIx.	.
2	[number]	k4	2
5	[number]	k4	5
<g/>
:	:	kIx,	:
369	[number]	k4	369
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Utraria	Utrarium	k1gNnPc1	Utrarium
utriformis	utriformis	k1gFnSc2	utriformis
(	(	kIx(	(
<g/>
Bull	bulla	k1gFnPc2	bulla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Quél	Quél	k1gInSc1	Quél
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Mém	můj	k3xOp1gNnSc6	můj
<g/>
.	.	kIx.	.
</s>
<s>
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
Émul	Émul	k1gMnSc1	Émul
<g/>
.	.	kIx.	.
</s>
<s>
Montbéliard	Montbéliarda	k1gFnPc2	Montbéliarda
<g/>
,	,	kIx,	,
Sér	sérum	k1gNnPc2	sérum
<g/>
.	.	kIx.	.
2	[number]	k4	2
5	[number]	k4	5
<g/>
:	:	kIx,	:
369	[number]	k4	369
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Záměna	záměna	k1gFnSc1	záměna
==	==	k?	==
</s>
</p>
<p>
<s>
pýchavka	pýchavka	k1gFnSc1	pýchavka
obrovská	obrovský	k2eAgFnSc1d1	obrovská
(	(	kIx(	(
<g/>
Langermannia	Langermannium	k1gNnSc2	Langermannium
gigantea	gigante	k1gInSc2	gigante
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
ale	ale	k8xC	ale
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průměru	průměr	k1gInSc2	průměr
až	až	k6eAd1	až
50	[number]	k4	50
cm	cm	kA	cm
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
v	v	k7c6	v
teřich	teřich	k?	teřich
a	a	k8xC	a
nohu	noha	k1gFnSc4	noha
</s>
</p>
<p>
<s>
pýchavka	pýchavka	k1gFnSc1	pýchavka
palicovitá	palicovitý	k2eAgFnSc1d1	palicovitá
(	(	kIx(	(
<g/>
Lycoperdon	Lycoperdon	k1gMnSc1	Lycoperdon
excipuliforme	excipuliform	k1gInSc5	excipuliform
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
lze	lze	k6eAd1	lze
zaměnit	zaměnit	k5eAaPmF	zaměnit
pouze	pouze	k6eAd1	pouze
velké	velký	k2eAgInPc1d1	velký
exempláře	exemplář	k1gInPc1	exemplář
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GRÜNERT	GRÜNERT	kA	GRÜNERT
<g/>
,	,	kIx,	,
Helmut	Helmut	k1gMnSc1	Helmut
<g/>
;	;	kIx,	;
GRÜNERT	GRÜNERT	kA	GRÜNERT
<g/>
,	,	kIx,	,
Renate	Renat	k1gInSc5	Renat
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houba	k1gFnPc5	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7176	[number]	k4	7176
<g/>
-	-	kIx~	-
<g/>
183	[number]	k4	183
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOLEC	holec	k1gMnSc1	holec
<g/>
,	,	kIx,	,
BIELICH	BIELICH	kA	BIELICH
<g/>
,	,	kIx,	,
BERAN	Beran	k1gMnSc1	Beran
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
hub	houba	k1gFnPc2	houba
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
624	[number]	k4	624
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2077	[number]	k4	2077
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
288	[number]	k4	288
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LAESSOE	LAESSOE	kA	LAESSOE
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
;	;	kIx,	;
DEL	DEL	kA	DEL
CONTE	CONTE	kA	CONTE
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
houbách	houba	k1gFnPc6	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
print	printa	k1gFnPc2	printa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85873	[number]	k4	85873
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAGARA	HAGARA	kA	HAGARA
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
BAIER	Baier	k1gMnSc1	Baier
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
atlas	atlas	k1gInSc4	atlas
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pýchavka	pýchavka	k1gFnSc1	pýchavka
dlabaná	dlabaný	k2eAgFnSc1d1	dlabaná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.houbareni.cz/houba.php?id=306	[url]	k4	http://www.houbareni.cz/houba.php?id=306
</s>
</p>
<p>
<s>
http://www.ohoubach.cz/atlas-hub/detail/189/Plesivka-dlabana/	[url]	k4	http://www.ohoubach.cz/atlas-hub/detail/189/Plesivka-dlabana/
</s>
</p>
