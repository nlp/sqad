<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
stupnice	stupnice	k1gFnSc1	stupnice
jaderných	jaderný	k2eAgFnPc2d1	jaderná
událostí	událost	k1gFnPc2	událost
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
International	International	k1gMnSc1	International
Nuclear	Nuclear	k1gMnSc1	Nuclear
Event	Event	k1gMnSc1	Event
Scale	Scale	k1gFnSc2	Scale
–	–	k?	–
INES	INES	kA	INES
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osmistupňová	osmistupňový	k2eAgFnSc1d1	osmistupňová
škála	škála	k1gFnSc1	škála
<g/>
,	,	kIx,	,
zavedená	zavedený	k2eAgFnSc1d1	zavedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
pro	pro	k7c4	pro
posuzování	posuzování	k1gNnSc4	posuzování
poruch	porucha	k1gFnPc2	porucha
a	a	k8xC	a
havárií	havárie	k1gFnPc2	havárie
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
