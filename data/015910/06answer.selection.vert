<s>
Velmistryně	velmistryně	k1gFnSc1
(	(	kIx(
<g/>
angl.	angl.	k?
Woman	Woman	k1gMnSc1
Grandmaster	Grandmaster	k1gMnSc1
<g/>
,	,	kIx,
WGM	WGM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
titul	titul	k1gInSc4
rezervovaný	rezervovaný	k2eAgInSc4d1
v	v	k7c6
šachu	šach	k1gInSc6
ženám	žena	k1gFnPc3
(	(	kIx(
<g/>
pomineme	pominout	k5eAaPmIp1nP
<g/>
-li	-li	k?
titul	titul	k1gInSc4
mistryně	mistryně	k1gFnSc2
světa	svět	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>