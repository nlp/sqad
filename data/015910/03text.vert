<s>
Velmistryně	velmistryně	k1gFnSc1
</s>
<s>
Velmistryně	velmistryně	k1gFnSc1
(	(	kIx(
<g/>
angl.	angl.	k?
Woman	Woman	k1gMnSc1
Grandmaster	Grandmaster	k1gMnSc1
<g/>
,	,	kIx,
WGM	WGM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
titul	titul	k1gInSc4
rezervovaný	rezervovaný	k2eAgInSc4d1
v	v	k7c6
šachu	šach	k1gInSc6
ženám	žena	k1gFnPc3
(	(	kIx(
<g/>
pomineme	pominout	k5eAaPmIp1nP
<g/>
-li	-li	k?
titul	titul	k1gInSc4
mistryně	mistryně	k1gFnSc2
světa	svět	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaveden	zaveden	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
prvním	první	k4xOgInSc6
udělení	udělení	k1gNnSc6
nižšího	nízký	k2eAgInSc2d2
titulu	titul	k1gInSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
mistryně	mistryně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Požadavky	požadavek	k1gInPc1
na	na	k7c4
udělení	udělení	k1gNnSc4
tohoto	tento	k3xDgInSc2
titulu	titul	k1gInSc2
jsou	být	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
menší	malý	k2eAgFnPc1d2
<g/>
,	,	kIx,
než	než	k8xS
požadavky	požadavek	k1gInPc1
na	na	k7c4
udělení	udělení	k1gNnSc4
titulu	titul	k1gInSc2
mezinárodního	mezinárodní	k2eAgMnSc4d1
velmistra	velmistr	k1gMnSc4
(	(	kIx(
<g/>
GM	GM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
se	se	k3xPyFc4
ovšem	ovšem	k9
vzájemně	vzájemně	k6eAd1
nevylučuje	vylučovat	k5eNaImIp3nS
<g/>
,	,	kIx,
takže	takže	k8xS
některé	některý	k3yIgFnPc1
šachistky	šachistka	k1gFnPc1
vlastní	vlastní	k2eAgFnPc1d1
tituly	titul	k1gInPc4
oba	dva	k4xCgInPc4
(	(	kIx(
<g/>
např.	např.	kA
Judit	Judit	k1gFnPc2
Polgárová	Polgárový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgFnPc1d1
nositelky	nositelka	k1gFnPc1
</s>
<s>
Jana	Jana	k1gFnSc1
Bellinová	Bellinový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Květa	Květa	k1gFnSc1
Eretová	Eretová	k1gFnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jana	Jana	k1gFnSc1
Jacková	Jacková	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petra	Petra	k1gFnSc1
Krupková	Krupková	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eva	Eva	k1gFnSc1
Kulovaná	kulovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Němcová	Němcová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tereza	Tereza	k1gFnSc1
Olšarová	Olšarová	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lenka	Lenka	k1gFnSc1
Ptáčníková	Ptáčníková	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
4	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eliška	Eliška	k1gFnSc1
Richtrová	Richtrová	k1gFnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olga	Olga	k1gFnSc1
Sikorová	Sikorová	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Joanna	Joanna	k1gFnSc1
Worek	Worky	k1gFnPc2
<g/>
[	[	kIx(
<g/>
p.	p.	k?
5	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
Petrová	Petrová	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
od	od	k7c2
roku	rok	k1gInSc2
1970	#num#	k4
reprezentuje	reprezentovat	k5eAaImIp3nS
Anglii	Anglie	k1gFnSc4
<g/>
1	#num#	k4
2	#num#	k4
není	být	k5eNaImIp3nS
aktivní	aktivní	k2eAgFnSc7d1
hráčkou	hráčka	k1gFnSc7
<g/>
↑	↑	k?
od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
reprezentuje	reprezentovat	k5eAaImIp3nS
USA	USA	kA
<g/>
↑	↑	k?
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
reprezentuje	reprezentovat	k5eAaImIp3nS
Island	Island	k1gInSc1
<g/>
↑	↑	k?
Česko	Česko	k1gNnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
reprezentovala	reprezentovat	k5eAaImAgFnS
Polsko	Polsko	k1gNnSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šachy	šach	k1gInPc1
</s>
