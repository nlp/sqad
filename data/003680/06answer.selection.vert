<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
chlor	chlor	k1gInSc1	chlor
přítomen	přítomen	k2eAgInSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
některých	některý	k3yIgNnPc2	některý
vnitrozemských	vnitrozemský	k2eAgNnPc2d1	vnitrozemské
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
