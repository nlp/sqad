<s>
Lípa	lípa	k1gFnSc1
Jana	Jan	k1gMnSc2
Gurreho	Gurre	k1gMnSc2
</s>
<s>
Památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
(	(	kIx(
<g/>
chráněný	chráněný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
Lípa	lípa	k1gFnSc1
Jana	Jan	k1gMnSc2
Gurreho	Gurre	k1gMnSc2
srpen	srpen	k1gInSc4
2009	#num#	k4
<g/>
Druh	druh	k1gInSc4
</s>
<s>
lípa	lípa	k1gFnSc1
velkolistáTilia	velkolistáTilium	k1gNnSc2
platyphyllos	platyphyllosa	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Evidenční	evidenční	k2eAgFnSc1d1
č.	č.	k?
</s>
<s>
103273	#num#	k4
(	(	kIx(
<g/>
10073	#num#	k4
<g/>
)	)	kIx)
Ochrana	ochrana	k1gFnSc1
</s>
<s>
od	od	k7c2
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2000	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Význam	význam	k1gInSc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
ekologický	ekologický	k2eAgInSc4d1
<g/>
,	,	kIx,
dendrologický	dendrologický	k2eAgInSc4d1
<g/>
,	,	kIx,
<g/>
historický	historický	k2eAgInSc4d1
<g/>
,	,	kIx,
dominanta	dominanta	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
Obec	obec	k1gFnSc1
</s>
<s>
Římov	Římov	k1gInSc1
Katastr	katastr	k1gInSc1
</s>
<s>
Římov	Římov	k1gInSc1
Poloha	poloha	k1gFnSc1
</s>
<s>
u	u	k7c2
kostela	kostel	k1gInSc2
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
480	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
22,86	22,86	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
17,25	17,25	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Lípa	lípa	k1gFnSc1
Jana	Jan	k1gMnSc2
Gurreho	Gurre	k1gMnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lípa	lípa	k1gFnSc1
Jana	Jan	k1gMnSc2
Gurreho	Gurre	k1gMnSc2
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgInSc1d3
a	a	k8xC
nejmohutnější	mohutný	k2eAgInSc1d3
památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
okresu	okres	k1gInSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
poblíž	poblíž	k7c2
poutního	poutní	k2eAgInSc2d1
areálu	areál	k1gInSc2
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
v	v	k7c6
centru	centrum	k1gNnSc6
obce	obec	k1gFnSc2
Římov	Římov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
název	název	k1gInSc1
<g/>
:	:	kIx,
lípa	lípa	k1gFnSc1
Jana	Jan	k1gMnSc2
Gurreho	Gurre	k1gMnSc2
<g/>
,	,	kIx,
Římovská	římovský	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
Lípa	lípa	k1gFnSc1
v	v	k7c6
Římově	Římov	k1gInSc6
III	III	kA
</s>
<s>
stáří	stáří	k1gNnSc1
<g/>
:	:	kIx,
600	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
,	,	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
650-660	650-660	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
680	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
~	~	kIx~
<g/>
400	#num#	k4
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
výška	výška	k1gFnSc1
<g/>
:	:	kIx,
19	#num#	k4
m	m	kA
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
14	#num#	k4
m	m	kA
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
10,5	10,5	k4
m	m	kA
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
12	#num#	k4
m	m	kA
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
15	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
výška	výška	k1gFnSc1
koruny	koruna	k1gFnSc2
<g/>
:	:	kIx,
9	#num#	k4
m	m	kA
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
šířka	šířka	k1gFnSc1
koruny	koruna	k1gFnSc2
<g/>
:	:	kIx,
17	#num#	k4
m	m	kA
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
obvod	obvod	k1gInSc1
kmene	kmen	k1gInSc2
<g/>
:	:	kIx,
681	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
730	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
740	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
760	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
737	#num#	k4
cm	cm	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
sanace	sanace	k1gFnSc1
<g/>
:	:	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
vítěz	vítěz	k1gMnSc1
soutěže	soutěž	k1gFnSc2
Strom	strom	k1gInSc1
roku	rok	k1gInSc2
2008	#num#	k4
</s>
<s>
zápis	zápis	k1gInSc1
v	v	k7c6
Guinnessově	Guinnessův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
rekordů	rekord	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
(	(	kIx(
<g/>
51	#num#	k4
osob	osoba	k1gFnPc2
v	v	k7c6
duté	dutý	k2eAgFnSc6d1
lípě	lípa	k1gFnSc6
o	o	k7c6
výšce	výška	k1gFnSc6
12	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
rekord	rekord	k1gInSc1
překonán	překonán	k2eAgInSc1d1
55	#num#	k4
osobami	osoba	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
</s>
<s>
souřadnice	souřadnice	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
'	'	kIx"
<g/>
22.86	22.86	k4
<g/>
"	"	kIx"
<g/>
N	N	kA
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
'	'	kIx"
<g/>
17.25	17.25	k4
<g/>
"	"	kIx"
<g/>
E	E	kA
</s>
<s>
Stav	stav	k1gInSc1
stromu	strom	k1gInSc2
a	a	k8xC
údržba	údržba	k1gFnSc1
</s>
<s>
Mohutný	mohutný	k2eAgInSc1d1
kmen	kmen	k1gInSc1
lípy	lípa	k1gFnSc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
dutý	dutý	k2eAgMnSc1d1
<g/>
,	,	kIx,
dutina	dutina	k1gFnSc1
je	být	k5eAaImIp3nS
u	u	k7c2
země	zem	k1gFnSc2
otevřená	otevřený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
středem	střed	k1gInSc7
vede	vést	k5eAaImIp3nS
adventivní	adventivní	k2eAgInSc4d1
kořen	kořen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
valné	valný	k2eAgFnSc2d1
části	část	k1gFnSc2
tvořena	tvořit	k5eAaImNgFnS
novými	nový	k2eAgInPc7d1
výmladky	výmladek	k1gInPc7
<g/>
,	,	kIx,
z	z	k7c2
původní	původní	k2eAgFnSc2d1
se	se	k3xPyFc4
zachovalo	zachovat	k5eAaPmAgNnS
minimum	minimum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1990	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
první	první	k4xOgFnSc3
novodobé	novodobý	k2eAgFnSc3d1
údržbě	údržba	k1gFnSc3
stromu	strom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
odstranění	odstranění	k1gNnSc4
větví	větev	k1gFnPc2
poškozených	poškozený	k2eAgFnPc2d1
bleskem	blesk	k1gInSc7
a	a	k8xC
ošetření	ošetření	k1gNnSc4
zabraňující	zabraňující	k2eAgNnSc4d1
šíření	šíření	k1gNnSc4
hniloby	hniloba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
ošetření	ošetření	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
100	#num#	k4
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
<g/>
)	)	kIx)
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
demolici	demolice	k1gFnSc3
přizděné	přizděný	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
a	a	k8xC
otevření	otevření	k1gNnSc1
zazděných	zazděný	k2eAgInPc2d1
i	i	k8xC
zabetonovaných	zabetonovaný	k2eAgInPc2d1
otvorů	otvor	k1gInPc2
<g/>
,	,	kIx,
odstranění	odstranění	k1gNnSc1
několika	několik	k4yIc2
kubických	kubický	k2eAgInPc2d1
metrů	metr	k1gInPc2
hniloby	hniloba	k1gFnSc2
z	z	k7c2
dutiny	dutina	k1gFnSc2
stromu	strom	k1gInSc2
<g/>
,	,	kIx,
zkrácení	zkrácení	k1gNnSc4
koruny	koruna	k1gFnSc2
o	o	k7c4
6	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
injektáži	injektáž	k1gFnSc3
výživy	výživa	k1gFnSc2
do	do	k7c2
okolí	okolí	k1gNnSc2
stromu	strom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
těchto	tento	k3xDgFnPc2
prací	práce	k1gFnPc2
byl	být	k5eAaImAgInS
odhalen	odhalit	k5eAaPmNgInS
vzdušný	vzdušný	k2eAgInSc1d1
kořen	kořen	k1gInSc1
o	o	k7c6
výšce	výška	k1gFnSc6
3,5	3,5	k4
metru	metr	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vede	vést	k5eAaImIp3nS
středem	střed	k1gInSc7
lípy	lípa	k1gFnSc2
a	a	k8xC
zajišťuje	zajišťovat	k5eAaImIp3nS
výživu	výživa	k1gFnSc4
a	a	k8xC
stabilitu	stabilita	k1gFnSc4
stromu	strom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
provedený	provedený	k2eAgInSc1d1
odhad	odhad	k1gInSc1
věku	věk	k1gInSc2
mluvil	mluvit	k5eAaImAgInS
o	o	k7c6
650-660	650-660	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
většina	většina	k1gFnSc1
odhadů	odhad	k1gInPc2
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
věku	věk	k1gInSc6
mezi	mezi	k7c7
600-700	600-700	k4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
uvádí	uvádět	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Němec	Němec	k1gMnSc1
jako	jako	k8xS,k8xC
pravděpodobnějších	pravděpodobný	k2eAgMnPc2d2
jen	jen	k6eAd1
zhruba	zhruba	k6eAd1
400	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc4d1
údržby	údržba	k1gFnPc4
se	se	k3xPyFc4
lípa	lípa	k1gFnSc1
dočkala	dočkat	k5eAaPmAgFnS
roku	rok	k1gInSc2
2009	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
výhry	výhra	k1gFnSc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Strom	strom	k1gInSc1
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
pověsti	pověst	k1gFnPc1
</s>
<s>
Strom	strom	k1gInSc1
byl	být	k5eAaImAgInS
vysazen	vysadit	k5eAaPmNgInS
kolem	kolem	k7c2
roku	rok	k1gInSc2
1330	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1626	#num#	k4
došlo	dojít	k5eAaPmAgNnS
pod	pod	k7c4
300	#num#	k4
let	léto	k1gNnPc2
starou	starý	k2eAgFnSc4d1
lípou	lípa	k1gFnSc7
k	k	k7c3
božskému	božský	k2eAgNnSc3d1
zjevení	zjevení	k1gNnSc3
šestadvacetiletému	šestadvacetiletý	k2eAgMnSc3d1
jezuitskému	jezuitský	k2eAgMnSc3d1
lékárníkovi	lékárník	k1gMnSc3
Janu	Jan	k1gMnSc3
Gurreovi	Gurreus	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gInSc6
základě	základ	k1gInSc6
se	se	k3xPyFc4
zasadil	zasadit	k5eAaPmAgInS
o	o	k7c4
výstavbu	výstavba	k1gFnSc4
poutního	poutní	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
–	–	k?
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
a	a	k8xC
loretánské	loretánský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Traduje	tradovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Gurre	Gurr	k1gInSc5
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
nechal	nechat	k5eAaPmAgMnS
posunout	posunout	k5eAaPmF
stavbu	stavba	k1gFnSc4
o	o	k7c4
tři	tři	k4xCgInPc4
metry	metr	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
lípa	lípa	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
nedotčena	dotknout	k5eNaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
římovskou	římovský	k2eAgFnSc4d1
Madonu	Madona	k1gFnSc4
přinesli	přinést	k5eAaPmAgMnP
poutníci	poutník	k1gMnPc1
z	z	k7c2
Habeše	Habeš	k1gFnSc2
položenou	položená	k1gFnSc4
na	na	k7c6
lipové	lipový	k2eAgFnSc6d1
větvi	větev	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větev	větev	k1gFnSc4
poté	poté	k6eAd1
zasadili	zasadit	k5eAaPmAgMnP
před	před	k7c7
kostelem	kostel	k1gInSc7
a	a	k8xC
během	běh	k1gInSc7
staletí	staletí	k1gNnSc2
z	z	k7c2
ní	on	k3xPp3gFnSc2
vyrostla	vyrůst	k5eAaPmAgFnS
současná	současný	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rekordy	rekord	k1gInPc1
<g/>
,	,	kIx,
tituly	titul	k1gInPc1
<g/>
,	,	kIx,
zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Provizorní	provizorní	k2eAgNnSc1d1
lešení	lešení	k1gNnSc1
postavené	postavený	k2eAgNnSc1d1
v	v	k7c6
dutině	dutina	k1gFnSc6
stromu	strom	k1gInSc2
při	při	k7c6
údržbě	údržba	k1gFnSc6
roku	rok	k1gInSc2
1996	#num#	k4
přivedlo	přivést	k5eAaPmAgNnS
obyvatele	obyvatel	k1gMnPc4
Římova	Římov	k1gInSc2
k	k	k7c3
zajímavé	zajímavý	k2eAgFnSc3d1
myšlence	myšlenka	k1gFnSc3
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1996	#num#	k4
se	se	k3xPyFc4
za	za	k7c2
přísných	přísný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
vměstnalo	vměstnat	k5eAaPmAgNnS
do	do	k7c2
lípy	lípa	k1gFnSc2
51	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
rekordu	rekord	k1gInSc3
ve	v	k7c6
znění	znění	k1gNnSc6
„	„	k?
<g/>
V	v	k7c6
duté	dutý	k2eAgFnSc6d1
lípě	lípa	k1gFnSc6
vysoké	vysoký	k2eAgFnSc6d1
12	#num#	k4
metrů	metr	k1gInPc2
bylo	být	k5eAaImAgNnS
51	#num#	k4
osob	osoba	k1gFnPc2
<g/>
“	“	k?
zapsaného	zapsaný	k2eAgMnSc2d1
v	v	k7c6
Guinnessově	Guinnessův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
rekordů	rekord	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Pokus	pokus	k1gInSc1
o	o	k7c4
druhý	druhý	k4xOgInSc4
rekord	rekord	k1gInSc4
inicializovala	inicializovat	k5eAaImAgFnS
Pelhřimovská	pelhřimovský	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
Dobrý	dobrý	k2eAgInSc1d1
den	den	k1gInSc4
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1997	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
lípy	lípa	k1gFnSc2
vešlo	vejít	k5eAaPmAgNnS
55	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
účastníky	účastník	k1gMnPc4
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
žena	žena	k1gFnSc1
a	a	k8xC
dva	dva	k4xCgMnPc1
nezletilí	nezletilý	k1gMnPc1
<g/>
;	;	kIx,
nejstaršímu	starý	k2eAgMnSc3d3
účastníkovi	účastník	k1gMnSc3
bylo	být	k5eAaImAgNnS
76	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
lípa	lípa	k1gFnSc1
Jana	Jan	k1gMnSc2
Gurreho	Gurre	k1gMnSc2
prohlášena	prohlásit	k5eAaPmNgFnS
stromem	strom	k1gInSc7
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
87	#num#	k4
338	#num#	k4
<g/>
,	,	kIx,
rozloženého	rozložený	k2eAgInSc2d1
mezi	mezi	k7c4
12	#num#	k4
finalistů	finalista	k1gMnPc2
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
téměř	téměř	k6eAd1
polovinu	polovina	k1gFnSc4
všech	všecek	k3xTgInPc2
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lípě	lípa	k1gFnSc3
byl	být	k5eAaImAgInS
věnován	věnovat	k5eAaImNgInS,k5eAaPmNgInS
prostor	prostor	k1gInSc1
v	v	k7c6
pořadu	pořad	k1gInSc6
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Paměť	paměť	k1gFnSc1
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
dílu	díl	k1gInSc6
č.	č.	k?
12	#num#	k4
Stromy	strom	k1gInPc7
u	u	k7c2
klášterů	klášter	k1gInPc2
a	a	k8xC
poutních	poutní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Registr	registr	k1gInSc4
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
AOPK	AOPK	kA
ČR	ČR	kA
drusop	drusop	k1gInSc4
<g/>
.	.	kIx.
<g/>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Úřad	úřad	k1gInSc1
města	město	k1gNnSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
odbor	odbor	k1gInSc1
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlášení	vyhlášení	k1gNnSc1
památného	památný	k2eAgInSc2d1
stromu	strom	k1gInSc2
podle	podle	k7c2
ustanovení	ustanovení	k1gNnSc2
§	§	k?
<g/>
46	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
zákona	zákon	k1gInSc2
ČNR	ČNR	kA
č.	č.	k?
114	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
ochraně	ochrana	k1gFnSc6
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Úřad	úřad	k1gInSc1
města	město	k1gNnSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
odbor	odbor	k1gInSc1
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
s.	s.	k?
S.	S.	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Informační	informační	k2eAgInSc4d1
list	list	k1gInSc4
obce	obec	k1gFnSc2
Římov	Římov	k1gInSc1
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2008	#num#	k4
"	"	kIx"
<g/>
STROM	strom	k1gInSc1
ROKU	rok	k1gInSc2
2008	#num#	k4
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
rimov	rimov	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
NĚMEC	Němec	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
224	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7033	#num#	k4
<g/>
-	-	kIx~
<g/>
781	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Hodnocení	hodnocení	k1gNnPc2
vybraných	vybraný	k2eAgInPc2d1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
Archivováno	archivován	k2eAgNnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
mzp	mzp	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Informační	informační	k2eAgInSc4d1
list	list	k1gInSc4
obce	obec	k1gFnSc2
Římov	Římov	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc4
2008	#num#	k4
Seznam	seznam	k1gInSc4
památných	památný	k2eAgMnPc2d1
<g/>
,	,	kIx,
chráněných	chráněný	k2eAgMnPc2d1
stromů	strom	k1gInPc2
v	v	k7c6
obci	obec	k1gFnSc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
rimov	rimov	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
HRUŠKOVÁ	Hrušková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
MICHÁLEK	Michálek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podivuhodné	podivuhodný	k2eAgInPc1d1
stromy	strom	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
Jaroslav	Jaroslav	k1gMnSc1
Turek	Turek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
Klub	klub	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
168	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
2950	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
43	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
,	,	kIx,
167	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HRUŠKOVÁ	Hrušková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
LUDVÍK	Ludvík	k1gMnSc1
<g/>
,	,	kIx,
Bedřich	Bedřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměť	paměť	k1gFnSc1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1500	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
XII	XII	kA
<g/>
,	,	kIx,
s.	s.	k?
181	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Českobudějovický	českobudějovický	k2eAgInSc4d1
deník	deník	k1gInSc4
"	"	kIx"
<g/>
Římovská	římovský	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
se	se	k3xPyFc4
utká	utkat	k5eAaPmIp3nS
o	o	k7c4
titul	titul	k1gInSc4
strom	strom	k1gInSc1
roku	rok	k1gInSc2
<g/>
"	"	kIx"
ceskobudejovicky	ceskobudejovicky	k6eAd1
<g/>
.	.	kIx.
<g/>
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
beseda	beseda	k1gFnSc1
Rijeka	Rijeka	k1gFnSc1
"	"	kIx"
<g/>
Stromem	strom	k1gInSc7
roku	rok	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
ČR	ČR	kA
stala	stát	k5eAaPmAgFnS
římovská	římovský	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
<g/>
"	"	kIx"
Archivováno	archivovat	k5eAaBmNgNnS
11	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
ceskabesedarijeka	ceskabesedarijeka	k6eAd1
<g/>
.	.	kIx.
<g/>
hr	hr	k6eAd1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NĚMEC	Němec	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
224	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7033	#num#	k4
<g/>
-	-	kIx~
<g/>
781	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HRUŠKOVÁ	Hrušková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
MICHÁLEK	Michálek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podivuhodné	podivuhodný	k2eAgInPc1d1
stromy	strom	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
Jaroslav	Jaroslav	k1gMnSc1
Turek	Turek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
Klub	klub	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
168	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
2950	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lípy	lípa	k1gFnPc1
v	v	k7c6
Římově	Římov	k1gInSc6
</s>
<s>
Lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Stromy	strom	k1gInPc1
</s>
