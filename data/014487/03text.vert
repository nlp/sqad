<s>
Franz	Franz	k1gMnSc1
Heilmayer	Heilmayer	k1gMnSc1
</s>
<s>
Franz	Franz	k1gMnSc1
Heilmayer	Heilmayer	k1gMnSc1
Franz	Franz	k1gMnSc1
Heilmayer	Heilmayer	k1gMnSc1
<g/>
,	,	kIx,
foto	foto	k1gNnSc1
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
r.	r.	kA
1907	#num#	k4
</s>
<s>
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1907	#num#	k4
–	–	k?
1918	#num#	k4
</s>
<s>
poslanec	poslanec	k1gMnSc1
Provizor	provizor	k1gMnSc1
<g/>
.	.	kIx.
nár	nár	k?
<g/>
.	.	kIx.
shromáždění	shromáždění	k1gNnSc1
Německého	německý	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1919	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Křesťansko	Křesťansko	k1gNnSc1
sociální	sociální	k2eAgFnSc2d1
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1859	#num#	k4
MattseeRakouské	MattseeRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
60	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
MattseeRakousko	MattseeRakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Franz	Franz	k1gMnSc1
Heilmayer	Heilmayer	k1gMnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1859	#num#	k4
Mattsee	Mattse	k1gInSc2
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
Mattsee	Mattsee	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
křesťansko	křesťansko	k6eAd1
sociální	sociální	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
poválečném	poválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
poslanec	poslanec	k1gMnSc1
rakouské	rakouský	k2eAgFnSc2d1
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Působil	působit	k5eAaImAgMnS
jako	jako	k9
barvířský	barvířský	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angažoval	angažovat	k5eAaBmAgMnS
se	se	k3xPyFc4
v	v	k7c4
Křesťansko	Křesťansko	k1gNnSc4
sociální	sociální	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
živnostenského	živnostenský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
v	v	k7c6
soudním	soudní	k2eAgInSc6d1
okresu	okres	k1gInSc6
Mattsee	Mattse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
obecní	obecní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
starostou	starosta	k1gMnSc7
Mattsee	Mattse	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgInS
i	i	k9
do	do	k7c2
celostátní	celostátní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
,	,	kIx,
konaných	konaný	k2eAgFnPc2d1
poprvé	poprvé	k6eAd1
podle	podle	k7c2
všeobecného	všeobecný	k2eAgNnSc2d1
a	a	k8xC
rovného	rovný	k2eAgNnSc2d1
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
mandát	mandát	k1gInSc4
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
(	(	kIx(
<g/>
celostátní	celostátní	k2eAgInSc1d1
zákonodárný	zákonodárný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
)	)	kIx)
za	za	k7c4
obvod	obvod	k1gInSc4
Salcbursko	Salcbursko	k1gNnSc1
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usedl	usednout	k5eAaPmAgMnS
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
frakce	frakce	k1gFnSc2
Křesťansko-sociální	křesťansko-sociální	k2eAgNnSc4d1
sjednocení	sjednocení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandát	mandát	k1gInSc1
obhájil	obhájit	k5eAaPmAgInS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1911	#num#	k4
za	za	k7c4
týž	týž	k3xTgInSc4
obvod	obvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usedl	usednout	k5eAaPmAgMnS
do	do	k7c2
klubu	klub	k1gInSc2
Křesťansko-sociální	křesťansko-sociální	k2eAgInSc1d1
klub	klub	k1gInSc1
německých	německý	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vídeňském	vídeňský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
setrval	setrvat	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
zániku	zánik	k1gInSc2
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
roku	rok	k1gInSc3
1911	#num#	k4
se	se	k3xPyFc4
profesně	profesně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
starosta	starosta	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
zasedal	zasedat	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1918	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
jako	jako	k8xC,k8xS
poslanec	poslanec	k1gMnSc1
Provizorního	provizorní	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
Německého	německý	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
(	(	kIx(
<g/>
Provisorische	Provisorische	k1gFnSc1
Nationalversammlung	Nationalversammlung	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Atanas	Atanas	k1gMnSc1
Guggenberg	Guggenberg	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
parlament	parlament	k1gInSc1
<g/>
.	.	kIx.
<g/>
gv	gv	k?
<g/>
.	.	kIx.
<g/>
at	at	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Databáze	databáze	k1gFnSc2
stenografických	stenografický	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
a	a	k8xC
rejstříků	rejstřík	k1gInPc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
z	z	k7c2
příslušných	příslušný	k2eAgNnPc2d1
volebních	volební	k2eAgNnPc2d1
období	období	k1gNnPc2
<g/>
,	,	kIx,
http://alex.onb.ac.at/spa.htm.	http://alex.onb.ac.at/spa.htm.	k?
<g/>
↑	↑	k?
http://alex.onb.ac.at/cgi-content/alex?aid=spa&	http://alex.onb.ac.at/cgi-content/alex?aid=spa&	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1105583775	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83146825115007631625	#num#	k4
</s>
