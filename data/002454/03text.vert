<s>
Cizinec	cizinec	k1gMnSc1	cizinec
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Étranger	Étranger	k1gMnSc1	Étranger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
<g/>
/	/	kIx~	/
<g/>
novela	novela	k1gFnSc1	novela
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Alberta	Albert	k1gMnSc2	Albert
Camuse	Camuse	k1gFnSc2	Camuse
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
líčí	líčit	k5eAaImIp3nS	líčit
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
Meursault	Meursault	k1gMnSc1	Meursault
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
pohřbu	pohřeb	k1gInSc2	pohřeb
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
citů	cit	k1gInPc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
Jistá	jistý	k2eAgFnSc1d1	jistá
lhostejnost	lhostejnost	k1gFnSc1	lhostejnost
až	až	k8xS	až
indolence	indolence	k1gFnSc1	indolence
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
i	i	k9	i
jeho	jeho	k3xOp3gInSc4	jeho
další	další	k2eAgInSc4d1	další
běžný	běžný	k2eAgInSc4d1	běžný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	s	k7c7	s
sousedem	soused	k1gMnSc7	soused
Raymondem	Raymond	k1gMnSc7	Raymond
<g/>
,	,	kIx,	,
podezřelým	podezřelý	k2eAgMnSc7d1	podezřelý
z	z	k7c2	z
pasáctví	pasáctví	k1gNnSc2	pasáctví
<g/>
,	,	kIx,	,
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
mu	on	k3xPp3gNnSc3	on
se	se	k3xPyFc4	se
pomstít	pomstít	k5eAaPmF	pomstít
své	svůj	k3xOyFgFnPc4	svůj
arabské	arabský	k2eAgFnPc4d1	arabská
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gNnSc4	on
podvedla	podvést	k5eAaPmAgFnS	podvést
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedělním	nedělní	k2eAgInSc6d1	nedělní
výletu	výlet	k1gInSc6	výlet
na	na	k7c4	na
pláž	pláž	k1gFnSc4	pláž
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
Raymondem	Raymond	k1gInSc7	Raymond
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
potyčky	potyčka	k1gFnSc2	potyčka
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
sousedovy	sousedův	k2eAgFnSc2d1	sousedova
expřítelkyně	expřítelkyně	k1gFnSc2	expřítelkyně
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
arabským	arabský	k2eAgMnSc7d1	arabský
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
jeho	jeho	k3xOp3gFnSc2	jeho
bývalé	bývalý	k2eAgFnSc2d1	bývalá
milenky	milenka	k1gFnSc2	milenka
ho	on	k3xPp3gMnSc4	on
zraní	zranit	k5eAaPmIp3nS	zranit
nožem	nůž	k1gInSc7	nůž
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Meursault	Meursault	k1gInSc1	Meursault
se	se	k3xPyFc4	se
jde	jít	k5eAaImIp3nS	jít
poté	poté	k6eAd1	poté
projít	projít	k5eAaPmF	projít
po	po	k7c6	po
pláži	pláž	k1gFnSc6	pláž
a	a	k8xC	a
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
slunícího	slunící	k2eAgMnSc4d1	slunící
se	se	k3xPyFc4	se
araba	arab	k1gMnSc2	arab
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
arab	arab	k1gMnSc1	arab
zpozoruje	zpozorovat	k5eAaPmIp3nS	zpozorovat
<g/>
,	,	kIx,	,
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
nůž	nůž	k1gInSc4	nůž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Meursault	Meursault	k1gMnSc1	Meursault
ho	on	k3xPp3gInSc4	on
bez	bez	k1gInSc4	bez
přemýšlení	přemýšlení	k1gNnSc2	přemýšlení
zastřelí	zastřelit	k5eAaPmIp3nP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
líčí	líčit	k5eAaImIp3nS	líčit
soudní	soudní	k2eAgNnSc4d1	soudní
přelíčení	přelíčení	k1gNnSc4	přelíčení
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
se	se	k3xPyFc4	se
Meursault	Meursault	k2eAgInSc1d1	Meursault
ani	ani	k8xC	ani
nehájí	hájit	k5eNaImIp3nS	hájit
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
ani	ani	k8xC	ani
poukázat	poukázat	k5eAaPmF	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
před	před	k7c7	před
vraždou	vražda	k1gFnSc7	vražda
cítil	cítit	k5eAaImAgInS	cítit
skutečně	skutečně	k6eAd1	skutečně
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
končí	končit	k5eAaImIp3nS	končit
Meursaultovými	Meursaultův	k2eAgFnPc7d1	Meursaultův
úvahami	úvaha	k1gFnPc7	úvaha
v	v	k7c6	v
cele	cela	k1gFnSc6	cela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
vykonání	vykonání	k1gNnSc4	vykonání
rozsudku	rozsudek	k1gInSc2	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
úsudku	úsudek	k1gInSc2	úsudek
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Sartra	Sartra	k1gFnSc1	Sartra
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
"	"	kIx"	"
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
děl	dělo	k1gNnPc2	dělo
raného	raný	k2eAgInSc2d1	raný
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
,	,	kIx,	,
Camus	Camus	k1gMnSc1	Camus
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
existencialismem	existencialismus	k1gInSc7	existencialismus
rozešel	rozejít	k5eAaPmAgInS	rozejít
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
filosofický	filosofický	k2eAgInSc4d1	filosofický
postoj	postoj	k1gInSc4	postoj
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
spíš	spíš	k9	spíš
jako	jako	k9	jako
filosofii	filosofie	k1gFnSc4	filosofie
absurdity	absurdita	k1gFnSc2	absurdita
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
zfilmován	zfilmován	k2eAgMnSc1d1	zfilmován
a	a	k8xC	a
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgNnPc2d1	další
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
natočil	natočit	k5eAaBmAgMnS	natočit
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
italsko-francouzské	italskorancouzský	k2eAgNnSc4d1	italsko-francouzský
drama	drama	k1gNnSc4	drama
italský	italský	k2eAgMnSc1d1	italský
režisér	režisér	k1gMnSc1	režisér
Luchino	Luchin	k2eAgNnSc4d1	Luchino
Visconti	Visconti	k1gNnSc4	Visconti
s	s	k7c7	s
Marcello	Marcello	k1gNnSc4	Marcello
Mastroiannim	Mastroiannim	k1gInSc1	Mastroiannim
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
poprvé	poprvé	k6eAd1	poprvé
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
překlad	překlad	k1gInSc1	překlad
Miloslava	Miloslav	k1gMnSc2	Miloslav
Žiliny	Žilina	k1gFnSc2	Žilina
vyšel	vyjít	k5eAaPmAgMnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
a	a	k8xC	a
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
1988	[number]	k4	1988
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
