<p>
<s>
Mohsova	Mohsův	k2eAgFnSc1d1	Mohsova
stupnice	stupnice	k1gFnSc1	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
schopnost	schopnost	k1gFnSc1	schopnost
jednoho	jeden	k4xCgInSc2	jeden
materiálu	materiál	k1gInSc2	materiál
rýt	rýt	k1gInSc4	rýt
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
německým	německý	k2eAgMnSc7d1	německý
mineralogem	mineralog	k1gMnSc7	mineralog
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Mohsem	Mohs	k1gMnSc7	Mohs
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc4d1	základní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Tvrdost	tvrdost	k1gFnSc1	tvrdost
je	být	k5eAaImIp3nS	být
vektorová	vektorový	k2eAgFnSc1d1	vektorová
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jako	jako	k9	jako
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
vnikání	vnikání	k1gNnSc3	vnikání
cizího	cizí	k2eAgNnSc2d1	cizí
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Závislá	závislý	k2eAgFnSc1d1	závislá
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
struktuře	struktura	k1gFnSc6	struktura
krystalů	krystal	k1gInPc2	krystal
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
na	na	k7c6	na
pevnosti	pevnost	k1gFnSc6	pevnost
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
několik	několik	k4yIc1	několik
základních	základní	k2eAgFnPc2d1	základní
obecných	obecný	k2eAgFnPc2d1	obecná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
tvrdost	tvrdost	k1gFnSc1	tvrdost
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
klesající	klesající	k2eAgFnSc7d1	klesající
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
iontů	ion	k1gInPc2	ion
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
krystalové	krystalový	k2eAgFnSc2d1	krystalová
mřížky	mřížka	k1gFnSc2	mřížka
</s>
</p>
<p>
<s>
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
mocenstvím	mocenství	k1gNnSc7	mocenství
iontů	ion	k1gInPc2	ion
respektive	respektive	k9	respektive
s	s	k7c7	s
násobností	násobnost	k1gFnSc7	násobnost
vazby	vazba	k1gFnSc2	vazba
</s>
</p>
<p>
<s>
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
iontové	iontový	k2eAgFnSc2d1	iontová
vazby	vazba	k1gFnSc2	vazba
ve	v	k7c6	v
smíšených	smíšený	k2eAgFnPc6d1	smíšená
vazbách	vazba	k1gFnPc6	vazba
</s>
</p>
<p>
<s>
krystalové	krystalový	k2eAgInPc1d1	krystalový
agregáty	agregát	k1gInPc1	agregát
(	(	kIx(	(
<g/>
drúzy	drúza	k1gFnPc1	drúza
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
měkčí	měkký	k2eAgInSc1d2	měkčí
než	než	k8xS	než
samostatný	samostatný	k2eAgInSc1d1	samostatný
krystalPro	krystalPro	k6eAd1	krystalPro
běžné	běžný	k2eAgNnSc4d1	běžné
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
se	se	k3xPyFc4	se
orientovat	orientovat	k5eAaBmF	orientovat
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
tvrdosti	tvrdost	k1gFnSc6	tvrdost
předmětů	předmět	k1gInPc2	předmět
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
dosahu	dosah	k1gInSc6	dosah
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nehet	nehet	k1gInSc1	nehet
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc4	tvrdost
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnSc1	mince
3,4	[number]	k4	3,4
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kapesní	kapesní	k2eAgInSc1d1	kapesní
nůž	nůž	k1gInSc1	nůž
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
a	a	k8xC	a
pokud	pokud	k8xS	pokud
minerál	minerál	k1gInSc1	minerál
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
rýhu	rýha	k1gFnSc4	rýha
ve	v	k7c6	v
skle	sklo	k1gNnSc6	sklo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc1	tvrdost
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
uměle	uměle	k6eAd1	uměle
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
tvrdší	tvrdý	k2eAgInSc4d2	tvrdší
než	než	k8xS	než
diamant	diamant	k1gInSc4	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
uhlík	uhlík	k1gInSc4	uhlík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zkouška	zkouška	k1gFnSc1	zkouška
tvrdosti	tvrdost	k1gFnSc2	tvrdost
podle	podle	k7c2	podle
Vickerse	Vickerse	k1gFnSc2	Vickerse
</s>
</p>
