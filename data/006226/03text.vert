<s>
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1957	[number]	k4	1957
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
významného	významný	k2eAgMnSc2d1	významný
českého	český	k2eAgMnSc2d1	český
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Petra	Petr	k1gMnSc2	Petr
Ebena	Eben	k1gMnSc2	Eben
a	a	k8xC	a
Šárky	Šárka	k1gFnSc2	Šárka
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Hurníkové	Hurníková	k1gFnSc2	Hurníková
<g/>
,	,	kIx,	,
a	a	k8xC	a
synovcem	synovec	k1gMnSc7	synovec
skladatele	skladatel	k1gMnSc2	skladatel
Ilji	Ilja	k1gMnSc2	Ilja
Hurníka	Hurník	k1gMnSc2	Hurník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
Markéta	Markéta	k1gFnSc1	Markéta
Fišerová	Fišerová	k1gFnSc1	Fišerová
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
bratry	bratr	k1gMnPc7	bratr
(	(	kIx(	(
<g/>
Davidem	David	k1gMnSc7	David
a	a	k8xC	a
Kryštofem	Kryštof	k1gMnSc7	Kryštof
<g/>
)	)	kIx)	)
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
českých	český	k2eAgMnPc2d1	český
moderátorů	moderátor	k1gMnPc2	moderátor
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
divácké	divácký	k2eAgFnSc6d1	divácká
anketě	anketa	k1gFnSc6	anketa
Týtý	Týtý	k1gFnSc2	Týtý
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
moderátor	moderátor	k1gMnSc1	moderátor
publicistických	publicistický	k2eAgInPc2d1	publicistický
pořadů	pořad	k1gInPc2	pořad
či	či	k8xC	či
osobnost	osobnost	k1gFnSc1	osobnost
zábavných	zábavný	k2eAgInPc2d1	zábavný
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
ceněn	ceněn	k2eAgInSc1d1	ceněn
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gInSc1	jeho
jemný	jemný	k2eAgInSc1d1	jemný
a	a	k8xC	a
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
humor	humor	k1gInSc1	humor
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
moderoval	moderovat	k5eAaBmAgMnS	moderovat
např.	např.	kA	např.
cenu	cena	k1gFnSc4	cena
TýTý	TýTý	k1gFnSc1	TýTý
<g/>
,	,	kIx,	,
soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
pořad	pořad	k1gInSc1	pořad
O	o	k7c4	o
poklad	poklad	k1gInSc4	poklad
Anežky	Anežka	k1gFnSc2	Anežka
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
Staance	Staance	k1gFnSc2	Staance
...	...	k?	...
<g/>
když	když	k8xS	když
hvězdy	hvězda	k1gFnPc1	hvězda
tančí	tančit	k5eAaImIp3nP	tančit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
osobnostmi	osobnost	k1gFnPc7	osobnost
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zpracovány	zpracovat	k5eAaPmNgFnP	zpracovat
i	i	k9	i
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Moderuje	moderovat	k5eAaBmIp3nS	moderovat
i	i	k9	i
televizní	televizní	k2eAgNnSc1d1	televizní
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
z	z	k7c2	z
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
filmového	filmový	k2eAgInSc2d1	filmový
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
hudebně-dramatický	hudebněramatický	k2eAgInSc1d1	hudebně-dramatický
obor	obor	k1gInSc1	obor
Pražské	pražský	k2eAgFnSc2d1	Pražská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
a	a	k8xC	a
poté	poté	k6eAd1	poté
získal	získat	k5eAaPmAgMnS	získat
angažmá	angažmá	k1gNnSc3	angažmá
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nezvala	Nezval	k1gMnSc2	Nezval
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
a	a	k8xC	a
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Průchy	Průcha	k1gMnSc2	Průcha
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Studiu	studio	k1gNnSc6	studio
Ypsilon	ypsilon	k1gNnSc2	ypsilon
působil	působit	k5eAaImAgInS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
zde	zde	k6eAd1	zde
např.	např.	kA	např.
v	v	k7c6	v
inscenacích	inscenace	k1gFnPc6	inscenace
Divadlo	divadlo	k1gNnSc1	divadlo
za	za	k7c2	za
časů	čas	k1gInPc2	čas
Nerona	Nero	k1gMnSc2	Nero
a	a	k8xC	a
Seneky	Seneka	k1gMnSc2	Seneka
<g/>
,	,	kIx,	,
Vosková	voskový	k2eAgFnSc1d1	vosková
figura	figura	k1gFnSc1	figura
<g/>
,	,	kIx,	,
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Sebevrah	sebevrah	k1gMnSc1	sebevrah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jako	jako	k9	jako
host	host	k1gMnSc1	host
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Prodané	prodaný	k2eAgFnSc6d1	prodaná
nevěstě	nevěsta	k1gFnSc6	nevěsta
a	a	k8xC	a
Praze	Praha	k1gFnSc6	Praha
stověžaté	stověžatý	k2eAgNnSc1d1	stověžaté
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
k	k	k7c3	k
inscenacím	inscenace	k1gFnPc3	inscenace
Matěj	Matěj	k1gMnSc1	Matěj
Poctivý	poctivý	k2eAgMnSc1d1	poctivý
<g/>
,	,	kIx,	,
Vosková	voskový	k2eAgFnSc1d1	vosková
figura	figura	k1gFnSc1	figura
<g/>
,	,	kIx,	,
Othello	Othello	k1gMnSc1	Othello
<g/>
,	,	kIx,	,
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
píše	psát	k5eAaImIp3nS	psát
i	i	k9	i
pro	pro	k7c4	pro
jiná	jiný	k2eAgNnPc4d1	jiné
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
Zimní	zimní	k2eAgFnSc4d1	zimní
pohádku	pohádka	k1gFnSc4	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc4	eben
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
texty	text	k1gInPc4	text
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
nebo	nebo	k8xC	nebo
dabing	dabing	k1gInSc4	dabing
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
hudebních	hudební	k2eAgFnPc2d1	hudební
nahrávek	nahrávka	k1gFnPc2	nahrávka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
Poste	post	k1gInSc5	post
restante	restant	k1gInSc5	restant
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
a	a	k8xC	a
rep	rep	k?	rep
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
každého	každý	k3xTgInSc2	každý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
folkové	folkový	k2eAgFnSc2d1	folková
skupiny	skupina	k1gFnSc2	skupina
Bratři	bratr	k1gMnPc1	bratr
Ebenové	ebenový	k2eAgFnSc2d1	ebenová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svá	svůj	k3xOyFgNnPc4	svůj
alba	album	k1gNnPc4	album
vydává	vydávat	k5eAaPmIp3nS	vydávat
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
Sony	Sony	kA	Sony
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
.	.	kIx.	.
</s>
<s>
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
Platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
několik	několik	k4yIc4	několik
textů	text	k1gInPc2	text
písní	píseň	k1gFnPc2	píseň
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Mišíka	Mišík	k1gMnSc2	Mišík
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Nejezchleby	Nejezchleb	k1gMnPc7	Nejezchleb
<g/>
.	.	kIx.	.
</s>
<s>
Kamarádi	kamarád	k1gMnPc1	kamarád
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
Válečka	válečka	k1gFnSc1	válečka
O	o	k7c6	o
líném	líný	k2eAgMnSc6d1	líný
Honzovi	Honz	k1gMnSc6	Honz
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Sázka	sázka	k1gFnSc1	sázka
na	na	k7c4	na
třináctku	třináctka	k1gFnSc4	třináctka
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
O	o	k7c6	o
statečné	statečný	k2eAgFnSc6d1	statečná
princezně	princezna	k1gFnSc6	princezna
Janě	Jana	k1gFnSc6	Jana
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
O	o	k7c6	o
chudém	chudý	k2eAgNnSc6d1	chudé
královstvíčku	královstvíčko	k1gNnSc6	královstvíčko
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
–	–	k?	–
role	role	k1gFnSc1	role
Vendelína	Vendelína	k1gFnSc1	Vendelína
Nevěsta	nevěsta	k1gFnSc1	nevěsta
k	k	k7c3	k
zulíbání	zulíbání	k1gNnSc3	zulíbání
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Chudák	chudák	k1gMnSc1	chudák
muzika	muzika	k1gFnSc1	muzika
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Tvář	tvář	k1gFnSc1	tvář
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Pusu	pusa	k1gFnSc4	pusa
<g/>
,	,	kIx,	,
pusu	pusa	k1gFnSc4	pusa
<g/>
,	,	kIx,	,
pusu	pusa	k1gFnSc4	pusa
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
-	-	kIx~	-
hl.	hl.	k?	hl.
role	role	k1gFnPc1	role
svobodníka	svobodník	k1gMnSc2	svobodník
Martina	Martina	k1gFnSc1	Martina
Pohádka	pohádka	k1gFnSc1	pohádka
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Kam	kam	k6eAd1	kam
doskáče	doskákat	k5eAaPmIp3nS	doskákat
ranní	ranní	k2eAgNnSc1d1	ranní
ptáče	ptáče	k1gNnSc1	ptáče
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Balónová	balónový	k2eAgFnSc1d1	balónová
pohádka	pohádka	k1gFnSc1	pohádka
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Překvapení	překvapení	k1gNnSc1	překvapení
pana	pan	k1gMnSc2	pan
Milberryho	Milberry	k1gMnSc2	Milberry
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
O	o	k7c6	o
princi	princa	k1gFnSc6	princa
<g />
.	.	kIx.	.
</s>
<s>
Bečkovi	Bečka	k1gMnSc3	Bečka
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Kocourkov	Kocourkov	k1gInSc1	Kocourkov
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
O	o	k7c6	o
třech	tři	k4xCgInPc6	tři
stříbrných	stříbrný	k2eAgInPc6d1	stříbrný
hřebenech	hřeben	k1gInPc6	hřeben
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Zálety	zálet	k1gInPc1	zálet
koňského	koňský	k2eAgMnSc2d1	koňský
handlíře	handlíř	k1gMnSc2	handlíř
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Náhrdelník	náhrdelník	k1gInSc1	náhrdelník
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Ruská	ruský	k2eAgFnSc1d1	ruská
ruleta	ruleta	k1gFnSc1	ruleta
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Hospoda	Hospoda	k?	Hospoda
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
–	–	k?	–
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
filmu	film	k1gInSc2	film
–	–	k?	–
v	v	k7c6	v
teple	teplo	k1gNnSc6	teplo
domova	domov	k1gInSc2	domov
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Láska	láska	k1gFnSc1	láska
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Osudy	osud	k1gInPc1	osud
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
:	:	kIx,	:
Váleček	váleček	k1gInSc1	váleček
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
(	(	kIx(	(
<g/>
od	od	k7c2	od
1998	[number]	k4	1998
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
:	:	kIx,	:
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
lišky	liška	k1gFnSc2	liška
Bystroušky	Bystrouška	k1gFnSc2	Bystrouška
<g/>
.	.	kIx.	.
</s>
<s>
Dramatizace	dramatizace	k1gFnSc1	dramatizace
Anna	Anna	k1gFnSc1	Anna
Jurásková	Jurásková	k1gFnSc1	Jurásková
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Redl	Redl	k1gMnSc1	Redl
<g/>
.	.	kIx.	.
</s>
<s>
Dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
Václava	Václav	k1gMnSc2	Václav
Ledvinková	Ledvinková	k1gFnSc1	Ledvinková
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kodeš	Kodeš	k1gMnSc1	Kodeš
<g/>
.	.	kIx.	.
</s>
<s>
Účinkují	účinkovat	k5eAaImIp3nP	účinkovat
<g/>
:	:	kIx,	:
Lucie	Lucie	k1gFnSc1	Lucie
Pernetová	Pernetový	k2eAgFnSc1d1	Pernetová
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Vydra	Vydra	k1gMnSc1	Vydra
<g/>
,	,	kIx,	,
Taťjana	Taťjana	k1gFnSc1	Taťjana
Medvecká	Medvecký	k2eAgFnSc1d1	Medvecká
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Köhler	Köhler	k1gMnSc1	Köhler
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Zindulka	Zindulka	k1gFnSc1	Zindulka
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Maršál	maršál	k1gMnSc1	maršál
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Lábus	Lábus	k1gMnSc1	Lábus
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vlach	Vlach	k1gMnSc1	Vlach
<g/>
,	,	kIx,	,
Jitka	Jitka	k1gFnSc1	Jitka
Smutná	Smutná	k1gFnSc1	Smutná
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Drbohlavová	Drbohlavový	k2eAgFnSc1d1	Drbohlavová
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Racek	racek	k1gMnSc1	racek
<g/>
,	,	kIx,	,
Denisa	Denisa	k1gFnSc1	Denisa
Nová	Nová	k1gFnSc1	Nová
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Suchánková	Suchánková	k1gFnSc1	Suchánková
<g/>
,	,	kIx,	,
Nikola	Nikola	k1gFnSc1	Nikola
Bartošová	Bartošová	k1gFnSc1	Bartošová
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Reichová	Reichová	k1gFnSc1	Reichová
<g/>
,	,	kIx,	,
Valerie	Valerie	k1gFnSc1	Valerie
Rosa	Rosa	k1gFnSc1	Rosa
Hetzendorfová	Hetzendorfový	k2eAgFnSc1d1	Hetzendorfový
<g/>
,	,	kIx,	,
Justýna	Justýna	k1gFnSc1	Justýna
Anna	Anna	k1gFnSc1	Anna
Šmuclerová	Šmuclerová	k1gFnSc1	Šmuclerová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Fišer	Fišer	k1gMnSc1	Fišer
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Tuček	Tuček	k1gMnSc1	Tuček
a	a	k8xC	a
Dorota	Dorota	k1gFnSc1	Dorota
Tučková	Tučková	k1gFnSc1	Tučková
<g/>
.	.	kIx.	.
????	????	k?	????
Douglas	Douglas	k1gInSc1	Douglas
Adams	Adams	k1gInSc1	Adams
<g/>
:	:	kIx,	:
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c4	po
galaxii	galaxie	k1gFnSc4	galaxie
Účinkují	účinkovat	k5eAaImIp3nP	účinkovat
<g/>
:	:	kIx,	:
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc4	eben
v	v	k7c6	v
roli	role	k1gFnSc6	role
palubního	palubní	k2eAgInSc2d1	palubní
počítače	počítač	k1gInSc2	počítač
Eddieho	Eddie	k1gMnSc2	Eddie
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Lábus	Lábus	k1gMnSc1	Lábus
v	v	k7c6	v
roli	role	k1gFnSc6	role
Forda	ford	k1gMnSc2	ford
Prefecta	Prefect	k1gMnSc2	Prefect
viz	vidět	k5eAaImRp2nS	vidět
Diskografie	diskografie	k1gFnPc4	diskografie
bratrů	bratr	k1gMnPc2	bratr
Ebenů	eben	k1gInPc2	eben
Rozmluvy	rozmluva	k1gFnSc2	rozmluva
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Halíkem	Halík	k1gMnSc7	Halík
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
a	a	k8xC	a
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Komentáře	komentář	k1gInPc1	komentář
k	k	k7c3	k
filmům	film	k1gInPc3	film
O.	O.	kA	O.
Schmidta	Schmidt	k1gMnSc2	Schmidt
(	(	kIx(	(
<g/>
Nejsme	být	k5eNaImIp1nP	být
andělé	anděl	k1gMnPc1	anděl
<g/>
,	,	kIx,	,
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
duši	duše	k1gFnSc3	duše
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Recenze	recenze	k1gFnSc1	recenze
knihy	kniha	k1gFnSc2	kniha
pro	pro	k7c4	pro
portál	portál	k1gInSc4	portál
Literarium	Literarium	k1gNnSc4	Literarium
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
Jako	jako	k8xS	jako
cool	cool	k1gInSc1	cool
v	v	k7c6	v
plotě	plot	k1gInSc6	plot
</s>
