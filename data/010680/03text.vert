<p>
<s>
Levandule	levandule	k1gFnSc1	levandule
(	(	kIx(	(
<g/>
Lavandula	Lavandula	k1gFnSc1	Lavandula
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
rostlin	rostlina	k1gFnPc2	rostlina
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
hluchavkovité	hluchavkovitý	k2eAgFnSc2d1	hluchavkovitá
(	(	kIx(	(
<g/>
Lamiaceae	Lamiacea	k1gFnSc2	Lamiacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
polokeře	polokeř	k1gInPc4	polokeř
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
byliny	bylina	k1gFnPc1	bylina
a	a	k8xC	a
kvetou	kvést	k5eAaImIp3nP	kvést
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
nebo	nebo	k8xC	nebo
růžovým	růžový	k2eAgInSc7d1	růžový
květem	květ	k1gInSc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nezaměnitelnou	zaměnitelný	k2eNgFnSc4d1	nezaměnitelná
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
pěstovány	pěstovat	k5eAaImNgInP	pěstovat
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
druh	druh	k1gInSc1	druh
levandule	levandule	k1gFnSc2	levandule
úzkolistá	úzkolistý	k2eAgFnSc1d1	úzkolistá
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
levandule	levandule	k1gFnSc1	levandule
lékařská	lékařský	k2eAgFnSc1d1	lékařská
(	(	kIx(	(
<g/>
Lavandula	Lavandula	k1gFnSc1	Lavandula
angustifolia	angustifolia	k1gFnSc1	angustifolia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
polokeře	polokeř	k1gInPc4	polokeř
nebo	nebo	k8xC	nebo
keříky	keřík	k1gInPc4	keřík
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
byliny	bylina	k1gFnPc1	bylina
s	s	k7c7	s
dřevnatějícími	dřevnatějící	k2eAgFnPc7d1	dřevnatějící
bázemi	báze	k1gFnPc7	báze
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
celokrajné	celokrajný	k2eAgFnPc1d1	celokrajná
<g/>
,	,	kIx,	,
nekdy	nekdy	k?	nekdy
též	též	k9	též
zubaté	zubatý	k2eAgNnSc4d1	zubaté
nebo	nebo	k8xC	nebo
zpeřené	zpeřený	k2eAgNnSc4d1	zpeřené
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
;	;	kIx,	;
trubkovitý	trubkovitý	k2eAgInSc1d1	trubkovitý
kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
žláznatými	žláznatý	k2eAgInPc7d1	žláznatý
chlupy	chlup	k1gInPc7	chlup
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
aromatické	aromatický	k2eAgFnPc1d1	aromatická
silice	silice	k1gFnSc1	silice
<g/>
,	,	kIx,	,
koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
či	či	k8xC	či
jenom	jenom	k6eAd1	jenom
nevýrazně	výrazně	k6eNd1	výrazně
dvoupyskatá	dvoupyskatý	k2eAgFnSc1d1	dvoupyskatý
<g/>
.	.	kIx.	.
</s>
<s>
Umístěny	umístěn	k2eAgFnPc1d1	umístěna
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
lichopřeslenech	lichopřeslen	k1gInPc6	lichopřeslen
klasovitě	klasovitě	k6eAd1	klasovitě
nahloučených	nahloučený	k2eAgInPc2d1	nahloučený
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
květonosných	květonosný	k2eAgFnPc2d1	květonosná
lodyh	lodyha	k1gFnPc2	lodyha
<g/>
.	.	kIx.	.
</s>
<s>
Opylovány	opylován	k2eAgFnPc1d1	opylována
jsou	být	k5eAaImIp3nP	být
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tvrdka	tvrdka	k1gFnSc1	tvrdka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
od	od	k7c2	od
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
přes	přes	k7c4	přes
Středomoří	středomoří	k1gNnSc4	středomoří
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
včetně	včetně	k7c2	včetně
saharských	saharský	k2eAgMnPc2d1	saharský
států	stát	k1gInPc2	stát
až	až	k9	až
po	po	k7c4	po
Arabský	arabský	k2eAgInSc4d1	arabský
poloostrov	poloostrov	k1gInSc4	poloostrov
a	a	k8xC	a
Indický	indický	k2eAgInSc4d1	indický
subkontinent	subkontinent	k1gInSc4	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
biotopem	biotop	k1gInSc7	biotop
jsou	být	k5eAaImIp3nP	být
suché	suchý	k2eAgFnPc1d1	suchá
nízké	nízký	k2eAgFnPc1d1	nízká
křoviny	křovina	k1gFnPc1	křovina
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
garrigue	garrigu	k1gFnPc1	garrigu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
se	se	k3xPyFc4	se
při	při	k7c6	při
výsadbě	výsadba	k1gFnSc6	výsadba
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
krásný	krásný	k2eAgInSc4d1	krásný
kvetoucí	kvetoucí	k2eAgInSc4d1	kvetoucí
koberec	koberec	k1gInSc4	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Silice	silice	k1gFnSc1	silice
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
levandulový	levandulový	k2eAgInSc4d1	levandulový
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
kosmetice	kosmetika	k1gFnSc6	kosmetika
a	a	k8xC	a
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
protizánětlivé	protizánětlivý	k2eAgInPc4d1	protizánětlivý
<g/>
,	,	kIx,	,
antiseptické	antiseptický	k2eAgInPc4d1	antiseptický
a	a	k8xC	a
repelentní	repelentní	k2eAgInPc4d1	repelentní
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Široké	Široké	k2eAgNnSc1d1	Široké
využití	využití	k1gNnSc1	využití
má	mít	k5eAaImIp3nS	mít
druh	druh	k1gInSc4	druh
v	v	k7c6	v
gastronomii	gastronomie	k1gFnSc6	gastronomie
jako	jako	k8xS	jako
koření	koření	k1gNnSc6	koření
<g/>
,	,	kIx,	,
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
sirupů	sirup	k1gInPc2	sirup
či	či	k8xC	či
jako	jako	k9	jako
pot-pourri	potourri	k6eAd1	pot-pourri
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
o	o	k7c4	o
medonosné	medonosný	k2eAgFnPc4d1	medonosná
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Levandule	levandule	k1gFnSc1	levandule
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
sucho	sucho	k6eAd1	sucho
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
lehkých	lehký	k2eAgFnPc6d1	lehká
hlinitopísčitých	hlinitopísčitý	k2eAgFnPc6d1	hlinitopísčitá
<g/>
,	,	kIx,	,
písčitých	písčitý	k2eAgFnPc6d1	písčitá
nebo	nebo	k8xC	nebo
štěrkovitých	štěrkovitý	k2eAgFnPc6d1	štěrkovitá
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
odvodněných	odvodněný	k2eAgFnPc6d1	odvodněná
půdách	půda	k1gFnPc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
plně	plně	k6eAd1	plně
osluněné	osluněný	k2eAgNnSc4d1	osluněné
stanoviště	stanoviště	k1gNnSc4	stanoviště
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
cirkulací	cirkulace	k1gFnSc7	cirkulace
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Prospívají	prospívat	k5eAaImIp3nP	prospívat
i	i	k9	i
bez	bez	k7c2	bez
přihnojování	přihnojování	k1gNnSc2	přihnojování
<g/>
,	,	kIx,	,
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
vlhkosti	vlhkost	k1gFnSc6	vlhkost
či	či	k8xC	či
přemokření	přemokření	k1gNnSc6	přemokření
mohou	moct	k5eAaImIp3nP	moct
trpět	trpět	k5eAaImF	trpět
uhníváním	uhnívání	k1gNnSc7	uhnívání
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybraní	vybraný	k2eAgMnPc1d1	vybraný
zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
dělen	dělit	k5eAaImNgInS	dělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
podrodůː	podrodůː	k?	podrodůː
</s>
</p>
<p>
<s>
Podrod	podrod	k1gInSc1	podrod
Lavandula	Lavandulum	k1gNnSc2	Lavandulum
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
angustifolia	angustifolia	k1gFnSc1	angustifolia
(	(	kIx(	(
<g/>
levandule	levandule	k1gFnSc1	levandule
úzkolistá	úzkolistý	k2eAgFnSc1d1	úzkolistá
<g/>
)	)	kIx)	)
–	–	k?	–
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
Provence	Provence	k1gFnSc1	Provence
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
lanata	lanata	k1gFnSc1	lanata
–	–	k?	–
jižní	jižní	k2eAgNnSc4d1	jižní
Španělsko	Španělsko	k1gNnSc4	Španělsko
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
latifolia	latifolius	k1gMnSc2	latifolius
–	–	k?	–
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
Provence	Provence	k1gFnSc1	Provence
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
dentata	dentata	k1gFnSc1	dentata
(	(	kIx(	(
<g/>
levandule	levandule	k1gFnSc1	levandule
zubatá	zubatá	k1gFnSc1	zubatá
<g/>
)	)	kIx)	)
–	–	k?	–
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
pedunculata	peduncule	k1gNnPc1	peduncule
–	–	k?	–
Maroko	Maroko	k1gNnSc4	Maroko
<g/>
,	,	kIx,	,
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
stoechas	stoechasa	k1gFnPc2	stoechasa
(	(	kIx(	(
<g/>
levandule	levandule	k1gFnSc1	levandule
korunkatá	korunkatý	k2eAgFnSc1d1	korunkatá
<g/>
)	)	kIx)	)
–	–	k?	–
většina	většina	k1gFnSc1	většina
Mediteránu	mediterán	k1gInSc2	mediterán
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
viridis	viridis	k1gFnSc1	viridis
–	–	k?	–
jih	jih	k1gInSc4	jih
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
Madeira	Madeira	k1gFnSc1	Madeira
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
x	x	k?	x
allardii	allardie	k1gFnSc4	allardie
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
x	x	k?	x
ginginsii	ginginsie	k1gFnSc4	ginginsie
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
x	x	k?	x
heterophylla	heterophylnout	k5eAaImAgFnS	heterophylnout
</s>
</p>
<p>
<s>
Podrod	podrod	k1gInSc1	podrod
Fabricia	Fabricium	k1gNnSc2	Fabricium
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
canariensis	canariensis	k1gFnSc2	canariensis
–	–	k?	–
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
multifida	multifida	k1gFnSc1	multifida
–	–	k?	–
západní	západní	k2eAgFnSc1d1	západní
StředomoříLavandula	StředomoříLavandula	k1gFnSc1	StředomoříLavandula
minutolii	minutolie	k1gFnSc4	minutolie
–	–	k?	–
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
</s>
</p>
<p>
<s>
Lavandula	Lavandout	k5eAaPmAgFnS	Lavandout
buchii	buchie	k1gFnSc4	buchie
–	–	k?	–
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
(	(	kIx(	(
<g/>
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
pinnata	pinnata	k1gFnSc1	pinnata
–	–	k?	–
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Madeira	Madeira	k1gFnSc1	Madeira
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
rotundifolia	rotundifolius	k1gMnSc2	rotundifolius
–	–	k?	–
Kapverdy	Kapverda	k1gMnSc2	Kapverda
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
antineae	antinea	k1gInSc2	antinea
–	–	k?	–
centrální	centrální	k2eAgFnSc1d1	centrální
Sahara	Sahara	k1gFnSc1	Sahara
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
aristibracteata	aristibracteata	k1gFnSc1	aristibracteata
–	–	k?	–
Somálsko	Somálsko	k1gNnSc1	Somálsko
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
bipinnata	bipinnat	k2eAgFnSc1d1	bipinnat
–	–	k?	–
Indie	Indie	k1gFnSc1	Indie
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
citriodora	citriodor	k1gMnSc2	citriodor
–	–	k?	–
Arabský	arabský	k2eAgInSc4d1	arabský
poloostrov	poloostrov	k1gInSc4	poloostrov
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
coronopifolia	coronopifolius	k1gMnSc2	coronopifolius
–	–	k?	–
široký	široký	k2eAgInSc4d1	široký
areál	areál	k1gInSc4	areál
od	od	k7c2	od
Kapverd	Kapverda	k1gFnPc2	Kapverda
přes	přes	k7c4	přes
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
Arábii	Arábie	k1gFnSc4	Arábie
po	po	k7c4	po
Írán	Írán	k1gInSc4	Írán
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
dhofarensis	dhofarensis	k1gFnSc1	dhofarensis
–	–	k?	–
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Dhofar	Dhofar	k1gInSc1	Dhofar
<g/>
,	,	kIx,	,
Omán	Omán	k1gInSc1	Omán
</s>
</p>
<p>
<s>
Lavandula	Lavandout	k5eAaPmAgFnS	Lavandout
gibsonii	gibsonie	k1gFnSc4	gibsonie
–	–	k?	–
západní	západní	k2eAgInSc4d1	západní
Ghát	Ghát	k1gInSc4	Ghát
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
macra	macra	k1gFnSc1	macra
–	–	k?	–
jih	jih	k1gInSc4	jih
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
Somálsko	Somálsko	k1gNnSc4	Somálsko
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
maroccana	maroccana	k1gFnSc1	maroccana
–	–	k?	–
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Atlas	Atlas	k1gInSc1	Atlas
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
nimmoi	nimmo	k1gFnSc2	nimmo
–	–	k?	–
Sokotra	Sokotra	k1gFnSc1	Sokotra
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
pubescens	pubescens	k1gInSc1	pubescens
–	–	k?	–
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Blízký	blízký	k2eAgInSc1d1	blízký
východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
Arabský	arabský	k2eAgInSc1d1	arabský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
Jemen	Jemen	k1gInSc1	Jemen
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
saharica	saharic	k1gInSc2	saharic
–	–	k?	–
jižní	jižní	k2eAgNnSc1d1	jižní
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
samhanensis	samhanensis	k1gFnSc1	samhanensis
–	–	k?	–
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Dhofar	Dhofar	k1gInSc1	Dhofar
<g/>
,	,	kIx,	,
Omán	Omán	k1gInSc1	Omán
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
somaliensis	somaliensis	k1gFnPc2	somaliensis
–	–	k?	–
Somálsko	Somálsko	k1gNnSc4	Somálsko
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
sublepidota	sublepidota	k1gFnSc1	sublepidota
–	–	k?	–
Írán	Írán	k1gInSc1	Írán
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
subnuda	subnuda	k1gFnSc1	subnuda
–	–	k?	–
Omán	Omán	k1gInSc1	Omán
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
tenuisecta	tenuisecta	k1gFnSc1	tenuisecta
–	–	k?	–
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Atlas	Atlas	k1gInSc1	Atlas
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
x	x	k?	x
chaytorae	chaytoraat	k5eAaPmIp3nS	chaytoraat
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
x	x	k?	x
christiana	christiana	k1gFnSc1	christiana
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
x	x	k?	x
intermediaPodrod	intermediaPodrod	k1gInSc1	intermediaPodrod
Sabaudia	Sabaudium	k1gNnSc2	Sabaudium
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
atriplicifolia	atriplicifolia	k1gFnSc1	atriplicifolia
–	–	k?	–
západ	západ	k1gInSc4	západ
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
</s>
</p>
<p>
<s>
Lavandula	Lavandula	k1gFnSc1	Lavandula
erythraeae	erythraeaat	k5eAaPmIp3nS	erythraeaat
–	–	k?	–
Eritrea	Eritrea	k1gFnSc1	Eritrea
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lavandula	Lavandulum	k1gNnSc2	Lavandulum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
levandule	levandule	k1gFnSc2	levandule
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
levandule	levandule	k1gFnSc2	levandule
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Lavandula	Lavandulum	k1gNnSc2	Lavandulum
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
levandule	levandule	k1gFnSc2	levandule
</s>
</p>
