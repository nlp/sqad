<s>
Levandule	levandule	k1gFnSc1	levandule
(	(	kIx(	(
<g/>
Lavandula	Lavandula	k1gFnSc1	Lavandula
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
rostlin	rostlina	k1gFnPc2	rostlina
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
hluchavkovité	hluchavkovitý	k2eAgFnSc2d1	hluchavkovitá
(	(	kIx(	(
<g/>
Lamiaceae	Lamiacea	k1gFnSc2	Lamiacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
polokeře	polokeř	k1gInPc4	polokeř
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
byliny	bylina	k1gFnPc1	bylina
a	a	k8xC	a
kvetou	kvést	k5eAaImIp3nP	kvést
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
nebo	nebo	k8xC	nebo
růžovým	růžový	k2eAgInSc7d1	růžový
květem	květ	k1gInSc7	květ
<g/>
.	.	kIx.	.
</s>
