<s>
Danubius	Danubius	k1gMnSc1
Cable	Cable	k1gNnSc2
TV	TV	kA
</s>
<s>
Danubius	Danubius	k1gMnSc1
Cable	Cable	k1gNnSc2
TV	TV	kA
Zahájení	zahájení	k1gNnSc1
vysílání	vysílání	k1gNnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
<g/>
(	(	kIx(
<g/>
testovací	testovací	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1995	#num#	k4
<g/>
(	(	kIx(
<g/>
pravidelné	pravidelný	k2eAgNnSc1d1
<g/>
)	)	kIx)
Ukončení	ukončení	k1gNnSc1
vysílání	vysílání	k1gNnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1995	#num#	k4
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
WN	WN	kA
DANUBIUS	DANUBIUS	kA
FILM	film	k1gInSc1
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
Generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Nittnaus	Nittnaus	k1gMnSc1
Formát	formát	k1gInSc4
obrazu	obraz	k1gInSc2
</s>
<s>
4	#num#	k4
:	:	kIx,
3	#num#	k4
Země	zem	k1gFnSc2
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
-	-	kIx~
Nové	Nové	k2eAgNnSc1d1
Mesto	Mesto	k1gNnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
Júnová	Júnová	k1gFnSc1
10	#num#	k4
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
slovenštinačeština	slovenštinačeština	k1gFnSc1
Oblast	oblast	k1gFnSc1
vysílání	vysílání	k1gNnSc4
</s>
<s>
ČeskoSlovensko	Československo	k1gNnSc1
Sesterské	sesterský	k2eAgInPc1d1
kanály	kanál	k1gInPc1
</s>
<s>
TV	TV	kA
Luna	luna	k1gFnSc1
Dostupnost	dostupnost	k1gFnSc1
(	(	kIx(
<g/>
V	v	k7c6
době	doba	k1gFnSc6
ukončení	ukončení	k1gNnSc2
vysílání	vysílání	k1gNnSc2
<g/>
)	)	kIx)
Terestriální	Terestriální	k2eAgInSc1d1
Analog	analog	k1gInSc1
</s>
<s>
50	#num#	k4
<g/>
.	.	kIx.
kanálBratislava	kanálBratislava	k1gFnSc1
Kamzík	kamzík	k1gMnSc1
Satelitní	satelitní	k2eAgMnSc1d1
Eutelsat	Eutelsat	k1gMnSc7
I	i	k8xC
F5	F5	k1gMnSc7
</s>
<s>
11	#num#	k4
097	#num#	k4
MHz	Mhz	kA
<g/>
/	/	kIx~
<g/>
V	V	kA
Kabelová	kabelový	k2eAgFnSc1d1
SKT	SKT	kA
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
42	#num#	k4
<g/>
.	.	kIx.
kanál	kanál	k1gInSc1
CATV	CATV	kA
Prostějov	Prostějov	k1gInSc1
<g/>
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
??	??	k?
Kabel	kabela	k1gFnPc2
Plus	plus	k1gInSc1
Střední	střední	k1gMnSc1
Morava	Morava	k1gFnSc1
a.s.	a.s.	k?
<g/>
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
??	??	k?
Kabelová	kabelový	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Česká	český	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
??	??	k?
Kabelová	kabelový	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Třinec	Třinec	k1gInSc1
<g/>
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
??	??	k?
NOEL	NOEL	kA
<g/>
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
??	??	k?
Novák	Novák	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
??	??	k?
</s>
<s>
Danubius	Danubius	k2
Cable	Cable	k2
TV	TV	kA
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
DCTV	DCTV	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
první	první	k4xOgInSc1
slovenský	slovenský	k2eAgInSc1d1
kabelový	kabelový	k2eAgInSc1d1
rodinný	rodinný	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
kanál	kanál	k1gInSc1
vysílající	vysílající	k2eAgInSc1d1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1995	#num#	k4
prostřednictvím	prostřednictvím	k7c2
satelitu	satelit	k1gInSc2
a	a	k8xC
kabelových	kabelový	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozovatelem	provozovatel	k1gMnSc7
vysílání	vysílání	k1gNnSc2
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
WN	WN	kA
DANUBIUS	DANUBIUS	kA
FILM	film	k1gInSc1
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
prezidentem	prezident	k1gMnSc7
byl	být	k5eAaImAgMnS
Walter	Walter	k1gMnSc1
Nittnaus	Nittnaus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Stanice	stanice	k1gFnSc1
byla	být	k5eAaImAgFnS
koncipována	koncipovat	k5eAaBmNgFnS
jako	jako	k8xC,k8xS
rodinný	rodinný	k2eAgInSc4d1
kanál	kanál	k1gInSc4
vysílající	vysílající	k2eAgInPc1d1
filmy	film	k1gInPc1
zahraniční	zahraniční	k2eAgFnSc2d1
i	i	k8xC
české	český	k2eAgFnSc2d1
provenience	provenience	k1gFnSc2
<g/>
,	,	kIx,
seriály	seriál	k1gInPc4
<g/>
,	,	kIx,
pořady	pořad	k1gInPc4
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
převzaté	převzatý	k2eAgInPc4d1
zábavní	zábavní	k2eAgInPc4d1
pořady	pořad	k1gInPc4
českých	český	k2eAgMnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgMnPc2d1
producentů	producent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štáb	štáb	k1gInSc1
DCTV	DCTV	kA
měl	mít	k5eAaImAgInS
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
17	#num#	k4
a	a	k8xC
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
25	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vize	vize	k1gFnSc1
před	před	k7c7
spuštěním	spuštění	k1gNnSc7
vysílání	vysílání	k1gNnSc2
</s>
<s>
Podle	podle	k7c2
vyjádření	vyjádření	k1gNnSc2
programového	programový	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
J.	J.	kA
Morávka	Morávek	k1gMnSc2
chtěla	chtít	k5eAaImAgFnS
stanice	stanice	k1gFnSc1
klást	klást	k5eAaImF
důraz	důraz	k1gInSc4
na	na	k7c4
původní	původní	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
publicistiky	publicistika	k1gFnSc2
a	a	k8xC
zábavy	zábava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
společných	společný	k2eAgInPc6d1
projektech	projekt	k1gInPc6
jednali	jednat	k5eAaImAgMnP
také	také	k9
s	s	k7c7
českými	český	k2eAgMnPc7d1
producenty	producent	k1gMnPc7
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zdrojem	zdroj	k1gInSc7
příjmu	příjem	k1gInSc2
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
reklamy	reklama	k1gFnPc4
a	a	k8xC
komerční	komerční	k2eAgFnSc4d1
úspěšnost	úspěšnost	k1gFnSc4
této	tento	k3xDgFnSc2
nové	nový	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
projevit	projevit	k5eAaPmF
prodloužením	prodloužení	k1gNnSc7
vysílacího	vysílací	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
slov	slovo	k1gNnPc2
prezidenta	prezident	k1gMnSc2
společnosti	společnost	k1gFnSc2
Waltera	Walter	k1gMnSc2
Nittnause	Nittnause	k1gFnSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
tak	tak	k9
mělo	mít	k5eAaImAgNnS
stát	stát	k5eAaPmF,k5eAaImF
do	do	k7c2
podzimu	podzim	k1gInSc2
nadcházejícího	nadcházející	k2eAgInSc2d1
roku	rok	k1gInSc2
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
sdělil	sdělit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
je	být	k5eAaImIp3nS
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
ke	k	k7c3
kabelovému	kabelový	k2eAgInSc3d1
rozvodu	rozvod	k1gInSc3
připojeno	připojen	k2eAgNnSc1d1
již	již	k9
300	#num#	k4
000	#num#	k4
domácností	domácnost	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mylně	mylně	k6eAd1
počítal	počítat	k5eAaImAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
bude	být	k5eAaImBp3nS
ke	k	k7c3
kabelové	kabelový	k2eAgFnSc3d1
televizi	televize	k1gFnSc3
připojeno	připojit	k5eAaPmNgNnS
70	#num#	k4
<g/>
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
jednat	jednat	k5eAaImF
až	až	k9
o	o	k7c4
900	#num#	k4
000	#num#	k4
domácností	domácnost	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obsahově	obsahově	k6eAd1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
jednat	jednat	k5eAaImF
o	o	k7c6
televizi	televize	k1gFnSc6
rodinného	rodinný	k2eAgInSc2d1
typu	typ	k1gInSc2
s	s	k7c7
programem	program	k1gInSc7
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
věkové	věkový	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
plánu	plán	k1gInSc6
bylo	být	k5eAaImAgNnS
vysílat	vysílat	k5eAaImF
denně	denně	k6eAd1
4	#num#	k4
hodiny	hodina	k1gFnSc2
a	a	k8xC
od	od	k7c2
pátku	pátek	k1gInSc2
do	do	k7c2
soboty	sobota	k1gFnSc2
o	o	k7c4
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
déle	dlouho	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strukturu	struktura	k1gFnSc4
pořadů	pořad	k1gInPc2
měly	mít	k5eAaImAgInP
tvořit	tvořit	k5eAaImF
animované	animovaný	k2eAgInPc1d1
filmy	film	k1gInPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
seriály	seriál	k1gInPc4
<g/>
,	,	kIx,
kvízy	kvíz	k1gInPc4
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
den	den	k1gInSc1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
vysílat	vysílat	k5eAaImF
film	film	k1gInSc1
a	a	k8xC
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
dnech	den	k1gInPc6
také	také	k9
publicistický	publicistický	k2eAgInSc4d1
pořad	pořad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozovatel	provozovatel	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
dodržet	dodržet	k5eAaPmF
zásadu	zásada	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
podíl	podíl	k1gInSc1
americké	americký	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
nepřesáhne	přesáhnout	k5eNaPmIp3nS
50	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
filmy	film	k1gInPc1
české	český	k2eAgFnSc2d1
nebo	nebo	k8xC
slovenské	slovenský	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
se	se	k3xPyFc4
ve	v	k7c6
vysílání	vysílání	k1gNnSc6
objeví	objevit	k5eAaPmIp3nS
minimálně	minimálně	k6eAd1
jednou	jeden	k4xCgFnSc7
týdně	týdně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
vzdali	vzdát	k5eAaPmAgMnP
možnosti	možnost	k1gFnSc3
výroby	výroba	k1gFnSc2
vlastního	vlastní	k2eAgNnSc2d1
zpravodajství	zpravodajství	k1gNnSc2
s	s	k7c7
odvoláním	odvolání	k1gNnSc7
na	na	k7c4
názor	názor	k1gInSc4
Waltera	Walter	k1gMnSc2
Nittnause	Nittnause	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
nejdříve	dříve	k6eAd3
musí	muset	k5eAaImIp3nS
vyrůst	vyrůst	k5eAaPmF
nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
zpravodajců	zpravodajce	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
budou	být	k5eAaImBp3nP
umět	umět	k5eAaImF
zpravodajství	zpravodajství	k1gNnSc4
dělat	dělat	k5eAaImF
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
Nittnaus	Nittnaus	k1gMnSc1
také	také	k9
zvažoval	zvažovat	k5eAaImAgMnS
výrobu	výroba	k1gFnSc4
publicistického	publicistický	k2eAgInSc2d1
pořadu	pořad	k1gInSc2
s	s	k7c7
jednotlivými	jednotlivý	k2eAgNnPc7d1
ministerstvy	ministerstvo	k1gNnPc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
by	by	kYmCp3nP
propagovali	propagovat	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
a	a	k8xC
záměry	záměr	k1gInPc4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pravidelné	pravidelný	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
Danubius	Danubius	k1gMnSc1
Cable	Cabl	k1gMnSc2
TV	TV	kA
naplánované	naplánovaný	k2eAgNnSc1d1
na	na	k7c4
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1995	#num#	k4
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
slavnostně	slavnostně	k6eAd1
zahájeno	zahájen	k2eAgNnSc4d1
show	show	k1gNnSc4
Moravská	moravský	k2eAgFnSc1d1
svatba	svatba	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
vysílání	vysílání	k1gNnSc6
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
podílet	podílet	k5eAaImF
také	také	k9
Kabel	kabel	k1gInSc4
Plus	plus	k1gInSc1
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Začátek	začátek	k1gInSc1
vysílání	vysílání	k1gNnSc2
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1
vysílal	vysílat	k5eAaImAgMnS
prostřednictvím	prostřednictvím	k7c2
analogového	analogový	k2eAgInSc2d1
vysílače	vysílač	k1gInSc2
Bratislava-Kamzík	Bratislava-Kamzík	k1gMnSc1
na	na	k7c6
kanálu	kanál	k1gInSc6
50	#num#	k4
a	a	k8xC
pro	pro	k7c4
distribuci	distribuce	k1gFnSc4
svého	svůj	k3xOyFgInSc2
kanálu	kanál	k1gInSc2
do	do	k7c2
kabelových	kabelový	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
zvolil	zvolit	k5eAaPmAgInS
družici	družice	k1gFnSc4
Eutelsat	Eutelsat	k1gFnPc2
I	i	k8xC
F5	F5	k1gFnPc2
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výběr	výběr	k1gInSc1
zvolené	zvolený	k2eAgFnSc2d1
družice	družice	k1gFnSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
ukázal	ukázat	k5eAaPmAgInS
jako	jako	k9
nešťastný	šťastný	k2eNgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
ta	ten	k3xDgFnSc1
již	již	k6eAd1
byla	být	k5eAaImAgFnS
na	na	k7c4
hranici	hranice	k1gFnSc4
své	svůj	k3xOyFgFnSc2
životnosti	životnost	k1gFnSc2
a	a	k8xC
inklinovala	inklinovat	k5eAaImAgFnS
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
družice	družice	k1gFnSc2
krátce	krátce	k6eAd1
vysílal	vysílat	k5eAaImAgInS
i	i	k9
český	český	k2eAgInSc1d1
filmový	filmový	k2eAgInSc1d1
kabelový	kabelový	k2eAgInSc1d1
kanál	kanál	k1gInSc1
Kabel	kabel	k1gInSc1
Plus	plus	k1gInSc1
Film	film	k1gInSc1
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
příjem	příjem	k1gInSc4
byla	být	k5eAaImAgFnS
navíc	navíc	k6eAd1
zapotřebí	zapotřebí	k6eAd1
parabola	parabola	k1gFnSc1
větších	veliký	k2eAgInPc2d2
rozměrů	rozměr	k1gInPc2
(	(	kIx(
<g/>
okolo	okolo	k7c2
120	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
a	a	k8xC
kvalita	kvalita	k1gFnSc1
signálu	signál	k1gInSc2
neodpovídala	odpovídat	k5eNaImAgFnS
představám	představa	k1gFnPc3
provozovatele	provozovatel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Tři	tři	k4xCgInPc1
dny	den	k1gInPc1
po	po	k7c6
spuštění	spuštění	k1gNnSc6
vysílání	vysílání	k1gNnSc2
slovenský	slovenský	k2eAgInSc4d1
kabelový	kabelový	k2eAgInSc4d1
operátor	operátor	k1gInSc4
SKT	SKT	kA
Bratislava	Bratislava	k1gFnSc1
vysílání	vysílání	k1gNnSc4
přerušil	přerušit	k5eAaPmAgMnS
a	a	k8xC
obnovil	obnovit	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
až	až	k9
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
distribuční	distribuční	k2eAgInSc1d1
systém	systém	k1gInSc1
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zásobování	zásobování	k1gNnSc4
signálu	signál	k1gInSc2
u	u	k7c2
tohoto	tento	k3xDgMnSc2
kabelového	kabelový	k2eAgMnSc2d1
operátora	operátor	k1gMnSc2
byla	být	k5eAaImAgFnS
zvolena	zvolit	k5eAaPmNgFnS
radioreléová	radioreléový	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distribuce	distribuce	k1gFnSc1
signálu	signál	k1gInSc2
na	na	k7c4
zbývající	zbývající	k2eAgFnPc4d1
kabelové	kabelový	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
byla	být	k5eAaImAgFnS
ponechána	ponechat	k5eAaPmNgFnS
přes	přes	k7c4
družici	družice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozovatel	provozovatel	k1gMnSc1
DCTV	DCTV	kA
zvažoval	zvažovat	k5eAaImAgMnS
<g/>
,	,	kIx,
zda	zda	k8xS
má	mít	k5eAaImIp3nS
vysílání	vysílání	k1gNnSc4
přes	přes	k7c4
satelit	satelit	k1gInSc4
ukončit	ukončit	k5eAaPmF
<g/>
,	,	kIx,
nebo	nebo	k8xC
zajistit	zajistit	k5eAaPmF
vysílání	vysílání	k1gNnSc4
přes	přes	k7c4
jinou	jiný	k2eAgFnSc4d1
družici	družice	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
si	se	k3xPyFc3
podle	podle	k7c2
slov	slovo	k1gNnPc2
provozovatele	provozovatel	k1gMnSc2
vyžádá	vyžádat	k5eAaPmIp3nS
čas	čas	k1gInSc1
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prvotní	prvotní	k2eAgInSc1d1
signál	signál	k1gInSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
již	již	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
dopoledních	dopolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
byl	být	k5eAaImAgInS
vysílán	vysílat	k5eAaImNgInS
monoskop	monoskop	k1gInSc1
a	a	k8xC
ve	v	k7c6
večerních	večerní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
běžela	běžet	k5eAaImAgFnS
promo	promo	k6eAd1
smyčka	smyčka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vysílání	vysílání	k1gNnSc1
bylo	být	k5eAaImAgNnS
určeno	určit	k5eAaPmNgNnS
pro	pro	k7c4
sladění	sladění	k1gNnSc4
technických	technický	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
v	v	k7c6
kabelových	kabelový	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
pravidelné	pravidelný	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
distribuce	distribuce	k1gFnSc1
přes	přes	k7c4
satelit	satelit	k1gInSc4
byla	být	k5eAaImAgFnS
k	k	k7c3
tomuto	tento	k3xDgInSc3
dni	den	k1gInSc3
přerušena	přerušit	k5eAaPmNgFnS
a	a	k8xC
Danubius	Danubius	k1gInSc1
Cable	Cable	k1gInSc1
TV	TV	kA
byl	být	k5eAaImAgInS
šířen	šířit	k5eAaImNgInS
pouze	pouze	k6eAd1
v	v	k7c6
kabelových	kabelový	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
SKT	SKT	kA
Bratislava	Bratislava	k1gFnSc1
a	a	k8xC
přes	přes	k7c4
vysílač	vysílač	k1gInSc4
Kamzík	kamzík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozovatel	provozovatel	k1gMnSc1
kanálu	kanál	k1gInSc2
informoval	informovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
jednání	jednání	k1gNnPc1
s	s	k7c7
německými	německý	k2eAgInPc7d1
úřady	úřad	k1gInPc7
o	o	k7c6
možném	možný	k2eAgNnSc6d1
vysílání	vysílání	k1gNnSc6
prostřednictvím	prostřednictvím	k7c2
družice	družice	k1gFnSc2
DFS	DFS	kA
Kopernikus	Kopernikus	k1gInSc4
2	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
spuštěno	spustit	k5eAaPmNgNnS
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
nestalo	stát	k5eNaPmAgNnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
skutečnosti	skutečnost	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
provozovateli	provozovatel	k1gMnSc3
nepodařilo	podařit	k5eNaPmAgNnS
vyřešit	vyřešit	k5eAaPmF
technické	technický	k2eAgInPc4d1
problémy	problém	k1gInPc4
s	s	k7c7
kvalitou	kvalita	k1gFnSc7
signálu	signál	k1gInSc2
distribuovaného	distribuovaný	k2eAgInSc2d1
přes	přes	k7c4
satelit	satelit	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
prostřednictvím	prostřednictvím	k7c2
jedné	jeden	k4xCgFnSc2
kabelové	kabelový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
a	a	k8xC
jednoho	jeden	k4xCgInSc2
terestrického	terestrický	k2eAgInSc2d1
vysílače	vysílač	k1gInSc2
nedosahoval	dosahovat	k5eNaImAgMnS
dostatečných	dostatečný	k2eAgInPc2d1
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
pokrývající	pokrývající	k2eAgInPc4d1
provozní	provozní	k2eAgInPc4d1
náklady	náklad	k1gInPc4
stanice	stanice	k1gFnSc2
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
provozovatel	provozovatel	k1gMnSc1
vysílání	vysílání	k1gNnSc4
ke	k	k7c3
dni	den	k1gInSc3
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1995	#num#	k4
ukončit	ukončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílání	vysílání	k1gNnSc1
již	již	k6eAd1
nebylo	být	k5eNaImAgNnS
obnoveno	obnovit	k5eAaPmNgNnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pokus	pokus	k1gInSc1
o	o	k7c4
obnovení	obnovení	k1gNnSc4
vysílání	vysílání	k1gNnSc2
</s>
<s>
Technické	technický	k2eAgInPc1d1
problémy	problém	k1gInPc1
s	s	k7c7
distribuci	distribuce	k1gFnSc4
signálu	signál	k1gInSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
provozovateli	provozovatel	k1gMnSc3
vyřešit	vyřešit	k5eAaPmF
v	v	k7c6
listopadu	listopad	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1996	#num#	k4
opět	opět	k6eAd1
žádal	žádat	k5eAaImAgInS
Radu	rada	k1gFnSc4
SR	SR	kA
pro	pro	k7c4
rozhlasové	rozhlasový	k2eAgNnSc4d1
a	a	k8xC
televizní	televizní	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
(	(	kIx(
<g/>
Rada	rada	k1gFnSc1
SR	SR	kA
pre	pre	k?
rozhlasové	rozhlasový	k2eAgFnSc2d1
a	a	k8xC
televízne	televíznout	k5eAaPmIp3nS
vysielanie	vysielanie	k1gFnSc1
<g/>
)	)	kIx)
o	o	k7c4
odložení	odložení	k1gNnSc4
obnovy	obnova	k1gFnSc2
vysílání	vysílání	k1gNnSc2
z	z	k7c2
ledna	leden	k1gInSc2
na	na	k7c4
květen	květen	k1gInSc4
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
změnu	změna	k1gFnSc4
koncepce	koncepce	k1gFnSc2
vysílání	vysílání	k1gNnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
vysílací	vysílací	k2eAgInSc1d1
čas	čas	k1gInSc1
rozšířit	rozšířit	k5eAaPmF
na	na	k7c4
12	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
změny	změna	k1gFnSc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
také	také	k9
změna	změna	k1gFnSc1
názvu	název	k1gInSc2
kanálu	kanál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1996	#num#	k4
rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c4
odebrání	odebrání	k1gNnSc4
vysílací	vysílací	k2eAgFnSc2d1
licence	licence	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
slovenský	slovenský	k2eAgInSc1d1
zákon	zákon	k1gInSc1
neumožňoval	umožňovat	k5eNaImAgInS
přerušení	přerušení	k1gNnSc4
vysílání	vysílání	k1gNnSc2
kvůli	kvůli	k7c3
změně	změna	k1gFnSc3
koncepce	koncepce	k1gFnSc2
vysílání	vysílání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Licence	licence	k1gFnSc1
byla	být	k5eAaImAgFnS
provozovateli	provozovatel	k1gMnSc3
vrácena	vrácen	k2eAgNnPc1d1
na	na	k7c6
základě	základ	k1gInSc6
soudního	soudní	k2eAgNnSc2d1
rozhodnutí	rozhodnutí	k1gNnSc2
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ukázka	ukázka	k1gFnSc1
televizního	televizní	k2eAgInSc2d1
programu	program	k1gInSc2
</s>
<s>
Pondělí	pondělí	k1gNnSc1
-	-	kIx~
16.1	16.1	k4
<g/>
.1995	.1995	k4
<g/>
(	(	kIx(
<g/>
zkušební	zkušební	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Čtvrtek	čtvrtek	k1gInSc1
-	-	kIx~
16.2	16.2	k4
<g/>
.1995	.1995	k4
<g/>
(	(	kIx(
<g/>
pravidelné	pravidelný	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
Čo	Čo	k1gFnPc2
dnes	dnes	k6eAd1
uvidíte	uvidět	k5eAaPmIp2nP
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
Čo	Čo	k1gFnPc2
dnes	dnes	k6eAd1
uvidíte	uvidět	k5eAaPmIp2nP
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
Janko	Janko	k1gMnSc1
Hraško	Hraška	k1gFnSc5
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
Kocúrkovské	Kocúrkovský	k2eAgFnSc2d1
záhrady	záhrada	k1gFnSc2
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
Dobrodružstvá	Dobrodružstvá	k1gFnSc1
Čierneho	Čierne	k1gMnSc2
žrebca	žrebcus	k1gMnSc2
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
Dobrodružstvá	Dobrodružstvá	k1gFnSc1
Čierneho	Čierne	k1gMnSc4
žrebca	žrebcus	k1gMnSc4
</s>
<s>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
Gól	gól	k1gInSc1
sem	sem	k6eAd1
<g/>
,	,	kIx,
gól	gól	k1gInSc1
tam	tam	k6eAd1
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
Gól	gól	k1gInSc1
sem	sem	k6eAd1
<g/>
,	,	kIx,
gól	gól	k1gInSc4
tam	tam	k6eAd1
</s>
<s>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
Dievčatá	Dievčatá	k1gFnSc1
odvedľa	odvedľa	k1gFnSc1
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
Dievčatá	Dievčatá	k1gFnSc1
odvedľa	odvedľa	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
Lepšie	Lepšie	k1gFnSc2
byť	byť	k8xS
bohatý	bohatý	k2eAgMnSc1d1
a	a	k8xC
zdravý	zdravý	k2eAgMnSc1d1
ako	ako	k?
chudobný	chudobný	k2eAgInSc1d1
a	a	k8xC
chorý	chorý	k2eAgInSc1d1
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
Mŕtvy	Mŕtva	k1gFnSc2
bod	bod	k1gInSc4
</s>
<s>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
Teleshopping	Teleshopping	k1gInSc1
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
Krimimagazín	Krimimagazína	k1gFnPc2
</s>
<s>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
Čo	Čo	k1gMnPc1
uvidíte	uvidět	k5eAaPmIp2nP
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
Teleshopping	Teleshopping	k1gInSc1
</s>
<s>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
Čo	Čo	k1gFnPc2
uvidíte	uvidět	k5eAaPmIp2nP
</s>
<s>
TV	TV	kA
pořady	pořad	k1gInPc4
</s>
<s>
Seriály	seriál	k1gInPc1
</s>
<s>
Dívky	dívka	k1gFnPc1
od	od	k7c2
vedle	vedle	k7c2
</s>
<s>
Dobrodružství	dobrodružství	k1gNnSc1
černého	černé	k1gNnSc2
hřebce	hřebec	k1gMnSc2
</s>
<s>
Emil	Emil	k1gMnSc1
z	z	k7c2
Lönnebergy	Lönneberg	k1gMnPc4
</s>
<s>
Tarzan	Tarzan	k1gMnSc1
</s>
<s>
Zmije	zmije	k1gFnSc1
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Česko	Česko	k1gNnSc1
a	a	k8xC
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Anděl	Anděl	k1gMnSc1
svádí	svádět	k5eAaImIp3nS
ďábla	ďábel	k1gMnSc2
</s>
<s>
Čas	čas	k1gInSc1
sluhů	sluha	k1gMnPc2
</s>
<s>
Dido	Dido	k6eAd1
</s>
<s>
Dým	dým	k1gInSc1
bramborové	bramborový	k2eAgFnSc2d1
natě	nať	k1gFnSc2
</s>
<s>
Hry	hra	k1gFnPc1
lásky	láska	k1gFnSc2
šálivé	šálivý	k2eAgFnPc1d1
</s>
<s>
Jako	jako	k8xC,k8xS
jed	jed	k1gInSc1
</s>
<s>
Jára	Jára	k1gMnSc1
Cimrman	Cimrman	k1gMnSc1
ležící	ležící	k2eAgMnSc1d1
<g/>
,	,	kIx,
spící	spící	k2eAgMnSc1d1
</s>
<s>
Lepší	dobrý	k2eAgNnSc1d2
je	být	k5eAaImIp3nS
být	být	k5eAaImF
bohatý	bohatý	k2eAgMnSc1d1
a	a	k8xC
zdravý	zdravý	k2eAgMnSc1d1
než	než	k8xS
chudý	chudý	k2eAgMnSc1d1
a	a	k8xC
nemocný	nemocný	k2eAgMnSc1d1,k2eNgMnSc1d1
</s>
<s>
Limonádový	limonádový	k2eAgMnSc1d1
Joe	Joe	k1gMnSc1
aneb	aneb	k?
Koňská	koňský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
</s>
<s>
Medzi	Medze	k1gFnSc4
nebom	nebom	k1gInSc1
a	a	k8xC
zemou	zemou	k6eAd1
</s>
<s>
Muka	muka	k1gFnSc1
obraznosti	obraznost	k1gFnSc2
</s>
<s>
Pasodoble	Pasodoble	k6eAd1
pre	pre	k?
troch	trocha	k1gFnPc2
</s>
<s>
Pomocník	pomocník	k1gMnSc1
</s>
<s>
Rabaka	Rabak	k1gMnSc4
</s>
<s>
Ružové	Ružové	k2eAgInPc1d1
sny	sen	k1gInPc1
</s>
<s>
Skrytý	skrytý	k2eAgInSc4d1
prameň	pramenit	k5eAaImRp2nS
</s>
<s>
Spalovač	spalovač	k1gMnSc1
mrtvol	mrtvola	k1gFnPc2
</s>
<s>
Asijská	asijský	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
</s>
<s>
Bláznivá	bláznivý	k2eAgFnSc1d1
mise	mise	k1gFnSc1
3	#num#	k4
(	(	kIx(
<g/>
Hong	Hong	k1gInSc1
Kong	Kongo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Sbohem	sbohem	k0
Bruce	Bruka	k1gFnSc3
Lee	Lea	k1gFnSc3
(	(	kIx(
<g/>
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Žijem	Žijem	k?
so	so	k?
svojím	svojit	k5eAaImIp1nS
otcom	otcom	k1gInSc4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Dáma	dáma	k1gFnSc1
a	a	k8xC
lupič	lupič	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Valdézovi	Valdézův	k2eAgMnPc1d1
koně	kůň	k1gMnPc1
</s>
<s>
Zvíře	zvíře	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Dvaja	Dvaja	k1gFnSc1
pre	pre	k?
dobrodružstvo	dobrodružstvo	k1gNnSc1
</s>
<s>
Únos	únos	k1gInSc1
lodě	loď	k1gFnSc2
</s>
<s>
Žralok	žralok	k1gMnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Harry	Harra	k1gFnPc1
Tracy	Traca	k1gFnSc2
<g/>
,	,	kIx,
Desperado	Desperada	k1gFnSc5
</s>
<s>
Křižovatka	křižovatka	k1gFnSc1
smrti	smrt	k1gFnSc2
</s>
<s>
Stín	stín	k1gInSc1
vlka	vlk	k1gMnSc2
</s>
<s>
Vrata	vrata	k1gNnPc1
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Podoba	podoba	k1gFnSc1
čistě	čistě	k6eAd1
náhodná	náhodný	k2eAgFnSc1d1
</s>
<s>
Kaminsky	Kaminsky	k6eAd1
</s>
<s>
Srdcom	Srdcom	k1gInSc1
matky	matka	k1gFnSc2
</s>
<s>
Zabijak	Zabijak	k1gMnSc1
Kid	Kid	k1gMnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Dvojí	dvojí	k4xRgInSc4
svět	svět	k1gInSc4
hotelu	hotel	k1gInSc2
Pacifik	Pacifik	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Andrej	Andrej	k1gMnSc1
Rublev	Rublev	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Aj	aj	kA
vlčí	vlčet	k5eAaImIp3nS
mak	mako	k1gNnPc2
je	být	k5eAaImIp3nS
kvet	kvet	k5eAaPmF
</s>
<s>
Americké	americký	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
</s>
<s>
Bat	Bat	k?
21	#num#	k4
</s>
<s>
Buffalo	Buffat	k5eAaPmAgNnS
Bill	Bill	k1gMnSc1
a	a	k8xC
Indiáni	Indián	k1gMnPc1
</s>
<s>
Cyber-Tracker	Cyber-Tracker	k1gMnSc1
</s>
<s>
Dracula	Dracula	k1gFnSc1
-	-	kIx~
milostný	milostný	k2eAgInSc1d1
příběh	příběh	k1gInSc1
</s>
<s>
Dům	dům	k1gInSc1
veselých	veselý	k2eAgMnPc2d1
duchů	duch	k1gMnPc2
</s>
<s>
Flash	Flash	k1gMnSc1
Gordon	Gordon	k1gMnSc1
</s>
<s>
Hurikán	hurikán	k1gInSc1
</s>
<s>
Jako	jako	k8xS,k8xC
led	led	k1gInSc1
</s>
<s>
MacGyver	MacGyver	k1gInSc1
<g/>
:	:	kIx,
Ztracený	ztracený	k2eAgInSc1d1
poklad	poklad	k1gInSc1
Atlantidy	Atlantida	k1gFnSc2
</s>
<s>
Mrtvý	mrtvý	k2eAgInSc1d1
bod	bod	k1gInSc1
</s>
<s>
Nebezpečná	bezpečný	k2eNgFnSc1d1
hra	hra	k1gFnSc1
na	na	k7c4
vojáky	voják	k1gMnPc4
</s>
<s>
Noční	noční	k2eAgNnPc1d1
oči	oko	k1gNnPc1
II	II	kA
</s>
<s>
Propuštěn	propuštěn	k2eAgMnSc1d1
na	na	k7c6
kauci	kauce	k1gFnSc6
</s>
<s>
Serpico	Serpico	k6eAd1
</s>
<s>
Sladké	Sladké	k2eAgFnPc4d1
starosti	starost	k1gFnPc4
</s>
<s>
Smyslná	smyslný	k2eAgFnSc1d1
orchidej	orchidej	k1gFnSc1
</s>
<s>
Tajomstvo	Tajomstvo	k1gNnSc1
rytierov	rytierovo	k1gNnPc2
rádu	rád	k2eAgFnSc4d1
Delta	delta	k1gFnSc1
</s>
<s>
V	v	k7c6
dobrém	dobrý	k2eAgNnSc6d1
i	i	k8xC
zlém	zlý	k2eAgNnSc6d1
</s>
<s>
Záhrobní	záhrobní	k2eAgNnSc1d1
komando	komando	k1gNnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Casanova	Casanův	k2eAgFnSc1d1
</s>
<s>
Fantom	fantom	k1gInSc1
v	v	k7c6
Monte	Mont	k1gInSc5
Carlu	Carl	k1gMnSc6
</s>
<s>
Hazard	hazard	k1gInSc1
srdcí	srdce	k1gNnPc2
</s>
<s>
Pro	pro	k7c4
děti	dítě	k1gFnPc4
</s>
<s>
Dita	Dita	k1gFnSc1
na	na	k7c4
fronte	front	k1gInSc5
</s>
<s>
Dita	Dita	k1gFnSc1
na	na	k7c4
pošte	pošt	k1gMnSc5
</s>
<s>
Dita	Dita	k1gFnSc1
na	na	k7c4
šibačke	šibačke	k1gFnSc4
</s>
<s>
Dita	Dita	k1gFnSc1
na	na	k7c6
vianoce	vianoka	k1gFnSc6
</s>
<s>
Edita	Edita	k1gFnSc1
na	na	k7c6
vianoce	vianoka	k1gFnSc6
</s>
<s>
Janko	Janka	k1gFnSc5
Hraško	Hraška	k1gFnSc5
</s>
<s>
Janko	Janka	k1gFnSc5
Hraško	Hraška	k1gFnSc5
na	na	k7c4
dejepise	dejepise	k1gFnSc5
</s>
<s>
Kocúrkovský	Kocúrkovský	k2eAgInSc1d1
dom	dom	k?
</s>
<s>
Kocúrkovský	Kocúrkovský	k2eAgInSc1d1
majster	majster	k1gInSc1
</s>
<s>
Kocúrkovský	Kocúrkovský	k2eAgInSc1d1
mlyn	mlyn	k1gInSc1
</s>
<s>
Kocúrkovské	Kocúrkovský	k2eAgFnPc1d1
záhrady	záhrada	k1gFnPc1
</s>
<s>
Kocúrkovský	Kocúrkovský	k2eAgInSc4d1
zlodej	zlodat	k5eAaImRp2nS,k5eAaPmRp2nS
</s>
<s>
Dokumenty	dokument	k1gInPc1
</s>
<s>
Cyklamen	Cyklamen	k2eAgInSc1d1
fatranský	fatranský	k2eAgInSc1d1
</s>
<s>
Encyklopédia	Encyklopédium	k1gNnPc1
slovenských	slovenský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
</s>
<s>
Jelenia	Jelenium	k1gNnPc4
zver	zver	k1gInSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Murgaš	Murgaš	k1gMnSc1
</s>
<s>
Kamzík	kamzík	k1gMnSc1
tatranský	tatranský	k2eAgMnSc1d1
</s>
<s>
Parnasius	Parnasius	k1gMnSc1
Apollo	Apollo	k1gMnSc1
L	L	kA
</s>
<s>
Sestrička	Sestrička	k1gFnSc1
</s>
<s>
Stredoeurópská	Stredoeurópský	k2eAgFnSc1d1
čierná	čierný	k2eAgFnSc1d1
zver	zver	k1gMnSc1
</s>
<s>
Veľké	Veľký	k2eAgFnPc1d1
bitky	bitka	k1gFnPc1
spojencov	spojencovo	k1gNnPc2
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
umenie	umenie	k1gFnSc1
veľkej	veľkat	k5eAaBmRp2nS,k5eAaImRp2nS,k5eAaPmRp2nS
Moravy	Morava	k1gFnSc2
</s>
<s>
Zábavní	zábavní	k2eAgInPc1d1
pořady	pořad	k1gInPc1
a	a	k8xC
publicistika	publicistika	k1gFnSc1
</s>
<s>
Automoto	Automota	k1gFnSc5
-	-	kIx~
magazín	magazín	k1gInSc4
</s>
<s>
Čo	Čo	k?
nás	my	k3xPp1nPc4
páli	pálit	k5eAaPmRp2nS,k5eAaImRp2nS
-	-	kIx~
talkshow	talkshow	k?
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
-	-	kIx~
magazín	magazín	k1gInSc1
</s>
<s>
Film	film	k1gInSc1
a	a	k8xC
video	video	k1gNnSc1
</s>
<s>
Gól	gól	k1gInSc1
sem	sem	k6eAd1
<g/>
,	,	kIx,
gól	gól	k1gInSc4
tam	tam	k6eAd1
</s>
<s>
Krimimagazín	Krimimagazín	k1gMnSc1
</s>
<s>
Kultúra	Kultúra	k6eAd1
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
svatba	svatba	k1gFnSc1
-	-	kIx~
zahajovací	zahajovací	k2eAgFnSc1d1
show	show	k1gFnSc1
</s>
<s>
Premeny	Premen	k1gInPc1
podob	podoba	k1gFnPc2
<g/>
,	,	kIx,
Jozef	Jozef	k1gMnSc1
Kostka	Kostka	k1gMnSc1
</s>
<s>
Soirée	soirée	k1gFnSc1
</s>
<s>
Šport	Šport	k1gInSc1
spoza	spoza	k1gFnSc1
dverí	dverí	k1gMnSc1
-	-	kIx~
magazín	magazín	k1gInSc1
</s>
<s>
TV	TV	kA
Klub	klub	k1gInSc1
pre	pre	k?
<g/>
...	...	k?
<g/>
dámy	dáma	k1gFnSc2
</s>
<s>
TV	TV	kA
Klub	klub	k1gInSc1
pre	pre	k?
<g/>
...	...	k?
<g/>
pánov	pánov	k1gInSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
MS	MS	kA
sveta	sveta	k1gFnSc1
v	v	k7c4
rock	rock	k1gInSc4
<g/>
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
<g/>
rolle	rolle	k1gNnPc4
</s>
<s>
Dostupnost	dostupnost	k1gFnSc1
</s>
<s>
Terestrické	terestrický	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
</s>
<s>
Analogové	analogový	k2eAgNnSc1d1
terestrické	terestrický	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
bylo	být	k5eAaImAgNnS
provozováno	provozovat	k5eAaImNgNnS
z	z	k7c2
bratislavského	bratislavský	k2eAgInSc2d1
vysílače	vysílač	k1gInSc2
Kamzík	kamzík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
KanálUmístěníNázev	KanálUmístěníNázet	k5eAaPmDgMnS
vysílače	vysílač	k1gInSc2
</s>
<s>
50	#num#	k4
<g/>
BratislavaKamzík	BratislavaKamzík	k1gInSc1
</s>
<s>
Satelitní	satelitní	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
</s>
<s>
DružiceFrekvence	DružiceFrekvence	k1gFnSc1
(	(	kIx(
<g/>
GHz	GHz	k1gFnSc1
<g/>
)	)	kIx)
<g/>
KódováníVysílací	KódováníVysílací	k2eAgInSc1d1
čas	čas	k1gInSc1
(	(	kIx(
<g/>
SEČ	SEČ	kA
<g/>
)	)	kIx)
<g/>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Eutelsat	Eutelsat	k5eAaImF,k5eAaPmF
I-F5	I-F5	k1gFnSc4
(	(	kIx(
<g/>
21.5	21.5	k4
<g/>
°	°	k?
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
11.097	11.097	k4
<g/>
/	/	kIx~
<g/>
V	V	kA
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
signál	signál	k1gInSc1
nebyl	být	k5eNaImAgInS
kódován	kódovat	k5eAaBmNgInS
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
-	-	kIx~
23	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
Kabelové	kabelový	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
</s>
<s>
Níže	nízce	k6eAd2
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
seznam	seznam	k1gInSc1
kabelových	kabelový	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
si	se	k3xPyFc3
televizní	televizní	k2eAgInSc4d1
program	program	k1gInSc4
Danubius	Danubius	k1gInSc1
Cable	Cable	k1gNnSc1
Television	Television	k1gInSc1
nechaly	nechat	k5eAaPmAgInP
zaregistrovat	zaregistrovat	k5eAaPmF
u	u	k7c2
Rady	rada	k1gFnSc2
pro	pro	k7c4
rozhlasové	rozhlasový	k2eAgNnSc4d1
a	a	k8xC
televizní	televizní	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
SKT	SKT	kA
Bratislava	Bratislava	k1gFnSc1
na	na	k7c6
kanálu	kanál	k1gInSc6
42	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
CATV	CATV	kA
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Kabel	kabel	k1gInSc1
Plus	plus	k6eAd1
Střední	střední	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
a.s.	a.s.	k?
</s>
<s>
Kabelová	kabelový	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Česká	český	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
</s>
<s>
Kabelová	kabelový	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Třinec	Třinec	k1gInSc1
</s>
<s>
NOEL	NOEL	kA
</s>
<s>
Novák	Novák	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.orsr.sk/vypis.asp?ID=5935&	http://www.orsr.sk/vypis.asp?ID=5935&	k?
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
http://www.sme.sk/c/2114387/sestnasteho-januara-zacne-pokusne-vysielanie-prva-slovenska-sukromna-televizna-stanica.html1	http://www.sme.sk/c/2114387/sestnasteho-januara-zacne-pokusne-vysielanie-prva-slovenska-sukromna-televizna-stanica.html1	k4
2	#num#	k4
3	#num#	k4
http://www.sme.sk/c/2139776/16-januara-zacne-pokusne-vysielat-prvy-slovensky-sukromny-televizny-kanal.html	http://www.sme.sk/c/2139776/16-januara-zacne-pokusne-vysielat-prvy-slovensky-sukromny-televizny-kanal.html	k1gMnSc1
<g/>
↑	↑	k?
DANUBIUS	DANUBIUS	kA
CABLE	CABLE	kA
TV	TV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kabel	kabel	k1gInSc1
Plus	plus	k1gInSc4
-	-	kIx~
Programový	programový	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Únor	únor	k1gInSc1
1995	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
http://www.ivankrasko.sk/diplomovapraca.pdf1	http://www.ivankrasko.sk/diplomovapraca.pdf1	k4
2	#num#	k4
3	#num#	k4
http://www.satelitnatv.sk/2012/03/dctv-prva-slovenska-sukromna-televizia-ktora-vysielala-prostrednictvom-satelitu/	http://www.satelitnatv.sk/2012/03/dctv-prva-slovenska-sukromna-televizia-ktora-vysielala-prostrednictvom-satelitu/	k4
<g/>
↑	↑	k?
http://www.sme.sk/c/2115478/od-pondelka-v-stopercentnej-kvalite-jedina-otazka-pre-prezidenta-prvej-sukromnej-televiznej-spolocnosti-danubius-cable-tv-dctv-m.html	http://www.sme.sk/c/2115478/od-pondelka-v-stopercentnej-kvalite-jedina-otazka-pre-prezidenta-prvej-sukromnej-televiznej-spolocnosti-danubius-cable-tv-dctv-m.html	k1gMnSc1
<g/>
↑	↑	k?
DANUBIUS	DANUBIUS	kA
TV	TV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Satelit	satelit	k1gInSc1
Parabola	parabola	k1gFnSc1
<g/>
:	:	kIx,
týdeník	týdeník	k1gInSc1
pro	pro	k7c4
satelitní	satelitní	k2eAgInSc4d1
a	a	k8xC
kabelový	kabelový	k2eAgInSc4d1
příjem	příjem	k1gInSc4
<g/>
,	,	kIx,
programy	program	k1gInPc4
<g/>
,	,	kIx,
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
zajímavosti	zajímavost	k1gFnPc4
a	a	k8xC
inzerci	inzerce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květen	květen	k1gInSc1
1995	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
17	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
4434	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.sme.sk/c/2100909/danubius-cable-tv-ziada-o-odsun-zaciatku-vysielania.html	http://www.sme.sk/c/2100909/danubius-cable-tv-ziada-o-odsun-zaciatku-vysielania.html	k1gMnSc1
<g/>
↑	↑	k?
http://archiv.rrtv.cz/zprava1999/a42.html	http://archiv.rrtv.cz/zprava1999/a42.html	k1gMnSc1
<g/>
↑	↑	k?
http://archiv.rrtv.cz/zprava2001/254.html	http://archiv.rrtv.cz/zprava2001/254.html	k1gMnSc1
<g/>
↑	↑	k?
http://archiv.rrtv.cz/zprava1997/zprava1997.html	http://archiv.rrtv.cz/zprava1997/zprava1997.html	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
