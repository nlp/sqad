<s>
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Italia	Italius	k1gMnSc2	Italius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
488	[number]	k4	488
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
(	(	kIx(	(
<g/>
740	[number]	k4	740
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakouskem	Rakousko	k1gNnSc7	Rakousko
(	(	kIx(	(
<g/>
430	[number]	k4	430
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
(	(	kIx(	(
<g/>
232	[number]	k4	232
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
Itálie	Itálie	k1gFnSc2	Itálie
leží	ležet	k5eAaImIp3nP	ležet
dva	dva	k4xCgInPc1	dva
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Vatikán	Vatikán	k1gInSc1	Vatikán
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
(	(	kIx(	(
<g/>
39	[number]	k4	39
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Itálii	Itálie	k1gFnSc4	Itálie
navíc	navíc	k6eAd1	navíc
patří	patřit	k5eAaImIp3nS	patřit
území	území	k1gNnSc1	území
obklopené	obklopený	k2eAgNnSc1d1	obklopené
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
Campione	Campion	k1gInSc5	Campion
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
východu	východ	k1gInSc2	východ
Itálii	Itálie	k1gFnSc3	Itálie
omývá	omývat	k5eAaImIp3nS	omývat
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Jónské	jónský	k2eAgNnSc4d1	Jónské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Tyrhénské	tyrhénský	k2eAgNnSc4d1	Tyrhénské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
Ligurské	ligurský	k2eAgNnSc4d1	Ligurské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
7600	[number]	k4	7600
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
patří	patřit	k5eAaImIp3nP	patřit
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
ostrovy	ostrov	k1gInPc1	ostrov
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
:	:	kIx,	:
Sardinie	Sardinie	k1gFnSc1	Sardinie
a	a	k8xC	a
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
EU	EU	kA	EU
<g/>
,	,	kIx,	,
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
Eurozóny	Eurozóna	k1gFnSc2	Eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
Itálie	Itálie	k1gFnSc1	Itálie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
jako	jako	k8xS	jako
království	království	k1gNnSc1	království
spojením	spojení	k1gNnSc7	spojení
mnoha	mnoho	k4c2	mnoho
menších	malý	k2eAgInPc2d2	menší
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgNnP	být
nejprve	nejprve	k6eAd1	nejprve
členem	člen	k1gMnSc7	člen
Trojspolku	trojspolek	k1gInSc2	trojspolek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přidala	přidat	k5eAaPmAgFnS	přidat
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
chopili	chopit	k5eAaPmAgMnP	chopit
moci	moct	k5eAaImF	moct
fašisté	fašista	k1gMnPc1	fašista
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Benitem	Benitum	k1gNnSc7	Benitum
Mussolinim	Mussolinima	k1gFnPc2	Mussolinima
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
hitlerovského	hitlerovský	k2eAgNnSc2d1	hitlerovské
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
nahrazeno	nahrazen	k2eAgNnSc4d1	nahrazeno
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
EU	EU	kA	EU
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
zde	zde	k6eAd1	zde
existovalo	existovat	k5eAaImAgNnS	existovat
mnoho	mnoho	k4c1	mnoho
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
založených	založený	k2eAgNnPc2d1	založené
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
řecké	řecký	k2eAgFnSc2d1	řecká
kolonizace	kolonizace	k1gFnSc2	kolonizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
města	město	k1gNnSc2	město
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
z	z	k7c2	z
několika	několik	k4yIc2	několik
osad	osada	k1gFnPc2	osada
konsolidovalo	konsolidovat	k5eAaBmAgNnS	konsolidovat
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
neustálým	neustálý	k2eAgInSc7d1	neustálý
růstem	růst	k1gInSc7	růst
určovalo	určovat	k5eAaImAgNnS	určovat
dění	dění	k1gNnSc1	dění
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
<g/>
)	)	kIx)	)
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
roku	rok	k1gInSc2	rok
476	[number]	k4	476
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Řím	Řím	k1gInSc1	Řím
zůstal	zůstat	k5eAaPmAgInS	zůstat
centrem	centrum	k1gNnSc7	centrum
křesťanství	křesťanství	k1gNnPc2	křesťanství
i	i	k8xC	i
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
zhroucení	zhroucení	k1gNnSc6	zhroucení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Apeninský	apeninský	k2eAgInSc1d1	apeninský
poloostrov	poloostrov	k1gInSc1	poloostrov
ovládnut	ovládnout	k5eAaPmNgMnS	ovládnout
barbary	barbar	k1gMnPc7	barbar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
existovalo	existovat	k5eAaImAgNnS	existovat
více	hodně	k6eAd2	hodně
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
sjednoceny	sjednotit	k5eAaPmNgInP	sjednotit
králem	král	k1gMnSc7	král
Viktorem	Viktor	k1gMnSc7	Viktor
Emanuelem	Emanuel	k1gMnSc7	Emanuel
II	II	kA	II
<g/>
.	.	kIx.	.
do	do	k7c2	do
Italského	italský	k2eAgNnSc2d1	italské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Benátsko	Benátsko	k1gNnSc1	Benátsko
nebo	nebo	k8xC	nebo
Papežský	papežský	k2eAgInSc1d1	papežský
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
připojovány	připojovat	k5eAaImNgFnP	připojovat
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bojovala	bojovat	k5eAaImAgFnS	bojovat
Itálie	Itálie	k1gFnSc1	Itálie
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
připojila	připojit	k5eAaPmAgFnS	připojit
území	území	k1gNnSc4	území
Jižního	jižní	k2eAgNnSc2d1	jižní
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
,	,	kIx,	,
přístav	přístav	k1gInSc1	přístav
Terst	Terst	k1gInSc1	Terst
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
si	se	k3xPyFc3	se
ponechat	ponechat	k5eAaPmF	ponechat
Dodekaneské	Dodekaneský	k2eAgInPc4d1	Dodekaneský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
italská	italský	k2eAgFnSc1d1	italská
veřejnost	veřejnost	k1gFnSc1	veřejnost
nespokojena	spokojen	k2eNgFnSc1d1	nespokojena
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
po	po	k7c6	po
pochodu	pochod	k1gInSc6	pochod
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Benito	Benita	k1gMnSc5	Benita
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
<g/>
.	.	kIx.	.
</s>
<s>
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
fašistickou	fašistický	k2eAgFnSc4d1	fašistická
diktaturu	diktatura	k1gFnSc4	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dojednat	dojednat	k5eAaPmF	dojednat
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
ohledně	ohledně	k7c2	ohledně
rozsahu	rozsah	k1gInSc2	rozsah
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Fašistická	fašistický	k2eAgFnSc1d1	fašistická
Itálie	Itálie	k1gFnSc1	Itálie
vedla	vést	k5eAaImAgFnS	vést
výbojnou	výbojný	k2eAgFnSc4d1	výbojná
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
italská	italský	k2eAgNnPc1d1	italské
vojska	vojsko	k1gNnPc4	vojsko
také	také	k9	také
na	na	k7c4	na
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Mussoliniho	Mussolinize	k6eAd1	Mussolinize
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
hitlerovským	hitlerovský	k2eAgNnSc7d1	hitlerovské
Německem	Německo	k1gNnSc7	Německo
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vedlo	vést	k5eAaImAgNnS	vést
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
měla	mít	k5eAaImAgFnS	mít
Itálie	Itálie	k1gFnSc1	Itálie
52	[number]	k4	52
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
ekonomicky	ekonomicky	k6eAd1	ekonomicky
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
členité	členitý	k2eAgInPc1d1	členitý
se	se	k3xPyFc4	se
zálivy	záliv	k1gInPc7	záliv
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ploché	plochý	k2eAgInPc1d1	plochý
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
7600	[number]	k4	7600
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
převážně	převážně	k6eAd1	převážně
hornatý	hornatý	k2eAgInSc1d1	hornatý
<g/>
.	.	kIx.	.
</s>
<s>
Dominují	dominovat	k5eAaImIp3nP	dominovat
Západní	západní	k2eAgFnPc1d1	západní
a	a	k8xC	a
Východní	východní	k2eAgFnPc1d1	východní
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
přesahující	přesahující	k2eAgFnSc4d1	přesahující
4000	[number]	k4	4000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
včetně	včetně	k7c2	včetně
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
4807	[number]	k4	4807
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
(	(	kIx(	(
<g/>
Monte	Mont	k1gInSc5	Mont
Bianco	bianco	k2eAgMnPc3d1	bianco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
jsou	být	k5eAaImIp3nP	být
častá	častý	k2eAgNnPc4d1	časté
jezera	jezero	k1gNnPc4	jezero
(	(	kIx(	(
<g/>
Lago	Lago	k6eAd1	Lago
di	di	k?	di
Garda	garda	k1gFnSc1	garda
<g/>
,	,	kIx,	,
Lago	Laga	k1gMnSc5	Laga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
Lago	Lago	k6eAd1	Lago
di	di	k?	di
Como	Como	k1gMnSc1	Como
<g/>
)	)	kIx)	)
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
ústupem	ústup	k1gInSc7	ústup
pleistocenních	pleistocenní	k2eAgInPc2d1	pleistocenní
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
Apeninský	apeninský	k2eAgInSc1d1	apeninský
a	a	k8xC	a
Kalabrijský	Kalabrijský	k2eAgInSc1d1	Kalabrijský
poloostrov	poloostrov	k1gInSc1	poloostrov
(	(	kIx(	(
<g/>
Calabria	Calabrium	k1gNnSc2	Calabrium
<g/>
)	)	kIx)	)
a	a	k8xC	a
největší	veliký	k2eAgInSc4d3	veliký
středomořský	středomořský	k2eAgInSc4d1	středomořský
ostrov	ostrov	k1gInSc4	ostrov
Sicílii	Sicílie	k1gFnSc4	Sicílie
(	(	kIx(	(
<g/>
Sicilia	Sicilia	k1gFnSc1	Sicilia
<g/>
)	)	kIx)	)
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
pohoří	pohoří	k1gNnSc4	pohoří
Apeniny	Apeniny	k1gFnPc4	Apeniny
dosahující	dosahující	k2eAgFnPc1d1	dosahující
téměř	téměř	k6eAd1	téměř
3000	[number]	k4	3000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Pásmo	pásmo	k1gNnSc1	pásmo
Apenin	Apeniny	k1gFnPc2	Apeniny
je	být	k5eAaImIp3nS	být
seismicky	seismicky	k6eAd1	seismicky
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
jsou	být	k5eAaImIp3nP	být
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
a	a	k8xC	a
erupce	erupce	k1gFnPc1	erupce
sopek	sopka	k1gFnPc2	sopka
Vesuv	Vesuv	k1gInSc1	Vesuv
(	(	kIx(	(
<g/>
Vesuvio	Vesuvio	k1gNnSc1	Vesuvio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Etna	Etna	k1gFnSc1	Etna
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
činná	činný	k2eAgFnSc1d1	činná
sopka	sopka	k1gFnSc1	sopka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
3323	[number]	k4	3323
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
sopek	sopka	k1gFnPc2	sopka
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Lipari	Lipar	k1gFnSc2	Lipar
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
Pádská	pádský	k2eAgFnSc1d1	Pádská
nížina	nížina	k1gFnSc1	nížina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Pád	Pád	k1gInSc1	Pád
(	(	kIx(	(
<g/>
Po	Po	kA	Po
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
Sardinie	Sardinie	k1gFnSc1	Sardinie
(	(	kIx(	(
<g/>
Sardegna	Sardegna	k1gFnSc1	Sardegna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
je	být	k5eAaImIp3nS	být
obklopená	obklopený	k2eAgFnSc1d1	obklopená
mořem	moře	k1gNnSc7	moře
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
peninsulární	peninsulární	k2eAgInSc4d1	peninsulární
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Mare	Mar	k1gInPc1	Mar
Nostrum	Nostrum	k1gNnSc1	Nostrum
<g/>
,	,	kIx,	,
naše	náš	k3xOp1gNnSc4	náš
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
Římané	Říman	k1gMnPc1	Říman
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Tyrhénské	tyrhénský	k2eAgNnSc4d1	Tyrhénské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Jónské	jónský	k2eAgNnSc4d1	Jónské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
Ligurské	ligurský	k2eAgNnSc4d1	Ligurské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Sardinie	Sardinie	k1gFnSc2	Sardinie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Sardské	sardský	k2eAgNnSc1d1	sardský
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
Mar	Mar	k1gFnPc7	Mar
di	di	k?	di
Sardegna	Sardegn	k1gInSc2	Sardegn
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sicílie	Sicílie	k1gFnSc2	Sicílie
Sicilský	sicilský	k2eAgInSc4d1	sicilský
kanál	kanál	k1gInSc4	kanál
(	(	kIx(	(
<g/>
Canale	Canala	k1gFnSc3	Canala
di	di	k?	di
Sicilia	Sicilia	k1gFnSc1	Sicilia
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maltézský	maltézský	k2eAgInSc4d1	maltézský
kanál	kanál	k1gInSc4	kanál
(	(	kIx(	(
<g/>
Canale	Canala	k1gFnSc3	Canala
di	di	k?	di
Malta	Malta	k1gFnSc1	Malta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sicílii	Sicílie	k1gFnSc4	Sicílie
od	od	k7c2	od
Kalábrie	Kalábrie	k1gFnSc2	Kalábrie
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Messinská	messinský	k2eAgFnSc1d1	Messinská
úžina	úžina	k1gFnSc1	úžina
(	(	kIx(	(
<g/>
Stretto	stretto	k1gNnSc1	stretto
di	di	k?	di
Messina	Messina	k1gFnSc1	Messina
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
Afriky	Afrika	k1gFnSc2	Afrika
Sicilský	sicilský	k2eAgInSc4d1	sicilský
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
moře	moře	k1gNnSc4	moře
<g/>
:	:	kIx,	:
Ligurské	ligurský	k2eAgNnSc4d1	Ligurské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Tyrhénské	tyrhénský	k2eAgNnSc4d1	Tyrhénské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Jónské	jónský	k2eAgNnSc4d1	Jónské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
zálivy	záliv	k1gInPc1	záliv
<g/>
:	:	kIx,	:
Janovský	janovský	k2eAgInSc1d1	janovský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
Gaetský	Gaetský	k2eAgInSc1d1	Gaetský
záliv	záliv	k1gInSc1	záliv
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Salernský	Salernský	k2eAgInSc1d1	Salernský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
Tarentský	Tarentský	k2eAgInSc1d1	Tarentský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
Benátský	benátský	k2eAgInSc1d1	benátský
záliv	záliv	k1gInSc1	záliv
průlivy	průliv	k1gInPc1	průliv
<g/>
:	:	kIx,	:
Messinský	messinský	k2eAgInSc1d1	messinský
průliv	průliv	k1gInSc1	průliv
<g/>
,	,	kIx,	,
Otrantský	Otrantský	k2eAgInSc1d1	Otrantský
průliv	průliv	k1gInSc1	průliv
<g/>
,	,	kIx,	,
Bonifácký	Bonifácký	k2eAgInSc1d1	Bonifácký
průliv	průliv	k1gInSc1	průliv
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
:	:	kIx,	:
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc1	Sardinie
<g/>
,	,	kIx,	,
Elba	Elba	k1gFnSc1	Elba
<g/>
,	,	kIx,	,
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
:	:	kIx,	:
Toskánské	toskánský	k2eAgNnSc1d1	toskánské
souostroví	souostroví	k1gNnSc1	souostroví
(	(	kIx(	(
<g/>
Arcipelago	Arcipelago	k1gNnSc1	Arcipelago
Toscano	Toscana	k1gFnSc5	Toscana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Kampánské	Kampánský	k2eAgNnSc1d1	Kampánský
souostroví	souostroví	k1gNnSc1	souostroví
(	(	kIx(	(
<g/>
Arcipelago	Arcipelago	k1gNnSc1	Arcipelago
Campano	Campana	k1gFnSc5	Campana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eolské	eolský	k2eAgInPc1d1	eolský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Isole	Isole	k1gFnSc1	Isole
Eolie	Eolie	k1gFnSc2	Eolie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tremitské	Tremitský	k2eAgInPc1d1	Tremitský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Isole	Isole	k1gFnSc1	Isole
Tremiti	Tremiti	k1gMnSc1	Tremiti
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Isole	Isole	k1gFnSc1	Isole
Pelagie	Pelagie	k1gFnSc1	Pelagie
<g/>
,	,	kIx,	,
Isole	Isole	k1gFnSc1	Isole
Egadi	Egad	k1gMnPc1	Egad
poloostrovy	poloostrov	k1gInPc1	poloostrov
<g/>
:	:	kIx,	:
Apeninský	apeninský	k2eAgInSc1d1	apeninský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
Salentina	Salentina	k1gFnSc1	Salentina
<g/>
,	,	kIx,	,
Kalabrijský	Kalabrijský	k2eAgInSc1d1	Kalabrijský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Gargano	Gargana	k1gFnSc5	Gargana
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
:	:	kIx,	:
Apeniny	Apeniny	k1gFnPc1	Apeniny
<g/>
,	,	kIx,	,
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Dolomity	Dolomity	k1gInPc1	Dolomity
jezera	jezero	k1gNnSc2	jezero
<g/>
:	:	kIx,	:
Gardské	Gardský	k2eAgNnSc1d1	Gardské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Lago	Lago	k1gNnSc1	Lago
di	di	k?	di
Garda	garda	k1gFnSc1	garda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Como	Coma	k1gMnSc5	Coma
<g/>
,	,	kIx,	,
Lago	Laga	k1gMnSc5	Laga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
Trasimenské	Trasimenský	k2eAgNnSc1d1	Trasimenské
jezero	jezero	k1gNnSc1	jezero
řeky	řeka	k1gFnSc2	řeka
<g/>
:	:	kIx,	:
Pád	Pád	k1gInSc1	Pád
<g/>
,	,	kIx,	,
Adiže	Adiže	k1gFnSc1	Adiže
(	(	kIx(	(
<g/>
Adige	Adige	k1gInSc1	Adige
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tibera	Tibera	k1gFnSc1	Tibera
(	(	kIx(	(
<g/>
Tevere	Tever	k1gMnSc5	Tever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arno	Arno	k1gNnSc1	Arno
<g/>
,	,	kIx,	,
Piave	Piaev	k1gFnPc1	Piaev
<g/>
,	,	kIx,	,
Ofanto	Ofanto	k1gNnSc1	Ofanto
<g/>
,	,	kIx,	,
Sineto	Sineto	k1gNnSc1	Sineto
sopky	sopka	k1gFnSc2	sopka
<g/>
:	:	kIx,	:
Etna	Etna	k1gFnSc1	Etna
<g/>
,	,	kIx,	,
Vesuv	Vesuv	k1gInSc1	Vesuv
(	(	kIx(	(
<g/>
Vesuvio	Vesuvio	k1gNnSc1	Vesuvio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stromboli	Strombole	k1gFnSc4	Strombole
Alpy	alpa	k1gFnSc2	alpa
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
s	s	k7c7	s
rozdíly	rozdíl	k1gInPc7	rozdíl
mezi	mezi	k7c4	mezi
vrcholy	vrchol	k1gInPc4	vrchol
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
údolími	údolí	k1gNnPc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Pádská	pádský	k2eAgFnSc1d1	Pádská
nížina	nížina	k1gFnSc1	nížina
má	mít	k5eAaImIp3nS	mít
chladný	chladný	k2eAgInSc4d1	chladný
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
klima	klima	k1gNnSc1	klima
s	s	k7c7	s
typickým	typický	k2eAgNnSc7d1	typické
horkým	horký	k2eAgNnSc7d1	horké
suchým	suchý	k2eAgNnSc7d1	suché
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
mírnou	mírný	k2eAgFnSc7d1	mírná
zimou	zima	k1gFnSc7	zima
bohatou	bohatý	k2eAgFnSc7d1	bohatá
na	na	k7c4	na
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Italské	italský	k2eAgFnSc2d1	italská
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
20	[number]	k4	20
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
regioni	regioň	k1gFnSc6	regioň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5	[number]	k4	5
má	mít	k5eAaImIp3nS	mít
autonomní	autonomní	k2eAgInSc4d1	autonomní
status	status	k1gInSc4	status
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
109	[number]	k4	109
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
20	[number]	k4	20
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
regione	region	k1gInSc5	region
<g/>
)	)	kIx)	)
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
109	[number]	k4	109
okresů	okres	k1gInPc2	okres
<g/>
/	/	kIx~	/
<g/>
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
provincia	provincia	k1gFnSc1	provincia
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
obce	obec	k1gFnPc4	obec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
seskupeny	seskupit	k5eAaPmNgFnP	seskupit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pod	pod	k7c4	pod
jednu	jeden	k4xCgFnSc4	jeden
větší	veliký	k2eAgFnSc4d2	veliký
zvanou	zvaný	k2eAgFnSc4d1	zvaná
comune	comun	k1gInSc5	comun
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
je	být	k5eAaImIp3nS	být
kontrolována	kontrolovat	k5eAaImNgFnS	kontrolovat
Ústavním	ústavní	k2eAgInSc7d1	ústavní
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
parlamentem	parlament	k1gInSc7	parlament
a	a	k8xC	a
krajskými	krajský	k2eAgInPc7d1	krajský
sbory	sbor	k1gInPc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Voliči	volič	k1gMnPc1	volič
volí	volit	k5eAaImIp3nP	volit
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
regionálních	regionální	k2eAgFnPc2d1	regionální
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
má	mít	k5eAaImIp3nS	mít
630	[number]	k4	630
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
senát	senát	k1gInSc1	senát
326	[number]	k4	326
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
10	[number]	k4	10
na	na	k7c6	na
doživotí	doživotí	k1gNnSc6	doživotí
(	(	kIx(	(
<g/>
bývalí	bývalý	k2eAgMnPc1d1	bývalý
prezidenti	prezident	k1gMnPc1	prezident
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
ústavní	ústavní	k2eAgMnPc1d1	ústavní
činitelé	činitel	k1gMnPc1	činitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
sedmi	sedm	k4xCc2	sedm
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
a	a	k8xC	a
nejrozvinutějších	rozvinutý	k2eAgMnPc2d3	nejrozvinutější
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
,	,	kIx,	,
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Západním	západní	k2eAgNnSc7d1	západní
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
průmyslově-	průmyslově-	k?	průmyslově-
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
nejrozvinutější	rozvinutý	k2eAgInSc1d3	nejrozvinutější
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
samotného	samotný	k2eAgInSc2d1	samotný
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
severem	sever	k1gInSc7	sever
a	a	k8xC	a
zemědělským	zemědělský	k2eAgInSc7d1	zemědělský
jihem	jih	k1gInSc7	jih
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
i	i	k9	i
separatistické	separatistický	k2eAgFnPc4d1	separatistická
tendence	tendence	k1gFnPc4	tendence
na	na	k7c6	na
severu	sever	k1gInSc6	sever
především	především	k6eAd1	především
v	v	k7c6	v
Lombardii	Lombardie	k1gFnSc6	Lombardie
(	(	kIx(	(
<g/>
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převažuje	převažovat	k5eAaImIp3nS	převažovat
export	export	k1gInSc4	export
<g/>
.	.	kIx.	.
</s>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
euro	euro	k1gNnSc1	euro
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
platili	platit	k5eAaImAgMnP	platit
lirami	lira	k1gFnPc7	lira
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
hrubém	hrubý	k2eAgInSc6d1	hrubý
národním	národní	k2eAgInSc6d1	národní
produktu	produkt	k1gInSc6	produkt
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
jen	jen	k9	jen
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
téměř	téměř	k6eAd1	téměř
6	[number]	k4	6
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivního	aktivní	k2eAgNnSc2d1	aktivní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
zabírá	zabírat	k5eAaImIp3nS	zabírat
41,6	[number]	k4	41,6
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
22,4	[number]	k4	22,4
%	%	kIx~	%
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
privátní	privátní	k2eAgFnSc1d1	privátní
intenzifikované	intenzifikovaný	k2eAgFnSc3d1	intenzifikovaná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
latifundie	latifundie	k1gFnPc1	latifundie
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
plodiny	plodina	k1gFnPc4	plodina
<g/>
:	:	kIx,	:
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
sója	sója	k1gFnSc1	sója
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
<g/>
,	,	kIx,	,
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
hrušky	hruška	k1gFnPc1	hruška
<g/>
,	,	kIx,	,
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
vyniká	vynikat	k5eAaImIp3nS	vynikat
ve	v	k7c6	v
sklizni	sklizeň	k1gFnSc6	sklizeň
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Sklizeň	sklizeň	k1gFnSc1	sklizeň
okolo	okolo	k7c2	okolo
9	[number]	k4	9
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
okolo	okolo	k7c2	okolo
57	[number]	k4	57
000	[number]	k4	000
hektolitrů	hektolitr	k1gInPc2	hektolitr
vína	vína	k1gFnSc1	vína
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
jen	jen	k9	jen
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
:	:	kIx,	:
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
osli	osel	k1gMnPc1	osel
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc1	koza
a	a	k8xC	a
bourci	bourec	k1gMnPc1	bourec
morušoví	morušový	k2eAgMnPc1d1	morušový
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
též	též	k9	též
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
zemědělskými	zemědělský	k2eAgFnPc7d1	zemědělská
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
Pádská	pádský	k2eAgFnSc1d1	Pádská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
Apulie	Apulie	k1gFnSc1	Apulie
<g/>
,	,	kIx,	,
Toskánsko	Toskánsko	k1gNnSc1	Toskánsko
<g/>
,	,	kIx,	,
Sicílie	Sicílie	k1gFnSc1	Sicílie
a	a	k8xC	a
Kampánie	Kampánie	k1gFnSc1	Kampánie
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgFnPc4d1	malá
zásoby	zásoba	k1gFnPc4	zásoba
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
veškerá	veškerý	k3xTgNnPc1	veškerý
se	se	k3xPyFc4	se
dovážejí	dovážet	k5eAaImIp3nP	dovážet
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
velký	velký	k2eAgInSc4d1	velký
potenciál	potenciál	k1gInSc4	potenciál
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
energetice	energetika	k1gFnSc6	energetika
vodní	vodní	k2eAgInPc4d1	vodní
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
přes	přes	k7c4	přes
Tunisko	Tunisko	k1gNnSc4	Tunisko
plynovod	plynovod	k1gInSc4	plynovod
<g/>
.	.	kIx.	.
</s>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
se	se	k3xPyFc4	se
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
těží	těžet	k5eAaImIp3nS	těžet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Apenin	Apeniny	k1gFnPc2	Apeniny
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
v	v	k7c6	v
ještě	ještě	k9	ještě
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
zde	zde	k6eAd1	zde
čtyři	čtyři	k4xCgFnPc4	čtyři
jaderné	jaderný	k2eAgFnPc4d1	jaderná
elektrárny	elektrárna	k1gFnPc4	elektrárna
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnSc1	jejich
produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
pozastavena	pozastavit	k5eAaPmNgFnS	pozastavit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
referenda	referendum	k1gNnSc2	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
Livorna	Livorno	k1gNnSc2	Livorno
dokonce	dokonce	k9	dokonce
jedna	jeden	k4xCgFnSc1	jeden
geotermická	geotermický	k2eAgFnSc1d1	geotermická
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
převažují	převažovat	k5eAaImIp3nP	převažovat
tepelné	tepelný	k2eAgFnPc1d1	tepelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
i	i	k9	i
asfalt	asfalt	k1gInSc4	asfalt
a	a	k8xC	a
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
Itálie	Itálie	k1gFnSc1	Itálie
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
jsou	být	k5eAaImIp3nP	být
významná	významný	k2eAgNnPc1d1	významné
ložiska	ložisko	k1gNnPc1	ložisko
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
pyritu	pyrit	k1gInSc2	pyrit
<g/>
,	,	kIx,	,
kamenné	kamenný	k2eAgFnSc2d1	kamenná
a	a	k8xC	a
draselné	draselný	k2eAgFnSc2d1	draselná
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
strojírenstvím	strojírenství	k1gNnSc7	strojírenství
<g/>
,	,	kIx,	,
především	především	k9	především
dopravním	dopravní	k2eAgInSc6d1	dopravní
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
FIAT	fiat	k1gInSc4	fiat
Holding	holding	k1gInSc1	holding
(	(	kIx(	(
<g/>
FIAT	fiat	k1gInSc1	fiat
<g/>
,	,	kIx,	,
Iveco	Iveco	k1gMnSc1	Iveco
<g/>
,	,	kIx,	,
Ferrari	Ferrari	k1gMnSc1	Ferrari
<g/>
,	,	kIx,	,
Lancia	Lancia	k1gFnSc1	Lancia
<g/>
,	,	kIx,	,
Alfa	alfa	k1gFnSc1	alfa
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Maserati	Maserat	k1gMnPc1	Maserat
<g/>
)	)	kIx)	)
a	a	k8xC	a
společnosti	společnost	k1gFnPc1	společnost
vyrábějící	vyrábějící	k2eAgInPc4d1	vyrábějící
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
je	být	k5eAaImIp3nS	být
pátá	pátá	k1gFnSc1	pátá
na	na	k7c6	na
světě	svět	k1gInSc6	svět
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
0,777	[number]	k4	0,777
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
BRT	BRT	k?	BRT
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
průmysl	průmysl	k1gInSc4	průmysl
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc1	průmysl
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Hongkongu	Hongkong	k1gInSc6	Hongkong
třetí	třetí	k4xOgMnSc1	třetí
největší	veliký	k2eAgMnSc1d3	veliký
vývozce	vývozce	k1gMnSc1	vývozce
(	(	kIx(	(
<g/>
28	[number]	k4	28
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
sedmý	sedmý	k4xOgMnSc1	sedmý
největší	veliký	k2eAgMnSc1d3	veliký
dovozce	dovozce	k1gMnSc1	dovozce
textilních	textilní	k2eAgInPc2d1	textilní
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
15	[number]	k4	15
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
produkce	produkce	k1gFnSc1	produkce
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
sýrů	sýr	k1gInPc2	sýr
<g/>
,	,	kIx,	,
těstovin	těstovina	k1gFnPc2	těstovina
<g/>
,	,	kIx,	,
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
rybích	rybí	k2eAgInPc2d1	rybí
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
a	a	k8xC	a
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
500	[number]	k4	500
km	km	kA	km
dálnic	dálnice	k1gFnPc2	dálnice
spojujících	spojující	k2eAgFnPc2d1	spojující
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
disponuje	disponovat	k5eAaBmIp3nS	disponovat
s	s	k7c7	s
16	[number]	k4	16
225	[number]	k4	225
km	km	kA	km
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
855	[number]	k4	855
km	km	kA	km
jsou	být	k5eAaImIp3nP	být
tratě	trať	k1gFnPc1	trať
vysokorychlostní	vysokorychlostní	k2eAgFnPc1d1	vysokorychlostní
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
námořní	námořní	k2eAgFnSc4d1	námořní
a	a	k8xC	a
leteckou	letecký	k2eAgFnSc4d1	letecká
flotilu	flotila	k1gFnSc4	flotila
(	(	kIx(	(
<g/>
státní	státní	k2eAgFnPc1d1	státní
aerolinie	aerolinie	k1gFnPc1	aerolinie
Alitalia	Alitalium	k1gNnSc2	Alitalium
jsou	být	k5eAaImIp3nP	být
členem	člen	k1gInSc7	člen
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ČSA	ČSA	kA	ČSA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
námořní	námořní	k2eAgInPc1d1	námořní
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
Janov	Janov	k1gInSc4	Janov
(	(	kIx(	(
<g/>
Genova	Genovo	k1gNnSc2	Genovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Livorno	Livorno	k1gNnSc1	Livorno
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
(	(	kIx(	(
<g/>
Napoli	Napole	k1gFnSc4	Napole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Salerno	Salerna	k1gFnSc5	Salerna
<g/>
,	,	kIx,	,
Reggio	Reggio	k1gMnSc1	Reggio
di	di	k?	di
Calabria	Calabrium	k1gNnSc2	Calabrium
<g/>
,	,	kIx,	,
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
,	,	kIx,	,
Tarent	Tarent	k1gInSc1	Tarent
(	(	kIx(	(
<g/>
Taranto	Taranta	k1gFnSc5	Taranta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bari	Bari	k1gNnSc1	Bari
<g/>
,	,	kIx,	,
Ancona	Ancona	k1gFnSc1	Ancona
a	a	k8xC	a
Terst	Terst	k1gInSc1	Terst
(	(	kIx(	(
<g/>
Trieste	Triest	k1gInSc5	Triest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
letiště	letiště	k1gNnPc1	letiště
jsou	být	k5eAaImIp3nP	být
Řím-Fiumicino-Leonardo	Řím-Fiumicino-Leonardo	k1gNnSc4	Řím-Fiumicino-Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
Milano-Malpensa	Milano-Malpens	k1gMnSc2	Milano-Malpens
a	a	k8xC	a
Palermo-Falcone	Palermo-Falcon	k1gMnSc5	Palermo-Falcon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
přes	přes	k7c4	přes
Tunisko	Tunisko	k1gNnSc4	Tunisko
plynovod	plynovod	k1gInSc4	plynovod
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
turisticky	turisticky	k6eAd1	turisticky
nejnavštěvovanější	navštěvovaný	k2eAgInPc4d3	nejnavštěvovanější
státy	stát	k1gInPc4	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nP	navštívit
mezi	mezi	k7c7	mezi
40	[number]	k4	40
a	a	k8xC	a
105	[number]	k4	105
miliony	milion	k4xCgInPc1	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Turismus	turismus	k1gInSc1	turismus
zde	zde	k6eAd1	zde
přitom	přitom	k6eAd1	přitom
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
12	[number]	k4	12
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Turisty	turist	k1gMnPc4	turist
sem	sem	k6eAd1	sem
lákají	lákat	k5eAaImIp3nP	lákat
antické	antický	k2eAgFnPc1d1	antická
a	a	k8xC	a
středověké	středověký	k2eAgFnPc1d1	středověká
renesanční	renesanční	k2eAgFnPc1d1	renesanční
památky	památka	k1gFnPc1	památka
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
Roma	Rom	k1gMnSc2	Rom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Florencie	Florencie	k1gFnSc1	Florencie
(	(	kIx(	(
<g/>
Firenze	Firenze	k1gFnSc1	Firenze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
(	(	kIx(	(
<g/>
Venezia	Venezia	k1gFnSc1	Venezia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pisa	Pisa	k1gFnSc1	Pisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pláže	pláž	k1gFnPc4	pláž
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
Bibione	Bibion	k1gInSc5	Bibion
<g/>
,	,	kIx,	,
Rimini	Rimin	k2eAgMnPc1d1	Rimin
<g/>
;	;	kIx,	;
Gardské	Gardský	k2eAgNnSc1d1	Gardské
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Cortina	Cortina	k1gFnSc1	Cortina
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ampezzo	Ampezza	k1gFnSc5	Ampezza
<g/>
)	)	kIx)	)
i	i	k9	i
venkov	venkov	k1gInSc1	venkov
(	(	kIx(	(
<g/>
Umbrie	Umbrie	k1gFnSc1	Umbrie
<g/>
,	,	kIx,	,
Toskánsko	Toskánsko	k1gNnSc1	Toskánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lákadly	lákadlo	k1gNnPc7	lákadlo
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
lázně	lázeň	k1gFnPc4	lázeň
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
filmové	filmový	k2eAgInPc4d1	filmový
a	a	k8xC	a
hudební	hudební	k2eAgInPc4d1	hudební
festivaly	festival	k1gInPc4	festival
(	(	kIx(	(
<g/>
Benátky	Benátky	k1gFnPc4	Benátky
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Remo	Remo	k1gMnSc1	Remo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvíce	nejvíce	k6eAd1	nejvíce
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
zapsaných	zapsaný	k2eAgFnPc2d1	zapsaná
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
UNESCO	UNESCO	kA	UNESCO
(	(	kIx(	(
<g/>
47	[number]	k4	47
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
62	[number]	k4	62
milióny	milión	k4xCgInPc1	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
patří	patřit	k5eAaImIp3nS	patřit
Itálie	Itálie	k1gFnSc1	Itálie
k	k	k7c3	k
nejlidnatějším	lidnatý	k2eAgInPc3d3	nejlidnatější
státům	stát	k1gInPc3	stát
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
bývala	bývat	k5eAaImAgFnS	bývat
zemí	zem	k1gFnSc7	zem
masové	masový	k2eAgFnSc2d1	masová
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc6	konec
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vyhledávaných	vyhledávaný	k2eAgInPc2d1	vyhledávaný
cílů	cíl	k1gInPc2	cíl
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
procesu	proces	k1gInSc6	proces
neregulérního	regulérní	k2eNgNnSc2d1	neregulérní
zaměstnávání	zaměstnávání	k1gNnSc2	zaměstnávání
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
týkalo	týkat	k5eAaImAgNnS	týkat
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
až	až	k8xS	až
milionu	milion	k4xCgInSc2	milion
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nejsilněji	silně	k6eAd3	silně
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
přidávali	přidávat	k5eAaImAgMnP	přidávat
skupiny	skupina	k1gFnPc4	skupina
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
italských	italský	k2eAgFnPc2d1	italská
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
/	/	kIx~	/
<g/>
hlavně	hlavně	k9	hlavně
Somálsko	Somálsko	k1gNnSc1	Somálsko
a	a	k8xC	a
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
/	/	kIx~	/
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
světa	svět	k1gInSc2	svět
-	-	kIx~	-
od	od	k7c2	od
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
/	/	kIx~	/
<g/>
např.	např.	kA	např.
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
/	/	kIx~	/
až	až	k9	až
po	po	k7c6	po
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
imigrantů	imigrant	k1gMnPc2	imigrant
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Urbanizace	urbanizace	k1gFnSc1	urbanizace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
67	[number]	k4	67
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
měst	město	k1gNnPc2	město
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
u	u	k7c2	u
deseti	deset	k4xCc2	deset
nejlidnatějších	lidnatý	k2eAgNnPc2d3	nejlidnatější
italských	italský	k2eAgNnPc2d1	italské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
Roma	Rom	k1gMnSc2	Rom
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2	[number]	k4	2
774	[number]	k4	774
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Milán	Milán	k1gInSc1	Milán
(	(	kIx(	(
<g/>
Milano	Milana	k1gFnSc5	Milana
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1	[number]	k4	1
256	[number]	k4	256
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Neapol	Neapol	k1gFnSc1	Neapol
(	(	kIx(	(
<g/>
Napoli	Napole	k1gFnSc4	Napole
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1	[number]	k4	1
004	[number]	k4	004
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Turín	Turín	k1gInSc1	Turín
(	(	kIx(	(
<g/>
Torino	Torino	k1gNnSc1	Torino
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
903	[number]	k4	903
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Palermo	Palermo	k1gNnSc1	Palermo
(	(	kIx(	(
<g/>
687	[number]	k4	687
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Janov	Janov	k1gInSc1	Janov
(	(	kIx(	(
<g/>
Genova	Genův	k2eAgFnSc1d1	Genova
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
610	[number]	k4	610
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Bologna	Bologna	k1gFnSc1	Bologna
(	(	kIx(	(
<g/>
374	[number]	k4	374
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Florencie	Florencie	k1gFnSc1	Florencie
(	(	kIx(	(
<g/>
Firenze	Firenze	k1gFnSc1	Firenze
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
356	[number]	k4	356
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Bari	Bari	k1gNnSc1	Bari
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
317	[number]	k4	317
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Catania	Catanium	k1gNnPc1	Catanium
(	(	kIx(	(
<g/>
313	[number]	k4	313
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Benátky	Benátky	k1gFnPc1	Benátky
(	(	kIx(	(
<g/>
Venezia	Venezia	k1gFnSc1	Venezia
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
272	[number]	k4	272
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Převažující	převažující	k2eAgFnSc1d1	převažující
národnost	národnost	k1gFnSc1	národnost
je	být	k5eAaImIp3nS	být
italská	italský	k2eAgFnSc1d1	italská
(	(	kIx(	(
<g/>
94	[number]	k4	94
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
někteří	některý	k3yIgMnPc1	některý
Italové	Ital	k1gMnPc1	Ital
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
národa	národ	k1gInSc2	národ
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
jiný	jiný	k2eAgInSc4d1	jiný
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
Sardů	Sard	k1gMnPc2	Sard
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
,	,	kIx,	,
hovořících	hovořící	k2eAgFnPc6d1	hovořící
sardsky	sardsky	k6eAd1	sardsky
<g/>
,	,	kIx,	,
Rétorománů	Rétoromán	k1gMnPc2	Rétoromán
u	u	k7c2	u
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
hranic	hranice	k1gFnPc2	hranice
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
,	,	kIx,	,
hovořících	hovořící	k2eAgFnPc6d1	hovořící
rétorománsky	rétorománsky	k6eAd1	rétorománsky
<g/>
,	,	kIx,	,
Tyrolanů	Tyrolan	k1gMnPc2	Tyrolan
na	na	k7c6	na
severu	sever	k1gInSc6	sever
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
,	,	kIx,	,
hovořících	hovořící	k2eAgFnPc6d1	hovořící
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
a	a	k8xC	a
Provensálců	Provensálec	k1gMnPc2	Provensálec
na	na	k7c6	na
západě	západ	k1gInSc6	západ
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
hovořících	hovořící	k2eAgFnPc6d1	hovořící
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
pocházejí	pocházet	k5eAaImIp3nP	pocházet
především	především	k9	především
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc2	Libye
<g/>
,	,	kIx,	,
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc2	Etiopie
a	a	k8xC	a
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
arabským	arabský	k2eAgInSc7d1	arabský
jarem	jar	k1gInSc7	jar
a	a	k8xC	a
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
je	být	k5eAaImIp3nS	být
Itálie	Itálie	k1gFnSc1	Itálie
vystavena	vystavit	k5eAaPmNgFnS	vystavit
náporu	nápor	k1gInSc3	nápor
všemožných	všemožný	k2eAgMnPc2d1	všemožný
migrantů	migrant	k1gMnPc2	migrant
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
snaží	snažit	k5eAaImIp3nP	snažit
dostat	dostat	k5eAaPmF	dostat
přes	přes	k7c4	přes
středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
katolické	katolický	k2eAgNnSc1d1	katolické
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
(	(	kIx(	(
<g/>
85	[number]	k4	85
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
historií	historie	k1gFnSc7	historie
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
Vatikán	Vatikán	k1gInSc1	Vatikán
a	a	k8xC	a
osobnost	osobnost	k1gFnSc1	osobnost
papeže	papež	k1gMnSc2	papež
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Argentinec	Argentinec	k1gMnSc1	Argentinec
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
František	František	k1gMnSc1	František
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
ateisté	ateista	k1gMnPc1	ateista
nebo	nebo	k8xC	nebo
valdenští	valdenský	k2eAgMnPc1d1	valdenský
evangelíci	evangelík	k1gMnPc1	evangelík
a	a	k8xC	a
jen	jen	k6eAd1	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
tvořená	tvořený	k2eAgFnSc1d1	tvořená
především	především	k9	především
přistěhovalci	přistěhovalec	k1gMnSc3	přistěhovalec
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
-	-	kIx~	-
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
Capodanno	Capodanno	k6eAd1	Capodanno
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
-	-	kIx~	-
Tři	tři	k4xCgFnPc1	tři
králové	králová	k1gFnPc1	králová
(	(	kIx(	(
<g/>
Epifania	Epifanium	k1gNnPc1	Epifanium
<g/>
)	)	kIx)	)
Pasquetta	Pasquetta	k1gFnSc1	Pasquetta
-	-	kIx~	-
Velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
pondělí	pondělí	k1gNnSc1	pondělí
(	(	kIx(	(
<g/>
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
<g />
.	.	kIx.	.
</s>
<s>
svátek	svátek	k1gInSc1	svátek
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
-	-	kIx~	-
Den	den	k1gInSc1	den
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
Festa	Festa	k?	Festa
della	della	k6eAd1	della
Liberazione	Liberazion	k1gInSc5	Liberazion
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
-	-	kIx~	-
Svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
Festa	Festa	k?	Festa
del	del	k?	del
Lavoro	Lavora	k1gFnSc5	Lavora
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
-	-	kIx~	-
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Ferragosto	Ferragosta	k1gMnSc5	Ferragosta
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
-	-	kIx~	-
Svátek	svátek	k1gInSc1	svátek
<g />
.	.	kIx.	.
</s>
<s>
všech	všecek	k3xTgMnPc2	všecek
svatých	svatý	k1gMnPc2	svatý
(	(	kIx(	(
<g/>
Ognissanti	Ognissant	k1gMnPc1	Ognissant
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
-	-	kIx~	-
Neposkvrněné	poskvrněný	k2eNgNnSc1d1	neposkvrněné
početí	početí	k1gNnSc1	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Immacolata	Immacole	k1gNnPc1	Immacole
Concezione	Concezion	k1gInSc5	Concezion
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
-	-	kIx~	-
Narození	narození	k1gNnSc1	narození
páně	páně	k2eAgNnSc1d1	páně
(	(	kIx(	(
<g/>
Natale	Natal	k1gInSc5	Natal
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
-	-	kIx~	-
Svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Štěpána	Štěpána	k1gFnSc1	Štěpána
(	(	kIx(	(
<g/>
Santo	Santo	k1gNnSc1	Santo
Stefano	Stefana	k1gFnSc5	Stefana
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Italská	italský	k2eAgFnSc1d1	italská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
