<s>
Národní	národní	k2eAgInSc1d1	národní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
NASA	NASA	kA	NASA
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
National	National	k1gMnSc1	National
Aeronautics	Aeronauticsa	k1gFnPc2	Aeronauticsa
and	and	k?	and
Space	Spaec	k1gInSc2	Spaec
Administration	Administration	k1gInSc1	Administration
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
vládní	vládní	k2eAgFnSc1d1	vládní
agentura	agentura	k1gFnSc1	agentura
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
americký	americký	k2eAgInSc4d1	americký
kosmický	kosmický	k2eAgInSc4d1	kosmický
program	program	k1gInSc4	program
a	a	k8xC	a
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
