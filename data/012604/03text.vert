<p>
<s>
Alioramus	Alioramus	k1gInSc1	Alioramus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jiná	jiný	k2eAgFnSc1d1	jiná
větev	větev	k1gFnSc1	větev
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
vyhynulého	vyhynulý	k2eAgMnSc2d1	vyhynulý
masožravého	masožravý	k2eAgMnSc2d1	masožravý
tyranosaurida	tyranosaurid	k1gMnSc2	tyranosaurid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Časově	časově	k6eAd1	časově
se	se	k3xPyFc4	se
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
do	do	k7c2	do
období	období	k1gNnSc2	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
křídy	křída	k1gFnSc2	křída
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
70	[number]	k4	70
až	až	k8xS	až
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tyranosaura	tyranosaur	k1gMnSc2	tyranosaur
se	se	k3xPyFc4	se
Alioramus	Alioramus	k1gMnSc1	Alioramus
vzhledově	vzhledově	k6eAd1	vzhledově
lišil	lišit	k5eAaImAgMnS	lišit
tvarem	tvar	k1gInSc7	tvar
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
prodloužený	prodloužený	k2eAgInSc4d1	prodloužený
čenich	čenich	k1gInSc4	čenich
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
čelisti	čelist	k1gFnPc1	čelist
byly	být	k5eAaImAgFnP	být
slabší	slabý	k2eAgFnPc1d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nozdrami	nozdra	k1gFnPc7	nozdra
a	a	k8xC	a
očima	oko	k1gNnPc7	oko
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
několik	několik	k4yIc1	několik
kostěných	kostěný	k2eAgMnPc2d1	kostěný
výrůstků	výrůstek	k1gMnPc2	výrůstek
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
snad	snad	k9	snad
používal	používat	k5eAaImAgInS	používat
během	během	k7c2	během
námluv	námluva	k1gFnPc2	námluva
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
je	on	k3xPp3gNnSc4	on
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
samci	samec	k1gMnSc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Alioramus	Alioramus	k1gMnSc1	Alioramus
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
aktivní	aktivní	k2eAgMnSc1d1	aktivní
lovec	lovec	k1gMnSc1	lovec
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
však	však	k9	však
živit	živit	k5eAaImF	živit
také	také	k9	také
mršinami	mršina	k1gFnPc7	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
dinosaura	dinosaurus	k1gMnSc4	dinosaurus
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
objev	objev	k1gInSc1	objev
nového	nový	k2eAgInSc2d1	nový
druhu	druh	k1gInSc2	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
A.	A.	kA	A.
altai	alta	k1gInPc7	alta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
podle	podle	k7c2	podle
kompletnějšího	kompletní	k2eAgInSc2d2	kompletnější
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
lehkého	lehký	k2eAgMnSc4d1	lehký
(	(	kIx(	(
<g/>
asi	asi	k9	asi
370	[number]	k4	370
kg	kg	kA	kg
vážícího	vážící	k2eAgInSc2d1	vážící
<g/>
)	)	kIx)	)
tyranosaurida	tyranosaurida	k1gFnSc1	tyranosaurida
s	s	k7c7	s
"	"	kIx"	"
<g/>
rohatou	rohatý	k2eAgFnSc7d1	rohatá
<g/>
"	"	kIx"	"
štíhlou	štíhlý	k2eAgFnSc7d1	štíhlá
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
rychlým	rychlý	k2eAgMnSc7d1	rychlý
běžcem	běžec	k1gMnSc7	běžec
<g/>
,	,	kIx,	,
žijícím	žijící	k2eAgMnSc7d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fosílie	fosílie	k1gFnSc1	fosílie
nového	nový	k2eAgInSc2d1	nový
druhu	druh	k1gInSc2	druh
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
odborníků	odborník	k1gMnPc2	odborník
je	být	k5eAaImIp3nS	být
však	však	k9	však
A.	A.	kA	A.
altus	altus	k1gMnSc1	altus
stejným	stejný	k2eAgMnSc7d1	stejný
druhem	druh	k1gMnSc7	druh
jako	jako	k8xS	jako
A.	A.	kA	A.
remotus	remotus	k1gInSc1	remotus
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
možná	možná	k9	možná
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nedospělý	dospělý	k2eNgInSc4d1	nedospělý
exemplář	exemplář	k1gInSc4	exemplář
rodu	rod	k1gInSc2	rod
Tarbosaurus	Tarbosaurus	k1gInSc1	Tarbosaurus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
druhu	druh	k1gInSc2	druh
A.	A.	kA	A.
altai	altai	k6eAd1	altai
na	na	k7c6	na
webu	web	k1gInSc6	web
DinosaurusBlog	DinosaurusBloga	k1gFnPc2	DinosaurusBloga
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
alioramovi	alioram	k1gMnSc6	alioram
na	na	k7c6	na
webu	web	k1gInSc6	web
DinosaurHome	DinosaurHom	k1gInSc5	DinosaurHom
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOCHA	Socha	k1gMnSc1	Socha
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Alioramus	Alioramus	k1gMnSc1	Alioramus
remotus	remotus	k1gMnSc1	remotus
<g/>
.	.	kIx.	.
</s>
<s>
OSEL	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
