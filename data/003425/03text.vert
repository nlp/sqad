<s>
Pštros	pštros	k1gMnSc1	pštros
dvouprstý	dvouprstý	k2eAgMnSc1d1	dvouprstý
(	(	kIx(	(
<g/>
Struthio	Struthio	k1gMnSc1	Struthio
camelus	camelus	k1gMnSc1	camelus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
nelétavý	létavý	k2eNgMnSc1d1	nelétavý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
žijícím	žijící	k2eAgMnSc7d1	žijící
představitelem	představitel	k1gMnSc7	představitel
čeledi	čeleď	k1gFnSc2	čeleď
pštrosovitých	pštrosovitý	k2eAgNnPc2d1	pštrosovitý
(	(	kIx(	(
<g/>
Struthionidae	Struthionidae	k1gNnPc2	Struthionidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Struthio	Struthio	k1gMnSc1	Struthio
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
krkem	krk	k1gInSc7	krk
a	a	k8xC	a
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
rychlosti	rychlost	k1gFnSc2	rychlost
70	[number]	k4	70
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
jsou	být	k5eAaImIp3nP	být
největšími	veliký	k2eAgMnPc7d3	veliký
žijícími	žijící	k2eAgMnPc7d1	žijící
ptáky	pták	k1gMnPc7	pták
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
chováni	chován	k2eAgMnPc1d1	chován
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
pštrosa	pštros	k1gMnSc2	pštros
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
vrabce	vrabec	k1gMnSc4	vrabec
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
velblouda	velbloud	k1gMnSc2	velbloud
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
struthio	struthio	k1gMnSc1	struthio
a	a	k8xC	a
camel	camel	k1gMnSc1	camel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
běžců	běžec	k1gMnPc2	běžec
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
plně	plně	k6eAd1	plně
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
běžci	běžec	k1gMnPc1	běžec
měli	mít	k5eAaImAgMnP	mít
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
létal	létat	k5eAaImAgMnS	létat
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
ztratili	ztratit	k5eAaPmAgMnP	ztratit
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
rychlými	rychlý	k2eAgMnPc7d1	rychlý
běžci	běžec	k1gMnPc7	běžec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
utéci	utéct	k5eAaPmF	utéct
před	před	k7c7	před
nepřítelem	nepřítel	k1gMnSc7	nepřítel
a	a	k8xC	a
putovat	putovat	k5eAaImF	putovat
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
přímého	přímý	k2eAgMnSc2d1	přímý
zástupce	zástupce	k1gMnSc2	zástupce
považují	považovat	k5eAaImIp3nP	považovat
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
známého	známý	k1gMnSc2	známý
prapštrosa	prapštrosa	k1gFnSc1	prapštrosa
Palaeotis	Palaeotis	k1gFnSc1	Palaeotis
weigelti	weigelť	k1gFnSc2	weigelť
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
fosílie	fosílie	k1gFnSc1	fosílie
z	z	k7c2	z
eocénu	eocén	k1gInSc2	eocén
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
např.	např.	kA	např.
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
Messelském	Messelský	k2eAgInSc6d1	Messelský
lomu	lom	k1gInSc6	lom
a	a	k8xC	a
v	v	k7c6	v
Geiseltalu	Geiseltal	k1gMnSc6	Geiseltal
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zvíře	zvíře	k1gNnSc1	zvíře
však	však	k9	však
po	po	k7c6	po
nových	nový	k2eAgInPc6d1	nový
výzkumech	výzkum	k1gInPc6	výzkum
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
podobností	podobnost	k1gFnSc7	podobnost
s	s	k7c7	s
pštrosem	pštros	k1gMnSc7	pštros
nandu	nandu	k1gMnSc7	nandu
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
spíše	spíše	k9	spíše
slušel	slušet	k5eAaImAgInS	slušet
název	název	k1gInSc1	název
pranandu	pranand	k1gInSc2	pranand
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInSc4d1	patřící
nepochybně	pochybně	k6eNd1	pochybně
ke	k	k7c3	k
pštrosům	pštros	k1gMnPc3	pštros
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
doloženi	doložit	k5eAaPmNgMnP	doložit
od	od	k7c2	od
miocénu	miocén	k1gInSc2	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obří	obří	k2eAgMnPc4d1	obří
ptáky	pták	k1gMnPc4	pták
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
okolo	okolo	k7c2	okolo
4	[number]	k4	4
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
12	[number]	k4	12
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
výška	výška	k1gFnSc1	výška
postupně	postupně	k6eAd1	postupně
snižovala	snižovat	k5eAaImAgFnS	snižovat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
asi	asi	k9	asi
před	před	k7c7	před
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
již	již	k6eAd1	již
podobali	podobat	k5eAaImAgMnP	podobat
dnešním	dnešní	k2eAgMnPc3d1	dnešní
pštrosům	pštros	k1gMnPc3	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
známým	známý	k2eAgInSc7d1	známý
druhem	druh	k1gInSc7	druh
je	on	k3xPp3gNnSc4	on
Struthio	Struthio	k1gNnSc4	Struthio
orlovi	orlův	k2eAgMnPc1d1	orlův
<g/>
,	,	kIx,	,
nalezený	nalezený	k2eAgInSc4d1	nalezený
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
žilo	žít	k5eAaImAgNnS	žít
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
a	a	k8xC	a
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
Struthio	Struthio	k6eAd1	Struthio
chersonensis	chersonensis	k1gFnSc1	chersonensis
<g/>
,	,	kIx,	,
Struthio	Struthio	k1gMnSc1	Struthio
mongolicus	mongolicus	k1gMnSc1	mongolicus
<g/>
,	,	kIx,	,
Struthio	Struthio	k6eAd1	Struthio
wimani	wiman	k1gMnPc1	wiman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asijský	asijský	k2eAgMnSc1d1	asijský
pštros	pštros	k1gMnSc1	pštros
(	(	kIx(	(
<g/>
Struthio	Struthio	k1gMnSc1	Struthio
asiaticus	asiaticus	k1gMnSc1	asiaticus
<g/>
)	)	kIx)	)
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
centrální	centrální	k2eAgFnSc2d1	centrální
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zatím	zatím	k6eAd1	zatím
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
vymizeli	vymizet	k5eAaPmAgMnP	vymizet
postupně	postupně	k6eAd1	postupně
pštrosi	pštros	k1gMnPc1	pštros
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pliocénu	pliocén	k1gInSc6	pliocén
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
dnes	dnes	k6eAd1	dnes
žijící	žijící	k2eAgMnSc1d1	žijící
pštros	pštros	k1gMnSc1	pštros
dvouprstý	dvouprstý	k2eAgMnSc1d1	dvouprstý
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
rozšíření	rozšíření	k1gNnSc4	rozšíření
za	za	k7c2	za
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
sahalo	sahat	k5eAaImAgNnS	sahat
až	až	k9	až
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupujícími	postupující	k2eAgFnPc7d1	postupující
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
proměnily	proměnit	k5eAaPmAgFnP	proměnit
zelenou	zelený	k2eAgFnSc4d1	zelená
Saharu	Sahara	k1gFnSc4	Sahara
v	v	k7c4	v
poušť	poušť	k1gFnSc4	poušť
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
pštrosi	pštros	k1gMnPc1	pštros
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
i	i	k8xC	i
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
jejich	jejich	k3xOp3gFnSc2	jejich
populace	populace	k1gFnSc2	populace
dnes	dnes	k6eAd1	dnes
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
obvykle	obvykle	k6eAd1	obvykle
váží	vážit	k5eAaImIp3nP	vážit
mezi	mezi	k7c7	mezi
90	[number]	k4	90
a	a	k8xC	a
130	[number]	k4	130
kg	kg	kA	kg
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
zaznamenáni	zaznamenat	k5eAaPmNgMnP	zaznamenat
i	i	k9	i
samci	samec	k1gMnPc1	samec
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
až	až	k9	až
155	[number]	k4	155
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Pera	pero	k1gNnPc1	pero
samců	samec	k1gMnPc2	samec
jsou	být	k5eAaImIp3nP	být
černá	černé	k1gNnPc4	černé
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
bílých	bílý	k2eAgNnPc2d1	bílé
per	pero	k1gNnPc2	pero
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
peří	peří	k1gNnSc4	peří
šedohnědé	šedohnědý	k2eAgNnSc4d1	šedohnědé
s	s	k7c7	s
pár	pár	k4xCyI	pár
bílými	bílé	k1gNnPc7	bílé
pery	pero	k1gNnPc7	pero
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
zakrnělá	zakrnělý	k2eAgFnSc1d1	zakrnělá
křídla	křídlo	k1gNnPc4	křídlo
samci	samec	k1gMnSc3	samec
při	při	k7c6	při
tokání	tokání	k1gNnSc6	tokání
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
na	na	k7c4	na
odiv	odiv	k1gInSc4	odiv
před	před	k7c7	před
samicemi	samice	k1gFnPc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
jako	jako	k8xS	jako
slunečník	slunečník	k1gInSc1	slunečník
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Pera	pero	k1gNnPc1	pero
jsou	být	k5eAaImIp3nP	být
měkká	měkký	k2eAgNnPc1d1	měkké
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
odlišná	odlišný	k2eAgNnPc1d1	odlišné
od	od	k7c2	od
tuhých	tuhý	k2eAgNnPc2d1	tuhé
per	pero	k1gNnPc2	pero
létavých	létavý	k2eAgMnPc2d1	létavý
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
u	u	k7c2	u
pštrosích	pštrosí	k2eAgNnPc2d1	pštrosí
per	pero	k1gNnPc2	pero
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
paprsky	paprsek	k1gInPc4	paprsek
vybíhající	vybíhající	k2eAgInPc4d1	vybíhající
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
z	z	k7c2	z
osy	osa	k1gFnSc2	osa
pera	pero	k1gNnSc2	pero
zvané	zvaný	k2eAgNnSc1d1	zvané
osten	osten	k1gInSc4	osten
zcela	zcela	k6eAd1	zcela
volné	volný	k2eAgNnSc4d1	volné
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
spojujících	spojující	k2eAgInPc2d1	spojující
háčků	háček	k1gInPc2	háček
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
pštrosí	pštrosí	k2eAgNnSc1d1	pštrosí
peří	peří	k1gNnSc1	peří
působí	působit	k5eAaImIp3nS	působit
nadýchaným	nadýchaný	k2eAgInSc7d1	nadýchaný
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgFnPc1d1	silná
nohy	noha	k1gFnPc1	noha
pštrosů	pštros	k1gMnPc2	pštros
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
prstech	prst	k1gInPc6	prst
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
větší	veliký	k2eAgInSc1d2	veliký
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prst	prst	k1gInSc1	prst
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
kopytu	kopyto	k1gNnSc3	kopyto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
adaptace	adaptace	k1gFnSc1	adaptace
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlý	rychlý	k2eAgInSc1d1	rychlý
běh	běh	k1gInSc1	běh
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
života	život	k1gInSc2	život
vyvinout	vyvinout	k5eAaPmF	vyvinout
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
70	[number]	k4	70
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prst	prst	k1gInSc1	prst
zároveň	zároveň	k6eAd1	zároveň
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pštros	pštros	k1gMnSc1	pštros
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nepřátelům	nepřítel	k1gMnPc3	nepřítel
brání	bránit	k5eAaImIp3nS	bránit
také	také	k9	také
kopáním	kopání	k1gNnSc7	kopání
<g/>
.	.	kIx.	.
</s>
<s>
Pštros	pštros	k1gMnSc1	pštros
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
s	s	k7c7	s
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
krkem	krk	k1gInSc7	krk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
přidušeného	přidušený	k2eAgInSc2d1	přidušený
křiku	křik	k1gInSc2	křik
se	se	k3xPyFc4	se
holý	holý	k2eAgInSc1d1	holý
krk	krk	k1gInSc1	krk
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
nafukuje	nafukovat	k5eAaImIp3nS	nafukovat
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
pštrosů	pštros	k1gMnPc2	pštros
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
silnými	silný	k2eAgFnPc7d1	silná
černými	černý	k2eAgFnPc7d1	černá
řasami	řasa	k1gFnPc7	řasa
(	(	kIx(	(
<g/>
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
prachu	prach	k1gInSc3	prach
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgNnPc4d3	veliký
oči	oko	k1gNnPc4	oko
všech	všecek	k3xTgMnPc2	všecek
žijících	žijící	k2eAgMnPc2d1	žijící
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
tvorů	tvor	k1gMnPc2	tvor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
prachem	prach	k1gInSc7	prach
a	a	k8xC	a
pískem	písek	k1gInSc7	písek
slouží	sloužit	k5eAaImIp3nP	sloužit
pštrosům	pštros	k1gMnPc3	pštros
také	také	k9	také
třetí	třetí	k4xOgNnSc4	třetí
víčko	víčko	k1gNnSc4	víčko
–	–	k?	–
mžurka	mžurka	k1gFnSc1	mžurka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zavírá	zavírat	k5eAaImIp3nS	zavírat
horizontálně	horizontálně	k6eAd1	horizontálně
od	od	k7c2	od
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
očního	oční	k2eAgInSc2d1	oční
koutku	koutek	k1gInSc2	koutek
k	k	k7c3	k
vnějšímu	vnější	k2eAgNnSc3d1	vnější
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
i	i	k9	i
široce	široko	k6eAd1	široko
otevřené	otevřený	k2eAgNnSc1d1	otevřené
ústí	ústí	k1gNnSc1	ústí
sluchovodu	sluchovod	k1gInSc2	sluchovod
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
směrem	směr	k1gInSc7	směr
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nS	peřit
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gNnSc2	on
totiž	totiž	k9	totiž
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tzv.	tzv.	kA	tzv.
příuší	příušit	k5eAaPmIp3nS	příušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
sexuální	sexuální	k2eAgFnSc2d1	sexuální
zralosti	zralost	k1gFnSc2	zralost
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
samci	samec	k1gMnPc1	samec
dosahovat	dosahovat	k5eAaImF	dosahovat
výšky	výška	k1gFnPc4	výška
od	od	k7c2	od
1,8	[number]	k4	1,8
do	do	k7c2	do
2,7	[number]	k4	2,7
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
od	od	k7c2	od
1,7	[number]	k4	1,7
do	do	k7c2	do
2	[number]	k4	2
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
mláďata	mládě	k1gNnPc4	mládě
rostou	růst	k5eAaImIp3nP	růst
o	o	k7c4	o
25	[number]	k4	25
cm	cm	kA	cm
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
váží	vážit	k5eAaImIp3nS	vážit
mladý	mladý	k2eAgMnSc1d1	mladý
pštros	pštros	k1gMnSc1	pštros
okolo	okolo	k7c2	okolo
45	[number]	k4	45
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dožívat	dožívat	k5eAaImF	dožívat
až	až	k9	až
75	[number]	k4	75
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
savanách	savana	k1gFnPc6	savana
a	a	k8xC	a
Sahelu	Sahel	k1gInSc6	Sahel
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
i	i	k9	i
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
zóny	zóna	k1gFnSc2	zóna
rovníkových	rovníkový	k2eAgInPc2d1	rovníkový
pralesů	prales	k1gInPc2	prales
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
řádu	řád	k1gInSc3	řád
Struthioniformes	Struthioniformesa	k1gFnPc2	Struthioniformesa
(	(	kIx(	(
<g/>
Afričtí	africký	k2eAgMnPc1d1	africký
pštrosi	pštros	k1gMnPc1	pštros
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
členy	člen	k1gMnPc4	člen
nadřádu	nadřád	k1gInSc2	nadřád
běžců	běžec	k1gMnPc2	běžec
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nP	patřit
nanduové	nandu	k1gMnPc1	nandu
<g/>
,	,	kIx,	,
kasuárové	kasuár	k1gMnPc1	kasuár
<g/>
,	,	kIx,	,
pštrosi	pštros	k1gMnPc1	pštros
emu	emu	k1gMnSc1	emu
<g/>
,	,	kIx,	,
kiviové	kivi	k1gMnPc1	kivi
a	a	k8xC	a
již	již	k6eAd1	již
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
největší	veliký	k2eAgMnPc1d3	veliký
ptáci	pták	k1gMnPc1	pták
světa	svět	k1gInSc2	svět
-	-	kIx~	-
běžci	běžec	k1gMnPc1	běžec
rodu	rod	k1gInSc2	rod
Aepyornithes	Aepyornithes	k1gInSc1	Aepyornithes
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
následujících	následující	k2eAgInPc2d1	následující
pět	pět	k4xCc4	pět
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
S.	S.	kA	S.
<g/>
c.	c.	k?	c.
australis	australis	k1gInSc1	australis
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
S.	S.	kA	S.
<g/>
c.	c.	k?	c.
camelus	camelus	k1gMnSc1	camelus
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
také	také	k9	také
nazýván	nazýván	k2eAgMnSc1d1	nazýván
severoafrický	severoafrický	k2eAgMnSc1d1	severoafrický
pštros	pštros	k1gMnSc1	pštros
nebo	nebo	k8xC	nebo
červenokrký	červenokrký	k2eAgMnSc1d1	červenokrký
pštros	pštros	k1gMnSc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
<g/>
c.	c.	k?	c.
masaicus	masaicus	k1gMnSc1	masaicus
z	z	k7c2	z
Východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
masajský	masajský	k2eAgMnSc1d1	masajský
pštros	pštros	k1gMnSc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
samcům	samec	k1gInPc3	samec
změní	změnit	k5eAaPmIp3nS	změnit
krk	krk	k1gInSc1	krk
a	a	k8xC	a
stehna	stehno	k1gNnSc2	stehno
svou	svůj	k3xOyFgFnSc4	svůj
barvu	barva	k1gFnSc4	barva
na	na	k7c4	na
růžovo-oranžovou	růžovoranžový	k2eAgFnSc4d1	růžovo-oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
jeho	jeho	k3xOp3gInSc2	jeho
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Etiopie	Etiopie	k1gFnSc2	Etiopie
po	po	k7c6	po
Keňu	keňu	k1gNnSc6	keňu
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
<g/>
c.	c.	k?	c.
molypdophanes	molypdophanes	k1gInSc1	molypdophanes
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
,	,	kIx,	,
Etiopii	Etiopie	k1gFnSc6	Etiopie
a	a	k8xC	a
severní	severní	k2eAgFnSc3d1	severní
Keni	Keňa	k1gFnSc3	Keňa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
somálský	somálský	k2eAgMnSc1d1	somálský
pštros	pštros	k1gMnSc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
samcům	samec	k1gInPc3	samec
změní	změnit	k5eAaPmIp3nS	změnit
krk	krk	k1gInSc1	krk
a	a	k8xC	a
stehna	stehno	k1gNnSc2	stehno
svou	svůj	k3xOyFgFnSc4	svůj
barvu	barva	k1gFnSc4	barva
na	na	k7c4	na
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozšíření	rozšíření	k1gNnSc1	rozšíření
se	se	k3xPyFc4	se
překrývá	překrývat	k5eAaImIp3nS	překrývat
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Keni	Keňa	k1gFnSc6	Keňa
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
S.	S.	kA	S.
<g/>
c.	c.	k?	c.
masaicus	masaicus	k1gInSc1	masaicus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
přírodovědecké	přírodovědecký	k2eAgFnPc1d1	Přírodovědecká
autority	autorita	k1gFnPc1	autorita
považují	považovat	k5eAaImIp3nP	považovat
somálského	somálský	k2eAgMnSc4d1	somálský
pštrosa	pštros	k1gMnSc4	pštros
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
<g/>
c.	c.	k?	c.
syriacus	syriacus	k1gMnSc1	syriacus
ze	z	k7c2	z
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
také	také	k9	také
jako	jako	k9	jako
arabský	arabský	k2eAgMnSc1d1	arabský
pštros	pštros	k1gMnSc1	pštros
nebo	nebo	k8xC	nebo
Středovýchodní	středovýchodní	k2eAgMnSc1d1	středovýchodní
pštros	pštros	k1gMnSc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poddruh	poddruh	k1gInSc1	poddruh
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hojný	hojný	k2eAgMnSc1d1	hojný
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vyhynulého	vyhynulý	k2eAgMnSc4d1	vyhynulý
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc7	jejich
zvykem	zvyk	k1gInSc7	zvyk
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
strčit	strčit	k5eAaPmF	strčit
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Pštros	pštros	k1gMnSc1	pštros
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
společenský	společenský	k2eAgMnSc1d1	společenský
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
kočujících	kočující	k2eAgFnPc6d1	kočující
skupinách	skupina	k1gFnPc6	skupina
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
vládne	vládnout	k5eAaImIp3nS	vládnout
přísná	přísný	k2eAgFnSc1d1	přísná
hierarchie	hierarchie	k1gFnSc1	hierarchie
–	–	k?	–
dominantní	dominantní	k2eAgMnPc1d1	dominantní
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
řídí	řídit	k5eAaImIp3nS	řídit
přesuny	přesun	k1gInPc4	přesun
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
prachové	prachový	k2eAgFnPc1d1	prachová
koupele	koupel	k1gFnPc1	koupel
<g/>
,	,	kIx,	,
tok	tok	k1gInSc1	tok
atd.	atd.	kA	atd.
</s>
<s>
Na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
jsou	být	k5eAaImIp3nP	být
pštrosi	pštros	k1gMnPc1	pštros
za	za	k7c4	za
den	den	k1gInSc4	den
schopni	schopen	k2eAgMnPc1d1	schopen
urazit	urazit	k5eAaPmF	urazit
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Putování	putování	k1gNnPc1	putování
přerušují	přerušovat	k5eAaImIp3nP	přerušovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
dravci	dravec	k1gMnPc7	dravec
se	se	k3xPyFc4	se
pštrosi	pštros	k1gMnPc1	pštros
přidružují	přidružovat	k5eAaImIp3nP	přidružovat
k	k	k7c3	k
některým	některý	k3yIgMnPc3	některý
býložravcům	býložravec	k1gMnPc3	býložravec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
zebry	zebra	k1gFnSc2	zebra
<g/>
,	,	kIx,	,
gazely	gazela	k1gFnSc2	gazela
a	a	k8xC	a
antilopy	antilopa	k1gFnSc2	antilopa
<g/>
.	.	kIx.	.
</s>
<s>
Udržují	udržovat	k5eAaImIp3nP	udržovat
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
vztahy	vztah	k1gInPc1	vztah
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
při	při	k7c6	při
hlídání	hlídání	k1gNnSc6	hlídání
okolí	okolí	k1gNnSc2	okolí
před	před	k7c7	před
možným	možný	k2eAgNnSc7d1	možné
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosy	pštros	k1gMnPc4	pštros
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
jejich	jejich	k3xOp3gFnSc1	jejich
výška	výška	k1gFnSc1	výška
a	a	k8xC	a
vynikající	vynikající	k2eAgInSc1d1	vynikající
zrak	zrak	k1gInSc1	zrak
a	a	k8xC	a
sluch	sluch	k1gInSc1	sluch
do	do	k7c2	do
role	role	k1gFnSc2	role
hlídek	hlídka	k1gFnPc2	hlídka
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
neunikne	uniknout	k5eNaPmIp3nS	uniknout
žádná	žádný	k3yNgFnSc1	žádný
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
býložravců	býložravec	k1gMnPc2	býložravec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
spatří	spatřit	k5eAaPmIp3nP	spatřit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
oni	onen	k3xDgMnPc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
potravou	potrava	k1gFnSc7	potrava
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
musí	muset	k5eAaImIp3nS	muset
zkonzumovat	zkonzumovat	k5eAaPmF	zkonzumovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
nízké	nízký	k2eAgFnSc3d1	nízká
energetické	energetický	k2eAgFnSc3d1	energetická
hodnotě	hodnota	k1gFnSc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
nejen	nejen	k6eAd1	nejen
listy	list	k1gInPc4	list
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jejich	jejich	k3xOp3gNnPc4	jejich
zrna	zrno	k1gNnPc4	zrno
<g/>
,	,	kIx,	,
poupata	poupě	k1gNnPc4	poupě
<g/>
,	,	kIx,	,
pupeny	pupen	k1gInPc4	pupen
<g/>
,	,	kIx,	,
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
plody	plod	k1gInPc4	plod
a	a	k8xC	a
také	také	k9	také
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
každý	každý	k3xTgMnSc1	každý
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
i	i	k8xC	i
pštros	pštros	k1gMnSc1	pštros
si	se	k3xPyFc3	se
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
její	její	k3xOp3gNnSc4	její
rozmělnění	rozmělnění	k1gNnSc4	rozmělnění
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
laloky	lalok	k1gInPc4	lalok
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
polykání	polykání	k1gNnSc2	polykání
kamínků	kamínek	k1gInPc2	kamínek
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
takto	takto	k6eAd1	takto
spolyká	spolykat	k5eAaPmIp3nS	spolykat
několik	několik	k4yIc4	několik
hrstí	hrst	k1gFnPc2	hrst
oblázků	oblázek	k1gInPc2	oblázek
a	a	k8xC	a
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pštros	pštros	k1gMnSc1	pštros
je	být	k5eAaImIp3nS	být
všežravec	všežravec	k1gMnSc1	všežravec
<g/>
,	,	kIx,	,
zpestřuje	zpestřovat	k5eAaImIp3nS	zpestřovat
si	se	k3xPyFc3	se
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
stravu	strava	k1gFnSc4	strava
hmyzem	hmyz	k1gInSc7	hmyz
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
termity	termit	k1gInPc7	termit
a	a	k8xC	a
sarančemi	saranče	k1gFnPc7	saranče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
však	však	k9	však
ani	ani	k8xC	ani
malými	malý	k2eAgMnPc7d1	malý
savci	savec	k1gMnPc7	savec
(	(	kIx(	(
<g/>
hlodavci	hlodavec	k1gMnPc7	hlodavec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plazy	plaz	k1gInPc7	plaz
a	a	k8xC	a
ptáčaty	ptáče	k1gNnPc7	ptáče
<g/>
.	.	kIx.	.
</s>
<s>
Pštros	pštros	k1gMnSc1	pštros
nevydrží	vydržet	k5eNaPmIp3nS	vydržet
dlouho	dlouho	k6eAd1	dlouho
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
jedince	jedinec	k1gMnPc4	jedinec
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
nutnou	nutný	k2eAgFnSc4d1	nutná
podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nemůže	moct	k5eNaImIp3nS	moct
vodu	voda	k1gFnSc4	voda
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
spokojit	spokojit	k5eAaPmF	spokojit
s	s	k7c7	s
tučnými	tučný	k2eAgFnPc7d1	tučná
a	a	k8xC	a
šťavnatými	šťavnatý	k2eAgFnPc7d1	šťavnatá
rostlinami	rostlina	k1gFnPc7	rostlina
(	(	kIx(	(
<g/>
halofyty	halofyt	k1gInPc7	halofyt
<g/>
)	)	kIx)	)
a	a	k8xC	a
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jenom	jenom	k9	jenom
trochu	trochu	k6eAd1	trochu
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
vypije	vypít	k5eAaPmIp3nS	vypít
pštros	pštros	k1gMnSc1	pštros
denně	denně	k6eAd1	denně
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
koupání	koupání	k1gNnSc4	koupání
pštros	pštros	k1gMnSc1	pštros
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
vodu	voda	k1gFnSc4	voda
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
spíše	spíše	k9	spíše
prachové	prachový	k2eAgFnPc4d1	prachová
koupele	koupel	k1gFnPc4	koupel
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mu	on	k3xPp3gMnSc3	on
částečně	částečně	k6eAd1	částečně
prachem	prach	k1gInSc7	prach
<g/>
,	,	kIx,	,
usazeným	usazený	k2eAgNnSc7d1	usazené
v	v	k7c4	v
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
maz	maz	k1gInSc4	maz
(	(	kIx(	(
<g/>
pštrosovi	pštros	k1gMnSc3	pštros
chybí	chybět	k5eAaImIp3nS	chybět
kostrční	kostrční	k2eAgFnPc4d1	kostrční
mazové	mazový	k2eAgFnPc4d1	mazová
žlázy	žláza	k1gFnPc4	žláza
<g/>
)	)	kIx)	)
chránící	chránící	k2eAgFnSc4d1	chránící
jeho	jeho	k3xOp3gFnSc4	jeho
peří	peřit	k5eAaImIp3nP	peřit
proti	proti	k7c3	proti
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
pštros	pštros	k1gMnSc1	pštros
pověstný	pověstný	k2eAgInSc4d1	pověstný
skrýváním	skrývání	k1gNnSc7	skrývání
své	svůj	k3xOyFgFnSc2	svůj
hlavy	hlava	k1gFnSc2	hlava
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
svým	svůj	k3xOyFgNnSc7	svůj
líčením	líčení	k1gNnSc7	líčení
pštrosa	pštros	k1gMnSc2	pštros
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Naturalis	Naturalis	k1gFnSc2	Naturalis
Historia	Historium	k1gNnSc2	Historium
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
pštrosa	pštros	k1gMnSc4	pštros
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
skrývat	skrývat	k5eAaImF	skrývat
svou	svůj	k3xOyFgFnSc4	svůj
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
keři	keř	k1gInSc6	keř
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
žádné	žádný	k3yNgNnSc1	žádný
pozorování	pozorování	k1gNnSc1	pozorování
tohoto	tento	k3xDgNnSc2	tento
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
protiargumentem	protiargument	k1gInSc7	protiargument
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
chovaly	chovat	k5eAaImAgFnP	chovat
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
moc	moc	k6eAd1	moc
dlouho	dlouho	k6eAd1	dlouho
nepřežily	přežít	k5eNaPmAgFnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
může	moct	k5eAaImIp3nS	moct
vyplývat	vyplývat	k5eAaImF	vyplývat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
pštrosi	pštros	k1gMnPc1	pštros
zdálky	zdálky	k6eAd1	zdálky
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zároveň	zároveň	k6eAd1	zároveň
kvůli	kvůli	k7c3	kvůli
trávení	trávení	k1gNnSc3	trávení
polykají	polykat	k5eAaImIp3nP	polykat
písek	písek	k1gInSc4	písek
a	a	k8xC	a
kaménky	kamének	k1gInPc4	kamének
<g/>
,	,	kIx,	,
vypadají	vypadat	k5eAaPmIp3nP	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
strkali	strkat	k5eAaImAgMnP	strkat
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
když	když	k8xS	když
se	se	k3xPyFc4	se
pštros	pštros	k1gMnSc1	pštros
cítí	cítit	k5eAaImIp3nS	cítit
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
<g/>
,	,	kIx,	,
přitiskne	přitisknout	k5eAaPmIp3nS	přitisknout
se	se	k3xPyFc4	se
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
svůj	svůj	k3xOyFgInSc4	svůj
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
položí	položit	k5eAaPmIp3nS	položit
ho	on	k3xPp3gInSc4	on
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
před	před	k7c4	před
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
tak	tak	k9	tak
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
skrytí	skrytí	k1gNnSc3	skrytí
jejich	jejich	k3xOp3gNnPc2	jejich
nápadných	nápadný	k2eAgNnPc2d1	nápadné
bílých	bílý	k2eAgNnPc2d1	bílé
per	pero	k1gNnPc2	pero
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
chvějící	chvějící	k2eAgMnSc1d1	chvějící
se	se	k3xPyFc4	se
horký	horký	k2eAgInSc1d1	horký
vzduch	vzduch	k1gInSc1	vzduch
pak	pak	k6eAd1	pak
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k7c2	místo
pštrosa	pštros	k1gMnSc2	pštros
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejasnou	jasný	k2eNgFnSc4d1	nejasná
tmavou	tmavý	k2eAgFnSc4d1	tmavá
hroudu	hrouda	k1gFnSc4	hrouda
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pštros	pštros	k1gMnSc1	pštros
poblíž	poblíž	k7c2	poblíž
horizontu	horizont	k1gInSc2	horizont
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
zdát	zdát	k5eAaImF	zdát
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
zázrakem	zázrak	k1gInSc7	zázrak
zmizel	zmizet	k5eAaPmAgMnS	zmizet
z	z	k7c2	z
pouštní	pouštní	k2eAgFnSc2d1	pouštní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hrozba	hrozba	k1gFnSc1	hrozba
pro	pro	k7c4	pro
pštrosa	pštros	k1gMnSc4	pštros
nachází	nacházet	k5eAaImIp3nS	nacházet
příliš	příliš	k6eAd1	příliš
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
raději	rád	k6eAd2	rád
volí	volit	k5eAaImIp3nP	volit
útěk	útěk	k1gInSc4	útěk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umí	umět	k5eAaImIp3nS	umět
se	se	k3xPyFc4	se
také	také	k9	také
účinně	účinně	k6eAd1	účinně
bránit	bránit	k5eAaImF	bránit
kopy	kop	k1gInPc4	kop
svých	svůj	k3xOyFgFnPc2	svůj
silných	silný	k2eAgFnPc2d1	silná
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
pštrosa	pštros	k1gMnSc2	pštros
je	být	k5eAaImIp3nS	být
také	také	k9	také
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
starozákonní	starozákonní	k2eAgFnSc6d1	starozákonní
části	část	k1gFnSc6	část
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
Boha	bůh	k1gMnSc2	bůh
k	k	k7c3	k
Jobovi	Job	k1gMnSc3	Job
(	(	kIx(	(
<g/>
Job	Job	k1gMnSc1	Job
39	[number]	k4	39
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pštros	pštros	k1gMnSc1	pštros
popsán	popsán	k2eAgMnSc1d1	popsán
jako	jako	k8xS	jako
radostně	radostně	k6eAd1	radostně
pyšný	pyšný	k2eAgMnSc1d1	pyšný
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
malá	malý	k2eAgNnPc4d1	malé
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
nedbalý	dbalý	k2eNgInSc4d1	nedbalý
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zachází	zacházet	k5eAaImIp3nS	zacházet
neurvale	urvale	k6eNd1	urvale
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
potomky	potomek	k1gMnPc7	potomek
<g/>
,	,	kIx,	,
neoplývá	oplývat	k5eNaImIp3nS	oplývat
příliš	příliš	k6eAd1	příliš
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zahanbit	zahanbit	k5eAaPmF	zahanbit
i	i	k9	i
rychlého	rychlý	k2eAgMnSc4d1	rychlý
koně	kůň	k1gMnSc4	kůň
s	s	k7c7	s
jezdcem	jezdec	k1gMnSc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
jsou	být	k5eAaImIp3nP	být
pštrosi	pštros	k1gMnPc1	pštros
zmíněni	zmíněn	k2eAgMnPc1d1	zmíněn
jako	jako	k8xS	jako
příslovečné	příslovečný	k2eAgInPc4d1	příslovečný
příklady	příklad	k1gInPc4	příklad
špatných	špatný	k2eAgMnPc2d1	špatný
rodičů	rodič	k1gMnPc2	rodič
<g/>
;	;	kIx,	;
blíže	blíže	k1gFnSc1	blíže
viz	vidět	k5eAaImRp2nS	vidět
Arabský	arabský	k2eAgMnSc1d1	arabský
pštros	pštros	k1gMnSc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
jsou	být	k5eAaImIp3nP	být
známí	známý	k2eAgMnPc1d1	známý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	jíst	k5eAaImIp3nS	jíst
téměř	téměř	k6eAd1	téměř
všechno	všechen	k3xTgNnSc4	všechen
(	(	kIx(	(
<g/>
dietní	dietní	k2eAgFnSc1d1	dietní
indiskrétnost	indiskrétnost	k1gFnSc1	indiskrétnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
jsou	být	k5eAaImIp3nP	být
přizpůsobeni	přizpůsoben	k2eAgMnPc1d1	přizpůsoben
velkým	velký	k2eAgNnPc3d1	velké
výkyvům	výkyv	k1gInPc3	výkyv
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
denní	denní	k2eAgFnSc7d1	denní
a	a	k8xC	a
noční	noční	k2eAgFnSc7d1	noční
teplotou	teplota	k1gFnSc7	teplota
činit	činit	k5eAaImF	činit
až	až	k9	až
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Pštros	pštros	k1gMnSc1	pštros
má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
husté	hustý	k2eAgNnSc4d1	husté
a	a	k8xC	a
nadýchané	nadýchaný	k2eAgNnSc4d1	nadýchané
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
izolační	izolační	k2eAgInSc1d1	izolační
polštář	polštář	k1gInSc1	polštář
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nS	peřit
díky	díky	k7c3	díky
zadržovanému	zadržovaný	k2eAgInSc3d1	zadržovaný
vzduchu	vzduch	k1gInSc3	vzduch
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
přístup	přístup	k1gInSc1	přístup
slunečních	sluneční	k2eAgInPc2d1	sluneční
paprsků	paprsek	k1gInPc2	paprsek
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
ve	v	k7c6	v
dne	den	k1gInSc2	den
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Máváním	mávání	k1gNnSc7	mávání
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
také	také	k9	také
pštros	pštros	k1gMnSc1	pštros
schopen	schopen	k2eAgMnSc1d1	schopen
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
krev	krev	k1gFnSc4	krev
v	v	k7c6	v
povrchových	povrchový	k2eAgFnPc6d1	povrchová
cévách	céva	k1gFnPc6	céva
holých	holý	k2eAgNnPc2d1	holé
stehen	stehno	k1gNnPc2	stehno
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
chvění	chvění	k1gNnSc4	chvění
kůže	kůže	k1gFnSc2	kůže
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
a	a	k8xC	a
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
pštrosího	pštrosí	k2eAgInSc2d1	pštrosí
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
pohlavní	pohlavní	k2eAgFnPc4d1	pohlavní
dospělosti	dospělost	k1gFnPc4	dospělost
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
letech	let	k1gInPc6	let
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
asi	asi	k9	asi
o	o	k7c4	o
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pštrosů	pštros	k1gMnPc2	pštros
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oplodnění	oplodnění	k1gNnSc3	oplodnění
vícekrát	vícekrát	k6eAd1	vícekrát
za	za	k7c4	za
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
jedinci	jedinec	k1gMnPc1	jedinec
obývají	obývat	k5eAaImIp3nP	obývat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
savan	savana	k1gFnPc2	savana
spadá	spadat	k5eAaPmIp3nS	spadat
doba	doba	k1gFnSc1	doba
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
sucha	sucho	k1gNnSc2	sucho
mezi	mezi	k7c7	mezi
červnem	červen	k1gInSc7	červen
a	a	k8xC	a
říjnem	říjen	k1gInSc7	říjen
<g/>
,	,	kIx,	,
v	v	k7c6	v
pouštích	poušť	k1gFnPc6	poušť
Namibie	Namibie	k1gFnSc2	Namibie
naopak	naopak	k6eAd1	naopak
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
konce	konec	k1gInSc2	konec
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Normálně	normálně	k6eAd1	normálně
žijící	žijící	k2eAgMnPc1d1	žijící
hejna	hejno	k1gNnSc2	hejno
pštrosů	pštros	k1gMnPc2	pštros
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
skupiny	skupina	k1gFnPc1	skupina
začínají	začínat	k5eAaImIp3nP	začínat
mísit	mísit	k5eAaImF	mísit
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
tok	tok	k1gInSc1	tok
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
předvádět	předvádět	k5eAaImF	předvádět
<g/>
,	,	kIx,	,
vztyčují	vztyčovat	k5eAaImIp3nP	vztyčovat
krky	krk	k1gInPc4	krk
<g/>
,	,	kIx,	,
roztahují	roztahovat	k5eAaImIp3nP	roztahovat
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
mávají	mávat	k5eAaImIp3nP	mávat
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
oběma	dva	k4xCgFnPc7	dva
současně	současně	k6eAd1	současně
nebo	nebo	k8xC	nebo
každým	každý	k3xTgNnSc7	každý
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Partnera	partner	k1gMnSc2	partner
si	se	k3xPyFc3	se
však	však	k9	však
vybírají	vybírat	k5eAaImIp3nP	vybírat
samice	samice	k1gFnPc1	samice
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc1	nějaký
samec	samec	k1gInSc1	samec
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
<g/>
,	,	kIx,	,
rozevírají	rozevírat	k5eAaImIp3nP	rozevírat
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
vytahují	vytahovat	k5eAaImIp3nP	vytahovat
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
pak	pak	k6eAd1	pak
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
močí	močit	k5eAaImIp3nP	močit
a	a	k8xC	a
kálí	kálet	k5eAaImIp3nP	kálet
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
samci	samec	k1gMnPc1	samec
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
pouštějí	pouštět	k5eAaImIp3nP	pouštět
do	do	k7c2	do
předstíraných	předstíraný	k2eAgInPc2d1	předstíraný
soubojů	souboj	k1gInPc2	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
vybraní	vybraný	k2eAgMnPc1d1	vybraný
partneři	partner	k1gMnPc1	partner
oddělí	oddělit	k5eAaPmIp3nP	oddělit
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
společném	společný	k2eAgNnSc6d1	společné
zobání	zobání	k1gNnSc6	zobání
potravy	potrava	k1gFnSc2	potrava
samec	samec	k1gMnSc1	samec
usedne	usednout	k5eAaPmIp3nS	usednout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
křídly	křídlo	k1gNnPc7	křídlo
vířit	vířit	k5eAaImF	vířit
prach	prach	k1gInSc1	prach
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
hlasitě	hlasitě	k6eAd1	hlasitě
křičí	křičet	k5eAaImIp3nP	křičet
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
samice	samice	k1gFnSc1	samice
připravena	připraven	k2eAgFnSc1d1	připravena
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
svěsí	svěsit	k5eAaPmIp3nS	svěsit
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
lehne	lehnout	k5eAaPmIp3nS	lehnout
si	se	k3xPyFc3	se
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
obkročme	obkročit	k5eAaPmRp1nP	obkročit
sedne	sednout	k5eAaPmIp3nS	sednout
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
páření	páření	k1gNnSc1	páření
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
samec	samec	k1gMnSc1	samec
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
kole	kolo	k1gNnSc6	kolo
spářit	spářit	k5eAaPmF	spářit
i	i	k9	i
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
samicemi	samice	k1gFnPc7	samice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
výběru	výběr	k1gInSc6	výběr
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
dominantní	dominantní	k2eAgFnSc1d1	dominantní
partnerka	partnerka	k1gFnSc1	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
od	od	k7c2	od
páření	páření	k1gNnSc2	páření
do	do	k7c2	do
snůšky	snůška	k1gFnSc2	snůška
vajec	vejce	k1gNnPc2	vejce
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
si	se	k3xPyFc3	se
pštrosí	pštrosí	k2eAgFnSc1d1	pštrosí
rodina	rodina	k1gFnSc1	rodina
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
nevelké	velký	k2eNgNnSc4d1	nevelké
území	území	k1gNnSc4	území
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
m	m	kA	m
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
otevřenosti	otevřenost	k1gFnSc2	otevřenost
terénu	terén	k1gInSc2	terén
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
do	do	k7c2	do
stáří	stář	k1gFnPc2	stář
mláďat	mládě	k1gNnPc2	mládě
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
území	území	k1gNnSc2	území
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
křiku	křik	k1gInSc6	křik
<g/>
,	,	kIx,	,
výhružném	výhružný	k2eAgNnSc6d1	výhružné
držení	držení	k1gNnSc6	držení
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
mávání	mávání	k1gNnSc2	mávání
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgNnSc7	tento
územím	území	k1gNnSc7	území
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
tzv.	tzv.	kA	tzv.
zóna	zóna	k1gFnSc1	zóna
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
vlivu	vliv	k1gInSc6	vliv
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dosahu	dosah	k1gInSc2	dosah
hlasu	hlas	k1gInSc2	hlas
<g/>
)	)	kIx)	)
daného	daný	k2eAgInSc2d1	daný
samce	samec	k1gInSc2	samec
a	a	k8xC	a
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
hájena	hájen	k2eAgFnSc1d1	hájena
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdní	hnízdní	k2eAgNnSc1d1	hnízdní
teritorium	teritorium	k1gNnSc1	teritorium
a	a	k8xC	a
zóna	zóna	k1gFnSc1	zóna
vlivu	vliv	k1gInSc2	vliv
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nS	tvořit
superteritorium	superteritorium	k1gNnSc4	superteritorium
pštrosí	pštrosí	k2eAgFnSc2d1	pštrosí
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosí	pštrosí	k2eAgNnSc1d1	pštrosí
hnízdo	hnízdo	k1gNnSc1	hnízdo
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
samec	samec	k1gInSc4	samec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vybírá	vybírat	k5eAaImIp3nS	vybírat
jeho	jeho	k3xOp3gFnSc4	jeho
polohu	poloha	k1gFnSc4	poloha
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
prohlubni	prohlubeň	k1gFnSc6	prohlubeň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
samec	samec	k1gMnSc1	samec
hrabáním	hrabání	k1gNnPc3	hrabání
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vsedě	vsedě	k6eAd1	vsedě
vrtěním	vrtění	k1gNnSc7	vrtění
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
mělký	mělký	k2eAgInSc1d1	mělký
dolík	dolík	k1gInSc1	dolík
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
m	m	kA	m
a	a	k8xC	a
hloubce	hloubka	k1gFnSc6	hloubka
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
hnízda	hnízdo	k1gNnSc2	hnízdo
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
samec	samec	k1gMnSc1	samec
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
sedět	sedět	k5eAaImF	sedět
a	a	k8xC	a
dominantní	dominantní	k2eAgFnSc1d1	dominantní
partnerka	partnerka	k1gFnSc1	partnerka
začne	začít	k5eAaPmIp3nS	začít
vedle	vedle	k7c2	vedle
hnízda	hnízdo	k1gNnSc2	hnízdo
snášet	snášet	k5eAaImF	snášet
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
si	se	k3xPyFc3	se
samec	samec	k1gMnSc1	samec
zobákem	zobák	k1gInSc7	zobák
navalí	navalit	k5eAaPmIp3nP	navalit
pod	pod	k7c4	pod
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Snášení	snášení	k1gNnSc1	snášení
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
pštros	pštros	k1gMnSc1	pštros
více	hodně	k6eAd2	hodně
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
snáší	snášet	k5eAaImIp3nP	snášet
tyto	tento	k3xDgInPc4	tento
společně	společně	k6eAd1	společně
bez	bez	k7c2	bez
žádného	žádný	k3yNgNnSc2	žádný
zjevného	zjevný	k2eAgNnSc2d1	zjevné
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
snesených	snesený	k2eAgNnPc2d1	snesené
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dominantní	dominantní	k2eAgFnSc1d1	dominantní
samice	samice	k1gFnSc1	samice
jich	on	k3xPp3gFnPc2	on
snese	snést	k5eAaPmIp3nS	snést
až	až	k9	až
8	[number]	k4	8
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
samice	samice	k1gFnPc1	samice
snesou	snést	k5eAaPmIp3nP	snést
kolem	kolem	k7c2	kolem
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
snůška	snůška	k1gFnSc1	snůška
obnáší	obnášet	k5eAaImIp3nS	obnášet
zpravidla	zpravidla	k6eAd1	zpravidla
20	[number]	k4	20
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
až	až	k8xS	až
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
prodloužit	prodloužit	k5eAaPmF	prodloužit
až	až	k9	až
do	do	k7c2	do
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k8xC	i
snůšky	snůška	k1gFnSc2	snůška
dvě	dva	k4xCgNnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosí	pštrosí	k2eAgNnSc1d1	pštrosí
vejce	vejce	k1gNnSc1	vejce
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
vajec	vejce	k1gNnPc2	vejce
všech	všecek	k3xTgMnPc2	všecek
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
pštrosů	pštros	k1gMnPc2	pštros
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgInPc2d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
750	[number]	k4	750
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
gramů	gram	k1gInPc2	gram
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
vážit	vážit	k5eAaImF	vážit
až	až	k6eAd1	až
jako	jako	k8xS	jako
30	[number]	k4	30
slepičích	slepičí	k2eAgNnPc2d1	slepičí
vajec	vejce	k1gNnPc2	vejce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
rozměry	rozměr	k1gInPc1	rozměr
16	[number]	k4	16
×	×	k?	×
13	[number]	k4	13
cm	cm	kA	cm
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
největší	veliký	k2eAgFnSc7d3	veliký
známou	známý	k2eAgFnSc7d1	známá
současnou	současný	k2eAgFnSc7d1	současná
buňkou	buňka	k1gFnSc7	buňka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
až	až	k9	až
krémově	krémově	k6eAd1	krémově
lesklou	lesklý	k2eAgFnSc4d1	lesklá
<g/>
,	,	kIx,	,
silnou	silný	k2eAgFnSc4d1	silná
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
skořápku	skořápka	k1gFnSc4	skořápka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
vajec	vejce	k1gNnPc2	vejce
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
všechny	všechen	k3xTgFnPc1	všechen
samice	samice	k1gFnPc1	samice
kromě	kromě	k7c2	kromě
dominantní	dominantní	k2eAgFnSc3d1	dominantní
z	z	k7c2	z
teritoria	teritorium	k1gNnSc2	teritorium
vyhnány	vyhnán	k2eAgFnPc4d1	vyhnána
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
hnízda	hnízdo	k1gNnPc1	hnízdo
jsou	být	k5eAaImIp3nP	být
umístěna	umístěn	k2eAgNnPc1d1	umístěno
vejce	vejce	k1gNnPc1	vejce
od	od	k7c2	od
dominantní	dominantní	k2eAgFnSc2d1	dominantní
samice	samice	k1gFnSc2	samice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vejce	vejce	k1gNnPc1	vejce
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
samic	samice	k1gFnPc2	samice
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
okraji	okraj	k1gInSc6	okraj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pštros	pštros	k1gMnSc1	pštros
dokáže	dokázat	k5eAaPmIp3nS	dokázat
svým	svůj	k3xOyFgNnSc7	svůj
tělem	tělo	k1gNnSc7	tělo
maximálně	maximálně	k6eAd1	maximálně
zakrýt	zakrýt	k5eAaPmF	zakrýt
asi	asi	k9	asi
20	[number]	k4	20
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ostatní	ostatní	k2eAgNnPc1d1	ostatní
vejce	vejce	k1gNnPc1	vejce
nechráněná	chráněný	k2eNgNnPc1d1	nechráněné
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
predátoři	predátor	k1gMnPc1	predátor
většinou	většina	k1gFnSc7	většina
vybírají	vybírat	k5eAaImIp3nP	vybírat
vejce	vejce	k1gNnPc4	vejce
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
jsou	být	k5eAaImIp3nP	být
vejce	vejce	k1gNnPc1	vejce
dominantní	dominantní	k2eAgFnSc2d1	dominantní
samice	samice	k1gFnSc2	samice
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
pro	pro	k7c4	pro
hnízdo	hnízdo	k1gNnSc4	hnízdo
jsou	být	k5eAaImIp3nP	být
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
šakali	šakal	k1gMnPc1	šakal
<g/>
,	,	kIx,	,
hyeny	hyena	k1gFnPc1	hyena
a	a	k8xC	a
supi	sup	k1gMnPc1	sup
mrchožraví	mrchožravý	k2eAgMnPc1d1	mrchožravý
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
a	a	k8xC	a
hyeny	hyena	k1gFnPc1	hyena
především	především	k9	především
loví	lovit	k5eAaImIp3nP	lovit
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
supi	sup	k1gMnPc1	sup
dokážou	dokázat	k5eAaPmIp3nP	dokázat
údery	úder	k1gInPc4	úder
kamene	kámen	k1gInSc2	kámen
drženém	držený	k2eAgInSc6d1	držený
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
zobáku	zobák	k1gInSc6	zobák
rozbít	rozbít	k5eAaPmF	rozbít
i	i	k9	i
silnou	silný	k2eAgFnSc4d1	silná
skořápku	skořápka	k1gFnSc4	skořápka
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Šakali	šakal	k1gMnPc1	šakal
zase	zase	k9	zase
používají	používat	k5eAaImIp3nP	používat
metodu	metoda	k1gFnSc4	metoda
spouštění	spouštění	k1gNnSc4	spouštění
vajec	vejce	k1gNnPc2	vejce
z	z	k7c2	z
okraje	okraj	k1gInSc2	okraj
hnízda	hnízdo	k1gNnSc2	hnízdo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
rozbití	rozbití	k1gNnSc4	rozbití
nárazem	náraz	k1gInSc7	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
F.	F.	kA	F.
a	a	k8xC	a
E.	E.	kA	E.
Sauerových	Sauerových	k2eAgMnSc1d1	Sauerových
dokonce	dokonce	k9	dokonce
i	i	k9	i
antilopa	antilopa	k1gFnSc1	antilopa
přímorožec	přímorožec	k1gMnSc1	přímorožec
šavlorohý	šavlorohý	k2eAgMnSc1d1	šavlorohý
občas	občas	k6eAd1	občas
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
pštrosími	pštrosí	k2eAgNnPc7d1	pštrosí
vejci	vejce	k1gNnPc7	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Neupřesňují	upřesňovat	k5eNaImIp3nP	upřesňovat
sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
přímorožec	přímorožec	k1gMnSc1	přímorožec
skořápky	skořápka	k1gFnSc2	skořápka
rozbíjí	rozbíjet	k5eAaImIp3nS	rozbíjet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
můžeme	moct	k5eAaImIp1nP	moct
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
údery	úder	k1gInPc4	úder
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
začíná	začínat	k5eAaImIp3nS	začínat
teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
až	až	k9	až
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
vejce	vejce	k1gNnPc4	vejce
snesena	snesen	k2eAgNnPc4d1	sneseno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
všechna	všechen	k3xTgNnPc1	všechen
mláďata	mládě	k1gNnPc1	mládě
vylíhla	vylíhnout	k5eAaPmAgNnP	vylíhnout
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
hnízdo	hnízdo	k1gNnSc4	hnízdo
opustit	opustit	k5eAaPmF	opustit
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Sezení	sezení	k1gNnSc1	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
dní	den	k1gInPc2	den
a	a	k8xC	a
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
samec	samec	k1gMnSc1	samec
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gNnSc4	její
nenápadné	nápadný	k2eNgNnSc4d1	nenápadné
zbarvení	zbarvení	k1gNnSc4	zbarvení
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
větší	veliký	k2eAgFnSc1d2	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnízdo	hnízdo	k1gNnSc1	hnízdo
nebude	být	k5eNaImBp3nS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
podaří	podařit	k5eAaPmIp3nS	podařit
vysedět	vysedět	k5eAaPmF	vysedět
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
10	[number]	k4	10
<g/>
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
násad	násada	k1gFnPc2	násada
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
mají	mít	k5eAaImIp3nP	mít
mláďata	mládě	k1gNnPc4	mládě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zobáčku	zobáček	k1gInSc2	zobáček
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
rohovitý	rohovitý	k2eAgInSc4d1	rohovitý
výrůstek	výrůstek	k1gInSc4	výrůstek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
později	pozdě	k6eAd2	pozdě
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
jim	on	k3xPp3gMnPc3	on
až	až	k9	až
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
z	z	k7c2	z
vejce	vejce	k1gNnSc2	vejce
vyprostí	vyprostit	k5eAaPmIp3nP	vyprostit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
se	se	k3xPyFc4	se
ozývají	ozývat	k5eAaImIp3nP	ozývat
slabým	slabý	k2eAgInSc7d1	slabý
křikem	křik	k1gInSc7	křik
a	a	k8xC	a
po	po	k7c6	po
oschnutí	oschnutí	k1gNnSc6	oschnutí
ihned	ihned	k6eAd1	ihned
začínají	začínat	k5eAaImIp3nP	začínat
zobat	zobat	k5eAaImF	zobat
malé	malý	k2eAgInPc4d1	malý
kamínky	kamínek	k1gInPc4	kamínek
a	a	k8xC	a
přiměřeně	přiměřeně	k6eAd1	přiměřeně
velkou	velký	k2eAgFnSc4d1	velká
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
a	a	k8xC	a
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
jiné	jiný	k2eAgNnSc1d1	jiné
než	než	k8xS	než
rodičů	rodič	k1gMnPc2	rodič
–	–	k?	–
jejich	jejich	k3xOp3gNnSc1	jejich
peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
pruhované	pruhovaný	k2eAgNnSc1d1	pruhované
<g/>
,	,	kIx,	,
kropenaté	kropenatý	k2eAgNnSc1d1	kropenaté
a	a	k8xC	a
nevýrazně	výrazně	k6eNd1	výrazně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
mohla	moct	k5eAaImAgFnS	moct
přitisknout	přitisknout	k5eAaPmF	přitisknout
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
neviditelnými	viditelný	k2eNgNnPc7d1	neviditelné
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
rostou	růst	k5eAaImIp3nP	růst
<g/>
,	,	kIx,	,
odvažují	odvažovat	k5eAaImIp3nP	odvažovat
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
rodičů	rodič	k1gMnPc2	rodič
stále	stále	k6eAd1	stále
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
ochraně	ochrana	k1gFnSc6	ochrana
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
často	často	k6eAd1	často
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
ke	k	k7c3	k
lsti	lest	k1gFnSc3	lest
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
odlákat	odlákat	k5eAaPmF	odlákat
predátora	predátor	k1gMnSc4	predátor
od	od	k7c2	od
ptáčat	ptáče	k1gNnPc2	ptáče
předstíráním	předstírání	k1gNnSc7	předstírání
zranění	zranění	k1gNnSc1	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
pštrosi	pštros	k1gMnPc1	pštros
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
,	,	kIx,	,
výšky	výška	k1gFnPc1	výška
dospělých	dospělí	k1gMnPc2	dospělí
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
18	[number]	k4	18
měsících	měsíc	k1gInPc6	měsíc
a	a	k8xC	a
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
většinou	většinou	k6eAd1	většinou
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
života	život	k1gInSc2	život
tráví	trávit	k5eAaImIp3nP	trávit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
snůšce	snůška	k1gFnSc3	snůška
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
rodiči	rodič	k1gMnPc7	rodič
odehnána	odehnán	k2eAgNnPc1d1	odehnáno
(	(	kIx(	(
<g/>
po	po	k7c6	po
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
týdnech	týden	k1gInPc6	týden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
po	po	k7c6	po
desítkách	desítka	k1gFnPc6	desítka
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
po	po	k7c6	po
stovkách	stovka	k1gFnPc6	stovka
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
adoptovány	adoptovat	k5eAaPmNgFnP	adoptovat
dospělými	dospělí	k1gMnPc7	dospělí
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
rodinu	rodina	k1gFnSc4	rodina
nezaložili	založit	k5eNaPmAgMnP	založit
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
skupinám	skupina	k1gFnPc3	skupina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
říká	říkat	k5eAaImIp3nS	říkat
superrodiny	superrodina	k1gFnPc4	superrodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
ty	ten	k3xDgInPc4	ten
nevydrží	vydržet	k5eNaPmIp3nP	vydržet
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
jsou	být	k5eAaImIp3nP	být
mladí	mladý	k1gMnPc1	mladý
definitivně	definitivně	k6eAd1	definitivně
ponecháni	ponechat	k5eAaPmNgMnP	ponechat
sami	sám	k3xTgMnPc1	sám
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
založí	založit	k5eAaPmIp3nP	založit
se	se	k3xPyFc4	se
potulují	potulovat	k5eAaImIp3nP	potulovat
až	až	k9	až
do	do	k7c2	do
dosažení	dosažení	k1gNnSc2	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc2	ten
však	však	k9	však
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jen	jen	k9	jen
asi	asi	k9	asi
15	[number]	k4	15
<g/>
%	%	kIx~	%
vylíhlých	vylíhlý	k2eAgNnPc2d1	vylíhlé
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
pštrosa	pštros	k1gMnSc2	pštros
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
byla	být	k5eAaImAgNnP	být
pštrosí	pštrosí	k2eAgNnPc1d1	pštrosí
pera	pero	k1gNnPc1	pero
vyhledávána	vyhledáván	k2eAgNnPc1d1	vyhledáváno
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
per	pero	k1gNnPc2	pero
jiných	jiný	k2eAgMnPc2d1	jiný
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
centrální	centrální	k2eAgFnSc2d1	centrální
osy	osa	k1gFnSc2	osa
(	(	kIx(	(
<g/>
ostnu	osten	k1gInSc2	osten
<g/>
)	)	kIx)	)
stejně	stejně	k6eAd1	stejně
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Bohyně	bohyně	k1gFnSc1	bohyně
Maat	Maatum	k1gNnPc2	Maatum
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
dohlížela	dohlížet	k5eAaImAgNnP	dohlížet
na	na	k7c4	na
vážení	vážení	k1gNnSc4	vážení
lidských	lidský	k2eAgFnPc2d1	lidská
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
s	s	k7c7	s
pštrosími	pštrosí	k2eAgNnPc7d1	pštrosí
pery	pero	k1gNnPc7	pero
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
vějíře	vějíř	k1gInPc1	vějíř
a	a	k8xC	a
plácačky	plácačka	k1gFnPc1	plácačka
na	na	k7c4	na
mouchy	moucha	k1gFnPc4	moucha
vysokých	vysoký	k2eAgMnPc2d1	vysoký
hodnostářů	hodnostář	k1gMnPc2	hodnostář
a	a	k8xC	a
faraonů	faraon	k1gMnPc2	faraon
byly	být	k5eAaImAgFnP	být
zhotoveny	zhotovit	k5eAaPmNgFnP	zhotovit
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
per	pero	k1gNnPc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosí	pštrosí	k2eAgNnPc1d1	pštrosí
pera	pero	k1gNnPc1	pero
sloužila	sloužit	k5eAaImAgNnP	sloužit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
i	i	k9	i
ke	k	k7c3	k
zdobení	zdobení	k1gNnSc3	zdobení
válečné	válečný	k2eAgFnSc2d1	válečná
výstroje	výstroj	k1gFnSc2	výstroj
<g/>
,	,	kIx,	,
od	od	k7c2	od
čel	čelo	k1gNnPc2	čelo
egyptských	egyptský	k2eAgMnPc2d1	egyptský
koní	kůň	k1gMnPc2	kůň
přes	přes	k7c4	přes
helmy	helma	k1gFnPc4	helma
středověkých	středověký	k2eAgMnPc2d1	středověký
rytířů	rytíř	k1gMnPc2	rytíř
až	až	k9	až
po	po	k7c4	po
známé	známý	k2eAgInPc4d1	známý
klobouky	klobouk	k1gInPc4	klobouk
mušketýrů	mušketýr	k1gMnPc2	mušketýr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pera	pero	k1gNnSc2	pero
pštrosů	pštros	k1gMnPc2	pštros
stala	stát	k5eAaPmAgFnS	stát
módní	módní	k2eAgFnSc7d1	módní
záležitostí	záležitost	k1gFnSc7	záležitost
a	a	k8xC	a
krášlila	krášlit	k5eAaImAgFnS	krášlit
účesy	účes	k1gInPc4	účes
dam	dáma	k1gFnPc2	dáma
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
největší	veliký	k2eAgFnSc1d3	veliký
obliba	obliba	k1gFnSc1	obliba
kulminovala	kulminovat	k5eAaImAgFnS	kulminovat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stav	stav	k1gInSc1	stav
pštrosů	pštros	k1gMnPc2	pštros
počal	počnout	k5eAaPmAgInS	počnout
již	již	k6eAd1	již
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
povážlivě	povážlivě	k6eAd1	povážlivě
klesat	klesat	k5eAaImF	klesat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zahájeno	zahájen	k2eAgNnSc1d1	zahájeno
zakládání	zakládání	k1gNnSc1	zakládání
pštrosích	pštrosí	k2eAgFnPc2d1	pštrosí
farem	farma	k1gFnPc2	farma
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
farma	farma	k1gFnSc1	farma
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Little	Little	k1gFnSc2	Little
Karroo	Karroo	k6eAd1	Karroo
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Kapské	kapský	k2eAgFnSc2d1	kapská
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Farmy	farma	k1gFnPc1	farma
rychle	rychle	k6eAd1	rychle
přibývaly	přibývat	k5eAaImAgFnP	přibývat
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
chováno	chován	k2eAgNnSc1d1	chováno
2159	[number]	k4	2159
pštrosů	pštros	k1gMnPc2	pštros
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
již	již	k9	již
110.000	[number]	k4	110.000
pštrosů	pštros	k1gMnPc2	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
vyvážela	vyvážet	k5eAaImAgFnS	vyvážet
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
ročně	ročně	k6eAd1	ročně
370.000	[number]	k4	370.000
kg	kg	kA	kg
pštrosího	pštrosí	k2eAgNnSc2d1	pštrosí
peří	peří	k1gNnSc2	peří
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
farmy	farma	k1gFnPc1	farma
i	i	k8xC	i
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
však	však	k9	však
změnila	změnit	k5eAaPmAgFnS	změnit
móda	móda	k1gFnSc1	móda
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
pštrosů	pštros	k1gMnPc2	pštros
zkolaboval	zkolabovat	k5eAaPmAgInS	zkolabovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
rozsahu	rozsah	k1gInSc6	rozsah
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
pštrosi	pštros	k1gMnPc1	pštros
chovají	chovat	k5eAaImIp3nP	chovat
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc4	maso
nebo	nebo	k8xC	nebo
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
již	již	k6eAd1	již
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
o	o	k7c6	o
takovém	takový	k3xDgNnSc6	takový
množství	množství	k1gNnSc6	množství
ptáků	pták	k1gMnPc2	pták
jako	jako	k8xC	jako
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
farmy	farma	k1gFnPc1	farma
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
(	(	kIx(	(
<g/>
Little	Little	k1gFnSc1	Little
Karroo	Karroo	k1gMnSc1	Karroo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
chováno	chovat	k5eAaImNgNnS	chovat
na	na	k7c4	na
40.000	[number]	k4	40.000
pštrosů	pštros	k1gMnPc2	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Nejžádanějšími	žádaný	k2eAgFnPc7d3	nejžádanější
pštrosími	pštrosí	k2eAgFnPc7d1	pštrosí
pery	pero	k1gNnPc7	pero
jsou	být	k5eAaImIp3nP	být
černá	černý	k2eAgNnPc4d1	černé
samčí	samčí	k2eAgNnPc4d1	samčí
pera	pero	k1gNnPc4	pero
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
barvit	barvit	k5eAaImF	barvit
<g/>
.	.	kIx.	.
</s>
<s>
Pera	pero	k1gNnPc1	pero
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
sbírat	sbírat	k5eAaImF	sbírat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
pelichání	pelichání	k1gNnSc6	pelichání
pštrosů	pštros	k1gMnPc2	pštros
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k9	ještě
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
sběr	sběr	k1gInSc1	sběr
trvá	trvat	k5eAaImIp3nS	trvat
po	po	k7c4	po
celých	celý	k2eAgInPc2d1	celý
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pera	pero	k1gNnSc2	pero
mladých	mladý	k2eAgInPc2d1	mladý
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
letých	letý	k2eAgMnPc2d1	letý
pštrosů	pštros	k1gMnPc2	pštros
a	a	k8xC	a
pera	pero	k1gNnSc2	pero
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nevytrhávají	vytrhávat	k5eNaImIp3nP	vytrhávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stříhají	stříhat	k5eAaImIp3nP	stříhat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
bezbolestné	bezbolestný	k2eAgNnSc1d1	bezbolestné
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
pštrosů	pštros	k1gMnPc2	pštros
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
žádné	žádný	k3yNgFnPc4	žádný
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
pštrosími	pštrosí	k2eAgFnPc7d1	pštrosí
farmami	farma	k1gFnPc7	farma
např.	např.	kA	např.
i	i	k8xC	i
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosům	pštros	k1gMnPc3	pštros
postačuje	postačovat	k5eAaImIp3nS	postačovat
teplota	teplota	k1gFnSc1	teplota
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
<g/>
°	°	k?	°
C	C	kA	C
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	s	k7c7	s
pštrosími	pštrosí	k2eAgFnPc7d1	pštrosí
farmami	farma	k1gFnPc7	farma
setkáváme	setkávat	k5eAaImIp1nP	setkávat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
největší	veliký	k2eAgNnSc4d3	veliký
množství	množství	k1gNnSc4	množství
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
chováno	chovat	k5eAaImNgNnS	chovat
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
chovu	chov	k1gInSc2	chov
pštrosů	pštros	k1gMnPc2	pštros
je	být	k5eAaImIp3nS	být
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
poměr	poměr	k1gInSc4	poměr
množství	množství	k1gNnSc4	množství
krmiva	krmivo	k1gNnSc2	krmivo
ku	k	k7c3	k
hmotnostnímu	hmotnostní	k2eAgInSc3d1	hmotnostní
přírůstku	přírůstek	k1gInSc3	přírůstek
(	(	kIx(	(
<g/>
na	na	k7c4	na
3,5	[number]	k4	3,5
kg	kg	kA	kg
krmiva	krmivo	k1gNnSc2	krmivo
přírůstek	přírůstek	k1gInSc4	přírůstek
1	[number]	k4	1
kg	kg	kA	kg
živé	živý	k2eAgFnSc2d1	živá
váhy	váha	k1gFnSc2	váha
<g/>
)	)	kIx)	)
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
chovaných	chovaný	k2eAgNnPc2d1	chované
zvířat	zvíře	k1gNnPc2	zvíře
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
farmáře	farmář	k1gMnPc4	farmář
atraktivní	atraktivní	k2eAgMnPc1d1	atraktivní
(	(	kIx(	(
<g/>
dobytek	dobytek	k1gMnSc1	dobytek
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc4	tento
poměr	poměr	k1gInSc4	poměr
např.	např.	kA	např.
až	až	k6eAd1	až
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
především	především	k6eAd1	především
chováni	chovat	k5eAaImNgMnP	chovat
pro	pro	k7c4	pro
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pštrosi	pštros	k1gMnPc1	pštros
mají	mít	k5eAaImIp3nP	mít
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
komerčně	komerčně	k6eAd1	komerčně
využitelnou	využitelný	k2eAgFnSc4d1	využitelná
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosí	pštrosí	k2eAgNnSc1d1	pštrosí
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
chuťově	chuťově	k6eAd1	chuťově
podobné	podobný	k2eAgNnSc1d1	podobné
libovému	libový	k2eAgInSc3d1	libový
masu	masa	k1gFnSc4	masa
hovězímu	hovězí	k1gNnSc3	hovězí
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
jsou	být	k5eAaImIp3nP	být
dost	dost	k6eAd1	dost
velcí	velký	k2eAgMnPc1d1	velký
pro	pro	k7c4	pro
menší	malý	k2eAgMnPc4d2	menší
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
mohli	moct	k5eAaImAgMnP	moct
jezdit	jezdit	k5eAaImF	jezdit
<g/>
;	;	kIx,	;
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
se	se	k3xPyFc4	se
jezdec	jezdec	k1gMnSc1	jezdec
drží	držet	k5eAaImIp3nS	držet
pštrosa	pštros	k1gMnSc4	pštros
za	za	k7c4	za
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
byli	být	k5eAaImAgMnP	být
trénováni	trénovat	k5eAaImNgMnP	trénovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Arábie	Arábie	k1gFnSc1	Arábie
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
závodní	závodní	k2eAgMnPc1d1	závodní
koně	kůň	k1gMnPc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosí	pštrosí	k2eAgInPc1d1	pštrosí
závody	závod	k1gInPc1	závod
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
USA	USA	kA	USA
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
organizací	organizace	k1gFnPc2	organizace
Animal	animal	k1gMnSc1	animal
rights	rightsit	k5eAaPmRp2nS	rightsit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
dále	daleko	k6eAd2	daleko
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
nesnadnému	snadný	k2eNgNnSc3d1	nesnadné
sedlání	sedlání	k1gNnSc3	sedlání
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
a	a	k8xC	a
pštrosi	pštros	k1gMnPc1	pštros
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
nechvalně	chvalně	k6eNd1	chvalně
známí	známit	k5eAaImIp3nS	známit
svou	svůj	k3xOyFgFnSc7	svůj
popudlivou	popudlivý	k2eAgFnSc7d1	popudlivá
povahou	povaha	k1gFnSc7	povaha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tradiční	tradiční	k2eAgMnPc4d1	tradiční
obyvatele	obyvatel	k1gMnPc4	obyvatel
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Křováci	Křovák	k1gMnPc1	Křovák
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pštros	pštros	k1gMnSc1	pštros
běžnou	běžný	k2eAgFnSc4d1	běžná
lovnou	lovný	k2eAgFnSc4d1	lovná
zvěří	zvěří	k2eAgFnSc4d1	zvěří
<g/>
.	.	kIx.	.
</s>
<s>
Pštros	pštros	k1gMnSc1	pštros
je	být	k5eAaImIp3nS	být
loven	lovit	k5eAaImNgMnS	lovit
pomocí	pomocí	k7c2	pomocí
luku	luk	k1gInSc2	luk
a	a	k8xC	a
kromě	kromě	k7c2	kromě
masa	maso	k1gNnSc2	maso
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
jeho	jeho	k3xOp3gNnPc1	jeho
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
dělají	dělat	k5eAaImIp3nP	dělat
omelety	omeleta	k1gFnPc1	omeleta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
půlky	půlka	k1gFnPc1	půlka
skořápek	skořápka	k1gFnPc2	skořápka
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
na	na	k7c4	na
misky	miska	k1gFnPc4	miska
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
skořápka	skořápka	k1gFnSc1	skořápka
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
otvorem	otvor	k1gInSc7	otvor
má	mít	k5eAaImIp3nS	mít
použití	použití	k1gNnSc1	použití
jako	jako	k8xC	jako
nádoba	nádoba	k1gFnSc1	nádoba
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Rozbité	rozbitý	k2eAgFnPc1d1	rozbitá
skořápky	skořápka	k1gFnPc1	skořápka
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
šperky	šperk	k1gInPc1	šperk
a	a	k8xC	a
celá	celý	k2eAgNnPc1d1	celé
vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
vzácnými	vzácný	k2eAgInPc7d1	vzácný
pohřebními	pohřební	k2eAgInPc7d1	pohřební
dary	dar	k1gInPc7	dar
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
Masajové	Masaj	k1gMnPc1	Masaj
zase	zase	k9	zase
loví	lovit	k5eAaImIp3nP	lovit
pštrosy	pštros	k1gMnPc4	pštros
kvůli	kvůli	k7c3	kvůli
perům	pero	k1gNnPc3	pero
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
si	se	k3xPyFc3	se
zdobí	zdobit	k5eAaImIp3nS	zdobit
hlavu	hlava	k1gFnSc4	hlava
na	na	k7c6	na
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
obřadu	obřad	k1gInSc6	obřad
Eunoto	Eunota	k1gFnSc5	Eunota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zasvěcením	zasvěcení	k1gNnSc7	zasvěcení
stávají	stávat	k5eAaImIp3nP	stávat
válečníci	válečník	k1gMnPc1	válečník
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
jsou	být	k5eAaImIp3nP	být
pštrosi	pštros	k1gMnPc1	pštros
klasifikováni	klasifikován	k2eAgMnPc1d1	klasifikován
jako	jako	k8xS	jako
nebezpečná	bezpečný	k2eNgNnPc4d1	nebezpečné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
několik	několik	k4yIc1	několik
incidentů	incident	k1gInPc2	incident
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
napadeni	napadnout	k5eAaPmNgMnP	napadnout
a	a	k8xC	a
zabiti	zabít	k5eAaPmNgMnP	zabít
pštrosy	pštros	k1gMnPc4	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
samci	samec	k1gMnPc1	samec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
teritoriální	teritoriální	k2eAgFnSc7d1	teritoriální
a	a	k8xC	a
agresivní	agresivní	k2eAgFnSc7d1	agresivní
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
zaútočit	zaútočit	k5eAaPmF	zaútočit
silným	silný	k2eAgNnSc7d1	silné
kopáním	kopání	k1gNnSc7	kopání
svýma	svůj	k3xOyFgFnPc7	svůj
mocnýma	mocný	k2eAgFnPc7d1	mocná
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
dost	dost	k6eAd1	dost
silné	silný	k2eAgFnPc1d1	silná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rozpáraly	rozpárat	k5eAaPmAgInP	rozpárat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
větší	veliký	k2eAgNnSc4d2	veliký
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
chovají	chovat	k5eAaImIp3nP	chovat
pštrosy	pštros	k1gMnPc4	pštros
dvouprsté	dvouprstý	k2eAgNnSc1d1	dvouprsté
tyto	tento	k3xDgFnPc4	tento
velké	velká	k1gFnPc4	velká
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
:	:	kIx,	:
Praha-Trója	Praha-Trója	k1gFnSc1	Praha-Trója
<g/>
,	,	kIx,	,
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
a	a	k8xC	a
Lešná-Zlín	Lešná-Zlín	k1gInSc1	Lešná-Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
chováni	chovat	k5eAaImNgMnP	chovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
pštrosích	pštrosí	k2eAgFnPc6d1	pštrosí
farmách	farma	k1gFnPc6	farma
<g/>
.	.	kIx.	.
</s>
