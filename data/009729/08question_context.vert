<s>
Platónská	platónský	k2eAgNnPc4d1	platónské
tělesa	těleso	k1gNnPc4	těleso
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
řeckého	řecký	k2eAgMnSc2d1	řecký
filosofa	filosof	k1gMnSc2	filosof
Platóna	Platón	k1gMnSc2	Platón
(	(	kIx(	(
<g/>
427	[number]	k4	427
<g/>
–	–	k?	–
<g/>
347	[number]	k4	347
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
krychli	krychle	k1gFnSc4	krychle
<g/>
,	,	kIx,	,
osmistěn	osmistěn	k1gInSc1	osmistěn
<g/>
,	,	kIx,	,
čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
a	a	k8xC	a
dvacetistěn	dvacetistěn	k2eAgInSc1d1	dvacetistěn
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c2	za
představitele	představitel	k1gMnSc2	představitel
čtyř	čtyři	k4xCgInPc2	čtyři
základních	základní	k2eAgInPc2d1	základní
živlů	živel	k1gInPc2	živel
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
