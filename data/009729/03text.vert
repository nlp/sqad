<p>
<s>
Platónské	platónský	k2eAgNnSc1d1	platónské
těleso	těleso	k1gNnSc1	těleso
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
konvexní	konvexní	k2eAgInSc1d1	konvexní
mnohostěn	mnohostěn	k1gInSc1	mnohostěn
(	(	kIx(	(
<g/>
polyedr	polyedr	k1gInSc1	polyedr
<g/>
)	)	kIx)	)
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
vrcholu	vrchol	k1gInSc2	vrchol
vychází	vycházet	k5eAaImIp3nS	vycházet
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
hran	hrana	k1gFnPc2	hrana
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
stěny	stěna	k1gFnPc1	stěna
tvoří	tvořit	k5eAaImIp3nP	tvořit
shodné	shodný	k2eAgInPc4d1	shodný
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
mnohoúhelníky	mnohoúhelník	k1gInPc4	mnohoúhelník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
ukázka	ukázka	k1gFnSc1	ukázka
všech	všecek	k3xTgNnPc2	všecek
pěti	pět	k4xCc2	pět
Platónských	platónský	k2eAgNnPc2d1	platónské
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
prostoru	prostor	k1gInSc6	prostor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Tabulka	tabulka	k1gFnSc1	tabulka
vlastností	vlastnost	k1gFnPc2	vlastnost
platónských	platónský	k2eAgNnPc2d1	platónské
těles	těleso	k1gNnPc2	těleso
==	==	k?	==
</s>
</p>
<p>
<s>
Platónských	platónský	k2eAgNnPc2d1	platónské
těles	těleso	k1gNnPc2	těleso
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
euklidovském	euklidovský	k2eAgInSc6d1	euklidovský
prostoru	prostor	k1gInSc6	prostor
právě	právě	k9	právě
pět	pět	k4xCc4	pět
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Dualismus	dualismus	k1gInSc4	dualismus
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
tabulku	tabulka	k1gFnSc4	tabulka
je	být	k5eAaImIp3nS	být
nápadné	nápadný	k2eAgNnSc1d1	nápadné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
co	co	k9	co
např.	např.	kA	např.
krychle	krychle	k1gFnSc1	krychle
má	mít	k5eAaImIp3nS	mít
8	[number]	k4	8
vrcholů	vrchol	k1gInPc2	vrchol
a	a	k8xC	a
6	[number]	k4	6
stěn	stěna	k1gFnPc2	stěna
<g/>
,	,	kIx,	,
u	u	k7c2	u
osmistěnu	osmistěn	k1gInSc2	osmistěn
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
právě	právě	k6eAd1	právě
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
krychle	krychle	k1gFnSc1	krychle
duální	duální	k2eAgFnSc1d1	duální
k	k	k7c3	k
osmistěnu	osmistěn	k1gInSc3	osmistěn
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
dvanáctistěn	dvanáctistěn	k2eAgInSc1d1	dvanáctistěn
duální	duální	k2eAgInSc1d1	duální
k	k	k7c3	k
dvacetistěnu	dvacetistěn	k2eAgFnSc4d1	dvacetistěn
(	(	kIx(	(
<g/>
20	[number]	k4	20
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
,	,	kIx,	,
12	[number]	k4	12
stěn	stěna	k1gFnPc2	stěna
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
je	být	k5eAaImIp3nS	být
duální	duální	k2eAgMnSc1d1	duální
sám	sám	k3xTgMnSc1	sám
k	k	k7c3	k
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
vrcholy	vrchol	k1gInPc4	vrchol
a	a	k8xC	a
4	[number]	k4	4
stěny	stěna	k1gFnPc1	stěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Platónská	platónský	k2eAgNnPc4d1	platónské
tělesa	těleso	k1gNnPc4	těleso
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
řeckého	řecký	k2eAgMnSc2d1	řecký
filosofa	filosof	k1gMnSc2	filosof
Platóna	Platón	k1gMnSc2	Platón
(	(	kIx(	(
<g/>
427	[number]	k4	427
<g/>
–	–	k?	–
<g/>
347	[number]	k4	347
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
krychli	krychle	k1gFnSc4	krychle
<g/>
,	,	kIx,	,
osmistěn	osmistěn	k1gInSc1	osmistěn
<g/>
,	,	kIx,	,
čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
a	a	k8xC	a
dvacetistěn	dvacetistěn	k2eAgInSc1d1	dvacetistěn
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c2	za
představitele	představitel	k1gMnSc2	představitel
čtyř	čtyři	k4xCgInPc2	čtyři
základních	základní	k2eAgInPc2d1	základní
živlů	živel	k1gInPc2	živel
<g/>
:	:	kIx,	:
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáctistěn	Dvanáctistěn	k2eAgMnSc1d1	Dvanáctistěn
byl	být	k5eAaImAgMnS	být
představitelem	představitel	k1gMnSc7	představitel
jsoucna	jsoucno	k1gNnSc2	jsoucno
neboli	neboli	k8xC	neboli
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eukleidés	Eukleidés	k6eAd1	Eukleidés
sepsal	sepsat	k5eAaPmAgMnS	sepsat
kompletní	kompletní	k2eAgInSc4d1	kompletní
matematický	matematický	k2eAgInSc4d1	matematický
popis	popis	k1gInSc4	popis
Platonských	Platonský	k2eAgNnPc2d1	Platonský
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
Základech	základ	k1gInPc6	základ
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
XIII	XIII	kA	XIII
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
jejich	jejich	k3xOp3gFnPc3	jejich
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tvrzení	tvrzení	k1gNnSc1	tvrzení
13-17	[number]	k4	13-17
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
XIII	XIII	kA	XIII
popisují	popisovat	k5eAaImIp3nP	popisovat
stavbu	stavba	k1gFnSc4	stavba
čtyřstěnu	čtyřstěn	k1gInSc2	čtyřstěn
<g/>
,	,	kIx,	,
krychle	krychle	k1gFnSc2	krychle
<g/>
,	,	kIx,	,
osmistěnu	osmistěn	k1gInSc2	osmistěn
a	a	k8xC	a
dvanáctistěnu	dvanáctistěn	k2eAgFnSc4d1	dvanáctistěn
a	a	k8xC	a
dvacetistěnu	dvacetistěn	k2eAgFnSc4d1	dvacetistěn
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
Platonské	Platonský	k2eAgNnSc4d1	Platonský
těleso	těleso	k1gNnSc4	těleso
Euklid	Euklida	k1gFnPc2	Euklida
našel	najít	k5eAaPmAgInS	najít
poměr	poměr	k1gInSc1	poměr
průměru	průměr	k1gInSc2	průměr
opsané	opsaný	k2eAgFnSc2d1	opsaná
kulové	kulový	k2eAgFnSc2d1	kulová
plochy	plocha	k1gFnSc2	plocha
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
hrany	hrana	k1gFnSc2	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgInPc1	žádný
další	další	k2eAgInPc1d1	další
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
konvexní	konvexní	k2eAgInPc1d1	konvexní
mnohostěny	mnohostěn	k1gInPc1	mnohostěn
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
mezi	mezi	k7c4	mezi
šest	šest	k4xCc4	šest
tehdy	tehdy	k6eAd1	tehdy
známých	známý	k2eAgFnPc2d1	známá
planet	planeta	k1gFnPc2	planeta
vložit	vložit	k5eAaPmF	vložit
těchto	tento	k3xDgNnPc2	tento
pět	pět	k4xCc4	pět
platónských	platónský	k2eAgNnPc2d1	platónské
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
Merkur	Merkur	k1gInSc4	Merkur
a	a	k8xC	a
Venuši	Venuše	k1gFnSc4	Venuše
dal	dát	k5eAaPmAgInS	dát
osmistěn	osmistěn	k1gInSc1	osmistěn
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
Venuši	Venuše	k1gFnSc4	Venuše
a	a	k8xC	a
Zemi	zem	k1gFnSc4	zem
dvacetistěn	dvacetistěn	k2eAgMnSc1d1	dvacetistěn
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
Mars	Mars	k1gInSc4	Mars
dvanáctistěn	dvanáctistěn	k2eAgInSc4d1	dvanáctistěn
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
Mars	Mars	k1gInSc4	Mars
a	a	k8xC	a
Jupiter	Jupiter	k1gMnSc1	Jupiter
čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
a	a	k8xC	a
mezi	mezi	k7c4	mezi
Jupiter	Jupiter	k1gInSc4	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gInSc4	Saturn
krychli	krychle	k1gFnSc4	krychle
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
tělesa	těleso	k1gNnPc1	těleso
měla	mít	k5eAaImAgNnP	mít
představovat	představovat	k5eAaImF	představovat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
planetami	planeta	k1gFnPc7	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
vědy	věda	k1gFnPc1	věda
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
symetrii	symetrie	k1gFnSc3	symetrie
se	se	k3xPyFc4	se
platónská	platónský	k2eAgNnPc1d1	platónské
tělesa	těleso	k1gNnPc1	těleso
objevují	objevovat	k5eAaImIp3nP	objevovat
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
krystalografii	krystalografie	k1gFnSc6	krystalografie
<g/>
,	,	kIx,	,
krystalochemii	krystalochemie	k1gFnSc6	krystalochemie
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc6d1	molekulární
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
tvarů	tvar	k1gInPc2	tvar
krystalů	krystal	k1gInPc2	krystal
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
symetrií	symetrie	k1gFnSc7	symetrie
krystalové	krystalový	k2eAgFnSc2d1	krystalová
mřížky	mřížka	k1gFnSc2	mřížka
nabývá	nabývat	k5eAaImIp3nS	nabývat
forem	forum	k1gNnSc7	forum
platónských	platónský	k2eAgNnPc2d1	platónské
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
krystaly	krystal	k1gInPc4	krystal
běžné	běžný	k2eAgFnSc2d1	běžná
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
soli	sůl	k1gFnSc2	sůl
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
krychle	krychle	k1gFnSc2	krychle
<g/>
,	,	kIx,	,
u	u	k7c2	u
sfaleritu	sfalerit	k1gInSc2	sfalerit
někdy	někdy	k6eAd1	někdy
tvar	tvar	k1gInSc4	tvar
čtyřstěnu	čtyřstěn	k1gInSc2	čtyřstěn
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
symetrické	symetrický	k2eAgFnPc1d1	symetrická
molekuly	molekula	k1gFnPc1	molekula
mají	mít	k5eAaImIp3nP	mít
mnohdy	mnohdy	k6eAd1	mnohdy
tvar	tvar	k1gInSc4	tvar
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
<g/>
:	:	kIx,	:
methan	methan	k1gInSc1	methan
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
atomy	atom	k1gInPc4	atom
vodíku	vodík	k1gInSc2	vodík
ve	v	k7c6	v
vrcholech	vrchol	k1gInPc6	vrchol
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
čtyřstěnu	čtyřstěn	k1gInSc2	čtyřstěn
s	s	k7c7	s
uhlíkovým	uhlíkový	k2eAgInSc7d1	uhlíkový
atomem	atom	k1gInSc7	atom
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
těžišti	těžiště	k1gNnSc6	těžiště
<g/>
,	,	kIx,	,
molekula	molekula	k1gFnSc1	molekula
hexafluoridu	hexafluorid	k1gInSc2	hexafluorid
sírového	sírový	k2eAgInSc2d1	sírový
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
osmistěnu	osmistěn	k1gInSc2	osmistěn
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
dimenze	dimenze	k1gFnSc1	dimenze
==	==	k?	==
</s>
</p>
<p>
<s>
Pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
mnohostěny	mnohostěn	k1gInPc1	mnohostěn
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
dimenzích	dimenze	k1gFnPc6	dimenze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
čtyřrozměrném	čtyřrozměrný	k2eAgInSc6d1	čtyřrozměrný
prostoru	prostor	k1gInSc6	prostor
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-nadstěn	adstěn	k2eAgInSc1d1	-nadstěn
<g/>
,	,	kIx,	,
teserakt	teserakt	k1gInSc1	teserakt
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
-nadstěn	adstěn	k2eAgInSc1d1	-nadstěn
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
-nadstěn	adstěn	k2eAgInSc1d1	-nadstěn
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
-nadstěn	adstěn	k2eAgInSc1d1	-nadstěn
<g/>
,	,	kIx,	,
600	[number]	k4	600
<g/>
-nadstěn	adstěn	k2eAgInSc1d1	-nadstěn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
prostorech	prostor	k1gInPc6	prostor
dimenze	dimenze	k1gFnSc1	dimenze
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
čtyři	čtyři	k4xCgMnPc1	čtyři
existují	existovat	k5eAaImIp3nP	existovat
vždy	vždy	k6eAd1	vždy
právě	právě	k9	právě
tři	tři	k4xCgInPc1	tři
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
mnohostěny	mnohostěn	k1gInPc1	mnohostěn
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zobecnění	zobecnění	k1gNnSc1	zobecnění
čtyřstěnu	čtyřstěn	k1gInSc2	čtyřstěn
<g/>
,	,	kIx,	,
zobecnění	zobecnění	k1gNnSc1	zobecnění
krychle	krychle	k1gFnSc2	krychle
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
duální	duální	k2eAgNnSc1d1	duální
těleso	těleso	k1gNnSc1	těleso
–	–	k?	–
zobecnění	zobecnění	k1gNnSc1	zobecnění
osmistěnu	osmistěn	k1gInSc2	osmistěn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Platonic	Platonice	k1gFnPc2	Platonice
solid	solid	k1gInSc1	solid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Polopravidelná	Polopravidelný	k2eAgNnPc1d1	Polopravidelný
tělesa	těleso	k1gNnPc1	těleso
</s>
</p>
<p>
<s>
Čtyřrozměrná	čtyřrozměrný	k2eAgNnPc1d1	čtyřrozměrné
platónská	platónský	k2eAgNnPc1d1	platónské
tělesa	těleso	k1gNnPc1	těleso
</s>
</p>
<p>
<s>
n-rozměrná	nozměrný	k2eAgNnPc1d1	n-rozměrný
platónská	platónský	k2eAgNnPc1d1	platónské
tělesa	těleso	k1gNnPc1	těleso
</s>
</p>
<p>
<s>
Archimédovské	archimédovský	k2eAgNnSc1d1	archimédovský
těleso	těleso	k1gNnSc1	těleso
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Platónské	platónský	k2eAgFnSc2d1	platónská
těleso	těleso	k1gNnSc4	těleso
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.darius.cz/ag_nikola/cl_dvanacti.html	[url]	k1gMnSc1	http://www.darius.cz/ag_nikola/cl_dvanacti.html
</s>
</p>
<p>
<s>
http://telesa.wz.cz	[url]	k1gInSc1	http://telesa.wz.cz
(	(	kIx(	(
<g/>
databáze	databáze	k1gFnSc1	databáze
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
</s>
</p>
