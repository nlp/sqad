<p>
<s>
Lalokoploutví	lalokoploutvý	k2eAgMnPc1d1	lalokoploutvý
(	(	kIx(	(
<g/>
Coelacanthiformes	Coelacanthiformes	k1gMnSc1	Coelacanthiformes
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
Coelacanthimorpha	Coelacanthimorpha	k1gMnSc1	Coelacanthimorpha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podtřída	podtřída	k1gFnSc1	podtřída
nozdratých	nozdratý	k2eAgFnPc2d1	nozdratý
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Lalokoploutvé	lalokoploutvý	k2eAgFnPc1d1	lalokoploutvá
ryby	ryba	k1gFnPc1	ryba
žily	žít	k5eAaImAgFnP	žít
dle	dle	k7c2	dle
odhadů	odhad	k1gInPc2	odhad
před	před	k7c7	před
410	[number]	k4	410
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k6eAd1	jak
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
úlovky	úlovek	k1gInPc1	úlovek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
odchycena	odchytit	k5eAaPmNgFnS	odchytit
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
nejznámější	známý	k2eAgMnPc4d3	nejznámější
zástupce	zástupce	k1gMnPc4	zástupce
lalokoploutvých	lalokoploutvý	k2eAgInPc2d1	lalokoploutvý
–	–	k?	–
latimérie	latimérie	k1gFnSc1	latimérie
podivná	podivný	k2eAgFnSc1d1	podivná
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
latimérie	latimérie	k1gFnSc2	latimérie
podivné	podivný	k2eAgNnSc1d1	podivné
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgInSc1	jeden
recentní	recentní	k2eAgInSc1d1	recentní
druh	druh	k1gInSc1	druh
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
objevený	objevený	k2eAgMnSc1d1	objevený
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
–	–	k?	–
latimérie	latimérie	k1gFnSc1	latimérie
celebeská	celebeský	k2eAgFnSc1d1	celebeská
(	(	kIx(	(
<g/>
Latimeria	Latimerium	k1gNnPc1	Latimerium
menadoensis	menadoensis	k1gFnPc2	menadoensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lalokoploutví	lalokoploutvý	k2eAgMnPc1d1	lalokoploutvý
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
donedávna	donedávna	k6eAd1	donedávna
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
lalokoploutvé	lalokoploutvý	k2eAgFnPc1d1	lalokoploutvá
ryby	ryba	k1gFnPc1	ryba
vyhynuly	vyhynout	k5eAaPmAgFnP	vyhynout
před	před	k7c7	před
70	[number]	k4	70
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Silvio	Silvia	k1gMnSc5	Silvia
Renesto	Renesta	k1gMnSc5	Renesta
&	&	k?	&
Rudolf	Rudolf	k1gMnSc1	Rudolf
Stockar	Stockar	k1gMnSc1	Stockar
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
First	First	k1gMnSc1	First
record	record	k1gMnSc1	record
of	of	k?	of
a	a	k8xC	a
coelacanth	coelacanth	k1gMnSc1	coelacanth
fish	fish	k1gMnSc1	fish
from	from	k1gMnSc1	from
the	the	k?	the
Middle	Middle	k1gMnSc1	Middle
Triassic	Triassic	k1gMnSc1	Triassic
Meride	Merid	k1gInSc5	Merid
Limestone	Limeston	k1gInSc5	Limeston
of	of	k?	of
Monte	Mont	k1gMnSc5	Mont
San	San	k1gMnSc5	San
Giorgio	Giorgia	k1gMnSc5	Giorgia
(	(	kIx(	(
<g/>
Canton	Canton	k1gInSc4	Canton
Ticino	Ticino	k1gNnSc1	Ticino
<g/>
,	,	kIx,	,
Switzerland	Switzerland	k1gInSc1	Switzerland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rivista	Rivista	k1gMnSc1	Rivista
Italiana	Italian	k1gMnSc2	Italian
di	di	k?	di
Paleontologia	Paleontologius	k1gMnSc2	Paleontologius
e	e	k0	e
Stratigrafia	Stratigrafium	k1gNnPc1	Stratigrafium
(	(	kIx(	(
<g/>
Research	Research	k1gInSc1	Research
in	in	k?	in
Paleontology	paleontolog	k1gMnPc4	paleontolog
and	and	k?	and
Stratigraphy	Stratigraph	k1gMnPc7	Stratigraph
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
124	[number]	k4	124
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
639	[number]	k4	639
<g/>
-	-	kIx~	-
<g/>
653	[number]	k4	653
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
https://doi.org/10.13130/2039-4942/10771	[url]	k4	https://doi.org/10.13130/2039-4942/10771
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lalokoploutví	lalokoploutvý	k2eAgMnPc1d1	lalokoploutvý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
