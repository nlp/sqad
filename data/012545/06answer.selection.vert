<s>
Lalokoploutvé	lalokoploutvý	k2eAgFnPc1d1	lalokoploutvá
ryby	ryba	k1gFnPc1	ryba
žily	žít	k5eAaImAgFnP	žít
dle	dle	k7c2	dle
odhadů	odhad	k1gInPc2	odhad
před	před	k7c7	před
410	[number]	k4	410
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k6eAd1	jak
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
úlovky	úlovek	k1gInPc1	úlovek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
odchycena	odchytit	k5eAaPmNgFnS	odchytit
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
nejznámější	známý	k2eAgMnPc4d3	nejznámější
zástupce	zástupce	k1gMnPc4	zástupce
lalokoploutvých	lalokoploutvý	k2eAgInPc2d1	lalokoploutvý
–	–	k?	–
latimérie	latimérie	k1gFnSc1	latimérie
podivná	podivný	k2eAgFnSc1d1	podivná
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
