<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
popisuje	popisovat	k5eAaImIp3nS	popisovat
třetí	třetí	k4xOgFnSc4	třetí
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
ale	ale	k8xC	ale
hledáte	hledat	k5eAaImIp2nP	hledat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
soutěži	soutěž	k1gFnSc6	soutěž
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
ČFL	ČFL	kA	ČFL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
společně	společně	k6eAd1	společně
s	s	k7c7	s
Moravskou	moravský	k2eAgFnSc7d1	Moravská
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
ligou	liga	k1gFnSc7	liga
(	(	kIx(	(
<g/>
MSFL	MSFL	kA	MSFL
<g/>
)	)	kIx)	)
třetí	třetí	k4xOgFnSc7	třetí
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
soutěží	soutěž	k1gFnSc7	soutěž
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
řízena	řízen	k2eAgFnSc1d1	řízena
Řídicí	řídicí	k2eAgFnSc1d1	řídicí
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc1d1	herní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc1	soutěž
přerušena	přerušit	k5eAaPmNgFnS	přerušit
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
zbývající	zbývající	k2eAgNnPc1d1	zbývající
utkání	utkání	k1gNnPc1	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
polovina	polovina	k1gFnSc1	polovina
předepsaných	předepsaný	k2eAgInPc2d1	předepsaný
zápasů	zápas	k1gInPc2	zápas
sehraje	sehrát	k5eAaPmIp3nS	sehrát
před	před	k7c7	před
přestávkou	přestávka	k1gFnSc7	přestávka
a	a	k8xC	a
zbývající	zbývající	k2eAgFnSc1d1	zbývající
polovina	polovina	k1gFnSc1	polovina
v	v	k7c6	v
jarní	jarní	k2eAgFnSc6d1	jarní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
2020	[number]	k4	2020
se	se	k3xPyFc4	se
soutěž	soutěž	k1gFnSc1	soutěž
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
svou	svůj	k3xOyFgFnSc7	svůj
skupin	skupina	k1gFnPc2	skupina
A	A	kA	A
a	a	k8xC	a
B.	B.	kA	B.
Každá	každý	k3xTgFnSc1	každý
skupina	skupina	k1gFnSc1	skupina
má	mít	k5eAaImIp3nS	mít
16	[number]	k4	16
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
ČFL	ČFL	kA	ČFL
32	[number]	k4	32
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
byly	být	k5eAaImAgFnP	být
přesunuty	přesunut	k2eAgFnPc1d1	přesunuta
B-mužstva	Bužstvo	k1gNnPc4	B-mužstvo
prvoligových	prvoligový	k2eAgInPc2d1	prvoligový
a	a	k8xC	a
druholigových	druholigový	k2eAgInPc2d1	druholigový
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc4	každý
mužstvo	mužstvo	k1gNnSc4	mužstvo
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
soupeřem	soupeř	k1gMnSc7	soupeř
sehraje	sehrát	k5eAaPmIp3nS	sehrát
dvě	dva	k4xCgNnPc4	dva
utkání	utkání	k1gNnPc4	utkání
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
jednou	jednou	k6eAd1	jednou
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
domácím	domácí	k2eAgNnSc6d1	domácí
hřišti	hřiště	k1gNnSc6	hřiště
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
soupeřově	soupeřův	k2eAgNnSc6d1	soupeřovo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
se	se	k3xPyFc4	se
při	při	k7c6	při
nerozhodném	rozhodný	k2eNgInSc6d1	nerozhodný
stavu	stav	k1gInSc6	stav
na	na	k7c6	na
konci	konec	k1gInSc6	konec
utkání	utkání	k1gNnSc2	utkání
kopou	kopat	k5eAaImIp3nP	kopat
penalty	penalta	k1gFnPc4	penalta
a	a	k8xC	a
vítězné	vítězný	k2eAgNnSc4d1	vítězné
mužstvo	mužstvo	k1gNnSc4	mužstvo
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
penaltového	penaltový	k2eAgInSc2d1	penaltový
rozstřelu	rozstřel	k1gInSc2	rozstřel
získává	získávat	k5eAaImIp3nS	získávat
bod	bod	k1gInSc4	bod
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
tým	tým	k1gInSc1	tým
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Postupující	postupující	k2eAgInPc1d1	postupující
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
)	)	kIx)	)
postupuje	postupovat	k5eAaImIp3nS	postupovat
tým	tým	k1gInSc1	tým
umístěný	umístěný	k2eAgInSc1d1	umístěný
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sestupující	sestupující	k2eAgInPc1d1	sestupující
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Týmy	tým	k1gInPc1	tým
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
tabulky	tabulka	k1gFnSc2	tabulka
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
do	do	k7c2	do
příslušné	příslušný	k2eAgFnSc2d1	příslušná
divizní	divizní	k2eAgFnSc2d1	divizní
skupiny	skupina	k1gFnSc2	skupina
<g/>
:	:	kIx,	:
Divize	divize	k1gFnSc2	divize
A	A	kA	A
<g/>
,	,	kIx,	,
Divize	divize	k1gFnPc1	divize
B	B	kA	B
nebo	nebo	k8xC	nebo
Divize	divize	k1gFnSc2	divize
C.	C.	kA	C.
Počet	počet	k1gInSc1	počet
sestoupivších	sestoupivší	k2eAgInPc2d1	sestoupivší
týmů	tým	k1gInPc2	tým
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
je	být	k5eAaImIp3nS	být
odvislý	odvislý	k2eAgInSc1d1	odvislý
od	od	k7c2	od
počtu	počet	k1gInSc2	počet
sestupujících	sestupující	k2eAgInPc2d1	sestupující
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
sestoupí	sestoupit	k5eAaPmIp3nP	sestoupit
dva	dva	k4xCgInPc1	dva
české	český	k2eAgInPc1d1	český
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
z	z	k7c2	z
ČFL	ČFL	kA	ČFL
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
čtyři	čtyři	k4xCgInPc4	čtyři
poslední	poslední	k2eAgInPc4d1	poslední
týmy	tým	k1gInPc4	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
jeden	jeden	k4xCgInSc4	jeden
český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
moravský	moravský	k2eAgMnSc1d1	moravský
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
z	z	k7c2	z
ČFL	ČFL	kA	ČFL
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
tři	tři	k4xCgInPc4	tři
poslední	poslední	k2eAgInPc4d1	poslední
týmy	tým	k1gInPc4	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
nesestoupí	sestoupit	k5eNaPmIp3nS	sestoupit
žádný	žádný	k3yNgInSc1	žádný
český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dva	dva	k4xCgInPc1	dva
moravské	moravský	k2eAgInPc1d1	moravský
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
z	z	k7c2	z
ČFL	ČFL	kA	ČFL
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vítězové	vítěz	k1gMnPc1	vítěz
soutěže	soutěž	k1gFnSc2	soutěž
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vícenásobní	vícenásobný	k2eAgMnPc1d1	vícenásobný
vítězové	vítěz	k1gMnPc1	vítěz
===	===	k?	===
</s>
</p>
<p>
<s>
3	[number]	k4	3
–	–	k?	–
FK	FK	kA	FK
Bohemians	Bohemians	k1gInSc1	Bohemians
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
–	–	k?	–
FK	FK	kA	FK
Kolín	Kolín	k1gInSc1	Kolín
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
a	a	k8xC	a
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
Viktorie	Viktorie	k1gFnSc1	Viktorie
Jirny	Jirny	k1gInPc1	Jirny
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
–	–	k?	–
FK	FK	kA	FK
Ústí	ústit	k5eAaImIp3nS	ústit
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
–	–	k?	–
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
B	B	kA	B
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
–	–	k?	–
MFK	MFK	kA	MFK
Chrudim	Chrudim	k1gFnSc1	Chrudim
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
a	a	k8xC	a
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
</s>
</p>
