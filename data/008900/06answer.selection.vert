<s>
Česká	český	k2eAgFnSc1d1	Česká
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
ČFL	ČFL	kA	ČFL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
společně	společně	k6eAd1	společně
s	s	k7c7	s
Moravskou	moravský	k2eAgFnSc7d1	Moravská
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
ligou	liga	k1gFnSc7	liga
(	(	kIx(	(
<g/>
MSFL	MSFL	kA	MSFL
<g/>
)	)	kIx)	)
třetí	třetí	k4xOgFnSc7	třetí
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
soutěží	soutěž	k1gFnSc7	soutěž
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
