<s>
Jon	Jon	k?
Sníh	sníh	k1gInSc1
</s>
<s>
Jon	Jon	k?
Sníh	sníh	k1gInSc1
Cosplay	Cosplaa	k1gFnSc2
seriálového	seriálový	k2eAgInSc2d1
Jona	Jon	k1gInSc2
SněhaDílo	SněhaDílo	k1gNnSc1
</s>
<s>
Píseň	píseň	k1gFnSc1
ledu	led	k1gInSc2
a	a	k8xC
ohně	oheň	k1gInSc2
(	(	kIx(
<g/>
knihy	kniha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
První	první	k4xOgInSc1
výskyt	výskyt	k1gInSc1
</s>
<s>
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
(	(	kIx(
<g/>
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
Zima	zima	k1gFnSc1
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
)	)	kIx)
Tvůrce	tvůrce	k1gMnSc1
</s>
<s>
George	Georg	k1gInPc1
R.	R.	kA
R.	R.	kA
Martin	Martin	k1gMnSc1
Ztvárnil	ztvárnit	k5eAaPmAgMnS
<g/>
/	/	kIx~
<g/>
a	a	k8xC
</s>
<s>
Kit	kit	k1gInSc1
Harington	Harington	k1gInSc1
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
Informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jon	Jon		k1gMnSc1
Sníh	Sníh	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Jon	Jon	k1gMnSc1
Snow	Snow	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fiktivní	fiktivní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
z	z	k7c2
knižní	knižní	k2eAgFnSc2d1
ságy	sága	k1gFnSc2
Píseň	píseň	k1gFnSc1
ledu	led	k1gInSc2
a	a	k8xC
ohně	oheň	k1gInSc2
spisovatele	spisovatel	k1gMnSc2
George	George	k1gMnSc2
R.	R.	kA
R.	R.	kA
Martina	Martin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
televizní	televizní	k2eAgFnSc6d1
adaptaci	adaptace	k1gFnSc6
<g/>
,	,	kIx,
seriálu	seriál	k1gInSc6
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
<g/>
,	,	kIx,
jej	on	k3xPp3gInSc4
ztvárnil	ztvárnit	k5eAaPmAgMnS
herec	herec	k1gMnSc1
Kit	kit	k1gInSc4
Harington	Harington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
a	a	k8xC
v	v	k7c6
knize	kniha	k1gFnSc6
jedním	jeden	k4xCgMnSc7
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
vypravěčů	vypravěč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Knižní	knižní	k2eAgFnSc1d1
sága	sága	k1gFnSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
</s>
<s>
Jon	Jon	k?
Sníh	sníh	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
283	#num#	k4
AL	ala	k1gFnPc2
(	(	kIx(
<g/>
po	po	k7c6
Aegonově	Aegonův	k2eAgNnSc6d1
vylodění	vylodění	k1gNnSc6
<g/>
)	)	kIx)
Eddardu	Eddard	k1gMnSc3
Starkovi	Starek	k1gMnSc3
a	a	k8xC
neznámé	známý	k2eNgFnSc3d1
ženě	žena	k1gFnSc3
při	při	k7c6
Robertově	Robertův	k2eAgFnSc6d1
rebelii	rebelie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjmení	příjmení	k1gNnSc4
Sníh	sníh	k1gInSc4
dostávají	dostávat	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
nemanželské	manželský	k2eNgFnPc4d1
děti	dítě	k1gFnPc4
ze	z	k7c2
Severu	sever	k1gInSc2
Západozemí	Západozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nalezení	nalezení	k1gNnSc6
zlovlčích	zlovlčí	k2eAgNnPc2d1
štěňat	štěně	k1gNnPc2
přemluví	přemluvit	k5eAaPmIp3nS
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
je	on	k3xPp3gMnPc4
nezabíjí	zabíjet	k5eNaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
rozdělí	rozdělit	k5eAaPmIp3nS
je	být	k5eAaImIp3nS
mezi	mezi	k7c4
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
přijde	přijít	k5eAaPmIp3nS
k	k	k7c3
čistě	čistě	k6eAd1
bílému	bílý	k2eAgNnSc3d1
zlovlku	zlovlko	k1gNnSc3
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
pojmenuje	pojmenovat	k5eAaPmIp3nS
Duch	duch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nenávist	nenávist	k1gFnSc1
nevlastní	vlastní	k2eNgFnSc2d1
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
Catelyn	Catelyn	k1gNnSc1
Stark	Stark	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
Jon	Jon	k1gFnSc1
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
strýcem	strýc	k1gMnSc7
Benjenem	Benjen	k1gMnSc7
Starkem	Starek	k1gMnSc7
na	na	k7c4
Zeď	zeď	k1gFnSc4
<g/>
,	,	kIx,
přidat	přidat	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
Noční	noční	k2eAgFnSc3d1
hlídce	hlídka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1
bratři	bratr	k1gMnPc1
z	z	k7c2
hlídky	hlídka	k1gFnSc2
ho	on	k3xPp3gMnSc4
nejprve	nejprve	k6eAd1
nesnáší	snášet	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
si	se	k3xPyFc3
získá	získat	k5eAaPmIp3nS
respekt	respekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
se	se	k3xPyFc4
spřátelí	spřátelit	k5eAaPmIp3nS
s	s	k7c7
tlustým	tlustý	k2eAgMnSc7d1
hochem	hoch	k1gMnSc7
Samem	Sam	k1gMnSc7
(	(	kIx(
<g/>
Samwell	Samwell	k1gMnSc1
Tarly	Tarla	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
podobní	podobný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gNnSc3
donese	donést	k5eAaPmIp3nS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
smrti	smrt	k1gFnSc6
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
chce	chtít	k5eAaImIp3nS
odjet	odjet	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc4
by	by	kYmCp3nP
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
znamenalo	znamenat	k5eAaImAgNnS
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
muži	muž	k1gMnPc7
hlídky	hlídka	k1gFnSc2
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
za	za	k7c4
Zeď	zeď	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
děje	dít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Střet	střet	k1gInSc1
králů	král	k1gMnPc2
</s>
<s>
Po	po	k7c6
několika	několik	k4yIc6
dobrodružstvích	dobrodružství	k1gNnPc6
se	se	k3xPyFc4
Jon	Jon	k1gFnSc1
přidává	přidávat	k5eAaImIp3nS
k	k	k7c3
Divokým	divoký	k2eAgNnPc3d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
o	o	k7c6
nich	on	k3xPp3gMnPc6
něco	něco	k3yInSc1
mohl	moct	k5eAaImAgMnS
zjistit	zjistit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Bouře	bouře	k1gFnSc1
mečů	meč	k1gInPc2
</s>
<s>
U	u	k7c2
Divokých	divoký	k2eAgMnPc2d1
se	se	k3xPyFc4
zamiluje	zamilovat	k5eAaPmIp3nS
do	do	k7c2
dívky	dívka	k1gFnSc2
Ygritte	Ygritt	k1gMnSc5
a	a	k8xC
dokonce	dokonce	k9
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
jejich	jejich	k3xOp3gMnSc7
vůdcem	vůdce	k1gMnSc7
Mancem	Mance	k1gMnSc7
Nájezdníkem	nájezdník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vybrán	vybrat	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
člen	člen	k1gMnSc1
komanda	komando	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
jde	jít	k5eAaImIp3nS
zničit	zničit	k5eAaPmF
Černý	černý	k2eAgInSc1d1
hrad	hrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dezertuje	dezertovat	k5eAaBmIp3nS
však	však	k9
zpět	zpět	k6eAd1
k	k	k7c3
Noční	noční	k2eAgFnSc3d1
hlídce	hlídka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
s	s	k7c7
Jonovou	Jonový	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
a	a	k8xC
s	s	k7c7
nečekanou	čekaný	k2eNgFnSc7d1
posilou	posila	k1gFnSc7
v	v	k7c6
podobě	podoba	k1gFnSc6
armády	armáda	k1gFnSc2
Stannise	Stannise	k1gFnSc2
Baratheona	Baratheona	k1gFnSc1
Divoké	divoký	k2eAgNnSc1d1
poráží	porážet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jon	Jon	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
závěru	závěr	k1gInSc6
knihy	kniha	k1gFnSc2
zvolen	zvolen	k2eAgInSc4d1
lordem	lord	k1gMnSc7
velitelem	velitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Hostina	hostina	k1gFnSc1
pro	pro	k7c4
vrány	vrána	k1gFnPc4
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
knize	kniha	k1gFnSc6
Jon	Jon	k1gFnSc2
vystupuje	vystupovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
posílá	posílat	k5eAaImIp3nS
Sama	Sam	k1gMnSc4
a	a	k8xC
mistra	mistr	k1gMnSc4
Aemona	Aemon	k1gMnSc4
do	do	k7c2
Citadely	citadela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tanec	tanec	k1gInSc1
s	s	k7c7
draky	drak	k1gInPc7
</s>
<s>
Jon	Jon	k?
pořád	pořád	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
se	s	k7c7
Stannisem	Stannis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
pomoci	pomoct	k5eAaPmF
i	i	k9
Divokým	divoký	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nějaké	nějaký	k3yIgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
spojit	spojit	k5eAaPmF
hlídku	hlídka	k1gFnSc4
s	s	k7c7
Divokými	divoký	k2eAgInPc7d1
a	a	k8xC
sám	sám	k3xTgMnSc1
chce	chtít	k5eAaImIp3nS
vyrazit	vyrazit	k5eAaPmF
proti	proti	k7c3
Ramsayovi	Ramsaya	k1gMnSc3
Boltonovi	Bolton	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
drží	držet	k5eAaImIp3nS
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
bratra	bratr	k1gMnSc2
Rickona	Rickon	k1gMnSc2
Starka	starka	k1gFnSc1
a	a	k8xC
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zabil	zabít	k5eAaPmAgMnS
Stannise	Stannise	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
když	když	k8xS
se	se	k3xPyFc4
Jon	Jon	k1gMnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
vyrazit	vyrazit	k5eAaPmF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pobodán	pobodat	k5eAaPmNgMnS
muži	muž	k1gMnPc7
Noční	noční	k2eAgFnSc2d1
hlídky	hlídka	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
s	s	k7c7
ním	on	k3xPp3gInSc7
nesouhlasí	souhlasit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Televizní	televizní	k2eAgFnSc1d1
adaptace	adaptace	k1gFnSc1
</s>
<s>
Kit	kit	k1gInSc1
Harington	Harington	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
Jona	Jona	k1gMnSc1
Sněha	Sněha	k1gMnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
</s>
<s>
Osudy	osud	k1gInPc1
seriálového	seriálový	k2eAgNnSc2d1
Jona	Jonum	k1gNnSc2
Sněha	Sněha	k1gFnSc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
podobné	podobný	k2eAgFnSc3d1
knižní	knižní	k2eAgFnSc3d1
předloze	předloha	k1gFnSc3
a	a	k8xC
pokračují	pokračovat	k5eAaImIp3nP
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pátá	pátá	k1gFnSc1
série	série	k1gFnSc2
seriálu	seriál	k1gInSc2
končí	končit	k5eAaImIp3nS
scénou	scéna	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
Jon	Jon	k1gMnSc3
Sníh	sníh	k1gInSc4
zabit	zabit	k2eAgInSc4d1
bratry	bratr	k1gMnPc7
z	z	k7c2
Noční	noční	k2eAgFnSc2d1
hlídky	hlídka	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nesouhlasí	souhlasit	k5eNaImIp3nP
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
spojenectvím	spojenectví	k1gNnSc7
s	s	k7c7
Divokými	divoký	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
šesté	šestý	k4xOgFnSc6
sérii	série	k1gFnSc6
je	být	k5eAaImIp3nS
oživen	oživit	k5eAaPmNgInS
rudou	rudý	k2eAgFnSc7d1
kněžkou	kněžka	k1gFnSc7
Mellisandrou	Mellisandra	k1gFnSc7
<g/>
,	,	kIx,
opustí	opustit	k5eAaPmIp3nP
Noční	noční	k2eAgFnSc4d1
hlídku	hlídka	k1gFnSc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
se	s	k7c7
sestrou	sestra	k1gFnSc7
Sansou	Sansa	k1gFnSc7
získá	získat	k5eAaPmIp3nS
spojence	spojenec	k1gMnPc4
a	a	k8xC
porazí	porazit	k5eAaPmIp3nP
rod	rod	k1gInSc4
Boltonů	Bolton	k1gInPc2
<g/>
,	,	kIx,
získá	získat	k5eAaPmIp3nS
zpět	zpět	k6eAd1
Zimohrad	Zimohrad	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
svými	svůj	k3xOyFgMnPc7
vazaly	vazal	k1gMnPc7
prohlášen	prohlášen	k2eAgInSc4d1
králem	král	k1gMnSc7
Severu	sever	k1gInSc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
předtím	předtím	k6eAd1
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Robb	Robb	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bran	brána	k1gFnPc2
Stark	Stark	k1gInSc1
mezitím	mezitím	k6eAd1
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Jon	Jon	k1gMnSc1
není	být	k5eNaImIp3nS
synem	syn	k1gMnSc7
Eddarda	Eddard	k1gMnSc2
Starka	starka	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnPc4
sestry	sestra	k1gFnPc4
Lyanny	Lyanna	k1gMnSc2
a	a	k8xC
Rhaegara	Rhaegar	k1gMnSc2
Targaryena	Targaryen	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sedmé	sedmý	k4xOgFnSc6
sérii	série	k1gFnSc6
se	se	k3xPyFc4
Jon	Jon	k1gFnSc1
Sníh	sníh	k1gInSc1
vypraví	vypravit	k5eAaPmIp3nS
na	na	k7c4
Dračí	dračí	k2eAgInSc4d1
kámen	kámen	k1gInSc4
za	za	k7c4
Daenerys	Daenerys	k1gInSc4
Targaryen	Targaryna	k1gFnPc2
a	a	k8xC
podaří	podařit	k5eAaPmIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gInSc3
ji	on	k3xPp3gFnSc4
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gMnSc7
společným	společný	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
jsou	být	k5eAaImIp3nP
Bílí	bílý	k2eAgMnPc1d1
chodci	chodec	k1gMnPc1
na	na	k7c6
Severu	sever	k1gInSc6
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
Daenerys	Daenerys	k1gInSc1
přijme	přijmout	k5eAaPmIp3nS
jako	jako	k9
svou	svůj	k3xOyFgFnSc4
královnu	královna	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
předtím	předtím	k6eAd1
odmítal	odmítat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
vyjednat	vyjednat	k5eAaPmF
příměří	příměří	k1gNnSc4
s	s	k7c7
Lannistery	Lannister	k1gMnPc7
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
odeberou	odebrat	k5eAaPmIp3nP
zpět	zpět	k6eAd1
na	na	k7c4
Sever	sever	k1gInSc4
<g/>
,	,	kIx,
mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
také	také	k9
intimně	intimně	k6eAd1
sblíží	sblížit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bran	brána	k1gFnPc2
Stark	Stark	k1gInSc1
a	a	k8xC
Samwell	Samwell	k1gInSc1
Tarly	Tarla	k1gFnSc2
se	se	k3xPyFc4
setkají	setkat	k5eAaPmIp3nP
na	na	k7c4
Zimohradu	Zimohrada	k1gFnSc4
a	a	k8xC
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Jon	Jon	k1gMnSc1
není	být	k5eNaImIp3nS
nemanželský	manželský	k2eNgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
manželský	manželský	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Lyanny	Lyanna	k1gFnSc2
a	a	k8xC
Rhaegara	Rhaegar	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
pravé	pravý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
je	být	k5eAaImIp3nS
Aegon	Aegon	k1gMnSc1
Targaryen	Targaryen	k2eAgMnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
dědicem	dědic	k1gMnSc7
Železného	železný	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
králem	král	k1gMnSc7
se	se	k3xPyFc4
nestane	stanout	k5eNaPmIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
po	po	k7c4
zabití	zabití	k1gNnSc4
Daenerys	Daenerysa	k1gFnPc2
Targaryen	Targaryna	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
předtím	předtím	k6eAd1
porazili	porazit	k5eAaPmAgMnP
Nočního	noční	k2eAgMnSc4d1
krále	král	k1gMnSc4
a	a	k8xC
zlikvidovali	zlikvidovat	k5eAaPmAgMnP
hrozbu	hrozba	k1gFnSc4
zničení	zničení	k1gNnSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
nucen	nucen	k2eAgMnSc1d1
odejít	odejít	k5eAaPmF
do	do	k7c2
vyhnanství	vyhnanství	k1gNnSc2
za	za	k7c4
Zeď	zeď	k1gFnSc4
<g/>
.	.	kIx.
<g/>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MARTIN	Martin	k1gInSc1
<g/>
,	,	kIx,
George	George	k1gFnSc1
Raymond	Raymond	k1gMnSc1
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Hana	Hana	k1gFnSc1
Březáková	Březáková	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
opravené	opravený	k2eAgFnSc2d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
TALPRESS	TALPRESS	kA
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
848	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7197	#num#	k4
<g/>
-	-	kIx~
<g/>
412	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Profil	profil	k1gInSc1
rodu	rod	k1gInSc2
Starků	Starek	k1gMnPc2
na	na	k7c6
HBO	HBO	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Profil	profil	k1gInSc1
Jona	Jon	k1gInSc2
na	na	k7c6
Edna	Edna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Profil	profil	k1gInSc1
Jona	Jonum	k1gNnSc2
na	na	k7c4
postavy	postava	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Jon	Jon	k1gFnSc1
Sníh	sníh	k1gInSc1
na	na	k7c6
A	A	kA
Wiki	Wiki	k1gNnSc1
of	of	k?
Ice	Ice	k1gMnSc2
and	and	k?
Fire	Fir	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Píseň	píseň	k1gFnSc1
ledu	led	k1gInSc2
a	a	k8xC
ohně	oheň	k1gInSc2
od	od	k7c2
George	Georg	k1gMnSc2
R.	R.	kA
R.	R.	kA
Martina	Martin	k1gMnSc2
Knihy	kniha	k1gFnSc2
</s>
<s>
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Střet	střet	k1gInSc4
králů	král	k1gMnPc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bouře	bouře	k1gFnSc1
mečů	meč	k1gInPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hostina	hostina	k1gFnSc1
pro	pro	k7c4
vrány	vrána	k1gFnPc4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tanec	tanec	k1gInSc1
s	s	k7c7
draky	drak	k1gInPc7
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc2
Winds	Windsa	k1gFnPc2
of	of	k?
Winter	Winter	k1gMnSc1
(	(	kIx(
<g/>
připravovaná	připravovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
A	a	k8xC
Dream	Dream	k1gInSc1
of	of	k?
Spring	Spring	k1gInSc1
(	(	kIx(
<g/>
připravovaná	připravovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
Postavy	postav	k1gInPc1
</s>
<s>
Petyr	Petyr	k1gMnSc1
Baeliš	Baeliš	k1gMnSc1
•	•	k?
Joffrey	Joffrea	k1gFnSc2
Baratheon	Baratheon	k1gMnSc1
•	•	k?
Renly	Renla	k1gFnSc2
Baratheon	Baratheon	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
Baratheon	Baratheon	k1gMnSc1
•	•	k?
Stannis	Stannis	k1gInSc1
Baratheon	Baratheon	k1gMnSc1
•	•	k?
Tommen	Tommen	k2eAgMnSc1d1
Baratheon	Baratheon	k1gMnSc1
•	•	k?
Ramsay	Ramsay	k1gInPc1
Bolton	Bolton	k1gInSc1
•	•	k?
Roose	Roose	k1gFnSc1
Bolton	Bolton	k1gInSc1
•	•	k?
Bronn	Bronn	k1gInSc1
•	•	k?
Sandor	Sandor	k1gInSc1
Clegane	Clegan	k1gMnSc5
•	•	k?
Khal	Khal	k1gInSc1
Drogo	droga	k1gFnSc5
•	•	k?
Tormund	Tormund	k1gMnSc1
Obrozhouba	Obrozhouba	k1gMnSc1
•	•	k?
Theon	Theon	k1gMnSc1
Greyjoy	Greyjoa	k1gFnSc2
•	•	k?
Cersei	Cerse	k1gFnSc2
Lannister	Lannister	k1gMnSc1
•	•	k?
Jaime	Jaim	k1gInSc5
Lannister	Lannister	k1gInSc4
•	•	k?
Tyrion	Tyrion	k1gInSc1
Lannister	Lannister	k1gMnSc1
•	•	k?
Tywin	Tywin	k1gMnSc1
Lannister	Lannister	k1gMnSc1
•	•	k?
Oberyn	Oberyn	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Martell	Martell	k1gInSc1
•	•	k?
Melisandra	Melisandr	k1gMnSc2
z	z	k7c2
Ašaje	Ašaj	k1gFnSc2
•	•	k?
Jorah	Jorah	k1gMnSc1
Mormont	Mormont	k1gMnSc1
•	•	k?
Davos	Davos	k1gMnSc1
Mořský	mořský	k2eAgMnSc1d1
•	•	k?
Daario	Daario	k1gMnSc1
Naharis	Naharis	k1gFnSc2
•	•	k?
Jon	Jon	k1gFnSc1
Sníh	sníh	k1gInSc1
•	•	k?
Arya	Ary	k1gInSc2
Stark	Stark	k1gInSc1
•	•	k?
Brandon	Brandon	k1gInSc1
„	„	k?
<g/>
Bran	brána	k1gFnPc2
<g/>
“	“	k?
Stark	Stark	k1gInSc1
•	•	k?
Catelyn	Catelyn	k1gInSc1
Stark	Stark	k1gInSc1
•	•	k?
Eddard	Eddard	k1gInSc1
Stark	Stark	k1gInSc1
•	•	k?
Robb	Robb	k1gInSc1
Stark	Stark	k1gInSc1
•	•	k?
Sansa	Sansa	k1gFnSc1
Stark	Stark	k1gInSc1
•	•	k?
Daenerys	Daenerys	k1gInSc4
Targaryen	Targaryen	k2eAgInSc4d1
•	•	k?
Viserys	Viserys	k1gInSc4
Targaryen	Targaryen	k2eAgInSc4d1
•	•	k?
Samwell	Samwell	k1gInSc4
Tarly	Tarla	k1gMnSc2
•	•	k?
Brienne	Brienn	k1gInSc5
z	z	k7c2
Tarthu	Tarth	k1gInSc2
•	•	k?
Margaery	Margaera	k1gFnSc2
Tyrell	Tyrell	k1gMnSc1
•	•	k?
Olenna	Olenna	k1gFnSc1
Tyrell	Tyrell	k1gInSc1
•	•	k?
Varys	Varys	k1gInSc1
•	•	k?
Ygritte	Ygritt	k1gInSc5
Svět	svět	k1gInSc1
</s>
<s>
dothračtina	dothračtina	k1gFnSc1
•	•	k?
valyrijština	valyrijština	k1gFnSc1
•	•	k?
Západozemí	Západozemí	k1gNnSc2
•	•	k?
Essos	Essosa	k1gFnPc2
TV	TV	kA
seriál	seriál	k1gInSc1
</s>
<s>
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
•	•	k?
seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
•	•	k?
seznam	seznam	k1gInSc1
postav	postava	k1gFnPc2
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Aidan	Aidan	k1gMnSc1
Gillen	Gillna	k1gFnPc2
•	•	k?
Alfie	Alfie	k1gFnSc2
Allen	Allen	k1gMnSc1
•	•	k?
Carice	Carice	k1gFnSc1
van	vana	k1gFnPc2
Houten	Houten	k2eAgInSc1d1
•	•	k?
Conleth	Conleth	k1gInSc1
Hill	Hill	k1gMnSc1
•	•	k?
Dean-Charles	Dean-Charles	k1gMnSc1
Chapman	Chapman	k1gMnSc1
•	•	k?
Emilia	Emilius	k1gMnSc4
Clarke	Clark	k1gMnSc4
•	•	k?
Gwendoline	Gwendolin	k1gInSc5
Christie	Christie	k1gFnPc4
•	•	k?
Hannah	Hannah	k1gInSc1
Murrayová	Murrayová	k1gFnSc1
•	•	k?
Harry	Harra	k1gFnSc2
Lloyd	Lloyd	k1gMnSc1
•	•	k?
Charles	Charles	k1gMnSc1
Dance	Danka	k1gFnSc3
•	•	k?
Iain	Iain	k1gMnSc1
Glen	Glen	k1gMnSc1
•	•	k?
Indira	Indira	k1gMnSc1
Varma	Varmum	k1gNnSc2
•	•	k?
Isaac	Isaac	k1gFnSc1
Hempstead	Hempstead	k1gInSc1
Wright	Wright	k1gMnSc1
•	•	k?
Iwan	Iwan	k1gMnSc1
Rheon	Rheon	k1gMnSc1
•	•	k?
Jack	Jack	k1gMnSc1
Gleeson	Gleeson	k1gMnSc1
•	•	k?
James	James	k1gMnSc1
Cosmo	Cosma	k1gFnSc5
•	•	k?
Jerome	Jerom	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
Flynn	Flynn	k1gMnSc1
•	•	k?
Jason	Jason	k1gMnSc1
Momoa	Momoa	k1gMnSc1
•	•	k?
Joe	Joe	k1gMnSc1
Dempsie	Dempsie	k1gFnSc2
•	•	k?
John	John	k1gMnSc1
Bradley	Bradlea	k1gFnSc2
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Pryce	Pryce	k1gMnSc1
•	•	k?
Kit	kit	k1gInSc1
Harington	Harington	k1gInSc1
•	•	k?
Kristofer	Kristofer	k1gInSc1
Hivju	Hivju	k1gFnSc1
•	•	k?
Lena	Lena	k1gFnSc1
Headey	Headea	k1gFnSc2
•	•	k?
Liam	Liam	k1gInSc1
Cunningham	Cunningham	k1gInSc1
•	•	k?
Maisie	Maisie	k1gFnSc2
Williams	Williamsa	k1gFnPc2
•	•	k?
Mark	Mark	k1gMnSc1
Addy	Adda	k1gFnSc2
•	•	k?
Michael	Michael	k1gMnSc1
McElhatton	McElhatton	k1gInSc1
•	•	k?
Michelle	Michell	k1gMnSc2
Fairley	Fairlea	k1gMnSc2
•	•	k?
Michiel	Michiel	k1gMnSc1
Huisman	Huisman	k1gMnSc1
•	•	k?
Natalie	Natalie	k1gFnSc2
Dormer	Dormer	k1gMnSc1
•	•	k?
Nathalie	Nathalie	k1gFnSc2
Emmanuel	Emmanuel	k1gMnSc1
•	•	k?
Nikolaj	Nikolaj	k1gMnSc1
Coster-Waldau	Coster-Waldaus	k1gInSc2
•	•	k?
Oona	Oona	k1gMnSc1
Chaplin	Chaplin	k2eAgMnSc1d1
•	•	k?
Peter	Peter	k1gMnSc1
Dinklage	Dinklag	k1gFnSc2
•	•	k?
Richard	Richard	k1gMnSc1
Madden	Maddno	k1gNnPc2
•	•	k?
Rory	Rora	k1gFnSc2
McCann	McCann	k1gMnSc1
•	•	k?
Rose	Rose	k1gMnSc1
Leslie	Leslie	k1gFnSc2
•	•	k?
Sean	Sean	k1gMnSc1
Bean	Bean	k1gMnSc1
•	•	k?
Sibel	Sibel	k1gMnSc1
Kekili	Kekili	k1gMnSc1
•	•	k?
Sophie	Sophie	k1gFnSc2
Turner	turner	k1gMnSc1
•	•	k?
Stephen	Stephen	k1gInSc1
Dillane	Dillan	k1gMnSc5
•	•	k?
Tom	Tom	k1gMnSc1
Wlaschiha	Wlaschiha	k1gMnSc1
</s>
