<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Dědek	Dědek	k1gMnSc1	Dědek
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1910	[number]	k4	1910
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
–	–	k?	–
???	???	k?	???
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
poúnorový	poúnorový	k2eAgMnSc1d1	poúnorový
poslanec	poslanec	k1gMnSc1	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c2	za
KSČ	KSČ	kA	KSČ
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSSR	ČSSR	kA	ČSSR
za	za	k7c4	za
Severočeský	severočeský	k2eAgInSc4d1	severočeský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
nabyl	nabýt	k5eAaPmAgInS	nabýt
až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1961	[number]	k4	1961
jako	jako	k8xC	jako
náhradník	náhradník	k1gMnSc1	náhradník
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poslanec	poslanec	k1gMnSc1	poslanec
Václav	Václav	k1gMnSc1	Václav
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
<g/>
Profesně	profesně	k6eAd1	profesně
se	se	k3xPyFc4	se
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1961	[number]	k4	1961
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
Severočeského	severočeský	k2eAgNnSc2d1	Severočeské
krajského	krajský	k2eAgNnSc2d1	krajské
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
aktivním	aktivní	k2eAgMnSc7d1	aktivní
komunistou	komunista	k1gMnSc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nositelem	nositel	k1gMnSc7	nositel
několika	několik	k4yIc2	několik
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Československá	československý	k2eAgFnSc1d1	Československá
cena	cena	k1gFnSc1	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
za	za	k7c2	za
počínající	počínající	k2eAgFnSc2d1	počínající
normalizace	normalizace	k1gFnSc2	normalizace
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
čistkách	čistka	k1gFnPc6	čistka
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
a	a	k8xC	a
novinářské	novinářský	k2eAgFnSc6d1	novinářská
obci	obec	k1gFnSc6	obec
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
sám	sám	k3xTgMnSc1	sám
jako	jako	k8xS	jako
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc2	několik
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
regionálních	regionální	k2eAgNnPc6d1	regionální
tématech	téma	k1gNnPc6	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Dědek	Dědek	k1gMnSc1	Dědek
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Dědek	Dědek	k1gMnSc1	Dědek
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
</s>
</p>
