<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Anaklét	Anaklét	k1gMnSc1	Anaklét
(	(	kIx(	(
<g/>
Anacletus	Anacletus	k1gMnSc1	Anacletus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
třetího	třetí	k4xOgMnSc2	třetí
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
a	a	k8xC	a
tedy	tedy	k9	tedy
třetího	třetí	k4xOgMnSc4	třetí
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
