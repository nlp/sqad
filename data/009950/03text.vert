<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Anaklét	Anaklét	k1gMnSc1	Anaklét
(	(	kIx(	(
<g/>
Anacletus	Anacletus	k1gMnSc1	Anacletus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
třetího	třetí	k4xOgMnSc2	třetí
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
a	a	k8xC	a
tedy	tedy	k9	tedy
třetího	třetí	k4xOgMnSc4	třetí
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
je	být	k5eAaImIp3nS	být
datován	datován	k2eAgMnSc1d1	datován
do	do	k7c2	do
let	léto	k1gNnPc2	léto
78	[number]	k4	78
<g/>
/	/	kIx~	/
<g/>
79	[number]	k4	79
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
se	se	k3xPyFc4	se
spory	spor	k1gInPc7	spor
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
se	s	k7c7	s
svatým	svatý	k2eAgInSc7d1	svatý
Klétem	Klét	k1gInSc7	Klét
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
svatý	svatý	k2eAgMnSc1d1	svatý
Klétus	Klétus	k1gMnSc1	Klétus
a	a	k8xC	a
svatý	svatý	k2eAgMnSc1d1	svatý
Anaklétus	Anaklétus	k1gMnSc1	Anaklétus
za	za	k7c4	za
tutéž	týž	k3xTgFnSc4	týž
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
na	na	k7c6	na
biskupa	biskup	k1gMnSc2	biskup
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
svatým	svatý	k1gMnSc7	svatý
Petrem	Petr	k1gMnSc7	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vynikal	vynikat	k5eAaImAgMnS	vynikat
učeností	učenost	k1gFnSc7	učenost
a	a	k8xC	a
svatým	svatý	k2eAgInSc7d1	svatý
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
více	hodně	k6eAd2	hodně
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisováno	připisován	k2eAgNnSc1d1	připisováno
rozdělení	rozdělení	k1gNnSc1	rozdělení
Říma	Řím	k1gInSc2	Řím
na	na	k7c4	na
25	[number]	k4	25
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
použil	použít	k5eAaPmAgMnS	použít
formule	formule	k1gFnPc4	formule
<g/>
:	:	kIx,	:
Salutem	salut	k1gInSc7	salut
et	et	k?	et
benedictionem	benediction	k1gInSc7	benediction
apostolicam	apostolicam	k1gInSc4	apostolicam
–	–	k?	–
trvalé	trvalý	k2eAgFnSc2d1	trvalá
součásti	součást	k1gFnSc2	součást
papežských	papežský	k2eAgInPc2d1	papežský
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanským	křesťanský	k2eAgMnSc7d1	křesťanský
duchovním	duchovní	k1gMnSc7	duchovní
nařídil	nařídit	k5eAaPmAgMnS	nařídit
nosit	nosit	k5eAaImF	nosit
krátké	krátký	k2eAgInPc4d1	krátký
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
mučednické	mučednický	k2eAgFnSc2d1	mučednická
smrti	smrt	k1gFnSc2	smrt
sv.	sv.	kA	sv.
Anakléta	Anaklét	k1gMnSc2	Anaklét
je	být	k5eAaImIp3nS	být
nepotvrzená	potvrzený	k2eNgFnSc1d1	nepotvrzená
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
jsou	být	k5eAaImIp3nP	být
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Lina	Lina	k1gFnSc1	Lina
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
<g/>
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
<g/>
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anaklét	Anakléta	k1gFnPc2	Anakléta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Anaklét	Anakléta	k1gFnPc2	Anakléta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
