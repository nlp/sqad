<s>
Protože	protože	k8xS	protože
sám	sám	k3xTgMnSc1	sám
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
ani	ani	k8xC	ani
stavebních	stavební	k2eAgFnPc6d1	stavební
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
pomoc	pomoc	k1gFnSc4	pomoc
u	u	k7c2	u
Vidjadhár	Vidjadhár	k1gInSc4	Vidjadhár
Bhattačárji	Bhattačárje	k1gFnSc4	Bhattačárje
<g/>
,	,	kIx,	,
bráhmanského	bráhmanský	k2eAgMnSc4d1	bráhmanský
učence	učenec	k1gMnSc4	učenec
z	z	k7c2	z
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgInS	svěřit
mu	on	k3xPp3gMnSc3	on
plánování	plánování	k1gNnSc1	plánování
architektury	architektura	k1gFnSc2	architektura
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
