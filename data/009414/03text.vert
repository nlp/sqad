<p>
<s>
Džajpur	Džajpur	k1gMnSc1	Džajpur
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgInSc1d1	anglický
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Jaipur	Jaipur	k1gMnSc1	Jaipur
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
,	,	kIx,	,
dévanágarí	dévanágarí	k1gFnSc1	dévanágarí
<g/>
:	:	kIx,	:
ज	ज	k?	ज
<g/>
ु	ु	k?	ु
<g/>
र	र	k?	र
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
často	často	k6eAd1	často
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
růžové	růžový	k2eAgNnSc1d1	růžové
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
indického	indický	k2eAgInSc2d1	indický
státu	stát	k1gInSc2	stát
Rádžasthán	Rádžasthán	k2eAgMnSc1d1	Rádžasthán
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
mahárádžou	mahárádža	k1gMnSc7	mahárádža
Saváí	Saváí	k1gMnSc1	Saváí
Džaj	Džaj	k1gMnSc1	Džaj
Singhem	Singh	k1gInSc7	Singh
II	II	kA	II
<g/>
,	,	kIx,	,
vládcem	vládce	k1gMnSc7	vládce
Amberu	Amber	k1gInSc2	Amber
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Džajpur	Džajpur	k1gMnSc1	Džajpur
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
pečlivě	pečlivě	k6eAd1	pečlivě
naplánované	naplánovaný	k2eAgNnSc4d1	naplánované
město	město	k1gNnSc4	město
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dříve	dříve	k6eAd2	dříve
bývalo	bývat	k5eAaImAgNnS	bývat
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
království	království	k1gNnSc2	království
a	a	k8xC	a
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
města	město	k1gNnSc2	město
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
vkus	vkus	k1gInSc1	vkus
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
ostatní	ostatní	k2eAgNnPc4d1	ostatní
indická	indický	k2eAgNnPc4d1	indické
města	město	k1gNnPc4	město
z	z	k7c2	z
předmoderní	předmoderní	k2eAgFnSc2d1	předmoderní
éry	éra	k1gFnSc2	éra
vyniká	vynikat	k5eAaImIp3nS	vynikat
Džajpur	Džajpur	k1gMnSc1	Džajpur
především	především	k9	především
šířkou	šířka	k1gFnSc7	šířka
a	a	k8xC	a
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
do	do	k7c2	do
šesti	šest	k4xCc2	šest
sektorů	sektor	k1gInPc2	sektor
oddělených	oddělený	k2eAgInPc2d1	oddělený
bulváry	bulvár	k1gInPc7	bulvár
širokými	široký	k2eAgInPc7d1	široký
34	[number]	k4	34
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
sítí	síť	k1gFnSc7	síť
menších	malý	k2eAgFnPc2d2	menší
uliček	ulička	k1gFnPc2	ulička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
mahárádžou	mahárádža	k1gMnSc7	mahárádža
Džajpuru	Džajpur	k1gInSc2	Džajpur
jeho	jeho	k3xOp3gFnSc4	jeho
výsost	výsost	k1gFnSc4	výsost
Padmanabh	Padmanabh	k1gInSc1	Padmanabh
Singhji	Singhj	k1gFnSc3	Singhj
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
trůn	trůn	k1gInSc4	trůn
dostal	dostat	k5eAaPmAgMnS	dostat
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
dědovi	děd	k1gMnSc6	děd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Džajpur	Džajpur	k1gMnSc1	Džajpur
byl	být	k5eAaImAgMnS	být
založen	založit	k5eAaPmNgMnS	založit
roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
mahárádžou	mahárádža	k1gMnSc7	mahárádža
Saváí	Saváí	k1gMnSc1	Saváí
Džaj	Džaj	k1gMnSc1	Džaj
Singhem	Singh	k1gInSc7	Singh
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1699	[number]	k4	1699
<g/>
-	-	kIx~	-
<g/>
1744	[number]	k4	1744
<g/>
.	.	kIx.	.
</s>
<s>
Mahárádžovým	mahárádžův	k2eAgNnSc7d1	mahárádžův
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
Amber	ambra	k1gFnPc2	ambra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
11	[number]	k4	11
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Džajpuru	Džajpur	k1gInSc2	Džajpur
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
přesunutí	přesunutí	k1gNnSc3	přesunutí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
populace	populace	k1gFnSc1	populace
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojený	spojený	k2eAgInSc1d1	spojený
nedostatek	nedostatek	k1gInSc1	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
Maráthy	Maráth	k1gInPc4	Maráth
si	se	k3xPyFc3	se
Džaj	Džaj	k1gMnSc1	Džaj
Singh	Singh	k1gMnSc1	Singh
začal	začít	k5eAaPmAgMnS	začít
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
důležitost	důležitost	k1gFnSc4	důležitost
obrany	obrana	k1gFnSc2	obrana
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
sám	sám	k3xTgMnSc1	sám
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
ani	ani	k8xC	ani
stavebních	stavební	k2eAgFnPc6d1	stavební
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
pomoc	pomoc	k1gFnSc4	pomoc
u	u	k7c2	u
Vidjadhár	Vidjadhár	k1gInSc4	Vidjadhár
Bhattačárji	Bhattačárje	k1gFnSc4	Bhattačárje
<g/>
,	,	kIx,	,
bráhmanského	bráhmanský	k2eAgMnSc4d1	bráhmanský
učence	učenec	k1gMnSc4	učenec
z	z	k7c2	z
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgInS	svěřit
mu	on	k3xPp3gMnSc3	on
plánování	plánování	k1gNnSc1	plánování
architektury	architektura	k1gFnSc2	architektura
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
města	město	k1gNnSc2	město
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
a	a	k8xC	a
4	[number]	k4	4
roky	rok	k1gInPc7	rok
trvalo	trvat	k5eAaImAgNnS	trvat
postavit	postavit	k5eAaPmF	postavit
hlavní	hlavní	k2eAgInSc4d1	hlavní
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
devíti	devět	k4xCc2	devět
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
skládaly	skládat	k5eAaImAgInP	skládat
ze	z	k7c2	z
státních	státní	k2eAgFnPc2d1	státní
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
masivní	masivní	k2eAgNnSc1d1	masivní
opevnění	opevnění	k1gNnSc1	opevnění
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
branami	brána	k1gFnPc7	brána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Architektura	architektura	k1gFnSc1	architektura
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
pokroková	pokrokový	k2eAgFnSc1d1	pokroková
a	a	k8xC	a
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgFnPc3d3	nejlepší
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
město	město	k1gNnSc4	město
navštívil	navštívit	k5eAaPmAgMnS	navštívit
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
na	na	k7c4	na
uvítanou	uvítaná	k1gFnSc4	uvítaná
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
natřeno	natřen	k2eAgNnSc1d1	natřeno
na	na	k7c4	na
růžovou	růžový	k2eAgFnSc4d1	růžová
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dodnes	dodnes	k6eAd1	dodnes
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
hlavních	hlavní	k2eAgFnPc6d1	hlavní
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
razantně	razantně	k6eAd1	razantně
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
činila	činit	k5eAaImAgFnS	činit
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
160	[number]	k4	160
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgInPc1d1	městský
bulváry	bulvár	k1gInPc1	bulvár
byly	být	k5eAaImAgInP	být
vydlážděny	vydláždit	k5eAaPmNgInP	vydláždit
a	a	k8xC	a
osvětleny	osvětlit	k5eAaPmNgInP	osvětlit
plynovými	plynový	k2eAgFnPc7d1	plynová
lampami	lampa	k1gFnPc7	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
několik	několik	k4yIc4	několik
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
průmyslovými	průmyslový	k2eAgMnPc7d1	průmyslový
odvětvími	odvětví	k1gNnPc7	odvětví
bylo	být	k5eAaImAgNnS	být
zpracování	zpracování	k1gNnSc4	zpracování
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
o	o	k7c4	o
což	což	k3yQnSc4	což
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
přičinila	přičinit	k5eAaPmAgFnS	přičinit
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
městu	město	k1gNnSc3	město
patřily	patřit	k5eAaImAgFnP	patřit
také	také	k9	také
tři	tři	k4xCgFnPc1	tři
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
včetně	včetně	k7c2	včetně
školy	škola	k1gFnSc2	škola
Sanskrtu	sanskrt	k1gInSc2	sanskrt
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
a	a	k8xC	a
dívčí	dívčí	k2eAgFnPc4d1	dívčí
školy	škola	k1gFnPc4	škola
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
mahárádži	mahárádža	k1gMnSc2	mahárádža
Saváí	Saváí	k1gFnSc2	Saváí
Rám	rám	k1gInSc1	rám
Singha	Singha	k1gMnSc1	Singha
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žila	žít	k5eAaImAgFnS	žít
i	i	k9	i
bohatá	bohatý	k2eAgFnSc1d1	bohatá
komunita	komunita	k1gFnSc1	komunita
místních	místní	k2eAgMnPc2d1	místní
bankéřů	bankéř	k1gMnPc2	bankéř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Silniční	silniční	k2eAgInSc1d1	silniční
===	===	k?	===
</s>
</p>
<p>
<s>
Džajpur	Džajpur	k1gMnSc1	Džajpur
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
Rádžasthán	Rádžasthán	k2eAgInSc1d1	Rádžasthán
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
silnice	silnice	k1gFnSc1	silnice
číslo	číslo	k1gNnSc1	číslo
8	[number]	k4	8
ho	on	k3xPp3gMnSc2	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Dillím	Dillí	k1gNnSc7	Dillí
a	a	k8xC	a
Bombají	Bombaj	k1gFnSc7	Bombaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
11	[number]	k4	11
s	s	k7c7	s
Bikanérem	Bikanér	k1gMnSc7	Bikanér
a	a	k8xC	a
Ágrou	Ágra	k1gMnSc7	Ágra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
distriktu	distrikt	k1gInSc6	distrikt
Džajpur	Džajpura	k1gFnPc2	Džajpura
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
366	[number]	k4	366
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
cest	cesta	k1gFnPc2	cesta
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
102	[number]	k4	102
km	km	kA	km
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železniční	železniční	k2eAgNnSc1d1	železniční
===	===	k?	===
</s>
</p>
<p>
<s>
Džajpur	Džajpur	k1gMnSc1	Džajpur
je	být	k5eAaImIp3nS	být
železnicí	železnice	k1gFnSc7	železnice
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
spojený	spojený	k2eAgInSc1d1	spojený
se	s	k7c7	s
všemi	všecek	k3xTgNnPc7	všecek
hlavními	hlavní	k2eAgNnPc7d1	hlavní
městy	město	k1gNnPc7	město
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
měst	město	k1gNnPc2	město
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Indor	Indor	k1gInSc1	Indor
-	-	kIx~	-
3	[number]	k4	3
spoje	spoj	k1gInPc4	spoj
(	(	kIx(	(
<g/>
Indor-	Indor-	k1gMnSc1	Indor-
Džajpur	Džajpur	k1gMnSc1	Džajpur
-	-	kIx~	-
Indore	Indor	k1gInSc5	Indor
Super	super	k2eAgInSc1d1	super
Fast	Fast	k2eAgInSc1d1	Fast
Express	express	k1gInSc1	express
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ágra	Ágra	k1gFnSc1	Ágra
<g/>
,	,	kIx,	,
Dillí	Dillí	k1gNnSc1	Dillí
<g/>
,	,	kIx,	,
Gwalior	Gwalior	k1gInSc1	Gwalior
<g/>
,	,	kIx,	,
Bombaj	Bombaj	k1gFnSc1	Bombaj
<g/>
,	,	kIx,	,
Howrah	Howrah	k1gInSc1	Howrah
<g/>
,	,	kIx,	,
Hajdarábád	Hajdarábád	k1gInSc1	Hajdarábád
<g/>
,	,	kIx,	,
Čennaí	Čennaí	k1gMnSc1	Čennaí
<g/>
,	,	kIx,	,
Maisúr	Maisúr	k1gMnSc1	Maisúr
<g/>
,	,	kIx,	,
Bengalúru	Bengalúr	k1gInSc2	Bengalúr
<g/>
,	,	kIx,	,
Lakhnaú	Lakhnaú	k1gMnSc1	Lakhnaú
<g/>
,	,	kIx,	,
Kánpur	Kánpur	k1gMnSc1	Kánpur
<g/>
,	,	kIx,	,
Patna	Patna	k1gFnSc1	Patna
atd.	atd.	kA	atd.
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
všech	všecek	k3xTgNnPc2	všecek
důležitých	důležitý	k2eAgNnPc2d1	důležité
měst	město	k1gNnPc2	město
Rádžasthánu	Rádžasthán	k2eAgFnSc4d1	Rádžasthán
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Adžmér	Adžmér	k1gInSc1	Adžmér
<g/>
,	,	kIx,	,
Sawai	Sawai	k1gNnSc1	Sawai
Madhopur	Madhopura	k1gFnPc2	Madhopura
<g/>
,	,	kIx,	,
Kota	kot	k1gMnSc4	kot
<g/>
,	,	kIx,	,
Džódhpur	Džódhpur	k1gMnSc1	Džódhpur
<g/>
,	,	kIx,	,
Bikanér	Bikanér	k1gMnSc1	Bikanér
a	a	k8xC	a
Udajpur	Udajpur	k1gMnSc1	Udajpur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letecká	letecký	k2eAgFnSc1d1	letecká
===	===	k?	===
</s>
</p>
<p>
<s>
Džajpurské	Džajpurský	k2eAgNnSc1d1	Džajpurský
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
JAI	JAI	kA	JAI
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
přilehlém	přilehlý	k2eAgNnSc6d1	přilehlé
městě	město	k1gNnSc6	město
Sanganer	Sanganra	k1gFnPc2	Sanganra
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
občasné	občasný	k2eAgInPc4d1	občasný
lety	let	k1gInPc4	let
do	do	k7c2	do
Maskatu	Maskat	k1gInSc2	Maskat
<g/>
,	,	kIx,	,
Šardžá	Šardžá	k1gFnSc1	Šardžá
<g/>
,	,	kIx,	,
Bangkoku	Bangkok	k1gInSc2	Bangkok
a	a	k8xC	a
Dubaje	Dubaj	k1gInSc2	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Džajpuru	Džajpur	k1gInSc2	Džajpur
létá	létat	k5eAaImIp3nS	létat
mnoho	mnoho	k4c1	mnoho
vnitrostátních	vnitrostátní	k2eAgFnPc2d1	vnitrostátní
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Džódhpuru	Džódhpur	k1gInSc2	Džódhpur
<g/>
,	,	kIx,	,
Udajpuru	Udajpur	k1gInSc2	Udajpur
<g/>
,	,	kIx,	,
Aurangabádu	Aurangabád	k1gInSc2	Aurangabád
<g/>
,	,	kIx,	,
Dillí	Dillí	k1gNnSc2	Dillí
<g/>
,	,	kIx,	,
Hajdarábádu	Hajdarábád	k1gInSc2	Hajdarábád
<g/>
,	,	kIx,	,
Kalkaty	Kalkata	k1gFnSc2	Kalkata
<g/>
,	,	kIx,	,
Goa	Goa	k1gFnSc1	Goa
<g/>
,	,	kIx,	,
Čennaíe	Čennaíe	k1gFnSc1	Čennaíe
<g/>
,	,	kIx,	,
Ahmedabadu	Ahmedabada	k1gFnSc4	Ahmedabada
<g/>
,	,	kIx,	,
Indoru	Indora	k1gFnSc4	Indora
<g/>
,	,	kIx,	,
Bangaloru	Bangalora	k1gFnSc4	Bangalora
<g/>
,	,	kIx,	,
Bombaje	Bombaj	k1gFnPc4	Bombaj
<g/>
,	,	kIx,	,
Suratu	Surata	k1gFnSc4	Surata
a	a	k8xC	a
Raipuru	Raipura	k1gFnSc4	Raipura
<g/>
,	,	kIx,	,
Lakhnaú	Lakhnaú	k1gFnSc4	Lakhnaú
<g/>
,	,	kIx,	,
Gorakhpuru	Gorakhpura	k1gFnSc4	Gorakhpura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Calgary	Calgary	k1gNnSc1	Calgary
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
</s>
</p>
<p>
<s>
Fremont	Fremont	k1gMnSc1	Fremont
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Lagos	Lagos	k1gInSc1	Lagos
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
</s>
</p>
<p>
<s>
Port	porta	k1gFnPc2	porta
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
Mauricius	Mauricius	k1gMnSc1	Mauricius
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
