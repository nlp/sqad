<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
România	Românium	k1gNnSc2	Românium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
a	a	k8xC	a
Moldavskem	Moldavsko	k1gNnSc7	Moldavsko
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInPc1d1	východní
břehy	břeh	k1gInPc1	břeh
země	zem	k1gFnSc2	zem
omývají	omývat	k5eAaImIp3nP	omývat
vody	voda	k1gFnPc1	voda
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Dákové	Dák	k1gMnPc1	Dák
byli	být	k5eAaImAgMnP	být
poraženi	porazit	k5eAaPmNgMnP	porazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
106	[number]	k4	106
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
území	území	k1gNnSc6	území
si	se	k3xPyFc3	se
však	však	k9	však
i	i	k9	i
poté	poté	k6eAd1	poté
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
značnou	značný	k2eAgFnSc4d1	značná
míru	míra	k1gFnSc4	míra
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
Valašsko	Valašsko	k1gNnSc1	Valašsko
a	a	k8xC	a
Moldávie	Moldávie	k1gFnSc1	Moldávie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
bylo	být	k5eAaImAgNnS	být
zprvu	zprvu	k6eAd1	zprvu
součástí	součást	k1gFnSc7	součást
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
větší	veliký	k2eAgFnSc4d2	veliký
míru	míra	k1gFnSc4	míra
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Uhrám	Uhry	k1gFnPc3	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Valašsko	Valašsko	k1gNnSc1	Valašsko
a	a	k8xC	a
Moldávie	Moldávie	k1gFnPc1	Moldávie
získaly	získat	k5eAaPmAgFnP	získat
roku	rok	k1gInSc3	rok
1829	[number]	k4	1829
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
se	se	k3xPyFc4	se
sjednotily	sjednotit	k5eAaPmAgInP	sjednotit
a	a	k8xC	a
položily	položit	k5eAaPmAgInP	položit
tak	tak	k6eAd1	tak
základ	základ	k1gInSc4	základ
samostatnému	samostatný	k2eAgNnSc3d1	samostatné
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc3	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
bylo	být	k5eAaImAgNnS	být
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
o	o	k7c4	o
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
<g/>
,	,	kIx,	,
Bukovinu	Bukovina	k1gFnSc4	Bukovina
a	a	k8xC	a
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgNnSc1d1	velké
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
anexi	anexe	k1gFnSc6	anexe
Besarábie	Besarábie	k1gFnSc1	Besarábie
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
obranná	obranný	k2eAgFnSc1d1	obranná
aliance	aliance	k1gFnSc1	aliance
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
proti	proti	k7c3	proti
Rusům	Rus	k1gMnPc3	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Maďaři	Maďar	k1gMnPc1	Maďar
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
rozdělením	rozdělení	k1gNnSc7	rozdělení
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
a	a	k8xC	a
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
ztracené	ztracený	k2eAgFnSc2d1	ztracená
země	zem	k1gFnSc2	zem
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Rumunské	rumunský	k2eAgFnPc1d1	rumunská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
však	však	k9	však
vedly	vést	k5eAaImAgFnP	vést
tuhý	tuhý	k2eAgInSc4d1	tuhý
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
posléze	posléze	k6eAd1	posléze
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c4	v
rázný	rázný	k2eAgInSc4d1	rázný
útok	útok	k1gInSc4	útok
a	a	k8xC	a
maďarské	maďarský	k2eAgFnPc4d1	maďarská
síly	síla	k1gFnPc4	síla
byly	být	k5eAaImAgFnP	být
rychle	rychle	k6eAd1	rychle
zahnány	zahnat	k5eAaPmNgFnP	zahnat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
Rumunské	rumunský	k2eAgNnSc1d1	rumunské
království	království	k1gNnSc4	království
součástí	součást	k1gFnPc2	součást
Malé	Malé	k2eAgFnSc2d1	Malé
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Úzce	úzko	k6eAd1	úzko
spolupracovalo	spolupracovat	k5eAaImAgNnS	spolupracovat
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
proti	proti	k7c3	proti
nárůstu	nárůst	k1gInSc3	nárůst
maďarského	maďarský	k2eAgInSc2d1	maďarský
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
však	však	k8xC	však
mělo	mít	k5eAaImAgNnS	mít
jen	jen	k9	jen
krátkou	krátký	k2eAgFnSc4d1	krátká
hranici	hranice	k1gFnSc4	hranice
v	v	k7c6	v
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
proto	proto	k8xC	proto
užší	úzký	k2eAgFnSc1d2	užší
spolupráce	spolupráce	k1gFnSc1	spolupráce
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
rozmělňování	rozmělňování	k1gNnSc3	rozmělňování
integrity	integrita	k1gFnSc2	integrita
obou	dva	k4xCgInPc2	dva
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
států	stát	k1gInPc2	stát
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
později	pozdě	k6eAd2	pozdě
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
opuštěno	opuštěn	k2eAgNnSc1d1	opuštěno
a	a	k8xC	a
Budapešti	Budapešť	k1gFnSc6	Budapešť
se	se	k3xPyFc4	se
přece	přece	k9	přece
jen	jen	k9	jen
podařilo	podařit	k5eAaPmAgNnS	podařit
zajistit	zajistit	k5eAaPmF	zajistit
si	se	k3xPyFc3	se
úpravy	úprava	k1gFnPc4	úprava
hranic	hranice	k1gFnPc2	hranice
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
karpatské	karpatský	k2eAgFnSc2d1	Karpatská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
bojovalo	bojovat	k5eAaImAgNnS	bojovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Rumuni	Rumun	k1gMnPc1	Rumun
přišli	přijít	k5eAaPmAgMnP	přijít
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
arbitráže	arbitráž	k1gFnSc2	arbitráž
o	o	k7c4	o
severní	severní	k2eAgNnSc4d1	severní
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
expandujícímu	expandující	k2eAgNnSc3d1	expandující
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
okupoval	okupovat	k5eAaBmAgInS	okupovat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
Severní	severní	k2eAgFnSc4d1	severní
Bukovinu	Bukovina	k1gFnSc4	Bukovina
a	a	k8xC	a
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgInSc1d3	veliký
vliv	vliv	k1gInSc1	vliv
maršál	maršál	k1gMnSc1	maršál
Ion	ion	k1gInSc1	ion
Antonescu	Antonescus	k1gInSc6	Antonescus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
SSSR	SSSR	kA	SSSR
Německem	Německo	k1gNnSc7	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
získali	získat	k5eAaPmAgMnP	získat
Rumuni	Rumun	k1gMnPc1	Rumun
území	území	k1gNnSc4	území
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
opět	opět	k6eAd1	opět
odňata	odňat	k2eAgFnSc1d1	odňata
Besarábie	Besarábie	k1gFnSc1	Besarábie
a	a	k8xC	a
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
nucen	nutit	k5eAaImNgMnS	nutit
abdikovat	abdikovat	k5eAaBmF	abdikovat
a	a	k8xC	a
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
komunistického	komunistický	k2eAgInSc2d1	komunistický
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Zahájily	zahájit	k5eAaPmAgInP	zahájit
se	se	k3xPyFc4	se
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
projekty	projekt	k1gInPc1	projekt
na	na	k7c4	na
industrializaci	industrializace	k1gFnSc4	industrializace
chudé	chudý	k2eAgFnSc2d1	chudá
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
potřebné	potřebný	k2eAgFnPc1d1	potřebná
technologie	technologie	k1gFnPc1	technologie
byly	být	k5eAaImAgFnP	být
zajišťovány	zajišťovat	k5eAaImNgFnP	zajišťovat
hlavně	hlavně	k6eAd1	hlavně
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pokročily	pokročit	k5eAaPmAgInP	pokročit
mnohé	mnohý	k2eAgInPc1d1	mnohý
velkolepé	velkolepý	k2eAgInPc1d1	velkolepý
projekty	projekt	k1gInPc1	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
rozvoj	rozvoj	k1gInSc4	rozvoj
atomové	atomový	k2eAgFnSc2d1	atomová
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
výstavba	výstavba	k1gFnSc1	výstavba
vodních	vodní	k2eAgInPc2d1	vodní
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
či	či	k8xC	či
přehrada	přehrada	k1gFnSc1	přehrada
Železná	železný	k2eAgFnSc1d1	železná
vrata	vrata	k1gNnPc4	vrata
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
situaci	situace	k1gFnSc4	situace
radikálně	radikálně	k6eAd1	radikálně
změnit	změnit	k5eAaPmF	změnit
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
se	se	k3xPyFc4	se
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgFnPc4	ten
nejchudší	chudý	k2eAgFnPc4d3	nejchudší
země	zem	k1gFnPc4	zem
RVHP	RVHP	kA	RVHP
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patřilo	patřit	k5eAaImAgNnS	patřit
víceméně	víceméně	k9	víceméně
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
prosazovat	prosazovat	k5eAaImF	prosazovat
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
neúčastnila	účastnit	k5eNaImAgFnS	účastnit
některých	některý	k3yIgFnPc2	některý
akcí	akce	k1gFnPc2	akce
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
invaze	invaze	k1gFnPc4	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
nebo	nebo	k8xC	nebo
bojkot	bojkot	k1gInSc4	bojkot
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Diktátor	diktátor	k1gMnSc1	diktátor
Nicolae	Nicolae	k1gNnSc2	Nicolae
Ceauș	Ceauș	k1gMnSc1	Ceauș
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zemi	zem	k1gFnSc4	zem
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
vládl	vládnout	k5eAaImAgInS	vládnout
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
polním	polní	k2eAgInSc7d1	polní
soudem	soud	k1gInSc7	soud
a	a	k8xC	a
následně	následně	k6eAd1	následně
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
i	i	k8xC	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
nabytá	nabytý	k2eAgFnSc1d1	nabytá
svoboda	svoboda	k1gFnSc1	svoboda
rumunskému	rumunský	k2eAgNnSc3d1	rumunské
lidu	lido	k1gNnSc3	lido
otevřela	otevřít	k5eAaPmAgFnS	otevřít
staronové	staronový	k2eAgFnPc1d1	staronová
a	a	k8xC	a
problematické	problematický	k2eAgFnPc1d1	problematická
otázky	otázka	k1gFnPc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgFnPc3	ten
patřilo	patřit	k5eAaImAgNnS	patřit
jak	jak	k6eAd1	jak
soužití	soužití	k1gNnSc1	soužití
se	s	k7c7	s
zhruba	zhruba	k6eAd1	zhruba
dvoumilionovou	dvoumilionový	k2eAgFnSc7d1	dvoumilionová
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
menšinou	menšina	k1gFnSc7	menšina
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
se	se	k3xPyFc4	se
propadlo	propadnout	k5eAaPmAgNnS	propadnout
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
do	do	k7c2	do
problémů	problém	k1gInPc2	problém
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgInP	začít
národnostní	národnostní	k2eAgInPc1d1	národnostní
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
,	,	kIx,	,
měna	měna	k1gFnSc1	měna
se	se	k3xPyFc4	se
destabilizovala	destabilizovat	k5eAaBmAgFnS	destabilizovat
a	a	k8xC	a
mnohé	mnohý	k2eAgFnSc2d1	mnohá
země	zem	k1gFnSc2	zem
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
k	k	k7c3	k
integrované	integrovaný	k2eAgFnSc3d1	integrovaná
části	část	k1gFnSc3	část
kontinentu	kontinent	k1gInSc2	kontinent
přibližovaly	přibližovat	k5eAaImAgInP	přibližovat
stále	stále	k6eAd1	stále
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
oblasti	oblast	k1gFnPc1	oblast
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
však	však	k9	však
stále	stále	k6eAd1	stále
připomínaly	připomínat	k5eAaImAgFnP	připomínat
oblasti	oblast	k1gFnPc1	oblast
z	z	k7c2	z
jiného	jiný	k2eAgNnSc2d1	jiné
století	století	k1gNnSc2	století
než	než	k8xS	než
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
postupnými	postupný	k2eAgFnPc7d1	postupná
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zajistit	zajistit	k5eAaPmF	zajistit
i	i	k9	i
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
cestu	cesta	k1gFnSc4	cesta
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
země	země	k1gFnSc1	země
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
společně	společně	k6eAd1	společně
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejchudším	chudý	k2eAgFnPc3d3	nejchudší
zemím	zem	k1gFnPc3	zem
"	"	kIx"	"
<g/>
osmadvacitky	osmadvacitek	k1gInPc4	osmadvacitek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
významným	významný	k2eAgMnSc7d1	významný
příjemcem	příjemce	k1gMnSc7	příjemce
mnohých	mnohý	k2eAgInPc2d1	mnohý
rozvojových	rozvojový	k2eAgInPc2d1	rozvojový
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
238	[number]	k4	238
391	[number]	k4	391
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
zabírá	zabírat	k5eAaImIp3nS	zabírat
5,4	[number]	k4	5,4
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
rumunská	rumunský	k2eAgFnSc1d1	rumunská
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
a	a	k8xC	a
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Moldavskem	Moldavsko	k1gNnSc7	Moldavsko
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Prut	prut	k1gInSc1	prut
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
na	na	k7c6	na
rumunském	rumunský	k2eAgNnSc6d1	rumunské
území	území	k1gNnSc6	území
tvoří	tvořit	k5eAaImIp3nP	tvořit
deltu	delta	k1gFnSc4	delta
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
deltu	delta	k1gFnSc4	delta
a	a	k8xC	a
biosférickou	biosférický	k2eAgFnSc4d1	biosférická
rezervaci	rezervace	k1gFnSc4	rezervace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
seznamu	seznam	k1gInSc2	seznam
Světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
13	[number]	k4	13
rumunských	rumunský	k2eAgInPc2d1	rumunský
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
je	být	k5eAaImIp3nS	být
hornatý	hornatý	k2eAgMnSc1d1	hornatý
<g/>
,	,	kIx,	,
dominují	dominovat	k5eAaImIp3nP	dominovat
mu	on	k3xPp3gMnSc3	on
Karpaty	Karpaty	k1gInPc5	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Moldoveanu	Moldoveana	k1gFnSc4	Moldoveana
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
2544	[number]	k4	2544
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
oblasti	oblast	k1gFnSc2	oblast
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
nížinami	nížina	k1gFnPc7	nížina
<g/>
:	:	kIx,	:
Panonská	panonský	k2eAgFnSc1d1	Panonská
pánev	pánev	k1gFnSc1	pánev
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Valašská	valašský	k2eAgFnSc1d1	Valašská
nížina	nížina	k1gFnSc1	nížina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
rovina	rovina	k1gFnSc1	rovina
kolem	kolem	k7c2	kolem
řeky	řeka	k1gFnSc2	řeka
Prut	prut	k1gInSc1	prut
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
také	také	k9	také
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
pohoří	pohoří	k1gNnSc4	pohoří
Munț	Munț	k1gMnPc2	Munț
Apuseni	Apusen	k2eAgMnPc1d1	Apusen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
sídla	sídlo	k1gNnPc4	sídlo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Bukurešť	Bukurešť	k1gFnSc1	Bukurešť
Brašov	Brašov	k1gInSc4	Brašov
Temešvár	Temešvár	k1gInSc1	Temešvár
Kluž	Kluž	k1gFnSc1	Kluž
Constanț	Constanț	k1gMnSc2	Constanț
Craiova	Craiův	k2eAgInSc2d1	Craiův
Jasy	Jasy	k1gInPc7	Jasy
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
Senát	senát	k1gInSc4	senát
a	a	k8xC	a
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
,	,	kIx,	,
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
Camera	Camero	k1gNnSc2	Camero
Deputaț	Deputaț	k1gFnPc1	Deputaț
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
má	mít	k5eAaImIp3nS	mít
137	[number]	k4	137
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
334	[number]	k4	334
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
konajících	konající	k2eAgFnPc6d1	konající
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
představitel	představitel	k1gMnSc1	představitel
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgFnSc2d1	výkonná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
volen	volit	k5eAaImNgInS	volit
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Ministerského	ministerský	k2eAgMnSc4d1	ministerský
předsedu	předseda	k1gMnSc4	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
prezidentem	prezident	k1gMnSc7	prezident
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
je	být	k5eAaImIp3nS	být
Klaus	Klaus	k1gMnSc1	Klaus
Iohannis	Iohannis	k1gFnSc2	Iohannis
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
soudí	soudit	k5eAaImIp3nS	soudit
s	s	k7c7	s
Čadem	Čad	k1gInSc7	Čad
o	o	k7c4	o
podobu	podoba	k1gFnSc4	podoba
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
41	[number]	k4	41
administrativních	administrativní	k2eAgFnPc2d1	administrativní
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
žup	župa	k1gFnPc2	župa
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Bukurešť	Bukurešť	k1gFnSc1	Bukurešť
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
bylo	být	k5eAaImAgNnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
chudších	chudý	k2eAgFnPc2d2	chudší
zemí	zem	k1gFnPc2	zem
RVHP	RVHP	kA	RVHP
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
méně	málo	k6eAd2	málo
rozvinutým	rozvinutý	k2eAgFnPc3d1	rozvinutá
zemím	zem	k1gFnPc3	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
druhý	druhý	k4xOgInSc1	druhý
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
<g/>
,	,	kIx,	,
po	po	k7c6	po
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
především	především	k6eAd1	především
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
autoritativní	autoritativní	k2eAgFnSc1d1	autoritativní
vláda	vláda	k1gFnSc1	vláda
Nicolae	Nicolae	k1gFnSc1	Nicolae
Ceauș	Ceauș	k1gFnSc1	Ceauș
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
naprosto	naprosto	k6eAd1	naprosto
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
rozvoj	rozvoj	k1gInSc4	rozvoj
rumunské	rumunský	k2eAgFnSc2d1	rumunská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
rumunskou	rumunský	k2eAgFnSc4d1	rumunská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
výroba	výroba	k1gFnSc1	výroba
aut	auto	k1gNnPc2	auto
značky	značka	k1gFnSc2	značka
Dacia	Dacia	k1gFnSc1	Dacia
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
francouzského	francouzský	k2eAgInSc2d1	francouzský
koncernu	koncern	k1gInSc2	koncern
Renault	renault	k1gInSc1	renault
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
rumunské	rumunský	k2eAgNnSc1d1	rumunské
hutnictví	hutnictví	k1gNnSc1	hutnictví
–	–	k?	–
nicméně	nicméně	k8xC	nicméně
zásoby	zásoba	k1gFnPc1	zásoba
rumunského	rumunský	k2eAgNnSc2d1	rumunské
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
využity	využít	k5eAaPmNgInP	využít
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
poměrně	poměrně	k6eAd1	poměrně
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
–	–	k?	–
ve	v	k7c6	v
Vlašské	vlašský	k2eAgFnSc6d1	vlašská
nížině	nížina	k1gFnSc6	nížina
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
plodiny	plodina	k1gFnPc1	plodina
jako	jako	k8xC	jako
pšenice	pšenice	k1gFnPc1	pšenice
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnPc1	slunečnice
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnPc1	kukuřice
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
či	či	k8xC	či
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
chov	chov	k1gInSc4	chov
vepřů	vepř	k1gMnPc2	vepř
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
či	či	k8xC	či
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
pro	pro	k7c4	pro
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
účely	účel	k1gInPc4	účel
pak	pak	k8xC	pak
chov	chov	k1gInSc4	chov
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
5	[number]	k4	5
%	%	kIx~	%
rumunského	rumunský	k2eAgNnSc2d1	rumunské
HDP	HDP	kA	HDP
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přijelo	přijet	k5eAaPmAgNnS	přijet
přes	přes	k7c4	přes
1	[number]	k4	1
900	[number]	k4	900
000	[number]	k4	000
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
potenciál	potenciál	k1gInSc4	potenciál
pro	pro	k7c4	pro
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
turisty	turist	k1gMnPc4	turist
lákají	lákat	k5eAaImIp3nP	lákat
především	především	k6eAd1	především
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
slabé	slabý	k2eAgFnSc3d1	slabá
úrovni	úroveň	k1gFnSc3	úroveň
služeb	služba	k1gFnPc2	služba
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
jsou	být	k5eAaImIp3nP	být
příjmy	příjem	k1gInPc7	příjem
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
bylo	být	k5eAaImAgNnS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
míře	míra	k1gFnSc3	míra
korupce	korupce	k1gFnSc2	korupce
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
proto	proto	k8xC	proto
speciální	speciální	k2eAgFnSc1d1	speciální
protikorupční	protikorupční	k2eAgFnSc1d1	protikorupční
prokuratura	prokuratura	k1gFnSc1	prokuratura
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgMnS	být
rumunský	rumunský	k2eAgMnSc1d1	rumunský
premiér	premiér	k1gMnSc1	premiér
Victor	Victor	k1gMnSc1	Victor
Ponta	Ponta	k1gMnSc1	Ponta
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
praní	praní	k1gNnSc2	praní
špinavých	špinavý	k2eAgInPc2d1	špinavý
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
i	i	k8xC	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
skončili	skončit	k5eAaPmAgMnP	skončit
i	i	k9	i
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
nebo	nebo	k8xC	nebo
předseda	předseda	k1gMnSc1	předseda
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
bezúhonnost	bezúhonnost	k1gFnSc4	bezúhonnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
soudem	soud	k1gInSc7	soud
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
premiér	premiér	k1gMnSc1	premiér
Adrian	Adrian	k1gMnSc1	Adrian
Năstase	Năstasa	k1gFnSc3	Năstasa
nebo	nebo	k8xC	nebo
mediální	mediální	k2eAgMnSc1d1	mediální
magnát	magnát	k1gMnSc1	magnát
Adrian	Adrian	k1gMnSc1	Adrian
Sarbu	Sarba	k1gMnSc4	Sarba
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
prokuratura	prokuratura	k1gFnSc1	prokuratura
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
až	až	k9	až
2016	[number]	k4	2016
obžalovala	obžalovat	k5eAaPmAgFnS	obžalovat
ze	z	k7c2	z
zneužití	zneužití	k1gNnSc2	zneužití
postavení	postavení	k1gNnSc2	postavení
2000	[number]	k4	2000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
škodu	škoda	k1gFnSc4	škoda
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
korupčním	korupční	k2eAgNnSc7d1	korupční
jednáním	jednání	k1gNnSc7	jednání
vypočítala	vypočítat	k5eAaPmAgFnS	vypočítat
na	na	k7c4	na
částku	částka	k1gFnSc4	částka
27	[number]	k4	27
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
vydala	vydat	k5eAaPmAgFnS	vydat
rumunská	rumunský	k2eAgFnSc1d1	rumunská
vláda	vláda	k1gFnSc1	vláda
dekret	dekret	k1gInSc4	dekret
o	o	k7c6	o
beztrestnosti	beztrestnost	k1gFnSc6	beztrestnost
některých	některý	k3yIgInPc2	některý
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
"	"	kIx"	"
<g/>
amnestii	amnestie	k1gFnSc4	amnestie
<g/>
"	"	kIx"	"
politikům	politik	k1gMnPc3	politik
odsouzeným	odsouzený	k1gMnPc3	odsouzený
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
korupčního	korupční	k2eAgNnSc2d1	korupční
jednání	jednání	k1gNnSc2	jednání
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
obviněn	obvinit	k5eAaPmNgMnS	obvinit
i	i	k9	i
předseda	předseda	k1gMnSc1	předseda
vládní	vládní	k2eAgFnSc2d1	vládní
strany	strana	k1gFnSc2	strana
Liviu	Livium	k1gNnSc3	Livium
Dragnea	Dragnea	k1gFnSc1	Dragnea
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vládnímu	vládní	k2eAgNnSc3d1	vládní
nařízení	nařízení	k1gNnSc3	nařízení
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
statisíce	statisíce	k1gInPc4	statisíce
Rumunů	Rumun	k1gMnPc2	Rumun
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
nakonec	nakonec	k6eAd1	nakonec
nařízení	nařízení	k1gNnSc1	nařízení
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgFnPc1d1	národnostní
skupiny	skupina	k1gFnPc1	skupina
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Rumuni	Rumun	k1gMnPc1	Rumun
89,5	[number]	k4	89,5
%	%	kIx~	%
Maďaři	Maďar	k1gMnPc1	Maďar
6,6	[number]	k4	6,6
%	%	kIx~	%
Romové	Rom	k1gMnPc1	Rom
2,5	[number]	k4	2,5
%	%	kIx~	%
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
0,8	[number]	k4	0,8
%	%	kIx~	%
Němci	Němec	k1gMnSc3	Němec
0,3	[number]	k4	0,3
%	%	kIx~	%
Rusové	Rusová	k1gFnSc2	Rusová
0,2	[number]	k4	0,2
%	%	kIx~	%
Turci	Turek	k1gMnPc1	Turek
0,2	[number]	k4	0,2
%	%	kIx~	%
další	další	k2eAgFnSc1d1	další
0,4	[number]	k4	0,4
%	%	kIx~	%
</s>
<s>
Komunita	komunita	k1gFnSc1	komunita
Čechů	Čech	k1gMnPc2	Čech
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Banátu	Banát	k1gInSc2	Banát
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
čítá	čítat	k5eAaImIp3nS	čítat
2	[number]	k4	2
242	[number]	k4	242
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
deseti	deset	k4xCc7	deset
lety	léto	k1gNnPc7	léto
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
banátských	banátský	k2eAgFnPc6d1	banátská
vesnicích	vesnice	k1gFnPc6	vesnice
žilo	žít	k5eAaImAgNnS	žít
dvakrát	dvakrát	k6eAd1	dvakrát
více	hodně	k6eAd2	hodně
Čechů	Čech	k1gMnPc2	Čech
–	–	k?	–
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
emigraci	emigrace	k1gFnSc3	emigrace
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vysoké	vysoký	k2eAgFnSc2d1	vysoká
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
mladé	mladý	k2eAgFnPc4d1	mladá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
organizace	organizace	k1gFnSc1	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
,	,	kIx,	,
působící	působící	k2eAgFnPc1d1	působící
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
5	[number]	k4	5
800	[number]	k4	800
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
žijí	žít	k5eAaImIp3nP	žít
maďarští	maďarský	k2eAgMnPc1d1	maďarský
Sikulové	Sikul	k1gMnPc1	Sikul
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
pověřeni	pověřen	k2eAgMnPc1d1	pověřen
ostrahou	ostraha	k1gFnSc7	ostraha
uherských	uherský	k2eAgFnPc2d1	uherská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
tzv.	tzv.	kA	tzv.
Sasové	Sas	k1gMnPc1	Sas
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
usazovali	usazovat	k5eAaImAgMnP	usazovat
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
745	[number]	k4	745
000	[number]	k4	000
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Dunaje	Dunaj	k1gInSc2	Dunaj
žijí	žít	k5eAaImIp3nP	žít
ruští	ruský	k2eAgMnPc1d1	ruský
Lipované	Lipovan	k1gMnPc1	Lipovan
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
starověrců	starověrec	k1gMnPc2	starověrec
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
záporožských	záporožský	k2eAgInPc2d1	záporožský
kozáků	kozák	k1gInPc2	kozák
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
v	v	k7c6	v
rumunských	rumunský	k2eAgInPc6d1	rumunský
Karpatech	Karpaty	k1gInPc6	Karpaty
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
Rusíny	rusín	k1gInPc4	rusín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Dobrudže	Dobrudža	k1gFnSc2	Dobrudža
žijí	žít	k5eAaImIp3nP	žít
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Tataři	Tatar	k1gMnPc1	Tatar
a	a	k8xC	a
Nogajci	Nogajce	k1gMnPc1	Nogajce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
před	před	k7c7	před
holokaustem	holokaust	k1gInSc7	holokaust
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
728	[number]	k4	728
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
statistik	statistika	k1gFnPc2	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
okolo	okolo	k7c2	okolo
620	[number]	k4	620
tisíc	tisíc	k4xCgInPc2	tisíc
Romů	Rom	k1gMnPc2	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	oficiální	k2eNgInPc1d1	neoficiální
odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
Romů	Rom	k1gMnPc2	Rom
se	se	k3xPyFc4	se
však	však	k9	však
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c4	mezi
1,2	[number]	k4	1,2
milionů	milion	k4xCgInPc2	milion
až	až	k8xS	až
2,5	[number]	k4	2,5
miliony	milion	k4xCgInPc7	milion
a	a	k8xC	a
rumunští	rumunský	k2eAgMnPc1d1	rumunský
Romové	Rom	k1gMnPc1	Rom
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
romskou	romský	k2eAgFnSc4d1	romská
komunitu	komunita	k1gFnSc4	komunita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Reformovaná	reformovaný	k2eAgFnSc1d1	reformovaná
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlásila	hlásit	k5eAaImAgFnS	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
81	[number]	k4	81
%	%	kIx~	%
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
Rumunské	rumunský	k2eAgFnSc3d1	rumunská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
6,2	[number]	k4	6,2
%	%	kIx~	%
k	k	k7c3	k
protestanství	protestanství	k1gNnSc3	protestanství
<g/>
,	,	kIx,	,
4,3	[number]	k4	4,3
%	%	kIx~	%
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
0,8	[number]	k4	0,8
%	%	kIx~	%
k	k	k7c3	k
řeckokatolické	řeckokatolický	k2eAgFnSc3d1	řeckokatolická
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
64	[number]	k4	64
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
především	především	k9	především
Turků	Turek	k1gMnPc2	Turek
a	a	k8xC	a
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
3	[number]	k4	3
500	[number]	k4	500
bylo	být	k5eAaImAgNnS	být
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
rumunským	rumunský	k2eAgMnSc7d1	rumunský
výtvarným	výtvarný	k2eAgMnSc7d1	výtvarný
umělcem	umělec	k1gMnSc7	umělec
je	být	k5eAaImIp3nS	být
sochař	sochař	k1gMnSc1	sochař
Constantin	Constantin	k1gMnSc1	Constantin
Brâncuș	Brâncuș	k1gMnSc1	Brâncuș
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
sílu	síla	k1gFnSc4	síla
projevila	projevit	k5eAaPmAgFnS	projevit
rumunská	rumunský	k2eAgFnSc1d1	rumunská
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
opustili	opustit	k5eAaPmAgMnP	opustit
a	a	k8xC	a
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
nezřídka	nezřídka	k6eAd1	nezřídka
i	i	k8xC	i
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc4	případ
klasika	klasika	k1gFnSc1	klasika
absurdního	absurdní	k2eAgNnSc2d1	absurdní
dramatu	drama	k1gNnSc2	drama
Eugè	Eugè	k1gMnSc2	Eugè
Ionesca	Ionesco	k1gMnSc2	Ionesco
(	(	kIx(	(
<g/>
blízkého	blízký	k2eAgMnSc2d1	blízký
přítele	přítel	k1gMnSc2	přítel
Eliada	Eliada	k1gFnSc1	Eliada
a	a	k8xC	a
Ciorana	Ciorana	k1gFnSc1	Ciorana
a	a	k8xC	a
podporovatele	podporovatel	k1gMnPc4	podporovatel
rumunského	rumunský	k2eAgInSc2d1	rumunský
fašismu	fašismus	k1gInSc2	fašismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zakladatel	zakladatel	k1gMnSc1	zakladatel
dadaismu	dadaismus	k1gInSc2	dadaismus
Tristan	Tristan	k1gInSc1	Tristan
Tzara	Tzaro	k1gNnSc2	Tzaro
byl	být	k5eAaImAgInS	být
původem	původ	k1gInSc7	původ
Rumun	Rumun	k1gMnSc1	Rumun
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Ionesco	Ionesco	k1gMnSc1	Ionesco
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
i	i	k9	i
nositelka	nositelka	k1gFnSc1	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Herta	Herta	k1gFnSc1	Herta
Müllerová	Müllerová	k1gFnSc1	Müllerová
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jako	jako	k9	jako
etnická	etnický	k2eAgFnSc1d1	etnická
Němka	Němka	k1gFnSc1	Němka
<g/>
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
Paul	Paul	k1gMnSc1	Paul
Celan	Celan	k1gMnSc1	Celan
<g/>
.	.	kIx.	.
</s>
<s>
Francouzsky	francouzsky	k6eAd1	francouzsky
psala	psát	k5eAaImAgFnS	psát
Anna	Anna	k1gFnSc1	Anna
de	de	k?	de
Noailles	Noaillesa	k1gFnPc2	Noaillesa
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
básnířka	básnířka	k1gFnSc1	básnířka
Linda	Linda	k1gFnSc1	Linda
Maria	Maria	k1gFnSc1	Maria
Baros	Baros	k1gInSc4	Baros
<g/>
.	.	kIx.	.
</s>
<s>
Dimitrie	Dimitrie	k1gFnSc1	Dimitrie
Cantemir	Cantemira	k1gFnPc2	Cantemira
sepsal	sepsat	k5eAaPmAgInS	sepsat
první	první	k4xOgInSc1	první
rumunský	rumunský	k2eAgInSc1d1	rumunský
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
autorem	autor	k1gMnSc7	autor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neopustil	opustit	k5eNaPmAgMnS	opustit
ani	ani	k8xC	ani
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
rumunštinu	rumunština	k1gFnSc4	rumunština
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
romantický	romantický	k2eAgMnSc1d1	romantický
básník	básník	k1gMnSc1	básník
Mihai	Mihai	k1gNnSc2	Mihai
Eminescu	Eminescus	k1gInSc2	Eminescus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
spoluzakladateli	spoluzakladatel	k1gMnPc7	spoluzakladatel
vlivné	vlivný	k2eAgFnSc2d1	vlivná
literární	literární	k2eAgFnSc2d1	literární
skupiny	skupina	k1gFnSc2	skupina
Junimea	Junime	k1gInSc2	Junime
byli	být	k5eAaImAgMnP	být
prozaik	prozaik	k1gMnSc1	prozaik
Ion	ion	k1gInSc4	ion
Creangă	Creangă	k1gFnSc2	Creangă
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Ion	ion	k1gInSc4	ion
Luca	Lucum	k1gNnSc2	Lucum
Caragiale	Caragiala	k1gFnSc3	Caragiala
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
obrozeneckou	obrozenecký	k2eAgFnSc7d1	obrozenecká
postavou	postava	k1gFnSc7	postava
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Vasile	Vasila	k1gFnSc3	Vasila
Alecsandri	Alecsandri	k1gNnPc2	Alecsandri
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
sběratel	sběratel	k1gMnSc1	sběratel
lidového	lidový	k2eAgNnSc2d1	lidové
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
literární	literární	k2eAgFnPc1d1	literární
historie	historie	k1gFnPc1	historie
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
Panaitu	Panait	k1gInSc3	Panait
Istratimu	Istratim	k1gInSc2	Istratim
<g/>
,	,	kIx,	,
muži	muž	k1gMnSc6	muž
pozoruhodného	pozoruhodný	k2eAgInSc2d1	pozoruhodný
a	a	k8xC	a
bolestného	bolestný	k2eAgInSc2d1	bolestný
životního	životní	k2eAgInSc2d1	životní
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
Tudor	tudor	k1gInSc1	tudor
Arghezi	Argheze	k1gFnSc4	Argheze
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
autorem	autor	k1gMnSc7	autor
rumunské	rumunský	k2eAgFnSc2d1	rumunská
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
George	Georg	k1gMnSc4	Georg
Enescu	Enesca	k1gMnSc4	Enesca
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
kmotrem	kmotr	k1gMnSc7	kmotr
i	i	k8xC	i
učitelem	učitel	k1gMnSc7	učitel
Dina	Dinus	k1gMnSc2	Dinus
Lipattiho	Lipatti	k1gMnSc2	Lipatti
<g/>
,	,	kIx,	,
předčasně	předčasně	k6eAd1	předčasně
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
talentovaného	talentovaný	k2eAgMnSc2d1	talentovaný
tvůrce	tvůrce	k1gMnSc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Ciprian	Ciprian	k1gInSc1	Ciprian
Porumbescu	Porumbescus	k1gInSc2	Porumbescus
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
autorem	autor	k1gMnSc7	autor
melodie	melodie	k1gFnPc4	melodie
dnešní	dnešní	k2eAgFnSc2d1	dnešní
albánské	albánský	k2eAgFnSc2d1	albánská
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dirigent	dirigent	k1gMnSc1	dirigent
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
zejména	zejména	k9	zejména
Sergiu	Sergium	k1gNnSc3	Sergium
Celibidache	Celibidach	k1gFnSc2	Celibidach
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
získala	získat	k5eAaPmAgFnS	získat
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
proslulost	proslulost	k1gFnSc4	proslulost
sopranistka	sopranistka	k1gFnSc1	sopranistka
Angela	Angela	k1gFnSc1	Angela
Gheorghiu	Gheorghium	k1gNnSc6	Gheorghium
<g/>
,	,	kIx,	,
pěvec	pěvec	k1gMnSc1	pěvec
Florin	Florin	k1gInSc1	Florin
Cezar	Cezara	k1gFnPc2	Cezara
Ouatu	Ouat	k1gInSc2	Ouat
<g/>
,	,	kIx,	,
pianistka	pianistka	k1gFnSc1	pianistka
Clara	Clara	k1gFnSc1	Clara
Haskilová	Haskilová	k1gFnSc1	Haskilová
či	či	k8xC	či
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
Panovu	Panův	k2eAgFnSc4d1	Panova
flétnu	flétna	k1gFnSc4	flétna
Gheorghe	Gheorghe	k1gNnSc2	Gheorghe
Zamfir	Zamfira	k1gFnPc2	Zamfira
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
vojenské	vojenský	k2eAgFnSc2d1	vojenská
pochodové	pochodový	k2eAgFnSc2d1	pochodová
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
Ion	ion	k1gInSc1	ion
Ivanovici	Ivanovice	k1gFnSc4	Ivanovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
pronikla	proniknout	k5eAaPmAgFnS	proniknout
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
za	za	k7c2	za
hranice	hranice	k1gFnSc2	hranice
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Alexandra	Alexandra	k1gFnSc1	Alexandra
Stan	stan	k1gInSc1	stan
či	či	k8xC	či
Inna	Inna	k1gFnSc1	Inna
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Cretu	Cret	k1gMnSc3	Cret
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
elektronického	elektronický	k2eAgInSc2d1	elektronický
projektu	projekt	k1gInSc2	projekt
Enigma	enigma	k1gFnSc1	enigma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
originální	originální	k2eAgMnSc1d1	originální
řecký	řecký	k2eAgMnSc1d1	řecký
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
experimentátor	experimentátor	k1gMnSc1	experimentátor
Iannis	Iannis	k1gFnSc2	Iannis
Xenakis	Xenakis	k1gFnSc2	Xenakis
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
film	film	k1gInSc1	film
70	[number]	k4	70
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
podbarvil	podbarvit	k5eAaPmAgMnS	podbarvit
svou	svůj	k3xOyFgFnSc7	svůj
nezaměnitelnou	zaměnitelný	k2eNgFnSc7d1	nezaměnitelná
hudbou	hudba	k1gFnSc7	hudba
Vladimir	Vladimira	k1gFnPc2	Vladimira
Cosma	Cosm	k1gMnSc2	Cosm
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
zažívá	zažívat	k5eAaImIp3nS	zažívat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
veliký	veliký	k2eAgInSc4d1	veliký
rozmach	rozmach	k1gInSc4	rozmach
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
rumunské	rumunský	k2eAgFnSc6d1	rumunská
nové	nový	k2eAgFnSc6d1	nová
vlně	vlna	k1gFnSc6	vlna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejím	její	k3xOp3gMnPc3	její
klíčovým	klíčový	k2eAgMnPc3d1	klíčový
představitelům	představitel	k1gMnPc3	představitel
patří	patřit	k5eAaImIp3nP	patřit
režisér	režisér	k1gMnSc1	režisér
Cristian	Cristian	k1gMnSc1	Cristian
Mungiu	Mungius	k1gMnSc3	Mungius
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
2	[number]	k4	2
dny	den	k1gInPc7	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
režisérů	režisér	k1gMnPc2	režisér
starší	starý	k2eAgFnSc2d2	starší
generace	generace	k1gFnSc2	generace
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
Sergiu	Sergium	k1gNnSc6	Sergium
Nicolaescu	Nicolaescus	k1gInSc3	Nicolaescus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Jean	Jean	k1gMnSc1	Jean
Negulesco	Negulesco	k1gMnSc1	Negulesco
<g/>
,	,	kIx,	,
z	z	k7c2	z
herců	herc	k1gInPc2	herc
Maia	Mai	k2eAgFnSc1d1	Maia
Morgensternová	Morgensternová	k1gFnSc1	Morgensternová
či	či	k8xC	či
Sebastian	Sebastian	k1gMnSc1	Sebastian
Stan	stan	k1gInSc1	stan
<g/>
.	.	kIx.	.
</s>
<s>
Rumunskými	rumunský	k2eAgMnPc7d1	rumunský
rodáky	rodák	k1gMnPc7	rodák
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
klasické	klasický	k2eAgFnSc2d1	klasická
hollywoodské	hollywoodský	k2eAgFnSc2d1	hollywoodská
hvězdy	hvězda	k1gFnSc2	hvězda
Edward	Edward	k1gMnSc1	Edward
G.	G.	kA	G.
Robinson	Robinson	k1gMnSc1	Robinson
a	a	k8xC	a
Johnny	Johnn	k1gMnPc4	Johnn
Weissmuller	Weissmuller	k1gInSc4	Weissmuller
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
exaktních	exaktní	k2eAgFnPc6d1	exaktní
vědách	věda	k1gFnPc6	věda
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
<g/>
,	,	kIx,	,
cytolog	cytolog	k1gMnSc1	cytolog
George	Georg	k1gMnSc2	Georg
Emil	Emil	k1gMnSc1	Emil
Palade	Palad	k1gInSc5	Palad
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgInPc1d1	pracující
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
další	další	k2eAgMnSc1d1	další
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
Hell	Hell	k1gMnSc1	Hell
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
proslulém	proslulý	k2eAgInSc6d1	proslulý
Institutu	institut	k1gInSc6	institut
Maxe	Max	k1gMnSc2	Max
Plancka	Plancko	k1gNnSc2	Plancko
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
prožil	prožít	k5eAaPmAgMnS	prožít
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
aerodynamiku	aerodynamika	k1gFnSc4	aerodynamika
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Henri	Henr	k1gFnSc2	Henr
Coandă	Coandă	k1gFnSc2	Coandă
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
průkopník	průkopník	k1gMnSc1	průkopník
bakteriologie	bakteriologie	k1gFnSc2	bakteriologie
Victor	Victor	k1gMnSc1	Victor
Babeş	Babeş	k1gMnSc1	Babeş
byl	být	k5eAaImAgMnS	být
Rumun	Rumun	k1gMnSc1	Rumun
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
Temešváru	Temešvár	k1gInSc6	Temešvár
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
lékařská	lékařský	k2eAgFnSc1d1	lékařská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Dumitru	Dumitr	k1gMnSc3	Dumitr
Prunariu	Prunarium	k1gNnSc6	Prunarium
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jediným	jediný	k2eAgMnSc7d1	jediný
rumunským	rumunský	k2eAgMnSc7d1	rumunský
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
<g/>
.	.	kIx.	.
</s>
<s>
Rumunským	rumunský	k2eAgMnSc7d1	rumunský
rodákem	rodák	k1gMnSc7	rodák
je	být	k5eAaImIp3nS	být
i	i	k9	i
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
psycholog	psycholog	k1gMnSc1	psycholog
David	David	k1gMnSc1	David
Wechsler	Wechsler	k1gMnSc1	Wechsler
či	či	k8xC	či
raketový	raketový	k2eAgMnSc1d1	raketový
odborník	odborník	k1gMnSc1	odborník
Hermann	Hermann	k1gMnSc1	Hermann
Oberth	Oberth	k1gMnSc1	Oberth
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc1	dva
jména	jméno	k1gNnPc1	jméno
světové	světový	k2eAgFnSc2d1	světová
proslulosti	proslulost	k1gFnSc2	proslulost
dalo	dát	k5eAaPmAgNnS	dát
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
světu	svět	k1gInSc3	svět
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
religionista	religionista	k1gMnSc1	religionista
Mircea	Mircea	k1gMnSc1	Mircea
Eliade	Eliad	k1gInSc5	Eliad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
přímo	přímo	k6eAd1	přímo
ikonou	ikona	k1gFnSc7	ikona
svého	svůj	k3xOyFgInSc2	svůj
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Eliade	Eliad	k1gInSc5	Eliad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
i	i	k9	i
pesimistický	pesimistický	k2eAgMnSc1d1	pesimistický
filozof	filozof	k1gMnSc1	filozof
Emil	Emil	k1gMnSc1	Emil
Cioran	Cioran	k1gInSc4	Cioran
<g/>
.	.	kIx.	.
</s>
<s>
Intelektuální	intelektuální	k2eAgFnSc1d1	intelektuální
Paříž	Paříž	k1gFnSc1	Paříž
je	on	k3xPp3gInPc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
zapletli	zaplést	k5eAaPmAgMnP	zaplést
s	s	k7c7	s
fašismem	fašismus	k1gInSc7	fašismus
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dlouho	dlouho	k6eAd1	dlouho
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
tvůrce	tvůrce	k1gMnSc1	tvůrce
originálního	originální	k2eAgInSc2d1	originální
filozofického	filozofický	k2eAgInSc2d1	filozofický
systému	systém	k1gInSc2	systém
Lucian	Lucian	k1gInSc1	Lucian
Blaga	blaga	k1gFnSc1	blaga
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
potýkal	potýkat	k5eAaImAgMnS	potýkat
se	se	k3xPyFc4	se
s	s	k7c7	s
nepřízní	nepřízeň	k1gFnSc7	nepřízeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
dnes	dnes	k6eAd1	dnes
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
doceňováno	doceňován	k2eAgNnSc1d1	doceňováno
<g/>
.	.	kIx.	.
</s>
<s>
Jakýmsi	jakýsi	k3yIgMnSc7	jakýsi
zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
rumunské	rumunský	k2eAgFnSc2d1	rumunská
učenosti	učenost	k1gFnSc2	učenost
byl	být	k5eAaImAgMnS	být
všestranný	všestranný	k2eAgMnSc1d1	všestranný
učenec	učenec	k1gMnSc1	učenec
Dimitrie	Dimitrie	k1gFnSc2	Dimitrie
Cantemir	Cantemir	k1gMnSc1	Cantemir
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
literárním	literární	k2eAgMnSc7d1	literární
historikem	historik	k1gMnSc7	historik
Nicolae	Nicola	k1gFnSc2	Nicola
Iorga	Iorga	k1gFnSc1	Iorga
<g/>
,	,	kIx,	,
zavražděný	zavražděný	k2eAgInSc1d1	zavražděný
fašistickou	fašistický	k2eAgFnSc7d1	fašistická
Železnou	železný	k2eAgFnSc7d1	železná
gardou	garda	k1gFnSc7	garda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
sportovní	sportovní	k2eAgFnSc7d1	sportovní
hvězdou	hvězda	k1gFnSc7	hvězda
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
je	být	k5eAaImIp3nS	být
fotbalista	fotbalista	k1gMnSc1	fotbalista
Gheorghe	Gheorgh	k1gFnSc2	Gheorgh
Hagi	Hag	k1gFnSc2	Hag
<g/>
.	.	kIx.	.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
i	i	k9	i
rumunská	rumunský	k2eAgFnSc1d1	rumunská
gymnastická	gymnastický	k2eAgFnSc1d1	gymnastická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
Nadia	Nadia	k1gFnSc1	Nadia
Comăneciová	Comăneciový	k2eAgFnSc1d1	Comăneciový
<g/>
.	.	kIx.	.
</s>
<s>
Rumunský	rumunský	k2eAgInSc1d1	rumunský
tenis	tenis	k1gInSc1	tenis
zrodil	zrodit	k5eAaPmAgInS	zrodit
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
hvězdu	hvězda	k1gFnSc4	hvězda
Ilie	Ili	k1gMnSc2	Ili
Năstaseho	Năstase	k1gMnSc2	Năstase
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
absolutní	absolutní	k2eAgFnSc3d1	absolutní
světové	světový	k2eAgFnSc3d1	světová
špičce	špička	k1gFnSc3	špička
Simona	Simona	k1gFnSc1	Simona
Halepová	Halepová	k1gFnSc1	Halepová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
olympiádách	olympiáda	k1gFnPc6	olympiáda
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
vysokém	vysoký	k2eAgInSc6d1	vysoký
Jolanda	Jolando	k1gNnSc2	Jolando
Balaș	Balaș	k1gFnPc1	Balaș
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
běžkyní	běžkyně	k1gFnSc7	běžkyně
byla	být	k5eAaImAgFnS	být
Gabriela	Gabriela	k1gFnSc1	Gabriela
Szabóová	Szabóová	k1gFnSc1	Szabóová
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
čtyři	čtyři	k4xCgFnPc4	čtyři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
má	mít	k5eAaImIp3nS	mít
kanoista	kanoista	k1gMnSc1	kanoista
Ivan	Ivan	k1gMnSc1	Ivan
Patzaichin	Patzaichin	k1gMnSc1	Patzaichin
<g/>
.	.	kIx.	.
</s>
