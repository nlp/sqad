<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1972	[number]	k4	1972
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
hrající	hrající	k2eAgMnSc1d1	hrající
za	za	k7c4	za
klub	klub	k1gInSc4	klub
Rytíři	Rytíř	k1gMnSc3	Rytíř
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
extraligy	extraliga	k1gFnSc2	extraliga
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
kanadských	kanadský	k2eAgInPc6d1	kanadský
klubech	klub	k1gInPc6	klub
Pittsburgh	Pittsburgh	k1gInSc4	Pittsburgh
Penguins	Penguinsa	k1gFnPc2	Penguinsa
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
Capitals	Capitals	k1gInSc1	Capitals
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Rangers	Rangers	k1gInSc1	Rangers
<g/>
,	,	kIx,	,
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Flyers	Flyers	k1gInSc1	Flyers
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
Stars	Stars	k1gInSc1	Stars
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
Devils	Devils	k1gInSc1	Devils
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
Panthers	Panthersa	k1gFnPc2	Panthersa
a	a	k8xC	a
Calgary	Calgary	k1gNnSc2	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
za	za	k7c4	za
ruský	ruský	k2eAgInSc4d1	ruský
tým	tým	k1gInSc4	tým
Avangard	Avangard	k1gInSc1	Avangard
Omsk	Omsk	k1gInSc1	Omsk
v	v	k7c6	v
KHL	KHL	kA	KHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
spjatý	spjatý	k2eAgInSc1d1	spjatý
s	s	k7c7	s
kladenským	kladenský	k2eAgInSc7d1	kladenský
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
většinovým	většinový	k2eAgMnSc7d1	většinový
majitelem	majitel	k1gMnSc7	majitel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
reprezentací	reprezentace	k1gFnSc7	reprezentace
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
olympijský	olympijský	k2eAgInSc1d1	olympijský
hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
i	i	k8xC	i
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
respektovanou	respektovaný	k2eAgFnSc7d1	respektovaná
a	a	k8xC	a
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
osobností	osobnost	k1gFnSc7	osobnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
hokejové	hokejový	k2eAgMnPc4d1	hokejový
hráče	hráč	k1gMnPc4	hráč
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
NHL	NHL	kA	NHL
získal	získat	k5eAaPmAgMnS	získat
dvakrát	dvakrát	k6eAd1	dvakrát
Stanley	Stanle	k1gMnPc4	Stanle
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
byl	být	k5eAaImAgInS	být
vítězem	vítěz	k1gMnSc7	vítěz
kanadského	kanadský	k2eAgNnSc2d1	kanadské
bodování	bodování	k1gNnSc2	bodování
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
Evropanem	Evropan	k1gMnSc7	Evropan
hrajícím	hrající	k2eAgMnSc7d1	hrající
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
statistických	statistický	k2eAgInPc6d1	statistický
žebříčcích	žebříček	k1gInPc6	žebříček
této	tento	k3xDgFnSc2	tento
kanadsko-americké	kanadskomerický	k2eAgFnSc2d1	kanadsko-americká
ligy	liga	k1gFnSc2	liga
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
předních	přední	k2eAgInPc6d1	přední
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
aktivními	aktivní	k2eAgMnPc7d1	aktivní
hokejisty	hokejista	k1gMnPc7	hokejista
je	být	k5eAaImIp3nS	být
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
asistencí	asistence	k1gFnPc2	asistence
i	i	k8xC	i
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
i	i	k8xC	i
dalších	další	k2eAgInPc2d1	další
rekordů	rekord	k1gInPc2	rekord
NHL	NHL	kA	NHL
–	–	k?	–
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
například	například	k6eAd1	například
nejstarším	starý	k2eAgMnSc7d3	nejstarší
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
hattrick	hattrick	k1gInSc4	hattrick
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
Jágr	Jágr	k1gMnSc1	Jágr
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Šlégrem	Šlégr	k1gMnSc7	Šlégr
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Triple	tripl	k1gInSc5	tripl
Gold	Gold	k1gInSc4	Gold
Clubu	club	k1gInSc2	club
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
hokejové	hokejový	k2eAgFnPc4d1	hokejová
soutěže	soutěž	k1gFnPc4	soutěž
–	–	k?	–
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
byl	být	k5eAaImAgMnS	být
draftován	draftovat	k5eAaImNgMnS	draftovat
týmem	tým	k1gInSc7	tým
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguinsa	k1gFnPc2	Penguinsa
jako	jako	k8xS	jako
celkově	celkově	k6eAd1	celkově
pátá	pátý	k4xOgFnSc1	pátý
volba	volba	k1gFnSc1	volba
1	[number]	k4	1
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
draftu	draft	k1gInSc2	draft
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Jágrův	Jágrův	k2eAgMnSc1d1	Jágrův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
draftován	draftován	k2eAgInSc1d1	draftován
až	až	k9	až
jako	jako	k9	jako
číslo	číslo	k1gNnSc4	číslo
pět	pět	k4xCc1	pět
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
Pittsburghu	Pittsburgh	k1gInSc2	Pittsburgh
<g/>
,	,	kIx,	,
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
nového	nový	k2eAgMnSc2d1	nový
trenéra	trenér	k1gMnSc2	trenér
týmu	tým	k1gInSc2	tým
Boba	Bob	k1gMnSc4	Bob
Johnsona	Johnson	k1gMnSc4	Johnson
<g/>
,	,	kIx,	,
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
i	i	k9	i
skaut	skaut	k1gMnSc1	skaut
týmu	tým	k1gInSc2	tým
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Flyers	Flyers	k1gInSc1	Flyers
Inge	Inge	k1gFnSc4	Inge
Hammarström	Hammarströmo	k1gNnPc2	Hammarströmo
<g/>
.	.	kIx.	.
</s>
<s>
Philadephia	Philadephia	k1gFnSc1	Philadephia
měla	mít	k5eAaImAgFnS	mít
právo	právo	k1gNnSc4	právo
volby	volba	k1gFnSc2	volba
jako	jako	k8xS	jako
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
(	(	kIx(	(
<g/>
před	před	k7c7	před
Pittsburghem	Pittsburgh	k1gInSc7	Pittsburgh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
Jágra	Jágr	k1gInSc2	Jágr
tým	tým	k1gInSc1	tým
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
jméno	jméno	k1gNnSc4	jméno
Mike	Mik	k1gFnSc2	Mik
Ricci	Ricce	k1gMnSc3	Ricce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
draftem	draft	k1gInSc7	draft
experty	expert	k1gMnPc4	expert
pasován	pasován	k2eAgInSc4d1	pasován
jako	jako	k8xS	jako
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Hammarströmovi	Hammarström	k1gMnSc3	Hammarström
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
nepodařilo	podařit	k5eNaPmAgNnS	podařit
přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
své	svůj	k3xOyFgMnPc4	svůj
americké	americký	k2eAgMnPc4d1	americký
kolegy	kolega	k1gMnPc4	kolega
(	(	kIx(	(
<g/>
brali	brát	k5eAaImAgMnP	brát
straku	straka	k1gFnSc4	straka
v	v	k7c6	v
hrsti	hrst	k1gFnSc6	hrst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
1992	[number]	k4	1992
s	s	k7c7	s
"	"	kIx"	"
<g/>
Tučňáky	tučňák	k1gMnPc7	tučňák
<g/>
"	"	kIx"	"
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
hráčem	hráč	k1gMnSc7	hráč
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
(	(	kIx(	(
<g/>
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
branku	branka	k1gFnSc4	branka
ve	v	k7c6	v
finálovém	finálový	k2eAgNnSc6d1	finálové
utkání	utkání	k1gNnSc6	utkání
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Jaromír	Jaromír	k1gMnSc1	Jaromír
Art	Art	k1gMnSc1	Art
Ross	Ross	k1gInSc4	Ross
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
hráči	hráč	k1gMnSc3	hráč
kanadského	kanadský	k2eAgNnSc2d1	kanadské
bodování	bodování	k1gNnSc2	bodování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
remizoval	remizovat	k5eAaPmAgInS	remizovat
s	s	k7c7	s
Ericem	Erice	k1gMnSc7	Erice
Lindrosem	Lindros	k1gMnSc7	Lindros
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
o	o	k7c6	o
výhře	výhra	k1gFnSc6	výhra
musel	muset	k5eAaImAgInS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
počet	počet	k1gInSc1	počet
nastřílených	nastřílený	k2eAgFnPc2d1	nastřílená
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Jágra	Jágra	k1gMnSc1	Jágra
(	(	kIx(	(
<g/>
Jágr	Jágr	k1gMnSc1	Jágr
-	-	kIx~	-
32	[number]	k4	32
<g/>
;	;	kIx,	;
Lindros	Lindrosa	k1gFnPc2	Lindrosa
-	-	kIx~	-
29	[number]	k4	29
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rok	rok	k1gInSc1	rok
Jágr	Jágra	k1gFnPc2	Jágra
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
rekord	rekord	k1gInSc1	rekord
počtu	počet	k1gInSc2	počet
získaných	získaný	k2eAgInPc2d1	získaný
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
evropským	evropský	k2eAgMnSc7d1	evropský
hráčem	hráč	k1gMnSc7	hráč
-	-	kIx~	-
149	[number]	k4	149
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
62	[number]	k4	62
gólů	gól	k1gInPc2	gól
a	a	k8xC	a
87	[number]	k4	87
asistencí	asistence	k1gFnPc2	asistence
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
dosavadním	dosavadní	k2eAgNnSc7d1	dosavadní
maximem	maximum	k1gNnSc7	maximum
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
se	se	k3xPyFc4	se
Jágrovi	Jágr	k1gMnSc3	Jágr
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
4	[number]	k4	4
Art	Art	k1gFnPc2	Art
Ross	Rossa	k1gFnPc2	Rossa
Trophy	Tropha	k1gFnSc2	Tropha
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
Hart	Hart	k2eAgInSc4d1	Hart
Memorial	Memorial	k1gInSc4	Memorial
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
udělovanou	udělovaný	k2eAgFnSc4d1	udělovaná
novináři	novinář	k1gMnPc1	novinář
a	a	k8xC	a
funkcionáři	funkcionář	k1gMnPc1	funkcionář
NHL	NHL	kA	NHL
nejužitečnějšímu	užitečný	k2eAgMnSc3d3	nejužitečnější
hráči	hráč	k1gMnSc3	hráč
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lester	Lester	k1gMnSc1	Lester
B.	B.	kA	B.
Pearson	Pearson	k1gMnSc1	Pearson
Award	Award	k1gMnSc1	Award
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
uděluje	udělovat	k5eAaImIp3nS	udělovat
nejužitečnějšímu	užitečný	k2eAgMnSc3d3	nejužitečnější
hráči	hráč	k1gMnSc3	hráč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
hráči	hráč	k1gMnPc7	hráč
samotnými	samotný	k2eAgMnPc7d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
dovedl	dovést	k5eAaPmAgMnS	dovést
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
českou	český	k2eAgFnSc4d1	Česká
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
reprezentaci	reprezentace	k1gFnSc4	reprezentace
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
až	až	k8xS	až
k	k	k7c3	k
vytouženému	vytoužený	k2eAgNnSc3d1	vytoužené
zlatu	zlato	k1gNnSc3	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
se	se	k3xPyFc4	se
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
nemohl	moct	k5eNaImAgMnS	moct
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
nedařilo	dařit	k5eNaImAgNnS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
musel	muset	k5eAaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
kritice	kritika	k1gFnSc6	kritika
svého	svůj	k3xOyFgInSc2	svůj
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
Ivanem	Ivan	k1gMnSc7	Ivan
Hlinkou	Hlinka	k1gMnSc7	Hlinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Washington	Washington	k1gInSc1	Washington
Capitals	Capitals	k1gInSc1	Capitals
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2001	[number]	k4	2001
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Kučerou	Kučera	k1gMnSc7	Kučera
za	za	k7c2	za
Krise	kris	k1gInSc5	kris
Beeche	Beech	k1gMnSc2	Beech
<g/>
,	,	kIx,	,
Michala	Michal	k1gMnSc2	Michal
Sivka	sivka	k1gFnSc1	sivka
a	a	k8xC	a
Rosse	Rosse	k1gFnSc1	Rosse
Lupaschuka	Lupaschuk	k1gMnSc2	Lupaschuk
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velká	k1gFnSc1	velká
očekávání	očekávání	k1gNnSc2	očekávání
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
Washington	Washington	k1gInSc1	Washington
vkládal	vkládat	k5eAaImAgInS	vkládat
<g/>
,	,	kIx,	,
však	však	k9	však
Jágr	Jágr	k1gMnSc1	Jágr
nenaplnil	naplnit	k5eNaPmAgMnS	naplnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Rangers	Rangers	k1gInSc1	Rangers
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Rangers	Rangers	k1gInSc1	Rangers
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
stávky	stávka	k1gFnSc2	stávka
v	v	k7c6	v
NHL	NHL	kA	NHL
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
hrál	hrát	k5eAaImAgMnS	hrát
Jágr	Jágr	k1gMnSc1	Jágr
nejprve	nejprve	k6eAd1	nejprve
českou	český	k2eAgFnSc4d1	Česká
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soutěž	soutěž	k1gFnSc4	soutěž
za	za	k7c4	za
HC	HC	kA	HC
Kladno	Kladno	k1gNnSc4	Kladno
a	a	k8xC	a
poté	poté	k6eAd1	poté
ruskou	ruský	k2eAgFnSc4d1	ruská
superligu	superliga	k1gFnSc4	superliga
za	za	k7c4	za
Avangard	Avangard	k1gInSc4	Avangard
Omsk	Omsk	k1gInSc1	Omsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
stal	stát	k5eAaPmAgMnS	stát
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
Evropanem	Evropan	k1gMnSc7	Evropan
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
tabulce	tabulka	k1gFnSc6	tabulka
kanadského	kanadský	k2eAgMnSc2d1	kanadský
bodovaní	bodovaný	k2eAgMnPc1d1	bodovaný
dostihl	dostihnout	k5eAaPmAgMnS	dostihnout
Jari	jar	k1gFnSc2	jar
Kurriho	Kurri	k1gMnSc2	Kurri
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výhře	výhra	k1gFnSc3	výhra
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
nad	nad	k7c7	nad
Philadelphií	Philadelphia	k1gFnSc7	Philadelphia
přispěl	přispět	k5eAaPmAgMnS	přispět
dvěma	dva	k4xCgInPc7	dva
góly	gól	k1gInPc7	gól
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
přihrávkou	přihrávka	k1gFnSc7	přihrávka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zaokrouhlil	zaokrouhlit	k5eAaPmAgMnS	zaokrouhlit
svoji	svůj	k3xOyFgFnSc4	svůj
celkovou	celkový	k2eAgFnSc4d1	celková
bilanci	bilance	k1gFnSc4	bilance
v	v	k7c6	v
NHL	NHL	kA	NHL
na	na	k7c4	na
1400	[number]	k4	1400
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ho	on	k3xPp3gNnSc4	on
vynesly	vynést	k5eAaPmAgInP	vynést
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
body	bod	k1gInPc1	bod
za	za	k7c4	za
gól	gól	k1gInSc4	gól
a	a	k8xC	a
asistenci	asistence	k1gFnSc4	asistence
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
Pittsburghu	Pittsburgh	k1gInSc2	Pittsburgh
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
pořadí	pořadí	k1gNnSc6	pořadí
produktivity	produktivita	k1gFnSc2	produktivita
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
před	před	k7c4	před
Slováka	Slovák	k1gMnSc4	Slovák
Stana	Stan	k1gMnSc4	Stan
Mikitu	Mikita	k1gMnSc4	Mikita
<g/>
.	.	kIx.	.
</s>
<s>
Jágr	Jágr	k1gMnSc1	Jágr
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
1468	[number]	k4	1468
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
už	už	k6eAd1	už
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
žádný	žádný	k1gMnSc1	žádný
hokejista	hokejista	k1gMnSc1	hokejista
s	s	k7c7	s
evropskými	evropský	k2eAgInPc7d1	evropský
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Avangard	Avangarda	k1gFnPc2	Avangarda
Omsk	Omsk	k1gInSc1	Omsk
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
podepsal	podepsat	k5eAaPmAgMnS	podepsat
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
ruského	ruský	k2eAgInSc2d1	ruský
týmu	tým	k1gInSc2	tým
Avangard	Avangard	k1gInSc1	Avangard
Omsk	Omsk	k1gInSc1	Omsk
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Přešel	přejít	k5eAaPmAgMnS	přejít
tak	tak	k9	tak
do	do	k7c2	do
Kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
klubu	klub	k1gInSc6	klub
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
NHL	NHL	kA	NHL
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
prodloužit	prodloužit	k5eAaPmF	prodloužit
kontrakt	kontrakt	k1gInSc1	kontrakt
i	i	k9	i
o	o	k7c4	o
sezonu	sezona	k1gFnSc4	sezona
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Flyers	Flyersa	k1gFnPc2	Flyersa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Jágr	Jágr	k1gMnSc1	Jágr
jako	jako	k8xC	jako
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Philadelphií	Philadelphia	k1gFnSc7	Philadelphia
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
39	[number]	k4	39
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
nejstarších	starý	k2eAgMnPc2d3	nejstarší
aktivních	aktivní	k2eAgMnPc2d1	aktivní
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
odehrál	odehrát	k5eAaPmAgInS	odehrát
73	[number]	k4	73
utkání	utkání	k1gNnPc2	utkání
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
54	[number]	k4	54
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dallas	Dallas	k1gInSc1	Dallas
Stars	Stars	k1gInSc1	Stars
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
roční	roční	k2eAgFnSc4d1	roční
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Dallas	Dallas	k1gInSc1	Dallas
Stars	Starsa	k1gFnPc2	Starsa
za	za	k7c4	za
4,55	[number]	k4	4,55
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
premiéře	premiéra	k1gFnSc6	premiéra
za	za	k7c4	za
Dallas	Dallas	k1gInSc4	Dallas
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
4	[number]	k4	4
kanadské	kanadský	k2eAgInPc4d1	kanadský
body	bod	k1gInPc4	bod
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
texaského	texaský	k2eAgInSc2d1	texaský
klubu	klub	k1gInSc2	klub
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
34	[number]	k4	34
utkání	utkání	k1gNnSc2	utkání
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
14	[number]	k4	14
branek	branka	k1gFnPc2	branka
a	a	k8xC	a
připsal	připsat	k5eAaPmAgMnS	připsat
si	se	k3xPyFc3	se
12	[number]	k4	12
asistencí	asistence	k1gFnPc2	asistence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc4	Bruins
za	za	k7c4	za
duo	duo	k1gNnSc4	duo
mladíků	mladík	k1gMnPc2	mladík
Lane	Lane	k1gNnSc2	Lane
MacDermid	MacDermida	k1gFnPc2	MacDermida
a	a	k8xC	a
Cody	coda	k1gFnSc2	coda
Payne	Payn	k1gInSc5	Payn
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
draftu	draft	k1gInSc6	draft
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jágr	Jágr	k1gMnSc1	Jágr
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
působišti	působiště	k1gNnSc6	působiště
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k9	i
s	s	k7c7	s
krajanem	krajan	k1gMnSc7	krajan
Davidem	David	k1gMnSc7	David
Krejčím	Krejčí	k1gMnSc7	Krejčí
a	a	k8xC	a
se	s	k7c7	s
slovenským	slovenský	k2eAgMnSc7d1	slovenský
obráncem	obránce	k1gMnSc7	obránce
Zdenem	Zden	k1gMnSc7	Zden
Chárou	Chára	k1gMnSc7	Chára
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
kapitánem	kapitán	k1gMnSc7	kapitán
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mužstvem	mužstvo	k1gNnSc7	mužstvo
došel	dojít	k5eAaPmAgInS	dojít
až	až	k6eAd1	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tým	tým	k1gInSc1	tým
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Chicago	Chicago	k1gNnSc4	Chicago
Blackhawks	Blackhawksa	k1gFnPc2	Blackhawksa
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
prohrál	prohrát	k5eAaPmAgInS	prohrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
sezony	sezona	k1gFnSc2	sezona
už	už	k6eAd1	už
mu	on	k3xPp3gMnSc3	on
vedení	vedení	k1gNnSc1	vedení
Bruins	Bruinsa	k1gFnPc2	Bruinsa
nenabídlo	nabídnout	k5eNaPmAgNnS	nabídnout
prodloužení	prodloužení	k1gNnSc4	prodloužení
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
Jágr	Jágr	k1gMnSc1	Jágr
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
hledat	hledat	k5eAaImF	hledat
nové	nový	k2eAgNnSc4d1	nové
angažmá	angažmá	k1gNnSc4	angažmá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
New	New	k1gMnPc7	New
Jersey	Jersea	k1gFnSc2	Jersea
Devils	Devils	k1gInSc1	Devils
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Jágr	Jágr	k1gMnSc1	Jágr
roční	roční	k2eAgFnSc4d1	roční
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
Devils	Devils	k1gInSc4	Devils
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
třemi	tři	k4xCgMnPc7	tři
českými	český	k2eAgMnPc7d1	český
hokejisty	hokejista	k1gMnPc7	hokejista
–	–	k?	–
Patrikem	Patrik	k1gMnSc7	Patrik
Eliášem	Eliáš	k1gMnSc7	Eliáš
<g/>
,	,	kIx,	,
Markem	Marek	k1gMnSc7	Marek
Židlickým	Židlický	k1gMnSc7	Židlický
a	a	k8xC	a
Rostislavem	Rostislav	k1gMnSc7	Rostislav
Oleszem	Olesz	k1gMnSc7	Olesz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klubu	klub	k1gInSc6	klub
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Florida	Florida	k1gFnSc1	Florida
Panthers	Panthersa	k1gFnPc2	Panthersa
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
Jágr	Jágr	k1gInSc1	Jágr
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
z	z	k7c2	z
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Florida	Florida	k1gFnSc1	Florida
Panthers	Panthers	k1gInSc4	Panthers
za	za	k7c4	za
volbu	volba	k1gFnSc4	volba
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
draftu	draft	k1gInSc2	draft
2015	[number]	k4	2015
a	a	k8xC	a
podmíněnou	podmíněný	k2eAgFnSc4d1	podmíněná
volbu	volba	k1gFnSc4	volba
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
draftu	draft	k1gInSc2	draft
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Floridy	Florida	k1gFnSc2	Florida
projevilo	projevit	k5eAaPmAgNnS	projevit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
kladenského	kladenský	k2eAgMnSc4d1	kladenský
rodáka	rodák	k1gMnSc4	rodák
ještě	ještě	k9	ještě
šest	šest	k4xCc4	šest
jiných	jiný	k2eAgInPc2d1	jiný
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
manažer	manažer	k1gMnSc1	manažer
Panthers	Panthers	k1gInSc4	Panthers
Dale	Dale	k1gNnSc2	Dale
Tallon	Tallon	k1gInSc1	Tallon
se	se	k3xPyFc4	se
k	k	k7c3	k
přestupu	přestup	k1gInSc3	přestup
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
vítězit	vítězit	k5eAaImF	vítězit
a	a	k8xC	a
získávat	získávat	k5eAaImF	získávat
trofeje	trofej	k1gFnPc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Dáváme	dávat	k5eAaImIp1nP	dávat
dohromady	dohromady	k6eAd1	dohromady
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kousky	kousek	k1gInPc4	kousek
skládačky	skládačka	k1gFnSc2	skládačka
<g/>
,	,	kIx,	,
jdeme	jít	k5eAaImIp1nP	jít
krok	krok	k1gInSc4	krok
po	po	k7c6	po
kroku	krok	k1gInSc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Doufáme	doufat	k5eAaImIp1nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jaromír	Jaromír	k1gMnSc1	Jaromír
dovede	dovést	k5eAaPmIp3nS	dovést
náš	náš	k3xOp1gInSc4	náš
tým	tým	k1gInSc4	tým
do	do	k7c2	do
země	zem	k1gFnSc2	zem
zaslíbené	zaslíbená	k1gFnSc2	zaslíbená
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
ještě	ještě	k6eAd1	ještě
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
bych	by	kYmCp1nS	by
ho	on	k3xPp3gMnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
vidět	vidět	k5eAaImF	vidět
hrát	hrát	k5eAaImF	hrát
za	za	k7c4	za
náš	náš	k3xOp1gInSc4	náš
klub	klub	k1gInSc4	klub
ještě	ještě	k9	ještě
rok	rok	k1gInSc4	rok
nebo	nebo	k8xC	nebo
i	i	k9	i
dva	dva	k4xCgInPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
fyzička	fyzička	k1gFnSc1	fyzička
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
perfektní	perfektní	k2eAgMnSc1d1	perfektní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sám	sám	k3xTgMnSc1	sám
Jágr	Jágr	k1gMnSc1	Jágr
se	se	k3xPyFc4	se
k	k	k7c3	k
přestupu	přestup	k1gInSc3	přestup
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
jasně	jasně	k6eAd1	jasně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemám	mít	k5eNaImIp1nS	mít
problém	problém	k1gInSc4	problém
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Florida	Florida	k1gFnSc1	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
zápas	zápas	k1gInSc1	zápas
je	být	k5eAaImIp3nS	být
kritický	kritický	k2eAgInSc1d1	kritický
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
o	o	k7c4	o
play-off	playff	k1gInSc4	play-off
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
tohle	tenhle	k3xDgNnSc1	tenhle
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
naší	náš	k3xOp1gFnSc3	náš
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
beru	brát	k5eAaImIp1nS	brát
to	ten	k3xDgNnSc1	ten
zase	zase	k9	zase
jako	jako	k9	jako
další	další	k2eAgFnSc4d1	další
výzvu	výzva	k1gFnSc4	výzva
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
dohrál	dohrát	k5eAaPmAgInS	dohrát
zbytek	zbytek	k1gInSc1	zbytek
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
mladých	mladý	k2eAgMnPc2d1	mladý
útočníků	útočník	k1gMnPc2	útočník
Huberdeaua	Huberdeau	k1gInSc2	Huberdeau
a	a	k8xC	a
Barkova	Barkov	k1gInSc2	Barkov
začal	začít	k5eAaPmAgInS	začít
sbírat	sbírat	k5eAaImF	sbírat
body	bod	k1gInPc4	bod
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
týmu	tým	k1gInSc3	tým
přiblížit	přiblížit	k5eAaPmF	přiblížit
se	se	k3xPyFc4	se
k	k	k7c3	k
pozicím	pozice	k1gFnPc3	pozice
pro	pro	k7c4	pro
postup	postup	k1gInSc4	postup
do	do	k7c2	do
play	play	k0	play
off	off	k?	off
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Panthers	Panthers	k1gInSc4	Panthers
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
roční	roční	k2eAgFnSc6d1	roční
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
.	.	kIx.	.
<g/>
Dařilo	dařit	k5eAaImAgNnS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
i	i	k9	i
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
ročníku	ročník	k1gInSc6	ročník
<g/>
,	,	kIx,	,
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
27	[number]	k4	27
góly	gól	k1gInPc7	gól
a	a	k8xC	a
39	[number]	k4	39
asistencemi	asistence	k1gFnPc7	asistence
nejproduktivnějším	produktivní	k2eAgMnSc7d3	nejproduktivnější
hráčem	hráč	k1gMnSc7	hráč
Floridy	Florida	k1gFnSc2	Florida
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
českým	český	k2eAgMnSc7d1	český
hokejistou	hokejista	k1gMnSc7	hokejista
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
příspěvku	příspěvek	k1gInSc3	příspěvek
se	se	k3xPyFc4	se
Florida	Florida	k1gFnSc1	Florida
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
play	play	k0	play
off	off	k?	off
(	(	kIx(	(
<g/>
teprve	teprve	k6eAd1	teprve
podruhé	podruhé	k6eAd1	podruhé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c6	na
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Islanders	Islandersa	k1gFnPc2	Islandersa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
však	však	k9	však
nenavázal	navázat	k5eNaPmAgMnS	navázat
na	na	k7c4	na
výkony	výkon	k1gInPc4	výkon
předchozích	předchozí	k2eAgInPc2d1	předchozí
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
asistence	asistence	k1gFnPc4	asistence
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
týmu	tým	k1gInSc2	tým
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nedařilo	dařit	k5eNaImAgNnS	dařit
a	a	k8xC	a
Panthers	Panthers	k1gInSc4	Panthers
nakonec	nakonec	k6eAd1	nakonec
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
Islanders	Islanders	k1gInSc4	Islanders
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
oznámil	oznámit	k5eAaPmAgInS	oznámit
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
roční	roční	k2eAgInSc4d1	roční
kontrakt	kontrakt	k1gInSc4	kontrakt
s	s	k7c7	s
Floridou	Florida	k1gFnSc7	Florida
se	s	k7c7	s
základním	základní	k2eAgInSc7d1	základní
platem	plat	k1gInSc7	plat
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
oddanost	oddanost	k1gFnSc4	oddanost
hokeji	hokej	k1gInSc6	hokej
oceněn	oceněn	k2eAgMnSc1d1	oceněn
Bill	Bill	k1gMnSc1	Bill
Masterton	Masterton	k1gInSc4	Masterton
Memorial	Memorial	k1gInSc4	Memorial
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
individuální	individuální	k2eAgFnSc1d1	individuální
cena	cena	k1gFnSc1	cena
v	v	k7c6	v
NHL	NHL	kA	NHL
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
sezoně	sezona	k1gFnSc6	sezona
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
se	se	k3xPyFc4	se
Jaromíru	Jaromír	k1gMnSc3	Jaromír
Jágrovi	Jágr	k1gMnSc3	Jágr
zpočátku	zpočátku	k6eAd1	zpočátku
nedařilo	dařit	k5eNaImAgNnS	dařit
bodově	bodově	k6eAd1	bodově
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
během	během	k7c2	během
několika	několik	k4yIc2	několik
zápasů	zápas	k1gInPc2	zápas
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
asistenci	asistence	k1gFnSc3	asistence
proti	proti	k7c3	proti
Bostonu	Boston	k1gInSc3	Boston
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
historického	historický	k2eAgNnSc2d1	historické
pořadí	pořadí	k1gNnSc2	pořadí
kanadského	kanadský	k2eAgNnSc2d1	kanadské
bodování	bodování	k1gNnSc2	bodování
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
křídelník	křídelník	k1gInSc1	křídelník
tak	tak	k6eAd1	tak
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
bod	bod	k1gInSc4	bod
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
(	(	kIx(	(
<g/>
755	[number]	k4	755
<g/>
+	+	kIx~	+
<g/>
1133	[number]	k4	1133
<g/>
)	)	kIx)	)
a	a	k8xC	a
překonal	překonat	k5eAaPmAgMnS	překonat
Marka	Marek	k1gMnSc4	Marek
Messiera	Messier	k1gMnSc4	Messier
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
získal	získat	k5eAaPmAgMnS	získat
46	[number]	k4	46
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
vypršela	vypršet	k5eAaPmAgFnS	vypršet
roční	roční	k2eAgFnSc1d1	roční
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Floridou	Florida	k1gFnSc7	Florida
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
ohlášená	ohlášený	k2eAgNnPc4d1	ohlášené
jednání	jednání	k1gNnPc4	jednání
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
obnovení	obnovení	k1gNnSc3	obnovení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
volným	volný	k2eAgMnSc7d1	volný
hráčem	hráč	k1gMnSc7	hráč
a	a	k8xC	a
následně	následně	k6eAd1	následně
generální	generální	k2eAgMnSc1d1	generální
manažer	manažer	k1gMnSc1	manažer
klubu	klub	k1gInSc2	klub
Dale	Dale	k1gInSc1	Dale
Tallon	Tallon	k1gInSc1	Tallon
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jágr	Jágr	k1gMnSc1	Jágr
už	už	k6eAd1	už
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
pokračovat	pokračovat	k5eAaImF	pokračovat
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Calgary	Calgary	k1gNnSc1	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
2017	[number]	k4	2017
se	se	k3xPyFc4	se
objevovalo	objevovat	k5eAaImAgNnS	objevovat
množství	množství	k1gNnSc1	množství
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Jágr	Jágr	k1gMnSc1	Jágr
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
sezóně	sezóna	k1gFnSc6	sezóna
zamíří	zamířit	k5eAaPmIp3nS	zamířit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
preferoval	preferovat	k5eAaImAgMnS	preferovat
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
týmy	tým	k1gInPc7	tým
jeho	on	k3xPp3gNnSc2	on
agent	agent	k1gMnSc1	agent
vyjednával	vyjednávat	k5eAaImAgMnS	vyjednávat
<g/>
,	,	kIx,	,
reálná	reálný	k2eAgFnSc1d1	reálná
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
nastoupí	nastoupit	k5eAaPmIp3nP	nastoupit
za	za	k7c4	za
prvoligové	prvoligový	k2eAgNnSc4d1	prvoligové
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
dalšího	další	k2eAgInSc2d1	další
ročníku	ročník	k1gInSc2	ročník
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
podepsal	podepsat	k5eAaPmAgMnS	podepsat
s	s	k7c7	s
Calgary	Calgary	k1gNnPc7	Calgary
Flames	Flames	k1gMnSc1	Flames
roční	roční	k2eAgInSc4d1	roční
kontrakt	kontrakt	k1gInSc4	kontrakt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
zaručil	zaručit	k5eAaPmAgInS	zaručit
milion	milion	k4xCgInSc1	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
milion	milion	k4xCgInSc1	milion
v	v	k7c6	v
bonusech	bonus	k1gInPc6	bonus
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
tak	tak	k6eAd1	tak
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
kanadský	kanadský	k2eAgInSc4d1	kanadský
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ročníku	ročník	k1gInSc2	ročník
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
zápasy	zápas	k1gInPc4	zápas
vynechal	vynechat	k5eAaPmAgInS	vynechat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
fyzičku	fyzička	k1gFnSc4	fyzička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ledě	led	k1gInSc6	led
poté	poté	k6eAd1	poté
nedostával	dostávat	k5eNaImAgMnS	dostávat
takový	takový	k3xDgInSc4	takový
prostor	prostor	k1gInSc4	prostor
jako	jako	k9	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgMnS	potýkat
se	s	k7c7	s
zraněními	zranění	k1gNnPc7	zranění
třísel	tříslo	k1gNnPc2	tříslo
a	a	k8xC	a
kolena	koleno	k1gNnSc2	koleno
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
odehrál	odehrát	k5eAaPmAgInS	odehrát
pouze	pouze	k6eAd1	pouze
22	[number]	k4	22
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
z	z	k7c2	z
39	[number]	k4	39
možných	možný	k2eAgMnPc2d1	možný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
a	a	k8xC	a
šest	šest	k4xCc4	šest
asistencí	asistence	k1gFnPc2	asistence
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
za	za	k7c7	za
Calgary	Calgary	k1gNnSc7	Calgary
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
týdnech	týden	k1gInPc6	týden
nehrál	hrát	k5eNaImAgInS	hrát
kvůli	kvůli	k7c3	kvůli
opakujícím	opakující	k2eAgInPc3d1	opakující
se	se	k3xPyFc4	se
zdravotním	zdravotní	k2eAgFnPc3d1	zdravotní
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
jej	on	k3xPp3gNnSc4	on
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
listinu	listina	k1gFnSc4	listina
volných	volný	k2eAgMnPc2d1	volný
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
předznamenává	předznamenávat	k5eAaImIp3nS	předznamenávat
přeřazení	přeřazení	k1gNnSc4	přeřazení
hokejisty	hokejista	k1gMnSc2	hokejista
na	na	k7c4	na
farmu	farma	k1gFnSc4	farma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Jágra	Jágra	k1gMnSc1	Jágra
žádný	žádný	k3yNgInSc4	žádný
jiný	jiný	k2eAgInSc4d1	jiný
klub	klub	k1gInSc4	klub
NHL	NHL	kA	NHL
neangažoval	angažovat	k5eNaBmAgMnS	angažovat
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
rodného	rodný	k2eAgNnSc2d1	rodné
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
je	být	k5eAaImIp3nS	být
také	také	k9	také
majitelem	majitel	k1gMnSc7	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
roční	roční	k2eAgFnSc1d1	roční
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Calgary	Calgary	k1gNnSc7	Calgary
však	však	k9	však
nadále	nadále	k6eAd1	nadále
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rytíři	rytíř	k1gMnPc1	rytíř
Kladno	Kladno	k1gNnSc1	Kladno
(	(	kIx(	(
<g/>
od	od	k7c2	od
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
se	se	k3xPyFc4	se
Jágr	Jágr	k1gMnSc1	Jágr
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Kladnem	Kladno	k1gNnSc7	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Kladno	Kladno	k1gNnSc1	Kladno
nepostoupilo	postoupit	k5eNaPmAgNnS	postoupit
do	do	k7c2	do
extraligové	extraligový	k2eAgFnSc2d1	extraligová
baráže	baráž	k1gFnSc2	baráž
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
by	by	kYmCp3nS	by
Třinci	Třinec	k1gInSc3	Třinec
v	v	k7c6	v
extraligovém	extraligový	k2eAgNnSc6d1	extraligové
play	play	k0	play
off	off	k?	off
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nejistotu	nejistota	k1gFnSc4	nejistota
ohledně	ohledně	k7c2	ohledně
stavu	stav	k1gInSc2	stav
kolena	koleno	k1gNnSc2	koleno
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
hned	hned	k6eAd1	hned
v	v	k7c6	v
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
prvoligovém	prvoligový	k2eAgInSc6d1	prvoligový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Benátkám	Benátky	k1gFnPc3	Benátky
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
zájmu	zájem	k1gInSc3	zájem
diváků	divák	k1gMnPc2	divák
bylo	být	k5eAaImAgNnS	být
utkání	utkání	k1gNnSc1	utkání
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
domácí	domácí	k1gMnPc4	domácí
navíc	navíc	k6eAd1	navíc
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
Petr	Petr	k1gMnSc1	Petr
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ukončil	ukončit	k5eAaPmAgInS	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
a	a	k8xC	a
který	který	k3yIgMnSc1	který
zápas	zápas	k1gInSc4	zápas
bral	brát	k5eAaImAgMnS	brát
jako	jako	k8xC	jako
rozloučení	rozloučení	k1gNnSc1	rozloučení
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kariérou	kariéra	k1gFnSc7	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jágr	Jágr	k1gMnSc1	Jágr
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
třikrát	třikrát	k6eAd1	třikrát
asistoval	asistovat	k5eAaImAgMnS	asistovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
se	se	k3xPyFc4	se
Kladno	Kladno	k1gNnSc1	Kladno
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
probojovalo	probojovat	k5eAaPmAgNnS	probojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
baráže	baráž	k1gFnSc2	baráž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
Extraligy	extraliga	k1gFnSc2	extraliga
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devátém	devátý	k4xOgNnSc6	devátý
barážovém	barážový	k2eAgNnSc6d1	barážové
kole	kolo	k1gNnSc6	kolo
proti	proti	k7c3	proti
Motoru	motor	k1gInSc3	motor
České	český	k2eAgFnSc2d1	Česká
Budějovice	Budějovice	k1gInPc1	Budějovice
Jágr	Jágr	k1gMnSc1	Jágr
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
čtyři	čtyři	k4xCgFnPc4	čtyři
branky	branka	k1gFnPc4	branka
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
tak	tak	k6eAd1	tak
výhru	výhra	k1gFnSc4	výhra
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
neoficiální	oficiální	k2eNgInSc4d1	neoficiální
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
zápas	zápas	k1gInSc4	zápas
odehrál	odehrát	k5eAaPmAgMnS	odehrát
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1989	[number]	k4	1989
proti	proti	k7c3	proti
Calgary	Calgary	k1gNnSc3	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
jako	jako	k8xS	jako
junior	junior	k1gMnSc1	junior
získal	získat	k5eAaPmAgMnS	získat
stříbro	stříbro	k1gNnSc4	stříbro
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
na	na	k7c6	na
juniorském	juniorský	k2eAgInSc6d1	juniorský
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
1990	[number]	k4	1990
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
akci	akce	k1gFnSc6	akce
se	se	k3xPyFc4	se
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
kategorii	kategorie	k1gFnSc6	kategorie
Jágr	Jágr	k1gMnSc1	Jágr
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
při	při	k7c6	při
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
mladíky	mladík	k1gInPc7	mladík
–	–	k?	–
Robertem	Robert	k1gMnSc7	Robert
Reichelem	Reichel	k1gMnSc7	Reichel
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
Holíkem	Holík	k1gMnSc7	Holík
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
výběr	výběr	k1gInSc1	výběr
zde	zde	k6eAd1	zde
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Československo	Československo	k1gNnSc4	Československo
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgMnS	představit
ještě	ještě	k9	ještě
při	při	k7c6	při
Kanadském	kanadský	k2eAgInSc6d1	kanadský
poháru	pohár	k1gInSc6	pohár
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
obsadil	obsadit	k5eAaPmAgMnS	obsadit
poslední	poslední	k2eAgNnSc4d1	poslední
šesté	šestý	k4xOgNnSc4	šestý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
českou	český	k2eAgFnSc4d1	Česká
reprezentaci	reprezentace	k1gFnSc4	reprezentace
poprvé	poprvé	k6eAd1	poprvé
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
z	z	k7c2	z
play	play	k0	play
off	off	k?	off
NHL	NHL	kA	NHL
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
se	s	k7c7	s
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
z	z	k7c2	z
Pittsburghu	Pittsburgh	k1gInSc2	Pittsburgh
Martinem	Martin	k1gMnSc7	Martin
Strakou	Straka	k1gMnSc7	Straka
<g/>
.	.	kIx.	.
</s>
<s>
Mužstvu	mužstvo	k1gNnSc3	mužstvo
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nepomohli	pomoct	k5eNaPmAgMnP	pomoct
od	od	k7c2	od
vyřazení	vyřazení	k1gNnSc2	vyřazení
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgInSc6	který
celek	celek	k1gInSc4	celek
obsadil	obsadit	k5eAaPmAgMnS	obsadit
nelichotivé	lichotivý	k2eNgNnSc4d1	nelichotivé
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
Jágr	Jágr	k1gInSc4	Jágr
i	i	k9	i
u	u	k7c2	u
neúspěchu	neúspěch	k1gInSc2	neúspěch
na	na	k7c6	na
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
přicestoval	přicestovat	k5eAaPmAgInS	přicestovat
na	na	k7c4	na
Olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
do	do	k7c2	do
japonského	japonský	k2eAgNnSc2d1	Japonské
Nagana	Nagano	k1gNnSc2	Nagano
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
díky	díky	k7c3	díky
přerušení	přerušení	k1gNnSc3	přerušení
NHL	NHL	kA	NHL
hrály	hrát	k5eAaImAgInP	hrát
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
výběry	výběr	k1gInPc1	výběr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
zde	zde	k6eAd1	zde
slavila	slavit	k5eAaImAgFnS	slavit
historicky	historicky	k6eAd1	historicky
poprvé	poprvé	k6eAd1	poprvé
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
mužstvo	mužstvo	k1gNnSc1	mužstvo
vyřazeno	vyřadit	k5eAaPmNgNnS	vyřadit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgMnSc1d1	stejný
soupeř	soupeř	k1gMnSc1	soupeř
opět	opět	k6eAd1	opět
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
i	i	k9	i
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Jágr	Jágr	k1gMnSc1	Jágr
rovněž	rovněž	k9	rovněž
představil	představit	k5eAaPmAgMnS	představit
díky	dík	k1gInPc4	dík
neúčasti	neúčast	k1gFnSc2	neúčast
jeho	jeho	k3xOp3gInSc2	jeho
Washingtonu	Washington	k1gInSc2	Washington
v	v	k7c6	v
play	play	k0	play
off	off	k?	off
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
si	se	k3xPyFc3	se
Jágr	Jágr	k1gMnSc1	Jágr
zahrál	zahrát	k5eAaPmAgMnS	zahrát
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tentokrát	tentokrát	k6eAd1	tentokrát
byl	být	k5eAaImAgInS	být
tým	tým	k1gInSc1	tým
vyřazen	vyřadit	k5eAaPmNgInS	vyřadit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
po	po	k7c6	po
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Trofej	trofej	k1gFnSc1	trofej
pro	pro	k7c4	pro
mistry	mistr	k1gMnPc4	mistr
světa	svět	k1gInSc2	svět
tedy	tedy	k9	tedy
získal	získat	k5eAaPmAgInS	získat
až	až	k9	až
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
šampionátu	šampionát	k1gInSc6	šampionát
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
díky	díky	k7c3	díky
výluce	výluka	k1gFnSc3	výluka
v	v	k7c6	v
NHL	NHL	kA	NHL
nastoupily	nastoupit	k5eAaPmAgFnP	nastoupit
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
složené	složený	k2eAgInPc1d1	složený
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
hrál	hrát	k5eAaImAgInS	hrát
i	i	k9	i
na	na	k7c6	na
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
vyřazen	vyřazen	k2eAgInSc1d1	vyřazen
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
získal	získat	k5eAaPmAgInS	získat
bronz	bronz	k1gInSc4	bronz
<g/>
,	,	kIx,	,
Jágr	Jágr	k1gMnSc1	Jágr
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
reprezentaci	reprezentace	k1gFnSc4	reprezentace
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
až	až	k9	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejproduktivnější	produktivní	k2eAgMnSc1d3	nejproduktivnější
evropský	evropský	k2eAgMnSc1d1	evropský
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
NHL	NHL	kA	NHL
tuto	tento	k3xDgFnSc4	tento
ligu	liga	k1gFnSc4	liga
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nechyběl	chybět	k5eNaImAgMnS	chybět
na	na	k7c6	na
žádné	žádný	k3yNgFnSc6	žádný
velké	velký	k2eAgFnSc6d1	velká
akci	akce	k1gFnSc6	akce
-	-	kIx~	-
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
MS	MS	kA	MS
2009	[number]	k4	2009
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
OH	OH	kA	OH
2010	[number]	k4	2010
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
zlatého	zlatý	k1gInSc2	zlatý
MS	MS	kA	MS
2010	[number]	k4	2010
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
i	i	k8xC	i
MS	MS	kA	MS
2011	[number]	k4	2011
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
převzal	převzít	k5eAaPmAgInS	převzít
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
medaili	medaile	k1gFnSc4	medaile
Za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
o	o	k7c4	o
stát	stát	k1gInSc4	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
hokejistou	hokejista	k1gMnSc7	hokejista
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
této	tento	k3xDgFnSc2	tento
pocty	pocta	k1gFnSc2	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Jágrem	Jágr	k1gInSc7	Jágr
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
oceněni	ocenit	k5eAaPmNgMnP	ocenit
Augustin	Augustin	k1gMnSc1	Augustin
Bubník	Bubník	k1gMnSc1	Bubník
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Hlinka	Hlinka	k1gMnSc1	Hlinka
a	a	k8xC	a
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
hattrick	hattrick	k1gInSc4	hattrick
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
ve	v	k7c6	v
čtvrtfinálovém	čtvrtfinálový	k2eAgInSc6d1	čtvrtfinálový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
MS	MS	kA	MS
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
útočníkem	útočník	k1gMnSc7	útočník
podle	podle	k7c2	podle
direktoriátu	direktoriát	k1gInSc2	direktoriát
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
členem	člen	k1gInSc7	člen
All-star	Alltar	k1gInSc1	All-star
teamu	team	k1gInSc2	team
sestavovaného	sestavovaný	k2eAgInSc2d1	sestavovaný
sportovními	sportovní	k2eAgFnPc7d1	sportovní
novináři	novinář	k1gMnPc7	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohraném	prohraný	k2eAgInSc6d1	prohraný
duelu	duel	k1gInSc6	duel
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
na	na	k7c6	na
MS	MS	kA	MS
2014	[number]	k4	2014
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
oznámil	oznámit	k5eAaPmAgMnS	oznámit
ve	v	k7c6	v
42	[number]	k4	42
letech	léto	k1gNnPc6	léto
svůj	svůj	k3xOyFgInSc4	svůj
konec	konec	k1gInSc4	konec
v	v	k7c6	v
reprezentačním	reprezentační	k2eAgInSc6d1	reprezentační
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
ještě	ještě	k9	ještě
před	před	k7c7	před
domácím	domácí	k2eAgNnSc7d1	domácí
MS	MS	kA	MS
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
přehodnotil	přehodnotit	k5eAaPmAgMnS	přehodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
šampionátu	šampionát	k1gInSc6	šampionát
si	se	k3xPyFc3	se
Jágr	Jágr	k1gMnSc1	Jágr
připsal	připsat	k5eAaPmAgMnS	připsat
9	[number]	k4	9
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
6	[number]	k4	6
gólů	gól	k1gInPc2	gól
a	a	k8xC	a
3	[number]	k4	3
asistence	asistence	k1gFnSc2	asistence
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
Jakubu	Jakub	k1gMnSc6	Jakub
Voráčkovi	Voráček	k1gMnSc6	Voráček
druhým	druhý	k4xOgNnSc7	druhý
nejproduktivnějším	produktivní	k2eAgMnSc7d3	nejproduktivnější
hráčem	hráč	k1gMnSc7	hráč
českého	český	k2eAgInSc2d1	český
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šampionátu	šampionát	k1gInSc6	šampionát
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Nejužitečnějším	užitečný	k2eAgMnSc7d3	nejužitečnější
hráčem	hráč	k1gMnSc7	hráč
(	(	kIx(	(
<g/>
MVP	MVP	kA	MVP
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
All-star	Alltara	k1gFnPc2	All-stara
Teamu	team	k1gInSc2	team
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
následně	následně	k6eAd1	následně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
ukončení	ukončení	k1gNnSc4	ukončení
reprezentační	reprezentační	k2eAgFnSc2d1	reprezentační
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
<g/>
Trenéři	trenér	k1gMnPc1	trenér
národního	národní	k2eAgInSc2d1	národní
týmu	tým	k1gInSc2	tým
mu	on	k3xPp3gMnSc3	on
nabízeli	nabízet	k5eAaImAgMnP	nabízet
možnost	možnost	k1gFnSc4	možnost
účasti	účast	k1gFnSc2	účast
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
Jágr	Jágr	k1gMnSc1	Jágr
se	se	k3xPyFc4	se
ale	ale	k9	ale
po	po	k7c6	po
vypadnutí	vypadnutí	k1gNnSc6	vypadnutí
Floridy	Florida	k1gFnSc2	Florida
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
play	play	k0	play
off	off	k?	off
omluvil	omluvit	k5eAaPmAgMnS	omluvit
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
náročnou	náročný	k2eAgFnSc4d1	náročná
sezónu	sezóna	k1gFnSc4	sezóna
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
únavu	únava	k1gFnSc4	únava
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
možnost	možnost	k1gFnSc4	možnost
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
pro	pro	k7c4	pro
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
záporně	záporně	k6eAd1	záporně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c4	na
prázdniny	prázdniny	k1gFnPc4	prázdniny
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
dědečkem	dědeček	k1gMnSc7	dědeček
do	do	k7c2	do
Nezdic	Nezdice	k1gFnPc2	Nezdice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
začátky	začátek	k1gInPc1	začátek
jeho	jeho	k3xOp3gFnSc2	jeho
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
brzy	brzy	k6eAd1	brzy
vstával	vstávat	k5eAaImAgMnS	vstávat
a	a	k8xC	a
chodil	chodit	k5eAaImAgMnS	chodit
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
dědečkem	dědeček	k1gMnSc7	dědeček
trénovat	trénovat	k5eAaImF	trénovat
hokej	hokej	k1gInSc4	hokej
na	na	k7c4	na
lední	lední	k2eAgFnSc4d1	lední
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
Střední	střední	k2eAgFnSc4d1	střední
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
školu	škola	k1gFnSc4	škola
stavební	stavební	k2eAgFnSc4d1	stavební
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc4	studium
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
tréninkům	trénink	k1gInPc3	trénink
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
si	se	k3xPyFc3	se
doplnil	doplnit	k5eAaPmAgMnS	doplnit
vzdělání	vzdělání	k1gNnSc2	vzdělání
získáním	získání	k1gNnSc7	získání
maturity	maturita	k1gFnSc2	maturita
na	na	k7c6	na
Soukromém	soukromý	k2eAgNnSc6d1	soukromé
středním	střední	k2eAgNnSc6d1	střední
učilišti	učiliště	k1gNnSc6	učiliště
hotelového	hotelový	k2eAgInSc2d1	hotelový
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
známý	známý	k1gMnSc1	známý
svým	svůj	k3xOyFgNnSc7	svůj
sázením	sázení	k1gNnSc7	sázení
v	v	k7c6	v
kasinech	kasino	k1gNnPc6	kasino
<g/>
.	.	kIx.	.
<g/>
Jágr	Jágr	k1gInSc1	Jágr
je	být	k5eAaImIp3nS	být
pravoslavným	pravoslavný	k2eAgMnSc7d1	pravoslavný
křesťanem	křesťan	k1gMnSc7	křesťan
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
modlí	modlit	k5eAaImIp3nS	modlit
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
Bibli	bible	k1gFnSc4	bible
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
liturgie	liturgie	k1gFnSc1	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
slovenský	slovenský	k2eAgInSc4d1	slovenský
deník	deník	k1gInSc4	deník
SME	SME	k?	SME
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
věřící	věřící	k1gFnSc1	věřící
<g/>
,	,	kIx,	,
stoprocentně	stoprocentně	k6eAd1	stoprocentně
věřící	věřící	k2eAgMnPc1d1	věřící
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gNnSc4	on
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
pražský	pražský	k2eAgMnSc1d1	pražský
pravoslavný	pravoslavný	k2eAgMnSc1d1	pravoslavný
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
jsem	být	k5eAaImIp1nS	být
vycítil	vycítit	k5eAaPmAgMnS	vycítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
potřebuju	potřebovat	k5eAaImIp1nS	potřebovat
<g/>
,	,	kIx,	,
řekla	říct	k5eAaPmAgFnS	říct
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
intuice	intuice	k1gFnSc1	intuice
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
taky	taky	k6eAd1	taky
jsem	být	k5eAaImIp1nS	být
potkal	potkat	k5eAaPmAgMnS	potkat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jsem	být	k5eAaImIp1nS	být
asi	asi	k9	asi
potkat	potkat	k5eAaPmF	potkat
měl	mít	k5eAaImAgMnS	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
skončil	skončit	k5eAaPmAgMnS	skončit
u	u	k7c2	u
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
neřeším	řešit	k5eNaImIp1nS	řešit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pro	pro	k7c4	pro
Idnes	Idnes	k1gInSc4	Idnes
<g/>
.	.	kIx.	.
<g/>
czBěhem	czBěh	k1gInSc7	czBěh
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
víry	víra	k1gFnSc2	víra
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
víře	víra	k1gFnSc6	víra
to	ten	k3xDgNnSc1	ten
nejkrásnější	krásný	k2eAgNnSc1d3	nejkrásnější
<g/>
?	?	kIx.	?
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
víru	víra	k1gFnSc4	víra
nemůžeš	moct	k5eNaImIp2nS	moct
očurat	očurat	k5eAaImF	očurat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ti	ty	k3xPp2nSc3	ty
nikdo	nikdo	k3yNnSc1	nikdo
nezaručí	zaručit	k5eNaPmIp3nS	zaručit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
opravdu	opravdu	k6eAd1	opravdu
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
lidem	člověk	k1gMnPc3	člověk
někdo	někdo	k3yInSc1	někdo
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
když	když	k8xS	když
budete	být	k5eAaImBp2nP	být
hodní	hodný	k2eAgMnPc1d1	hodný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
najisto	najisto	k9	najisto
dostanete	dostat	k5eAaPmIp2nP	dostat
někam	někam	k6eAd1	někam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
strašně	strašně	k6eAd1	strašně
skvělý	skvělý	k2eAgMnSc1d1	skvělý
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
jim	on	k3xPp3gMnPc3	on
jasný	jasný	k2eAgInSc4d1	jasný
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ty	ten	k3xDgMnPc4	ten
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
vyčuraní	vyčuraný	k2eAgMnPc1d1	vyčuraný
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
chovali	chovat	k5eAaImAgMnP	chovat
hezky	hezky	k6eAd1	hezky
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
někde	někde	k6eAd1	někde
ještě	ještě	k6eAd1	ještě
líp	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
by	by	kYmCp3nS	by
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
touha	touha	k1gFnSc1	touha
dělat	dělat	k5eAaImF	dělat
dobrý	dobrý	k2eAgInSc1d1	dobrý
věci	věc	k1gFnSc3	věc
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
právě	právě	k9	právě
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevíš	vědět	k5eNaImIp2nS	vědět
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
snažíš	snažit	k5eAaImIp2nS	snažit
být	být	k5eAaImF	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Nejistota	nejistota	k1gFnSc1	nejistota
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
víře	víra	k1gFnSc6	víra
vlastně	vlastně	k9	vlastně
to	ten	k3xDgNnSc1	ten
nejzajímavější	zajímavý	k2eAgNnSc1d3	nejzajímavější
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
někam	někam	k6eAd1	někam
dál	daleko	k6eAd2	daleko
dostanou	dostat	k5eAaPmIp3nP	dostat
jen	jen	k9	jen
ty	ten	k3xDgFnPc4	ten
opravdu	opravdu	k6eAd1	opravdu
dobrý	dobrý	k2eAgMnSc1d1	dobrý
duše	duše	k1gFnPc4	duše
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
deník	deník	k1gInSc4	deník
Sport	sport	k1gInSc4	sport
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
na	na	k7c4	na
dotaz	dotaz	k1gInSc4	dotaz
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yRnSc6	co
ho	on	k3xPp3gMnSc4	on
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
nejvíce	hodně	k6eAd3	hodně
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Asi	asi	k9	asi
nejvíc	hodně	k6eAd3	hodně
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
silně	silně	k6eAd1	silně
věřící	věřící	k2eAgMnPc1d1	věřící
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
když	když	k8xS	když
někdo	někdo	k3yInSc1	někdo
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
si	se	k3xPyFc3	se
druzí	druhý	k4xOgMnPc1	druhý
klepali	klepat	k5eAaImAgMnP	klepat
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
<g/>
...	...	k?	...
Tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
nestydím	stydět	k5eNaImIp1nS	stydět
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
změna	změna	k1gFnSc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
cítím	cítit	k5eAaImIp1nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tohle	tenhle	k3xDgNnSc4	tenhle
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
<s>
Obava	obava	k1gFnSc1	obava
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
někdo	někdo	k3yInSc1	někdo
bude	být	k5eAaImBp3nS	být
smát	smát	k5eAaImF	smát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
mně	já	k3xPp1nSc6	já
už	už	k9	už
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ztratil	ztratit	k5eAaPmAgMnS	ztratit
jsem	být	k5eAaImIp1nS	být
ostych	ostych	k1gInSc4	ostych
říkat	říkat	k5eAaImF	říkat
názory	názor	k1gInPc4	názor
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
nikdo	nikdo	k3yNnSc1	nikdo
nestydí	stydět	k5eNaImIp3nS	stydět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíš	spíš	k9	spíš
naopak	naopak	k6eAd1	naopak
–	–	k?	–
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
nevěřící	věřící	k2eNgNnSc1d1	nevěřící
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
ostatní	ostatní	k2eAgMnPc1d1	ostatní
diví	divit	k5eAaImIp3nP	divit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgMnSc1d1	svobodný
a	a	k8xC	a
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
chodil	chodit	k5eAaImAgMnS	chodit
s	s	k7c7	s
Ivou	Iva	k1gFnSc7	Iva
Kubelkovou	Kubelková	k1gFnSc7	Kubelková
<g/>
,	,	kIx,	,
Nicol	Nicol	k1gInSc1	Nicol
Lenertovou	Lenertová	k1gFnSc4	Lenertová
<g/>
,	,	kIx,	,
Andreou	Andrea	k1gFnSc7	Andrea
Verešovou	verešův	k2eAgFnSc7d1	verešův
<g/>
,	,	kIx,	,
Lucií	Lucie	k1gFnSc7	Lucie
Borhyovou	Borhyový	k2eAgFnSc7d1	Borhyová
<g/>
,	,	kIx,	,
Innou	Inný	k2eAgFnSc7d1	Inný
Puhajkovou	Puhajková	k1gFnSc7	Puhajková
a	a	k8xC	a
Veronikou	Veronika	k1gFnSc7	Veronika
Kopřivovou	Kopřivová	k1gFnSc7	Kopřivová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
aktivity	aktivita	k1gFnPc1	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
Jágr	Jágr	k1gMnSc1	Jágr
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
reklam	reklama	k1gFnPc2	reklama
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tváří	tvář	k1gFnSc7	tvář
společnosti	společnost	k1gFnSc2	společnost
Klenoty	klenot	k1gInPc1	klenot
Aurum	Aurum	k1gInSc1	Aurum
a	a	k8xC	a
mobilů	mobil	k1gInPc2	mobil
značky	značka	k1gFnSc2	značka
Huawei	Huawe	k1gFnSc2	Huawe
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
také	také	k9	také
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
klipu	klip	k1gInSc6	klip
Ewy	Ewy	k1gMnSc5	Ewy
Farne	Farn	k1gMnSc5	Farn
"	"	kIx"	"
<g/>
Na	na	k7c6	na
ostří	ostří	k1gNnSc6	ostří
nože	nůž	k1gInSc2	nůž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
Jágrovy	Jágrův	k2eAgInPc4d1	Jágrův
největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
patří	patřit	k5eAaImIp3nS	patřit
zlato	zlato	k1gNnSc1	zlato
z	z	k7c2	z
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
zisk	zisk	k1gInSc4	zisk
dvou	dva	k4xCgMnPc2	dva
Stanley	Stanleum	k1gNnPc7	Stanleum
Cupů	cup	k1gInPc2	cup
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
2005	[number]	k4	2005
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Art	Art	k1gFnSc1	Art
Ross	Rossa	k1gFnPc2	Rossa
Trophy	Tropha	k1gMnSc2	Tropha
pro	pro	k7c4	pro
nejproduktivnějšího	produktivní	k2eAgMnSc4d3	nejproduktivnější
hráče	hráč	k1gMnSc4	hráč
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
získal	získat	k5eAaPmAgMnS	získat
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
Hart	Hart	k2eAgInSc1d1	Hart
Memorial	Memorial	k1gInSc1	Memorial
Trophy	Tropha	k1gMnSc2	Tropha
pro	pro	k7c4	pro
nejužitečnějšího	užitečný	k2eAgMnSc4d3	nejužitečnější
hráče	hráč	k1gMnSc4	hráč
za	za	k7c4	za
ročník	ročník	k1gInSc4	ročník
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
českého	český	k2eAgInSc2d1	český
Klubu	klub	k1gInSc2	klub
sportovních	sportovní	k2eAgMnPc2d1	sportovní
novinářů	novinář	k1gMnPc2	novinář
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Šlégrem	Šlégr	k1gMnSc7	Šlégr
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Triple	tripl	k1gInSc5	tripl
Gold	Gold	k1gInSc4	Gold
Clubu	club	k1gInSc2	club
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
hlavní	hlavní	k2eAgInPc1d1	hlavní
hokejové	hokejový	k2eAgInPc1d1	hokejový
turnaje	turnaj	k1gInPc1	turnaj
–	–	k?	–
Stanley	Stanle	k2eAgInPc1d1	Stanle
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejvýše	vysoce	k6eAd3	vysoce
postaveným	postavený	k2eAgMnSc7d1	postavený
hráčem	hráč	k1gMnSc7	hráč
kanadského	kanadský	k2eAgNnSc2d1	kanadské
bodování	bodování	k1gNnSc2	bodování
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
povedlo	povést	k5eAaPmAgNnS	povést
(	(	kIx(	(
<g/>
Wayne	Wayn	k1gMnSc5	Wayn
Gretzky	Gretzka	k1gFnSc2	Gretzka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
hráčem	hráč	k1gMnSc7	hráč
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
statistice	statistika	k1gFnSc6	statistika
kanadského	kanadský	k2eAgNnSc2d1	kanadské
bodování	bodování	k1gNnSc2	bodování
v	v	k7c6	v
NHL	NHL	kA	NHL
před	před	k7c7	před
Jágrem	Jágr	k1gInSc7	Jágr
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
ani	ani	k8xC	ani
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sportovec	sportovec	k1gMnSc1	sportovec
světa	svět	k1gInSc2	svět
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
vlajkonošem	vlajkonoš	k1gMnSc7	vlajkonoš
na	na	k7c6	na
ZOH	ZOH	kA	ZOH
2010	[number]	k4	2010
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
NHL	NHL	kA	NHL
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sta	sto	k4xCgNnSc2	sto
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hráčů	hráč	k1gMnPc2	hráč
historie	historie	k1gFnSc2	historie
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Individuální	individuální	k2eAgFnSc1d1	individuální
===	===	k?	===
</s>
</p>
<p>
<s>
Art	Art	k?	Art
Ross	Ross	k1gInSc1	Ross
Trophy	Tropha	k1gMnSc2	Tropha
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
kanadského	kanadský	k2eAgNnSc2d1	kanadské
bodování	bodování	k1gNnSc2	bodování
<g/>
)	)	kIx)	)
-	-	kIx~	-
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Hart	Hart	k2eAgInSc1d1	Hart
Memorial	Memorial	k1gInSc1	Memorial
Trophy	Troph	k1gInPc1	Troph
(	(	kIx(	(
<g/>
nejužitečnější	užitečný	k2eAgMnSc1d3	nejužitečnější
hráč	hráč	k1gMnSc1	hráč
ligy	liga	k1gFnSc2	liga
<g/>
)	)	kIx)	)
-	-	kIx~	-
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Lester	Lester	k1gMnSc1	Lester
B.	B.	kA	B.
Pearson	Pearson	k1gMnSc1	Pearson
Award	Award	k1gMnSc1	Award
(	(	kIx(	(
<g/>
nejužitečnější	užitečný	k2eAgMnSc1d3	nejužitečnější
hráč	hráč	k1gMnSc1	hráč
ligy	liga	k1gFnSc2	liga
dle	dle	k7c2	dle
hlasování	hlasování	k1gNnSc2	hlasování
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
-	-	kIx~	-
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Bill	Bill	k1gMnSc1	Bill
Masterton	Masterton	k1gInSc4	Masterton
Memorial	Memorial	k1gInSc4	Memorial
Trophy	Tropha	k1gFnSc2	Tropha
(	(	kIx(	(
<g/>
za	za	k7c4	za
oddanost	oddanost	k1gFnSc4	oddanost
hokeji	hokej	k1gInSc6	hokej
<g/>
)	)	kIx)	)
–	–	k?	–
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hokejka	hokejka	k1gFnSc1	hokejka
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
)	)	kIx)	)
-	-	kIx~	-
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
Člen	člen	k1gMnSc1	člen
prvního	první	k4xOgInSc2	první
NHL	NHL	kA	NHL
All-Star	All-Star	k1gInSc4	All-Star
Teamu	team	k1gInSc2	team
-	-	kIx~	-
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Člen	člen	k1gMnSc1	člen
druhého	druhý	k4xOgInSc2	druhý
NHL	NHL	kA	NHL
All-Star	All-Star	k1gInSc4	All-Star
Teamu	team	k1gInSc2	team
-	-	kIx~	-
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Člen	člen	k1gMnSc1	člen
NHL	NHL	kA	NHL
All-Rookie	All-Rookie	k1gFnSc1	All-Rookie
Teamu	team	k1gInSc2	team
-	-	kIx~	-
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Michel	Michelit	k5eAaPmRp2nS	Michelit
Briere	Brier	k1gMnSc5	Brier
Memorial	Memorial	k1gMnSc1	Memorial
Rookie	Rookie	k1gFnSc2	Rookie
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
Trophy	Tropha	k1gMnSc2	Tropha
(	(	kIx(	(
<g/>
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
nováčka	nováček	k1gMnSc4	nováček
týmu	tým	k1gInSc2	tým
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
<g/>
)	)	kIx)	)
–	–	k?	–
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Člen	člen	k1gMnSc1	člen
All-Star	All-Star	k1gMnSc1	All-Star
Teamu	team	k1gInSc2	team
na	na	k7c4	na
MS	MS	kA	MS
20	[number]	k4	20
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
Člen	člen	k1gMnSc1	člen
All-Star	All-Star	k1gMnSc1	All-Star
Teamu	team	k1gInSc2	team
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
a	a	k8xC	a
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
Nejužitečnější	užitečný	k2eAgMnSc1d3	nejužitečnější
hráč	hráč	k1gMnSc1	hráč
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
držitel	držitel	k1gMnSc1	držitel
medaile	medaile	k1gFnSc2	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
stát	stát	k1gInSc4	stát
-	-	kIx~	-
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
člen	člen	k1gInSc1	člen
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
</s>
</p>
<p>
<s>
==	==	k?	==
Rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
střelec	střelec	k1gMnSc1	střelec
gólu	gól	k1gInSc2	gól
(	(	kIx(	(
<g/>
43	[number]	k4	43
let	léto	k1gNnPc2	léto
a	a	k8xC	a
88	[number]	k4	88
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
NHL	NHL	kA	NHL
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
asistencí	asistence	k1gFnPc2	asistence
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nováček	nováček	k1gMnSc1	nováček
–	–	k?	–
5	[number]	k4	5
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
–	–	k?	–
149	[number]	k4	149
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
asistencí	asistence	k1gFnPc2	asistence
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
–	–	k?	–
87	[number]	k4	87
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
získaných	získaný	k2eAgInPc2d1	získaný
evropským	evropský	k2eAgMnSc7d1	evropský
hráčem	hráč	k1gMnSc7	hráč
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
–	–	k?	–
149	[number]	k4	149
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
získaných	získaný	k2eAgInPc2d1	získaný
nekanadským	kanadský	k2eNgMnSc7d1	kanadský
hráčem	hráč	k1gMnSc7	hráč
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
–	–	k?	–
149	[number]	k4	149
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
získaných	získaný	k2eAgInPc2d1	získaný
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
kariéru	kariéra	k1gFnSc4	kariéra
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgInS	začít
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
–	–	k?	–
1802	[number]	k4	1802
(	(	kIx(	(
<g/>
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
gólů	gól	k1gInPc2	gól
nastřílených	nastřílený	k2eAgInPc2d1	nastřílený
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
kariéru	kariéra	k1gFnSc4	kariéra
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgInS	začít
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
–	–	k?	–
722	[number]	k4	722
(	(	kIx(	(
<g/>
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
získaných	získaný	k2eAgInPc2d1	získaný
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
kariéru	kariéra	k1gFnSc4	kariéra
hráčem	hráč	k1gMnSc7	hráč
narozeným	narozený	k2eAgMnSc7d1	narozený
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
–	–	k?	–
1802	[number]	k4	1802
(	(	kIx(	(
<g/>
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
gólů	gól	k1gInPc2	gól
v	v	k7c4	v
prodloužení	prodloužení	k1gNnSc4	prodloužení
–	–	k?	–
19	[number]	k4	19
(	(	kIx(	(
<g/>
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
sezón	sezóna	k1gFnPc2	sezóna
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
s	s	k7c7	s
minimálně	minimálně	k6eAd1	minimálně
30	[number]	k4	30
nastřílenými	nastřílený	k2eAgFnPc7d1	nastřílená
brankami	branka	k1gFnPc7	branka
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
15	[number]	k4	15
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zkrácené	zkrácený	k2eAgFnSc2d1	zkrácená
sezóny	sezóna	k1gFnSc2	sezóna
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
<g/>
,	,	kIx,	,
48	[number]	k4	48
zápasů	zápas	k1gInPc2	zápas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
sezón	sezóna	k1gFnPc2	sezóna
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
s	s	k7c7	s
minimálně	minimálně	k6eAd1	minimálně
70	[number]	k4	70
kanadskými	kanadský	k2eAgInPc7d1	kanadský
body	bod	k1gInPc7	bod
–	–	k?	–
15	[number]	k4	15
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zkrácené	zkrácený	k2eAgFnSc2d1	zkrácená
sezóny	sezóna	k1gFnSc2	sezóna
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
<g/>
,	,	kIx,	,
48	[number]	k4	48
zápasů	zápas	k1gInPc2	zápas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
hokejových	hokejový	k2eAgInPc2d1	hokejový
stadionů	stadion	k1gInPc2	stadion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hráči	hráč	k1gMnSc3	hráč
podařilo	podařit	k5eAaPmAgNnS	podařit
vstřelit	vstřelit	k5eAaPmF	vstřelit
gól	gól	k1gInSc4	gól
–	–	k?	–
53	[number]	k4	53
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
věk	věk	k1gInSc1	věk
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
vstřelení	vstřelení	k1gNnSc2	vstřelení
hattricku	hattrick	k1gInSc2	hattrick
–	–	k?	–
42	[number]	k4	42
let	léto	k1gNnPc2	léto
a	a	k8xC	a
322	[number]	k4	322
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
vítězných	vítězný	k2eAgInPc2d1	vítězný
gólů	gól	k1gInPc2	gól
–	–	k?	–
135	[number]	k4	135
</s>
</p>
<p>
<s>
===	===	k?	===
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Rangers	Rangers	k1gInSc1	Rangers
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
nastřílených	nastřílený	k2eAgFnPc2d1	nastřílená
hráčem	hráč	k1gMnSc7	hráč
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Rangers	Rangersa	k1gFnPc2	Rangersa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
54	[number]	k4	54
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
získaných	získaný	k2eAgInPc2d1	získaný
hráčem	hráč	k1gMnSc7	hráč
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Rangers	Rangersa	k1gFnPc2	Rangersa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
123	[number]	k4	123
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
přesilovkových	přesilovkový	k2eAgInPc2d1	přesilovkový
gólů	gól	k1gInPc2	gól
nastřílených	nastřílený	k2eAgInPc2d1	nastřílený
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
hráčem	hráč	k1gMnSc7	hráč
New	New	k1gMnSc7	New
York	York	k1gInSc4	York
Rangers	Rangers	k1gInSc1	Rangers
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
24	[number]	k4	24
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
střel	střela	k1gFnPc2	střela
na	na	k7c4	na
branku	branka	k1gFnSc4	branka
vyslaných	vyslaný	k2eAgFnPc2d1	vyslaná
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
hráčem	hráč	k1gMnSc7	hráč
New	New	k1gMnSc7	New
York	York	k1gInSc4	York
Rangers	Rangers	k1gInSc1	Rangers
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
368	[number]	k4	368
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
vítězných	vítězný	k2eAgInPc2d1	vítězný
gólů	gól	k1gInPc2	gól
nastřílených	nastřílený	k2eAgInPc2d1	nastřílený
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
hráčem	hráč	k1gMnSc7	hráč
New	New	k1gMnSc7	New
York	York	k1gInSc4	York
Rangers	Rangers	k1gInSc1	Rangers
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
9	[number]	k4	9
(	(	kIx(	(
<g/>
sdíleno	sdílet	k5eAaImNgNnS	sdílet
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Messierem	Messier	k1gMnSc7	Messier
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
97	[number]	k4	97
a	a	k8xC	a
Donem	Don	k1gMnSc7	Don
Murdochem	Murdoch	k1gMnSc7	Murdoch
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
81	[number]	k4	81
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
nastřílených	nastřílený	k2eAgInPc2d1	nastřílený
pravým	pravý	k2eAgNnSc7d1	pravé
křídlem	křídlo	k1gNnSc7	křídlo
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Rangers	Rangersa	k1gFnPc2	Rangersa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
54	[number]	k4	54
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
dosažených	dosažený	k2eAgFnPc2d1	dosažená
asistencí	asistence	k1gFnPc2	asistence
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
pravým	pravý	k2eAgNnSc7d1	pravé
křídlem	křídlo	k1gNnSc7	křídlo
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Rangers	Rangersa	k1gFnPc2	Rangersa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
69	[number]	k4	69
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
získaných	získaný	k2eAgInPc2d1	získaný
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
pravým	pravý	k2eAgNnSc7d1	pravé
křídlem	křídlo	k1gNnSc7	křídlo
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Rangers	Rangersa	k1gFnPc2	Rangersa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
123	[number]	k4	123
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
NHL	NHL	kA	NHL
===	===	k?	===
</s>
</p>
<p>
<s>
Stav	stav	k1gInSc1	stav
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
<g/>
Počet	počet	k1gInSc4	počet
zápasů	zápas	k1gInPc2	zápas
–	–	k?	–
1733	[number]	k4	1733
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
gólů	gól	k1gInPc2	gól
–	–	k?	–
766	[number]	k4	766
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
asistencí	asistence	k1gFnPc2	asistence
–	–	k?	–
1155	[number]	k4	1155
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
–	–	k?	–
1921	[number]	k4	1921
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Play	play	k0	play
off	off	k?	off
NHL	NHL	kA	NHL
===	===	k?	===
</s>
</p>
<p>
<s>
Stav	stav	k1gInSc1	stav
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
<g/>
Počet	počet	k1gInSc4	počet
zápasů	zápas	k1gInPc2	zápas
–	–	k?	–
208	[number]	k4	208
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
gólů	gól	k1gInPc2	gól
–	–	k?	–
78	[number]	k4	78
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
asistencí	asistence	k1gFnPc2	asistence
–	–	k?	–
123	[number]	k4	123
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
–	–	k?	–
201	[number]	k4	201
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
<g/>
Český	český	k2eAgInSc1d1	český
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
NHL	NHL	kA	NHL
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
zápase	zápas	k1gInSc6	zápas
–	–	k?	–
7	[number]	k4	7
(	(	kIx(	(
<g/>
3	[number]	k4	3
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
4	[number]	k4	4
asistence	asistence	k1gFnSc1	asistence
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc4	Penguins
-	-	kIx~	-
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc1	Islanders
9	[number]	k4	9
:	:	kIx,	:
3	[number]	k4	3
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
Washington	Washington	k1gInSc1	Washington
Capitals	Capitals	k1gInSc4	Capitals
-	-	kIx~	-
Florida	Florida	k1gFnSc1	Florida
Panthers	Panthers	k1gInSc1	Panthers
12	[number]	k4	12
:	:	kIx,	:
2	[number]	k4	2
</s>
</p>
<p>
<s>
===	===	k?	===
Klubové	klubový	k2eAgFnPc1d1	klubová
statistiky	statistika	k1gFnPc1	statistika
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
statistiky	statistika	k1gFnPc1	statistika
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
567	[number]	k4	567
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
775	[number]	k4	775
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902586	[number]	k4	902586
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
242	[number]	k4	242
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
268	[number]	k4	268
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
100	[number]	k4	100
let	let	k1gInSc1	let
českého	český	k2eAgInSc2d1	český
sportu	sport	k1gInSc2	sport
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7376	[number]	k4	7376
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
362	[number]	k4	362
<g/>
-	-	kIx~	-
<g/>
363	[number]	k4	363
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hráčů	hráč	k1gMnPc2	hráč
NHL	NHL	kA	NHL
s	s	k7c7	s
1000	[number]	k4	1000
a	a	k8xC	a
více	hodně	k6eAd2	hodně
body	bod	k1gInPc4	bod
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hráčů	hráč	k1gMnPc2	hráč
NHL	NHL	kA	NHL
s	s	k7c7	s
500	[number]	k4	500
a	a	k8xC	a
více	hodně	k6eAd2	hodně
góly	gól	k1gInPc4	gól
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágra	k1gFnPc2	Jágra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
</s>
</p>
