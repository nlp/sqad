<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velikost	velikost	k1gFnSc4	velikost
schopnosti	schopnost	k1gFnSc2	schopnost
působit	působit	k5eAaImF	působit
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
