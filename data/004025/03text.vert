<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velikost	velikost	k1gFnSc4	velikost
schopnosti	schopnost	k1gFnSc2	schopnost
působit	působit	k5eAaImF	působit
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
určitou	určitý	k2eAgFnSc4d1	určitá
vlastnost	vlastnost	k1gFnSc4	vlastnost
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
mezi	mezi	k7c7	mezi
tělesy	těleso	k1gNnPc7	těleso
(	(	kIx(	(
<g/>
částicemi	částice	k1gFnPc7	částice
<g/>
)	)	kIx)	)
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
elektrického	elektrický	k2eAgNnSc2d1	elektrické
nebo	nebo	k8xC	nebo
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
veličiny	veličina	k1gFnSc2	veličina
<g/>
:	:	kIx,	:
Q	Q	kA	Q
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
quantity	quantit	k1gInPc1	quantit
of	of	k?	of
charge	charg	k1gInSc2	charg
<g/>
)	)	kIx)	)
Jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
coulomb	coulomb	k1gInSc1	coulomb
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
C	C	kA	C
Další	další	k1gNnPc4	další
používané	používaný	k2eAgFnSc2d1	používaná
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
milicoulomb	milicoulomb	k1gInSc1	milicoulomb
mC	mC	k?	mC
<g/>
,	,	kIx,	,
mikrocoulomb	mikrocoulomb	k1gMnSc1	mikrocoulomb
μ	μ	k?	μ
<g/>
,	,	kIx,	,
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
Měřidlo	měřidlo	k1gNnSc1	měřidlo
<g/>
:	:	kIx,	:
elektrometr	elektrometr	k1gInSc1	elektrometr
<g/>
,	,	kIx,	,
elektroskop	elektroskop	k1gInSc1	elektroskop
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgFnSc7d1	skalární
veličinou	veličina	k1gFnSc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
(	(	kIx(	(
<g/>
částice	částice	k1gFnSc1	částice
<g/>
)	)	kIx)	)
s	s	k7c7	s
nulovým	nulový	k2eAgInSc7d1	nulový
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
elektricky	elektricky	k6eAd1	elektricky
neutrálními	neutrální	k2eAgNnPc7d1	neutrální
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
(	(	kIx(	(
<g/>
částice	částice	k1gFnSc1	částice
<g/>
)	)	kIx)	)
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
(	(	kIx(	(
<g/>
říkáme	říkat	k5eAaImIp1nP	říkat
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesou	nést	k5eAaImIp3nP	nést
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
nabitá	nabitý	k2eAgNnPc1d1	nabité
tělesa	těleso	k1gNnPc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
kladnou	kladný	k2eAgFnSc4d1	kladná
nebo	nebo	k8xC	nebo
zápornou	záporný	k2eAgFnSc4d1	záporná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
kladná	kladný	k2eAgFnSc1d1	kladná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tělesech	těleso	k1gNnPc6	těleso
nesoucích	nesoucí	k2eAgInPc2d1	nesoucí
kladný	kladný	k2eAgInSc4d1	kladný
náboj	náboj	k1gInSc4	náboj
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
kladně	kladně	k6eAd1	kladně
nabitá	nabitý	k2eAgNnPc1d1	nabité
<g/>
,	,	kIx,	,
a	a	k8xC	a
tělesa	těleso	k1gNnPc1	těleso
se	s	k7c7	s
záporným	záporný	k2eAgInSc7d1	záporný
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
záporně	záporně	k6eAd1	záporně
nabitá	nabitý	k2eAgFnSc1d1	nabitá
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
působící	působící	k2eAgFnPc1d1	působící
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
nabitými	nabitý	k2eAgFnPc7d1	nabitá
(	(	kIx(	(
<g/>
nepohybujícími	pohybující	k2eNgFnPc7d1	nepohybující
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
tělesy	těleso	k1gNnPc7	těleso
jsou	být	k5eAaImIp3nP	být
přitažlivé	přitažlivý	k2eAgNnSc1d1	přitažlivé
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
mají	mít	k5eAaImIp3nP	mít
tělesa	těleso	k1gNnPc1	těleso
náboje	náboj	k1gInSc2	náboj
s	s	k7c7	s
opačnými	opačný	k2eAgNnPc7d1	opačné
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
,	,	kIx,	,
a	a	k8xC	a
odpudivé	odpudivý	k2eAgNnSc1d1	odpudivé
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
tělesa	těleso	k1gNnPc1	těleso
náboje	náboj	k1gInSc2	náboj
se	s	k7c7	s
shodnými	shodný	k2eAgNnPc7d1	shodné
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
elektrostatické	elektrostatický	k2eAgInPc4d1	elektrostatický
<g/>
.	.	kIx.	.
</s>
<s>
Pohybující	pohybující	k2eAgMnSc1d1	pohybující
se	se	k3xPyFc4	se
nabitá	nabitý	k2eAgNnPc1d1	nabité
tělesa	těleso	k1gNnPc1	těleso
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navíc	navíc	k6eAd1	navíc
působí	působit	k5eAaImIp3nP	působit
magnetickými	magnetický	k2eAgFnPc7d1	magnetická
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pohybující	pohybující	k2eAgMnSc1d1	pohybující
se	se	k3xPyFc4	se
elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
popisován	popisovat	k5eAaImNgInS	popisovat
pomocí	pomocí	k7c2	pomocí
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
kladného	kladný	k2eAgInSc2d1	kladný
a	a	k8xC	a
záporného	záporný	k2eAgInSc2d1	záporný
náboje	náboj	k1gInSc2	náboj
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
pouze	pouze	k6eAd1	pouze
konvencí	konvence	k1gFnSc7	konvence
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgInS	být
směr	směr	k1gInSc1	směr
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
stanoven	stanovit	k5eAaPmNgInS	stanovit
jako	jako	k8xC	jako
směr	směr	k1gInSc1	směr
od	od	k7c2	od
kladného	kladný	k2eAgInSc2d1	kladný
pólu	pól	k1gInSc2	pól
k	k	k7c3	k
zápornému	záporný	k2eAgMnSc3d1	záporný
<g/>
;	;	kIx,	;
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektronu	elektron	k1gInSc2	elektron
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
přisouzen	přisouzen	k2eAgInSc1d1	přisouzen
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
tělesa	těleso	k1gNnSc2	těleso
roven	roven	k2eAgInSc1d1	roven
algebraickému	algebraický	k2eAgInSc3d1	algebraický
součtu	součet	k1gInSc3	součet
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
označuje	označovat	k5eAaImIp3nS	označovat
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
}	}	kIx)	}
-té	é	k?	-té
části	část	k1gFnSc2	část
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
celkový	celkový	k2eAgInSc4d1	celkový
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
tělesa	těleso	k1gNnPc1	těleso
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
nositele	nositel	k1gMnSc4	nositel
kladného	kladný	k2eAgMnSc4d1	kladný
i	i	k8xC	i
záporného	záporný	k2eAgInSc2d1	záporný
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celková	celkový	k2eAgFnSc1d1	celková
hodnota	hodnota	k1gFnSc1	hodnota
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
takového	takový	k3xDgNnSc2	takový
tělesa	těleso	k1gNnSc2	těleso
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
těleso	těleso	k1gNnSc1	těleso
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
tělesa	těleso	k1gNnSc2	těleso
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
i	i	k9	i
takové	takový	k3xDgNnSc4	takový
těleso	těleso	k1gNnSc4	těleso
působit	působit	k5eAaImF	působit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
určitými	určitý	k2eAgFnPc7d1	určitá
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Nebudou	být	k5eNaImBp3nP	být
<g/>
-li	i	k?	-li
nositelé	nositel	k1gMnPc1	nositel
náboje	náboj	k1gInPc1	náboj
rozptýleni	rozptýlen	k2eAgMnPc1d1	rozptýlen
po	po	k7c6	po
tělese	těleso	k1gNnSc6	těleso
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
působení	působení	k1gNnSc2	působení
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
projevovat	projevovat	k5eAaImF	projevovat
i	i	k9	i
v	v	k7c6	v
makroskopickém	makroskopický	k2eAgNnSc6d1	makroskopické
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc4	takový
tělesa	těleso	k1gNnPc4	těleso
pak	pak	k6eAd1	pak
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k8xC	jako
polarizovaná	polarizovaný	k2eAgNnPc4d1	polarizované
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
makroskopických	makroskopický	k2eAgNnPc2d1	makroskopické
těles	těleso	k1gNnPc2	těleso
můžeme	moct	k5eAaImIp1nP	moct
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
rozložení	rozložení	k1gNnSc2	rozložení
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
využít	využít	k5eAaPmF	využít
hustotu	hustota	k1gFnSc4	hustota
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
však	však	k9	však
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
není	být	k5eNaImIp3nS	být
rozložení	rozložení	k1gNnSc1	rozložení
náboje	náboj	k1gInSc2	náboj
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
podstatné	podstatný	k2eAgInPc1d1	podstatný
<g/>
,	,	kIx,	,
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
těleso	těleso	k1gNnSc4	těleso
můžeme	moct	k5eAaImIp1nP	moct
nahradit	nahradit	k5eAaPmF	nahradit
tzv.	tzv.	kA	tzv.
bodovým	bodový	k2eAgInSc7d1	bodový
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
bodového	bodový	k2eAgInSc2d1	bodový
náboje	náboj	k1gInSc2	náboj
je	být	k5eAaImIp3nS	být
analogií	analogie	k1gFnSc7	analogie
pojmu	pojem	k1gInSc2	pojem
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
působení	působení	k1gNnSc6	působení
(	(	kIx(	(
<g/>
interakci	interakce	k1gFnSc6	interakce
<g/>
)	)	kIx)	)
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkový	celkový	k2eAgInSc4d1	celkový
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
k	k	k7c3	k
interakci	interakce	k1gFnSc3	interakce
dochází	docházet	k5eAaImIp3nP	docházet
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
samovolnému	samovolný	k2eAgInSc3d1	samovolný
vzniku	vznik	k1gInSc3	vznik
nebo	nebo	k8xC	nebo
zániku	zánik	k1gInSc3	zánik
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
náboje	náboj	k1gInSc2	náboj
v	v	k7c6	v
elektricky	elektricky	k6eAd1	elektricky
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
soustavě	soustava	k1gFnSc6	soustava
tedy	tedy	k9	tedy
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
nelze	lze	k6eNd1	lze
elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
vytvořit	vytvořit	k5eAaPmF	vytvořit
ani	ani	k8xC	ani
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
jen	jen	k9	jen
přemístit	přemístit	k5eAaPmF	přemístit
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgInPc1d1	elektrický
náboje	náboj	k1gInPc1	náboj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
volně	volně	k6eAd1	volně
pohybovat	pohybovat	k5eAaImF	pohybovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
vodičích	vodič	k1gInPc6	vodič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
volné	volný	k2eAgInPc1d1	volný
náboje	náboj	k1gInPc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
polarizaci	polarizace	k1gFnSc6	polarizace
dielektrika	dielektrikum	k1gNnSc2	dielektrikum
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
polarizační	polarizační	k2eAgInSc1d1	polarizační
náboj	náboj	k1gInSc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Polarizační	polarizační	k2eAgInPc1d1	polarizační
náboje	náboj	k1gInPc1	náboj
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nábojů	náboj	k1gInPc2	náboj
ve	v	k7c6	v
vodičích	vodič	k1gInPc6	vodič
nemohou	moct	k5eNaImIp3nP	moct
v	v	k7c6	v
dielektriku	dielektrikum	k1gNnSc6	dielektrikum
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
na	na	k7c6	na
makroskopické	makroskopický	k2eAgFnSc6d1	makroskopická
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
vázané	vázaný	k2eAgInPc1d1	vázaný
náboje	náboj	k1gInPc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hodnota	hodnota	k1gFnSc1	hodnota
vázaných	vázaný	k2eAgInPc2d1	vázaný
nábojů	náboj	k1gInPc2	náboj
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
polarizací	polarizace	k1gFnPc2	polarizace
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
objemu	objem	k1gInSc6	objem
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
a	a	k8xC	a
rozložení	rozložení	k1gNnSc4	rozložení
těchto	tento	k3xDgInPc2	tento
polarizačních	polarizační	k2eAgInPc2d1	polarizační
nábojů	náboj	k1gInPc2	náboj
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
elektrostatického	elektrostatický	k2eAgMnSc2d1	elektrostatický
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
jiného	jiný	k1gMnSc2	jiný
fyzikálního	fyzikální	k2eAgMnSc2d1	fyzikální
<g/>
)	)	kIx)	)
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
často	často	k6eAd1	často
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
veličinu	veličina	k1gFnSc4	veličina
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkový	celkový	k2eAgInSc1d1	celkový
elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
násobkem	násobek	k1gInSc7	násobek
tzv.	tzv.	kA	tzv.
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
elektrický	elektrický	k2eAgInSc1d1	elektrický
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
kvantován	kvantovat	k5eAaImNgMnS	kvantovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgInSc2	tento
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
náboji	náboj	k1gInSc3	náboj
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
látky	látka	k1gFnSc2	látka
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nás	my	k3xPp1nPc4	my
opravňuje	opravňovat	k5eAaImIp3nS	opravňovat
přiřazovat	přiřazovat	k5eAaImF	přiřazovat
celkovému	celkový	k2eAgInSc3d1	celkový
náboji	náboj	k1gInSc3	náboj
makroskopického	makroskopický	k2eAgNnSc2d1	makroskopické
tělesa	těleso	k1gNnSc2	těleso
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
násobky	násobek	k1gInPc4	násobek
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Nositeli	nositel	k1gMnSc3	nositel
elementárního	elementární	k2eAgInSc2d1	elementární
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
běžných	běžný	k2eAgFnPc2d1	běžná
látek	látka	k1gFnPc2	látka
protony	proton	k1gInPc1	proton
(	(	kIx(	(
<g/>
kladný	kladný	k2eAgInSc1d1	kladný
náboj	náboj	k1gInSc1	náboj
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektrony	elektron	k1gInPc1	elektron
(	(	kIx(	(
<g/>
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náboje	náboj	k1gInPc1	náboj
obou	dva	k4xCgFnPc2	dva
částic	částice	k1gFnPc2	částice
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
atom	atom	k1gInSc4	atom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
elektronů	elektron	k1gInPc2	elektron
jako	jako	k8xS	jako
protonů	proton	k1gInPc2	proton
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
náboj	náboj	k1gInSc1	náboj
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
=	=	kIx~	=
1,602	[number]	k4	1,602
177	[number]	k4	177
⋅	⋅	k?	⋅
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
−	−	k?	−
19	[number]	k4	19
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
602177	[number]	k4	602177
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Nositelem	nositel	k1gMnSc7	nositel
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgFnPc1d1	další
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náboj	náboj	k1gInSc1	náboj
hadronů	hadron	k1gInPc2	hadron
<g/>
,	,	kIx,	,
leptonů	lepton	k1gInPc2	lepton
i	i	k8xC	i
intermediálních	intermediální	k2eAgFnPc2d1	intermediální
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
roven	roven	k2eAgInSc1d1	roven
celému	celý	k2eAgInSc3d1	celý
násobku	násobek	k1gInSc3	násobek
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kvarků	kvark	k1gInPc2	kvark
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
mínus	mínus	k1gInSc1	mínus
jedné	jeden	k4xCgFnSc3	jeden
třetině	třetina	k1gFnSc3	třetina
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgFnPc7	dva
třetinám	třetina	k1gFnPc3	třetina
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Elektricky	elektricky	k6eAd1	elektricky
nabitý	nabitý	k2eAgInSc1d1	nabitý
atom	atom	k1gInSc1	atom
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ion	ion	k1gInSc1	ion
(	(	kIx(	(
<g/>
kation	kation	k1gInSc1	kation
jestliže	jestliže	k8xS	jestliže
atom	atom	k1gInSc1	atom
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
elektrony	elektron	k1gInPc4	elektron
a	a	k8xC	a
anion	anion	k1gInSc4	anion
jestliže	jestliže	k8xS	jestliže
atom	atom	k1gInSc4	atom
přijme	přijmout	k5eAaPmIp3nS	přijmout
elektrony	elektron	k1gInPc4	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Experimentálně	experimentálně	k6eAd1	experimentálně
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
prověřeno	prověřen	k2eAgNnSc1d1	prověřeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
se	se	k3xPyFc4	se
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
pohybu	pohyb	k1gInSc6	pohyb
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
např.	např.	kA	např.
od	od	k7c2	od
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
rychlostí	rychlost	k1gFnSc7	rychlost
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
invariantní	invariantní	k2eAgInSc1d1	invariantní
při	při	k7c6	při
transformacích	transformace	k1gFnPc6	transformace
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Doc.	doc.	kA	doc.
Ing.	ing.	kA	ing.
Ivan	Ivan	k1gMnSc1	Ivan
Štoll	Štoll	k1gMnSc1	Štoll
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
:	:	kIx,	:
Elektřina	elektřina	k1gFnSc1	elektřina
a	a	k8xC	a
magnetismus	magnetismus	k1gInSc1	magnetismus
<g/>
,	,	kIx,	,
Vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
ČVUT	ČVUT	kA	ČVUT
Elektřina	elektřina	k1gFnSc1	elektřina
a	a	k8xC	a
magnetismus	magnetismus	k1gInSc1	magnetismus
Elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
Coulombův	Coulombův	k2eAgInSc1d1	Coulombův
zákon	zákon	k1gInSc1	zákon
Elektrování	elektrování	k1gNnSc2	elektrování
tělesa	těleso	k1gNnSc2	těleso
</s>
