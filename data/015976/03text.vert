<s>
Svoradov	Svoradov	k1gInSc1
</s>
<s>
Svoradov	Svoradov	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1926	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Svoradova	Svoradův	k2eAgFnSc1d1
ulica	ulica	k1gFnSc1
č.	č.	k?
<g/>
13	#num#	k4
<g/>
,	,	kIx,
Staré	Staré	k2eAgNnSc1d1
Mesto	Mesto	k1gNnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
44,04	44,04	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
1,72	1,72	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Svoradov	Svoradov	k1gInSc1
je	být	k5eAaImIp3nS
vysokoškolský	vysokoškolský	k2eAgInSc4d1
internát	internát	k1gInSc4
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
,	,	kIx,
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
,	,	kIx,
Svoradova	Svoradův	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
č.	č.	k?
<g/>
13	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
nejdůležitější	důležitý	k2eAgFnSc4d3
místo	místo	k1gNnSc4
formování	formování	k1gNnSc2
slovenské	slovenský	k2eAgFnSc2d1
katolické	katolický	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Začátky	začátek	k1gInPc1
</s>
<s>
Po	po	k7c6
založení	založení	k1gNnSc6
Univerzity	univerzita	k1gFnSc2
Komenského	Komenský	k1gMnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
nastal	nastat	k5eAaPmAgInS
velký	velký	k2eAgInSc1d1
problém	problém	k1gInSc1
s	s	k7c7
ubytováním	ubytování	k1gNnSc7
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
neexistoval	existovat	k5eNaImAgMnS
žádný	žádný	k3yNgInSc4
vysokoškolský	vysokoškolský	k2eAgInSc4d1
internát	internát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
část	část	k1gFnSc1
katolických	katolický	k2eAgMnPc2d1
studentů	student	k1gMnPc2
obrátila	obrátit	k5eAaPmAgFnS
na	na	k7c4
kněze	kněz	k1gMnPc4
Eugena	Eugeno	k1gNnSc2
Filkorna	Filkorno	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
zkušenosti	zkušenost	k1gFnPc4
ze	z	k7c2
zakládání	zakládání	k1gNnSc2
středoškolských	středoškolský	k2eAgFnPc2d1
kolejí	kolej	k1gFnPc2
během	během	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
se	se	k3xPyFc4
Eugen	Eugen	k2eAgInSc1d1
Filkorn	Filkorn	k1gInSc1
dohodl	dohodnout	k5eAaPmAgInS
se	s	k7c7
správou	správa	k1gFnSc7
sirotčince	sirotčinec	k1gInSc2
svaté	svatý	k2eAgFnSc2d1
Alžběty	Alžběta	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
na	na	k7c6
místě	místo	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
Svoradova	Svoradův	k2eAgInSc2d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tam	tam	k6eAd1
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
ubytovávat	ubytovávat	k5eAaImF
studenti	student	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostory	prostor	k1gInPc1
však	však	k9
byly	být	k5eAaImAgInP
velmi	velmi	k6eAd1
nevyhovující	vyhovující	k2eNgInPc1d1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
rozhodlo	rozhodnout	k5eAaPmAgNnS
o	o	k7c6
výstavbě	výstavba	k1gFnSc6
prvního	první	k4xOgInSc2
moderního	moderní	k2eAgInSc2d1
internátu	internát	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
Bratislavy	Bratislava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
se	se	k3xPyFc4
založil	založit	k5eAaPmAgInS
podpůrný	podpůrný	k2eAgInSc1d1
Spolek	spolek	k1gInSc1
Kolegia	kolegium	k1gNnSc2
svatého	svatý	k1gMnSc2
Svorada	Svorada	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
trnavského	trnavský	k2eAgMnSc2d1
biskupa	biskup	k1gMnSc2
Pavla	Pavel	k1gMnSc2
Jantauscha	Jantausch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakládající	zakládající	k2eAgFnSc1d1
a	a	k8xC
doživotní	doživotní	k2eAgMnSc1d1
členové	člen	k1gMnPc1
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
2000	#num#	k4
resp.	resp.	kA
500	#num#	k4
Kčs	Kčs	kA
<g/>
,	,	kIx,
řádní	řádný	k2eAgMnPc1d1
platili	platit	k5eAaImAgMnP
ročně	ročně	k6eAd1
20	#num#	k4
Kčs	Kčs	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
Spolku	spolek	k1gInSc2
bylo	být	k5eAaImAgNnS
zakládat	zakládat	k5eAaImF
<g/>
,	,	kIx,
podporovat	podporovat	k5eAaImF
a	a	k8xC
vydržovat	vydržovat	k5eAaImF
internáty	internát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filkorn	Filkorn	k1gInSc1
zvolil	zvolit	k5eAaPmAgInS
pro	pro	k7c4
vznikající	vznikající	k2eAgInSc4d1
internát	internát	k1gInSc4
název	název	k1gInSc4
Svoradov	Svoradovo	k1gNnPc2
podle	podle	k7c2
sv.	sv.	kA
Svorada	Svorada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolek	spolek	k1gInSc1
kolegia	kolegium	k1gNnSc2
sv.	sv.	kA
Svorada	Svorada	k1gFnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
garantem	garant	k1gMnSc7
výstavby	výstavba	k1gFnSc2
a	a	k8xC
správy	správa	k1gFnSc2
internátu	internát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
členy	člen	k1gMnPc7
kromě	kromě	k7c2
několika	několik	k4yIc7
slovenské	slovenský	k2eAgFnSc3d1
biskupů	biskup	k1gMnPc2
byli	být	k5eAaImAgMnP
i	i	k9
kněží	kněz	k1gMnPc1
-	-	kIx~
politici	politik	k1gMnPc1
<g/>
:	:	kIx,
papežský	papežský	k2eAgMnSc1d1
prelát	prelát	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
<g/>
,	,	kIx,
ThDr.	ThDr.	k1gMnSc1
Jozef	Jozef	k1gMnSc1
Tiso	Tisa	k1gFnSc5
<g/>
,	,	kIx,
Ferdiš	Ferdiš	k1gMnSc1
Juriga	Juriga	k1gFnSc1
nebo	nebo	k8xC
Tomáš	Tomáš	k1gMnSc1
Růžička	Růžička	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
stála	stát	k5eAaImAgFnS
1	#num#	k4
100	#num#	k4
000	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gNnPc2
se	se	k3xPyFc4
v	v	k7c6
Americe	Amerika	k1gFnSc6
Filkornovou	Filkornový	k2eAgFnSc7d1
zásluhou	zásluha	k1gFnSc7
podařilo	podařit	k5eAaPmAgNnS
vysbírat	vysbírat	k5eAaPmF
200	#num#	k4
000	#num#	k4
Kčs	Kčs	kA
<g/>
,	,	kIx,
200	#num#	k4
000	#num#	k4
Kčs	Kčs	kA
dala	dát	k5eAaPmAgFnS
bratislavská	bratislavský	k2eAgFnSc1d1
župa	župa	k1gFnSc1
<g/>
,	,	kIx,
200	#num#	k4
000	#num#	k4
slovenští	slovenský	k2eAgMnPc1d1
biskupové	biskup	k1gMnPc1
(	(	kIx(
<g/>
biskup	biskup	k1gMnSc1
Ján	Ján	k1gMnSc1
Vojtaššák	Vojtaššák	k1gMnSc1
přispěl	přispět	k5eAaPmAgMnS
50	#num#	k4
000	#num#	k4
korunami	koruna	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
sbírky	sbírka	k1gFnPc1
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
vynesly	vynést	k5eAaPmAgInP
nezanedbatelnou	zanedbatelný	k2eNgFnSc4d1
částku	částka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc4
peněz	peníze	k1gInPc2
poskytli	poskytnout	k5eAaPmAgMnP
jako	jako	k8xS,k8xC
úvěr	úvěr	k1gInSc4
banky	banka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
postavení	postavení	k1gNnSc4
"	"	kIx"
<g/>
starého	starý	k2eAgNnSc2d1
Svoradova	Svoradův	k2eAgNnSc2d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
první	první	k4xOgMnPc1
studenti	student	k1gMnPc1
nastěhovali	nastěhovat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1926	#num#	k4
posvětil	posvětit	k5eAaPmAgMnS
rožňavský	rožňavský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Michal	Michal	k1gMnSc1
Bubnič	Bubnič	k1gMnSc1
základní	základní	k2eAgInSc1d1
kámen	kámen	k1gInSc1
"	"	kIx"
<g/>
nového	nový	k2eAgNnSc2d1
Svoradova	Svoradův	k2eAgNnSc2d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
slavnosti	slavnost	k1gFnSc6
řečnil	řečnit	k5eAaImAgMnS
Ferdiš	Ferdiš	k1gMnSc1
Juriga	Jurig	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
si	se	k3xPyFc3
Spolek	spolek	k1gInSc1
Kolegia	kolegium	k1gNnSc2
sv.	sv.	kA
Svorada	Svorada	k1gFnSc1
podal	podat	k5eAaPmAgMnS
žádost	žádost	k1gFnSc4
o	o	k7c4
zřízení	zřízení	k1gNnSc4
telefonu	telefon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
vyznívá	vyznívat	k5eAaImIp3nS
zajímavě	zajímavě	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
policie	policie	k1gFnSc1
musela	muset	k5eAaImAgFnS
dát	dát	k5eAaPmF
vyjádření	vyjádření	k1gNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
k	k	k7c3
tomu	ten	k3xDgNnSc3
nemá	mít	k5eNaImIp3nS
"	"	kIx"
<g/>
politických	politický	k2eAgFnPc2d1
námitek	námitka	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1928	#num#	k4
biskup	biskup	k1gMnSc1
Pavol	Pavola	k1gFnPc2
Jantausch	Jantausch	k1gMnSc1
posvětil	posvětit	k5eAaPmAgMnS
křídlo	křídlo	k1gNnSc4
"	"	kIx"
<g/>
nového	nový	k2eAgNnSc2d1
<g/>
"	"	kIx"
Svoradova	Svoradův	k2eAgNnSc2d1
za	za	k7c2
účasti	účast	k1gFnSc2
ministra	ministr	k1gMnSc2
školství	školství	k1gNnSc2
Milana	Milan	k1gMnSc2
Hodži	Hodža	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policii	policie	k1gFnSc3
neušlo	ujít	k5eNaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
místo	místo	k7c2
státní	státní	k2eAgFnSc2d1
hymny	hymna	k1gFnSc2
se	se	k3xPyFc4
zpívala	zpívat	k5eAaImAgFnS
píseň	píseň	k1gFnSc4
Hej	hej	k6eAd1
Slováci	Slovák	k1gMnPc1
a	a	k8xC
vlály	vlát	k5eAaImAgFnP
bílo	bílo	k6eAd1
-	-	kIx~
modro	modro	k6eAd1
-	-	kIx~
červené	červený	k2eAgNnSc1d1
a	a	k8xC
bílo	bílo	k6eAd1
-	-	kIx~
žluté	žlutý	k2eAgFnPc4d1
zástavy	zástava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
dalších	další	k2eAgInPc2d1
traktů	trakt	k1gInPc2
Svoradova	Svoradův	k2eAgNnSc2d1
trvala	trvalo	k1gNnSc2
postupně	postupně	k6eAd1
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
na	na	k7c6
Svoradově	Svoradův	k2eAgFnSc6d1
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1
byl	být	k5eAaImAgInS
vnitřní	vnitřní	k2eAgInSc1d1
řád	řád	k1gInSc1
internátu	internát	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
vyznačoval	vyznačovat	k5eAaImAgMnS
téměř	téměř	k6eAd1
sparťanským	sparťanský	k2eAgMnSc7d1
duchem	duch	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budíček	budíček	k1gInSc1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
byl	být	k5eAaImAgMnS
v	v	k7c4
7.00	7.00	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
bohoslužba	bohoslužba	k1gFnSc1
<g/>
,	,	kIx,
snídaně	snídaně	k1gFnSc1
<g/>
,	,	kIx,
přednášky	přednáška	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c4
12.00	12.00	k4
oběd	oběd	k1gInSc4
<g/>
,	,	kIx,
opět	opět	k6eAd1
přednášky	přednáška	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c4
18.00	18.00	k4
večeře	večeře	k1gFnSc2
<g/>
,	,	kIx,
mše	mše	k1gFnSc2
a	a	k8xC
do	do	k7c2
23.00	23.00	k4
studium	studium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanovy	stanova	k1gFnPc1
byly	být	k5eAaImAgFnP
napsány	napsat	k5eAaBmNgFnP,k5eAaPmNgFnP
v	v	k7c6
křesťanském	křesťanský	k2eAgMnSc6d1
a	a	k8xC
národním	národní	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Svoradov	Svoradov	k1gInSc1
byl	být	k5eAaImAgInS
čistě	čistě	k6eAd1
chlapecký	chlapecký	k2eAgInSc1d1
internát	internát	k1gInSc1
(	(	kIx(
<g/>
na	na	k7c6
začátku	začátek	k1gInSc6
existence	existence	k1gFnSc2
zde	zde	k6eAd1
bydleli	bydlet	k5eAaImAgMnP
hlavně	hlavně	k9
právníci	právník	k1gMnPc1
a	a	k8xC
medici	medik	k1gMnPc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
přírodovědci	přírodovědec	k1gMnPc1
a	a	k8xC
technici	technik	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přísně	přísně	k6eAd1
se	se	k3xPyFc4
dbalo	dbát	k5eAaImAgNnS
na	na	k7c4
morálku	morálka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
bylo	být	k5eAaImAgNnS
zakázáno	zakázat	k5eAaPmNgNnS
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
prostoru	prostor	k1gInSc6
politizovat	politizovat	k5eAaImF
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
kouření	kouření	k1gNnSc1
na	na	k7c6
pokojích	pokoj	k1gInPc6
bylo	být	k5eAaImAgNnS
povoleno	povolen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovala	existovat	k5eAaImAgFnS
zde	zde	k6eAd1
volejbalová	volejbalový	k2eAgFnSc1d1
a	a	k8xC
fotbalová	fotbalový	k2eAgNnPc1d1
mužstva	mužstvo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mše	mše	k1gFnSc1
se	se	k3xPyFc4
sloužily	sloužit	k5eAaImAgInP
v	v	k7c6
kapli	kaple	k1gFnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
s	s	k7c7
impozantní	impozantní	k2eAgFnSc7d1
mozaikou	mozaika	k1gFnSc7
od	od	k7c2
Šimkoviče	Šimkovič	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1932	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
byla	být	k5eAaImAgFnS
rozebrána	rozebrán	k2eAgFnSc1d1
a	a	k8xC
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
kostele	kostel	k1gInSc6
na	na	k7c4
Kalvárii	Kalvárie	k1gFnSc4
v	v	k7c6
Horském	horský	k2eAgInSc6d1
parku	park	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
kaple	kaple	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
a	a	k8xC
nahradily	nahradit	k5eAaPmAgFnP
ji	on	k3xPp3gFnSc4
pokoje	pokoj	k1gInPc1
-	-	kIx~
tento	tento	k3xDgInSc1
stav	stav	k1gInSc1
trvá	trvat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Svoradově	Svoradův	k2eAgNnSc6d1
byla	být	k5eAaImAgFnS
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
knihy	kniha	k1gFnPc1
do	do	k7c2
ní	on	k3xPp3gFnSc2
věnovalo	věnovat	k5eAaPmAgNnS,k5eAaImAgNnS
vydavatelství	vydavatelství	k1gNnPc2
Lev	lev	k1gInSc1
z	z	k7c2
Ružomberka	Ružomberk	k1gInSc2
či	či	k8xC
nakladatelství	nakladatelství	k1gNnSc2
L.	L.	kA
Mazáč	Mazáč	k1gInSc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fungovala	fungovat	k5eAaImAgFnS
čítárna	čítárna	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
denní	denní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
a	a	k8xC
časopisy	časopis	k1gInPc1
<g/>
,	,	kIx,
také	také	k9
hudební	hudební	k2eAgInSc4d1
kroužek	kroužek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
vlastní	vlastní	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
mohli	moct	k5eAaImAgMnP
využívat	využívat	k5eAaPmF,k5eAaImF
tělocvičnu	tělocvična	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelem	ředitel	k1gMnSc7
internátu	internát	k1gInSc2
byl	být	k5eAaImAgMnS
Eugen	Eugen	k2eAgMnSc1d1
Filkorn	Filkorn	k1gMnSc1
<g/>
,	,	kIx,
postava	postava	k1gFnSc1
požívající	požívající	k2eAgFnSc4d1
všeobecnou	všeobecný	k2eAgFnSc4d1
úctu	úcta	k1gFnSc4
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
vztah	vztah	k1gInSc4
ke	k	k7c3
studentům	student	k1gMnPc3
a	a	k8xC
pro	pro	k7c4
způsob	způsob	k1gInSc4
udržování	udržování	k1gNnSc4
ducha	duch	k1gMnSc2
Svoradova	Svoradův	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomickým	ekonomický	k2eAgMnSc7d1
správcem	správce	k1gMnSc7
byl	být	k5eAaImAgMnS
Tomáš	Tomáš	k1gMnSc1
Beneš	Beneš	k1gMnSc1
a	a	k8xC
za	za	k7c4
duchovní	duchovní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
odpovídal	odpovídat	k5eAaImAgMnS
lazarista	lazarista	k1gMnSc1
Ján	Ján	k1gMnSc1
Hutyra	Hutyra	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestřičky	sestřička	k1gFnPc1
vincentky	vincentka	k1gFnSc2
měly	mít	k5eAaImAgFnP
na	na	k7c4
starost	starost	k1gFnSc4
stravu	strava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bydlet	bydlet	k5eAaImF
na	na	k7c6
Svoradově	Svoradův	k2eAgNnSc6d1
byla	být	k5eAaImAgFnS
pocta	pocta	k1gFnSc1
(	(	kIx(
<g/>
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
ubytovat	ubytovat	k5eAaPmF
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
mít	mít	k5eAaImF
doporučení	doporučení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
něj	on	k3xPp3gMnSc4
hlásilo	hlásit	k5eAaImAgNnS
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
míst	místo	k1gNnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
vznikl	vzniknout	k5eAaPmAgInS
druhý	druhý	k4xOgInSc1
<g/>
,	,	kIx,
státní	státní	k2eAgInSc1d1
internát	internát	k1gInSc1
Lafranconi	Lafrancon	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Svoradovem	Svoradovo	k1gNnSc7
a	a	k8xC
Lafranconi	Lafrancoň	k1gFnSc3
byla	být	k5eAaImAgFnS
zdravá	zdravá	k1gFnSc1
rivalita	rivalita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staly	stát	k5eAaPmAgInP
se	s	k7c7
středisky	středisko	k1gNnPc7
studentského	studentský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
Svoradov	Svoradov	k1gInSc1
křesťansko	křesťansko	k6eAd1
-	-	kIx~
nacionálního	nacionální	k2eAgMnSc2d1
<g/>
,	,	kIx,
ovlivňovaného	ovlivňovaný	k2eAgNnSc2d1
zejména	zejména	k9
osobností	osobnost	k1gFnSc7
a	a	k8xC
politikou	politika	k1gFnSc7
Andreje	Andrej	k1gMnSc2
Hlinky	Hlinka	k1gMnSc2
a	a	k8xC
Lafranconi	Lafrancon	k1gMnPc1
střediskem	středisko	k1gNnSc7
diferencovaných	diferencovaný	k2eAgInPc2d1
studentských	studentský	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
agrárně	agrárně	k6eAd1
-	-	kIx~
liberálních	liberální	k2eAgFnPc2d1
až	až	k9
po	po	k7c4
socialisticko	socialisticko	k1gNnSc4
-	-	kIx~
komunistické	komunistický	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
odpovídalo	odpovídat	k5eAaImAgNnS
i	i	k8xC
demokratickému	demokratický	k2eAgInSc3d1
pluralismu	pluralismus	k1gInSc3
tehdejšího	tehdejší	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoradovíbyl	Svoradovíbyl	k1gInSc4
soběstačným	soběstačný	k2eAgInSc7d1
po	po	k7c6
stránce	stránka	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc6d1
<g/>
,	,	kIx,
protože	protože	k8xS
k	k	k7c3
němu	on	k3xPp3gMnSc3
patřil	patřit	k5eAaImAgInS
mlýn	mlýn	k1gInSc1
v	v	k7c6
Podunajských	podunajský	k2eAgFnPc6d1
Biskupicích	Biskupice	k1gFnPc6
a	a	k8xC
90	#num#	k4
ha	ha	kA
velký	velký	k2eAgInSc4d1
statek	statek	k1gInSc4
v	v	k7c4
Torcs	Torcs	k1gInSc4
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Dunajská	dunajský	k2eAgFnSc1d1
Lužná	Lužná	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pěstovala	pěstovat	k5eAaImAgFnS
zelenina	zelenina	k1gFnSc1
a	a	k8xC
chovala	chovat	k5eAaImAgNnP
domácí	domácí	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
ubytování	ubytování	k1gNnSc4
a	a	k8xC
stravu	strava	k1gFnSc4
studenti	student	k1gMnPc1
platili	platit	k5eAaImAgMnP
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
za	za	k7c4
každou	každý	k3xTgFnSc4
zkoušku	zkouška	k1gFnSc4
(	(	kIx(
<g/>
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
během	během	k7c2
trvání	trvání	k1gNnSc2
Slovenského	slovenský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejchudší	chudý	k2eAgInSc1d3
byli	být	k5eAaImAgMnP
dotováni	dotován	k2eAgMnPc1d1
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
neplatili	platit	k5eNaImAgMnP
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
bydleli	bydlet	k5eAaImAgMnP
v	v	k7c6
takzvaných	takzvaný	k2eAgFnPc6d1
fundacionálnych	fundacionálny	k1gFnPc6
pokojích	pokoj	k1gInPc6
(	(	kIx(
<g/>
platili	platit	k5eAaImAgMnP
jejich	jejich	k3xOp3gFnPc4
jednotlivé	jednotlivý	k2eAgFnPc4d1
fary	fara	k1gFnPc4
a	a	k8xC
sponzoři	sponzor	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
bylo	být	k5eAaImAgNnS
72	#num#	k4
takových	takový	k3xDgNnPc2
míst	místo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
stipendijních	stipendijní	k2eAgInPc6d1
státních	státní	k2eAgInPc6d1
pokojích	pokoj	k1gInPc6
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
27	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostředky	prostředek	k1gInPc1
pro	pro	k7c4
studenty	student	k1gMnPc4
se	se	k3xPyFc4
získávaly	získávat	k5eAaImAgFnP
i	i	k8xC
prodejem	prodej	k1gInSc7
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
koleji	kolej	k1gFnSc6
bylo	být	k5eAaImAgNnS
centrum	centrum	k1gNnSc1
Ústředí	ústředí	k1gNnSc2
slovenského	slovenský	k2eAgNnSc2d1
katolického	katolický	k2eAgNnSc2d1
studentstva	studentstvo	k1gNnSc2
(	(	kIx(
<g/>
ÚSKŠ	ÚSKŠ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zahrnovalo	zahrnovat	k5eAaImAgNnS
asi	asi	k9
30	#num#	k4
vysokoškolských	vysokoškolský	k2eAgMnPc2d1
a	a	k8xC
středoškolských	středoškolský	k2eAgMnPc2d1
spolků	spolek	k1gInPc2
ze	z	k7c2
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějším	známý	k2eAgNnSc7d3
byl	být	k5eAaImAgInS
spolek	spolek	k1gInSc1
Moyzes	Moyzesa	k1gFnPc2
<g/>
,	,	kIx,
dalšími	další	k1gNnPc7
Tatran	Tatran	k1gInSc1
nebo	nebo	k8xC
Považan	Považan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydával	vydávat	k5eAaPmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
vlastní	vlastnit	k5eAaImIp3nS
časopis	časopis	k1gInSc1
Svoradov	Svoradovo	k1gNnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
např.	např.	kA
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
vycházela	vycházet	k5eAaImAgFnS
studie	studie	k1gFnSc1
o	o	k7c6
nacismu	nacismus	k1gInSc6
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
novopohanství	novopohanství	k1gNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
periodikem	periodikum	k1gNnSc7
byl	být	k5eAaImAgInS
Rozvoj	rozvoj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
středoškolákům	středoškolák	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
se	se	k3xPyFc4
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
i	i	k9
charitativní	charitativní	k2eAgFnSc4d1
činnosti	činnost	k1gFnPc4
pro	pro	k7c4
chudé	chudý	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
Bratislavy	Bratislava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovala	existovat	k5eAaImAgFnS
zde	zde	k6eAd1
tzv.	tzv.	kA
malá	malý	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
měli	mít	k5eAaImAgMnP
přednášky	přednáška	k1gFnPc4
mnozí	mnohý	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
činitelé	činitel	k1gMnPc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
i	i	k9
mnoho	mnoho	k4c1
českých	český	k2eAgMnPc2d1
profesorů	profesor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
katolíků	katolík	k1gMnPc2
měli	mít	k5eAaImAgMnP
na	na	k7c6
Svoradově	Svoradův	k2eAgFnSc6d1
své	svůj	k3xOyFgFnSc6
pokoje	pokoj	k1gInPc4
i	i	k9
evangelíci	evangelík	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
byl	být	k5eAaImAgInS
Svoradov	Svoradov	k1gInSc1
v	v	k7c6
nemilosti	nemilost	k1gFnSc6
ústřední	ústřední	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
něm	on	k3xPp3gNnSc6
viděla	vidět	k5eAaImAgFnS
své	svůj	k3xOyFgNnSc4
ohrožení	ohrožení	k1gNnSc4
a	a	k8xC
považovala	považovat	k5eAaImAgFnS
ho	on	k3xPp3gMnSc4
za	za	k7c2
"	"	kIx"
<g/>
pařeniště	pařeniště	k1gNnSc2
<g/>
"	"	kIx"
luďáctví	luďáctví	k1gNnSc1
a	a	k8xC
často	často	k6eAd1
ho	on	k3xPp3gNnSc4
prověřovala	prověřovat	k5eAaImAgFnS
policií	policie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
Kolegium	kolegium	k1gNnSc4
sv.	sv.	kA
Svorada	Svorada	k1gFnSc1
požádalo	požádat	k5eAaPmAgNnS
o	o	k7c4
prostředky	prostředek	k1gInPc4
z	z	k7c2
podpůrné	podpůrný	k2eAgFnSc2d1
Masarykovy	Masarykův	k2eAgFnSc2d1
nadace	nadace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
mu	on	k3xPp3gMnSc3
zamítnuty	zamítnout	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policejní	policejní	k2eAgFnSc1d1
ředitelství	ředitelství	k1gNnSc2
to	ten	k3xDgNnSc1
odůvodnilo	odůvodnit	k5eAaPmAgNnS
účastí	účast	k1gFnSc7
studentů	student	k1gMnPc2
na	na	k7c6
autonomistickým	autonomistický	k2eAgFnPc3d1
manifestacích	manifestace	k1gFnPc6
či	či	k8xC
na	na	k7c6
demonstraci	demonstrace	k1gFnSc6
studentů	student	k1gMnPc2
proti	proti	k7c3
pojmenování	pojmenování	k1gNnSc3
petržalského	petržalský	k2eAgNnSc2d1
nábřeží	nábřeží	k1gNnSc2
po	po	k7c6
Tyršovi	Tyrš	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Těsně	těsně	k6eAd1
před	před	k7c7
vznikem	vznik	k1gInSc7
Slovenského	slovenský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
během	během	k7c2
tzv.	tzv.	kA
Homolova	Homolův	k2eAgInSc2d1
puče	puč	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
byl	být	k5eAaImAgInS
internát	internát	k1gInSc1
obklíčen	obklíčen	k2eAgInSc1d1
tanky	tank	k1gInPc7
československé	československý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
studenti	student	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
zabarikádovali	zabarikádovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
Vyšší	vysoký	k2eAgFnSc1d2
vůdčí	vůdčí	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Hlinkovy	Hlinkův	k2eAgFnSc2d1
mládeže	mládež	k1gFnSc2
a	a	k8xC
jistý	jistý	k2eAgInSc4d1
čas	čas	k1gInSc4
i	i	k8xC
Akademická	akademický	k2eAgFnSc1d1
Hlinkova	Hlinkův	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
půdě	půda	k1gFnSc6
internátu	internát	k1gInSc2
během	během	k7c2
války	válka	k1gFnSc2
několik	několik	k4yIc4
let	léto	k1gNnPc2
působila	působit	k5eAaImAgFnS
zajímavá	zajímavý	k2eAgFnSc1d1
postava	postava	k1gFnSc1
-	-	kIx~
jezuita	jezuita	k1gMnSc1
Tomislav	Tomislav	k1gMnSc1
Kolakovič	Kolakovič	k1gMnSc1
z	z	k7c2
Chorvatska	Chorvatsko	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
připravoval	připravovat	k5eAaImAgInS
studenty	student	k1gMnPc4
na	na	k7c4
život	život	k1gInSc4
církve	církev	k1gFnSc2
v	v	k7c6
totalitě	totalita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
společenství	společenství	k1gNnSc4
Rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
uspořádalo	uspořádat	k5eAaPmAgNnS
mnoho	mnoho	k6eAd1
svých	svůj	k3xOyFgFnPc2
buněk	buňka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
Slovenského	slovenský	k2eAgInSc2d1
státu	stát	k1gInSc2
žilo	žít	k5eAaImAgNnS
na	na	k7c6
koleji	kolej	k1gFnSc6
cca	cca	kA
500	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
se	s	k7c7
stavbou	stavba	k1gFnSc7
dalšího	další	k2eAgInSc2d1
vysokoškolského	vysokoškolský	k2eAgInSc2d1
katolického	katolický	k2eAgInSc2d1
internátu	internát	k1gInSc2
na	na	k7c6
Wilsonově	Wilsonův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Roky	rok	k1gInPc1
1945-1989	1945-1989	k4
</s>
<s>
Rok	rok	k1gInSc1
1945	#num#	k4
pro	pro	k7c4
Svoradov	Svoradov	k1gInSc4
znamenal	znamenat	k5eAaImAgInS
zlom	zlom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastala	nastat	k5eAaPmAgFnS
atmosféra	atmosféra	k1gFnSc1
napětí	napětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bombardování	bombardování	k1gNnSc6
Bratislavy	Bratislava	k1gFnSc2
se	se	k3xPyFc4
sem	sem	k6eAd1
uchýlil	uchýlit	k5eAaPmAgInS
Slovenský	slovenský	k2eAgInSc1d1
červený	červený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
červenci	červenec	k1gInSc6
1945	#num#	k4
byly	být	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
školy	škola	k1gFnPc1
a	a	k8xC
internáty	internát	k1gInPc1
postátněné	postátněný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
pro	pro	k7c4
Spolek	spolek	k1gInSc4
Kolegia	kolegium	k1gNnSc2
sv.	sv.	kA
Svorada	Svorada	k1gFnSc1
nastaly	nastat	k5eAaPmAgInP
krušné	krušný	k2eAgInPc4d1
časy	čas	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
čela	čelo	k1gNnSc2
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Ambrož	Ambrož	k1gMnSc1
Lazík	Lazík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
Kolegiu	kolegium	k1gNnSc6
nebyly	být	k5eNaImAgInP
ministerstvem	ministerstvo	k1gNnSc7
vnitra	vnitro	k1gNnSc2
schváleny	schválen	k2eAgFnPc4d1
nové	nový	k2eAgFnPc4d1
stanovy	stanova	k1gFnPc4
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1946	#num#	k4
byla	být	k5eAaImAgFnS
demonstrace	demonstrace	k1gFnSc1
studentů	student	k1gMnPc2
před	před	k7c7
Svoradovem	Svoradov	k1gInSc7
napadena	napaden	k2eAgFnSc1d1
střelbou	střelba	k1gFnSc7
ozbrojenců	ozbrojenec	k1gMnPc2
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Slovenska	Slovensko	k1gNnSc2
<g/>
,	,	kIx,
minimálně	minimálně	k6eAd1
pět	pět	k4xCc1
osob	osoba	k1gFnPc2
bylo	být	k5eAaImAgNnS
zraněno	zranit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
volbami	volba	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
půdě	půda	k1gFnSc6
Svoradova	Svoradův	k2eAgFnSc1d1
živě	živě	k6eAd1
diskutovalo	diskutovat	k5eAaImAgNnS
o	o	k7c6
založení	založení	k1gNnSc6
katolické	katolický	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
pak	pak	k6eAd1
podpořili	podpořit	k5eAaPmAgMnP
Demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
volebním	volební	k2eAgInSc7d1
manažerem	manažer	k1gInSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
svoradovčan	svoradovčan	k1gMnSc1
Imrich	Imrich	k1gMnSc1
Kružliak	Kružliak	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
strana	strana	k1gFnSc1
drtivě	drtivě	k6eAd1
porazila	porazit	k5eAaPmAgFnS
Komunistickou	komunistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Slovenska	Slovensko	k1gNnSc2
(	(	kIx(
<g/>
více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
<g/>
%	%	kIx~
hlasů	hlas	k1gInPc2
oproti	oproti	k7c3
30	#num#	k4
<g/>
%	%	kIx~
KSS	KSS	kA
<g/>
)	)	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
se	se	k3xPyFc4
obnovilo	obnovit	k5eAaPmAgNnS
zatýkání	zatýkání	k1gNnSc1
studentů	student	k1gMnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
StB	StB	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
pokračovalo	pokračovat	k5eAaImAgNnS
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
komunistickém	komunistický	k2eAgInSc6d1
převratu	převrat	k1gInSc6
v	v	k7c6
únoru	únor	k1gInSc6
1948	#num#	k4
Spolku	spolek	k1gInSc2
zabavili	zabavit	k5eAaPmAgMnP
veškerý	veškerý	k3xTgInSc4
majetek	majetek	k1gInSc4
a	a	k8xC
byl	být	k5eAaImAgInS
vymazán	vymazat	k5eAaPmNgInS
z	z	k7c2
registru	registr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
kvůli	kvůli	k7c3
zlikvidování	zlikvidování	k1gNnSc3
svoradovské	svoradovský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
internát	internát	k1gInSc1
přejmenoval	přejmenovat	k5eAaPmAgInS
na	na	k7c4
Studentský	studentský	k2eAgInSc4d1
domov	domov	k1gInSc4
Mirka	Mirek	k1gMnSc2
Nešpora	Nešpor	k1gMnSc2
-	-	kIx~
partyzána	partyzán	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
neměl	mít	k5eNaImAgMnS
se	s	k7c7
Svoradovem	Svoradov	k1gInSc7
nic	nic	k3yNnSc1
společného	společný	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
název	název	k1gInSc1
platil	platit	k5eAaImAgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
navrácen	navrácen	k2eAgInSc4d1
původní	původní	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
koleji	kolej	k1gFnSc6
bydlely	bydlet	k5eAaImAgInP
především	především	k6eAd1
studentky	studentka	k1gFnSc2
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
organizovaly	organizovat	k5eAaBmAgInP
"	"	kIx"
<g/>
podzemní	podzemní	k2eAgInPc1d1
<g/>
"	"	kIx"
katolické	katolický	k2eAgInPc1d1
kroužky	kroužek	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
se	se	k3xPyFc4
často	často	k6eAd1
účastnil	účastnit	k5eAaImAgInS
i	i	k9
tajně	tajně	k6eAd1
vysvěcený	vysvěcený	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
Korec	Korec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
revoluci	revoluce	k1gFnSc6
1989	#num#	k4
</s>
<s>
Po	po	k7c6
listopadové	listopadový	k2eAgFnSc6d1
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
byl	být	k5eAaImAgInS
Svoradov	Svoradov	k1gInSc1
v	v	k7c6
restituci	restituce	k1gFnSc6
vrácen	vrátit	k5eAaPmNgInS
katolické	katolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ho	on	k3xPp3gMnSc4
nechala	nechat	k5eAaPmAgFnS
v	v	k7c6
bezplatném	bezplatný	k2eAgInSc6d1
pronájmu	pronájem	k1gInSc6
Slovenské	slovenský	k2eAgFnSc6d1
technické	technický	k2eAgFnSc3d1
univerzitě	univerzita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
je	být	k5eAaImIp3nS
ve	v	k7c6
vestibulu	vestibul	k1gInSc6
umístěna	umístit	k5eAaPmNgFnS
pamětní	pamětní	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
Eugenovi	Eugen	k1gMnSc3
Filkornovi	Filkorn	k1gMnSc3
<g/>
,	,	kIx,
(	(	kIx(
<g/>
odhalil	odhalit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
arcibiskup	arcibiskup	k1gMnSc1
Ján	Ján	k1gMnSc1
Sokol	Sokol	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zůstala	zůstat	k5eAaPmAgFnS
tam	tam	k6eAd1
i	i	k9
busta	busta	k1gFnSc1
Mirka	Mirka	k1gFnSc1
Nešpora	nešpora	k1gFnSc1
(	(	kIx(
<g/>
skvělá	skvělý	k2eAgFnSc1d1
práce	práce	k1gFnSc1
od	od	k7c2
Trizuljaka	Trizuljak	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
internát	internát	k1gInSc4
obývali	obývat	k5eAaImAgMnP
hlavně	hlavně	k9
studenti	student	k1gMnPc1
chemickotechnologické	chemickotechnologický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
STU	sto	k4xCgNnSc3
<g/>
,	,	kIx,
od	od	k7c2
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
převažují	převažovat	k5eAaImIp3nP
mladí	mladý	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
pravidelně	pravidelně	k6eAd1
setkávají	setkávat	k5eAaImIp3nP
bývalí	bývalý	k2eAgMnPc1d1
svoradovčané	svoradovčan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
založili	založit	k5eAaPmAgMnP
Kruh	kruh	k1gInSc4
přátel	přítel	k1gMnPc2
Svoradova	Svoradův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Svoradově	Svoradův	k2eAgNnSc6d1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
místnost	místnost	k1gFnSc4
i	i	k9
Slovenský	slovenský	k2eAgInSc1d1
katolický	katolický	k2eAgInSc1d1
akademický	akademický	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Istropolitana	Istropolitana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Známí	známý	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
Svoradova	Svoradův	k2eAgInSc2d1
</s>
<s>
Svoradov	Svoradov	k1gInSc1
dal	dát	k5eAaPmAgInS
Slovensku	Slovensko	k1gNnSc3
mnoho	mnoho	k4c1
výjimečných	výjimečný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
například	například	k6eAd1
o	o	k7c4
básníka	básník	k1gMnSc4
katolické	katolický	k2eAgFnSc2d1
moderny	moderna	k1gFnSc2
<g/>
,	,	kIx,
-	-	kIx~
lékaře	lékař	k1gMnSc4
Andreje	Andrej	k1gMnSc4
Žarnova	Žarnov	k1gInSc2
(	(	kIx(
<g/>
byl	být	k5eAaImAgInS
při	při	k7c6
exhumaci	exhumace	k1gFnSc6
pozůstatků	pozůstatek	k1gInPc2
povražděných	povražděný	k2eAgMnPc2d1
polských	polský	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
v	v	k7c6
Katyni	Katyně	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc1
verše	verš	k1gInPc1
zdobí	zdobit	k5eAaImIp3nP
pamětní	pamětní	k2eAgFnSc4d1
desku	deska	k1gFnSc4
E.	E.	kA
Filkorna	Filkorna	k1gFnSc1
ve	v	k7c6
vestibulu	vestibul	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
politiky	politika	k1gFnSc2
Karla	Karel	k1gMnSc2
Sidora	Sidor	k1gMnSc2
<g/>
,	,	kIx,
Júlia	Július	k1gMnSc2
Stana	Stan	k1gMnSc2
<g/>
,	,	kIx,
Jozefa	Jozef	k1gMnSc2
Kirschbauma	Kirschbaum	k1gMnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Antona	Anton	k1gMnSc2
Neuwirtha	Neuwirth	k1gMnSc2
<g/>
,	,	kIx,
Josefa	Josef	k1gMnSc2
Vicena	Vicen	k1gMnSc2
(	(	kIx(
<g/>
zakladatele	zakladatel	k1gMnSc2
vysílače	vysílač	k1gInSc2
Bílé	bílý	k2eAgFnSc2d1
legie	legie	k1gFnSc2
po	po	k7c6
komunistickém	komunistický	k2eAgInSc6d1
puči	puč	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herce	herec	k1gMnSc2
Gustáva	Gustáv	k1gMnSc2
Valacha	Valach	k1gMnSc2
<g/>
,	,	kIx,
historika	historik	k1gMnSc2
Františka	František	k1gMnSc2
Hrušovského	hrušovský	k2eAgMnSc2d1
<g/>
,	,	kIx,
archeologa	archeolog	k1gMnSc2
Bela	Bela	k1gFnSc1
Polla	Polla	k1gFnSc1
<g/>
,	,	kIx,
zpěváka	zpěvák	k1gMnSc2
Štěpána	Štěpán	k1gMnSc4
Hozu	Hoza	k1gMnSc4
<g/>
,	,	kIx,
právníky	právník	k1gMnPc4
Vojtěcha	Vojtěch	k1gMnSc4
Marka	Marek	k1gMnSc4
<g/>
,	,	kIx,
Júlia	Július	k1gMnSc4
Virsika	Virsik	k1gMnSc4
<g/>
,	,	kIx,
Františka	František	k1gMnSc4
Braxatorise	Braxatorise	k1gFnSc2
<g/>
,	,	kIx,
lékaře	lékař	k1gMnSc2
Františka	František	k1gMnSc4
Sýkoru	Sýkora	k1gMnSc4
<g/>
,	,	kIx,
novináře	novinář	k1gMnSc2
a	a	k8xC
publicisty	publicista	k1gMnSc2
Imricha	Imrich	k1gMnSc2
Kružliaka	Kružliak	k1gMnSc2
<g/>
,	,	kIx,
diplomata	diplomat	k1gMnSc2
Josefa	Josef	k1gMnSc2
Augusta	August	k1gMnSc2
Mikuše	Mikuše	k1gFnSc2
<g/>
,	,	kIx,
malíře	malíř	k1gMnSc2
Ladislava	Ladislav	k1gMnSc2
Záborského	záborský	k2eAgMnSc2d1
<g/>
,	,	kIx,
básníka	básník	k1gMnSc2
Severina	Severin	k1gMnSc2
Zrubce	Zrubce	k1gMnSc2
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chvíli	chvíle	k1gFnSc4
tu	tu	k6eAd1
dokonce	dokonce	k9
bydlel	bydlet	k5eAaImAgMnS
i	i	k9
Gustáv	Gustáva	k1gFnPc2
Husák	Husák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Napsali	napsat	k5eAaPmAgMnP,k5eAaBmAgMnP
o	o	k7c6
Svoradově	Svoradův	k2eAgFnSc6d1
</s>
<s>
Ze	z	k7c2
vzpomínek	vzpomínka	k1gFnPc2
rabína	rabín	k1gMnSc2
Armina	Armin	k1gMnSc2
Friedera	Frieder	k1gMnSc2
</s>
<s>
...	...	k?
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
[	[	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
v	v	k7c6
Žilině	Žilina	k1gFnSc6
<g/>
]	]	kIx)
extremista	extremista	k1gMnSc1
Karol	Karola	k1gFnPc2
Sidor	Sidor	k1gMnSc1
přešel	přejít	k5eAaPmAgMnS
k	k	k7c3
frakci	frakce	k1gFnSc3
Josefa	Josefa	k1gFnSc1
Tisa	Tisa	k1gFnSc1
<g/>
,	,	kIx,
vládly	vládnout	k5eAaImAgFnP
zde	zde	k6eAd1
mnohem	mnohem	k6eAd1
umírněnější	umírněný	k2eAgInPc1d2
poměry	poměr	k1gInPc1
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
bodě	bod	k1gInSc6
se	se	k3xPyFc4
spokojili	spokojit	k5eAaPmAgMnP
s	s	k7c7
prohlášením	prohlášení	k1gNnSc7
"	"	kIx"
<g/>
autonomie	autonomie	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
pak	pak	k6eAd1
oficiálně	oficiálně	k6eAd1
ratifikována	ratifikován	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
umírnění	umírněný	k2eAgMnPc1d1
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
,	,	kIx,
klíčové	klíčový	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
v	v	k7c6
aparátu	aparát	k1gInSc6
byly	být	k5eAaImAgInP
kontrolovány	kontrolován	k2eAgMnPc4d1
extremisty	extremista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dr	dr	kA
<g/>
.	.	kIx.
F.	F.	kA
Ďurčanský	Ďurčanský	k2eAgInSc1d1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
ministrem	ministr	k1gMnSc7
spravedlnosti	spravedlnost	k1gFnSc2
a	a	k8xC
propagandistický	propagandistický	k2eAgInSc1d1
aparát	aparát	k1gInSc1
byl	být	k5eAaImAgInS
svěřen	svěřen	k2eAgInSc4d1
Š.	Š.	kA
Machovi	Machovi	k1gRnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
muži	muž	k1gMnPc1
získali	získat	k5eAaPmAgMnP
vzdělání	vzdělání	k1gNnSc4
v	v	k7c6
nejnebezpečnějším	bezpečný	k2eNgNnSc6d3
semeništi	semeniště	k1gNnSc6
antisemitismu	antisemitismus	k1gInSc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
internátě	internát	k1gInSc6
univerzity	univerzita	k1gFnSc2
Svoradov	Svoradov	k1gInSc1
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
došli	dojít	k5eAaPmAgMnP
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
politický	politický	k2eAgInSc1d1
antisemitismus	antisemitismus	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejpopulárnějších	populární	k2eAgInPc2d3
a	a	k8xC
nejefektivnějších	efektivní	k2eAgInPc2d3
prostředků	prostředek	k1gInPc2
k	k	k7c3
získání	získání	k1gNnSc3
masové	masový	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
slovenského	slovenský	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Extremistická	extremistický	k2eAgFnSc1d1
slovenské	slovenský	k2eAgFnPc4d1
inteligence	inteligence	k1gFnPc4
<g/>
,	,	kIx,
mladá	mladý	k2eAgFnSc1d1
generace	generace	k1gFnSc1
nenávidící	nenávidící	k2eAgMnPc4d1
Židy	Žid	k1gMnPc4
a	a	k8xC
organizující	organizující	k2eAgFnPc4d1
demonstrace	demonstrace	k1gFnPc4
a	a	k8xC
povstání	povstání	k1gNnSc2
proti	proti	k7c3
nim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
vyrostla	vyrůst	k5eAaPmAgFnS
v	v	k7c6
Svoradově	Svoradův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
cvičná	cvičný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
pro	pro	k7c4
rezervy	rezerva	k1gFnPc4
aktivistů	aktivista	k1gMnPc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
rozsévačů	rozsévač	k1gMnPc2
rasismu	rasismus	k1gInSc2
a	a	k8xC
politického	politický	k2eAgInSc2d1
separatismu	separatismus	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
Svoradova	Svoradův	k2eAgMnSc2d1
Eugen	Eugen	k2eAgInSc4d1
Filkorn	Filkorn	k1gInSc4
o	o	k7c6
příchodu	příchod	k1gInSc6
lazaristů	lazarista	k1gMnPc2
na	na	k7c4
Svoradov	Svoradov	k1gInSc4
a	a	k8xC
jejich	jejich	k3xOp3gFnPc4
činnosti	činnost	k1gFnPc4
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
zajisté	zajisté	k9
z	z	k7c2
vyššího	vysoký	k2eAgNnSc2d2
vnuknutí	vnuknutí	k1gNnSc2
obrátil	obrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
nás	my	k3xPp1nPc4
lazarista	lazarista	k1gMnSc1
Jozef	Jozef	k1gMnSc1
Danielik	Danielik	k1gMnSc1
se	s	k7c7
žádostí	žádost	k1gFnSc7
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
přijali	přijmout	k5eAaPmAgMnP
do	do	k7c2
ústavu	ústav	k1gInSc2
(	(	kIx(
<g/>
Svoradova	Svoradův	k2eAgFnSc1d1
<g/>
)	)	kIx)
jejich	jejich	k3xOp3gInSc1
dorost	dorost	k1gInSc1
<g/>
,	,	kIx,
kleriků	klerik	k1gMnPc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
studovali	studovat	k5eAaImAgMnP
ve	v	k7c6
Štýrském	štýrský	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
(	(	kIx(
<g/>
Graz	Graz	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
doma	doma	k6eAd1
měli	mít	k5eAaImAgMnP
pokračovat	pokračovat	k5eAaImF
na	na	k7c6
teologické	teologický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádosti	žádost	k1gFnSc2
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
nevyhovět	vyhovět	k5eNaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždyť	vždyť	k8xC
veškerá	veškerý	k3xTgFnSc1
starost	starost	k1gFnSc1
o	o	k7c4
stravování	stravování	k1gNnSc4
<g/>
,	,	kIx,
pořádek	pořádek	k1gInSc4
a	a	k8xC
čistotu	čistota	k1gFnSc4
v	v	k7c6
ústavě	ústava	k1gFnSc6
<g/>
,	,	kIx,
o	o	k7c6
dodělávání	dodělávání	k1gNnSc6
potravin	potravina	k1gFnPc2
na	na	k7c6
statku	statek	k1gInSc6
byla	být	k5eAaImAgFnS
svěřena	svěřit	k5eAaPmNgFnS
milosrdným	milosrdný	k2eAgFnPc3d1
sestrám	sestra	k1gFnPc3
z	z	k7c2
kongregace	kongregace	k1gFnSc2
sv.	sv.	kA
Vincenta	Vincent	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
zde	zde	k6eAd1
však	však	k9
i	i	k9
jiná	jiný	k2eAgFnSc1d1
pohnutka	pohnutka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klericích	klerik	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
budou	být	k5eAaImBp3nP
růst	růst	k1gInSc4
spolu	spolu	k6eAd1
s	s	k7c7
akademiky	akademik	k1gMnPc7
<g/>
,	,	kIx,
viděl	vidět	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
budoucí	budoucí	k2eAgMnPc4d1
vychovatele	vychovatel	k1gMnPc4
Svoradovčanů	Svoradovčan	k1gMnPc2
...	...	k?
Najednou	najednou	k6eAd1
se	se	k3xPyFc4
našly	najít	k5eAaPmAgFnP
vhodné	vhodný	k2eAgFnPc1d1
místnosti	místnost	k1gFnPc1
<g/>
,	,	kIx,
upravením	upravení	k1gNnSc7
chodby	chodba	k1gFnSc2
při	při	k7c6
kostelíku	kostelík	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začali	začít	k5eAaPmAgMnP
tam	tam	k6eAd1
život	život	k1gInSc4
přesně	přesně	k6eAd1
podle	podle	k7c2
pravidel	pravidlo	k1gNnPc2
sv.	sv.	kA
Vincenta	Vincent	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladný	příkladný	k2eAgInSc4d1
řeholní	řeholní	k2eAgInSc4d1
život	život	k1gInSc4
kleriků	klerik	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
jejich	jejich	k3xOp3gFnSc1
upřímná	upřímný	k2eAgFnSc1d1
zbožnost	zbožnost	k1gFnSc1
<g/>
,	,	kIx,
hlubokým	hluboký	k2eAgInSc7d1
dojmem	dojem	k1gInSc7
působily	působit	k5eAaImAgFnP
na	na	k7c6
všech	všecek	k3xTgInPc6
obyvatele	obyvatel	k1gMnPc4
ústavu	ústav	k1gInSc2
<g/>
.	.	kIx.
"	"	kIx"
</s>
<s>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
'	'	kIx"
</s>
<s>
Eugen	Eugen	k2eAgInSc1d1
Filkorn	Filkorn	k1gInSc1
o	o	k7c6
výtkách	výtka	k1gFnPc6
<g/>
,	,	kIx,
že	že	k8xS
Svoradov	Svoradov	k1gInSc1
byl	být	k5eAaImAgInS
"	"	kIx"
<g/>
luďáckou	luďácký	k2eAgFnSc7d1
baštou	bašta	k1gFnSc7
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Nikoho	nikdo	k3yNnSc4
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
neptal	ptat	k5eNaImAgMnS
při	při	k7c6
přijetí	přijetí	k1gNnSc6
do	do	k7c2
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
jaké	jaký	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
strany	strana	k1gFnSc2
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
tam	tam	k6eAd1
československých	československý	k2eAgMnPc2d1
lidovců	lidovec	k1gMnPc2
v	v	k7c6
hojném	hojný	k2eAgInSc6d1
počtu	počet	k1gInSc6
<g/>
,	,	kIx,
agrárníků	agrárník	k1gMnPc2
méně	málo	k6eAd2
<g/>
,	,	kIx,
ba	ba	k9
i	i	k8xC
Čechů	Čech	k1gMnPc2
a	a	k8xC
v	v	k7c6
menším	malý	k2eAgInSc6d2
počtu	počet	k1gInSc6
i	i	k9
socialistů	socialist	k1gMnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Tzv.	tzv.	kA
Národní	národní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
rozsudku	rozsudek	k1gInSc6
nad	nad	k7c4
Filkornom	Filkornom	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
konstatuje	konstatovat	k5eAaBmIp3nS
<g/>
:	:	kIx,
...	...	k?
Obviněný	obviněný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Filkorn	Filkorn	k1gMnSc1
<g/>
)	)	kIx)
zachránil	zachránit	k5eAaPmAgMnS
ve	v	k7c6
Svoradově	Svoradův	k2eAgFnSc6d1
osoby	osoba	k1gFnPc1
rasově	rasově	k6eAd1
pronásledované	pronásledovaný	k2eAgFnPc1d1
(	(	kIx(
<g/>
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ba	ba	k9
i	i	k8xC
účastníků	účastník	k1gMnPc2
povstání	povstání	k1gNnSc2
(	(	kIx(
<g/>
z	z	k7c2
Francie	Francie	k1gFnSc2
navrátivších	navrátivší	k2eAgNnPc2d1
se	se	k3xPyFc4
svoradovčanů	svoradovčan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
dosud	dosud	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
ucelená	ucelený	k2eAgFnSc1d1
publikace	publikace	k1gFnSc1
o	o	k7c6
historii	historie	k1gFnSc6
Svoradově	Svoradův	k2eAgFnSc6d1
(	(	kIx(
<g/>
její	její	k3xOp3gNnSc4
vydání	vydání	k1gNnSc4
připravuje	připravovat	k5eAaImIp3nS
prof.	prof.	kA
Róbert	Róbert	k1gMnSc1
Letz	Letz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úlomky	úlomek	k1gInPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
poskládat	poskládat	k5eAaPmF
z	z	k7c2
prací	práce	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Eugen	Eugen	k2eAgInSc1d1
Filkorn	Filkorn	k1gInSc1
-	-	kIx~
Věrný	věrný	k2eAgInSc1d1
svému	svůj	k3xOyFgNnSc3
svědomí	svědomí	k1gNnSc1
<g/>
,	,	kIx,
Czech	Czech	k1gMnSc1
Canadian	Canadian	k1gMnSc1
Cultural	Cultural	k1gMnSc1
and	and	k?
Heritage	Heritage	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Toronto	Toronto	k1gNnSc1
<g/>
,	,	kIx,
Canada	Canada	k1gFnSc1
a	a	k8xC
Matica	Matica	k1gFnSc1
slovenská	slovenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
František	František	k1gMnSc1
Braxatoris	Braxatoris	k1gFnSc2
-	-	kIx~
"	"	kIx"
<g/>
Eugen	Eugen	k2eAgMnSc1d1
Filkorn	Filkorn	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
Svoradova	Svoradův	k2eAgNnSc2d1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Anton	Anton	k1gMnSc1
Neuwirth	Neuwirth	k1gMnSc1
-	-	kIx~
"	"	kIx"
<g/>
Léčit	léčit	k5eAaImF
zlo	zlo	k1gNnSc4
láskou	láska	k1gFnSc7
<g/>
"	"	kIx"
</s>
<s>
František	František	k1gMnSc1
Hrušovský	hrušovský	k2eAgMnSc1d1
-	-	kIx~
"	"	kIx"
<g/>
Slovenské	slovenský	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
<g/>
"	"	kIx"
</s>
<s>
Imrich	Imrich	k1gMnSc1
Kružliak	Kružliak	k1gMnSc1
-	-	kIx~
"	"	kIx"
<g/>
Svoradov	Svoradov	k1gInSc1
v	v	k7c6
životě	život	k1gInSc6
národa	národ	k1gInSc2
<g/>
"	"	kIx"
</s>
<s>
Ján	Ján	k1gMnSc1
Kaššovič	Kaššovič	k1gMnSc1
-	-	kIx~
"	"	kIx"
<g/>
Svoradov	Svoradov	k1gInSc4
<g/>
,	,	kIx,
katolický	katolický	k2eAgInSc4d1
vysokoškolský	vysokoškolský	k2eAgInSc4d1
internát	internát	k1gInSc4
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Sb	sb	kA
<g/>
.	.	kIx.
Katolické	katolický	k2eAgNnSc1d1
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svoradovu	Svoradův	k2eAgFnSc4d1
se	se	k3xPyFc4
ve	v	k7c4
své	svůj	k3xOyFgFnPc4
diplomové	diplomový	k2eAgFnPc4d1
práci	práce	k1gFnSc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
profesora	profesor	k1gMnSc2
Roberta	Robert	k1gMnSc2
Letze	Letze	k1gFnSc2
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
mladý	mladý	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Martin	Martin	k1gMnSc1
Rázga	Rázga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Svoradov	Svoradov	k1gInSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠUTAJ	ŠUTAJ	k?
<g/>
,	,	kIx,
Štefan	Štefan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovenské	slovenský	k2eAgFnSc2d1
občianské	občianský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
v	v	k7c6
dokumentoch	dokumentoch	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Košice	Košice	k1gInPc4
<g/>
:	:	kIx,
SAV	SAV	kA
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Emanuel	Emanuel	k1gMnSc1
Frieder	Frieder	k1gMnSc1
<g/>
,	,	kIx,
Z	z	k7c2
denníka	denník	k1gInSc2
mladého	mladý	k2eAgMnSc2d1
rabína	rabín	k1gMnSc2
(	(	kIx(
<g/>
Slovenské	slovenský	k2eAgNnSc1d1
národné	národný	k2eAgNnSc1d1
múzeum	múzeum	k1gNnSc1
Bratislava	Bratislava	k1gFnSc1
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
s.	s.	k?
15	#num#	k4
<g/>
↑	↑	k?
Miroslav	Miroslav	k1gMnSc1
Obšivan	Obšivan	k1gMnSc1
<g/>
,	,	kIx,
Život	život	k1gInSc1
a	a	k8xC
dielo	dienout	k5eAaImAgNnS,k5eAaPmAgNnS
Jána	Ján	k1gMnSc4
Hutyru	Hutyra	k1gMnSc4
-	-	kIx~
provinciála	provinciál	k1gMnSc4
lazaristov	lazaristov	k1gInSc4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práca	práca	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Komenského	Komenský	k1gMnSc2
<g/>
,	,	kIx,
Rímskokatolícka	Rímskokatolícka	k1gFnSc1
cyrilometodská	cyrilometodský	k2eAgFnSc1d1
bohoslovecká	bohoslovecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
↑	↑	k?
Eugen	Eugen	k2eAgMnSc1d1
Filkorn	Filkorn	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Verný	Verný	k1gMnSc1
svojmu	svojm	k1gMnSc3
svedomiu	svedomius	k1gMnSc3
(	(	kIx(
<g/>
Slovak	Slovak	k1gMnSc1
Canadian	Canadian	k1gMnSc1
Cultural	Cultural	k1gMnSc1
and	and	k?
Heritage	Heritage	k1gInSc1
Centre	centr	k1gInSc5
Toronto	Toronto	k1gNnSc1
a	a	k8xC
Matica	Matic	k2eAgFnSc1d1
slovenská	slovenský	k2eAgFnSc1d1
Martin	Martin	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
359	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Bratislava	Bratislava	k1gFnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
