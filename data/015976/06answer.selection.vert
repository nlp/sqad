<s>
Po	po	k7c6
založení	založení	k1gNnSc6
Univerzity	univerzita	k1gFnSc2
Komenského	Komenský	k1gMnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
nastal	nastat	k5eAaPmAgInS
velký	velký	k2eAgInSc1d1
problém	problém	k1gInSc1
s	s	k7c7
ubytováním	ubytování	k1gNnSc7
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
neexistoval	existovat	k5eNaImAgMnS
žádný	žádný	k3yNgInSc4
vysokoškolský	vysokoškolský	k2eAgInSc4d1
internát	internát	k1gInSc4
<g/>
.	.	kIx.
</s>