<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
kostra	kostra	k1gFnSc1	kostra
ptáků	pták	k1gMnPc2	pták
řadu	řada	k1gFnSc4	řada
anatomických	anatomický	k2eAgFnPc2d1	anatomická
odchylek	odchylka	k1gFnPc2	odchylka
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
savci	savec	k1gMnPc7	savec
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
relativní	relativní	k2eAgFnSc1d1	relativní
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
pneumatizace	pneumatizace	k1gFnSc1	pneumatizace
většiny	většina	k1gFnSc2	většina
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
redukce	redukce	k1gFnSc1	redukce
jejich	jejich	k3xOp3gInSc2	jejich
počtu	počet	k1gInSc2	počet
srůstem	srůst	k1gInSc7	srůst
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
kostry	kostra	k1gFnSc2	kostra
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
