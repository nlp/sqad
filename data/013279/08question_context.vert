<s>
Konin	konina	k1gFnPc2	konina
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Wartě	Wartha	k1gFnSc6	Wartha
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
město	město	k1gNnSc4	město
Velkopolského	velkopolský	k2eAgNnSc2d1	Velkopolské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
,	,	kIx,	,
po	po	k7c6	po
Poznani	Poznaň	k1gFnSc6	Poznaň
a	a	k8xC	a
Kaliszi	Kalisze	k1gFnSc6	Kalisze
<g/>
.	.	kIx.	.
</s>
