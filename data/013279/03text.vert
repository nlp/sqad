<p>
<s>
Konin	konina	k1gFnPc2	konina
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Wartě	Wartha	k1gFnSc6	Wartha
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
město	město	k1gNnSc4	město
Velkopolského	velkopolský	k2eAgNnSc2d1	Velkopolské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
,	,	kIx,	,
po	po	k7c6	po
Poznani	Poznaň	k1gFnSc6	Poznaň
a	a	k8xC	a
Kaliszi	Kalisze	k1gFnSc6	Kalisze
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
80	[number]	k4	80
471	[number]	k4	471
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
Města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Valašské	valašský	k2eAgNnSc1d1	Valašské
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Konin	konina	k1gFnPc2	konina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
