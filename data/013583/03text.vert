<s>
Vozovna	vozovna	k1gFnSc1
Strašnice	Strašnice	k1gFnPc1
</s>
<s>
Vozovna	vozovna	k1gFnSc1
Strašnice	Strašnice	k1gFnPc1
Vozovna	vozovna	k1gFnSc1
StrašniceÚčel	StrašniceÚčel	k1gFnSc1
stavby	stavba	k1gFnSc2
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
vozovna	vozovna	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1908	#num#	k4
Přestavba	přestavba	k1gFnSc1
</s>
<s>
1929-1933	1929-1933	k4
Současný	současný	k2eAgInSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
-Strašnice	-Strašnice	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vozovna	vozovna	k1gFnSc1
Strašnice	Strašnice	k1gFnPc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
sedmi	sedm	k4xCc2
tramvajových	tramvajový	k2eAgFnPc2d1
vozoven	vozovna	k1gFnPc2
provozovaných	provozovaný	k2eAgFnPc2d1
Dopravním	dopravní	k2eAgInSc7d1
podnikem	podnik	k1gInSc7
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
dnešní	dnešní	k2eAgNnSc1d1
využití	využití	k1gNnSc1
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1
v	v	k7c6
hale	hala	k1gFnSc6
strašnické	strašnický	k2eAgFnSc2d1
vozovny	vozovna	k1gFnSc2
</s>
<s>
Strašnická	strašnický	k2eAgFnSc1d1
vozovna	vozovna	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
provoz	provoz	k1gInSc4
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1908	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
nejstarší	starý	k2eAgInSc1d3
dosud	dosud	k6eAd1
provozovanou	provozovaný	k2eAgFnSc7d1
tramvajovou	tramvajový	k2eAgFnSc7d1
vozovnou	vozovna	k1gFnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tehdy	tehdy	k6eAd1
měla	mít	k5eAaImAgFnS
tři	tři	k4xCgFnPc4
lodi	loď	k1gFnPc4
po	po	k7c6
pěti	pět	k4xCc6
kolejích	kolej	k1gFnPc6
a	a	k8xC
kapacitu	kapacita	k1gFnSc4
90	#num#	k4
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
tu	tu	k6eAd1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
objízdná	objízdný	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
v	v	k7c6
pražských	pražský	k2eAgFnPc6d1
vozovnách	vozovna	k1gFnPc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1928	#num#	k4
zde	zde	k6eAd1
pak	pak	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
měnírna	měnírna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1929	#num#	k4
až	až	k8xS
1933	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
rekonstrukce	rekonstrukce	k1gFnSc1
vozovny	vozovna	k1gFnSc2
<g/>
,	,	kIx,
původní	původní	k2eAgFnSc2d1
dřevěné	dřevěný	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
byly	být	k5eAaImAgFnP
zbourány	zbourat	k5eAaPmNgFnP
a	a	k8xC
nahrazeny	nahradit	k5eAaPmNgFnP
cihlovými	cihlový	k2eAgFnPc7d1
<g/>
,	,	kIx,
přidána	přidat	k5eAaPmNgFnS
byla	být	k5eAaImAgFnS
i	i	k9
čtvrtá	čtvrtý	k4xOgFnSc1
loď	loď	k1gFnSc1
a	a	k8xC
zrušena	zrušen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
již	již	k6eAd1
zmíněná	zmíněný	k2eAgFnSc1d1
objízdná	objízdný	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
kapacita	kapacita	k1gFnSc1
vozovny	vozovna	k1gFnSc2
o	o	k7c4
něco	něco	k3yInSc4
snížila	snížit	k5eAaPmAgFnS
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
její	její	k3xOp3gFnSc3
další	další	k2eAgFnSc3d1
přestavbě	přestavba	k1gFnSc3
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
modernějších	moderní	k2eAgInPc2d2
širších	široký	k2eAgInPc2d2
vozů	vůz	k1gInPc2
typu	typ	k1gInSc2
T.	T.	kA
</s>
<s>
Začátkem	začátkem	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
proběhla	proběhnout	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
vozovny	vozovna	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
přestavba	přestavba	k1gFnSc1
kolejiště	kolejiště	k1gNnSc2
<g/>
,	,	kIx,
zahájená	zahájený	k2eAgFnSc1d1
již	již	k6eAd1
1964	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
objízdná	objízdný	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
deponovány	deponován	k2eAgInPc1d1
vozy	vůz	k1gInPc1
Tatra	Tatra	k1gFnSc1
T	T	kA
<g/>
3	#num#	k4
<g/>
R.P	R.P	k1gFnSc2
<g/>
,	,	kIx,
Tatra	Tatra	k1gFnSc1
T	T	kA
<g/>
3	#num#	k4
<g/>
R.	R.	kA
<g/>
PLF	PLF	kA
a	a	k8xC
jeden	jeden	k4xCgInSc4
pracovní	pracovní	k2eAgInSc4d1
vůz	vůz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
FOJTÍK	Fojtík	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
LINERT	LINERT	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
;	;	kIx,
PROŠEK	Prošek	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
5013	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
324	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Stránky	stránka	k1gFnSc2
o	o	k7c6
pražských	pražský	k2eAgFnPc6d1
tramvajích	tramvaj	k1gFnPc6
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
o	o	k7c6
vozovně	vozovna	k1gFnSc6
Strašnice	Strašnice	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vozovna	vozovna	k1gFnSc1
Strašnice	Strašnice	k1gFnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
o	o	k7c6
pražských	pražský	k2eAgFnPc6d1
tramvajích	tramvaj	k1gFnPc6
<g/>
,	,	kIx,
i	i	k9
vozovnách	vozovna	k1gFnPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Tramvajové	tramvajový	k2eAgFnSc2d1
vozovny	vozovna	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Vypravující	vypravující	k2eAgFnSc2d1
vozovny	vozovna	k1gFnSc2
</s>
<s>
Vozovna	vozovna	k1gFnSc1
Hloubětín	Hloubětín	k1gInSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Kobylisy	Kobylisy	k1gInPc4
•	•	k?
Vozovna	vozovna	k1gFnSc1
Motol	Motol	k1gInSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Pankrác	Pankrác	k1gFnSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Strašnice	Strašnice	k1gFnPc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Střešovice	Střešovice	k1gFnSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Vokovice	Vokovice	k1gFnSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Žižkov	Žižkov	k1gInSc1
Tramvajové	tramvajový	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
</s>
<s>
Ústřední	ústřední	k2eAgFnPc1d1
dílny	dílna	k1gFnPc1
DP	DP	kA
•	•	k?
Ústřední	ústřední	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
Centrála	centrála	k1gFnSc1
(	(	kIx(
<g/>
zrušené	zrušený	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Ústřední	ústřední	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
Rustonka	Rustonka	k1gFnSc1
(	(	kIx(
<g/>
zrušené	zrušený	k2eAgFnSc2d1
<g/>
)	)	kIx)
Zrušené	zrušený	k2eAgFnSc2d1
vozovny	vozovna	k1gFnSc2
</s>
<s>
Vozovna	vozovna	k1gFnSc1
Karlín	Karlín	k1gInSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Smíchov	Smíchov	k1gInSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Zvonařka	Zvonařka	k1gFnSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Letná	letný	k2eAgFnSc1d1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Libeň	Libeň	k1gFnSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Klamovka	Klamovka	k1gFnSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Královské	královský	k2eAgFnPc1d1
Vinohrady	Vinohrady	k1gInPc4
•	•	k?
Vozovna	vozovna	k1gFnSc1
Královská	královský	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Vozovna	vozovna	k1gFnSc1
Centrála	centrála	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
|	|	kIx~
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
