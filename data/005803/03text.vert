<s>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
LF	LF	kA	LF
MU	MU	kA	MU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
fakult	fakulta	k1gFnPc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
lékařských	lékařský	k2eAgInPc2d1	lékařský
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
specializací	specializace	k1gFnPc2	specializace
a	a	k8xC	a
vědecko-výzkumnou	vědeckoýzkumný	k2eAgFnSc4d1	vědecko-výzkumná
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
čtyři	čtyři	k4xCgFnPc4	čtyři
nejstarší	starý	k2eAgFnPc4d3	nejstarší
fakulty	fakulta	k1gFnPc4	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
sídlila	sídlit	k5eAaImAgFnS	sídlit
fakulta	fakulta	k1gFnSc1	fakulta
na	na	k7c6	na
Komenského	Komenského	k2eAgNnSc6d1	Komenského
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2010	[number]	k4	2010
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Univerzitním	univerzitní	k2eAgInSc6d1	univerzitní
kampusu	kampus	k1gInSc6	kampus
v	v	k7c6	v
Brně-Bohunicích	Brně-Bohunice	k1gFnPc6	Brně-Bohunice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přírodovědeckou	přírodovědecký	k2eAgFnSc7d1	Přírodovědecká
fakultou	fakulta	k1gFnSc7	fakulta
<g/>
,	,	kIx,	,
fakultou	fakulta	k1gFnSc7	fakulta
sportovních	sportovní	k2eAgFnPc2d1	sportovní
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
četnými	četný	k2eAgFnPc7d1	četná
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
institucemi	instituce	k1gFnPc7	instituce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
centra	centrum	k1gNnSc2	centrum
vědecké	vědecký	k2eAgFnSc2d1	vědecká
excelence	excelence	k1gFnSc2	excelence
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
věd	věda	k1gFnPc2	věda
o	o	k7c6	o
živé	živý	k2eAgFnSc6d1	živá
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
pokročilých	pokročilý	k2eAgInPc2d1	pokročilý
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
technologií	technologie	k1gFnSc7	technologie
CEITEC	CEITEC	kA	CEITEC
<g/>
.	.	kIx.	.
</s>
<s>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
prostor	prostor	k1gInSc1	prostor
Fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
Brno	Brno	k1gNnSc1	Brno
v	v	k7c6	v
kampusu	kampus	k1gInSc6	kampus
<g/>
,	,	kIx,	,
Fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
u	u	k7c2	u
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
Masarykova	Masarykův	k2eAgInSc2d1	Masarykův
onkologického	onkologický	k2eAgInSc2d1	onkologický
ústavu	ústav	k1gInSc2	ústav
na	na	k7c6	na
Žlutém	žlutý	k2eAgInSc6d1	žlutý
kopci	kopec	k1gInSc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
LF	LF	kA	LF
MU	MU	kA	MU
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
desíti	deset	k4xCc2	deset
lékařských	lékařský	k2eAgFnPc2d1	lékařská
fakult	fakulta	k1gFnPc2	fakulta
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
magisterské	magisterský	k2eAgInPc4d1	magisterský
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
přijato	přijmout	k5eAaPmNgNnS	přijmout
743	[number]	k4	743
ze	z	k7c2	z
4	[number]	k4	4
627	[number]	k4	627
uchazečů	uchazeč	k1gMnPc2	uchazeč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bakalářské	bakalářský	k2eAgInPc4d1	bakalářský
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
bylo	být	k5eAaImAgNnS	být
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
přijato	přijmout	k5eAaPmNgNnS	přijmout
582	[number]	k4	582
z	z	k7c2	z
3	[number]	k4	3
050	[number]	k4	050
uchazečů	uchazeč	k1gMnPc2	uchazeč
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
základě	základ	k1gInSc6	základ
usnesení	usnesení	k1gNnSc2	usnesení
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
na	na	k7c4	na
popud	popud	k1gInSc4	popud
poslanců	poslanec	k1gMnPc2	poslanec
Aloise	Alois	k1gMnSc2	Alois
Jiráska	Jirásek	k1gMnSc2	Jirásek
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Engliše	Engliš	k1gMnSc2	Engliš
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
kolegů	kolega	k1gMnPc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Vydáním	vydání	k1gNnSc7	vydání
Zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
1919	[number]	k4	1919
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
shromáždění	shromáždění	k1gNnSc1	shromáždění
nařídilo	nařídit	k5eAaPmAgNnS	nařídit
založení	založení	k1gNnSc4	založení
československé	československý	k2eAgFnSc2d1	Československá
státní	státní	k2eAgFnSc2d1	státní
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Právnické	právnický	k2eAgNnSc4d1	právnické
<g/>
,	,	kIx,	,
lékařské	lékařský	k2eAgNnSc4d1	lékařské
<g/>
,	,	kIx,	,
přírodovědecké	přírodovědecký	k2eAgNnSc4d1	Přírodovědecké
a	a	k8xC	a
filosofické	filosofický	k2eAgNnSc4d1	filosofické
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
dál	daleko	k6eAd2	daleko
uložil	uložit	k5eAaPmAgInS	uložit
zřízení	zřízení	k1gNnSc4	zřízení
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
důvodů	důvod	k1gInPc2	důvod
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
odlehčit	odlehčit	k5eAaPmF	odlehčit
pražským	pražský	k2eAgFnPc3d1	Pražská
fakultám	fakulta	k1gFnPc3	fakulta
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
kapacitě	kapacita	k1gFnSc3	kapacita
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
plánované	plánovaný	k2eAgFnSc3d1	plánovaná
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zákon	zákon	k1gInSc1	zákon
stanovil	stanovit	k5eAaPmAgInS	stanovit
časový	časový	k2eAgInSc4d1	časový
rámec	rámec	k1gInSc4	rámec
pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
zázemí	zázemí	k1gNnSc2	zázemí
univerzity	univerzita	k1gFnSc2	univerzita
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
signatáři	signatář	k1gMnPc1	signatář
bychom	by	kYmCp1nP	by
našli	najít	k5eAaPmAgMnP	najít
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
<g/>
,	,	kIx,	,
Švehlu	Švehla	k1gMnSc4	Švehla
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Stránského	Stránský	k1gMnSc2	Stránský
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Rašína	Rašín	k1gMnSc2	Rašín
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
byla	být	k5eAaImAgFnS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
existence	existence	k1gFnSc1	existence
Zemské	zemský	k2eAgFnSc2d1	zemská
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
Pekařské	pekařský	k2eAgFnSc6d1	Pekařská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc4d1	založený
roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
přítomnost	přítomnost	k1gFnSc1	přítomnost
specializovaného	specializovaný	k2eAgInSc2d1	specializovaný
Zemského	zemský	k2eAgInSc2d1	zemský
ústavu	ústav	k1gInSc2	ústav
černovického	černovický	k2eAgMnSc4d1	černovický
pro	pro	k7c4	pro
choromyslné	choromyslný	k1gMnPc4	choromyslný
postaveného	postavený	k2eAgInSc2d1	postavený
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
a	a	k8xC	a
Zemské	zemský	k2eAgFnPc1d1	zemská
porodnice	porodnice	k1gFnPc1	porodnice
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgFnPc1d1	vybudovaná
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
na	na	k7c6	na
Obilním	obilní	k2eAgInSc6d1	obilní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
zákona	zákon	k1gInSc2	zákon
šestičlenná	šestičlenný	k2eAgFnSc1d1	šestičlenná
přípravná	přípravný	k2eAgFnSc1d1	přípravná
komise	komise	k1gFnSc1	komise
předložila	předložit	k5eAaPmAgFnS	předložit
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
jmenování	jmenování	k1gNnSc4	jmenování
profesorského	profesorský	k2eAgInSc2d1	profesorský
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
byli	být	k5eAaImAgMnP	být
Edward	Edward	k1gMnSc1	Edward
Babák	Babák	k1gMnSc1	Babák
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Ostrčil	Ostrčil	k1gMnSc1	Ostrčil
<g/>
,	,	kIx,	,
Otomar	Otomar	k1gMnSc1	Otomar
Völker	Völker	k1gMnSc1	Völker
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
Studnička	Studnička	k1gMnSc1	Studnička
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Hamsík	Hamsík	k1gMnSc1	Hamsík
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Vanýsek	Vanýsek	k1gMnSc1	Vanýsek
a	a	k8xC	a
Julius	Julius	k1gMnSc1	Julius
Petřivalský	Petřivalský	k2eAgMnSc1d1	Petřivalský
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
prof.	prof.	kA	prof.
Pavel	Pavel	k1gMnSc1	Pavel
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kučera	Kučera	k1gMnSc1	Kučera
prvním	první	k4xOgMnSc6	první
děkanem	děkan	k1gMnSc7	děkan
a	a	k8xC	a
už	už	k6eAd1	už
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1919	[number]	k4	1919
prof.	prof.	kA	prof.
Edward	Edward	k1gMnSc1	Edward
Babák	Babák	k1gMnSc1	Babák
pronesl	pronést	k5eAaPmAgMnS	pronést
první	první	k4xOgFnSc4	první
přednášku	přednáška	k1gFnSc4	přednáška
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
Úvod	úvod	k1gInSc1	úvod
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
lékařskému	lékařský	k2eAgInSc3d1	lékařský
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Teoretická	teoretický	k2eAgNnPc1d1	teoretické
pracoviště	pracoviště	k1gNnPc1	pracoviště
byla	být	k5eAaImAgNnP	být
rozmístěna	rozmístit	k5eAaPmNgNnP	rozmístit
po	po	k7c6	po
budovách	budova	k1gFnPc6	budova
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kasáren	kasárny	k1gFnPc2	kasárny
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Úvoz	úvoz	k1gInSc4	úvoz
a	a	k8xC	a
Údolní	údolní	k2eAgInSc4d1	údolní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
klinické	klinický	k2eAgInPc1d1	klinický
obory	obor	k1gInPc1	obor
se	se	k3xPyFc4	se
vyučovaly	vyučovat	k5eAaImAgInP	vyučovat
v	v	k7c6	v
Zemské	zemský	k2eAgFnSc6d1	zemská
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
české	český	k2eAgFnPc1d1	Česká
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
uzavřeny	uzavřen	k2eAgMnPc4d1	uzavřen
nacisty	nacista	k1gMnPc4	nacista
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
nemocnice	nemocnice	k1gFnSc2	nemocnice
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
likvidaci	likvidace	k1gFnSc3	likvidace
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
jak	jak	k8xS	jak
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
nejdříve	dříve	k6eAd3	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
jako	jako	k8xS	jako
částečnou	částečný	k2eAgFnSc4d1	částečná
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
zničené	zničený	k2eAgInPc4d1	zničený
ústavy	ústav	k1gInPc4	ústav
získala	získat	k5eAaPmAgFnS	získat
část	část	k1gFnSc1	část
budovy	budova	k1gFnSc2	budova
zrušené	zrušený	k2eAgFnSc2d1	zrušená
německé	německý	k2eAgFnSc2d1	německá
polytechniky	polytechnika	k1gFnSc2	polytechnika
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
budovy	budova	k1gFnPc4	budova
na	na	k7c6	na
Komenského	Komenského	k2eAgNnSc6d1	Komenského
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Materiální	materiální	k2eAgFnSc7d1	materiální
pomocí	pomoc	k1gFnSc7	pomoc
přispěly	přispět	k5eAaPmAgFnP	přispět
UNRRA	UNRRA	kA	UNRRA
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zotavování	zotavování	k1gNnSc1	zotavování
skončilo	skončit	k5eAaPmAgNnS	skončit
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
změna	změna	k1gFnSc1	změna
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
propouštění	propouštění	k1gNnSc3	propouštění
demokraticky	demokraticky	k6eAd1	demokraticky
smýšlejících	smýšlející	k2eAgMnPc2d1	smýšlející
členů	člen	k1gMnPc2	člen
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k6eAd1	také
k	k	k7c3	k
oborovým	oborový	k2eAgFnPc3d1	oborová
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
dvouleté	dvouletý	k2eAgNnSc1d1	dvouleté
farmaceutické	farmaceutický	k2eAgNnSc1d1	farmaceutické
studium	studium	k1gNnSc1	studium
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
prodlouženo	prodloužit	k5eAaPmNgNnS	prodloužit
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgMnPc4d1	čtyřletý
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
obor	obor	k1gInSc1	obor
vyčleněn	vyčlenit	k5eAaPmNgInS	vyčlenit
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Zubní	zubní	k2eAgNnSc1d1	zubní
lékařství	lékařství	k1gNnSc1	lékařství
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
směr	směr	k1gInSc4	směr
zavedeno	zaveden	k2eAgNnSc1d1	zavedeno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výuka	výuka	k1gFnSc1	výuka
samostatného	samostatný	k2eAgInSc2d1	samostatný
oboru	obor	k1gInSc2	obor
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
byla	být	k5eAaImAgFnS	být
pozdržena	pozdržet	k5eAaPmNgFnS	pozdržet
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
bylo	být	k5eAaImAgNnS	být
studijní	studijní	k2eAgNnSc1d1	studijní
portfolio	portfolio	k1gNnSc1	portfolio
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
o	o	k7c4	o
pediatrický	pediatrický	k2eAgInSc4d1	pediatrický
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
výuka	výuka	k1gFnSc1	výuka
pediatrie	pediatrie	k1gFnSc2	pediatrie
vydržela	vydržet	k5eAaPmAgFnS	vydržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
splynula	splynout	k5eAaPmAgFnS	splynout
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
Všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
pětileté	pětiletý	k2eAgNnSc1d1	pětileté
studium	studium	k1gNnSc1	studium
medicíny	medicína	k1gFnSc2	medicína
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
prodlouženo	prodloužen	k2eAgNnSc1d1	prodlouženo
po	po	k7c6	po
semestrech	semestr	k1gInPc6	semestr
nejdříve	dříve	k6eAd3	dříve
na	na	k7c4	na
pět	pět	k4xCc4	pět
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
přinesla	přinést	k5eAaPmAgFnS	přinést
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
probíhal	probíhat	k5eAaImAgInS	probíhat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fyziologickém	fyziologický	k2eAgInSc6d1	fyziologický
ústavu	ústav	k1gInSc6	ústav
doc.	doc.	kA	doc.
MUDr.	MUDr.	kA	MUDr.
Jan	Jana	k1gFnPc2	Jana
Peňáz	Peňáza	k1gFnPc2	Peňáza
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
automatický	automatický	k2eAgInSc1d1	automatický
neinvazivní	invazivní	k2eNgInSc1d1	neinvazivní
měřič	měřič	k1gInSc1	měřič
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vedená	vedený	k2eAgFnSc1d1	vedená
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Vašků	Vašek	k1gMnPc2	Vašek
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
první	první	k4xOgFnSc1	první
aplikace	aplikace	k1gFnSc1	aplikace
umělého	umělý	k2eAgNnSc2d1	umělé
srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
klinické	klinický	k2eAgFnSc6d1	klinická
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
na	na	k7c6	na
Obilním	obilní	k2eAgInSc6d1	obilní
Trhu	trh	k1gInSc6	trh
narodilo	narodit	k5eAaPmAgNnS	narodit
první	první	k4xOgMnPc1	první
"	"	kIx"	"
<g/>
dítě	dítě	k1gNnSc1	dítě
ze	z	k7c2	z
zkumavky	zkumavka	k1gFnSc2	zkumavka
<g/>
"	"	kIx"	"
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
metody	metoda	k1gFnSc2	metoda
transferu	transfer	k1gInSc2	transfer
gamet	gamet	k1gInSc1	gamet
do	do	k7c2	do
vejcovodu	vejcovod	k1gInSc2	vejcovod
(	(	kIx(	(
<g/>
GIFT	GIFT	kA	GIFT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oboru	obor	k1gInSc6	obor
fertilizace	fertilizace	k1gFnSc1	fertilizace
a	a	k8xC	a
transplantologii	transplantologie	k1gFnSc3	transplantologie
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
dalších	další	k2eAgMnPc2d1	další
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byly	být	k5eAaImAgFnP	být
fakulta	fakulta	k1gFnSc1	fakulta
i	i	k8xC	i
její	její	k3xOp3gFnPc1	její
ústavy	ústava	k1gFnPc1	ústava
postupně	postupně	k6eAd1	postupně
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
moderní	moderní	k2eAgFnSc7d1	moderní
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
diagnostickými	diagnostický	k2eAgInPc7d1	diagnostický
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
zaváděly	zavádět	k5eAaImAgFnP	zavádět
nové	nový	k2eAgInPc4d1	nový
terapeutické	terapeutický	k2eAgInPc4d1	terapeutický
postupy	postup	k1gInPc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Potřebám	potřeba	k1gFnPc3	potřeba
praktické	praktický	k2eAgFnSc2d1	praktická
medicíny	medicína	k1gFnSc2	medicína
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
také	také	k6eAd1	také
zvyšující	zvyšující	k2eAgMnSc1d1	zvyšující
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
katedry	katedra	k1gFnPc1	katedra
pro	pro	k7c4	pro
nelékařské	lékařský	k2eNgInPc4d1	nelékařský
obory	obor	k1gInPc4	obor
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
studia	studio	k1gNnSc2	studio
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
navazujícím	navazující	k2eAgNnSc6d1	navazující
studiu	studio	k1gNnSc6	studio
magisterském	magisterský	k2eAgNnSc6d1	magisterské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
již	již	k6eAd1	již
vedle	vedle	k7c2	vedle
fakulty	fakulta	k1gFnSc2	fakulta
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
prostorách	prostora	k1gFnPc6	prostora
také	také	k9	také
celouniverzitní	celouniverzitní	k2eAgFnSc1d1	celouniverzitní
počítačová	počítačový	k2eAgFnSc1d1	počítačová
studovna	studovna	k1gFnSc1	studovna
s	s	k7c7	s
nepřetržitým	přetržitý	k2eNgInSc7d1	nepřetržitý
provozem	provoz	k1gInSc7	provoz
a	a	k8xC	a
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
studenty	student	k1gMnPc4	student
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
fakulta	fakulta	k1gFnSc1	fakulta
denním	denní	k2eAgNnSc7d1	denní
místem	místo	k1gNnSc7	místo
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
stěhování	stěhování	k1gNnSc2	stěhování
prvních	první	k4xOgNnPc2	první
pracovišť	pracoviště	k1gNnPc2	pracoviště
do	do	k7c2	do
nově	nově	k6eAd1	nově
zbudovaného	zbudovaný	k2eAgInSc2d1	zbudovaný
Univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
kampusu	kampus	k1gInSc2	kampus
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
výuková	výukový	k2eAgFnSc1d1	výuková
část	část	k1gFnSc1	část
kampusu	kampus	k1gInSc2	kampus
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
původní	původní	k2eAgFnSc2d1	původní
prostory	prostora	k1gFnSc2	prostora
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
na	na	k7c6	na
Komenského	Komenského	k2eAgNnSc6d1	Komenského
náměstí	náměstí	k1gNnSc6	náměstí
využívá	využívat	k5eAaImIp3nS	využívat
řada	řada	k1gFnSc1	řada
celouniverzitních	celouniverzitní	k2eAgNnPc2d1	celouniverzitní
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
transfer	transfer	k1gInSc4	transfer
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
Kariérní	kariérní	k2eAgNnSc1d1	kariérní
centrum	centrum	k1gNnSc1	centrum
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
nebo	nebo	k8xC	nebo
zázemí	zázemí	k1gNnSc2	zázemí
pro	pro	k7c4	pro
univerzitu	univerzita	k1gFnSc4	univerzita
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
66	[number]	k4	66
ústavů	ústav	k1gInPc2	ústav
a	a	k8xC	a
klinik	klinika	k1gFnPc2	klinika
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
Správní	správní	k2eAgNnPc1d1	správní
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
,	,	kIx,	,
Účelová	účelový	k2eAgNnPc1d1	účelové
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
zaměřená	zaměřený	k2eAgNnPc1d1	zaměřené
teoretická	teoretický	k2eAgNnPc1d1	teoretické
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
,	,	kIx,	,
pracoviště	pracoviště	k1gNnSc1	pracoviště
nelékařských	lékařský	k2eNgInPc2d1	nelékařský
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
pracoviště	pracoviště	k1gNnSc2	pracoviště
společná	společný	k2eAgFnSc1d1	společná
s	s	k7c7	s
klinikami	klinika	k1gFnPc7	klinika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kliniky	klinika	k1gFnPc1	klinika
společné	společný	k2eAgFnPc1d1	společná
s	s	k7c7	s
fakultou	fakulta	k1gFnSc7	fakulta
ve	v	k7c4	v
Fakultní	fakultní	k2eAgNnSc4d1	fakultní
nemocnicí	nemocnice	k1gFnSc7	nemocnice
u	u	k7c2	u
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
Fakultní	fakultní	k2eAgFnSc7d1	fakultní
nemocnicí	nemocnice	k1gFnSc7	nemocnice
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
pracovišti	pracoviště	k1gNnSc6	pracoviště
dospělého	dospělý	k2eAgInSc2d1	dospělý
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
dětského	dětský	k2eAgInSc2d1	dětský
věku	věk	k1gInSc2	věk
a	a	k8xC	a
reprodukční	reprodukční	k2eAgFnSc2d1	reprodukční
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Úrazové	úrazový	k2eAgFnSc6d1	Úrazová
nemocnici	nemocnice	k1gFnSc6	nemocnice
a	a	k8xC	a
na	na	k7c6	na
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
onkologickém	onkologický	k2eAgInSc6d1	onkologický
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bychom	by	kYmCp1nP	by
našli	najít	k5eAaPmAgMnP	najít
53	[number]	k4	53
klinik	klinika	k1gFnPc2	klinika
rozmístěných	rozmístěný	k2eAgFnPc2d1	rozmístěná
po	po	k7c6	po
areálech	areál	k1gInPc6	areál
nemocnic	nemocnice	k1gFnPc2	nemocnice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
potřebám	potřeba	k1gFnPc3	potřeba
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
pracovišť	pracoviště	k1gNnPc2	pracoviště
lékařské	lékařský	k2eAgFnSc2d1	lékařská
a	a	k8xC	a
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Institut	institut	k1gInSc1	institut
biostatistiky	biostatistika	k1gFnSc2	biostatistika
a	a	k8xC	a
analýz	analýza	k1gFnPc2	analýza
Lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Správní	správní	k2eAgNnSc1d1	správní
pracoviště	pracoviště	k1gNnSc1	pracoviště
fakulty	fakulta	k1gFnSc2	fakulta
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
technicko-provozní	technickorovozní	k2eAgNnSc4d1	technicko-provozní
oddělení	oddělení	k1gNnSc4	oddělení
a	a	k8xC	a
děkanát	děkanát	k1gInSc4	děkanát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gNnSc6	jehož
čele	čelo	k1gNnSc6	čelo
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nP	stát
prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
Jiří	Jiří	k1gMnSc1	Jiří
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
je	být	k5eAaImIp3nS	být
profesor	profesor	k1gMnSc1	profesor
Mayer	Mayer	k1gMnSc1	Mayer
39	[number]	k4	39
<g/>
.	.	kIx.	.
děkanem	děkan	k1gMnSc7	děkan
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Účelová	účelový	k2eAgNnPc1d1	účelové
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
heterogenní	heterogenní	k2eAgFnSc7d1	heterogenní
skupinou	skupina	k1gFnSc7	skupina
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
Centrum	centrum	k1gNnSc1	centrum
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
nekomerční	komerční	k2eNgNnSc4d1	nekomerční
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
pro	pro	k7c4	pro
výukové	výukový	k2eAgInPc4d1	výukový
a	a	k8xC	a
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
informačních	informační	k2eAgFnPc2d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
Knihovnu	knihovna	k1gFnSc4	knihovna
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
kampusu	kampus	k1gInSc2	kampus
<g/>
,	,	kIx,	,
vzniknuvší	vzniknuvší	k2eAgFnSc4d1	vzniknuvší
splynutím	splynutí	k1gNnSc7	splynutí
knihovny	knihovna	k1gFnSc2	knihovna
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
knihovny	knihovna	k1gFnSc2	knihovna
fakulty	fakulta	k1gFnSc2	fakulta
sportovních	sportovní	k2eAgFnPc2d1	sportovní
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
části	část	k1gFnSc2	část
knihovny	knihovna	k1gFnSc2	knihovna
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Teoretická	teoretický	k2eAgNnPc1d1	teoretické
pracoviště	pracoviště	k1gNnPc1	pracoviště
téměř	téměř	k6eAd1	téměř
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
svými	svůj	k3xOyFgInPc7	svůj
zaměřeními	zaměření	k1gNnPc7	zaměření
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
studia	studio	k1gNnSc2	studio
Všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Řadíme	řadit	k5eAaImIp1nP	řadit
sem	sem	k6eAd1	sem
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
ústavů	ústav	k1gInPc2	ústav
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgInPc4	který
řadíme	řadit	k5eAaImIp1nP	řadit
například	například	k6eAd1	například
Biofyzikální	biofyzikální	k2eAgInSc4d1	biofyzikální
<g/>
,	,	kIx,	,
Anatomický	anatomický	k2eAgInSc4d1	anatomický
a	a	k8xC	a
Fyziologický	fyziologický	k2eAgInSc4d1	fyziologický
ústav	ústav	k1gInSc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
způsob	způsob	k1gInSc1	způsob
řazení	řazení	k1gNnSc2	řazení
organizace	organizace	k1gFnSc2	organizace
fakulty	fakulta	k1gFnSc2	fakulta
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Akademické	akademický	k2eAgInPc1d1	akademický
orgány	orgán	k1gInPc1	orgán
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
Akademickým	akademický	k2eAgInSc7d1	akademický
senátem	senát	k1gInSc7	senát
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
Děkanem	děkan	k1gMnSc7	děkan
<g/>
,	,	kIx,	,
Vědeckou	vědecký	k2eAgFnSc7d1	vědecká
radou	rada	k1gFnSc7	rada
a	a	k8xC	a
Disciplinární	disciplinární	k2eAgFnSc7d1	disciplinární
komisí	komise	k1gFnSc7	komise
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
orgány	orgán	k1gInPc4	orgán
řadíme	řadit	k5eAaImIp1nP	řadit
Tajemníka	tajemník	k1gMnSc2	tajemník
a	a	k8xC	a
Poradu	porada	k1gFnSc4	porada
proděkanů	proděkan	k1gMnPc2	proděkan
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
kombinaci	kombinace	k1gFnSc4	kombinace
Aeskulapovy	Aeskulapův	k2eAgFnSc2d1	Aeskulapův
hole	hole	k1gFnSc2	hole
s	s	k7c7	s
hadem	had	k1gMnSc7	had
a	a	k8xC	a
závitu	závit	k1gInSc6	závit
dvoušroubovicové	dvoušroubovicový	k2eAgFnSc2d1	dvoušroubovicová
molekuly	molekula	k1gFnSc2	molekula
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
1795	[number]	k4	1795
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
dva	dva	k4xCgInPc4	dva
tradiční	tradiční	k2eAgInPc4d1	tradiční
magisterské	magisterský	k2eAgInPc4d1	magisterský
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
a	a	k8xC	a
stomatologii	stomatologie	k1gFnSc6	stomatologie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
také	také	k9	také
řada	řada	k1gFnSc1	řada
bakalářských	bakalářský	k2eAgInPc2d1	bakalářský
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vybrané	vybraný	k2eAgInPc4d1	vybraný
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
jak	jak	k6eAd1	jak
prezenční	prezenční	k2eAgFnSc1d1	prezenční
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kombinovanou	kombinovaný	k2eAgFnSc7d1	kombinovaná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízen	k2eAgInPc1d1	nabízen
tyto	tento	k3xDgInPc1	tento
programy	program	k1gInPc1	program
<g/>
:	:	kIx,	:
Magisterský	magisterský	k2eAgInSc1d1	magisterský
program	program	k1gInSc1	program
<g/>
:	:	kIx,	:
Všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
lékařství	lékařství	k1gNnSc1	lékařství
(	(	kIx(	(
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
zakončené	zakončený	k2eAgFnPc4d1	zakončená
titulem	titul	k1gInSc7	titul
MUDr.	MUDr.	kA	MUDr.
Zubní	zubní	k2eAgNnSc1d1	zubní
lékařství	lékařství	k1gNnSc1	lékařství
(	(	kIx(	(
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
zakončené	zakončený	k2eAgNnSc1d1	zakončené
titulem	titul	k1gInSc7	titul
MDDr	MDDra	k1gFnPc2	MDDra
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc1	program
jsou	být	k5eAaImIp3nP	být
vyučovány	vyučovat	k5eAaImNgInP	vyučovat
paralelně	paralelně	k6eAd1	paralelně
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
Anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
program	program	k1gInSc1	program
Specializace	specializace	k1gFnSc2	specializace
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
s	s	k7c7	s
obory	obor	k1gInPc7	obor
<g/>
:	:	kIx,	:
<g/>
Fyzioterapie	fyzioterapie	k1gFnPc4	fyzioterapie
Nutriční	nutriční	k2eAgMnSc1d1	nutriční
terapeut	terapeut	k1gMnSc1	terapeut
Optika	optik	k1gMnSc2	optik
a	a	k8xC	a
optometrie	optometrie	k1gFnSc2	optometrie
Ortoptika	Ortoptikum	k1gNnSc2	Ortoptikum
Radiologický	radiologický	k2eAgMnSc1d1	radiologický
asistent	asistent	k1gMnSc1	asistent
Zdravotní	zdravotní	k2eAgMnSc1d1	zdravotní
laborant	laborant	k1gMnSc1	laborant
Zdravotnický	zdravotnický	k2eAgMnSc1d1	zdravotnický
záchranář	záchranář	k1gMnSc1	záchranář
Obory	obora	k1gFnSc2	obora
fyzioterapie	fyzioterapie	k1gFnSc2	fyzioterapie
a	a	k8xC	a
optometrie	optometrie	k1gFnSc2	optometrie
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
jediné	jediný	k2eAgFnPc1d1	jediná
vyučované	vyučovaný	k2eAgFnPc1d1	vyučovaná
zároveň	zároveň	k6eAd1	zároveň
česky	česky	k6eAd1	česky
i	i	k9	i
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
program	program	k1gInSc1	program
Ošetřovatelství	ošetřovatelství	k1gNnSc2	ošetřovatelství
s	s	k7c7	s
oborem	obor	k1gInSc7	obor
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
sestra	sestra	k1gFnSc1	sestra
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
i	i	k8xC	i
kombinované	kombinovaný	k2eAgFnSc6d1	kombinovaná
formě	forma	k1gFnSc6	forma
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
program	program	k1gInSc1	program
Porodní	porodní	k2eAgFnSc1d1	porodní
asistentka	asistentka	k1gFnSc1	asistentka
s	s	k7c7	s
oborem	obor	k1gInSc7	obor
Porodní	porodní	k2eAgFnSc1d1	porodní
asistentka	asistentka	k1gFnSc1	asistentka
také	také	k9	také
v	v	k7c6	v
prezenční	prezenční	k2eAgFnSc6d1	prezenční
i	i	k8xC	i
kombinované	kombinovaný	k2eAgFnSc6d1	kombinovaná
formě	forma	k1gFnSc6	forma
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Magisterský	magisterský	k2eAgInSc1d1	magisterský
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgFnSc1d1	navazující
<g/>
:	:	kIx,	:
Fyzioterapie	fyzioterapie	k1gFnSc1	fyzioterapie
Intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
Nutriční	nutriční	k2eAgMnSc1d1	nutriční
specialista	specialista	k1gMnSc1	specialista
Optometrie	Optometrie	k1gFnSc2	Optometrie
Ve	v	k7c6	v
zvolených	zvolený	k2eAgFnPc6d1	zvolená
specializacích	specializace	k1gFnPc6	specializace
lze	lze	k6eAd1	lze
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
doktorském	doktorský	k2eAgNnSc6d1	doktorské
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zájemce	zájemce	k1gMnPc4	zájemce
konají	konat	k5eAaImIp3nP	konat
přípravně	přípravně	k6eAd1	přípravně
kurzy	kurz	k1gInPc1	kurz
pro	pro	k7c4	pro
přijímací	přijímací	k2eAgNnSc4d1	přijímací
řízení	řízení	k1gNnSc4	řízení
do	do	k7c2	do
magisterských	magisterský	k2eAgInPc2d1	magisterský
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
stipendijní	stipendijní	k2eAgInSc1d1	stipendijní
program	program	k1gInSc1	program
pro	pro	k7c4	pro
současné	současný	k2eAgMnPc4d1	současný
studenty	student	k1gMnPc4	student
fakulty	fakulta	k1gFnSc2	fakulta
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
přípravou	příprava	k1gFnSc7	příprava
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
P-PooL	P-PooL	k1gFnSc2	P-PooL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
jehož	jenž	k3xRgInSc2	jenž
stipendijního	stipendijní	k2eAgInSc2d1	stipendijní
programu	program	k1gInSc2	program
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
navštíveny	navštíven	k2eAgFnPc1d1	navštívena
konference	konference	k1gFnPc1	konference
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
nebo	nebo	k8xC	nebo
honorován	honorován	k2eAgMnSc1d1	honorován
student	student	k1gMnSc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
děkanů	děkan	k1gMnPc2	děkan
Lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
děkanem	děkan	k1gMnSc7	děkan
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
profesor	profesor	k1gMnSc1	profesor
Jiří	Jiří	k1gMnSc1	Jiří
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
zvolený	zvolený	k2eAgMnSc1d1	zvolený
již	již	k6eAd1	již
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
kandidatuře	kandidatura	k1gFnSc6	kandidatura
sumarizoval	sumarizovat	k5eAaBmAgInS	sumarizovat
dosažených	dosažený	k2eAgInPc2d1	dosažený
výsledků	výsledek	k1gInPc2	výsledek
a	a	k8xC	a
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
své	svůj	k3xOyFgInPc4	svůj
vysoké	vysoký	k2eAgInPc4d1	vysoký
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
vyučovat	vyučovat	k5eAaImF	vyučovat
náročně	náročně	k6eAd1	náročně
a	a	k8xC	a
publikovat	publikovat	k5eAaBmF	publikovat
ve	v	k7c6	v
srovnatelné	srovnatelný	k2eAgFnSc6d1	srovnatelná
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k6eAd1	potřeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
strategický	strategický	k2eAgInSc4d1	strategický
plán	plán	k1gInSc4	plán
rozvoje	rozvoj	k1gInSc2	rozvoj
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
bylo	být	k5eAaImAgNnS	být
pomocí	pomocí	k7c2	pomocí
systému	systém	k1gInSc2	systém
tzv.	tzv.	kA	tzv.
výukových	výukový	k2eAgFnPc2d1	výuková
jednotek	jednotka	k1gFnPc2	jednotka
provedena	proveden	k2eAgFnSc1d1	provedena
optimalizace	optimalizace	k1gFnSc1	optimalizace
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
projektu	projekt	k1gInSc2	projekt
OPTIMED	OPTIMED	kA	OPTIMED
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
využití	využití	k1gNnSc1	využití
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
ve	v	k7c6	v
výuce	výuka	k1gFnSc6	výuka
a	a	k8xC	a
definování	definování	k1gNnSc4	definování
minimálních	minimální	k2eAgInPc2d1	minimální
požadavků	požadavek	k1gInPc2	požadavek
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
oborů	obor	k1gInPc2	obor
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
provázání	provázání	k1gNnSc4	provázání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
speciálního	speciální	k2eAgNnSc2d1	speciální
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
programu	program	k1gInSc2	program
P-PooL	P-PooL	k1gFnPc2	P-PooL
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mimořádně	mimořádně	k6eAd1	mimořádně
nadané	nadaný	k2eAgMnPc4d1	nadaný
studenty	student	k1gMnPc4	student
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
koncept	koncept	k1gInSc1	koncept
dvourychlostní	dvourychlostní	k2eAgFnSc2d1	dvourychlostní
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
-	-	kIx~	-
mít	mít	k5eAaImF	mít
standard	standard	k1gInSc4	standard
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
také	také	k9	také
motivaci	motivace	k1gFnSc4	motivace
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Studentům	student	k1gMnPc3	student
bude	být	k5eAaImBp3nS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
kontinuální	kontinuální	k2eAgNnSc1d1	kontinuální
pokračování	pokračování	k1gNnSc1	pokračování
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
