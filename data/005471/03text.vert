<s>
Tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
neboli	neboli	k8xC	neboli
morfologie	morfologie	k1gFnSc1	morfologie
je	být	k5eAaImIp3nS	být
lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
ohýbáním	ohýbání	k1gNnSc7	ohýbání
(	(	kIx(	(
<g/>
skloňování	skloňování	k1gNnSc1	skloňování
<g/>
,	,	kIx,	,
časování	časování	k1gNnSc1	časování
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
odvozováním	odvozování	k1gNnSc7	odvozování
slov	slovo	k1gNnPc2	slovo
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
<g/>
,	,	kIx,	,
přípon	přípona	k1gFnPc2	přípona
a	a	k8xC	a
vpon	vpona	k1gFnPc2	vpona
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
disciplína	disciplína	k1gFnSc1	disciplína
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
strukturu	struktura	k1gFnSc4	struktura
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
uvedl	uvést	k5eAaPmAgInS	uvést
do	do	k7c2	do
jazykovědy	jazykověda	k1gFnSc2	jazykověda
August	August	k1gMnSc1	August
Schleicher	Schleichra	k1gFnPc2	Schleichra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Lingvista	lingvista	k1gMnSc1	lingvista
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
morfologií	morfologie	k1gFnSc7	morfologie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
morfolog	morfolog	k1gMnSc1	morfolog
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
určitého	určitý	k2eAgInSc2d1	určitý
jazyka	jazyk	k1gInSc2	jazyk
morfolog	morfolog	k1gMnSc1	morfolog
bere	brát	k5eAaImIp3nS	brát
jako	jako	k9	jako
sestavená	sestavený	k2eAgFnSc1d1	sestavená
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
morfémů	morfém	k1gInPc2	morfém
<g/>
.	.	kIx.	.
</s>
<s>
Morfologické	morfologický	k2eAgNnSc1d1	morfologické
hledisko	hledisko	k1gNnSc1	hledisko
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
typologické	typologický	k2eAgFnSc2d1	typologická
klasifikace	klasifikace	k1gFnSc2	klasifikace
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c4	na
analytické	analytický	k2eAgNnSc4d1	analytické
<g/>
,	,	kIx,	,
flektivní	flektivní	k2eAgNnSc4d1	flektivní
a	a	k8xC	a
aglutinační	aglutinační	k2eAgNnSc4d1	aglutinační
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jazyky	jazyk	k1gInPc4	jazyk
analytického	analytický	k2eAgInSc2d1	analytický
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
vietnamština	vietnamština	k1gFnSc1	vietnamština
<g/>
)	)	kIx)	)
ideálně	ideálně	k6eAd1	ideálně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
morfém	morfém	k1gInSc1	morfém
je	být	k5eAaImIp3nS	být
slovem	slovem	k6eAd1	slovem
a	a	k8xC	a
každé	každý	k3xTgNnSc1	každý
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
jedním	jeden	k4xCgInSc7	jeden
morfémem	morfém	k1gInSc7	morfém
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jazyků	jazyk	k1gInPc2	jazyk
aglutinačních	aglutinační	k2eAgInPc2d1	aglutinační
(	(	kIx(	(
<g/>
např.	např.	kA	např.
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
slovních	slovní	k2eAgFnPc2d1	slovní
forem	forma	k1gFnPc2	forma
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
ze	z	k7c2	z
snadno	snadno	k6eAd1	snadno
oddělitelných	oddělitelný	k2eAgInPc2d1	oddělitelný
segmentů	segment	k1gInPc2	segment
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
segment	segment	k1gInSc1	segment
přitom	přitom	k6eAd1	přitom
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jednomu	jeden	k4xCgInSc3	jeden
morfému	morfém	k1gInSc3	morfém
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
flektivní	flektivní	k2eAgInPc1d1	flektivní
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
spojují	spojovat	k5eAaImIp3nP	spojovat
často	často	k6eAd1	často
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
morfém	morfém	k1gInSc1	morfém
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
segmentu	segment	k1gInSc2	segment
<g/>
,	,	kIx,	,
morfu	morf	k1gInSc2	morf
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
se	se	k3xPyFc4	se
morfémy	morfém	k1gInPc1	morfém
realizují	realizovat	k5eAaBmIp3nP	realizovat
fúzí	fúze	k1gFnSc7	fúze
či	či	k8xC	či
nulovou	nulový	k2eAgFnSc7d1	nulová
realizací	realizace	k1gFnSc7	realizace
<g/>
.	.	kIx.	.
</s>
<s>
Segmentace	segmentace	k1gFnSc1	segmentace
slova	slovo	k1gNnSc2	slovo
na	na	k7c4	na
morfémy	morfém	k1gInPc4	morfém
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
flektivních	flektivní	k2eAgInPc2d1	flektivní
jazyků	jazyk	k1gInPc2	jazyk
tudíž	tudíž	k8xC	tudíž
často	často	k6eAd1	často
nemožná	možný	k2eNgFnSc1d1	nemožná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
proveditelná	proveditelný	k2eAgFnSc1d1	proveditelná
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
abstraktní	abstraktní	k2eAgFnSc6d1	abstraktní
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Morfém	morfém	k1gInSc1	morfém
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
strukturalismu	strukturalismus	k1gInSc2	strukturalismus
minimální	minimální	k2eAgFnSc7d1	minimální
<g/>
,	,	kIx,	,
sémanticky	sémanticky	k6eAd1	sémanticky
dále	daleko	k6eAd2	daleko
nedělitelná	dělitelný	k2eNgFnSc1d1	nedělitelná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
strukturalistickém	strukturalistický	k2eAgNnSc6d1	strukturalistické
učení	učení	k1gNnSc6	učení
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
bilaterální	bilaterální	k2eAgInSc4d1	bilaterální
jazykový	jazykový	k2eAgInSc4d1	jazykový
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jednotu	jednota	k1gFnSc4	jednota
označujícího	označující	k2eAgMnSc2d1	označující
(	(	kIx(	(
<g/>
formy	forma	k1gFnPc4	forma
<g/>
)	)	kIx)	)
a	a	k8xC	a
označovaného	označovaný	k2eAgInSc2d1	označovaný
(	(	kIx(	(
<g/>
významu	význam	k1gInSc2	význam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Morfémy	morfém	k1gInPc1	morfém
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
textu	text	k1gInSc6	text
realizovány	realizován	k2eAgInPc1d1	realizován
konkrétními	konkrétní	k2eAgFnPc7d1	konkrétní
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
morfy	morf	k1gInPc1	morf
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
různé	různý	k2eAgInPc1d1	různý
morfy	morf	k1gInPc1	morf
mohou	moct	k5eAaImIp3nP	moct
realizovat	realizovat	k5eAaBmF	realizovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
kontextech	kontext	k1gInPc6	kontext
jeden	jeden	k4xCgMnSc1	jeden
a	a	k8xC	a
tentýž	týž	k3xTgInSc4	týž
morfém	morfém	k1gInSc4	morfém
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
morfém	morfém	k1gInSc1	morfém
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
třídu	třída	k1gFnSc4	třída
alomorfů	alomorf	k1gInPc2	alomorf
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
morfém	morfém	k1gInSc1	morfém
plural	plurat	k5eAaImAgInS	plurat
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
alomorfy	alomorf	k1gInPc4	alomorf
-i	-i	k?	-i
<g/>
,	,	kIx,	,
-ové	vá	k1gFnPc1	-ová
<g/>
,	,	kIx,	,
-y	-y	k?	-y
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
a.	a.	k?	a.
</s>
