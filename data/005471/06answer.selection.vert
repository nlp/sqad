<s>
Tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
neboli	neboli	k8xC	neboli
morfologie	morfologie	k1gFnSc1	morfologie
je	být	k5eAaImIp3nS	být
lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
ohýbáním	ohýbání	k1gNnSc7	ohýbání
(	(	kIx(	(
<g/>
skloňování	skloňování	k1gNnSc1	skloňování
<g/>
,	,	kIx,	,
časování	časování	k1gNnSc1	časování
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
odvozováním	odvozování	k1gNnSc7	odvozování
slov	slovo	k1gNnPc2	slovo
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
<g/>
,	,	kIx,	,
přípon	přípona	k1gFnPc2	přípona
a	a	k8xC	a
vpon	vpona	k1gFnPc2	vpona
<g/>
.	.	kIx.	.
</s>
