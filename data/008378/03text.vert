<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Michael	Michael	k1gMnSc1	Michael
Benz	Benz	k1gMnSc1	Benz
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Carl	Carl	k1gMnSc1	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Benz	Benz	k1gMnSc1	Benz
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
Karlsruhe	Karlsruhe	k1gFnSc1	Karlsruhe
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Ladenburg	Ladenburg	k1gMnSc1	Ladenburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
automobilový	automobilový	k2eAgMnSc1d1	automobilový
konstruktér	konstruktér	k1gMnSc1	konstruktér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
první	první	k4xOgInSc4	první
benzínový	benzínový	k2eAgInSc4d1	benzínový
automobil	automobil	k1gInSc4	automobil
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
Benz	Benz	k1gInSc1	Benz
Patent-Motorwagen	Patent-Motorwagen	k1gInSc1	Patent-Motorwagen
Nummer	Nummer	k1gInSc1	Nummer
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Benz	Benz	k1gMnSc1	Benz
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1844	[number]	k4	1844
jako	jako	k8xS	jako
Karl	Karl	k1gMnSc1	Karl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Michael	Michael	k1gMnSc1	Michael
Wailend	Wailend	k1gMnSc1	Wailend
(	(	kIx(	(
<g/>
foneticky	foneticky	k6eAd1	foneticky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemanželské	manželský	k2eNgNnSc4d1	nemanželské
dítě	dítě	k1gNnSc4	dítě
Josephiny	Josephina	k1gFnSc2	Josephina
Vaillant	Vaillant	k1gMnSc1	Vaillant
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Karlsruhe	Karlsruhe	k1gNnSc2	Karlsruhe
Mühlburgu	Mühlburg	k1gInSc2	Mühlburg
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
narozených	narozený	k2eAgFnPc2d1	narozená
evangelické	evangelický	k2eAgFnSc2d1	evangelická
obce	obec	k1gFnSc2	obec
Mühlburg	Mühlburg	k1gMnSc1	Mühlburg
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
výpisu	výpis	k1gInSc2	výpis
z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Josephine	Josephin	k1gInSc5	Josephin
Vaillant	Vaillant	k1gMnSc1	Vaillant
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Johanna	Johanen	k2eAgMnSc4d1	Johanen
Georga	Georg	k1gMnSc4	Georg
Benze	Benze	k1gFnSc2	Benze
<g/>
.	.	kIx.	.
</s>
<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
Benz	Benz	k1gMnSc1	Benz
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
jméno	jméno	k1gNnSc4	jméno
mladého	mladý	k2eAgMnSc2d1	mladý
Karla	Karel	k1gMnSc2	Karel
Friedricha	Friedrich	k1gMnSc2	Friedrich
Michaela	Michael	k1gMnSc2	Michael
Benze	Benze	k1gFnSc2	Benze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c4	na
pofrancouzštěné	pofrancouzštěný	k2eAgInPc4d1	pofrancouzštěný
Carl	Carl	k1gInSc4	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Benz	Benz	k1gMnSc1	Benz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Benz	Benz	k1gMnSc1	Benz
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
mechanik	mechanika	k1gFnPc2	mechanika
a	a	k8xC	a
během	během	k7c2	během
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
založil	založit	k5eAaPmAgInS	založit
se	s	k7c7	s
společníkem	společník	k1gMnSc7	společník
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
firmu	firma	k1gFnSc4	firma
s	s	k7c7	s
materiály	materiál	k1gInPc7	materiál
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
však	však	k9	však
zajímaly	zajímat	k5eAaImAgInP	zajímat
motory	motor	k1gInPc1	motor
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
dvoudobého	dvoudobý	k2eAgInSc2d1	dvoudobý
motoru	motor	k1gInSc2	motor
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
zdokonalit	zdokonalit	k5eAaPmF	zdokonalit
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
konstrukci	konstrukce	k1gFnSc4	konstrukce
spalovacího	spalovací	k2eAgInSc2d1	spalovací
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1872	[number]	k4	1872
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Berthou	Bertha	k1gFnSc7	Bertha
Ringer	Ringra	k1gFnPc2	Ringra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
patent	patent	k1gInSc1	patent
obdržel	obdržet	k5eAaPmAgInS	obdržet
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgInS	založit
firmu	firma	k1gFnSc4	firma
Benz	Benza	k1gFnPc2	Benza
&	&	k?	&
Cie	Cie	k1gFnPc2	Cie
<g/>
.	.	kIx.	.
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
firma	firma	k1gFnSc1	firma
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
Benz	Benz	k1gMnSc1	Benz
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
začal	začít	k5eAaPmAgInS	začít
zabývat	zabývat	k5eAaImF	zabývat
myšlenkou	myšlenka	k1gFnSc7	myšlenka
vytvořit	vytvořit	k5eAaPmF	vytvořit
prakticky	prakticky	k6eAd1	prakticky
využitelný	využitelný	k2eAgInSc4d1	využitelný
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
již	již	k9	již
několikrát	několikrát	k6eAd1	několikrát
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
jiní	jiný	k2eAgMnPc1d1	jiný
konstruktéři	konstruktér	k1gMnPc1	konstruktér
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Benz	Benz	k1gMnSc1	Benz
tedy	tedy	k9	tedy
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
čtyřdobého	čtyřdobý	k2eAgInSc2d1	čtyřdobý
motoru	motor	k1gInSc2	motor
podle	podle	k7c2	podle
Ottova	Ottův	k2eAgInSc2d1	Ottův
patentu	patent	k1gInSc2	patent
(	(	kIx(	(
<g/>
958	[number]	k4	958
cm3	cm3	k4	cm3
<g/>
)	)	kIx)	)
a	a	k8xC	a
podvozkem	podvozek	k1gInSc7	podvozek
tříkolového	tříkolový	k2eAgInSc2d1	tříkolový
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Automobil	automobil	k1gInSc1	automobil
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
elektrické	elektrický	k2eAgNnSc4d1	elektrické
zapalování	zapalování	k1gNnSc4	zapalování
<g/>
,	,	kIx,	,
diferenciál	diferenciál	k1gInSc4	diferenciál
a	a	k8xC	a
vodní	vodní	k2eAgNnSc4d1	vodní
chlazení	chlazení	k1gNnSc4	chlazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
mohl	moct	k5eAaImAgInS	moct
Mannheimu	Mannheim	k1gInSc2	Mannheim
předvést	předvést	k5eAaPmF	předvést
svůj	svůj	k3xOyFgInSc4	svůj
automobil	automobil	k1gInSc4	automobil
<g/>
,	,	kIx,	,
na	na	k7c6	na
který	který	k3yIgMnSc1	který
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1886	[number]	k4	1886
dostal	dostat	k5eAaPmAgInS	dostat
patent	patent	k1gInSc1	patent
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
svého	svůj	k3xOyFgInSc2	svůj
automobilu	automobil	k1gInSc2	automobil
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Benz	Benza	k1gFnPc2	Benza
Velo	velo	k1gNnSc4	velo
<g/>
.	.	kIx.	.
</s>
<s>
Spolehlivosti	spolehlivost	k1gFnPc1	spolehlivost
jeho	jeho	k3xOp3gInSc2	jeho
vozu	vůz	k1gInSc2	vůz
lidé	člověk	k1gMnPc1	člověk
nevěřili	věřit	k5eNaImAgMnP	věřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Bertha	Bertha	k1gFnSc1	Bertha
Benzová	Benzová	k1gFnSc1	Benzová
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Benz	Benza	k1gFnPc2	Benza
Patent-Motorwagen	Patent-Motorwagen	k1gInSc4	Patent-Motorwagen
Nummer	Nummer	k1gInSc4	Nummer
3	[number]	k4	3
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
106	[number]	k4	106
kilometrů	kilometr	k1gInPc2	kilometr
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
Mannheimu	Mannheim	k1gInSc2	Mannheim
do	do	k7c2	do
Pforzheimu	Pforzheim	k1gInSc2	Pforzheim
u	u	k7c2	u
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
vozu	vůz	k1gInSc2	vůz
dokázala	dokázat	k5eAaPmAgFnS	dokázat
a	a	k8xC	a
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
tak	tak	k6eAd1	tak
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
Benz	Benz	k1gMnSc1	Benz
Velo	velo	k1gNnSc4	velo
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgInSc7	první
hromadně	hromadně	k6eAd1	hromadně
vyráběným	vyráběný	k2eAgInSc7d1	vyráběný
automobilem	automobil	k1gInSc7	automobil
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
firma	firma	k1gFnSc1	firma
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
čtyřkolové	čtyřkolový	k2eAgInPc4d1	čtyřkolový
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Benz	Benz	k1gInSc1	Benz
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
i	i	k9	i
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
aparáty	aparát	k1gInPc4	aparát
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
domácí	domácí	k2eAgInSc1d1	domácí
telefon	telefon	k1gInSc1	telefon
<g/>
,	,	kIx,	,
lis	lis	k1gInSc1	lis
na	na	k7c4	na
stlačování	stlačování	k1gNnSc4	stlačování
balíku	balík	k1gInSc2	balík
tabáku	tabák	k1gInSc2	tabák
apod.	apod.	kA	apod.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Medailí	medaile	k1gFnSc7	medaile
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Diesela	Diesel	k1gMnSc2	Diesel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
zásluhy	zásluha	k1gFnSc2	zásluha
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
Automobilové	automobilový	k2eAgFnSc2d1	automobilová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Benz	Benz	k1gMnSc1	Benz
<g/>
:	:	kIx,	:
Lebensfahrt	Lebensfahrt	k1gInSc1	Lebensfahrt
eines	einesa	k1gFnPc2	einesa
Erfinders	Erfindersa	k1gFnPc2	Erfindersa
<g/>
.	.	kIx.	.
</s>
<s>
Koehler	Koehler	k1gInSc1	Koehler
&	&	k?	&
Amelang	Amelang	k1gInSc1	Amelang
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7338	[number]	k4	7338
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
302	[number]	k4	302
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fabian	Fabian	k1gMnSc1	Fabian
Müller	Müller	k1gMnSc1	Müller
<g/>
:	:	kIx,	:
Daimler-Benz	Daimler-Benz	k1gMnSc1	Daimler-Benz
<g/>
.	.	kIx.	.
</s>
<s>
Ullstein	Ullstein	k1gInSc1	Ullstein
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
548	[number]	k4	548
<g/>
-	-	kIx~	-
<g/>
35946	[number]	k4	35946
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
H.	H.	kA	H.
Roth	Roth	k1gMnSc1	Roth
u.a.	u.a.	k?	u.a.
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Daimler-Benz-AG	Daimler-Benz-AG	k1gFnSc1	Daimler-Benz-AG
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Schlüsseldokumente	Schlüsseldokument	k1gMnSc5	Schlüsseldokument
zur	zur	k?	zur
Konzerngeschichte	Konzerngeschicht	k1gInSc5	Konzerngeschicht
<g/>
.	.	kIx.	.
</s>
<s>
Greno-Verlag	Greno-Verlag	k1gInSc1	Greno-Verlag
<g/>
,	,	kIx,	,
Nördlingen	Nördlingen	k1gInSc1	Nördlingen
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
89190	[number]	k4	89190
<g/>
-	-	kIx~	-
<g/>
955	[number]	k4	955
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schildtberger	Schildtberger	k1gMnSc1	Schildtberger
<g/>
:	:	kIx,	:
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gInSc1	Daimler
und	und	k?	und
Karl	Karl	k1gMnSc1	Karl
Benz	Benz	k1gMnSc1	Benz
<g/>
.	.	kIx.	.
</s>
<s>
Pioniere	Pionirat	k5eAaPmIp3nS	Pionirat
der	drát	k5eAaImRp2nS	drát
Automobilindustrie	Automobilindustrie	k1gFnSc5	Automobilindustrie
<g/>
.	.	kIx.	.
</s>
<s>
Verlag	Verlag	k1gMnSc1	Verlag
Musterschmidt	Musterschmidt	k1gMnSc1	Musterschmidt
<g/>
,	,	kIx,	,
Göttingen	Göttingen	k1gInSc1	Göttingen
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7881	[number]	k4	7881
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hans-Christoph	Hans-Christoph	k1gInSc1	Hans-Christoph
von	von	k1gInSc1	von
Seherr-Thoss	Seherr-Thoss	k1gInSc1	Seherr-Thoss
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gInSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Zwei	Zwei	k1gNnSc1	Zwei
Männer	Männra	k1gFnPc2	Männra
<g/>
,	,	kIx,	,
ein	ein	k?	ein
Stern	sternum	k1gNnPc2	sternum
<g/>
.	.	kIx.	.
</s>
<s>
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gMnSc1	Daimler
und	und	k?	und
Karl	Karl	k1gMnSc1	Karl
Benz	Benz	k1gMnSc1	Benz
in	in	k?	in
Bildern	Bildern	k1gInSc1	Bildern
<g/>
,	,	kIx,	,
Daten	Daten	k1gInSc1	Daten
<g/>
,	,	kIx,	,
Dokumenten	Dokumenten	k2eAgInSc1d1	Dokumenten
<g/>
.	.	kIx.	.
</s>
<s>
VDI-Verlag	VDI-Verlag	k1gInSc1	VDI-Verlag
<g/>
,	,	kIx,	,
Düsseldorf	Düsseldorf	k1gInSc1	Düsseldorf
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
400851	[number]	k4	400851
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Winfried	Winfried	k1gMnSc1	Winfried
A.	A.	kA	A.
Seidel	Seidel	k1gMnSc1	Seidel
<g/>
:	:	kIx,	:
Carl	Carl	k1gMnSc1	Carl
Benz	Benz	k1gMnSc1	Benz
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Einus	k1gMnSc5	Einus
badische	badischus	k1gMnSc5	badischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
.	.	kIx.	.
</s>
<s>
Edition	Edition	k1gInSc1	Edition
Diesbach	Diesbach	k1gInSc1	Diesbach
<g/>
,	,	kIx,	,
Weinheim	Weinheim	k1gInSc1	Weinheim
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
936468	[number]	k4	936468
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bertha	Bertha	k1gMnSc1	Bertha
Benz	Benz	k1gMnSc1	Benz
</s>
</p>
<p>
<s>
Benz	Benz	k1gMnSc1	Benz
&	&	k?	&
Cie	Cie	k1gMnSc1	Cie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
C.	C.	kA	C.
Benz	Benz	k1gMnSc1	Benz
Söhne	Söhn	k1gInSc5	Söhn
</s>
</p>
<p>
<s>
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Porsche	Porsche	k1gNnSc2	Porsche
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
Opel	opel	k1gInSc4	opel
</s>
</p>
<p>
<s>
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gMnSc1	Daimler
</s>
</p>
<p>
<s>
Siegfried	Siegfried	k1gMnSc1	Siegfried
Marcus	Marcus	k1gMnSc1	Marcus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karl	Karla	k1gFnPc2	Karla
Benz	Benza	k1gFnPc2	Benza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karl	Karl	k1gMnSc1	Karl
Benz	Benz	k1gMnSc1	Benz
</s>
</p>
<p>
<s>
Automuseum	Automuseum	k1gInSc1	Automuseum
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Carl	Carl	k1gMnSc1	Carl
Benz	Benz	k1gMnSc1	Benz
<g/>
,	,	kIx,	,
Ladenburg	Ladenburg	k1gMnSc1	Ladenburg
<g/>
/	/	kIx~	/
<g/>
Germany	German	k1gInPc1	German
</s>
</p>
<p>
<s>
Bertha	Bertha	k1gMnSc1	Bertha
Benz	Benz	k1gMnSc1	Benz
Memorial	Memorial	k1gMnSc1	Memorial
Route	Rout	k1gMnSc5	Rout
</s>
</p>
