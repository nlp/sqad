<s>
Karl	Karl	k1gMnSc1	Karl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Michael	Michael	k1gMnSc1	Michael
Benz	Benz	k1gMnSc1	Benz
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Carl	Carl	k1gMnSc1	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Benz	Benz	k1gMnSc1	Benz
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
Karlsruhe	Karlsruhe	k1gFnSc1	Karlsruhe
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Ladenburg	Ladenburg	k1gMnSc1	Ladenburg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
automobilový	automobilový	k2eAgMnSc1d1	automobilový
konstruktér	konstruktér	k1gMnSc1	konstruktér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
první	první	k4xOgInSc4	první
benzínový	benzínový	k2eAgInSc4d1	benzínový
automobil	automobil	k1gInSc4	automobil
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
Benz	Benz	k1gInSc1	Benz
Patent-Motorwagen	Patent-Motorwagen	k1gInSc1	Patent-Motorwagen
Nummer	Nummer	k1gInSc1	Nummer
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
