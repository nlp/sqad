<s>
ASCII	ascii	kA
</s>
<s>
Mapa	mapa	k1gFnSc1
všech	všecek	k3xTgFnPc2
128	#num#	k4
ASCII	ascii	kA
znaků	znak	k1gInPc2
</s>
<s>
ASCII	ascii	kA
je	být	k5eAaImIp3nS
anglická	anglický	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
American	American	k2eAgInSc4d1
Standard	standard	k2eAgInSc4d1
Code	Cod	k1gInSc4
for	forum	k7
Information	Information	k2eAgInSc4d1
Interchange	Interchang	k1gInPc4
(	(	kIx(
<g/>
„	„	kIx"
<g/>
americký	americký	k2eAgInSc4d1
standardní	standardní	k2eAgInSc4d1
kód	kód	k1gInSc4
pro	pro	k7c4
výměnu	výměna	k1gFnSc4
informací	informace	k1gFnPc2
<g/>
“	“	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
kódovou	kódový	k2eAgFnSc4d1
tabulku	tabulka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
definuje	definovat	k5eAaBmIp3nS
znaky	znak	k1gInPc4
anglické	anglický	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
znaky	znak	k1gInPc4
používané	používaný	k2eAgInPc4d1
v	v	k7c6
informatice	informatika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
historicky	historicky	k6eAd1
nejúspěšnější	úspěšný	k2eAgFnSc4d3
znakovou	znakový	k2eAgFnSc4d1
sadu	sada	k1gFnSc4
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
vychází	vycházet	k5eAaImIp3nS
většina	většina	k1gFnSc1
současných	současný	k2eAgInPc2d1
standardů	standard	k1gInPc2
pro	pro	k7c4
kódování	kódování	k1gNnSc4
textu	text	k1gInSc2
přinejmenším	přinejmenším	k6eAd1
v	v	k7c6
euro-americké	euro-americký	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
tisknutelné	tisknutelný	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
:	:	kIx,
písmena	písmeno	k1gNnPc4
<g/>
,	,	kIx,
číslice	číslice	k1gFnPc4
<g/>
,	,	kIx,
jiné	jiný	k2eAgInPc4d1
znaky	znak	k1gInPc4
(	(	kIx(
<g/>
závorky	závorka	k1gFnPc4
<g/>
,	,	kIx,
matematické	matematický	k2eAgInPc4d1
znaky	znak	k1gInPc4
(	(	kIx(
<g/>
+	+	kIx~
-	-	kIx~
*	*	kIx~
/	/	kIx~
%	%	kIx~
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
interpunkční	interpunkční	k2eAgNnPc1d1
znaménka	znaménko	k1gNnPc1
(	(	kIx(
<g/>
,	,	kIx,
.	.	kIx.
:	:	kIx,
;	;	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
speciální	speciální	k2eAgInPc4d1
znaky	znak	k1gInPc4
(	(	kIx(
<g/>
@	@	kIx~
$	$	kIx~
~	~	kIx~
atd.	atd.	kA
<g/>
))	))	k?
<g/>
,	,	kIx,
a	a	k8xC
řídicí	řídicí	k2eAgInPc4d1
(	(	kIx(
<g/>
netisknutelné	tisknutelný	k2eNgInPc4d1
<g/>
)	)	kIx)
kódy	kód	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
původně	původně	k6eAd1
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
řízení	řízení	k1gNnSc4
periferních	periferní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
tiskárny	tiskárna	k1gFnSc2
nebo	nebo	k8xC
dálnopisu	dálnopis	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kód	kód	k1gInSc1
ASCII	ascii	kA
je	být	k5eAaImIp3nS
podle	podle	k7c2
původní	původní	k2eAgFnSc2d1
definice	definice	k1gFnSc2
sedmibitový	sedmibitový	k2eAgInSc1d1
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
tedy	tedy	k9
128	#num#	k4
platných	platný	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
dalších	další	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
pro	pro	k7c4
rozšíření	rozšíření	k1gNnSc4
znakové	znakový	k2eAgFnSc2d1
sady	sada	k1gFnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
osmibitová	osmibitový	k2eAgNnPc1d1
rozšíření	rozšíření	k1gNnPc1
ASCII	ascii	kA
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
obsahují	obsahovat	k5eAaImIp3nP
dalších	další	k2eAgInPc2d1
128	#num#	k4
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
rozšířený	rozšířený	k2eAgInSc4d1
kód	kód	k1gInSc4
je	být	k5eAaImIp3nS
přesto	přesto	k8xC
příliš	příliš	k6eAd1
malý	malý	k2eAgInSc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pojal	pojmout	k5eAaPmAgMnS
třeba	třeba	k6eAd1
jen	jen	k9
evropské	evropský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
jednotlivých	jednotlivý	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
různé	různý	k2eAgFnPc1d1
kódové	kódový	k2eAgFnPc1d1
tabulky	tabulka	k1gFnPc1
<g/>
,	,	kIx,
význam	význam	k1gInSc1
kódů	kód	k1gInPc2
nad	nad	k7c7
127	#num#	k4
není	být	k5eNaImIp3nS
tedy	tedy	k9
jednoznačný	jednoznačný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
kódových	kódový	k2eAgFnPc2d1
tabulek	tabulka	k1gFnPc2
pro	pro	k7c4
národní	národní	k2eAgFnPc4d1
abecedy	abeceda	k1gFnPc4
vytvořila	vytvořit	k5eAaPmAgFnS
například	například	k6eAd1
organizace	organizace	k1gFnSc1
ISO	ISO	kA
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
ASCII	ascii	kA
kódů	kód	k1gInPc2
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
tabulce	tabulka	k1gFnSc6
(	(	kIx(
<g/>
kódy	kód	k1gInPc7
0	#num#	k4
<g/>
h	h	k?
až	až	k9
1	#num#	k4
<g/>
Fh	Fh	k1gFnPc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
řídicí	řídicí	k2eAgInPc4d1
kódy	kód	k1gInPc4
<g/>
,	,	kIx,
speciální	speciální	k2eAgInPc4d1
netisknutelné	tisknutelný	k2eNgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
sloužící	sloužící	k1gFnPc4
k	k	k7c3
řízení	řízení	k1gNnSc3
datového	datový	k2eAgInSc2d1
přenosu	přenos	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
formátování	formátování	k1gNnSc3
tisku	tisk	k1gInSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
k	k	k7c3
jiným	jiný	k2eAgInPc3d1
účelům	účel	k1gInPc3
(	(	kIx(
<g/>
např.	např.	kA
escape	escapat	k5eAaPmIp3nS
se	se	k3xPyFc4
používal	používat	k5eAaImAgMnS
pro	pro	k7c4
sekvence	sekvence	k1gFnPc4
sloužící	sloužící	k1gFnSc2
ke	k	k7c3
konfiguraci	konfigurace	k1gFnSc3
tiskárny	tiskárna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc3
<g/>
,	,	kIx,
třetí	třetí	k4xOgNnSc4
a	a	k8xC
čtvrté	čtvrtý	k4xOgNnSc4
tabulce	tabulka	k1gFnSc6
jsou	být	k5eAaImIp3nP
běžné	běžný	k2eAgInPc1d1
tisknutelné	tisknutelný	k2eAgInPc1d1
znaky	znak	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
znak	znak	k1gInSc4
mezera	mezera	k1gFnSc1
na	na	k7c6
začátku	začátek	k1gInSc6
druhé	druhý	k4xOgFnSc2
tabulky	tabulka	k1gFnSc2
(	(	kIx(
<g/>
decimální	decimální	k2eAgInSc4d1
kód	kód	k1gInSc4
32	#num#	k4
<g/>
,	,	kIx,
hexadecimální	hexadecimální	k2eAgFnSc1d1
20	#num#	k4
<g/>
h	h	k?
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
SP	SP	kA
<g/>
)	)	kIx)
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
počítán	počítat	k5eAaImNgInS
jak	jak	k6eAd1
k	k	k7c3
řídicím	řídicí	k2eAgFnPc3d1
<g/>
,	,	kIx,
tak	tak	k9
k	k	k7c3
tisknutelným	tisknutelný	k2eAgInPc3d1
znakům	znak	k1gInPc3
a	a	k8xC
řídicí	řídicí	k2eAgInSc1d1
znak	znak	k1gInSc1
DEL	DEL	kA
(	(	kIx(
<g/>
decimální	decimální	k2eAgInSc4d1
kód	kód	k1gInSc4
127	#num#	k4
<g/>
,	,	kIx,
hexadecimální	hexadecimální	k2eAgFnSc6d1
7	#num#	k4
<g/>
Fh	Fh	k1gFnSc6
<g/>
)	)	kIx)
na	na	k7c6
konci	konec	k1gInSc6
poslední	poslední	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tip	tip	k1gInSc1
<g/>
:	:	kIx,
Tyto	tento	k3xDgFnPc4
ASCII	ascii	kA
kódy	kód	k1gInPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
napsat	napsat	k5eAaPmF,k5eAaBmF
v	v	k7c6
české	český	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
DecHexZkratka	DecHexZkratka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Význam	význam	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
000NULNULL	000NULNULL	k4
character	character	k1gInSc1
</s>
<s>
101	#num#	k4
<g/>
SOHStart	SOHStart	k1gInSc1
of	of	k?
Header	Header	k1gInSc1
</s>
<s>
202	#num#	k4
<g/>
STXStart	STXStart	k1gInSc1
of	of	k?
Text	text	k1gInSc1
</s>
<s>
303	#num#	k4
<g/>
ETXEnd	ETXEnd	k1gInSc1
of	of	k?
Text	text	k1gInSc1
</s>
<s>
404	#num#	k4
<g/>
EOTEnd	EOTEnd	k1gInSc1
of	of	k?
Transmission	Transmission	k1gInSc1
</s>
<s>
505	#num#	k4
<g/>
ENQEnquiry	ENQEnquiro	k1gNnPc7
</s>
<s>
606	#num#	k4
<g/>
ACKAcknowledge	ACKAcknowledg	k1gInSc2
</s>
<s>
707	#num#	k4
<g/>
BELBell	BELBell	k1gInSc1
</s>
<s>
808	#num#	k4
<g/>
BSBackspace	BSBackspace	k1gFnSc1
</s>
<s>
909	#num#	k4
<g/>
HTHorizontal	HTHorizontal	k1gMnSc1
Tab	tab	kA
</s>
<s>
100	#num#	k4
<g/>
aLFLine	aLFLinout	k5eAaPmIp3nS
feed	feed	k6eAd1
</s>
<s>
110	#num#	k4
<g/>
bVTVertical	bVTVerticat	k5eAaPmAgMnS
Tab	tab	kA
</s>
<s>
120	#num#	k4
<g/>
cFFForm	cFFForm	k1gInSc1
Feed	Feed	k1gInSc4
</s>
<s>
130	#num#	k4
<g/>
dCRCarriage	dCRCarriage	k1gNnSc1
return	returna	k1gFnPc2
</s>
<s>
140	#num#	k4
<g/>
eSOShift	eSOShift	k1gMnSc1
Out	Out	k1gMnSc1
</s>
<s>
150	#num#	k4
<g/>
fSIShift	fSIShift	k1gMnSc1
In	In	k1gMnSc1
</s>
<s>
1610	#num#	k4
<g/>
DLEData	DLEData	k1gFnSc1
Link	Linka	k1gFnPc2
Escape	Escap	k1gMnSc5
</s>
<s>
1711	#num#	k4
<g/>
DC	DC	kA
<g/>
1	#num#	k4
<g/>
Device	device	k1gInSc2
Control	Control	k1gInSc1
(	(	kIx(
<g/>
XOn	XOn	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1812	#num#	k4
<g/>
DC	DC	kA
<g/>
2	#num#	k4
<g/>
Device	device	k1gInPc2
Control	Controla	k1gFnPc2
</s>
<s>
1913	#num#	k4
<g/>
DC	DC	kA
<g/>
3	#num#	k4
<g/>
Device	device	k1gInSc2
Control	Control	k1gInSc1
(	(	kIx(
<g/>
XOff	XOff	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
<g/>
DC	DC	kA
<g/>
4	#num#	k4
<g/>
Device	device	k1gInPc2
Control	Controla	k1gFnPc2
</s>
<s>
2115	#num#	k4
<g/>
NAKNegative	NAKNegativ	k1gInSc5
Acknowledge	Acknowledge	k1gFnPc6
</s>
<s>
2216	#num#	k4
<g/>
SYNSynchronous	SYNSynchronous	k1gInSc1
Idle	Idl	k1gFnSc2
</s>
<s>
2317	#num#	k4
<g/>
ETBEnd	ETBEnd	k1gInSc1
of	of	k?
Transmission	Transmission	k1gInSc1
Block	Block	k1gInSc1
</s>
<s>
2418	#num#	k4
<g/>
CANCancel	CANCancel	k1gFnSc1
</s>
<s>
2519	#num#	k4
<g/>
EMEnd	EMEnd	k1gMnSc1
of	of	k?
Medium	medium	k1gNnSc4
</s>
<s>
261	#num#	k4
<g/>
aSUBSubstitute	aSUBSubstitut	k1gMnSc5
</s>
<s>
271	#num#	k4
<g/>
bESCEscape	bESCEscapat	k5eAaPmIp3nS
</s>
<s>
281	#num#	k4
<g/>
cFSFile	cFSFile	k6eAd1
Separator	Separator	k1gMnSc1
</s>
<s>
291	#num#	k4
<g/>
dGSGroup	dGSGroup	k1gMnSc1
Separator	Separator	k1gMnSc1
</s>
<s>
301	#num#	k4
<g/>
eRSRecord	eRSRecord	k1gMnSc1
Separator	Separator	k1gMnSc1
</s>
<s>
311	#num#	k4
<g/>
fUSUnit	fUSUnit	k1gInSc1
Separator	Separator	k1gInSc4
</s>
<s>
DecHexZnak	DecHexZnak	k6eAd1
</s>
<s>
3220SP	3220SP	k4
(	(	kIx(
<g/>
mezera	mezera	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
3321	#num#	k4
<g/>
!	!	kIx.
</s>
<s>
3422	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
3523	#num#	k4
<g/>
#	#	kIx~
</s>
<s>
3624	#num#	k4
<g/>
$	$	kIx~
</s>
<s>
3725	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
3826	#num#	k4
<g/>
&	&	k?
</s>
<s>
3927	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
4028	#num#	k4
<g/>
(	(	kIx(
</s>
<s>
4129	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
422	#num#	k4
<g/>
a	a	k8xC
<g/>
*	*	kIx~
</s>
<s>
432	#num#	k4
<g/>
b	b	k?
<g/>
+	+	kIx~
</s>
<s>
442	#num#	k4
<g/>
c	c	k0
<g/>
,	,	kIx,
</s>
<s>
452	#num#	k4
<g/>
d-	d-	k?
</s>
<s>
462	#num#	k4
<g/>
e.	e.	k?
</s>
<s>
472	#num#	k4
<g/>
f	f	k?
<g/>
/	/	kIx~
</s>
<s>
48300	#num#	k4
</s>
<s>
49311	#num#	k4
</s>
<s>
50322	#num#	k4
</s>
<s>
51333	#num#	k4
</s>
<s>
52344	#num#	k4
</s>
<s>
53355	#num#	k4
</s>
<s>
54366	#num#	k4
</s>
<s>
55377	#num#	k4
</s>
<s>
56388	#num#	k4
</s>
<s>
57399	#num#	k4
</s>
<s>
583	#num#	k4
<g/>
a	a	k8xC
<g/>
:	:	kIx,
</s>
<s>
593	#num#	k4
<g/>
b	b	k?
<g/>
;	;	kIx,
</s>
<s>
603	#num#	k4
<g/>
c	c	k0
<g/>
<	<	kIx(
</s>
<s>
613	#num#	k4
<g/>
d	d	k?
<g/>
=	=	kIx~
</s>
<s>
623	#num#	k4
<g/>
e	e	k0
<g/>
>	>	kIx)
</s>
<s>
633	#num#	k4
<g/>
f	f	k?
<g/>
?	?	kIx.
</s>
<s>
DecHexZnak	DecHexZnak	k6eAd1
</s>
<s>
6440	#num#	k4
<g/>
@	@	kIx~
</s>
<s>
6541A	6541A	k4
</s>
<s>
6642B	6642B	k4
</s>
<s>
6743C	6743C	k4
</s>
<s>
6844D	6844D	k4
</s>
<s>
6945E	6945E	k4
</s>
<s>
7046F	7046F	k4
</s>
<s>
7147G	7147G	k4
</s>
<s>
7248H	7248H	k4
</s>
<s>
7349I	7349I	k4
</s>
<s>
744	#num#	k4
<g/>
aJ	aj	kA
</s>
<s>
754	#num#	k4
<g/>
bK	bK	k?
</s>
<s>
764	#num#	k4
<g/>
cL	cL	k?
</s>
<s>
774	#num#	k4
<g/>
dM	dm	kA
</s>
<s>
784	#num#	k4
<g/>
eN	eN	k?
</s>
<s>
794	#num#	k4
<g/>
fO	fO	k?
</s>
<s>
8050P	8050P	k4
</s>
<s>
8151Q	8151Q	k4
</s>
<s>
8252R	8252R	k4
</s>
<s>
8353S	8353S	k4
</s>
<s>
8454T	8454T	k4
</s>
<s>
8555U	8555U	k4
</s>
<s>
8656V	8656V	k4
</s>
<s>
8757W	8757W	k4
</s>
<s>
8858X	8858X	k4
</s>
<s>
8959Y	8959Y	k4
</s>
<s>
905	#num#	k4
<g/>
aZ	aZ	k?
</s>
<s>
915	#num#	k4
<g/>
b	b	k?
<g/>
[	[	kIx(
</s>
<s>
925	#num#	k4
<g/>
c	c	k0
<g/>
\	\	kIx~
</s>
<s>
935	#num#	k4
<g/>
d	d	k?
<g/>
]	]	kIx)
</s>
<s>
945	#num#	k4
<g/>
e	e	k0
<g/>
^	^	kIx~
</s>
<s>
955	#num#	k4
<g/>
f_	f_	k?
</s>
<s>
DecHexZnak	DecHexZnak	k6eAd1
</s>
<s>
9660	#num#	k4
<g/>
`	`	kIx"
</s>
<s>
9761	#num#	k4
<g/>
a	a	k8xC
</s>
<s>
9862	#num#	k4
<g/>
b	b	k?
</s>
<s>
9963	#num#	k4
<g/>
c	c	k0
</s>
<s>
10064	#num#	k4
<g/>
d	d	k?
</s>
<s>
10165	#num#	k4
<g/>
e	e	k0
</s>
<s>
10266	#num#	k4
<g/>
f	f	k?
</s>
<s>
10367	#num#	k4
<g/>
g	g	kA
</s>
<s>
10468	#num#	k4
<g/>
h	h	k?
</s>
<s>
10569	#num#	k4
<g/>
i	i	k8xC
</s>
<s>
1066	#num#	k4
<g/>
aj	aj	kA
</s>
<s>
1076	#num#	k4
<g/>
bk	bk	k?
</s>
<s>
1086	#num#	k4
<g/>
cl	cl	k?
</s>
<s>
1096	#num#	k4
<g/>
dm	dm	kA
</s>
<s>
1106	#num#	k4
<g/>
en	en	k?
</s>
<s>
1116	#num#	k4
<g/>
fo	fo	k?
</s>
<s>
11270	#num#	k4
<g/>
p	p	k?
</s>
<s>
11371	#num#	k4
<g/>
q	q	k?
</s>
<s>
11472	#num#	k4
<g/>
r	r	kA
</s>
<s>
11573	#num#	k4
<g/>
s	s	k7c7
</s>
<s>
11674	#num#	k4
<g/>
t	t	k?
</s>
<s>
11775	#num#	k4
<g/>
u	u	k7c2
</s>
<s>
11876	#num#	k4
<g/>
v	v	k7c6
</s>
<s>
11977	#num#	k4
<g/>
w	w	k?
</s>
<s>
12078	#num#	k4
<g/>
x	x	k?
</s>
<s>
12179	#num#	k4
<g/>
y	y	k?
</s>
<s>
1227	#num#	k4
<g/>
az	az	k?
</s>
<s>
1237	#num#	k4
<g/>
b	b	k?
<g/>
{	{	kIx(
</s>
<s>
1247	#num#	k4
<g/>
c	c	k0
<g/>
|	|	kIx~
</s>
<s>
1257	#num#	k4
<g/>
d	d	k?
<g/>
}	}	kIx)
</s>
<s>
1267	#num#	k4
<g/>
e	e	k0
<g/>
~	~	kIx~
</s>
<s>
1277	#num#	k4
<g/>
fDEL	fDEL	k?
(	(	kIx(
<g/>
delete	delat	k5eAaImIp2nP,k5eAaBmIp2nP,k5eAaPmIp2nP
<g/>
)	)	kIx)
</s>
<s>
Popis	popis	k1gInSc1
speciálních	speciální	k2eAgInPc2d1
a	a	k8xC
řídicích	řídicí	k2eAgInPc2d1
znaků	znak	k1gInPc2
</s>
<s>
Tyto	tento	k3xDgInPc1
neviditelné	viditelný	k2eNgInPc1d1
znaky	znak	k1gInPc1
byly	být	k5eAaImAgInP
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
řízení	řízení	k1gNnSc4
dálnopisu	dálnopis	k1gInSc2
nebo	nebo	k8xC
tiskárny	tiskárna	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
jen	jen	k9
poměrně	poměrně	k6eAd1
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
používané	používaný	k2eAgInPc1d1
speciální	speciální	k2eAgInPc1d1
znaky	znak	k1gInPc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
SPC	SPC	kA
–	–	k?
space	space	k1gFnSc1
<g/>
,	,	kIx,
mezera	mezera	k1gFnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
prázdný	prázdný	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
“	“	k?
</s>
<s>
HT	HT	kA
–	–	k?
Horizontal	Horizontal	k1gMnSc1
Tab	tab	kA
–	–	k?
tabulátor	tabulátor	k1gInSc1
</s>
<s>
LF	LF	kA
–	–	k?
Line	linout	k5eAaImIp3nS
Feed	Feed	k1gInSc4
–	–	k?
odřádkování	odřádkování	k1gNnSc2
</s>
<s>
CR	cr	k0
–	–	k?
Carriage	Carriage	k1gNnPc2
Return	Return	k1gInSc1
–	–	k?
návrat	návrat	k1gInSc4
vozíku	vozík	k1gInSc2
</s>
<s>
Ani	ani	k8xC
pro	pro	k7c4
používání	používání	k1gNnSc4
těchto	tento	k3xDgInPc2
kódů	kód	k1gInPc2
neexistuje	existovat	k5eNaImIp3nS
všeobecně	všeobecně	k6eAd1
přijímaný	přijímaný	k2eAgInSc1d1
standard	standard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
unixové	unixový	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
včetně	včetně	k7c2
Linux	Linux	kA
a	a	k8xC
Apple	Apple	kA
Mac	Mac	kA
OS	osa	k1gFnPc2
X	X	kA
používají	používat	k5eAaImIp3nP
pro	pro	k7c4
odřádkování	odřádkování	k1gNnSc4
kód	kód	k1gInSc4
LF	LF	kA
<g/>
,	,	kIx,
systémy	systém	k1gInPc7
DOS	DOS	kA
a	a	k8xC
Windows	Windows	kA
používají	používat	k5eAaImIp3nP
kombinaci	kombinace	k1gFnSc3
CR	cr	k0
<g/>
+	+	kIx~
<g/>
LF	LF	kA
<g/>
,	,	kIx,
Apple	Apple	kA
systémy	systém	k1gInPc1
z	z	k7c2
období	období	k1gNnSc2
před	před	k7c7
Mac	Mac	kA
OS	OS	kA
X	X	kA
používají	používat	k5eAaImIp3nP
kód	kód	k1gInSc1
CR	cr	k0
<g/>
.	.	kIx.
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Nový	nový	k2eAgInSc1d1
řádek	řádek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
speciální	speciální	k2eAgInPc1d1
znaky	znak	k1gInPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
například	například	k6eAd1
pro	pro	k7c4
definici	definice	k1gFnSc4
komunikačních	komunikační	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
při	při	k7c6
komunikaci	komunikace	k1gFnSc6
mezi	mezi	k7c7
počítači	počítač	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
význam	význam	k1gInSc4
speciálních	speciální	k2eAgInPc2d1
znaků	znak	k1gInPc2
podle	podle	k7c2
původního	původní	k2eAgInSc2d1
standardu	standard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Fyzické	fyzický	k2eAgNnSc1d1
ovládání	ovládání	k1gNnSc1
zařízení	zařízení	k1gNnSc2
</s>
<s>
BS	BS	kA
<g/>
:	:	kIx,
Backspace	Backspace	k1gFnSc1
(	(	kIx(
<g/>
návrat	návrat	k1gInSc1
o	o	k7c6
1	#num#	k4
znak	znak	k1gInSc1
zpět	zpět	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HT	HT	kA
<g/>
:	:	kIx,
Horizontal	Horizontal	k1gMnSc1
Tab	tab	kA
(	(	kIx(
<g/>
tabulátor	tabulátor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
LF	LF	kA
<g/>
:	:	kIx,
Line	linout	k5eAaImIp3nS
Feed	Feed	k1gMnSc1
(	(	kIx(
<g/>
posun	posun	k1gInSc1
o	o	k7c4
1	#num#	k4
řádek	řádka	k1gFnPc2
dolů	dolů	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
VT	VT	kA
<g/>
:	:	kIx,
Vertical	Vertical	k1gMnSc1
Tab	tab	kA
(	(	kIx(
<g/>
vertikální	vertikální	k2eAgInSc4d1
tabulátor	tabulátor	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FF	ff	kA
<g/>
:	:	kIx,
Form	Form	k1gMnSc1
Feed	Feed	k1gMnSc1
(	(	kIx(
<g/>
posun	posun	k1gInSc1
na	na	k7c4
další	další	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
CR	cr	k0
<g/>
:	:	kIx,
Carriage	Carriag	k1gInPc4
Return	Return	k1gInSc1
(	(	kIx(
<g/>
návrat	návrat	k1gInSc1
tiskové	tiskový	k2eAgFnSc2d1
hlavičky	hlavička	k1gFnSc2
na	na	k7c4
začátek	začátek	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Fyzické	fyzický	k2eAgNnSc1d1
ovládání	ovládání	k1gNnSc1
zařízení	zařízení	k1gNnSc2
<g/>
:	:	kIx,
ostatní	ostatní	k2eAgFnPc1d1
</s>
<s>
BEL	bel	k1gInSc1
<g/>
:	:	kIx,
Bell	bell	k1gInSc1
–	–	k?
zvonek	zvonek	k1gInSc4
</s>
<s>
DC	DC	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
DC	DC	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
DC	DC	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
DC	DC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
Device	device	k1gInSc2
Controls	Controls	k1gInSc1
–	–	k?
DC1	DC1	k1gFnPc1
a	a	k8xC
DC3	DC3	k1gFnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
jako	jako	k9
XON	XON	kA
and	and	k?
XOFF	XOFF	kA
v	v	k7c6
softwarovém	softwarový	k2eAgInSc6d1
handshakingu	handshaking	k1gInSc6
</s>
<s>
Logické	logický	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
komunikace	komunikace	k1gFnSc2
</s>
<s>
SOH	SOH	kA
<g/>
:	:	kIx,
Start	start	k1gInSc1
of	of	k?
Header	Header	k1gInSc1
–	–	k?
začátek	začátek	k1gInSc4
hlavičky	hlavička	k1gFnSc2
</s>
<s>
STX	STX	kA
<g/>
:	:	kIx,
Start	start	k1gInSc1
of	of	k?
Text	text	k1gInSc1
–	–	k?
začátek	začátek	k1gInSc4
textu	text	k1gInSc2
</s>
<s>
ETX	ETX	kA
<g/>
:	:	kIx,
End	End	k1gMnSc1
of	of	k?
Text	text	k1gInSc1
–	–	k?
konec	konec	k1gInSc1
textu	text	k1gInSc2
</s>
<s>
EOT	EOT	kA
<g/>
:	:	kIx,
End	End	k1gMnSc1
of	of	k?
Transmission	Transmission	k1gInSc1
–	–	k?
konec	konec	k1gInSc1
vysílání	vysílání	k1gNnSc2
</s>
<s>
ENQ	ENQ	kA
<g/>
:	:	kIx,
Enquiry	Enquir	k1gInPc1
–	–	k?
dotaz	dotaz	k1gInSc4
(	(	kIx(
<g/>
žádost	žádost	k1gFnSc4
o	o	k7c4
komunikaci	komunikace	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
ACK	ACK	kA
<g/>
:	:	kIx,
Acknowledge	Acknowledge	k1gFnPc2
–	–	k?
potvrzení	potvrzení	k1gNnSc4
(	(	kIx(
<g/>
připravenosti	připravenost	k1gFnSc2
ke	k	k7c3
komunikaci	komunikace	k1gFnSc3
<g/>
)	)	kIx)
</s>
<s>
DLE	dle	k7c2
<g/>
:	:	kIx,
Data	datum	k1gNnSc2
Link	Linko	k1gNnPc2
Escape	Escap	k1gInSc5
–	–	k?
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
kódování	kódování	k1gNnSc4
speciálních	speciální	k2eAgInPc2d1
znaků	znak	k1gInPc2
</s>
<s>
NAK	NAK	kA
<g/>
:	:	kIx,
Negative	negativ	k1gInSc5
Acknowledge	Acknowledge	k1gFnSc3
–	–	k?
zamítnutí	zamítnutí	k1gNnSc4
(	(	kIx(
<g/>
žádosti	žádost	k1gFnSc2
o	o	k7c4
komunikaci	komunikace	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
SYN	syn	k1gMnSc1
<g/>
:	:	kIx,
Synchronous	Synchronous	k1gMnSc1
Idle	Idl	k1gFnSc2
</s>
<s>
ETB	ETB	kA
<g/>
:	:	kIx,
End	End	k1gMnSc1
of	of	k?
Transmission	Transmission	k1gInSc1
Block	Block	k1gInSc1
–	–	k?
konec	konec	k1gInSc4
přenosového	přenosový	k2eAgInSc2d1
bloku	blok	k1gInSc2
</s>
<s>
Fyzické	fyzický	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
komunikace	komunikace	k1gFnSc2
</s>
<s>
NUL	nula	k1gFnPc2
<g/>
:	:	kIx,
Null	Null	k1gInSc1
–	–	k?
„	„	k?
<g/>
nic	nic	k6eAd1
<g/>
“	“	k?
</s>
<s>
DEL	DEL	kA
<g/>
:	:	kIx,
Delete	Dele	k1gNnSc2
–	–	k?
smazání	smazání	k1gNnSc1
</s>
<s>
CAN	CAN	kA
<g/>
:	:	kIx,
Cancel	Cancel	k1gMnSc1
–	–	k?
zrušení	zrušení	k1gNnSc4
</s>
<s>
EM	Ema	k1gFnPc2
<g/>
:	:	kIx,
End	End	k1gFnPc2
of	of	k?
Medium	medium	k1gNnSc4
–	–	k?
konec	konec	k1gInSc1
média	médium	k1gNnSc2
</s>
<s>
SUB	sub	k1gNnSc1
<g/>
:	:	kIx,
Substitute	substitut	k1gMnSc5
–	–	k?
substituce	substituce	k1gFnSc1
</s>
<s>
Oddělovače	oddělovač	k1gInPc1
informací	informace	k1gFnPc2
</s>
<s>
FS	FS	kA
<g/>
:	:	kIx,
File	Fil	k1gMnSc2
Separator	Separator	k1gInSc1
–	–	k?
oddělovač	oddělovač	k1gInSc1
souboru	soubor	k1gInSc2
</s>
<s>
GS	GS	kA
<g/>
:	:	kIx,
Group	Group	k1gMnSc1
Separator	Separator	k1gMnSc1
–	–	k?
oddělovač	oddělovač	k1gInSc1
skupiny	skupina	k1gFnSc2
</s>
<s>
RS	RS	kA
<g/>
:	:	kIx,
Record	Record	k1gMnSc1
Separator	Separator	k1gMnSc1
–	–	k?
oddělovač	oddělovač	k1gInSc1
záznamu	záznam	k1gInSc2
</s>
<s>
US	US	kA
<g/>
:	:	kIx,
Unit	Unit	k1gMnSc1
Separator	Separator	k1gMnSc1
–	–	k?
oddělovač	oddělovač	k1gInSc1
jednotek	jednotka	k1gFnPc2
</s>
<s>
Rozšiřování	rozšiřování	k1gNnSc1
kódu	kód	k1gInSc2
</s>
<s>
SI	si	k1gNnSc1
<g/>
:	:	kIx,
Shift	Shift	kA
In	In	k1gMnSc1
</s>
<s>
SO	So	kA
<g/>
:	:	kIx,
Shift	Shift	kA
Out	Out	k1gMnSc1
</s>
<s>
ESC	ESC	kA
<g/>
:	:	kIx,
Escape	Escap	k1gInSc5
</s>
<s>
Odvozená	odvozený	k2eAgNnPc1d1
kódování	kódování	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
znakové	znakový	k2eAgFnSc2d1
sady	sada	k1gFnSc2
ASCII	ascii	kA
z	z	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
odlišovala	odlišovat	k5eAaImAgFnS
od	od	k7c2
verze	verze	k1gFnSc2
publikované	publikovaný	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
–	–	k?
neobsahovala	obsahovat	k5eNaImAgFnS
malá	malý	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc4
a	a	k8xC
některé	některý	k3yIgInPc1
řídicí	řídicí	k2eAgInPc1d1
znaky	znak	k1gInPc1
měly	mít	k5eAaImAgInP
jiné	jiný	k2eAgInPc4d1
významy	význam	k1gInPc4
nebo	nebo	k8xC
kódy	kód	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
ASCII	ascii	kA
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
základem	základ	k1gInSc7
většiny	většina	k1gFnSc2
kódování	kódování	k1gNnSc2
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
význačnější	význačný	k2eAgFnSc7d2
výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
kódování	kódování	k1gNnSc1
EBCDIC	EBCDIC	kA
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vyvinula	vyvinout	k5eAaPmAgFnS
firma	firma	k1gFnSc1
IBM	IBM	kA
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
sálové	sálový	k2eAgInPc4d1
počítače	počítač	k1gInPc4
přibližně	přibližně	k6eAd1
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vznikal	vznikat	k5eAaImAgInS
kód	kód	k1gInSc1
ASCII	ascii	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představuje	představovat	k5eAaImIp3nS
rozumný	rozumný	k2eAgInSc4d1
kompromis	kompromis	k1gInSc4
mezi	mezi	k7c7
minimalistickými	minimalistický	k2eAgFnPc7d1
sadami	sada	k1gFnPc7
obsahujícími	obsahující	k2eAgFnPc7d1
pouze	pouze	k6eAd1
písmena	písmeno	k1gNnSc2
základní	základní	k2eAgFnSc2d1
latinské	latinský	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
,	,	kIx,
číslice	číslice	k1gFnPc4
a	a	k8xC
několik	několik	k4yIc4
málo	málo	k6eAd1
interpunkčních	interpunkční	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
a	a	k8xC
bohatými	bohatý	k2eAgFnPc7d1
znakovými	znakový	k2eAgFnPc7d1
sadami	sada	k1gFnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
sada	sada	k1gFnSc1
symbolů	symbol	k1gInPc2
používaných	používaný	k2eAgInPc2d1
programovacím	programovací	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
APL	APL	kA
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
směrem	směr	k1gInSc7
úprav	úprava	k1gFnPc2
ASCII	ascii	kA
bylo	být	k5eAaImAgNnS
vytváření	vytváření	k1gNnSc1
sedmibitových	sedmibitový	k2eAgFnPc2d1
národních	národní	k2eAgFnPc2d1
sad	sada	k1gFnPc2
popsané	popsaný	k2eAgFnPc4d1
v	v	k7c6
normách	norma	k1gFnPc6
CCITT	CCITT	kA
V	v	k7c6
<g/>
.3	.3	k4
(	(	kIx(
<g/>
později	pozdě	k6eAd2
CCITT	CCITT	kA
nebo	nebo	k8xC
ITU-T	ITU-T	k1gFnSc1
T	T	kA
<g/>
.50	.50	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
646	#num#	k4
a	a	k8xC
ECMA-	ECMA-	k1gMnSc1
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
sady	sada	k1gFnPc1
také	také	k9
zaváděly	zavádět	k5eAaImAgFnP
mezinárodní	mezinárodní	k2eAgFnSc4d1
abecedu	abeceda	k1gFnSc4
jako	jako	k8xC,k8xS
mírně	mírně	k6eAd1
upravenou	upravený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
verzi	verze	k1gFnSc3
amerického	americký	k2eAgNnSc2d1
ASCII	ascii	kA
kódování	kódování	k1gNnSc2
(	(	kIx(
<g/>
nahrazením	nahrazení	k1gNnSc7
znaku	znak	k1gInSc2
dolar	dolar	k1gInSc1
znakem	znak	k1gInSc7
měnové	měnový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
a	a	k8xC
znaku	znak	k1gInSc2
tilda	tilda	k1gFnSc1
nadtržítkem	nadtržítek	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čeština	čeština	k1gFnSc1
používá	používat	k5eAaImIp3nS
tolik	tolik	k4xDc4,k4yIc4
znaků	znak	k1gInPc2
s	s	k7c7
diakritikou	diakritika	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
vytvořit	vytvořit	k5eAaPmF
sedmibitovou	sedmibitový	k2eAgFnSc4d1
znakovou	znakový	k2eAgFnSc4d1
sadu	sada	k1gFnSc4
podle	podle	k7c2
ITU-T	ITU-T	k1gFnSc2
T	T	kA
<g/>
.50	.50	k4
nebo	nebo	k8xC
ISO-	ISO-	k1gMnSc1
<g/>
646	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
výstup	výstup	k1gInSc4
na	na	k7c4
tiskárny	tiskárna	k1gFnPc4
však	však	k9
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
jiný	jiný	k2eAgInSc1d1
postup	postup	k1gInSc1
popsaný	popsaný	k2eAgInSc1d1
v	v	k7c6
těchto	tento	k3xDgFnPc6
normách	norma	k1gFnPc6
–	–	k?
přetisk	přetisk	k1gInSc1
písmene	písmeno	k1gNnSc2
jiným	jiný	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
diakritické	diakritický	k2eAgNnSc4d1
znaménko	znaménko	k1gNnSc4
<g/>
;	;	kIx,
znak	znak	k1gInSc1
apostrof	apostrof	k1gInSc1
je	být	k5eAaImIp3nS
použitelný	použitelný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
čárka	čárka	k1gFnSc1
<g/>
,	,	kIx,
znak	znak	k1gInSc1
nadtržítko	nadtržítko	k1gNnSc1
(	(	kIx(
<g/>
případně	případně	k6eAd1
vlnovka	vlnovka	k1gFnSc1
<g/>
)	)	kIx)
lze	lze	k6eAd1
nouzově	nouzově	k6eAd1
použít	použít	k5eAaPmF
místo	místo	k1gNnSc4
háčku	háček	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Okolo	okolo	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
se	se	k3xPyFc4
začínají	začínat	k5eAaImIp3nP
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
používat	používat	k5eAaImF
a	a	k8xC
definovat	definovat	k5eAaBmF
osmibitové	osmibitový	k2eAgFnSc2d1
znakové	znakový	k2eAgFnSc2d1
sady	sada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
sady	sada	k1gFnPc1
mají	mít	k5eAaImIp3nP
mnoho	mnoho	k4c4
výhod	výhoda	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yIgFnPc3,k3yQgFnPc3,k3yRgFnPc3
se	se	k3xPyFc4
používaly	používat	k5eAaImAgInP
přibližně	přibližně	k6eAd1
tři	tři	k4xCgNnPc4
desetiletí	desetiletí	k1gNnPc4
–	–	k?
umožňují	umožňovat	k5eAaImIp3nP
vytvořit	vytvořit	k5eAaPmF
funkční	funkční	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
pro	pro	k7c4
jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
několik	několik	k4yIc4
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
nemají	mít	k5eNaImIp3nP
velké	velký	k2eAgInPc1d1
nároky	nárok	k1gInPc1
na	na	k7c4
systémové	systémový	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
,	,	kIx,
identita	identita	k1gFnSc1
jeden	jeden	k4xCgInSc1
oktet	oktet	k1gInSc1
=	=	kIx~
jeden	jeden	k4xCgInSc1
znak	znak	k1gInSc1
=	=	kIx~
jedna	jeden	k4xCgFnSc1
tisková	tiskový	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
zjednodušuje	zjednodušovat	k5eAaImIp3nS
programování	programování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Problémem	problém	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
mezinárodní	mezinárodní	k2eAgFnPc1d1
normy	norma	k1gFnPc1
pro	pro	k7c4
osmibitové	osmibitový	k2eAgFnPc4d1
sady	sada	k1gFnPc4
vznikly	vzniknout	k5eAaPmAgFnP
poměrně	poměrně	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
mnoho	mnoho	k4c1
dodavatelů	dodavatel	k1gMnPc2
software	software	k1gInSc4
a	a	k8xC
hardware	hardware	k1gInSc4
vytvořilo	vytvořit	k5eAaPmAgNnS
svoje	svůj	k3xOyFgFnPc4
vlastní	vlastní	k2eAgFnPc4d1
sady	sada	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
často	často	k6eAd1
dosáhly	dosáhnout	k5eAaPmAgFnP
většího	veliký	k2eAgNnSc2d2
rozšíření	rozšíření	k1gNnSc2
než	než	k8xS
mezinárodní	mezinárodní	k2eAgFnSc2d1
normy	norma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existence	existence	k1gFnSc1
nejméně	málo	k6eAd3
šesti	šest	k4xCc2
kódování	kódování	k1gNnPc2
češtiny	čeština	k1gFnSc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
problémům	problém	k1gInPc3
při	při	k7c6
komunikaci	komunikace	k1gFnSc6
po	po	k7c6
síti	síť	k1gFnSc6
a	a	k8xC
výměně	výměna	k1gFnSc6
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
nevýhodám	nevýhoda	k1gFnPc3
patří	patřit	k5eAaImIp3nP
nemožnost	nemožnost	k1gFnSc4
používání	používání	k1gNnSc2
jednoho	jeden	k4xCgNnSc2
kódování	kódování	k1gNnSc2
pro	pro	k7c4
texty	text	k1gInPc4
ve	v	k7c6
více	hodně	k6eAd2
jazycích	jazyk	k1gInPc6
a	a	k8xC
špatná	špatný	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
asijských	asijský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaha	snaha	k1gFnSc1
odstranit	odstranit	k5eAaPmF
tyto	tento	k3xDgFnPc4
nevýhody	nevýhoda	k1gFnPc4
vedla	vést	k5eAaImAgFnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
jednotného	jednotný	k2eAgInSc2d1
standardu	standard	k1gInSc2
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
10646	#num#	k4
–	–	k?
Unicode	Unicod	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
osmibitových	osmibitový	k2eAgNnPc2d1
kódování	kódování	k1gNnPc2
používala	používat	k5eAaImAgFnS
pro	pro	k7c4
prvních	první	k4xOgInPc2
128	#num#	k4
kódů	kód	k1gInPc2
kód	kód	k1gInSc4
ASCII	ascii	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
jeho	jeho	k3xOp3gFnSc4
mezinárodní	mezinárodní	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšíření	rozšíření	k1gNnSc1
osmibitových	osmibitový	k2eAgNnPc2d1
kódování	kódování	k1gNnPc2
a	a	k8xC
zahájením	zahájení	k1gNnSc7
práce	práce	k1gFnSc2
na	na	k7c6
univerzální	univerzální	k2eAgFnSc6d1
znakové	znakový	k2eAgFnSc6d1
sadě	sada	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
nových	nový	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
mezinárodních	mezinárodní	k2eAgInPc2d1
standardů	standard	k1gInPc2
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
převzata	převzat	k2eAgFnSc1d1
sada	sada	k1gFnSc1
ASCII	ascii	kA
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgFnPc2
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
kódování	kódování	k1gNnPc2
vzniklých	vzniklý	k2eAgNnPc2d1
po	po	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
tak	tak	k6eAd1
zahrnuje	zahrnovat	k5eAaImIp3nS
ASCII	ascii	kA
kód	kód	k1gInSc4
nebo	nebo	k8xC
jeho	jeho	k3xOp3gNnSc7
nějakou	nějaký	k3yIgFnSc4
modifikaci	modifikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
časté	častý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
rozšíření	rozšíření	k1gNnSc1
kódování	kódování	k1gNnSc2
o	o	k7c4
semigrafické	semigrafický	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
překrývají	překrývat	k5eAaImIp3nP
se	s	k7c7
znaky	znak	k1gInPc7
řídicími	řídicí	k2eAgInPc7d1
<g/>
,	,	kIx,
systém	systém	k1gInSc1
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
poskytovat	poskytovat	k5eAaImF
dvě	dva	k4xCgFnPc1
metody	metoda	k1gFnPc1
výpisu	výpis	k1gInSc2
na	na	k7c4
obrazovku	obrazovka	k1gFnSc4
<g/>
;	;	kIx,
jedna	jeden	k4xCgFnSc1
vypisuje	vypisovat	k5eAaImIp3nS
semigrafické	semigrafický	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
interpretuje	interpretovat	k5eAaBmIp3nS
tytéž	týž	k3xTgInPc4
znaky	znak	k1gInPc4
jako	jako	k8xS,k8xC
řídicí	řídicí	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
kódování	kódování	k1gNnSc2
z	z	k7c2
ASCII	ascii	kA
pouze	pouze	k6eAd1
volně	volně	k6eAd1
vycházejí	vycházet	k5eAaImIp3nP
–	–	k?
mnoho	mnoho	k4c4
kódování	kódování	k1gNnPc2
nedodržuje	dodržovat	k5eNaImIp3nS
rozdělení	rozdělení	k1gNnSc1
na	na	k7c4
oblast	oblast	k1gFnSc4
řídicích	řídicí	k2eAgInPc2d1
a	a	k8xC
tisknutelných	tisknutelný	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
některá	některý	k3yIgNnPc1
zachovávají	zachovávat	k5eAaImIp3nP
pouze	pouze	k6eAd1
kódy	kód	k1gInPc4
písmen	písmeno	k1gNnPc2
a	a	k8xC
číslic	číslice	k1gFnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
některých	některý	k3yIgInPc2
speciálních	speciální	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
takových	takový	k3xDgNnPc2
kódování	kódování	k1gNnPc2
je	být	k5eAaImIp3nS
kódování	kódování	k1gNnSc1
Cork	Corka	k1gFnPc2
evropských	evropský	k2eAgInPc2d1
fontů	font	k1gInPc2
pro	pro	k7c4
sázecí	sázecí	k2eAgInSc4d1
program	program	k1gInSc4
TeX	TeX	k1gFnPc2
nebo	nebo	k8xC
sedmibitová	sedmibitový	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
pro	pro	k7c4
GSM	GSM	kA
definovaná	definovaný	k2eAgFnSc1d1
v	v	k7c6
GSM	GSM	kA
0	#num#	k4
<g/>
3.38	3.38	k4
<g/>
.	.	kIx.
</s>
<s>
Escape	Escapat	k5eAaPmIp3nS
sekvence	sekvence	k1gFnSc1
</s>
<s>
Znak	znak	k1gInSc1
ESC	ESC	kA
(	(	kIx(
<g/>
escape	escapat	k5eAaPmIp3nS
<g/>
)	)	kIx)
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
např.	např.	kA
pro	pro	k7c4
definici	definice	k1gFnSc4
tzv.	tzv.	kA
escape	escapat	k5eAaPmIp3nS
sekvencí	sekvence	k1gFnSc7
používaných	používaný	k2eAgNnPc2d1
pro	pro	k7c4
rozšíření	rozšíření	k1gNnSc4
ASCII	ascii	kA
kódu	kód	k1gInSc3
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
několik	několik	k4yIc4
znaků	znak	k1gInPc2
následujících	následující	k2eAgInPc2d1
znak	znak	k1gInSc4
ESC	ESC	kA
nejsou	být	k5eNaImIp3nP
interpretovány	interpretován	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
ASCII	ascii	kA
kódy	kód	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
speciální	speciální	k2eAgInSc4d1
význam	význam	k1gInSc4
–	–	k?
například	například	k6eAd1
mohou	moct	k5eAaImIp3nP
definovat	definovat	k5eAaBmF
novou	nový	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
kurzoru	kurzor	k1gInSc2
na	na	k7c6
obrazovce	obrazovka	k1gFnSc6
terminálu	terminál	k1gInSc2
nebo	nebo	k8xC
mohou	moct	k5eAaImIp3nP
definovat	definovat	k5eAaBmF
velikost	velikost	k1gFnSc4
fontu	font	k1gInSc2
používaného	používaný	k2eAgInSc2d1
tiskárnou	tiskárna	k1gFnSc7
<g/>
,	,	kIx,
přepnout	přepnout	k5eAaPmF
tiskárnu	tiskárna	k1gFnSc4
ze	z	k7c2
znakového	znakový	k2eAgMnSc2d1
do	do	k7c2
grafického	grafický	k2eAgInSc2d1
módu	mód	k1gInSc2
atd.	atd.	kA
</s>
<s>
Organizace	organizace	k1gFnSc1
ANSI	ANSI	kA
definovala	definovat	k5eAaBmAgFnS
sekvence	sekvence	k1gFnSc1
určené	určený	k2eAgFnSc2d1
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
znakových	znakový	k2eAgInPc2d1
terminálů	terminál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
sekvence	sekvence	k1gFnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
např.	např.	kA
posun	posun	k1gInSc4
kurzoru	kurzor	k1gInSc2
na	na	k7c4
určitý	určitý	k2eAgInSc4d1
řádek	řádek	k1gInSc4
a	a	k8xC
sloupec	sloupec	k1gInSc4
obrazovky	obrazovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Faktickým	faktický	k2eAgInSc7d1
standardem	standard	k1gInSc7
pro	pro	k7c4
starší	starší	k1gMnPc4
jehličkové	jehličkový	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
jsou	být	k5eAaImIp3nP
escape	escapat	k5eAaPmIp3nS
sekvence	sekvence	k1gFnSc2
používané	používaný	k2eAgFnSc2d1
firmou	firma	k1gFnSc7
Epson	Epson	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ASCII	ascii	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	Americana	k1gFnPc2
Standard	standard	k1gInSc1
Code	Cod	k1gInSc2
for	forum	k1gNnPc2
Information	Information	k1gInSc1
Interchange	Interchang	k1gInPc1
<g/>
.	.	kIx.
1963	#num#	k4
<g/>
.	.	kIx.
detail	detail	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
</s>
<s>
Unicode	Unicod	k1gMnSc5
<g/>
,	,	kIx,
UTF-8	UTF-8	k1gMnSc5
</s>
<s>
ASCII	ascii	kA
art	art	k?
</s>
<s>
EBCDIC	EBCDIC	kA
</s>
<s>
Znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
ZX	ZX	kA
Spectrum	Spectrum	k1gNnSc1
</s>
<s>
ASCII	ascii	kA
tabulka	tabulka	k1gFnSc1
a	a	k8xC
přehled	přehled	k1gInSc1
znaků	znak	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
ASCII	ascii	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
ASCII	ascii	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ASCII	ascii	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
ASCII	ascii	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ASCII	ascii	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
