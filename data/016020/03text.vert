<s>
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
</s>
<s>
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
</s>
<s>
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
methanoát	methanoát	k1gInSc1
sodný	sodný	k2eAgInSc1d1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
CHNaO₂	CHNaO₂	k?
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
141-53-7	141-53-7	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
67,987	67,987	k4
u	u	k7c2
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
(	(	kIx(
<g/>
vzorec	vzorec	k1gInSc1
HCOONa	HCOON	k1gInSc2
<g/>
,	,	kIx,
systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
methanoát	methanoát	k1gInSc1
sodný	sodný	k2eAgInSc1d1
nebo	nebo	k8xC
sodná	sodný	k2eAgFnSc1d1
sůl	sůl	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
methanové	methanová	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sodná	sodný	k2eAgFnSc1d1
sůl	sůl	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
mravenčí	mravenčí	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
navlhavý	navlhavý	k2eAgInSc1d1
bílý	bílý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgMnSc1d1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgInPc6d1
procesech	proces	k1gInPc6
pro	pro	k7c4
potisk	potisk	k1gInSc4
a	a	k8xC
barvení	barvení	k1gNnSc4
textilu	textil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
jako	jako	k9
pufr	pufr	k1gInSc4
pro	pro	k7c4
silné	silný	k2eAgFnPc4d1
minerální	minerální	k2eAgFnPc4d1
kyseliny	kyselina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
</s>
<s>
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
lze	lze	k6eAd1
připravovat	připravovat	k5eAaImF
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
neutralizací	neutralizace	k1gFnPc2
kyseliny	kyselina	k1gFnSc2
mravenčí	mravenčit	k5eAaImIp3nS
uhličitanem	uhličitan	k1gInSc7
sodným	sodný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
jej	on	k3xPp3gMnSc4
také	také	k9
získat	získat	k5eAaPmF
reakcí	reakce	k1gFnSc7
chloroformu	chloroform	k1gInSc2
s	s	k7c7
alkoholovým	alkoholový	k2eAgInSc7d1
roztokem	roztok	k1gInSc7
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
<g/>
:	:	kIx,
</s>
<s>
CHCl	CHCl	k1gInSc1
<g/>
3	#num#	k4
+	+	kIx~
4	#num#	k4
<g/>
NaOH	NaOH	k1gMnSc1
→	→	k?
HCOONa	HCOONa	k1gMnSc1
+	+	kIx~
3	#num#	k4
<g/>
NaCl	NaCla	k1gFnPc2
+	+	kIx~
2H2O	2H2O	k4
</s>
<s>
nebo	nebo	k8xC
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
s	s	k7c7
chloralhydrátem	chloralhydrát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
C	C	kA
<g/>
2	#num#	k4
<g/>
HCl	HCl	k1gFnSc1
<g/>
3	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
+	+	kIx~
NaOH	NaOH	k1gFnSc1
→	→	k?
CHCl	CHCl	k1gInSc1
<g/>
3	#num#	k4
+	+	kIx~
HCOONa	HCOON	k1gInSc2
+	+	kIx~
H2O	H2O	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
z	z	k7c2
metod	metoda	k1gFnPc2
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
preferována	preferován	k2eAgFnSc1d1
před	před	k7c7
tou	ten	k3xDgFnSc7
první	první	k4xOgFnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
nízká	nízký	k2eAgFnSc1d1
rozpustnost	rozpustnost	k1gFnSc1
chloroformu	chloroform	k1gInSc2
ve	v	k7c6
vodě	voda	k1gFnSc6
zjednodušuje	zjednodušovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
separaci	separace	k1gFnSc4
z	z	k7c2
roztoku	roztok	k1gInSc2
mravenčanu	mravenčan	k1gInSc2
sodného	sodný	k2eAgInSc2d1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
rozpustného	rozpustný	k2eAgInSc2d1
chloridu	chlorid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Komerčně	komerčně	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
pohlcováním	pohlcování	k1gNnSc7
oxidu	oxid	k1gInSc2
uhelnatého	uhelnatý	k2eAgInSc2d1
pod	pod	k7c7
tlakem	tlak	k1gInSc7
v	v	k7c6
tuhém	tuhý	k2eAgInSc6d1
hydroxidu	hydroxid	k1gInSc6
sodném	sodný	k2eAgInSc6d1
při	při	k7c6
160	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
:	:	kIx,
</s>
<s>
CO	co	k3yQnSc1,k3yRnSc1,k3yInSc1
+	+	kIx~
NaOH	NaOH	k1gMnSc1
→	→	k?
HCOONa	HCOONa	k1gMnSc1
</s>
<s>
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
lze	lze	k6eAd1
vyrobit	vyrobit	k5eAaPmF
také	také	k9
haloformovou	haloformový	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
mezi	mezi	k7c7
ethanolem	ethanol	k1gInSc7
a	a	k8xC
chlornanem	chlornan	k1gInSc7
sodným	sodný	k2eAgInSc7d1
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
zásady	zásada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
procedura	procedura	k1gFnSc1
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
zdokumentována	zdokumentovat	k5eAaPmNgFnS
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
chloroformu	chloroform	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
octan	octan	k1gInSc1
sodný	sodný	k2eAgInSc1d1
</s>
<s>
mravenčan	mravenčan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Sodium	Sodium	k1gNnSc1
formate	format	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mravenčan	mravenčan	k1gInSc1
sodný	sodný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4333487-8	4333487-8	k4
</s>
