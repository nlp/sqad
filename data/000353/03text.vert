<s>
Slezsko	Slezsko	k1gNnSc1	Slezsko
(	(	kIx(	(
<g/>
slezsky	slezsky	k6eAd1	slezsky
Ślůnsk	Ślůnsk	k1gInSc1	Ślůnsk
<g/>
,	,	kIx,	,
Ślůnzek	Ślůnzek	k1gInSc1	Ślůnzek
<g/>
,	,	kIx,	,
Ślůnsko	Ślůnsko	k1gNnSc1	Ślůnsko
<g/>
,	,	kIx,	,
Ślesko	Ślesko	k1gNnSc1	Ślesko
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
slezskoněmecky	slezskoněmecky	k6eAd1	slezskoněmecky
Schläsing	Schläsing	k1gInSc1	Schläsing
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Śląsk	Śląsk	k1gInSc1	Śląsk
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Schlesien	Schlesien	k1gInSc1	Schlesien
<g/>
,	,	kIx,	,
Latinsky	latinsky	k6eAd1	latinsky
Silesia	Silesia	k1gFnSc1	Silesia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historické	historický	k2eAgNnSc4d1	historické
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
územní	územní	k2eAgInSc4d1	územní
výběžek	výběžek	k1gInSc4	výběžek
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
exklávy	exkláv	k1gInPc4	exkláv
<g/>
)	)	kIx)	)
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
..	..	k?	..
Tradičně	tradičně	k6eAd1	tradičně
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
rozlišovány	rozlišován	k2eAgInPc1d1	rozlišován
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
regiony	region	k1gInPc1	region
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
Slezsko	Slezsko	k1gNnSc1	Slezsko
a	a	k8xC	a
Horní	horní	k2eAgNnSc1d1	horní
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
historií	historie	k1gFnSc7	historie
a	a	k8xC	a
charakteristikami	charakteristika	k1gFnPc7	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Slezska	Slezsko	k1gNnSc2	Slezsko
patřilo	patřit	k5eAaImAgNnS	patřit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
státům	stát	k1gInPc3	stát
-	-	kIx~	-
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
,	,	kIx,	,
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakouskému	rakouský	k2eAgNnSc3d1	rakouské
císařství	císařství	k1gNnSc3	císařství
<g/>
,	,	kIx,	,
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc3	Rakousku-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1335-1742	[number]	k4	1335-1742
Slezsko	Slezsko	k1gNnSc1	Slezsko
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
;	;	kIx,	;
od	od	k7c2	od
uvedeného	uvedený	k2eAgInSc2d1	uvedený
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
s	s	k7c7	s
proměnlivým	proměnlivý	k2eAgInSc7d1	proměnlivý
rozsahem	rozsah	k1gInSc7	rozsah
a	a	k8xC	a
právním	právní	k2eAgNnSc7d1	právní
postavením	postavení	k1gNnSc7	postavení
<g/>
,	,	kIx,	,
patřících	patřící	k2eAgInPc2d1	patřící
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
státům	stát	k1gInPc3	stát
(	(	kIx(	(
<g/>
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
Pruské	pruský	k2eAgNnSc1d1	pruské
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
Polské	polský	k2eAgNnSc1d1	polské
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
teorií	teorie	k1gFnPc2	teorie
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
vzniku	vznik	k1gInSc2	vznik
slova	slovo	k1gNnSc2	slovo
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
němečtí	německý	k2eAgMnPc1d1	německý
badatelé	badatel	k1gMnPc1	badatel
odvozovali	odvozovat	k5eAaImAgMnP	odvozovat
pojmenování	pojmenování	k1gNnSc4	pojmenování
Slezska	Slezsko	k1gNnSc2	Slezsko
od	od	k7c2	od
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
Silingů	Siling	k1gInPc2	Siling
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Silingae	Silinga	k1gMnSc2	Silinga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
příslušníci	příslušník	k1gMnPc1	příslušník
na	na	k7c6	na
slezském	slezský	k2eAgNnSc6d1	Slezské
území	území	k1gNnSc6	území
dočasně	dočasně	k6eAd1	dočasně
sídlili	sídlit	k5eAaImAgMnP	sídlit
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
dalších	další	k2eAgFnPc2d1	další
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odvodit	odvodit	k5eAaPmF	odvodit
název	název	k1gInSc4	název
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
od	od	k7c2	od
hory	hora	k1gFnSc2	hora
Ślęży	Ślęża	k1gFnSc2	Ślęża
či	či	k8xC	či
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Slenzy	Slenza	k1gFnSc2	Slenza
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Ślęza	Ślęza	k1gFnSc1	Ślęza
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Lohe	Lohe	k1gNnSc1	Lohe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
Slezska	Slezsko	k1gNnSc2	Slezsko
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
původ	původ	k1gInSc4	původ
i	i	k9	i
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
výrazech	výraz	k1gInPc6	výraz
ślęg	ślęg	k1gInSc1	ślęg
<g/>
,	,	kIx,	,
ślęgać	ślęgać	k?	ślęgać
či	či	k8xC	či
śleganina	śleganina	k1gFnSc1	śleganina
označujících	označující	k2eAgFnPc2d1	označující
nepohodu	nepohoda	k1gFnSc4	nepohoda
<g/>
,	,	kIx,	,
vlhkost	vlhkost	k1gFnSc1	vlhkost
a	a	k8xC	a
deštivé	deštivý	k2eAgNnSc1d1	deštivé
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
hledají	hledat	k5eAaImIp3nP	hledat
původ	původ	k1gInSc4	původ
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
až	až	k9	až
v	v	k7c6	v
starším	starý	k2eAgInSc6d2	starší
indoevropském	indoevropský	k2eAgInSc6d1	indoevropský
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
studie	studie	k1gFnSc1	studie
se	se	k3xPyFc4	se
však	však	k9	však
přiklánějí	přiklánět	k5eAaImIp3nP	přiklánět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
Slezska	Slezsko	k1gNnSc2	Slezsko
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Slenzy	Slenza	k1gFnSc2	Slenza
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Slezska	Slezsko	k1gNnSc2	Slezsko
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
nížinatý	nížinatý	k2eAgInSc1d1	nížinatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
části	část	k1gFnSc6	část
Slezska	Slezsko	k1gNnSc2	Slezsko
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
Slezská	slezský	k2eAgFnSc1d1	Slezská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
protéká	protékat	k5eAaImIp3nS	protékat
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
slezská	slezský	k2eAgFnSc1d1	Slezská
řeka	řeka	k1gFnSc1	řeka
Odra	Odra	k1gFnSc1	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Slezska	Slezsko	k1gNnSc2	Slezsko
je	být	k5eAaImIp3nS	být
Sněžka	Sněžka	k1gFnSc1	Sněžka
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
hranici	hranice	k1gFnSc6	hranice
Slezska	Slezsko	k1gNnSc2	Slezsko
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
pohoří	pohoří	k1gNnSc4	pohoří
Sudety	Sudety	k1gFnPc1	Sudety
(	(	kIx(	(
<g/>
Lužické	lužický	k2eAgFnPc1d1	Lužická
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
Rychlebské	Rychlebský	k2eAgFnPc1d1	Rychlebská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Hrubý	hrubý	k2eAgInSc1d1	hrubý
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moravskoslezské	moravskoslezský	k2eAgInPc1d1	moravskoslezský
Beskydy	Beskyd	k1gInPc1	Beskyd
a	a	k8xC	a
Slezské	slezský	k2eAgInPc1d1	slezský
Beskydy	Beskyd	k1gInPc1	Beskyd
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ke	k	k7c3	k
Slezsku	Slezsko	k1gNnSc3	Slezsko
připočteme	připočíst	k5eAaPmIp1nP	připočíst
i	i	k9	i
Kladsko	Kladsko	k1gNnSc4	Kladsko
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
hranici	hranice	k1gFnSc6	hranice
Slezska	Slezsko	k1gNnSc2	Slezsko
také	také	k6eAd1	také
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Králický	králický	k2eAgInSc4d1	králický
Sněžník	Sněžník	k1gInSc4	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
Visly	Visla	k1gFnSc2	Visla
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
Liswarta	Liswarta	k1gFnSc1	Liswarta
a	a	k8xC	a
Przemsze	Przemsze	k1gFnSc1	Przemsze
<g/>
,	,	kIx,	,
dolinou	dolina	k1gFnSc7	dolina
řeky	řeka	k1gFnSc2	řeka
Barycze	Barycze	k1gFnSc2	Barycze
a	a	k8xC	a
následně	následně	k6eAd1	následně
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
Odry	Odra	k1gFnSc2	Odra
a	a	k8xC	a
Lužické	lužický	k2eAgFnSc2d1	Lužická
Nisy	Nisa	k1gFnSc2	Nisa
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
putuje	putovat	k5eAaImIp3nS	putovat
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Lužickým	lužický	k2eAgFnPc3d1	Lužická
horám	hora	k1gFnPc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Slezsko	Slezsko	k1gNnSc1	Slezsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
několika	několik	k4yIc7	několik
historickými	historický	k2eAgFnPc7d1	historická
zeměmi	zem	k1gFnPc7	zem
<g/>
;	;	kIx,	;
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc1d1	dolní
Lužice	Lužice	k1gFnSc1	Lužice
<g/>
,	,	kIx,	,
Braniborsko	Braniborsko	k1gNnSc1	Braniborsko
<g/>
,	,	kIx,	,
Velkopolsko	Velkopolsko	k1gNnSc1	Velkopolsko
<g/>
,	,	kIx,	,
Malopolsko	Malopolsko	k1gNnSc1	Malopolsko
a	a	k8xC	a
Horní	horní	k2eAgFnPc1d1	horní
Uhry	Uhry	k1gFnPc1	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
století	století	k1gNnSc6	století
bylo	být	k5eAaImAgNnS	být
Slezsko	Slezsko	k1gNnSc1	Slezsko
osídleno	osídlit	k5eAaPmNgNnS	osídlit
slovanskými	slovanský	k2eAgInPc7d1	slovanský
kmeny	kmen	k1gInPc7	kmen
Slezanů	Slezan	k1gMnPc2	Slezan
<g/>
,	,	kIx,	,
Opolanů	Opolan	k1gMnPc2	Opolan
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
Slezsko	Slezsko	k1gNnSc4	Slezsko
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Dolní	dolní	k2eAgNnSc4d1	dolní
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
;	;	kIx,	;
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Horní	horní	k2eAgNnSc4d1	horní
Slezsko	Slezsko	k1gNnSc4	Slezsko
bylo	být	k5eAaImAgNnS	být
kdysi	kdysi	k6eAd1	kdysi
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Opolsko	Opolsko	k1gNnSc1	Opolsko
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Slezska	Slezsko	k1gNnSc2	Slezsko
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
990	[number]	k4	990
součástí	součást	k1gFnPc2	součást
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1138	[number]	k4	1138
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc1	období
decentralizace	decentralizace	k1gFnSc2	decentralizace
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Slezsko	Slezsko	k1gNnSc1	Slezsko
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
údělných	údělný	k2eAgNnPc2d1	údělné
knížectví	knížectví	k1gNnPc2	knížectví
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
členem	člen	k1gMnSc7	člen
polské	polský	k2eAgFnSc2d1	polská
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
Piastovské	Piastovský	k2eAgFnSc2d1	Piastovská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podléhal	podléhat	k5eAaImAgInS	podléhat
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
polskému	polský	k2eAgMnSc3d1	polský
knížeti	kníže	k1gMnSc3	kníže
sídlícímu	sídlící	k2eAgMnSc3d1	sídlící
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1177	[number]	k4	1177
bylo	být	k5eAaImAgNnS	být
Slezsko	Slezsko	k1gNnSc1	Slezsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
západní	západní	k2eAgMnPc1d1	západní
(	(	kIx(	(
<g/>
Velké	velký	k2eAgNnSc1d1	velké
Hlohovsko	Hlohovsko	k1gNnSc1	Hlohovsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
Velké	velký	k2eAgNnSc1d1	velké
Vratislavsko	Vratislavsko	k1gNnSc1	Vratislavsko
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
spojeny	spojit	k5eAaPmNgInP	spojit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Jindřichem	Jindřich	k1gMnSc7	Jindřich
I.	I.	kA	I.
Ke	k	k7c3	k
třetímu	třetí	k4xOgMnSc3	třetí
<g/>
,	,	kIx,	,
východnímu	východní	k2eAgMnSc3d1	východní
údělu	úděl	k1gInSc6	úděl
připojil	připojit	k5eAaPmAgInS	připojit
Měšek	Měšek	k1gInSc1	Měšek
I.	I.	kA	I.
Slezský	slezský	k2eAgInSc1d1	slezský
část	část	k1gFnSc4	část
<g />
.	.	kIx.	.
</s>
<s>
jižního	jižní	k2eAgNnSc2d1	jižní
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
původně	původně	k6eAd1	původně
součástí	součást	k1gFnSc7	součást
Slezska	Slezsko	k1gNnSc2	Slezsko
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgNnSc1d2	pozdější
Bytomsko	Bytomsko	k1gNnSc1	Bytomsko
<g/>
,	,	kIx,	,
Osvětimsko	Osvětimsko	k1gNnSc1	Osvětimsko
<g/>
,	,	kIx,	,
Zátorsko	Zátorsko	k1gNnSc1	Zátorsko
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgNnSc1d1	velké
Opolsko	Opolsko	k1gNnSc1	Opolsko
-	-	kIx~	-
pozdější	pozdní	k2eAgNnSc1d2	pozdější
Horní	horní	k2eAgNnSc1d1	horní
Slezsko	Slezsko	k1gNnSc1	Slezsko
(	(	kIx(	(
<g/>
Górny	Górn	k1gInPc4	Górn
Śląsk	Śląsk	k1gInSc1	Śląsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mělo	mít	k5eAaImAgNnS	mít
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
Ratiboři	Ratiboř	k1gFnSc6	Ratiboř
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Opolí	Opolí	k1gNnSc6	Opolí
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1202	[number]	k4	1202
se	se	k3xPyFc4	se
piastovští	piastovský	k2eAgMnPc1d1	piastovský
panovníci	panovník	k1gMnPc1	panovník
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
Slezska	Slezsko	k1gNnSc2	Slezsko
vzdali	vzdát	k5eAaPmAgMnP	vzdát
vzájemně	vzájemně	k6eAd1	vzájemně
dědických	dědický	k2eAgNnPc2d1	dědické
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
rozdělení	rozdělení	k1gNnSc3	rozdělení
území	území	k1gNnSc2	území
na	na	k7c4	na
Horní	horní	k2eAgNnSc4d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgNnSc4d1	dolní
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názvy	název	k1gInPc1	název
však	však	k9	však
byly	být	k5eAaImAgInP	být
užívány	užívat	k5eAaImNgInP	užívat
až	až	k6eAd1	až
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
měly	mít	k5eAaImAgFnP	mít
ryze	ryze	k6eAd1	ryze
geografický	geografický	k2eAgInSc4d1	geografický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
proces	proces	k1gInSc1	proces
dalšího	další	k2eAgNnSc2d1	další
dělení	dělení	k1gNnSc2	dělení
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
Slezska	Slezsko	k1gNnSc2	Slezsko
rychle	rychle	k6eAd1	rychle
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
po	po	k7c6	po
drtivé	drtivý	k2eAgFnSc6d1	drtivá
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lehnice	Lehnice	k1gFnSc2	Lehnice
s	s	k7c7	s
Mongoly	Mongol	k1gMnPc7	Mongol
roku	rok	k1gInSc2	rok
1241	[number]	k4	1241
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
osamostatnění	osamostatnění	k1gNnSc3	osamostatnění
tzv.	tzv.	kA	tzv.
Středního	střední	k2eAgNnSc2d1	střední
Slezska	Slezsko	k1gNnSc2	Slezsko
od	od	k7c2	od
Lehnicka	Lehnicko	k1gNnSc2	Lehnicko
a	a	k8xC	a
koncem	koncem	k7c2	koncem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
osamostatnilo	osamostatnit	k5eAaPmAgNnS	osamostatnit
i	i	k9	i
Hlohovsko	Hlohovsko	k1gNnSc1	Hlohovsko
<g/>
,	,	kIx,	,
Zaháňsko	Zaháňsko	k1gNnSc1	Zaháňsko
<g/>
,	,	kIx,	,
Javorsko	Javorsko	k1gNnSc1	Javorsko
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Volovsko	Volovsko	k1gNnSc1	Volovsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Vratislavska	Vratislavsko	k1gNnSc2	Vratislavsko
(	(	kIx(	(
<g/>
spojeného	spojený	k2eAgMnSc2d1	spojený
už	už	k9	už
s	s	k7c7	s
Niskem	Nisko	k1gNnSc7	Nisko
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
Svídnicko	Svídnicko	k1gNnSc1	Svídnicko
<g/>
,	,	kIx,	,
Olešnicko	Olešnicko	k1gNnSc1	Olešnicko
<g/>
,	,	kIx,	,
Minsterbersko	Minsterbersko	k1gNnSc1	Minsterbersko
<g/>
,	,	kIx,	,
Břežsko	Břežsko	k1gNnSc1	Břežsko
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
menších	malý	k2eAgNnPc2d2	menší
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
ovšem	ovšem	k9	ovšem
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
i	i	k9	i
Horní	horní	k2eAgNnSc1d1	horní
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
(	(	kIx(	(
<g/>
Velkého	velký	k2eAgNnSc2d1	velké
<g/>
)	)	kIx)	)
Opolska	Opolsko	k1gNnSc2	Opolsko
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1281	[number]	k4	1281
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
<g/>
,	,	kIx,	,
Ratibořsko	Ratibořska	k1gFnSc5	Ratibořska
<g/>
,	,	kIx,	,
Bytomsko	Bytomska	k1gFnSc5	Bytomska
<g/>
,	,	kIx,	,
Kozelsko	Kozelska	k1gFnSc5	Kozelska
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
za	za	k7c4	za
slezská	slezský	k2eAgNnPc4d1	Slezské
knížectví	knížectví	k1gNnSc4	knížectví
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
považovány	považován	k2eAgFnPc1d1	považována
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
Opavsko	Opavsko	k1gNnSc1	Opavsko
a	a	k8xC	a
Krnovsko	Krnovsko	k1gNnSc1	Krnovsko
(	(	kIx(	(
<g/>
definitivně	definitivně	k6eAd1	definitivně
stvrzeno	stvrdit	k5eAaPmNgNnS	stvrdit
až	až	k9	až
roku	rok	k1gInSc2	rok
1659	[number]	k4	1659
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dříve	dříve	k6eAd2	dříve
příslušely	příslušet	k5eAaImAgFnP	příslušet
k	k	k7c3	k
Moravě	Morava	k1gFnSc3	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
uvedl	uvést	k5eAaPmAgMnS	uvést
řadu	řada	k1gFnSc4	řada
slezských	slezský	k2eAgNnPc2d1	Slezské
knížectví	knížectví	k1gNnPc2	knížectví
do	do	k7c2	do
lenního	lenní	k2eAgInSc2d1	lenní
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
Českému	český	k2eAgNnSc3d1	české
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
dovršil	dovršit	k5eAaPmAgMnS	dovršit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1327-1335	[number]	k4	1327-1335
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
nad	nad	k7c7	nad
Slezskem	Slezsko	k1gNnSc7	Slezsko
opakovaně	opakovaně	k6eAd1	opakovaně
uznal	uznat	k5eAaPmAgInS	uznat
i	i	k9	i
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Kazimír	Kazimír	k1gMnSc1	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
1333	[number]	k4	1333
<g/>
-	-	kIx~	-
<g/>
1370	[number]	k4	1370
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
roku	rok	k1gInSc2	rok
1333	[number]	k4	1333
Vyšehradskou	vyšehradský	k2eAgFnSc4d1	Vyšehradská
a	a	k8xC	a
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1335	[number]	k4	1335
Trenčínskou	trenčínský	k2eAgFnSc7d1	Trenčínská
smlouvou	smlouva	k1gFnSc7	smlouva
(	(	kIx(	(
<g/>
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
markrabětem	markrabě	k1gMnSc7	markrabě
Karlem	Karel	k1gMnSc7	Karel
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
polskou	polský	k2eAgFnSc4d1	polská
korunu	koruna	k1gFnSc4	koruna
<g/>
)	)	kIx)	)
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
mírem	mír	k1gInSc7	mír
v	v	k7c6	v
Namyslově	Namyslově	k1gFnSc6	Namyslově
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Svídnickou	Svídnický	k2eAgFnSc7d1	Svídnická
získal	získat	k5eAaPmAgInS	získat
dědickým	dědický	k2eAgInSc7d1	dědický
nárokem	nárok	k1gInSc7	nárok
poslední	poslední	k2eAgFnSc1d1	poslední
dosud	dosud	k6eAd1	dosud
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
slezská	slezský	k2eAgFnSc1d1	Slezská
knížectví	knížectví	k1gNnSc1	knížectví
(	(	kIx(	(
<g/>
Javorsko	Javorsko	k1gNnSc1	Javorsko
<g/>
,	,	kIx,	,
Svídnicko	Svídnicko	k1gNnSc1	Svídnicko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svídnicko	Svídnicko	k6eAd1	Svídnicko
pak	pak	k6eAd1	pak
vedle	vedle	k7c2	vedle
Vratislavska	Vratislavsko	k1gNnSc2	Vratislavsko
a	a	k8xC	a
Hlohovska	Hlohovsko	k1gNnSc2	Hlohovsko
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
oporám	opora	k1gFnPc3	opora
moci	moc	k1gFnSc2	moc
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
proces	proces	k1gInSc1	proces
drobení	drobení	k1gNnSc2	drobení
slezského	slezský	k2eAgNnSc2d1	Slezské
území	území	k1gNnSc2	území
<g/>
:	:	kIx,	:
osamostatnilo	osamostatnit	k5eAaPmAgNnS	osamostatnit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
Stínavsko	Stínavsko	k1gNnSc1	Stínavsko
(	(	kIx(	(
<g/>
Stěnavsko	Stěnavska	k1gMnSc5	Stěnavska
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Steinau	Steinaus	k1gInSc6	Steinaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
území	území	k1gNnSc6	území
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Kladska	Kladsko	k1gNnSc2	Kladsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
načas	načas	k6eAd1	načas
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
od	od	k7c2	od
Minsterberska	Minsterbersk	k1gInSc2	Minsterbersk
(	(	kIx(	(
<g/>
za	za	k7c2	za
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
1474	[number]	k4	1474
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Namyslov	Namyslov	k1gInSc1	Namyslov
<g/>
,	,	kIx,	,
Nemodlínsko	Nemodlínsko	k1gNnSc1	Nemodlínsko
(	(	kIx(	(
<g/>
Falkenberg	Falkenberg	k1gInSc1	Falkenberg
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
úděly	úděl	k1gInPc1	úděl
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
počet	počet	k1gInSc1	počet
vévodství	vévodství	k1gNnSc2	vévodství
a	a	k8xC	a
knížectví	knížectví	k1gNnSc2	knížectví
přesahoval	přesahovat	k5eAaImAgMnS	přesahovat
i	i	k9	i
dvě	dva	k4xCgFnPc4	dva
desítky	desítka	k1gFnPc4	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Vymíráním	vymírání	k1gNnSc7	vymírání
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
piastovských	piastovský	k2eAgFnPc2d1	piastovský
větví	větev	k1gFnPc2	větev
(	(	kIx(	(
<g/>
1335	[number]	k4	1335
vratislavské	vratislavský	k2eAgInPc1d1	vratislavský
<g/>
,	,	kIx,	,
1368	[number]	k4	1368
svídnické	svídnický	k2eAgInPc1d1	svídnický
<g/>
,	,	kIx,	,
1492	[number]	k4	1492
olešnické	olešnický	k2eAgInPc1d1	olešnický
<g/>
,	,	kIx,	,
1504	[number]	k4	1504
hlohovské	hlohovský	k2eAgInPc1d1	hlohovský
<g/>
,	,	kIx,	,
1532	[number]	k4	1532
opolské	opolský	k2eAgInPc1d1	opolský
<g/>
,	,	kIx,	,
1625	[number]	k4	1625
těšínské	těšínské	k1gNnSc1	těšínské
a	a	k8xC	a
1675	[number]	k4	1675
břežsko-lehnické	břežskoehnický	k2eAgFnSc2d1	břežsko-lehnický
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
poslední	poslední	k2eAgFnSc1d1	poslední
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
rodu	rod	k1gInSc2	rod
Piastovců	Piastovec	k1gMnPc2	Piastovec
<g/>
)	)	kIx)	)
připadala	připadat	k5eAaImAgFnS	připadat
území	území	k1gNnSc4	území
přímo	přímo	k6eAd1	přímo
České	český	k2eAgFnSc3d1	Česká
koruně	koruna	k1gFnSc3	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zároveň	zároveň	k6eAd1	zároveň
Polsko	Polsko	k1gNnSc1	Polsko
získalo	získat	k5eAaPmAgNnS	získat
zpět	zpět	k6eAd1	zpět
koupí	koupě	k1gFnSc7	koupě
Osvětimsko	Osvětimsko	k1gNnSc1	Osvětimsko
<g/>
,	,	kIx,	,
Zátorsko	Zátorsko	k1gNnSc1	Zátorsko
a	a	k8xC	a
Seversko	Seversko	k1gNnSc1	Seversko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přestala	přestat	k5eAaPmAgFnS	přestat
počítat	počítat	k5eAaImF	počítat
mezi	mezi	k7c4	mezi
slezská	slezský	k2eAgNnPc4d1	Slezské
území	území	k1gNnPc4	území
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
Horní	horní	k2eAgNnSc4d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgNnSc4d1	dolní
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
oslabení	oslabení	k1gNnSc3	oslabení
vztahu	vztah	k1gInSc2	vztah
Slezska	Slezsko	k1gNnSc2	Slezsko
k	k	k7c3	k
Českému	český	k2eAgNnSc3d1	české
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
zůstali	zůstat	k5eAaPmAgMnP	zůstat
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Slezska	Slezsko	k1gNnSc2	Slezsko
nejen	nejen	k6eAd1	nejen
věrni	věren	k2eAgMnPc1d1	věren
katolicismu	katolicismus	k1gInSc3	katolicismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vesměs	vesměs	k6eAd1	vesměs
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
rozhodným	rozhodný	k2eAgMnPc3d1	rozhodný
odpůrcům	odpůrce	k1gMnPc3	odpůrce
husitů	husita	k1gMnPc2	husita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
přílivu	příliv	k1gInSc3	příliv
německých	německý	k2eAgMnPc2d1	německý
kolonistů	kolonista	k1gMnPc2	kolonista
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
už	už	k9	už
území	území	k1gNnSc1	území
především	především	k6eAd1	především
ve	v	k7c6	v
městech	město	k1gNnPc6	město
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
germanizováno	germanizován	k2eAgNnSc1d1	germanizováno
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
však	však	k9	však
stále	stále	k6eAd1	stále
převažovali	převažovat	k5eAaImAgMnP	převažovat
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
sice	sice	k8xC	sice
řadu	řada	k1gFnSc4	řada
měst	město	k1gNnPc2	město
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
knížectví	knížectví	k1gNnPc4	knížectví
dobyli	dobýt	k5eAaPmAgMnP	dobýt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nepevné	pevný	k2eNgFnSc3d1	nepevná
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
samotných	samotný	k2eAgFnPc6d1	samotná
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
pouta	pouto	k1gNnSc2	pouto
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
Kladsko	Kladsko	k1gNnSc4	Kladsko
a	a	k8xC	a
Minstrberské	Minstrberský	k2eAgNnSc4d1	Minstrberský
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
ztratil	ztratit	k5eAaPmAgMnS	ztratit
nejen	nejen	k6eAd1	nejen
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatní	ostatní	k2eAgFnPc4d1	ostatní
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
zde	zde	k6eAd1	zde
provedl	provést	k5eAaPmAgMnS	provést
určitou	určitý	k2eAgFnSc4d1	určitá
centralizaci	centralizace	k1gFnSc4	centralizace
slezské	slezský	k2eAgFnSc2d1	Slezská
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
krále	král	k1gMnSc2	král
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
vrchní	vrchní	k2eAgMnSc1d1	vrchní
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
;	;	kIx,	;
centrální	centrální	k2eAgInPc1d1	centrální
orgány	orgán	k1gInPc1	orgán
měly	mít	k5eAaImAgInP	mít
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
o	o	k7c4	o
Slezsko	Slezsko	k1gNnSc4	Slezsko
projevovat	projevovat	k5eAaImF	projevovat
zájem	zájem	k1gInSc4	zájem
braniborští	braniborský	k2eAgMnPc1d1	braniborský
Hohenzollerni	Hohenzollern	k1gMnPc1	Hohenzollern
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
roku	rok	k1gInSc2	rok
1482	[number]	k4	1482
získali	získat	k5eAaPmAgMnP	získat
Krosensko	Krosensko	k1gNnSc4	Krosensko
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
další	další	k2eAgNnPc1d1	další
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Krnovsko	Krnovsko	k1gNnSc1	Krnovsko
a	a	k8xC	a
Bohumínsko	Bohumínsko	k1gNnSc1	Bohumínsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
České	český	k2eAgFnSc3d1	Česká
koruně	koruna	k1gFnSc3	koruna
se	se	k3xPyFc4	se
Slezsko	Slezsko	k1gNnSc1	Slezsko
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
až	až	k9	až
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
de	de	k?	de
iure	iurat	k5eAaPmIp3nS	iurat
však	však	k9	však
až	až	k9	až
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
však	však	k9	však
území	území	k1gNnSc4	území
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
silná	silný	k2eAgFnSc1d1	silná
reformace	reformace	k1gFnSc1	reformace
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
důsledku	důsledek	k1gInSc6	důsledek
se	se	k3xPyFc4	se
Slezsko	Slezsko	k1gNnSc1	Slezsko
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
významně	významně	k6eAd1	významně
podílelo	podílet	k5eAaImAgNnS	podílet
na	na	k7c6	na
stavovském	stavovský	k2eAgInSc6d1	stavovský
protihabsburském	protihabsburský	k2eAgInSc6d1	protihabsburský
odboji	odboj	k1gInSc6	odboj
<g/>
,	,	kIx,	,
po	po	k7c6	po
Vestfálském	vestfálský	k2eAgInSc6d1	vestfálský
míru	mír	k1gInSc6	mír
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
tu	tu	k6eAd1	tu
pak	pak	k6eAd1	pak
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
dokonce	dokonce	k9	dokonce
byla	být	k5eAaImAgFnS	být
povolena	povolit	k5eAaPmNgFnS	povolit
náboženská	náboženský	k2eAgFnSc1d1	náboženská
tolerance	tolerance	k1gFnSc1	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
nezabránila	zabránit	k5eNaPmAgFnS	zabránit
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
inklinovala	inklinovat	k5eAaImAgFnS	inklinovat
k	k	k7c3	k
protestantskému	protestantský	k2eAgNnSc3d1	protestantské
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Slezsko	Slezsko	k1gNnSc4	Slezsko
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
rozvinutým	rozvinutý	k2eAgNnSc7d1	rozvinuté
tkalcovstvím	tkalcovství	k1gNnSc7	tkalcovství
zároveň	zároveň	k6eAd1	zároveň
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
nejvyspělejším	vyspělý	k2eAgFnPc3d3	nejvyspělejší
a	a	k8xC	a
nejbohatším	bohatý	k2eAgFnPc3d3	nejbohatší
zemím	zem	k1gFnPc3	zem
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
cílem	cíl	k1gInSc7	cíl
pruské	pruský	k2eAgFnSc2d1	pruská
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
vpádem	vpád	k1gInSc7	vpád
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
bez	bez	k7c2	bez
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1740	[number]	k4	1740
<g/>
-	-	kIx~	-
<g/>
1742	[number]	k4	1742
<g/>
,	,	kIx,	,
1744	[number]	k4	1744
<g/>
-	-	kIx~	-
<g/>
1745	[number]	k4	1745
a	a	k8xC	a
1756	[number]	k4	1756
<g/>
-	-	kIx~	-
<g/>
1763	[number]	k4	1763
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
války	válek	k1gInPc4	válek
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
dědictví	dědictví	k1gNnSc4	dědictví
mezi	mezi	k7c7	mezi
Habsburskou	habsburský	k2eAgFnSc7d1	habsburská
monarchií	monarchie	k1gFnSc7	monarchie
a	a	k8xC	a
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
neuznalo	uznat	k5eNaPmAgNnS	uznat
pragmatickou	pragmatický	k2eAgFnSc4d1	pragmatická
sankci	sankce	k1gFnSc4	sankce
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
vzalo	vzít	k5eAaPmAgNnS	vzít
za	za	k7c4	za
záminku	záminka	k1gFnSc4	záminka
dědické	dědický	k2eAgInPc1d1	dědický
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
slezské	slezský	k2eAgInPc4d1	slezský
úděly	úděl	k1gInPc4	úděl
<g/>
,	,	kIx,	,
držené	držený	k2eAgInPc1d1	držený
braniborskými	braniborský	k2eAgInPc7d1	braniborský
Hohenzollerny	Hohenzollern	k1gInPc7	Hohenzollern
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Vratislavského	vratislavský	k2eAgInSc2d1	vratislavský
míru	mír	k1gInSc2	mír
získalo	získat	k5eAaPmAgNnS	získat
Prusko	Prusko	k1gNnSc1	Prusko
většinu	většina	k1gFnSc4	většina
slezského	slezský	k2eAgNnSc2d1	Slezské
území	území	k1gNnSc2	území
včetně	včetně	k7c2	včetně
Kladska	Kladsko	k1gNnSc2	Kladsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dosud	dosud	k6eAd1	dosud
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
Čechám	Čechy	k1gFnPc3	Čechy
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
i	i	k9	i
mírem	mír	k1gInSc7	mír
Hubertusburským	Hubertusburský	k2eAgInSc7d1	Hubertusburský
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
Slezska	Slezsko	k1gNnSc2	Slezsko
(	(	kIx(	(
<g/>
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
4	[number]	k4	4
459	[number]	k4	459
km2	km2	k4	km2
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
s	s	k7c7	s
1	[number]	k4	1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k9	jako
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
<g/>
,	,	kIx,	,
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
České	český	k2eAgNnSc4d1	české
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
země	zem	k1gFnSc2	zem
součástí	součást	k1gFnPc2	součást
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
zemským	zemský	k2eAgNnSc7d1	zemské
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
něm.	něm.	k?	něm.
Troppau	Troppa	k1gMnSc3	Troppa
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
snahy	snaha	k1gFnSc2	snaha
omezit	omezit	k5eAaPmF	omezit
německý	německý	k2eAgInSc4d1	německý
vliv	vliv	k1gInSc4	vliv
zbavena	zbaven	k2eAgFnSc1d1	zbavena
titulu	titul	k1gInSc3	titul
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
a	a	k8xC	a
země	země	k1gFnSc1	země
Slezská	slezský	k2eAgFnSc1d1	Slezská
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Moravskou	moravský	k2eAgFnSc7d1	Moravská
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Moravskoslezskou	moravskoslezský	k2eAgFnSc7d1	Moravskoslezská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
získaném	získaný	k2eAgNnSc6d1	získané
území	území	k1gNnSc6	území
Prusko	Prusko	k1gNnSc1	Prusko
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
provincii	provincie	k1gFnSc4	provincie
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Československo-polský	československoolský	k2eAgInSc4d1	československo-polský
spor	spor	k1gInSc4	spor
o	o	k7c4	o
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
i	i	k8xC	i
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Slezska	Slezsko	k1gNnSc2	Slezsko
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
západní	západní	k2eAgFnPc1d1	západní
části	část	k1gFnPc1	část
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
už	už	k9	už
Slezané	Slezan	k1gMnPc1	Slezan
de	de	k?	de
facto	facta	k1gFnSc5	facta
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
vymřela	vymřít	k5eAaPmAgFnS	vymřít
slezština	slezština	k1gFnSc1	slezština
slovanského	slovanský	k2eAgInSc2d1	slovanský
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ještě	ještě	k9	ještě
v	v	k7c6	v
XVII	XVII	kA	XVII
století	století	k1gNnSc1	století
sahala	sahat	k5eAaImAgFnS	sahat
až	až	k9	až
k	k	k7c3	k
Vratislavi	Vratislav	k1gFnSc3	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
jiná	jiná	k1gFnSc1	jiná
-	-	kIx~	-
převažovali	převažovat	k5eAaImAgMnP	převažovat
tam	tam	k6eAd1	tam
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Ostoberschlesien	Ostoberschlesien	k1gInSc1	Ostoberschlesien
<g/>
"	"	kIx"	"
převažovali	převažovat	k5eAaImAgMnP	převažovat
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
Slezska	Slezsko	k1gNnSc2	Slezsko
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Jesenicka	Jesenicko	k1gNnSc2	Jesenicko
<g/>
,	,	kIx,	,
Opavska	Opavsko	k1gNnSc2	Opavsko
atd.	atd.	kA	atd.
převážně	převážně	k6eAd1	převážně
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Opavska	Opavsko	k1gNnSc2	Opavsko
převládali	převládat	k5eAaImAgMnP	převládat
Češi	Čech	k1gMnPc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Těšínsku	Těšínsko	k1gNnSc6	Těšínsko
<g/>
,	,	kIx,	,
převažovali	převažovat	k5eAaImAgMnP	převažovat
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žili	žít	k5eAaImAgMnP	žít
tam	tam	k6eAd1	tam
také	také	k9	také
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Těšínsku	Těšínsko	k1gNnSc6	Těšínsko
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vedle	vedle	k7c2	vedle
národnostně	národnostně	k6eAd1	národnostně
uvědomělých	uvědomělý	k2eAgMnPc2d1	uvědomělý
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
početná	početný	k2eAgFnSc1d1	početná
skupina	skupina	k1gFnSc1	skupina
místního	místní	k2eAgNnSc2d1	místní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nehlásila	hlásit	k5eNaImAgFnS	hlásit
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
z	z	k7c2	z
výše	výše	k1gFnSc1	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
jazykových	jazykový	k2eAgFnPc2d1	jazyková
skupin	skupina	k1gFnPc2	skupina
tzv.	tzv.	kA	tzv.
Šlonzáci	Šlonzák	k1gMnPc5	Šlonzák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Hlučínsku	Hlučínsko	k1gNnSc6	Hlučínsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
součástí	součást	k1gFnSc7	součást
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
českojazyčné	českojazyčný	k2eAgNnSc1d1	českojazyčné
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tzv.	tzv.	kA	tzv.
Moravci	Moravec	k1gMnPc7	Moravec
nebo	nebo	k8xC	nebo
Prajzáci	Prajzáci	k?	Prajzáci
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
neměcké	měcký	k2eNgFnSc3d1	neměcká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
však	však	k9	však
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
Slezané	Slezan	k1gMnPc1	Slezan
mluvící	mluvící	k2eAgMnPc1d1	mluvící
"	"	kIx"	"
<g/>
slovanskými	slovanský	k2eAgInPc7d1	slovanský
dialekty	dialekt	k1gInPc7	dialekt
<g/>
"	"	kIx"	"
spadali	spadat	k5eAaImAgMnP	spadat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
pod	pod	k7c7	pod
tzv.	tzv.	kA	tzv.
Deutsche	Deutschus	k1gMnSc5	Deutschus
Volksliste	Volkslist	k1gMnSc5	Volkslist
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
muselo	muset	k5eAaImAgNnS	muset
narukovat	narukovat	k5eAaPmF	narukovat
do	do	k7c2	do
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
mnoho	mnoho	k4c1	mnoho
Šlonzáků	Šlonzák	k1gMnPc2	Šlonzák
i	i	k8xC	i
obyvatel	obyvatel	k1gMnPc2	obyvatel
Hlučínska	Hlučínsko	k1gNnSc2	Hlučínsko
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
zvyšujícím	zvyšující	k2eAgMnSc7d1	zvyšující
se	se	k3xPyFc4	se
nacionalismem	nacionalismus	k1gInSc7	nacionalismus
nebyl	být	k5eNaImAgInS	být
žádný	žádný	k3yNgInSc1	žádný
velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
v	v	k7c6	v
soužití	soužití	k1gNnSc6	soužití
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgMnPc7	tento
třemi	tři	k4xCgMnPc7	tři
národnostmi	národnost	k1gFnPc7	národnost
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc7	Němec
nebo	nebo	k8xC	nebo
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
Poláky	Polák	k1gMnPc7	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc4	příchod
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
však	však	k9	však
neslo	nést	k5eAaImAgNnS	nést
pro	pro	k7c4	pro
polské	polský	k2eAgNnSc4d1	polské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Slezska	Slezsko	k1gNnSc2	Slezsko
velké	velký	k2eAgInPc1d1	velký
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
at	at	k?	at
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
vysidlování	vysidlování	k1gNnSc4	vysidlování
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byli	být	k5eAaImAgMnP	být
jinak	jinak	k6eAd1	jinak
terorizováni	terorizován	k2eAgMnPc1d1	terorizován
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Slezska	Slezsko	k1gNnSc2	Slezsko
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
mělo	mít	k5eAaImAgNnS	mít
menší	malý	k2eAgInPc4d2	menší
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
blahobyt	blahobyt	k1gInSc4	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
"	"	kIx"	"
<g/>
Slezanů	Slezan	k1gMnPc2	Slezan
<g/>
"	"	kIx"	"
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
slezské	slezský	k2eAgFnSc2d1	Slezská
problematiky	problematika	k1gFnSc2	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Němci	Němec	k1gMnPc1	Němec
museli	muset	k5eAaImAgMnP	muset
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
a	a	k8xC	a
bojovat	bojovat	k5eAaImF	bojovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Postupimské	postupimský	k2eAgFnSc6d1	Postupimská
dohodě	dohoda	k1gFnSc6	dohoda
dohodnuta	dohodnut	k2eAgFnSc1d1	dohodnuta
změna	změna	k1gFnSc1	změna
hranic	hranice	k1gFnPc2	hranice
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Východní	východní	k2eAgNnSc4d1	východní
Prusko	Prusko	k1gNnSc4	Prusko
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Pomořanska	Pomořansko	k1gNnSc2	Pomořansko
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
Slezsko	Slezsko	k1gNnSc1	Slezsko
odebráno	odebrán	k2eAgNnSc1d1	odebráno
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
přičleněno	přičleněn	k2eAgNnSc1d1	přičleněno
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimku	výjimka	k1gFnSc4	výjimka
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
musela	muset	k5eAaImAgFnS	muset
opustit	opustit	k5eAaPmF	opustit
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
a	a	k8xC	a
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
započal	započnout	k5eAaPmAgInS	započnout
zánik	zánik	k1gInSc1	zánik
"	"	kIx"	"
<g/>
Slezanů	Slezan	k1gMnPc2	Slezan
<g/>
"	"	kIx"	"
respektive	respektive	k9	respektive
"	"	kIx"	"
<g/>
původního	původní	k2eAgInSc2d1	původní
lidu	lid	k1gInSc2	lid
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Dolního	dolní	k2eAgNnSc2d1	dolní
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
vylidněné	vylidněný	k2eAgNnSc1d1	vylidněné
od	od	k7c2	od
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
polské	polský	k2eAgFnSc2d1	polská
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
smělo	smět	k5eAaImAgNnS	smět
mnoho	mnoho	k6eAd1	mnoho
Němců	Němec	k1gMnPc2	Němec
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
,	,	kIx,	,
podmínkou	podmínka	k1gFnSc7	podmínka
bylo	být	k5eAaImAgNnS	být
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
umět	umět	k5eAaImF	umět
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
verzi	verze	k1gFnSc4	verze
slezského	slezský	k2eAgInSc2d1	slezský
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
Němci	Němec	k1gMnPc1	Němec
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
používali	používat	k5eAaImAgMnP	používat
tzv.	tzv.	kA	tzv.
slezskou	slezský	k2eAgFnSc4d1	Slezská
němčinu	němčina	k1gFnSc4	němčina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgInSc1d1	polský
jazyk	jazyk	k1gInSc1	jazyk
nebo	nebo	k8xC	nebo
mít	mít	k5eAaImF	mít
polské	polský	k2eAgNnSc4d1	polské
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byla	být	k5eAaImAgFnS	být
tvrdě	tvrdě	k6eAd1	tvrdě
potlačována	potlačovat	k5eAaImNgFnS	potlačovat
německá	německý	k2eAgFnSc1d1	německá
či	či	k8xC	či
slezská	slezský	k2eAgFnSc1d1	Slezská
národnost	národnost	k1gFnSc1	národnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
komunistického	komunistický	k2eAgNnSc2d1	komunistické
Polska	Polsko	k1gNnSc2	Polsko
se	se	k3xPyFc4	se
z	z	k7c2	z
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
vystěhovalo	vystěhovat	k5eAaPmAgNnS	vystěhovat
až	až	k9	až
na	na	k7c4	na
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
polské	polský	k2eAgFnSc2d1	polská
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
začali	začít	k5eAaPmAgMnP	začít
vracet	vracet	k5eAaImF	vracet
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
národnosti	národnost	k1gFnSc3	národnost
i	i	k9	i
k	k	k7c3	k
německému	německý	k2eAgNnSc3d1	německé
občanství	občanství	k1gNnSc3	občanství
<g/>
,	,	kIx,	,
k	k	k7c3	k
německým	německý	k2eAgFnPc3d1	německá
tradicím	tradice	k1gFnPc3	tradice
i	i	k8xC	i
řeči	řeč	k1gFnSc3	řeč
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
tak	tak	k6eAd1	tak
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
mnoho	mnoho	k4c4	mnoho
Slezanů	Slezan	k1gMnPc2	Slezan
a	a	k8xC	a
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
polským	polský	k2eAgInPc3d1	polský
krajům	kraj	k1gInPc3	kraj
menší	malý	k2eAgNnPc1d2	menší
procento	procento	k1gNnSc4	procento
Poláků	polák	k1gInPc2	polák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
Horní	horní	k2eAgNnSc4d1	horní
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Slezané	Slezan	k1gMnPc1	Slezan
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
autonomii	autonomie	k1gFnSc4	autonomie
vůči	vůči	k7c3	vůči
Polsku	Polsko	k1gNnSc3	Polsko
či	či	k8xC	či
úplné	úplný	k2eAgNnSc4d1	úplné
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
Slezanů	Slezan	k1gMnPc2	Slezan
jako	jako	k8xC	jako
samostatného	samostatný	k2eAgInSc2d1	samostatný
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Slezština	Slezština	k1gFnSc1	Slezština
(	(	kIx(	(
<g/>
lechický	lechický	k2eAgInSc1d1	lechický
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slezská	slezský	k2eAgFnSc1d1	Slezská
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
Dvojjazyčnost	dvojjazyčnost	k1gFnSc1	dvojjazyčnost
Horního	horní	k2eAgNnSc2d1	horní
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Slovanské	slovanský	k2eAgInPc1d1	slovanský
kmeny	kmen	k1gInPc1	kmen
usadivší	usadivší	k2eAgInPc1d1	usadivší
se	se	k3xPyFc4	se
na	na	k7c6	na
Slezsku	Slezsko	k1gNnSc6	Slezsko
měly	mít	k5eAaImAgFnP	mít
svůj	svůj	k3xOyFgInSc4	svůj
specifický	specifický	k2eAgInSc4d1	specifický
jazyk	jazyk	k1gInSc4	jazyk
-	-	kIx~	-
slezštinu	slezština	k1gFnSc4	slezština
<g/>
.	.	kIx.	.
</s>
<s>
Slezština	Slezština	k1gFnSc1	Slezština
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
západoslovanských	západoslovanský	k2eAgInPc2d1	západoslovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
polštinou	polština	k1gFnSc7	polština
<g/>
,	,	kIx,	,
češtinou	čeština	k1gFnSc7	čeština
<g/>
,	,	kIx,	,
slovenštinou	slovenština	k1gFnSc7	slovenština
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lužickou	lužický	k2eAgFnSc7d1	Lužická
srbštinou	srbština	k1gFnSc7	srbština
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
tedy	tedy	k9	tedy
mezi	mezi	k7c4	mezi
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
jazyků	jazyk	k1gInPc2	jazyk
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
<g/>
.	.	kIx.	.
</s>
<s>
Slezština	Slezština	k1gFnSc1	Slezština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
polského	polský	k2eAgNnSc2d1	polské
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	s	k7c7	s
60	[number]	k4	60
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
hlásilo	hlásit	k5eAaImAgNnS	hlásit
ke	k	k7c3	k
slezštině	slezština	k1gFnSc3	slezština
jako	jako	k8xC	jako
rodnému	rodný	k2eAgInSc3d1	rodný
jazyku	jazyk	k1gInSc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jazyk	jazyk	k1gInSc1	jazyk
ji	on	k3xPp3gFnSc4	on
pokládá	pokládat	k5eAaImIp3nS	pokládat
Knihovna	knihovna	k1gFnSc1	knihovna
Kongresu	kongres	k1gInSc2	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slezštině	slezština	k1gFnSc3	slezština
přiřadila	přiřadit	k5eAaPmAgFnS	přiřadit
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
kód	kód	k1gInSc4	kód
"	"	kIx"	"
<g/>
szl	szl	k?	szl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Slezska	Slezsko	k1gNnSc2	Slezsko
začali	začít	k5eAaPmAgMnP	začít
usazovat	usazovat	k5eAaImF	usazovat
také	také	k9	také
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
postupem	postup	k1gInSc7	postup
doby	doba	k1gFnSc2	doba
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
dialekt	dialekt	k1gInSc4	dialekt
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
slezskou	slezský	k2eAgFnSc4d1	Slezská
němečinu	němečina	k1gFnSc4	němečina
(	(	kIx(	(
<g/>
Schläsisch	Schläsisch	k1gMnSc1	Schläsisch
<g/>
,	,	kIx,	,
Schläsch	Schläsch	k1gMnSc1	Schläsch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
dialektu	dialekt	k1gInSc6	dialekt
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
"	"	kIx"	"
<g/>
en	en	k?	en
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Kirschen	Kirschen	k1gInSc1	Kirschen
-	-	kIx~	-
Kerscha	Kerscha	k1gFnSc1	Kerscha
<g/>
,	,	kIx,	,
essen	essen	k1gInSc1	essen
-	-	kIx~	-
assa	assa	k1gFnSc1	assa
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Hornoslezští	hornoslezský	k2eAgMnPc1d1	hornoslezský
autoři	autor	k1gMnPc1	autor
píší	psát	k5eAaImIp3nP	psát
své	své	k1gNnSc4	své
díla	dílo	k1gNnSc2	dílo
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
polštině	polština	k1gFnSc6	polština
i	i	k8xC	i
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Hornoslezská	hornoslezský	k2eAgFnSc1d1	Hornoslezská
literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
na	na	k7c4	na
první	první	k4xOgFnSc4	první
polovinu	polovina	k1gFnSc4	polovina
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
především	především	k9	především
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
deportace	deportace	k1gFnSc2	deportace
nebo	nebo	k8xC	nebo
popolštování	popolštování	k1gNnSc2	popolštování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
známých	známý	k2eAgMnPc2d1	známý
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
tvořil	tvořit	k5eAaImAgInS	tvořit
literaturu	literatura	k1gFnSc4	literatura
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Horst	Horst	k1gMnSc1	Horst
Bieniek	Bieniek	k1gMnSc1	Bieniek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
komunismu	komunismus	k1gInSc2	komunismus
bylo	být	k5eAaImAgNnS	být
potlačováno	potlačován	k2eAgNnSc4d1	potlačováno
psaní	psaní	k1gNnSc4	psaní
o	o	k7c6	o
německé	německý	k2eAgFnSc6d1	německá
nebo	nebo	k8xC	nebo
české	český	k2eAgFnSc6d1	Česká
historii	historie	k1gFnSc6	historie
v	v	k7c6	v
regionu	region	k1gInSc6	region
(	(	kIx(	(
<g/>
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
části	část	k1gFnSc6	část
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
meziválečného	meziválečný	k2eAgNnSc2d1	meziválečné
období	období	k1gNnSc2	období
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
mnoho	mnoho	k4c1	mnoho
spisovatelů	spisovatel	k1gMnPc2	spisovatel
(	(	kIx(	(
<g/>
hlavně	hlavně	k6eAd1	hlavně
<g/>
)	)	kIx)	)
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
odkrývání	odkrývání	k1gNnSc1	odkrývání
slezské	slezský	k2eAgFnSc2d1	Slezská
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
spisovatelé	spisovatel	k1gMnPc1	spisovatel
psali	psát	k5eAaImAgMnP	psát
o	o	k7c6	o
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
tradicích	tradice	k1gFnPc6	tradice
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Psané	psaný	k2eAgFnPc1d1	psaná
poezie	poezie	k1gFnPc1	poezie
ve	v	k7c6	v
slezštině	slezština	k1gFnSc6	slezština
moc	moc	k6eAd1	moc
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
však	však	k9	však
mnoho	mnoho	k4c1	mnoho
slezských	slezský	k2eAgMnPc2d1	slezský
básníků	básník	k1gMnPc2	básník
píše	psát	k5eAaImIp3nS	psát
básně	báseň	k1gFnPc4	báseň
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
takových	takový	k3xDgFnPc2	takový
autorek	autorka	k1gFnPc2	autorka
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Marta	Marta	k1gFnSc1	Marta
Fox	fox	k1gInSc1	fox
<g/>
.	.	kIx.	.
</s>
<s>
Kroje	kroj	k1gInPc1	kroj
se	se	k3xPyFc4	se
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
nosily	nosit	k5eAaImAgInP	nosit
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Bojków	Bojków	k1gFnSc1	Bojków
<g/>
)	)	kIx)	)
tradice	tradice	k1gFnSc1	tradice
kroje	kroj	k1gInSc2	kroj
vydržela	vydržet	k5eAaPmAgFnS	vydržet
částečně	částečně	k6eAd1	částečně
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
kroj	kroj	k1gInSc1	kroj
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
staromódní	staromódní	k2eAgInSc4d1	staromódní
<g/>
.	.	kIx.	.
</s>
<s>
Kroj	kroj	k1gInSc4	kroj
lidé	člověk	k1gMnPc1	člověk
rozlišovali	rozlišovat	k5eAaImAgMnP	rozlišovat
na	na	k7c4	na
všední	všední	k2eAgNnSc4d1	všední
<g/>
,	,	kIx,	,
nedělní	nedělní	k2eAgNnSc4d1	nedělní
a	a	k8xC	a
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
kroj	kroj	k1gInSc1	kroj
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
s	s	k7c7	s
pěti	pět	k4xCc2	pět
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
naškrobených	naškrobený	k2eAgFnPc2d1	naškrobená
spodnic	spodnice	k1gFnPc2	spodnice
<g/>
,	,	kIx,	,
svrchní	svrchní	k2eAgFnPc4d1	svrchní
sukně	sukně	k1gFnPc4	sukně
<g/>
,	,	kIx,	,
rukávců	rukávec	k1gInPc2	rukávec
a	a	k8xC	a
kordulky	kordulka	k1gFnSc2	kordulka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nohách	noha	k1gFnPc6	noha
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
nosily	nosit	k5eAaImAgFnP	nosit
vysoké	vysoký	k2eAgFnPc1d1	vysoká
šněrovací	šněrovací	k2eAgFnPc1d1	šněrovací
boty	bota	k1gFnPc1	bota
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
kostýmy	kostým	k1gInPc1	kostým
nošeny	nosit	k5eAaImNgInP	nosit
folklorními	folklorní	k2eAgFnPc7d1	folklorní
skupinami	skupina	k1gFnPc7	skupina
nebo	nebo	k8xC	nebo
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
<g/>
,	,	kIx,	,
galeriích	galerie	k1gFnPc6	galerie
či	či	k8xC	či
domácích	domácí	k2eAgFnPc6d1	domácí
skříních	skříň	k1gFnPc6	skříň
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
kostýmy	kostým	k1gInPc1	kostým
nošeny	nošen	k2eAgInPc1d1	nošen
na	na	k7c6	na
lidových	lidový	k2eAgFnPc6d1	lidová
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
kostýmy	kostým	k1gInPc1	kostým
již	již	k6eAd1	již
vůbec	vůbec	k9	vůbec
nenosí	nosit	k5eNaImIp3nP	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
pochází	pocházet	k5eAaImIp3nS	pocházet
mnoho	mnoho	k4c1	mnoho
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
světců	světec	k1gMnPc2	světec
protestantské	protestantský	k2eAgFnSc2d1	protestantská
i	i	k8xC	i
katolické	katolický	k2eAgFnSc2d1	katolická
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
ministrů	ministr	k1gMnPc2	ministr
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
vysokých	vysoký	k2eAgMnPc2d1	vysoký
činitelů	činitel	k1gMnPc2	činitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Hanna	Hanna	k1gFnSc1	Hanna
Reitsch	Reitscha	k1gFnPc2	Reitscha
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
v	v	k7c6	v
Jelení	jelení	k2eAgFnSc6d1	jelení
Hoře	hora	k1gFnSc6	hora
-	-	kIx~	-
1979	[number]	k4	1979
ve	v	k7c6	v
Frankfurtě	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
významná	významný	k2eAgFnSc1d1	významná
a	a	k8xC	a
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
německá	německý	k2eAgFnSc1d1	německá
pilotka	pilotka	k1gFnSc1	pilotka
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hanna	Hanna	k1gFnSc1	Hanna
Reitsch	Reitscha	k1gFnPc2	Reitscha
zdolala	zdolat	k5eAaPmAgFnS	zdolat
přes	přes	k7c4	přes
40	[number]	k4	40
rekordů	rekord	k1gInPc2	rekord
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třídách	třída	k1gFnPc6	třída
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Slezští	slezský	k2eAgMnPc1d1	slezský
spisovatelé	spisovatel	k1gMnPc1	spisovatel
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Walenty	Walent	k1gInPc1	Walent
Roździeński	Roździeńsk	k1gFnSc2	Roździeńsk
(	(	kIx(	(
<g/>
1570	[number]	k4	1570
<g/>
-	-	kIx~	-
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gerhart	Gerhart	k1gInSc1	Gerhart
Hauptmann	Hauptmanna	k1gFnPc2	Hauptmanna
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
básníkem	básník	k1gMnSc7	básník
byl	být	k5eAaImAgMnS	být
Andreas	Andreas	k1gMnSc1	Andreas
Gryphius	Gryphius	k1gMnSc1	Gryphius
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgInSc1d1	narozen
roku	rok	k1gInSc2	rok
1616	[number]	k4	1616
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Glogau	Glogaum	k1gNnSc6	Glogaum
(	(	kIx(	(
<g/>
Głogów	Głogów	k1gMnSc1	Głogów
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hlavní	hlavní	k2eAgInSc1d1	hlavní
okruh	okruh	k1gInSc1	okruh
práce	práce	k1gFnSc2	práce
bylo	být	k5eAaImAgNnS	být
téma	téma	k1gNnSc1	téma
na	na	k7c4	na
třicetiletou	třicetiletý	k2eAgFnSc4d1	třicetiletá
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
důsledky	důsledek	k1gInPc7	důsledek
na	na	k7c4	na
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
básníkem	básník	k1gMnSc7	básník
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Opitz	Opitz	k1gMnSc1	Opitz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
období	období	k1gNnSc2	období
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
tzv.	tzv.	kA	tzv.
slezské	slezský	k2eAgFnSc2d1	Slezská
básnické	básnický	k2eAgFnSc2d1	básnická
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgMnSc7d1	významný
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jakob	Jakob	k1gMnSc1	Jakob
Böhme	Böhm	k1gInSc5	Böhm
(	(	kIx(	(
<g/>
1575	[number]	k4	1575
-	-	kIx~	-
1624	[number]	k4	1624
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
Jakob	Jakob	k1gMnSc1	Jakob
Boehme	Boehm	k1gInSc5	Boehm
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
mystik	mystik	k1gMnSc1	mystik
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
slezský	slezský	k2eAgMnSc1d1	slezský
básník	básník	k1gMnSc1	básník
německé	německý	k2eAgFnSc2d1	německá
romantiky	romantika	k1gFnSc2	romantika
je	být	k5eAaImIp3nS	být
Joseph	Joseph	k1gInSc1	Joseph
von	von	k1gInSc1	von
Eichendorff	Eichendorff	k1gInSc1	Eichendorff
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
<g/>
-	-	kIx~	-
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
spisovatele	spisovatel	k1gMnPc4	spisovatel
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
řadí	řadit	k5eAaImIp3nS	řadit
Óndra	Óndra	k1gMnSc1	Óndra
Łysohorský	Łysohorský	k2eAgMnSc1d1	Łysohorský
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
-	-	kIx~	-
kromě	kromě	k7c2	kromě
poezie	poezie	k1gFnSc2	poezie
co	co	k3yRnSc4	co
napsal	napsat	k5eAaBmAgInS	napsat
také	také	k9	také
zkodifikoval	zkodifikovat	k5eAaPmAgInS	zkodifikovat
lašský	lašský	k2eAgInSc1d1	lašský
dialekt	dialekt	k1gInSc1	dialekt
spadající	spadající	k2eAgInSc1d1	spadající
pod	pod	k7c4	pod
slezštinu	slezština	k1gFnSc4	slezština
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
slezské	slezský	k2eAgMnPc4d1	slezský
malíře	malíř	k1gMnPc4	malíř
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
Adolph	Adolph	k1gInSc1	Adolph
von	von	k1gInSc4	von
Menzel	Menzel	k1gFnSc2	Menzel
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
slezským	slezský	k2eAgMnPc3d1	slezský
vědcům	vědec	k1gMnPc3	vědec
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
fyzička	fyzička	k1gFnSc1	fyzička
oceněna	ocenit	k5eAaPmNgFnS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
Maria	Maria	k1gFnSc1	Maria
Göppert-Mayer	Göppert-Mayer	k1gMnSc1	Göppert-Mayer
nebo	nebo	k8xC	nebo
doktor	doktor	k1gMnSc1	doktor
a	a	k8xC	a
bakteriolog	bakteriolog	k1gMnSc1	bakteriolog
Paul	Paul	k1gMnSc1	Paul
Ehrlich	Ehrlich	k1gMnSc1	Ehrlich
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
mají	mít	k5eAaImIp3nP	mít
Slezané	Slezan	k1gMnPc1	Slezan
13	[number]	k4	13
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
německé	německý	k2eAgFnPc4d1	německá
národnosti	národnost	k1gFnPc4	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Thierse	Thierse	k1gFnSc2	Thierse
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
Töpfer	Töpfer	k1gMnSc1	Töpfer
<g/>
,	,	kIx,	,
Manfred	Manfred	k1gMnSc1	Manfred
Kanther	Kanthra	k1gFnPc2	Kanthra
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Mende	Mend	k1gInSc5	Mend
<g/>
,	,	kIx,	,
Katja	Katja	k1gMnSc1	Katja
Ebstein	Ebstein	k1gMnSc1	Ebstein
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Taubitz	Taubitz	k1gMnSc1	Taubitz
<g/>
,	,	kIx,	,
Joachim	Joachim	k1gMnSc1	Joachim
kardinál	kardinál	k1gMnSc1	kardinál
Meisner	Meisner	k1gMnSc1	Meisner
nebo	nebo	k8xC	nebo
Dieter	Dietrum	k1gNnPc2	Dietrum
Hildebrandt	Hildebrandta	k1gFnPc2	Hildebrandta
jsou	být	k5eAaImIp3nP	být
Slezané	Slezan	k1gMnPc1	Slezan
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
reprezentaci	reprezentace	k1gFnSc6	reprezentace
jako	jako	k8xC	jako
Lukas	Lukas	k1gInSc4	Lukas
Podolski	Podolsk	k1gFnSc2	Podolsk
narozen	narozen	k2eAgInSc4d1	narozen
v	v	k7c6	v
Glivicích	Glivik	k1gInPc6	Glivik
nebo	nebo	k8xC	nebo
Miroslav	Miroslav	k1gMnSc1	Miroslav
Klose	Klose	k1gFnSc2	Klose
narozen	narodit	k5eAaPmNgInS	narodit
v	v	k7c6	v
Opolí	Opolí	k1gNnSc6	Opolí
také	také	k9	také
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
slezská	slezský	k2eAgNnPc4d1	Slezské
města	město	k1gNnPc4	město
s	s	k7c7	s
populací	populace	k1gFnSc7	populace
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
*	*	kIx~	*
Ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
</s>
