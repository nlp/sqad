<s>
Teropodi	Teropod	k1gMnPc1	Teropod
(	(	kIx(	(
<g/>
Theropoda	Theropoda	k1gMnSc1	Theropoda
<g/>
,	,	kIx,	,
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
θ	θ	k?	θ
=	=	kIx~	=
obludný	obludný	k2eAgInSc4d1	obludný
<g/>
,	,	kIx,	,
π	π	k?	π
=	=	kIx~	=
noha	noha	k1gFnSc1	noha
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
obecně	obecně	k6eAd1	obecně
všichni	všechen	k3xTgMnPc1	všechen
masožraví	masožraví	k1gMnPc1	masožraví
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
druhotně	druhotně	k6eAd1	druhotně
býložraví	býložravý	k2eAgMnPc1d1	býložravý
a	a	k8xC	a
všežraví	všežravý	k2eAgMnPc1d1	všežravý
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ptáků	pták	k1gMnPc2	pták
dodnes	dodnes	k6eAd1	dodnes
jsou	být	k5eAaImIp3nP	být
<g/>
)	)	kIx)	)
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
dvounozí	dvounohý	k2eAgMnPc1d1	dvounohý
(	(	kIx(	(
<g/>
bipední	bipednit	k5eAaPmIp3nP	bipednit
<g/>
)	)	kIx)	)
a	a	k8xC	a
patřili	patřit	k5eAaImAgMnP	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Saurischia	Saurischium	k1gNnSc2	Saurischium
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
plazopánvých	plazopánvý	k2eAgMnPc2d1	plazopánvý
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
masožravost	masožravost	k1gFnSc4	masožravost
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
křídové	křídový	k2eAgFnPc1d1	křídová
formy	forma	k1gFnPc1	forma
byly	být	k5eAaImAgFnP	být
také	také	k9	také
druhotně	druhotně	k6eAd1	druhotně
všežravé	všežravý	k2eAgNnSc1d1	všežravé
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
býložravé	býložravý	k2eAgNnSc1d1	býložravé
<g/>
.	.	kIx.	.
</s>
<s>
Teropodi	Teropod	k1gMnPc1	Teropod
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
objevují	objevovat	k5eAaImIp3nP	objevovat
během	během	k7c2	během
období	období	k1gNnSc2	období
karnu	karn	k1gInSc2	karn
(	(	kIx(	(
<g/>
svrchní	svrchní	k2eAgInSc1d1	svrchní
trias	trias	k1gInSc1	trias
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
před	před	k7c7	před
235	[number]	k4	235
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
triasu	trias	k1gInSc2	trias
představovali	představovat	k5eAaImAgMnP	představovat
jediné	jediný	k2eAgFnPc4d1	jediná
velké	velká	k1gFnPc4	velká
pozemní	pozemní	k2eAgFnSc1d1	pozemní
predátory	predátor	k1gMnPc7	predátor
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těmi	ten	k3xDgInPc7	ten
zůstali	zůstat	k5eAaPmAgMnP	zůstat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhohor	druhohory	k1gFnPc2	druhohory
před	před	k7c7	před
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgMnPc4d1	dominantní
predátory	predátor	k1gMnPc4	predátor
tedy	tedy	k9	tedy
představovali	představovat	k5eAaImAgMnP	představovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
celých	celý	k2eAgInPc2d1	celý
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
reprezentováni	reprezentovat	k5eAaImNgMnP	reprezentovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
800	[number]	k4	800
druhy	druh	k1gInPc7	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jury	jura	k1gFnSc2	jura
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
160	[number]	k4	160
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
právě	právě	k9	právě
z	z	k7c2	z
malých	malý	k2eAgInPc2d1	malý
teropodů	teropod	k1gInPc2	teropod
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
teropodů	teropod	k1gMnPc2	teropod
?	?	kIx.	?
</s>
<s>
<g/>
†	†	k?	†
<g/>
Herrerasauria	Herrerasaurium	k1gNnSc2	Herrerasaurium
†	†	k?	†
<g/>
Coelophysoidea	Coelophysoideum	k1gNnSc2	Coelophysoideum
†	†	k?	†
<g/>
Dilophosauridae	Dilophosauridae	k1gNnSc2	Dilophosauridae
†	†	k?	†
<g/>
Ceratosauria	Ceratosaurium	k1gNnSc2	Ceratosaurium
Tetanurae	Tetanurae	k1gFnPc2	Tetanurae
†	†	k?	†
<g/>
Megalosauroidea	Megalosauroidea	k1gMnSc1	Megalosauroidea
†	†	k?	†
<g/>
Carnosauria	Carnosaurium	k1gNnSc2	Carnosaurium
Coelurosauria	Coelurosaurium	k1gNnSc2	Coelurosaurium
†	†	k?	†
<g/>
Compsognathidae	Compsognathidae	k1gNnSc2	Compsognathidae
†	†	k?	†
<g/>
Tyrannosauridae	Tyrannosauridae	k1gNnSc2	Tyrannosauridae
†	†	k?	†
<g/>
Ornithomimosauria	Ornithomimosaurium	k1gNnSc2	Ornithomimosaurium
†	†	k?	†
<g/>
Alvarezsauridae	Alvarezsaurida	k1gMnSc2	Alvarezsaurida
Maniraptora	Maniraptor	k1gMnSc2	Maniraptor
†	†	k?	†
<g/>
Therizinosauria	Therizinosaurium	k1gNnPc4	Therizinosaurium
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Therizinosauroidea	Therizinosauroide	k1gInSc2	Therizinosauroide
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Scansoriopterygidae	Scansoriopterygidae	k1gInSc1	Scansoriopterygidae
†	†	k?	†
<g/>
Oviraptorosauria	Oviraptorosaurium	k1gNnSc2	Oviraptorosaurium
†	†	k?	†
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Archaeopterygidae	Archaeopterygidae	k1gFnSc1	Archaeopterygidae
†	†	k?	†
<g/>
Dromaeosauridae	Dromaeosaurida	k1gMnSc2	Dromaeosaurida
†	†	k?	†
<g/>
Troodontidae	Troodontida	k1gMnSc2	Troodontida
Avialae	Aviala	k1gMnSc2	Aviala
(	(	kIx(	(
<g/>
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
blízcí	blízký	k2eAgMnPc1d1	blízký
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
)	)	kIx)	)
†	†	k?	†
<g/>
Omnivoropterygidae	Omnivoropterygidae	k1gInSc1	Omnivoropterygidae
†	†	k?	†
<g/>
Confuciusornithidae	Confuciusornithida	k1gFnSc2	Confuciusornithida
†	†	k?	†
<g/>
Enantiornithes	Enantiornithes	k1gMnSc1	Enantiornithes
Euornithes	Euornithes	k1gMnSc1	Euornithes
†	†	k?	†
<g/>
Yanornithiformes	Yanornithiformes	k1gMnSc1	Yanornithiformes
†	†	k?	†
<g/>
Hesperornithes	Hesperornithes	k1gInSc1	Hesperornithes
Aves	Aves	k1gInSc1	Aves
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgMnPc1d1	moderní
ptáci	pták	k1gMnPc1	pták
<g/>
)	)	kIx)	)
Kladogram	Kladogram	k1gInSc1	Kladogram
zobrazující	zobrazující	k2eAgFnSc1d1	zobrazující
příbuznost	příbuznost	k1gFnSc1	příbuznost
mezi	mezi	k7c7	mezi
ranými	raný	k2eAgInPc7d1	raný
teropody	teropod	k1gInPc7	teropod
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
základními	základní	k2eAgFnPc7d1	základní
skupinami	skupina	k1gFnPc7	skupina
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Dinosauří	dinosauří	k2eAgInPc1d1	dinosauří
rekordy	rekord	k1gInPc1	rekord
a	a	k8xC	a
Velikost	velikost	k1gFnSc1	velikost
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dobře	dobře	k6eAd1	dobře
známým	známý	k2eAgMnSc7d1	známý
teropodem	teropod	k1gMnSc7	teropod
je	být	k5eAaImIp3nS	být
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
a	a	k8xC	a
Carcharodontosaurus	Carcharodontosaurus	k1gMnSc1	Carcharodontosaurus
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
kolem	kolem	k7c2	kolem
8	[number]	k4	8
tun	tuna	k1gFnPc2	tuna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k9	ještě
větší	veliký	k2eAgMnSc1d2	veliký
byl	být	k5eAaImAgInS	být
však	však	k9	však
Spinosaurus	Spinosaurus	k1gInSc1	Spinosaurus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mohl	moct	k5eAaImAgInS	moct
údajně	údajně	k6eAd1	údajně
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
18	[number]	k4	18
metrové	metrový	k2eAgFnSc2d1	metrová
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
kolem	kolem	k7c2	kolem
12	[number]	k4	12
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
lebku	lebka	k1gFnSc4	lebka
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgMnSc1d1	dosahující
snad	snad	k9	snad
až	až	k9	až
2,4	[number]	k4	2,4
metru	metr	k1gInSc2	metr
(	(	kIx(	(
<g/>
prokazatelně	prokazatelně	k6eAd1	prokazatelně
však	však	k9	však
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
1,75	[number]	k4	1,75
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
stopy	stopa	k1gFnPc1	stopa
teropoda	teropoda	k1gFnSc1	teropoda
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
měřily	měřit	k5eAaImAgFnP	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
90	[number]	k4	90
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
osm	osm	k4xCc4	osm
teropodů	teropod	k1gInPc2	teropod
zřejmě	zřejmě	k6eAd1	zřejmě
přesáhlo	přesáhnout	k5eAaPmAgNnS	přesáhnout
délku	délka	k1gFnSc4	délka
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejmenším	malý	k2eAgInSc7d3	nejmenší
neptačím	ptačí	k2eNgInSc7d1	ptačí
teropodem	teropod	k1gInSc7	teropod
(	(	kIx(	(
<g/>
a	a	k8xC	a
neptačím	ptačit	k5eNaImIp1nS	ptačit
dinosaurem	dinosaurus	k1gMnSc7	dinosaurus
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Scansoriopteryx	Scansoriopteryx	k1gInSc1	Scansoriopteryx
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Epidendrosaurus	Epidendrosaurus	k1gInSc4	Epidendrosaurus
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
synonyma	synonymum	k1gNnPc4	synonymum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
se	se	k3xPyFc4	se
však	však	k9	však
jednat	jednat	k5eAaImF	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
mládě	mládě	k1gNnSc4	mládě
nějakého	nějaký	k3yIgMnSc2	nějaký
většího	veliký	k2eAgMnSc2d2	veliký
teropoda	teropod	k1gMnSc2	teropod
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ještě	ještě	k9	ještě
menší	malý	k2eAgMnPc1d2	menší
jsou	být	k5eAaImIp3nP	být
mnozí	mnohý	k2eAgMnPc1d1	mnohý
současní	současný	k2eAgMnPc1d1	současný
ptáci	pták	k1gMnPc1	pták
-	-	kIx~	-
nejmenším	malý	k2eAgInSc7d3	nejmenší
teropodem	teropod	k1gInSc7	teropod
a	a	k8xC	a
tedy	tedy	k9	tedy
dinosaurem	dinosaurus	k1gMnSc7	dinosaurus
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
přírodě	příroda	k1gFnSc6	příroda
drobný	drobný	k2eAgMnSc1d1	drobný
kolibřík	kolibřík	k1gMnSc1	kolibřík
kalypta	kalypt	k1gInSc2	kalypt
nejmenší	malý	k2eAgMnSc1d3	nejmenší
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
kolem	kolem	k7c2	kolem
5	[number]	k4	5
cm	cm	kA	cm
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
několika	několik	k4yIc2	několik
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
známé	známý	k2eAgMnPc4d1	známý
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
nálezy	nález	k1gInPc4	nález
fosílií	fosílie	k1gFnPc2	fosílie
neptačích	ptačí	k2eNgMnPc2d1	neptačí
teropodních	teropodní	k2eAgMnPc2d1	teropodní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
otisk	otisk	k1gInSc4	otisk
stopy	stopa	k1gFnSc2	stopa
malého	malý	k2eAgMnSc2d1	malý
teropoda	teropod	k1gMnSc2	teropod
(	(	kIx(	(
<g/>
možná	možná	k9	možná
ale	ale	k8xC	ale
jen	jen	k9	jen
dinosauromorfa	dinosauromorf	k1gMnSc4	dinosauromorf
<g/>
)	)	kIx)	)
z	z	k7c2	z
lomu	lom	k1gInSc2	lom
U	u	k7c2	u
Devíti	devět	k4xCc2	devět
křížů	kříž	k1gInPc2	kříž
u	u	k7c2	u
Červeného	Červeného	k2eAgInSc2d1	Červeného
Kostelce	Kostelec	k1gInSc2	Kostelec
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
potom	potom	k6eAd1	potom
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
zub	zub	k1gInSc4	zub
jurského	jurský	k2eAgMnSc2d1	jurský
teropoda	teropod	k1gMnSc2	teropod
z	z	k7c2	z
lokality	lokalita	k1gFnSc2	lokalita
Švédské	švédský	k2eAgFnSc2d1	švédská
šance	šance	k1gFnSc2	šance
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Rowe	Rowe	k1gFnSc1	Rowe
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
,	,	kIx,	,
and	and	k?	and
Gauthier	Gauthier	k1gInSc1	Gauthier
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ceratosauria	Ceratosaurium	k1gNnPc4	Ceratosaurium
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pp	Pp	k1gMnSc1	Pp
<g/>
.	.	kIx.	.
151	[number]	k4	151
<g/>
-	-	kIx~	-
<g/>
168	[number]	k4	168
in	in	k?	in
Weishampel	Weishampel	k1gMnSc1	Weishampel
<g/>
,	,	kIx,	,
D.	D.	kA	D.
B.	B.	kA	B.
<g/>
,	,	kIx,	,
Dodson	Dodson	k1gNnSc1	Dodson
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
and	and	k?	and
Osmólska	Osmólska	k1gFnSc1	Osmólska
<g/>
,	,	kIx,	,
H.	H.	kA	H.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
,	,	kIx,	,
University	universita	k1gFnSc2	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
Berkley	Berklea	k1gFnPc1	Berklea
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Carrano	Carrana	k1gFnSc5	Carrana
<g/>
,	,	kIx,	,
M.	M.	kA	M.
T.	T.	kA	T.
<g/>
,	,	kIx,	,
Sampson	Sampson	k1gNnSc1	Sampson
<g/>
,	,	kIx,	,
S.	S.	kA	S.
D.	D.	kA	D.
and	and	k?	and
Forster	Forster	k1gInSc1	Forster
<g/>
,	,	kIx,	,
C.	C.	kA	C.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
osteology	osteolog	k1gMnPc7	osteolog
of	of	k?	of
Masiakasaurus	Masiakasaurus	k1gInSc4	Masiakasaurus
knopfleri	knopfler	k1gFnSc2	knopfler
<g/>
,	,	kIx,	,
a	a	k8xC	a
small	smalnout	k5eAaPmAgMnS	smalnout
abelisauroid	abelisauroid	k1gInSc4	abelisauroid
(	(	kIx(	(
<g/>
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
:	:	kIx,	:
Theropoda	Theropoda	k1gFnSc1	Theropoda
<g/>
)	)	kIx)	)
from	from	k1gInSc1	from
the	the	k?	the
Late	lat	k1gInSc5	lat
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
of	of	k?	of
Madagascar	Madagascar	k1gMnSc1	Madagascar
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Journal	Journal	k1gFnSc1	Journal
of	of	k?	of
Vertebrate	Vertebrat	k1gInSc5	Vertebrat
Paleontology	paleontolog	k1gMnPc7	paleontolog
<g/>
,	,	kIx,	,
22	[number]	k4	22
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
510	[number]	k4	510
<g/>
-	-	kIx~	-
<g/>
534	[number]	k4	534
<g/>
.	.	kIx.	.
</s>
<s>
Ostrom	Ostrom	k1gInSc1	Ostrom
<g/>
,	,	kIx,	,
J.H.	J.H.	k1gFnSc1	J.H.
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Osteology	Osteolog	k1gMnPc4	Osteolog
of	of	k?	of
Deinonychus	Deinonychus	k1gMnSc1	Deinonychus
antirrhopus	antirrhopus	k1gMnSc1	antirrhopus
<g/>
,	,	kIx,	,
an	an	k?	an
unusual	unusual	k1gInSc1	unusual
theropod	theropoda	k1gFnPc2	theropoda
from	from	k1gMnSc1	from
the	the	k?	the
Lower	Lower	k1gMnSc1	Lower
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
of	of	k?	of
Montana	Montana	k1gFnSc1	Montana
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Peabody	Peabod	k1gInPc1	Peabod
Museum	museum	k1gNnSc1	museum
Natural	Natural	k?	Natural
History	Histor	k1gMnPc4	Histor
Bulletin	bulletin	k1gInSc1	bulletin
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
165	[number]	k4	165
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
S.	S.	kA	S.
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Predatory	Predator	k1gInPc1	Predator
Dinosaurs	Dinosaursa	k1gFnPc2	Dinosaursa
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Simon	Simon	k1gMnSc1	Simon
and	and	k?	and
Schuster	Schuster	k1gInSc1	Schuster
Co	co	k3yRnSc1	co
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
671	[number]	k4	671
<g/>
-	-	kIx~	-
<g/>
61946	[number]	k4	61946
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Dingus	Dingus	k1gInSc1	Dingus
<g/>
,	,	kIx,	,
L.	L.	kA	L.
and	and	k?	and
Rowe	Rowe	k1gInSc1	Rowe
<g/>
,	,	kIx,	,
T.	T.	kA	T.
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Mistaken	Mistaken	k2eAgInSc4d1	Mistaken
Extinction	Extinction	k1gInSc4	Extinction
<g/>
:	:	kIx,	:
Dinosaur	Dinosaura	k1gFnPc2	Dinosaura
Evolution	Evolution	k1gInSc1	Evolution
and	and	k?	and
the	the	k?	the
Origin	Origin	k1gInSc1	Origin
of	of	k?	of
Birds	Birds	k1gInSc1	Birds
<g/>
.	.	kIx.	.
</s>
<s>
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
S.	S.	kA	S.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dinosaurs	Dinosaurs	k1gInSc1	Dinosaurs
of	of	k?	of
the	the	k?	the
Air	Air	k1gFnSc1	Air
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Evolution	Evolution	k1gInSc1	Evolution
and	and	k?	and
Loss	Loss	k1gInSc1	Loss
of	of	k?	of
Flight	Flight	k1gInSc1	Flight
in	in	k?	in
Dinosaurs	Dinosaurs	k1gInSc1	Dinosaurs
and	and	k?	and
Birds	Birds	k1gInSc1	Birds
<g/>
.	.	kIx.	.
</s>
<s>
Baltimore	Baltimore	k1gInSc1	Baltimore
<g/>
:	:	kIx,	:
Johns	Johns	k1gInSc1	Johns
Hopkins	Hopkinsa	k1gFnPc2	Hopkinsa
University	universita	k1gFnSc2	universita
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
472	[number]	k4	472
pp	pp	k?	pp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8018	[number]	k4	8018
<g/>
-	-	kIx~	-
<g/>
6763	[number]	k4	6763
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Rauhut	Rauhut	k1gMnSc1	Rauhut
<g/>
,	,	kIx,	,
O.W.	O.W.	k1gMnSc1	O.W.
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Interrelationships	Interrelationships	k1gInSc1	Interrelationships
and	and	k?	and
Evolution	Evolution	k1gInSc1	Evolution
of	of	k?	of
Basal	Basal	k1gInSc1	Basal
Theropod	Theropoda	k1gFnPc2	Theropoda
Dinosaurs	Dinosaursa	k1gFnPc2	Dinosaursa
<g/>
.	.	kIx.	.
</s>
<s>
Blackwell	Blackwell	k1gInSc1	Blackwell
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
213	[number]	k4	213
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
90170279	[number]	k4	90170279
<g/>
X	X	kA	X
Matthew	Matthew	k1gFnPc2	Matthew
<g/>
,	,	kIx,	,
W.	W.	kA	W.
D.	D.	kA	D.
<g/>
,	,	kIx,	,
and	and	k?	and
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
B.	B.	kA	B.
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
family	famila	k1gFnSc2	famila
Deinodontidae	Deinodontidae	k1gFnSc1	Deinodontidae
<g/>
,	,	kIx,	,
with	with	k1gInSc1	with
notice	notice	k?	notice
of	of	k?	of
a	a	k8xC	a
new	new	k?	new
genus	genus	k1gMnSc1	genus
from	from	k1gMnSc1	from
the	the	k?	the
Cretaceous	Cretaceous	k1gMnSc1	Cretaceous
of	of	k?	of
Alberta	Alberta	k1gFnSc1	Alberta
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bulletin	bulletin	k1gInSc1	bulletin
of	of	k?	of
the	the	k?	the
American	American	k1gMnSc1	American
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
<g/>
,	,	kIx,	,
46	[number]	k4	46
<g/>
:	:	kIx,	:
367	[number]	k4	367
<g/>
-	-	kIx~	-
<g/>
385	[number]	k4	385
<g/>
.	.	kIx.	.
</s>
<s>
Weishampel	Weishampel	k1gMnSc1	Weishampel
<g/>
,	,	kIx,	,
D.B.	D.B.	k1gMnSc1	D.B.
<g/>
,	,	kIx,	,
Dodson	Dodson	k1gMnSc1	Dodson
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
and	and	k?	and
Osmólska	Osmólska	k1gFnSc1	Osmólska
<g/>
,	,	kIx,	,
H.	H.	kA	H.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Dinosauria	Dinosaurium	k1gNnSc2	Dinosaurium
<g/>
,	,	kIx,	,
Second	Second	k1gInSc1	Second
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
861	[number]	k4	861
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
Socha	Socha	k1gMnSc1	Socha
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Úžasný	úžasný	k2eAgInSc1d1	úžasný
svět	svět	k1gInSc1	svět
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Teropodi	Teropod	k1gMnPc1	Teropod
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
pravěkých	pravěký	k2eAgMnPc6d1	pravěký
predátorech	predátor	k1gMnPc6	predátor
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
největších	veliký	k2eAgFnPc2d3	veliký
stop	stopa	k1gFnPc2	stopa
teropodů	teropod	k1gInPc2	teropod
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
Osel	osít	k5eAaPmAgInS	osít
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
