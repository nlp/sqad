<s>
Největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dobře	dobře	k6eAd1	dobře
známým	známý	k2eAgMnSc7d1	známý
teropodem	teropod	k1gMnSc7	teropod
je	být	k5eAaImIp3nS	být
Giganotosaurus	Giganotosaurus	k1gMnSc1	Giganotosaurus
a	a	k8xC	a
Carcharodontosaurus	Carcharodontosaurus	k1gMnSc1	Carcharodontosaurus
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
kolem	kolem	k7c2	kolem
8	[number]	k4	8
tun	tuna	k1gFnPc2	tuna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k9	ještě
větší	veliký	k2eAgMnSc1d2	veliký
byl	být	k5eAaImAgInS	být
však	však	k9	však
Spinosaurus	Spinosaurus	k1gInSc1	Spinosaurus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mohl	moct	k5eAaImAgInS	moct
údajně	údajně	k6eAd1	údajně
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
18	[number]	k4	18
metrové	metrový	k2eAgFnSc2d1	metrová
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
kolem	kolem	k7c2	kolem
12	[number]	k4	12
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
