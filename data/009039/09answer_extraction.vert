římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
od	od	k7c2	od
1531	[number]	k4	1531
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
od	od	k7c2	od
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
