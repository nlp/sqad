<s>
Vietnamský	vietnamský	k2eAgInSc1d1
dong	dong	k1gInSc1
</s>
<s>
Vietnamský	vietnamský	k2eAgInSc1d1
dongĐồ	dongĐồ	k1gInSc1
(	(	kIx(
<g/>
vietnamsky	vietnamsky	k6eAd1
<g/>
)	)	kIx)
Bankovka	bankovka	k1gFnSc1
50	#num#	k4
dongůZemě	dongůZemě	k6eAd1
</s>
<s>
Vietnam	Vietnam	k1gInSc1
Vietnam	Vietnam	k1gInSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
VND	VND	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
2,7	2,7	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2016	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
₫	₫	k?
Mince	mince	k1gFnSc1
</s>
<s>
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
a	a	k8xC
5000	#num#	k4
dong	dong	k1gInSc1
Bankovky	bankovka	k1gFnSc2
</s>
<s>
1000	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
5000	#num#	k4
<g/>
,	,	kIx,
10000	#num#	k4
<g/>
,	,	kIx,
20000	#num#	k4
<g/>
,	,	kIx,
50000	#num#	k4
<g/>
,	,	kIx,
100000	#num#	k4
<g/>
,	,	kIx,
200000	#num#	k4
a	a	k8xC
500000	#num#	k4
dong	dong	k1gInSc1
</s>
<s>
Vietnamský	vietnamský	k2eAgInSc1d1
dong	dong	k1gInSc1
(	(	kIx(
<g/>
vietnamsky	vietnamsky	k6eAd1
Đồ	Đồ	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
měna	měna	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
Státní	státní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Vietnamu	Vietnam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
symbol	symbol	k1gInSc1
je	být	k5eAaImIp3nS
₫	₫	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedné	jeden	k4xCgFnSc3
koruně	koruna	k1gFnSc3
odpovídá	odpovídat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1000	#num#	k4
₫	₫	k?
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
jména	jméno	k1gNnSc2
</s>
<s>
Dong	dong	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
vietnamsky	vietnamsky	k6eAd1
měď	měď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
v	v	k7c6
době	doba	k1gFnSc6
Francouzské	francouzský	k2eAgFnSc2d1
Indočíny	Indočína	k1gFnSc2
používal	používat	k5eAaImAgInS
francouzský	francouzský	k2eAgInSc1d1
indočínský	indočínský	k2eAgInSc1d1
piastr	piastr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
měděný	měděný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
Dnes	dnes	k6eAd1
platí	platit	k5eAaImIp3nS
mince	mince	k1gFnPc4
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
200	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
500	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
₫	₫	k?
a	a	k8xC
5000	#num#	k4
<g/>
₫	₫	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
mince	mince	k1gFnSc1
mezi	mezi	k7c7
obyvateli	obyvatel	k1gMnPc7
příliš	příliš	k6eAd1
neujaly	ujmout	k5eNaPmAgFnP
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
přestaly	přestat	k5eAaPmAgInP
být	být	k5eAaImF
Státní	státní	k2eAgFnSc7d1
bankou	banka	k1gFnSc7
Vietnamu	Vietnam	k1gInSc2
vydávány	vydáván	k2eAgInPc4d1
a	a	k8xC
dnes	dnes	k6eAd1
již	již	k6eAd1
prakticky	prakticky	k6eAd1
nejsou	být	k5eNaImIp3nP
v	v	k7c6
oběhu	oběh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
platí	platit	k5eAaImIp3nS
bankovky	bankovka	k1gFnPc4
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
2	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
5	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
10	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
20	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
a	a	k8xC
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
dále	daleko	k6eAd2
platí	platit	k5eAaImIp3nS
také	také	k9
prakticky	prakticky	k6eAd1
nepoužívané	používaný	k2eNgFnPc4d1
bankovky	bankovka	k1gFnPc4
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
100	#num#	k4
<g/>
₫	₫	k?
<g/>
,	,	kIx,
200	#num#	k4
<g/>
₫	₫	k?
a	a	k8xC
500	#num#	k4
<g/>
₫	₫	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
všech	všecek	k3xTgFnPc6
bankovkách	bankovka	k1gFnPc6
je	být	k5eAaImIp3nS
vyobrazen	vyobrazen	k2eAgInSc1d1
Ho	on	k3xPp3gInSc2
Či	či	k8xC
Minh	Minh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vietnamský	vietnamský	k2eAgInSc4d1
dong	dong	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Asie	Asie	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
Střední	střední	k2eAgFnSc2d1
</s>
<s>
Kazachstánský	kazachstánský	k2eAgInSc1d1
tenge	tenge	k1gInSc1
•	•	k?
Kyrgyzský	kyrgyzský	k2eAgInSc1d1
som	soma	k1gFnPc2
•	•	k?
Tádžický	tádžický	k2eAgInSc1d1
somoni	somoň	k1gFnSc3
•	•	k?
Turkmenský	turkmenský	k2eAgMnSc1d1
manat	manat	k2eAgMnSc1d1
•	•	k?
Uzbecký	uzbecký	k2eAgInSc1d1
sum	suma	k1gFnPc2
Jihozápadní	jihozápadní	k2eAgNnSc4d1
</s>
<s>
Abchazský	abchazský	k2eAgInSc1d1
apsar	apsar	k1gInSc1
•	•	k?
Arménský	arménský	k2eAgInSc1d1
dram	drama	k1gFnPc2
•	•	k?
Ázerbájdžánský	ázerbájdžánský	k2eAgInSc1d1
manat	manat	k1gInSc1
•	•	k?
Bahrajnský	Bahrajnský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Irácký	irácký	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Íránský	íránský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
•	•	k?
Jemenský	jemenský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Jordánský	jordánský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Katarský	katarský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Kuvajtský	kuvajtský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Libanonská	libanonský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Ománský	ománský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Saúdský	saúdský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
</s>
<s>
Afghánský	afghánský	k2eAgInSc1d1
afghání	afghání	k1gNnSc6
•	•	k?
Bangladéšská	bangladéšský	k2eAgFnSc1d1
taka	taka	k1gFnSc1
•	•	k?
Bhútánský	bhútánský	k2eAgInSc1d1
ngultrum	ngultrum	k1gNnSc1
•	•	k?
Indická	indický	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Maledivská	maledivský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Nepálská	nepálský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Pákistánská	pákistánský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Srílanská	srílanský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
jüan	jüan	k1gInSc1
•	•	k?
Hongkongský	hongkongský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
•	•	k?
Jihokorejský	jihokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Macajská	macajský	k2eAgFnSc1d1
pataca	pataca	k1gFnSc1
•	•	k?
Mongolský	mongolský	k2eAgInSc1d1
tugrik	tugrik	k1gInSc1
•	•	k?
Tchajwanský	tchajwanský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
</s>
<s>
Brunejský	brunejský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Filipínské	filipínský	k2eAgNnSc4d1
peso	peso	k1gNnSc4
•	•	k?
Indonéská	indonéský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Kambodžský	kambodžský	k2eAgInSc1d1
riel	riel	k1gInSc1
•	•	k?
Laoský	laoský	k2eAgInSc1d1
kip	kip	k?
•	•	k?
Malajsijský	malajsijský	k2eAgInSc1d1
ringgit	ringgit	k1gInSc1
•	•	k?
Myanmarský	Myanmarský	k2eAgInSc1d1
kyat	kyat	k1gInSc1
•	•	k?
Severokorejský	severokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Singapurský	singapurský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Thajský	thajský	k2eAgInSc1d1
baht	baht	k1gInSc1
•	•	k?
Vietnamský	vietnamský	k2eAgInSc1d1
dong	dong	k1gInSc1
•	•	k?
Východotimorské	Východotimorský	k2eAgFnPc4d1
centavové	centavový	k2eAgFnPc4d1
mince	mince	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Vietnam	Vietnam	k1gInSc1
</s>
