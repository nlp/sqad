<p>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gMnSc1	Cervus
elaphus	elaphus	k1gMnSc1	elaphus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jelenovitých	jelenovití	k1gMnPc2	jelenovití
(	(	kIx(	(
<g/>
Cervidae	Cervidae	k1gNnSc1	Cervidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
,	,	kIx,	,
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
izolovaně	izolovaně	k6eAd1	izolovaně
také	také	k9	také
na	na	k7c6	na
území	území	k1gNnSc6	území
mezi	mezi	k7c7	mezi
Marokem	Maroko	k1gNnSc7	Maroko
a	a	k8xC	a
Tuniskem	Tunisko	k1gNnSc7	Tunisko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
jelena	jelen	k1gMnSc2	jelen
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Člověkem	člověk	k1gMnSc7	člověk
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
zavlečen	zavlečen	k2eAgMnSc1d1	zavlečen
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
např.	např.	kA	např.
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
století	století	k1gNnSc4	století
byl	být	k5eAaImAgMnS	být
jelen	jelen	k1gMnSc1	jelen
evropský	evropský	k2eAgMnSc1d1	evropský
velmi	velmi	k6eAd1	velmi
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
lovnou	lovný	k2eAgFnSc7d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
na	na	k7c6	na
značné	značný	k2eAgFnSc6d1	značná
části	část	k1gFnSc6	část
jeho	jeho	k3xOp3gInSc2	jeho
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
platí	platit	k5eAaImIp3nS	platit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
početnost	početnost	k1gFnSc1	početnost
často	často	k6eAd1	často
viditelně	viditelně	k6eAd1	viditelně
oslabena	oslaben	k2eAgFnSc1d1	oslabena
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
zpětné	zpětný	k2eAgFnSc3d1	zpětná
reintrodukci	reintrodukce	k1gFnSc3	reintrodukce
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
mnoha	mnoho	k4c3	mnoho
ochranářským	ochranářský	k2eAgFnPc3d1	ochranářská
opatřením	opatření	k1gNnSc7	opatření
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
hojném	hojný	k2eAgInSc6d1	hojný
počtu	počet	k1gInSc6	počet
i	i	k8xC	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
evropský	evropský	k2eAgMnSc1d1	evropský
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
(	(	kIx(	(
<g/>
nejmohutnější	mohutný	k2eAgMnPc4d3	nejmohutnější
<g/>
)	)	kIx)	)
zástupce	zástupce	k1gMnPc4	zástupce
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
175	[number]	k4	175
<g/>
–	–	k?	–
<g/>
230	[number]	k4	230
cm	cm	kA	cm
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
160	[number]	k4	160
<g/>
–	–	k?	–
<g/>
240	[number]	k4	240
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
samcům	samec	k1gMnPc3	samec
značně	značně	k6eAd1	značně
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
160	[number]	k4	160
<g/>
–	–	k?	–
<g/>
210	[number]	k4	210
cm	cm	kA	cm
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnPc1	hmotnost
mezi	mezi	k7c7	mezi
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
přitom	přitom	k6eAd1	přitom
měří	měřit	k5eAaImIp3nS	měřit
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnSc2	výška
120	[number]	k4	120
až	až	k9	až
150	[number]	k4	150
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
i	i	k8xC	i
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
viditelně	viditelně	k6eAd1	viditelně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
(	(	kIx(	(
<g/>
C.	C.	kA	C.
e.	e.	k?	e.
elaphus	elaphus	k1gInSc1	elaphus
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
500	[number]	k4	500
kg	kg	kA	kg
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
jeleni	jelen	k1gMnPc1	jelen
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
méně	málo	k6eAd2	málo
příznivých	příznivý	k2eAgFnPc6d1	příznivá
podmínkách	podmínka	k1gFnPc6	podmínka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
poddruh	poddruh	k1gInSc1	poddruh
z	z	k7c2	z
Korsiky	Korsika	k1gFnSc2	Korsika
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
pouhých	pouhý	k2eAgFnPc2d1	pouhá
70	[number]	k4	70
cm	cm	kA	cm
a	a	k8xC	a
vážit	vážit	k5eAaImF	vážit
sotva	sotva	k6eAd1	sotva
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
má	mít	k5eAaImIp3nS	mít
jelení	jelení	k2eAgFnSc1d1	jelení
srst	srst	k1gFnSc4	srst
obvykle	obvykle	k6eAd1	obvykle
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
s	s	k7c7	s
rudějším	rudý	k2eAgInSc7d2	rudější
nádechem	nádech	k1gInSc7	nádech
a	a	k8xC	a
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
navíc	navíc	k6eAd1	navíc
patrná	patrný	k2eAgFnSc1d1	patrná
i	i	k8xC	i
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
srst	srst	k1gFnSc1	srst
na	na	k7c6	na
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1	zimní
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
šedohnědá	šedohnědý	k2eAgFnSc1d1	šedohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc4	mládě
jelena	jelen	k1gMnSc2	jelen
evropského	evropský	k2eAgMnSc2d1	evropský
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
skvrnitým	skvrnitý	k2eAgNnSc7d1	skvrnité
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
C.	C.	kA	C.
e.	e.	k?	e.
barbarus	barbarus	k1gMnSc1	barbarus
z	z	k7c2	z
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
)	)	kIx)	)
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
i	i	k9	i
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zbarvením	zbarvení	k1gNnSc7	zbarvení
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
odchylky	odchylka	k1gFnPc1	odchylka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jelen	jelen	k1gMnSc1	jelen
karpatský	karpatský	k2eAgMnSc1d1	karpatský
(	(	kIx(	(
<g/>
C.	C.	kA	C.
e.	e.	k?	e.
montanus	montanus	k1gInSc1	montanus
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
srsti	srst	k1gFnSc6	srst
patrné	patrný	k2eAgInPc4d1	patrný
šedavé	šedavý	k2eAgInPc4d1	šedavý
tóny	tón	k1gInPc4	tón
a	a	k8xC	a
nebývá	bývat	k5eNaImIp3nS	bývat
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
hříva	hříva	k1gFnSc1	hříva
<g/>
,	,	kIx,	,
u	u	k7c2	u
západoevropských	západoevropský	k2eAgMnPc2d1	západoevropský
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
C.	C.	kA	C.
e.	e.	k?	e.
hippelaphus	hippelaphus	k1gInSc1	hippelaphus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zbarvení	zbarvení	k1gNnSc1	zbarvení
červenější	červený	k2eAgNnSc1d2	červenější
a	a	k8xC	a
hříva	hříva	k1gFnSc1	hříva
více	hodně	k6eAd2	hodně
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Jeleni	jelen	k1gMnPc1	jelen
z	z	k7c2	z
polopouštních	polopouštní	k2eAgFnPc2d1	polopouštní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jelen	jelen	k1gMnSc1	jelen
bucharský	bucharský	k2eAgMnSc1d1	bucharský
(	(	kIx(	(
<g/>
C.	C.	kA	C.
e.	e.	k?	e.
bactrianus	bactrianus	k1gInSc1	bactrianus
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
zbarvení	zbarvení	k1gNnSc4	zbarvení
spíše	spíše	k9	spíše
plavé	plavý	k2eAgNnSc1d1	plavé
nebo	nebo	k8xC	nebo
žlutavé	žlutavý	k2eAgNnSc1d1	žlutavé
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
poddruhů	poddruh	k1gInPc2	poddruh
jsou	být	k5eAaImIp3nP	být
krční	krční	k2eAgFnPc1d1	krční
partie	partie	k1gFnPc1	partie
výrazně	výrazně	k6eAd1	výrazně
tmavší	tmavý	k2eAgFnPc1d2	tmavší
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
několik	několik	k4yIc1	několik
barvených	barvený	k2eAgFnPc2d1	barvená
mutací	mutace	k1gFnPc2	mutace
jelení	jelení	k2eAgFnSc2d1	jelení
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
jeleni	jelen	k1gMnPc1	jelen
bílí	bílý	k2eAgMnPc1d1	bílý
<g/>
,	,	kIx,	,
chovaní	chovaný	k2eAgMnPc1d1	chovaný
např.	např.	kA	např.
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
Žehušice	Žehušice	k1gFnSc2	Žehušice
poblíž	poblíž	k6eAd1	poblíž
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
jeleni	jelen	k1gMnPc1	jelen
nejsou	být	k5eNaImIp3nP	být
albíni	albín	k1gMnPc1	albín
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mají	mít	k5eAaImIp3nP	mít
pigmentované	pigmentovaný	k2eAgFnPc1d1	pigmentovaná
(	(	kIx(	(
<g/>
modré	modrý	k2eAgFnPc1d1	modrá
či	či	k8xC	či
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
)	)	kIx)	)
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
,	,	kIx,	,
geneticky	geneticky	k6eAd1	geneticky
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
kavkazským	kavkazský	k2eAgMnPc3d1	kavkazský
a	a	k8xC	a
severoíránským	severoíránský	k2eAgMnPc3d1	severoíránský
jelenům	jelen	k1gMnPc3	jelen
(	(	kIx(	(
<g/>
poddruh	poddruh	k1gInSc1	poddruh
C.	C.	kA	C.
e.	e.	k?	e.
maral	maral	k1gMnSc1	maral
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
první	první	k4xOgMnPc1	první
bílí	bílý	k2eAgMnPc1d1	bílý
jeleni	jelen	k1gMnPc1	jelen
dostali	dostat	k5eAaPmAgMnP	dostat
počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
věnoval	věnovat	k5eAaImAgMnS	věnovat
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
císaři	císař	k1gMnSc3	císař
Karlu	Karel	k1gMnSc3	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
je	být	k5eAaImIp3nS	být
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
poblíž	poblíž	k7c2	poblíž
Brandýsa	Brandýs	k1gInSc2	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
zásilka	zásilka	k1gFnSc1	zásilka
přišla	přijít	k5eAaPmAgFnS	přijít
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
a	a	k8xC	a
převzali	převzít	k5eAaPmAgMnP	převzít
ji	on	k3xPp3gFnSc4	on
příslušníci	příslušník	k1gMnPc1	příslušník
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
českých	český	k2eAgInPc2d1	český
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
<g/>
:	:	kIx,	:
knížata	kníže	k1gMnPc1wR	kníže
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
<g/>
,	,	kIx,	,
Schwarzenberg	Schwarzenberg	k1gInSc1	Schwarzenberg
<g/>
,	,	kIx,	,
hrabata	hrabě	k1gNnPc1	hrabě
Kinský	Kinský	k1gMnSc1	Kinský
a	a	k8xC	a
Černín	Černín	k1gMnSc1	Černín
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
však	však	k9	však
přežila	přežít	k5eAaPmAgNnP	přežít
jen	jen	k6eAd1	jen
Valdštejnům	Valdštejn	k1gInPc3	Valdštejn
a	a	k8xC	a
Kinským	Kinská	k1gFnPc3	Kinská
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejnové	Valdštejn	k1gMnPc1	Valdštejn
je	on	k3xPp3gMnPc4	on
chovali	chovat	k5eAaImAgMnP	chovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
v	v	k7c6	v
Mnichově	mnichův	k2eAgNnSc6d1	Mnichovo
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Kinští	Kinský	k1gMnPc1	Kinský
bílé	bílý	k2eAgFnSc2d1	bílá
jeleny	jelena	k1gFnSc2	jelena
vysadili	vysadit	k5eAaPmAgMnP	vysadit
v	v	k7c6	v
Chlumci	Chlumec	k1gInSc6	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
je	být	k5eAaImIp3nS	být
však	však	k9	však
věnovali	věnovat	k5eAaPmAgMnP	věnovat
hraběti	hrabě	k1gMnSc3	hrabě
Thunovi	Thun	k1gMnSc3	Thun
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
panství	panství	k1gNnSc6	panství
v	v	k7c6	v
Žehušicích	Žehušice	k1gFnPc6	Žehušice
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
jeleni	jelen	k1gMnPc1	jelen
nejsou	být	k5eNaImIp3nP	být
čistokrevní	čistokrevný	k2eAgMnPc1d1	čistokrevný
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zkříženi	zkřížen	k2eAgMnPc1d1	zkřížen
s	s	k7c7	s
obvykle	obvykle	k6eAd1	obvykle
zbarvenou	zbarvený	k2eAgFnSc7d1	zbarvená
zvěří	zvěř	k1gFnSc7	zvěř
tuzemského	tuzemský	k2eAgInSc2d1	tuzemský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
dovezenými	dovezený	k2eAgMnPc7d1	dovezený
americkými	americký	k2eAgMnPc7d1	americký
jeleny	jelen	k1gMnPc7	jelen
wapiti	wapit	k5eAaBmF	wapit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnPc4d1	další
stáda	stádo	k1gNnPc4	stádo
bílých	bílý	k2eAgMnPc2d1	bílý
jelenů	jelen	k1gMnPc2	jelen
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
oborách	obora	k1gFnPc6	obora
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Parohy	paroh	k1gInPc4	paroh
===	===	k?	===
</s>
</p>
<p>
<s>
Nejtypičtějším	typický	k2eAgInSc7d3	nejtypičtější
znakem	znak	k1gInSc7	znak
pro	pro	k7c4	pro
samce	samec	k1gMnSc4	samec
jsou	být	k5eAaImIp3nP	být
parohy	paroh	k1gInPc7	paroh
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
shazuje	shazovat	k5eAaImIp3nS	shazovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jelenům	jelen	k1gMnPc3	jelen
rostou	růst	k5eAaImIp3nP	růst
parohy	paroh	k1gInPc1	paroh
nové	nový	k2eAgInPc1d1	nový
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
porostlé	porostlý	k2eAgInPc1d1	porostlý
jemnou	jemný	k2eAgFnSc7d1	jemná
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
sametovou	sametový	k2eAgFnSc7d1	sametová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
lýčí	lýčí	k1gNnSc3	lýčí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyživuje	vyživovat	k5eAaImIp3nS	vyživovat
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
paroh	paroh	k1gInSc4	paroh
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
vytloukána	vytloukán	k2eAgFnSc1d1	vytloukán
o	o	k7c4	o
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Parohy	paroh	k1gInPc1	paroh
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
kostí	kost	k1gFnSc7	kost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
denně	denně	k6eAd1	denně
vyrůst	vyrůst	k5eAaPmF	vyrůst
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
2,5	[number]	k4	2,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
na	na	k7c6	na
parozích	paroh	k1gInPc6	paroh
navíc	navíc	k6eAd1	navíc
objevují	objevovat	k5eAaImIp3nP	objevovat
jakési	jakýsi	k3yIgInPc4	jakýsi
výrůstky	výrůstek	k1gInPc4	výrůstek
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
říká	říkat	k5eAaImIp3nS	říkat
výsady	výsada	k1gFnPc4	výsada
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yIgFnPc1	který
rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
přibývají	přibývat	k5eAaImIp3nP	přibývat
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
evropský	evropský	k2eAgMnSc1d1	evropský
je	být	k5eAaImIp3nS	být
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
i	i	k9	i
hojným	hojný	k2eAgInSc7d1	hojný
druhem	druh	k1gInSc7	druh
sudokopytníka	sudokopytník	k1gMnSc2	sudokopytník
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
prakticky	prakticky	k6eAd1	prakticky
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
chybí	chybit	k5eAaPmIp3nS	chybit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
nejsevernější	severní	k2eAgFnSc6d3	nejsevernější
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Asie	Asie	k1gFnSc2	Asie
obývá	obývat	k5eAaImIp3nS	obývat
její	její	k3xOp3gFnSc1	její
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
na	na	k7c6	na
území	území	k1gNnSc6	území
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
typického	typický	k2eAgMnSc4d1	typický
obyvatele	obyvatel	k1gMnSc4	obyvatel
starých	starý	k2eAgInPc2d1	starý
lesů	les	k1gInPc2	les
s	s	k7c7	s
občasnými	občasný	k2eAgInPc7d1	občasný
palouky	palouk	k1gInPc7	palouk
a	a	k8xC	a
pásy	pás	k1gInPc7	pás
křovin	křovina	k1gFnPc2	křovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obývá	obývat	k5eAaImIp3nS	obývat
i	i	k9	i
území	území	k1gNnSc4	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
díky	dík	k1gInPc7	dík
DNA	DNA	kA	DNA
testům	test	k1gMnPc3	test
je	být	k5eAaImIp3nS	být
však	však	k9	však
dnes	dnes	k6eAd1	dnes
populace	populace	k1gFnPc1	populace
žijící	žijící	k2eAgFnPc1d1	žijící
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zcela	zcela	k6eAd1	zcela
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
–	–	k?	–
jelen	jelena	k1gFnPc2	jelena
wapiti	wapit	k1gMnPc1	wapit
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gInSc1	Cervus
canadensis	canadensis	k1gFnSc2	canadensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gFnSc1	jeho
početnost	početnost	k1gFnSc1	početnost
obecně	obecně	k6eAd1	obecně
roste	růst	k5eAaImIp3nS	růst
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
území	území	k1gNnSc6	území
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
jeho	jeho	k3xOp3gFnSc4	jeho
přirození	přirozený	k2eAgMnPc1d1	přirozený
predátoři	predátor	k1gMnPc1	predátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
stále	stále	k6eAd1	stále
na	na	k7c6	na
poklesu	pokles	k1gInSc6	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zcela	zcela	k6eAd1	zcela
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
(	(	kIx(	(
<g/>
Albánie	Albánie	k1gFnPc1	Albánie
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc1	Libanon
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
<g/>
)	)	kIx)	)
a	a	k8xC	a
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
zachráněna	zachráněn	k2eAgFnSc1d1	zachráněna
díky	díky	k7c3	díky
vypouštění	vypouštění	k1gNnSc3	vypouštění
uměle	uměle	k6eAd1	uměle
odchovaných	odchovaný	k2eAgMnPc2d1	odchovaný
jedinců	jedinec	k1gMnPc2	jedinec
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Člověkem	člověk	k1gMnSc7	člověk
byl	být	k5eAaImAgMnS	být
zcela	zcela	k6eAd1	zcela
zavlečen	zavlečen	k2eAgMnSc1d1	zavlečen
např.	např.	kA	např.
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Argentiny	Argentina	k1gFnSc2	Argentina
nebo	nebo	k8xC	nebo
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
početnost	početnost	k1gFnSc1	početnost
natolik	natolik	k6eAd1	natolik
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dokonce	dokonce	k9	dokonce
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
významného	významný	k2eAgMnSc2d1	významný
škůdce	škůdce	k1gMnSc2	škůdce
mladých	mladý	k2eAgInPc2d1	mladý
stromků	stromek	k1gInPc2	stromek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
říji	říje	k1gFnSc4	říje
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
většinou	většinou	k6eAd1	většinou
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
čítat	čítat	k5eAaImF	čítat
až	až	k9	až
50	[number]	k4	50
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgNnSc1d1	aktivní
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
až	až	k9	až
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
při	při	k7c6	při
pastvě	pastva	k1gFnSc6	pastva
na	na	k7c6	na
lesních	lesní	k2eAgInPc6d1	lesní
paloucích	palouk	k1gInPc6	palouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnPc1	rychlost
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
krátkodobě	krátkodobě	k6eAd1	krátkodobě
i	i	k9	i
78	[number]	k4	78
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
evropský	evropský	k2eAgMnSc1d1	evropský
je	být	k5eAaImIp3nS	být
výlučně	výlučně	k6eAd1	výlučně
býložravý	býložravý	k2eAgMnSc1d1	býložravý
přežvýkavec	přežvýkavec	k1gMnSc1	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
ale	ale	k9	ale
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
roční	roční	k2eAgFnSc6d1	roční
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kterémkoli	kterýkoli	k3yIgNnSc6	kterýkoli
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
jelení	jelení	k2eAgFnSc1d1	jelení
zvěř	zvěř	k1gFnSc1	zvěř
živí	živit	k5eAaImIp3nS	živit
zejména	zejména	k9	zejména
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
travin	travina	k1gFnPc2	travina
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spásá	spásat	k5eAaImIp3nS	spásat
na	na	k7c6	na
lesních	lesní	k2eAgFnPc6d1	lesní
mýtinách	mýtina	k1gFnPc6	mýtina
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
okolo	okolo	k7c2	okolo
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
zkrátka	zkrátka	k6eAd1	zkrátka
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
světlu	světlo	k1gNnSc3	světlo
rostlin	rostlina	k1gFnPc2	rostlina
dostatek	dostatek	k1gInSc1	dostatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
přikrytá	přikrytý	k2eAgFnSc1d1	přikrytá
příliš	příliš	k6eAd1	příliš
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vrstvou	vrstva	k1gFnSc7	vrstva
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Byliny	bylina	k1gFnPc1	bylina
a	a	k8xC	a
tráva	tráva	k1gFnSc1	tráva
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
v	v	k7c6	v
příhodných	příhodný	k2eAgInPc6d1	příhodný
měsících	měsíc	k1gInPc6	měsíc
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
jelení	jelení	k2eAgFnSc2d1	jelení
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
stádo	stádo	k1gNnSc1	stádo
odvažuje	odvažovat	k5eAaImIp3nS	odvažovat
vyjít	vyjít	k5eAaPmF	vyjít
i	i	k9	i
na	na	k7c4	na
otevřenější	otevřený	k2eAgNnSc4d2	otevřenější
prostranství	prostranství	k1gNnSc4	prostranství
a	a	k8xC	a
spásá	spásat	k5eAaPmIp3nS	spásat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
obilí	obilí	k1gNnSc2	obilí
nebo	nebo	k8xC	nebo
kukuřici	kukuřice	k1gFnSc3	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
stoupá	stoupat	k5eAaImIp3nS	stoupat
v	v	k7c4	v
denní	denní	k2eAgFnSc4d1	denní
porci	porce	k1gFnSc4	porce
potravy	potrava	k1gFnSc2	potrava
zastoupení	zastoupení	k1gNnSc2	zastoupení
keřů	keř	k1gInPc2	keř
a	a	k8xC	a
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
:	:	kIx,	:
jeleni	jelen	k1gMnPc1	jelen
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
ostružiníkem	ostružiník	k1gInSc7	ostružiník
<g/>
,	,	kIx,	,
břečťanem	břečťan	k1gInSc7	břečťan
nebo	nebo	k8xC	nebo
okusováním	okusování	k1gNnSc7	okusování
kůry	kůra	k1gFnSc2	kůra
mladých	mladý	k2eAgInPc2d1	mladý
stromků	stromek	k1gInPc2	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
rádi	rád	k2eAgMnPc1d1	rád
kůru	kůr	k1gInSc2	kůr
a	a	k8xC	a
větévky	větévka	k1gFnSc2	větévka
břízy	bříza	k1gFnSc2	bříza
<g/>
,	,	kIx,	,
habru	habr	k1gInSc2	habr
a	a	k8xC	a
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
.	.	kIx.	.
</s>
<s>
Dostatek	dostatek	k1gInSc1	dostatek
energie	energie	k1gFnSc2	energie
dostává	dostávat	k5eAaImIp3nS	dostávat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
zvěř	zvěř	k1gFnSc1	zvěř
i	i	k9	i
z	z	k7c2	z
plodů	plod	k1gInPc2	plod
–	–	k?	–
bukvic	bukvice	k1gFnPc2	bukvice
<g/>
,	,	kIx,	,
kaštanů	kaštan	k1gInPc2	kaštan
a	a	k8xC	a
žaludů	žalud	k1gInPc2	žalud
<g/>
.	.	kIx.	.
</s>
<s>
Chutnají	chutnat	k5eAaImIp3nP	chutnat
jí	on	k3xPp3gFnSc3	on
ale	ale	k9	ale
též	též	k9	též
padaná	padaný	k2eAgNnPc1d1	padané
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
krmná	krmný	k2eAgFnSc1d1	krmná
řepa	řepa	k1gFnSc1	řepa
a	a	k8xC	a
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc5	predátor
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velké	velký	k2eAgFnSc3d1	velká
velikosti	velikost	k1gFnSc3	velikost
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jeleni	jelen	k1gMnPc1	jelen
nemají	mít	k5eNaImIp3nP	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
kromě	kromě	k7c2	kromě
člověka	člověk	k1gMnSc2	člověk
příliš	příliš	k6eAd1	příliš
přirozených	přirozený	k2eAgMnPc2d1	přirozený
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
představuje	představovat	k5eAaImIp3nS	představovat
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
může	moct	k5eAaImIp3nS	moct
zaútočit	zaútočit	k5eAaPmF	zaútočit
i	i	k9	i
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Kolouši	kolouch	k1gMnPc1	kolouch
se	se	k3xPyFc4	se
zase	zase	k9	zase
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
relativně	relativně	k6eAd1	relativně
snadnou	snadný	k2eAgFnSc7d1	snadná
obětí	oběť	k1gFnSc7	oběť
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
napadení	napadení	k1gNnPc4	napadení
se	se	k3xPyFc4	se
samci	samec	k1gMnPc1	samec
většinou	většinou	k6eAd1	většinou
ohání	ohánět	k5eAaImIp3nP	ohánět
svými	svůj	k3xOyFgInPc7	svůj
parohy	paroh	k1gInPc7	paroh
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
predátora	predátor	k1gMnSc4	predátor
zastrašit	zastrašit	k5eAaPmF	zastrašit
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
obvykle	obvykle	k6eAd1	obvykle
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
ostražitost	ostražitost	k1gFnSc4	ostražitost
a	a	k8xC	a
při	při	k7c6	při
pastvě	pastva	k1gFnSc6	pastva
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
ploše	plocha	k1gFnSc6	plocha
nad	nad	k7c7	nad
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
stáda	stádo	k1gNnSc2	stádo
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
vydají	vydat	k5eAaPmIp3nP	vydat
poplašný	poplašný	k2eAgInSc4d1	poplašný
signál	signál	k1gInSc4	signál
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
stádo	stádo	k1gNnSc4	stádo
se	se	k3xPyFc4	se
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
lesního	lesní	k2eAgInSc2d1	lesní
porostu	porost	k1gInSc2	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Samci	Samek	k1gMnPc1	Samek
jelena	jelen	k1gMnSc4	jelen
evropského	evropský	k2eAgInSc2d1	evropský
jsou	být	k5eAaImIp3nP	být
během	během	k7c2	během
říje	říje	k1gFnSc2	říje
charakterističtí	charakteristický	k2eAgMnPc1d1	charakteristický
svým	svůj	k3xOyFgNnPc3	svůj
hlasitým	hlasitý	k2eAgNnPc3d1	hlasité
troubením	troubení	k1gNnPc3	troubení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
upoutat	upoutat	k5eAaPmF	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
tak	tak	k9	tak
své	svůj	k3xOyFgNnSc4	svůj
stádo	stádo	k1gNnSc4	stádo
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
můžeme	moct	k5eAaImIp1nP	moct
nejčastěji	často	k6eAd3	často
zaslechnout	zaslechnout	k5eAaPmF	zaslechnout
při	při	k7c6	při
svítání	svítání	k1gNnSc6	svítání
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
soumraku	soumrak	k1gInSc6	soumrak
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
jeleni	jelen	k1gMnPc1	jelen
nejaktivnější	aktivní	k2eAgMnPc1d3	nejaktivnější
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
troubení	troubení	k1gNnSc1	troubení
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
při	při	k7c6	při
soubojích	souboj	k1gInPc6	souboj
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pomocí	pomocí	k7c2	pomocí
svých	svůj	k3xOyFgInPc2	svůj
parohů	paroh	k1gInPc2	paroh
vyhnat	vyhnat	k5eAaPmF	vyhnat
svého	svůj	k3xOyFgMnSc4	svůj
konkurenta	konkurent	k1gMnSc4	konkurent
od	od	k7c2	od
blízkosti	blízkost	k1gFnSc2	blízkost
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
druhým	druhý	k4xOgInSc7	druhý
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
a	a	k8xC	a
březost	březost	k1gFnSc1	březost
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
trvá	trvat	k5eAaImIp3nS	trvat
240	[number]	k4	240
<g/>
–	–	k?	–
<g/>
262	[number]	k4	262
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
až	až	k9	až
dvě	dva	k4xCgNnPc4	dva
mláďata	mládě	k1gNnPc4	mládě
vážící	vážící	k2eAgFnSc4d1	vážící
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
kg	kg	kA	kg
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
několik	několik	k4yIc1	několik
prvních	první	k4xOgFnPc2	první
dnů	den	k1gInPc2	den
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
skrytá	skrytý	k2eAgFnSc1d1	skrytá
v	v	k7c6	v
travnatém	travnatý	k2eAgInSc6d1	travnatý
porostu	porost	k1gInSc6	porost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
jsou	být	k5eAaImIp3nP	být
kolouši	kolouch	k1gMnPc1	kolouch
schopni	schopen	k2eAgMnPc1d1	schopen
se	se	k3xPyFc4	se
připojit	připojit	k5eAaPmF	připojit
ke	k	k7c3	k
stádu	stádo	k1gNnSc3	stádo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
matce	matka	k1gFnSc6	matka
jsou	být	k5eAaImIp3nP	být
závislí	závislý	k2eAgMnPc1d1	závislý
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
bílé	bílý	k2eAgNnSc1d1	bílé
skvrnění	skvrnění	k1gNnSc1	skvrnění
obvykle	obvykle	k6eAd1	obvykle
mizí	mizet	k5eAaImIp3nS	mizet
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc4	několik
bílých	bílý	k2eAgFnPc2d1	bílá
skvrn	skvrna	k1gFnPc2	skvrna
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
srsti	srst	k1gFnSc6	srst
mladých	mladý	k2eAgMnPc2d1	mladý
jelenů	jelen	k1gMnPc2	jelen
patrných	patrný	k2eAgFnPc2d1	patrná
ještě	ještě	k9	ještě
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
jelen	jelen	k1gMnSc1	jelen
evropský	evropský	k2eAgMnSc1d1	evropský
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nemoci	nemoc	k1gFnPc1	nemoc
jelenů	jelen	k1gMnPc2	jelen
==	==	k?	==
</s>
</p>
<p>
<s>
virové	virový	k2eAgNnSc1d1	virové
<g/>
:	:	kIx,	:
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
nemoc	nemoc	k1gFnSc1	nemoc
jelenů	jelen	k1gMnPc2	jelen
(	(	kIx(	(
<g/>
virus	virus	k1gInSc1	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Reoviridae	Reovirida	k1gFnSc2	Reovirida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
endemická	endemický	k2eAgFnSc1d1	endemická
paréza	paréza	k1gFnSc1	paréza
jelenů	jelen	k1gMnPc2	jelen
(	(	kIx(	(
<g/>
původce	původce	k1gMnSc1	původce
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
slow	slow	k?	slow
virus	virus	k1gInSc1	virus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzteklina	vzteklina	k1gMnSc1	vzteklina
(	(	kIx(	(
<g/>
Lyssavirus	Lyssavirus	k1gMnSc1	Lyssavirus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc1d1	infekční
bovinní	bovinní	k2eAgFnSc1d1	bovinní
rhinotracheitida	rhinotracheitida	k1gFnSc1	rhinotracheitida
(	(	kIx(	(
<g/>
virus	virus	k1gInSc1	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Herpesviridae	Herpesvirida	k1gFnSc2	Herpesvirida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavnička	hlavnička	k1gFnSc1	hlavnička
(	(	kIx(	(
<g/>
virus	virus	k1gInSc1	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Herpesviridae	Herpesvirida	k1gFnSc2	Herpesvirida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klíšťová	klíšťový	k2eAgFnSc1d1	klíšťová
encefalitída	encefalitída	k1gFnSc1	encefalitída
(	(	kIx(	(
<g/>
virus	virus	k1gInSc1	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Flaviviridae	Flavivirida	k1gFnSc2	Flavivirida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
papilomatóza	papilomatóza	k1gFnSc1	papilomatóza
(	(	kIx(	(
<g/>
virus	virus	k1gInSc1	virus
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Papillomavirus	Papillomavirus	k1gInSc1	Papillomavirus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
<g/>
:	:	kIx,	:
paratuberkulóza	paratuberkulóza	k1gFnSc1	paratuberkulóza
(	(	kIx(	(
<g/>
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
paratuberculosis	paratuberculosis	k1gFnPc2	paratuberculosis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Q	Q	kA	Q
<g/>
–	–	k?	–
<g/>
horečka	horečka	k1gFnSc1	horečka
(	(	kIx(	(
<g/>
Coxiella	Coxiella	k1gFnSc1	Coxiella
burneti	burnet	k5eAaPmF	burnet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antrax	antrax	k1gInSc1	antrax
(	(	kIx(	(
<g/>
Baccilus	Baccilus	k1gInSc1	Baccilus
anthracis	anthracis	k1gFnSc2	anthracis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chlamydióza	chlamydióza	k1gFnSc1	chlamydióza
(	(	kIx(	(
<g/>
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
parazitární	parazitární	k2eAgFnSc4d1	parazitární
</s>
</p>
<p>
<s>
prvoci	prvok	k1gMnPc1	prvok
<g/>
:	:	kIx,	:
Giardia	Giardium	k1gNnPc1	Giardium
intestinalis	intestinalis	k1gFnPc2	intestinalis
<g/>
,	,	kIx,	,
Cryptospordium	Cryptospordium	k1gNnSc1	Cryptospordium
parvum	parvum	k1gInSc1	parvum
<g/>
,	,	kIx,	,
Eimeria	Eimerium	k1gNnPc1	Eimerium
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Toxoplasma	Toxoplasma	k1gFnSc1	Toxoplasma
gondii	gondie	k1gFnSc4	gondie
<g/>
,	,	kIx,	,
Babesia	Babesia	k1gFnSc1	Babesia
divergens	divergens	k1gInSc1	divergens
<g/>
,	,	kIx,	,
Sarcocystis	Sarcocystis	k1gFnSc1	Sarcocystis
gracilis	gracilis	k1gFnSc1	gracilis
<g/>
,	,	kIx,	,
Neospora	Neospora	k1gFnSc1	Neospora
caninum	caninum	k1gInSc1	caninum
</s>
</p>
<p>
<s>
helminté	helmintý	k2eAgFnPc4d1	helmintý
<g/>
:	:	kIx,	:
motolice	motolice	k1gFnPc4	motolice
jaterní	jaterní	k2eAgFnSc2d1	jaterní
<g/>
,	,	kIx,	,
Fascioloides	Fascioloides	k1gMnSc1	Fascioloides
magna	magna	k1gFnSc1	magna
<g/>
,	,	kIx,	,
Dicrocoelium	Dicrocoelium	k1gNnSc1	Dicrocoelium
dendriticum	dendriticum	k1gInSc1	dendriticum
<g/>
,	,	kIx,	,
Paramphistomum	Paramphistomum	k1gInSc1	Paramphistomum
cervi	cerev	k1gFnSc3	cerev
<g/>
,	,	kIx,	,
Moniesia	Moniesium	k1gNnSc2	Moniesium
benedeni	beneden	k2eAgMnPc1d1	beneden
<g/>
,	,	kIx,	,
Taenia	Taenium	k1gNnPc1	Taenium
cervi	cervit	k5eAaPmRp2nS	cervit
<g/>
,	,	kIx,	,
Taenia	Taenium	k1gNnPc1	Taenium
hydatigena	hydatigena	k1gFnSc1	hydatigena
<g/>
,	,	kIx,	,
Dictyocaulus	Dictyocaulus	k1gInSc1	Dictyocaulus
noerneri	noerner	k1gFnSc2	noerner
<g/>
,	,	kIx,	,
Muellerius	Muellerius	k1gInSc4	Muellerius
capillaris	capillaris	k1gFnSc2	capillaris
<g/>
,	,	kIx,	,
Bicaulus	Bicaulus	k1gMnSc1	Bicaulus
sagitatus	sagitatus	k1gMnSc1	sagitatus
<g/>
,	,	kIx,	,
Elaphostrongylus	Elaphostrongylus	k1gMnSc1	Elaphostrongylus
cervi	cervit	k5eAaPmRp2nS	cervit
<g/>
,	,	kIx,	,
Haemonchus	Haemonchus	k1gMnSc1	Haemonchus
contortus	contortus	k1gMnSc1	contortus
<g/>
,	,	kIx,	,
Teladorstagia	Teladorstagia	k1gFnSc1	Teladorstagia
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Cooperia	Cooperium	k1gNnPc1	Cooperium
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Trichostrongylus	Trichostrongylus	k1gMnSc1	Trichostrongylus
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Nematodirus	Nematodirus	k1gMnSc1	Nematodirus
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Oesophagostomum	Oesophagostomum	k1gInSc1	Oesophagostomum
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Trichuris	Trichuris	k1gInSc1	Trichuris
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Setaria	Setarium	k1gNnPc1	Setarium
cervi	cerev	k1gFnSc5	cerev
</s>
</p>
<p>
<s>
pavoukovci	pavoukovec	k1gMnPc1	pavoukovec
a	a	k8xC	a
hmyz	hmyz	k1gInSc1	hmyz
<g/>
:	:	kIx,	:
Ixodes	Ixodes	k1gMnSc1	Ixodes
ricinus	ricinus	k1gMnSc1	ricinus
<g/>
,	,	kIx,	,
Cervicola	Cervicola	k1gFnSc1	Cervicola
longicornis	longicornis	k1gFnSc1	longicornis
<g/>
,	,	kIx,	,
Solenoptes	Solenoptes	k1gInSc1	Solenoptes
burmeisteri	burmeisteri	k1gNnSc2	burmeisteri
<g/>
,	,	kIx,	,
Hypoderma	Hypodermum	k1gNnSc2	Hypodermum
acteon	acteona	k1gFnPc2	acteona
<g/>
,	,	kIx,	,
Cephenemyia	Cephenemyia	k1gFnSc1	Cephenemyia
auribarbis	auribarbis	k1gFnSc1	auribarbis
</s>
</p>
<p>
<s>
==	==	k?	==
Jeleni	jelen	k1gMnPc1	jelen
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Zobrazení	zobrazení	k1gNnSc4	zobrazení
jelení	jelení	k2eAgFnSc2d1	jelení
zvěře	zvěř	k1gFnSc2	zvěř
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
paleolitickém	paleolitický	k2eAgNnSc6d1	paleolitické
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
jeskyni	jeskyně	k1gFnSc6	jeskyně
Lascaux	Lascaux	k1gInSc4	Lascaux
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
paleolitu	paleolit	k1gInSc2	paleolit
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
obrazy	obraz	k1gInPc4	obraz
tančících	tančící	k2eAgFnPc2d1	tančící
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
snad	snad	k9	snad
šamanů	šaman	k1gMnPc2	šaman
<g/>
,	,	kIx,	,
zdobených	zdobený	k2eAgFnPc2d1	zdobená
jeleními	jelení	k2eAgFnPc7d1	jelení
nebo	nebo	k8xC	nebo
sobími	sobí	k2eAgInPc7d1	sobí
parohy	paroh	k1gInPc7	paroh
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
Sumerů	Sumer	k1gInPc2	Sumer
motiv	motiv	k1gInSc4	motiv
dvou	dva	k4xCgMnPc2	dva
jelenů	jelen	k1gMnPc2	jelen
<g/>
,	,	kIx,	,
chráněných	chráněný	k2eAgMnPc2d1	chráněný
mytickým	mytický	k2eAgMnSc7d1	mytický
orlem	orel	k1gMnSc7	orel
Anzu	Anzus	k1gInSc2	Anzus
se	s	k7c7	s
lví	lví	k2eAgFnSc7d1	lví
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
dvanácterák	dvanácterák	k1gMnSc1	dvanácterák
byl	být	k5eAaImAgMnS	být
také	také	k9	také
symbolickým	symbolický	k2eAgNnSc7d1	symbolické
zvířetem	zvíře	k1gNnSc7	zvíře
mezopotámského	mezopotámský	k2eAgMnSc2d1	mezopotámský
boha	bůh	k1gMnSc2	bůh
Enkiho	Enki	k1gMnSc2	Enki
<g/>
.	.	kIx.	.
</s>
<s>
Reliéfy	reliéf	k1gInPc1	reliéf
a	a	k8xC	a
sošky	soška	k1gFnPc1	soška
jelenů	jelen	k1gMnPc2	jelen
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
zanechaly	zanechat	k5eAaPmAgFnP	zanechat
také	také	k9	také
starověké	starověký	k2eAgFnPc1d1	starověká
kultury	kultura	k1gFnPc1	kultura
Chetitů	Chetit	k1gMnPc2	Chetit
<g/>
,	,	kIx,	,
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
,	,	kIx,	,
Skythů	Skyth	k1gMnPc2	Skyth
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zejména	zejména	k9	zejména
pro	pro	k7c4	pro
Skythy	Skyth	k1gMnPc4	Skyth
měl	mít	k5eAaImAgMnS	mít
jelen	jelen	k1gMnSc1	jelen
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
průvodce	průvodce	k1gMnPc4	průvodce
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
duši	duše	k1gFnSc4	duše
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Onen	onen	k3xDgInSc4	onen
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
sošky	soška	k1gFnPc1	soška
jelenů	jelen	k1gMnPc2	jelen
<g/>
,	,	kIx,	,
zhotovené	zhotovený	k2eAgFnPc1d1	zhotovená
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
i	i	k8xC	i
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ukládány	ukládat	k5eAaImNgInP	ukládat
do	do	k7c2	do
skythských	skythský	k2eAgInPc2d1	skythský
hrobů	hrob	k1gInPc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
koně	kůň	k1gMnPc4	kůň
skythští	skythský	k2eAgMnPc1d1	skythský
náčelníci	náčelník	k1gMnPc1	náčelník
často	často	k6eAd1	často
zdobili	zdobit	k5eAaImAgMnP	zdobit
maskami	maska	k1gFnPc7	maska
s	s	k7c7	s
umělým	umělý	k2eAgNnSc7d1	umělé
parožím	paroží	k1gNnSc7	paroží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jim	on	k3xPp3gMnPc3	on
mělo	mít	k5eAaImAgNnS	mít
dodat	dodat	k5eAaPmF	dodat
jelení	jelení	k2eAgInSc4d1	jelení
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
masky	maska	k1gFnPc1	maska
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
např.	např.	kA	např.
v	v	k7c6	v
pohŕebních	pohŕební	k2eAgFnPc6d1	pohŕební
mohylách	mohyla	k1gFnPc6	mohyla
Pazyrycké	Pazyrycký	k2eAgFnSc2d1	Pazyrycký
kultury	kultura	k1gFnSc2	kultura
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chetitská	chetitský	k2eAgFnSc1d1	Chetitská
bohyně	bohyně	k1gFnSc1	bohyně
Rutaš	Rutaš	k1gMnSc1	Rutaš
i	i	k8xC	i
keltský	keltský	k2eAgMnSc1d1	keltský
bůh	bůh	k1gMnSc1	bůh
Cernunnos	Cernunnosa	k1gFnPc2	Cernunnosa
byli	být	k5eAaImAgMnP	být
vládci	vládce	k1gMnPc1	vládce
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zobrazováni	zobrazován	k2eAgMnPc1d1	zobrazován
s	s	k7c7	s
jeleními	jelení	k2eAgInPc7d1	jelení
parohy	paroh	k1gInPc7	paroh
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
keltských	keltský	k2eAgInPc6d1	keltský
i	i	k8xC	i
slovanských	slovanský	k2eAgInPc6d1	slovanský
příbězích	příběh	k1gInPc6	příběh
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
motiv	motiv	k1gInSc1	motiv
víly	víla	k1gFnSc2	víla
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
laně	laň	k1gFnSc2	laň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vdá	vdát	k5eAaPmIp3nS	vdát
za	za	k7c4	za
smrtelníka	smrtelník	k1gMnSc4	smrtelník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
irskému	irský	k2eAgMnSc3d1	irský
hrdinovi	hrdina	k1gMnSc3	hrdina
Finnovi	Finn	k1gMnSc3	Finn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
takovouto	takovýto	k3xDgFnSc7	takovýto
vílou	víla	k1gFnSc7	víla
zplodil	zplodit	k5eAaPmAgMnS	zplodit
syna	syn	k1gMnSc4	syn
jménem	jméno	k1gNnSc7	jméno
Oissin	Oissin	k1gMnSc1	Oissin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Kolouch	Kolouch	k1gMnSc1	Kolouch
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
motiv	motiv	k1gInSc4	motiv
laně	laň	k1gFnSc2	laň
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
ve	v	k7c4	v
vílu	víla	k1gFnSc4	víla
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
či	či	k8xC	či
polských	polský	k2eAgFnPc6d1	polská
pověstech	pověst	k1gFnPc6	pověst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
staroindickém	staroindický	k2eAgInSc6d1	staroindický
eposu	epos	k1gInSc6	epos
Rámajána	Rámaján	k1gMnSc2	Rámaján
se	se	k3xPyFc4	se
sluha	sluha	k1gMnSc1	sluha
démona	démon	k1gMnSc2	démon
Rávany	Rávana	k1gFnSc2	Rávana
jménem	jméno	k1gNnSc7	jméno
Máríča	Máríča	k1gFnSc1	Máríča
promění	proměnit	k5eAaPmIp3nS	proměnit
ve	v	k7c4	v
zlatého	zlatý	k2eAgMnSc4d1	zlatý
jelena	jelen	k1gMnSc4	jelen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odlákal	odlákat	k5eAaPmAgMnS	odlákat
hrdinu	hrdina	k1gMnSc4	hrdina
Rámu	Ráma	k1gMnSc4	Ráma
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Síty	síto	k1gNnPc7	síto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gMnSc1	jeho
pán	pán	k1gMnSc1	pán
mohl	moct	k5eAaImAgMnS	moct
Sítu	síto	k1gNnSc3	síto
snáze	snadno	k6eAd2	snadno
unést	unést	k5eAaPmF	unést
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
indický	indický	k2eAgInSc1d1	indický
příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
poustevníku	poustevník	k1gMnSc6	poustevník
Ršiasrngovi	Ršiasrng	k1gMnSc6	Ršiasrng
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
z	z	k7c2	z
laně	laň	k1gFnSc2	laň
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
proto	proto	k6eAd1	proto
jelení	jelení	k2eAgInPc4d1	jelení
parohy	paroh	k1gInPc4	paroh
<g/>
.	.	kIx.	.
</s>
<s>
Šintoisté	šintoista	k1gMnPc1	šintoista
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
brouk	brouk	k1gMnSc1	brouk
roháč	roháč	k1gMnSc1	roháč
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
malý	malý	k2eAgMnSc1d1	malý
okřídlený	okřídlený	k2eAgMnSc1d1	okřídlený
jelínek	jelínek	k1gMnSc1	jelínek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
kami	kame	k1gFnSc4	kame
jelenů	jelen	k1gMnPc2	jelen
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Číňany	Číňan	k1gMnPc4	Číňan
měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
mýtech	mýtus	k1gInPc6	mýtus
i	i	k8xC	i
lékařství	lékařství	k1gNnSc2	lékařství
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
než	než	k8xS	než
jelen	jelen	k1gMnSc1	jelen
evropský	evropský	k2eAgMnSc1d1	evropský
místní	místní	k2eAgInPc4d1	místní
druhy	druh	k1gInPc4	druh
–	–	k?	–
jelen	jelena	k1gFnPc2	jelena
milu	milu	k6eAd1	milu
a	a	k8xC	a
jelen	jelen	k1gMnSc1	jelen
sika	sika	k1gMnSc1	sika
<g/>
.	.	kIx.	.
</s>
<s>
Parohy	paroh	k1gInPc1	paroh
jelenů	jelen	k1gMnPc2	jelen
v	v	k7c6	v
lýčí	lýčí	k1gNnSc6	lýčí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
panty	pant	k1gInPc1	pant
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
ceněny	cenit	k5eAaImNgFnP	cenit
jako	jako	k9	jako
posilující	posilující	k2eAgInSc1d1	posilující
lék	lék	k1gInSc1	lék
<g/>
,	,	kIx,	,
protijed	protijed	k1gInSc1	protijed
či	či	k8xC	či
afrodisiakum	afrodisiakum	k1gNnSc1	afrodisiakum
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
parohy	paroh	k1gInPc1	paroh
nakládané	nakládaný	k2eAgInPc1d1	nakládaný
v	v	k7c6	v
lihu	líh	k1gInSc6	líh
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
obdobným	obdobný	k2eAgInPc3d1	obdobný
účelům	účel	k1gInPc3	účel
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mýtu	mýtus	k1gInSc6	mýtus
Algonkinů	Algonkin	k1gInPc2	Algonkin
běžící	běžící	k2eAgMnSc1d1	běžící
jelen	jelen	k1gMnSc1	jelen
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
věků	věk	k1gInPc2	věk
oživuje	oživovat	k5eAaImIp3nS	oživovat
dosud	dosud	k6eAd1	dosud
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnPc1	jeho
kopýtka	kopýtko	k1gNnPc1	kopýtko
dotknou	dotknout	k5eAaPmIp3nP	dotknout
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
vytryskne	vytrysknout	k5eAaPmIp3nS	vytrysknout
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
vyraší	vyrašit	k5eAaPmIp3nS	vyrašit
tráva	tráva	k1gFnSc1	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kalifornských	kalifornský	k2eAgMnPc2d1	kalifornský
indiánů	indián	k1gMnPc2	indián
zase	zase	k9	zase
jelen	jelen	k1gMnSc1	jelen
daroval	darovat	k5eAaPmAgMnS	darovat
člověku	člověk	k1gMnSc3	člověk
oheň	oheň	k1gInSc4	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Lakotové	lakota	k1gMnPc1	lakota
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jelen	jelen	k1gMnSc1	jelen
je	být	k5eAaImIp3nS	být
pánem	pán	k1gMnSc7	pán
kouzla	kouzlo	k1gNnSc2	kouzlo
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
narážka	narážka	k1gFnSc1	narážka
na	na	k7c4	na
jelení	jelení	k2eAgFnSc4d1	jelení
říji	říje	k1gFnSc4	říje
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
člověku	člověk	k1gMnSc3	člověk
své	svůj	k3xOyFgNnSc4	svůj
kouzlo	kouzlo	k1gNnSc4	kouzlo
propůjčit	propůjčit	k5eAaPmF	propůjčit
<g/>
.	.	kIx.	.
</s>
<s>
Jelení	jelení	k2eAgInPc4d1	jelení
zuby	zub	k1gInPc4	zub
používali	používat	k5eAaImAgMnP	používat
severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
indiáni	indián	k1gMnPc1	indián
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Lakotové	lakota	k1gMnPc1	lakota
<g/>
,	,	kIx,	,
Apači	Apač	k1gMnPc1	Apač
nebo	nebo	k8xC	nebo
Mandanové	Mandan	k1gMnPc1	Mandan
jako	jako	k9	jako
ozdobu	ozdoba	k1gFnSc4	ozdoba
oděvu	oděv	k1gInSc2	oděv
a	a	k8xC	a
z	z	k7c2	z
parohů	paroh	k1gInPc2	paroh
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
Mandanové	Mandan	k1gMnPc1	Mandan
i	i	k8xC	i
Irokézové	Irokéz	k1gMnPc1	Irokéz
hrábě	hrábě	k1gFnPc4	hrábě
<g/>
.	.	kIx.	.
</s>
<s>
Jakiové	Jakius	k1gMnPc1	Jakius
ze	z	k7c2	z
severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
Mexika	Mexiko	k1gNnSc2	Mexiko
pokládali	pokládat	k5eAaImAgMnP	pokládat
jelena	jelena	k1gFnSc1	jelena
za	za	k7c4	za
posvátné	posvátný	k2eAgNnSc4d1	posvátné
zvíře	zvíře	k1gNnSc4	zvíře
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jelení	jelení	k2eAgFnSc2d1	jelení
říje	říje	k1gFnSc2	říje
tančili	tančit	k5eAaImAgMnP	tančit
jelení	jelení	k2eAgInSc4d1	jelení
tanec	tanec	k1gInSc4	tanec
v	v	k7c6	v
maskách	maska	k1gFnPc6	maska
zhotovených	zhotovený	k2eAgFnPc2d1	zhotovená
z	z	k7c2	z
vycpaných	vycpaný	k2eAgFnPc2d1	vycpaná
jeleních	jelení	k2eAgFnPc2d1	jelení
hlav	hlava	k1gFnPc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Karukové	Karukový	k2eAgFnPc1d1	Karukový
a	a	k8xC	a
Jurokové	Jurokový	k2eAgFnPc1d1	Jurokový
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
tančili	tančit	k5eAaImAgMnP	tančit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
rituální	rituální	k2eAgInSc1d1	rituální
Tanec	tanec	k1gInSc1	tanec
bílého	bílý	k1gMnSc2	bílý
jelena	jelen	k1gMnSc2	jelen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
představovat	představovat	k5eAaImF	představovat
každoroční	každoroční	k2eAgFnSc4d1	každoroční
obnovu	obnova	k1gFnSc4	obnova
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
rekvizitou	rekvizita	k1gFnSc7	rekvizita
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
kůže	kůže	k1gFnSc1	kůže
posvátného	posvátný	k2eAgNnSc2d1	posvátné
bílého	bílé	k1gNnSc2	bílé
jelena	jelen	k1gMnSc2	jelen
<g/>
.	.	kIx.	.
</s>
<s>
Čerokíové	Čerokíus	k1gMnPc1	Čerokíus
si	se	k3xPyFc3	se
zase	zase	k9	zase
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
bajky	bajka	k1gFnPc4	bajka
o	o	k7c6	o
soupeření	soupeření	k1gNnSc6	soupeření
jelena	jelen	k1gMnSc2	jelen
s	s	k7c7	s
králíkem	králík	k1gMnSc7	králík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
jelena	jelen	k1gMnSc4	jelen
připravit	připravit	k5eAaPmF	připravit
o	o	k7c4	o
parohy	paroh	k1gInPc4	paroh
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gNnSc3	on
ze	z	k7c2	z
závisti	závist	k1gFnSc2	závist
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
přední	přední	k2eAgInPc4d1	přední
zuby	zub	k1gInPc4	zub
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
(	(	kIx(	(
<g/>
jelenům	jelen	k1gMnPc3	jelen
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jiným	jiný	k2eAgMnPc3d1	jiný
přežvýkavcům	přežvýkavec	k1gMnPc3	přežvýkavec
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
řezáky	řezák	k1gInPc4	řezák
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
indiánských	indiánský	k2eAgInPc2d1	indiánský
mýtů	mýtus	k1gInPc2	mýtus
a	a	k8xC	a
rituálů	rituál	k1gInPc2	rituál
však	však	k9	však
často	často	k6eAd1	často
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yIgInSc4	který
druh	druh	k1gInSc4	druh
jelena	jelen	k1gMnSc2	jelen
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
jak	jak	k6eAd1	jak
o	o	k7c4	o
jelena	jelen	k1gMnSc4	jelen
wapiti	wapit	k5eAaImF	wapit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
o	o	k7c6	o
jelence	jelenka	k1gFnSc6	jelenka
běloocasého	běloocasý	k2eAgInSc2d1	běloocasý
či	či	k8xC	či
ušatého	ušatý	k2eAgInSc2d1	ušatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
mytologii	mytologie	k1gFnSc6	mytologie
jelen	jelen	k1gMnSc1	jelen
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
bohyni	bohyně	k1gFnSc3	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
či	či	k8xC	či
její	její	k3xOp3gInSc1	její
římský	římský	k2eAgInSc1d1	římský
protějšek	protějšek	k1gInSc1	protějšek
Dianu	Diana	k1gFnSc4	Diana
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
posvátnou	posvátný	k2eAgFnSc4d1	posvátná
laň	laň	k1gFnSc4	laň
ze	z	k7c2	z
zlatými	zlatý	k2eAgInPc7d1	zlatý
parohy	paroh	k1gInPc7	paroh
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
polapit	polapit	k5eAaPmF	polapit
hrdina	hrdina	k1gMnSc1	hrdina
Héraklés	Héraklés	k1gInSc4	Héraklés
<g/>
.	.	kIx.	.
</s>
<s>
Lovec	lovec	k1gMnSc1	lovec
Aktaión	Aktaión	k1gMnSc1	Aktaión
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Artemis	Artemis	k1gFnSc1	Artemis
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
spatřil	spatřit	k5eAaPmAgMnS	spatřit
nahou	nahý	k2eAgFnSc4d1	nahá
při	při	k7c6	při
koupeli	koupel	k1gFnSc6	koupel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
bohyní	bohyně	k1gFnSc7	bohyně
za	za	k7c4	za
trest	trest	k1gInSc4	trest
proměněn	proměnit	k5eAaPmNgMnS	proměnit
v	v	k7c4	v
jelena	jelen	k1gMnSc4	jelen
a	a	k8xC	a
poté	poté	k6eAd1	poté
zadáven	zadávit	k5eAaPmNgInS	zadávit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
psí	psí	k2eAgFnSc7d1	psí
smečkou	smečka	k1gFnSc7	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Dionýsových	Dionýsův	k2eAgNnPc6d1	Dionýsovo
mystériích	mystérium	k1gNnPc6	mystérium
býval	bývat	k5eAaImAgMnS	bývat
obětován	obětován	k2eAgMnSc1d1	obětován
kolouch	kolouch	k1gMnSc1	kolouch
či	či	k8xC	či
kozel	kozel	k1gMnSc1	kozel
<g/>
,	,	kIx,	,
ztotožněný	ztotožněný	k2eAgMnSc1d1	ztotožněný
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Věřící	věřící	k1gMnPc1	věřící
poté	poté	k6eAd1	poté
pojídali	pojídat	k5eAaImAgMnP	pojídat
jeho	jeho	k3xOp3gNnSc4	jeho
syrové	syrový	k2eAgNnSc4d1	syrové
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
mystického	mystický	k2eAgNnSc2d1	mystické
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Dionýsem	Dionýsos	k1gMnSc7	Dionýsos
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
mýtu	mýtus	k1gInSc2	mýtus
rozsápán	rozsápat	k5eAaPmNgMnS	rozsápat
Titány	Titán	k1gMnPc7	Titán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vidět	vidět	k5eAaImF	vidět
jistá	jistý	k2eAgFnSc1d1	jistá
paralela	paralela	k1gFnSc1	paralela
s	s	k7c7	s
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
eucharistií	eucharistie	k1gFnSc7	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeleního	jelení	k2eAgNnSc2d1	jelení
paroží	paroží	k1gNnSc2	paroží
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
oltář	oltář	k1gInSc1	oltář
Apollóna	Apollón	k1gMnSc2	Apollón
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Délu	Délos	k1gInSc2	Délos
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
zdobení	zdobení	k1gNnSc2	zdobení
oltářů	oltář	k1gInPc2	oltář
jelením	jelení	k2eAgNnSc7d1	jelení
parožím	paroží	k1gNnSc7	paroží
je	být	k5eAaImIp3nS	být
však	však	k9	však
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
udržela	udržet	k5eAaPmAgFnS	udržet
i	i	k9	i
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
má	mít	k5eAaImIp3nS	mít
značný	značný	k2eAgInSc4d1	značný
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
i	i	k9	i
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
či	či	k8xC	či
laň	laň	k1gFnSc1	laň
hledající	hledající	k2eAgNnSc4d1	hledající
napajedlo	napajedlo	k1gNnSc4	napajedlo
jsou	být	k5eAaImIp3nP	být
symbolem	symbol	k1gInSc7	symbol
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
toužící	toužící	k2eAgFnPc1d1	toužící
po	po	k7c6	po
Bohu	bůh	k1gMnSc6	bůh
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
biblický	biblický	k2eAgInSc1d1	biblický
verš	verš	k1gInSc1	verš
Jako	jako	k8xC	jako
laň	laň	k1gFnSc1	laň
touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c6	po
čisté	čistý	k2eAgFnSc6d1	čistá
vodě	voda	k1gFnSc6	voda
dychtí	dychtit	k5eAaImIp3nS	dychtit
má	můj	k3xOp1gFnSc1	můj
duše	duše	k1gFnSc1	duše
po	po	k7c6	po
tobě	ty	k3xPp2nSc6	ty
<g/>
,	,	kIx,	,
Bože	bůh	k1gMnSc5	bůh
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ž.	Ž.	kA	Ž.
42	[number]	k4	42
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
se	s	k7c7	s
světly	světlo	k1gNnPc7	světlo
na	na	k7c6	na
výsadách	výsada	k1gFnPc6	výsada
paroží	paroží	k1gNnSc2	paroží
či	či	k8xC	či
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
mezi	mezi	k7c4	mezi
parohy	paroh	k1gInPc4	paroh
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
v	v	k7c6	v
legendách	legenda	k1gFnPc6	legenda
o	o	k7c6	o
sv.	sv.	kA	sv.
Hubertovi	Hubert	k1gMnSc6	Hubert
a	a	k8xC	a
sv.	sv.	kA	sv.
Eustachu	Eustach	k1gMnSc3	Eustach
přímluvce	přímluvce	k1gMnSc1	přímluvce
člověka	člověk	k1gMnSc2	člověk
pŕed	pŕed	k1gMnSc1	pŕed
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
samotného	samotný	k2eAgMnSc2d1	samotný
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	díl	k1gInSc6	díl
německého	německý	k2eAgMnSc2d1	německý
barokního	barokní	k2eAgMnSc2d1	barokní
básníka	básník	k1gMnSc2	básník
Friedricha	Friedrich	k1gMnSc2	Friedrich
von	von	k1gInSc4	von
Spee	Spee	k1gNnSc2	Spee
je	být	k5eAaImIp3nS	být
ukřižovaný	ukřižovaný	k2eAgMnSc1d1	ukřižovaný
Kristus	Kristus	k1gMnSc1	Kristus
přirovnáván	přirovnávat	k5eAaImNgMnS	přirovnávat
ke	k	k7c3	k
kolouchovi	kolouch	k1gMnSc3	kolouch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
chytil	chytit	k5eAaPmAgMnS	chytit
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
visí	viset	k5eAaImIp3nS	viset
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
za	za	k7c4	za
hrdlo	hrdlo	k1gNnSc4	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
mezi	mezi	k7c7	mezi
parohy	paroh	k1gInPc7	paroh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
atributem	atribut	k1gInSc7	atribut
některých	některý	k3yIgInPc2	některý
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
světců	světec	k1gMnPc2	světec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
sv.	sv.	kA	sv.
Hubert	Hubert	k1gMnSc1	Hubert
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Eustach	Eustach	k1gMnSc1	Eustach
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Mathy	Matha	k1gFnSc2	Matha
a	a	k8xC	a
sv.	sv.	kA	sv.
Felix	Felix	k1gMnSc1	Felix
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dílech	díl	k1gInPc6	díl
antických	antický	k2eAgMnPc2d1	antický
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
a	a	k8xC	a
zejména	zejména	k9	zejména
ve	v	k7c6	v
středověkých	středověký	k2eAgInPc6d1	středověký
bestiářích	bestiář	k1gInPc6	bestiář
se	se	k3xPyFc4	se
o	o	k7c6	o
jelenech	jelen	k1gMnPc6	jelen
uvádí	uvádět	k5eAaImIp3nP	uvádět
různé	různý	k2eAgFnPc1d1	různá
pověry	pověra	k1gFnPc1	pověra
a	a	k8xC	a
tvrzení	tvrzení	k1gNnPc1	tvrzení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
člověku	člověk	k1gMnSc6	člověk
připadají	připadat	k5eAaPmIp3nP	připadat
až	až	k9	až
absurdní	absurdní	k2eAgMnSc1d1	absurdní
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jeleni	jelen	k1gMnPc1	jelen
chodí	chodit	k5eAaImIp3nP	chodit
barvit	barvit	k5eAaImF	barvit
paroží	paroží	k1gNnSc4	paroží
do	do	k7c2	do
milíře	milíř	k1gInSc2	milíř
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
pěkně	pěkně	k6eAd1	pěkně
tmavé	tmavý	k2eAgNnSc1d1	tmavé
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
údajů	údaj	k1gInPc2	údaj
jelen	jelen	k1gMnSc1	jelen
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
shozené	shozený	k2eAgInPc4d1	shozený
parohy	paroh	k1gInPc4	paroh
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gInPc4	on
nenašli	najít	k5eNaPmAgMnP	najít
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
jejich	jejich	k3xOp3gFnSc7	jejich
velké	velký	k2eAgFnPc4d1	velká
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jelen	jelen	k1gMnSc1	jelen
zeslábne	zeslábnout	k5eAaPmIp3nS	zeslábnout
nebo	nebo	k8xC	nebo
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
sežrat	sežrat	k5eAaPmF	sežrat
hada	had	k1gMnSc4	had
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
hadí	hadí	k2eAgInSc1d1	hadí
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
napije	napít	k5eAaPmIp3nS	napít
<g/>
,	,	kIx,	,
vyzvrací	vyzvracet	k5eAaPmIp3nS	vyzvracet
a	a	k8xC	a
vykálí	vykálet	k5eAaPmIp3nS	vykálet
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
celý	celý	k2eAgInSc1d1	celý
omládne	omládnout	k5eAaPmIp3nS	omládnout
a	a	k8xC	a
zkrásní	zkrásnit	k5eAaPmIp3nS	zkrásnit
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sezónní	sezónní	k2eAgNnSc4d1	sezónní
línání	línání	k1gNnSc4	línání
a	a	k8xC	a
barvoměnu	barvoměna	k1gFnSc4	barvoměna
jelení	jelení	k2eAgFnSc2d1	jelení
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lov	lov	k1gInSc1	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
impozantní	impozantní	k2eAgInSc4d1	impozantní
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
značnou	značný	k2eAgFnSc4d1	značná
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
chutné	chutný	k2eAgNnSc4d1	chutné
maso	maso	k1gNnSc4	maso
patřila	patřit	k5eAaImAgFnS	patřit
jelení	jelení	k2eAgFnSc1d1	jelení
zvěř	zvěř	k1gFnSc1	zvěř
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
k	k	k7c3	k
oblíbené	oblíbený	k2eAgFnSc3d1	oblíbená
kořisti	kořist	k1gFnSc3	kořist
člověka	člověk	k1gMnSc2	člověk
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgInSc2	svůj
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
lovci	lovec	k1gMnPc1	lovec
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
pozoruhodné	pozoruhodný	k2eAgInPc4d1	pozoruhodný
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tato	tento	k3xDgNnPc4	tento
ostražitá	ostražitý	k2eAgNnPc4d1	ostražité
zvířata	zvíře	k1gNnPc4	zvíře
přelstít	přelstít	k5eAaPmF	přelstít
<g/>
.	.	kIx.	.
</s>
<s>
Severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
indiáni	indián	k1gMnPc1	indián
se	se	k3xPyFc4	se
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
maskovali	maskovat	k5eAaBmAgMnP	maskovat
jelení	jelení	k2eAgFnSc7d1	jelení
kůží	kůže	k1gFnSc7	kůže
s	s	k7c7	s
parohy	paroh	k1gInPc7	paroh
a	a	k8xC	a
při	při	k7c6	při
přiblížení	přiblížení	k1gNnSc6	přiblížení
zvěř	zvěř	k1gFnSc4	zvěř
stříleli	střílet	k5eAaImAgMnP	střílet
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Aztécké	aztécký	k2eAgInPc1d1	aztécký
a	a	k8xC	a
mayské	mayský	k2eAgInPc1d1	mayský
kodexy	kodex	k1gInPc1	kodex
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
chytání	chytání	k1gNnSc4	chytání
jelena	jelen	k1gMnSc2	jelen
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
močická	močický	k2eAgFnSc1d1	močický
a	a	k8xC	a
incká	incký	k2eAgFnSc1d1	incká
keramika	keramika	k1gFnSc1	keramika
dokládá	dokládat	k5eAaImIp3nS	dokládat
společný	společný	k2eAgInSc4d1	společný
lov	lov	k1gInSc4	lov
jelena	jelen	k1gMnSc2	jelen
huemula	huemul	k1gMnSc2	huemul
do	do	k7c2	do
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
,	,	kIx,	,
Babylóňané	Babylóňan	k1gMnPc1	Babylóňan
či	či	k8xC	či
Peršané	Peršan	k1gMnPc1	Peršan
lovili	lovit	k5eAaImAgMnP	lovit
jeleny	jelen	k1gMnPc4	jelen
a	a	k8xC	a
daňky	daněk	k1gMnPc4	daněk
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
koní	kůň	k1gMnPc2	kůň
či	či	k8xC	či
dvoukolých	dvoukolý	k2eAgInPc2d1	dvoukolý
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
je	on	k3xPp3gMnPc4	on
chytali	chytat	k5eAaImAgMnP	chytat
živé	živá	k1gFnPc4	živá
do	do	k7c2	do
lasa	laso	k1gNnSc2	laso
a	a	k8xC	a
chovali	chovat	k5eAaImAgMnP	chovat
v	v	k7c6	v
královských	královský	k2eAgFnPc6d1	královská
oborách	obora	k1gFnPc6	obora
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
pěšímu	pěší	k2eAgInSc3d1	pěší
lovu	lov	k1gInSc3	lov
se	se	k3xPyFc4	se
psy	pes	k1gMnPc7	pes
nebo	nebo	k8xC	nebo
používali	používat	k5eAaImAgMnP	používat
pasti	past	k1gFnSc2	past
<g/>
.	.	kIx.	.
</s>
<s>
Xenofón	Xenofón	k1gInSc1	Xenofón
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
spisu	spis	k1gInSc6	spis
Kynagetikos	Kynagetikos	k1gInSc1	Kynagetikos
(	(	kIx(	(
<g/>
překládá	překládat	k5eAaImIp3nS	překládat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Psovod	psovod	k1gMnSc1	psovod
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
O	o	k7c6	o
lovu	lov	k1gInSc6	lov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
nejprve	nejprve	k6eAd1	nejprve
chytit	chytit	k5eAaPmF	chytit
koloucha	kolouch	k1gMnSc4	kolouch
<g/>
,	,	kIx,	,
aby	aby	k9	aby
svým	svůj	k3xOyFgNnSc7	svůj
voláním	volání	k1gNnSc7	volání
přilákal	přilákat	k5eAaPmAgInS	přilákat
laň	laň	k1gFnSc4	laň
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
používali	používat	k5eAaImAgMnP	používat
jeleny	jelena	k1gFnPc4	jelena
při	při	k7c6	při
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
cirku	cirk	k1gInSc6	cirk
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
maso	maso	k1gNnSc4	maso
podávali	podávat	k5eAaImAgMnP	podávat
jako	jako	k8xC	jako
vybranou	vybraný	k2eAgFnSc4d1	vybraná
lahůdku	lahůdka	k1gFnSc4	lahůdka
při	při	k7c6	při
hostinách	hostina	k1gFnPc6	hostina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
štvanice	štvanice	k1gFnSc1	štvanice
na	na	k7c4	na
jelena	jelen	k1gMnSc4	jelen
pokládána	pokládán	k2eAgMnSc4d1	pokládán
za	za	k7c4	za
mužnou	mužný	k2eAgFnSc4d1	mužná
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaImAgMnP	věnovat
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
panovníci	panovník	k1gMnPc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Jaromír	Jaromír	k1gMnSc1	Jaromír
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
na	na	k7c4	na
jelena	jelen	k1gMnSc4	jelen
zajat	zajat	k2eAgMnSc1d1	zajat
svými	svůj	k3xOyFgMnPc7	svůj
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
středověké	středověký	k2eAgFnSc2d1	středověká
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
štvanice	štvanice	k1gFnSc2	štvanice
v	v	k7c4	v
propracovanější	propracovaný	k2eAgInSc4d2	propracovanější
parforsní	parforsní	k2eAgInSc4d1	parforsní
hon	hon	k1gInSc4	hon
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
e	e	k0	e
o	o	k7c4	o
společný	společný	k2eAgInSc4d1	společný
způsob	způsob	k1gInSc4	způsob
lovu	lov	k1gInSc2	lov
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
jezdeckých	jezdecký	k2eAgMnPc2d1	jezdecký
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
smečky	smečka	k1gFnSc2	smečka
honicích	honicí	k2eAgMnPc2d1	honicí
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Povely	povel	k1gInPc1	povel
účastníkům	účastník	k1gMnPc3	účastník
lovu	lov	k1gInSc3	lov
se	se	k3xPyFc4	se
předávaly	předávat	k5eAaImAgFnP	předávat
pomocí	pomocí	k7c2	pomocí
troubení	troubení	k1gNnSc2	troubení
na	na	k7c4	na
lovecké	lovecký	k2eAgInPc4d1	lovecký
rohy	roh	k1gInPc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
když	když	k8xS	když
psi	pes	k1gMnPc1	pes
spatřili	spatřit	k5eAaPmAgMnP	spatřit
jelena	jelen	k1gMnSc4	jelen
<g/>
,	,	kIx,	,
troubil	troubit	k5eAaImAgInS	troubit
se	se	k3xPyFc4	se
signál	signál	k1gInSc1	signál
"	"	kIx"	"
<g/>
En	En	k1gFnSc1	En
vue	vue	k?	vue
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
jelen	jelen	k1gMnSc1	jelen
psy	pes	k1gMnPc4	pes
zpravidla	zpravidla	k6eAd1	zpravidla
uštván	uštván	k2eAgMnSc1d1	uštván
a	a	k8xC	a
obklíčen	obklíčen	k2eAgMnSc1d1	obklíčen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Stop	stop	k2eAgFnSc7d1	stop
fanfárou	fanfára	k1gFnSc7	fanfára
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
lovců	lovec	k1gMnPc2	lovec
pak	pak	k8xC	pak
jelena	jelen	k1gMnSc4	jelen
dorazil	dorazit	k5eAaPmAgMnS	dorazit
(	(	kIx(	(
<g/>
dal	dát	k5eAaPmAgMnS	dát
mu	on	k3xPp3gInSc3	on
záraz	záraz	k1gInSc4	záraz
<g/>
)	)	kIx)	)
loveckým	lovecký	k2eAgInSc7d1	lovecký
tesákem	tesák	k1gInSc7	tesák
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
troubil	troubit	k5eAaImAgInS	troubit
signál	signál	k1gInSc1	signál
halali	halali	k1gNnSc2	halali
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k6eAd1	franc
<g/>
.	.	kIx.	.
</s>
<s>
Há	Há	k?	Há
<g/>
,	,	kIx,	,
la	la	k1gNnSc1	la
lit	lit	k1gInSc1	lit
–	–	k?	–
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Hle	hle	k0	hle
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
leží	ležet	k5eAaImIp3nS	ležet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
signál	signál	k1gInSc1	signál
halali	halali	k1gNnSc2	halali
<g/>
,	,	kIx,	,
oznamující	oznamující	k2eAgInSc1d1	oznamující
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
konec	konec	k1gInSc1	konec
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poctu	pocta	k1gFnSc4	pocta
složené	složený	k2eAgFnSc3d1	složená
zvěři	zvěř	k1gFnSc3	zvěř
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přejat	přejmout	k5eAaPmNgInS	přejmout
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
způsobů	způsob	k1gInPc2	způsob
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ulovení	ulovení	k1gNnSc6	ulovení
jelena	jelen	k1gMnSc2	jelen
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
lov	lov	k1gInSc1	lov
ukončil	ukončit	k5eAaPmAgInS	ukončit
tzv.	tzv.	kA	tzv.
curée	curé	k1gFnSc2	curé
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
ukázána	ukázán	k2eAgFnSc1d1	ukázána
jelení	jelení	k2eAgFnSc1d1	jelení
trofej	trofej	k1gFnSc1	trofej
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
odměny	odměna	k1gFnPc4	odměna
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vnitřností	vnitřnost	k1gFnPc2	vnitřnost
a	a	k8xC	a
méně	málo	k6eAd2	málo
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
částí	část	k1gFnPc2	část
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dočkali	dočkat	k5eAaPmAgMnP	dočkat
lovečtí	lovecký	k2eAgMnPc1d1	lovecký
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Parforsní	parforsní	k2eAgInPc4d1	parforsní
hony	hon	k1gInPc4	hon
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
nejdéle	dlouho	k6eAd3	dlouho
se	se	k3xPyFc4	se
pořádaly	pořádat	k5eAaImAgInP	pořádat
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
stály	stát	k5eAaImAgFnP	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
Velké	velká	k1gFnSc2	velká
pardubické	pardubický	k2eAgFnSc2d1	pardubická
<g/>
.	.	kIx.	.
</s>
<s>
Dědictvím	dědictví	k1gNnSc7	dědictví
parforsních	parforsní	k2eAgInPc2d1	parforsní
honů	hon	k1gInPc2	hon
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
místy	místy	k6eAd1	místy
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
svatohubertské	svatohubertský	k2eAgFnSc2d1	svatohubertská
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
však	však	k9	však
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
lovena	loven	k2eAgFnSc1d1	lovena
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
oblíben	oblíben	k2eAgInSc4d1	oblíben
lov	lov	k1gInSc4	lov
jelení	jelení	k2eAgFnSc1d1	jelení
i	i	k9	i
jiné	jiný	k2eAgNnSc1d1	jiné
(	(	kIx(	(
<g/>
především	především	k9	především
černé	černý	k2eAgFnSc2d1	černá
<g/>
)	)	kIx)	)
zvěře	zvěř	k1gFnSc2	zvěř
v	v	k7c6	v
leči	leč	k1gFnSc6	leč
obestavěné	obestavěný	k2eAgFnSc6d1	obestavěná
plátny	plátno	k1gNnPc7	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
surový	surový	k2eAgInSc4d1	surový
způsob	způsob	k1gInSc4	způsob
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
zvěř	zvěř	k1gFnSc1	zvěř
nahnána	nahnat	k5eAaPmNgFnS	nahnat
do	do	k7c2	do
ohrady	ohrada	k1gFnSc2	ohrada
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnPc4d1	postavená
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
plachet	plachta	k1gFnPc2	plachta
či	či	k8xC	či
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
napnutých	napnutý	k2eAgFnPc2d1	napnutá
na	na	k7c4	na
tyče	tyč	k1gFnPc4	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Lovci	lovec	k1gMnPc1	lovec
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
i	i	k9	i
dámy	dáma	k1gFnSc2	dáma
<g/>
)	)	kIx)	)
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
ostřelovali	ostřelovat	k5eAaImAgMnP	ostřelovat
z	z	k7c2	z
krytých	krytý	k2eAgInPc2d1	krytý
vyvýšených	vyvýšený	k2eAgInPc2d1	vyvýšený
altánů	altán	k1gInPc2	altán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zvláštní	zvláštní	k2eAgMnPc4d1	zvláštní
milovníky	milovník	k1gMnPc4	milovník
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
lovu	lov	k1gInSc2	lov
patřili	patřit	k5eAaImAgMnP	patřit
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Alžběta	Alžběta	k1gFnSc1	Alžběta
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
sama	sám	k3xTgFnSc1	sám
císařovna	císařovna	k1gFnSc1	císařovna
dokázala	dokázat	k5eAaPmAgFnS	dokázat
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
podobném	podobný	k2eAgInSc6d1	podobný
lovu	lov	k1gInSc6	lov
složit	složit	k5eAaPmF	složit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
laní	laň	k1gFnPc2	laň
a	a	k8xC	a
jelenů	jelen	k1gMnPc2	jelen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jelení	jelení	k2eAgFnSc1d1	jelení
zvěř	zvěř	k1gFnSc1	zvěř
loví	lovit	k5eAaImIp3nP	lovit
obvykle	obvykle	k6eAd1	obvykle
osamělými	osamělý	k2eAgInPc7d1	osamělý
způsoby	způsob	k1gInPc7	způsob
lovu	lov	k1gInSc2	lov
–	–	k?	–
na	na	k7c6	na
čekané	čekaná	k1gFnSc6	čekaná
z	z	k7c2	z
posedu	posed	k1gInSc2	posed
nebo	nebo	k8xC	nebo
šoulačkou	šoulačka	k1gFnSc7	šoulačka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říji	říje	k1gFnSc6	říje
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jelena	jelena	k1gFnSc1	jelena
ulovit	ulovit	k5eAaPmF	ulovit
také	také	k9	také
vábením	vábení	k1gNnSc7	vábení
(	(	kIx(	(
<g/>
napodobením	napodobení	k1gNnSc7	napodobení
jeleního	jelení	k2eAgNnSc2d1	jelení
troubení	troubení	k1gNnSc2	troubení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
společných	společný	k2eAgInPc2d1	společný
honů	hon	k1gInPc2	hon
se	se	k3xPyFc4	se
jelení	jelení	k2eAgFnSc4d1	jelení
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
holá	holý	k2eAgFnSc1d1	holá
(	(	kIx(	(
<g/>
kolouši	kolouch	k1gMnPc1	kolouch
a	a	k8xC	a
laně	laň	k1gFnPc1	laň
<g/>
)	)	kIx)	)
loví	lovit	k5eAaImIp3nP	lovit
naháňkou	naháňka	k1gFnSc7	naháňka
a	a	k8xC	a
nátlačkou	nátlačka	k1gFnSc7	nátlačka
<g/>
.	.	kIx.	.
</s>
<s>
Živou	živý	k2eAgFnSc4d1	živá
jelení	jelení	k2eAgFnSc4d1	jelení
zvěř	zvěř	k1gFnSc4	zvěř
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
odchytávat	odchytávat	k5eAaImF	odchytávat
do	do	k7c2	do
lapacích	lapací	k2eAgFnPc2d1	lapací
ohrad	ohrada	k1gFnPc2	ohrada
nebo	nebo	k8xC	nebo
pomocí	pomoc	k1gFnPc2	pomoc
narkotizační	narkotizační	k2eAgFnSc2d1	narkotizační
střely	střela	k1gFnSc2	střela
<g/>
.	.	kIx.	.
</s>
<s>
Trofejí	trofej	k1gFnSc7	trofej
z	z	k7c2	z
lovu	lov	k1gInSc2	lov
jelení	jelení	k2eAgFnSc2d1	jelení
zvěře	zvěř	k1gFnSc2	zvěř
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
paroží	paroží	k1gNnSc1	paroží
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zakrnělé	zakrnělý	k2eAgInPc4d1	zakrnělý
špičáky	špičák	k1gInPc4	špičák
mandlovitého	mandlovitý	k2eAgInSc2d1	mandlovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kelce	kelec	k1gInSc2	kelec
či	či	k8xC	či
grandle	grandle	k6eAd1	grandle
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
využívaly	využívat	k5eAaPmAgFnP	využívat
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
mysliveckých	myslivecký	k2eAgInPc2d1	myslivecký
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
chutné	chutný	k2eAgNnSc4d1	chutné
maso	maso	k1gNnSc4	maso
se	se	k3xPyFc4	se
jeleni	jelen	k1gMnPc1	jelen
někdy	někdy	k6eAd1	někdy
chovají	chovat	k5eAaImIp3nP	chovat
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
archeologické	archeologický	k2eAgInPc4d1	archeologický
doklady	doklad	k1gInPc4	doklad
o	o	k7c6	o
chovu	chov	k1gInSc6	chov
jelenů	jelen	k1gMnPc2	jelen
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
paleolitu	paleolit	k1gInSc2	paleolit
ještě	ještě	k9	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
domestikace	domestikace	k1gFnSc2	domestikace
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
jelenovitých	jelenovitý	k2eAgFnPc2d1	jelenovitá
vhodnými	vhodný	k2eAgInPc7d1	vhodný
pro	pro	k7c4	pro
farmový	farmový	k2eAgInSc4d1	farmový
chov	chov	k1gInSc4	chov
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
daněk	daněk	k1gMnSc1	daněk
evropský	evropský	k2eAgMnSc1d1	evropský
nebo	nebo	k8xC	nebo
jelen	jelen	k1gMnSc1	jelen
sika	sika	k1gMnSc1	sika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Red	Red	k1gMnSc1	Red
Deer	Deer	k1gMnSc1	Deer
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Andreska	Andreska	k1gFnSc1	Andreska
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
Andresková	Andresková	k1gFnSc1	Andresková
<g/>
,	,	kIx,	,
Erika	Erika	k1gFnSc1	Erika
<g/>
:	:	kIx,	:
Tisíc	tisíc	k4xCgInSc1	tisíc
let	let	k1gInSc1	let
myslivosti	myslivost	k1gFnSc2	myslivost
<g/>
.	.	kIx.	.
</s>
<s>
Vimperk	Vimperk	k1gInSc1	Vimperk
<g/>
:	:	kIx,	:
Tina	Tina	k1gFnSc1	Tina
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakušan	Rakušan	k1gMnSc1	Rakušan
<g/>
,	,	kIx,	,
Ctirad	Ctirad	k1gMnSc1	Ctirad
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Myslivecký	myslivecký	k2eAgInSc4d1	myslivecký
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brázda	Brázda	k1gMnSc1	Brázda
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
:	:	kIx,	:
Zvěř	zvěř	k1gFnSc1	zvěř
a	a	k8xC	a
myslivost	myslivost	k1gFnSc1	myslivost
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
věků	věk	k1gInPc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Emil	Emil	k1gMnSc1	Emil
Hladík	Hladík	k1gMnSc1	Hladík
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pluháček	Pluháček	k1gMnSc1	Pluháček
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
Hrabina	hrabina	k1gFnSc1	hrabina
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
Robovský	Robovský	k1gMnSc1	Robovský
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
České	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
Mammalia	Mammalia	k1gFnSc1	Mammalia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodatek	dodatek	k1gInSc1	dodatek
2	[number]	k4	2
–	–	k?	–
jelenovití	jelenovití	k1gMnPc1	jelenovití
(	(	kIx(	(
<g/>
Cervidae	Cervidae	k1gInSc1	Cervidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kabarovití	kabarovitý	k2eAgMnPc1d1	kabarovitý
(	(	kIx(	(
<g/>
Moschidae	Moschidae	k1gNnSc7	Moschidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
kančilovití	kančilovitý	k2eAgMnPc1d1	kančilovitý
(	(	kIx(	(
<g/>
Tragulidae	Tragulidae	k1gNnSc7	Tragulidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lynx	Lynx	k1gInSc1	Lynx
<g/>
,	,	kIx,	,
n.	n.	k?	n.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
42	[number]	k4	42
<g/>
:	:	kIx,	:
281	[number]	k4	281
<g/>
–	–	k?	–
<g/>
296	[number]	k4	296
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jelen	jelen	k1gMnSc1	jelen
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
jelen	jelena	k1gFnPc2	jelena
lesní	lesní	k2eAgMnPc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Cervus	Cervus	k1gInSc1	Cervus
elaphus	elaphus	k1gInSc1	elaphus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Jelen	Jelen	k1gMnSc1	Jelen
lesní	lesní	k2eAgMnSc1d1	lesní
na	na	k7c4	na
biolib	biolib	k1gInSc4	biolib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
