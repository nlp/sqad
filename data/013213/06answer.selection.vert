<s>
V	v	k7c6	v
kterémkoli	kterýkoli	k3yIgNnSc6	kterýkoli
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
jelení	jelení	k2eAgFnSc1d1	jelení
zvěř	zvěř	k1gFnSc1	zvěř
živí	živit	k5eAaImIp3nS	živit
zejména	zejména	k9	zejména
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
travin	travina	k1gFnPc2	travina
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spásá	spásat	k5eAaPmIp3nS	spásat
na	na	k7c6	na
lesních	lesní	k2eAgFnPc6d1	lesní
mýtinách	mýtina	k1gFnPc6	mýtina
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
okolo	okolo	k7c2	okolo
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
zkrátka	zkrátka	k6eAd1	zkrátka
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
světlu	světlo	k1gNnSc3	světlo
rostlin	rostlina	k1gFnPc2	rostlina
dostatek	dostatek	k1gInSc1	dostatek
<g/>
.	.	kIx.	.
</s>
