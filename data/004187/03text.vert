<s>
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
pomáhat	pomáhat	k5eAaImF	pomáhat
je	on	k3xPp3gNnSc4	on
české	český	k2eAgNnSc4d1	české
filmové	filmový	k2eAgNnSc4d1	filmové
drama	drama	k1gNnSc4	drama
režiséra	režisér	k1gMnSc2	režisér
Jana	Jan	k1gMnSc2	Jan
Hřebejka	Hřebejka	k1gFnSc1	Hřebejka
a	a	k8xC	a
scenáristy	scenárista	k1gMnPc4	scenárista
Petra	Petr	k1gMnSc4	Petr
Jarchovského	Jarchovský	k2eAgMnSc4d1	Jarchovský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
za	za	k7c4	za
německé	německý	k2eAgFnPc4d1	německá
okupace	okupace	k1gFnPc4	okupace
Československa	Československo	k1gNnSc2	Československo
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
hrdinství	hrdinství	k1gNnSc4	hrdinství
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
schovávala	schovávat	k5eAaImAgFnS	schovávat
doma	doma	k6eAd1	doma
židovského	židovský	k2eAgMnSc2d1	židovský
uprchlíka	uprchlík	k1gMnSc2	uprchlík
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
pět	pět	k4xCc4	pět
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
pěti	pět	k4xCc7	pět
nominovanými	nominovaný	k2eAgMnPc7d1	nominovaný
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Jarchovský	Jarchovský	k2eAgMnSc1d1	Jarchovský
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
Divadlo	divadlo	k1gNnSc4	divadlo
Na	na	k7c6	na
Jezerce	Jezerka	k1gFnSc6	Jezerka
předělal	předělat	k5eAaPmAgInS	předělat
film	film	k1gInSc1	film
do	do	k7c2	do
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
divadelního	divadelní	k2eAgNnSc2d1	divadelní
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
1937	[number]	k4	1937
Automobil	automobil	k1gInSc1	automobil
s	s	k7c7	s
řidičem	řidič	k1gMnSc7	řidič
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
cestujícími	cestující	k1gFnPc7	cestující
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
u	u	k7c2	u
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
pasažéři	pasažér	k1gMnPc1	pasažér
se	se	k3xPyFc4	se
jdou	jít	k5eAaImIp3nP	jít
vymočit	vymočit	k5eAaPmF	vymočit
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
udělá	udělat	k5eAaPmIp3nS	udělat
i	i	k9	i
řidič	řidič	k1gMnSc1	řidič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trvá	trvat	k5eAaImIp3nS	trvat
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
mu	on	k3xPp3gMnSc3	on
jakoby	jakoby	k8xS	jakoby
naschvál	naschvál	k6eAd1	naschvál
chtějí	chtít	k5eAaImIp3nP	chtít
ujet	ujet	k5eAaPmF	ujet
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
s	s	k7c7	s
rozepnutým	rozepnutý	k2eAgInSc7d1	rozepnutý
poklopcem	poklopec	k1gInSc7	poklopec
klopýtá	klopýtat	k5eAaImIp3nS	klopýtat
za	za	k7c7	za
automobilem	automobil	k1gInSc7	automobil
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
slitují	slitovat	k5eAaPmIp3nP	slitovat
a	a	k8xC	a
zastaví	zastavit	k5eAaPmIp3nP	zastavit
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
Němec	Němec	k1gMnSc1	Němec
a	a	k8xC	a
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
se	se	k3xPyFc4	se
Horst	Horst	k1gInSc1	Horst
Prohaska	Prohasek	k1gMnSc2	Prohasek
<g/>
,	,	kIx,	,
cestujícími	cestující	k1gMnPc7	cestující
jsou	být	k5eAaImIp3nP	být
Josef	Josef	k1gMnSc1	Josef
Čížek	Čížek	k1gMnSc1	Čížek
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
Žid	Žid	k1gMnSc1	Žid
David	David	k1gMnSc1	David
Wiener	Wiener	k1gMnSc1	Wiener
<g/>
.	.	kIx.	.
</s>
<s>
Čížek	Čížek	k1gMnSc1	Čížek
je	být	k5eAaImIp3nS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
u	u	k7c2	u
rodiny	rodina	k1gFnSc2	rodina
Wienerů	Wiener	k1gMnPc2	Wiener
<g/>
,	,	kIx,	,
Prohaska	Prohaska	k1gFnSc1	Prohaska
tam	tam	k6eAd1	tam
pracuje	pracovat	k5eAaImIp3nS	pracovat
také	také	k9	také
na	na	k7c4	na
nižší	nízký	k2eAgFnSc4d2	nižší
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
Rodina	rodina	k1gFnSc1	rodina
Wienerů	Wiener	k1gMnPc2	Wiener
je	být	k5eAaImIp3nS	být
nucena	nucen	k2eAgFnSc1d1	nucena
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
se	se	k3xPyFc4	se
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
honosného	honosný	k2eAgInSc2d1	honosný
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vystěhování	vystěhování	k1gNnSc2	vystěhování
asistuje	asistovat	k5eAaImIp3nS	asistovat
Prohaska	Prohaska	k1gFnSc1	Prohaska
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
kolaborant	kolaborant	k1gMnSc1	kolaborant
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
prozradí	prozradit	k5eAaPmIp3nS	prozradit
Čížkovi	Čížek	k1gMnSc3	Čížek
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pracovně	pracovna	k1gFnSc6	pracovna
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
skrýši	skrýš	k1gFnSc6	skrýš
část	část	k1gFnSc4	část
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
může	moct	k5eAaImIp3nS	moct
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
bylo	být	k5eAaImAgNnS	být
nejhůř	zle	k6eAd3	zle
<g/>
.	.	kIx.	.
</s>
<s>
Nikomu	nikdo	k3yNnSc3	nikdo
jinému	jiný	k2eAgMnSc3d1	jiný
to	ten	k3xDgNnSc4	ten
neprozradil	prozradit	k5eNaPmAgMnS	prozradit
<g/>
.	.	kIx.	.
</s>
<s>
Wienerovi	Wiener	k1gMnSc3	Wiener
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
do	do	k7c2	do
domu	dům	k1gInSc2	dům
k	k	k7c3	k
Čížkům	Čížek	k1gMnPc3	Čížek
<g/>
.	.	kIx.	.
1941	[number]	k4	1941
Wienerovi	Wienerův	k2eAgMnPc1d1	Wienerův
musí	muset	k5eAaImIp3nP	muset
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
transport	transport	k1gInSc4	transport
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Terezín	Terezín	k1gInSc1	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
se	se	k3xPyFc4	se
loučí	loučit	k5eAaImIp3nS	loučit
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Josefa	Josef	k1gMnSc2	Josef
Čížka	Čížek	k1gMnSc2	Čížek
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
tetina	tetin	k2eAgInSc2d1	tetin
dopisu	dopis	k1gInSc2	dopis
z	z	k7c2	z
Terezína	Terezín	k1gInSc2	Terezín
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
dobře	dobře	k6eAd1	dobře
postaráno	postarán	k2eAgNnSc1d1	postaráno
<g/>
,	,	kIx,	,
jenom	jenom	k8xS	jenom
jim	on	k3xPp3gMnPc3	on
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
psala	psát	k5eAaImAgFnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
strýc	strýc	k1gMnSc1	strýc
Otto	Otto	k1gMnSc1	Otto
kašle	kašlat	k5eAaImIp3nS	kašlat
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
let	léto	k1gNnPc2	léto
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
1943	[number]	k4	1943
Davidu	David	k1gMnSc3	David
Wienerovi	Wiener	k1gMnSc3	Wiener
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ještě	ještě	k9	ještě
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
mužem	muž	k1gMnSc7	muž
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
posláni	poslat	k5eAaPmNgMnP	poslat
z	z	k7c2	z
Terezína	Terezín	k1gInSc2	Terezín
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
uplatili	uplatit	k5eAaPmAgMnP	uplatit
vojáka	voják	k1gMnSc2	voják
SS	SS	kA	SS
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
spatří	spatřit	k5eAaPmIp3nS	spatřit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
soused	soused	k1gMnSc1	soused
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pokusí	pokusit	k5eAaPmIp3nS	pokusit
zavolat	zavolat	k5eAaPmF	zavolat
německého	německý	k2eAgMnSc4d1	německý
vojáka	voják	k1gMnSc4	voják
na	na	k7c4	na
sajdkáře	sajdkář	k1gMnSc4	sajdkář
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jude	Jude	k1gInSc1	Jude
ist	ist	k?	ist
hier	hier	k1gInSc1	hier
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Je	být	k5eAaImIp3nS	být
tady	tady	k6eAd1	tady
Žid	Žid	k1gMnSc1	Žid
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
Německý	německý	k2eAgMnSc1d1	německý
voják	voják	k1gMnSc1	voják
ho	on	k3xPp3gNnSc4	on
ale	ale	k9	ale
neslyší	slyšet	k5eNaImIp3nS	slyšet
a	a	k8xC	a
jede	jet	k5eAaImIp3nS	jet
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
se	se	k3xPyFc4	se
ukryje	ukrýt	k5eAaPmIp3nS	ukrýt
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
domě	dům	k1gInSc6	dům
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Penzionovaný	penzionovaný	k2eAgMnSc1d1	penzionovaný
Josef	Josef	k1gMnSc1	Josef
Čížek	Čížek	k1gMnSc1	Čížek
(	(	kIx(	(
<g/>
Bolek	Bolek	k1gMnSc1	Bolek
Polívka	Polívka	k1gMnSc1	Polívka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
během	během	k7c2	během
války	válka	k1gFnSc2	válka
snaží	snažit	k5eAaImIp3nP	snažit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Marií	Maria	k1gFnSc7	Maria
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Šišková	Šišková	k1gFnSc1	Šišková
<g/>
)	)	kIx)	)
přežívat	přežívat	k5eAaImF	přežívat
v	v	k7c6	v
co	co	k9	co
největší	veliký	k2eAgFnSc6d3	veliký
tichosti	tichost	k1gFnSc6	tichost
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
mu	on	k3xPp3gMnSc3	on
vlezlý	vlezlý	k2eAgInSc1d1	vlezlý
Prohaska	Prohasko	k1gNnSc2	Prohasko
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
domu	dům	k1gInSc2	dům
po	po	k7c6	po
Wienerech	Wiener	k1gMnPc6	Wiener
se	se	k3xPyFc4	se
nastěhují	nastěhovat	k5eAaPmIp3nP	nastěhovat
noví	nový	k2eAgMnPc1d1	nový
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
vědom	vědom	k2eAgMnSc1d1	vědom
slibu	slib	k1gInSc3	slib
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
domu	dům	k1gInSc2	dům
vypravit	vypravit	k5eAaPmF	vypravit
a	a	k8xC	a
vzít	vzít	k5eAaPmF	vzít
ukryté	ukrytý	k2eAgInPc4d1	ukrytý
šperky	šperk	k1gInPc4	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
narazí	narazit	k5eAaPmIp3nP	narazit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
Davida	David	k1gMnSc4	David
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc2	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
skrýt	skrýt	k5eAaPmF	skrýt
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
spíži	spíž	k1gFnSc6	spíž
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Wiener	Wiener	k1gMnSc1	Wiener
Čížkovi	Čížkův	k2eAgMnPc1d1	Čížkův
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
slíbí	slíbit	k5eAaPmIp3nP	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
uprchlíkem	uprchlík	k1gMnSc7	uprchlík
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
jej	on	k3xPp3gMnSc4	on
tam	tam	k6eAd1	tam
odveze	odvézt	k5eAaPmIp3nS	odvézt
vypůjčeným	vypůjčený	k2eAgInSc7d1	vypůjčený
automobilem	automobil	k1gInSc7	automobil
a	a	k8xC	a
když	když	k8xS	když
druhý	druhý	k4xOgMnSc1	druhý
muž	muž	k1gMnSc1	muž
dlouho	dlouho	k6eAd1	dlouho
nejde	jít	k5eNaImIp3nS	jít
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
David	David	k1gMnSc1	David
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Čížek	Čížek	k1gMnSc1	Čížek
svádí	svádět	k5eAaImIp3nS	svádět
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
boj	boj	k1gInSc4	boj
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
Davidovi	David	k1gMnSc3	David
pomoci	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
ukrýt	ukrýt	k5eAaPmF	ukrýt
ho	on	k3xPp3gInSc4	on
doma	doma	k6eAd1	doma
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
odhalení	odhalení	k1gNnSc1	odhalení
(	(	kIx(	(
<g/>
a	a	k8xC	a
popravu	poprava	k1gFnSc4	poprava
za	za	k7c2	za
ukrývání	ukrývání	k1gNnSc2	ukrývání
uprchlíka	uprchlík	k1gMnSc2	uprchlík
<g/>
)	)	kIx)	)
riskuje	riskovat	k5eAaBmIp3nS	riskovat
již	již	k6eAd1	již
při	při	k7c6	při
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
zastaví	zastavit	k5eAaPmIp3nS	zastavit
německý	německý	k2eAgMnSc1d1	německý
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
požádá	požádat	k5eAaPmIp3nS	požádat
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
klíč	klíč	k1gInSc4	klíč
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
svého	své	k1gNnSc2	své
vozu	vůz	k1gInSc2	vůz
Tatra	Tatra	k1gFnSc1	Tatra
87	[number]	k4	87
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
Davida	David	k1gMnSc2	David
ukrytého	ukrytý	k2eAgMnSc2d1	ukrytý
v	v	k7c6	v
kufru	kufr	k1gInSc6	kufr
automobilu	automobil	k1gInSc2	automobil
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
války	válka	k1gFnSc2	válka
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
Čížek	Čížek	k1gMnSc1	Čížek
tolerovat	tolerovat	k5eAaImF	tolerovat
chování	chování	k1gNnSc4	chování
Prohasky	Prohaska	k1gFnSc2	Prohaska
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dušek	Dušek	k1gMnSc1	Dušek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
Čížkovým	Čížkův	k2eAgNnSc7d1	Čížkův
známým	známý	k2eAgNnSc7d1	známé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
návštěvy	návštěva	k1gFnPc4	návštěva
chodí	chodit	k5eAaImIp3nS	chodit
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zlíbí	zlíbit	k5eAaPmIp3nS	zlíbit
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgMnSc2	jeden
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
když	když	k8xS	když
náhodou	náhodou	k6eAd1	náhodou
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
přes	přes	k7c4	přes
okapovou	okapový	k2eAgFnSc4d1	okapová
rouru	roura	k1gFnSc4	roura
hovor	hovor	k1gInSc1	hovor
<g/>
)	)	kIx)	)
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čížkovi	Čížek	k1gMnSc3	Čížek
někoho	někdo	k3yInSc4	někdo
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neudá	udat	k5eNaPmIp3nS	udat
je	on	k3xPp3gNnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Marii	Maria	k1gFnSc4	Maria
o	o	k7c6	o
zkušenostech	zkušenost	k1gFnPc6	zkušenost
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
sestře	sestra	k1gFnSc3	sestra
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
místo	místo	k1gNnSc1	místo
u	u	k7c2	u
ženského	ženský	k2eAgInSc2d1	ženský
oddílu	oddíl	k1gInSc2	oddíl
Sonderkommandos	Sonderkommandosa	k1gFnPc2	Sonderkommandosa
<g/>
.	.	kIx.	.
</s>
<s>
Musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
ale	ale	k8xC	ale
ukázat	ukázat	k5eAaPmF	ukázat
bezcitnost	bezcitnost	k1gFnSc4	bezcitnost
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
ubít	ubít	k5eAaPmF	ubít
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rodiče	rodič	k1gMnPc1	rodič
prosili	prosít	k5eAaPmAgMnP	prosít
sestru	sestra	k1gFnSc4	sestra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc4	ten
udělala	udělat	k5eAaPmAgFnS	udělat
(	(	kIx(	(
<g/>
aby	aby	k9	aby
sama	sám	k3xTgFnSc1	sám
měla	mít	k5eAaImAgFnS	mít
větší	veliký	k2eAgFnSc4d2	veliký
naději	naděje	k1gFnSc4	naděje
přežít	přežít	k5eAaPmF	přežít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prohaska	Prohaska	k1gFnSc1	Prohaska
pak	pak	k6eAd1	pak
Čížkovi	Čížek	k1gMnSc3	Čížek
nabízí	nabízet	k5eAaImIp3nS	nabízet
práci	práce	k1gFnSc4	práce
při	při	k7c6	při
sepisování	sepisování	k1gNnSc6	sepisování
majetku	majetek	k1gInSc2	majetek
zabaveného	zabavený	k2eAgInSc2d1	zabavený
Židům	Žid	k1gMnPc3	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
brání	bránit	k5eAaImIp3nS	bránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
na	na	k7c4	na
popud	popud	k1gInSc4	popud
manželky	manželka	k1gFnSc2	manželka
přijme	přijmout	k5eAaPmIp3nS	přijmout
(	(	kIx(	(
<g/>
zmenší	zmenšit	k5eAaPmIp3nS	zmenšit
tak	tak	k9	tak
podezření	podezření	k1gNnSc4	podezření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
někteří	některý	k3yIgMnPc1	některý
jeho	jeho	k3xOp3gMnPc1	jeho
sousedé	soused	k1gMnPc1	soused
odplivují	odplivovat	k5eAaImIp3nP	odplivovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
podezírán	podezírat	k5eAaImNgInS	podezírat
z	z	k7c2	z
kolaborace	kolaborace	k1gFnSc2	kolaborace
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
postupně	postupně	k6eAd1	postupně
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
předvídatelným	předvídatelný	k2eAgFnPc3d1	předvídatelná
liniím	linie	k1gFnPc3	linie
(	(	kIx(	(
<g/>
Prohaska	Prohaska	k1gFnSc1	Prohaska
vezme	vzít	k5eAaPmIp3nS	vzít
Marii	Maria	k1gFnSc4	Maria
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
Josefa	Josef	k1gMnSc2	Josef
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
snaží	snažit	k5eAaImIp3nS	snažit
svést	svést	k5eAaPmF	svést
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
Marii	Maria	k1gFnSc3	Maria
nepříjemné	příjemný	k2eNgFnSc3d1	nepříjemná
<g/>
)	)	kIx)	)
i	i	k8xC	i
neočekávaným	očekávaný	k2eNgInPc3d1	neočekávaný
zvratům	zvrat	k1gInPc3	zvrat
<g/>
.	.	kIx.	.
</s>
<s>
Kepke	Kepkat	k5eAaPmIp3nS	Kepkat
se	se	k3xPyFc4	se
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
nejmladšího	mladý	k2eAgMnSc4d3	nejmladší
neplnoletého	plnoletý	k2eNgMnSc4d1	neplnoletý
syna	syn	k1gMnSc4	syn
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
obviněn	obviněn	k2eAgMnSc1d1	obviněn
z	z	k7c2	z
dezerce	dezerce	k1gFnSc2	dezerce
a	a	k8xC	a
zastřelen	zastřelen	k2eAgMnSc1d1	zastřelen
<g/>
,	,	kIx,	,
Prohaska	Prohaska	k1gFnSc1	Prohaska
chce	chtít	k5eAaImIp3nS	chtít
nastěhovat	nastěhovat	k5eAaPmF	nastěhovat
Kepkeho	Kepke	k1gMnSc4	Kepke
k	k	k7c3	k
Čížkům	Čížek	k1gMnPc3	Čížek
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Marie	Marie	k1gFnSc1	Marie
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
je	být	k5eAaImIp3nS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
být	být	k5eAaImF	být
natolik	natolik	k6eAd1	natolik
přesvědčivá	přesvědčivý	k2eAgFnSc1d1	přesvědčivá
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Horsta	Horst	k1gMnSc2	Horst
<g/>
)	)	kIx)	)
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
a	a	k8xC	a
naštve	naštvat	k5eAaBmIp3nS	naštvat
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zahla	zahnout	k5eAaImAgFnS	zahnout
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
infertilní	infertilní	k2eAgMnSc1d1	infertilní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
údajné	údajný	k2eAgNnSc1d1	údajné
těhotenství	těhotenství	k1gNnSc1	těhotenství
provalí	provalit	k5eAaPmIp3nS	provalit
<g/>
,	,	kIx,	,
Čížek	Čížek	k1gMnSc1	Čížek
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc7	jejich
jedinou	jediný	k2eAgFnSc7d1	jediná
nadějí	naděje	k1gFnSc7	naděje
je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
dítě	dítě	k1gNnSc4	dítě
pořídit	pořídit	k5eAaPmF	pořídit
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
přimět	přimět	k5eAaPmF	přimět
Marii	Maria	k1gFnSc4	Maria
k	k	k7c3	k
souloži	soulož	k1gFnSc3	soulož
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Marii	Maria	k1gFnSc4	Maria
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
těžko	těžko	k6eAd1	těžko
přijatelné	přijatelný	k2eAgNnSc1d1	přijatelné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
mladým	mladý	k2eAgMnSc7d1	mladý
Wienerem	Wiener	k1gMnSc7	Wiener
oplodnit	oplodnit	k5eAaPmF	oplodnit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Horst	Horst	k1gFnSc1	Horst
Prohaska	Prohask	k1gInSc2	Prohask
Marii	Maria	k1gFnSc3	Maria
omluví	omluvit	k5eAaPmIp3nS	omluvit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
všem	všecek	k3xTgMnPc3	všecek
zachrání	zachránit	k5eAaPmIp3nP	zachránit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
německé	německý	k2eAgFnSc6d1	německá
razii	razie	k1gFnSc6	razie
využije	využít	k5eAaPmIp3nS	využít
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
zabrání	zabránit	k5eAaPmIp3nS	zabránit
německému	německý	k2eAgMnSc3d1	německý
veliteli	velitel	k1gMnSc3	velitel
v	v	k7c6	v
prohlídce	prohlídka	k1gFnSc6	prohlídka
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
Osvobození	osvobození	k1gNnPc2	osvobození
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
pořádány	pořádán	k2eAgInPc1d1	pořádán
pogromy	pogrom	k1gInPc1	pogrom
na	na	k7c4	na
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
kolaboranty	kolaborant	k1gMnPc4	kolaborant
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
bude	být	k5eAaImBp3nS	být
brzy	brzy	k6eAd1	brzy
rodit	rodit	k5eAaImF	rodit
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaPmIp3nS	vydávat
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
sehnat	sehnat	k5eAaPmF	sehnat
doktora	doktor	k1gMnSc4	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Fischer	Fischer	k1gMnSc1	Fischer
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
odvážen	odvážen	k2eAgMnSc1d1	odvážen
odbojáři	odbojář	k1gMnPc1	odbojář
pryč	pryč	k1gFnSc1	pryč
a	a	k8xC	a
Čížek	Čížek	k1gMnSc1	Čížek
jde	jít	k5eAaImIp3nS	jít
až	až	k9	až
na	na	k7c6	na
velitelství	velitelství	k1gNnSc6	velitelství
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
spoluobčanů	spoluobčan	k1gMnPc2	spoluobčan
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
také	také	k9	také
kolaborant	kolaborant	k1gMnSc1	kolaborant
<g/>
.	.	kIx.	.
</s>
<s>
Hájí	hájit	k5eAaImIp3nS	hájit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachránil	zachránit	k5eAaPmAgMnS	zachránit
jednoho	jeden	k4xCgMnSc2	jeden
židovského	židovský	k2eAgMnSc2d1	židovský
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
navíc	navíc	k6eAd1	navíc
místní	místní	k2eAgFnPc1d1	místní
znají	znát	k5eAaImIp3nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
svolí	svolit	k5eAaPmIp3nS	svolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
šel	jít	k5eAaImAgMnS	jít
doktor	doktor	k1gMnSc1	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Čížek	Čížek	k1gMnSc1	Čížek
jej	on	k3xPp3gNnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nS	vidět
zajatého	zajatý	k2eAgMnSc4d1	zajatý
a	a	k8xC	a
zmláceného	zmlácený	k2eAgMnSc4d1	zmlácený
Horsta	Horst	k1gMnSc4	Horst
Prohasku	Prohasek	k1gInSc2	Prohasek
<g/>
,	,	kIx,	,
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
doktor	doktor	k1gMnSc1	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnSc1d1	místní
člen	člen	k1gMnSc1	člen
domobrany	domobrana	k1gFnSc2	domobrana
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
zná	znát	k5eAaImIp3nS	znát
<g/>
,	,	kIx,	,
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Čížků	Čížek	k1gMnPc2	Čížek
doma	doma	k6eAd1	doma
musí	muset	k5eAaImIp3nS	muset
Horst	Horst	k1gMnSc1	Horst
pomoci	pomoct	k5eAaPmF	pomoct
s	s	k7c7	s
porodem	porod	k1gInSc7	porod
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
nemůže	moct	k5eNaImIp3nS	moct
najít	najít	k5eAaPmF	najít
Davida	David	k1gMnSc4	David
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vše	všechen	k3xTgNnSc4	všechen
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
a	a	k8xC	a
odvádí	odvádět	k5eAaImIp3nS	odvádět
ho	on	k3xPp3gMnSc4	on
ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
hodlá	hodlat	k5eAaImIp3nS	hodlat
popravit	popravit	k5eAaPmF	popravit
zastřelením	zastřelení	k1gNnSc7	zastřelení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
moment	moment	k1gInSc4	moment
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
David	David	k1gMnSc1	David
a	a	k8xC	a
zachrání	zachránit	k5eAaPmIp3nS	zachránit
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
řekne	říct	k5eAaPmIp3nS	říct
kapitánovi	kapitán	k1gMnSc3	kapitán
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prohaska	Prohaska	k1gFnSc1	Prohaska
je	být	k5eAaImIp3nS	být
slušný	slušný	k2eAgMnSc1d1	slušný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnSc1d1	místní
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
patrolou	patrolou	k?	patrolou
pak	pak	k6eAd1	pak
podotkne	podotknout	k5eAaPmIp3nS	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prohaska	Prohaska	k1gFnSc1	Prohaska
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
epilogu	epilog	k1gInSc6	epilog
tlačí	tlačit	k5eAaImIp3nS	tlačit
Čížek	Čížek	k1gMnSc1	Čížek
kočárek	kočárek	k1gInSc4	kočárek
s	s	k7c7	s
Davidovým	Davidův	k2eAgNnSc7d1	Davidovo
dítětem	dítě	k1gNnSc7	dítě
po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
rozbombardovaného	rozbombardovaný	k2eAgNnSc2d1	rozbombardované
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snové	snový	k2eAgFnSc6d1	snová
představě	představa	k1gFnSc6	představa
vidí	vidět	k5eAaImIp3nS	vidět
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
stolu	stol	k1gInSc2	stol
piknikovat	piknikovat	k5eAaPmF	piknikovat
společně	společně	k6eAd1	společně
pana	pan	k1gMnSc4	pan
Wienera	Wiener	k1gMnSc4	Wiener
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
(	(	kIx(	(
<g/>
mrtvi	mrtvit	k5eAaImRp2nS	mrtvit
<g/>
,	,	kIx,	,
o	o	k7c6	o
nich	on	k3xPp3gInPc2	on
mluvil	mluvit	k5eAaImAgMnS	mluvit
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kepkeho	Kepke	k1gMnSc2	Kepke
nejmladšího	mladý	k2eAgMnSc2d3	nejmladší
syna	syn	k1gMnSc2	syn
(	(	kIx(	(
<g/>
zastřelen	zastřelit	k5eAaPmNgInS	zastřelit
za	za	k7c4	za
dezerci	dezerce	k1gFnSc4	dezerce
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
i	i	k9	i
pes	pes	k1gMnSc1	pes
od	od	k7c2	od
sousedky	sousedka	k1gFnSc2	sousedka
(	(	kIx(	(
<g/>
kterého	který	k3yRgMnSc4	který
Němci	Němec	k1gMnPc1	Němec
odstřelili	odstřelit	k5eAaPmAgMnP	odstřelit
při	při	k7c6	při
razii	razie	k1gFnSc6	razie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
vytahuje	vytahovat	k5eAaImIp3nS	vytahovat
z	z	k7c2	z
kočárku	kočárek	k1gInSc2	kočárek
polonahé	polonahý	k2eAgNnSc4d1	polonahé
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jim	on	k3xPp3gMnPc3	on
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
usmívají	usmívat	k5eAaImIp3nP	usmívat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
máme	mít	k5eAaImIp1nP	mít
jen	jen	k9	jen
větší	veliký	k2eAgFnSc4d2	veliký
a	a	k8xC	a
světlejší	světlý	k2eAgFnSc4d2	světlejší
celu	cela	k1gFnSc4	cela
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Wienerem	Wiener	k1gMnSc7	Wiener
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oni	onen	k3xDgMnPc1	onen
nejsou	být	k5eNaImIp3nP	být
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
více	hodně	k6eAd2	hodně
svobodnější	svobodný	k2eAgMnSc1d2	svobodnější
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
ukrytý	ukrytý	k2eAgMnSc1d1	ukrytý
ve	v	k7c6	v
spíži	spíž	k1gFnSc6	spíž
"	"	kIx"	"
<g/>
Víte	vědět	k5eAaImIp2nP	vědět
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
pánové	pán	k1gMnPc1	pán
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
samozřejmě	samozřejmě	k6eAd1	samozřejmě
cítím	cítit	k5eAaImIp1nS	cítit
zármutek	zármutek	k1gInSc1	zármutek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převládá	převládat	k5eAaImIp3nS	převládat
hrdost	hrdost	k1gFnSc1	hrdost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
Němec	Němec	k1gMnSc1	Němec
Albrecht	Albrecht	k1gMnSc1	Albrecht
Kepke	Kepke	k1gNnSc2	Kepke
popisuje	popisovat	k5eAaImIp3nS	popisovat
Čížkovi	Čížkův	k2eAgMnPc1d1	Čížkův
a	a	k8xC	a
Prohaskovi	Prohaskův	k2eAgMnPc1d1	Prohaskův
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
když	když	k8xS	když
myslí	myslet	k5eAaImIp3nS	myslet
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
padl	padnout	k5eAaPmAgMnS	padnout
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
u	u	k7c2	u
Charkova	Charkov	k1gInSc2	Charkov
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
pomáhat	pomáhat	k5eAaImF	pomáhat
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
pomáhat	pomáhat	k5eAaImF	pomáhat
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
pomáhat	pomáhat	k5eAaImF	pomáhat
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
