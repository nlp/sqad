<s>
Fermium	fermium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Fm	Fm	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dvanáctým	dvanáctý	k4xOgInSc7
členem	člen	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
osmým	osmý	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
ozařováním	ozařování	k1gNnSc7
jader	jádro	k1gNnPc2
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>