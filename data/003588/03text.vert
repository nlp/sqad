<s>
Telegrafie	telegrafie	k1gFnSc1	telegrafie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
tele	tele	k1gNnSc1	tele
(	(	kIx(	(
<g/>
τ	τ	k?	τ
<g/>
)	)	kIx)	)
=	=	kIx~	=
daleký	daleký	k2eAgInSc1d1	daleký
a	a	k8xC	a
grafein	grafein	k2eAgInSc1d1	grafein
(	(	kIx(	(
<g/>
γ	γ	k?	γ
<g/>
)	)	kIx)	)
=	=	kIx~	=
psát	psát	k5eAaImF	psát
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
telekomunikační	telekomunikační	k2eAgFnSc1d1	telekomunikační
metoda	metoda	k1gFnSc1	metoda
umožňující	umožňující	k2eAgFnSc1d1	umožňující
přenést	přenést	k5eAaPmF	přenést
obsah	obsah	k1gInSc4	obsah
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
telegramů	telegram	k1gInPc2	telegram
<g/>
)	)	kIx)	)
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počátky	počátek	k1gInPc4	počátek
telegrafie	telegrafie	k1gFnSc2	telegrafie
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
dopravování	dopravování	k1gNnSc4	dopravování
velmi	velmi	k6eAd1	velmi
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
zpráv	zpráva	k1gFnPc2	zpráva
pomocí	pomocí	k7c2	pomocí
bubnů	buben	k1gInPc2	buben
(	(	kIx(	(
<g/>
tam-tamy	tamam	k1gInPc4	tam-tam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kouřových	kouřový	k2eAgInPc2d1	kouřový
signálů	signál	k1gInPc2	signál
apod.	apod.	kA	apod.
</s>
<s>
První	první	k4xOgInSc4	první
optický	optický	k2eAgInSc4d1	optický
telegraf	telegraf	k1gInSc4	telegraf
představil	představit	k5eAaPmAgMnS	představit
Robert	Robert	k1gMnSc1	Robert
Hook	Hook	k1gMnSc1	Hook
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
přednášek	přednáška	k1gFnPc2	přednáška
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1684	[number]	k4	1684
<g/>
.	.	kIx.	.
</s>
<s>
Předvedl	předvést	k5eAaPmAgInS	předvést
posluchačům	posluchač	k1gMnPc3	posluchač
zařízení	zařízení	k1gNnSc4	zařízení
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
s	s	k7c7	s
trojúhelníkovým	trojúhelníkový	k2eAgInSc7d1	trojúhelníkový
terčem	terč	k1gInSc7	terč
<g/>
,	,	kIx,	,
posouvaným	posouvaný	k2eAgInSc7d1	posouvaný
a	a	k8xC	a
natáčeným	natáčený	k2eAgInSc7d1	natáčený
soustavou	soustava	k1gFnSc7	soustava
lan	lano	k1gNnPc2	lano
a	a	k8xC	a
kladek	kladka	k1gFnPc2	kladka
<g/>
.	.	kIx.	.
</s>
<s>
Polohám	poloha	k1gFnPc3	poloha
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
číslice	číslice	k1gFnPc4	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
možno	možno	k6eAd1	možno
signalizovat	signalizovat	k5eAaImF	signalizovat
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
myšlenka	myšlenka	k1gFnSc1	myšlenka
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
neujala	ujmout	k5eNaPmAgFnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
pokusů	pokus	k1gInPc2	pokus
Francouz	Francouz	k1gMnSc1	Francouz
Claude	Claud	k1gInSc5	Claud
Chappe	Chapp	k1gInSc5	Chapp
semaforový	semaforový	k2eAgInSc4d1	semaforový
telegraf	telegraf	k1gInSc4	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
stožáru	stožár	k1gInSc6	stožár
umístěna	umístěn	k2eAgNnPc1d1	umístěno
pohyblivá	pohyblivý	k2eAgNnPc1d1	pohyblivé
ramena	rameno	k1gNnPc1	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Kombinací	kombinace	k1gFnSc7	kombinace
natočení	natočení	k1gNnSc2	natočení
ramen	rameno	k1gNnPc2	rameno
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zakódovat	zakódovat	k5eAaPmF	zakódovat
až	až	k9	až
196	[number]	k4	196
různých	různý	k2eAgInPc2d1	různý
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Věže	věž	k1gFnPc1	věž
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
si	se	k3xPyFc3	se
předávaly	předávat	k5eAaImAgFnP	předávat
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Trasy	trasa	k1gFnPc1	trasa
tohoto	tento	k3xDgInSc2	tento
telegrafu	telegraf	k1gInSc2	telegraf
po	po	k7c6	po
Francii	Francie	k1gFnSc6	Francie
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
vydržel	vydržet	k5eAaPmAgInS	vydržet
až	až	k9	až
do	do	k7c2	do
objevu	objev	k1gInSc2	objev
elektrického	elektrický	k2eAgInSc2d1	elektrický
telegrafu	telegraf	k1gInSc2	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
optického	optický	k2eAgInSc2d1	optický
telegrafu	telegraf	k1gInSc2	telegraf
(	(	kIx(	(
<g/>
o.	o.	k?	o.
t.	t.	k?	t.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
Tróje	Trója	k1gFnSc2	Trója
bylo	být	k5eAaImAgNnS	být
prý	prý	k9	prý
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
do	do	k7c2	do
Mykén	Mykény	k1gFnPc2	Mykény
ohňovými	ohňový	k2eAgInPc7d1	ohňový
signály	signál	k1gInPc7	signál
řetězcem	řetězec	k1gInSc7	řetězec
ohnišť	ohniště	k1gNnPc2	ohniště
asi	asi	k9	asi
v	v	k7c6	v
r.	r.	kA	r.
1184	[number]	k4	1184
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Římě	Řím	k1gInSc6	Řím
vyslána	vyslat	k5eAaPmNgFnS	vyslat
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
císař	císař	k1gMnSc1	císař
Claudius	Claudius	k1gMnSc1	Claudius
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svoje	svůj	k3xOyFgNnPc4	svůj
vítězství	vítězství	k1gNnPc4	vítězství
nad	nad	k7c7	nad
britskými	britský	k2eAgInPc7d1	britský
kmeny	kmen	k1gInPc7	kmen
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
46	[number]	k4	46
n.	n.	k?	n.
l.	l.	k?	l.
Zkoušky	zkouška	k1gFnSc2	zkouška
s	s	k7c7	s
o.	o.	k?	o.
t.	t.	k?	t.
dělal	dělat	k5eAaImAgInS	dělat
v	v	k7c6	v
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
i	i	k8xC	i
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
Johann	Johann	k1gMnSc1	Johann
Lorenz	Lorenz	k1gMnSc1	Lorenz
Boeckmann	Boeckmann	k1gMnSc1	Boeckmann
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
vynálezce	vynálezce	k1gMnSc1	vynálezce
Claude	Claud	k1gInSc5	Claud
Chappe	Chapp	k1gInSc5	Chapp
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ignácem	Ignác	k1gMnSc7	Ignác
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
optický	optický	k2eAgInSc1d1	optický
semaforový	semaforový	k2eAgInSc1d1	semaforový
telegraf	telegraf	k1gInSc1	telegraf
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
jako	jako	k8xS	jako
telegraf	telegraf	k1gInSc4	telegraf
Chappův	Chappův	k2eAgInSc4d1	Chappův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
jej	on	k3xPp3gMnSc4	on
nainstaloval	nainstalovat	k5eAaPmAgMnS	nainstalovat
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
pařížského	pařížský	k2eAgInSc2d1	pařížský
Louvru	Louvre	k1gInSc2	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
ovládanými	ovládaný	k2eAgMnPc7d1	ovládaný
rameny	rameno	k1gNnPc7	rameno
bylo	být	k5eAaImAgNnS	být
možno	možno	k9	možno
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
dohledu	dohled	k1gInSc2	dohled
oka	oko	k1gNnSc2	oko
vysílat	vysílat	k5eAaImF	vysílat
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
76	[number]	k4	76
polohách	poloha	k1gFnPc6	poloha
písmena	písmeno	k1gNnSc2	písmeno
<g/>
,	,	kIx,	,
číslice	číslice	k1gFnSc2	číslice
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
diakritická	diakritický	k2eAgNnPc1d1	diakritické
znaménka	znaménko	k1gNnPc1	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
linka	linka	k1gFnSc1	linka
Chappova	Chappův	k2eAgInSc2d1	Chappův
telegrafu	telegraf	k1gInSc2	telegraf
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
do	do	k7c2	do
Lille	Lille	k1gFnSc2	Lille
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
30	[number]	k4	30
mil	míle	k1gFnPc2	míle
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
22	[number]	k4	22
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnPc1	stanice
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
na	na	k7c6	na
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
věžích	věž	k1gFnPc6	věž
<g/>
,	,	kIx,	,
nadstavbách	nadstavba	k1gFnPc6	nadstavba
<g/>
,	,	kIx,	,
střechách	střecha	k1gFnPc6	střecha
či	či	k8xC	či
vyvýšeninách	vyvýšenina	k1gFnPc6	vyvýšenina
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zaučení	zaučení	k1gNnSc4	zaučení
telegrafistů	telegrafista	k1gMnPc2	telegrafista
byla	být	k5eAaImAgFnS	být
zpráva	zpráva	k1gFnSc1	zpráva
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
trase	trasa	k1gFnSc6	trasa
přenesena	přenést	k5eAaPmNgFnS	přenést
za	za	k7c4	za
4	[number]	k4	4
minuty	minuta	k1gFnSc2	minuta
a	a	k8xC	a
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
stanic	stanice	k1gFnPc2	stanice
rychle	rychle	k6eAd1	rychle
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
šířila	šířit	k5eAaImAgFnS	šířit
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
napoleonských	napoleonský	k2eAgNnPc2d1	napoleonské
tažení	tažení	k1gNnPc2	tažení
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
používal	používat	k5eAaImAgInS	používat
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
předávání	předávání	k1gNnSc2	předávání
informací	informace	k1gFnPc2	informace
pro	pro	k7c4	pro
koordinaci	koordinace	k1gFnSc4	koordinace
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
byl	být	k5eAaImAgInS	být
o.	o.	k?	o.
t.	t.	k?	t.
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
534	[number]	k4	534
stanic	stanice	k1gFnPc2	stanice
spojoval	spojovat	k5eAaImAgMnS	spojovat
Paříž	Paříž	k1gFnSc4	Paříž
s	s	k7c7	s
29	[number]	k4	29
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
rychlostí	rychlost	k1gFnSc7	rychlost
předávání	předávání	k1gNnSc2	předávání
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
Lyon	Lyon	k1gInSc1	Lyon
předal	předat	k5eAaPmAgInS	předat
zprávu	zpráva	k1gFnSc4	zpráva
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
20	[number]	k4	20
stanic	stanice	k1gFnPc2	stanice
za	za	k7c4	za
2	[number]	k4	2
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
XIX	XIX	kA	XIX
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
o.	o.	k?	o.
t.	t.	k?	t.
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
už	už	k6eAd1	už
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
součásti	součást	k1gFnPc4	součást
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
první	první	k4xOgFnSc1	první
linka	linka	k1gFnSc1	linka
o.	o.	k?	o.
t.	t.	k?	t.
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Varšava	Varšava	k1gFnSc1	Varšava
-	-	kIx~	-
Modlin	Modlin	k1gInSc1	Modlin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
sítě	síť	k1gFnSc2	síť
146	[number]	k4	146
stanic	stanice	k1gFnPc2	stanice
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
síť	síť	k1gFnSc1	síť
Varšava	Varšava	k1gFnSc1	Varšava
-	-	kIx~	-
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
začínala	začínat	k5eAaImAgFnS	začínat
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
Teatr	Teatr	k1gInSc1	Teatr
Wielki	Wielk	k1gFnSc2	Wielk
<g/>
)	)	kIx)	)
na	na	k7c6	na
varšavském	varšavský	k2eAgNnSc6d1	Varšavské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
Plac	plac	k1gInSc1	plac
Teatralny	Teatralna	k1gFnSc2	Teatralna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
původní	původní	k2eAgFnSc2d1	původní
stanice	stanice	k1gFnSc2	stanice
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
terase	teras	k1gInSc6	teras
malého	malý	k2eAgInSc2d1	malý
balkonu	balkon	k1gInSc2	balkon
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
síť	síť	k1gFnSc1	síť
stanic	stanice	k1gFnPc2	stanice
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Varšava	Varšava	k1gFnSc1	Varšava
-	-	kIx~	-
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
220	[number]	k4	220
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
obsluhovalo	obsluhovat	k5eAaImAgNnS	obsluhovat
ji	on	k3xPp3gFnSc4	on
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
1320	[number]	k4	1320
operátorů	operátor	k1gInPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
Hvozdu	hvozd	k1gInSc6	hvozd
(	(	kIx(	(
<g/>
dominantní	dominantní	k2eAgFnSc1d1	dominantní
hora	hora	k1gFnSc1	hora
Hvozdského	Hvozdský	k2eAgInSc2d1	Hvozdský
hřbetu	hřbet	k1gInSc2	hřbet
Lužických	lužický	k2eAgFnPc2d1	Lužická
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
750	[number]	k4	750
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
bývala	bývat	k5eAaImAgFnS	bývat
stanice	stanice	k1gFnSc1	stanice
optického	optický	k2eAgNnSc2d1	optické
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
lotynkového	lotynkový	k2eAgInSc2d1	lotynkový
<g/>
"	"	kIx"	"
tyčového	tyčový	k2eAgInSc2d1	tyčový
telegrafu	telegraf	k1gInSc2	telegraf
pro	pro	k7c4	pro
signalizování	signalizování	k1gNnPc4	signalizování
vyšlých	vyšlý	k2eAgNnPc2d1	vyšlé
čísel	číslo	k1gNnPc2	číslo
v	v	k7c6	v
Lotynce	Lotynka	k1gFnSc6	Lotynka
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
vycházel	vycházet	k5eAaImAgInS	vycházet
ze	z	k7c2	z
Žitavy	Žitava	k1gFnSc2	Žitava
přes	přes	k7c4	přes
Toepfer	Toepfer	k1gInSc4	Toepfer
u	u	k7c2	u
Ojvína	Ojvín	k1gInSc2	Ojvín
(	(	kIx(	(
<g/>
Oybin	Oybin	k1gInSc1	Oybin
<g/>
)	)	kIx)	)
na	na	k7c4	na
Hvozd	hvozd	k1gInSc4	hvozd
<g/>
,	,	kIx,	,
Luž	Luž	k1gFnSc4	Luž
<g/>
,	,	kIx,	,
Jezevčí	jezevčí	k2eAgInSc4d1	jezevčí
vrch	vrch	k1gInSc4	vrch
a	a	k8xC	a
Bezděz	Bezděz	k1gInSc4	Bezděz
ku	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Anonymní	anonymní	k2eAgInSc1d1	anonymní
článek	článek	k1gInSc1	článek
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
skotském	skotský	k2eAgInSc6d1	skotský
časopise	časopis	k1gInSc6	časopis
popisoval	popisovat	k5eAaImAgMnS	popisovat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lze	lze	k6eAd1	lze
zasílat	zasílat	k5eAaImF	zasílat
informace	informace	k1gFnSc1	informace
svazkem	svazek	k1gInSc7	svazek
26	[number]	k4	26
drátů	drát	k1gInPc2	drát
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
písmeno	písmeno	k1gNnSc4	písmeno
jeden	jeden	k4xCgMnSc1	jeden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příjemce	příjemce	k1gMnSc1	příjemce
sledoval	sledovat	k5eAaImAgMnS	sledovat
bublinky	bublinka	k1gFnPc4	bublinka
<g/>
,	,	kIx,	,
vyvíjené	vyvíjený	k2eAgFnPc4d1	vyvíjená
v	v	k7c6	v
elektrolytu	elektrolyt	k1gInSc6	elektrolyt
nad	nad	k7c7	nad
drátem	drát	k1gInSc7	drát
pod	pod	k7c7	pod
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
se	se	k3xPyFc4	se
však	však	k9	však
neujal	ujmout	k5eNaPmAgMnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
prakticky	prakticky	k6eAd1	prakticky
využitelný	využitelný	k2eAgInSc1d1	využitelný
telegraf	telegraf	k1gInSc1	telegraf
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
elektromagnetickém	elektromagnetický	k2eAgInSc6d1	elektromagnetický
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
Carl	Carl	k1gMnSc1	Carl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gauss	gauss	k1gInSc1	gauss
a	a	k8xC	a
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Eduard	Eduard	k1gMnSc1	Eduard
Weber	Weber	k1gMnSc1	Weber
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
typ	typ	k1gInSc4	typ
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Wheatstone	Wheatston	k1gInSc5	Wheatston
a	a	k8xC	a
William	William	k1gInSc4	William
Fothergill	Fothergilla	k1gFnPc2	Fothergilla
Cooke	Cook	k1gInSc2	Cook
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
systém	systém	k1gInSc1	systém
využíval	využívat	k5eAaImAgInS	využívat
jako	jako	k9	jako
detektor	detektor	k1gInSc1	detektor
zmagnetizované	zmagnetizovaný	k2eAgFnSc2d1	zmagnetizovaná
jehly	jehla	k1gFnSc2	jehla
vychylované	vychylovaný	k2eAgFnSc2d1	vychylovaný
proudem	proud	k1gInSc7	proud
v	v	k7c6	v
blízkých	blízký	k2eAgInPc6d1	blízký
vodičích	vodič	k1gInPc6	vodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1844	[number]	k4	1844
odeslal	odeslat	k5eAaPmAgMnS	odeslat
americký	americký	k2eAgMnSc1d1	americký
malíř	malíř	k1gMnSc1	malíř
Samuel	Samuel	k1gMnSc1	Samuel
Morse	Morse	k1gMnSc1	Morse
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
zprávu	zpráva	k1gFnSc4	zpráva
"	"	kIx"	"
<g/>
Co	co	k3yQnSc4	co
stvořil	stvořit	k5eAaPmAgMnS	stvořit
Bůh	bůh	k1gMnSc1	bůh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nejkrásnějšího	krásný	k2eAgInSc2d3	nejkrásnější
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
z	z	k7c2	z
Washingtonu	Washington	k1gInSc2	Washington
do	do	k7c2	do
Baltimoru	Baltimore	k1gInSc2	Baltimore
(	(	kIx(	(
<g/>
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Završil	završit	k5eAaPmAgMnS	završit
tak	tak	k9	tak
12	[number]	k4	12
let	léto	k1gNnPc2	léto
svých	svůj	k3xOyFgMnPc2	svůj
pokusů	pokus	k1gInPc2	pokus
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tak	tak	k6eAd1	tak
Morseův	Morseův	k2eAgInSc4d1	Morseův
telegraf	telegraf	k1gInSc4	telegraf
-	-	kIx~	-
komunikační	komunikační	k2eAgInSc4d1	komunikační
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
dalších	další	k2eAgNnPc6d1	další
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Morseův	Morseův	k2eAgInSc1d1	Morseův
telegraf	telegraf	k1gInSc1	telegraf
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
informace	informace	k1gFnSc2	informace
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
stavy	stav	k1gInPc4	stav
vysílače	vysílač	k1gInSc2	vysílač
resp.	resp.	kA	resp.
zdroje	zdroj	k1gInSc2	zdroj
signálu	signál	k1gInSc2	signál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
svítí	svítit	k5eAaImIp3nS	svítit
/	/	kIx~	/
nesvítí	svítit	k5eNaImIp3nS	svítit
<g/>
,	,	kIx,	,
vysílá	vysílat	k5eAaImIp3nS	vysílat
/	/	kIx~	/
nevysílá	vysílat	k5eNaImIp3nS	vysílat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gFnPc4	on
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
vnímat	vnímat	k5eAaImF	vnímat
lidskými	lidský	k2eAgInPc7d1	lidský
smysly	smysl	k1gInPc7	smysl
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
sluch	sluch	k1gInSc4	sluch
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
sérii	série	k1gFnSc4	série
mezer	mezera	k1gFnPc2	mezera
<g/>
,	,	kIx,	,
teček	tečka	k1gFnPc2	tečka
a	a	k8xC	a
čárek	čárka	k1gFnPc2	čárka
a	a	k8xC	a
následně	následně	k6eAd1	následně
dekódovat	dekódovat	k5eAaBmF	dekódovat
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
písmen	písmeno	k1gNnPc2	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
číslic	číslice	k1gFnPc2	číslice
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zakódování	zakódování	k1gNnSc4	zakódování
a	a	k8xC	a
dekódování	dekódování	k1gNnSc4	dekódování
informace	informace	k1gFnSc2	informace
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
telegrafii	telegrafie	k1gFnSc6	telegrafie
používá	používat	k5eAaImIp3nS	používat
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgInSc1d1	uznávaný
protokol	protokol	k1gInSc1	protokol
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
telegrafní	telegrafní	k2eAgFnSc1d1	telegrafní
abeceda	abeceda	k1gFnSc1	abeceda
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
znaku	znak	k1gInSc3	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
série	série	k1gFnSc1	série
teček	tečka	k1gFnPc2	tečka
a	a	k8xC	a
čárek	čárka	k1gFnPc2	čárka
oddělených	oddělený	k2eAgFnPc2d1	oddělená
mezerami	mezera	k1gFnPc7	mezera
<g/>
,	,	kIx,	,
reprezentovaných	reprezentovaný	k2eAgInPc2d1	reprezentovaný
stavy	stav	k1gInPc7	stav
vysílá	vysílat	k5eAaImIp3nS	vysílat
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
vysílá	vysílat	k5eAaImIp3nS	vysílat
dlouze	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
nevysílá	vysílat	k5eNaImIp3nS	vysílat
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
svítí	svítit	k5eAaImIp3nS	svítit
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
svítí	svítit	k5eAaImIp3nS	svítit
dlouze	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
nesvítí	svítit	k5eNaImIp3nS	svítit
<g/>
...	...	k?	...
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezery	mezera	k1gFnPc1	mezera
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
jsou	být	k5eAaImIp3nP	být
reprezentovány	reprezentován	k2eAgInPc1d1	reprezentován
delšími	dlouhý	k2eAgFnPc7d2	delší
mezerami	mezera	k1gFnPc7	mezera
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
médiem	médium	k1gNnSc7	médium
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
zpráv	zpráva	k1gFnPc2	zpráva
elektrickým	elektrický	k2eAgInSc7d1	elektrický
telegrafem	telegraf	k1gInSc7	telegraf
byly	být	k5eAaImAgInP	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
<g/>
)	)	kIx)	)
elektrické	elektrický	k2eAgInPc1d1	elektrický
vodiče	vodič	k1gInPc1	vodič
(	(	kIx(	(
<g/>
kabely	kabel	k1gInPc1	kabel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
přenos	přenos	k1gInSc4	přenos
zpráv	zpráva	k1gFnPc2	zpráva
typicky	typicky	k6eAd1	typicky
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
poštovními	poštovní	k2eAgInPc7d1	poštovní
úřady	úřad	k1gInPc7	úřad
<g/>
,	,	kIx,	,
železničními	železniční	k2eAgFnPc7d1	železniční
stanicemi	stanice	k1gFnPc7	stanice
apod.	apod.	kA	apod.
Zpráva	zpráva	k1gFnSc1	zpráva
přenášená	přenášený	k2eAgFnSc1d1	přenášená
telegrafem	telegraf	k1gInSc7	telegraf
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
telegram	telegram	k1gInSc4	telegram
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgNnSc7d1	velké
omezením	omezení	k1gNnSc7	omezení
však	však	k9	však
byla	být	k5eAaImAgFnS	být
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
každého	každý	k3xTgNnSc2	každý
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
možno	možno	k6eAd1	možno
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
telegram	telegram	k1gInSc4	telegram
doručit	doručit	k5eAaPmF	doručit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nejprve	nejprve	k6eAd1	nejprve
nutno	nutno	k6eAd1	nutno
kabely	kabel	k1gInPc4	kabel
zavést	zavést	k5eAaPmF	zavést
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
doručování	doručování	k1gNnSc3	doručování
telegrafických	telegrafický	k2eAgFnPc2d1	telegrafická
zpráv	zpráva	k1gFnPc2	zpráva
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
i	i	k9	i
podmořské	podmořský	k2eAgInPc1d1	podmořský
kabely	kabel	k1gInPc1	kabel
spojující	spojující	k2eAgInPc1d1	spojující
např.	např.	kA	např.
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
Evropu	Evropa	k1gFnSc4	Evropa
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Evropu	Evropa	k1gFnSc4	Evropa
s	s	k7c7	s
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
takto	takto	k6eAd1	takto
doručenou	doručený	k2eAgFnSc4d1	doručená
zprávu	zpráva	k1gFnSc4	zpráva
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
pojem	pojem	k1gInSc1	pojem
kabelogram	kabelogram	k1gInSc1	kabelogram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
telegraf	telegraf	k1gInSc1	telegraf
do	do	k7c2	do
veřejného	veřejný	k2eAgInSc2d1	veřejný
provozu	provoz	k1gInSc2	provoz
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
už	už	k6eAd1	už
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
bylo	být	k5eAaImAgNnS	být
podáno	podat	k5eAaPmNgNnS	podat
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
telegramů	telegram	k1gInPc2	telegram
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
rozvoji	rozvoj	k1gInSc6	rozvoj
telegrafie	telegrafie	k1gFnSc2	telegrafie
pak	pak	k6eAd1	pak
nastal	nastat	k5eAaPmAgInS	nastat
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
radiového	radiový	k2eAgNnSc2d1	radiové
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Radiotelegrafie	radiotelegrafie	k1gFnSc1	radiotelegrafie
umožnila	umožnit	k5eAaPmAgFnS	umožnit
velký	velký	k2eAgInSc4d1	velký
boom	boom	k1gInSc4	boom
v	v	k7c6	v
poštovním	poštovní	k2eAgInSc6d1	poštovní
styku	styk	k1gInSc6	styk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odstranila	odstranit	k5eAaPmAgFnS	odstranit
kabely	kabel	k1gInPc4	kabel
jako	jako	k8xC	jako
extrémně	extrémně	k6eAd1	extrémně
limitující	limitující	k2eAgInSc1d1	limitující
faktor	faktor	k1gInSc1	faktor
rozvoje	rozvoj	k1gInSc2	rozvoj
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
prakticky	prakticky	k6eAd1	prakticky
okamžitě	okamžitě	k6eAd1	okamžitě
zřídit	zřídit	k5eAaPmF	zřídit
telegrafní	telegrafní	k2eAgNnSc4d1	telegrafní
pracoviště	pracoviště	k1gNnSc4	pracoviště
a	a	k8xC	a
přijmout	přijmout	k5eAaPmF	přijmout
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
vyslat	vyslat	k5eAaPmF	vyslat
<g/>
)	)	kIx)	)
zprávu	zpráva	k1gFnSc4	zpráva
v	v	k7c6	v
kterémkoliv	kterýkoliv	k3yIgNnSc6	kterýkoliv
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
vysílače	vysílač	k1gInSc2	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Obsluha	obsluha	k1gFnSc1	obsluha
radiotelegrafní	radiotelegrafní	k2eAgFnSc2d1	radiotelegrafní
stanice	stanice	k1gFnSc2	stanice
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
náročnější	náročný	k2eAgFnSc1d2	náročnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
obsluha	obsluha	k1gFnSc1	obsluha
stanice	stanice	k1gFnSc2	stanice
propojených	propojený	k2eAgFnPc2d1	propojená
kabely	kabela	k1gFnSc2	kabela
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
kabelogramy	kabelogram	k1gInPc1	kabelogram
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
zapisovaly	zapisovat	k5eAaImAgInP	zapisovat
na	na	k7c4	na
papírovou	papírový	k2eAgFnSc4d1	papírová
pásku	páska	k1gFnSc4	páska
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
číst	číst	k5eAaImF	číst
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
a	a	k8xC	a
originál	originál	k1gInSc1	originál
zprávy	zpráva	k1gFnSc2	zpráva
archivovat	archivovat	k5eAaBmF	archivovat
<g/>
,	,	kIx,	,
u	u	k7c2	u
radiotelegrafního	radiotelegrafní	k2eAgNnSc2d1	radiotelegrafní
spojení	spojení	k1gNnSc2	spojení
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
operátor	operátor	k1gMnSc1	operátor
přítomen	přítomen	k2eAgMnSc1d1	přítomen
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
schopen	schopen	k2eAgInSc1d1	schopen
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
zprávu	zpráva	k1gFnSc4	zpráva
vysílanou	vysílaný	k2eAgFnSc7d1	vysílaná
v	v	k7c6	v
Morseově	Morseův	k2eAgFnSc6d1	Morseova
abecedě	abeceda	k1gFnSc6	abeceda
dekódovat	dekódovat	k5eAaBmF	dekódovat
a	a	k8xC	a
zapisovat	zapisovat	k5eAaImF	zapisovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
bezdrátový	bezdrátový	k2eAgInSc1d1	bezdrátový
přenos	přenos	k1gInSc1	přenos
mluveného	mluvený	k2eAgNnSc2d1	mluvené
slova	slovo	k1gNnSc2	slovo
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
slovenský	slovenský	k2eAgMnSc1d1	slovenský
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Jozef	Jozef	k1gMnSc1	Jozef
Murgaš	Murgaš	k1gMnSc1	Murgaš
23.11	[number]	k4	23.11
<g/>
.1905	.1905	k4	.1905
<g/>
,	,	kIx,	,
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Wilkes-Barre	Wilkes-Barr	k1gInSc5	Wilkes-Barr
a	a	k8xC	a
Scranton	Scranton	k1gInSc4	Scranton
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pennsylvanii	Pennsylvanie	k1gFnSc6	Pennsylvanie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10.05	[number]	k4	10.05
<g/>
.1904	.1904	k4	.1904
mu	on	k3xPp3gNnSc3	on
federální	federální	k2eAgInSc4d1	federální
patentový	patentový	k2eAgInSc4d1	patentový
úřad	úřad	k1gInSc4	úřad
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
k	k	k7c3	k
vynálezu	vynález	k1gInSc3	vynález
přidelil	přidelit	k5eAaImAgMnS	přidelit
dva	dva	k4xCgInPc4	dva
patenty	patent	k1gInPc4	patent
<g/>
:	:	kIx,	:
Zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
telegrafii	telegrafie	k1gFnSc4	telegrafie
(	(	kIx(	(
<g/>
759	[number]	k4	759
852	[number]	k4	852
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Apparatus	Apparatus	k1gInSc1	Apparatus
for	forum	k1gNnPc2	forum
wireless	wireless	k6eAd1	wireless
telegraphy	telegrapha	k1gFnSc2	telegrapha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Způsob	způsob	k1gInSc1	způsob
přenášení	přenášení	k1gNnSc2	přenášení
zpráv	zpráva	k1gFnPc2	zpráva
bezdrátovou	bezdrátový	k2eAgFnSc7d1	bezdrátová
telegrafií	telegrafie	k1gFnSc7	telegrafie
(	(	kIx(	(
<g/>
876	[number]	k4	876
383	[number]	k4	383
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
way	way	k?	way
of	of	k?	of
transmitted	transmitted	k1gMnSc1	transmitted
messages	messages	k1gMnSc1	messages
by	by	kYmCp3nP	by
wireless	wireless	k6eAd1	wireless
telegraphy	telegrapha	k1gFnPc1	telegrapha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Rozvoj	rozvoj	k1gInSc1	rozvoj
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
umožňujících	umožňující	k2eAgInPc2d1	umožňující
dopravování	dopravování	k1gNnSc4	dopravování
zpráv	zpráva	k1gFnPc2	zpráva
však	však	k9	však
postupně	postupně	k6eAd1	postupně
vytlačil	vytlačit	k5eAaPmAgInS	vytlačit
i	i	k9	i
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
telegrafii	telegrafie	k1gFnSc4	telegrafie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
částečně	částečně	k6eAd1	částečně
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
radiofonickými	radiofonický	k2eAgFnPc7d1	radiofonická
a	a	k8xC	a
telefonickými	telefonický	k2eAgFnPc7d1	telefonická
zprávami	zpráva	k1gFnPc7	zpráva
umožňujícími	umožňující	k2eAgFnPc7d1	umožňující
de-facto	deact	k2eAgNnSc1d1	de-facto
(	(	kIx(	(
<g/>
vyjádřeno	vyjádřen	k2eAgNnSc1d1	vyjádřeno
moderní	moderní	k2eAgFnSc7d1	moderní
terminologií	terminologie	k1gFnSc7	terminologie
<g/>
)	)	kIx)	)
vyšší	vysoký	k2eAgFnSc4d2	vyšší
přenosovou	přenosový	k2eAgFnSc4d1	přenosová
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
také	také	k9	také
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
telegrafie	telegrafie	k1gFnSc1	telegrafie
začala	začít	k5eAaPmAgFnS	začít
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
ubírat	ubírat	k5eAaImF	ubírat
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
vícestavovými	vícestavový	k2eAgInPc7d1	vícestavový
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tónová	tónový	k2eAgFnSc1d1	tónová
telegrafie	telegrafie	k1gFnSc1	telegrafie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
dopravovat	dopravovat	k5eAaImF	dopravovat
zprávy	zpráva	k1gFnPc4	zpráva
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
vedení	vedení	k1gNnSc6	vedení
i	i	k9	i
více	hodně	k6eAd2	hodně
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
definitivní	definitivní	k2eAgNnSc4d1	definitivní
upuštění	upuštění	k1gNnSc4	upuštění
od	od	k7c2	od
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
telegrafie	telegrafie	k1gFnSc2	telegrafie
v	v	k7c6	v
komerční	komerční	k2eAgFnSc6d1	komerční
komunikaci	komunikace	k1gFnSc6	komunikace
nastal	nastat	k5eAaPmAgInS	nastat
rozvojem	rozvoj	k1gInSc7	rozvoj
datových	datový	k2eAgInPc2d1	datový
(	(	kIx(	(
<g/>
digitálních	digitální	k2eAgInPc2d1	digitální
<g/>
)	)	kIx)	)
druhů	druh	k1gInPc2	druh
provozu	provoz	k1gInSc2	provoz
jako	jako	k8xC	jako
např.	např.	kA	např.
telexu	telex	k1gInSc2	telex
(	(	kIx(	(
<g/>
dálnopisu	dálnopis	k1gInSc2	dálnopis
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
telefaxu	telefax	k1gInSc2	telefax
(	(	kIx(	(
<g/>
faxu	fax	k1gInSc2	fax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
v	v	k7c6	v
telekomunikační	telekomunikační	k2eAgFnSc6d1	telekomunikační
technice	technika	k1gFnSc6	technika
pak	pak	k6eAd1	pak
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
přes	přes	k7c4	přes
telefonní	telefonní	k2eAgInSc4d1	telefonní
modem	modem	k1gInSc4	modem
<g/>
,	,	kIx,	,
satelitní	satelitní	k2eAgInPc4d1	satelitní
transpondéry	transpondér	k1gInPc4	transpondér
<g/>
,	,	kIx,	,
internet	internet	k1gInSc1	internet
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
služby	služba	k1gFnPc1	služba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celulární	celulární	k2eAgMnPc1d1	celulární
(	(	kIx(	(
<g/>
buňkové	buňkový	k2eAgFnPc4d1	buňková
<g/>
)	)	kIx)	)
telefonní	telefonní	k2eAgFnPc4d1	telefonní
sítě	síť	k1gFnPc4	síť
až	až	k6eAd1	až
k	k	k7c3	k
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
pojmu	pojem	k1gInSc3	pojem
informační	informační	k2eAgFnSc2d1	informační
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
,	,	kIx,	,
slučujícího	slučující	k2eAgNnSc2d1	slučující
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
kombinaci	kombinace	k1gFnSc4	kombinace
všech	všecek	k3xTgInPc2	všecek
progresivních	progresivní	k2eAgInPc2d1	progresivní
druhů	druh	k1gInPc2	druh
datové	datový	k2eAgFnSc2d1	datová
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Telegrafie	telegrafie	k1gFnSc1	telegrafie
však	však	k9	však
z	z	k7c2	z
profesionální	profesionální	k2eAgFnSc2d1	profesionální
praxe	praxe	k1gFnSc2	praxe
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
výhodám	výhoda	k1gFnPc3	výhoda
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
dosud	dosud	k6eAd1	dosud
definitivně	definitivně	k6eAd1	definitivně
nezmizela	zmizet	k5eNaPmAgFnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Telegrafními	telegrafní	k2eAgFnPc7d1	telegrafní
značkami	značka	k1gFnPc7	značka
vysílají	vysílat	k5eAaImIp3nP	vysílat
a	a	k8xC	a
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
se	se	k3xPyFc4	se
radiomajáky	radiomaják	k1gInPc1	radiomaják
<g/>
,	,	kIx,	,
převáděče	převáděč	k1gInPc1	převáděč
apod.	apod.	kA	apod.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
použití	použití	k1gNnSc1	použití
telegrafie	telegrafie	k1gFnSc2	telegrafie
je	být	k5eAaImIp3nS	být
především	především	k9	především
v	v	k7c6	v
radioamatérském	radioamatérský	k2eAgInSc6d1	radioamatérský
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Komunikační	komunikační	k2eAgInSc1d1	komunikační
mód	mód	k1gInSc1	mód
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
principiálně	principiálně	k6eAd1	principiálně
zcela	zcela	k6eAd1	zcela
totožný	totožný	k2eAgInSc4d1	totožný
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
používali	používat	k5eAaImAgMnP	používat
průkopníci	průkopník	k1gMnPc1	průkopník
radiotelegrafie	radiotelegrafie	k1gFnSc2	radiotelegrafie
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
tzv.	tzv.	kA	tzv.
jiskrové	jiskrový	k2eAgFnSc2d1	Jiskrová
telegrafie	telegrafie	k1gFnSc2	telegrafie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Continuous	Continuous	k1gInSc1	Continuous
wave	wav	k1gFnSc2	wav
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
zkratkou	zkratka	k1gFnSc7	zkratka
"	"	kIx"	"
<g/>
CW	CW	kA	CW
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
nadšení	nadšený	k2eAgMnPc1d1	nadšený
zastánci	zastánce	k1gMnPc1	zastánce
a	a	k8xC	a
obdivovatelé	obdivovatel	k1gMnPc1	obdivovatel
CW	CW	kA	CW
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
radioamatérů	radioamatér	k1gMnPc2	radioamatér
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
soutěžích	soutěž	k1gFnPc6	soutěž
ve	v	k7c6	v
vysokorychlostní	vysokorychlostní	k2eAgFnSc6d1	vysokorychlostní
telegrafii	telegrafie	k1gFnSc6	telegrafie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pořádána	pořádat	k5eAaImNgFnS	pořádat
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
CW	CW	kA	CW
telegrafie	telegrafie	k1gFnSc2	telegrafie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc4	její
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
<g/>
.	.	kIx.	.
</s>
<s>
Telegrafii	telegrafie	k1gFnSc4	telegrafie
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
způsobů	způsob	k1gInPc2	způsob
přenosu	přenos	k1gInSc2	přenos
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
:	:	kIx,	:
Telegrafie	telegrafie	k1gFnSc1	telegrafie
přenášená	přenášený	k2eAgFnSc1d1	přenášená
po	po	k7c6	po
radiových	radiový	k2eAgFnPc6d1	radiová
vlnách	vlna	k1gFnPc6	vlna
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pouze	pouze	k6eAd1	pouze
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
vysílač	vysílač	k1gInSc1	vysílač
<g/>
:	:	kIx,	:
Informace	informace	k1gFnPc1	informace
buď	buď	k8xC	buď
není	být	k5eNaImIp3nS	být
modulována	modulován	k2eAgFnSc1d1	modulována
<g/>
,	,	kIx,	,
a	a	k8xC	a
zapíná	zapínat	k5eAaImIp3nS	zapínat
se	se	k3xPyFc4	se
a	a	k8xC	a
vypíná	vypínat	k5eAaImIp3nS	vypínat
sama	sám	k3xTgFnSc1	sám
nosná	nosný	k2eAgFnSc1d1	nosná
vlna	vlna	k1gFnSc1	vlna
(	(	kIx(	(
<g/>
provoz	provoz	k1gInSc1	provoz
A	A	kA	A
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
stačí	stačit	k5eAaBmIp3nS	stačit
prosté	prostý	k2eAgNnSc1d1	prosté
namodulování	namodulování	k1gNnSc1	namodulování
jednoho	jeden	k4xCgInSc2	jeden
tónu	tón	k1gInSc2	tón
na	na	k7c4	na
nosný	nosný	k2eAgInSc4d1	nosný
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
či	či	k8xC	či
střídání	střídání	k1gNnSc2	střídání
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
vysílané	vysílaný	k2eAgInPc1d1	vysílaný
telegraficky	telegraficky	k6eAd1	telegraficky
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
posloupnosti	posloupnost	k1gFnPc4	posloupnost
teček	tečka	k1gFnPc2	tečka
a	a	k8xC	a
čárek	čárka	k1gFnPc2	čárka
Morseovy	Morseův	k2eAgFnSc2d1	Morseova
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
radiových	radiový	k2eAgFnPc6d1	radiová
vlnách	vlna	k1gFnPc6	vlna
dostatečně	dostatečně	k6eAd1	dostatečně
dobře	dobře	k6eAd1	dobře
rozeznatelné	rozeznatelný	k2eAgNnSc1d1	rozeznatelné
(	(	kIx(	(
<g/>
slyšitelné	slyšitelný	k2eAgNnSc1d1	slyšitelné
<g/>
)	)	kIx)	)
i	i	k9	i
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
úrovni	úroveň	k1gFnSc6	úroveň
rušení	rušení	k1gNnSc2	rušení
<g/>
.	.	kIx.	.
</s>
<s>
Telegrafní	telegrafní	k2eAgNnSc1d1	telegrafní
vysílání	vysílání	k1gNnSc1	vysílání
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
také	také	k9	také
malou	malý	k2eAgFnSc4d1	malá
šířku	šířka	k1gFnSc4	šířka
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
různé	různý	k2eAgFnPc1d1	různá
stanice	stanice	k1gFnPc1	stanice
mohou	moct	k5eAaImIp3nP	moct
vysílat	vysílat	k5eAaImF	vysílat
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
blízkých	blízký	k2eAgInPc6d1	blízký
kmitočtech	kmitočet	k1gInPc6	kmitočet
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
rušily	rušit	k5eAaImAgFnP	rušit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vysílání	vysílání	k1gNnSc1	vysílání
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
provozu	provoz	k1gInSc2	provoz
nemyslitelné	myslitelný	k2eNgNnSc1d1	nemyslitelné
<g/>
.	.	kIx.	.
</s>
<s>
Obecnými	obecný	k2eAgFnPc7d1	obecná
nevýhodami	nevýhoda	k1gFnPc7	nevýhoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
fonickými	fonický	k2eAgFnPc7d1	fonická
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
digitálními	digitální	k2eAgInPc7d1	digitální
druhy	druh	k1gInPc7	druh
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
nízká	nízký	k2eAgFnSc1d1	nízká
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
začátečník	začátečník	k1gMnSc1	začátečník
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyslat	vyslat	k5eAaPmF	vyslat
či	či	k8xC	či
přijmout	přijmout	k5eAaPmF	přijmout
cca	cca	kA	cca
40	[number]	k4	40
znaků	znak	k1gInPc2	znak
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
zkušený	zkušený	k2eAgMnSc1d1	zkušený
uživatel	uživatel	k1gMnSc1	uživatel
cca	cca	kA	cca
100	[number]	k4	100
-	-	kIx~	-
200	[number]	k4	200
znaků	znak	k1gInPc2	znak
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
nutnost	nutnost	k1gFnSc4	nutnost
speciální	speciální	k2eAgFnSc2d1	speciální
dovednosti	dovednost	k1gFnSc2	dovednost
(	(	kIx(	(
<g/>
příjmu	příjem	k1gInSc2	příjem
i	i	k8xC	i
vysílání	vysílání	k1gNnSc2	vysílání
Morseovy	Morseův	k2eAgFnSc2d1	Morseova
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
v	v	k7c6	v
radioamatérském	radioamatérský	k2eAgInSc6d1	radioamatérský
provozu	provoz	k1gInSc6	provoz
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nezastupitelná	zastupitelný	k2eNgFnSc1d1	nezastupitelná
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
použitelnost	použitelnost	k1gFnSc4	použitelnost
i	i	k9	i
při	při	k7c6	při
extrémně	extrémně	k6eAd1	extrémně
slabých	slabý	k2eAgInPc6d1	slabý
signálech	signál	k1gInPc6	signál
<g/>
,	,	kIx,	,
ztrácejících	ztrácející	k2eAgMnPc2d1	ztrácející
se	se	k3xPyFc4	se
v	v	k7c6	v
šumu	šum	k1gInSc6	šum
a	a	k8xC	a
rušení	rušení	k1gNnSc6	rušení
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
radioamatérů	radioamatér	k1gMnPc2	radioamatér
má	mít	k5eAaImIp3nS	mít
takové	takový	k3xDgNnSc4	takový
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gInSc4	jejich
nejoblíbenější	oblíbený	k2eAgInSc4d3	nejoblíbenější
druh	druh	k1gInSc4	druh
komunikace	komunikace	k1gFnSc2	komunikace
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Radioamatérský	radioamatérský	k2eAgInSc1d1	radioamatérský
provoz	provoz	k1gInSc1	provoz
měl	mít	k5eAaImAgInS	mít
a	a	k8xC	a
stále	stále	k6eAd1	stále
má	mít	k5eAaImIp3nS	mít
nezastupitelný	zastupitelný	k2eNgInSc4d1	nezastupitelný
význam	význam	k1gInSc4	význam
i	i	k9	i
ze	z	k7c2	z
společenských	společenský	k2eAgInPc2d1	společenský
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
Nízkou	nízký	k2eAgFnSc4d1	nízká
přenosovou	přenosový	k2eAgFnSc4d1	přenosová
rychlost	rychlost	k1gFnSc4	rychlost
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
lidský	lidský	k2eAgInSc1d1	lidský
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
automatizované	automatizovaný	k2eAgFnPc1d1	automatizovaná
radiostanice	radiostanice	k1gFnPc1	radiostanice
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
realizovat	realizovat	k5eAaBmF	realizovat
přenosy	přenos	k1gInPc4	přenos
i	i	k9	i
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
běžné	běžný	k2eAgInPc1d1	běžný
informační	informační	k2eAgInPc1d1	informační
kanály	kanál	k1gInPc1	kanál
buď	buď	k8xC	buď
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
vůbec	vůbec	k9	vůbec
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
<g/>
/	/	kIx~	/
<g/>
byly	být	k5eAaImAgInP	být
cenzurované	cenzurovaný	k2eAgInPc1d1	cenzurovaný
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
individuálně	individuálně	k6eAd1	individuálně
se	se	k3xPyFc4	se
i	i	k9	i
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
dnešních	dnešní	k2eAgInPc2d1	dnešní
masivních	masivní	k2eAgInPc2d1	masivní
komunikačních	komunikační	k2eAgInPc2d1	komunikační
kanálů	kanál	k1gInPc2	kanál
dařily	dařit	k5eAaImAgInP	dařit
datové	datový	k2eAgInPc1d1	datový
přenosy	přenos	k1gInPc1	přenos
i	i	k9	i
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc4	první
stahování	stahování	k1gNnPc2	stahování
obrázků	obrázek	k1gInPc2	obrázek
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
totality	totalita	k1gFnSc2	totalita
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
služba	služba	k1gFnSc1	služba
telegrafie	telegrafie	k1gFnSc1	telegrafie
poskytována	poskytován	k2eAgFnSc1d1	poskytována
k	k	k7c3	k
7	[number]	k4	7
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
přestala	přestat	k5eAaPmAgFnS	přestat
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
poskytovat	poskytovat	k5eAaImF	poskytovat
službu	služba	k1gFnSc4	služba
telegrafie	telegrafie	k1gFnSc2	telegrafie
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
Telefónica	Telefónica	k1gFnSc1	Telefónica
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
služba	služba	k1gFnSc1	služba
telegrafie	telegrafie	k1gFnSc1	telegrafie
poskytována	poskytován	k2eAgFnSc1d1	poskytována
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
služba	služba	k1gFnSc1	služba
telegrafie	telegrafie	k1gFnSc1	telegrafie
poskytována	poskytován	k2eAgFnSc1d1	poskytována
k	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
služba	služba	k1gFnSc1	služba
telegrafie	telegrafie	k1gFnSc1	telegrafie
poskytována	poskytován	k2eAgFnSc1d1	poskytována
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
služba	služba	k1gFnSc1	služba
telegrafie	telegrafie	k1gFnSc1	telegrafie
poskytována	poskytován	k2eAgFnSc1d1	poskytována
k	k	k7c3	k
9	[number]	k4	9
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
