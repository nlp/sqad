<s>
První	první	k4xOgInSc4	první
optický	optický	k2eAgInSc4d1	optický
telegraf	telegraf	k1gInSc4	telegraf
představil	představit	k5eAaPmAgMnS	představit
Robert	Robert	k1gMnSc1	Robert
Hook	Hook	k1gMnSc1	Hook
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
přednášek	přednáška	k1gFnPc2	přednáška
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1684	[number]	k4	1684
<g/>
.	.	kIx.	.
</s>
