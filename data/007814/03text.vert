<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
(	(	kIx(	(
<g/>
Campanula	Campanula	k1gFnSc1	Campanula
gelida	gelida	k1gFnSc1	gelida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stenoendemická	stenoendemický	k2eAgFnSc1d1	stenoendemický
<g/>
,	,	kIx,	,
kriticky	kriticky	k6eAd1	kriticky
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
zvonkovitých	zvonkovitý	k2eAgMnPc2d1	zvonkovitý
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
diferenciací	diferenciace	k1gFnSc7	diferenciace
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
populace	populace	k1gFnSc2	populace
zvonku	zvonek	k1gInSc2	zvonek
Scheuchzerova	Scheuchzerův	k2eAgInSc2d1	Scheuchzerův
(	(	kIx(	(
<g/>
Campanula	Campanula	k1gFnSc1	Campanula
scheuchzeri	scheuchzer	k1gFnSc2	scheuchzer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
alpského	alpský	k2eAgInSc2d1	alpský
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Sudet	Sudety	k1gFnPc2	Sudety
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
některého	některý	k3yIgMnSc4	některý
z	z	k7c2	z
chladnějších	chladný	k2eAgNnPc2d2	chladnější
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k1gMnSc1	příbuzný
s	s	k7c7	s
krkonošským	krkonošský	k2eAgInSc7d1	krkonošský
endemitem	endemit	k1gInSc7	endemit
zvonkem	zvonek	k1gInSc7	zvonek
českým	český	k2eAgInSc7d1	český
(	(	kIx(	(
<g/>
Campanula	Campanula	k1gFnPc6	Campanula
bohemica	bohemic	k1gInSc2	bohemic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgInSc1d1	označován
za	za	k7c4	za
jeho	on	k3xPp3gInSc4	on
poddruh	poddruh	k1gInSc1	poddruh
a	a	k8xC	a
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
zvonek	zvonek	k1gInSc1	zvonek
český	český	k2eAgInSc1d1	český
jesenický	jesenický	k2eAgInSc1d1	jesenický
(	(	kIx(	(
<g/>
Campanula	Campanula	k1gMnSc1	Campanula
bohemica	bohemica	k1gMnSc1	bohemica
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
gelida	gelida	k1gFnSc1	gelida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
příbuzenského	příbuzenský	k2eAgInSc2d1	příbuzenský
komplexu	komplex	k1gInSc2	komplex
Campanula	Campanula	k1gMnSc2	Campanula
rotundifolia	rotundifolius	k1gMnSc2	rotundifolius
agg	agg	k?	agg
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
známým	známý	k2eAgNnSc7d1	známé
místem	místo	k1gNnSc7	místo
výskytu	výskyt	k1gInSc2	výskyt
zvonku	zvonek	k1gInSc2	zvonek
jesenického	jesenický	k2eAgInSc2d1	jesenický
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
Petrovy	Petrův	k2eAgInPc1d1	Petrův
kameny	kámen	k1gInPc1	kámen
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Praděd	praděd	k1gMnSc1	praděd
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
a	a	k8xC	a
východním	východní	k2eAgInSc6d1	východní
svahu	svah	k1gInSc6	svah
vrcholové	vrcholový	k2eAgFnSc2d1	vrcholová
skály	skála	k1gFnSc2	skála
Petrových	Petrových	k2eAgInPc2d1	Petrových
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
v	v	k7c6	v
kostřavovém	kostřavový	k2eAgInSc6d1	kostřavový
travním	travní	k2eAgInSc6d1	travní
porostu	porost	k1gInSc6	porost
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
úpatí	úpatí	k1gNnSc2	úpatí
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
438	[number]	k4	438
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
musí	muset	k5eAaImIp3nP	muset
odolávat	odolávat	k5eAaImF	odolávat
nízkým	nízký	k2eAgFnPc3d1	nízká
teplotám	teplota	k1gFnPc3	teplota
<g/>
,	,	kIx,	,
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
není	být	k5eNaImIp3nS	být
přístupné	přístupný	k2eAgFnPc4d1	přístupná
veřejnosti	veřejnost	k1gFnPc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
svému	svůj	k3xOyFgInSc3	svůj
příbuznému	příbuzný	k2eAgInSc3d1	příbuzný
zvonku	zvonek	k1gInSc3	zvonek
českému	český	k2eAgInSc3d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Botanik	botanik	k1gMnSc1	botanik
Miloslav	Miloslav	k1gMnSc1	Miloslav
Kovanda	Kovanda	k1gMnSc1	Kovanda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
popsal	popsat	k5eAaPmAgInS	popsat
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
dokonce	dokonce	k9	dokonce
později	pozdě	k6eAd2	pozdě
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
zvonku	zvonek	k1gInSc2	zvonek
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ho	on	k3xPp3gNnSc4	on
většina	většina	k1gFnSc1	většina
botaniků	botanik	k1gMnPc2	botanik
opět	opět	k6eAd1	opět
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
se	se	k3xPyFc4	se
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
nejbližšího	blízký	k2eAgMnSc2d3	nejbližší
příbuzného	příbuzný	k1gMnSc2	příbuzný
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
bohatými	bohatý	k2eAgInPc7d1	bohatý
trsy	trs	k1gInPc7	trs
a	a	k8xC	a
menšími	malý	k2eAgInPc7d2	menší
květy	květ	k1gInPc7	květ
a	a	k8xC	a
tobolkami	tobolka	k1gFnPc7	tobolka
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
často	často	k6eAd1	často
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
tvoří	tvořit	k5eAaImIp3nP	tvořit
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
nejvýše	nejvýše	k6eAd1	nejvýše
čtyřkvěté	čtyřkvětý	k2eAgInPc4d1	čtyřkvětý
hrozny	hrozen	k1gInPc4	hrozen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
menší	malý	k2eAgFnPc1d2	menší
lodyhy	lodyha	k1gFnPc1	lodyha
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
délku	délka	k1gFnSc4	délka
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Přízemní	přízemní	k2eAgInPc1d1	přízemní
listy	list	k1gInPc1	list
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
rostlina	rostlina	k1gFnSc1	rostlina
neodkvete	odkvést	k5eNaPmIp3nS	odkvést
<g/>
.	.	kIx.	.
</s>
<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
trsech	trs	k1gInPc6	trs
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
škvírách	škvíra	k1gFnPc6	škvíra
skalnatého	skalnatý	k2eAgMnSc2d1	skalnatý
<g/>
,	,	kIx,	,
kyselého	kyselý	k2eAgNnSc2d1	kyselé
podloží	podloží	k1gNnSc2	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
plochám	plocha	k1gFnPc3	plocha
přímo	přímo	k6eAd1	přímo
osvíceným	osvícený	k2eAgNnSc7d1	osvícené
sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některým	některý	k3yIgFnPc3	některý
rostlinám	rostlina	k1gFnPc3	rostlina
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
i	i	k9	i
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
životu	život	k1gInSc3	život
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
hodně	hodně	k6eAd1	hodně
vláhy	vláha	k1gFnSc2	vláha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
té	ten	k3xDgFnSc3	ten
však	však	k9	však
na	na	k7c6	na
Petrových	Petrových	k2eAgInPc6d1	Petrových
kamenech	kámen	k1gInPc6	kámen
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odkázán	odkázat	k5eAaPmNgInS	odkázat
na	na	k7c4	na
deště	dešť	k1gInPc4	dešť
či	či	k8xC	či
vysráženou	vysrážený	k2eAgFnSc4d1	vysrážená
mlhu	mlha	k1gFnSc4	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
vegetativně	vegetativně	k6eAd1	vegetativně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
možnosti	možnost	k1gFnPc4	možnost
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
jsou	být	k5eAaImIp3nP	být
omezovány	omezovat	k5eAaImNgFnP	omezovat
nepříznivými	příznivý	k2eNgFnPc7d1	nepříznivá
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
podmínkami	podmínka	k1gFnPc7	podmínka
jeho	jeho	k3xOp3gNnSc2	jeho
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
konce	konec	k1gInSc2	konec
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
zvonků	zvonek	k1gInPc2	zvonek
se	se	k3xPyFc4	se
v	v	k7c6	v
květech	květ	k1gInPc6	květ
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
blizna	blizna	k1gFnSc1	blizna
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
pyl	pyl	k1gInSc1	pyl
z	z	k7c2	z
prašníků	prašník	k1gInPc2	prašník
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
brání	bránit	k5eAaImIp3nS	bránit
samoopylení	samoopylení	k1gNnSc4	samoopylení
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
kriticky	kriticky	k6eAd1	kriticky
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
a	a	k8xC	a
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
zdá	zdát	k5eAaImIp3nS	zdát
stabilizovaná	stabilizovaný	k2eAgFnSc1d1	stabilizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
jej	on	k3xPp3gNnSc4	on
zejména	zejména	k9	zejména
lyžaři	lyžař	k1gMnPc1	lyžař
projíždějící	projíždějící	k2eAgFnSc2d1	projíždějící
zakázaným	zakázaný	k2eAgNnSc7d1	zakázané
územím	území	k1gNnSc7	území
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
jako	jako	k8xS	jako
metlice	metlice	k1gFnSc1	metlice
trsnatá	trsnatý	k2eAgFnSc1d1	trsnatá
<g/>
,	,	kIx,	,
maliník	maliník	k1gInSc1	maliník
obecný	obecný	k2eAgInSc1d1	obecný
a	a	k8xC	a
třezalka	třezalka	k1gFnSc1	třezalka
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
.	.	kIx.	.
</s>
<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
zvláště	zvláště	k6eAd1	zvláště
chráněných	chráněný	k2eAgFnPc2d1	chráněná
rostlin	rostlina	k1gFnPc2	rostlina
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
uvedenému	uvedený	k2eAgInSc3d1	uvedený
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
provádějí	provádět	k5eAaImIp3nP	provádět
některá	některý	k3yIgNnPc1	některý
ustanovení	ustanovení	k1gNnPc1	ustanovení
zákona	zákon	k1gInSc2	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
jej	on	k3xPp3gMnSc4	on
trhat	trhat	k5eAaImF	trhat
<g/>
,	,	kIx,	,
vykopávat	vykopávat	k5eAaImF	vykopávat
<g/>
,	,	kIx,	,
poškozovat	poškozovat	k5eAaImF	poškozovat
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
rušit	rušit	k5eAaImF	rušit
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c4	na
Červený	červený	k2eAgInSc4d1	červený
seznam	seznam	k1gInSc4	seznam
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
rostlin	rostlina	k1gFnPc2	rostlina
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
C1	C1	k1gFnSc2	C1
(	(	kIx(	(
<g/>
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
Červený	červený	k2eAgInSc4d1	červený
seznam	seznam	k1gInSc4	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
IUCN	IUCN	kA	IUCN
<g/>
,	,	kIx,	,
kategorie	kategorie	k1gFnSc2	kategorie
E	E	kA	E
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
bernské	bernský	k2eAgFnSc2d1	Bernská
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
evropských	evropský	k2eAgFnPc2d1	Evropská
planě	planě	k6eAd1	planě
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
přírodních	přírodní	k2eAgNnPc2d1	přírodní
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
jeho	on	k3xPp3gInSc2	on
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
soustavy	soustava	k1gFnSc2	soustava
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Správy	správa	k1gFnSc2	správa
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Jeseníky	Jeseník	k1gInPc7	Jeseník
také	také	k9	také
zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgMnSc1d1	jesenický
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Vsi	ves	k1gFnSc6	ves
u	u	k7c2	u
Rýmařova	rýmařův	k2eAgNnSc2d1	rýmařův
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgMnSc1d1	jesenický
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc4	taxon
Campanula	Campanula	k1gFnSc4	Campanula
gelida	gelid	k1gMnSc2	gelid
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
