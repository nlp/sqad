<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
(	(	kIx(	(
<g/>
Campanula	Campanula	k1gFnSc1	Campanula
gelida	gelida	k1gFnSc1	gelida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stenoendemická	stenoendemický	k2eAgFnSc1d1	stenoendemický
<g/>
,	,	kIx,	,
kriticky	kriticky	k6eAd1	kriticky
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
zvonkovitých	zvonkovitý	k2eAgMnPc2d1	zvonkovitý
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
