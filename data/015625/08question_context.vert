<s>
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
–	–	k?
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
NKC	NKC	kA
-	-	kIx~
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
NKC	NKC	kA
<g/>
)	)	kIx)
vzniklo	vzniknout	k5eAaPmAgNnS
roku	rok	k1gInSc2
2001	#num#	k4
jako	jako	k8xC,k8xS
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
-	-	kIx~
ženy	žena	k1gFnPc1
a	a	k8xC
věda	věda	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
oddělení	oddělení	k1gNnSc2
Gender	Gender	k1gInSc1
a	a	k8xC
sociologie	sociologie	k1gFnSc1
Sociologického	sociologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>