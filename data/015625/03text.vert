<s>
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
–	–	k?
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
–	–	k?
ženy	žena	k1gFnSc2
a	a	k8xC
věda	věda	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
2001	#num#	k4
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Sociologický	sociologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
genderaveda	genderaveda	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
–	–	k?
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
NKC	NKC	kA
-	-	kIx~
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
NKC	NKC	kA
<g/>
)	)	kIx)
vzniklo	vzniknout	k5eAaPmAgNnS
roku	rok	k1gInSc2
2001	#num#	k4
jako	jako	k8xC,k8xS
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
-	-	kIx~
ženy	žena	k1gFnPc1
a	a	k8xC
věda	věda	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
oddělení	oddělení	k1gNnSc2
Gender	Gender	k1gInSc1
a	a	k8xC
sociologie	sociologie	k1gFnSc1
Sociologického	sociologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
se	se	k3xPyFc4
NKC	NKC	kA
stalo	stát	k5eAaPmAgNnS
samostatným	samostatný	k2eAgNnSc7d1
výzkumným	výzkumný	k2eAgNnSc7d1
oddělením	oddělení	k1gNnSc7
Sociologického	sociologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jediným	jediný	k2eAgNnSc7d1
specializovaným	specializovaný	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
zaměřeným	zaměřený	k2eAgNnSc7d1
na	na	k7c4
výzkum	výzkum	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
genderové	genderové	k2eAgFnSc1d1
sociologie	sociologie	k1gFnSc1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
genderových	genderův	k2eAgNnPc2d1
studií	studio	k1gNnPc2
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
genderové	genderový	k2eAgFnSc2d1
rovnosti	rovnost	k1gFnSc2
a	a	k8xC
genderového	genderový	k2eAgInSc2d1
mainstreamingu	mainstreaming	k1gInSc2
ve	v	k7c6
vědě	věda	k1gFnSc6
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabezpečuje	zabezpečovat	k5eAaImIp3nS
expertízy	expertíza	k1gFnPc4
<g/>
,	,	kIx,
podkladové	podkladový	k2eAgInPc4d1
materiály	materiál	k1gInPc4
<g/>
,	,	kIx,
analýzy	analýza	k1gFnPc4
a	a	k8xC
konzultační	konzultační	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
pro	pro	k7c4
orgány	orgán	k1gInPc4
státní	státní	k2eAgFnSc2d1
a	a	k8xC
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
a	a	k8xC
výzkumné	výzkumný	k2eAgFnSc2d1
a	a	k8xC
vysokoškolské	vysokoškolský	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
genderové	genderový	k2eAgFnSc2d1
rovnosti	rovnost	k1gFnSc2
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
(	(	kIx(
<g/>
Úřad	úřad	k1gInSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
výzkumné	výzkumný	k2eAgFnPc1d1
a	a	k8xC
vysokoškolské	vysokoškolský	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
genderové	genderový	k2eAgFnSc2d1
rovnosti	rovnost	k1gFnSc2
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
je	být	k5eAaImIp3nS
činnost	činnost	k1gFnSc1
NKC	NKC	kA
uváděna	uvádět	k5eAaImNgFnS
jako	jako	k8xS,k8xC
jediná	jediný	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
v	v	k7c6
ČR	ČR	kA
ve	v	k7c6
zprávách	zpráva	k1gFnPc6
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Čermáková	Čermáková	k1gFnSc1
a	a	k8xC
Havelková	Havelková	k1gFnSc1
2001	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
European	European	k1gInSc1
Commission	Commission	k1gInSc1
2002	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
a	a	k8xC
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
b	b	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Lipinsky	Lipinsky	k1gFnSc1
2014	#num#	k4
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
bylo	být	k5eAaImAgNnS
NKC	NKC	kA
zařazeno	zařadit	k5eAaPmNgNnS
mezi	mezi	k7c4
tzv.	tzv.	kA
golden	goldna	k1gFnPc2
practices	practices	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
nejlepší	dobrý	k2eAgInPc4d3
příklady	příklad	k1gInPc4
dobré	dobrý	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
<g/>
)	)	kIx)
projektu	projekt	k1gInSc2
PRAGES	PRAGES	kA
Archivováno	archivován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
mapoval	mapovat	k5eAaImAgMnS
aktivity	aktivita	k1gFnPc4
na	na	k7c4
podporu	podpora	k1gFnSc4
genderové	genderový	k2eAgFnSc2d1
rovnosti	rovnost	k1gFnSc2
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
<g/>
Kanadě	Kanada	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
–	–	k?
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
je	být	k5eAaImIp3nS
zapojeno	zapojen	k2eAgNnSc1d1
do	do	k7c2
mezinárodních	mezinárodní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
a	a	k8xC
projektů	projekt	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
The	The	k?
International	International	k1gFnSc1
Research	Research	k1gMnSc1
Association	Association	k1gInSc1
of	of	k?
Institutions	Institutions	k1gInSc1
of	of	k?
Advanced	Advanced	k1gInSc1
Gender	Gender	k1gMnSc1
Studies	Studies	k1gMnSc1
RINGS	RINGS	kA
</s>
<s>
European	European	k1gInSc1
Platform	Platform	k1gInSc1
of	of	k?
Women	Women	k2eAgInSc1d1
Scientists	Scientists	k1gInSc1
(	(	kIx(
<g/>
EPWS	EPWS	kA
<g/>
)	)	kIx)
</s>
<s>
ATGender	ATGender	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
European	European	k1gMnSc1
Association	Association	k1gInSc4
for	forum	k1gNnPc2
Gender	Gender	k1gMnSc1
Research	Research	k1gMnSc1
<g/>
,	,	kIx,
Education	Education	k1gInSc1
and	and	k?
Documentation	Documentation	k1gInSc1
</s>
<s>
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
-	-	kIx~
gender	gender	k1gInSc1
a	a	k8xC
věda	věda	k1gFnSc1
se	se	k3xPyFc4
zapojuje	zapojovat	k5eAaImIp3nS
do	do	k7c2
evropského	evropský	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
akce	akce	k1gFnSc1
COST	COST	kA
gender	gender	k1gInSc4
<g/>
,	,	kIx,
science	scienec	k1gMnPc4
<g/>
,	,	kIx,
technology	technolog	k1gMnPc4
<g/>
,	,	kIx,
environment	environment	k1gInSc1
genderSTE	genderSTE	k?
(	(	kIx(
<g/>
TN	TN	kA
<g/>
1201	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Knowledge	Knowledge	k1gFnSc1
<g/>
,	,	kIx,
Institutions	Institutions	k1gInSc1
<g/>
,	,	kIx,
Gender	Gender	k1gMnSc1
<g/>
:	:	kIx,
An	An	k1gMnSc1
East-West	East-West	k1gMnSc1
Comparative	Comparativ	k1gInSc5
Study	stud	k1gInPc1
(	(	kIx(
<g/>
KNOWING	KNOWING	kA
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
rámcový	rámcový	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Central	Centrat	k5eAaPmAgMnS,k5eAaImAgMnS
European	European	k1gMnSc1
Centre	centr	k1gInSc5
for	forum	k1gNnPc2
Women	Womno	k1gNnPc2
and	and	k?
Youth	Youth	k1gMnSc1
in	in	k?
Science	Science	k1gFnSc1
(	(	kIx(
<g/>
CEC-WYS	CEC-WYS	k1gMnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
rámcový	rámcový	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stimulating	Stimulating	k1gInSc1
Policy	Polica	k1gFnSc2
Debate	Debat	k1gInSc5
on	on	k3xPp3gMnSc1
Women	Womno	k1gNnPc2
and	and	k?
Science	Science	k1gFnSc2
Issues	Issues	k1gMnSc1
in	in	k?
Central	Central	k1gMnSc5
Europe	Europ	k1gMnSc5
(	(	kIx(
<g/>
WS	WS	kA
Debate	Debat	k1gInSc5
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
rámcový	rámcový	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Study	stud	k1gInPc1
on	on	k3xPp3gMnSc1
Databases	Databases	k1gMnSc1
of	of	k?
Women	Women	k1gInSc1
Scientists	Scientists	k1gInSc1
(	(	kIx(
<g/>
DATAWOMSCI	DATAWOMSCI	kA
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
rámcový	rámcový	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
</s>
<s>
členství	členství	k1gNnSc1
v	v	k7c6
Helsinki	Helsink	k1gFnSc6
Group	Group	k1gInSc4
on	on	k3xPp3gMnSc1
Gender	Gender	k1gMnSc1
in	in	k?
Research	Research	k1gMnSc1
and	and	k?
Innovation	Innovation	k1gInSc1
</s>
<s>
členství	členství	k1gNnSc4
ve	v	k7c4
Steering	Steering	k1gInSc4
Group	Group	k1gInSc4
on	on	k3xPp3gMnSc1
Human	Human	k1gMnSc1
Resources	Resources	k1gMnSc1
and	and	k?
Mobility	mobilita	k1gFnSc2
</s>
<s>
členství	členství	k1gNnSc1
v	v	k7c6
expertních	expertní	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
</s>
<s>
Expertní	expertní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
pro	pro	k7c4
ERA	ERA	kA
komunikaci	komunikace	k1gFnSc6
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Expert	expert	k1gMnSc1
Group	Group	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Research	Research	k1gInSc1
Profession	Profession	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Networking	Networking	k1gInSc1
the	the	k?
Networks	Networks	k1gInSc1
Expert	expert	k1gMnSc1
Group	Group	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
členství	členství	k1gNnSc4
v	v	k7c6
poradních	poradní	k2eAgInPc6d1
orgánech	orgán	k1gInPc6
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
</s>
<s>
Stocktaking	Stocktaking	k1gInSc1
10	#num#	k4
years	yearsa	k1gFnPc2
<g/>
'	'	kIx"
activities	activities	k1gMnSc1
on	on	k3xPp3gMnSc1
women	womet	k5eAaImNgInS,k5eAaBmNgInS,k5eAaPmNgInS
and	and	k?
science	science	k1gFnSc1
conference	conference	k1gFnSc2
advisory	advisor	k1gInPc1
group	group	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
FP	FP	kA
7	#num#	k4
External	External	k1gFnSc2
Advisory	Advisor	k1gInPc1
Board	Boarda	k1gFnPc2
Science	Scienec	k1gInSc2
in	in	k?
Society	societa	k1gFnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Projekty	projekt	k1gInPc1
</s>
<s>
Mezi	mezi	k7c4
aktivity	aktivita	k1gFnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
akce	akce	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
NKC	NKC	kA
-	-	kIx~
ženy	žena	k1gFnPc1
a	a	k8xC
věda	věda	k1gFnSc1
podílí	podílet	k5eAaImIp3nS
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Cena	cena	k1gFnSc1
Milady	Milada	k1gFnSc2
Paulové	Paulová	k1gFnSc2
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
Ministerstvem	ministerstvo	k1gNnSc7
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
ČR	ČR	kA
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
uděluje	udělovat	k5eAaImIp3nS
Národní	národní	k2eAgNnSc1d1
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
–	–	k?
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
Cenu	cena	k1gFnSc4
Milady	Milada	k1gFnSc2
Paulové	Paulová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výstavy	výstava	k1gFnPc1
</s>
<s>
NKC	NKC	kA
pořádá	pořádat	k5eAaImIp3nS
výstavy	výstava	k1gFnPc4
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
představuje	představovat	k5eAaImIp3nS
významné	významný	k2eAgFnPc4d1
české	český	k2eAgFnPc4d1
vědkyně	vědkyně	k1gFnPc4
<g/>
,	,	kIx,
vědce	vědec	k1gMnPc4
i	i	k8xC
vědecké	vědecký	k2eAgInPc4d1
páry	pár	k1gInPc4
působící	působící	k2eAgInPc1d1
v	v	k7c6
různých	různý	k2eAgInPc6d1
oborech	obor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
Výstava	výstava	k1gFnSc1
Heyrovského	Heyrovský	k2eAgInSc2d1
sestra	sestra	k1gFnSc1
<g/>
:	:	kIx,
ženy	žena	k1gFnPc1
v	v	k7c6
české	český	k2eAgFnSc6d1
vědě	věda	k1gFnSc6
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Výstava	výstava	k1gFnSc1
Akademické	akademický	k2eAgFnSc2d1
páry	pára	k1gFnSc2
<g/>
:	:	kIx,
dvoukariérní	dvoukariérní	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
ve	v	k7c6
vědě	věda	k1gFnSc6
<g/>
,	,	kIx,
výzkumu	výzkum	k1gInSc6
a	a	k8xC
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
křest	křest	k1gInSc4
knihy	kniha	k1gFnSc2
Akademické	akademický	k2eAgInPc1d1
duety	duet	k1gInPc1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2010	#num#	k4
</s>
<s>
Výstava	výstava	k1gFnSc1
Vědci	vědec	k1gMnPc1
a	a	k8xC
vědkyně	vědkyně	k1gFnPc1
v	v	k7c6
pohybu	pohyb	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
zpracována	zpracovat	k5eAaPmNgFnS
i	i	k9
knižně	knižně	k6eAd1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2012	#num#	k4
</s>
<s>
Konference	konference	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
ženách	žena	k1gFnPc6
a	a	k8xC
vědě	věda	k1gFnSc6
<g/>
:	:	kIx,
Cesty	cesta	k1gFnSc2
labyrintem	labyrint	k1gInSc7
<g/>
:	:	kIx,
proč	proč	k6eAd1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
tak	tak	k9
málo	málo	k4c1
žen	žena	k1gFnPc2
ve	v	k7c6
vědě	věda	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
ženách	žena	k1gFnPc6
a	a	k8xC
vědě	věda	k1gFnSc6
<g/>
:	:	kIx,
Genderová	Genderový	k2eAgFnSc1d1
rovnost	rovnost	k1gFnSc1
jako	jako	k8xC,k8xS
sociální	sociální	k2eAgFnSc1d1
inovace	inovace	k1gFnSc1
<g/>
:	:	kIx,
Rovné	rovný	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
v	v	k7c6
měnícím	měnící	k2eAgMnSc6d1
se	se	k3xPyFc4
vědeckém	vědecký	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2014	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
genderu	gender	k1gInSc6
a	a	k8xC
vědě	věda	k1gFnSc6
<g/>
:	:	kIx,
Role	role	k1gFnSc1
státu	stát	k1gInSc2
a	a	k8xC
výzkumných	výzkumný	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2016	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
genderu	gendero	k1gNnSc6
a	a	k8xC
věda	věda	k1gFnSc1
<g/>
:	:	kIx,
Moje	můj	k3xOp1gFnSc1
instituce	instituce	k1gFnSc1
<g/>
,	,	kIx,
moje	můj	k3xOp1gFnSc1
odpovědnost	odpovědnost	k1gFnSc1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1
a	a	k8xC
institucionální	institucionální	k2eAgFnSc1d1
změna	změna	k1gFnSc1
</s>
<s>
Klub	klub	k1gInSc1
NKC	NKC	kA
</s>
<s>
Noc	noc	k1gFnSc1
vědců	vědec	k1gMnPc2
a	a	k8xC
vědkyň	vědkyně	k1gFnPc2
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc2
<g/>
:	:	kIx,
<g/>
Nástěnka	nástěnka	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Workshopy	workshop	k1gInPc4
<g/>
/	/	kIx~
<g/>
Edit-a-thon	Edit-a-thon	k1gInSc4
Ženy	žena	k1gFnSc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Wikipedie	Wikipedie	k1gFnSc2
<g/>
:	:	kIx,
<g/>
Nástěnka	nástěnka	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Workshopy	workshop	k1gInPc4
<g/>
/	/	kIx~
<g/>
Edit-a-thon	Edit-a-thon	k1gInSc4
Posviťme	posvítit	k5eAaPmRp1nP
si	se	k3xPyFc3
na	na	k7c4
(	(	kIx(
<g/>
české	český	k2eAgFnPc4d1
<g/>
)	)	kIx)
vědkyně	vědkyně	k1gFnPc4
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Sociologický	sociologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
v.v.i	v.v.i	k6eAd1
</s>
<s>
European	European	k1gInSc1
Platform	Platform	k1gInSc1
of	of	k?
Women	Women	k2eAgInSc1d1
Scientists	Scientists	k1gInSc1
</s>
<s>
ATGender	ATGender	k1gMnSc1
</s>
<s>
Brožura	brožura	k1gFnSc1
k	k	k7c3
Ceně	cena	k1gFnSc3
Milady	Milada	k1gFnSc2
Paulové	Paulová	k1gFnSc2
</s>
<s>
ADVANCE	ADVANCE	kA
</s>
<s>
Stránky	stránka	k1gFnPc1
Kulturní	kulturní	k2eAgFnPc1d1
a	a	k8xC
institucionální	institucionální	k2eAgFnPc1d1
změny	změna	k1gFnPc1
</s>
<s>
Manuál	manuál	k1gInSc4
Proč	proč	k6eAd1
a	a	k8xC
jak	jak	k6eAd1
na	na	k7c4
genderovou	genderový	k2eAgFnSc4d1
rovnost	rovnost	k1gFnSc4
ve	v	k7c6
vědě	věda	k1gFnSc6
</s>
<s>
Gendered	Gendered	k1gInSc1
Innovations	Innovationsa	k1gFnPc2
</s>
<s>
GenPORT	GenPORT	k?
</s>
<s>
COST	COST	kA
genderSTE	genderSTE	k?
</s>
<s>
Videozáznam	videozáznam	k1gInSc1
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
ženách	žena	k1gFnPc6
a	a	k8xC
vědě	věda	k1gFnSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ČERMÁKOVÁ	Čermáková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
HAVELKOVÁ	Havelková	k1gFnSc1
<g/>
,	,	kIx,
HANA	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PhDr.	PhDr.	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Policies	Policies	k1gMnSc1
on	on	k3xPp3gMnSc1
Women	Women	k2eAgInSc1d1
and	and	k?
Science	Science	k1gFnSc1
in	in	k?
Europe	Europ	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
na	na	k7c4
http://cordis.europa.eu/improving/women/policies.htm.	http://cordis.europa.eu/improving/women/policies.htm.	k?
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Waste	Wast	k1gInSc5
of	of	k?
talents	talentsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turning	Turning	k1gInSc1
private	privat	k1gInSc5
struggles	struggles	k1gInSc4
into	into	k1gMnSc1
a	a	k8xC
public	publicum	k1gNnPc2
issue	issue	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
na	na	k7c4
https://ec.europa.eu/research/swafs/pdf/pub_gender_equality/enwise-report_en.pdf.	https://ec.europa.eu/research/swafs/pdf/pub_gender_equality/enwise-report_en.pdf.	k?
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Women	Women	k1gInSc1
and	and	k?
Science	Science	k1gFnSc1
<g/>
:	:	kIx,
Excellence	Excellence	k1gFnSc1
and	and	k?
Innovation	Innovation	k1gInSc1
–	–	k?
Gender	Gender	k1gInSc1
Equality	Equalita	k1gFnSc2
in	in	k?
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
na	na	k7c4
https://ec.europa.eu/research/swafs/pdf/pub_gender_equality/sec_report_en.pdf.	https://ec.europa.eu/research/swafs/pdf/pub_gender_equality/sec_report_en.pdf.	k?
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Benchmarking	Benchmarking	k1gInSc1
policy	polica	k1gFnSc2
measures	measuresa	k1gFnPc2
for	forum	k1gNnPc2
gender	gender	k1gMnSc1
equality	equalita	k1gFnSc2
in	in	k?
science	science	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
na	na	k7c6
http://ec.europa.eu/research/science-society/document_library/pdf_06/benchmarking-policy-measures_en.pdf.	http://ec.europa.eu/research/science-society/document_library/pdf_06/benchmarking-policy-measures_en.pdf.	k4
<g/>
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Gender	Gender	k1gMnSc1
Challenge	Challeng	k1gFnSc2
in	in	k?
Research	Research	k1gInSc1
Funding	Funding	k1gInSc1
<g/>
:	:	kIx,
Assessing	Assessing	k1gInSc1
the	the	k?
European	European	k1gInSc1
national	nationat	k5eAaPmAgMnS,k5eAaImAgMnS
scenes	scenes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
na	na	k7c6
https://ec.europa.eu/research/science-society/document_library/pdf_06/gender-challenge-in-research-funding_en.pdf.	https://ec.europa.eu/research/science-society/document_library/pdf_06/gender-challenge-in-research-funding_en.pdf.	k4
<g/>
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
b	b	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Women	Women	k1gInSc1
and	and	k?
Science	Science	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
march	marcha	k1gFnPc2
toward	towarda	k1gFnPc2
equality	equalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Research	Research	k1gInSc1
<g/>
*	*	kIx~
<g/>
eu	eu	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
magazine	magazinout	k5eAaPmIp3nS
of	of	k?
the	the	k?
european	european	k1gInSc1
research	research	k1gMnSc1
area	area	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
na	na	k7c4
https://ec.europa.eu/research/research-eu/pdf/research_eu_women_en.pdf.	https://ec.europa.eu/research/research-eu/pdf/research_eu_women_en.pdf.	k?
↑	↑	k?
Lipinsky	Lipinsky	k1gMnSc7
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gender	Gender	k1gInSc1
Equality	Equalita	k1gFnSc2
Policies	Policies	k1gMnSc1
in	in	k?
Public	publicum	k1gNnPc2
Research	Researcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
na	na	k7c6
http://ec.europa.eu/research/pdf/199627_2014%202971_rtd_report.pdf.	http://ec.europa.eu/research/pdf/199627_2014%202971_rtd_report.pdf.	k4
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recommendations	Recommendations	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Implementation	Implementation	k1gInSc1
of	of	k?
the	the	k?
ERA	ERA	kA
Communication	Communication	k1gInSc1
Report	report	k1gInSc1
of	of	k?
the	the	k?
Expert	expert	k1gMnSc1
Group	Group	k1gMnSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
</s>
<s>
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
na	na	k7c6
http://ec.europa.eu/research/era/pdf/era_progress_report2013/expert-group-support.pdf.	http://ec.europa.eu/research/era/pdf/era_progress_report2013/expert-group-support.pdf.	k4
<g/>
↑	↑	k?
European	European	k1gInSc1
Commission	Commission	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Excellence	Excellence	k1gFnSc1
<g/>
,	,	kIx,
Equality	Equalita	k1gFnPc1
and	and	k?
Enterpreneurialism	Enterpreneurialism	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Building	Building	k1gInSc1
Sustainable	Sustainable	k1gFnSc2
Research	Researcha	k1gFnPc2
Careers	Careers	k1gInSc1
in	in	k?
the	the	k?
European	European	k1gInSc1
Research	Research	k1gMnSc1
Area	area	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luxembourg	Luxembourg	k1gInSc1
<g/>
:	:	kIx,
Office	Office	kA
for	forum	k1gNnPc2
Official	Official	k1gMnSc1
Publications	Publicationsa	k1gFnPc2
of	of	k?
the	the	k?
European	European	k1gMnSc1
Communities	Communities	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
na	na	k7c4
http://ec.europa.eu/euraxess/pdf/research_policies/ExpertGrouponResearchProfession.pdf.	http://ec.europa.eu/euraxess/pdf/research_policies/ExpertGrouponResearchProfession.pdf.	k?
<g/>
↑	↑	k?
VOHLÍDALOVÁ	VOHLÍDALOVÁ	kA
<g/>
,	,	kIx,
Marta	Marta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademické	akademický	k2eAgInPc1d1
duety	duet	k1gInPc1
<g/>
:	:	kIx,
o	o	k7c6
profesním	profesní	k2eAgInSc6d1
a	a	k8xC
soukromém	soukromý	k2eAgInSc6d1
životě	život	k1gInSc6
ve	v	k7c6
vědě	věda	k1gFnSc6
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologický	sociologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7330	#num#	k4
<g/>
-	-	kIx~
<g/>
180	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VOHLÍDALOVÁ	VOHLÍDALOVÁ	kA
<g/>
,	,	kIx,
Marta	Marta	k1gFnSc1
<g/>
;	;	kIx,
ČERVINKOVÁ	Červinková	k1gFnSc1
<g/>
,	,	kIx,
Alice	Alice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
a	a	k8xC
vědkyně	vědkyně	k1gFnPc1
v	v	k7c6
pohybu	pohyb	k1gInSc6
<g/>
:	:	kIx,
o	o	k7c6
akademické	akademický	k2eAgFnSc6d1
mobilitě	mobilita	k1gFnSc6
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sociologický	sociologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7330	#num#	k4
<g/>
-	-	kIx~
<g/>
226	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Cesty	cesta	k1gFnSc2
labyrintem	labyrint	k1gInSc7
<g/>
:	:	kIx,
proč	proč	k6eAd1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
tak	tak	k9
málo	málo	k4c1
žen	žena	k1gFnPc2
ve	v	k7c6
vědě	věda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademický	akademický	k2eAgInSc1d1
bulletin	bulletin	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TENGLEROVÁ	TENGLEROVÁ	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gender	Gendra	k1gFnPc2
<g/>
,	,	kIx,
rovné	rovný	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
,	,	kIx,
výzkum	výzkum	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Výstupy	výstup	k1gInPc7
2	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
ženách	žena	k1gFnPc6
a	a	k8xC
vědě	věda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
AULA	aula	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
3	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
genderu	gender	k1gInSc6
a	a	k8xC
vědě	věda	k1gFnSc6
<g/>
,	,	kIx,
pozvánka	pozvánka	k1gFnSc1
<g/>
:	:	kIx,
http://www.zenyaveda.cz/konference2014	http://www.zenyaveda.cz/konference2014	k4
Archivováno	archivován	k2eAgNnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
NKC	NKC	kA
-	-	kIx~
gender	gender	k1gMnSc1
a	a	k8xC
věda	věda	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
genderu	gender	k1gInSc6
a	a	k8xC
vědě	věda	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
navšíveno	navšívit	k5eAaPmNgNnS,k5eAaImNgNnS
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
http://www.genderaveda.cz/gender-veda/181103-konference/4-narodni-konference	http://www.genderaveda.cz/gender-veda/181103-konference/4-narodni-konference	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
17	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
132360440	#num#	k4
</s>
