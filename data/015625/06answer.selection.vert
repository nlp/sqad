<s desamb="1">
Je	být	k5eAaImIp3nS
jediným	jediný	k2eAgNnSc7d1
specializovaným	specializovaný	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
zaměřeným	zaměřený	k2eAgNnSc7d1
na	na	k7c4
výzkum	výzkum	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
genderové	genderové	k2eAgFnSc1d1
sociologie	sociologie	k1gFnSc1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
genderových	genderův	k2eAgNnPc2d1
studií	studio	k1gNnPc2
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
genderové	genderový	k2eAgFnSc2d1
rovnosti	rovnost	k1gFnSc2
a	a	k8xC
genderového	genderový	k2eAgInSc2d1
mainstreamingu	mainstreaming	k1gInSc2
ve	v	k7c6
vědě	věda	k1gFnSc6
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>