<s>
Balalajka	balalajka	k1gFnSc1	balalajka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
б	б	k?	б
<g/>
́	́	k?	́
<g/>
й	й	k?	й
<g/>
,	,	kIx,	,
balalajka	balalajka	k1gFnSc1	balalajka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc4d1	strunný
nástroj	nástroj	k1gInSc4	nástroj
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
s	s	k7c7	s
trojdílnou	trojdílný	k2eAgFnSc7d1	trojdílná
rezonanční	rezonanční	k2eAgFnSc7d1	rezonanční
skříní	skříň	k1gFnSc7	skříň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
