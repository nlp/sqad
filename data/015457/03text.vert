<s>
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
PSČ	PSČ	kA
(	(	kIx(
<g/>
též	též	k9
poštovní	poštovní	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
číselným	číselný	k2eAgNnSc7d1
označením	označení	k1gNnSc7
územního	územní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
adresní	adresní	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
každý	každý	k3xTgInSc4
územní	územní	k2eAgInSc4d1
obvod	obvod	k1gInSc4
je	být	k5eAaImIp3nS
PSČ	PSČ	kA
jedinečné	jedinečný	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
poštovním	poštovní	k2eAgInSc6d1
styku	styk	k1gInSc6
jako	jako	k9
součást	součást	k1gFnSc4
adresy	adresa	k1gFnSc2
především	především	k9
pro	pro	k7c4
identifikaci	identifikace	k1gFnSc4
místa	místo	k1gNnSc2
doručení	doručení	k1gNnSc2
při	při	k7c6
automatizovaném	automatizovaný	k2eAgNnSc6d1
třídění	třídění	k1gNnSc6
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
číselné	číselný	k2eAgNnSc4d1
označení	označení	k1gNnSc4
(	(	kIx(
<g/>
v	v	k7c6
řadě	řada	k1gFnSc6
zemí	zem	k1gFnPc2
kombinace	kombinace	k1gFnSc2
číslic	číslice	k1gFnPc2
a	a	k8xC
písmen	písmeno	k1gNnPc2
<g/>
)	)	kIx)
územního	územní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
dodávací	dodávací	k2eAgFnSc2d1
(	(	kIx(
<g/>
adresní	adresní	k2eAgFnSc2d1
<g/>
)	)	kIx)
pošty	pošta	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
menšího	malý	k2eAgInSc2d2
dodávacího	dodávací	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
nebo	nebo	k8xC
poštovní	poštovní	k2eAgFnSc2d1
přihrádky	přihrádka	k1gFnSc2
(	(	kIx(
<g/>
P.	P.	kA
O.	O.	kA
Box	box	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Správcem	správce	k1gMnSc7
číselníku	číselník	k1gInSc2
PSČ	PSČ	kA
je	být	k5eAaImIp3nS
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číselník	číselník	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
územní	územní	k2eAgFnPc4d1
PSČ	PSČ	kA
a	a	k8xC
firemní	firemní	k2eAgFnSc7d1
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správcem	správce	k1gMnSc7
názvů	název	k1gInPc2
je	být	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Prvopočátkem	prvopočátek	k1gInSc7
bylo	být	k5eAaImAgNnS
rozdělování	rozdělování	k1gNnSc4
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
na	na	k7c4
doručovací	doručovací	k2eAgInPc4d1
obvody	obvod	k1gInPc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Londýn	Londýn	k1gInSc1
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
10	#num#	k4
obvodů	obvod	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1857	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
již	již	k9
takové	takový	k3xDgNnSc1
číslování	číslování	k1gNnSc1
bylo	být	k5eAaImAgNnS
zavedeno	zavést	k5eAaPmNgNnS
ve	v	k7c6
více	hodně	k6eAd2
evropských	evropský	k2eAgNnPc6d1
velkoměstech	velkoměsto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
bylo	být	k5eAaImAgNnS
poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
na	na	k7c6
území	území	k1gNnSc6
celé	celý	k2eAgFnSc2d1
země	zem	k1gFnSc2
zavedeno	zavést	k5eAaPmNgNnS
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byl	být	k5eAaImAgInS
systém	systém	k1gInSc1
ale	ale	k8xC
opuštěn	opuštěn	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
se	s	k7c7
zaváděním	zavádění	k1gNnSc7
poštovních	poštovní	k2eAgNnPc2d1
směrovacích	směrovací	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
provádělo	provádět	k5eAaImAgNnS
Německo	Německo	k1gNnSc1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souviselo	souviset	k5eAaImAgNnS
to	ten	k3xDgNnSc1
zejména	zejména	k9
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
na	na	k7c6
poštách	pošta	k1gFnPc6
ubývalo	ubývat	k5eAaImAgNnS
pracovníků	pracovník	k1gMnPc2
s	s	k7c7
dobrou	dobrý	k2eAgFnSc7d1
znalostí	znalost	k1gFnSc7
zeměpisu	zeměpis	k1gInSc2
tak	tak	k6eAd1
i	i	k9
s	s	k7c7
rozšiřujícím	rozšiřující	k2eAgMnSc7d1
se	se	k3xPyFc4
územím	území	k1gNnSc7
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
doručovat	doručovat	k5eAaImF
zásilky	zásilka	k1gFnPc4
německým	německý	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
zdařilo	zdařit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
PSČ	PSČ	kA
zavedla	zavést	k5eAaPmAgFnS
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc3
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
USA	USA	kA
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
Švýcarsko	Švýcarsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
zaváděním	zavádění	k1gNnSc7
PSČ	PSČ	kA
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
objevovat	objevovat	k5eAaImF
úvahy	úvaha	k1gFnPc4
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
využití	využití	k1gNnSc6
při	při	k7c6
strojním	strojní	k2eAgNnSc6d1
třídění	třídění	k1gNnSc6
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
evidovala	evidovat	k5eAaImAgFnS
Světová	světový	k2eAgFnSc1d1
poštovní	poštovní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
zavedení	zavedení	k1gNnSc2
PSČ	PSČ	kA
u	u	k7c2
117	#num#	k4
ze	z	k7c2
190	#num#	k4
svých	svůj	k3xOyFgMnPc2
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PSČ	PSČ	kA
dodnes	dodnes	k6eAd1
nepoužívá	používat	k5eNaImIp3nS
např.	např.	kA
Irsko	Irsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
PSČ	PSČ	kA
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
<s>
Přípravou	příprava	k1gFnSc7
projektu	projekt	k1gInSc2
zavedení	zavedení	k1gNnSc2
PSČ	PSČ	kA
v	v	k7c6
Československu	Československo	k1gNnSc6
se	se	k3xPyFc4
započalo	započnout	k5eAaPmAgNnS
koncem	koncem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ve	v	k7c6
Výzkumném	výzkumný	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
spojů	spoj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dostal	dostat	k5eAaPmAgMnS
projekt	projekt	k1gInSc4
na	na	k7c4
starost	starost	k1gFnSc4
ministerský	ministerský	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Špaček	Špaček	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
bylo	být	k5eAaImAgNnS
třídění	třídění	k1gNnSc1
poštovních	poštovní	k2eAgFnPc2d1
zásilek	zásilka	k1gFnPc2
závislé	závislý	k2eAgFnPc4d1
na	na	k7c6
specialistech	specialista	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
perfektně	perfektně	k6eAd1
uměli	umět	k5eAaImAgMnP
poštovní	poštovní	k2eAgInSc4d1
zeměpis	zeměpis	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
vzory	vzor	k1gInPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
směrovací	směrovací	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
již	již	k9
užívala	užívat	k5eAaImAgNnP
<g/>
,	,	kIx,
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
Špaček	Špaček	k1gMnSc1
Německo	Německo	k1gNnSc4
a	a	k8xC
Švédsko	Švédsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
definitivním	definitivní	k2eAgNnSc6d1
zavedení	zavedení	k1gNnSc6
se	se	k3xPyFc4
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
rozhodlo	rozhodnout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavedena	zaveden	k2eAgFnSc1d1
v	v	k7c6
Československu	Československo	k1gNnSc6
byla	být	k5eAaImAgFnS
PSČ	PSČ	kA
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
sdělení	sdělení	k1gNnSc2
tehdejšího	tehdejší	k2eAgNnSc2d1
Federálního	federální	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
spojů	spoj	k1gInPc2
bylo	být	k5eAaImAgNnS
důvodem	důvod	k1gInSc7
úsilí	úsilí	k1gNnSc2
o	o	k7c4
neustálé	neustálý	k2eAgNnSc4d1
zdokonalování	zdokonalování	k1gNnSc4
služeb	služba	k1gFnPc2
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
závěry	závěr	k1gInPc7
XIV	XIV	kA
<g/>
.	.	kIx.
sjezdu	sjezd	k1gInSc6
KSČ	KSČ	kA
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
nových	nový	k2eAgFnPc2d1
moderních	moderní	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
<g/>
,	,	kIx,
zracionalizovat	zracionalizovat	k5eAaPmF
pracovní	pracovní	k2eAgInPc4d1
postupy	postup	k1gInPc4
a	a	k8xC
připravit	připravit	k5eAaPmF
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
použití	použití	k1gNnSc4
poloautomatických	poloautomatický	k2eAgInPc2d1
a	a	k8xC
automatických	automatický	k2eAgInPc2d1
strojů	stroj	k1gInPc2
na	na	k7c4
třídění	třídění	k1gNnSc4
poštovních	poštovní	k2eAgFnPc2d1
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Zavedení	zavedení	k1gNnSc1
bylo	být	k5eAaImAgNnS
provázeno	provázet	k5eAaImNgNnS
masivní	masivní	k2eAgFnSc7d1
reklamní	reklamní	k2eAgFnSc7d1
kampaní	kampaň	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
předvánočním	předvánoční	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
zavádění	zavádění	k1gNnSc2
PSČ	PSČ	kA
bylo	být	k5eAaImAgNnS
nejprve	nejprve	k6eAd1
na	na	k7c6
vzorcích	vzorec	k1gInPc6
měsičně	měsičně	k6eAd1
sledováno	sledován	k2eAgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zásilek	zásilka	k1gFnPc2
s	s	k7c7
PSČ	PSČ	kA
přibývá	přibývat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
vlastní	vlastní	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
zpracování	zpracování	k1gNnSc1
podle	podle	k7c2
směrových	směrový	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
se	se	k3xPyFc4
nespustí	spustit	k5eNaPmIp3nS
dřív	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
jím	on	k3xPp3gNnSc7
bude	být	k5eAaImBp3nS
označeno	označit	k5eAaPmNgNnS
80	#num#	k4
procent	procento	k1gNnPc2
zásilek	zásilka	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
splněno	splnit	k5eAaPmNgNnS
asi	asi	k9
do	do	k7c2
roka	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
zavedení	zavedení	k1gNnSc1
mezi	mezi	k7c7
obyvatelstvem	obyvatelstvo	k1gNnSc7
v	v	k7c6
počátcích	počátek	k1gInPc6
naráželo	narážet	k5eAaPmAgNnS,k5eAaImAgNnS
na	na	k7c4
nedostatečnou	dostatečný	k2eNgFnSc4d1
informovanost	informovanost	k1gFnSc4
<g/>
,	,	kIx,
používalo	používat	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c6
polovině	polovina	k1gFnSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
na	na	k7c4
90	#num#	k4
%	%	kIx~
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
se	se	k3xPyFc4
podle	podle	k7c2
směrovacích	směrovací	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
začalo	začít	k5eAaPmAgNnS
třídit	třídit	k5eAaImF
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
se	se	k3xPyFc4
rozeběhlo	rozeběhnout	k5eAaPmAgNnS
automatické	automatický	k2eAgNnSc1d1
rovnání	rovnání	k1gNnSc1
a	a	k8xC
razítkování	razítkování	k1gNnSc1
zásilek	zásilka	k1gFnPc2
na	na	k7c6
centrální	centrální	k2eAgFnSc6d1
pražské	pražský	k2eAgFnSc6d1
poště	pošta	k1gFnSc6
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
dodán	dodán	k2eAgInSc1d1
zbytek	zbytek	k1gInSc1
třídicí	třídicí	k2eAgFnSc2d1
linky	linka	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
automatického	automatický	k2eAgNnSc2d1
čtení	čtení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1979	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
poště	pošta	k1gFnSc6
Praha	Praha	k1gFnSc1
025	#num#	k4
slavnostně	slavnostně	k6eAd1
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
provozu	provoz	k1gInSc6
první	první	k4xOgFnSc1
třídící	třídící	k2eAgFnSc1d1
automatizovaná	automatizovaný	k2eAgFnSc1d1
linka	linka	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
od	od	k7c2
japonské	japonský	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Nippon	Nippon	k1gMnSc1
Electric	Electric	k1gMnSc1
Co	co	k9
<g/>
.	.	kIx.
(	(	kIx(
<g/>
část	část	k1gFnSc1
této	tento	k3xDgFnSc2
linky	linka	k1gFnSc2
byla	být	k5eAaImAgFnS
ve	v	k7c6
zkušebním	zkušební	k2eAgInSc6d1
provozu	provoz	k1gInSc6
již	již	k6eAd1
od	od	k7c2
dubna	duben	k1gInSc2
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1982	#num#	k4
ji	on	k3xPp3gFnSc4
následovala	následovat	k5eAaImAgFnS
linka	linka	k1gFnSc1
ATL	ATL	kA
NEC	NEC	kA
na	na	k7c6
poště	pošta	k1gFnSc6
Bratislava	Bratislava	k1gFnSc1
0	#num#	k4
<g/>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
námitkou	námitka	k1gFnSc7
proti	proti	k7c3
PSČ	PSČ	kA
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tato	tento	k3xDgNnPc1
čísla	číslo	k1gNnPc1
uživatelé	uživatel	k1gMnPc1
i	i	k8xC
pracovníci	pracovník	k1gMnPc1
poštovních	poštovní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
museli	muset	k5eAaImAgMnP
učit	učit	k5eAaImF
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
mnozí	mnohý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
zejména	zejména	k9
ve	v	k7c6
městech	město	k1gNnPc6
s	s	k7c7
více	hodně	k6eAd2
poštami	pošta	k1gFnPc7
ani	ani	k8xC
nezajímali	zajímat	k5eNaImAgMnP
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
pod	pod	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
dodávací	dodávací	k2eAgFnSc4d1
poštu	pošta	k1gFnSc4
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumentem	argument	k1gInSc7
pro	pro	k7c4
zavedení	zavedení	k1gNnSc4
pak	pak	k6eAd1
byly	být	k5eAaImAgFnP
zejména	zejména	k9
obce	obec	k1gFnPc1
se	s	k7c7
shodnými	shodný	k2eAgInPc7d1
názvy	název	k1gInPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
tak	tak	k9
snížilo	snížit	k5eAaPmAgNnS
riziko	riziko	k1gNnSc1
záměny	záměna	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
adrese	adresa	k1gFnSc6
standardně	standardně	k6eAd1
uváděl	uvádět	k5eAaImAgMnS
název	název	k1gInSc4
okresu	okres	k1gInSc2
a	a	k8xC
povinnost	povinnost	k1gFnSc4
tohoto	tento	k3xDgInSc2
údaje	údaj	k1gInSc2
byla	být	k5eAaImAgFnS
se	s	k7c7
zavedením	zavedení	k1gNnSc7
PSČ	PSČ	kA
zrušena	zrušen	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Automatizace	automatizace	k1gFnSc1
třídění	třídění	k1gNnSc2
zásilek	zásilka	k1gFnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
zavedení	zavedení	k1gNnSc1
PSČ	PSČ	kA
zatím	zatím	k6eAd1
jen	jen	k9
velmi	velmi	k6eAd1
výhledovou	výhledový	k2eAgFnSc7d1
představou	představa	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PSČ	PSČ	kA
se	se	k3xPyFc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
stalo	stát	k5eAaPmAgNnS
téměř	téměř	k6eAd1
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
adresy	adresa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgNnSc7
z	z	k7c2
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
s	s	k7c7
adresou	adresa	k1gFnSc7
není	být	k5eNaImIp3nS
uváděno	uvádět	k5eAaImNgNnS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
občanské	občanský	k2eAgInPc4d1
průkazy	průkaz	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
nezměněné	změněný	k2eNgFnSc6d1
podobě	podoba	k1gFnSc6
používá	používat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
koncepty	koncept	k1gInPc7
možných	možný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
v	v	k7c6
systému	systém	k1gInSc6
PSČ	PSČ	kA
se	se	k3xPyFc4
zaměřují	zaměřovat	k5eAaImIp3nP
na	na	k7c4
geomarketingové	geomarketingový	k2eAgNnSc4d1
využití	využití	k1gNnSc4
PSČ	PSČ	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
ale	ale	k8xC
především	především	k9
z	z	k7c2
finančních	finanční	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
se	se	k3xPyFc4
změna	změna	k1gFnSc1
neočekává	očekávat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Formát	formát	k1gInSc1
a	a	k8xC
význam	význam	k1gInSc1
PSČ	PSČ	kA
</s>
<s>
PSČ	PSČ	kA
je	být	k5eAaImIp3nS
pěticiferné	pěticiferný	k2eAgInPc4d1
<g/>
,	,	kIx,
členěné	členěný	k2eAgInPc4d1
na	na	k7c4
trojčíslí	trojčíslí	k1gNnSc4
a	a	k8xC
dvojčíslí	dvojčíslí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
číslice	číslice	k1gFnSc1
PSČ	PSČ	kA
přibližně	přibližně	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
starému	starý	k2eAgNnSc3d1
krajskému	krajský	k2eAgNnSc3d1
rozdělení	rozdělení	k1gNnSc3
Československa	Československo	k1gNnSc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samostatnou	samostatný	k2eAgFnSc4d1
číslici	číslice	k1gFnSc4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
ovšem	ovšem	k9
dostala	dostat	k5eAaPmAgFnS
i	i	k9
Bratislava	Bratislava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
pak	pak	k6eAd1
byly	být	k5eAaImAgInP
spojeny	spojen	k2eAgInPc1d1
Jihočeský	jihočeský	k2eAgMnSc1d1
a	a	k8xC
Západočeský	západočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
pod	pod	k7c4
jedno	jeden	k4xCgNnSc4
číslo	číslo	k1gNnSc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tři	tři	k4xCgInPc4
slovenské	slovenský	k2eAgInPc4d1
kraje	kraj	k1gInPc4
pak	pak	k6eAd1
zbyly	zbýt	k5eAaPmAgInP
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
číslice	číslice	k1gFnPc4
<g/>
:	:	kIx,
Středoslovenský	středoslovenský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
byl	být	k5eAaImAgInS
nakonec	nakonec	k6eAd1
rozdělen	rozdělit	k5eAaPmNgInS
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jestli	jestli	k8xS
se	se	k3xPyFc4
odtud	odtud	k6eAd1
zásilky	zásilka	k1gFnPc1
vozily	vozit	k5eAaImAgFnP
do	do	k7c2
sběrného	sběrný	k2eAgInSc2d1
přepravního	přepravní	k2eAgInSc2d1
uzlu	uzel	k1gInSc2
v	v	k7c6
Žilině	Žilina	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
do	do	k7c2
Zvolena	Zvolen	k1gInSc2
<g/>
:	:	kIx,
jih	jih	k1gInSc1
dostal	dostat	k5eAaPmAgInS
devítku	devítka	k1gFnSc4
a	a	k8xC
sever	sever	k1gInSc4
nulu	nula	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
Hlavní	hlavní	k2eAgInSc4d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
+	+	kIx~
<g/>
speciální	speciální	k2eAgFnPc1d1
adresy	adresa	k1gFnPc1
21	#num#	k4
<g/>
xxx	xxx	k?
a	a	k8xC
22	#num#	k4
<g/>
xxx	xxx	k?
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
25	#num#	k4
<g/>
xxx	xxx	k?
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
Jihočeský	jihočeský	k2eAgInSc4d1
a	a	k8xC
Západočeský	západočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
</s>
<s>
4	#num#	k4
Severočeský	severočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
5	#num#	k4
Východočeský	východočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
+	+	kIx~
část	část	k1gFnSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
7	#num#	k4
Severomoravský	severomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
;	;	kIx,
číslicí	číslice	k1gFnSc7
7	#num#	k4
ale	ale	k8xC
začínají	začínat	k5eAaImIp3nP
i	i	k9
PSČ	PSČ	kA
okresů	okres	k1gInPc2
Zlín	Zlín	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Gottwaldov	Gottwaldov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kroměříž	Kroměříž	k1gFnSc1
a	a	k8xC
Prostějov	Prostějov	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
územně	územně	k6eAd1
součástí	součást	k1gFnSc7
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
Zlín	Zlín	k1gInSc1
–	–	k?
760	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Kroměříž	Kroměříž	k1gFnSc1
–	–	k?
767	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Prostějov	Prostějov	k1gInSc1
–	–	k?
796	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
Bratislava	Bratislava	k1gFnSc1
a	a	k8xC
okolí	okolí	k1gNnSc1
</s>
<s>
9	#num#	k4
Západoslovenský	západoslovenský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
a	a	k8xC
Středoslovenský	středoslovenský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
Středoslovenský	středoslovenský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Východoslovenský	východoslovenský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Podobné	podobný	k2eAgNnSc1d1
číslování	číslování	k1gNnSc1
krajů	kraj	k1gInPc2
bylo	být	k5eAaImAgNnS
použito	použít	k5eAaPmNgNnS
i	i	k9
k	k	k7c3
číslování	číslování	k1gNnSc3
okresních	okresní	k2eAgInPc2d1
závodů	závod	k1gInPc2
krajských	krajský	k2eAgInPc2d1
podniků	podnik	k1gInPc2
ČSAD	ČSAD	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
číslice	číslice	k1gFnSc1
PSČ	PSČ	kA
označovala	označovat	k5eAaImAgFnS
obvody	obvod	k1gInPc1
sběrných	sběrný	k2eAgInPc2d1
přepravních	přepravní	k2eAgInPc2d1
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
číslice	číslice	k1gFnSc1
označovala	označovat	k5eAaImAgFnS
okresní	okresní	k2eAgInSc4d1
přepravní	přepravní	k2eAgInSc4d1
uzel	uzel	k1gInSc4
<g/>
,	,	kIx,
okresní	okresní	k2eAgNnSc4d1
přepravní	přepravní	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
<g/>
,	,	kIx,
okresní	okresní	k2eAgNnPc1d1
a	a	k8xC
bývalá	bývalý	k2eAgNnPc1d1
okresní	okresní	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimku	výjimek	k1gInSc2
tvořilo	tvořit	k5eAaImAgNnS
6	#num#	k4
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
a	a	k8xC
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yIgFnPc2,k3yRgFnPc2,k3yQgFnPc2
již	již	k9
druhá	druhý	k4xOgFnSc1
a	a	k8xC
třetí	třetí	k4xOgFnSc1
číslice	číslice	k1gFnSc1
vyjadřovala	vyjadřovat	k5eAaImAgFnS
obvod	obvod	k1gInSc4
dodávací	dodávací	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
dvojčíslí	dvojčíslí	k1gNnSc1
<g/>
,	,	kIx,
uváděné	uváděný	k2eAgNnSc1d1
za	za	k7c7
mezerou	mezera	k1gFnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
přidělovalo	přidělovat	k5eAaImAgNnS
poštám	pošta	k1gFnPc3
v	v	k7c6
rámci	rámec	k1gInSc6
okresu	okres	k1gInSc2
v	v	k7c6
tom	ten	k3xDgNnSc6
pořadí	pořadí	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
je	být	k5eAaImIp3nS
objížděla	objíždět	k5eAaImAgFnS
auta	auto	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zajišťovala	zajišťovat	k5eAaImAgFnS
svoz	svoz	k1gInSc4
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Své	svůj	k3xOyFgNnSc1
vlastní	vlastní	k2eAgNnSc1d1
poštovní	poštovní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
ale	ale	k8xC
získaly	získat	k5eAaPmAgFnP
i	i	k9
některé	některý	k3yIgInPc1
velké	velký	k2eAgInPc1d1
podniky	podnik	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
dostávaly	dostávat	k5eAaImAgInP
hodně	hodně	k6eAd1
poštovních	poštovní	k2eAgFnPc2d1
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Československá	československý	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
nepochybně	pochybně	k6eNd1
i	i	k9
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
televizi	televize	k1gFnSc6
pro	pro	k7c4
propagaci	propagace	k1gFnSc4
PSČ	PSČ	kA
redaktor	redaktor	k1gMnSc1
Pech	Pech	k1gMnSc1
vymyslel	vymyslet	k5eAaPmAgMnS
slogan	slogan	k1gInSc4
<g/>
:	:	kIx,
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
chce	chtít	k5eAaImIp3nS
televizi	televize	k1gFnSc4
psát	psát	k5eAaImF
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc4
jedničky	jednička	k1gFnPc4
padesát	padesát	k4xCc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Praze	Praha	k1gFnSc6
druhá	druhý	k4xOgFnSc1
číslice	číslice	k1gFnSc1
PSČ	PSČ	kA
odpovídá	odpovídat	k5eAaImIp3nS
jednomu	jeden	k4xCgNnSc3
z	z	k7c2
deseti	deset	k4xCc2
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnPc4
číslice	číslice	k1gFnPc4
zpravidla	zpravidla	k6eAd1
rozlišuje	rozlišovat	k5eAaImIp3nS
dodávací	dodávací	k2eAgFnSc4d1
poštu	pošta	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
obvodu	obvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PSČ	PSČ	kA
většiny	většina	k1gFnSc2
pražských	pražský	k2eAgFnPc2d1
dodávacích	dodávací	k2eAgFnPc2d1
pošt	pošta	k1gFnPc2
končí	končit	k5eAaImIp3nP
číslicemi	číslice	k1gFnPc7
„	„	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiná	jiný	k2eAgNnPc1d1
koncová	koncový	k2eAgNnPc1d1
dvojčíslí	dvojčíslí	k1gNnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
k	k	k7c3
rozlišení	rozlišení	k1gNnSc3
poštovních	poštovní	k2eAgFnPc2d1
přihrádek	přihrádka	k1gFnPc2
na	na	k7c6
dodávací	dodávací	k2eAgFnSc6d1
poště	pošta	k1gFnSc6
určené	určený	k2eAgFnSc6d1
prvním	první	k4xOgNnPc3
trojčíslím	trojčíslí	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
5	#num#	k4
a	a	k8xC
v	v	k7c6
Praze	Praha	k1gFnSc6
9	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
deset	deset	k4xCc4
dodávacích	dodávací	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
některé	některý	k3yIgFnPc1
dodávací	dodávací	k2eAgFnPc1d1
pošty	pošta	k1gFnPc1
rozlišeny	rozlišit	k5eAaPmNgFnP
posledním	poslední	k2eAgNnSc7d1
dvojčíslím	dvojčíslí	k1gNnSc7
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
190	#num#	k4
00	#num#	k4
je	být	k5eAaImIp3nS
PSČ	PSČ	kA
dodávací	dodávací	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
dodávací	dodávací	k2eAgFnSc7d1
poštou	pošta	k1gFnSc7
pro	pro	k7c4
část	část	k1gFnSc4
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
190	#num#	k4
22	#num#	k4
je	být	k5eAaImIp3nS
PSČ	PSČ	kA
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
sídlo	sídlo	k1gNnSc1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
jejího	její	k3xOp3gInSc2
dodávacího	dodávací	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g/>
,	,	kIx,
197	#num#	k4
00	#num#	k4
je	být	k5eAaImIp3nS
PSČ	PSČ	kA
dodávací	dodávací	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
Praha	Praha	k1gFnSc1
97	#num#	k4
<g/>
,	,	kIx,
umístěné	umístěný	k2eAgInPc1d1
ve	v	k7c6
Kbelích	Kbely	k1gInPc6
v	v	k7c6
městském	městský	k2eAgInSc6d1
obvodě	obvod	k1gInSc6
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
190	#num#	k4
14	#num#	k4
je	být	k5eAaImIp3nS
PSČ	PSČ	kA
dodávací	dodávací	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
Praha	Praha	k1gFnSc1
914	#num#	k4
v	v	k7c6
Klánovicích	Klánovice	k1gFnPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1986	#num#	k4
a	a	k8xC
1991	#num#	k4
byla	být	k5eAaImAgFnS
změněna	změnit	k5eAaPmNgFnS
PSČ	PSČ	kA
částí	část	k1gFnSc7
připojených	připojený	k2eAgMnPc2d1
k	k	k7c3
Praze	Praha	k1gFnSc3
roku	rok	k1gInSc2
1974	#num#	k4
z	z	k7c2
čísel	číslo	k1gNnPc2
začínajících	začínající	k2eAgMnPc2d1
číslicí	číslice	k1gFnSc7
2	#num#	k4
na	na	k7c6
pražská	pražský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
byla	být	k5eAaImAgFnS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1982	#num#	k4
provedena	proveden	k2eAgFnSc1d1
reorganizace	reorganizace	k1gFnSc1
dodávacích	dodávací	k2eAgFnPc2d1
pošt	pošta	k1gFnPc2
a	a	k8xC
změněna	změněn	k2eAgFnSc1d1
všechna	všechen	k3xTgFnSc1
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s>
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
jedinečné	jedinečný	k2eAgNnSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
státu	stát	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
neměnné	měnný	k2eNgNnSc1d1,k2eAgNnSc1d1
<g/>
,	,	kIx,
nemění	měnit	k5eNaImIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
příslušnost	příslušnost	k1gFnSc1
pošty	pošta	k1gFnSc2
k	k	k7c3
přepravnímu	přepravní	k2eAgInSc3d1
uzlu	uzel	k1gInSc3
(	(	kIx(
<g/>
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
však	však	k9
měnit	měnit	k5eAaImF
dodávací	dodávací	k2eAgInPc4d1
územní	územní	k2eAgInPc4d1
obvody	obvod	k1gInPc4
pošt	pošta	k1gFnPc2
a	a	k8xC
zřizovat	zřizovat	k5eAaImF
či	či	k8xC
rušit	rušit	k5eAaImF
pošty	pošta	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správcem	správce	k1gMnSc7
číselníku	číselník	k1gInSc2
PSČ	PSČ	kA
byla	být	k5eAaImAgFnS
Československá	československý	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodávacím	dodávací	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
pošty	pošta	k1gFnSc2
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
území	území	k1gNnSc4
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
obcí	obec	k1gFnPc2
nebo	nebo	k8xC
souvislé	souvislý	k2eAgFnSc2d1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
samoty	samota	k1gFnPc1
či	či	k8xC
jiné	jiný	k2eAgFnPc1d1
osamocené	osamocený	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
mimo	mimo	k7c4
hlavní	hlavní	k2eAgFnSc4d1
část	část	k1gFnSc4
obce	obec	k1gFnSc2
bývají	bývat	k5eAaImIp3nP
někdy	někdy	k6eAd1
přičleněny	přičlenit	k5eAaPmNgFnP
k	k	k7c3
dodávacímu	dodávací	k2eAgInSc3d1
obvodu	obvod	k1gInSc2
té	ten	k3xDgFnSc2
pošty	pošta	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
níž	jenž	k3xRgFnSc2
jsou	být	k5eAaImIp3nP
přístupnější	přístupný	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
podniky	podnik	k1gInPc1
nebo	nebo	k8xC
instituce	instituce	k1gFnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
vlastní	vlastní	k2eAgNnSc4d1
směrovací	směrovací	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
v	v	k7c6
takových	takový	k3xDgInPc6
případech	případ	k1gInPc6
odpovídá	odpovídat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc3
poštovní	poštovní	k2eAgFnSc3d1
přihrádce	přihrádka	k1gFnSc3
na	na	k7c6
dodávací	dodávací	k2eAgFnSc6d1
poště	pošta	k1gFnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
ČTÚ	ČTÚ	kA
má	mít	k5eAaImIp3nS
PSČ	PSČ	kA
225	#num#	k4
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgInSc4d1
dvojčíslí	dvojčíslí	k1gNnSc1
pak	pak	k6eAd1
bývá	bývat	k5eAaImIp3nS
společně	společně	k6eAd1
použito	použít	k5eAaPmNgNnS
pro	pro	k7c4
ostatní	ostatní	k2eAgMnPc4d1
adresáty	adresát	k1gMnPc4
s	s	k7c7
přihrádkami	přihrádka	k1gFnPc7
(	(	kIx(
<g/>
P.	P.	kA
<g/>
O.	O.	kA
BOXy	box	k1gInPc4
<g/>
)	)	kIx)
na	na	k7c6
hlavních	hlavní	k2eAgFnPc6d1
poštách	pošta	k1gFnPc6
v	v	k7c6
regionu	region	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
PSČ	PSČ	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
Československa	Československo	k1gNnSc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1993	#num#	k4
a	a	k8xC
rozdělení	rozdělení	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pošt	pošta	k1gFnPc2
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
na	na	k7c4
Českou	český	k2eAgFnSc4d1
poštu	pošta	k1gFnSc4
a	a	k8xC
Slovenskou	slovenský	k2eAgFnSc4d1
poštu	pošta	k1gFnSc4
převzaly	převzít	k5eAaPmAgFnP
obě	dva	k4xCgFnPc1
československá	československý	k2eAgNnPc4d1
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nebyly	být	k5eNaImAgFnP
žádné	žádný	k3yNgFnPc1
výraznější	výrazný	k2eAgFnPc1d2
snahy	snaha	k1gFnPc1
změnit	změnit	k5eAaPmF
systém	systém	k1gInSc4
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s>
Zdrojem	zdroj	k1gInSc7
PSČ	PSČ	kA
je	být	k5eAaImIp3nS
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
(	(	kIx(
<g/>
ČSÚ	ČSÚ	kA
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Příslušný	příslušný	k2eAgInSc1d1
číselník	číselník	k1gInSc1
ČSÚ	ČSÚ	kA
má	mít	k5eAaImIp3nS
číslo	číslo	k1gNnSc4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přiřazuje	přiřazovat	k5eAaImIp3nS
jednoznačně	jednoznačně	k6eAd1
k	k	k7c3
názvu	název	k1gInSc3
obce	obec	k1gFnSc2
její	její	k3xOp3gFnSc1
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správcem	správce	k1gMnSc7
názvů	název	k1gInPc2
je	být	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
Správcem	správce	k1gMnSc7
číselníku	číselník	k1gInSc2
0028	#num#	k4
je	být	k5eAaImIp3nS
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
číselníku	číselník	k1gInSc6
se	s	k7c7
PSČ	PSČ	kA
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
pětimístné	pětimístný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
bez	bez	k7c2
mezery	mezera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
je	být	k5eAaImIp3nS
psaní	psaní	k1gNnSc4
čísel	číslo	k1gNnPc2
a	a	k8xC
kódů	kód	k1gInPc2
v	v	k7c6
adresách	adresa	k1gFnPc6
sjednoceno	sjednotit	k5eAaPmNgNnS
ve	v	k7c6
společných	společný	k2eAgNnPc6d1
pravidlech	pravidlo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
tato	tento	k3xDgNnPc1
pravidla	pravidlo	k1gNnPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
"	"	kIx"
<g/>
Česko	Česko	k1gNnSc1
|	|	kIx~
5	#num#	k4
číslic	číslice	k1gFnPc2
|	|	kIx~
Mezi	mezi	k7c7
třetí	třetí	k4xOgFnSc7
a	a	k8xC
čtvrtou	čtvrtý	k4xOgFnSc7
číslicí	číslice	k1gFnSc7
je	být	k5eAaImIp3nS
mezera	mezera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
poštovním	poštovní	k2eAgNnSc7d1
směrovacím	směrovací	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
a	a	k8xC
názvem	název	k1gInSc7
města	město	k1gNnSc2
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
mezery	mezera	k1gFnPc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
PSČ	PSČ	kA
v	v	k7c6
samostatném	samostatný	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
PSČ	PSČ	kA
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
navrhla	navrhnout	k5eAaPmAgFnS
Ministerstvu	ministerstvo	k1gNnSc3
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
pošt	pošta	k1gFnPc2
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
SR	SR	kA
návrh	návrh	k1gInSc1
změnit	změnit	k5eAaPmF
PSČ	PSČ	kA
a	a	k8xC
využít	využít	k5eAaPmF
i	i	k9
čísla	číslo	k1gNnPc4
s	s	k7c7
počátečními	počáteční	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
1	#num#	k4
až	až	k6eAd1
7	#num#	k4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
směrovací	směrovací	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
byla	být	k5eAaImAgNnP
přidělována	přidělovat	k5eAaImNgNnP
až	až	k6eAd1
do	do	k7c2
úrovně	úroveň	k1gFnSc2
doručovacích	doručovací	k2eAgInPc2d1
rajónů	rajón	k1gInPc2
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
dodávacích	dodávací	k2eAgFnPc2d1
pošt	pošta	k1gFnPc2
jako	jako	k8xC,k8xS
dosud	dosud	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
uskutečněna	uskutečnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Psaní	psaní	k1gNnSc1
PSČ	PSČ	kA
v	v	k7c6
adrese	adresa	k1gFnSc6
</s>
<s>
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
v	v	k7c6
adrese	adresa	k1gFnSc6
společně	společně	k6eAd1
s	s	k7c7
názvem	název	k1gInSc7
dodávací	dodávací	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
ten	ten	k3xDgInSc1
zpravidla	zpravidla	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
názvu	název	k1gInSc3
obce	obec	k1gFnSc2
a	a	k8xC
číselnému	číselný	k2eAgNnSc3d1
značení	značení	k1gNnSc3
pošty	pošta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Novák	Novák	k1gMnSc1
Karel	Karel	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
</s>
<s>
Bohumínská	bohumínský	k2eAgFnSc1d1
125	#num#	k4
Hrozenkovská	Hrozenkovský	k2eAgFnSc1d1
12	#num#	k4
</s>
<s>
735	#num#	k4
14	#num#	k4
Orlová	Orlová	k1gFnSc1
4	#num#	k4
155	#num#	k4
21	#num#	k4
Praha	Praha	k1gFnSc1
517	#num#	k4
</s>
<s>
Místo	místo	k7c2
názvu	název	k1gInSc2
dodávací	dodávací	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
se	se	k3xPyFc4
běžně	běžně	k6eAd1
používá	používat	k5eAaImIp3nS
a	a	k8xC
toleruje	tolerovat	k5eAaImIp3nS
název	název	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Orlová-Lutyně	Orlová-Lutyně	k1gFnSc1
nebo	nebo	k8xC
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
-Zličín	-Zličína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čtečky	čtečka	k1gFnPc1
třídicích	třídicí	k2eAgInPc2d1
strojů	stroj	k1gInPc2
hledají	hledat	k5eAaImIp3nP
PSČ	PSČ	kA
na	na	k7c6
začátku	začátek	k1gInSc6
posledního	poslední	k2eAgInSc2d1
řádku	řádek	k1gInSc2
adresy	adresa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraniční	zahraniční	k2eAgInSc4d1
poštovní	poštovní	k2eAgInSc4d1
kódy	kód	k1gInPc4
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
doplněny	doplnit	k5eAaPmNgInP
zkratkou	zkratka	k1gFnSc7
země	zem	k1gFnSc2
určení	určení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
země	zem	k1gFnSc2
určení	určení	k1gNnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
uveden	uvést	k5eAaPmNgMnS
samostatně	samostatně	k6eAd1
na	na	k7c6
posledním	poslední	k2eAgInSc6d1
řádku	řádek	k1gInSc6
adresy	adresa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nepoštovní	poštovní	k2eNgNnSc1d1
použití	použití	k1gNnSc1
PSČ	PSČ	kA
</s>
<s>
Předseda	předseda	k1gMnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
PČR	PČR	kA
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
usneseních	usnesení	k1gNnPc6
o	o	k7c6
jmenování	jmenování	k1gNnSc6
obcí	obec	k1gFnPc2
městy	město	k1gNnPc7
nebo	nebo	k8xC
městysi	městys	k1gInSc6
poštovní	poštovní	k2eAgNnSc4d1
směrovací	směrovací	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
spolu	spolu	k6eAd1
s	s	k7c7
názvem	název	k1gInSc7
obce	obec	k1gFnSc2
k	k	k7c3
jednoznačné	jednoznačný	k2eAgFnSc3d1
specifikaci	specifikace	k1gFnSc3
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
usnesení	usnesení	k1gNnSc1
týká	týkat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Jinak	jinak	k6eAd1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
obce	obec	k1gFnSc2
používá	používat	k5eAaImIp3nS
údaj	údaj	k1gInSc4
o	o	k7c6
okresu	okres	k1gInSc6
podle	podle	k7c2
Zákona	zákon	k1gInSc2
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
poštovních	poštovní	k2eAgFnPc6d1
adresách	adresa	k1gFnPc6
před	před	k7c7
zavedením	zavedení	k1gNnSc7
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
obce	obec	k1gFnSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
jedinečný	jedinečný	k2eAgInSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
okresu	okres	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
jednoznačný	jednoznačný	k2eAgInSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
kraje	kraj	k1gInSc2
či	či	k8xC
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PSČ	PSČ	kA
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Označení	označení	k1gNnSc1
států	stát	k1gInPc2
podle	podle	k7c2
normy	norma	k1gFnSc2
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
ZIP	zip	k1gInSc1
code	codat	k5eAaPmIp3nS
v	v	k7c6
USA	USA	kA
</s>
<s>
Pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
PSČ	PSČ	kA
se	se	k3xPyFc4
ve	v	k7c6
světě	svět	k1gInSc6
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
používá	používat	k5eAaImIp3nS
numerických	numerický	k2eAgInPc2d1
(	(	kIx(
<g/>
číselných	číselný	k2eAgInPc2d1
<g/>
)	)	kIx)
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
poštovní	poštovní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
ale	ale	k8xC
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
alfanumerických	alfanumerický	k2eAgInPc2d1
(	(	kIx(
<g/>
kombinace	kombinace	k1gFnSc1
písemných	písemný	k2eAgInPc2d1
a	a	k8xC
číselných	číselný	k2eAgInPc2d1
<g/>
)	)	kIx)
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
alfanumerických	alfanumerický	k2eAgInPc2d1
kódů	kód	k1gInPc2
oproti	oproti	k7c3
kódům	kód	k1gInPc3
numerickým	numerický	k2eAgInPc3d1
spočívá	spočívat	k5eAaImIp3nS
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
množství	množství	k1gNnSc6
kombinací	kombinace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
k	k	k7c3
přesnému	přesný	k2eAgNnSc3d1
určení	určení	k1gNnSc3
ulice	ulice	k1gFnSc2
či	či	k8xC
dokonce	dokonce	k9
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfanumerického	alfanumerický	k2eAgInSc2d1
systému	systém	k1gInSc2
směrovacích	směrovací	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
např.	např.	kA
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
či	či	k8xC
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
poštovním	poštovní	k2eAgInSc6d1
provozu	provoz	k1gInSc6
se	se	k3xPyFc4
pro	pro	k7c4
rozlišení	rozlišení	k1gNnSc4
států	stát	k1gInPc2
(	(	kIx(
<g/>
zejména	zejména	k9
používajících	používající	k2eAgInPc2d1
čtyř	čtyři	k4xCgMnPc2
či	či	k8xC
pětimístní	pětimístní	k2eAgInSc1d1
numerický	numerický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
používá	používat	k5eAaImIp3nS
předřazení	předřazení	k1gNnSc4
kódu	kód	k1gInSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
D	D	kA
pro	pro	k7c4
Německo	Německo	k1gNnSc4
či	či	k8xC
F	F	kA
pro	pro	k7c4
Francii	Francie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
se	se	k3xPyFc4
na	na	k7c4
doporučení	doporučení	k1gNnSc4
Evropského	evropský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
v	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
poštovním	poštovní	k2eAgInSc6d1
styku	styk	k1gInSc6
používá	používat	k5eAaImIp3nS
ISO	ISO	kA
3166-1	3166-1	k4
alpha-	alpha-	k?
<g/>
2	#num#	k4
(	(	kIx(
<g/>
DE	DE	k?
pro	pro	k7c4
Německo	Německo	k1gNnSc4
<g/>
,	,	kIx,
FR	fr	k0
pro	pro	k7c4
Francii	Francie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
ale	ale	k8xC
není	být	k5eNaImIp3nS
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
příliš	příliš	k6eAd1
rozšířen	rozšířit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Umístění	umístění	k1gNnSc1
PSČ	PSČ	kA
v	v	k7c6
adrese	adresa	k1gFnSc6
není	být	k5eNaImIp3nS
mezinárodně	mezinárodně	k6eAd1
kodifikováno	kodifikován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
anglofonních	anglofonní	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
tvoří	tvořit	k5eAaImIp3nP
PSČ	PSČ	kA
poslední	poslední	k2eAgInSc4d1
řádek	řádek	k1gInSc4
adresy	adresa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
zemích	zem	k1gFnPc6
kontinentální	kontinentální	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
před	před	k7c4
název	název	k1gInSc4
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
Rusku	Rusko	k1gNnSc6
či	či	k8xC
Číně	Čína	k1gFnSc6
je	být	k5eAaImIp3nS
PSČ	PSČ	kA
umístěno	umístit	k5eAaPmNgNnS
na	na	k7c6
začátku	začátek	k1gInSc6
adresy	adresa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klíčové	klíčový	k2eAgInPc1d1
pojmy	pojem	k1gInPc1
pro	pro	k7c4
poštovní	poštovní	k2eAgNnSc4d1
adresování	adresování	k1gNnSc4
<g/>
,	,	kIx,
komponenty	komponent	k1gInPc4
poštovní	poštovní	k2eAgFnSc2d1
adresy	adresa	k1gFnSc2
a	a	k8xC
omezení	omezení	k1gNnSc2
jejich	jejich	k3xOp3gNnSc2
použití	použití	k1gNnSc2
definuje	definovat	k5eAaBmIp3nS
EN	EN	kA
ISO	ISO	kA
19160	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
:	:	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
definuje	definovat	k5eAaBmIp3nS
komponenty	komponent	k1gInPc4
poštovní	poštovní	k2eAgFnSc2d1
adresy	adresa	k1gFnSc2
organizované	organizovaný	k2eAgFnSc2d1
do	do	k7c2
tří	tři	k4xCgFnPc2
hierarchických	hierarchický	k2eAgFnPc2d1
úrovní	úroveň	k1gFnPc2
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
i	i	k9
poštovní	poštovní	k2eAgInSc4d1
kód	kód	k1gInSc4
(	(	kIx(
<g/>
PSČ	PSČ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
směrovacích	směrovací	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c1
států	stát	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
ZIP	zip	k1gInSc1
code	codat	k5eAaPmIp3nS
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
PLZ	PLZ	kA
v	v	k7c6
Německu	Německo	k1gNnSc6
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc4d3
PSČ	PSČ	kA
ve	v	k7c6
světě	svět	k1gInSc6
používá	používat	k5eAaImIp3nS
Írán	Írán	k1gInSc1
(	(	kIx(
<g/>
desetimístné	desetimístný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
je	být	k5eAaImIp3nS
desetimístný	desetimístný	k2eAgInSc1d1
systém	systém	k1gInSc1
používán	používat	k5eAaImNgInS
v	v	k7c6
USA	USA	kA
pro	pro	k7c4
tzv.	tzv.	kA
ZIP	zip	k1gInSc4
<g/>
+	+	kIx~
<g/>
4	#num#	k4
System	Syst	k1gInSc7
<g/>
;	;	kIx,
ten	ten	k3xDgInSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
pětimístného	pětimístný	k2eAgInSc2d1
PSČ	PSČ	kA
<g/>
,	,	kIx,
spojovací	spojovací	k2eAgFnSc2d1
čárky	čárka	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
čtyř	čtyři	k4xCgFnPc2
číslic	číslice	k1gFnPc2
(	(	kIx(
<g/>
dohromady	dohromady	k6eAd1
tedy	tedy	k9
též	též	k9
10	#num#	k4
znaků	znak	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejkratší	krátký	k2eAgFnSc2d3
PSČ	PSČ	kA
jsou	být	k5eAaImIp3nP
třímístná	třímístný	k2eAgNnPc1d1
(	(	kIx(
<g/>
v	v	k7c6
Evropě	Evropa	k1gFnSc6
Malta	Malta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Poštovní	poštovní	k2eAgNnPc1d1
směrovací	směrovací	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
</s>
<s>
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Z	z	k7c2
celosvětového	celosvětový	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
je	být	k5eAaImIp3nS
vhodnější	vhodný	k2eAgInSc1d2
výraz	výraz	k1gInSc1
„	„	k?
<g/>
Poštovní	poštovní	k2eAgInSc1d1
kód	kód	k1gInSc1
<g/>
“	“	k?
protože	protože	k8xS
v	v	k7c6
řadě	řada	k1gFnSc6
zemí	zem	k1gFnPc2
zahrnuje	zahrnovat	k5eAaImIp3nS
číslice	číslice	k1gFnPc4
i	i	k8xC
písmena	písmeno	k1gNnPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
psáno	psát	k5eAaImNgNnS
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českých	český	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
jsou	být	k5eAaImIp3nP
výraz	výraz	k1gInSc4
„	„	k?
<g/>
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
“	“	k?
a	a	k8xC
zkratka	zkratka	k1gFnSc1
PSČ	PSČ	kA
běžné	běžný	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tyto	tento	k3xDgInPc4
stroje	stroj	k1gInPc4
již	již	k6eAd1
nejsou	být	k5eNaImIp3nP
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
nahrazeny	nahrazen	k2eAgInPc1d1
stroji	stroj	k1gInPc7
novějších	nový	k2eAgFnPc2d2
generací	generace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Poštovní	poštovní	k2eAgFnSc1d1
směrovací	směrovací	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
filatelistická	filatelistický	k2eAgFnSc1d1
a	a	k8xC
numismatická	numismatický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
:	:	kIx,
První	první	k4xOgInSc4
poštovní	poštovní	k2eAgInSc4d1
kód	kód	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
Radka	Radka	k1gFnSc1
Hrdinová	Hrdinová	k1gFnSc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
směrovací	směrovací	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
učit	učit	k5eAaImF
nechtěli	chtít	k5eNaImAgMnP
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
uváděl	uvádět	k5eAaImAgMnS
do	do	k7c2
praxe	praxe	k1gFnSc2
<g/>
,	,	kIx,
iDnes	iDnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
↑	↑	k?
Rychlejší	rychlý	k2eAgFnSc1d2
cesta	cesta	k1gFnSc1
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1972	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jak	jak	k6eAd1
psát	psát	k5eAaImF
PSČ	PSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1982	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pavel	Pavel	k1gMnSc1
Lukavský	Lukavský	k2eAgMnSc1d1
<g/>
:	:	kIx,
Znáte	znát	k5eAaImIp2nP
někdo	někdo	k3yInSc1
odpověď	odpověď	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Infofila	Infofil	k1gMnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
↑	↑	k?
Směrnice	směrnice	k1gFnSc2
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
poštovních	poštovní	k2eAgFnPc2d1
zásilek	zásilka	k1gFnPc2
podle	podle	k7c2
poštovních	poštovní	k2eAgNnPc2d1
směrovacích	směrovací	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
(	(	kIx(
<g/>
opatření	opatření	k1gNnPc2
č.	č.	k?
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
<g/>
,	,	kIx,
uveřejněné	uveřejněný	k2eAgFnSc6d1
v	v	k7c6
částce	částka	k1gFnSc6
4	#num#	k4
Věstníku	věstník	k1gInSc2
federálního	federální	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
spojů	spoj	k1gInPc2
ze	z	k7c2
dne	den	k1gInSc2
24.1	24.1	k4
<g/>
.1974	.1974	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
–	–	k?
Jak	jak	k6eAd1
správně	správně	k6eAd1
nadepsat	nadepsat	k5eAaPmF
zásilku	zásilka	k1gFnSc4
<g/>
↑	↑	k?
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc4d1
registr	registr	k1gInSc4
ČR	ČR	kA
–	–	k?
Nápověda	nápověda	k1gFnSc1
Archivováno	archivován	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
↑	↑	k?
EN	EN	kA
ISO	ISO	kA
19160	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
:	:	kIx,
2017	#num#	k4
Součásti	součást	k1gFnPc4
mezinárodní	mezinárodní	k2eAgFnSc2d1
poštovní	poštovní	k2eAgFnSc2d1
adresy	adresa	k1gFnSc2
a	a	k8xC
jazyk	jazyk	k1gInSc1
šablony	šablona	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
UPU	UPU	kA
<g/>
:	:	kIx,
Addressing	Addressing	k1gInSc1
issues	issues	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
část	část	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
www.upu.int	www.upu.int	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Federální	federální	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
spojů	spoj	k1gInPc2
<g/>
:	:	kIx,
Poštovní	poštovní	k2eAgFnPc4d1
směrovací	směrovací	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
dopravy	doprava	k1gFnSc2
a	a	k8xC
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
,	,	kIx,
schváleno	schválit	k5eAaPmNgNnS
FMS	FMS	kA
pod	pod	k7c7
č.	č.	k?
j.	j.	k?
2726	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
-PN	-PN	k?
</s>
<s>
Federální	federální	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
spojů	spoj	k1gInPc2
<g/>
:	:	kIx,
Poštovní	poštovní	k2eAgFnPc4d1
směrovací	směrovací	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
dopravy	doprava	k1gFnSc2
a	a	k8xC
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
schváleno	schválit	k5eAaPmNgNnS
FMS	FMS	kA
pod	pod	k7c7
č.	č.	k?
j.	j.	k?
14	#num#	k4
506	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
-PN	-PN	k?
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
a	a	k8xC
další	další	k2eAgFnPc1d1
příručky	příručka	k1gFnPc1
a	a	k8xC
seznamy	seznam	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
poštovní	poštovní	k2eAgFnSc2d1
směrovací	směrovací	k2eAgFnSc2d1
číslo	číslo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Wikidata	Wikide	k1gNnPc1
používají	používat	k5eAaImIp3nP
Poštovní	poštovní	k2eAgFnSc7d1
směrovací	směrovací	k2eAgFnSc7d1
číslo	číslo	k1gNnSc4
jako	jako	k8xS,k8xC
vlastnost	vlastnost	k1gFnSc4
P281	P281	k1gFnSc2
(	(	kIx(
<g/>
použití	použití	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Definice	definice	k1gFnSc1
PSČ	PSČ	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
ČSÚ	ČSÚ	kA
</s>
<s>
Vyhledávání	vyhledávání	k1gNnSc1
PSČ	PSČ	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
České	český	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
</s>
<s>
Vyhledávání	vyhledávání	k1gNnSc1
PSČ	PSČ	kA
-	-	kIx~
alternativní	alternativní	k2eAgFnSc2d1
databáze	databáze	k1gFnSc2
</s>
<s>
Databáze	databáze	k1gFnSc1
PSČ	PSČ	kA
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
-	-	kIx~
přehled	přehled	k1gInSc1
PSČ	PSČ	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Postal	Postal	k1gMnSc1
Addressing	Addressing	k1gInSc4
Systems	Systems	k1gInSc1
<g/>
:	:	kIx,
CZE	CZE	kA
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Vzory	vzor	k1gInPc1
správného	správný	k2eAgNnSc2d1
psaní	psaní	k1gNnSc2
adres	adresa	k1gFnPc2
</s>
<s>
Úprava	úprava	k1gFnSc1
adresní	adresní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
zásilky	zásilka	k1gFnSc2
(	(	kIx(
<g/>
poštovní	poštovní	k2eAgFnSc1d1
norma	norma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
PSČ	PSČ	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
Deustschepost	Deustschepost	k1gInSc1
</s>
<s>
Historie	historie	k1gFnSc1
zavádění	zavádění	k1gNnSc2
PSČ	PSČ	kA
v	v	k7c6
pořadu	pořad	k1gInSc6
ČT	ČT	kA
Retro	retro	k1gNnSc2
na	na	k7c4
iVysílání	iVysílání	k1gNnSc4
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
od	od	k7c2
21	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
124483	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4316292-7	4316292-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
99004086	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
99004086	#num#	k4
</s>
