<s>
Poštovní	poštovní	k2eAgNnSc1d1
směrovací	směrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
PSČ	PSČ	kA
(	(	kIx(
<g/>
též	též	k9
poštovní	poštovní	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
číselným	číselný	k2eAgNnSc7d1
označením	označení	k1gNnSc7
územního	územní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
adresní	adresní	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
každý	každý	k3xTgInSc4
územní	územní	k2eAgInSc4d1
obvod	obvod	k1gInSc4
je	být	k5eAaImIp3nS
PSČ	PSČ	kA
jedinečné	jedinečný	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>