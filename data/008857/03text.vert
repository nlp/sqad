<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
barev	barva	k1gFnPc2	barva
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
délek	délka	k1gFnPc2	délka
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nepřetržitě	přetržitě	k6eNd1	přetržitě
je	být	k5eAaImIp3nS	být
vlajkou	vlajka	k1gFnSc7	vlajka
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
(	(	kIx(	(
<g/>
služební	služební	k2eAgFnSc1d1	služební
<g/>
)	)	kIx)	)
vlajka	vlajka	k1gFnSc1	vlajka
navíc	navíc	k6eAd1	navíc
nese	nést	k5eAaImIp3nS	nést
štít	štít	k1gInSc1	štít
s	s	k7c7	s
upraveným	upravený	k2eAgInSc7d1	upravený
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
soukromé	soukromý	k2eAgNnSc1d1	soukromé
užití	užití	k1gNnSc1	užití
ani	ani	k8xC	ani
užití	užitý	k2eAgMnPc1d1	užitý
obcemi	obec	k1gFnPc7	obec
či	či	k8xC	či
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
není	být	k5eNaImIp3nS	být
přípustné	přípustný	k2eAgNnSc1d1	přípustné
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
neoficiálně	neoficiálně	k6eAd1	neoficiálně
tolerováno	tolerovat	k5eAaImNgNnS	tolerovat
i	i	k9	i
užití	užití	k1gNnSc3	užití
vlajky	vlajka	k1gFnSc2	vlajka
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Černo-červeno-zlatá	Černo-červenolatý	k2eAgFnSc1d1	Černo-červeno-zlatý
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
užíval	užívat	k5eAaImAgInS	užívat
dobrovolnický	dobrovolnický	k2eAgInSc1d1	dobrovolnický
sbor	sbor	k1gInSc1	sbor
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Adolfa	Adolf	k1gMnSc2	Adolf
Wilhelma	Wilhelma	k1gFnSc1	Wilhelma
von	von	k1gInSc1	von
Lützow	Lützow	k1gFnSc1	Lützow
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
německého	německý	k2eAgInSc2d1	německý
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
barvy	barva	k1gFnPc1	barva
byly	být	k5eAaImAgFnP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
říšského	říšský	k2eAgInSc2d1	říšský
znaku	znak	k1gInSc2	znak
–	–	k?	–
černého	černý	k2eAgMnSc4d1	černý
orla	orel	k1gMnSc4	orel
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
zbrojí	zbroj	k1gFnSc7	zbroj
na	na	k7c6	na
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
po	po	k7c6	po
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
německého	německý	k2eAgInSc2d1	německý
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
ji	on	k3xPp3gFnSc4	on
frankfurtský	frankfurtský	k2eAgInSc1d1	frankfurtský
sněm	sněm	k1gInSc1	sněm
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
za	za	k7c4	za
oficiální	oficiální	k2eAgFnSc4d1	oficiální
vlajku	vlajka	k1gFnSc4	vlajka
německého	německý	k2eAgInSc2d1	německý
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
vyhrané	vyhraný	k2eAgFnSc6d1	vyhraná
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
spojil	spojit	k5eAaPmAgInS	spojit
Severoněmecký	severoněmecký	k2eAgInSc1d1	severoněmecký
spolek	spolek	k1gInSc1	spolek
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
německými	německý	k2eAgInPc7d1	německý
státy	stát	k1gInPc7	stát
do	do	k7c2	do
jednotného	jednotný	k2eAgNnSc2d1	jednotné
německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
pruské	pruský	k2eAgFnSc2d1	pruská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
tohoto	tento	k3xDgInSc2	tento
útvaru	útvar	k1gInSc2	útvar
černo-bílo-červená	černoílo-červený	k2eAgFnSc1d1	černo-bílo-červená
trikolóra	trikolóra	k1gFnSc1	trikolóra
používaná	používaný	k2eAgFnSc1d1	používaná
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
Severoněmeckým	severoněmecký	k2eAgInSc7d1	severoněmecký
spolkem	spolek	k1gInSc7	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
byly	být	k5eAaImAgFnP	být
barvami	barva	k1gFnPc7	barva
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
červená	červené	k1gNnPc1	červené
a	a	k8xC	a
bílá	bílé	k1gNnPc1	bílé
zastupovaly	zastupovat	k5eAaImAgFnP	zastupovat
hanzovní	hanzovní	k2eAgFnPc4d1	hanzovní
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1919	[number]	k4	1919
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vztyčovat	vztyčovat	k5eAaImF	vztyčovat
opět	opět	k6eAd1	opět
vlajky	vlajka	k1gFnPc4	vlajka
černo-červeno-zlaté	černo-červenolatý	k2eAgFnPc4d1	černo-červeno-zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1933	[number]	k4	1933
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
císařské	císařský	k2eAgFnSc3d1	císařská
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vztyčovat	vztyčovat	k5eAaImF	vztyčovat
vlajka	vlajka	k1gFnSc1	vlajka
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1935	[number]	k4	1935
jedinou	jediný	k2eAgFnSc7d1	jediná
německou	německý	k2eAgFnSc7d1	německá
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
SRN	SRN	kA	SRN
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgInP	vrátit
k	k	k7c3	k
barvám	barva	k1gFnPc3	barva
černo-červeno-zlaté	černo-červenolatý	k2eAgFnSc2d1	černo-červeno-zlatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NDR	NDR	kA	NDR
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
vložen	vložen	k2eAgInSc4d1	vložen
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
černo-červeno-zlatá	černo-červenolatý	k2eAgFnSc1d1	černo-červeno-zlatý
trikolóra	trikolóra	k1gFnSc1	trikolóra
opět	opět	k6eAd1	opět
vlajkou	vlajka	k1gFnSc7	vlajka
celého	celý	k2eAgNnSc2d1	celé
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnPc1d1	historická
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Německa	Německo	k1gNnSc2	Německo
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Německa	Německo	k1gNnSc2	Německo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Německá	německý	k2eAgFnSc1d1	německá
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
