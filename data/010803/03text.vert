<p>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
Zálešák	Zálešák	k1gMnSc1	Zálešák
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
Kuželov	Kuželov	k1gInSc1	Kuželov
−	−	k?	−
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
choreograf	choreograf	k1gMnSc1	choreograf
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
folklorista	folklorista	k1gMnSc1	folklorista
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumal	zkoumat	k5eAaImAgInS	zkoumat
slovenské	slovenský	k2eAgInPc4d1	slovenský
lidové	lidový	k2eAgInPc4d1	lidový
tance	tanec	k1gInPc4	tanec
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
metodických	metodický	k2eAgFnPc2d1	metodická
příruček	příručka	k1gFnPc2	příručka
a	a	k8xC	a
sborníků	sborník	k1gInPc2	sborník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	díl	k1gInSc6	díl
Opisovanie	Opisovanie	k1gFnSc2	Opisovanie
ľudových	ľudových	k2eAgInSc4d1	ľudových
tancov	tancov	k1gInSc4	tancov
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
systém	systém	k1gInSc1	systém
názvosloví	názvosloví	k1gNnSc2	názvosloví
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
choreograf	choreograf	k1gMnSc1	choreograf
a	a	k8xC	a
vedoucí	vedoucí	k1gMnSc1	vedoucí
působil	působit	k5eAaImAgMnS	působit
25	[number]	k4	25
let	let	k1gInSc4	let
ve	v	k7c6	v
folklorním	folklorní	k2eAgInSc6d1	folklorní
souboru	soubor	k1gInSc6	soubor
Technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Režíroval	režírovat	k5eAaImAgMnS	režírovat
několik	několik	k4yIc4	několik
scénických	scénický	k2eAgInPc2d1	scénický
programů	program	k1gInPc2	program
na	na	k7c6	na
folklorních	folklorní	k2eAgInPc6d1	folklorní
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Folklór	folklór	k1gInSc1	folklór
na	na	k7c4	na
scéne	scénout	k5eAaImIp3nS	scénout
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
Folklórne	Folklórn	k1gInSc5	Folklórn
hnutie	hnutie	k1gFnSc1	hnutie
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
:	:	kIx,	:
od	od	k7c2	od
oslobodenia	oslobodenium	k1gNnSc2	oslobodenium
do	do	k7c2	do
súčasnosti	súčasnost	k1gFnSc2	súčasnost
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
–	–	k?	–
Motívy	Motív	k1gInPc1	Motív
a	a	k8xC	a
zostavy	zostav	k1gInPc1	zostav
pre	pre	k?	pre
nácvik	nácvik	k1gInSc4	nácvik
slovenských	slovenský	k2eAgNnPc2d1	slovenské
ľudových	ľudův	k2eAgNnPc2d1	ľudův
tancov	tancovo	k1gNnPc2	tancovo
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
Prehľad	Prehľad	k1gInSc1	Prehľad
slovenských	slovenský	k2eAgInPc2d1	slovenský
ľudových	ľudových	k2eAgInSc4d1	ľudových
tancov	tancov	k1gInSc4	tancov
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
Tradičné	tradičný	k2eAgInPc1d1	tradičný
zvyky	zvyk	k1gInPc1	zvyk
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
tancami	tanca	k1gFnPc7	tanca
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
–	–	k?	–
Ľudové	Ľudové	k2eAgInSc2d1	Ľudové
tance	tanec	k1gInSc2	tanec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
–	–	k?	–
Rytmika	rytmik	k1gMnSc2	rytmik
a	a	k8xC	a
ľudové	ľudové	k2eAgInPc4d1	ľudové
tance	tanec	k1gInPc4	tanec
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
–	–	k?	–
Opisovanie	Opisovanie	k1gFnSc2	Opisovanie
ľudových	ľudův	k2eAgNnPc2d1	ľudův
tancov	tancovo	k1gNnPc2	tancovo
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
–	–	k?	–
Pohronské	pohronský	k2eAgInPc4d1	pohronský
tance	tanec	k1gInPc4	tanec
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
–	–	k?	–
Horňácky	horňácky	k6eAd1	horňácky
odzemok	odzemok	k1gInSc1	odzemok
a	a	k8xC	a
verbunk	verbunk	k1gInSc1	verbunk
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cyril	Cyril	k1gMnSc1	Cyril
Zálešák	Zálešák	k1gMnSc1	Zálešák
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
