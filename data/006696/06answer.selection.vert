<s>
Robert	Robert	k1gMnSc1	Robert
Hooke	Hook	k1gInSc2	Hook
objevil	objevit	k5eAaPmAgMnS	objevit
rostlinné	rostlinný	k2eAgFnPc4d1	rostlinná
buňky	buňka	k1gFnPc4	buňka
v	v	k7c6	v
korku	korek	k1gInSc6	korek
<g/>
,	,	kIx,	,
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
Carl	Carl	k1gMnSc1	Carl
von	von	k1gInSc4	von
Linné	Linná	k1gFnSc2	Linná
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
rostliny	rostlina	k1gFnPc4	rostlina
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
Systema	Systema	k1gFnSc1	Systema
naturae	naturaat	k5eAaPmIp3nS	naturaat
na	na	k7c4	na
25	[number]	k4	25
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
