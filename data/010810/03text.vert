<p>
<s>
Golgiho	Golgize	k6eAd1	Golgize
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
biochemická	biochemický	k2eAgFnSc1d1	biochemická
metoda	metoda	k1gFnSc1	metoda
barvení	barvení	k1gNnSc2	barvení
preparátu	preparát	k1gInSc2	preparát
nervové	nervový	k2eAgFnSc2d1	nervová
tkáně	tkáň	k1gFnSc2	tkáň
za	za	k7c4	za
užití	užití	k1gNnSc4	užití
dichromanu	dichroman	k1gMnSc3	dichroman
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k1gInSc2	stříbrný
<g/>
.	.	kIx.	.
</s>
<s>
Metodu	metoda	k1gFnSc4	metoda
barvení	barvení	k1gNnSc2	barvení
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
pozdější	pozdní	k2eAgFnSc4d2	pozdější
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Camillo	Camillo	k1gNnSc4	Camillo
Golgi	Golgi	k1gNnSc2	Golgi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postup	postup	k1gInSc4	postup
==	==	k?	==
</s>
</p>
<p>
<s>
Ponořit	ponořit	k5eAaPmF	ponořit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
nervovou	nervový	k2eAgFnSc4d1	nervová
tkáň	tkáň	k1gFnSc4	tkáň
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
cca	cca	kA	cca
10	[number]	k4	10
<g/>
x	x	k?	x
<g/>
5	[number]	k4	5
mm	mm	kA	mm
fixovanou	fixovaný	k2eAgFnSc4d1	fixovaná
formaldehydem	formaldehyd	k1gInSc7	formaldehyd
nebo	nebo	k8xC	nebo
paraformaldehydem	paraformaldehyd	k1gInSc7	paraformaldehyd
napuštěnou	napuštěný	k2eAgFnSc4d1	napuštěná
glutaraldehydem	glutaraldehyd	k1gInSc7	glutaraldehyd
do	do	k7c2	do
2	[number]	k4	2
<g/>
%	%	kIx~	%
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
Cr	cr	k0	cr
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
</s>
</p>
<p>
<s>
Vysušit	vysušit	k5eAaPmF	vysušit
rychle	rychle	k6eAd1	rychle
tkáň	tkáň	k1gFnSc4	tkáň
filtračním	filtrační	k2eAgInSc7d1	filtrační
papírem	papír	k1gInSc7	papír
</s>
</p>
<p>
<s>
Ponořit	ponořit	k5eAaPmF	ponořit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
nervovou	nervový	k2eAgFnSc4d1	nervová
tká	tkát	k5eAaImIp3nS	tkát
do	do	k7c2	do
2	[number]	k4	2
<g/>
%	%	kIx~	%
vodného	vodné	k1gNnSc2	vodné
roztoku	roztok	k1gInSc2	roztok
AgNO	AgNO	k1gFnSc2	AgNO
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
Nařezat	nařezat	k5eAaPmF	nařezat
řezy	řez	k1gInPc4	řez
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
μ	μ	k?	μ
tenké	tenký	k2eAgFnSc6d1	tenká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysušit	vysušit	k5eAaPmF	vysušit
rychle	rychle	k6eAd1	rychle
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
,	,	kIx,	,
vymýt	vymýt	k5eAaPmF	vymýt
a	a	k8xC	a
zafixovat	zafixovat	k5eAaPmF	zafixovat
Depexem	Depex	k1gInSc7	Depex
nebo	nebo	k8xC	nebo
Enthalanem	Enthalan	k1gInSc7	Enthalan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
