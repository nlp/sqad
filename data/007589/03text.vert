<s>
Nirvana	Nirvan	k1gMnSc4	Nirvan
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
grungeová	grungeový	k2eAgFnSc1d1	grungeová
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
v	v	k7c6	v
Aberdeenu	Aberdeen	k1gInSc6	Aberdeen
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvlivnější	vlivný	k2eAgFnPc4d3	nejvlivnější
hudební	hudební	k2eAgFnPc4d1	hudební
skupiny	skupina	k1gFnPc4	skupina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zakládajícími	zakládající	k2eAgMnPc7d1	zakládající
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Krist	Krista	k1gFnPc2	Krista
Novoselic	Novoselice	k1gFnPc2	Novoselice
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bubeníka	bubeník	k1gMnSc2	bubeník
nakonec	nakonec	k6eAd1	nakonec
zakotvil	zakotvit	k5eAaPmAgMnS	zakotvit
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gMnSc6	Grohl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgMnSc1d1	poslední
v	v	k7c6	v
řadě	řad	k1gInSc6	řad
několika	několik	k4yIc2	několik
bubeníků	bubeník	k1gMnPc2	bubeník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Nirvaně	Nirvaně	k1gFnSc6	Nirvaně
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
třetí	třetí	k4xOgMnSc1	třetí
kytarista	kytarista	k1gMnSc1	kytarista
byl	být	k5eAaImAgMnS	být
přijat	přijat	k2eAgInSc4d1	přijat
Pat	pat	k1gInSc4	pat
Smear	Smeara	k1gFnPc2	Smeara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Nirvana	Nirvan	k1gMnSc4	Nirvan
nahrála	nahrát	k5eAaBmAgFnS	nahrát
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bleach	Bleacha	k1gFnPc2	Bleacha
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stále	stále	k6eAd1	stále
nepatřila	patřit	k5eNaImAgFnS	patřit
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
proudu	proud	k1gInSc3	proud
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
vyšel	vyjít	k5eAaPmAgInS	vyjít
její	její	k3xOp3gInSc4	její
nový	nový	k2eAgInSc4d1	nový
singl	singl	k1gInSc4	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Like	k1gNnSc1	Like
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirita	k1gFnPc2	Spirita
<g/>
"	"	kIx"	"
k	k	k7c3	k
albu	album	k1gNnSc3	album
Nevermind	Neverminda	k1gFnPc2	Neverminda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
první	první	k4xOgFnPc4	první
příčky	příčka	k1gFnPc4	příčka
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
a	a	k8xC	a
otevřela	otevřít	k5eAaPmAgFnS	otevřít
Nirvaně	Nirvaně	k1gFnSc1	Nirvaně
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
slávě	sláva	k1gFnSc3	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgNnSc1d1	poslední
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
In	In	k1gFnSc2	In
Utero	Utero	k1gNnSc1	Utero
<g/>
;	;	kIx,	;
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
později	pozdě	k6eAd2	pozdě
populární	populární	k2eAgFnSc2d1	populární
skladby	skladba	k1gFnSc2	skladba
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Rape	rape	k1gNnSc1	rape
Me	Me	k1gFnSc2	Me
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Heart-Shaped	Heart-Shaped	k1gInSc1	Heart-Shaped
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nirvana	Nirvan	k1gMnSc2	Nirvan
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgNnP	rozpadnout
po	po	k7c6	po
sebevraždě	sebevražda	k1gFnSc6	sebevražda
frontmana	frontman	k1gMnSc2	frontman
skupiny	skupina	k1gFnSc2	skupina
Kurta	Kurt	k1gMnSc2	Kurt
Cobaina	Cobain	k1gMnSc2	Cobain
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
prodala	prodat	k5eAaPmAgFnS	prodat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Hudebnímu	hudební	k2eAgInSc3d1	hudební
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Nirvana	Nirvan	k1gMnSc2	Nirvan
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
grunge	grunge	k6eAd1	grunge
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
hudbu	hudba	k1gFnSc4	hudba
reprezentovaly	reprezentovat	k5eAaImAgInP	reprezentovat
a	a	k8xC	a
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
například	například	k6eAd1	například
kapely	kapela	k1gFnPc1	kapela
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Chains	Chains	k1gInSc1	Chains
<g/>
,	,	kIx,	,
Mudhoney	Mudhoney	k1gInPc1	Mudhoney
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gInSc1	Pearl
Jam	jáma	k1gFnPc2	jáma
a	a	k8xC	a
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
kapely	kapela	k1gFnSc2	kapela
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Buzze	Buzze	k1gFnSc2	Buzze
Osborna	Osborno	k1gNnSc2	Osborno
<g/>
,	,	kIx,	,
kytaristy	kytarista	k1gMnSc2	kytarista
a	a	k8xC	a
zpěváka	zpěvák	k1gMnSc2	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Melvins	Melvinsa	k1gFnPc2	Melvinsa
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
setkali	setkat	k5eAaPmAgMnP	setkat
zakládající	zakládající	k2eAgMnPc1d1	zakládající
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
a	a	k8xC	a
Krist	Krista	k1gFnPc2	Krista
Novoselic	Novoselice	k1gFnPc2	Novoselice
<g/>
.	.	kIx.	.
</s>
<s>
Cobain	Cobain	k1gMnSc1	Cobain
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgMnS	zpívat
v	v	k7c6	v
punkové	punkový	k2eAgFnSc6d1	punková
skupině	skupina	k1gFnSc6	skupina
Fecal	Fecal	k1gMnSc1	Fecal
Matter	Matter	k1gMnSc1	Matter
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgInS	založit
tentýž	týž	k3xTgInSc1	týž
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
Novoselic	Novoselice	k1gFnPc2	Novoselice
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Fecal	Fecal	k1gMnSc1	Fecal
Matter	Matter	k1gMnSc1	Matter
Cobain	Cobain	k1gMnSc1	Cobain
s	s	k7c7	s
Novoselicem	Novoselic	k1gMnSc7	Novoselic
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
nové	nový	k2eAgFnSc2d1	nová
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
najít	najít	k5eAaPmF	najít
bubeníka	bubeník	k1gMnSc4	bubeník
Aarona	Aaron	k1gMnSc4	Aaron
Burckharda	Burckhard	k1gMnSc4	Burckhard
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
nacvičili	nacvičit	k5eAaBmAgMnP	nacvičit
některé	některý	k3yIgFnPc1	některý
ze	z	k7c2	z
skladeb	skladba	k1gFnPc2	skladba
Fecal	Fecal	k1gMnSc1	Fecal
Matter	Matter	k1gMnSc1	Matter
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Melvins	Melvinsa	k1gFnPc2	Melvinsa
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
pilného	pilný	k2eAgNnSc2d1	pilné
zkoušení	zkoušení	k1gNnSc2	zkoušení
nových	nový	k2eAgFnPc2d1	nová
skladeb	skladba	k1gFnPc2	skladba
<g/>
;	;	kIx,	;
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
jich	on	k3xPp3gMnPc2	on
měli	mít	k5eAaImAgMnP	mít
asi	asi	k9	asi
tucet	tucet	k1gInSc4	tucet
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Cobainem	Cobain	k1gInSc7	Cobain
přála	přát	k5eAaImAgFnS	přát
vystupovat	vystupovat	k5eAaImF	vystupovat
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
opravdové	opravdový	k2eAgNnSc4d1	opravdové
vystoupení	vystoupení	k1gNnSc4	vystoupení
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
v	v	k7c4	v
Raymondu	Raymonda	k1gFnSc4	Raymonda
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
domácím	domácí	k2eAgInSc6d1	domácí
večírku	večírek	k1gInSc6	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
několik	několik	k4yIc1	několik
dalších	další	k2eAgNnPc2d1	další
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
Aberdeenu	Aberdeen	k1gInSc6	Aberdeen
a	a	k8xC	a
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
byl	být	k5eAaImAgInS	být
Burckhard	Burckhard	k1gInSc1	Burckhard
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
vyhozen	vyhozen	k2eAgInSc4d1	vyhozen
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgInS	nahradit
Dale	Dale	k1gInSc1	Dale
Crover	Crovero	k1gNnPc2	Crovero
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Melvins	Melvins	k1gInSc1	Melvins
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
nahrál	nahrát	k5eAaPmAgInS	nahrát
s	s	k7c7	s
Nirvanou	Nirvaná	k1gFnSc7	Nirvaná
v	v	k7c6	v
seattleském	seattleský	k2eAgNnSc6d1	seattleský
studiu	studio	k1gNnSc6	studio
Reciprocal	Reciprocal	k1gFnSc2	Reciprocal
Recording	Recording	k1gInSc4	Recording
první	první	k4xOgFnSc2	první
demo	demo	k2eAgFnSc2d1	demo
nahrávky	nahrávka	k1gFnSc2	nahrávka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
albu	album	k1gNnSc6	album
Incesticide	Incesticid	k1gInSc5	Incesticid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
dema	dem	k1gInSc2	dem
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetím	třetí	k4xOgNnSc6	třetí
bubeníkem	bubeník	k1gMnSc7	bubeník
Dave	Dav	k1gInSc5	Dav
Foster	Foster	k1gMnSc1	Foster
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
měsíců	měsíc	k1gInPc2	měsíc
Nirvana	Nirvan	k1gMnSc2	Nirvan
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
množství	množství	k1gNnSc4	množství
jmen	jméno	k1gNnPc2	jméno
jako	jako	k8xC	jako
Skid	Skid	k1gMnSc1	Skid
Row	Row	k1gMnSc1	Row
<g/>
,	,	kIx,	,
Pen	Pen	k1gMnSc1	Pen
Cap	cap	k1gMnSc1	cap
Chew	Chew	k1gMnSc1	Chew
a	a	k8xC	a
Ted	Ted	k1gMnSc1	Ted
Ed	Ed	k1gMnSc1	Ed
Fred	Fred	k1gMnSc1	Fred
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Nirvana	Nirvan	k1gMnSc2	Nirvan
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1987	[number]	k4	1987
a	a	k8xC	a
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nirvána	nirvána	k1gFnSc1	nirvána
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
východních	východní	k2eAgNnPc2d1	východní
náboženství	náboženství	k1gNnPc2	náboženství
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
buddhismu	buddhismus	k1gInSc2	buddhismus
a	a	k8xC	a
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
"	"	kIx"	"
<g/>
osvobození	osvobození	k1gNnSc1	osvobození
<g/>
"	"	kIx"	"
bytostí	bytost	k1gFnPc2	bytost
z	z	k7c2	z
hmotného	hmotný	k2eAgInSc2d1	hmotný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
Nirvany	Nirvan	k1gMnPc7	Nirvan
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Buzz	Buzz	k1gInSc4	Buzz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
skladba	skladba	k1gFnSc1	skladba
holandské	holandský	k2eAgFnSc2d1	holandská
kapely	kapela	k1gFnSc2	kapela
Shocking	Shocking	k1gInSc4	Shocking
Blue	Blu	k1gFnSc2	Blu
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nahrán	nahrát	k5eAaBmNgMnS	nahrát
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
u	u	k7c2	u
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
hudebního	hudební	k2eAgNnSc2d1	hudební
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Sub	sub	k7c4	sub
Pop	pop	k1gInSc4	pop
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
<g/>
–	–	k?	–
<g/>
stranu	strana	k1gFnSc4	strana
singlu	singl	k1gInSc2	singl
tvořila	tvořit	k5eAaImAgFnS	tvořit
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc1	Big
Cheese	Cheese	k1gFnSc2	Cheese
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
pouhých	pouhý	k2eAgInPc2d1	pouhý
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dnes	dnes	k6eAd1	dnes
činí	činit	k5eAaImIp3nS	činit
vzácný	vzácný	k2eAgInSc4d1	vzácný
kus	kus	k1gInSc4	kus
pro	pro	k7c4	pro
sběratele	sběratel	k1gMnPc4	sběratel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
singlu	singl	k1gInSc2	singl
se	se	k3xPyFc4	se
Cobain	Cobain	k1gMnSc1	Cobain
definitivně	definitivně	k6eAd1	definitivně
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nirvana	Nirvan	k1gMnSc2	Nirvan
nebude	být	k5eNaImBp3nS	být
jen	jen	k9	jen
jeho	jeho	k3xOp3gInSc7	jeho
koníčkem	koníček	k1gInSc7	koníček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
vydat	vydat	k5eAaPmF	vydat
na	na	k7c4	na
hudební	hudební	k2eAgFnSc4d1	hudební
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vydat	vydat	k5eAaPmF	vydat
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
kapela	kapela	k1gFnSc1	kapela
poprvé	poprvé	k6eAd1	poprvé
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
bubeníkem	bubeník	k1gMnSc7	bubeník
Chadem	Chad	k1gMnSc7	Chad
Channingenem	Channingen	k1gInSc7	Channingen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
alba	album	k1gNnSc2	album
Nirvana	Nirvan	k1gMnSc2	Nirvan
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
sehnat	sehnat	k5eAaPmF	sehnat
poplatek	poplatek	k1gInSc4	poplatek
606,17	[number]	k4	606,17
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
takovou	takový	k3xDgFnSc4	takový
částku	částka	k1gFnSc4	částka
neměl	mít	k5eNaImAgMnS	mít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
požádali	požádat	k5eAaPmAgMnP	požádat
společného	společný	k2eAgMnSc4d1	společný
přítele	přítel	k1gMnSc4	přítel
Jasona	Jason	k1gMnSc4	Jason
Evermana	Everman	k1gMnSc4	Everman
o	o	k7c4	o
zapůjčení	zapůjčení	k1gNnSc4	zapůjčení
obnosu	obnos	k1gInSc2	obnos
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
Everman	Everman	k1gMnSc1	Everman
poslední	poslední	k2eAgFnSc6d1	poslední
dobou	doba	k1gFnSc7	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc4	peníz
měl	mít	k5eAaImAgInS	mít
a	a	k8xC	a
ochotně	ochotně	k6eAd1	ochotně
je	být	k5eAaImIp3nS	být
kapele	kapela	k1gFnSc3	kapela
zapůjčil	zapůjčit	k5eAaPmAgMnS	zapůjčit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Bleach	Bleacha	k1gFnPc2	Bleacha
<g/>
,	,	kIx,	,
Everman	Everman	k1gMnSc1	Everman
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
sice	sice	k8xC	sice
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgMnS	uvést
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
na	na	k7c6	na
albu	album	k1gNnSc6	album
nehrál	hrát	k5eNaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Krist	Krista	k1gFnPc2	Krista
Novoselic	Novoselice	k1gFnPc2	Novoselice
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Prostě	prostě	k6eAd1	prostě
jsme	být	k5eAaImIp1nP	být
chtěli	chtít	k5eAaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
cítil	cítit	k5eAaImAgMnS	cítit
víc	hodně	k6eAd2	hodně
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bleach	Bleach	k1gInSc1	Bleach
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vydáno	vydat	k5eAaPmNgNnS	vydat
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
turné	turné	k1gNnSc4	turné
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
následovalo	následovat	k5eAaImAgNnS	následovat
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
Everman	Everman	k1gMnSc1	Everman
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
odešel	odejít	k5eAaPmAgInS	odejít
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
hrál	hrát	k5eAaImAgMnS	hrát
se	s	k7c7	s
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Každá	každý	k3xTgFnSc1	každý
kapela	kapela	k1gFnSc1	kapela
funguje	fungovat	k5eAaImIp3nS	fungovat
trochu	trochu	k6eAd1	trochu
jako	jako	k9	jako
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
připadal	připadat	k5eAaPmAgMnS	připadat
jako	jako	k8xS	jako
mentálně	mentálně	k6eAd1	mentálně
retardovaný	retardovaný	k2eAgInSc1d1	retardovaný
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
odložený	odložený	k2eAgMnSc1d1	odložený
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Alba	alba	k1gFnSc1	alba
Bleach	Bleacha	k1gFnPc2	Bleacha
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
prodalo	prodat	k5eAaPmAgNnS	prodat
35	[number]	k4	35
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
pozdější	pozdní	k2eAgFnSc2d2	pozdější
reedice	reedice	k1gFnSc2	reedice
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
jediné	jediný	k2eAgNnSc4d1	jediné
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přineslo	přinést	k5eAaPmAgNnS	přinést
nahrávací	nahrávací	k2eAgFnPc4d1	nahrávací
společnosti	společnost	k1gFnPc4	společnost
Sub	sub	k7c4	sub
Pop	pop	k1gInSc4	pop
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
Nirvana	Nirvan	k1gMnSc4	Nirvan
nahrála	nahrát	k5eAaBmAgFnS	nahrát
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgFnSc6	první
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
Blew	Blew	k1gMnSc2	Blew
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
producentem	producent	k1gMnSc7	producent
byl	být	k5eAaImAgMnS	být
Steve	Steve	k1gMnSc1	Steve
Fisk	Fisk	k1gMnSc1	Fisk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
TAD	TAD	kA	TAD
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
USA	USA	kA	USA
se	se	k3xPyFc4	se
Nirvana	Nirvan	k1gMnSc2	Nirvan
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
po	po	k7c6	po
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Cobain	Cobain	k1gMnSc1	Cobain
společně	společně	k6eAd1	společně
s	s	k7c7	s
Novoselicem	Novoselic	k1gMnSc7	Novoselic
však	však	k9	však
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
nespokojení	nespokojení	k1gNnPc4	nespokojení
s	s	k7c7	s
Channinganem	Channingan	k1gInSc7	Channingan
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
za	za	k7c4	za
něj	on	k3xPp3gNnSc4	on
hledat	hledat	k5eAaImF	hledat
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Channingan	Channingan	k1gInSc1	Channingan
naopak	naopak	k6eAd1	naopak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Dan	Dan	k1gMnSc1	Dan
Peters	Peters	k1gInSc4	Peters
z	z	k7c2	z
Mudhoney	Mudhonea	k1gFnSc2	Mudhonea
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
ještě	ještě	k6eAd1	ještě
stačili	stačit	k5eAaBmAgMnP	stačit
nahrát	nahrát	k5eAaPmF	nahrát
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Sliver	Sliver	k1gInSc4	Sliver
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
trvalé	trvalý	k2eAgNnSc4d1	trvalé
místo	místo	k1gNnSc4	místo
si	se	k3xPyFc3	se
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
až	až	k9	až
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
bubeník	bubeník	k1gMnSc1	bubeník
kapely	kapela	k1gFnSc2	kapela
Scream	Scream	k1gInSc1	Scream
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gFnPc2	Grohl
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Sliver	Sliver	k1gInSc1	Sliver
<g/>
"	"	kIx"	"
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
singl	singl	k1gInSc4	singl
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
producentem	producent	k1gMnSc7	producent
byl	být	k5eAaImAgMnS	být
tentokrát	tentokrát	k6eAd1	tentokrát
Butch	Butch	k1gMnSc1	Butch
Vig	Vig	k1gMnSc1	Vig
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
Garbage	Garbag	k1gFnSc2	Garbag
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
již	již	k6eAd1	již
měl	mít	k5eAaImAgMnS	mít
producentské	producentský	k2eAgFnSc2d1	producentská
zkušenosti	zkušenost	k1gFnSc2	zkušenost
například	například	k6eAd1	například
s	s	k7c7	s
The	The	k1gFnSc7	The
Smashing	Smashing	k1gInSc4	Smashing
Pumpkins	Pumpkinsa	k1gFnPc2	Pumpkinsa
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
skladbou	skladba	k1gFnSc7	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Nirvana	Nirvan	k1gMnSc4	Nirvan
nahrála	nahrát	k5eAaPmAgFnS	nahrát
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Sub	sub	k7c4	sub
Pop	pop	k1gInSc4	pop
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Molly	Molla	k1gFnPc1	Molla
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lips	Lipsa	k1gFnPc2	Lipsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Nirvana	Nirvan	k1gMnSc4	Nirvan
pak	pak	k6eAd1	pak
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
k	k	k7c3	k
mnohem	mnohem	k6eAd1	mnohem
většímu	veliký	k2eAgNnSc3d2	veliký
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
Geffen	Geffen	k2eAgInSc4d1	Geffen
Records	Records	k1gInSc4	Records
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
zajistit	zajistit	k5eAaPmF	zajistit
rozsáhlejší	rozsáhlý	k2eAgFnSc4d2	rozsáhlejší
distribuci	distribuce	k1gFnSc4	distribuce
alb	album	k1gNnPc2	album
a	a	k8xC	a
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Nirvana	Nirvan	k1gMnSc4	Nirvan
zůstala	zůstat	k5eAaPmAgFnS	zůstat
u	u	k7c2	u
Geffenu	Geffen	k1gInSc2	Geffen
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Novoselic	Novoselice	k1gFnPc2	Novoselice
s	s	k7c7	s
Grohlem	Grohlo	k1gNnSc7	Grohlo
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cobain	Cobain	k1gInSc1	Cobain
bere	brát	k5eAaImIp3nS	brát
heroin	heroin	k1gInSc4	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
finanční	finanční	k2eAgFnSc3d1	finanční
podpoře	podpora	k1gFnSc3	podpora
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
Geffen	Geffna	k1gFnPc2	Geffna
bylo	být	k5eAaImAgNnS	být
Nirvaně	Nirvaně	k1gFnPc3	Nirvaně
umožněno	umožnit	k5eAaPmNgNnS	umožnit
upustit	upustit	k5eAaPmF	upustit
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
se	s	k7c7	s
Sub	sub	k1gNnSc7	sub
Popem	pop	k1gMnSc7	pop
<g/>
.	.	kIx.	.
</s>
<s>
Sub	sub	k7c4	sub
Pop	pop	k1gInSc4	pop
bylo	být	k5eAaImAgNnS	být
oproti	oproti	k7c3	oproti
Geffenu	Geffena	k1gFnSc4	Geffena
malým	malý	k2eAgNnSc7d1	malé
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
a	a	k8xC	a
pokud	pokud	k6eAd1	pokud
si	se	k3xPyFc3	se
Nirvana	Nirvan	k1gMnSc2	Nirvan
přála	přát	k5eAaImAgFnS	přát
prorazit	prorazit	k5eAaPmF	prorazit
<g/>
,	,	kIx,	,
Geffen	Geffen	k1gInSc1	Geffen
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgInS	jevit
jako	jako	k8xS	jako
vhodná	vhodný	k2eAgFnSc1d1	vhodná
příležitost	příležitost	k1gFnSc1	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
Nirvana	Nirvan	k1gMnSc2	Nirvan
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Geffenem	Geffen	k1gInSc7	Geffen
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
producenta	producent	k1gMnSc2	producent
nového	nový	k2eAgNnSc2d1	nové
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
alba	album	k1gNnSc2	album
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Butch	Butch	k1gMnSc1	Butch
Vig	Vig	k1gMnSc1	Vig
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc4d1	Nové
album	album	k1gNnSc4	album
začali	začít	k5eAaPmAgMnP	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Sound	Sounda	k1gFnPc2	Sounda
City	city	k1gNnPc2	city
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Cobain	Cobain	k1gMnSc1	Cobain
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
Courtney	Courtnea	k1gFnSc2	Courtnea
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Nevermind	Neverminda	k1gFnPc2	Neverminda
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
Nirvana	Nirvan	k1gMnSc2	Nirvan
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
na	na	k7c4	na
menší	malý	k2eAgNnSc4d2	menší
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
USA	USA	kA	USA
jako	jako	k8xC	jako
předkapela	předkapela	k1gFnSc1	předkapela
Dinosaur	Dinosaura	k1gFnPc2	Dinosaura
Jr	Jr	k1gFnPc2	Jr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
Nirvana	Nirvan	k1gMnSc2	Nirvan
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Sonic	Sonic	k1gMnSc1	Sonic
Youth	Youth	k1gMnSc1	Youth
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
The	The	k1gFnPc2	The
Year	Year	k1gInSc1	Year
Punk	punk	k1gMnSc1	punk
Broke	Broke	k1gInSc1	Broke
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
vyšel	vyjít	k5eAaPmAgMnS	vyjít
Nirvaně	Nirvaně	k1gMnSc1	Nirvaně
nový	nový	k2eAgInSc1d1	nový
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Like	k1gNnSc1	Like
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirita	k1gFnPc2	Spirita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
připravit	připravit	k5eAaPmF	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
neprodával	prodávat	k5eNaImAgMnS	prodávat
moc	moc	k6eAd1	moc
dobře	dobře	k6eAd1	dobře
<g/>
;	;	kIx,	;
zlom	zlom	k1gInSc1	zlom
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
klip	klip	k1gInSc1	klip
ke	k	k7c3	k
"	"	kIx"	"
<g/>
Smells	Smellsa	k1gFnPc2	Smellsa
Like	Like	k1gInSc1	Like
Teen	Teen	k1gMnSc1	Teen
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
hudební	hudební	k2eAgFnSc1d1	hudební
stanice	stanice	k1gFnSc1	stanice
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rychle	rychle	k6eAd1	rychle
prodávat	prodávat	k5eAaImF	prodávat
a	a	k8xC	a
totéž	týž	k3xTgNnSc4	týž
platí	platit	k5eAaImIp3nS	platit
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
následovalo	následovat	k5eAaImAgNnS	následovat
14	[number]	k4	14
dní	den	k1gInPc2	den
po	po	k7c6	po
něm.	něm.	k?	něm.
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Geffen	Geffna	k1gFnPc2	Geffna
Records	Recordsa	k1gFnPc2	Recordsa
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
alba	alba	k1gFnSc1	alba
Nevermind	Neverminda	k1gFnPc2	Neverminda
prodá	prodat	k5eAaPmIp3nS	prodat
alespoň	alespoň	k9	alespoň
50	[number]	k4	50
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
alba	album	k1gNnSc2	album
Goo	Goa	k1gFnSc5	Goa
od	od	k7c2	od
Sonic	Sonice	k1gFnPc2	Sonice
Youth	Youtha	k1gFnPc2	Youtha
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kolem	kolem	k7c2	kolem
Vánoc	Vánoce	k1gFnPc2	Vánoce
se	se	k3xPyFc4	se
prodávalo	prodávat	k5eAaImAgNnS	prodávat
takřka	takřka	k6eAd1	takřka
400	[number]	k4	400
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
Nevermind	Neverminda	k1gFnPc2	Neverminda
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
jaký	jaký	k9	jaký
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
trvalý	trvalý	k2eAgInSc4d1	trvalý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Cobain	Cobain	k1gInSc1	Cobain
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgInS	začít
stěžovat	stěžovat	k5eAaImF	stěžovat
na	na	k7c6	na
zhoršující	zhoršující	k2eAgFnSc6d1	zhoršující
se	se	k3xPyFc4	se
bolesti	bolest	k1gFnSc2	bolest
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Courtney	Courtne	k1gMnPc7	Courtne
Love	lov	k1gInSc5	lov
nadále	nadále	k6eAd1	nadále
bral	brát	k5eAaImAgMnS	brát
heroin	heroin	k1gInSc4	heroin
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiné	jiný	k2eAgFnPc4d1	jiná
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Nirvana	Nirvan	k1gMnSc2	Nirvan
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
nedlouhé	dlouhý	k2eNgNnSc4d1	nedlouhé
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
Red	Red	k1gFnSc7	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnSc3	Chil
Peppers	Peppersa	k1gFnPc2	Peppersa
a	a	k8xC	a
Pearl	Pearla	k1gFnPc2	Pearla
Jam	jáma	k1gFnPc2	jáma
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
lukrativním	lukrativní	k2eAgInSc6d1	lukrativní
pořadu	pořad	k1gInSc6	pořad
stanice	stanice	k1gFnSc2	stanice
MTV	MTV	kA	MTV
Saturday	Saturdaa	k1gMnSc2	Saturdaa
Night	Night	k1gMnSc1	Night
Live	Liv	k1gMnSc2	Liv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
Nirvana	Nirvan	k1gMnSc2	Nirvan
podnikla	podniknout	k5eAaPmAgFnS	podniknout
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgMnSc2	jenž
navštívila	navštívit	k5eAaPmAgFnS	navštívit
i	i	k9	i
státy	stát	k1gInPc1	stát
jako	jako	k8xC	jako
Japonsko	Japonsko	k1gNnSc1	Japonsko
či	či	k8xC	či
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
turné	turné	k1gNnSc2	turné
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jen	jen	k9	jen
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
série	série	k1gFnSc1	série
alba	album	k1gNnSc2	album
Hormoaning	Hormoaning	k1gInSc1	Hormoaning
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kapela	kapela	k1gFnSc1	kapela
přijela	přijet	k5eAaPmAgFnS	přijet
na	na	k7c4	na
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
Cobain	Cobain	k1gInSc1	Cobain
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Courtney	Courtne	k1gMnPc7	Courtne
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
Frances	Francesa	k1gFnPc2	Francesa
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
měli	mít	k5eAaImAgMnP	mít
členové	člen	k1gMnPc1	člen
Nirvany	Nirvan	k1gMnPc7	Nirvan
absolvovat	absolvovat	k5eAaPmF	absolvovat
vystoupení	vystoupení	k1gNnPc1	vystoupení
na	na	k7c4	na
Reading	Reading	k1gInSc4	Reading
festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
festivalů	festival	k1gInPc2	festival
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgFnP	šířit
pověsti	pověst	k1gFnPc1	pověst
ohledně	ohledně	k7c2	ohledně
zdraví	zdraví	k1gNnSc2	zdraví
Kurta	Kurt	k1gMnSc2	Kurt
Cobaina	Cobain	k1gMnSc2	Cobain
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
přivézt	přivézt	k5eAaPmF	přivézt
na	na	k7c6	na
nemocničním	nemocniční	k2eAgInSc6d1	nemocniční
vozíku	vozík	k1gInSc6	vozík
a	a	k8xC	a
když	když	k8xS	když
Novoselic	Novoselice	k1gFnPc2	Novoselice
řekl	říct	k5eAaPmAgMnS	říct
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kurt	Kurt	k1gMnSc1	Kurt
jim	on	k3xPp3gMnPc3	on
přijel	přijet	k5eAaPmAgMnS	přijet
zahrát	zahrát	k5eAaPmF	zahrát
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
Cobain	Cobain	k1gMnSc1	Cobain
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgMnS	zvednout
z	z	k7c2	z
vozíku	vozík	k1gInSc2	vozík
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
"	"	kIx"	"
<g/>
Breed	Breed	k1gInSc1	Breed
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
Nirvana	Nirvan	k1gMnSc4	Nirvan
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
udílení	udílení	k1gNnSc6	udílení
cen	cena	k1gFnPc2	cena
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc4	čtyři
ocenění	ocenění	k1gNnPc4	ocenění
a	a	k8xC	a
odnesla	odnést	k5eAaPmAgFnS	odnést
si	se	k3xPyFc3	se
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
za	za	k7c2	za
"	"	kIx"	"
<g/>
Smells	Smellsa	k1gFnPc2	Smellsa
Like	Like	k1gInSc1	Like
Teen	Teen	k1gMnSc1	Teen
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Incesticide	Incesticid	k1gMnSc5	Incesticid
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
sbírkou	sbírka	k1gFnSc7	sbírka
B	B	kA	B
<g/>
–	–	k?	–
<g/>
stran	strana	k1gFnPc2	strana
singlů	singl	k1gInPc2	singl
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
producenta	producent	k1gMnSc2	producent
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
si	se	k3xPyFc3	se
kapela	kapela	k1gFnSc1	kapela
vybrala	vybrat	k5eAaPmAgFnS	vybrat
Steva	Steve	k1gMnSc4	Steve
Albiniho	Albini	k1gMnSc4	Albini
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgMnSc1d1	známý
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
Surfer	Surfer	k1gMnSc1	Surfer
Rosa	Rosa	k1gMnSc1	Rosa
od	od	k7c2	od
Pixies	Pixiesa	k1gFnPc2	Pixiesa
<g/>
.	.	kIx.	.
</s>
<s>
Cobain	Cobain	k1gMnSc1	Cobain
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gNnSc1	jejich
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
mělo	mít	k5eAaImAgNnS	mít
podobný	podobný	k2eAgInSc4d1	podobný
zvuk	zvuk	k1gInSc4	zvuk
právě	právě	k9	právě
jako	jako	k8xC	jako
Sufer	Sufer	k1gMnSc1	Sufer
Rosa	Rosa	k1gMnSc1	Rosa
<g/>
.	.	kIx.	.
</s>
<s>
Cobain	Cobain	k1gInSc1	Cobain
měl	mít	k5eAaImAgInS	mít
základní	základní	k2eAgFnSc4d1	základní
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
písničky	písnička	k1gFnPc4	písnička
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
objeví	objevit	k5eAaPmIp3nS	objevit
a	a	k8xC	a
jak	jak	k6eAd1	jak
budou	být	k5eAaImBp3nP	být
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Albinim	Albini	k1gNnSc7	Albini
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
Nirvany	Nirvan	k1gMnPc7	Nirvan
vydali	vydat	k5eAaPmAgMnP	vydat
do	do	k7c2	do
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Minnesotě	Minnesota	k1gFnSc6	Minnesota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zaregistrovali	zaregistrovat	k5eAaPmAgMnP	zaregistrovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
The	The	k1gFnSc2	The
Simon	Simon	k1gMnSc1	Simon
Ritchie	Ritchie	k1gFnSc2	Ritchie
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgNnSc1	třetí
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
In	In	k1gFnSc2	In
Utero	Utero	k1gNnSc4	Utero
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
jmenovat	jmenovat	k5eAaBmF	jmenovat
"	"	kIx"	"
<g/>
I	i	k9	i
Hate	Hate	k1gFnPc2	Hate
Myself	Myself	k1gMnSc1	Myself
and	and	k?	and
Want	Want	k1gMnSc1	Want
to	ten	k3xDgNnSc1	ten
Die	Die	k1gFnPc1	Die
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapele	kapela	k1gFnSc3	kapela
stačily	stačit	k5eAaBmAgFnP	stačit
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
nahrání	nahrání	k1gNnSc3	nahrání
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
In	In	k1gMnPc1	In
Utero	Utero	k1gNnSc4	Utero
odeslali	odeslat	k5eAaPmAgMnP	odeslat
do	do	k7c2	do
Geffenu	Geffen	k1gInSc2	Geffen
<g/>
,	,	kIx,	,
tamějšímu	tamější	k2eAgNnSc3d1	tamější
vedení	vedení	k1gNnSc3	vedení
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nelíbilo	líbit	k5eNaImAgNnS	líbit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
po	po	k7c6	po
Cobainovi	Cobain	k1gMnSc3	Cobain
chtělo	chtít	k5eAaImAgNnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nakonec	nakonec	k6eAd1	nakonec
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
In	In	k1gFnPc1	In
Utero	Utero	k1gNnSc1	Utero
vyšplhalo	vyšplhat	k5eAaPmAgNnS	vyšplhat
na	na	k7c4	na
přední	přední	k2eAgFnPc4d1	přední
příčky	příčka	k1gFnPc4	příčka
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
,	,	kIx,	,
takového	takový	k3xDgInSc2	takový
úspěchu	úspěch	k1gInSc2	úspěch
jako	jako	k8xS	jako
Nevermind	Nevermind	k1gInSc4	Nevermind
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
albu	album	k1gNnSc3	album
postupně	postupně	k6eAd1	postupně
vyšly	vyjít	k5eAaPmAgInP	vyjít
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Heart-Shaped	Heart-Shaped	k1gInSc1	Heart-Shaped
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Apologies	Apologies	k1gMnSc1	Apologies
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
Rape	rape	k1gNnSc1	rape
Me	Me	k1gFnSc2	Me
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pennyroyal	Pennyroyal	k1gInSc1	Pennyroyal
Tea	Tea	k1gFnSc1	Tea
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
našel	najít	k5eAaPmAgMnS	najít
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
fanoušků	fanoušek	k1gMnPc2	fanoušek
na	na	k7c6	na
zakoupeném	zakoupený	k2eAgInSc6d1	zakoupený
magnetofonovém	magnetofonový	k2eAgInSc6d1	magnetofonový
pásku	pásek	k1gInSc6	pásek
z	z	k7c2	z
nahrávání	nahrávání	k1gNnSc2	nahrávání
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
dvě	dva	k4xCgFnPc4	dva
neznámé	známý	k2eNgFnPc4d1	neznámá
skladby	skladba	k1gFnPc4	skladba
bez	bez	k7c2	bez
vokálů	vokál	k1gInPc2	vokál
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
pracovně	pracovně	k6eAd1	pracovně
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
jako	jako	k8xS	jako
Dave	Dav	k1gInSc5	Dav
solo	solo	k6eAd1	solo
a	a	k8xC	a
Lullaby	Lullaba	k1gFnSc2	Lullaba
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
v	v	k7c6	v
trvání	trvání	k1gNnSc6	trvání
zhruba	zhruba	k6eAd1	zhruba
necelých	celý	k2eNgFnPc2d1	necelá
dvou	dva	k4xCgFnPc2	dva
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc4	skladba
poprvé	poprvé	k6eAd1	poprvé
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
internetová	internetový	k2eAgFnSc1d1	internetová
stanice	stanice	k1gFnSc1	stanice
VirginRadio	VirginRadio	k6eAd1	VirginRadio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
Nirvana	Nirvan	k1gMnSc2	Nirvan
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nečekané	čekaný	k2eNgNnSc1d1	nečekané
<g/>
,	,	kIx,	,
že	že	k8xS	že
takřka	takřka	k6eAd1	takřka
polovina	polovina	k1gFnSc1	polovina
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Nirvana	Nirvan	k1gMnSc4	Nirvan
zahrála	zahrát	k5eAaPmAgNnP	zahrát
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
převzaté	převzatý	k2eAgFnPc1d1	převzatá
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
jediným	jediný	k2eAgInSc7d1	jediný
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
zazněl	zaznít	k5eAaPmAgInS	zaznít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Come	Come	k1gInSc1	Come
as	as	k1gNnSc2	as
You	You	k1gFnSc2	You
Are	ar	k1gInSc5	ar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Cobainem	Cobain	k1gMnSc7	Cobain
<g/>
,	,	kIx,	,
Grohlem	Grohl	k1gMnSc7	Grohl
a	a	k8xC	a
Novoselicem	Novoselic	k1gMnSc7	Novoselic
<g/>
,	,	kIx,	,
objevili	objevit	k5eAaPmAgMnP	objevit
ještě	ještě	k9	ještě
bratři	bratr	k1gMnPc1	bratr
Kirkwoodovi	Kirkwoodův	k2eAgMnPc1d1	Kirkwoodův
z	z	k7c2	z
Meat	Meata	k1gFnPc2	Meata
Puppets	Puppetsa	k1gFnPc2	Puppetsa
<g/>
,	,	kIx,	,
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
violoncellistka	violoncellistka	k1gFnSc1	violoncellistka
a	a	k8xC	a
stálý	stálý	k2eAgMnSc1d1	stálý
host	host	k1gMnSc1	host
Lori	Lori	k1gFnSc2	Lori
Goldston	Goldston	k1gInSc1	Goldston
a	a	k8xC	a
Pat	pat	k1gInSc1	pat
Smear	Smeara	k1gFnPc2	Smeara
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
dříve	dříve	k6eAd2	dříve
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
vystoupení	vystoupení	k1gNnPc1	vystoupení
Nirvany	Nirvan	k1gMnPc4	Nirvan
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něj	on	k3xPp3gMnSc4	on
Kurt	kurt	k1gInSc4	kurt
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
ztratil	ztratit	k5eAaPmAgMnS	ztratit
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
nucen	nutit	k5eAaImNgMnS	nutit
dočasně	dočasně	k6eAd1	dočasně
opustit	opustit	k5eAaPmF	opustit
pódium	pódium	k1gNnSc4	pódium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
kapely	kapela	k1gFnSc2	kapela
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
pokusil	pokusit	k5eAaPmAgMnS	pokusit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
požitím	požití	k1gNnSc7	požití
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
Rohypnolu	rohypnol	k1gInSc2	rohypnol
<g/>
.	.	kIx.	.
</s>
<s>
Nirvana	Nirvan	k1gMnSc4	Nirvan
byla	být	k5eAaImAgFnS	být
uprostřed	uprostřed	k7c2	uprostřed
turné	turné	k1gNnSc2	turné
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
jí	on	k3xPp3gFnSc3	on
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
měla	mít	k5eAaImAgFnS	mít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
poprvé	poprvé	k6eAd1	poprvé
navštívit	navštívit	k5eAaPmF	navštívit
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
i	i	k8xC	i
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Cobain	Cobain	k1gInSc1	Cobain
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byl	být	k5eAaImAgMnS	být
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobaina	k1gFnPc2	Cobaina
nalezen	naleznout	k5eAaPmNgInS	naleznout
mrtev	mrtev	k2eAgInSc1d1	mrtev
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
i	i	k9	i
přes	přes	k7c4	přes
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
vraždě	vražda	k1gFnSc6	vražda
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xS	jako
sebevražda	sebevražda	k1gFnSc1	sebevražda
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
pátého	pátý	k4xOgInSc2	pátý
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
znamenala	znamenat	k5eAaImAgFnS	znamenat
rozpad	rozpad	k1gInSc4	rozpad
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Nirvany	Nirvan	k1gMnPc4	Nirvan
byla	být	k5eAaImAgFnS	být
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
vydávána	vydáván	k2eAgFnSc1d1	vydávána
její	její	k3xOp3gFnSc1	její
alba	alba	k1gFnSc1	alba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gMnSc1	Unplugged
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
záznamem	záznam	k1gInSc7	záznam
akustického	akustický	k2eAgNnSc2d1	akustické
vystoupení	vystoupení	k1gNnSc2	vystoupení
Nirvany	Nirvan	k1gMnPc4	Nirvan
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
předešlého	předešlý	k2eAgInSc2d1	předešlý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
pod	pod	k7c7	pod
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Geffen	Geffna	k1gFnPc2	Geffna
Records	Records	k1gInSc4	Records
vydáno	vydán	k2eAgNnSc4d1	vydáno
video	video	k1gNnSc4	video
Live	Liv	k1gFnSc2	Liv
<g/>
!	!	kIx.	!
</s>
<s>
Tonight	Tonight	k1gMnSc1	Tonight
<g/>
!	!	kIx.	!
</s>
<s>
Sold	sold	k1gInSc1	sold
Out	Out	k1gFnSc2	Out
<g/>
!!	!!	k?	!!
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1996	[number]	k4	1996
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
další	další	k2eAgNnSc1d1	další
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
From	From	k1gMnSc1	From
the	the	k?	the
Muddy	Mudd	k1gInPc1	Mudd
Banks	Banks	k1gInSc1	Banks
of	of	k?	of
the	the	k?	the
Wishkah	Wishkah	k1gInSc1	Wishkah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
Courtney	Courtnea	k1gFnSc2	Courtnea
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Novoselic	Novoselice	k1gFnPc2	Novoselice
a	a	k8xC	a
Grohl	Grohl	k1gMnPc1	Grohl
společně	společně	k6eAd1	společně
založili	založit	k5eAaPmAgMnP	založit
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
názvem	název	k1gInSc7	název
Nirvana	Nirvan	k1gMnSc2	Nirvan
L.	L.	kA	L.
L.	L.	kA	L.
C.	C.	kA	C.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
spravovat	spravovat	k5eAaImF	spravovat
veškerou	veškerý	k3xTgFnSc4	veškerý
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
Nirvany	Nirvan	k1gMnPc4	Nirvan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
začali	začít	k5eAaPmAgMnP	začít
Novoselic	Novoselice	k1gFnPc2	Novoselice
s	s	k7c7	s
Grohlem	Grohl	k1gInSc7	Grohl
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
většinu	většina	k1gFnSc4	většina
nahrávek	nahrávka	k1gFnPc2	nahrávka
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
2001	[number]	k4	2001
se	se	k3xPyFc4	se
chystali	chystat	k5eAaImAgMnP	chystat
vydat	vydat	k5eAaPmF	vydat
box	box	k1gInSc4	box
set	set	k1gInSc4	set
k	k	k7c3	k
desátému	desátý	k4xOgInSc3	desátý
výročí	výročí	k1gNnSc3	výročí
Nevermind	Nevermind	k1gInSc1	Nevermind
<g/>
.	.	kIx.	.
</s>
<s>
Cobainova	Cobainův	k2eAgFnSc1d1	Cobainova
vdova	vdova	k1gFnSc1	vdova
Courtney	Courtnea	k1gFnSc2	Courtnea
Love	lov	k1gInSc5	lov
však	však	k9	však
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
zabránila	zabránit	k5eAaPmAgFnS	zabránit
vydání	vydání	k1gNnSc4	vydání
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
soudit	soudit	k5eAaImF	soudit
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
zbývajícími	zbývající	k2eAgInPc7d1	zbývající
členy	člen	k1gInPc7	člen
kapely	kapela	k1gFnSc2	kapela
o	o	k7c4	o
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
písně	píseň	k1gFnPc4	píseň
Nirvany	Nirvan	k1gMnPc7	Nirvan
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
pozdržení	pozdržení	k1gNnSc4	pozdržení
vydání	vydání	k1gNnSc2	vydání
nahrávek	nahrávka	k1gFnPc2	nahrávka
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgFnSc1d1	soudní
pře	pře	k1gFnSc1	pře
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
dohodou	dohoda	k1gFnSc7	dohoda
mezi	mezi	k7c7	mezi
zúčastněnými	zúčastněný	k2eAgFnPc7d1	zúčastněná
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
výběrové	výběrový	k2eAgNnSc1d1	výběrové
album	album	k1gNnSc1	album
s	s	k7c7	s
prostým	prostý	k2eAgInSc7d1	prostý
názvem	název	k1gInSc7	název
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
dosud	dosud	k6eAd1	dosud
nepublikovanou	publikovaný	k2eNgFnSc4d1	nepublikovaná
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
You	You	k1gFnSc4	You
Know	Know	k1gMnSc2	Know
You	You	k1gMnSc2	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Right	Right	k1gInSc1	Right
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
následoval	následovat	k5eAaImAgInS	následovat
box	box	k1gInSc1	box
set	set	k1gInSc1	set
With	With	k1gInSc1	With
the	the	k?	the
Lights	Lights	k1gInSc1	Lights
Out	Out	k1gMnSc2	Out
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
3	[number]	k4	3
CD	CD	kA	CD
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
albem	album	k1gNnSc7	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
další	další	k2eAgNnSc4d1	další
výběrové	výběrový	k2eAgNnSc4d1	výběrové
CD	CD	kA	CD
Sliver	Sliver	k1gMnSc1	Sliver
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
Box	box	k1gInSc1	box
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgNnSc7d1	poslední
vydaným	vydaný	k2eAgNnSc7d1	vydané
albem	album	k1gNnSc7	album
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
stalo	stát	k5eAaPmAgNnS	stát
koncertní	koncertní	k2eAgNnSc1d1	koncertní
CD	CD	kA	CD
<g/>
/	/	kIx~	/
<g/>
DVD	DVD	kA	DVD
Live	Live	k1gInSc1	Live
at	at	k?	at
Reading	Reading	k1gInSc1	Reading
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
koncertu	koncert	k1gInSc2	koncert
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
zůstali	zůstat	k5eAaPmAgMnP	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hudebně	hudebně	k6eAd1	hudebně
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gMnPc6	Grohl
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
Cobainově	Cobainův	k2eAgFnSc6d1	Cobainova
smrti	smrt	k1gFnSc6	smrt
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
svým	svůj	k3xOyFgMnSc7	svůj
prvním	první	k4xOgFnPc3	první
demo	demo	k2eAgFnPc3d1	demo
nahrávkám	nahrávka	k1gFnPc3	nahrávka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
skupiny	skupina	k1gFnSc2	skupina
Foo	Foo	k1gFnSc2	Foo
Fighters	Fightersa	k1gFnPc2	Fightersa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Grohl	Grohl	k1gFnSc4	Grohl
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Foo	Foo	k1gMnPc7	Foo
Fighters	Fightersa	k1gFnPc2	Fightersa
někdy	někdy	k6eAd1	někdy
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
i	i	k9	i
Pat	pat	k1gInSc1	pat
Smear	Smeara	k1gFnPc2	Smeara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jejich	jejich	k3xOp3gNnSc1	jejich
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Foo	Foo	k1gFnSc2	Foo
Fighters	Fighters	k1gInSc1	Fighters
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
In	In	k1gMnSc1	In
Your	Your	k1gMnSc1	Your
Honor	Honor	k1gMnSc1	Honor
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
píseň	píseň	k1gFnSc4	píseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Friend	Friend	k1gInSc1	Friend
of	of	k?	of
a	a	k8xC	a
Friend	Friend	k1gMnSc1	Friend
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Grohl	Grohl	k1gMnSc1	Grohl
napsal	napsat	k5eAaBmAgMnS	napsat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgInSc2	první
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
Novoselicem	Novoselic	k1gMnSc7	Novoselic
a	a	k8xC	a
Cobainem	Cobain	k1gMnSc7	Cobain
<g/>
.	.	kIx.	.
</s>
<s>
Grohl	Grohnout	k5eAaPmAgInS	Grohnout
dále	daleko	k6eAd2	daleko
bubnoval	bubnovat	k5eAaImAgInS	bubnovat
nebo	nebo	k8xC	nebo
bubnuje	bubnovat	k5eAaImIp3nS	bubnovat
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
a	a	k8xC	a
osobnostmi	osobnost	k1gFnPc7	osobnost
jako	jako	k9	jako
Tom	Tom	k1gMnSc1	Tom
Petty	Petta	k1gFnSc2	Petta
and	and	k?	and
the	the	k?	the
Heartbreakers	Heartbreakers	k1gInSc1	Heartbreakers
<g/>
,	,	kIx,	,
Mike	Mike	k1gInSc1	Mike
Watt	watt	k1gInSc1	watt
<g/>
,	,	kIx,	,
Queens	Queens	k1gInSc1	Queens
of	of	k?	of
the	the	k?	the
Stone	ston	k1gInSc5	ston
Age	Age	k1gMnSc1	Age
<g/>
,	,	kIx,	,
Tenacious	Tenacious	k1gMnSc1	Tenacious
D	D	kA	D
a	a	k8xC	a
Nine	Nine	k1gFnSc1	Nine
Inch	Incha	k1gFnPc2	Incha
Nails	Nailsa	k1gFnPc2	Nailsa
<g/>
.	.	kIx.	.
</s>
<s>
Krist	Krista	k1gFnPc2	Krista
Novoselic	Novoselice	k1gFnPc2	Novoselice
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Nirvany	Nirvan	k1gMnPc4	Nirvan
zformoval	zformovat	k5eAaPmAgInS	zformovat
kapelu	kapela	k1gFnSc4	kapela
Sweet	Sweet	k1gInSc1	Sweet
75	[number]	k4	75
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
vydaném	vydaný	k2eAgNnSc6d1	vydané
albu	album	k1gNnSc6	album
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgMnS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Curtem	Curt	k1gMnSc7	Curt
Kirkwoodem	Kirkwood	k1gMnSc7	Kirkwood
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Meat	Meat	k2eAgInSc1d1	Meat
Puppets	Puppets	k1gInSc1	Puppets
a	a	k8xC	a
Budem	Budem	k?	Budem
Gaughem	Gaugh	k1gInSc7	Gaugh
ze	z	k7c2	z
Sublime	Sublim	k1gInSc5	Sublim
projekt	projekt	k1gInSc4	projekt
Eyes	Eyes	k1gInSc4	Eyes
Adrift	Adrifta	k1gFnPc2	Adrifta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Novoselic	Novoselice	k1gFnPc2	Novoselice
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgInS	stát
politickým	politický	k2eAgMnSc7d1	politický
aktivistou	aktivista	k1gMnSc7	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Grohl	Grohl	k1gFnSc1	Grohl
<g/>
,	,	kIx,	,
Novoselic	Novoselice	k1gFnPc2	Novoselice
a	a	k8xC	a
Smear	Smeara	k1gFnPc2	Smeara
po	po	k7c6	po
osmnácti	osmnáct	k4xCc6	osmnáct
letech	let	k1gInPc6	let
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertu	koncert	k1gInSc6	koncert
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Concert	Concert	k1gInSc1	Concert
for	forum	k1gNnPc2	forum
Sandy	Sanda	k1gFnSc2	Sanda
Relief	Relief	k1gInSc4	Relief
<g/>
,	,	kIx,	,
věnovaném	věnovaný	k2eAgInSc6d1	věnovaný
obětem	oběť	k1gFnPc3	oběť
hurikánu	hurikán	k1gInSc2	hurikán
Sandy	Sand	k1gMnPc4	Sand
<g/>
,	,	kIx,	,
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
McCartneym	McCartneym	k1gInSc4	McCartneym
zahráli	zahrát	k5eAaPmAgMnP	zahrát
společnou	společný	k2eAgFnSc4d1	společná
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Cut	Cut	k1gFnSc4	Cut
Me	Me	k1gMnSc2	Me
Some	Som	k1gMnSc2	Som
Slack	Slack	k1gMnSc1	Slack
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
dokmentu	dokment	k1gInSc6	dokment
Sound	Sounda	k1gFnPc2	Sounda
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Grohl	Grohl	k1gFnSc4	Grohl
režíruje	režírovat	k5eAaImIp3nS	režírovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
skupina	skupina	k1gFnSc1	skupina
do	do	k7c2	do
Rock	rock	k1gInSc4	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Nirvana	Nirvan	k1gMnSc4	Nirvan
měla	mít	k5eAaImAgFnS	mít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hudební	hudební	k2eAgInSc4d1	hudební
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nejednou	jednou	k6eNd1	jednou
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
Nevermind	Neverminda	k1gFnPc2	Neverminda
prohlášeno	prohlášen	k2eAgNnSc1d1	prohlášeno
za	za	k7c4	za
nejzásadnější	zásadní	k2eAgNnSc4d3	nejzásadnější
album	album	k1gNnSc4	album
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
Nirvany	Nirvan	k1gMnPc7	Nirvan
je	být	k5eAaImIp3nS	být
znát	znát	k5eAaImF	znát
např.	např.	kA	např.
u	u	k7c2	u
kapel	kapela	k1gFnPc2	kapela
Mudhoney	Mudhonea	k1gFnSc2	Mudhonea
či	či	k8xC	či
novější	nový	k2eAgFnSc2d2	novější
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Strokes	Strokes	k1gMnSc1	Strokes
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
The	The	k1gFnPc1	The
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Rock	rock	k1gInSc1	rock
&	&	k?	&
Roll	Roll	k1gInSc1	Roll
Nirvana	Nirvan	k1gMnSc2	Nirvan
změnila	změnit	k5eAaPmAgFnS	změnit
ráz	ráz	k1gInSc4	ráz
hudby	hudba	k1gFnSc2	hudba
devadesátých	devadesátý	k4xOgInPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
členové	člen	k1gMnPc1	člen
Nirvany	Nirvan	k1gMnPc4	Nirvan
byli	být	k5eAaImAgMnP	být
ovlivněni	ovlivněn	k2eAgMnPc1d1	ovlivněn
různými	různý	k2eAgFnPc7d1	různá
kapelami	kapela	k1gFnPc7	kapela
a	a	k8xC	a
hudebními	hudební	k2eAgInPc7d1	hudební
styly	styl	k1gInPc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přímo	přímo	k6eAd1	přímo
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
Kurta	kurta	k1gFnSc1	kurta
Cobaina	Cobaina	k1gFnSc1	Cobaina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Pixies	Pixies	k1gInSc4	Pixies
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
dokonce	dokonce	k9	dokonce
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebýt	být	k5eNaImF	být
Pixies	Pixies	k1gInSc4	Pixies
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
by	by	kYmCp3nS	by
Nirvana	Nirvan	k1gMnSc4	Nirvan
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
kapel	kapela	k1gFnPc2	kapela
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
zajisté	zajisté	k9	zajisté
Melvins	Melvins	k1gInSc4	Melvins
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
frontman	frontman	k1gMnSc1	frontman
Buzz	Buzz	k1gMnSc1	Buzz
Osborne	Osborn	k1gInSc5	Osborn
byl	být	k5eAaImAgMnS	být
Cobainův	Cobainův	k2eAgMnSc1d1	Cobainův
kamarád	kamarád	k1gMnSc1	kamarád
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Black	Black	k1gInSc4	Black
Flag	flaga	k1gFnPc2	flaga
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
koncerty	koncert	k1gInPc4	koncert
Cobain	Cobaina	k1gFnPc2	Cobaina
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
jezdil	jezdit	k5eAaImAgMnS	jezdit
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Sonic	Sonic	k1gMnSc1	Sonic
Youth	Youth	k1gMnSc1	Youth
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
Pistols	Pistols	k1gInSc1	Pistols
<g/>
,	,	kIx,	,
<g/>
Dead	Dead	k1gInSc1	Dead
Kennedys	Kennedysa	k1gFnPc2	Kennedysa
<g/>
,	,	kIx,	,
Wipers	Wipersa	k1gFnPc2	Wipersa
<g/>
,	,	kIx,	,
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
Beat	beat	k1gInSc1	beat
Happening	happening	k1gInSc1	happening
a	a	k8xC	a
The	The	k1gMnSc1	The
Vaselines	Vaselines	k1gMnSc1	Vaselines
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
Nirvany	Nirvan	k1gMnPc4	Nirvan
měly	mít	k5eAaImAgInP	mít
také	také	k9	také
hard	harda	k1gFnPc2	harda
rockové	rockový	k2eAgFnSc2d1	rocková
kapely	kapela	k1gFnSc2	kapela
ze	z	k7c2	z
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
především	především	k9	především
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
Aerosmith	Aerosmith	k1gInSc1	Aerosmith
(	(	kIx(	(
<g/>
na	na	k7c4	na
což	což	k3yQnSc4	což
Cobain	Cobain	k1gInSc1	Cobain
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
i	i	k9	i
písní	píseň	k1gFnSc7	píseň
Aero	aero	k1gNnSc1	aero
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
<g/>
)	)	kIx)	)
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Krist	Krista	k1gFnPc2	Krista
Novoselic	Novoselice	k1gFnPc2	Novoselice
–	–	k?	–
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gMnPc4	Grohl
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Pat	pata	k1gFnPc2	pata
Smear	Smear	k1gMnSc1	Smear
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
Aaron	Aaron	k1gMnSc1	Aaron
Burckhard	Burckhard	k1gMnSc1	Burckhard
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Dale	Dal	k1gInSc2	Dal
Crover	Crover	k1gMnSc1	Crover
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
<g />
.	.	kIx.	.
</s>
<s>
Foster	Foster	k1gMnSc1	Foster
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Chad	Chad	k1gInSc1	Chad
Channing	Channing	k1gInSc1	Channing
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Jason	Jason	k1gMnSc1	Jason
Everman	Everman	k1gMnSc1	Everman
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Dan	Dan	k1gMnSc1	Dan
Peters	Petersa	k1gFnPc2	Petersa
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Lori	Lori	k1gFnSc1	Lori
Goldston	Goldston	k1gInSc1	Goldston
–	–	k?	–
violoncello	violoncello	k1gNnSc1	violoncello
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Melora	Melora	k1gFnSc1	Melora
Creager	Creagra	k1gFnPc2	Creagra
–	–	k?	–
violoncello	violoncello	k1gNnSc4	violoncello
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Nirvany	Nirvan	k1gMnPc4	Nirvan
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
Nirvany	Nirvan	k1gMnPc7	Nirvan
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
Bleach	Bleach	k1gInSc1	Bleach
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Nevermind	Nevermind	k1gInSc1	Nevermind
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
In	In	k1gFnSc1	In
Utero	Utero	k1gNnSc1	Utero
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gMnSc1	Unplugged
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
From	Froma	k1gFnPc2	Froma
the	the	k?	the
Muddy	Mudd	k1gInPc1	Mudd
Banks	Banks	k1gInSc1	Banks
of	of	k?	of
the	the	k?	the
Wishkah	Wishkah	k1gInSc1	Wishkah
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
at	at	k?	at
Reading	Reading	k1gInSc1	Reading
2010	[number]	k4	2010
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Live	Live	k1gFnSc3	Live
at	at	k?	at
Paramount	Paramount	k1gInSc1	Paramount
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Hormoaning	Hormoaning	k1gInSc1	Hormoaning
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jen	jen	k9	jen
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Incesticide	Incesticid	k1gInSc5	Incesticid
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Nirvana	Nirvan	k1gMnSc2	Nirvan
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
With	With	k1gInSc1	With
the	the	k?	the
Lights	Lights	k1gInSc1	Lights
Out	Out	k1gFnSc1	Out
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Sliver	Sliver	k1gMnSc1	Sliver
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
Box	box	k1gInSc1	box
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
<g />
.	.	kIx.	.
</s>
<s>
Buzz	Buzz	k1gInSc1	Buzz
<g/>
"	"	kIx"	"
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sliver	Sliver	k1gInSc1	Sliver
<g/>
"	"	kIx"	"
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Like	k1gNnSc1	Like
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirita	k1gFnPc2	Spirita
<g/>
"	"	kIx"	"
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Come	Come	k1gInSc1	Come
as	as	k1gNnSc2	as
You	You	k1gFnSc2	You
Are	ar	k1gInSc5	ar
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lithium	lithium	k1gNnSc1	lithium
<g/>
"	"	kIx"	"
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
Bloom	Bloom	k1gInSc1	Bloom
<g/>
"	"	kIx"	"
1993	[number]	k4	1993
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Heart-Shaped	Heart-Shaped	k1gInSc1	Heart-Shaped
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Apologies	Apologies	k1gMnSc1	Apologies
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
Rape	rape	k1gNnSc1	rape
Me	Me	k1gFnSc2	Me
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pennyroyal	Pennyroyal	k1gInSc1	Pennyroyal
Tea	Tea	k1gFnSc1	Tea
<g/>
"	"	kIx"	"
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
About	About	k1gInSc1	About
a	a	k8xC	a
Girl	girl	k1gFnSc1	girl
<g/>
"	"	kIx"	"
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
You	You	k1gMnSc1	You
Know	Know	k1gMnSc1	Know
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Right	Right	k1gInSc1	Right
<g/>
"	"	kIx"	"
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Drain	Drain	k1gMnSc1	Drain
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Polly	Polla	k1gFnPc1	Polla
<g/>
"	"	kIx"	"
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dumb	Dumb	k1gMnSc1	Dumb
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
–	–	k?	–
1992	[number]	k4	1992
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
videoklip	videoklip	k1gInSc4	videoklip
nového	nový	k2eAgMnSc2d1	nový
umělce	umělec	k1gMnSc2	umělec
–	–	k?	–
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Smells	Smells	k1gInSc1	Smells
Like	Lik	k1gFnSc2	Lik
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
–	–	k?	–
1992	[number]	k4	1992
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
alternativní	alternativní	k2eAgInSc4d1	alternativní
videoklip	videoklip	k1gInSc4	videoklip
–	–	k?	–
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Lik	k1gFnSc2	Lik
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
–	–	k?	–
1993	[number]	k4	1993
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
alternativní	alternativní	k2eAgInSc4d1	alternativní
videoklip	videoklip	k1gInSc4	videoklip
–	–	k?	–
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
Bloom	Bloom	k1gInSc1	Bloom
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
–	–	k?	–
1994	[number]	k4	1994
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g />
.	.	kIx.	.
</s>
<s>
alternativní	alternativní	k2eAgInSc4d1	alternativní
videoklip	videoklip	k1gInSc4	videoklip
–	–	k?	–
"	"	kIx"	"
<g/>
Heart-Shaped	Heart-Shaped	k1gInSc1	Heart-Shaped
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
–	–	k?	–
1994	[number]	k4	1994
–	–	k?	–
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
videoklipu	videoklip	k1gInSc2	videoklip
–	–	k?	–
"	"	kIx"	"
<g/>
Heart-Shaped	Heart-Shaped	k1gInSc1	Heart-Shaped
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
Grammy	Gramm	k1gInPc1	Gramm
–	–	k?	–
1996	[number]	k4	1996
–	–	k?	–
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
alternativní	alternativní	k2eAgNnSc1d1	alternativní
album	album	k1gNnSc1	album
–	–	k?	–
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gMnSc1	Unplugged
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
MTV	MTV	kA	MTV
Music	Musice	k1gFnPc2	Musice
Video	video	k1gNnSc1	video
Awards	Awards	k1gInSc1	Awards
–	–	k?	–
1992	[number]	k4	1992
–	–	k?	–
Videoklip	videoklip	k1gInSc1	videoklip
roku	rok	k1gInSc2	rok
–	–	k?	–
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Lik	k1gFnSc2	Lik
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Music	Music	k1gMnSc1	Music
Video	video	k1gNnSc1	video
Awards	Awards	k1gInSc1	Awards
–	–	k?	–
1992	[number]	k4	1992
–	–	k?	–
Cena	cena	k1gFnSc1	cena
diváků	divák	k1gMnPc2	divák
–	–	k?	–
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Lik	k1gFnSc2	Lik
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Music	Music	k1gMnSc1	Music
Video	video	k1gNnSc1	video
Awards	Awards	k1gInSc1	Awards
–	–	k?	–
1994	[number]	k4	1994
–	–	k?	–
Videoklip	videoklip	k1gInSc1	videoklip
roku	rok	k1gInSc2	rok
–	–	k?	–
"	"	kIx"	"
<g/>
Heart-Shaped	Heart-Shaped	k1gInSc1	Heart-Shaped
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
MTV	MTV	kA	MTV
Music	Music	k1gMnSc1	Music
Video	video	k1gNnSc1	video
Awards	Awards	k1gInSc1	Awards
–	–	k?	–
1994	[number]	k4	1994
–	–	k?	–
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Heart-Shaped	Heart-Shaped	k1gInSc1	Heart-Shaped
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
Grammy	Gramm	k1gInPc1	Gramm
–	–	k?	–
1992	[number]	k4	1992
–	–	k?	–
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
alternativní	alternativní	k2eAgMnSc1d1	alternativní
hudební	hudební	k2eAgNnSc4d1	hudební
album	album	k1gNnSc4	album
–	–	k?	–
Nevermind	Nevermind	k1gMnSc1	Nevermind
Grammy	Gramma	k1gFnSc2	Gramma
–	–	k?	–
1992	[number]	k4	1992
–	–	k?	–
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
rocková	rockový	k2eAgFnSc1d1	rocková
píseň	píseň	k1gFnSc1	píseň
–	–	k?	–
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Lik	k1gFnSc2	Lik
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
Grammy	Gramm	k1gInPc1	Gramm
–	–	k?	–
1992	[number]	k4	1992
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hardrockový	hardrockový	k2eAgInSc4d1	hardrockový
výkon	výkon	k1gInSc4	výkon
–	–	k?	–
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Lik	k1gFnSc2	Lik
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
Grammy	Gramm	k1gInPc1	Gramm
–	–	k?	–
1994	[number]	k4	1994
–	–	k?	–
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
alternativní	alternativní	k2eAgMnSc1d1	alternativní
hudební	hudební	k2eAgNnSc4d1	hudební
album	album	k1gNnSc4	album
–	–	k?	–
In	In	k1gMnSc1	In
Utero	Utero	k1gNnSc1	Utero
</s>
