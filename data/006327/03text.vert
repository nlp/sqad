<s>
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1978	[number]	k4	1978
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dabér	dabér	k1gMnSc1	dabér
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmileté	osmiletý	k2eAgFnSc6d1	osmiletá
základní	základní	k2eAgFnSc6d1	základní
školní	školní	k2eAgFnSc6d1	školní
docházce	docházka	k1gFnSc6	docházka
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
herectví	herectví	k1gNnSc4	herectví
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
činoherního	činoherní	k2eAgInSc2d1	činoherní
souboru	soubor	k1gInSc2	soubor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
dabér	dabér	k1gInSc1	dabér
<g/>
,	,	kIx,	,
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
hlas	hlas	k1gInSc1	hlas
třeba	třeba	k6eAd1	třeba
Bilbovi	Bilba	k1gMnSc3	Bilba
Pytlíkovi	pytlík	k1gMnSc3	pytlík
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
Hobit	hobit	k1gMnSc1	hobit
nebo	nebo	k8xC	nebo
třeba	třeba	k9	třeba
Philipu	Philip	k1gMnSc3	Philip
J.	J.	kA	J.
Fryovi	Frya	k1gMnSc3	Frya
seriálů	seriál	k1gInPc2	seriál
Futurama	Futurama	k?	Futurama
<g/>
)	)	kIx)	)
a	a	k8xC	a
voiceover	voiceover	k1gInSc1	voiceover
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
hlas	hlas	k1gInSc1	hlas
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
umělecké	umělecký	k2eAgFnSc2d1	umělecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Mária	Mária	k1gFnSc1	Mária
Dolanská	dolanský	k2eAgFnSc1d1	Dolanská
je	být	k5eAaImIp3nS	být
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Dolanský	Dolanský	k1gMnSc1	Dolanský
je	být	k5eAaImIp3nS	být
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dabér	dabér	k1gMnSc1	dabér
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
bratra	bratr	k1gMnSc4	bratr
Jiřího	Jiří	k1gMnSc4	Jiří
(	(	kIx(	(
<g/>
dvojče	dvojče	k1gNnSc4	dvojče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Lenka	Lenka	k1gFnSc1	Lenka
Vlasáková	Vlasáková	k1gFnSc1	Vlasáková
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
trestně	trestně	k6eAd1	trestně
stíhán	stíhat	k5eAaImNgMnS	stíhat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
devadesáti	devadesát	k4xCc7	devadesát
osobami	osoba	k1gFnPc7	osoba
kvůli	kvůli	k7c3	kvůli
uplácení	uplácení	k1gNnSc3	uplácení
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
získání	získání	k1gNnSc2	získání
řidičského	řidičský	k2eAgInSc2d1	řidičský
průkazu	průkaz	k1gInSc2	průkaz
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
chebský	chebský	k2eAgMnSc1d1	chebský
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
Viktor	Viktor	k1gMnSc1	Viktor
Böhm	Böhm	k1gMnSc1	Böhm
podal	podat	k5eAaPmAgMnS	podat
obžalobu	obžaloba	k1gFnSc4	obžaloba
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
Hospoda	Hospoda	k?	Hospoda
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
Kouzelnice	kouzelnice	k1gFnSc1	kouzelnice
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
1997	[number]	k4	1997
Jak	jak	k6eAd1	jak
vyléčit	vyléčit	k5eAaPmF	vyléčit
Ježibabu	ježibaba	k1gFnSc4	ježibaba
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
Perníková	perníkový	k2eAgFnSc1d1	Perníková
věž	věž	k1gFnSc1	věž
2001	[number]	k4	2001
Královský	královský	k2eAgInSc4d1	královský
slib	slib	k1gInSc4	slib
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Zvon	zvon	k1gInSc1	zvon
Lukáš	Lukáš	k1gMnSc1	Lukáš
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Udělení	udělení	k1gNnSc6	udělení
milosti	milost	k1gFnSc2	milost
se	se	k3xPyFc4	se
zamítá	zamítat	k5eAaImIp3nS	zamítat
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Pátek	pátek	k1gInSc1	pátek
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Lakomec	lakomec	k1gMnSc1	lakomec
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
Čert	čert	k1gMnSc1	čert
ví	vědět	k5eAaImIp3nS	vědět
proč	proč	k6eAd1	proč
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
prince	princ	k1gMnSc2	princ
Filipa	Filip	k1gMnSc2	Filip
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
Choking	Choking	k1gInSc1	Choking
Hazard	hazard	k2eAgInSc1d1	hazard
2005	[number]	k4	2005
Sametoví	sametový	k2eAgMnPc1d1	sametový
vrazi	vrah	k1gMnPc1	vrah
2005	[number]	k4	2005
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
čtvrť	čtvrť	k1gFnSc1	čtvrť
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Bazén	bazén	k1gInSc1	bazén
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Všechno	všechen	k3xTgNnSc1	všechen
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
!	!	kIx.	!
</s>
<s>
2006	[number]	k4	2006
Naši	náš	k3xOp1gMnPc1	náš
furianti	furiant	k1gMnPc1	furiant
(	(	kIx(	(
<g/>
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Místo	místo	k1gNnSc1	místo
v	v	k7c6	v
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Letiště	letiště	k1gNnSc2	letiště
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
100	[number]	k4	100
+	+	kIx~	+
1	[number]	k4	1
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Trapasy	trapas	k1gInPc4	trapas
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
hádanka	hádanka	k1gFnSc1	hádanka
2007	[number]	k4	2007
Hraběnky	hraběnka	k1gFnPc1	hraběnka
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Hodina	hodina	k1gFnSc1	hodina
klavíru	klavír	k1gInSc2	klavír
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Líbáš	líbat	k5eAaImIp2nS	líbat
jako	jako	k8xS	jako
Bůh	bůh	k1gMnSc1	bůh
2008	[number]	k4	2008
Les	les	k1gInSc4	les
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Herbert	Herbert	k1gInSc1	Herbert
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Děti	dítě	k1gFnPc4	dítě
noci	noc	k1gFnSc2	noc
2008	[number]	k4	2008
David	David	k1gMnSc1	David
a	a	k8xC	a
Goliáš	Goliáš	k1gMnSc1	Goliáš
aneb	aneb	k?	aneb
Pepička	Pepička	k1gFnSc1	Pepička
to	ten	k3xDgNnSc4	ten
zařídí	zařídit	k5eAaPmIp3nS	zařídit
(	(	kIx(	(
<g/>
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Bathory	Bathora	k1gFnSc2	Bathora
2009	[number]	k4	2009
Láska	láska	k1gFnSc1	láska
za	za	k7c4	za
milion	milion	k4xCgInSc4	milion
2009	[number]	k4	2009
Jménem	jméno	k1gNnSc7	jméno
krále	král	k1gMnSc4	král
2009	[number]	k4	2009
Cukrárna	cukrárna	k1gFnSc1	cukrárna
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Cesty	cesta	k1gFnSc2	cesta
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
vraždy	vražda	k1gFnPc4	vražda
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
Expozitura	expozitura	k1gFnSc1	expozitura
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
Babovřesky	Babovřeska	k1gFnPc1	Babovřeska
...	...	k?	...
<g/>
aneb	aneb	k?	aneb
z	z	k7c2	z
dopisu	dopis	k1gInSc2	dopis
venkovské	venkovský	k2eAgFnSc2d1	venkovská
drbny	drbna	k1gFnSc2	drbna
2014	[number]	k4	2014
Babovřesky	Babovřeska	k1gFnSc2	Babovřeska
2	[number]	k4	2
2015	[number]	k4	2015
Babovřesky	Babovřeska	k1gFnSc2	Babovřeska
3	[number]	k4	3
2015	[number]	k4	2015
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
2016	[number]	k4	2016
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
kuře	kura	k1gFnSc3	kura
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
Rapl	Rapl	k?	Rapl
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
George	George	k1gNnPc2	George
Tabori	Tabor	k1gFnSc2	Tabor
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
být	být	k5eAaImF	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
a	a	k8xC	a
neuštvat	uštvat	k5eNaPmF	uštvat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Magdalena	Magdalena	k1gFnSc1	Magdalena
Štulcová	Štulcová	k1gFnSc1	Štulcová
<g/>
,	,	kIx,	,
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
:	:	kIx,	:
Hynek	Hynek	k1gMnSc1	Hynek
Pekárek	Pekárek	k1gMnSc1	Pekárek
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Aleš	Aleš	k1gMnSc1	Aleš
Vrzák	Vrzák	k1gMnSc1	Vrzák
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Vypravěč	vypravěč	k1gMnSc1	vypravěč
a	a	k8xC	a
Starý	Starý	k1gMnSc1	Starý
Cvi	Cvi	k1gMnSc1	Cvi
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Somr	Somr	k1gMnSc1	Somr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladý	mladý	k2eAgMnSc1d1	mladý
Šabtaj	Šabtaj	k1gMnSc1	Šabtaj
Cvi	Cvi	k1gMnSc1	Cvi
(	(	kIx(	(
<g/>
Kamil	Kamil	k1gMnSc1	Kamil
Halbich	Halbich	k1gMnSc1	Halbich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iokaste	Iokast	k1gMnSc5	Iokast
(	(	kIx(	(
<g/>
Eliška	Eliška	k1gFnSc1	Eliška
Balzerová	Balzerová	k1gFnSc1	Balzerová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oidipus	Oidipus	k1gMnSc1	Oidipus
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
John	John	k1gMnSc1	John
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Švehlík	Švehlík	k1gMnSc1	Švehlík
<g/>
)	)	kIx)	)
a	a	k8xC	a
Amanda	Amanda	k1gFnSc1	Amanda
Lollypop	Lollypop	k1gInSc1	Lollypop
(	(	kIx(	(
<g/>
Pavla	Pavla	k1gFnSc1	Pavla
Beretová	Beretový	k2eAgFnSc1d1	Beretová
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Jan	Jan	k1gMnSc1	Jan
Dolanský	Dolanský	k1gMnSc1	Dolanský
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
