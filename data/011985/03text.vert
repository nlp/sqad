<p>
<s>
Lidice	Lidice	k1gInPc4	Lidice
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
scénář	scénář	k1gInSc4	scénář
napsal	napsat	k5eAaPmAgMnS	napsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Mahler	Mahler	k1gMnSc1	Mahler
na	na	k7c4	na
náměty	námět	k1gInPc4	námět
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
Nokturno	nokturno	k1gNnSc1	nokturno
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
byla	být	k5eAaImAgFnS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
Alici	Alice	k1gFnSc4	Alice
Nellis	Nellis	k1gFnSc2	Nellis
(	(	kIx(	(
<g/>
původní	původní	k2eAgNnSc4d1	původní
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Svobodou	Svoboda	k1gMnSc7	Svoboda
či	či	k8xC	či
Polkou	Polka	k1gFnSc7	Polka
Agnieszkou	Agnieszka	k1gFnSc7	Agnieszka
Hollandovou	Hollandový	k2eAgFnSc4d1	Hollandová
neskončila	skončit	k5eNaPmAgFnS	skončit
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
její	její	k3xOp3gFnSc3	její
nemoci	nemoc	k1gFnSc3	nemoc
(	(	kIx(	(
<g/>
borelióza	borelióza	k1gFnSc1	borelióza
<g/>
)	)	kIx)	)
ji	on	k3xPp3gFnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Petr	Petr	k1gMnSc1	Petr
Nikolaev	Nikolava	k1gFnPc2	Nikolava
<g/>
.	.	kIx.	.
<g/>
Nikolaev	Nikolaev	k1gFnSc1	Nikolaev
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jej	on	k3xPp3gMnSc4	on
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Mahlerova	Mahlerův	k2eAgFnSc1d1	Mahlerova
kniha	kniha	k1gFnSc1	kniha
Nocturno	Nocturno	k1gNnSc4	Nocturno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přistoupení	přistoupení	k1gNnSc6	přistoupení
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
na	na	k7c6	na
postu	post	k1gInSc6	post
kameramana	kameraman	k1gMnSc2	kameraman
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Smutného	Smutný	k1gMnSc2	Smutný
za	za	k7c4	za
Antonia	Antonio	k1gMnSc4	Antonio
Riestru	Riestr	k1gInSc2	Riestr
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
udělal	udělat	k5eAaPmAgMnS	udělat
i	i	k9	i
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
obsazení	obsazení	k1gNnSc6	obsazení
–	–	k?	–
Lenku	Lenka	k1gFnSc4	Lenka
Vlasákovou	Vlasáková	k1gFnSc4	Vlasáková
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Fialovou	Fialův	k2eAgFnSc7d1	Fialova
a	a	k8xC	a
Marthu	Martha	k1gFnSc4	Martha
Issovou	Issový	k2eAgFnSc7d1	Issová
Veronikou	Veronika	k1gFnSc7	Veronika
Kubařovou	Kubařův	k2eAgFnSc7d1	Kubařův
<g/>
.	.	kIx.	.
<g/>
Rozpočet	rozpočet	k1gInSc1	rozpočet
filmu	film	k1gInSc2	film
činil	činit	k5eAaImAgInS	činit
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
natáčení	natáčení	k1gNnSc1	natáčení
probíhalo	probíhat	k5eAaImAgNnS	probíhat
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
byla	být	k5eAaImAgFnS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
původně	původně	k6eAd1	původně
na	na	k7c6	na
září	září	k1gNnSc6	září
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
posunuta	posunout	k5eAaPmNgFnS	posunout
na	na	k7c4	na
červen	červen	k1gInSc4	červen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
filmu	film	k1gInSc6	film
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
také	také	k9	také
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Natáčení	natáčení	k1gNnSc2	natáčení
==	==	k?	==
</s>
</p>
<p>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
filmu	film	k1gInSc2	film
začalo	začít	k5eAaPmAgNnS	začít
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
mladoboleslavské	mladoboleslavský	k2eAgFnSc6d1	mladoboleslavská
věznici	věznice	k1gFnSc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
natáčení	natáčení	k1gNnSc2	natáčení
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Natáčet	natáčet	k5eAaImF	natáčet
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
49	[number]	k4	49
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Strašecí	Strašecí	k1gNnSc6	Strašecí
<g/>
,	,	kIx,	,
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
na	na	k7c4	na
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
scény	scéna	k1gFnSc2	scéna
z	z	k7c2	z
filmového	filmový	k2eAgNnSc2d1	filmové
kladenského	kladenský	k2eAgNnSc2d1	kladenské
kina	kino	k1gNnSc2	kino
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
filmové	filmový	k2eAgInPc4d1	filmový
Lidice	Lidice	k1gInPc4	Lidice
poslouží	posloužit	k5eAaPmIp3nS	posloužit
vesnice	vesnice	k1gFnSc1	vesnice
Chcebuz	Chcebuza	k1gFnPc2	Chcebuza
poblíž	poblíž	k6eAd1	poblíž
Štětí	štětit	k5eAaImIp3nS	štětit
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
režiséra	režisér	k1gMnSc2	režisér
filmu	film	k1gInSc2	film
svým	svůj	k3xOyFgNnSc7	svůj
rozložením	rozložení	k1gNnSc7	rozložení
Lidicím	Lidice	k1gInPc3	Lidice
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
natáčení	natáčení	k1gNnSc1	natáčení
probíhalo	probíhat	k5eAaImAgNnS	probíhat
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
natáčel	natáčet	k5eAaImAgInS	natáčet
10	[number]	k4	10
dnů	den	k1gInPc2	den
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2010	[number]	k4	2010
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dalších	další	k2eAgInPc2d1	další
devět	devět	k4xCc4	devět
dnů	den	k1gInPc2	den
pro	pro	k7c4	pro
scény	scéna	k1gFnPc4	scéna
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Praze-Podolí	Praze-Podolí	k1gNnSc6	Praze-Podolí
ještě	ještě	k6eAd1	ještě
dotočena	dotočen	k2eAgFnSc1d1	dotočena
asi	asi	k9	asi
minutová	minutový	k2eAgFnSc1d1	minutová
scéna	scéna	k1gFnSc1	scéna
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
rozpočtu	rozpočet	k1gInSc2	rozpočet
filmu	film	k1gInSc2	film
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k8xC	i
města	město	k1gNnPc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Opava	Opava	k1gFnSc1	Opava
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
Lidice	Lidice	k1gInPc4	Lidice
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
představen	představit	k5eAaPmNgInS	představit
teaser	teaser	k1gInSc1	teaser
a	a	k8xC	a
první	první	k4xOgInPc4	první
plakáty	plakát	k1gInPc4	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vaverka	Vaverka	k1gMnSc1	Vaverka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plakátech	plakát	k1gInPc6	plakát
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Roden	Roden	k2eAgMnSc1d1	Roden
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
a	a	k8xC	a
Martha	Martha	k1gMnSc1	Martha
Issová	Issová	k1gFnSc1	Issová
<g/>
.	.	kIx.	.
</s>
<s>
Dvouminutový	dvouminutový	k2eAgInSc1d1	dvouminutový
trailer	trailer	k1gInSc1	trailer
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaBmNgInS	napsat
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc4	začátek
je	být	k5eAaImIp3nS	být
posazen	posadit	k5eAaPmNgMnS	posadit
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Protagonista	protagonista	k1gMnSc1	protagonista
filmu	film	k1gInSc2	film
Šíma	Šíma	k1gMnSc1	Šíma
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
syny	syn	k1gMnPc7	syn
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Lidicích	Lidice	k1gInPc6	Lidice
<g/>
.	.	kIx.	.
</s>
<s>
Šímova	Šímův	k2eAgFnSc1d1	Šímova
žena	žena	k1gFnSc1	žena
Anežka	Anežka	k1gFnSc1	Anežka
ochrnula	ochrnout	k5eAaPmAgFnS	ochrnout
a	a	k8xC	a
Šíma	Šíma	k1gMnSc1	Šíma
si	se	k3xPyFc3	se
najde	najít	k5eAaPmIp3nS	najít
milenku	milenka	k1gFnSc4	milenka
<g/>
,	,	kIx,	,
sousedku	sousedka	k1gFnSc4	sousedka
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
o	o	k7c4	o
rodinu	rodina	k1gFnSc4	rodina
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
stará	starat	k5eAaImIp3nS	starat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Šímův	Šímův	k2eAgMnSc1d1	Šímův
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
otcův	otcův	k2eAgInSc4d1	otcův
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
milenkou	milenka	k1gFnSc7	milenka
těžce	těžce	k6eAd1	těžce
nese	nést	k5eAaImIp3nS	nést
a	a	k8xC	a
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
vyprovokuje	vyprovokovat	k5eAaPmIp3nS	vyprovokovat
rvačku	rvačka	k1gFnSc4	rvačka
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
ho	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
je	být	k5eAaImIp3nS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
ke	k	k7c3	k
čtyřem	čtyři	k4xCgInPc3	čtyři
letům	let	k1gInPc3	let
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
jiný	jiný	k2eAgMnSc1d1	jiný
obyvatel	obyvatel	k1gMnSc1	obyvatel
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
Pepík	Pepík	k1gMnSc1	Pepík
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
mimomanželský	mimomanželský	k2eAgInSc1d1	mimomanželský
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
Aničkou	Anička	k1gFnSc7	Anička
a	a	k8xC	a
maskuje	maskovat	k5eAaBmIp3nS	maskovat
ho	on	k3xPp3gInSc4	on
předstíraným	předstíraný	k2eAgInSc7d1	předstíraný
odbojem	odboj	k1gInSc7	odboj
proti	proti	k7c3	proti
okupantům	okupant	k1gMnPc3	okupant
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
chce	chtít	k5eAaImIp3nS	chtít
poměr	poměr	k1gInSc1	poměr
ukončit	ukončit	k5eAaPmF	ukončit
(	(	kIx(	(
<g/>
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
lepšího	dobrý	k2eAgNnSc2d2	lepší
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
odboji	odboj	k1gInSc6	odboj
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
zmizet	zmizet	k5eAaPmF	zmizet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
místo	místo	k6eAd1	místo
Aničce	Anička	k1gFnSc3	Anička
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
gestapa	gestapo	k1gNnSc2	gestapo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
využit	využít	k5eAaPmNgInS	využít
jako	jako	k8xC	jako
záminka	záminka	k1gFnSc1	záminka
k	k	k7c3	k
represím	represe	k1gFnPc3	represe
<g/>
,	,	kIx,	,
Lidice	Lidice	k1gInPc1	Lidice
jsou	být	k5eAaImIp3nP	být
vypáleny	vypálit	k5eAaPmNgInP	vypálit
a	a	k8xC	a
srovnány	srovnat	k5eAaPmNgInP	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
odvlečeny	odvlečen	k2eAgFnPc1d1	odvlečena
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
dány	dán	k2eAgFnPc1d1	dána
na	na	k7c6	na
poněmčení	poněmčení	k1gNnSc6	poněmčení
nebo	nebo	k8xC	nebo
povražděny	povražděn	k2eAgInPc4d1	povražděn
<g/>
.	.	kIx.	.
</s>
<s>
Šíma	Šíma	k1gMnSc1	Šíma
se	se	k3xPyFc4	se
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
nic	nic	k3yNnSc1	nic
nedozví	dozvědět	k5eNaPmIp3nS	dozvědět
a	a	k8xC	a
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
šokován	šokovat	k5eAaBmNgMnS	šokovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Scénář	scénář	k1gInSc1	scénář
==	==	k?	==
</s>
</p>
<p>
<s>
Námět	námět	k1gInSc1	námět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
částečně	částečně	k6eAd1	částečně
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
vyhlazení	vyhlazení	k1gNnSc2	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Mahler	Mahler	k1gMnSc1	Mahler
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
U	u	k7c2	u
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mlýn	mlýn	k1gInSc1	mlýn
inscenována	inscenován	k2eAgFnSc1d1	inscenována
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
režisérem	režisér	k1gMnSc7	režisér
Otomarem	Otomar	k1gMnSc7	Otomar
Krejčou	Krejča	k1gMnSc7	Krejča
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Mahler	Mahler	k1gMnSc1	Mahler
o	o	k7c6	o
Lidicích	Lidice	k1gInPc6	Lidice
také	také	k9	také
napsal	napsat	k5eAaBmAgInS	napsat
televizní	televizní	k2eAgFnSc4d1	televizní
hru	hra	k1gFnSc4	hra
Jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žijete	žít	k5eAaImIp2nP	žít
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Mahlera	Mahler	k1gMnSc2	Mahler
František	František	k1gMnSc1	František
Saidl	Saidl	k1gMnSc1	Saidl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
vyhlazení	vyhlazení	k1gNnSc4	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
odpykával	odpykávat	k5eAaImAgInS	odpykávat
trest	trest	k1gInSc4	trest
za	za	k7c4	za
usmrcení	usmrcení	k1gNnSc4	usmrcení
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Primus	primus	k1gInSc1	primus
Mahlerovu	Mahlerův	k2eAgFnSc4d1	Mahlerova
novelu	novela	k1gFnSc4	novela
Nokturno	nokturno	k1gNnSc4	nokturno
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
získal	získat	k5eAaPmAgMnS	získat
Cenu	cena	k1gFnSc4	cena
Sazky	Sazka	k1gFnSc2	Sazka
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dosud	dosud	k6eAd1	dosud
nerealizovaný	realizovaný	k2eNgInSc4d1	nerealizovaný
filmový	filmový	k2eAgInSc4d1	filmový
scénář	scénář	k1gInSc4	scénář
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ankety	anketa	k1gFnSc2	anketa
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
tragické	tragický	k2eAgFnSc2d1	tragická
události	událost	k1gFnSc2	událost
chtěl	chtít	k5eAaImAgMnS	chtít
natočil	natočit	k5eAaBmAgMnS	natočit
už	už	k9	už
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
i	i	k8xC	i
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
již	již	k6eAd1	již
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Mahlerem	Mahler	k1gMnSc7	Mahler
<g/>
.	.	kIx.	.
</s>
<s>
Forman	Forman	k1gMnSc1	Forman
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
linii	linie	k1gFnSc4	linie
filmu	film	k1gInSc2	film
vylíčit	vylíčit	k5eAaPmF	vylíčit
osud	osud	k1gInSc4	osud
Pepíka	Pepík	k1gMnSc4	Pepík
Fialy	fiala	k1gFnSc2	fiala
a	a	k8xC	a
tak	tak	k6eAd1	tak
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
mohli	moct	k5eAaImAgMnP	moct
film	film	k1gInSc4	film
natočit	natočit	k5eAaBmF	natočit
také	také	k9	také
Ján	Ján	k1gMnSc1	Ján
Kadár	Kadár	k1gMnSc1	Kadár
s	s	k7c7	s
Elmarem	Elmar	k1gMnSc7	Elmar
Klosem	Klos	k1gMnSc7	Klos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soundtrack	soundtrack	k1gInSc4	soundtrack
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
filmu	film	k1gInSc3	film
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
kolekce	kolekce	k1gFnSc1	kolekce
písniček	písnička	k1gFnPc2	písnička
vydaná	vydaný	k2eAgFnSc1d1	vydaná
samostatně	samostatně	k6eAd1	samostatně
na	na	k7c6	na
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Harries	Harries	k1gMnSc1	Harries
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
<g/>
,	,	kIx,	,
Photolab	Photolab	k1gMnSc1	Photolab
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Hrůza	Hrůza	k1gMnSc1	Hrůza
<g/>
,	,	kIx,	,
Aneta	Aneta	k1gFnSc1	Aneta
Langerová	Langerová	k1gFnSc1	Langerová
<g/>
,	,	kIx,	,
Xindl	Xindl	k1gFnSc1	Xindl
X	X	kA	X
<g/>
,	,	kIx,	,
Vypsaná	vypsaný	k2eAgNnPc4d1	vypsané
fiXa	fixum	k1gNnPc4	fixum
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Lota	Lota	k1gFnSc1	Lota
<g/>
,	,	kIx,	,
X-Core	X-Cor	k1gMnSc5	X-Cor
<g/>
,	,	kIx,	,
Dorota	Dorota	k1gFnSc1	Dorota
Bárová	Bárová	k1gFnSc1	Bárová
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Cirkus	cirkus	k1gInSc1	cirkus
a	a	k8xC	a
Tobi	Tobi	k1gNnSc1	Tobi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
festivalech	festival	k1gInPc6	festival
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
promítal	promítat	k5eAaImAgInS	promítat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Týdne	týden	k1gInSc2	týden
českých	český	k2eAgInPc2d1	český
filmů	film	k1gInPc2	film
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
uveden	uvést	k5eAaPmNgMnS	uvést
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
holocaust	holocaust	k1gInSc4	holocaust
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
byl	být	k5eAaImAgInS	být
premiérově	premiérově	k6eAd1	premiérově
uveden	uveden	k2eAgInSc1d1	uveden
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
lidické	lidický	k2eAgFnSc2d1	Lidická
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgInS	vybrat
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
soutěže	soutěž	k1gFnSc2	soutěž
Evropských	evropský	k2eAgFnPc2d1	Evropská
filmových	filmový	k2eAgFnPc2d1	filmová
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ankety	anketa	k1gFnSc2	anketa
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
2011	[number]	k4	2011
získal	získat	k5eAaPmAgInS	získat
nominace	nominace	k1gFnSc2	nominace
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Karla	Karel	k1gMnSc4	Karel
Rodena	Roden	k1gMnSc4	Roden
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
a	a	k8xC	a
zvuk	zvuk	k1gInSc1	zvuk
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
také	také	k9	také
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
plakát	plakát	k1gInSc4	plakát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vstupenka	vstupenka	k1gFnSc1	vstupenka
pro	pro	k7c4	pro
maturanty	maturant	k1gMnPc4	maturant
===	===	k?	===
</s>
</p>
<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
věnoval	věnovat	k5eAaPmAgInS	věnovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
úspěšným	úspěšný	k2eAgMnPc3d1	úspěšný
jihomoravským	jihomoravský	k2eAgMnPc3d1	jihomoravský
maturantům	maturant	k1gMnPc3	maturant
vstupenku	vstupenka	k1gFnSc4	vstupenka
na	na	k7c4	na
film	film	k1gInSc4	film
Lidice	Lidice	k1gInPc4	Lidice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vlastního	vlastní	k2eAgInSc2d1	vlastní
zájmu	zájem	k1gInSc2	zájem
mohli	moct	k5eAaImAgMnP	moct
seznámit	seznámit	k5eAaPmF	seznámit
s	s	k7c7	s
lidickou	lidický	k2eAgFnSc7d1	Lidická
tragédií	tragédie	k1gFnSc7	tragédie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Recenze	recenze	k1gFnSc1	recenze
==	==	k?	==
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Cinepur	Cinepur	k1gMnSc1	Cinepur
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Kinobox	Kinobox	k1gInSc1	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Alena	Alena	k1gFnSc1	Alena
Prokopová	Prokopová	k1gFnSc1	Prokopová
<g/>
,	,	kIx,	,
Alenaprokopova	Alenaprokopův	k2eAgFnSc1d1	Alenaprokopův
<g/>
.	.	kIx.	.
<g/>
blogspot	blogspot	k1gMnSc1	blogspot
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Fuka	Fuka	k1gMnSc1	Fuka
<g/>
,	,	kIx,	,
FFFilm	FFFilm	k1gMnSc1	FFFilm
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Kamil	Kamil	k1gMnSc1	Kamil
Fila	Fila	k1gMnSc1	Fila
<g/>
,	,	kIx,	,
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Spáčilová	Spáčilová	k1gFnSc1	Spáčilová
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Moviescreen	Moviescreen	k2eAgMnSc1d1	Moviescreen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Karolína	Karolína	k1gFnSc1	Karolína
Černá	Černá	k1gFnSc1	Černá
<g/>
,	,	kIx,	,
Film	film	k1gInSc1	film
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Saidl	Saidl	k1gMnSc1	Saidl
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Lidice	Lidice	k1gInPc1	Lidice
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
filmu	film	k1gInSc2	film
Lidice	Lidice	k1gInPc1	Lidice
</s>
</p>
<p>
<s>
Lidice	Lidice	k1gInPc1	Lidice
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Lidice	Lidice	k1gInPc1	Lidice
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
