<s>
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
byl	být	k5eAaImAgInS	být
jejím	její	k3xOp3gMnSc7	její
jediným	jediný	k2eAgMnSc7d1	jediný
vlastníkem	vlastník	k1gMnSc7	vlastník
a	a	k8xC	a
jednatelem	jednatel	k1gMnSc7	jednatel
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančura	k1gFnSc1	Jančura
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
se	s	k7c7	s
statutárním	statutární	k2eAgInSc7d1	statutární
orgánem	orgán	k1gInSc7	orgán
–	–	k?	–
komplementářem	komplementář	k1gMnSc7	komplementář
–	–	k?	–
stala	stát	k5eAaPmAgFnS	stát
mateřská	mateřský	k2eAgFnSc1d1	mateřská
společnost	společnost	k1gFnSc1	společnost
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k9	již
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
98	[number]	k4	98
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
společníkem-komanditistou	společníkemomanditista	k1gMnSc7	společníkem-komanditista
se	s	k7c7	s
vkladem	vklad	k1gInSc7	vklad
20	[number]	k4	20
000	[number]	k4	000
Kč	Kč	kA	Kč
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
<g/>
.	.	kIx.	.
</s>
