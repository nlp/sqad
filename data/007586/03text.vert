<s>
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
k.s.	k.s.	k?	k.s.
(	(	kIx(	(
<g/>
IČ	IČ	kA	IČ
25317075	[number]	k4	25317075
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k8xC	jako
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
transformována	transformovat	k5eAaBmNgFnS	transformovat
na	na	k7c4	na
komanditní	komanditní	k2eAgFnSc4d1	komanditní
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
byl	být	k5eAaImAgInS	být
jejím	její	k3xOp3gMnSc7	její
jediným	jediný	k2eAgMnSc7d1	jediný
vlastníkem	vlastník	k1gMnSc7	vlastník
a	a	k8xC	a
jednatelem	jednatel	k1gMnSc7	jednatel
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančura	k1gFnSc1	Jančura
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
se	s	k7c7	s
statutárním	statutární	k2eAgInSc7d1	statutární
orgánem	orgán	k1gInSc7	orgán
–	–	k?	–
komplementářem	komplementář	k1gMnSc7	komplementář
–	–	k?	–
stala	stát	k5eAaPmAgFnS	stát
mateřská	mateřský	k2eAgFnSc1d1	mateřská
společnost	společnost	k1gFnSc1	společnost
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
již	již	k9	již
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
98	[number]	k4	98
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
společníkem-komanditistou	společníkemomanditista	k1gMnSc7	společníkem-komanditista
se	s	k7c7	s
vkladem	vklad	k1gInSc7	vklad
20	[number]	k4	20
000	[number]	k4	000
Kč	Kč	kA	Kč
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
<g/>
.	.	kIx.	.
</s>
<s>
Tržby	tržba	k1gFnPc1	tržba
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
výše	vysoce	k6eAd2	vysoce
3,5	[number]	k4	3,5
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
měl	mít	k5eAaImAgInS	mít
prodej	prodej	k1gInSc4	prodej
letenek	letenka	k1gFnPc2	letenka
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc2	ubytování
a	a	k8xC	a
doplňkových	doplňkový	k2eAgFnPc2d1	doplňková
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
66	[number]	k4	66
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
dopravou	doprava	k1gFnSc7	doprava
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
studijními	studijní	k2eAgInPc7d1	studijní
pobyty	pobyt	k1gInPc7	pobyt
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
školními	školní	k2eAgInPc7d1	školní
zájezdy	zájezd	k1gInPc7	zájezd
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
pracovními	pracovní	k2eAgFnPc7d1	pracovní
a	a	k8xC	a
au	au	k0	au
pair	pair	k1gMnSc1	pair
programy	program	k1gInPc7	program
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrálu	centrála	k1gFnSc4	centrála
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
renesančním	renesanční	k2eAgInSc6d1	renesanční
Domě	dům	k1gInSc6	dům
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Jančurova	Jančurův	k2eAgFnSc1d1	Jančurova
společnost	společnost	k1gFnSc1	společnost
DPL	DPL	kA	DPL
Real	Real	k1gInSc1	Real
<g/>
.	.	kIx.	.
</s>
<s>
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
holding	holding	k1gInSc1	holding
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
IČ	IČ	kA	IČ
29379261	[number]	k4	29379261
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
jediným	jediný	k2eAgMnSc7d1	jediný
akcionářem	akcionář	k1gMnSc7	akcionář
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
členem	člen	k1gMnSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
je	být	k5eAaImIp3nS	být
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
2	[number]	k4	2
akcie	akcie	k1gFnPc4	akcie
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
od	od	k7c2	od
navýšení	navýšení	k1gNnSc2	navýšení
základního	základní	k2eAgInSc2d1	základní
kapitálu	kapitál	k1gInSc2	kapitál
k	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2013	[number]	k4	2013
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
454	[number]	k4	454
takovýchto	takovýto	k3xDgFnPc2	takovýto
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kapitál	kapitál	k1gInSc1	kapitál
byl	být	k5eAaImAgInS	být
navýšen	navýšit	k5eAaPmNgInS	navýšit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
započtení	započtení	k1gNnSc2	započtení
pohledávek	pohledávka	k1gFnPc2	pohledávka
společnosti	společnost	k1gFnSc2	společnost
z	z	k7c2	z
převodu	převod	k1gInSc2	převod
obchodních	obchodní	k2eAgInPc2d1	obchodní
podílů	podíl	k1gInPc2	podíl
Radima	Radim	k1gMnSc2	Radim
Jančury	Jančura	k1gFnSc2	Jančura
ze	z	k7c2	z
společností	společnost	k1gFnSc7	společnost
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
ORBIX	ORBIX	kA	ORBIX
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
DPL	DPL	kA	DPL
Real	Real	k1gInSc1	Real
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
převodech	převod	k1gInPc6	převod
svých	svůj	k3xOyFgMnPc2	svůj
podílů	podíl	k1gInPc2	podíl
uzavřen	uzavřen	k2eAgMnSc1d1	uzavřen
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
holding	holding	k1gInSc1	holding
a.s.	a.s.	k?	a.s.
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2016	[number]	k4	2016
postupně	postupně	k6eAd1	postupně
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
značku	značka	k1gFnSc4	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc4	který
původně	původně	k6eAd1	původně
používal	používat	k5eAaImAgInS	používat
pro	pro	k7c4	pro
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
prý	prý	k9	prý
bude	být	k5eAaImBp3nS	být
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
srozumitelnější	srozumitelný	k2eAgFnSc1d2	srozumitelnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
příběh	příběh	k1gInSc1	příběh
jeho	jeho	k3xOp3gFnSc2	jeho
firmy	firma	k1gFnSc2	firma
lidé	člověk	k1gMnPc1	člověk
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
neznají	znát	k5eNaImIp3nP	znát
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nepraktický	praktický	k2eNgInSc1d1	nepraktický
název	název	k1gInSc1	název
pro	pro	k7c4	pro
dopravní	dopravní	k2eAgFnSc4d1	dopravní
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
zavádí	zavádět	k5eAaImIp3nP	zavádět
i	i	k9	i
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
rebranding	rebranding	k1gInSc1	rebranding
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
oficiálně	oficiálně	k6eAd1	oficiálně
zahájen	zahájit	k5eAaPmNgInS	zahájit
<g/>
,	,	kIx,	,
logo	logo	k1gNnSc4	logo
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
měněno	měnit	k5eAaImNgNnS	měnit
na	na	k7c6	na
autobusových	autobusový	k2eAgInPc6d1	autobusový
nádražích	nádraží	k1gNnPc6	nádraží
<g/>
,	,	kIx,	,
autobusech	autobus	k1gInPc6	autobus
<g/>
,	,	kIx,	,
v	v	k7c6	v
informačních	informační	k2eAgNnPc6d1	informační
hlášeních	hlášení	k1gNnPc6	hlášení
<g/>
,	,	kIx,	,
na	na	k7c6	na
informačních	informační	k2eAgInPc6d1	informační
a	a	k8xC	a
propagačních	propagační	k2eAgInPc6d1	propagační
materiálech	materiál	k1gInPc6	materiál
<g/>
,	,	kIx,	,
prodejních	prodejní	k2eAgNnPc6d1	prodejní
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
chce	chtít	k5eAaImIp3nS	chtít
RegioJet	RegioJet	k1gInSc4	RegioJet
prezentovat	prezentovat	k5eAaBmF	prezentovat
jako	jako	k9	jako
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
středoevropskou	středoevropský	k2eAgFnSc4d1	středoevropská
síť	síť	k1gFnSc4	síť
autobusových	autobusový	k2eAgInPc2d1	autobusový
a	a	k8xC	a
vlakových	vlakový	k2eAgInPc2d1	vlakový
spojů	spoj	k1gInPc2	spoj
s	s	k7c7	s
přepravními	přepravní	k2eAgFnPc7d1	přepravní
službami	služba	k1gFnPc7	služba
špičkové	špičkový	k2eAgFnSc2d1	špičková
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc7d1	původní
hlavní	hlavní	k2eAgFnSc7d1	hlavní
činností	činnost	k1gFnSc7	činnost
společnosti	společnost	k1gFnSc2	společnost
byly	být	k5eAaImAgInP	být
studijní	studijní	k2eAgInPc1d1	studijní
a	a	k8xC	a
pracovní	pracovní	k2eAgInPc1d1	pracovní
pobyty	pobyt	k1gInPc1	pobyt
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
a	a	k8xC	a
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
si	se	k3xPyFc3	se
hlídání	hlídání	k1gNnSc4	hlídání
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
rodinách	rodina	k1gFnPc6	rodina
(	(	kIx(	(
<g/>
au	au	k0	au
pair	pair	k1gMnSc1	pair
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Pobyty	pobyt	k1gInPc1	pobyt
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
tříměsíční	tříměsíční	k2eAgNnPc4d1	tříměsíční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
jeden	jeden	k4xCgInSc4	jeden
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
také	také	k9	také
pracovní	pracovní	k2eAgInPc4d1	pracovní
pobyty	pobyt	k1gInPc4	pobyt
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
pracovní	pracovní	k2eAgInPc4d1	pracovní
programy	program	k1gInPc4	program
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
a	a	k8xC	a
nekvalifikovanou	kvalifikovaný	k2eNgFnSc4d1	nekvalifikovaná
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
mladé	mladý	k1gMnPc4	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
rovněž	rovněž	k9	rovněž
studovat	studovat	k5eAaImF	studovat
cizí	cizí	k2eAgInPc4d1	cizí
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
francouzštinu	francouzština	k1gFnSc4	francouzština
nebo	nebo	k8xC	nebo
španělštinu	španělština	k1gFnSc4	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
zpočátku	zpočátku	k6eAd1	zpočátku
provozovala	provozovat	k5eAaImAgFnS	provozovat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
měst	město	k1gNnPc2	město
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Beneluxu	Benelux	k1gInSc6	Benelux
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
například	například	k6eAd1	například
tyto	tento	k3xDgFnPc4	tento
linky	linka	k1gFnPc4	linka
<g/>
:	:	kIx,	:
000010	[number]	k4	000010
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
zavedena	zaveden	k2eAgFnSc1d1	zavedena
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
000022	[number]	k4	000022
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Janov	Janov	k1gInSc4	Janov
000031	[number]	k4	000031
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
Neapol	Neapol	k1gFnSc1	Neapol
000032	[number]	k4	000032
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Vídeň	Vídeň	k1gFnSc1	Vídeň
000038	[number]	k4	000038
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Budapešť	Budapešť	k1gFnSc1	Budapešť
000039	[number]	k4	000039
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Hamburg	Hamburg	k1gInSc4	Hamburg
000042	[number]	k4	000042
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Oslo	Oslo	k1gNnSc1	Oslo
000044	[number]	k4	000044
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Bratislava	Bratislava	k1gFnSc1	Bratislava
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Nitra	Nitra	k1gFnSc1	Nitra
–	–	k?	–
Zvolen	Zvolen	k1gInSc1	Zvolen
–	–	k?	–
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
–	–	k?	–
Rožňava	Rožňava	k1gFnSc1	Rožňava
–	–	k?	–
Košice	Košice	k1gInPc1	Košice
000095	[number]	k4	000095
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ženeva	Ženeva	k1gFnSc1	Ženeva
000153	[number]	k4	000153
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Bratislava	Bratislava	k1gFnSc1	Bratislava
000170	[number]	k4	000170
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Vídeň	Vídeň	k1gFnSc1	Vídeň
000191	[number]	k4	000191
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Essen	Essen	k1gInSc4	Essen
000194	[number]	k4	000194
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Lyon	Lyon	k1gInSc4	Lyon
000195	[number]	k4	000195
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Paříž	Paříž	k1gFnSc1	Paříž
000209	[number]	k4	000209
Ostrava	Ostrava	k1gFnSc1	Ostrava
–	–	k?	–
Krakov	Krakov	k1gInSc4	Krakov
000263	[number]	k4	000263
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Kodaň	Kodaň	k1gFnSc1	Kodaň
000286	[number]	k4	000286
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Bardejov	Bardejov	k1gInSc4	Bardejov
000287	[number]	k4	000287
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Žilina	Žilina	k1gFnSc1	Žilina
–	–	k?	–
Poprad	Poprad	k1gInSc1	Poprad
–	–	k?	–
Košice	Košice	k1gInPc1	Košice
000388	[number]	k4	000388
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Varšava	Varšava	k1gFnSc1	Varšava
000540	[number]	k4	000540
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
postupně	postupně	k6eAd1	postupně
zahájila	zahájit	k5eAaPmAgFnS	zahájit
i	i	k9	i
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
linky	linka	k1gFnPc1	linka
<g/>
:	:	kIx,	:
721309	[number]	k4	721309
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
linky	linka	k1gFnSc2	linka
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
zavedeny	zavést	k5eAaPmNgInP	zavést
i	i	k9	i
spoje	spoj	k1gInPc4	spoj
se	s	k7c7	s
zastávkou	zastávka	k1gFnSc7	zastávka
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
144101	[number]	k4	144101
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Plzeň	Plzeň	k1gFnSc1	Plzeň
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
154420	[number]	k4	154420
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Liberec	Liberec	k1gInSc1	Liberec
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
191110	[number]	k4	191110
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
zrušena	zrušen	k2eAgFnSc1d1	zrušena
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
vlakem	vlak	k1gInSc7	vlak
RegioJet	RegioJet	k1gInSc4	RegioJet
142107	[number]	k4	142107
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
141103	[number]	k4	141103
do	do	k7c2	do
Sokolova	Sokolov	k1gInSc2	Sokolov
a	a	k8xC	a
Chebu	Cheb	k1gInSc2	Cheb
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
podmínce	podmínka	k1gFnSc3	podmínka
souhlasu	souhlas	k1gInSc2	souhlas
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
však	však	k9	však
vyloučena	vyloučen	k2eAgFnSc1d1	vyloučena
vnitrokrajová	vnitrokrajový	k2eAgFnSc1d1	vnitrokrajový
přeprava	přeprava	k1gFnSc1	přeprava
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
souběhu	souběh	k1gInSc2	souběh
s	s	k7c7	s
dotovanými	dotovaný	k2eAgFnPc7d1	dotovaná
linkami	linka	k1gFnPc7	linka
182101	[number]	k4	182101
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Zlín	Zlín	k1gInSc1	Zlín
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
zrušena	zrušen	k2eAgFnSc1d1	zrušena
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
vlaků	vlak	k1gInPc2	vlak
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
linkou	linka	k1gFnSc7	linka
721311	[number]	k4	721311
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
133109	[number]	k4	133109
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
-	-	kIx~	-
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
linky	linka	k1gFnSc2	linka
SA	SA	kA	SA
opakovaně	opakovaně	k6eAd1	opakovaně
oznamovala	oznamovat	k5eAaImAgFnS	oznamovat
a	a	k8xC	a
odkládala	odkládat	k5eAaImAgFnS	odkládat
<g/>
:	:	kIx,	:
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
po	po	k7c6	po
prázdninách	prázdniny	k1gFnPc6	prázdniny
linku	linka	k1gFnSc4	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
ji	on	k3xPp3gFnSc4	on
slibovala	slibovat	k5eAaImAgFnS	slibovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
uplynutí	uplynutí	k1gNnSc6	uplynutí
uváděla	uvádět	k5eAaImAgFnS	uvádět
podzim	podzim	k1gInSc4	podzim
2007	[number]	k4	2007
s	s	k7c7	s
pokračováním	pokračování	k1gNnSc7	pokračování
do	do	k7c2	do
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zveřejněném	zveřejněný	k2eAgInSc6d1	zveřejněný
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
platném	platný	k2eAgInSc6d1	platný
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
linka	linka	k1gFnSc1	linka
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
větvemi	větev	k1gFnPc7	větev
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Smíchova	Smíchov	k1gInSc2	Smíchov
přes	přes	k7c4	přes
Písek	Písek	k1gInSc4	Písek
a	a	k8xC	a
z	z	k7c2	z
Roztyl	Roztyl	k1gInSc1	Roztyl
a	a	k8xC	a
Opatova	opatův	k2eAgFnSc1d1	Opatova
přes	přes	k7c4	přes
Tábor	Tábor	k1gInSc4	Tábor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	se	k3xPyFc4	se
zahájením	zahájení	k1gNnSc7	zahájení
provozu	provoz	k1gInSc2	provoz
až	až	k6eAd1	až
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nového	nový	k2eAgInSc2d1	nový
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
platného	platný	k2eAgNnSc2d1	platné
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
i	i	k9	i
dalšího	další	k2eAgMnSc4d1	další
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
však	však	k9	však
žádný	žádný	k3yNgInSc4	žádný
spoj	spoj	k1gInSc4	spoj
nejel	jet	k5eNaImAgMnS	jet
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
SA	SA	kA	SA
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
odloží	odložit	k5eAaPmIp3nS	odložit
svůj	svůj	k3xOyFgInSc4	svůj
nástup	nástup	k1gInSc4	nástup
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
<g/>
"	"	kIx"	"
kvůli	kvůli	k7c3	kvůli
cenové	cenový	k2eAgFnSc3d1	cenová
válce	válka	k1gFnSc3	válka
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Asiana	Asiana	k1gFnSc1	Asiana
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
trase	trasa	k1gFnSc6	trasa
a	a	k8xC	a
souvisejícímu	související	k2eAgNnSc3d1	související
posílení	posílení	k1gNnSc3	posílení
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
systému	systém	k1gInSc6	systém
IDOS	IDOS	kA	IDOS
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
nový	nový	k2eAgInSc1d1	nový
jízdní	jízdní	k2eAgInSc1d1	jízdní
řád	řád	k1gInSc4	řád
platný	platný	k2eAgInSc4d1	platný
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
čtyři	čtyři	k4xCgInPc4	čtyři
páry	pár	k1gInPc1	pár
spojů	spoj	k1gInPc2	spoj
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
přes	přes	k7c4	přes
Písek	Písek	k1gInSc4	Písek
mají	mít	k5eAaImIp3nP	mít
zahájit	zahájit	k5eAaPmF	zahájit
provoz	provoz	k1gInSc4	provoz
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
spojů	spoj	k1gInPc2	spoj
přes	přes	k7c4	přes
Tábor	Tábor	k1gInSc4	Tábor
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
platnosti	platnost	k1gFnSc2	platnost
JŘ	JŘ	kA	JŘ
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
nejede	jet	k5eNaImIp3nS	jet
<g/>
.	.	kIx.	.
161220	[number]	k4	161220
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
linku	linka	k1gFnSc4	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
zavede	zavést	k5eAaPmIp3nS	zavést
po	po	k7c6	po
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2007	[number]	k4	2007
zmínil	zmínit	k5eAaPmAgInS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
uvažovaných	uvažovaný	k2eAgFnPc6d1	uvažovaná
autobusových	autobusový	k2eAgFnPc6d1	autobusová
linkách	linka	k1gFnPc6	linka
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
nebo	nebo	k8xC	nebo
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
jezdit	jezdit	k5eAaImF	jezdit
autobusy	autobus	k1gInPc1	autobus
stažené	stažený	k2eAgInPc1d1	stažený
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
z	z	k7c2	z
trasy	trasa	k1gFnSc2	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
linkou	linka	k1gFnSc7	linka
vedoucí	vedoucí	k1gFnSc2	vedoucí
až	až	k6eAd1	až
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
721308	[number]	k4	721308
Karviná	Karviná	k1gFnSc1	Karviná
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ostrava	Ostrava	k1gFnSc1	Ostrava
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
od	od	k7c2	od
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
úprava	úprava	k1gFnSc1	úprava
linky	linka	k1gFnSc2	linka
Ostrava	Ostrava	k1gFnSc1	Ostrava
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
vlakové	vlakový	k2eAgNnSc4d1	vlakové
spojení	spojení	k1gNnSc4	spojení
IC	IC	kA	IC
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
.	.	kIx.	.
721312	[number]	k4	721312
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
varianty	varianta	k1gFnPc1	varianta
<g />
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
–	–	k?	–
Tábor	Tábor	k1gInSc1	Tábor
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Třebíč	Třebíč	k1gFnSc1	Třebíč
–	–	k?	–
Telč	Telč	k1gFnSc1	Telč
–	–	k?	–
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
po	po	k7c4	po
1	[number]	k4	1
páru	pára	k1gFnSc4	pára
spojů	spoj	k1gInPc2	spoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
přibýt	přibýt	k5eAaPmF	přibýt
zastávka	zastávka	k1gFnSc1	zastávka
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
.	.	kIx.	.
152101	[number]	k4	152101
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
–	–	k?	–
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
169223	[number]	k4	169223
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
–	–	k?	–
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
–	–	k?	–
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
–	–	k?	–
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
linka	linka	k1gFnSc1	linka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
prodloužením	prodloužení	k1gNnSc7	prodloužení
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
linky	linka	k1gFnSc2	linka
161220	[number]	k4	161220
183102	[number]	k4	183102
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Humpolec	Humpolec	k1gInSc1	Humpolec
–	–	k?	–
Jihlava	Jihlava	k1gFnSc1	Jihlava
–	–	k?	–
Želetava	Želetava	k1gFnSc1	Želetava
–	–	k?	–
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
–	–	k?	–
Pavlice	Pavlice	k1gFnSc2	Pavlice
–	–	k?	–
Vranovská	vranovský	k2eAgFnSc1d1	Vranovská
Ves	ves	k1gFnSc1	ves
–	–	k?	–
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
zavedla	zavést	k5eAaPmAgFnS	zavést
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
autobusových	autobusový	k2eAgFnPc6d1	autobusová
linkách	linka	k1gFnPc6	linka
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
předtím	předtím	k6eAd1	předtím
nebyly	být	k5eNaImAgFnP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
běžné	běžný	k2eAgFnSc6d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
autobusech	autobus	k1gInPc6	autobus
jsou	být	k5eAaImIp3nP	být
zdarma	zdarma	k6eAd1	zdarma
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
časopisy	časopis	k1gInPc4	časopis
(	(	kIx(	(
<g/>
získávané	získávaný	k2eAgInPc1d1	získávaný
z	z	k7c2	z
remitendy	remitenda	k1gFnSc2	remitenda
distributorů	distributor	k1gMnPc2	distributor
<g/>
)	)	kIx)	)
a	a	k8xC	a
denní	denní	k2eAgInSc1d1	denní
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
film	film	k1gInSc4	film
nebo	nebo	k8xC	nebo
poslouchat	poslouchat	k5eAaImF	poslouchat
hudbu	hudba	k1gFnSc4	hudba
do	do	k7c2	do
sluchátek	sluchátko	k1gNnPc2	sluchátko
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
z	z	k7c2	z
osmi	osm	k4xCc2	osm
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
cestujícím	cestující	k1gMnPc3	cestující
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
stevard	stevard	k1gMnSc1	stevard
či	či	k8xC	či
stevardka	stevardka	k1gFnSc1	stevardka
a	a	k8xC	a
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
jízdného	jízdné	k1gNnSc2	jízdné
je	být	k5eAaImIp3nS	být
zahrnutá	zahrnutý	k2eAgFnSc1d1	zahrnutá
i	i	k8xC	i
káva	káva	k1gFnSc1	káva
či	či	k8xC	či
jiné	jiný	k2eAgInPc1d1	jiný
teplé	teplý	k2eAgInPc1d1	teplý
nápoje	nápoj	k1gInPc1	nápoj
<g/>
,	,	kIx,	,
studené	studený	k2eAgInPc1d1	studený
nápoje	nápoj	k1gInPc1	nápoj
lze	lze	k6eAd1	lze
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
zakoupit	zakoupit	k5eAaPmF	zakoupit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
SA	SA	kA	SA
nasadila	nasadit	k5eAaPmAgFnS	nasadit
nižší	nízký	k2eAgFnPc4d2	nižší
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
donutila	donutit	k5eAaPmAgFnS	donutit
i	i	k9	i
ostatní	ostatní	k2eAgMnPc4d1	ostatní
dopravce	dopravce	k1gMnPc4	dopravce
včetně	včetně	k7c2	včetně
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
cen	cena	k1gFnPc2	cena
nebo	nebo	k8xC	nebo
speciálním	speciální	k2eAgFnPc3d1	speciální
relačním	relační	k2eAgFnPc3d1	relační
slevám	sleva	k1gFnPc3	sleva
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
cen	cena	k1gFnPc2	cena
musel	muset	k5eAaImAgMnS	muset
majoritní	majoritní	k2eAgMnSc1d1	majoritní
dopravce	dopravce	k1gMnSc1	dopravce
přistoupit	přistoupit	k5eAaPmF	přistoupit
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
občerstvení	občerstvení	k1gNnSc2	občerstvení
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
Čebus	Čebus	k1gInSc1	Čebus
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
trasu	tras	k1gInSc2	tras
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
spoje	spoj	k1gInPc4	spoj
převzala	převzít	k5eAaPmAgFnS	převzít
firma	firma	k1gFnSc1	firma
Český	český	k2eAgInSc4d1	český
národní	národní	k2eAgInSc4d1	národní
expres	expres	k1gInSc4	expres
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jich	on	k3xPp3gMnPc2	on
poté	poté	k6eAd1	poté
většinu	většina	k1gFnSc4	většina
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
zavedla	zavést	k5eAaPmAgFnS	zavést
nové	nový	k2eAgFnPc4d1	nová
typy	typa	k1gFnPc4	typa
placení	placení	k1gNnPc2	placení
jízdenek	jízdenka	k1gFnPc2	jízdenka
a	a	k8xC	a
rezervace	rezervace	k1gFnSc2	rezervace
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
otevřené	otevřený	k2eAgFnPc4d1	otevřená
nebo	nebo	k8xC	nebo
kreditové	kreditový	k2eAgFnPc4d1	kreditová
jízdenky	jízdenka	k1gFnPc4	jízdenka
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
předplacení	předplacení	k1gNnSc6	předplacení
lze	lze	k6eAd1	lze
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
spoj	spoj	k1gInSc4	spoj
rezervovat	rezervovat	k5eAaBmF	rezervovat
přes	přes	k7c4	přes
web	web	k1gInSc4	web
nebo	nebo	k8xC	nebo
po	po	k7c6	po
SMS	SMS	kA	SMS
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
elektronického	elektronický	k2eAgNnSc2d1	elektronické
mýtného	mýtné	k1gNnSc2	mýtné
společnost	společnost	k1gFnSc1	společnost
zdražuje	zdražovat	k5eAaImIp3nS	zdražovat
jízdné	jízdné	k1gNnSc4	jízdné
o	o	k7c4	o
15	[number]	k4	15
%	%	kIx~	%
a	a	k8xC	a
přestává	přestávat	k5eAaImIp3nS	přestávat
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zrušeny	zrušen	k2eAgFnPc1d1	zrušena
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
linky	linka	k1gFnPc1	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
provozující	provozující	k2eAgFnSc4d1	provozující
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
již	již	k6eAd1	již
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
,	,	kIx,	,
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
nejkratších	krátký	k2eAgFnPc6d3	nejkratší
linkách	linka	k1gFnPc6	linka
(	(	kIx(	(
<g/>
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
<g/>
,	,	kIx,	,
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
ruší	rušit	k5eAaImIp3nS	rušit
stevardy	stevard	k1gMnPc4	stevard
<g/>
,	,	kIx,	,
roznášející	roznášející	k2eAgInSc1d1	roznášející
denní	denní	k2eAgInSc1d1	denní
tisk	tisk	k1gInSc1	tisk
či	či	k8xC	či
horké	horký	k2eAgInPc1d1	horký
nápoje	nápoj	k1gInPc1	nápoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
je	být	k5eAaImIp3nS	být
personálem	personál	k1gInSc7	personál
v	v	k7c6	v
nástupních	nástupní	k2eAgFnPc6d1	nástupní
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
cestujícím	cestující	k1gMnPc3	cestující
vydá	vydat	k5eAaPmIp3nS	vydat
sluchátka	sluchátko	k1gNnSc2	sluchátko
<g/>
,	,	kIx,	,
balenou	balený	k2eAgFnSc4d1	balená
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
denní	denní	k2eAgInSc4d1	denní
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
opatření	opatření	k1gNnSc4	opatření
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
systém	systém	k1gInSc1	systém
obsluhy	obsluha	k1gFnSc2	obsluha
a	a	k8xC	a
odbavení	odbavení	k1gNnSc2	odbavení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
byly	být	k5eAaImAgFnP	být
bez	bez	k7c2	bez
palubního	palubní	k2eAgInSc2d1	palubní
personálu	personál	k1gInSc2	personál
pouze	pouze	k6eAd1	pouze
posilové	posilový	k2eAgInPc4d1	posilový
spoje	spoj	k1gInPc4	spoj
a	a	k8xC	a
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
dopravce	dopravce	k1gMnSc1	dopravce
provozoval	provozovat	k5eAaImAgMnS	provozovat
pro	pro	k7c4	pro
Deutsche	Deutsche	k1gNnSc4	Deutsche
Bahn	Bahna	k1gFnPc2	Bahna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
dopravce	dopravce	k1gMnSc1	dopravce
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
dopravcem	dopravce	k1gMnSc7	dopravce
Deutsche	Deutsch	k1gMnSc2	Deutsch
Bahn	Bahn	k1gMnSc1	Bahn
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
spolu	spolu	k6eAd1	spolu
spouští	spouštět	k5eAaImIp3nS	spouštět
autobusové	autobusový	k2eAgNnSc4d1	autobusové
spojení	spojení	k1gNnSc4	spojení
DB	db	kA	db
Expressbus	Expressbus	k1gInSc4	Expressbus
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Mnichov	Mnichov	k1gInSc1	Mnichov
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
denně	denně	k6eAd1	denně
s	s	k7c7	s
jízdní	jízdní	k2eAgFnSc7d1	jízdní
dobou	doba	k1gFnSc7	doba
3	[number]	k4	3
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
linku	linka	k1gFnSc4	linka
má	mít	k5eAaImIp3nS	mít
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
nasadit	nasadit	k5eAaPmF	nasadit
nové	nový	k2eAgInPc4d1	nový
autobusy	autobus	k1gInPc4	autobus
Fun	Fun	k1gFnSc2	Fun
<g/>
&	&	k?	&
<g/>
Relax	Relax	k1gInSc1	Relax
v	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
provedení	provedení	k1gNnSc6	provedení
DB	db	kA	db
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
<g/>
,	,	kIx,	,
že	že	k8xS	že
linka	linka	k1gFnSc1	linka
je	být	k5eAaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	se	k3xPyFc4	se
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lince	linka	k1gFnSc6	linka
platí	platit	k5eAaImIp3nS	platit
nabídka	nabídka	k1gFnSc1	nabídka
tarifů	tarif	k1gInPc2	tarif
Europa-Spezial	Europa-Spezial	k1gInSc1	Europa-Spezial
<g/>
,	,	kIx,	,
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
úseku	úsek	k1gInSc6	úsek
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
kombinace	kombinace	k1gFnSc1	kombinace
s	s	k7c7	s
vnitrostátní	vnitrostátní	k2eAgFnSc7d1	vnitrostátní
přepravou	přeprava	k1gFnSc7	přeprava
DB	db	kA	db
a	a	k8xC	a
kartou	karta	k1gFnSc7	karta
BahnCard	BahnCarda	k1gFnPc2	BahnCarda
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
úseku	úsek	k1gInSc6	úsek
platí	platit	k5eAaImIp3nS	platit
25	[number]	k4	25
<g/>
%	%	kIx~	%
sleva	sleva	k1gFnSc1	sleva
RAILPLUS	RAILPLUS	kA	RAILPLUS
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
jízdenky	jízdenka	k1gFnPc4	jízdenka
<g/>
,	,	kIx,	,
místenky	místenka	k1gFnPc1	místenka
jsou	být	k5eAaImIp3nP	být
povinné	povinný	k2eAgFnPc1d1	povinná
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
zahrnuta	zahrnout	k5eAaPmNgFnS	zahrnout
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
jízdenky	jízdenka	k1gFnPc1	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Obdobná	obdobný	k2eAgFnSc1d1	obdobná
spolupráce	spolupráce	k1gFnSc1	spolupráce
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Norimberk	Norimberk	k1gInSc1	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
linkách	linka	k1gFnPc6	linka
jsou	být	k5eAaImIp3nP	být
nasazovány	nasazován	k2eAgInPc1d1	nasazován
patrové	patrový	k2eAgInPc1d1	patrový
autobusy	autobus	k1gInPc1	autobus
typu	typ	k1gInSc2	typ
Setra	setr	k1gMnSc2	setr
S	s	k7c7	s
431	[number]	k4	431
<g/>
DT	DT	kA	DT
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
doprava	doprava	k6eAd1	doprava
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
španělskými	španělský	k2eAgInPc7d1	španělský
autobusy	autobus	k1gInPc7	autobus
značky	značka	k1gFnSc2	značka
Ayats	Ayatsa	k1gFnPc2	Ayatsa
Atlantis	Atlantis	k1gFnSc1	Atlantis
moderního	moderní	k2eAgInSc2d1	moderní
vzhledu	vzhled	k1gInSc2	vzhled
(	(	kIx(	(
<g/>
na	na	k7c6	na
podvozku	podvozek	k1gInSc6	podvozek
MAN	mana	k1gFnPc2	mana
<g/>
)	)	kIx)	)
a	a	k8xC	a
nápadného	nápadný	k2eAgInSc2d1	nápadný
žlutého	žlutý	k2eAgInSc2d1	žlutý
nátěru	nátěr	k1gInSc2	nátěr
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
však	však	k9	však
již	již	k6eAd1	již
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přibyly	přibýt	k5eAaPmAgInP	přibýt
španělské	španělský	k2eAgInPc1d1	španělský
autobusy	autobus	k1gInPc1	autobus
Beulas	Beulasa	k1gFnPc2	Beulasa
(	(	kIx(	(
<g/>
na	na	k7c6	na
podvozku	podvozek	k1gInSc6	podvozek
Scania	Scanium	k1gNnSc2	Scanium
a	a	k8xC	a
Volvo	Volvo	k1gNnSc1	Volvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
i	i	k9	i
pět	pět	k4xCc1	pět
autobusů	autobus	k1gInPc2	autobus
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
Travego	Travego	k6eAd1	Travego
<g/>
,	,	kIx,	,
nasazovaných	nasazovaný	k2eAgMnPc2d1	nasazovaný
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
firma	firma	k1gFnSc1	firma
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
25	[number]	k4	25
nových	nový	k2eAgInPc2d1	nový
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
především	především	k9	především
značky	značka	k1gFnPc1	značka
Beulas	Beulasa	k1gFnPc2	Beulasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
plánovala	plánovat	k5eAaImAgFnS	plánovat
nákup	nákup	k1gInSc4	nákup
dalších	další	k2eAgInPc2d1	další
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
společnost	společnost	k1gFnSc1	společnost
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
cca	cca	kA	cca
110	[number]	k4	110
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
5	[number]	k4	5
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
a	a	k8xC	a
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
provozovala	provozovat	k5eAaImAgFnS	provozovat
linky	linka	k1gFnSc2	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Norimberk	Norimberk	k1gInSc1	Norimberk
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Mnichov	Mnichov	k1gInSc1	Mnichov
pro	pro	k7c4	pro
Deutsche	Deutsche	k1gNnSc4	Deutsche
Bahn	Bahna	k1gFnPc2	Bahna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
firma	firma	k1gFnSc1	firma
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
152	[number]	k4	152
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
typem	typ	k1gInSc7	typ
autobusu	autobus	k1gInSc2	autobus
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
Irizar	Irizar	k1gMnSc1	Irizar
PB	PB	kA	PB
na	na	k7c6	na
podvozku	podvozek	k1gInSc6	podvozek
Scania	Scanium	k1gNnSc2	Scanium
nebo	nebo	k8xC	nebo
Volvo	Volvo	k1gNnSc1	Volvo
<g/>
,	,	kIx,	,
firma	firma	k1gFnSc1	firma
jich	on	k3xPp3gInPc2	on
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
93	[number]	k4	93
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
67	[number]	k4	67
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
výbavě	výbava	k1gFnSc6	výbava
Fun	Fun	k1gFnSc2	Fun
&	&	k?	&
Relax	Relax	k1gInSc1	Relax
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
typem	typ	k1gInSc7	typ
byl	být	k5eAaImAgInS	být
Beulas	Beulas	k1gInSc1	Beulas
Aura	aura	k1gFnSc1	aura
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
firma	firma	k1gFnSc1	firma
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
o	o	k7c4	o
45	[number]	k4	45
supermoderních	supermoderní	k2eAgInPc2d1	supermoderní
autobusů	autobus	k1gInPc2	autobus
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
Irizar	Irizara	k1gFnPc2	Irizara
i	i	k8xC	i
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Drážďany	Drážďany	k1gInPc1	Drážďany
–	–	k?	–
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
odstaveny	odstavit	k5eAaPmNgInP	odstavit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
starší	starý	k2eAgInPc4d2	starší
typy	typ	k1gInPc4	typ
autobusů	autobus	k1gInPc2	autobus
–	–	k?	–
na	na	k7c6	na
pravidelných	pravidelný	k2eAgFnPc6d1	pravidelná
linkách	linka	k1gFnPc6	linka
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
autobusy	autobus	k1gInPc1	autobus
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
nasazovány	nasazovat	k5eAaImNgInP	nasazovat
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c6	v
vybavení	vybavení	k1gNnSc6	vybavení
Fun	Fun	k1gFnSc2	Fun
&	&	k?	&
Relax	Relax	k1gInSc1	Relax
–	–	k?	–
starší	starý	k2eAgMnPc1d2	starší
typy	typ	k1gInPc1	typ
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
nasazovány	nasazovat	k5eAaImNgInP	nasazovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
posily	posila	k1gFnPc1	posila
<g/>
,	,	kIx,	,
záloha	záloha	k1gFnSc1	záloha
či	či	k8xC	či
v	v	k7c6	v
nepravidelné	pravidelný	k2eNgFnSc6d1	nepravidelná
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
autobusů	autobus	k1gInPc2	autobus
ve	v	k7c6	v
vozovém	vozový	k2eAgInSc6d1	vozový
parku	park	k1gInSc6	park
RegioJet	RegioJet	k1gInSc1	RegioJet
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
210	[number]	k4	210
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Liberec	Liberec	k1gInSc4	Liberec
byla	být	k5eAaImAgFnS	být
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
lukrativní	lukrativní	k2eAgMnSc1d1	lukrativní
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgInSc1d1	dopravní
úřad	úřad	k1gInSc1	úřad
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
ji	on	k3xPp3gFnSc4	on
zahrnul	zahrnout	k5eAaPmAgMnS	zahrnout
do	do	k7c2	do
závazku	závazek	k1gInSc2	závazek
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
financoval	financovat	k5eAaBmAgMnS	financovat
soukromému	soukromý	k2eAgMnSc3d1	soukromý
dopravci	dopravce	k1gMnSc3	dopravce
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
a.s.	a.s.	k?	a.s.
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
na	na	k7c6	na
dotovaných	dotovaný	k2eAgFnPc6d1	dotovaná
místních	místní	k2eAgFnPc6d1	místní
linkách	linka	k1gFnPc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dotovanou	dotovaný	k2eAgFnSc4d1	dotovaná
trasu	trasa	k1gFnSc4	trasa
přitom	přitom	k6eAd1	přitom
zákon	zákon	k1gInSc1	zákon
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
vydat	vydat	k5eAaPmF	vydat
licenci	licence	k1gFnSc4	licence
dalšímu	další	k2eAgMnSc3d1	další
dopravci	dopravce	k1gMnSc3	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
magistrát	magistrát	k1gInSc1	magistrát
ovšem	ovšem	k9	ovšem
zastával	zastávat	k5eAaImAgInS	zastávat
jiný	jiný	k2eAgInSc1d1	jiný
výklad	výklad	k1gInSc1	výklad
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
přes	přes	k7c4	přes
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
licenci	licence	k1gFnSc4	licence
firmě	firma	k1gFnSc3	firma
Hotliner	Hotliner	k1gInSc1	Hotliner
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
i	i	k8xC	i
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1	liberecký
dopravní	dopravní	k2eAgInSc1d1	dopravní
úřad	úřad	k1gInSc1	úřad
hodlal	hodlat	k5eAaImAgInS	hodlat
u	u	k7c2	u
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
napadnout	napadnout	k5eAaPmF	napadnout
platnost	platnost	k1gFnSc4	platnost
licence	licence	k1gFnSc2	licence
vydané	vydaný	k2eAgFnSc2d1	vydaná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tuto	tento	k3xDgFnSc4	tento
linku	linka	k1gFnSc4	linka
vyjmul	vyjmout	k5eAaPmAgMnS	vyjmout
ze	z	k7c2	z
závazku	závazek	k1gInSc2	závazek
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc4	Liberec
podstatně	podstatně	k6eAd1	podstatně
omezilo	omezit	k5eAaPmAgNnS	omezit
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trase	trasa	k1gFnSc6	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Hotliner	Hotlinra	k1gFnPc2	Hotlinra
trasu	tras	k1gInSc2	tras
opustila	opustit	k5eAaPmAgFnS	opustit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
odmítala	odmítat	k5eAaImAgFnS	odmítat
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc4	Liberec
uzavřít	uzavřít	k5eAaPmF	uzavřít
se	se	k3xPyFc4	se
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
užívání	užívání	k1gNnSc6	užívání
autobusového	autobusový	k2eAgNnSc2d1	autobusové
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
(	(	kIx(	(
<g/>
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
uzavíráním	uzavírání	k1gNnSc7	uzavírání
smlouvy	smlouva	k1gFnSc2	smlouva
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
i	i	k9	i
Hotliner	Hotliner	k1gInSc1	Hotliner
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
vozy	vůz	k1gInPc4	vůz
na	na	k7c4	na
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
vpouštět	vpouštět	k5eAaImF	vpouštět
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
linku	linka	k1gFnSc4	linka
řádnou	řádný	k2eAgFnSc4d1	řádná
licenci	licence	k1gFnSc4	licence
vydanou	vydaný	k2eAgFnSc4d1	vydaná
pražským	pražský	k2eAgInSc7d1	pražský
magistrátem	magistrát	k1gInSc7	magistrát
<g/>
.	.	kIx.	.
</s>
<s>
ČSAD	ČSAD	kA	ČSAD
to	ten	k3xDgNnSc4	ten
zdůvodňovala	zdůvodňovat	k5eAaImAgFnS	zdůvodňovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjezdové	odjezdový	k2eAgNnSc1d1	odjezdové
stání	stání	k1gNnSc1	stání
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
odjezdy	odjezd	k1gInPc4	odjezd
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
vytížené	vytížený	k2eAgNnSc1d1	vytížené
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
stání	stání	k1gNnPc1	stání
jsou	být	k5eAaImIp3nP	být
určena	určit	k5eAaPmNgNnP	určit
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
tak	tak	k6eAd1	tak
probíhal	probíhat	k5eAaImAgInS	probíhat
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
boj	boj	k1gInSc1	boj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
zastavovalo	zastavovat	k5eAaImAgNnS	zastavovat
mimo	mimo	k7c4	mimo
zastávku	zastávka	k1gFnSc4	zastávka
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
před	před	k7c7	před
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
nádraží	nádraží	k1gNnPc2	nádraží
celodenně	celodenně	k6eAd1	celodenně
hlídala	hlídat	k5eAaImAgFnS	hlídat
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
agentura	agentura	k1gFnSc1	agentura
najatá	najatý	k2eAgFnSc1d1	najatá
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
u	u	k7c2	u
vjezdu	vjezd	k1gInSc2	vjezd
do	do	k7c2	do
nádraží	nádraží	k1gNnSc2	nádraží
nechala	nechat	k5eAaPmAgFnS	nechat
postavit	postavit	k5eAaPmF	postavit
i	i	k9	i
závoru	závora	k1gFnSc4	závora
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gMnSc2	Agenca
kompenzovala	kompenzovat	k5eAaBmAgFnS	kompenzovat
cestujícím	cestující	k1gMnPc3	cestující
tuto	tento	k3xDgFnSc4	tento
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
symbolickou	symbolický	k2eAgFnSc7d1	symbolická
cenou	cena	k1gFnSc7	cena
1	[number]	k4	1
Kč	Kč	kA	Kč
za	za	k7c4	za
jízdu	jízda	k1gFnSc4	jízda
z	z	k7c2	z
Liberce	Liberec	k1gInSc2	Liberec
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
Hotliner	Hotliner	k1gMnSc1	Hotliner
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
podal	podat	k5eAaPmAgInS	podat
na	na	k7c4	na
Student	student	k1gMnSc1	student
Agency	Agenca	k1gMnSc2	Agenca
žalobu	žaloba	k1gFnSc4	žaloba
pro	pro	k7c4	pro
nekalou	kalý	k2eNgFnSc4d1	nekalá
soutěž	soutěž	k1gFnSc4	soutěž
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
Úřadu	úřad	k1gInSc3	úřad
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
se	se	k3xPyFc4	se
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podá	podat	k5eAaPmIp3nS	podat
žalobu	žaloba	k1gFnSc4	žaloba
i	i	k8xC	i
podnět	podnět	k1gInSc4	podnět
<g/>
,	,	kIx,	,
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
vydal	vydat	k5eAaPmAgInS	vydat
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
předběžné	předběžný	k2eAgNnSc1d1	předběžné
opatření	opatření	k1gNnSc1	opatření
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
zakázal	zakázat	k5eAaPmAgMnS	zakázat
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
prodávat	prodávat	k5eAaImF	prodávat
jízdenky	jízdenka	k1gFnPc4	jízdenka
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
za	za	k7c4	za
1	[number]	k4	1
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dalšího	další	k2eAgNnSc2d1	další
předběžného	předběžný	k2eAgNnSc2d1	předběžné
opatření	opatření	k1gNnSc2	opatření
soudu	soud	k1gInSc2	soud
začalo	začít	k5eAaPmAgNnS	začít
autobusy	autobus	k1gInPc4	autobus
vpouštět	vpouštět	k5eAaImF	vpouštět
na	na	k7c4	na
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
jim	on	k3xPp3gFnPc3	on
zase	zase	k9	zase
ve	v	k7c6	v
vjezdu	vjezd	k1gInSc6	vjezd
bránilo	bránit	k5eAaImAgNnS	bránit
až	až	k6eAd1	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
předběžné	předběžný	k2eAgNnSc1d1	předběžné
opatření	opatření	k1gNnSc1	opatření
stále	stále	k6eAd1	stále
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
soutěž	soutěž	k1gFnSc4	soutěž
ve	v	k7c4	v
sporu	spora	k1gFnSc4	spora
SA	SA	kA	SA
–	–	k?	–
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
zneužilo	zneužít	k5eAaPmAgNnS	zneužít
svého	svůj	k3xOyFgNnSc2	svůj
monopolního	monopolní	k2eAgNnSc2d1	monopolní
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
dostalo	dostat	k5eAaPmAgNnS	dostat
pokutu	pokuta	k1gFnSc4	pokuta
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
rozkladu	rozklást	k5eAaPmIp1nS	rozklást
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
úřad	úřad	k1gInSc1	úřad
vydal	vydat	k5eAaPmAgInS	vydat
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
rozkladu	rozklad	k1gInSc6	rozklad
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ÚOHS	ÚOHS	kA	ÚOHS
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sporů	spor	k1gInPc2	spor
majoritní	majoritní	k2eAgMnSc1d1	majoritní
vlastník	vlastník	k1gMnSc1	vlastník
ČSAD	ČSAD	kA	ČSAD
Liberec	Liberec	k1gInSc1	Liberec
založil	založit	k5eAaPmAgInS	založit
novou	nový	k2eAgFnSc4d1	nová
firmu	firma	k1gFnSc4	firma
ANL	ANL	kA	ANL
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
žádného	žádný	k3yNgMnSc4	žádný
dopravce	dopravce	k1gMnSc4	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
vytlačení	vytlačení	k1gNnSc6	vytlačení
ostatních	ostatní	k2eAgMnPc2d1	ostatní
dopravců	dopravce	k1gMnPc2	dopravce
začala	začít	k5eAaPmAgFnS	začít
kvalita	kvalita	k1gFnSc1	kvalita
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Liberec	Liberec	k1gInSc4	Liberec
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
začala	začít	k5eAaPmAgFnS	začít
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
poruchy	porucha	k1gFnSc2	porucha
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgInPc2d1	používaný
autobusů	autobus	k1gInPc2	autobus
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
spoje	spoj	k1gInPc4	spoj
nasazovat	nasazovat	k5eAaImF	nasazovat
starší	starý	k2eAgInPc1d2	starší
vozy	vůz	k1gInPc1	vůz
Neoplan	Neoplana	k1gFnPc2	Neoplana
a	a	k8xC	a
Mercedes	mercedes	k1gInSc1	mercedes
bez	bez	k7c2	bez
obvyklého	obvyklý	k2eAgInSc2d1	obvyklý
servisu	servis	k1gInSc2	servis
(	(	kIx(	(
<g/>
občerstvení	občerstvení	k1gNnSc1	občerstvení
<g/>
,	,	kIx,	,
promítání	promítání	k1gNnSc1	promítání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
je	být	k5eAaImIp3nS	být
i	i	k9	i
vysoká	vysoký	k2eAgFnSc1d1	vysoká
obsazenost	obsazenost	k1gFnSc1	obsazenost
spojů	spoj	k1gInPc2	spoj
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
týden	týden	k1gInSc4	týden
dopředu	dopříst	k5eAaPmIp1nS	dopříst
vyprodané	vyprodaný	k2eAgNnSc1d1	vyprodané
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
zavedl	zavést	k5eAaPmAgInS	zavést
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Liberce	Liberec	k1gInSc2	Liberec
novou	nový	k2eAgFnSc4d1	nová
linku	linka	k1gFnSc4	linka
540	[number]	k4	540
851	[number]	k4	851
Liberec	Liberec	k1gInSc1	Liberec
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
s	s	k7c7	s
odjezdem	odjezd	k1gInSc7	odjezd
z	z	k7c2	z
terminálu	terminál	k1gInSc2	terminál
MHD	MHD	kA	MHD
Fügnerova	Fügnerův	k2eAgFnSc1d1	Fügnerova
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
následně	následně	k6eAd1	následně
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
lince	linka	k1gFnSc6	linka
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
zrušila	zrušit	k5eAaPmAgFnS	zrušit
asi	asi	k9	asi
8	[number]	k4	8
párů	pár	k1gInPc2	pár
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
zastavovala	zastavovat	k5eAaImAgFnS	zastavovat
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnPc4	Agenca
i	i	k9	i
na	na	k7c6	na
zastávce	zastávka	k1gFnSc6	zastávka
ve	v	k7c6	v
Fügnerově	Fügnerův	k2eAgFnSc6d1	Fügnerova
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
dopravci	dopravce	k1gMnPc7	dopravce
byla	být	k5eAaImAgFnS	být
vedena	veden	k2eAgFnSc1d1	vedena
cenová	cenový	k2eAgFnSc1d1	cenová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
bude	být	k5eAaImBp3nS	být
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
provozovat	provozovat	k5eAaImF	provozovat
linku	linka	k1gFnSc4	linka
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
konkurent	konkurent	k1gMnSc1	konkurent
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
prodělečnou	prodělečný	k2eAgFnSc4d1	prodělečná
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
dopravce	dopravce	k1gMnSc2	dopravce
ICOM	ICOM	kA	ICOM
transport	transport	k1gInSc1	transport
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c4	na
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
dopravní	dopravní	k2eAgFnSc4d1	dopravní
značku	značka	k1gFnSc4	značka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
zakázal	zakázat	k5eAaPmAgMnS	zakázat
na	na	k7c4	na
stanoviště	stanoviště	k1gNnSc4	stanoviště
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
vjíždět	vjíždět	k5eAaImF	vjíždět
vozidly	vozidlo	k1gNnPc7	vozidlo
vyššími	vysoký	k2eAgNnPc7d2	vyšší
než	než	k8xS	než
3,60	[number]	k4	3,60
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
zastřešení	zastřešení	k1gNnSc1	zastřešení
nástupiště	nástupiště	k1gNnSc2	nástupiště
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgInPc4d2	vyšší
vozy	vůz	k1gInPc4	vůz
upravil	upravit	k5eAaPmAgMnS	upravit
vlastník	vlastník	k1gMnSc1	vlastník
nádraží	nádraží	k1gNnSc2	nádraží
pouze	pouze	k6eAd1	pouze
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
vjezd	vjezd	k1gInSc4	vjezd
vysokým	vysoký	k2eAgInSc7d1	vysoký
autobusům	autobus	k1gInPc3	autobus
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnPc4	Agenca
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
zastavujícím	zastavující	k2eAgMnPc3d1	zastavující
u	u	k7c2	u
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
7	[number]	k4	7
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnSc2	jehož
blízkosti	blízkost	k1gFnSc2	blízkost
jezdí	jezdit	k5eAaImIp3nP	jezdit
i	i	k9	i
linky	linka	k1gFnPc1	linka
jiných	jiný	k2eAgMnPc2d1	jiný
dopravců	dopravce	k1gMnPc2	dopravce
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
jihlavský	jihlavský	k2eAgInSc4d1	jihlavský
magistrát	magistrát	k1gInSc4	magistrát
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
Úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
na	na	k7c4	na
zneužívání	zneužívání	k1gNnSc4	zneužívání
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
podnět	podnět	k1gInSc4	podnět
odložil	odložit	k5eAaPmAgMnS	odložit
jako	jako	k9	jako
nedůvodný	důvodný	k2eNgMnSc1d1	nedůvodný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
konkurenčního	konkurenční	k2eAgInSc2d1	konkurenční
boje	boj	k1gInSc2	boj
se	s	k7c7	s
železničním	železniční	k2eAgMnSc7d1	železniční
dopravcem	dopravce	k1gMnSc7	dopravce
zavedla	zavést	k5eAaPmAgFnS	zavést
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnPc4	Agenca
pro	pro	k7c4	pro
prvních	první	k4xOgInPc2	první
10	[number]	k4	10
cestujících	cestující	k1gMnPc2	cestující
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
autobusu	autobus	k1gInSc6	autobus
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
jízdné	jízdné	k1gNnSc1	jízdné
1	[number]	k4	1
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnSc2d1	podobná
nabídky	nabídka	k1gFnSc2	nabídka
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
i	i	k9	i
pro	pro	k7c4	pro
trasu	trasa	k1gFnSc4	trasa
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
zneužívání	zneužívání	k1gNnSc4	zneužívání
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
předseda	předseda	k1gMnSc1	předseda
ÚOHS	ÚOHS	kA	ÚOHS
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
žádné	žádný	k3yNgFnSc6	žádný
z	z	k7c2	z
linek	linka	k1gFnPc2	linka
nemá	mít	k5eNaImIp3nS	mít
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdné	k1gNnSc1	jízdné
za	za	k7c4	za
korunu	koruna	k1gFnSc4	koruna
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
bylo	být	k5eAaImAgNnS	být
nejprve	nejprve	k6eAd1	nejprve
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c4	na
prvních	první	k4xOgInPc2	první
5	[number]	k4	5
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
následně	následně	k6eAd1	následně
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
ke	k	k7c3	k
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
uložil	uložit	k5eAaPmAgInS	uložit
Úřad	úřad	k1gInSc1	úřad
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
soutěže	soutěž	k1gFnPc1	soutěž
dopravci	dopravce	k1gMnPc1	dopravce
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnPc4	Agenca
pokutu	pokuta	k1gFnSc4	pokuta
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
konkurenčního	konkurenční	k2eAgMnSc2d1	konkurenční
dopravce	dopravce	k1gMnSc2	dopravce
Asiana	Asian	k1gMnSc2	Asian
na	na	k7c4	na
trasu	trasa	k1gFnSc4	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
s	s	k7c7	s
jízdným	jízdné	k1gNnSc7	jízdné
50	[number]	k4	50
Kč	Kč	kA	Kč
reagoval	reagovat	k5eAaBmAgMnS	reagovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
snížením	snížení	k1gNnSc7	snížení
ceny	cena	k1gFnSc2	cena
kreditových	kreditový	k2eAgFnPc2d1	kreditová
jízdenek	jízdenka	k1gFnPc2	jízdenka
rovněž	rovněž	k9	rovněž
na	na	k7c4	na
50	[number]	k4	50
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
údajně	údajně	k6eAd1	údajně
podnákladovou	podnákladový	k2eAgFnSc4d1	podnákladová
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
se	se	k3xPyFc4	se
odvolala	odvolat	k5eAaPmAgFnS	odvolat
<g/>
:	:	kIx,	:
namítá	namítat	k5eAaImIp3nS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
akční	akční	k2eAgFnPc1d1	akční
slevy	sleva	k1gFnPc1	sleva
pro	pro	k7c4	pro
vymezený	vymezený	k2eAgInSc4d1	vymezený
okruh	okruh	k1gInSc4	okruh
zákazníků	zákazník	k1gMnPc2	zákazník
jsou	být	k5eAaImIp3nP	být
běžným	běžný	k2eAgInSc7d1	běžný
nástrojem	nástroj	k1gInSc7	nástroj
a	a	k8xC	a
že	že	k8xS	že
linka	linka	k1gFnSc1	linka
Student	student	k1gMnSc1	student
Agency	Agenca	k1gMnSc2	Agenca
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
byla	být	k5eAaImAgFnS	být
i	i	k9	i
po	po	k7c4	po
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
stále	stále	k6eAd1	stále
zisková	ziskový	k2eAgFnSc1d1	zisková
<g/>
.	.	kIx.	.
</s>
<s>
SA	SA	kA	SA
rovněž	rovněž	k9	rovněž
vytýká	vytýkat	k5eAaImIp3nS	vytýkat
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
relevantního	relevantní	k2eAgInSc2d1	relevantní
trhu	trh	k1gInSc2	trh
ve	v	k7c6	v
zdůvodnění	zdůvodnění	k1gNnSc6	zdůvodnění
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
nezahrnul	zahrnout	k5eNaPmAgMnS	zahrnout
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
nabízely	nabízet	k5eAaImAgFnP	nabízet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
slevy	sleva	k1gFnSc2	sleva
eLiška	eLišek	k1gInSc2	eLišek
jízdné	jízdné	k1gNnSc1	jízdné
35	[number]	k4	35
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
SA	SA	kA	SA
však	však	k9	však
uvítala	uvítat	k5eAaPmAgFnS	uvítat
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
ÚOHS	ÚOHS	kA	ÚOHS
jako	jako	k8xC	jako
precedent	precedent	k1gInSc1	precedent
<g/>
,	,	kIx,	,
že	že	k8xS	že
větší	veliký	k2eAgInSc4d2	veliký
operátor	operátor	k1gInSc4	operátor
(	(	kIx(	(
<g/>
dominantní	dominantní	k2eAgMnSc1d1	dominantní
dopravce	dopravce	k1gMnSc1	dopravce
<g/>
)	)	kIx)	)
nemůže	moct	k5eNaImIp3nS	moct
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
dalších	další	k2eAgInPc2d1	další
subjektů	subjekt	k1gInPc2	subjekt
na	na	k7c4	na
trh	trh	k1gInSc4	trh
reagovat	reagovat	k5eAaBmF	reagovat
úpravou	úprava	k1gFnSc7	úprava
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	k9	aby
stejným	stejný	k2eAgNnSc7d1	stejné
metrem	metro	k1gNnSc7	metro
posuzoval	posuzovat	k5eAaImAgMnS	posuzovat
i	i	k8xC	i
dopravce	dopravce	k1gMnSc1	dopravce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nabízejí	nabízet	k5eAaImIp3nP	nabízet
podobné	podobný	k2eAgFnPc4d1	podobná
obchodní	obchodní	k2eAgFnPc4d1	obchodní
nabídky	nabídka	k1gFnPc4	nabídka
či	či	k8xC	či
jiné	jiný	k2eAgFnPc4d1	jiná
relační	relační	k2eAgFnPc4d1	relační
slevy	sleva	k1gFnPc4	sleva
<g/>
.	.	kIx.	.
</s>
<s>
Pokuta	pokuta	k1gFnSc1	pokuta
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
ÚOHS	ÚOHS	kA	ÚOHS
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
ke	k	k7c3	k
Krajskému	krajský	k2eAgInSc3d1	krajský
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
zcela	zcela	k6eAd1	zcela
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
případ	případ	k1gInSc1	případ
vrácen	vrácen	k2eAgInSc1d1	vrácen
ÚOHS	ÚOHS	kA	ÚOHS
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
projednání	projednání	k1gNnSc3	projednání
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
přitom	přitom	k6eAd1	přitom
uznal	uznat	k5eAaPmAgInS	uznat
tvrzení	tvrzení	k1gNnSc4	tvrzení
SA	SA	kA	SA
<g/>
,	,	kIx,	,
že	že	k8xS	že
relevantním	relevantní	k2eAgInSc7d1	relevantní
trhem	trh	k1gInSc7	trh
je	být	k5eAaImIp3nS	být
i	i	k9	i
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
projednával	projednávat	k5eAaImAgInS	projednávat
pražský	pražský	k2eAgInSc1d1	pražský
magistrát	magistrát	k1gInSc1	magistrát
porušení	porušení	k1gNnSc2	porušení
licence	licence	k1gFnSc2	licence
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
spočívat	spočívat	k5eAaImF	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestující	cestující	k1gMnPc1	cestující
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
linky	linka	k1gFnSc2	linka
Liberec	Liberec	k1gInSc1	Liberec
–	–	k?	–
Praha-Černý	Praha-Černý	k2eAgInSc1d1	Praha-Černý
most	most	k1gInSc1	most
–	–	k?	–
Praha-letiště	Prahaetiště	k1gNnSc2	Praha-letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
přestupovat	přestupovat	k5eAaImF	přestupovat
z	z	k7c2	z
dvoupatrového	dvoupatrový	k2eAgInSc2d1	dvoupatrový
autobusu	autobus	k1gInSc2	autobus
do	do	k7c2	do
minibusu	minibus	k1gInSc2	minibus
pokračujícího	pokračující	k2eAgInSc2d1	pokračující
na	na	k7c6	na
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
licence	licence	k1gFnSc1	licence
přestup	přestup	k1gInSc1	přestup
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
neumožňovala	umožňovat	k5eNaImAgFnS	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
si	se	k3xPyFc3	se
cestující	cestující	k1gMnSc1	cestující
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dálkové	dálkový	k2eAgInPc4d1	dálkový
autobusy	autobus	k1gInPc4	autobus
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
licencí	licence	k1gFnSc7	licence
a	a	k8xC	a
jízdním	jízdní	k2eAgInSc7d1	jízdní
řádem	řád	k1gInSc7	řád
sjíždějí	sjíždět	k5eAaImIp3nP	sjíždět
z	z	k7c2	z
trasy	trasa	k1gFnSc2	trasa
na	na	k7c4	na
technickou	technický	k2eAgFnSc4d1	technická
zastávku	zastávka	k1gFnSc4	zastávka
v	v	k7c6	v
garážích	garáž	k1gFnPc6	garáž
firmy	firma	k1gFnSc2	firma
u	u	k7c2	u
Velké	velký	k2eAgFnSc2d1	velká
Bíteše	Bíteše	k1gFnSc2	Bíteše
pro	pro	k7c4	pro
vyprázdnění	vyprázdnění	k1gNnSc4	vyprázdnění
záchodů	záchod	k1gInPc2	záchod
a	a	k8xC	a
doplnění	doplnění	k1gNnSc4	doplnění
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zpožděním	zpoždění	k1gNnPc3	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančura	k1gFnSc1	Jančura
tuto	tento	k3xDgFnSc4	tento
praxi	praxe	k1gFnSc4	praxe
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
a	a	k8xC	a
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dočasné	dočasný	k2eAgNnSc4d1	dočasné
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádě	řád	k1gInSc6	řád
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
zastávka	zastávka	k1gFnSc1	zastávka
není	být	k5eNaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
a	a	k8xC	a
kritiky	kritika	k1gFnPc1	kritika
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
barvě	barva	k1gFnSc3	barva
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
expanzivnímu	expanzivní	k2eAgInSc3d1	expanzivní
způsobu	způsob	k1gInSc3	způsob
zavádění	zavádění	k1gNnSc2	zavádění
nových	nový	k2eAgFnPc2d1	nová
linek	linka	k1gFnPc2	linka
někdy	někdy	k6eAd1	někdy
firmě	firma	k1gFnSc6	firma
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
žlutý	žlutý	k2eAgInSc1d1	žlutý
mor	mor	k1gInSc1	mor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stejnou	stejný	k2eAgFnSc4d1	stejná
přezdívku	přezdívka	k1gFnSc4	přezdívka
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
Český	český	k2eAgInSc1d1	český
Telecom	Telecom	k1gInSc1	Telecom
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
třeba	třeba	k9	třeba
i	i	k9	i
pro	pro	k7c4	pro
řepku	řepka	k1gFnSc4	řepka
olejku	olejka	k1gFnSc4	olejka
či	či	k8xC	či
další	další	k2eAgInPc4d1	další
typické	typický	k2eAgInPc4d1	typický
"	"	kIx"	"
<g/>
žluté	žlutý	k2eAgInPc4d1	žlutý
<g/>
"	"	kIx"	"
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířící	šířící	k2eAgInPc1d1	šířící
produkty	produkt	k1gInPc1	produkt
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
pokutu	pokuta	k1gFnSc4	pokuta
vymeřenou	vymeřený	k2eAgFnSc4d1	vymeřený
Úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
ve	v	k7c6	v
výší	výšit	k5eAaImIp3nS	výšit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
miliónu	milión	k4xCgInSc2	milión
korun	koruna	k1gFnPc2	koruna
pro	pro	k7c4	pro
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
firma	firma	k1gFnSc1	firma
uhradit	uhradit	k5eAaPmF	uhradit
za	za	k7c4	za
zneužití	zneužití	k1gNnSc4	zneužití
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha-Brno	Praha-Brno	k1gNnSc1	Praha-Brno
nabízet	nabízet	k5eAaImF	nabízet
podnákladové	podnákladový	k2eAgNnSc4d1	podnákladové
jízdné	jízdné	k1gNnSc4	jízdné
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zničit	zničit	k5eAaPmF	zničit
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trase	trasa	k1gFnSc6	trasa
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
firmy	firma	k1gFnSc2	firma
s	s	k7c7	s
rozsudkem	rozsudek	k1gInSc7	rozsudek
nesouhlasilo	souhlasit	k5eNaImAgNnS	souhlasit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
ve	v	k7c4	v
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
nedaleko	nedaleko	k7c2	nedaleko
Nového	Nového	k2eAgInSc2d1	Nového
Jičína	Jičín	k1gInSc2	Jičín
autobus	autobus	k1gInSc1	autobus
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Ostrava	Ostrava	k1gFnSc1	Ostrava
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
sjel	sjet	k5eAaPmAgMnS	sjet
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vyhýbal	vyhýbat	k5eAaImAgMnS	vyhýbat
dopravním	dopravní	k2eAgMnSc7d1	dopravní
kuželům	kužel	k1gInPc3	kužel
rozházeným	rozházený	k2eAgMnSc7d1	rozházený
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
nedaleko	nedaleko	k7c2	nedaleko
místa	místo	k1gNnSc2	místo
frézování	frézování	k1gNnSc2	frézování
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
16	[number]	k4	16
lehce	lehko	k6eAd1	lehko
zraněných	zraněný	k2eAgMnPc2d1	zraněný
byli	být	k5eAaImAgMnP	být
dva	dva	k4xCgMnPc1	dva
hospitalizováni	hospitalizován	k2eAgMnPc1d1	hospitalizován
na	na	k7c6	na
pozorování	pozorování	k1gNnSc6	pozorování
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2006	[number]	k4	2006
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
D1	D1	k1gMnSc2	D1
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
zadýmil	zadýmit	k5eAaPmAgInS	zadýmit
motor	motor	k1gInSc1	motor
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
starého	starý	k2eAgInSc2d1	starý
minibusu	minibus	k1gInSc2	minibus
značky	značka	k1gFnSc2	značka
SOR	SOR	kA	SOR
na	na	k7c6	na
pravidelné	pravidelný	k2eAgFnSc6d1	pravidelná
regionální	regionální	k2eAgFnSc6d1	regionální
lince	linka	k1gFnSc6	linka
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Velká	velká	k1gFnSc1	velká
Bíteš	Bíteš	k1gFnSc1	Bíteš
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc4	požár
uhasil	uhasit	k5eAaPmAgMnS	uhasit
řidič	řidič	k1gMnSc1	řidič
ještě	ještě	k9	ještě
před	před	k7c7	před
příjezdem	příjezd	k1gInSc7	příjezd
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
na	na	k7c4	na
97	[number]	k4	97
<g/>
.	.	kIx.	.
kilometru	kilometr	k1gInSc2	kilometr
dálnice	dálnice	k1gFnSc2	dálnice
D1	D1	k1gMnPc2	D1
vyhořel	vyhořet	k5eAaPmAgMnS	vyhořet
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
starý	starý	k2eAgInSc1d1	starý
autobus	autobus	k1gInSc1	autobus
značky	značka	k1gFnSc2	značka
Ayats	Ayatsa	k1gFnPc2	Ayatsa
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Brno	Brno	k1gNnSc1	Brno
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zranění	zranění	k1gNnSc3	zranění
cestujících	cestující	k1gFnPc2	cestující
ani	ani	k8xC	ani
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
jejich	jejich	k3xOp3gNnPc2	jejich
zavazadel	zavazadlo	k1gNnPc2	zavazadlo
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
se	se	k3xPyFc4	se
autobus	autobus	k1gInSc1	autobus
nedaleko	nedaleko	k7c2	nedaleko
italského	italský	k2eAgNnSc2d1	italské
města	město	k1gNnSc2	město
Padova	Padova	k1gFnSc1	Padova
střetl	střetnout	k5eAaPmAgInS	střetnout
se	s	k7c7	s
slovinským	slovinský	k2eAgInSc7d1	slovinský
kamionem	kamion	k1gInSc7	kamion
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bez	bez	k7c2	bez
předchozí	předchozí	k2eAgFnSc2d1	předchozí
signalizace	signalizace	k1gFnSc2	signalizace
najel	najet	k5eAaPmAgMnS	najet
do	do	k7c2	do
pruhu	pruh	k1gInSc2	pruh
před	před	k7c4	před
autobus	autobus	k1gInSc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Nehodu	nehoda	k1gFnSc4	nehoda
nepřežila	přežít	k5eNaPmAgFnS	přežít
dvaadvacetiletá	dvaadvacetiletý	k2eAgFnSc1d1	dvaadvacetiletá
stevardka	stevardka	k1gFnSc1	stevardka
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
sedadle	sedadlo	k1gNnSc6	sedadlo
vedle	vedle	k7c2	vedle
řidiče	řidič	k1gInSc2	řidič
<g/>
,	,	kIx,	,
cestujícím	cestující	k1gMnPc3	cestující
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
5	[number]	k4	5
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
nejméně	málo	k6eAd3	málo
20	[number]	k4	20
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
autobusu	autobus	k1gInSc2	autobus
u	u	k7c2	u
slovenské	slovenský	k2eAgFnSc2d1	slovenská
obce	obec	k1gFnSc2	obec
Kostolná	Kostolný	k2eAgFnSc1d1	Kostolná
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
se	se	k3xPyFc4	se
převrátil	převrátit	k5eAaPmAgInS	převrátit
autobus	autobus	k1gInSc1	autobus
značky	značka	k1gFnSc2	značka
Beluas	Beluas	k1gMnSc1	Beluas
Aura	aura	k1gFnSc1	aura
SPZ	SPZ	kA	SPZ
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
B	B	kA	B
<g/>
2	[number]	k4	2
5370	[number]	k4	5370
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Lubenec	Lubenec	k1gInSc1	Lubenec
–	–	k?	–
Drahonice	Drahonice	k1gFnSc2	Drahonice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Louny	Louny	k1gInPc1	Louny
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
43	[number]	k4	43
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
zraněno	zranit	k5eAaPmNgNnS	zranit
lehce	lehko	k6eAd1	lehko
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
autobusu	autobus	k1gInSc2	autobus
nepřizpůsobil	přizpůsobit	k5eNaPmAgMnS	přizpůsobit
jízdu	jízda	k1gFnSc4	jízda
stavu	stav	k1gInSc2	stav
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
obvykle	obvykle	k6eAd1	obvykle
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
frézování	frézování	k1gNnSc3	frézování
vozovky	vozovka	k1gFnSc2	vozovka
musel	muset	k5eAaImAgMnS	muset
řidič	řidič	k1gMnSc1	řidič
využít	využít	k5eAaPmF	využít
objížďku	objížďka	k1gFnSc4	objížďka
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
havaroval	havarovat	k5eAaPmAgMnS	havarovat
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
Autobus	autobus	k1gInSc1	autobus
společnosti	společnost	k1gFnSc2	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
se	se	k3xPyFc4	se
zapletl	zaplést	k5eAaPmAgInS	zaplést
do	do	k7c2	do
hromadné	hromadný	k2eAgFnSc2d1	hromadná
autonehody	autonehoda	k1gFnSc2	autonehoda
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
R35	R35	k1gFnSc2	R35
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
značené	značený	k2eAgNnSc4d1	značené
D	D	kA	D
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
na	na	k7c4	na
Ostravu	Ostrava	k1gFnSc4	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zranění	zraněný	k2eAgMnPc1d1	zraněný
dva	dva	k4xCgMnPc1	dva
cestující	cestující	k1gMnPc1	cestující
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
autobus	autobus	k1gInSc4	autobus
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Chomutov	Chomutov	k1gInSc1	Chomutov
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
narazil	narazit	k5eAaPmAgInS	narazit
do	do	k7c2	do
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
jedoucího	jedoucí	k2eAgInSc2d1	jedoucí
kamionu	kamion	k1gInSc2	kamion
<g/>
.	.	kIx.	.
20	[number]	k4	20
cestujících	cestující	k1gMnPc2	cestující
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1	[number]	k4	1
cestujicí	cestujice	k1gFnPc2	cestujice
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
nehody	nehoda	k1gFnSc2	nehoda
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehodu	nehoda	k1gFnSc4	nehoda
zavinil	zavinit	k5eAaPmAgMnS	zavinit
řidič	řidič	k1gMnSc1	řidič
Student	student	k1gMnSc1	student
Agency	Agenca	k1gMnSc2	Agenca
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nedobrzdil	dobrzdit	k5eNaPmAgMnS	dobrzdit
za	za	k7c7	za
kamionem	kamion	k1gInSc7	kamion
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
upadlo	upadnout	k5eAaPmAgNnS	upadnout
za	za	k7c2	za
jízdy	jízda	k1gFnSc2	jízda
kolo	kolo	k1gNnSc1	kolo
autobusu	autobus	k1gInSc2	autobus
společnosti	společnost	k1gFnSc2	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
jedoucím	jedoucí	k2eAgFnPc3d1	jedoucí
z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
zraněn	zraněn	k2eAgMnSc1d1	zraněn
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
zemřela	zemřít	k5eAaPmAgFnS	zemřít
pod	pod	k7c4	pod
koly	kola	k1gFnPc4	kola
žlutého	žlutý	k2eAgInSc2d1	žlutý
autobusu	autobus	k1gInSc2	autobus
společnosti	společnost	k1gFnSc2	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
pošťačka	pošťačka	k1gFnSc1	pošťačka
z	z	k7c2	z
Mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
když	když	k8xS	když
přecházela	přecházet	k5eAaImAgFnS	přecházet
silnici	silnice	k1gFnSc4	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
RegioJet	RegioJet	k1gInSc1	RegioJet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
v	v	k7c6	v
několika	několik	k4yIc6	několik
rozhovorech	rozhovor	k1gInPc6	rozhovor
záměr	záměr	k1gInSc1	záměr
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
firma	firma	k1gFnSc1	firma
v	v	k7c6	v
horizontu	horizont	k1gInSc6	horizont
zhruba	zhruba	k6eAd1	zhruba
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
železničním	železniční	k2eAgMnSc7d1	železniční
dopravcem	dopravce	k1gMnSc7	dopravce
<g/>
,	,	kIx,	,
prvním	první	k4xOgInSc7	první
propagovaným	propagovaný	k2eAgInSc7d1	propagovaný
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
provozování	provozování	k1gNnSc1	provozování
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
účastnila	účastnit	k5eAaImAgFnS	účastnit
výběrových	výběrový	k2eAgNnPc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
podávala	podávat	k5eAaImAgFnS	podávat
krajům	kraj	k1gInPc3	kraj
i	i	k8xC	i
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
nabídky	nabídka	k1gFnSc2	nabídka
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
dotované	dotovaný	k2eAgFnSc2d1	dotovaná
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
jednala	jednat	k5eAaImAgFnS	jednat
s	s	k7c7	s
kraji	kraj	k1gInPc7	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
vyjel	vyjet	k5eAaPmAgInS	vyjet
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
první	první	k4xOgInSc4	první
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
propagační	propagační	k2eAgFnSc2d1	propagační
akce	akce	k1gFnSc2	akce
Žluté	žlutý	k2eAgNnSc4d1	žluté
jaro	jaro	k1gNnSc4	jaro
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
poslancem	poslanec	k1gMnSc7	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
Josefem	Josef	k1gMnSc7	Josef
Zieleniecem	Zieleniece	k1gMnSc7	Zieleniece
a	a	k8xC	a
senátorem	senátor	k1gMnSc7	senátor
Igorem	Igor	k1gMnSc7	Igor
Petrovem	Petrov	k1gInSc7	Petrov
se	se	k3xPyFc4	se
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc4	Jančur
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
soudně	soudně	k6eAd1	soudně
napadnout	napadnout	k5eAaPmF	napadnout
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dopravcům	dopravce	k1gMnPc3	dopravce
nehradí	hradit	k5eNaImIp3nS	hradit
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
cestování	cestování	k1gNnSc1	cestování
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
mají	mít	k5eAaImIp3nP	mít
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
provedli	provést	k5eAaPmAgMnP	provést
oba	dva	k4xCgMnPc1	dva
politici	politik	k1gMnPc1	politik
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
demonstrativní	demonstrativní	k2eAgFnSc4d1	demonstrativní
jízdu	jízda	k1gFnSc4	jízda
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
které	který	k3yQgNnSc4	který
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
následně	následně	k6eAd1	následně
stát	stát	k1gInSc1	stát
zažaloval	zažalovat	k5eAaPmAgInS	zažalovat
o	o	k7c4	o
náhradu	náhrada	k1gFnSc4	náhrada
jízdného	jízdné	k1gNnSc2	jízdné
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
v	v	k7c6	v
diskusním	diskusní	k2eAgInSc6d1	diskusní
pořadu	pořad	k1gInSc6	pořad
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
se	s	k7c7	s
státem	stát	k1gInSc7	stát
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
a	a	k8xC	a
vysoudil	vysoudit	k5eAaPmAgInS	vysoudit
270	[number]	k4	270
Kč	Kč	kA	Kč
za	za	k7c4	za
přepravu	přeprava	k1gFnSc4	přeprava
členů	člen	k1gMnPc2	člen
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc4	Jančur
stal	stát	k5eAaPmAgMnS	stát
jednatelem	jednatel	k1gMnSc7	jednatel
nové	nový	k2eAgFnSc2d1	nová
společnosti	společnost	k1gFnSc2	společnost
Tick	Tick	k1gMnSc1	Tick
Tack	Tack	k1gMnSc1	Tack
Taxi	taxe	k1gFnSc4	taxe
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
má	mít	k5eAaImIp3nS	mít
80	[number]	k4	80
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
Student	student	k1gMnSc1	student
Agency	Agenc	k2eAgInPc4d1	Agenc
a	a	k8xC	a
20	[number]	k4	20
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
stávající	stávající	k2eAgFnSc1d1	stávající
společnost	společnost	k1gFnSc1	společnost
Tick	Tick	k1gMnSc1	Tick
Tack	Tack	k1gMnSc1	Tack
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
taxislužbu	taxislužba	k1gFnSc4	taxislužba
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
movitější	movitý	k2eAgFnSc4d2	movitější
a	a	k8xC	a
firemní	firemní	k2eAgFnSc4d1	firemní
klientelu	klientela	k1gFnSc4	klientela
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
vlastnit	vlastnit	k5eAaImF	vlastnit
společnost	společnost	k1gFnSc4	společnost
Kurýr	kurýr	k1gMnSc1	kurýr
Taxi	taxe	k1gFnSc4	taxe
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
spoluvlastní	spoluvlastnit	k5eAaImIp3nS	spoluvlastnit
Pavel	Pavel	k1gMnSc1	Pavel
Nytra	Nytra	k1gMnSc1	Nytra
<g/>
.	.	kIx.	.
</s>
<s>
Jančura	Jančura	k1gFnSc1	Jančura
avizoval	avizovat	k5eAaBmAgMnS	avizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
do	do	k7c2	do
roka	rok	k1gInSc2	rok
stát	stát	k5eAaPmF	stát
jedničkou	jednička	k1gFnSc7	jednička
v	v	k7c6	v
taxislužbě	taxislužba	k1gFnSc6	taxislužba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
si	se	k3xPyFc3	se
pořídit	pořídit	k5eAaPmF	pořídit
1000	[number]	k4	1000
nových	nový	k2eAgInPc2d1	nový
vozů	vůz	k1gInPc2	vůz
značek	značka	k1gFnPc2	značka
Audi	Audi	k1gNnSc1	Audi
A6	A6	k1gMnSc1	A6
a	a	k8xC	a
BMW	BMW	kA	BMW
GT	GT	kA	GT
průměrného	průměrný	k2eAgNnSc2d1	průměrné
stáří	stáří	k1gNnSc2	stáří
do	do	k7c2	do
1	[number]	k4	1
roku	rok	k1gInSc2	rok
a	a	k8xC	a
zlepšit	zlepšit	k5eAaPmF	zlepšit
špatné	špatný	k2eAgNnSc4d1	špatné
jméno	jméno	k1gNnSc4	jméno
pražské	pražský	k2eAgFnSc2d1	Pražská
taxislužby	taxislužba	k1gFnSc2	taxislužba
<g/>
.	.	kIx.	.
</s>
<s>
Zkušební	zkušební	k2eAgInSc1d1	zkušební
provoz	provoz	k1gInSc1	provoz
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
spuštěn	spustit	k5eAaPmNgInS	spustit
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
a	a	k8xC	a
plný	plný	k2eAgInSc4d1	plný
provoz	provoz	k1gInSc4	provoz
po	po	k7c6	po
letních	letní	k2eAgFnPc6d1	letní
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc4	Jančur
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
ocenění	ocenění	k1gNnPc2	ocenění
Podnikatel	podnikatel	k1gMnSc1	podnikatel
roku	rok	k1gInSc2	rok
udělované	udělovaný	k2eAgFnSc2d1	udělovaná
společností	společnost	k1gFnPc2	společnost
Ernst	Ernst	k1gMnSc1	Ernst
&	&	k?	&
Young	Young	k1gMnSc1	Young
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Czech	Czecha	k1gFnPc2	Czecha
top	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
každoročně	každoročně	k6eAd1	každoročně
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
Sdružení	sdružení	k1gNnSc1	sdružení
CZECH	CZECH	kA	CZECH
TOP	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
hlasováním	hlasování	k1gNnSc7	hlasování
manažerů	manažer	k1gMnPc2	manažer
<g/>
,	,	kIx,	,
analytiků	analytik	k1gMnPc2	analytik
a	a	k8xC	a
zástupců	zástupce	k1gMnPc2	zástupce
významných	významný	k2eAgFnPc2d1	významná
profesních	profesní	k2eAgFnPc2d1	profesní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
vyhodnocena	vyhodnotit	k5eAaPmNgFnS	vyhodnotit
jako	jako	k9	jako
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
nejobdivovanější	obdivovaný	k2eAgFnSc1d3	nejobdivovanější
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
nejobdivovanější	obdivovaný	k2eAgFnSc7d3	nejobdivovanější
společností	společnost	k1gFnSc7	společnost
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
odvětví	odvětví	k1gNnSc6	odvětví
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
pomocných	pomocný	k2eAgFnPc2d1	pomocná
činností	činnost	k1gFnPc2	činnost
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
cestovních	cestovní	k2eAgFnPc2d1	cestovní
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Student	student	k1gMnSc1	student
Agency	Agency	k1gInPc1	Agency
dvě	dva	k4xCgFnPc4	dva
ocenění	ocenění	k1gNnSc3	ocenění
TTG	TTG	kA	TTG
Travel	Travel	k1gInSc4	Travel
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
letenková	letenkový	k2eAgFnSc1d1	letenková
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
)	)	kIx)	)
agentura	agentura	k1gFnSc1	agentura
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
autobusový	autobusový	k2eAgMnSc1d1	autobusový
dopravce	dopravce	k1gMnSc1	dopravce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
neprovozuje	provozovat	k5eNaImIp3nS	provozovat
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádných	žádný	k3yNgFnPc2	žádný
smluv	smlouva	k1gFnPc2	smlouva
o	o	k7c6	o
základní	základní	k2eAgFnSc6d1	základní
dopravní	dopravní	k2eAgFnSc6d1	dopravní
obslužnosti	obslužnost	k1gFnSc6	obslužnost
a	a	k8xC	a
nečerpá	čerpat	k5eNaImIp3nS	čerpat
veřejné	veřejný	k2eAgInPc4d1	veřejný
prostředky	prostředek	k1gInPc4	prostředek
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Čerpá	čerpat	k5eAaImIp3nS	čerpat
však	však	k9	však
od	od	k7c2	od
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
kompenzace	kompenzace	k1gFnSc2	kompenzace
za	za	k7c4	za
studentské	studentský	k2eAgNnSc4d1	studentské
jízdné	jízdné	k1gNnSc4	jízdné
<g/>
:	:	kIx,	:
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
30	[number]	k4	30
164	[number]	k4	164
165,75	[number]	k4	165,75
Kč	Kč	kA	Kč
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
35	[number]	k4	35
550	[number]	k4	550
465,38	[number]	k4	465,38
Kč	Kč	kA	Kč
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
39	[number]	k4	39
502	[number]	k4	502
640,40	[number]	k4	640,40
Kč	Kč	kA	Kč
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
42,955	[number]	k4	42,955
mil	míle	k1gFnPc2	míle
Kč	Kč	kA	Kč
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
51,924	[number]	k4	51,924
mil	míle	k1gFnPc2	míle
Kč	Kč	kA	Kč
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
50,929	[number]	k4	50,929
mil	míle	k1gFnPc2	míle
Kč	Kč	kA	Kč
</s>
