<s>
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
Charles	Charles	k1gMnSc1
</s>
<s>
Princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Charles	Charles	k1gMnSc1
Philip	Philip	k1gMnSc1
Arthur	Arthur	k1gMnSc1
George	Georg	k1gMnSc4
</s>
<s>
Křest	křest	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1948	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1948	#num#	k4
(	(	kIx(
<g/>
72	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Londýn	Londýn	k1gInSc1
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Diana	Diana	k1gFnSc1
princezna	princezna	k1gFnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
</s>
<s>
Camilla	Camilla	k1gFnSc1
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Princ	princ	k1gMnSc1
William	William	k1gInSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
CambridgePrinc	CambridgePrinc	k1gFnSc1
Harry	Harra	k1gFnPc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
ze	z	k7c2
Sussexu	Sussex	k1gInSc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Windsorská	windsorský	k2eAgFnSc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Princ	princ	k1gMnSc1
Philip	Philip	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
královská	královský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
</s>
<s>
JV	JV	kA
královna	královna	k1gFnSc1
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
z	z	k7c2
WalesuJkV	WalesuJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
CambridgeJkV	CambridgeJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
George	Georg	k1gMnSc2
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Charlotte	Charlott	k1gInSc5
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
Louis	Louis	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Vévoda	vévoda	k1gMnSc1
ze	z	k7c2
SussexuVévodkyně	SussexuVévodkyně	k1gFnSc2
ze	z	k7c2
Sussexu	Sussex	k1gInSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Beatrice	Beatrice	k1gFnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Eugenie	Eugenie	k1gFnSc1
z	z	k7c2
Yorku	York	k1gInSc2
</s>
<s>
JkV	JkV	k?
hrabě	hrabě	k1gMnSc1
z	z	k7c2
WessexuJkV	WessexuJkV	k1gFnSc2
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Wessexu	Wessex	k1gInSc2
</s>
<s>
JkV	JkV	k?
královská	královský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
Anne	Ann	k1gFnSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
GloucesteruJkV	GloucesteruJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Gloucesteru	Gloucester	k1gInSc2
</s>
<s>
JkV	JkV	k?
vévoda	vévoda	k1gMnSc1
z	z	k7c2
KentuJkV	KentuJkV	k1gFnSc2
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Kentu	Kent	k1gInSc2
</s>
<s>
JkV	JkV	k?
princezna	princezna	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
</s>
<s>
JkV	JkV	k?
princ	princ	k1gMnSc1
Michael	Michael	k1gMnSc1
z	z	k7c2
KentuJkV	KentuJkV	k1gFnSc2
princezna	princezna	k1gFnSc1
Michaela	Michaela	k1gFnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Charles	Charles	k1gMnSc1
Philip	Philip	k1gMnSc1
Arthur	Arthur	k1gMnSc1
George	George	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
*	*	kIx~
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1948	#num#	k4
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
britské	britský	k2eAgFnSc2d1
královny	královna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
prince	princ	k1gMnSc2
Philipa	Philip	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
následníkem	následník	k1gMnSc7
trůnu	trůn	k1gInSc2
16	#num#	k4
suverénních	suverénní	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Bahamy	Bahamy	k1gFnPc1
<g/>
,	,	kIx,
Barbados	Barbados	k1gInSc1
<g/>
,	,	kIx,
Belize	Belize	k1gFnSc1
<g/>
,	,	kIx,
Grenada	Grenada	k1gFnSc1
<g/>
,	,	kIx,
Jamajka	Jamajka	k1gFnSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
,	,	kIx,
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
,	,	kIx,
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
<g/>
,	,	kIx,
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
<g/>
,	,	kIx,
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
<g/>
,	,	kIx,
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
a	a	k8xC
Tuvalu	Tuval	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Buckinghamském	buckinghamský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
narození	narození	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
královnou	královna	k1gFnSc7
britské	britský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charles	Charles	k1gMnSc1
studoval	studovat	k5eAaImAgMnS
prestižní	prestižní	k2eAgFnSc2d1
britské	britský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
jako	jako	k8xC,k8xS
každý	každý	k3xTgMnSc1
člen	člen	k1gMnSc1
britské	britský	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolvoval	absolvovat	k5eAaPmAgMnS
antropologii	antropologie	k1gFnSc4
a	a	k8xC
archeologii	archeologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1958	#num#	k4
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
kníže	kníže	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
manželství	manželství	k1gNnSc1
</s>
<s>
Diana	Diana	k1gFnSc1
<g/>
,	,	kIx,
princezna	princezna	k1gFnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
manželka	manželka	k1gFnSc1
prince	princ	k1gMnSc2
Charlese	Charles	k1gMnSc2
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
první	první	k4xOgFnSc7
manželkou	manželka	k1gFnSc7
byla	být	k5eAaImAgFnS
Diana	Diana	k1gFnSc1
Spencerová	Spencerová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgInS
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1981	#num#	k4
v	v	k7c6
londýnské	londýnský	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
sv.	sv.	kA
Pavla	Pavel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
svatbu	svatba	k1gFnSc4
sledovalo	sledovat	k5eAaImAgNnS
v	v	k7c6
televizi	televize	k1gFnSc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
na	na	k7c4
tři	tři	k4xCgFnPc4
čtvrtě	čtvrt	k1gFnPc4
miliardy	miliarda	k4xCgFnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diana	Diana	k1gFnSc1
svatbou	svatba	k1gFnSc7
získala	získat	k5eAaPmAgFnS
titul	titul	k1gInSc4
princezna	princezna	k1gFnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
hvězdou	hvězda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diana	Diana	k1gFnSc1
a	a	k8xC
Charles	Charles	k1gMnSc1
spolu	spolu	k6eAd1
mají	mít	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
děti	dítě	k1gFnPc1
–	–	k?
prince	princ	k1gMnSc2
Williama	William	k1gMnSc2
a	a	k8xC
prince	princ	k1gMnSc2
Harryho	Harry	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
mělo	mít	k5eAaImAgNnS
mnoho	mnoho	k4c4
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
především	především	k9
kvůli	kvůli	k7c3
Charlesově	Charlesův	k2eAgFnSc3d1
dlouholeté	dlouholetý	k2eAgFnSc3d1
přítelkyni	přítelkyně	k1gFnSc3
Camille	Camille	k1gNnSc2
Parker-Bowlesové	Parker-Bowlesový	k2eAgInPc4d1
a	a	k8xC
později	pozdě	k6eAd2
také	také	k9
kvůli	kvůli	k7c3
Dianiným	Dianin	k2eAgMnPc3d1
milencům	milenec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princezna	princezna	k1gFnSc1
Diana	Diana	k1gFnSc1
trpěla	trpět	k5eAaImAgFnS
depresemi	deprese	k1gFnPc7
a	a	k8xC
bulimií	bulimie	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přesto	přesto	k8xC
se	se	k3xPyFc4
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
charitě	charita	k1gFnSc6
<g/>
,	,	kIx,
boji	boj	k1gInSc6
proti	proti	k7c3
AIDS	AIDS	kA
apod.	apod.	kA
Problémy	problém	k1gInPc7
manželství	manželství	k1gNnSc2
byly	být	k5eAaImAgFnP
ale	ale	k9
velké	velký	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1996	#num#	k4
k	k	k7c3
rozvodu	rozvod	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1997	#num#	k4
princezna	princezna	k1gFnSc1
Diana	Diana	k1gFnSc1
zahynula	zahynout	k5eAaPmAgFnS
při	při	k7c6
automobilové	automobilový	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
Mercedesu	mercedes	k1gInSc6
i	i	k9
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
milencem	milenec	k1gMnSc7
Dodim	Dodim	k1gMnSc1
Al-Fayedem	Al-Fayed	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Druhé	druhý	k4xOgNnSc1
manželství	manželství	k1gNnSc1
</s>
<s>
Necelých	celý	k2eNgInPc2d1
devět	devět	k4xCc1
let	léto	k1gNnPc2
po	po	k7c6
rozvodu	rozvod	k1gInSc6
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
první	první	k4xOgFnSc7
ženou	žena	k1gFnSc7
si	se	k3xPyFc3
princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
vzal	vzít	k5eAaPmAgMnS
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
svou	svůj	k3xOyFgFnSc4
dlouholetou	dlouholetý	k2eAgFnSc4d1
přítelkyni	přítelkyně	k1gFnSc4
a	a	k8xC
jednu	jeden	k4xCgFnSc4
z	z	k7c2
příčin	příčina	k1gFnPc2
svého	svůj	k3xOyFgInSc2
rozvodu	rozvod	k1gInSc2
Camillu	Camilla	k1gFnSc4
Parker-Bowlesovou	Parker-Bowlesový	k2eAgFnSc7d1
v	v	k7c6
kapli	kaple	k1gFnSc6
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Camilla	Camilla	k1gFnSc1
neužívá	užívat	k5eNaImIp3nS
titulu	titul	k1gInSc3
princezna	princezna	k1gFnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
má	mít	k5eAaImIp3nS
díky	díky	k7c3
sňatku	sňatek	k1gInSc3
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgInSc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
titulů	titul	k1gInPc2
svého	své	k1gNnSc2
manžela	manžel	k1gMnSc2
nazývána	nazývat	k5eAaImNgFnS
vévodkyní	vévodkyně	k1gFnSc7
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Názory	názor	k1gInPc1
</s>
<s>
Princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
se	se	k3xPyFc4
osobně	osobně	k6eAd1
angažuje	angažovat	k5eAaBmIp3nS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
ochrany	ochrana	k1gFnSc2
klimatu	klima	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
v	v	k7c6
praxi	praxe	k1gFnSc6
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
chránit	chránit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc4
venkovské	venkovský	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
v	v	k7c6
Highgrove	Highgrov	k1gInSc5
je	být	k5eAaImIp3nS
zásobováno	zásobovat	k5eAaImNgNnS
elektřinou	elektřina	k1gFnSc7
vyráběnou	vyráběný	k2eAgFnSc7d1
z	z	k7c2
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
energie	energie	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInPc4
vozy	vůz	k1gInPc4
pohání	pohánět	k5eAaImIp3nS
bionafta	bionafta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
britským	britský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
nejstarší	starý	k2eAgInSc1d3
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
korunována	korunovat	k5eAaBmNgFnS
britským	britský	k2eAgMnSc7d1
monarchou	monarcha	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
i	i	k8xC
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
praprapravnukem	praprapravnuk	k1gInSc7
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
potomkem	potomek	k1gMnSc7
nejen	nejen	k6eAd1
českého	český	k2eAgInSc2d1
„	„	k?
<g/>
zimního	zimní	k2eAgMnSc4d1
<g/>
“	“	k?
krále	král	k1gMnSc4
Fridricha	Fridrich	k1gMnSc4
Falckého	falcký	k2eAgMnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
je	být	k5eAaImIp3nS
příznivcem	příznivec	k1gMnSc7
anglického	anglický	k2eAgInSc2d1
profesionálního	profesionální	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
Burnley	Burnlea	k1gFnSc2
FC	FC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Město	město	k1gNnSc1
Burnley	Burnlea	k1gFnSc2
navštěvuje	navštěvovat	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
a	a	k8xC
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
osobnímu	osobní	k2eAgInSc3d1
zájmu	zájem	k1gInSc3
o	o	k7c4
regeneraci	regenerace	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
charity	charita	k1gFnPc1
pak	pak	k6eAd1
podporují	podporovat	k5eAaImIp3nP
řadu	řada	k1gFnSc4
projektů	projekt	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
byl	být	k5eAaImAgInS
pozitivně	pozitivně	k6eAd1
testován	testovat	k5eAaImNgInS
na	na	k7c4
nemoc	nemoc	k1gFnSc4
covid-	covid-	k?
<g/>
19	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
několik	několik	k4yIc4
dní	den	k1gInPc2
vykazoval	vykazovat	k5eAaImAgMnS
mírné	mírný	k2eAgInPc4d1
příznaky	příznak	k1gInPc4
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
prince	princ	k1gMnSc2
Charlese	Charles	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
</s>
<s>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
(	(	kIx(
<g/>
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Rothesay	Rothesaa	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
1958	#num#	k4
Jeho	jeho	k3xOp3gFnSc4
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princ	princa	k1gFnPc2
z	z	k7c2
Walesu	Wales	k1gInSc2
(	(	kIx(
<g/>
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
Jeho	jeho	k3xOp3gFnSc1
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Rothesay	Rothesaa	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Kristián	Kristián	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dánský	dánský	k2eAgInSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
Řecký	řecký	k2eAgInSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Hesensko-Kasselská	hesensko-kasselský	k2eAgFnSc1d1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Řecký	řecký	k2eAgMnSc1d1
a	a	k8xC
Dánský	dánský	k2eAgMnSc1d1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Ruský	ruský	k2eAgMnSc1d1
</s>
<s>
Olga	Olga	k1gFnSc1
Konstantinovna	Konstantinovna	k1gFnSc1
Romanovová	Romanovová	k1gFnSc1
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Sasko-Altenburská	Sasko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Princ	princ	k1gMnSc1
Philip	Philip	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Hesensko-Darmstadtský	Hesensko-Darmstadtský	k2eAgMnSc1d1
</s>
<s>
Louis	Louis	k1gMnSc1
Battenberg	Battenberg	k1gMnSc1
</s>
<s>
Julie	Julie	k1gFnSc1
von	von	k1gInSc1
Hauke	Hauke	k1gFnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
z	z	k7c2
Battenbergu	Battenberg	k1gInSc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hesenský	hesenský	k2eAgInSc1d1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Alice	Alice	k1gFnSc1
Sasko-Koburská	Sasko-Koburský	k2eAgFnSc1d1
</s>
<s>
'	'	kIx"
<g/>
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
'	'	kIx"
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
Marie	Marie	k1gFnSc1
Adelaida	Adelaida	k1gFnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Claude	Claude	k6eAd1
Bowes-Lyon	Bowes-Lyon	k1gNnSc1
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
ze	z	k7c2
Strathmore	Strathmor	k1gInSc5
a	a	k8xC
Kinghorne	Kinghorn	k1gInSc5
</s>
<s>
Claude	Claude	k6eAd1
Bowes-Lyon	Bowes-Lyon	k1gNnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
ze	z	k7c2
Strathmore	Strathmor	k1gInSc5
a	a	k8xC
Kinghorne	Kinghorn	k1gInSc5
</s>
<s>
Frances	Frances	k1gInSc1
Bowes-Lyonová	Bowes-Lyonová	k1gFnSc1
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
Bowes-Lyon	Bowes-Lyona	k1gFnPc2
</s>
<s>
Charles	Charles	k1gMnSc1
Cavendish-Bentinck	Cavendish-Bentinck	k1gMnSc1
</s>
<s>
Cecilia	Cecilia	k1gFnSc1
Cavendish-Bentincková	Cavendish-Bentincková	k1gFnSc1
</s>
<s>
Louisa	Louisa	k?
Cavendish-Bentincková	Cavendish-Bentincková	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.ceskenoviny.cz/tema/index_view.php?id=366868&	http://www.ceskenoviny.cz/tema/index_view.php?id=366868&	k?
<g/>
↑	↑	k?
LOUŽECKÝ	LOUŽECKÝ	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Drocár	Drocár	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historická	historický	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
–	–	k?
život	život	k1gInSc4
po	po	k7c6
meči	meč	k1gInSc6
a	a	k8xC
po	po	k7c6
přeslici	přeslice	k1gFnSc6
<g/>
.	.	kIx.
www.historickaslechta.cz	www.historickaslechta.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
'	'	kIx"
<g/>
Closet	Closet	k1gMnSc1
Claret	Claret	k1gMnSc1
<g/>
'	'	kIx"
<g/>
:	:	kIx,
Prince	princ	k1gMnSc2
Charles	Charles	k1gMnSc1
admits	admits	k1gInSc1
to	ten	k3xDgNnSc1
being	being	k1gMnSc1
a	a	k8xC
Burnley	Burnlea	k1gFnPc1
FC	FC	kA
Fan	Fana	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
dailymail	dailymail	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
15.02	15.02	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Prince	princ	k1gMnSc2
Charles	Charles	k1gMnSc1
joins	joins	k1gInSc4
congratulations	congratulations	k1gInSc4
for	forum	k1gNnPc2
Burnley	Burnlea	k1gMnSc2
being	beinga	k1gFnPc2
named	named	k1gMnSc1
Britain	Britain	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Most	most	k1gInSc1
Enterprising	Enterprising	k1gInSc1
Area	area	k1gFnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
burnley	burnley	k1gInPc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
27.08	27.08	k4
<g/>
.2013	.2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Coronavirus	Coronavirus	k1gInSc1
<g/>
:	:	kIx,
Prince	princ	k1gMnSc2
Charles	Charles	k1gMnSc1
tests	tests	k1gInSc1
positive	positiv	k1gInSc5
for	forum	k1gNnPc2
COVID-19	COVID-19	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sky	Sky	k1gFnSc1
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2020-03-25	2020-03-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karel	Karla	k1gFnPc2
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
je	být	k5eAaImIp3nS
nejznámějším	známý	k2eAgMnSc7d3
čekatelem	čekatel	k1gMnSc7
na	na	k7c4
trůn	trůn	k1gInSc4
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Princ	princ	k1gMnSc1
Charles	Charles	k1gMnSc1
<g/>
:	:	kIx,
Geneticky	geneticky	k6eAd1
modifikované	modifikovaný	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
způsobí	způsobit	k5eAaPmIp3nS
světovou	světový	k2eAgFnSc4d1
ekologickou	ekologický	k2eAgFnSc4d1
katastrofu	katastrofa	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
JKV	JKV	kA
knížete	kníže	k1gMnSc4
z	z	k7c2
Walesu	Wales	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
statků	statek	k1gInPc2
Cornwallského	Cornwallský	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Vojenský	vojenský	k2eAgInSc1d1
postup	postup	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Official	Official	k1gInSc1
website	websit	k1gInSc5
of	of	k?
'	'	kIx"
<g/>
The	The	k1gMnSc2
Prince	princ	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Trust	trust	k1gInSc1
<g/>
'	'	kIx"
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Edward	Edward	k1gMnSc1
Albert	Albert	k1gMnSc1
Christian	Christian	k1gMnSc1
David	David	k1gMnSc1
</s>
<s>
Princ	princ	k1gMnSc1
z	z	k7c2
Walesu	Wales	k1gInSc2
Charles	Charles	k1gMnSc1
Philip	Philip	k1gMnSc1
Arthur	Arthur	k1gMnSc1
Georgeod	Georgeod	k1gInSc4
1958	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
dosud	dosud	k6eAd1
ve	v	k7c6
funkci	funkce	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700760	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118520180	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2137	#num#	k4
9521	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78089005	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500185586	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84034215	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78089005	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Monarchie	monarchie	k1gFnSc1
</s>
