<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ב	ב	k?	ב
ה	ה	k?	ה
ו	ו	k?	ו
ל	ל	k?	ל
ה	ה	k?	ה
ע	ע	k?	ע
<g/>
"	"	kIx"	"
<g/>
ש	ש	k?	ש
צ	צ	k?	צ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
synagoga	synagoga	k1gFnSc1	synagoga
Telaviské	Telaviský	k2eAgFnSc2d1	Telaviský
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
architektem	architekt	k1gMnSc7	architekt
Mario	Mario	k1gMnSc1	Mario
Bottou	Botta	k1gMnSc7	Botta
a	a	k8xC	a
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Mecenáši	mecenáš	k1gMnPc1	mecenáš
a	a	k8xC	a
jmenovci	jmenovec	k1gMnPc1	jmenovec
byli	být	k5eAaImAgMnP	být
Paulette	Paulett	k1gInSc5	Paulett
a	a	k8xC	a
Norbert	Norbert	k1gMnSc1	Norbert
Cymbalistovi	Cymbalista	k1gMnSc3	Cymbalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Půdorys	půdorys	k1gInSc1	půdorys
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
přibližně	přibližně	k6eAd1	přibližně
760	[number]	k4	760
až	až	k9	až
800	[number]	k4	800
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obdélníkové	obdélníkový	k2eAgFnSc2d1	obdélníková
základny	základna	k1gFnSc2	základna
stoupají	stoupat	k5eAaImIp3nP	stoupat
dvě	dva	k4xCgFnPc1	dva
párové	párový	k2eAgFnPc1d1	párová
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
úpatí	úpatí	k1gNnSc6	úpatí
čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
půdorys	půdorys	k1gInSc4	půdorys
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
kruh	kruh	k1gInSc4	kruh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
vrcholu	vrchol	k1gInSc6	vrchol
poloměr	poloměr	k1gInSc4	poloměr
13,5	[number]	k4	13,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
věže	věž	k1gFnPc4	věž
spojuje	spojovat	k5eAaImIp3nS	spojovat
pravoúhlá	pravoúhlý	k2eAgFnSc1d1	pravoúhlá
hala	hala	k1gFnSc1	hala
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
architektonická	architektonický	k2eAgFnSc1d1	architektonická
podoba	podoba	k1gFnSc1	podoba
věží	věž	k1gFnSc7	věž
je	být	k5eAaImIp3nS	být
realizací	realizace	k1gFnSc7	realizace
konstrukční	konstrukční	k2eAgFnSc2d1	konstrukční
úlohy	úloha	k1gFnSc2	úloha
kvadratura	kvadratura	k1gFnSc1	kvadratura
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
deska	deska	k1gFnSc1	deska
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
baldachýnu	baldachýn	k1gInSc2	baldachýn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
přírodní	přírodní	k2eAgNnSc4d1	přírodní
světlo	světlo	k1gNnSc4	světlo
po	po	k7c6	po
stěnách	stěna	k1gFnPc6	stěna
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
tradiční	tradiční	k2eAgInSc1d1	tradiční
židovský	židovský	k2eAgInSc1d1	židovský
svatební	svatební	k2eAgInSc1d1	svatební
baldachýn	baldachýn	k1gInSc1	baldachýn
zvaný	zvaný	k2eAgInSc1d1	zvaný
chupa	chupa	k1gFnSc1	chupa
<g/>
.	.	kIx.	.
</s>
<s>
Aron	Aron	k1gNnSc4	Aron
ha-kodeš	haodat	k5eAaPmIp2nS	ha-kodat
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
nasvícen	nasvítit	k5eAaPmNgInS	nasvítit
průsvitným	průsvitný	k2eAgInSc7d1	průsvitný
onyxem	onyx	k1gInSc7	onyx
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
verš	verš	k1gInSc1	verš
z	z	k7c2	z
Žalmů	žalm	k1gInPc2	žalm
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
ש	ש	k?	ש
י	י	k?	י
ל	ל	k?	ל
ת	ת	k?	ת
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hospodina	Hospodin	k1gMnSc4	Hospodin
stále	stále	k6eAd1	stále
před	před	k7c4	před
oči	oko	k1gNnPc4	oko
si	se	k3xPyFc3	se
stavím	stavit	k5eAaBmIp1nS	stavit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Žalm	žalm	k1gInSc1	žalm
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektonické	architektonický	k2eAgFnSc6d1	architektonická
souvislosti	souvislost	k1gFnSc6	souvislost
==	==	k?	==
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slavní	slavný	k2eAgMnPc1d1	slavný
architekti	architekt	k1gMnPc1	architekt
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
množství	množství	k1gNnSc3	množství
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
tak	tak	k6eAd1	tak
kreativní	kreativní	k2eAgFnSc4d1	kreativní
moderní	moderní	k2eAgFnSc4d1	moderní
architekturu	architektura	k1gFnSc4	architektura
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
židovskými	židovský	k2eAgFnPc7d1	židovská
institucemi	instituce	k1gFnPc7	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
taková	takový	k3xDgFnSc1	takový
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
Židovské	židovský	k2eAgNnSc1d1	Židovské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
hostila	hostit	k5eAaImAgFnS	hostit
výstavu	výstava	k1gFnSc4	výstava
Židovská	židovský	k2eAgFnSc1d1	židovská
identita	identita	k1gFnSc1	identita
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
(	(	kIx(	(
<g/>
Jewish	Jewish	k1gInSc1	Jewish
Identity	identita	k1gFnSc2	identita
in	in	k?	in
Architecture	Architectur	k1gMnSc5	Architectur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
představovala	představovat	k5eAaImAgFnS	představovat
synagogu	synagoga	k1gFnSc4	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaní	uznávaný	k2eAgMnPc1d1	uznávaný
architekti	architekt	k1gMnPc1	architekt
nové	nový	k2eAgFnSc2d1	nová
vzhledy	vzhled	k1gInPc4	vzhled
synagog	synagoga	k1gFnPc2	synagoga
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
moderní	moderní	k2eAgFnSc6d1	moderní
podobě	podoba	k1gFnSc6	podoba
<g/>
:	:	kIx,	:
kongregace	kongregace	k1gFnSc1	kongregace
Bet	Bet	k1gMnSc1	Bet
Šolom	Šolom	k1gInSc1	Šolom
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
navržená	navržený	k2eAgFnSc1d1	navržená
Frankem	Frank	k1gMnSc7	Frank
Lloydem	Lloyd	k1gMnSc7	Lloyd
Wrightem	Wright	k1gMnSc7	Wright
či	či	k8xC	či
navrhovaná	navrhovaný	k2eAgFnSc1d1	navrhovaná
přestavba	přestavba	k1gFnSc1	přestavba
synagogy	synagoga	k1gFnSc2	synagoga
Churva	Churva	k1gFnSc1	Churva
architektem	architekt	k1gMnSc7	architekt
Louisem	Louis	k1gMnSc7	Louis
Kahnem	Kahn	k1gMnSc7	Kahn
<g/>
.	.	kIx.	.
</s>
<s>
Mario	Mario	k1gMnSc1	Mario
Botta	Botta	k1gMnSc1	Botta
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
budovu	budova	k1gFnSc4	budova
Sanfranciského	sanfranciský	k2eAgNnSc2d1	sanfranciské
muzea	muzeum	k1gNnSc2	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
i	i	k9	i
katedrálu	katedrála	k1gFnSc4	katedrála
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Korbiniána	Korbinián	k1gMnSc2	Korbinián
v	v	k7c4	v
Évry	Évry	k1gInPc4	Évry
<g/>
,	,	kIx,	,
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
válcovitým	válcovitý	k2eAgInSc7d1	válcovitý
tvarem	tvar	k1gInSc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
byla	být	k5eAaImAgFnS	být
dvanáctým	dvanáctý	k4xOgFnPc3	dvanáctý
projektem	projekt	k1gInSc7	projekt
a	a	k8xC	a
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
série	série	k1gFnSc2	série
náboženských	náboženský	k2eAgFnPc2d1	náboženská
prací	práce	k1gFnPc2	práce
Mario	Mario	k1gMnSc1	Mario
Botty	Botta	k1gFnSc2	Botta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
představeny	představit	k5eAaPmNgFnP	představit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
Královském	královský	k2eAgInSc6d1	královský
institutu	institut	k1gInSc6	institut
britských	britský	k2eAgMnPc2d1	britský
architektů	architekt	k1gMnPc2	architekt
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
s	s	k7c7	s
názvem	název	k1gInSc7	název
Architetture	Architettur	k1gMnSc5	Architettur
del	del	k?	del
Sacro	Sacro	k1gNnSc1	Sacro
<g/>
:	:	kIx,	:
Prayers	Prayers	k1gInSc1	Prayers
in	in	k?	in
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
Synagogue	Synagogu	k1gFnSc2	Synagogu
and	and	k?	and
Jewish	Jewish	k1gMnSc1	Jewish
Heritage	Heritag	k1gFnSc2	Heritag
Center	centrum	k1gNnPc2	centrum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Telavivská	telavivský	k2eAgFnSc1d1	Telavivská
univerzita	univerzita	k1gFnSc1	univerzita
-	-	kIx~	-
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
na	na	k7c6	na
Structurae	Structurae	k1gFnSc6	Structurae
</s>
</p>
