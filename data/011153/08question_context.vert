<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ב	ב	k?	ב
ה	ה	k?	ה
ו	ו	k?	ו
ל	ל	k?	ל
ה	ה	k?	ה
ע	ע	k?	ע
<g/>
"	"	kIx"	"
<g/>
ש	ש	k?	ש
צ	צ	k?	צ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
synagoga	synagoga	k1gFnSc1	synagoga
Telaviské	Telaviský	k2eAgFnSc2d1	Telaviský
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
architektem	architekt	k1gMnSc7	architekt
Mario	Mario	k1gMnSc1	Mario
Bottou	Botta	k1gMnSc7	Botta
a	a	k8xC	a
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
