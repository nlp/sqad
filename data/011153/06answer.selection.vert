<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Cymbalista	Cymbalista	k1gMnSc1	Cymbalista
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
dědictví	dědictví	k1gNnSc2	dědictví
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ב	ב	k?	ב
ה	ה	k?	ה
ו	ו	k?	ו
ל	ל	k?	ל
ה	ה	k?	ה
ע	ע	k?	ע
<g/>
"	"	kIx"	"
<g/>
ש	ש	k?	ש
צ	צ	k?	צ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
synagoga	synagoga	k1gFnSc1	synagoga
Telaviské	Telaviský	k2eAgFnSc2d1	Telaviský
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
