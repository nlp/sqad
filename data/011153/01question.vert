<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
synagoga	synagoga	k1gFnSc1	synagoga
navržena	navržen	k2eAgFnSc1d1	navržena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
architektem	architekt	k1gMnSc7	architekt
Mario	Mario	k1gMnSc1	Mario
Bottou	Botta	k1gMnSc7	Botta
a	a	k8xC	a
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
<g/>
?	?	kIx.	?
</s>
