<s>
Ernest	Ernest	k1gMnSc1	Ernest
Miller	Miller	k1gMnSc1	Miller
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
[	[	kIx(	[
<g/>
hemingvej	hemingvat	k5eAaImRp2nS	hemingvat
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1899	[number]	k4	1899
Oak	Oak	k1gMnSc2	Oak
Park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc4	Illinois
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc4	červenec
1961	[number]	k4	1961
Ketchum	Ketchum	k1gNnSc4	Ketchum
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc2	Ida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
čelního	čelní	k2eAgMnSc4d1	čelní
představitele	představitel	k1gMnSc4	představitel
tzv.	tzv.	kA	tzv.
ztracené	ztracený	k2eAgFnPc4d1	ztracená
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Pulitzerovou	Pulitzerův	k2eAgFnSc7d1	Pulitzerova
cenou	cena	k1gFnSc7	cena
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
příkladem	příklad	k1gInSc7	příklad
a	a	k8xC	a
nadšením	nadšení	k1gNnSc7	nadšení
záhy	záhy	k6eAd1	záhy
v	v	k7c6	v
synovi	syn	k1gMnSc3	syn
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
<g/>
,	,	kIx,	,
lovu	lov	k1gInSc3	lov
a	a	k8xC	a
sportu	sport	k1gInSc3	sport
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
zbožná	zbožný	k2eAgFnSc1d1	zbožná
a	a	k8xC	a
chtěla	chtít	k5eAaImAgFnS	chtít
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
mít	mít	k5eAaImF	mít
slušně	slušně	k6eAd1	slušně
vychované	vychovaný	k2eAgNnSc4d1	vychované
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
boxoval	boxovat	k5eAaImAgMnS	boxovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
o	o	k7c4	o
verše	verš	k1gInPc4	verš
<g/>
,	,	kIx,	,
povídkové	povídkový	k2eAgInPc4d1	povídkový
črty	črt	k1gInPc4	črt
a	a	k8xC	a
sloupky	sloupek	k1gInPc4	sloupek
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
reportérem	reportér	k1gMnSc7	reportér
kansaského	kansaský	k2eAgInSc2d1	kansaský
listu	list	k1gInSc2	list
Star	Star	kA	Star
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
jako	jako	k8xC	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
ambulantních	ambulantní	k2eAgInPc2d1	ambulantní
sborů	sbor	k1gInPc2	sbor
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
americký	americký	k2eAgMnSc1d1	americký
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
raněn	ranit	k5eAaPmNgMnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
reportér	reportér	k1gMnSc1	reportér
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
jako	jako	k9	jako
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
,	,	kIx,	,
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Hemingway	Hemingwa	k1gMnPc4	Hemingwa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
amerických	americký	k2eAgMnPc2d1	americký
intelektuálů	intelektuál	k1gMnPc2	intelektuál
soustředěných	soustředěný	k2eAgMnPc2d1	soustředěný
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
kolem	kolem	k7c2	kolem
Gertrudy	Gertruda	k1gFnSc2	Gertruda
Steinové	Steinová	k1gFnSc2	Steinová
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
generace	generace	k1gFnSc1	generace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
trpké	trpký	k2eAgNnSc1d1	trpké
a	a	k8xC	a
bolestné	bolestný	k2eAgNnSc1d1	bolestné
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
nutí	nutit	k5eAaImIp3nS	nutit
zamýšlet	zamýšlet	k5eAaImF	zamýšlet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
motivy	motiv	k1gInPc7	motiv
lidské	lidský	k2eAgFnSc2d1	lidská
statečnosti	statečnost	k1gFnSc2	statečnost
a	a	k8xC	a
nezdolnosti	nezdolnost	k1gFnSc2	nezdolnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
nástupu	nástup	k1gInSc2	nástup
fašismu	fašismus	k1gInSc2	fašismus
se	se	k3xPyFc4	se
stupňuje	stupňovat	k5eAaImIp3nS	stupňovat
jeho	jeho	k3xOp3gNnSc1	jeho
humanistické	humanistický	k2eAgNnSc1d1	humanistické
cítění	cítění	k1gNnSc1	cítění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Hemingway	Hemingwa	k1gMnPc4	Hemingwa
byl	být	k5eAaImAgInS	být
mistrem	mistr	k1gMnSc7	mistr
krátkých	krátký	k2eAgFnPc2d1	krátká
forem	forma	k1gFnPc2	forma
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
úsporný	úsporný	k2eAgInSc1d1	úsporný
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pojmem	pojem	k1gInSc7	pojem
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
světové	světový	k2eAgFnSc6d1	světová
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
asi	asi	k9	asi
nejznámější	známý	k2eAgNnSc1d3	nejznámější
dílo	dílo	k1gNnSc1	dílo
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Nick	Nicka	k1gFnPc2	Nicka
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
svého	svůj	k1gMnSc2	svůj
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgInS	strávit
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
(	(	kIx(	(
<g/>
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
se	se	k3xPyFc4	se
puškou	puška	k1gFnSc7	puška
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
žádný	žádný	k3yNgInSc4	žádný
vysvětlující	vysvětlující	k2eAgInSc4d1	vysvětlující
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
vysvětlována	vysvětlován	k2eAgFnSc1d1	vysvětlována
jako	jako	k8xC	jako
nehoda	nehoda	k1gFnSc1	nehoda
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
hlavně	hlavně	k9	hlavně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
sebevraždu	sebevražda	k1gFnSc4	sebevražda
mluví	mluvit	k5eAaImIp3nS	mluvit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zkušený	zkušený	k2eAgMnSc1d1	zkušený
lovec	lovec	k1gMnSc1	lovec
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
je	být	k5eAaImIp3nS	být
takováto	takovýto	k3xDgFnSc1	takovýto
nehoda	nehoda	k1gFnSc1	nehoda
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
depresemi	deprese	k1gFnPc7	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
na	na	k7c6	na
pravděpodobnosti	pravděpodobnost	k1gFnSc6	pravděpodobnost
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Hemingway	Hemingwaa	k1gFnPc4	Hemingwaa
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
nebezpečných	bezpečný	k2eNgInPc6d1	nebezpečný
sportech	sport	k1gInPc6	sport
(	(	kIx(	(
<g/>
býčí	býčí	k2eAgInPc1d1	býčí
zápasy	zápas	k1gInPc1	zápas
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
lovu	lov	k1gInSc6	lov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
povídkách	povídka	k1gFnPc6	povídka
hledal	hledat	k5eAaImAgInS	hledat
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
v	v	k7c6	v
mezní	mezní	k2eAgFnSc6d1	mezní
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
své	svůj	k3xOyFgMnPc4	svůj
hrdiny	hrdina	k1gMnPc4	hrdina
stavěl	stavět	k5eAaImAgMnS	stavět
do	do	k7c2	do
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
prokazovali	prokazovat	k5eAaImAgMnP	prokazovat
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
jeho	jeho	k3xOp3gNnSc2	jeho
psaní	psaní	k1gNnSc2	psaní
je	být	k5eAaImIp3nS	být
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
této	tento	k3xDgFnSc6	tento
literatuře	literatura	k1gFnSc6	literatura
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
úsečný	úsečný	k2eAgMnSc1d1	úsečný
<g/>
,	,	kIx,	,
zbavený	zbavený	k2eAgMnSc1d1	zbavený
všeho	všecek	k3xTgNnSc2	všecek
nadbytečného	nadbytečný	k2eAgNnSc2d1	nadbytečné
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgNnSc1	žádný
citové	citový	k2eAgNnSc1d1	citové
zabarvení	zabarvení	k1gNnSc1	zabarvení
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
píše	psát	k5eAaImIp3nS	psát
pouze	pouze	k6eAd1	pouze
dialog	dialog	k1gInSc4	dialog
bez	bez	k7c2	bez
dalších	další	k2eAgInPc2d1	další
komentářů	komentář	k1gInPc2	komentář
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
objektivní	objektivní	k2eAgNnSc1d1	objektivní
vypravěčství	vypravěčství	k1gNnSc1	vypravěčství
<g/>
.	.	kIx.	.
</s>
<s>
Počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
podtextem	podtext	k1gInSc7	podtext
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
osobními	osobní	k2eAgInPc7d1	osobní
zážitky	zážitek	k1gInPc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc4	postava
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vedou	vést	k5eAaImIp3nP	vést
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
rybáři	rybář	k1gMnPc1	rybář
<g/>
,	,	kIx,	,
lovci	lovec	k1gMnPc1	lovec
<g/>
,	,	kIx,	,
toreadoři	toreador	k1gMnPc1	toreador
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fiesta	fiesta	k1gFnSc1	fiesta
-	-	kIx~	-
1926	[number]	k4	1926
-	-	kIx~	-
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
hrdinovi	hrdina	k1gMnSc3	hrdina
Jakobu	Jakob	k1gMnSc3	Jakob
Barnesovi	Barnes	k1gMnSc3	Barnes
brání	bránit	k5eAaImIp3nP	bránit
válečné	válečná	k1gFnPc1	válečná
zmrzačení	zmrzačení	k1gNnSc4	zmrzačení
v	v	k7c4	v
naplnění	naplnění	k1gNnSc4	naplnění
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
Brett	Brett	k1gInSc4	Brett
Ashleyové	Ashleyové	k2eAgFnPc1d1	Ashleyové
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
nemůže	moct	k5eNaImIp3nS	moct
chodit	chodit	k5eAaImF	chodit
-	-	kIx~	-
ona	onen	k3xDgFnSc1	onen
to	ten	k3xDgNnSc4	ten
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
nevěrná	věrný	k2eNgFnSc1d1	nevěrná
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
reportáž	reportáž	k1gFnSc1	reportáž
z	z	k7c2	z
každoroční	každoroční	k2eAgFnSc2d1	každoroční
fiesty	fiesta	k1gFnSc2	fiesta
v	v	k7c6	v
Pamploně	Pamplona	k1gFnSc6	Pamplona
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
-	-	kIx~	-
běh	běh	k1gInSc1	běh
býků	býk	k1gMnPc2	býk
ulicemi	ulice	k1gFnPc7	ulice
do	do	k7c2	do
arény	aréna	k1gFnSc2	aréna
a	a	k8xC	a
korida	korida	k1gFnSc1	korida
<g/>
.	.	kIx.	.
</s>
<s>
Sbohem	sbohem	k0	sbohem
armádo	armáda	k1gFnSc5	armáda
-	-	kIx~	-
1929	[number]	k4	1929
-	-	kIx~	-
Sbohem	sbohem	k0	sbohem
armádo	armáda	k1gFnSc5	armáda
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
s	s	k7c7	s
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěčem	vypravěč	k1gMnSc7	vypravěč
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
poručík	poručík	k1gMnSc1	poručík
Frederic	Frederic	k1gMnSc1	Frederic
Henry	Henry	k1gMnSc1	Henry
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
převzat	převzít	k5eAaPmNgInS	převzít
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
anglického	anglický	k2eAgMnSc2d1	anglický
dramatika	dramatik	k1gMnSc2	dramatik
George	Georg	k1gMnSc2	Georg
Peelea	Peeleus	k1gMnSc2	Peeleus
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
u	u	k7c2	u
příbuzných	příbuzný	k1gMnPc2	příbuzný
z	z	k7c2	z
manželčiny	manželčin	k2eAgFnSc2d1	manželčina
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Piggottu	Piggott	k1gInSc6	Piggott
v	v	k7c6	v
Arkansasu	Arkansas	k1gInSc6	Arkansas
a	a	k8xC	a
v	v	k7c6	v
domě	dům	k1gInSc6	dům
přátel	přítel	k1gMnPc2	přítel
Hemingwayovy	Hemingwayův	k2eAgFnSc2d1	Hemingwayova
manželky	manželka	k1gFnSc2	manželka
Pauline	Paulin	k1gInSc5	Paulin
Pfeiffer	Pfeiffra	k1gFnPc2	Pfeiffra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
čekala	čekat	k5eAaImAgFnS	čekat
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
dějovou	dějový	k2eAgFnSc4d1	dějová
linii	linie	k1gFnSc4	linie
románu	román	k1gInSc2	román
Sbohem	sbohem	k0	sbohem
armádo	armáda	k1gFnSc5	armáda
tvoří	tvořit	k5eAaImIp3nS	tvořit
příběh	příběh	k1gInSc1	příběh
tragické	tragický	k2eAgFnSc2d1	tragická
lásky	láska	k1gFnSc2	láska
mezi	mezi	k7c7	mezi
americkým	americký	k2eAgMnSc7d1	americký
vojákem	voják	k1gMnSc7	voják
Frederikem	Frederik	k1gMnSc7	Frederik
Henrym	Henry	k1gMnSc7	Henry
a	a	k8xC	a
britskou	britský	k2eAgFnSc7d1	britská
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
sestrou	sestra	k1gFnSc7	sestra
Catherine	Catherin	k1gInSc5	Catherin
Barkleyovou	Barkleyová	k1gFnSc4	Barkleyová
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
osobní	osobní	k2eAgFnSc2d1	osobní
tragédie	tragédie	k1gFnSc2	tragédie
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
střetává	střetávat	k5eAaImIp3nS	střetávat
s	s	k7c7	s
tragédií	tragédie	k1gFnSc7	tragédie
celospolečenskou	celospolečenský	k2eAgFnSc7d1	celospolečenská
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
cynismus	cynismus	k1gInSc4	cynismus
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
vytěsnění	vytěsnění	k1gNnSc1	vytěsnění
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vydáním	vydání	k1gNnSc7	vydání
románu	román	k1gInSc2	román
Sbohem	sbohem	k0	sbohem
armádo	armáda	k1gFnSc5	armáda
byla	být	k5eAaImAgFnS	být
posílena	posílit	k5eAaPmNgFnS	posílit
Hemingwayova	Hemingwayův	k2eAgFnSc1d1	Hemingwayova
pozice	pozice	k1gFnSc1	pozice
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
zfilmován	zfilmován	k2eAgInSc1d1	zfilmován
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
<g/>
:	:	kIx,	:
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
popisuje	popisovat	k5eAaImIp3nS	popisovat
začátek	začátek	k1gInSc4	začátek
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
Henrym	Henry	k1gMnSc7	Henry
a	a	k8xC	a
Catherine	Catherin	k1gInSc5	Catherin
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
frontě	fronta	k1gFnSc6	fronta
utrpí	utrpět	k5eAaPmIp3nS	utrpět
Henry	henry	k1gInSc7	henry
zranění	zranění	k1gNnSc2	zranění
kolena	koleno	k1gNnSc2	koleno
zásahem	zásah	k1gInSc7	zásah
střepiny	střepina	k1gFnPc4	střepina
z	z	k7c2	z
granátu	granát	k1gInSc2	granát
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	díl	k1gInSc6	díl
sledujeme	sledovat	k5eAaImIp1nP	sledovat
pokračující	pokračující	k2eAgInSc4d1	pokračující
vztah	vztah	k1gInSc4	vztah
Henryho	Henry	k1gMnSc2	Henry
a	a	k8xC	a
Catherine	Catherin	k1gInSc5	Catherin
během	během	k7c2	během
jejich	jejich	k3xOp3gInSc2	jejich
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Catherine	Catherin	k1gInSc5	Catherin
zamilován	zamilován	k2eAgMnSc1d1	zamilován
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
během	během	k7c2	během
jeho	jeho	k3xOp3gInSc2	jeho
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
otěhotní	otěhotnět	k5eAaPmIp3nS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
Henry	Henry	k1gMnSc1	Henry
vrací	vracet	k5eAaImIp3nS	vracet
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
jednotce	jednotka	k1gFnSc3	jednotka
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Caporetto	Caporetto	k1gNnSc4	Caporetto
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
armáda	armáda	k1gFnSc1	armáda
kapituluje	kapitulovat	k5eAaBmIp3nS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
vyšetřován	vyšetřovat	k5eAaImNgInS	vyšetřovat
kvůli	kvůli	k7c3	kvůli
zabití	zabití	k1gNnSc3	zabití
důstojníka	důstojník	k1gMnSc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
Catherine	Catherin	k1gInSc5	Catherin
a	a	k8xC	a
Henry	henry	k1gInPc1	henry
znovu	znovu	k6eAd1	znovu
setkávají	setkávat	k5eAaImIp3nP	setkávat
a	a	k8xC	a
odplouvají	odplouvat	k5eAaImIp3nP	odplouvat
spolu	spolu	k6eAd1	spolu
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
díle	dílo	k1gNnSc6	dílo
Henry	henry	k1gInSc2	henry
a	a	k8xC	a
Catherine	Catherin	k1gInSc5	Catherin
žijí	žít	k5eAaImIp3nP	žít
klidný	klidný	k2eAgInSc4d1	klidný
život	život	k1gInSc4	život
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Catherine	Catherin	k1gInSc5	Catherin
umírá	umírat	k5eAaImIp3nS	umírat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
porodí	porodit	k5eAaPmIp3nS	porodit
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Cenzura	cenzura	k1gFnSc1	cenzura
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
Scribnerově	Scribnerův	k2eAgNnSc6d1	Scribnerův
vydání	vydání	k1gNnSc6	vydání
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
vulgarit	vulgarita	k1gFnPc2	vulgarita
obsažených	obsažený	k2eAgFnPc2d1	obsažená
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
slovíčka	slovíčko	k1gNnSc2	slovíčko
"	"	kIx"	"
<g/>
shit	shit	k1gInSc1	shit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
fuck	fuck	k6eAd1	fuck
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
cocksucker	cocksucker	k1gInSc1	cocksucker
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
pomlčkami	pomlčka	k1gFnPc7	pomlčka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
---	---	k?	---
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
minimálně	minimálně	k6eAd1	minimálně
dva	dva	k4xCgInPc4	dva
výtisky	výtisk	k1gInPc4	výtisk
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
Hemingway	Hemingway	k1gInPc4	Hemingway
přepsal	přepsat	k5eAaPmAgInS	přepsat
cenzurovaný	cenzurovaný	k2eAgInSc1d1	cenzurovaný
text	text	k1gInSc1	text
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
dvou	dva	k4xCgNnPc2	dva
vydání	vydání	k1gNnPc2	vydání
bylo	být	k5eAaImAgNnS	být
věnováno	věnovat	k5eAaPmNgNnS	věnovat
Maurice	Maurika	k1gFnSc3	Maurika
Coindreauovi	Coindreauův	k2eAgMnPc1d1	Coindreauův
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
Jamesi	Jamese	k1gFnSc4	Jamese
Joyceovi	Joyceus	k1gMnSc3	Joyceus
<g/>
.	.	kIx.	.
</s>
<s>
Hemingwayem	Hemingwayem	k6eAd1	Hemingwayem
korigovaný	korigovaný	k2eAgInSc1d1	korigovaný
text	text	k1gInSc1	text
nebyl	být	k5eNaImAgMnS	být
začleněn	začlenit	k5eAaPmNgMnS	začlenit
do	do	k7c2	do
žádného	žádný	k3yNgNnSc2	žádný
vydání	vydání	k1gNnSc2	vydání
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Komu	kdo	k3yQnSc3	kdo
zvoní	zvonit	k5eAaImIp3nP	zvonit
hrana	hrana	k1gNnPc4	hrana
-	-	kIx~	-
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
námětem	námět	k1gInSc7	námět
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
románu	román	k1gInSc3	román
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
3	[number]	k4	3
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
prožije	prožít	k5eAaPmIp3nS	prožít
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
republikánů	republikán	k1gMnPc2	republikán
americký	americký	k2eAgMnSc1d1	americký
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
Robert	Robert	k1gMnSc1	Robert
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
.	.	kIx.	.
</s>
<s>
Partyzáni	partyzán	k1gMnPc1	partyzán
chtějí	chtít	k5eAaImIp3nP	chtít
vyhodit	vyhodit	k5eAaPmF	vyhodit
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgNnSc6	který
by	by	kYmCp3nS	by
nepřítel	nepřítel	k1gMnSc1	nepřítel
mohl	moct	k5eAaImAgMnS	moct
dopravit	dopravit	k5eAaPmF	dopravit
posily	posila	k1gFnPc4	posila
<g/>
,	,	kIx,	,
Jordan	Jordan	k1gMnSc1	Jordan
sice	sice	k8xC	sice
o	o	k7c6	o
zdaru	zdar	k1gInSc6	zdar
akce	akce	k1gFnSc2	akce
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
se	se	k3xPyFc4	se
Jordan	Jordan	k1gMnSc1	Jordan
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
dcery	dcera	k1gFnSc2	dcera
republikánského	republikánský	k2eAgMnSc2d1	republikánský
starosty	starosta	k1gMnSc2	starosta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
fašisty	fašista	k1gMnPc7	fašista
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
podaří	podařit	k5eAaPmIp3nS	podařit
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
destrukci	destrukce	k1gFnSc6	destrukce
zemře	zemřít	k5eAaPmIp3nS	zemřít
několik	několik	k4yIc1	několik
Jordanových	Jordanových	k2eAgMnPc2d1	Jordanových
společníků	společník	k1gMnPc2	společník
a	a	k8xC	a
Jordan	Jordan	k1gMnSc1	Jordan
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
vážně	vážně	k6eAd1	vážně
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dalšího	další	k2eAgInSc2d1	další
pochodu	pochod	k1gInSc2	pochod
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nedaleko	nedaleko	k7c2	nedaleko
bojiště	bojiště	k1gNnSc2	bojiště
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Jordan	Jordan	k1gMnSc1	Jordan
představuje	představovat	k5eAaImIp3nS	představovat
typického	typický	k2eAgMnSc4d1	typický
hrdinu	hrdina	k1gMnSc4	hrdina
Hemingwayových	Hemingwayových	k2eAgInSc2d1	Hemingwayových
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mužný	mužný	k2eAgMnSc1d1	mužný
<g/>
,	,	kIx,	,
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
žít	žít	k5eAaImF	žít
svobodně	svobodně	k6eAd1	svobodně
i	i	k9	i
v	v	k7c6	v
kritických	kritický	k2eAgFnPc6d1	kritická
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
nemá	mít	k5eNaImIp3nS	mít
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
co	co	k3yRnSc4	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Smysl	smysl	k1gInSc4	smysl
románu	román	k1gInSc2	román
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
autor	autor	k1gMnSc1	autor
úvodním	úvodní	k2eAgNnSc7d1	úvodní
mottem	motto	k1gNnSc7	motto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Smrtí	smrtit	k5eAaImIp3nS	smrtit
každého	každý	k3xTgMnSc4	každý
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
mne	já	k3xPp1nSc4	já
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsem	být	k5eAaImIp1nS	být
součástí	součást	k1gFnSc7	součást
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neodvažuj	odvažovat	k5eNaImRp2nS	odvažovat
ptát	ptát	k5eAaImF	ptát
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
zvoní	zvonit	k5eAaImIp3nS	zvonit
ta	ten	k3xDgFnSc1	ten
hrana	hrana	k1gFnSc1	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Zvoní	zvonit	k5eAaImIp3nS	zvonit
tobě	ty	k3xPp2nSc6	ty
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Autorem	autor	k1gMnSc7	autor
těchto	tento	k3xDgNnPc2	tento
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
však	však	k9	však
básník	básník	k1gMnSc1	básník
John	John	k1gMnSc1	John
Donne	Donn	k1gInSc5	Donn
(	(	kIx(	(
<g/>
1572	[number]	k4	1572
-	-	kIx~	-
1631	[number]	k4	1631
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgInPc1d1	Zelené
pahorky	pahorek	k1gInPc1	pahorek
africké	africký	k2eAgInPc1d1	africký
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
lovu	lov	k1gInSc6	lov
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
Pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
svátek	svátek	k1gInSc4	svátek
-	-	kIx~	-
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
vylíčen	vylíčen	k2eAgInSc1d1	vylíčen
bohémský	bohémský	k2eAgInSc1d1	bohémský
život	život	k1gInSc1	život
umělců	umělec	k1gMnPc2	umělec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
do	do	k7c2	do
stínu	stín	k1gInSc2	stín
stromů	strom	k1gInPc2	strom
Ostrovy	ostrov	k1gInPc1	ostrov
uprostřed	uprostřed	k7c2	uprostřed
proudu	proud	k1gInSc2	proud
Smrt	smrt	k1gFnSc4	smrt
odpoledne	odpoledne	k1gNnSc1	odpoledne
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Mít	mít	k5eAaImF	mít
a	a	k8xC	a
nemít	mít	k5eNaImF	mít
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
-	-	kIx~	-
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Starý	starý	k2eAgMnSc1d1	starý
kubánský	kubánský	k2eAgMnSc1d1	kubánský
rybář	rybář	k1gMnSc1	rybář
Santiago	Santiago	k1gNnSc1	Santiago
se	se	k3xPyFc4	se
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
lovech	lov	k1gInPc6	lov
(	(	kIx(	(
<g/>
lovil	lovit	k5eAaImAgMnS	lovit
s	s	k7c7	s
malým	malý	k1gMnSc7	malý
chlapcem	chlapec	k1gMnSc7	chlapec
<g/>
)	)	kIx)	)
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Uloví	ulovit	k5eAaPmIp3nS	ulovit
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
rybu	ryba	k1gFnSc4	ryba
-	-	kIx~	-
marlina	marlina	k1gFnSc1	marlina
tj.	tj.	kA	tj.
mečouna	mečoun	k1gMnSc2	mečoun
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
zápasí	zápasit	k5eAaImIp3nP	zápasit
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
ji	on	k3xPp3gFnSc4	on
podaří	podařit	k5eAaPmIp3nP	podařit
harpunovat	harpunovat	k5eAaBmF	harpunovat
a	a	k8xC	a
přivázat	přivázat	k5eAaPmF	přivázat
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
životním	životní	k2eAgInSc7d1	životní
úlovkem	úlovek	k1gInSc7	úlovek
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysílen	vysílen	k2eAgMnSc1d1	vysílen
a	a	k8xC	a
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
s	s	k7c7	s
rybou	ryba	k1gFnSc7	ryba
udělá	udělat	k5eAaPmIp3nS	udělat
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
rybu	ryba	k1gFnSc4	ryba
napadnou	napadnout	k5eAaPmIp3nP	napadnout
žraloci	žralok	k1gMnPc1	žralok
a	a	k8xC	a
stařec	stařec	k1gMnSc1	stařec
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
rybu	ryba	k1gFnSc4	ryba
ubránit	ubránit	k5eAaPmF	ubránit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
kostrou	kostra	k1gFnSc7	kostra
své	svůj	k3xOyFgFnSc2	svůj
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Smysl	smysl	k1gInSc1	smysl
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
beznaději	beznaděj	k1gFnSc6	beznaděj
nebo	nebo	k8xC	nebo
něčem	něco	k3yInSc6	něco
takovém	takový	k3xDgInSc6	takový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
naopak	naopak	k6eAd1	naopak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Santiago	Santiago	k1gNnSc4	Santiago
udělal	udělat	k5eAaPmAgMnS	udělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
chtěl	chtít	k5eAaImAgMnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
svůj	svůj	k3xOyFgInSc4	svůj
boj	boj	k1gInSc4	boj
s	s	k7c7	s
rybou	ryba	k1gFnSc7	ryba
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc4d1	poslední
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
autor	autor	k1gMnSc1	autor
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
"	"	kIx"	"
<g/>
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc4	moře
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
povídky	povídka	k1gFnPc4	povídka
a	a	k8xC	a
deset	deset	k4xCc4	deset
básní	báseň	k1gFnPc2	báseň
Za	za	k7c2	za
našich	náš	k3xOp1gInPc2	náš
časů	čas	k1gInPc2	čas
Prvních	první	k4xOgMnPc2	první
49	[number]	k4	49
povídek	povídka	k1gFnPc2	povídka
Muži	muž	k1gMnPc1	muž
bez	bez	k7c2	bez
žen	žena	k1gFnPc2	žena
Vojákův	vojákův	k2eAgInSc4d1	vojákův
návrat	návrat	k1gInSc4	návrat
<g/>
"	"	kIx"	"
Pátá	pátý	k4xOgFnSc1	pátý
kolona	kolona	k1gFnSc1	kolona
-	-	kIx~	-
jediné	jediný	k2eAgNnSc1d1	jediné
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Hemingway	Hemingwaa	k1gFnPc4	Hemingwaa
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Tematicky	tematicky	k6eAd1	tematicky
vychází	vycházet	k5eAaImIp3nS	vycházet
rovněž	rovněž	k9	rovněž
ze	z	k7c2	z
zkušeností	zkušenost	k1gFnPc2	zkušenost
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
armádo	armáda	k1gFnSc5	armáda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vajtauer	Vajtauer	k1gMnSc1	Vajtauer
<g/>
,	,	kIx,	,
Pět	pět	k4xCc1	pět
stováků	stovák	k1gInPc2	stovák
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Staša	Staša	k1gFnSc1	Staša
Jílovská	jílovský	k2eAgFnSc1d1	Jílovská
<g/>
,	,	kIx,	,
Komu	kdo	k3yRnSc3	kdo
zvoní	zvonit	k5eAaImIp3nS	zvonit
hrana	hrana	k1gFnSc1	hrana
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
A.	A.	kA	A.
Sonková	Sonková	k1gFnSc1	Sonková
a	a	k8xC	a
Z.	Z.	kA	Z.
Zinková	zinkový	k2eAgFnSc1d1	zinková
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Mít	mít	k5eAaImF	mít
i	i	k8xC	i
nemít	mít	k5eNaImF	mít
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Lukasík	Lukasík	k1gInSc1	Lukasík
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Soňa	Soňa	k1gFnSc1	Soňa
Bartáková	Bartáková	k1gFnSc1	Bartáková
<g/>
,	,	kIx,	,
Komu	kdo	k3yInSc3	kdo
zvoní	zvonit	k5eAaImIp3nP	zvonit
hrana	hrana	k1gNnPc4	hrana
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Alois	Alois	k1gMnSc1	Alois
Humplík	Humplík	k1gMnSc1	Humplík
<g/>
,	,	kIx,	,
Přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
a	a	k8xC	a
do	do	k7c2	do
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
Melantrich	Melantricha	k1gFnPc2	Melantricha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zorka	Zorka	k1gFnSc1	Zorka
Dostálová-Dandová	Dostálová-Dandový	k2eAgFnSc1d1	Dostálová-Dandový
<g/>
,	,	kIx,	,
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
armádo	armáda	k1gFnSc5	armáda
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
<g/>
,	,	kIx,	,
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1961	[number]	k4	1961
a	a	k8xC	a
1963	[number]	k4	1963
a	a	k8xC	a
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Komu	kdo	k3yRnSc3	kdo
zvoní	zvonit	k5eAaImIp3nS	zvonit
hrana	hrana	k1gFnSc1	hrana
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Valja	Valja	k1gMnSc1	Valja
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
svazek	svazek	k1gInSc1	svazek
autorových	autorův	k2eAgMnPc2d1	autorův
spisů	spis	k1gInPc2	spis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
a	a	k8xC	a
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgInPc1d1	Zelené
pahorky	pahorek	k1gInPc1	pahorek
africké	africký	k2eAgInPc1d1	africký
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellarův	k2eAgMnPc1d1	Pellarův
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
a	a	k8xC	a
1972	[number]	k4	1972
a	a	k8xC	a
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Pátá	pátý	k4xOgFnSc1	pátý
kolona	kolona	k1gFnSc1	kolona
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
znovu	znovu	k6eAd1	znovu
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
armádo	armáda	k1gFnSc5	armáda
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
<g/>
,	,	kIx,	,
Fiesta	fiesta	k1gFnSc1	fiesta
(	(	kIx(	(
<g/>
I	i	k9	i
slunce	slunce	k1gNnSc1	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
a	a	k8xC	a
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgInPc4d1	Zelené
pahorky	pahorek	k1gInPc4	pahorek
africké	africký	k2eAgInPc4d1	africký
<g/>
,	,	kIx,	,
Mít	mít	k5eAaImF	mít
a	a	k8xC	a
nemít	mít	k5eNaImF	mít
<g/>
,	,	kIx,	,
Pátá	pátý	k4xOgFnSc1	pátý
kolona	kolona	k1gFnSc1	kolona
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellarův	k2eAgMnPc1d1	Pellarův
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
Nenadál	nadát	k5eNaBmAgMnS	nadát
a	a	k8xC	a
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
svazek	svazek	k1gInSc4	svazek
autorových	autorův	k2eAgInPc2d1	autorův
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
Ostrovy	ostrov	k1gInPc1	ostrov
uprostřed	uprostřed	k7c2	uprostřed
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellarův	k2eAgMnPc1d1	Pellarův
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Radoslav	Radoslav	k1gMnSc1	Radoslav
Nenadál	nadát	k5eNaBmAgMnS	nadát
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
svazek	svazek	k1gInSc4	svazek
autorových	autorův	k2eAgInPc2d1	autorův
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
a	a	k8xC	a
1978	[number]	k4	1978
a	a	k8xC	a
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
armádo	armáda	k1gFnSc5	armáda
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Stuchl	Stuchl	k1gMnSc1	Stuchl
<g/>
,	,	kIx,	,
Přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
do	do	k7c2	do
stínu	stín	k1gInSc2	stín
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
Pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellarův	k2eAgMnPc1d1	Pellarův
(	(	kIx(	(
<g/>
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
Jan	Jan	k1gMnSc1	Jan
Zábrana	zábrana	k1gFnSc1	zábrana
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgInSc4	pátý
svazek	svazek	k1gInSc4	svazek
autorových	autorův	k2eAgInPc2d1	autorův
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
Mít	mít	k5eAaImF	mít
a	a	k8xC	a
nemít	mít	k5eNaImF	mít
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Radoslav	Radoslav	k1gMnSc1	Radoslav
Nenadál	nadát	k5eNaBmAgInS	nadát
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellarův	k2eAgMnPc1d1	Pellarův
<g/>
,	,	kIx,	,
šestý	šestý	k4xOgInSc4	šestý
svazek	svazek	k1gInSc4	svazek
autorových	autorův	k2eAgInPc2d1	autorův
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Fiesta	fiesta	k1gFnSc1	fiesta
(	(	kIx(	(
<g/>
I	i	k9	i
slunce	slunce	k1gNnSc1	slunce
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Vrba	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jitka	Jitka	k1gFnSc1	Jitka
Beránková	Beránková	k1gFnSc1	Beránková
<g/>
,	,	kIx,	,
Pravda	pravda	k1gFnSc1	pravda
za	za	k7c2	za
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellarův	k2eAgMnPc1d1	Pellarův
<g/>
,	,	kIx,	,
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
armádo	armáda	k1gFnSc5	armáda
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
a	a	k8xC	a
Lubomír	Lubomír	k1gMnSc1	Lubomír
Dorůžka	Dorůžka	k1gFnSc1	Dorůžka
<g/>
,	,	kIx,	,
Nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
341	[number]	k4	341
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Rákos	rákos	k1gInSc4	rákos
<g/>
,	,	kIx,	,
Neporažený	poražený	k2eNgMnSc1d1	neporažený
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
341	[number]	k4	341
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Rákos	rákos	k1gInSc4	rákos
<g/>
,	,	kIx,	,
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
341	[number]	k4	341
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Václav	Václav	k1gMnSc1	Václav
Rákos	rákos	k1gInSc1	rákos
a	a	k8xC	a
Daniel	Daniel	k1gMnSc1	Daniel
Deyl	Deyl	k1gMnSc1	Deyl
<g/>
.	.	kIx.	.
</s>
