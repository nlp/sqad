<s>
Malé	Malé	k2eAgInPc1d1
stydké	stydký	k2eAgInPc1d1
pysky	pysk	k1gInPc1
</s>
<s>
Malé	Malé	k2eAgInPc1d1
stydké	stydký	k2eAgInPc1d1
pysky	pysk	k1gInPc1
</s>
<s>
Malé	Malé	k2eAgInPc1d1
stydké	stydký	k2eAgInPc1d1
pysky	pysk	k1gInPc1
(	(	kIx(
<g/>
označeno	označit	k5eAaPmNgNnS
jako	jako	k9
„	„	k?
<g/>
labia	labia	k1gFnSc1
minora	minora	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Malé	Malé	k2eAgInPc1d1
stydké	stydký	k2eAgInPc1d1
pysky	pysk	k1gInPc1
(	(	kIx(
<g/>
označeno	označit	k5eAaPmNgNnS
jako	jako	k9
„	„	k?
<g/>
labia	labia	k1gFnSc1
minora	minora	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Latinsky	latinsky	k6eAd1
</s>
<s>
labia	labius	k1gMnSc4
minora	minor	k1gMnSc4
pudendi	pudend	k1gMnPc1
</s>
<s>
Prekurzor	Prekurzor	k1gMnSc1
</s>
<s>
urogenitální	urogenitální	k2eAgFnPc1d1
řasy	řasa	k1gFnPc1
</s>
<s>
Gray	Graa	k1gFnPc1
</s>
<s>
1265	#num#	k4
</s>
<s>
Malé	Malé	k2eAgInPc1d1
stydké	stydký	k2eAgInPc1d1
pysky	pysk	k1gInPc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
:	:	kIx,
labia	labia	k1gFnSc1
minora	minora	k1gFnSc1
pudendi	pudendit	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
nymfé	nymfé	k6eAd1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
:	:	kIx,
nymphae	nymphae	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
longitudinální	longitudinální	k2eAgFnPc4d1
(	(	kIx(
<g/>
podélné	podélný	k2eAgFnPc4d1
<g/>
)	)	kIx)
slizničné	slizničný	k2eAgFnPc4d1
řasy	řasa	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
velikost	velikost	k1gFnSc1
se	se	k3xPyFc4
ženu	hnát	k5eAaImIp1nS
od	od	k7c2
ženy	žena	k1gFnSc2
může	moct	k5eAaImIp3nS
velmi	velmi	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
lišit	lišit	k5eAaImF
(	(	kIx(
<g/>
přibližná	přibližný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
centimetry	centimetr	k1gInPc1
a	a	k8xC
tloušťka	tloušťka	k1gFnSc1
5	#num#	k4
milimetrů	milimetr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
situovány	situovat	k5eAaBmNgInP
mezi	mezi	k7c7
velkými	velký	k2eAgInPc7d1
stydkými	stydký	k2eAgInPc7d1
pysky	pysk	k1gInPc7
a	a	k8xC
rozšiřují	rozšiřovat	k5eAaImIp3nP
se	se	k3xPyFc4
od	od	k7c2
klitorisu	klitoris	k1gInSc2
šikmo	šikmo	k6eAd1
dolů	dolů	k6eAd1
<g/>
,	,	kIx,
do	do	k7c2
strany	strana	k1gFnSc2
a	a	k8xC
dozadu	dozadu	k6eAd1
ke	k	k7c3
každé	každý	k3xTgFnSc3
straně	strana	k1gFnSc3
poševní	poševní	k2eAgFnSc2d1
předsíně	předsíň	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
níž	jenž	k3xRgFnSc7
a	a	k8xC
velkými	velká	k1gFnPc7
stydkými	stydký	k2eAgInPc7d1
pysky	pysk	k1gInPc7
tyto	tento	k3xDgFnPc1
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
panny	panna	k1gFnSc2
jsou	být	k5eAaImIp3nP
zadní	zadní	k2eAgInPc1d1
konce	konec	k1gInPc1
malých	malý	k2eAgInPc2d1
stydkých	stydký	k2eAgInPc2d1
pysků	pysk	k1gInPc2
obvykle	obvykle	k6eAd1
spojeny	spojit	k5eAaPmNgInP
uprostřed	uprostřed	k6eAd1
kožní	kožní	k2eAgFnSc7d1
hranou	hrana	k1gFnSc7
nazývanou	nazývaný	k2eAgFnSc7d1
frenulum	frenulum	k1gNnSc4
labiorum	labiorum	k1gNnSc1
pudendi	pudend	k1gMnPc1
nebo	nebo	k8xC
fourchette	fourchette	k5eAaPmIp2nP
(	(	kIx(
<g/>
"	"	kIx"
<g/>
vidlička	vidlička	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vpředu	vpředu	k6eAd1
se	se	k3xPyFc4
každý	každý	k3xTgInSc1
malý	malý	k2eAgInSc1d1
stydký	stydký	k2eAgInSc1d1
pysk	pysk	k1gInSc1
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
:	:	kIx,
horní	horní	k2eAgInSc1d1
díl	díl	k1gInSc1
prochází	procházet	k5eAaImIp3nS
nad	nad	k7c7
klitorisem	klitoris	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
protějším	protější	k2eAgInSc7d1
pyskem	pysk	k1gInSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
oba	dva	k4xCgInPc1
stejně	stejně	k6eAd1
velké	velký	k2eAgInPc1d1
<g/>
;	;	kIx,
vytvářejí	vytvářet	k5eAaImIp3nP
zde	zde	k6eAd1
poštěváčkovou	poštěváčkový	k2eAgFnSc4d1
kapucu	kapuca	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
klene	klenout	k5eAaImIp3nS
přes	přes	k7c4
vnější	vnější	k2eAgFnSc4d1
část	část	k1gFnSc4
poštěváčku	poštěváček	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolní	dolní	k2eAgFnSc1d1
část	část	k1gFnSc1
pysků	pysk	k1gInPc2
prochází	procházet	k5eAaImIp3nS
pod	pod	k7c7
vnější	vnější	k2eAgFnSc7d1
částí	část	k1gFnSc7
klitorisu	klitoris	k1gInSc2
<g/>
,	,	kIx,
spojuje	spojovat	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
povrchem	povrch	k1gInSc7
pod	pod	k7c7
ním	on	k3xPp3gNnSc7
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
protějším	protější	k2eAgInSc7d1
pyskem	pysk	k1gInSc7
(	(	kIx(
<g/>
opět	opět	k6eAd1
nikoli	nikoli	k9
nutně	nutně	k6eAd1
stejné	stejný	k2eAgFnPc4d1
velikosti	velikost	k1gFnPc4
<g/>
)	)	kIx)
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
uzdičku	uzdička	k1gFnSc4
klitorisu	klitoris	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
opačné	opačný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
(	(	kIx(
<g/>
povrchu	povrch	k1gInSc6
<g/>
)	)	kIx)
malých	malý	k2eAgInPc2d1
stydkých	stydký	k2eAgInPc2d1
pysků	pysk	k1gInPc2
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
mazových	mazový	k2eAgFnPc2d1
žláz	žláza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Chirurgický	chirurgický	k2eAgInSc1d1
úkon	úkon	k1gInSc1
spočívající	spočívající	k2eAgInSc1d1
v	v	k7c6
úpravě	úprava	k1gFnSc6
velikosti	velikost	k1gFnSc6
a	a	k8xC
tvaru	tvar	k1gInSc6
malých	malý	k2eAgInPc2d1
stydkých	stydký	k2eAgInPc2d1
pysků	pysk	k1gInPc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
labioplastika	labioplastika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
úkon	úkon	k1gInSc1
bývá	bývat	k5eAaImIp3nS
prováděn	provádět	k5eAaImNgInS
z	z	k7c2
estetických	estetický	k2eAgInPc2d1
a	a	k8xC
praktických	praktický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
</s>
<s>
Sagitální	sagitální	k2eAgInSc1d1
řez	řez	k1gInSc1
dolní	dolní	k2eAgFnSc2d1
částí	část	k1gFnSc7
ženského	ženský	k2eAgInSc2d1
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
pravý	pravý	k2eAgInSc1d1
segment	segment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Adel	Adel	k1gMnSc1
Median	Median	k1gMnSc1
sagitální	sagitální	k2eAgInSc4d1
řez	řez	k1gInSc4
ženskou	ženský	k2eAgFnSc7d1
pánví	pánev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Orgány	orgány	k1gFnPc1
ženského	ženský	k2eAgNnSc2d1
rozmnožovacího	rozmnožovací	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Labia	Labius	k1gMnSc2
minora	minor	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nymphae	Nymphae	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dictionary	Dictionara	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROB	roba	k1gFnPc2
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
;	;	kIx,
MARTAN	MARTAN	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
;	;	kIx,
CITTERBART	CITTERBART	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gynekologie	gynekologie	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Galén	Galén	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7262	#num#	k4
<g/>
-	-	kIx~
<g/>
501	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Malé	Malá	k1gFnSc2
stydké	stydký	k2eAgInPc4d1
pysky	pysk	k1gInPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ženská	ženský	k2eAgFnSc1d1
rozmnožovací	rozmnožovací	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
adnexa	adnexa	k6eAd1
</s>
<s>
folikuly	folikul	k1gInPc1
<g/>
/	/	kIx~
<g/>
folikulogeneze	folikulogeneze	k1gFnSc1
</s>
<s>
tělísko	tělísko	k1gNnSc1
(	(	kIx(
<g/>
hemoragické	hemoragický	k2eAgFnPc1d1
<g/>
,	,	kIx,
žluté	žlutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc1d1
<g/>
)	)	kIx)
·	·	k?
theca	theca	k6eAd1
follicculi	folliccout	k5eAaPmAgMnP
(	(	kIx(
<g/>
externa	externa	k1gFnSc1
<g/>
,	,	kIx,
interna	interna	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
antrum	antrum	k1gInSc1
follicularum	follicularum	k1gInSc1
(	(	kIx(
<g/>
folikulární	folikulární	k2eAgFnSc1d1
tekutina	tekutina	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
corona	corona	k1gFnSc1
radiata	radiat	k2eAgFnSc1d1
·	·	k?
zona	zona	k1gFnSc1
pellucida	pellucida	k1gFnSc1
·	·	k?
membrana	membrana	k1gFnSc1
granulosa	granulosa	k1gFnSc1
(	(	kIx(
<g/>
folikulární	folikulární	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
<g/>
)	)	kIx)
vaječníky	vaječník	k1gInPc1
<g/>
/	/	kIx~
<g/>
oogeneze	oogeneze	k1gFnSc1
</s>
<s>
zárodečný	zárodečný	k2eAgInSc1d1
epitel	epitel	k1gInSc1
·	·	k?
tunica	tunica	k6eAd1
albuginea	albuginea	k6eAd1
·	·	k?
kortex	kortex	k1gInSc1
(	(	kIx(
<g/>
cumulus	cumulus	k1gInSc1
oophorus	oophorus	k1gInSc1
<g/>
,	,	kIx,
stroma	stroma	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
dřeň	dřeň	k1gFnSc1
vejcovody	vejcovod	k1gInPc4
</s>
<s>
isthmus	isthmus	k1gInSc1
·	·	k?
ampula	ampula	k1gFnSc1
·	·	k?
infundibulum	infundibulum	k1gInSc1
·	·	k?
fimbria	fimbrium	k1gNnSc2
vazy	vaz	k1gInPc4
</s>
<s>
široký	široký	k2eAgInSc1d1
děložní	děložní	k2eAgInSc1d1
vaz	vaz	k1gInSc1
(	(	kIx(
<g/>
části	část	k1gFnPc1
<g/>
:	:	kIx,
mesovarium	mesovarium	k1gNnSc1
<g/>
,	,	kIx,
mesosalpinx	mesosalpinx	k1gInSc1
<g/>
,	,	kIx,
mesometrium	mesometrium	k1gNnSc1
obsah	obsah	k1gInSc1
<g/>
:	:	kIx,
oblý	oblý	k2eAgInSc1d1
děložní	děložní	k2eAgInSc1d1
vaz	vaz	k1gInSc1
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgInSc1d1
vaječníkový	vaječníkový	k2eAgInSc1d1
vaz	vaz	k1gInSc1
<g/>
)	)	kIx)
·	·	k?
ligamentum	ligamentum	k1gNnSc1
suspensorium	suspensorium	k1gNnSc1
ovarii	ovarium	k1gNnPc7
·	·	k?
kardinální	kardinální	k2eAgInSc4d1
</s>
<s>
děloha	děloha	k1gFnSc1
</s>
<s>
corpus	corpus	k1gNnSc1
/	/	kIx~
tělo	tělo	k1gNnSc1
(	(	kIx(
<g/>
děložní	děložní	k2eAgFnSc1d1
dutina	dutina	k1gFnSc1
<g/>
,	,	kIx,
děložní	děložní	k2eAgNnSc1d1
dno	dno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
cervix	cervix	k1gInSc1
/	/	kIx~
hrdlo	hrdlo	k1gNnSc1
(	(	kIx(
<g/>
zevní	zevní	k2eAgFnSc1d1
branka	branka	k1gFnSc1
děložní	děložní	k2eAgFnSc1d1
<g/>
,	,	kIx,
kanál	kanál	k1gInSc1
děložního	děložní	k2eAgNnSc2d1
hrdla	hrdlo	k1gNnSc2
<g/>
,	,	kIx,
vnitřní	vnitřní	k2eAgFnSc1d1
branka	branka	k1gFnSc1
děložní	děložní	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
stěny	stěna	k1gFnPc1
(	(	kIx(
<g/>
endometrium	endometrium	k1gNnSc1
<g/>
,	,	kIx,
myometrium	myometrium	k1gNnSc1
<g/>
,	,	kIx,
perimetrium	perimetrium	k1gNnSc1
<g/>
)	)	kIx)
·	·	k?
děložní	děložní	k2eAgInPc4d1
rohy	roh	k1gInPc4
vulva	vulva	k1gFnSc1
<g/>
/	/	kIx~
<g/>
stydký	stydký	k2eAgInSc1d1
pahorek	pahorek	k1gInSc1
</s>
<s>
pochva	pochva	k1gFnSc1
</s>
<s>
vestibulární	vestibulární	k2eAgFnPc4d1
žlázy	žláza	k1gFnPc4
(	(	kIx(
<g/>
Bartholiniho	Bartholini	k1gMnSc2
žlázy	žláza	k1gFnSc2
<g/>
,	,	kIx,
Skeneho	Skene	k1gMnSc2
žlázy	žláza	k1gFnSc2
<g/>
)	)	kIx)
·	·	k?
fossa	fossa	k1gFnSc1
navicularis	navicularis	k1gFnSc1
·	·	k?
poševní	poševní	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
·	·	k?
panenská	panenský	k2eAgFnSc1d1
blána	blána	k1gFnSc1
·	·	k?
poševní	poševní	k2eAgInSc4d1
vchod	vchod	k1gInSc4
stydké	stydký	k2eAgInPc4d1
pysky	pysk	k1gInPc4
</s>
<s>
komisury	komisura	k1gFnPc1
(	(	kIx(
<g/>
přední	přední	k2eAgFnPc1d1
·	·	k?
zadní	zadní	k2eAgFnPc1d1
<g/>
)	)	kIx)
·	·	k?
frenulum	frenulum	k1gInSc1
labiorum	labiorum	k1gNnSc1
pudendi	pudend	k1gMnPc1
·	·	k?
velké	velký	k2eAgInPc4d1
stydké	stydký	k2eAgInPc4d1
pysky	pysk	k1gInPc4
·	·	k?
stydká	stydký	k2eAgFnSc1d1
štěrbina	štěrbina	k1gFnSc1
·	·	k?
malé	malý	k2eAgInPc4d1
stydké	stydký	k2eAgInPc4d1
pysky	pysk	k1gInPc4
·	·	k?
poševní	poševní	k2eAgFnSc1d1
předsíň	předsíň	k1gFnSc1
klitoris	klitoris	k1gFnSc1
</s>
<s>
bulbus	bulbus	k1gInSc4
vestibuli	vestibule	k1gFnSc4
·	·	k?
crus	crus	k1gInSc1
clitoris	clitoris	k1gFnSc1
·	·	k?
kavernózní	kavernózní	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
·	·	k?
žalud	žalud	k1gInSc1
klitorisu	klitoris	k1gInSc2
(	(	kIx(
<g/>
uzdička	uzdička	k1gFnSc1
<g/>
,	,	kIx,
předkožka	předkožka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
prsa	prsa	k1gNnPc1
</s>
<s>
mléčná	mléčný	k2eAgFnSc1d1
žláza	žláza	k1gFnSc1
·	·	k?
bradavka	bradavka	k1gFnSc1
·	·	k?
prsní	prsní	k2eAgInSc1d1
dvorec	dvorec	k1gInSc1
·	·	k?
mlékovod	mlékovod	k1gInSc1
·	·	k?
Cooperův	Cooperův	k2eAgInSc1d1
vaz	vaz	k1gInSc1
vývody	vývoda	k1gMnSc2
</s>
<s>
Wolffův	Wolffův	k2eAgInSc1d1
vývod	vývod	k1gInSc1
(	(	kIx(
<g/>
Gartnerův	Gartnerův	k2eAgInSc1d1
kanálek	kanálek	k1gInSc1
<g/>
,	,	kIx,
epoophoron	epoophoron	k1gInSc1
<g/>
,	,	kIx,
paroophoron	paroophoron	k1gInSc1
<g/>
)	)	kIx)
·	·	k?
Nuckův	Nuckův	k2eAgInSc4d1
kanál	kanál	k1gInSc4
ostatní	ostatní	k2eAgFnSc2d1
</s>
<s>
bod	bod	k1gInSc1
G	G	kA
·	·	k?
hráz	hráz	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sexualita	sexualita	k1gFnSc1
</s>
