<s>
Radek	Radek	k1gMnSc1	Radek
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
a	a	k8xC	a
bronzový	bronzový	k2eAgMnSc1d1	bronzový
medailista	medailista	k1gMnSc1	medailista
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
smíšené	smíšený	k2eAgFnSc2d1	smíšená
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
z	z	k7c2	z
brazilského	brazilský	k2eAgMnSc2d1	brazilský
Ria	Ria	k1gMnSc2	Ria
de	de	k?	de
Janeira	Janeir	k1gInSc2	Janeir
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
kariéře	kariéra	k1gFnSc6	kariéra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
pět	pět	k4xCc4	pět
turnajů	turnaj	k1gInPc2	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
osmnáct	osmnáct	k4xCc4	osmnáct
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
získal	získat	k5eAaPmAgMnS	získat
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
a	a	k8xC	a
US	US	kA	US
Open	Open	k1gInSc4	Open
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
s	s	k7c7	s
Leanderem	Leander	k1gMnSc7	Leander
Paesem	Paes	k1gMnSc7	Paes
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
si	se	k3xPyFc3	se
společně	společně	k6eAd1	společně
zahráli	zahrát	k5eAaPmAgMnP	zahrát
i	i	k9	i
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Novákem	Novák	k1gMnSc7	Novák
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
finále	finále	k1gNnSc4	finále
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříčko	k1gNnSc6	žebříčko
ATP	atp	kA	atp
byl	být	k5eAaImAgMnS	být
nejvýše	nejvýše	k6eAd1	nejvýše
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
klasifikován	klasifikovat	k5eAaImNgMnS	klasifikovat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
pak	pak	k6eAd1	pak
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
Paesem	Paes	k1gMnSc7	Paes
probojovali	probojovat	k5eAaPmAgMnP	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
londýnského	londýnský	k2eAgInSc2d1	londýnský
Turnaje	turnaj	k1gInSc2	turnaj
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
jej	on	k3xPp3gMnSc4	on
trénuje	trénovat	k5eAaImIp3nS	trénovat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
druhý	druhý	k4xOgMnSc1	druhý
hráč	hráč	k1gMnSc1	hráč
světa	svět	k1gInSc2	svět
Petr	Petr	k1gMnSc1	Petr
Korda	Korda	k1gMnSc1	Korda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
daviscupovém	daviscupový	k2eAgInSc6d1	daviscupový
týmu	tým	k1gInSc6	tým
debutoval	debutovat	k5eAaBmAgInS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
třikrát	třikrát	k6eAd1	třikrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Česko	Česko	k1gNnSc1	Česko
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
na	na	k7c6	na
barcelonské	barcelonský	k2eAgFnSc6d1	barcelonská
antuce	antuka	k1gFnSc6	antuka
Španělsku	Španělsko	k1gNnSc6	Španělsko
hladce	hladko	k6eAd1	hladko
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
plnil	plnit	k5eAaImAgMnS	plnit
roli	role	k1gFnSc4	role
české	český	k2eAgFnSc2d1	Česká
dvojky	dvojka	k1gFnSc2	dvojka
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgMnSc6	který
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
bilanci	bilance	k1gFnSc4	bilance
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
i	i	k9	i
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
dvouhře	dvouhra	k1gFnSc6	dvouhra
finále	finále	k1gNnSc2	finále
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
2013	[number]	k4	2013
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
porazil	porazit	k5eAaPmAgMnS	porazit
Dušana	Dušan	k1gMnSc4	Dušan
Lajoviće	Lajović	k1gMnSc4	Lajović
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
dvaceti	dvacet	k4xCc3	dvacet
čtyřem	čtyři	k4xCgInPc3	čtyři
mezistátním	mezistátní	k2eAgInPc3d1	mezistátní
zápasům	zápas	k1gInPc3	zápas
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
československém	československý	k2eAgInSc6d1	československý
<g/>
,	,	kIx,	,
týmu	tým	k1gInSc3	tým
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Berdychem	Berdych	k1gMnSc7	Berdych
historicky	historicky	k6eAd1	historicky
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
deblový	deblový	k2eAgInSc4d1	deblový
pár	pár	k1gInSc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
do	do	k7c2	do
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
tenistkou	tenistka	k1gFnSc7	tenistka
Nicole	Nicole	k1gFnSc2	Nicole
Vaidišovou	Vaidišová	k1gFnSc7	Vaidišová
<g/>
.	.	kIx.	.
</s>
<s>
Profesionálem	profesionál	k1gMnSc7	profesionál
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
nejprve	nejprve	k6eAd1	nejprve
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k8xC	jako
deblový	deblový	k2eAgMnSc1d1	deblový
specialista	specialista	k1gMnSc1	specialista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
získal	získat	k5eAaPmAgInS	získat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Dammem	Damm	k1gMnSc7	Damm
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgInS	porazit
americko-ekvádorský	americkokvádorský	k2eAgInSc1d1	americko-ekvádorský
pár	pár	k1gInSc1	pár
Mark	Mark	k1gMnSc1	Mark
Keil	Keil	k1gMnSc1	Keil
<g/>
,	,	kIx,	,
Nicolás	Nicolás	k1gInSc1	Nicolás
Lapentti	Lapentť	k1gFnSc2	Lapentť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
získal	získat	k5eAaPmAgInS	získat
celkově	celkově	k6eAd1	celkově
12	[number]	k4	12
titulů	titul	k1gInPc2	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP.	atp.	kA	atp.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
i	i	k9	i
na	na	k7c4	na
dvouhru	dvouhra	k1gFnSc4	dvouhra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
zůstával	zůstávat	k5eAaImAgInS	zůstávat
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
deblistou	deblista	k1gMnSc7	deblista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
první	první	k4xOgInSc4	první
turnaj	turnaj	k1gInSc4	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Belgičana	Belgičan	k1gMnSc4	Belgičan
Rochuse	Rochuse	k1gFnSc1	Rochuse
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazil	porazit	k5eAaPmAgMnS	porazit
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
světovou	světový	k2eAgFnSc4d1	světová
devítku	devítka	k1gFnSc4	devítka
Jamese	Jamese	k1gFnSc2	Jamese
Blakea	Blake	k1gInSc2	Blake
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
titul	titul	k1gInSc4	titul
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
zdolal	zdolat	k5eAaPmAgMnS	zdolat
Fernanda	Fernanda	k1gFnSc1	Fernanda
Verdasca	Verdasc	k2eAgFnSc1d1	Verdasc
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
finále	finále	k1gNnSc2	finále
postoupil	postoupit	k5eAaPmAgInS	postoupit
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Masters	Masters	k1gInSc1	Masters
Series	Series	k1gInSc1	Series
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2004	[number]	k4	2004
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
Maratu	Marata	k1gFnSc4	Marata
Safinovi	Safin	k1gMnSc3	Safin
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
Španěla	Španěl	k1gMnSc4	Španěl
Tommyho	Tommy	k1gMnSc4	Tommy
Robreda	Robred	k1gMnSc4	Robred
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výsledkem	výsledek	k1gInSc7	výsledek
na	na	k7c6	na
grandslamových	grandslamový	k2eAgInPc6d1	grandslamový
turnajích	turnaj	k1gInPc6	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc4	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
s	s	k7c7	s
Indem	Ind	k1gMnSc7	Ind
Leanderem	Leander	k1gMnSc7	Leander
Paesem	Paes	k1gMnSc7	Paes
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
skok	skok	k1gInSc4	skok
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
způsobila	způsobit	k5eAaPmAgFnS	způsobit
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
bitvě	bitva	k1gFnSc6	bitva
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
na	na	k7c6	na
kanadském	kanadský	k2eAgInSc6d1	kanadský
betonu	beton	k1gInSc6	beton
světové	světový	k2eAgFnSc2d1	světová
jedničce	jednička	k1gFnSc3	jednička
Rogeru	Roger	k1gInSc2	Roger
Federerovi	Federer	k1gMnSc3	Federer
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
porazil	porazit	k5eAaPmAgMnS	porazit
na	na	k7c6	na
antukovém	antukový	k2eAgInSc6d1	antukový
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
naposled	naposled	k6eAd1	naposled
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
porazil	porazit	k5eAaPmAgMnS	porazit
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
žebříčkového	žebříčkový	k2eAgNnSc2d1	žebříčkové
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
singlovém	singlový	k2eAgInSc6d1	singlový
žebříčku	žebříček	k1gInSc6	žebříček
ATP.	atp.	kA	atp.
Jako	jako	k8xS	jako
hráč	hráč	k1gMnSc1	hráč
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
však	však	k9	však
neodehrál	odehrát	k5eNaPmAgInS	odehrát
jediný	jediný	k2eAgInSc1d1	jediný
zápas	zápas	k1gInSc1	zápas
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
již	již	k6eAd1	již
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
turnajích	turnaj	k1gInPc6	turnaj
<g/>
,	,	kIx,	,
v	v	k7c6	v
australském	australský	k2eAgMnSc6d1	australský
Brisbane	Brisban	k1gMnSc5	Brisban
a	a	k8xC	a
americkém	americký	k2eAgMnSc6d1	americký
San	San	k1gMnSc6	San
José	Josá	k1gFnSc2	Josá
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
i	i	k9	i
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
dosud	dosud	k6eAd1	dosud
své	svůj	k3xOyFgNnSc4	svůj
nejcennější	cenný	k2eAgNnSc4d3	nejcennější
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
ATP	atp	kA	atp
World	World	k1gInSc1	World
Tour	Tour	k1gInSc1	Tour
500	[number]	k4	500
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc5	D.C.
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
aktivních	aktivní	k2eAgMnPc2d1	aktivní
tenistů	tenista	k1gMnPc2	tenista
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
herní	herní	k2eAgInSc1d1	herní
styl	styl	k1gInSc1	styl
servis	servis	k1gInSc1	servis
<g/>
-	-	kIx~	-
<g/>
volej	volej	k1gInSc1	volej
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
hrál	hrát	k5eAaImAgInS	hrát
zápas	zápas	k1gInSc1	zápas
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
proti	proti	k7c3	proti
Srbovi	Srb	k1gMnSc3	Srb
Janku	Janek	k1gMnSc3	Janek
Tipsarevičovi	Tipsarevič	k1gMnSc3	Tipsarevič
<g/>
.	.	kIx.	.
</s>
<s>
Srb	Srb	k1gMnSc1	Srb
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
po	po	k7c6	po
pětisetovém	pětisetový	k2eAgInSc6d1	pětisetový
průběhu	průběh	k1gInSc6	průběh
<g/>
,	,	kIx,	,
když	když	k8xS	když
výsledný	výsledný	k2eAgInSc1d1	výsledný
čas	čas	k1gInSc1	čas
činil	činit	k5eAaImAgInS	činit
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
Tipsarevič	Tipsarevič	k1gInSc1	Tipsarevič
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Štěpánka	Štěpánek	k1gMnSc4	Štěpánek
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
při	při	k7c6	při
podávání	podávání	k1gNnSc6	podávání
ruky	ruka	k1gFnSc2	ruka
u	u	k7c2	u
sítě	síť	k1gFnSc2	síť
ukázal	ukázat	k5eAaPmAgInS	ukázat
vztyčený	vztyčený	k2eAgInSc1d1	vztyčený
prostředník	prostředník	k1gInSc1	prostředník
a	a	k8xC	a
vulgárně	vulgárně	k6eAd1	vulgárně
ho	on	k3xPp3gMnSc4	on
urážel	urážet	k5eAaImAgMnS	urážet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ročníku	ročník	k1gInSc6	ročník
získal	získat	k5eAaPmAgMnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
Davis	Davis	k1gInSc4	Davis
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
porazil	porazit	k5eAaPmAgInS	porazit
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgMnSc6d1	rozhodující
pátém	pátý	k4xOgInSc6	pátý
zápase	zápas	k1gInSc6	zápas
Nicoláse	Nicoláse	k1gFnSc2	Nicoláse
Almagra	Almagr	k1gInSc2	Almagr
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
ročníku	ročník	k1gInSc6	ročník
2013	[number]	k4	2013
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
týmem	tým	k1gInSc7	tým
"	"	kIx"	"
<g/>
salátovou	salátový	k2eAgFnSc4d1	salátová
mísu	mísa	k1gFnSc4	mísa
<g/>
"	"	kIx"	"
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obhájit	obhájit	k5eAaPmF	obhájit
<g/>
,	,	kIx,	,
když	když	k8xS	když
opět	opět	k6eAd1	opět
získal	získat	k5eAaPmAgMnS	získat
vítězný	vítězný	k2eAgInSc4d1	vítězný
bod	bod	k1gInSc4	bod
v	v	k7c6	v
posledním	poslední	k2eAgMnSc6d1	poslední
pátém	pátý	k4xOgInSc6	pátý
zápase	zápas	k1gInSc6	zápas
finále	finále	k1gNnSc2	finále
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
(	(	kIx(	(
<g/>
tentokrát	tentokrát	k6eAd1	tentokrát
proti	proti	k7c3	proti
Dušanu	Dušan	k1gMnSc3	Dušan
Lajovićovi	Lajovića	k1gMnSc3	Lajovića
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
hráčem	hráč	k1gMnSc7	hráč
historie	historie	k1gFnSc2	historie
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgFnPc6d1	jdoucí
sezónách	sezóna	k1gFnPc6	sezóna
<g/>
;	;	kIx,	;
a	a	k8xC	a
třetím	třetí	k4xOgMnSc7	třetí
mezi	mezi	k7c7	mezi
tenisty	tenista	k1gMnPc7	tenista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
dokázali	dokázat	k5eAaPmAgMnP	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
pátý	pátý	k4xOgInSc4	pátý
zápas	zápas	k1gInSc4	zápas
finále	finále	k1gNnSc2	finále
dvakrát	dvakrát	k6eAd1	dvakrát
či	či	k8xC	či
vícekrát	vícekrát	k6eAd1	vícekrát
(	(	kIx(	(
<g/>
po	po	k7c6	po
Francouzovi	Francouz	k1gMnSc6	Francouz
Henri	Henr	k1gMnSc6	Henr
Cochetovi	Cochet	k1gMnSc6	Cochet
a	a	k8xC	a
Britovi	Brit	k1gMnSc6	Brit
Fredu	Fred	k1gMnSc6	Fred
Perrym	Perrymum	k1gNnPc2	Perrymum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2014	[number]	k4	2014
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
rozehrál	rozehrát	k5eAaPmAgMnS	rozehrát
jako	jako	k9	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2015	[number]	k4	2015
měl	mít	k5eAaImAgInS	mít
zahájit	zahájit	k5eAaPmF	zahájit
opět	opět	k6eAd1	opět
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
Šafářovou	Šafářová	k1gFnSc7	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
nohy	noha	k1gFnSc2	noha
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Adam	Adam	k1gMnSc1	Adam
Pavlásek	Pavlásek	k1gMnSc1	Pavlásek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zasnouben	zasnouben	k2eAgInSc1d1	zasnouben
s	s	k7c7	s
tenistkou	tenistka	k1gFnSc7	tenistka
a	a	k8xC	a
bývalou	bývalý	k2eAgFnSc7d1	bývalá
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
Martinou	Martina	k1gFnSc7	Martina
Hingisovou	Hingisový	k2eAgFnSc7d1	Hingisová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2007	[number]	k4	2007
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svatbu	svatba	k1gFnSc4	svatba
ruší	rušit	k5eAaImIp3nS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
stala	stát	k5eAaPmAgFnS	stát
česká	český	k2eAgFnSc1d1	Česká
tenistka	tenistka	k1gFnSc1	tenistka
Nicole	Nicole	k1gFnSc1	Nicole
Vaidišová	Vaidišová	k1gFnSc1	Vaidišová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
s	s	k7c7	s
Vaidišovou	Vaidišová	k1gFnSc7	Vaidišová
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
trvalo	trvat	k5eAaImAgNnS	trvat
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
spekulovat	spekulovat	k5eAaImF	spekulovat
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
druhou	druhý	k4xOgFnSc7	druhý
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitová	k1gFnSc7	Kvitová
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
však	však	k9	však
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
až	až	k9	až
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Několikaměsíční	několikaměsíční	k2eAgInSc4d1	několikaměsíční
vztah	vztah	k1gInSc4	vztah
ukončili	ukončit	k5eAaPmAgMnP	ukončit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
Jaromír	Jaromír	k1gMnSc1	Jaromír
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
.	.	kIx.	.
</s>
