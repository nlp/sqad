<s>
Partimage	Partimage	k6eAd1
</s>
<s>
Partimage	Partimage	k6eAd1
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Francois	Francois	k1gMnSc1
Dupoux	Dupoux	k1gInSc1
and	and	k?
Franck	Franck	k1gInSc1
Ladurelle	Ladurelle	k1gNnSc1
První	první	k4xOgFnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
Aktuální	aktuální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
0.6	0.6	k4
<g/>
.9	.9	k4
/	/	kIx~
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
Multiplatformní	multiplatformní	k2eAgInSc1d1
(	(	kIx(
<g/>
Live	Live	k1gInSc1
CD	CD	kA
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
Licence	licence	k1gFnSc2
</s>
<s>
GPL	GPL	kA
2	#num#	k4
(	(	kIx(
<g/>
GNU	gnu	k1gNnSc4
General	General	k1gFnSc2
Public	publicum	k1gNnPc2
Licence	licence	k1gFnSc1
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.partimage.org	www.partimage.org	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Partimage	Partimage	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
informatice	informatika	k1gFnSc6
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
zálohování	zálohování	k1gNnSc4
disků	disk	k1gInPc2
v	v	k7c6
prostředí	prostředí	k1gNnSc6
Linuxu	linux	k1gInSc2
<g/>
/	/	kIx~
<g/>
Unixu	Unix	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Partimage	Partimag	k1gInSc2
dokáže	dokázat	k5eAaPmIp3nS
ukládat	ukládat	k5eAaImF
diskové	diskový	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
nejrůznějších	různý	k2eAgInPc2d3
formátů	formát	k1gInPc2
do	do	k7c2
diskového	diskový	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
a	a	k8xC
následně	následně	k6eAd1
je	být	k5eAaImIp3nS
z	z	k7c2
tohoto	tento	k3xDgInSc2
obrazu	obraz	k1gInSc2
opět	opět	k6eAd1
obnovit	obnovit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Partimage	Partimag	k1gInSc2
nabízí	nabízet	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
většiny	většina	k1gFnSc2
souborových	souborový	k2eAgInPc2d1
systémů	systém	k1gInPc2
používaných	používaný	k2eAgInPc2d1
v	v	k7c6
Linuxu	linux	k1gInSc6
i	i	k9
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledný	výsledný	k2eAgInSc1d1
obraz	obraz	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
komprimován	komprimovat	k5eAaImNgInS
pro	pro	k7c4
ušetření	ušetření	k1gNnSc4
diskového	diskový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
a	a	k8xC
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
několika	několik	k4yIc2
menších	malý	k2eAgInPc2d2
souborů	soubor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vypáleny	vypálit	k5eAaPmNgInP
na	na	k7c6
CD	CD	kA
nebo	nebo	k8xC
DVD	DVD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Diskové	diskový	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ukládány	ukládat	k5eAaImNgInP
i	i	k9
přes	přes	k7c4
počítačovou	počítačový	k2eAgFnSc4d1
síť	síť	k1gFnSc4
například	například	k6eAd1
použitím	použití	k1gNnSc7
Samby	samba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Partimage	Partimag	k1gInSc2
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
mnoha	mnoho	k4c6
distribucích	distribuce	k1gFnPc6
Linuxu	linux	k1gInSc6
včetně	včetně	k7c2
specializovaných	specializovaný	k2eAgNnPc2d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
SystemRescueCd	SystemRescueCda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
</s>
<s>
Partimage	Partimage	k1gFnSc1
kopíruje	kopírovat	k5eAaImIp3nS
data	datum	k1gNnPc4
pouze	pouze	k6eAd1
z	z	k7c2
použitých	použitý	k2eAgFnPc2d1
částí	část	k1gFnPc2
oddílů	oddíl	k1gInPc2
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
nejsou	být	k5eNaImIp3nP
do	do	k7c2
výsledného	výsledný	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
disku	disk	k1gInSc2
kopírovány	kopírován	k2eAgInPc1d1
prázdné	prázdný	k2eAgInPc1d1
bloky	blok	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
dosáhne	dosáhnout	k5eAaPmIp3nS
menšího	malý	k2eAgInSc2d2
souboru	soubor	k1gInSc2
<g/>
,	,	kIx,
než	než	k8xS
za	za	k7c4
použití	použití	k1gNnSc4
unixového	unixový	k2eAgInSc2d1
příkazu	příkaz	k1gInSc2
dd	dd	k?
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
kopíruje	kopírovat	k5eAaImIp3nS
vše	všechen	k3xTgNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
použití	použití	k1gNnSc6
komprimace	komprimace	k1gFnSc2
gzip	gzip	k1gInSc4
lze	lze	k6eAd1
komprimovat	komprimovat	k5eAaImF
1	#num#	k4
GB	GB	kA
soubor	soubor	k1gInSc1
až	až	k9
na	na	k7c4
400	#num#	k4
MB	MB	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
vytváření	vytváření	k1gNnSc6
obrazu	obraz	k1gInSc2
disku	disk	k1gInSc2
si	se	k3xPyFc3
lze	lze	k6eAd1
vybrat	vybrat	k5eAaPmF
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
úrovní	úroveň	k1gFnPc2
komprimace	komprimace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druh	druh	k1gInSc1
</s>
<s>
Popis	popis	k1gInSc1
úrovně	úroveň	k1gFnSc2
komprimace	komprimace	k1gFnSc2
</s>
<s>
none	none	k6eAd1
</s>
<s>
bez	bez	k7c2
komprimace	komprimace	k1gFnSc2
</s>
<s>
gzip	gzip	k1gMnSc1
</s>
<s>
malý	malý	k2eAgInSc1d1
soubor	soubor	k1gInSc1
<g/>
,	,	kIx,
rychlá	rychlý	k2eAgFnSc1d1
komprimace	komprimace	k1gFnSc1
</s>
<s>
bzip	bzip	k1gInSc1
<g/>
2	#num#	k4
</s>
<s>
velmi	velmi	k6eAd1
malý	malý	k2eAgInSc1d1
soubor	soubor	k1gInSc1
<g/>
,	,	kIx,
pomalejší	pomalý	k2eAgFnSc1d2
komprimace	komprimace	k1gFnSc1
</s>
<s>
lzo	lzo	k?
</s>
<s>
malý	malý	k2eAgInSc1d1
soubor	soubor	k1gInSc1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
komprimace	komprimace	k1gFnSc1
</s>
<s>
Podporované	podporovaný	k2eAgInPc1d1
souborové	souborový	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
Partimage	Partimage	k1gFnSc1
ukládá	ukládat	k5eAaImIp3nS
pouze	pouze	k6eAd1
použité	použitý	k2eAgInPc4d1
bloky	blok	k1gInPc4
diskových	diskový	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
a	a	k8xC
proto	proto	k8xC
musí	muset	k5eAaImIp3nS
rozumět	rozumět	k5eAaImF
struktuře	struktura	k1gFnSc3
souborového	souborový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
tedy	tedy	k9
uložit	uložit	k5eAaPmF
nepodporovaný	podporovaný	k2eNgInSc1d1
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Stav	stav	k1gInSc1
</s>
<s>
ext	ext	k?
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
ext	ext	k?
<g/>
3	#num#	k4
</s>
<s>
linuxový	linuxový	k2eAgInSc1d1
standard	standard	k1gInSc1
</s>
<s>
stabilní	stabilní	k2eAgFnSc1d1
</s>
<s>
ext	ext	k?
<g/>
4	#num#	k4
</s>
<s>
vylepšená	vylepšený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
ext	ext	k?
<g/>
3	#num#	k4
</s>
<s>
nepodporováno	podporován	k2eNgNnSc1d1
</s>
<s>
reiserfs-	reiserfs-	k?
<g/>
3	#num#	k4
</s>
<s>
Žurnálovací	Žurnálovací	k2eAgInSc1d1
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
stabilní	stabilní	k2eAgFnSc1d1
</s>
<s>
FAT	fatum	k1gNnPc2
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
Souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
DOS	DOS	kA
a	a	k8xC
Windows	Windows	kA
</s>
<s>
stabilní	stabilní	k2eAgFnSc1d1
</s>
<s>
HPFS	HPFS	kA
</s>
<s>
Souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
IBM	IBM	kA
OS	OS	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
stabilní	stabilní	k2eAgFnSc1d1
</s>
<s>
JFS	JFS	kA
</s>
<s>
Žurnálovací	Žurnálovací	k2eAgInSc1d1
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
stabilní	stabilní	k2eAgFnSc1d1
</s>
<s>
XFS	XFS	kA
</s>
<s>
Žurnálovací	Žurnálovací	k2eAgInSc1d1
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
stabilní	stabilní	k2eAgFnSc1d1
</s>
<s>
UFS	UFS	kA
</s>
<s>
Souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
Unix	Unix	k1gInSc1
</s>
<s>
beta	beta	k1gNnSc1
</s>
<s>
HFS	HFS	kA
</s>
<s>
Souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
MacOS	MacOS	k1gFnSc2
</s>
<s>
beta	beta	k1gNnSc1
</s>
<s>
NTFS	NTFS	kA
</s>
<s>
Windows	Windows	kA
NT	NT	kA
<g/>
,	,	kIx,
2000	#num#	k4
a	a	k8xC
XP	XP	kA
</s>
<s>
experimentální	experimentální	k2eAgInSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Partimage	Partimage	k1gFnPc2
<g/>
.	.	kIx.
www.partimage.org	www.partimage.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Partimage-manual	Partimage-manual	k1gInSc1
Usage	Usage	k1gFnSc1
-	-	kIx~
Partimage	Partimage	k1gFnSc1
<g/>
.	.	kIx.
www.partimage.org	www.partimage.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Supported-Filesystems	Supported-Filesystems	k1gInSc1
-	-	kIx~
Partimage	Partimage	k1gInSc1
<g/>
.	.	kIx.
www.partimage.org	www.partimage.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
SystemRescueCD	SystemRescueCD	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
