<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
tedy	tedy	k9	tedy
nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
ani	ani	k8xC	ani
nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
pólu	pólo	k1gNnSc6	pólo
se	se	k3xPyFc4	se
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
všechny	všechen	k3xTgInPc1	všechen
poledníky	poledník	k1gInPc1	poledník
<g/>
.	.	kIx.	.
</s>
