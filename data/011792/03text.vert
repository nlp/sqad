<p>
<s>
Jamestown	Jamestown	k1gInSc1	Jamestown
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
zámořského	zámořský	k2eAgNnSc2d1	zámořské
území	území	k1gNnSc2	území
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
Ascension	Ascension	k1gInSc1	Ascension
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
da	da	k?	da
Cunha	Cunha	k1gFnSc1	Cunha
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1500	[number]	k4	1500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
míšenců	míšenec	k1gMnPc2	míšenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
náboženství	náboženství	k1gNnSc3	náboženství
protestanského	protestanský	k2eAgNnSc2d1	protestanské
(	(	kIx(	(
hlavně	hlavně	k9	hlavně
anglikáni	anglikán	k1gMnPc1	anglikán
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1659	[number]	k4	1659
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
před	před	k7c7	před
otevřením	otevření	k1gNnSc7	otevření
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
otevření	otevření	k1gNnSc4	otevření
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
město	město	k1gNnSc4	město
i	i	k8xC	i
celý	celý	k2eAgInSc4d1	celý
ostrov	ostrov	k1gInSc4	ostrov
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
upadá	upadat	k5eAaPmIp3nS	upadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jamestown	Jamestowna	k1gFnPc2	Jamestowna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Jamestown	Jamestowna	k1gFnPc2	Jamestowna
</s>
</p>
<p>
<s>
fotografie	fotografia	k1gFnPc1	fotografia
města	město	k1gNnSc2	město
Jamestown	Jamestowna	k1gFnPc2	Jamestowna
</s>
</p>
