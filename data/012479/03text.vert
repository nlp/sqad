<p>
<s>
Malířské	malířský	k2eAgNnSc1d1	malířské
plátno	plátno	k1gNnSc1	plátno
je	být	k5eAaImIp3nS	být
textilní	textilní	k2eAgFnSc1d1	textilní
látka	látka	k1gFnSc1	látka
používaná	používaný	k2eAgFnSc1d1	používaná
jako	jako	k8xS	jako
povrchové	povrchový	k2eAgNnSc1d1	povrchové
médium	médium	k1gNnSc1	médium
pro	pro	k7c4	pro
výtvarná	výtvarný	k2eAgNnPc4d1	výtvarné
díla	dílo	k1gNnPc4	dílo
(	(	kIx(	(
<g/>
malby	malba	k1gFnSc2	malba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nejstarší	starý	k2eAgNnPc1d3	nejstarší
malířská	malířský	k2eAgNnPc1d1	malířské
plátna	plátno	k1gNnPc1	plátno
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
malby	malba	k1gFnSc2	malba
na	na	k7c4	na
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
předtím	předtím	k6eAd1	předtím
jako	jako	k8xC	jako
médium	médium	k1gNnSc1	médium
převládalo	převládat	k5eAaImAgNnS	převládat
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
olejomalby	olejomalba	k1gFnPc4	olejomalba
<g/>
,	,	kIx,	,
malby	malba	k1gFnPc4	malba
temperovými	temperový	k2eAgFnPc7d1	temperová
<g/>
,	,	kIx,	,
akrylovými	akrylový	k2eAgFnPc7d1	akrylová
a	a	k8xC	a
řidčeji	řídce	k6eAd2	řídce
akvarelovými	akvarelový	k2eAgFnPc7d1	akvarelová
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
hustě	hustě	k6eAd1	hustě
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
tkané	tkaný	k2eAgInPc4d1	tkaný
a	a	k8xC	a
proti	proti	k7c3	proti
tahu	tah	k1gInSc3	tah
odolné	odolný	k2eAgNnSc1d1	odolné
plátno	plátno	k1gNnSc1	plátno
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
textilií	textilie	k1gFnPc2	textilie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
bavlny	bavlna	k1gFnPc1	bavlna
–	–	k?	–
většinou	většinou	k6eAd1	většinou
kolem	kolem	k7c2	kolem
200	[number]	k4	200
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
,	,	kIx,	,
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
pro	pro	k7c4	pro
menší	malý	k2eAgNnSc4d2	menší
plátna	plátno	k1gNnPc4	plátno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
většinou	většinou	k6eAd1	většinou
jemnější	jemný	k2eAgFnSc4d2	jemnější
strukturu	struktura	k1gFnSc4	struktura
</s>
</p>
<p>
<s>
lnu	lnout	k5eAaImIp1nS	lnout
–	–	k?	–
většinou	většinou	k6eAd1	většinou
kolem	kolem	k7c2	kolem
400	[number]	k4	400
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
,	,	kIx,	,
vhodnějšího	vhodný	k2eAgInSc2d2	vhodnější
pro	pro	k7c4	pro
větší	veliký	k2eAgNnPc4d2	veliký
plátna	plátno	k1gNnPc4	plátno
<g/>
,	,	kIx,	,
s	s	k7c7	s
hrubší	hrubý	k2eAgFnSc7d2	hrubší
strukturoupopřípadě	strukturoupopřípad	k1gInSc6	strukturoupopřípad
jejich	jejich	k3xOp3gFnSc2	jejich
směsi	směs	k1gFnSc2	směs
pro	pro	k7c4	pro
polohrubá	polohrubý	k2eAgNnPc4d1	polohrubé
plátna	plátno	k1gNnPc4	plátno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malířské	malířský	k2eAgNnSc1d1	malířské
plátno	plátno	k1gNnSc1	plátno
se	se	k3xPyFc4	se
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
na	na	k7c4	na
napínací	napínací	k2eAgInSc4d1	napínací
rám	rám	k1gInSc4	rám
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
upnuto	upnut	k2eAgNnSc4d1	upnuto
–	–	k?	–
hřebíky	hřebík	k1gInPc4	hřebík
<g/>
,	,	kIx,	,
nýty	nýt	k1gInPc4	nýt
<g/>
,	,	kIx,	,
napínáčky	napínáček	k1gInPc4	napínáček
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
u	u	k7c2	u
současných	současný	k2eAgFnPc2d1	současná
maleb	malba	k1gFnPc2	malba
<g/>
)	)	kIx)	)
nastřelovacími	nastřelovací	k2eAgFnPc7d1	nastřelovací
sešívačkovými	sešívačkův	k2eAgFnPc7d1	sešívačkův
sponami	spona	k1gFnPc7	spona
<g/>
.	.	kIx.	.
</s>
<s>
Rám	rám	k1gInSc1	rám
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
blind	blind	k1gInSc1	blind
rám	rám	k1gInSc1	rám
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
oválný	oválný	k2eAgInSc1d1	oválný
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
různých	různý	k2eAgInPc2d1	různý
souměrných	souměrný	k2eAgInPc2d1	souměrný
erbů	erb	k1gInPc2	erb
apod.	apod.	kA	apod.
U	u	k7c2	u
rozměrnějších	rozměrný	k2eAgInPc2d2	rozměrnější
rámů	rám	k1gInPc2	rám
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uprostřed	uprostřed	k6eAd1	uprostřed
zpevněn	zpevnit	k5eAaPmNgInS	zpevnit
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
"	"	kIx"	"
<g/>
žebry	žebr	k1gInPc7	žebr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
napnutí	napnutí	k1gNnSc6	napnutí
se	se	k3xPyFc4	se
plátno	plátno	k1gNnSc1	plátno
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
připraví	připravit	k5eAaPmIp3nS	připravit
impregnací	impregnace	k1gFnSc7	impregnace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klížením	klížení	k1gNnPc3	klížení
<g/>
)	)	kIx)	)
králičím	králičí	k2eAgMnSc6d1	králičí
(	(	kIx(	(
<g/>
kožním	kožní	k2eAgInSc6d1	kožní
nebo	nebo	k8xC	nebo
kostním	kostní	k2eAgInSc6d1	kostní
<g/>
)	)	kIx)	)
klihem	klih	k1gInSc7	klih
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
doplněný	doplněný	k2eAgInSc1d1	doplněný
glycerinem	glycerin	k1gInSc7	glycerin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
rozpuštěným	rozpuštěný	k2eAgFnPc3d1	rozpuštěná
a	a	k8xC	a
ředěným	ředěný	k2eAgFnPc3d1	ředěná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
cca	cca	kA	cca
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
přetřením	přetření	k1gNnSc7	přetření
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
šepsováním	šepsování	k1gNnSc7	šepsování
<g/>
)	)	kIx)	)
podkladovou	podkladový	k2eAgFnSc7d1	podkladová
akrylátovou	akrylátový	k2eAgFnSc7d1	akrylátová
či	či	k8xC	či
jinou	jiný	k2eAgFnSc7d1	jiná
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Olejomalba	olejomalba	k1gFnSc1	olejomalba
</s>
</p>
<p>
<s>
Plátno	plátno	k1gNnSc1	plátno
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Příprava	příprava	k1gFnSc1	příprava
plátna	plátno	k1gNnSc2	plátno
pro	pro	k7c4	pro
malování	malování	k1gNnSc4	malování
</s>
</p>
