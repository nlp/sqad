<p>
<s>
Celsiův	Celsiův	k2eAgInSc1d1	Celsiův
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
značený	značený	k2eAgMnSc1d1	značený
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1742	[number]	k4	1742
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
švédský	švédský	k2eAgMnSc1d1	švédský
astronom	astronom	k1gMnSc1	astronom
Anders	Andersa	k1gFnPc2	Andersa
Celsius	Celsius	k1gMnSc1	Celsius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
stupnice	stupnice	k1gFnSc1	stupnice
obrácená	obrácený	k2eAgFnSc1d1	obrácená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Celsius	Celsius	k1gMnSc1	Celsius
stanovil	stanovit	k5eAaPmAgMnS	stanovit
dva	dva	k4xCgInPc4	dva
pevné	pevný	k2eAgInPc4d1	pevný
body	bod	k1gInPc4	bod
<g/>
:	:	kIx,	:
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
pro	pro	k7c4	pro
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
ledu	led	k1gInSc2	led
a	a	k8xC	a
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
pro	pro	k7c4	pro
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
obojí	obojí	k4xRgFnPc1	obojí
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
vzduchu	vzduch	k1gInSc2	vzduch
1013,25	[number]	k4	1013,25
hPa	hPa	k?	hPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k2eAgFnSc4d1	Linné
stupnici	stupnice	k1gFnSc4	stupnice
později	pozdě	k6eAd2	pozdě
otočil	otočit	k5eAaPmAgMnS	otočit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc2	tání
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
100	[number]	k4	100
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Celsiův	Celsiův	k2eAgInSc1d1	Celsiův
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
odvozená	odvozený	k2eAgFnSc1d1	odvozená
jednotka	jednotka	k1gFnSc1	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
)	)	kIx)	)
definována	definovat	k5eAaBmNgFnS	definovat
pomocí	pomocí	k7c2	pomocí
základní	základní	k2eAgFnSc2d1	základní
jednotky	jednotka	k1gFnSc2	jednotka
kelvin	kelvin	k1gInSc1	kelvin
<g/>
:	:	kIx,	:
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
teploty	teplota	k1gFnSc2	teplota
rovná	rovnat	k5eAaImIp3nS	rovnat
1	[number]	k4	1
K	K	kA	K
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
teplotní	teplotní	k2eAgFnSc6d1	teplotní
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
teplotě	teplota	k1gFnSc6	teplota
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
vody	voda	k1gFnSc2	voda
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
teplota	teplota	k1gFnSc1	teplota
0,01	[number]	k4	0,01
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Přepočet	přepočet	k1gInSc1	přepočet
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
stupnice	stupnice	k1gFnPc4	stupnice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kelvinova	Kelvinův	k2eAgFnSc1d1	Kelvinova
absolutní	absolutní	k2eAgFnSc1d1	absolutní
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
273	[number]	k4	273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
K-	K-	k1gFnSc1	K-
<g/>
273	[number]	k4	273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
K	K	kA	K
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Kelvinově	Kelvinův	k2eAgFnSc6d1	Kelvinova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
C	C	kA	C
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
pro	pro	k7c4	pro
nulové	nulový	k2eAgFnPc4d1	nulová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
+	+	kIx~	+
<g/>
273	[number]	k4	273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
273	[number]	k4	273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Fahrenheitova	Fahrenheitův	k2eAgFnSc1d1	Fahrenheitova
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
32	[number]	k4	32
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
F-	F-	k1gFnSc1	F-
<g/>
32	[number]	k4	32
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
32	[number]	k4	32
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
32	[number]	k4	32
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
F	F	kA	F
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
teploty	teplota	k1gFnSc2	teplota
ve	v	k7c6	v
Fahrenheitově	Fahrenheitův	k2eAgFnSc6d1	Fahrenheitova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
C	C	kA	C
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
pro	pro	k7c4	pro
nulové	nulový	k2eAgFnPc4d1	nulová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
32	[number]	k4	32
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
+	+	kIx~	+
<g/>
32	[number]	k4	32
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
17	[number]	k4	17
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
̄	̄	k?	̄
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Réaumurova	Réaumurův	k2eAgFnSc1d1	Réaumurova
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
C	C	kA	C
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
R	R	kA	R
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Réaumurově	Réaumurův	k2eAgFnSc6d1	Réaumurova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
C	C	kA	C
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
pro	pro	k7c4	pro
nulové	nulový	k2eAgFnPc4d1	nulová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Rankinova	Rankinův	k2eAgFnSc1d1	Rankinova
absolutní	absolutní	k2eAgFnSc1d1	absolutní
stupnice	stupnice	k1gFnSc1	stupnice
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
Ra-	Ra-	k1gFnSc2	Ra-
<g/>
273	[number]	k4	273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Ra	ra	k0	ra
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
273	[number]	k4	273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k9	kde
Ra	ra	k0	ra
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Rankinově	Rankinův	k2eAgFnSc6d1	Rankinova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
C	C	kA	C
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
pro	pro	k7c4	pro
nulové	nulový	k2eAgFnPc4d1	nulová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
491	[number]	k4	491
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
491	[number]	k4	491
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Ra	ra	k0	ra
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
273	[number]	k4	273
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Ra	ra	k0	ra
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
273	[number]	k4	273
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
15	[number]	k4	15
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Pravopis	pravopis	k1gInSc4	pravopis
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
platí	platit	k5eAaImIp3nP	platit
stejná	stejný	k2eAgNnPc1d1	stejné
pravidla	pravidlo	k1gNnPc1	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
jako	jako	k9	jako
na	na	k7c4	na
procenta	procento	k1gNnPc4	procento
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
psát	psát	k5eAaImF	psát
s	s	k7c7	s
mezerou	mezera	k1gFnSc7	mezera
mezi	mezi	k7c7	mezi
svojí	svůj	k3xOyFgFnSc7	svůj
značkou	značka	k1gFnSc7	značka
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
se	se	k3xPyFc4	se
značka	značka	k1gFnSc1	značka
stupně	stupeň	k1gInSc2	stupeň
přidružuje	přidružovat	k5eAaImIp3nS	přidružovat
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
(	(	kIx(	(
<g/>
360	[number]	k4	360
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
znamená	znamenat	k5eAaImIp3nS	znamenat
jeden	jeden	k4xCgInSc1	jeden
stupeň	stupeň	k1gInSc1	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
1	[number]	k4	1
<g/>
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
mezery	mezera	k1gFnSc2	mezera
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
jednostupňový	jednostupňový	k2eAgMnSc1d1	jednostupňový
(	(	kIx(	(
<g/>
s	s	k7c7	s
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stupně	stupeň	k1gInPc4	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
není	být	k5eNaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vždy	vždy	k6eAd1	vždy
mezera	mezera	k1gFnSc1	mezera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektronický	elektronický	k2eAgInSc1d1	elektronický
zápis	zápis	k1gInSc1	zápis
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
stupeň	stupeň	k1gInSc4	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
existuje	existovat	k5eAaImIp3nS	existovat
samostatný	samostatný	k2eAgInSc4d1	samostatný
symbol	symbol	k1gInSc4	symbol
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
"	"	kIx"	"
<g/>
°C	°C	k?	°C
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2103	[number]	k4	2103
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kompatibilní	kompatibilní	k2eAgFnSc7d1	kompatibilní
normální	normální	k2eAgFnSc7d1	normální
formou	forma	k1gFnSc7	forma
NFKD	NFKD	kA	NFKD
i	i	k8xC	i
NFKC	NFKC	kA	NFKC
je	být	k5eAaImIp3nS	být
dvojice	dvojice	k1gFnSc1	dvojice
symbolu	symbol	k1gInSc2	symbol
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
a	a	k8xC	a
písmene	písmeno	k1gNnSc2	písmeno
C.	C.	kA	C.
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
také	také	k9	také
značka	značka	k1gFnSc1	značka
běžně	běžně	k6eAd1	běžně
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
typografickém	typografický	k2eAgInSc6d1	typografický
systému	systém	k1gInSc6	systém
TeX	TeX	k1gMnPc2	TeX
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
běžně	běžně	k6eAd1	běžně
používaném	používaný	k2eAgInSc6d1	používaný
balíku	balík	k1gInSc6	balík
LaTeX	latex	k1gInSc1	latex
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
(	(	kIx(	(
<g/>
v	v	k7c6	v
matematickém	matematický	k2eAgInSc6d1	matematický
módu	mód	k1gInSc6	mód
definovaný	definovaný	k2eAgInSc4d1	definovaný
<g/>
)	)	kIx)	)
znak	znak	k1gInSc4	znak
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
indexu	index	k1gInSc6	index
<g/>
.	.	kIx.	.
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
vysází	vysázet	k5eAaPmIp3nS	vysázet
pomocí	pomoc	k1gFnSc7	pomoc
$	$	kIx~	$
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
^	^	kIx~	^
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
C	C	kA	C
<g/>
}	}	kIx)	}
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
LaTeXovém	latexový	k2eAgInSc6d1	latexový
balíku	balík	k1gInSc6	balík
gensymb	gensymb	k1gMnSc1	gensymb
je	být	k5eAaImIp3nS	být
definován	definován	k2eAgInSc4d1	definován
příkaz	příkaz	k1gInSc4	příkaz
\	\	kIx~	\
<g/>
celsius	celsius	k1gInSc4	celsius
pro	pro	k7c4	pro
stupeň	stupeň	k1gInSc4	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
matematickém	matematický	k2eAgInSc6d1	matematický
režimu	režim	k1gInSc6	režim
i	i	k9	i
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
balíku	balík	k1gInSc6	balík
textcomp	textcomp	k1gMnSc1	textcomp
příkaz	příkaz	k1gInSc1	příkaz
\	\	kIx~	\
<g/>
textcelsius	textcelsius	k1gInSc1	textcelsius
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgInPc1d1	definován
příkazy	příkaz	k1gInPc1	příkaz
\	\	kIx~	\
<g/>
degree	degree	k1gNnPc1	degree
a	a	k8xC	a
\	\	kIx~	\
<g/>
textdegree	textdegree	k5eAaImRp2nP	textdegree
pro	pro	k7c4	pro
samotný	samotný	k2eAgInSc4d1	samotný
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
