<s>
Trent	Trent	k1gMnSc1	Trent
Reznor	Reznor	k1gMnSc1	Reznor
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1965	[number]	k4	1965
Mercer	Mercer	k1gMnSc1	Mercer
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
skladatel	skladatel	k1gMnSc1	skladatel
Nine	Nin	k1gFnSc2	Nin
Inch	Inch	k1gMnSc1	Inch
Nails	Nailsa	k1gFnPc2	Nailsa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
