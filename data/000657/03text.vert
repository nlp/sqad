<s>
Lawrencium	Lawrencium	k1gNnSc1	Lawrencium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Lr	Lr	k1gFnSc2	Lr
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Lawrencium	Lawrencium	k1gNnSc4	Lawrencium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patnáctým	patnáctý	k4xOgInSc7	patnáctý
(	(	kIx(	(
<g/>
posledním	poslední	k2eAgMnSc7d1	poslední
<g/>
)	)	kIx)	)
členem	člen	k1gInSc7	člen
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
aktinoidů	aktinoid	k1gInPc2	aktinoid
<g/>
,	,	kIx,	,
jedenáctým	jedenáctý	k4xOgInSc7	jedenáctý
transuranem	transuran	k1gInSc7	transuran
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
uměle	uměle	k6eAd1	uměle
ozařováním	ozařování	k1gNnSc7	ozařování
jader	jádro	k1gNnPc2	jádro
kalifornia	kalifornium	k1gNnSc2	kalifornium
<g/>
.	.	kIx.	.
</s>
<s>
Lawrencium	Lawrencium	k1gNnSc1	Lawrencium
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
izolován	izolovat	k5eAaBmNgInS	izolovat
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
konstanty	konstanta	k1gFnPc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
α	α	k?	α
a	a	k8xC	a
γ	γ	k?	γ
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
silným	silný	k2eAgInSc7d1	silný
zdrojem	zdroj	k1gInSc7	zdroj
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
manipulovat	manipulovat	k5eAaImF	manipulovat
za	za	k7c4	za
dodržování	dodržování	k1gNnSc4	dodržování
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
radioaktivními	radioaktivní	k2eAgInPc7d1	radioaktivní
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
sloučeninách	sloučenina	k1gFnPc6	sloučenina
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
chemickém	chemický	k2eAgNnSc6d1	chemické
chování	chování	k1gNnSc6	chování
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Lawrencium	Lawrencium	k1gNnSc1	Lawrencium
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uměle	uměle	k6eAd1	uměle
připravený	připravený	k2eAgInSc1d1	připravený
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
transuranů	transuran	k1gInPc2	transuran
<g/>
.	.	kIx.	.
</s>
<s>
Lawrencium	Lawrencium	k1gNnSc1	Lawrencium
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
připraveno	připravit	k5eAaPmNgNnS	připravit
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
university	universita	k1gFnSc2	universita
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nového	nový	k2eAgInSc2d1	nový
lineárního	lineární	k2eAgInSc2d1	lineární
urychlovače	urychlovač	k1gInSc2	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uvedeném	uvedený	k2eAgInSc6d1	uvedený
experimentu	experiment	k1gInSc6	experiment
byl	být	k5eAaImAgInS	být
bombardován	bombardován	k2eAgInSc1d1	bombardován
terč	terč	k1gInSc1	terč
z	z	k7c2	z
izotopů	izotop	k1gInPc2	izotop
kalifornia	kalifornium	k1gNnSc2	kalifornium
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
3	[number]	k4	3
mg	mg	kA	mg
jádry	jádro	k1gNnPc7	jádro
boru	bor	k1gInSc3	bor
10B	[number]	k4	10B
a	a	k8xC	a
11B	[number]	k4	11B
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
získáno	získán	k2eAgNnSc1d1	získáno
lawrencium	lawrencium	k1gNnSc1	lawrencium
257	[number]	k4	257
<g/>
Lr	Lr	k1gFnPc2	Lr
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
4,2	[number]	k4	4,2
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
252	[number]	k4	252
98	[number]	k4	98
Cf	Cf	k1gFnPc2	Cf
+	+	kIx~	+
11	[number]	k4	11
5	[number]	k4	5
B	B	kA	B
→	→	k?	→
258	[number]	k4	258
103	[number]	k4	103
Lr	Lr	k1gFnPc2	Lr
+	+	kIx~	+
5	[number]	k4	5
1	[number]	k4	1
0	[number]	k4	0
n	n	k0	n
Za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
objevitele	objevitel	k1gMnSc2	objevitel
jsou	být	k5eAaImIp3nP	být
označováni	označovat	k5eAaImNgMnP	označovat
Albert	Alberta	k1gFnPc2	Alberta
Ghiorso	Ghiorsa	k1gFnSc5	Ghiorsa
<g/>
,	,	kIx,	,
Almon	Almon	k1gMnSc1	Almon
Larsh	Larsh	k1gMnSc1	Larsh
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
M.	M.	kA	M.
Latimer	Latimer	k1gMnSc1	Latimer
a	a	k8xC	a
Torbjø	Torbjø	k1gMnSc1	Torbjø
Sikkeland	Sikkelanda	k1gFnPc2	Sikkelanda
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
vynálezce	vynálezce	k1gMnSc2	vynálezce
cyklotronu	cyklotron	k1gInSc2	cyklotron
Ernesta	Ernest	k1gMnSc2	Ernest
O.	O.	kA	O.
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgMnS	být
tomuto	tento	k3xDgInSc3	tento
prvku	prvek	k1gInSc3	prvek
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
symbol	symbol	k1gInSc1	symbol
Lw	Lw	k1gFnSc2	Lw
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
IUPAC	IUPAC	kA	IUPAC
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
byla	být	k5eAaImAgFnS	být
značka	značka	k1gFnSc1	značka
lawrencia	lawrencia	k1gFnSc1	lawrencia
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
Lr	Lr	k1gFnSc4	Lr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
Ústavu	ústav	k1gInSc2	ústav
jaderného	jaderný	k2eAgInSc2d1	jaderný
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
Dubně	Dubna	k1gFnSc6	Dubna
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
oznámili	oznámit	k5eAaPmAgMnP	oznámit
pozorovaný	pozorovaný	k2eAgInSc4d1	pozorovaný
vznik	vznik	k1gInSc4	vznik
izotopu	izotop	k1gInSc2	izotop
lawrencia	lawrencia	k1gFnSc1	lawrencia
256	[number]	k4	256
<g/>
Lr	Lr	k1gFnPc2	Lr
po	po	k7c4	po
reakci	reakce	k1gFnSc4	reakce
jader	jádro	k1gNnPc2	jádro
americia	americium	k1gNnSc2	americium
243	[number]	k4	243
<g/>
Am	Am	k1gFnPc2	Am
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
jádrem	jádro	k1gNnSc7	jádro
kyslíku	kyslík	k1gInSc2	kyslík
18	[number]	k4	18
<g/>
O.	O.	kA	O.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
243	[number]	k4	243
95	[number]	k4	95
Am	Am	k1gFnPc2	Am
+	+	kIx~	+
18	[number]	k4	18
8	[number]	k4	8
O	o	k7c6	o
→	→	k?	→
256	[number]	k4	256
103	[number]	k4	103
Lr	Lr	k1gFnPc2	Lr
+	+	kIx~	+
5	[number]	k4	5
1	[number]	k4	1
0	[number]	k4	0
n	n	k0	n
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
izotopů	izotop	k1gInPc2	izotop
lawrencia	lawrencius	k1gMnSc2	lawrencius
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejstabilnější	stabilní	k2eAgFnSc1d3	nejstabilnější
je	být	k5eAaImIp3nS	být
262	[number]	k4	262
<g/>
Lr	Lr	k1gFnPc2	Lr
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
216	[number]	k4	216
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Aktinoidy	Aktinoid	k1gInPc4	Aktinoid
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lawrencium	lawrencium	k1gNnSc4	lawrencium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lawrencium	lawrencium	k1gNnSc4	lawrencium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
