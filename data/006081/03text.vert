<s>
Metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yIgFnSc2	který
lze	lze	k6eAd1	lze
formálním	formální	k2eAgInSc7d1	formální
způsobem	způsob	k1gInSc7	způsob
definovat	definovat	k5eAaBmF	definovat
pojem	pojem	k1gInSc4	pojem
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
metrických	metrický	k2eAgInPc6d1	metrický
prostorech	prostor	k1gInPc6	prostor
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
definují	definovat	k5eAaBmIp3nP	definovat
další	další	k2eAgFnPc4d1	další
topologické	topologický	k2eAgFnPc4d1	topologická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xC	jako
např.	např.	kA	např.
otevřenost	otevřenost	k1gFnSc1	otevřenost
a	a	k8xC	a
uzavřenost	uzavřenost	k1gFnSc1	uzavřenost
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zobecnění	zobecnění	k1gNnSc1	zobecnění
pak	pak	k6eAd1	pak
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
ještě	ještě	k6eAd1	ještě
abstraktnější	abstraktní	k2eAgInSc4d2	abstraktnější
matematický	matematický	k2eAgInSc4d1	matematický
pojem	pojem	k1gInSc4	pojem
topologického	topologický	k2eAgInSc2d1	topologický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Fréchet	Frécheta	k1gFnPc2	Frécheta
zavedl	zavést	k5eAaPmAgMnS	zavést
pojem	pojem	k1gInSc4	pojem
metrického	metrický	k2eAgInSc2d1	metrický
prostoru	prostor	k1gInSc2	prostor
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Sur	Sur	k1gFnSc4	Sur
quelques	quelques	k1gMnSc1	quelques
points	pointsa	k1gFnPc2	pointsa
du	du	k?	du
calcul	calcout	k5eAaPmAgMnS	calcout
fonctionnel	fonctionnel	k1gMnSc1	fonctionnel
<g/>
,	,	kIx,	,
Rendic	Rendic	k1gMnSc1	Rendic
<g/>
.	.	kIx.	.
</s>
<s>
Circ	Circ	k6eAd1	Circ
<g/>
.	.	kIx.	.
</s>
<s>
Mat.	Mat.	k?	Mat.
Palermo	Palermo	k1gNnSc1	Palermo
22	[number]	k4	22
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
74	[number]	k4	74
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
<g/>
"	"	kIx"	"
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
některé	některý	k3yIgInPc1	některý
pojmy	pojem	k1gInPc1	pojem
(	(	kIx(	(
<g/>
definované	definovaný	k2eAgFnPc1d1	definovaná
pomocí	pomocí	k7c2	pomocí
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
bodů	bod	k1gInPc2	bod
na	na	k7c6	na
reálné	reálný	k2eAgFnSc6d1	reálná
ose	osa	k1gFnSc6	osa
<g/>
)	)	kIx)	)
daly	dát	k5eAaPmAgInP	dát
zavést	zavést	k5eAaPmF	zavést
pro	pro	k7c4	pro
širší	široký	k2eAgFnSc4d2	širší
skupinu	skupina	k1gFnSc4	skupina
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takových	takový	k3xDgInPc2	takový
pojmů	pojem	k1gInPc2	pojem
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Otevřené	otevřený	k2eAgFnPc1d1	otevřená
a	a	k8xC	a
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
množiny	množina	k1gFnPc1	množina
Spojité	spojitý	k2eAgFnPc4d1	spojitá
zobrazení	zobrazení	k1gNnSc4	zobrazení
Cauchyovská	Cauchyovský	k2eAgFnSc1d1	Cauchyovský
a	a	k8xC	a
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
posloupnost	posloupnost	k1gFnSc1	posloupnost
Kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
množina	množina	k1gFnSc1	množina
Tyto	tento	k3xDgInPc4	tento
pojmy	pojem	k1gInPc4	pojem
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
definice	definice	k1gFnPc4	definice
na	na	k7c6	na
reálné	reálný	k2eAgFnSc6d1	reálná
ose	osa	k1gFnSc6	osa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
silně	silně	k6eAd1	silně
využívají	využívat	k5eAaPmIp3nP	využívat
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
absolutní	absolutní	k2eAgFnSc1d1	absolutní
hodnota	hodnota	k1gFnSc1	hodnota
rozdílu	rozdíl	k1gInSc2	rozdíl
dvou	dva	k4xCgNnPc2	dva
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
však	však	k9	však
zobecnit	zobecnit	k5eAaPmF	zobecnit
na	na	k7c4	na
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
množinu	množina	k1gFnSc4	množina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
"	"	kIx"	"
nějak	nějak	k6eAd1	nějak
definovaný	definovaný	k2eAgInSc1d1	definovaný
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
množinu	množina	k1gFnSc4	množina
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
množinu	množina	k1gFnSc4	množina
spojitých	spojitý	k2eAgFnPc2d1	spojitá
funkcí	funkce	k1gFnPc2	funkce
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
je	být	k5eAaImIp3nS	být
maximum	maximum	k1gNnSc4	maximum
jejich	jejich	k3xOp3gInSc2	jejich
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
ptát	ptát	k5eAaImF	ptát
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
množina	množina	k1gFnSc1	množina
funkcí	funkce	k1gFnPc2	funkce
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
posloupnost	posloupnost	k1gFnSc1	posloupnost
funkcí	funkce	k1gFnPc2	funkce
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
apod.	apod.	kA	apod.
Jelikož	jelikož	k8xS	jelikož
studium	studium	k1gNnSc1	studium
těchto	tento	k3xDgFnPc2	tento
analogií	analogie	k1gFnPc2	analogie
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
reálnou	reálný	k2eAgFnSc7d1	reálná
osou	osa	k1gFnSc7	osa
a	a	k8xC	a
složitějšími	složitý	k2eAgFnPc7d2	složitější
množinami	množina	k1gFnPc7	množina
<g/>
)	)	kIx)	)
přináší	přinášet	k5eAaImIp3nS	přinášet
mnoho	mnoho	k4c1	mnoho
užitečných	užitečný	k2eAgInPc2d1	užitečný
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
formalizovány	formalizovat	k5eAaBmNgFnP	formalizovat
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
Metrický	metrický	k2eAgInSc4d1	metrický
prostor	prostor	k1gInSc4	prostor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zobrazením	zobrazení	k1gNnSc7	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
každé	každý	k3xTgNnSc1	každý
dvojici	dvojice	k1gFnSc3	dvojice
bodů	bod	k1gInPc2	bod
přiřadí	přiřadit	k5eAaPmIp3nS	přiřadit
tzv.	tzv.	kA	tzv.
metriku	metrika	k1gFnSc4	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Pojmy	pojem	k1gInPc1	pojem
"	"	kIx"	"
<g/>
metrika	metrika	k1gFnSc1	metrika
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
při	při	k7c6	při
neformálním	formální	k2eNgNnSc6d1	neformální
vyjadřování	vyjadřování	k1gNnSc6	vyjadřování
užívají	užívat	k5eAaImIp3nP	užívat
záměnně	záměnně	k6eAd1	záměnně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
metrika	metrika	k1gFnSc1	metrika
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
libovolné	libovolný	k2eAgNnSc4d1	libovolné
zobrazení	zobrazení	k1gNnSc4	zobrazení
splňující	splňující	k2eAgInPc4d1	splňující
axiomy	axiom	k1gInPc4	axiom
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
množině	množina	k1gFnSc6	množina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
body	bod	k1gInPc4	bod
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zavést	zavést	k5eAaPmF	zavést
několik	několik	k4yIc4	několik
různých	různý	k2eAgFnPc2d1	různá
metrik	metrika	k1gFnPc2	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Metrický	metrický	k2eAgInSc4d1	metrický
prostor	prostor	k1gInSc4	prostor
je	být	k5eAaImIp3nS	být
dvojice	dvojice	k1gFnSc1	dvojice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
ρ	ρ	k?	ρ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
libovolná	libovolný	k2eAgFnSc1d1	libovolná
neprázdná	prázdný	k2eNgFnSc1d1	neprázdná
množina	množina	k1gFnSc1	množina
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
metrika	metrika	k1gFnSc1	metrika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
×	×	k?	×
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
:	:	kIx,	:
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yIgNnSc1	který
splňuje	splňovat	k5eAaImIp3nS	splňovat
následující	následující	k2eAgInPc4d1	následující
axiomy	axiom	k1gInPc4	axiom
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
libovolná	libovolný	k2eAgFnSc1d1	libovolná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
,	,	kIx,	,
z	z	k7c2	z
∈	∈	k?	∈
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}}	}}}	k?	}}}
)	)	kIx)	)
<g/>
:	:	kIx,	:
Axiom	axiom	k1gInSc1	axiom
nezápornosti	nezápornost	k1gFnSc2	nezápornost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
≥	≥	k?	≥
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Axiom	axiom	k1gInSc1	axiom
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
⟺	⟺	k?	⟺
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
iff	iff	k?	iff
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
:	:	kIx,	:
Axiom	axiom	k1gInSc1	axiom
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
ρ	ρ	k?	ρ
(	(	kIx(	(
y	y	k?	y
,	,	kIx,	,
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Trojúhelníková	trojúhelníkový	k2eAgFnSc1d1	trojúhelníková
nerovnost	nerovnost	k1gFnSc1	nerovnost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
≤	≤	k?	≤
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
+	+	kIx~	+
ρ	ρ	k?	ρ
(	(	kIx(	(
y	y	k?	y
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tyto	tento	k3xDgInPc1	tento
axiomy	axiom	k1gInPc1	axiom
nejsou	být	k5eNaImIp3nP	být
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
,	,	kIx,	,
nezápornost	nezápornost	k1gFnSc1	nezápornost
totiž	totiž	k9	totiž
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
tří	tři	k4xCgInPc2	tři
axiomů	axiom	k1gInPc2	axiom
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
+	+	kIx~	+
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
≥	≥	k?	≥
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Nahradíme	nahradit	k5eAaPmIp1nP	nahradit
<g/>
-li	i	k?	-li
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
nerovnost	nerovnost	k1gFnSc4	nerovnost
pozměněným	pozměněný	k2eAgInSc7d1	pozměněný
tvarem	tvar	k1gInSc7	tvar
4	[number]	k4	4
<g/>
*	*	kIx~	*
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
≤	≤	k?	≤
ρ	ρ	k?	ρ
(	(	kIx(	(
z	z	k0	z
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
+	+	kIx~	+
ρ	ρ	k?	ρ
(	(	kIx(	(
y	y	k?	y
,	,	kIx,	,
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
z	z	k0	z
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
nezápornost	nezápornost	k1gFnSc1	nezápornost
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
axiomu	axiom	k1gInSc2	axiom
4	[number]	k4	4
<g/>
*	*	kIx~	*
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
axiomů	axiom	k1gInPc2	axiom
2	[number]	k4	2
a	a	k8xC	a
4	[number]	k4	4
<g/>
*	*	kIx~	*
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
symetrie	symetrie	k1gFnSc1	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
bodů	bod	k1gInPc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Vynecháme	vynechat	k5eAaPmIp1nP	vynechat
<g/>
-li	i	k?	-li
v	v	k7c6	v
axiomu	axiom	k1gInSc6	axiom
2	[number]	k4	2
implikaci	implikace	k1gFnSc4	implikace
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
připustíme	připustit	k5eAaPmIp1nP	připustit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
dva	dva	k4xCgInPc1	dva
různé	různý	k2eAgInPc1d1	různý
body	bod	k1gInPc1	bod
měly	mít	k5eAaImAgInP	mít
nulovou	nulový	k2eAgFnSc4d1	nulová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
)	)	kIx)	)
a	a	k8xC	a
ponecháme	ponechat	k5eAaPmIp1nP	ponechat
tak	tak	k6eAd1	tak
pouze	pouze	k6eAd1	pouze
rovnost	rovnost	k1gFnSc1	rovnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
zobrazení	zobrazení	k1gNnSc4	zobrazení
pseudometrikou	pseudometrika	k1gFnSc7	pseudometrika
<g/>
.	.	kIx.	.
</s>
<s>
Vynecháme	vynechat	k5eAaPmIp1nP	vynechat
<g/>
-li	i	k?	-li
4	[number]	k4	4
<g/>
.	.	kIx.	.
axiom	axiom	k1gInSc4	axiom
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
zobrazení	zobrazení	k1gNnSc4	zobrazení
semimetrikou	semimetrika	k1gFnSc7	semimetrika
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Každý	každý	k3xTgInSc1	každý
normovaný	normovaný	k2eAgInSc1d1	normovaný
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
metrickým	metrický	k2eAgInSc7d1	metrický
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
metrikou	metrika	k1gFnSc7	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
y	y	k?	y
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
x-y	x	k?	x-y
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
absolutní	absolutní	k2eAgFnSc1d1	absolutní
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
libovolné	libovolný	k2eAgInPc4d1	libovolný
body	bod	k1gInPc4	bod
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
úplný	úplný	k2eAgInSc1d1	úplný
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
euklidovském	euklidovský	k2eAgInSc6d1	euklidovský
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ve	v	k7c6	v
vícerozměrném	vícerozměrný	k2eAgInSc6d1	vícerozměrný
prostoru	prostor	k1gInSc6	prostor
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
metriku	metrika	k1gFnSc4	metrika
mnoha	mnoho	k4c7	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejběžnější	běžný	k2eAgMnPc1d3	Nejběžnější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Na	na	k7c6	na
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
tzv.	tzv.	kA	tzv.
euklidovskou	euklidovský	k2eAgFnSc4d1	euklidovská
metriku	metrika	k1gFnSc4	metrika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
délku	délka	k1gFnSc4	délka
úsečky	úsečka	k1gFnSc2	úsečka
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
euklidovský	euklidovský	k2eAgInSc1d1	euklidovský
prostor	prostor	k1gInSc1	prostor
dimenze	dimenze	k1gFnSc2	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Euklidovská	euklidovský	k2eAgFnSc1d1	euklidovská
metrika	metrika	k1gFnSc1	metrika
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
následujícím	následující	k2eAgInSc7d1	následující
vztahem	vztah	k1gInSc7	vztah
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigl	Bigl	k1gInSc1	Bigl
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigr	Bigr	k1gInSc1	Bigr
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{{	{{	k?	{{
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
součtová	součtový	k2eAgFnSc1d1	součtová
či	či	k8xC	či
manhattanská	manhattanský	k2eAgFnSc1d1	manhattanská
metrika	metrika	k1gFnSc1	metrika
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ujít	ujít	k5eAaPmF	ujít
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
křižovatkami	křižovatka	k1gFnPc7	křižovatka
<g />
.	.	kIx.	.
</s>
<s hack="1">
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
pohybovat	pohybovat	k5eAaImF	pohybovat
jen	jen	k9	jen
po	po	k7c6	po
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
kolmých	kolmý	k2eAgFnPc6d1	kolmá
ulicích	ulice	k1gFnPc6	ulice
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
obou	dva	k4xCgFnPc2	dva
os	osa	k1gFnPc2	osa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
<g />
.	.	kIx.	.
</s>
<s hack="1">
maximová	maximová	k1gFnSc1	maximová
metrika	metrika	k1gFnSc1	metrika
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
max	max	kA	max
{	{	kIx(	{
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
max	max	kA	max
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Na	na	k7c6	na
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
normovaném	normovaný	k2eAgInSc6d1	normovaný
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
pošťáckou	pošťácký	k2eAgFnSc7d1	Pošťácká
(	(	kIx(	(
<g/>
pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
<g/>
,	,	kIx,	,
moskevskou	moskevský	k2eAgFnSc7d1	Moskevská
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
metriku	metrika	k1gFnSc4	metrika
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
∥	∥	k?	∥
x	x	k?	x
∥	∥	k?	∥
+	+	kIx~	+
∥	∥	k?	∥
y	y	k?	y
∥	∥	k?	∥
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
≠	≠	k?	≠
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
y	y	k?	y
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
metrice	metrika	k1gFnSc6	metrika
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
počátek	počátek	k1gInSc1	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
představit	představit	k5eAaPmF	představit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
cesty	cesta	k1gFnPc1	cesta
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
A	a	k9	a
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
B	B	kA	B
vedou	vést	k5eAaImIp3nP	vést
nejprve	nejprve	k6eAd1	nejprve
z	z	k7c2	z
A	a	k8xC	a
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
významného	významný	k2eAgInSc2d1	významný
bodu	bod	k1gInSc2	bod
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
poté	poté	k6eAd1	poté
do	do	k7c2	do
B.	B.	kA	B.
Metrickým	metrický	k2eAgInSc7d1	metrický
prostorem	prostor	k1gInSc7	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
(	(	kIx(	(
⟨	⟨	k?	⟨
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
⟩	⟩	k?	⟩
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
)	)	kIx)	)
<g/>
}	}	kIx)	}
nazýváme	nazývat	k5eAaImIp1nP	nazývat
prostor	prostor	k1gInSc4	prostor
všech	všecek	k3xTgFnPc2	všecek
spojitých	spojitý	k2eAgFnPc2d1	spojitá
funkcí	funkce	k1gFnPc2	funkce
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
s	s	k7c7	s
metrikou	metrika	k1gFnSc7	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
f	f	k?	f
,	,	kIx,	,
g	g	kA	g
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
a	a	k8xC	a
≤	≤	k?	≤
x	x	k?	x
≤	≤	k?	≤
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
−	−	k?	−
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sup	sup	k1gMnSc1	sup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
supremová	supremový	k2eAgFnSc1d1	supremový
metrika	metrika	k1gFnSc1	metrika
<g/>
)	)	kIx)	)
Další	další	k2eAgNnSc1d1	další
možnou	možný	k2eAgFnSc7d1	možná
metrikou	metrika	k1gFnSc7	metrika
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
spojitých	spojitý	k2eAgFnPc2d1	spojitá
funkcí	funkce	k1gFnPc2	funkce
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
integrální	integrální	k2eAgFnSc7d1	integrální
<g />
.	.	kIx.	.
</s>
<s hack="1">
metrika	metrika	k1gFnSc1	metrika
(	(	kIx(	(
<g/>
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
nazývá	nazývat	k5eAaImIp3nS	nazývat
Lp	Lp	k1gFnSc4	Lp
prostor	prostora	k1gFnPc2	prostora
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
f	f	k?	f
,	,	kIx,	,
g	g	kA	g
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
−	−	k?	−
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
|	|	kIx~	|
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
left	left	k1gMnSc1	left
<g/>
|	|	kIx~	|
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
]	]	kIx)	]
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Na	na	k7c6	na
libovolné	libovolný	k2eAgFnSc6d1	libovolná
neprázdné	prázdný	k2eNgFnSc6d1	neprázdná
množině	množina	k1gFnSc6	množina
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
většina	většina	k1gFnSc1	většina
užitečných	užitečný	k2eAgFnPc2d1	užitečná
aplikací	aplikace	k1gFnPc2	aplikace
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
diskrétních	diskrétní	k2eAgFnPc2d1	diskrétní
množin	množina	k1gFnPc2	množina
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zavést	zavést	k5eAaPmF	zavést
diskrétní	diskrétní	k2eAgFnSc4d1	diskrétní
metriku	metrika	k1gFnSc4	metrika
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
≠	≠	k?	≠
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
y	y	k?	y
<g/>
}	}	kIx)	}
:	:	kIx,	:
Levenštejnova	Levenštejnův	k2eAgFnSc1d1	Levenštejnův
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
podobnost	podobnost	k1gFnSc1	podobnost
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
<g/>
)	)	kIx)	)
dvou	dva	k4xCgInPc2	dva
textových	textový	k2eAgInPc2d1	textový
řetězců	řetězec	k1gInPc2	řetězec
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
jako	jako	k9	jako
počet	počet	k1gInSc1	počet
změn	změna	k1gFnPc2	změna
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nahrazení	nahrazení	k1gNnSc4	nahrazení
<g/>
,	,	kIx,	,
vložení	vložení	k1gNnSc4	vložení
nebo	nebo	k8xC	nebo
vypuštění	vypuštění	k1gNnSc4	vypuštění
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
k	k	k7c3	k
transformaci	transformace	k1gFnSc3	transformace
jednoho	jeden	k4xCgInSc2	jeden
řetězce	řetězec	k1gInSc2	řetězec
v	v	k7c4	v
druhý	druhý	k4xOgInSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
grafu	graf	k1gInSc6	graf
je	být	k5eAaImIp3nS	být
metrikou	metrika	k1gFnSc7	metrika
na	na	k7c6	na
vrcholech	vrchol	k1gInPc6	vrchol
tohoto	tento	k3xDgInSc2	tento
grafu	graf	k1gInSc2	graf
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
neorientovaný	orientovaný	k2eNgMnSc1d1	neorientovaný
a	a	k8xC	a
souvislý	souvislý	k2eAgMnSc1d1	souvislý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
Riemannově	Riemannův	k2eAgInSc6d1	Riemannův
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
definovat	definovat	k5eAaBmF	definovat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
vlakových	vlakový	k2eAgNnPc2d1	vlakové
nádraží	nádraží	k1gNnPc2	nádraží
a	a	k8xC	a
metrika	metrika	k1gFnSc1	metrika
definovaná	definovaný	k2eAgFnSc1d1	definovaná
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
množině	množina	k1gFnSc6	množina
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
po	po	k7c6	po
kolejích	kolej	k1gFnPc6	kolej
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
nádražími	nádraží	k1gNnPc7	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
,	,	kIx,	,
ρ	ρ	k?	ρ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
)	)	kIx)	)
<g/>
}	}	kIx)	}
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
X	X	kA	X
,	,	kIx,	,
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
,	,	kIx,	,
M	M	kA	M
⊂	⊂	k?	⊂
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
X	X	kA	X
<g/>
}	}	kIx)	}
:	:	kIx,	:
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
koule	koule	k1gFnSc1	koule
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
ε	ε	k?	ε
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
{	{	kIx(	{
y	y	k?	y
∈	∈	k?	∈
X	X	kA	X
;	;	kIx,	;
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
<	<	kIx(	<
ε	ε	k?	ε
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
\	\	kIx~	\
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
místo	místo	k1gNnSc4	místo
o	o	k7c6	o
otevřené	otevřený	k2eAgFnSc6d1	otevřená
kouli	koule	k1gFnSc6	koule
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
ε	ε	k?	ε
bodu	bod	k1gInSc6	bod
x	x	k?	x
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Prstencové	prstencový	k2eAgInPc4d1	prstencový
(	(	kIx(	(
<g/>
redukované	redukovaný	k2eAgInPc4d1	redukovaný
<g/>
)	)	kIx)	)
ε	ε	k?	ε
bodu	bod	k1gInSc2	bod
x	x	k?	x
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
∖	∖	k?	∖
{	{	kIx(	{
x	x	k?	x
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
setminus	setminus	k1gInSc1	setminus
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
koule	koule	k1gFnSc1	koule
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
=	=	kIx~	=
{	{	kIx(	{
y	y	k?	y
∈	∈	k?	∈
X	X	kA	X
;	;	kIx,	;
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
≤	≤	k?	≤
ε	ε	k?	ε
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
zúžení	zúžení	k1gNnPc4	zúžení
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
metrika	metrika	k1gFnSc1	metrika
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
M	M	kA	M
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
podprostor	podprostor	k1gInSc1	podprostor
metrického	metrický	k2eAgInSc2d1	metrický
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
,	,	kIx,	,
ρ	ρ	k?	ρ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
bod	bod	k1gInSc1	bod
množiny	množina	k1gFnSc2	množina
M	M	kA	M
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
existuje	existovat	k5eAaImIp3nS	existovat
ε	ε	k?	ε
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
splňující	splňující	k2eAgFnSc1d1	splňující
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
⊂	⊂	k?	⊂
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gInSc1	subset
M	M	kA	M
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
bodů	bod	k1gInPc2	bod
množiny	množina	k1gFnSc2	množina
M	M	kA	M
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vnitřkem	vnitřek	k1gInSc7	vnitřek
množiny	množina	k1gFnSc2	množina
M	M	kA	M
a	a	k8xC	a
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k8xC	i
n	n	k0	n
t	t	k?	t
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
IntM	IntM	k1gFnSc3	IntM
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množinu	množina	k1gFnSc4	množina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
M	M	kA	M
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
x	x	k?	x
je	být	k5eAaImIp3nS	být
hromadným	hromadný	k2eAgInSc7d1	hromadný
bodem	bod	k1gInSc7	bod
množiny	množina	k1gFnSc2	množina
M	M	kA	M
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
∩	∩	k?	∩
M	M	kA	M
≠	≠	k?	≠
∅	∅	k?	∅
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cap	cap	k1gMnSc1	cap
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
\	\	kIx~	\
<g/>
varnothing	varnothing	k1gInSc1	varnothing
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
hromadných	hromadný	k2eAgInPc2d1	hromadný
bodů	bod	k1gInPc2	bod
množiny	množina	k1gFnSc2	množina
M	M	kA	M
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
derivace	derivace	k1gFnSc1	derivace
množiny	množina	k1gFnSc2	množina
M	M	kA	M
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
M	M	kA	M
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
∖	∖	k?	∖
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
setminus	setminus	k1gMnSc1	setminus
M	M	kA	M
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
taky	taky	k6eAd1	taky
jestliže	jestliže	k8xS	jestliže
všechny	všechen	k3xTgInPc1	všechen
hromadné	hromadný	k2eAgInPc1d1	hromadný
body	bod	k1gInPc1	bod
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
M	M	kA	M
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uzávěrem	uzávěr	k1gInSc7	uzávěr
množiny	množina	k1gFnSc2	množina
M	M	kA	M
rozumíme	rozumět	k5eAaImIp1nP	rozumět
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
{	{	kIx(	{
x	x	k?	x
∈	∈	k?	∈
X	X	kA	X
;	;	kIx,	;
∀	∀	k?	∀
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
∩	∩	k?	∩
M	M	kA	M
≠	≠	k?	≠
∅	∅	k?	∅
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cap	cap	k1gMnSc1	cap
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
\	\	kIx~	\
<g/>
varnothing	varnothing	k1gInSc1	varnothing
\	\	kIx~	\
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
bod	bod	k1gInSc1	bod
x	x	k?	x
je	být	k5eAaImIp3nS	být
hraničním	hraniční	k2eAgInSc7d1	hraniční
bodem	bod	k1gInSc7	bod
množiny	množina	k1gFnSc2	množina
M	M	kA	M
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
ε	ε	k?	ε
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
∩	∩	k?	∩
M	M	kA	M
≠	≠	k?	≠
∅	∅	k?	∅
)	)	kIx)	)
∧	∧	k?	∧
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
ε	ε	k?	ε
)	)	kIx)	)
∩	∩	k?	∩
(	(	kIx(	(
X	X	kA	X
∖	∖	k?	∖
M	M	kA	M
)	)	kIx)	)
≠	≠	k?	≠
∅	∅	k?	∅
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cap	cap	k1gMnSc1	cap
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
varnothing	varnothing	k1gInSc1	varnothing
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
land	land	k6eAd1	land
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cap	cap	k1gMnSc1	cap
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
setminus	setminus	k1gMnSc1	setminus
M	M	kA	M
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
\	\	kIx~	\
<g/>
varnothing	varnothing	k1gInSc1	varnothing
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgInPc2	všecek
hraničních	hraniční	k2eAgInPc2d1	hraniční
bodů	bod	k1gInPc2	bod
nazýváme	nazývat	k5eAaImIp1nP	nazývat
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
značíme	značit	k5eAaImIp1nP	značit
ji	on	k3xPp3gFnSc4	on
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gNnPc6	delta
M	M	kA	M
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
uzávěru	uzávěr	k1gInSc2	uzávěr
množiny	množina	k1gFnSc2	množina
i	i	k8xC	i
do	do	k7c2	do
uzávěru	uzávěr	k1gInSc2	uzávěr
doplňku	doplněk	k1gInSc2	doplněk
množiny	množina	k1gFnSc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
M	M	kA	M
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
v	v	k7c6	v
X	X	kA	X
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
bodu	bod	k1gInSc2	bod
x	x	k?	x
od	od	k7c2	od
množiny	množina	k1gFnSc2	množina
M	M	kA	M
definujeme	definovat	k5eAaBmIp1nP	definovat
předpisem	předpis	k1gInSc7	předpis
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
i	i	k8xC	i
s	s	k7c7	s
t	t	k?	t
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
M	M	kA	M
)	)	kIx)	)
:	:	kIx,	:
<g/>
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
inf	inf	k?	inf
:	:	kIx,	:
z	z	k7c2	z
∈	∈	k?	∈
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
dist	dist	k2eAgInSc4d1	dist
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
inf	inf	k?	inf
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
M	M	kA	M
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
inf	inf	k?	inf
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
inf	inf	k?	inf
}	}	kIx)	}
znační	značný	k2eAgMnPc1d1	značný
infimum	infimum	k1gInSc4	infimum
<g/>
.	.	kIx.	.
</s>
<s>
Diametrem	diametr	k1gInSc7	diametr
(	(	kIx(	(
<g/>
průměrem	průměr	k1gInSc7	průměr
<g/>
)	)	kIx)	)
množiny	množina	k1gFnSc2	množina
M	M	kA	M
rozumíme	rozumět	k5eAaImIp1nP	rozumět
číslo	číslo	k1gNnSc4	číslo
definované	definovaný	k2eAgFnSc2d1	definovaná
předpisem	předpis	k1gInSc7	předpis
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
i	i	k8xC	i
a	a	k8xC	a
m	m	kA	m
M	M	kA	M
=	=	kIx~	=
f	f	k?	f
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
pokud	pokud	k8xS	pokud
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
∅	∅	k?	∅
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
∈	∈	k?	∈
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
pokud	pokud	k8xS	pokud
:	:	kIx,	:
M	M	kA	M
≠	≠	k?	≠
∅	∅	k?	∅
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
diamM	diamM	k?	diamM
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gMnSc1	begin
<g/>
{	{	kIx(	{
<g/>
cases	cases	k1gMnSc1	cases
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
pokud	pokud	k8xS	pokud
}}	}}	k?	}}
<g/>
M	M	kA	M
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varnothing	varnothing	k1gInSc1	varnothing
,	,	kIx,	,
<g/>
\\	\\	k?	\\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sup	sup	k1gMnSc1	sup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
M	M	kA	M
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
pokud	pokud	k8xS	pokud
}}	}}	k?	}}
<g/>
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
\	\	kIx~	\
<g/>
varnothing	varnothing	k1gInSc1	varnothing
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
cases	cases	k1gMnSc1	cases
<g/>
}}}	}}}	k?	}}}
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sup	supit	k5eAaImRp2nS	supit
}	}	kIx)	}
značí	značit	k5eAaImIp3nS	značit
supremum	supremum	k1gInSc4	supremum
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
M	M	kA	M
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
K	K	kA	K
:	:	kIx,	:
d	d	k?	d
i	i	k8xC	i
a	a	k8xC	a
m	m	kA	m
M	M	kA	M
<	<	kIx(	<
K	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	exists	k1gInSc4	exists
K	k	k7c3	k
<g/>
:	:	kIx,	:
<g/>
diamM	diamM	k?	diamM
<g/>
<	<	kIx(	<
<g/>
K	K	kA	K
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
s	s	k7c7	s
eukleidovskou	eukleidovský	k2eAgFnSc7d1	eukleidovská
normou	norma	k1gFnSc7	norma
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0,1	[number]	k4	0,1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
M	M	kA	M
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
⟨	⟨	k?	⟨
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
⟩	⟩	k?	⟩
,	,	kIx,	,
δ	δ	k?	δ
M	M	kA	M
=	=	kIx~	=
{	{	kIx(	{
0	[number]	k4	0
;	;	kIx,	;
1	[number]	k4	1
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc2	rangle
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
,	,	kIx,	,
omezená	omezený	k2eAgFnSc1d1	omezená
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
⟨	⟨	k?	⟨
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
:	:	kIx,	:
není	být	k5eNaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
⟨	⟨	k?	⟨
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
⟩	⟩	k?	⟩
,	,	kIx,	,
δ	δ	k?	δ
M	M	kA	M
=	=	kIx~	=
{	{	kIx(	{
0	[number]	k4	0
;	;	kIx,	;
1	[number]	k4	1
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
M	M	kA	M
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc2	rangle
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
,	,	kIx,	,
omezená	omezený	k2eAgFnSc1d1	omezená
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bigl	bigl	k1gInSc1	bigl
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
0,1	[number]	k4	0,1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
:	:	kIx,	:
není	být	k5eNaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
ani	ani	k8xC	ani
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
⟨	⟨	k?	⟨
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
⟩	⟩	k?	⟩
,	,	kIx,	,
δ	δ	k?	δ
M	M	kA	M
=	=	kIx~	=
{	{	kIx(	{
0	[number]	k4	0
;	;	kIx,	;
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc1	rangle
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
,	,	kIx,	,
omezená	omezený	k2eAgFnSc1d1	omezená
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
(	(	kIx(	(
−	−	k?	−
∞	∞	k?	∞
,	,	kIx,	,
∞	∞	k?	∞
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
infty	inft	k1gMnPc7	inft
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
i	i	k8xC	i
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
M	M	kA	M
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
M	M	kA	M
,	,	kIx,	,
δ	δ	k?	δ
M	M	kA	M
=	=	kIx~	=
{	{	kIx(	{
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
neomezená	omezený	k2eNgFnSc5d1	neomezená
Mějme	mít	k5eAaImRp1nP	mít
na	na	k7c6	na
neprázdné	prázdný	k2eNgFnSc6d1	neprázdná
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
dvě	dva	k4xCgFnPc4	dva
libovolné	libovolný	k2eAgFnPc4d1	libovolná
metriky	metrika	k1gFnPc4	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
výroky	výrok	k1gInPc1	výrok
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgInPc1d1	ekvivalentní
<g/>
:	:	kIx,	:
každá	každý	k3xTgFnSc1	každý
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
také	také	k9	také
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
každá	každý	k3xTgFnSc1	každý
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
také	také	k9	také
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
c	c	k0	c
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
cl	cl	k?	cl
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gInSc1	subset
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
cl	cl	k?	cl
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
cl	cl	k?	cl
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
značí	značit	k5eAaImIp3nS	značit
uzávěr	uzávěr	k1gInSc1	uzávěr
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
metrice	metrika	k1gFnSc3	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
.	.	kIx.	.
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k8xC	i
n	n	k0	n
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
:	:	kIx,	:
i	i	k8xC	i
n	n	k0	n
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
int	int	k?	int
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gInSc1	subset
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
int	int	k?	int
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k8xC	i
n	n	k0	n
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
int	int	k?	int
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
značí	značit	k5eAaImIp3nS	značit
vnitřek	vnitřek	k1gInSc1	vnitřek
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
metrice	metrika	k1gFnSc3	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
.	.	kIx.	.
každé	každý	k3xTgNnSc1	každý
okolí	okolí	k1gNnSc1	okolí
bodu	bod	k1gInSc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
}	}	kIx)	}
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
okolím	okolí	k1gNnSc7	okolí
také	také	k6eAd1	také
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
identické	identický	k2eAgNnSc1d1	identické
zobrazení	zobrazení	k1gNnSc1	zobrazení
metrického	metrický	k2eAgInSc2d1	metrický
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
spojité	spojitý	k2eAgNnSc1d1	spojité
<g/>
.	.	kIx.	.
každá	každý	k3xTgFnSc1	každý
posloupnost	posloupnost	k1gFnSc1	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
metrickém	metrický	k2eAgInSc6d1	metrický
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
k	k	k7c3	k
x	x	k?	x
<g/>
,	,	kIx,	,
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
ke	k	k7c3	k
stejné	stejný	k2eAgFnSc3d1	stejná
limitě	limita	k1gFnSc3	limita
také	také	k9	také
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgNnPc1d1	uvedené
tvrzení	tvrzení	k1gNnPc1	tvrzení
definují	definovat	k5eAaBmIp3nP	definovat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
metrikami	metrika	k1gFnPc7	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
přitom	přitom	k6eAd1	přitom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
≠	≠	k?	≠
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
pak	pak	k6eAd1	pak
o	o	k7c6	o
takto	takto	k6eAd1	takto
definovaných	definovaný	k2eAgFnPc6d1	definovaná
metrikách	metrika	k1gFnPc6	metrika
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
metrikách	metrika	k1gFnPc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
každá	každý	k3xTgFnSc1	každý
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
metriky	metrika	k1gFnPc1	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
cl	cl	k?	cl
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
X	X	kA	X
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
cl	cl	k?	cl
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
cl	cl	k?	cl
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
uzávěr	uzávěr	k1gInSc1	uzávěr
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
jsou	být	k5eAaImIp3nP	být
metriky	metrika	k1gFnPc4	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
⊂	⊂	k?	⊂
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
}	}	kIx)	}
také	také	k9	také
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
i	i	k8xC	i
n	n	k0	n
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
int	int	k?	int
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
int	int	k?	int
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k8xC	i
n	n	k0	n
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
int	int	k?	int
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vnitřek	vnitřek	k1gInSc1	vnitřek
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
M	M	kA	M
je	být	k5eAaImIp3nS	být
totálně	totálně	k6eAd1	totálně
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
kladné	kladný	k2eAgNnSc4d1	kladné
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnPc3	epsilon
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
existuje	existovat	k5eAaImIp3nS	existovat
konečná	konečný	k2eAgFnSc1d1	konečná
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
⊆	⊆	k?	⊆
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
M	M	kA	M
<g/>
}	}	kIx)	}
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
prvek	prvek	k1gInSc1	prvek
M	M	kA	M
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
prvku	prvek	k1gInSc3	prvek
S	s	k7c7	s
blíže	blízce	k6eAd2	blízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnPc3	epsilon
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množině	množina	k1gFnSc3	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnPc6	epsilon
}	}	kIx)	}
-síť	íť	k1gFnSc4	-síť
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
M	M	kA	M
je	být	k5eAaImIp3nS	být
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
kladné	kladný	k2eAgNnSc1d1	kladné
číslo	číslo	k1gNnSc1	číslo
K	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
libovolné	libovolný	k2eAgFnSc2d1	libovolná
dvojice	dvojice	k1gFnSc2	dvojice
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
K.	K.	kA	K.
Konvergence	konvergence	k1gFnSc1	konvergence
posloupnosti	posloupnost	k1gFnSc2	posloupnost
a	a	k8xC	a
spojitost	spojitost	k1gFnSc1	spojitost
zobrazení	zobrazení	k1gNnSc2	zobrazení
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
analogicky	analogicky	k6eAd1	analogicky
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
na	na	k7c6	na
reálných	reálný	k2eAgNnPc6d1	reálné
číslech	číslo	k1gNnPc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
množina	množina	k1gFnSc1	množina
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
každého	každý	k3xTgNnSc2	každý
pokrytí	pokrytí	k1gNnSc1	pokrytí
otevřenými	otevřený	k2eAgFnPc7d1	otevřená
množinami	množina	k1gFnPc7	množina
lze	lze	k6eAd1	lze
vybrat	vybrat	k5eAaPmF	vybrat
konečné	konečný	k2eAgNnSc4d1	konečné
pokrytí	pokrytí	k1gNnSc4	pokrytí
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
podprostor	podprostor	k1gInSc1	podprostor
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
reálných	reálný	k2eAgInPc6d1	reálný
číslech	číslo	k1gNnPc6	číslo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
prostor	prostor	k1gInSc1	prostor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
vůči	vůči	k7c3	vůči
některým	některý	k3yIgMnPc3	některý
svým	svůj	k3xOyFgMnPc3	svůj
nadprostorům	nadprostor	k1gMnPc3	nadprostor
a	a	k8xC	a
otevřený	otevřený	k2eAgInSc4d1	otevřený
vůči	vůči	k7c3	vůči
jiným	jiný	k2eAgFnPc3d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
pak	pak	k9	pak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
absolutně	absolutně	k6eAd1	absolutně
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
Úplný	úplný	k2eAgInSc1d1	úplný
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
metrický	metrický	k2eAgInSc4d1	metrický
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
každá	každý	k3xTgFnSc1	každý
cauchyovská	cauchyovský	k2eAgFnSc1d1	cauchyovský
posloupnost	posloupnost	k1gFnSc1	posloupnost
je	být	k5eAaImIp3nS	být
konvergentní	konvergentní	k2eAgFnSc1d1	konvergentní
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
absolutně	absolutně	k6eAd1	absolutně
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
.	.	kIx.	.
</s>
<s>
Vynecháním	vynechání	k1gNnSc7	vynechání
podmínky	podmínka	k1gFnSc2	podmínka
symetrie	symetrie	k1gFnSc2	symetrie
se	se	k3xPyFc4	se
definice	definice	k1gFnSc1	definice
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
definici	definice	k1gFnSc4	definice
kvazimetrického	kvazimetrický	k2eAgInSc2d1	kvazimetrický
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
povolením	povolení	k1gNnSc7	povolení
nulové	nulový	k2eAgFnSc2d1	nulová
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
body	bod	k1gInPc4	bod
se	se	k3xPyFc4	se
definice	definice	k1gFnPc1	definice
mění	měnit	k5eAaImIp3nP	měnit
na	na	k7c4	na
definici	definice	k1gFnSc4	definice
pseudometrického	pseudometrický	k2eAgInSc2d1	pseudometrický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
topologický	topologický	k2eAgInSc4d1	topologický
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obecná	obecný	k2eAgFnSc1d1	obecná
struktura	struktura	k1gFnSc1	struktura
umožňující	umožňující	k2eAgFnSc1d1	umožňující
pracovat	pracovat	k5eAaImF	pracovat
jednotně	jednotně	k6eAd1	jednotně
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
množin	množina	k1gFnPc2	množina
(	(	kIx(	(
<g/>
množiny	množina	k1gFnPc4	množina
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
množiny	množina	k1gFnSc2	množina
funkcí	funkce	k1gFnPc2	funkce
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
mnohé	mnohý	k2eAgInPc4d1	mnohý
pojmy	pojem	k1gInPc4	pojem
z	z	k7c2	z
metrických	metrický	k2eAgInPc2d1	metrický
prostorů	prostor	k1gInPc2	prostor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
množina	množina	k1gFnSc1	množina
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
spojité	spojitý	k2eAgNnSc1d1	spojité
zobrazení	zobrazení	k1gNnSc1	zobrazení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
definovat	definovat	k5eAaBmF	definovat
ještě	ještě	k9	ještě
podstatně	podstatně	k6eAd1	podstatně
obecněji	obecně	k6eAd2	obecně
v	v	k7c6	v
pojmu	pojem	k1gInSc6	pojem
topologický	topologický	k2eAgInSc4d1	topologický
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
metrický	metrický	k2eAgInSc1d1	metrický
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
topologickým	topologický	k2eAgInSc7d1	topologický
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nikoli	nikoli	k9	nikoli
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
Topologické	topologický	k2eAgFnPc1d1	topologická
prostory	prostora	k1gFnPc1	prostora
tedy	tedy	k9	tedy
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
studovat	studovat	k5eAaImF	studovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
ještě	ještě	k6eAd1	ještě
širší	široký	k2eAgFnSc2d2	širší
skupiny	skupina	k1gFnSc2	skupina
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
než	než	k8xS	než
metrické	metrický	k2eAgFnPc4d1	metrická
prostory	prostora	k1gFnPc4	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
oblast	oblast	k1gFnSc1	oblast
matematiky	matematika	k1gFnSc2	matematika
zvaná	zvaný	k2eAgFnSc1d1	zvaná
topologie	topologie	k1gFnSc1	topologie
<g/>
.	.	kIx.	.
</s>
