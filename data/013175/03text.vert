<p>
<s>
Güemesův	Güemesův	k2eAgInSc1d1	Güemesův
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
ostrov	ostrov	k1gInSc1	ostrov
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Skagit	Skagita	k1gFnPc2	Skagita
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
Fidalgova	Fidalgův	k2eAgInSc2d1	Fidalgův
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
postaveno	postaven	k2eAgNnSc1d1	postaveno
město	město	k1gNnSc1	město
Anacortes	Anacortesa	k1gFnPc2	Anacortesa
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
přístupný	přístupný	k2eAgInSc1d1	přístupný
pomocí	pomocí	k7c2	pomocí
trajektu	trajekt	k1gInSc2	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
ostrov	ostrov	k1gInSc4	ostrov
nese	nést	k5eAaImIp3nS	nést
po	po	k7c6	po
místokráli	místokrál	k1gMnSc6	místokrál
Nového	Nového	k2eAgNnSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Juanu	Juan	k1gMnSc3	Juan
Vicentu	Vicent	k1gMnSc3	Vicent
de	de	k?	de
Güemesovi	Güemes	k1gMnSc6	Güemes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
vyslal	vyslat	k5eAaPmAgMnS	vyslat
expedici	expedice	k1gFnSc4	expedice
právě	právě	k9	právě
k	k	k7c3	k
souostroví	souostroví	k1gNnSc3	souostroví
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
je	být	k5eAaImIp3nS	být
Güemesův	Güemesův	k2eAgInSc4d1	Güemesův
ostrov	ostrov	k1gInSc4	ostrov
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
má	mít	k5eAaImIp3nS	mít
venkovský	venkovský	k2eAgInSc1d1	venkovský
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
omezené	omezený	k2eAgNnSc1d1	omezené
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
obchody	obchod	k1gInPc4	obchod
<g/>
:	:	kIx,	:
jedním	jeden	k4xCgInSc7	jeden
je	být	k5eAaImIp3nS	být
Andersonův	Andersonův	k2eAgInSc4d1	Andersonův
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
provozuje	provozovat	k5eAaImIp3nS	provozovat
místní	místní	k2eAgNnSc4d1	místní
letovisko	letovisko	k1gNnSc4	letovisko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
místních	místní	k2eAgFnPc6d1	místní
plážích	pláž	k1gFnPc6	pláž
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
dvou	dva	k4xCgNnPc2	dva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
s	s	k7c7	s
lodní	lodní	k2eAgFnSc7d1	lodní
rampou	rampa	k1gFnSc7	rampa
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
dva	dva	k4xCgInPc4	dva
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Školní	školní	k2eAgInSc1d1	školní
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
uprostřed	uprostřed	k7c2	uprostřed
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
Youngův	Youngův	k2eAgInSc1d1	Youngův
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
nedaleko	nedaleko	k7c2	nedaleko
již	již	k6eAd1	již
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
letoviska	letovisko	k1gNnSc2	letovisko
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
obec	obec	k1gFnSc1	obec
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
Guemes	Guemes	k1gInSc1	Guemes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
José	José	k1gNnSc7	José
María	Maríus	k1gMnSc2	Maríus
Narváezem	Narváez	k1gInSc7	Narváez
jako	jako	k8xS	jako
Isla	Isla	k1gMnSc1	Isla
de	de	k?	de
Güemes	Güemes	k1gMnSc1	Güemes
při	při	k7c6	při
expedici	expedice	k1gFnSc6	expedice
Francisca	Franciscum	k1gNnSc2	Franciscum
de	de	k?	de
Elizy	Eliz	k1gInPc1	Eliz
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
po	po	k7c6	po
místokráli	místokrál	k1gMnSc6	místokrál
Nového	Nového	k2eAgInSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
Juanu	Juan	k1gMnSc3	Juan
Vicentu	Vicent	k1gMnSc3	Vicent
de	de	k?	de
Güemesovi	Güemes	k1gMnSc3	Güemes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
sem	sem	k6eAd1	sem
zavítal	zavítat	k5eAaPmAgMnS	zavítat
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
expedici	expedice	k1gFnSc6	expedice
Charles	Charles	k1gMnSc1	Charles
Wilkes	Wilkes	k1gMnSc1	Wilkes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
překřtil	překřtít	k5eAaPmAgMnS	překřtít
na	na	k7c4	na
Lawrencův	Lawrencův	k2eAgInSc4d1	Lawrencův
ostrov	ostrov	k1gInSc4	ostrov
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
námořního	námořní	k2eAgMnSc4d1	námořní
důstojníka	důstojník	k1gMnSc4	důstojník
Jamese	Jamese	k1gFnSc2	Jamese
Lawrence	Lawrence	k1gFnSc2	Lawrence
<g/>
.	.	kIx.	.
</s>
<s>
Wilkes	Wilkes	k1gMnSc1	Wilkes
rovněž	rovněž	k9	rovněž
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
Güemesův	Güemesův	k2eAgInSc4d1	Güemesův
průliv	průliv	k1gInSc4	průliv
v	v	k7c6	v
památce	památka	k1gFnSc6	památka
na	na	k7c4	na
USS	USS	kA	USS
Hornet	Hornet	k1gInSc4	Hornet
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
James	James	k1gMnSc1	James
Lawrence	Lawrenec	k1gInSc2	Lawrenec
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
Britsko-americké	britskomerický	k2eAgFnSc6d1	britsko-americká
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Bellinghamova	Bellinghamův	k2eAgInSc2d1	Bellinghamův
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
severně	severně	k6eAd1	severně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
pak	pak	k6eAd1	pak
překřtil	překřtít	k5eAaPmAgMnS	překřtít
po	po	k7c6	po
lodi	loď	k1gFnSc6	loď
HMS	HMS	kA	HMS
Penguin	Penguin	k1gInSc1	Penguin
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Lawrence	Lawrence	k1gFnSc1	Lawrence
zajal	zajmout	k5eAaPmAgInS	zajmout
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
bitev	bitva	k1gFnPc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
však	však	k9	však
tato	tento	k3xDgNnPc4	tento
jména	jméno	k1gNnPc4	jméno
z	z	k7c2	z
map	mapa	k1gFnPc2	mapa
opět	opět	k6eAd1	opět
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
,	,	kIx,	,
když	když	k8xS	když
britský	britský	k2eAgMnSc1d1	britský
kapitán	kapitán	k1gMnSc1	kapitán
Henry	Henry	k1gMnSc1	Henry
Kellett	Kellett	k1gMnSc1	Kellett
přeorganizovával	přeorganizovávat	k5eAaImAgMnS	přeorganizovávat
námořní	námořní	k2eAgFnSc2d1	námořní
mapy	mapa	k1gFnSc2	mapa
britské	britský	k2eAgFnSc2d1	britská
admirality	admiralita	k1gFnSc2	admiralita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
nahrazoval	nahrazovat	k5eAaImAgInS	nahrazovat
americká	americký	k2eAgNnPc4d1	americké
jména	jméno	k1gNnPc4	jméno
těmi	ten	k3xDgMnPc7	ten
původními	původní	k2eAgMnPc7d1	původní
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
buď	buď	k8xC	buď
britskými	britský	k2eAgFnPc7d1	britská
nebo	nebo	k8xC	nebo
španělskými	španělský	k2eAgFnPc7d1	španělská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
obecně	obecně	k6eAd1	obecně
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
Psí	psí	k2eAgInSc1d1	psí
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
početná	početný	k2eAgFnSc1d1	početná
populace	populace	k1gFnSc1	populace
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
disturbance	disturbance	k1gFnSc1	disturbance
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
námořních	námořní	k2eAgInPc2d1	námořní
a	a	k8xC	a
leteckých	letecký	k2eAgInPc2d1	letecký
záznamů	záznam	k1gInPc2	záznam
se	se	k3xPyFc4	se
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
nachází	nacházet	k5eAaImIp3nS	nacházet
magnetická	magnetický	k2eAgFnSc1d1	magnetická
disturbance	disturbance	k1gFnSc1	disturbance
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
kompasy	kompas	k1gInPc1	kompas
polohu	poloh	k1gInSc2	poloh
až	až	k9	až
o	o	k7c4	o
14	[number]	k4	14
stupňů	stupeň	k1gInPc2	stupeň
chybnou	chybný	k2eAgFnSc7d1	chybná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Guemes	Guemes	k1gInSc1	Guemes
Island	Island	k1gInSc1	Island
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
