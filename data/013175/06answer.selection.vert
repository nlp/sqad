<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
ostrov	ostrov	k1gInSc4	ostrov
nese	nést	k5eAaImIp3nS	nést
po	po	k7c6	po
místokráli	místokrál	k1gMnSc6	místokrál
Nového	Nového	k2eAgNnSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Juanu	Juan	k1gMnSc3	Juan
Vicentu	Vicent	k1gMnSc3	Vicent
de	de	k?	de
Güemesovi	Güemes	k1gMnSc6	Güemes
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
vyslal	vyslat	k5eAaPmAgMnS	vyslat
expedici	expedice	k1gFnSc4	expedice
právě	právě	k9	právě
k	k	k7c3	k
souostroví	souostroví	k1gNnSc3	souostroví
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
je	být	k5eAaImIp3nS	být
Güemesův	Güemesův	k2eAgInSc4d1	Güemesův
ostrov	ostrov	k1gInSc4	ostrov
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
