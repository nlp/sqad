<s>
Bellegardové	Bellegardový	k2eAgNnSc1d1	Bellegardový
je	být	k5eAaImIp3nS	být
hraběcí	hraběcí	k2eAgInSc4d1	hraběcí
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Savojsku	Savojsko	k1gNnSc6	Savojsko
<g/>
.	.	kIx.	.
</s>
