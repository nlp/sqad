<p>
<s>
Bellegardové	Bellegardový	k2eAgNnSc1d1	Bellegardový
je	být	k5eAaImIp3nS	být
hraběcí	hraběcí	k2eAgInSc4d1	hraběcí
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Savojsku	Savojsko	k1gNnSc6	Savojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
doloženým	doložený	k2eAgInSc7d1	doložený
členem	člen	k1gInSc7	člen
rodu	rod	k1gInSc2	rod
byl	být	k5eAaImAgInS	být
Noyel	Noyel	k1gInSc1	Noyel
de	de	k?	de
Bellegarde	Bellegard	k1gInSc5	Bellegard
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1540	[number]	k4	1540
obdržel	obdržet	k5eAaPmAgInS	obdržet
François	François	k1gInSc1	François
de	de	k?	de
Bellegarde	Bellegard	k1gMnSc5	Bellegard
(	(	kIx(	(
<g/>
Franz	Franza	k1gFnPc2	Franza
von	von	k1gInSc1	von
Bellegarde	Bellegard	k1gMnSc5	Bellegard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
v	v	k7c6	v
Nizze	Nizza	k1gFnSc6	Nizza
<g/>
,	,	kIx,	,
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
hraběcí	hraběcí	k2eAgInSc1d1	hraběcí
titul	titul	k1gInSc1	titul
a	a	k8xC	a
rodový	rodový	k2eAgInSc1d1	rodový
erb	erb	k1gInSc1	erb
s	s	k7c7	s
říšským	říšský	k2eAgMnSc7d1	říšský
orlem	orel	k1gMnSc7	orel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vévody	vévoda	k1gMnSc2	vévoda
Victora	Victor	k1gMnSc2	Victor
Amadea	Amadeus	k1gMnSc2	Amadeus
Savojského	savojský	k2eAgMnSc2d1	savojský
později	pozdě	k6eAd2	pozdě
rod	rod	k1gInSc1	rod
obdržel	obdržet	k5eAaPmAgInS	obdržet
titul	titul	k1gInSc4	titul
Marquis	Marquis	k1gFnSc2	Marquis
des	des	k1gNnSc2	des
Marches	Marches	k1gMnSc1	Marches
et	et	k?	et
comte	comte	k5eAaPmIp2nP	comte
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Antremont	Antremont	k1gInSc1	Antremont
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
byl	být	k5eAaImAgMnS	být
Johann	Johann	k1gMnSc1	Johann
Franz	Franz	k1gMnSc1	Franz
von	von	k1gInSc4	von
Bellegarde	Bellegard	k1gMnSc5	Bellegard
(	(	kIx(	(
<g/>
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
říšským	říšský	k2eAgMnSc7d1	říšský
hrabětem	hrabě	k1gMnSc7	hrabě
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
udělen	udělen	k2eAgInSc4d1	udělen
inkolát	inkolát	k1gInSc4	inkolát
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
se	se	k3xPyFc4	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
rodové	rodový	k2eAgFnPc4d1	rodová
větve	větev	k1gFnPc4	větev
<g/>
,	,	kIx,	,
štýrskou	štýrský	k2eAgFnSc4d1	štýrská
a	a	k8xC	a
rakousko-slezskou	rakouskolezský	k2eAgFnSc4d1	rakousko-slezský
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rakousko-slezská	rakouskolezský	k2eAgFnSc1d1	rakousko-slezský
větev	větev	k1gFnSc1	větev
si	se	k3xPyFc3	se
za	za	k7c4	za
sídlo	sídlo	k1gNnSc4	sídlo
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Velké	velký	k2eAgFnPc4d1	velká
Heraltice	Heraltice	k1gFnPc4	Heraltice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
si	se	k3xPyFc3	se
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
dcera	dcera	k1gFnSc1	dcera
poslední	poslední	k2eAgFnSc2d1	poslední
majitelky	majitelka	k1gFnSc2	majitelka
velkoheraltického	velkoheraltický	k2eAgNnSc2d1	velkoheraltický
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
kněžny	kněžna	k1gFnSc2	kněžna
Vilemíny	Vilemína	k1gFnSc2	Vilemína
Kinské	Kinská	k1gFnSc2	Kinská
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
Rudolfina	Rudolfin	k2eAgNnSc2d1	Rudolfin
Karolina	Karolinum	k1gNnSc2	Karolinum
Kinská	Kinská	k1gFnSc1	Kinská
<g/>
,	,	kIx,	,
vzala	vzít	k5eAaPmAgFnS	vzít
hraběte	hrabě	k1gMnSc4	hrabě
Franze	Franze	k1gFnSc2	Franze
Alexandra	Alexandr	k1gMnSc4	Alexandr
Heinricha	Heinrich	k1gMnSc4	Heinrich
Ernsta	Ernst	k1gMnSc4	Ernst
von	von	k1gInSc1	von
Bellegarda	Bellegard	k1gMnSc4	Bellegard
<g/>
,	,	kIx,	,
rytíře	rytíř	k1gMnSc4	rytíř
maltézského	maltézský	k2eAgInSc2d1	maltézský
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
panství	panství	k1gNnSc2	panství
byly	být	k5eAaImAgFnP	být
obce	obec	k1gFnPc1	obec
Bohdanovice	Bohdanovice	k1gFnSc2	Bohdanovice
<g/>
,	,	kIx,	,
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Heřmanice	Heřmanice	k1gFnSc2	Heřmanice
<g/>
,	,	kIx,	,
Jakartovice	Jakartovice	k1gFnSc2	Jakartovice
<g/>
,	,	kIx,	,
Sádek	sádka	k1gFnPc2	sádka
<g/>
,	,	kIx,	,
Košetice	Košetika	k1gFnSc6	Košetika
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Životice	Životice	k1gFnSc1	Životice
<g/>
,	,	kIx,	,
Slezská	slezský	k2eAgFnSc1d1	Slezská
Harta	Harta	k1gFnSc1	Harta
a	a	k8xC	a
Staré	Staré	k2eAgFnPc1d1	Staré
Heřminovy	Heřminův	k2eAgFnPc1d1	Heřminův
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
syna	syn	k1gMnSc4	syn
Augusta	August	k1gMnSc4	August
Máriu	Márius	k1gMnSc6	Márius
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Emanuele	Emanuela	k1gFnSc3	Emanuela
Franze	Franza	k1gFnSc6	Franza
von	von	k1gInSc4	von
Bellegarda	Bellegard	k1gMnSc2	Bellegard
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Rudolfiny	Rudolfina	k1gFnSc2	Rudolfina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
zdědil	zdědit	k5eAaPmAgInS	zdědit
panství	panství	k1gNnSc4	panství
nejprve	nejprve	k6eAd1	nejprve
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
bylo	být	k5eAaImAgNnS	být
přepsáno	přepsat	k5eAaPmNgNnS	přepsat
na	na	k7c4	na
Augusta	August	k1gMnSc4	August
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
pak	pak	k6eAd1	pak
panství	panství	k1gNnSc6	panství
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
dcery	dcera	k1gFnPc4	dcera
Arnoštku	Arnoštek	k1gInSc2	Arnoštek
<g/>
,	,	kIx,	,
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
Rudolfinu	Rudolfina	k1gFnSc4	Rudolfina
<g/>
,	,	kIx,	,
Alžbětu	Alžběta	k1gFnSc4	Alžběta
a	a	k8xC	a
Žofii	Žofie	k1gFnSc4	Žofie
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Erb	erb	k1gInSc4	erb
==	==	k?	==
</s>
</p>
<p>
<s>
Štít	štít	k1gInSc1	štít
erbu	erb	k1gInSc2	erb
rodu	rod	k1gInSc2	rod
byl	být	k5eAaImAgMnS	být
dělený	dělený	k2eAgMnSc1d1	dělený
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
poli	pole	k1gNnSc6	pole
umístěn	umístěn	k2eAgMnSc1d1	umístěn
říšský	říšský	k2eAgMnSc1d1	říšský
orel	orel	k1gMnSc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
polovině	polovina	k1gFnSc6	polovina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
nacházel	nacházet	k5eAaImAgMnS	nacházet
zlatý	zlatý	k2eAgInSc4d1	zlatý
plamenný	plamenný	k2eAgInSc4d1	plamenný
oblouk	oblouk	k1gInSc4	oblouk
obrácený	obrácený	k2eAgInSc4d1	obrácený
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
pět	pět	k4xCc1	pět
šlehajících	šlehající	k2eAgInPc2d1	šlehající
zlatých	zlatý	k2eAgInPc2d1	zlatý
plamenů	plamen	k1gInPc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klenotu	klenot	k1gInSc6	klenot
<g/>
,	,	kIx,	,
na	na	k7c6	na
helmové	helmový	k2eAgFnSc6d1	Helmová
koruně	koruna	k1gFnSc6	koruna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
vzlétající	vzlétající	k2eAgFnSc1d1	vzlétající
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
holubice	holubice	k1gFnSc1	holubice
s	s	k7c7	s
palmovou	palmový	k2eAgFnSc7d1	Palmová
ratolestí	ratolest	k1gFnSc7	ratolest
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
erb	erb	k1gInSc1	erb
pak	pak	k6eAd1	pak
obklopovala	obklopovat	k5eAaImAgNnP	obklopovat
modro-zlatá	modrolatý	k2eAgNnPc1d1	modro-zlatý
přikryvadla	přikryvadlo	k1gNnPc1	přikryvadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
představitelé	představitel	k1gMnPc1	představitel
rodu	rod	k1gInSc2	rod
==	==	k?	==
</s>
</p>
<p>
<s>
François	François	k1gFnSc1	François
de	de	k?	de
Bellegarde	Bellegard	k1gMnSc5	Bellegard
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1540	[number]	k4	1540
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
hraběcí	hraběcí	k2eAgInSc1d1	hraběcí
titul	titul	k1gInSc1	titul
a	a	k8xC	a
rodový	rodový	k2eAgInSc1d1	rodový
erb	erb	k1gInSc1	erb
s	s	k7c7	s
říšským	říšský	k2eAgMnSc7d1	říšský
orlem	orel	k1gMnSc7	orel
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Bellegarde	Bellegard	k1gMnSc5	Bellegard
(	(	kIx(	(
<g/>
1756	[number]	k4	1756
<g/>
–	–	k?	–
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
–	–	k?	–
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Rakouského	rakouský	k2eAgInSc2d1	rakouský
císařství	císařství	k1gNnPc2	císařství
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
Karl	Karl	k1gMnSc1	Karl
Emanuel	Emanuel	k1gMnSc1	Emanuel
von	von	k1gInSc4	von
Bellegarde	Bellegard	k1gMnSc5	Bellegard
(	(	kIx(	(
<g/>
1795	[number]	k4	1795
<g/>
–	–	k?	–
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
–	–	k?	–
zakladatel	zakladatel	k1gMnSc1	zakladatel
slezské	slezský	k2eAgFnSc2d1	Slezská
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Bellegarde	Bellegard	k1gMnSc5	Bellegard
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
–	–	k?	–
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
–	–	k?	–
rakouský	rakouský	k2eAgMnSc1d1	rakouský
generál	generál	k1gMnSc1	generál
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Bellegarde	Bellegard	k1gMnSc5	Bellegard
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
–	–	k?	–
rakouský	rakouský	k2eAgMnSc1d1	rakouský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
za	za	k7c4	za
Bukovinu	Bukovina	k1gFnSc4	Bukovina
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
daroval	darovat	k5eAaPmAgMnS	darovat
August	August	k1gMnSc1	August
hrabě	hrabě	k1gMnSc1	hrabě
Bellegarde	Bellegard	k1gMnSc5	Bellegard
Slezskému	slezský	k2eAgNnSc3d1	Slezské
zemskému	zemský	k2eAgNnSc3d1	zemské
muzeu	muzeum	k1gNnSc3	muzeum
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
Epitaf	epitaf	k1gInSc4	epitaf
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Vachtla	Vachtla	k1gMnSc2	Vachtla
z	z	k7c2	z
Pantenova	Pantenův	k2eAgNnSc2d1	Pantenův
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
umístěný	umístěný	k2eAgInSc4d1	umístěný
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Neposkvrněného	poskvrněný	k2eNgNnSc2d1	neposkvrněné
početí	početí	k1gNnSc2	početí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Velkých	velký	k2eAgFnPc6d1	velká
Heralticích	Heraltice	k1gFnPc6	Heraltice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MAŠEK	Mašek	k1gMnSc1	Mašek
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
krev	krev	k1gFnSc1	krev
:	:	kIx,	:
minulost	minulost	k1gFnSc1	minulost
a	a	k8xC	a
přítomnost	přítomnost	k1gFnSc1	přítomnost
445	[number]	k4	445
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
upr	upr	k?	upr
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
330	[number]	k4	330
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
2041049	[number]	k4	2041049
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VALENTA	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
rodu	rod	k1gInSc2	rod
Kinských	Kinských	k2eAgInSc2d1	Kinských
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
..	..	k?	..
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
heslo	heslo	k1gNnSc1	heslo
Bellegarde	Bellegard	k1gMnSc5	Bellegard
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
687	[number]	k4	687
<g/>
-	-	kIx~	-
<g/>
688	[number]	k4	688
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bellegardové	Bellegardový	k2eAgFnPc4d1	Bellegardový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Gebauera	Gebauer	k1gMnSc2	Gebauer
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Opavy	Opava	k1gFnSc2	Opava
</s>
</p>
