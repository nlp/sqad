<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
izolační	izolační	k2eAgInSc1d1	izolační
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
teplota	teplota	k1gFnSc1	teplota
iglú	iglú	k1gNnSc2	iglú
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
i	i	k9	i
několika	několik	k4yIc2	několik
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
nad	nad	k7c7	nad
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
studený	studený	k2eAgInSc1d1	studený
vzduch	vzduch	k1gInSc1	vzduch
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
nepronikne	proniknout	k5eNaPmIp3nS	proniknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vchod	vchod	k1gInSc1	vchod
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
podlahy	podlaha	k1gFnSc2	podlaha
<g/>
.	.	kIx.	.
</s>
