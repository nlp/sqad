<s>
Iglú	iglú	k1gNnSc1	iglú
je	být	k5eAaImIp3nS	být
kopulovitý	kopulovitý	k2eAgInSc4d1	kopulovitý
přístřešek	přístřešek	k1gInSc4	přístřešek
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
ze	z	k7c2	z
sněhových	sněhový	k2eAgInPc2d1	sněhový
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Užívají	užívat	k5eAaImIp3nP	užívat
ho	on	k3xPp3gNnSc4	on
především	především	k9	především
kanadští	kanadský	k2eAgMnPc1d1	kanadský
Eskymáci	Eskymák	k1gMnPc1	Eskymák
jako	jako	k8xC	jako
provizorní	provizorní	k2eAgInSc4d1	provizorní
domov	domov	k1gInSc4	domov
v	v	k7c6	v
době	doba	k1gFnSc6	doba
arktické	arktický	k2eAgFnSc2d1	arktická
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Nežili	žít	k5eNaImAgMnP	žít
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
trvale	trvale	k6eAd1	trvale
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
myslí	mysl	k1gFnPc2	mysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přespávali	přespávat	k5eAaImAgMnP	přespávat
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
během	během	k7c2	během
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
výprav	výprava	k1gFnPc2	výprava
za	za	k7c7	za
lovem	lov	k1gInSc7	lov
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
iglú	iglú	k1gNnSc2	iglú
v	v	k7c6	v
inuitském	inuitský	k2eAgInSc6d1	inuitský
jazyce	jazyk	k1gInSc6	jazyk
označuje	označovat	k5eAaImIp3nS	označovat
dům	dům	k1gInSc4	dům
nebo	nebo	k8xC	nebo
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Iglú	iglú	k1gNnSc1	iglú
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
úzkou	úzký	k2eAgFnSc7d1	úzká
podlouhlou	podlouhlý	k2eAgFnSc7d1	podlouhlá
předsíní	předsíň	k1gFnSc7	předsíň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
přístupu	přístup	k1gInSc3	přístup
chladu	chlad	k1gInSc2	chlad
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc7d1	hlavní
obytnou	obytný	k2eAgFnSc7d1	obytná
místností	místnost	k1gFnSc7	místnost
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
vybavenou	vybavený	k2eAgFnSc4d1	vybavená
okny	okno	k1gNnPc7	okno
z	z	k7c2	z
tenkého	tenký	k2eAgInSc2d1	tenký
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
izolační	izolační	k2eAgInSc1d1	izolační
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
teplota	teplota	k1gFnSc1	teplota
iglú	iglú	k1gNnSc2	iglú
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
i	i	k9	i
několika	několik	k4yIc2	několik
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
nad	nad	k7c7	nad
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
studený	studený	k2eAgInSc1d1	studený
vzduch	vzduch	k1gInSc1	vzduch
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
nepronikne	proniknout	k5eNaPmIp3nS	proniknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vchod	vchod	k1gInSc1	vchod
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
podlahy	podlaha	k1gFnSc2	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Teplý	teplý	k2eAgInSc1d1	teplý
vzduch	vzduch	k1gInSc1	vzduch
stoupá	stoupat	k5eAaImIp3nS	stoupat
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
studený	studený	k2eAgMnSc1d1	studený
klesá	klesat	k5eAaImIp3nS	klesat
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Iglú	iglú	k1gNnSc2	iglú
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
