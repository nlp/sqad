<s>
Linsang	Linsang	k1gInSc1
skvrnitý	skvrnitý	k2eAgInSc1d1
</s>
<s>
Linsang	Linsang	k1gInSc1
skvrnitý	skvrnitý	k2eAgInSc4d1
Linsang	Linsang	k1gInSc4
skvrnitý	skvrnitý	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
šelmy	šelma	k1gFnPc1
(	(	kIx(
<g/>
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
asijští	asijský	k2eAgMnPc1d1
linsangové	linsang	k1gMnPc1
(	(	kIx(
<g/>
Prinodontidae	Prinodontidae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
linsang	linsang	k1gInSc1
(	(	kIx(
<g/>
Prionodon	Prionodon	k1gNnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Prionodon	Prionodon	k1gMnSc1
pardicolorHodgson	pardicolorHodgson	k1gMnSc1
<g/>
,	,	kIx,
1842	#num#	k4
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
linsanga	linsang	k1gMnSc2
skvrnitého	skvrnitý	k2eAgMnSc2d1
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
linsanga	linsang	k1gMnSc2
skvrnitého	skvrnitý	k2eAgMnSc2d1
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Linsang	Linsang	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Prionodon	Prionodon	k1gMnSc1
pardicolor	pardicolor	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šelma	šelma	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
asijští	asijský	k2eAgMnPc5d1
linsangové	linsang	k1gMnPc5
(	(	kIx(
<g/>
Prinodontidae	Prinodontidae	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
rodu	rod	k1gInSc2
linsang	linsang	k1gMnSc1
(	(	kIx(
<g/>
Priodon	Priodon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druh	druh	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
Brian	Brian	k1gMnSc1
Houghton	Houghton	k1gInSc4
Hodgson	Hodgson	k1gMnSc1
roku	rok	k1gInSc2
1842	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
nejbližší	blízký	k2eAgFnSc6d3
žijící	žijící	k2eAgFnSc6d1
příbuzné	příbuzná	k1gFnSc6
kočkovitých	kočkovití	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
dva	dva	k4xCgInPc1
poddruhy	poddruh	k1gInPc1
této	tento	k3xDgFnSc2
šelmy	šelma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Linsang	Linsang	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Nepálu	Nepál	k1gInSc6
<g/>
,	,	kIx,
Laosu	Laos	k1gInSc2
<g/>
,	,	kIx,
Myanmaru	Myanmar	k1gInSc2
<g/>
,	,	kIx,
Vietnamu	Vietnam	k1gInSc2
aj.	aj.	kA
Jeho	jeho	k3xOp3gMnSc1
populace	populace	k1gFnSc2
zásadním	zásadní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
neubývá	ubývat	k5eNaImIp3nS
a	a	k8xC
dle	dle	k7c2
IUCN	IUCN	kA
je	být	k5eAaImIp3nS
málo	málo	k6eAd1
dotčeným	dotčený	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Linsang	Linsang	k1gInSc1
měří	měřit	k5eAaImIp3nS
na	na	k7c4
délku	délka	k1gFnSc4
38	#num#	k4
až	až	k9
41	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
na	na	k7c4
ocas	ocas	k1gInSc4
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
dalších	další	k2eAgMnPc2d1
cca	cca	kA
34	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
výšku	výška	k1gFnSc4
dosahuje	dosahovat	k5eAaImIp3nS
13	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmotnost	hmotnost	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
600	#num#	k4
gramů	gram	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Samice	samice	k1gFnPc1
a	a	k8xC
samci	samec	k1gMnPc1
jsou	být	k5eAaImIp3nP
stejně	stejně	k6eAd1
velcí	velký	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
černě	černě	k6eAd1
pruhované	pruhovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zakončené	zakončený	k2eAgNnSc1d1
dlouhým	dlouhý	k2eAgInSc7d1
ocasem	ocas	k1gInSc7
s	s	k7c7
černými	černý	k2eAgInPc7d1
kruhovitými	kruhovitý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kožešina	kožešina	k1gFnSc1
je	být	k5eAaImIp3nS
hustá	hustý	k2eAgFnSc1d1
a	a	k8xC
jemná	jemný	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
šelmy	šelma	k1gFnPc4
lidmi	člověk	k1gMnPc7
loveny	lovit	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
dobrý	dobrý	k2eAgInSc4d1
zrak	zrak	k1gInSc4
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
oči	oko	k1gNnPc4
jim	on	k3xPp3gFnPc3
umožňují	umožňovat	k5eAaImIp3nP
vidět	vidět	k5eAaImF
i	i	k9
ve	v	k7c6
tmě	tma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tlapkách	tlapka	k1gFnPc6
mají	mít	k5eAaImIp3nP
ostré	ostrý	k2eAgInPc1d1
drápy	dráp	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Záměna	záměna	k1gFnSc1
</s>
<s>
Kvůli	kvůli	k7c3
plíživému	plíživý	k2eAgInSc3d1
pohybu	pohyb	k1gInSc3
po	po	k7c6
větvích	větev	k1gFnPc6
je	být	k5eAaImIp3nS
možná	možný	k2eAgFnSc1d1
záměna	záměna	k1gFnSc1
s	s	k7c7
některými	některý	k3yIgMnPc7
hady	had	k1gMnPc7
<g/>
,	,	kIx,
například	například	k6eAd1
s	s	k7c7
krajtami	krajta	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Poddruhy	poddruh	k1gInPc1
</s>
<s>
Prionodon	Prionodon	k1gMnSc1
pardicolor	pardicolor	k1gMnSc1
pardicolor	pardicolor	k1gMnSc1
<g/>
,	,	kIx,
Hodgson	Hodgson	k1gMnSc1
<g/>
,	,	kIx,
1842	#num#	k4
</s>
<s>
Prionodon	Prionodon	k1gMnSc1
pardicolor	pardicolor	k1gMnSc1
presina	presina	k1gFnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Linsangové	Linsang	k1gMnPc1
jsou	být	k5eAaImIp3nP
predátoři	predátor	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
života	život	k1gInSc2
tráví	trávit	k5eAaImIp3nP
ve	v	k7c6
stromoví	stromoví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohybu	pohyb	k1gInSc2
po	po	k7c6
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
nevyhýbají	vyhýbat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
noční	noční	k2eAgMnPc1d1
tvorové	tvor	k1gMnPc1
<g/>
,	,	kIx,
přes	přes	k7c4
den	den	k1gInSc4
spí	spát	k5eAaImIp3nP
v	v	k7c6
dutinách	dutina	k1gFnPc6
stromů	strom	k1gInPc2
nebo	nebo	k8xC
v	v	k7c6
úkrytech	úkryt	k1gInPc6
pod	pod	k7c7
kořeny	kořen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loví	lovit	k5eAaImIp3nS
hlodavce	hlodavec	k1gMnSc4
<g/>
,	,	kIx,
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
ptáky	pták	k1gMnPc4
a	a	k8xC
jiné	jiný	k2eAgMnPc4d1
menší	malý	k2eAgMnPc4d2
obratlovce	obratlovec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požírají	požírat	k5eAaImIp3nP
též	též	k9
mršiny	mršina	k1gFnPc1
<g/>
,	,	kIx,
vajíčka	vajíčko	k1gNnPc1
a	a	k8xC
ovoce	ovoce	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
únoru	únor	k1gInSc6
a	a	k8xC
srpnu	srpen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc4
se	se	k3xPyFc4
rodí	rodit	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
až	až	k8xS
dvě	dva	k4xCgFnPc1
<g/>
,	,	kIx,
po	po	k7c6
narození	narození	k1gNnSc6
váží	vážit	k5eAaImIp3nS
asi	asi	k9
40	#num#	k4
g.	g.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Spotted	Spotted	k1gMnSc1
linsang	linsang	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Prionodon	Prionodon	k1gMnSc1
pardicolor	pardicolor	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iucnredlist	iucnredlist	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KEPNER	KEPNER	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prionodon	Prionodon	k1gMnSc1
pardicolor	pardicolor	k1gMnSc1
(	(	kIx(
<g/>
spotted	spotted	k1gMnSc1
linsang	linsang	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Animal	animal	k1gMnSc1
Diversity	Diversit	k1gInPc4
Web	web	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Linsang	Linsang	k1gInSc1
skvrnitý	skvrnitý	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
biolib	biolib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
linsang	linsang	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Prionodon	Prionodon	k1gInSc1
pardicolor	pardicolor	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
