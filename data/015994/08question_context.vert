<s>
Linsang	Linsang	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Nepálu	Nepál	k1gInSc6
<g/>
,	,	kIx,
Laosu	Laos	k1gInSc2
<g/>
,	,	kIx,
Myanmaru	Myanmar	k1gInSc2
<g/>
,	,	kIx,
Vietnamu	Vietnam	k1gInSc2
aj.	aj.	kA
Jeho	jeho	k3xOp3gMnSc1
populace	populace	k1gFnSc2
zásadním	zásadní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
neubývá	ubývat	k5eNaImIp3nS
a	a	k8xC
dle	dle	k7c2
IUCN	IUCN	kA
je	být	k5eAaImIp3nS
málo	málo	k6eAd1
dotčeným	dotčený	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>