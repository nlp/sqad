<p>
<s>
HTTPS	HTTPS	kA	HTTPS
(	(	kIx(	(
<g/>
Hypertext	hypertext	k1gInSc1	hypertext
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
Secure	Secur	k1gMnSc5	Secur
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
protokol	protokol	k1gInSc4	protokol
umožňující	umožňující	k2eAgInSc4d1	umožňující
zabezpečenou	zabezpečený	k2eAgFnSc4d1	zabezpečená
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
HTTPS	HTTPS	kA	HTTPS
využívá	využívat	k5eAaPmIp3nS	využívat
protokol	protokol	k1gInSc1	protokol
HTTP	HTTP	kA	HTTP
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
protokolem	protokol	k1gInSc7	protokol
SSL	SSL	kA	SSL
nebo	nebo	k8xC	nebo
TLS	TLS	kA	TLS
<g/>
.	.	kIx.	.
</s>
<s>
HTTPS	HTTPS	kA	HTTPS
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
především	především	k9	především
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
s	s	k7c7	s
webovým	webový	k2eAgInSc7d1	webový
serverem	server	k1gInSc7	server
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
autentizaci	autentizace	k1gFnSc4	autentizace
<g/>
,	,	kIx,	,
důvěrnost	důvěrnost	k1gFnSc4	důvěrnost
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
integritu	integrita	k1gFnSc4	integrita
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
port	port	k1gInSc1	port
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
serveru	server	k1gInSc2	server
je	být	k5eAaImIp3nS	být
443	[number]	k4	443
TCP	TCP	kA	TCP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protokol	protokol	k1gInSc1	protokol
HTTPS	HTTPS	kA	HTTPS
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
společností	společnost	k1gFnPc2	společnost
Netscape	Netscap	k1gInSc5	Netscap
Communications	Communications	k1gInSc4	Communications
pro	pro	k7c4	pro
webový	webový	k2eAgInSc4d1	webový
prohlížeč	prohlížeč	k1gInSc4	prohlížeč
Netscape	Netscap	k1gInSc5	Netscap
Navigator	Navigator	k1gInSc4	Navigator
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
podporovat	podporovat	k5eAaImF	podporovat
HTTPS	HTTPS	kA	HTTPS
místo	místo	k1gNnSc4	místo
nezabezpečeného	zabezpečený	k2eNgMnSc2d1	nezabezpečený
HTTP	HTTP	kA	HTTP
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
funkce	funkce	k1gFnSc2	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
komunikace	komunikace	k1gFnSc2	komunikace
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
protokol	protokol	k1gInSc1	protokol
SSL	SSL	kA	SSL
anebo	anebo	k8xC	anebo
novější	nový	k2eAgFnSc2d2	novější
TLS	TLS	kA	TLS
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
asymetrické	asymetrický	k2eAgFnSc2d1	asymetrická
kryptografie	kryptografie	k1gFnSc2	kryptografie
je	být	k5eAaImIp3nS	být
ověřena	ověřen	k2eAgFnSc1d1	ověřena
identita	identita	k1gFnSc1	identita
webového	webový	k2eAgInSc2d1	webový
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
volitelně	volitelně	k6eAd1	volitelně
i	i	k9	i
klienta	klient	k1gMnSc4	klient
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
dohoda	dohoda	k1gFnSc1	dohoda
na	na	k7c6	na
klíči	klíč	k1gInSc6	klíč
pro	pro	k7c4	pro
symetrické	symetrický	k2eAgNnSc4d1	symetrické
šifrování	šifrování	k1gNnSc4	šifrování
samotné	samotný	k2eAgFnSc2d1	samotná
komunikace	komunikace	k1gFnSc2	komunikace
(	(	kIx(	(
<g/>
z	z	k7c2	z
výkonnostních	výkonnostní	k2eAgInPc2d1	výkonnostní
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klíč	klíč	k1gInSc1	klíč
pro	pro	k7c4	pro
symetrickou	symetrický	k2eAgFnSc4d1	symetrická
šifru	šifra	k1gFnSc4	šifra
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
vybrán	vybrán	k2eAgInSc1d1	vybrán
klientem	klient	k1gMnSc7	klient
a	a	k8xC	a
nebo	nebo	k8xC	nebo
dohodnut	dohodnout	k5eAaPmNgInS	dohodnout
pomocí	pomoc	k1gFnSc7	pomoc
Diffieho-Hellmanovy	Diffieho-Hellmanův	k2eAgFnSc2d1	Diffieho-Hellmanův
výměny	výměna	k1gFnSc2	výměna
klíče	klíč	k1gInSc2	klíč
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
dopředné	dopředný	k2eAgFnSc2d1	dopředná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
SSL	SSL	kA	SSL
a	a	k8xC	a
TLS	TLS	kA	TLS
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgFnSc1d1	zásadní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
veřejného	veřejný	k2eAgInSc2d1	veřejný
klíče	klíč	k1gInSc2	klíč
a	a	k8xC	a
X	X	kA	X
<g/>
.509	.509	k4	.509
certifikáty	certifikát	k1gInPc7	certifikát
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgMnPc3	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
autentizace	autentizace	k1gFnSc1	autentizace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
ověření	ověření	k1gNnSc4	ověření
identity	identita	k1gFnSc2	identita
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
důvěra	důvěra	k1gFnSc1	důvěra
v	v	k7c4	v
zaslaný	zaslaný	k2eAgInSc4d1	zaslaný
certifikát	certifikát	k1gInSc4	certifikát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nejčastěji	často	k6eAd3	často
bývá	bývat	k5eAaImIp3nS	bývat
zprostředkovaná	zprostředkovaný	k2eAgFnSc1d1	zprostředkovaná
některou	některý	k3yIgFnSc7	některý
z	z	k7c2	z
certifikačních	certifikační	k2eAgFnPc2d1	certifikační
autorit	autorita	k1gFnPc2	autorita
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
certifikáty	certifikát	k1gInPc1	certifikát
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
úložišti	úložiště	k1gNnSc6	úložiště
důvěryhodných	důvěryhodný	k2eAgInPc2d1	důvěryhodný
certifikátů	certifikát	k1gInPc2	certifikát
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
nebo	nebo	k8xC	nebo
aplikace	aplikace	k1gFnSc2	aplikace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
webového	webový	k2eAgInSc2d1	webový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
certifikačních	certifikační	k2eAgFnPc2d1	certifikační
autorit	autorita	k1gFnPc2	autorita
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
certifikáty	certifikát	k1gInPc4	certifikát
na	na	k7c6	na
komerční	komerční	k2eAgFnSc6d1	komerční
bázi	báze	k1gFnSc6	báze
(	(	kIx(	(
<g/>
Symantec	Symantec	kA	Symantec
<g/>
,	,	kIx,	,
Verisign	Verisign	k1gNnSc1	Verisign
<g/>
,	,	kIx,	,
COMODO	COMODO	kA	COMODO
<g/>
,	,	kIx,	,
Thawte	Thawt	k1gInSc5	Thawt
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
certifikační	certifikační	k2eAgFnPc1d1	certifikační
autority	autorita	k1gFnPc1	autorita
vystavující	vystavující	k2eAgInPc4d1	vystavující
certifikáty	certifikát	k1gInPc4	certifikát
bezplatně	bezplatně	k6eAd1	bezplatně
jako	jako	k8xC	jako
například	například	k6eAd1	například
Let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Encrypt	Encrypt	k1gInSc1	Encrypt
<g/>
,	,	kIx,	,
StartSSL	StartSSL	k1gFnSc1	StartSSL
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úložiště	úložiště	k1gNnSc2	úložiště
důvěryhodných	důvěryhodný	k2eAgInPc2d1	důvěryhodný
certifikátů	certifikát	k1gInPc2	certifikát
lze	lze	k6eAd1	lze
doinstalovat	doinstalovat	k5eAaPmF	doinstalovat
další	další	k2eAgFnPc4d1	další
certifikační	certifikační	k2eAgFnPc4d1	certifikační
autority	autorita	k1gFnPc4	autorita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
certifikát	certifikát	k1gInSc1	certifikát
ověřit	ověřit	k5eAaPmF	ověřit
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c4	na
uživatelův	uživatelův	k2eAgInSc4d1	uživatelův
pokyn	pokyn	k1gInSc4	pokyn
pokračovat	pokračovat	k5eAaImF	pokračovat
bez	bez	k7c2	bez
ověření	ověření	k1gNnSc4	ověření
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
rizikem	riziko	k1gNnSc7	riziko
Man	Man	k1gMnSc1	Man
in	in	k?	in
the	the	k?	the
middle	middle	k6eAd1	middle
útoku	útok	k1gInSc2	útok
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
ověření	ověření	k1gNnSc2	ověření
provést	provést	k5eAaPmF	provést
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
manuálně	manuálně	k6eAd1	manuálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Míra	Míra	k1gFnSc1	Míra
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
chování	chování	k1gNnSc4	chování
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
na	na	k7c6	na
implementaci	implementace	k1gFnSc6	implementace
protokolů	protokol	k1gInPc2	protokol
ve	v	k7c6	v
webovém	webový	k2eAgInSc6d1	webový
prohlížeči	prohlížeč	k1gInSc6	prohlížeč
a	a	k8xC	a
webovém	webový	k2eAgInSc6d1	webový
serveru	server	k1gInSc6	server
<g/>
,	,	kIx,	,
správné	správný	k2eAgFnSc6d1	správná
konfiguraci	konfigurace	k1gFnSc6	konfigurace
a	a	k8xC	a
na	na	k7c6	na
důvěryhodnosti	důvěryhodnost	k1gFnSc6	důvěryhodnost
certifikačních	certifikační	k2eAgFnPc2d1	certifikační
autorit	autorita	k1gFnPc2	autorita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výhody	výhoda	k1gFnSc2	výhoda
HTTPS	HTTPS	kA	HTTPS
===	===	k?	===
</s>
</p>
<p>
<s>
ověření	ověření	k1gNnSc1	ověření
identity	identita	k1gFnSc2	identita
</s>
</p>
<p>
<s>
důvěrnost	důvěrnost	k1gFnSc1	důvěrnost
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
</s>
</p>
<p>
<s>
integrita	integrita	k1gFnSc1	integrita
obsahu	obsah	k1gInSc2	obsah
</s>
</p>
<p>
<s>
možnost	možnost	k1gFnSc1	možnost
využití	využití	k1gNnSc2	využití
HTTP	HTTP	kA	HTTP
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
protokolu	protokol	k1gInSc2	protokol
</s>
</p>
<p>
<s>
zvýhodnění	zvýhodnění	k1gNnSc1	zvýhodnění
ve	v	k7c6	v
vyhledávači	vyhledávač	k1gMnSc6	vyhledávač
Google	Google	k1gFnSc2	Google
</s>
</p>
<p>
<s>
jen	jen	k9	jen
nepatrný	nepatrný	k2eAgInSc4d1	nepatrný
pokles	pokles	k1gInSc4	pokles
výkonu	výkon	k1gInSc2	výkon
u	u	k7c2	u
novějšího	nový	k2eAgInSc2d2	novější
hardwaru	hardware	k1gInSc2	hardware
</s>
</p>
<p>
<s>
===	===	k?	===
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
HTTPS	HTTPS	kA	HTTPS
===	===	k?	===
</s>
</p>
<p>
<s>
pokles	pokles	k1gInSc1	pokles
výkonu	výkon	k1gInSc2	výkon
u	u	k7c2	u
staršího	starý	k2eAgInSc2d2	starší
hardwaru	hardware	k1gInSc2	hardware
</s>
</p>
<p>
<s>
potřeba	potřeba	k1gFnSc1	potřeba
certifikátu	certifikát	k1gInSc2	certifikát
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
obnovování	obnovování	k1gNnSc2	obnovování
</s>
</p>
<p>
<s>
mírně	mírně	k6eAd1	mírně
složitější	složitý	k2eAgFnSc1d2	složitější
konfigurace	konfigurace	k1gFnSc1	konfigurace
webového	webový	k2eAgInSc2d1	webový
serveru	server	k1gInSc2	server
</s>
</p>
<p>
<s>
možné	možný	k2eAgFnPc1d1	možná
komplikace	komplikace	k1gFnPc1	komplikace
u	u	k7c2	u
starších	starý	k2eAgInPc2d2	starší
webových	webový	k2eAgInPc2d1	webový
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
</s>
</p>
<p>
<s>
===	===	k?	===
Budoucnost	budoucnost	k1gFnSc1	budoucnost
===	===	k?	===
</s>
</p>
<p>
<s>
Kryptografické	kryptografický	k2eAgInPc1d1	kryptografický
algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
HTTPS	HTTPS	kA	HTTPS
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
hypotetickým	hypotetický	k2eAgMnPc3d1	hypotetický
<g/>
)	)	kIx)	)
kvantovým	kvantový	k2eAgInPc3d1	kvantový
počítačům	počítač	k1gInPc3	počítač
(	(	kIx(	(
<g/>
např.	např.	kA	např.
algoritmus	algoritmus	k1gInSc1	algoritmus
RSA	RSA	kA	RSA
svou	svůj	k3xOyFgFnSc4	svůj
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
složitosti	složitost	k1gFnSc2	složitost
faktorizace	faktorizace	k1gFnSc2	faktorizace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nP	by
však	však	k9	však
Shorův	Shorův	k2eAgInSc1d1	Shorův
algoritmus	algoritmus	k1gInSc1	algoritmus
na	na	k7c6	na
kvantovém	kvantový	k2eAgInSc6d1	kvantový
počítači	počítač	k1gInSc6	počítač
dokázal	dokázat	k5eAaPmAgInS	dokázat
provádět	provádět	k5eAaImF	provádět
v	v	k7c6	v
polynomiálním	polynomiální	k2eAgInSc6d1	polynomiální
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
podařilo	podařit	k5eAaPmAgNnS	podařit
sestrojit	sestrojit	k5eAaPmF	sestrojit
prakticky	prakticky	k6eAd1	prakticky
použitelný	použitelný	k2eAgInSc1d1	použitelný
kvantový	kvantový	k2eAgInSc1d1	kvantový
počítač	počítač	k1gInSc1	počítač
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
internetové	internetový	k2eAgFnSc2d1	internetová
komunikace	komunikace	k1gFnSc2	komunikace
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
prolomena	prolomen	k2eAgFnSc1d1	prolomena
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
riziko	riziko	k1gNnSc1	riziko
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
eliminovat	eliminovat	k5eAaBmF	eliminovat
vývoj	vývoj	k1gInSc4	vývoj
tzv.	tzv.	kA	tzv.
postkvantové	postkvantový	k2eAgFnSc2d1	postkvantový
kryptografie	kryptografie	k1gFnSc2	kryptografie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
nějaký	nějaký	k3yIgMnSc1	nějaký
útočník	útočník	k1gMnSc1	útočník
dnes	dnes	k6eAd1	dnes
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
cizí	cizí	k2eAgFnSc4d1	cizí
internetovou	internetový	k2eAgFnSc4d1	internetová
komunikaci	komunikace	k1gFnSc4	komunikace
zabezpečenou	zabezpečený	k2eAgFnSc4d1	zabezpečená
HTTPS	HTTPS	kA	HTTPS
<g/>
,	,	kIx,	,
tak	tak	k9	tak
by	by	kYmCp3nS	by
sice	sice	k8xC	sice
zatím	zatím	k6eAd1	zatím
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
schopen	schopen	k2eAgInSc1d1	schopen
ji	on	k3xPp3gFnSc4	on
dešifrovat	dešifrovat	k5eAaBmF	dešifrovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
budou	být	k5eAaImBp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
kvantové	kvantový	k2eAgInPc4d1	kvantový
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
tuto	tento	k3xDgFnSc4	tento
uloženou	uložený	k2eAgFnSc4d1	uložená
komunikaci	komunikace	k1gFnSc4	komunikace
mohl	moct	k5eAaImAgInS	moct
dešifrovat	dešifrovat	k5eAaBmF	dešifrovat
zpětně	zpětně	k6eAd1	zpětně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nasazení	nasazení	k1gNnSc1	nasazení
HTTPS	HTTPS	kA	HTTPS
==	==	k?	==
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
rozšířenému	rozšířený	k2eAgInSc3d1	rozšířený
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
HTTPS	HTTPS	kA	HTTPS
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
weby	web	k1gInPc4	web
pracující	pracující	k1gFnSc2	pracující
s	s	k7c7	s
citlivými	citlivý	k2eAgInPc7d1	citlivý
údaji	údaj	k1gInPc7	údaj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgFnPc7d1	mnohá
organizacemi	organizace	k1gFnPc7	organizace
a	a	k8xC	a
odborníky	odborník	k1gMnPc7	odborník
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
používat	používat	k5eAaImF	používat
HTTPS	HTTPS	kA	HTTPS
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
webech	web	k1gInPc6	web
<g/>
.	.	kIx.	.
</s>
<s>
HTTPS	HTTPS	kA	HTTPS
představuje	představovat	k5eAaImIp3nS	představovat
účinnou	účinný	k2eAgFnSc4d1	účinná
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
vkládání	vkládání	k1gNnSc3	vkládání
nežádoucího	žádoucí	k2eNgInSc2d1	nežádoucí
obsahu	obsah	k1gInSc2	obsah
(	(	kIx(	(
<g/>
malware	malwar	k1gMnSc5	malwar
<g/>
,	,	kIx,	,
nevyžádané	vyžádaný	k2eNgFnPc4d1	nevyžádaná
reklamy	reklama	k1gFnPc4	reklama
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
poskytovatelem	poskytovatel	k1gMnSc7	poskytovatel
internetového	internetový	k2eAgNnSc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
či	či	k8xC	či
jiným	jiný	k2eAgMnSc7d1	jiný
útočníkem	útočník	k1gMnSc7	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
nasazení	nasazení	k1gNnSc4	nasazení
HTTPS	HTTPS	kA	HTTPS
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
ukončení	ukončení	k1gNnSc2	ukončení
podpory	podpora	k1gFnSc2	podpora
HTTP	HTTP	kA	HTTP
ve	v	k7c6	v
webových	webový	k2eAgInPc6d1	webový
prohlížečích	prohlížeč	k1gInPc6	prohlížeč
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
odstranění	odstranění	k1gNnSc2	odstranění
rizika	riziko	k1gNnPc4	riziko
SSL-stripping	SSLtripping	k1gInSc1	SSL-stripping
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
pro	pro	k7c4	pro
nasazení	nasazení	k1gNnSc4	nasazení
HTTPS	HTTPS	kA	HTTPS
je	být	k5eAaImIp3nS	být
získání	získání	k1gNnSc4	získání
certifikátu	certifikát	k1gInSc2	certifikát
od	od	k7c2	od
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
certifikačních	certifikační	k2eAgFnPc2d1	certifikační
autorit	autorita	k1gFnPc2	autorita
nebo	nebo	k8xC	nebo
vygenerování	vygenerování	k1gNnSc4	vygenerování
self-signed	selfigned	k1gMnSc1	self-signed
certifikátu	certifikát	k1gInSc2	certifikát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
konfigurace	konfigurace	k1gFnSc1	konfigurace
web	web	k1gInSc1	web
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
zejména	zejména	k9	zejména
výběr	výběr	k1gInSc4	výběr
šifrovacích	šifrovací	k2eAgFnPc2d1	šifrovací
sad	sada	k1gFnPc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
konfigurací	konfigurace	k1gFnSc7	konfigurace
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
online	onlinout	k5eAaPmIp3nS	onlinout
nástroj	nástroj	k1gInSc1	nástroj
Mozilla	Mozill	k1gMnSc2	Mozill
SSL	SSL	kA	SSL
Configuration	Configuration	k1gInSc1	Configuration
Generator	Generator	k1gInSc1	Generator
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
webového	webový	k2eAgInSc2d1	webový
serveru	server	k1gInSc2	server
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
otestovat	otestovat	k5eAaPmF	otestovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
například	například	k6eAd1	například
online	onlinout	k5eAaPmIp3nS	onlinout
nástroje	nástroj	k1gInPc4	nástroj
Qualys	Qualysa	k1gFnPc2	Qualysa
SSL	SSL	kA	SSL
Labs	Labs	k1gInSc1	Labs
<g/>
,	,	kIx,	,
CryptCheck	CryptCheck	k1gInSc1	CryptCheck
nebo	nebo	k8xC	nebo
Observatory	Observator	k1gInPc1	Observator
by	by	kYmCp3nS	by
Mozilla	Mozilla	k1gFnSc1	Mozilla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nasazení	nasazení	k1gNnSc6	nasazení
HTTPS	HTTPS	kA	HTTPS
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pravidelně	pravidelně	k6eAd1	pravidelně
obnovovat	obnovovat	k5eAaImF	obnovovat
certifikát	certifikát	k1gInSc4	certifikát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
platnost	platnost	k1gFnSc1	platnost
typicky	typicky	k6eAd1	typicky
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
certifikační	certifikační	k2eAgFnSc2d1	certifikační
autority	autorita	k1gFnSc2	autorita
Let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Encrypt	Encrypt	k1gInSc1	Encrypt
je	být	k5eAaImIp3nS	být
platnost	platnost	k1gFnSc4	platnost
certifikátu	certifikát	k1gInSc2	certifikát
pouze	pouze	k6eAd1	pouze
90	[number]	k4	90
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zautomatizován	zautomatizován	k2eAgMnSc1d1	zautomatizován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pokročilé	pokročilý	k2eAgFnPc4d1	pokročilá
možnosti	možnost	k1gFnPc4	možnost
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
===	===	k?	===
</s>
</p>
<p>
<s>
Zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
HTTPS	HTTPS	kA	HTTPS
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zvýšit	zvýšit	k5eAaPmF	zvýšit
některými	některý	k3yIgFnPc7	některý
technikami	technika	k1gFnPc7	technika
jakými	jaký	k3yRgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
HSTS	HSTS	kA	HSTS
<g/>
,	,	kIx,	,
HPKP	HPKP	kA	HPKP
<g/>
,	,	kIx,	,
CAA	CAA	kA	CAA
a	a	k8xC	a
TLSA	TLSA	kA	TLSA
záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
DNS	DNS	kA	DNS
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
mít	mít	k5eAaImF	mít
DNSSEC	DNSSEC	kA	DNSSEC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
HTTPS	HTTPS	kA	HTTPS
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
podporovalo	podporovat	k5eAaImAgNnS	podporovat
HTTPS	HTTPS	kA	HTTPS
protokol	protokol	k1gInSc1	protokol
jako	jako	k8xS	jako
výchozí	výchozí	k2eAgMnSc1d1	výchozí
35,3	[number]	k4	35,3
%	%	kIx~	%
webů	web	k1gInPc2	web
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
Alexa	Alex	k1gMnSc2	Alex
top	topit	k5eAaImRp2nS	topit
1,000,000	[number]	k4	1,000,000
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
Firefox	Firefox	k1gInSc1	Firefox
Telemetry	telemetr	k1gInPc4	telemetr
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
načteno	načíst	k5eAaPmNgNnS	načíst
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
protokolu	protokol	k1gInSc2	protokol
HTTPS	HTTPS	kA	HTTPS
69	[number]	k4	69
<g/>
%	%	kIx~	%
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
začal	začít	k5eAaPmAgInS	začít
prohlížeč	prohlížeč	k1gInSc4	prohlížeč
Chrome	chromat	k5eAaImIp3nS	chromat
označovat	označovat	k5eAaImF	označovat
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přijímají	přijímat	k5eAaImIp3nP	přijímat
od	od	k7c2	od
uživatele	uživatel	k1gMnSc2	uživatel
citlivé	citlivý	k2eAgFnPc1d1	citlivá
informace	informace	k1gFnPc1	informace
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
HTTPS	HTTPS	kA	HTTPS
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
změny	změna	k1gFnSc2	změna
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
výrazný	výrazný	k2eAgInSc1d1	výrazný
nárůst	nárůst	k1gInSc1	nárůst
použití	použití	k1gNnSc2	použití
HTTPS	HTTPS	kA	HTTPS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Server	server	k1gInSc1	server
Name	Name	k1gNnSc1	Name
Indication	Indication	k1gInSc1	Indication
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HTTPS	HTTPS	kA	HTTPS
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://wp.netscape.com/eng/ssl3/draft302.txt	[url]	k1gInSc1	http://wp.netscape.com/eng/ssl3/draft302.txt
–	–	k?	–
Netscape	Netscap	k1gInSc5	Netscap
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
SSL	SSL	kA	SSL
3.0	[number]	k4	3.0
Specification	Specification	k1gInSc4	Specification
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HTTPS	HTTPS	kA	HTTPS
Everywhere	Everywher	k1gInSc5	Everywher
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
Electronic	Electronice	k1gFnPc2	Electronice
Frontier	Frontira	k1gFnPc2	Frontira
Foundation	Foundation	k1gInSc1	Foundation
–	–	k?	–
rozšíření	rozšíření	k1gNnSc1	rozšíření
pro	pro	k7c4	pro
Mozillu	Mozilla	k1gFnSc4	Mozilla
Firefox	Firefox	k1gInSc4	Firefox
umožňující	umožňující	k2eAgNnSc1d1	umožňující
permanentní	permanentní	k2eAgNnSc1d1	permanentní
využívání	využívání	k1gNnSc1	využívání
protokolu	protokol	k1gInSc2	protokol
HTTPS	HTTPS	kA	HTTPS
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
https://web.archive.org/web/20090314040822/http://www.apache-ssl.org/	[url]	k4	https://web.archive.org/web/20090314040822/http://www.apache-ssl.org/
–	–	k?	–
Apache-SSL	Apache-SSL	k1gMnSc4	Apache-SSL
homepage	homepag	k1gMnSc4	homepag
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://httpd.apache.org/docs/2.2/ssl/	[url]	k4	http://httpd.apache.org/docs/2.2/ssl/
–	–	k?	–
Apache	Apach	k1gInSc2	Apach
2.2	[number]	k4	2.2
mod_ssl	mod_ssnout	k5eAaPmAgInS	mod_ssnout
documentation	documentation	k1gInSc1	documentation
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RFC	RFC	kA	RFC
2818	[number]	k4	2818
–	–	k?	–
HTTP	HTTP	kA	HTTP
Over	Over	k1gInSc1	Over
TLS	TLS	kA	TLS
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.g-loaded.eu/2007/08/10/ssl-enabled-name-based-apache-virtual-hosts-with-mod_gnutls/	[url]	k4	http://www.g-loaded.eu/2007/08/10/ssl-enabled-name-based-apache-virtual-hosts-with-mod_gnutls/
–	–	k?	–
SNI	snít	k5eAaImRp2nS	snít
s	s	k7c7	s
mod_gnutls	mod_gnutlsa	k1gFnPc2	mod_gnutlsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.lupa.cz/clanky/https-bezpecnost-jen-pro-vyvolene/	[url]	k?	http://www.lupa.cz/clanky/https-bezpecnost-jen-pro-vyvolene/
–	–	k?	–
český	český	k2eAgInSc4d1	český
článek	článek	k1gInSc4	článek
o	o	k7c6	o
protokolu	protokol	k1gInSc6	protokol
HTTPS	HTTPS	kA	HTTPS
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Čeleda	Čeleda	k1gMnSc1	Čeleda
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
Ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
bezpečné	bezpečný	k2eAgInPc1d1	bezpečný
HTTPS	HTTPS	kA	HTTPS
−	−	k?	−
část	část	k1gFnSc1	část
I	I	kA	I
<g/>
,	,	kIx,	,
Článek	článek	k1gInSc1	článek
v	v	k7c6	v
recenzovaném	recenzovaný	k2eAgInSc6d1	recenzovaný
časopise	časopis	k1gInSc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
DSM	DSM	kA	DSM
−	−	k?	−
Data	datum	k1gNnSc2	datum
security	securita	k1gFnSc2	securita
management	management	k1gInSc1	management
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hlavinka	hlavinka	k1gFnSc1	hlavinka
–	–	k?	–
Co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
pohlídat	pohlídat	k5eAaPmF	pohlídat
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
HTTPS	HTTPS	kA	HTTPS
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
HTTPS	HTTPS	kA	HTTPS
od	od	k7c2	od
SEO	SEO	kA	SEO
konzultanta	konzultant	k1gMnSc4	konzultant
z	z	k7c2	z
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Chrome	chromat	k5eAaImIp3nS	chromat
68	[number]	k4	68
jde	jít	k5eAaImIp3nS	jít
cestou	cesta	k1gFnSc7	cesta
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
–	–	k?	–
Článek	článek	k1gInSc1	článek
o	o	k7c4	o
Chrome	chromat	k5eAaImIp3nS	chromat
68	[number]	k4	68
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
zvýrazňovat	zvýrazňovat	k5eAaImF	zvýrazňovat
nezabezpečené	zabezpečený	k2eNgFnPc4d1	nezabezpečená
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
</p>
