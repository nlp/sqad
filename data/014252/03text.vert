<s>
Mášeňka	Mášeňka	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
odebrat	odebrat	k5eAaPmF
tučný	tučný	k2eAgInSc4d1
text	text	k1gInSc4
<g/>
,	,	kIx,
wikifikovat	wikifikovat	k5eAaBmF,k5eAaImF,k5eAaPmF
</s>
<s>
Mášeňka	Mášeňka	k1gFnSc1
Autor	autor	k1gMnSc1
</s>
<s>
Vladimir	Vladimir	k1gInSc1
Vladimirovič	Vladimirovič	k1gInSc1
Nabokov	Nabokov	k1gInSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
М	М	k?
Země	země	k1gFnSc1
</s>
<s>
Výmarská	výmarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
ruština	ruština	k1gFnSc1
a	a	k8xC
angličtina	angličtina	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
román	román	k1gInSc4
Datum	datum	k1gInSc1
vydání	vydání	k1gNnSc1
</s>
<s>
1926	#num#	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
King	King	k1gMnSc1
<g/>
,	,	kIx,
Queen	Queen	k1gInSc1
<g/>
,	,	kIx,
Knave	Knaev	k1gFnPc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mášeňka	Mášeňka	k1gFnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgInSc1
román	román	k1gInSc1
ruského	ruský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Vladimira	Vladimir	k1gMnSc2
Vladimiroviče	Vladimirovič	k1gMnSc2
Nabokova	Nabokův	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
románu	román	k1gInSc2
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
románu	román	k1gInSc2
je	být	k5eAaImIp3nS
ruský	ruský	k2eAgMnSc1d1
emigrant	emigrant	k1gMnSc1
Lev	Lev	k1gMnSc1
Glebovič	Glebovič	k1gMnSc1
Ganin	Ganin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zraněn	zranit	k5eAaPmNgMnS
v	v	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
Berlína	Berlín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
emigranty	emigrant	k1gMnPc7
v	v	k7c6
penzionu	penzion	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
náleží	náležet	k5eAaImIp3nS
vdově	vdova	k1gFnSc3
Lydii	Lydie	k1gFnSc4
Nikolajevně	Nikolajevně	k1gFnSc2
Dornové	Dornová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vede	vést	k5eAaImIp3nS
zde	zde	k6eAd1
podivný	podivný	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
schází	scházet	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
další	další	k2eAgFnSc7d1
emigrantkou	emigrantka	k1gFnSc7
Ludmilou	Ludmila	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
nemiluje	milovat	k5eNaImIp3nS
a	a	k8xC
neustále	neustále	k6eAd1
řeší	řešit	k5eAaImIp3nS
problém	problém	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
rozejít	rozejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sama	sám	k3xTgFnSc1
Ludmila	Ludmila	k1gFnSc1
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
naivní	naivní	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Ganinově	Ganinův	k2eAgNnSc6d1
chování	chování	k1gNnSc6
nezaznamená	zaznamenat	k5eNaPmIp3nS
nezájem	nezájem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
všech	všecek	k3xTgInPc6
emigrantech	emigrant	k1gMnPc6
žijících	žijící	k2eAgMnPc2d1
v	v	k7c6
penzionu	penzion	k1gInSc6
Ganin	Ganina	k1gFnPc2
smýšlí	smýšlet	k5eAaImIp3nS
jako	jako	k9
o	o	k7c6
ruských	ruský	k2eAgInPc6d1
stínech	stín	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
večer	večer	k1gInSc1
konverzuje	konverzovat	k5eAaImIp3nS
s	s	k7c7
Alfjorovem	Alfjorov	k1gInSc7
a	a	k8xC
zjišťuje	zjišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
ním	on	k3xPp3gMnSc7
má	mít	k5eAaImIp3nS
do	do	k7c2
emigrace	emigrace	k1gFnSc2
zavítat	zavítat	k5eAaPmF
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
Mášeňka	Mášeňka	k1gFnSc1
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
Ganinovi	Ganin	k1gMnSc3
její	její	k3xOp3gFnSc4
fotku	fotka	k1gFnSc4
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
zvedne	zvednout	k5eAaPmIp3nS
a	a	k8xC
bez	bez	k7c2
vysvětlení	vysvětlení	k1gNnSc2
odejde	odejít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
Ganin	Ganin	k1gInSc1
navštíví	navštívit	k5eAaPmIp3nS
Ludmilu	Ludmila	k1gFnSc4
a	a	k8xC
končí	končit	k5eAaImIp3nS
s	s	k7c7
ní	on	k3xPp3gFnSc7
poměr	poměr	k1gInSc1
<g/>
,	,	kIx,
vysvětluje	vysvětlovat	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
miluje	milovat	k5eAaImIp3nS
jinou	jiný	k2eAgFnSc4d1
ženu	žena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
jménem	jméno	k1gNnSc7
Mášeňka	Mášeňka	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
Ganin	Ganin	k1gInSc4
myslet	myslet	k5eAaImF
na	na	k7c4
Rusko	Rusko	k1gNnSc4
<g/>
,	,	kIx,
mění	měnit	k5eAaImIp3nS
se	se	k3xPyFc4
a	a	k8xC
začíná	začínat	k5eAaImIp3nS
doopravdy	doopravdy	k6eAd1
žít	žít	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpomíná	vzpomínat	k5eAaImIp3nS
na	na	k7c4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
šestnáct	šestnáct	k4xCc4
let	léto	k1gNnPc2
a	a	k8xC
léčil	léčit	k5eAaImAgInS
se	se	k3xPyFc4
z	z	k7c2
tyfové	tyfový	k2eAgFnSc2d1
nákazy	nákaza	k1gFnSc2
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
potkal	potkat	k5eAaPmAgInS
dívku	dívka	k1gFnSc4
jménem	jméno	k1gNnSc7
Mášeňka	Mášeňka	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
zamiloval	zamilovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vkrade	vkrást	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
pokoje	pokoj	k1gInSc2
k	k	k7c3
Alfjorovovi	Alfjorova	k1gMnSc3
a	a	k8xC
chce	chtít	k5eAaImIp3nS
se	se	k3xPyFc4
znovu	znovu	k6eAd1
podívat	podívat	k5eAaImF,k5eAaPmF
na	na	k7c4
její	její	k3xOp3gFnSc4
fotku	fotka	k1gFnSc4
<g/>
,	,	kIx,
přistihne	přistihnout	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
ale	ale	k9
emigrantka	emigrantka	k1gFnSc1
Klára	Klára	k1gFnSc1
<g/>
,	,	kIx,
kamarádka	kamarádka	k1gFnSc1
Ludmily	Ludmila	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
na	na	k7c4
Ganina	Ganin	k1gMnSc4
tajně	tajně	k6eAd1
myslí	myslet	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
domnívá	domnívat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
chtěl	chtít	k5eAaImAgMnS
odcizit	odcizit	k5eAaPmF
příteli	přítel	k1gMnSc3
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganin	Ganin	k1gInSc1
ji	on	k3xPp3gFnSc4
v	v	k7c6
tom	ten	k3xDgInSc6
nechává	nechávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myslí	myslet	k5eAaImIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
může	moct	k5eAaImIp3nS
porozumět	porozumět	k5eAaPmF
jedině	jedině	k6eAd1
stařičký	stařičký	k2eAgMnSc1d1
básník	básník	k1gMnSc1
Podťagin	Podťagin	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
marně	marně	k6eAd1
snaží	snažit	k5eAaImIp3nP
získat	získat	k5eAaPmF
povolení	povolení	k1gNnSc4
pro	pro	k7c4
výjezd	výjezd	k1gInSc4
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
Berlíně	Berlín	k1gInSc6
s	s	k7c7
pocitem	pocit	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
Ruska	Rusko	k1gNnSc2
už	už	k6eAd1
nikdy	nikdy	k6eAd1
nevrátí	vrátit	k5eNaPmIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
starý	starý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganin	Ganin	k1gMnSc1
emigraci	emigrace	k1gFnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
pouhý	pouhý	k2eAgInSc4d1
sen	sen	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
reálné	reálný	k2eAgNnSc1d1
vnímá	vnímat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
odehrálo	odehrát	k5eAaPmAgNnS
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpomíná	vzpomínat	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
s	s	k7c7
Mášeňkou	Mášeňka	k1gFnSc7
párkrát	párkrát	k6eAd1
setkali	setkat	k5eAaPmAgMnP
a	a	k8xC
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
do	do	k7c2
sebe	sebe	k3xPyFc4
zamilovali	zamilovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k8xC
začala	začít	k5eAaPmAgFnS
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
oba	dva	k4xCgInPc1
odjeli	odjet	k5eAaPmAgMnP
z	z	k7c2
venkova	venkov	k1gInSc2
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
a	a	k8xC
vídali	vídat	k5eAaImAgMnP
se	se	k3xPyFc4
méně	málo	k6eAd2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
byla	být	k5eAaImAgFnS
zima	zima	k1gFnSc1
a	a	k8xC
neměli	mít	k5eNaImAgMnP
kam	kam	k6eAd1
jít	jít	k5eAaImF
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
láska	láska	k1gFnSc1
chřadla	chřadnout	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mášeňka	Mášeňka	k1gFnSc1
odjela	odjet	k5eAaPmAgFnS
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
se	se	k3xPyFc4
opět	opět	k6eAd1
setkali	setkat	k5eAaPmAgMnP
<g/>
,	,	kIx,
jel	jet	k5eAaImAgMnS
za	za	k7c2
ní	on	k3xPp3gFnSc2
kus	kus	k6eAd1
na	na	k7c6
kole	kolo	k1gNnSc6
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
ale	ale	k9
došel	dojít	k5eAaPmAgInS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
nemiluje	milovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mášeňka	Mášeňka	k1gFnSc1
se	se	k3xPyFc4
už	už	k6eAd1
neozvala	ozvat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naposledy	naposledy	k6eAd1
se	se	k3xPyFc4
náhodou	náhodou	k6eAd1
setkali	setkat	k5eAaPmAgMnP
ve	v	k7c6
vlaku	vlak	k1gInSc6
a	a	k8xC
on	on	k3xPp3gMnSc1
měl	mít	k5eAaImAgMnS
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
opět	opět	k6eAd1
miluje	milovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganin	Ganin	k1gInSc1
vzpomíná	vzpomínat	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
jako	jako	k9
vyměněný	vyměněný	k2eAgInSc1d1
<g/>
,	,	kIx,
slíbí	slíbit	k5eAaPmIp3nP
básníkovi	básníkův	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
pomůže	pomoct	k5eAaPmIp3nS
získat	získat	k5eAaPmF
povolení	povolení	k1gNnSc4
pro	pro	k7c4
výjezd	výjezd	k1gInSc4
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Básník	básník	k1gMnSc1
nakonec	nakonec	k9
ztratí	ztratit	k5eAaPmIp3nS
pas	pas	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
zlé	zlý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k8xC
vypukla	vypuknout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
a	a	k8xC
Mášeňka	Mášeňka	k1gFnSc1
mu	on	k3xPp3gMnSc3
psala	psát	k5eAaImAgFnS
na	na	k7c4
frontu	fronta	k1gFnSc4
dopisy	dopis	k1gInPc4
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
jí	jíst	k5eAaImIp3nS
odepisoval	odepisovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
čte	číst	k5eAaImIp3nS
staré	starý	k2eAgInPc4d1
dopisy	dopis	k1gInPc4
a	a	k8xC
těší	těšit	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
znovu	znovu	k6eAd1
uvidí	uvidět	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
jejich	jejich	k3xOp3gNnPc2
setkání	setkání	k1gNnPc2
si	se	k3xPyFc3
hodně	hodně	k6eAd1
slibuje	slibovat	k5eAaImIp3nS
<g/>
,	,	kIx,
myslí	myslet	k5eAaImIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
všechno	všechen	k3xTgNnSc1
bude	být	k5eAaImBp3nS
jako	jako	k9
dřív	dříve	k6eAd2
a	a	k8xC
že	že	k8xS
s	s	k7c7
ním	on	k3xPp3gInSc7
Mášeňka	Mášeňka	k1gFnSc1
odjede	odjet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bojích	boj	k1gInPc6
byl	být	k5eAaImAgInS
raněn	raněn	k2eAgInSc1d1
<g/>
,	,	kIx,
emigroval	emigrovat	k5eAaBmAgInS
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
ji	on	k3xPp3gFnSc4
neviděl	vidět	k5eNaImAgMnS
ani	ani	k8xC
si	se	k3xPyFc3
s	s	k7c7
ní	on	k3xPp3gFnSc7
nepsal	psát	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mášeňka	Mášeňka	k1gFnSc1
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
znamená	znamenat	k5eAaImIp3nS
celé	celý	k2eAgNnSc4d1
Rusko	Rusko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opije	opít	k5eAaPmIp3nS
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
na	na	k7c4
ni	on	k3xPp3gFnSc4
ráno	ráno	k6eAd1
zapomněl	zapomnět	k5eAaImAgMnS,k5eAaPmAgMnS
a	a	k8xC
nejel	jet	k5eNaImAgMnS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
k	k	k7c3
vlaku	vlak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfjorov	Alfjorov	k1gInSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
opilosti	opilost	k1gFnSc6
přiznává	přiznávat	k5eAaImIp3nS
k	k	k7c3
nevěře	nevěra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganin	Ganina	k1gFnPc2
nakonec	nakonec	k6eAd1
ale	ale	k8xC
nejede	jet	k5eNaImIp3nS
na	na	k7c4
nádraží	nádraží	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
odjíždí	odjíždět	k5eAaImIp3nS
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Básník	básník	k1gMnSc1
umírá	umírat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpomínky	vzpomínka	k1gFnPc4
na	na	k7c4
Mášeňku	Mášeňka	k1gFnSc4
Ganin	Ganina	k1gFnPc2
nechává	nechávat	k5eAaImIp3nS
v	v	k7c6
domě	dům	k1gInSc6
společně	společně	k6eAd1
s	s	k7c7
umírajícím	umírající	k1gMnSc7
básníkem	básník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Postavy	postava	k1gFnPc1
</s>
<s>
Lev	Lev	k1gMnSc1
Glebovič	Glebovič	k1gMnSc1
Ganin	Ganin	k1gMnSc1
<g/>
:	:	kIx,
ruský	ruský	k2eAgMnSc1d1
emigrant	emigrant	k1gMnSc1
pobývající	pobývající	k2eAgMnSc1d1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
život	život	k1gInSc1
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
pohybu	pohyb	k1gInSc2
až	až	k9
se	s	k7c7
vzpomínkami	vzpomínka	k1gFnPc7
na	na	k7c4
dávnou	dávný	k2eAgFnSc4d1
lásku	láska	k1gFnSc4
z	z	k7c2
dětství	dětství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mášeňka	Mášeňka	k1gFnSc1
<g/>
:	:	kIx,
Ganinova	Ganinův	k2eAgFnSc1d1
láska	láska	k1gFnSc1
z	z	k7c2
dětství	dětství	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
románu	román	k1gInSc6
nevystupuje	vystupovat	k5eNaImIp3nS
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
ní	on	k3xPp3gFnSc6
dozvídáme	dozvídat	k5eAaImIp1nP
z	z	k7c2
Ganinových	Ganinův	k2eAgFnPc2d1
vzpomínek	vzpomínka	k1gFnPc2
a	a	k8xC
prostřednictvím	prostřednictví	k1gNnSc7
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Alexej	Alexej	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Alfjorov	Alfjorov	k1gInSc1
<g/>
:	:	kIx,
manžel	manžel	k1gMnSc1
Mášeňky	Mášeňka	k1gFnSc2
<g/>
,	,	kIx,
netrpělivě	trpělivě	k6eNd1
očekává	očekávat	k5eAaImIp3nS
její	její	k3xOp3gInSc4
příjezd	příjezd	k1gInSc4
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
sám	sám	k3xTgMnSc1
před	před	k7c7
lety	léto	k1gNnPc7
emigroval	emigrovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Podťagin	Podťagin	k1gInSc1
<g/>
:	:	kIx,
stařičký	stařičký	k2eAgMnSc1d1
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
dostat	dostat	k5eAaPmF
z	z	k7c2
Berlína	Berlín	k1gInSc2
do	do	k7c2
Francie	Francie	k1gFnSc2
za	za	k7c7
svou	svůj	k3xOyFgFnSc7
neteří	neteř	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
emigraci	emigrace	k1gFnSc6
již	již	k6eAd1
netvoří	tvořit	k5eNaImIp3nS
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
umírá	umírat	k5eAaImIp3nS
v	v	k7c6
penzionu	penzion	k1gInSc6
pro	pro	k7c4
emigranty	emigrant	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Rozbor	rozbor	k1gInSc1
díla	dílo	k1gNnSc2
</s>
<s>
Mášeňka	Mášeňka	k1gFnSc1
je	být	k5eAaImIp3nS
Nabokovův	Nabokovův	k2eAgInSc4d1
první	první	k4xOgInSc4
román	román	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mášeňka	Mášeňka	k1gFnSc1
má	mít	k5eAaImIp3nS
patrnou	patrný	k2eAgFnSc4d1
autobiografickou	autobiografický	k2eAgFnSc4d1
osnovu	osnova	k1gFnSc4
a	a	k8xC
sám	sám	k3xTgMnSc1
Nabokov	Nabokov	k1gInSc4
přiznal	přiznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
prostřednictvím	prostřednictvím	k7c2
tohoto	tento	k3xDgInSc2
románu	román	k1gInSc2
chtěl	chtít	k5eAaImAgMnS
odpoutat	odpoutat	k5eAaPmF
sám	sám	k3xTgMnSc1
od	od	k7c2
sebe	sebe	k3xPyFc4
a	a	k8xC
nasytit	nasytit	k5eAaPmF
touhu	touha	k1gFnSc4
po	po	k7c6
první	první	k4xOgFnSc6
lásce	láska	k1gFnSc6
a	a	k8xC
ztracené	ztracený	k2eAgFnSc6d1
vlasti	vlast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tématem	téma	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
románové	románový	k2eAgFnSc2d1
prvotiny	prvotina	k1gFnSc2
je	být	k5eAaImIp3nS
emigrace	emigrace	k1gFnSc1
jako	jako	k8xC,k8xS
ztráta	ztráta	k1gFnSc1
domova	domov	k1gInSc2
<g/>
,	,	kIx,
jistot	jistota	k1gFnPc2
a	a	k8xC
pevného	pevný	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
vlastní	vlastní	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekonstrukce	rekonstrukce	k1gFnSc1
milostného	milostný	k2eAgInSc2d1
románu	román	k1gInSc2
vytrhuje	vytrhovat	k5eAaImIp3nS
hlavního	hlavní	k2eAgMnSc4d1
hrdinu	hrdina	k1gMnSc4
románu	román	k1gInSc2
Ganina	Ganina	k1gFnSc1
z	z	k7c2
letargie	letargie	k1gFnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
přináší	přinášet	k5eAaImIp3nS
poznání	poznání	k1gNnSc4
vedoucí	vedoucí	k1gFnSc2
k	k	k7c3
opuštění	opuštění	k1gNnSc3
ideálů	ideál	k1gInPc2
mladosti	mladost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
dává	dávat	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc3
životu	život	k1gInSc3
smysl	smysl	k1gInSc4
a	a	k8xC
otevírá	otevírat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
dveře	dveře	k1gFnPc4
do	do	k7c2
budoucnosti	budoucnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
románu	román	k1gInSc2
je	být	k5eAaImIp3nS
do	do	k7c2
neslovanských	slovanský	k2eNgInPc2d1
jazyků	jazyk	k1gInPc2
těžko	těžko	k6eAd1
přeložitelný	přeložitelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titulní	titulní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
není	být	k5eNaImIp3nS
fyzicky	fyzicky	k6eAd1
přítomna	přítomen	k2eAgFnSc1d1
v	v	k7c6
ději	děj	k1gInSc6
<g/>
,	,	kIx,
figuruje	figurovat	k5eAaImIp3nS
zde	zde	k6eAd1
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
vzpomínek	vzpomínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mášeňka	Mášeňka	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
literární	literární	k2eAgFnSc1d1
maska	maska	k1gFnSc1
a	a	k8xC
zástupný	zástupný	k2eAgInSc1d1
symbol	symbol	k1gInSc1
poetizovaného	poetizovaný	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
nechybí	chybět	k5eNaImIp3nS,k5eNaPmIp3nS
ironická	ironický	k2eAgFnSc1d1
tónina	tónina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NABOKOV	NABOKOV	kA
<g/>
,	,	kIx,
Vladimir	Vladimir	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mášeňka	Mášeňka	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
114	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
939	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
