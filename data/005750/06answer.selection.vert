<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
studia	studio	k1gNnPc4	studio
Cinecittá	Cinecittý	k2eAgNnPc4d1	Cinecittý
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnPc4d3	veliký
zázemí	zázemí	k1gNnPc4	zázemí
televizní	televizní	k2eAgFnSc2d1	televizní
a	a	k8xC	a
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
také	také	k9	také
centrum	centrum	k1gNnSc1	centrum
Italské	italský	k2eAgFnSc2d1	italská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
natáčí	natáčet	k5eAaImIp3nS	natáčet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
současných	současný	k2eAgInPc2d1	současný
kasovních	kasovní	k2eAgInPc2d1	kasovní
trháků	trhák	k1gInPc2	trhák
<g/>
.	.	kIx.	.
</s>
