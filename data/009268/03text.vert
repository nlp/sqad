<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
světle	světlo	k1gNnSc6	světlo
modrého	modré	k1gNnSc2	modré
<g/>
,	,	kIx,	,
bílého	bílý	k2eAgNnSc2d1	bílé
a	a	k8xC	a
zeleného	zelený	k2eAgNnSc2d1	zelené
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
oddělené	oddělený	k2eAgFnPc4d1	oddělená
úzkými	úzký	k2eAgInPc7d1	úzký
červenými	červený	k2eAgInPc7d1	červený
proužky	proužek	k1gInPc7	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světle	světle	k6eAd1	světle
modrém	modrý	k2eAgInSc6d1	modrý
pruhu	pruh	k1gInSc6	pruh
je	být	k5eAaImIp3nS	být
vlevo	vlevo	k6eAd1	vlevo
umístěný	umístěný	k2eAgInSc4d1	umístěný
půlměsíc	půlměsíc	k1gInSc4	půlměsíc
a	a	k8xC	a
vedle	vedle	k6eAd1	vedle
dvanáct	dvanáct	k4xCc4	dvanáct
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
<g/>
Současná	současný	k2eAgFnSc1d1	současná
vlajka	vlajka	k1gFnSc1	vlajka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
úpravou	úprava	k1gFnSc7	úprava
vlajky	vlajka	k1gFnSc2	vlajka
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Uzbecké	uzbecký	k2eAgFnSc2d1	Uzbecká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrat	k5eAaImIp3nS	modrat
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
Timura	Timura	k1gFnSc1	Timura
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Tímura	Tímura	k1gFnSc1	Tímura
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
známého	známý	k2eAgInSc2d1	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Tamerlán	Tamerlán	k2eAgMnSc1d1	Tamerlán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vládce	vládce	k1gMnSc2	vládce
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
říše	říš	k1gFnSc2	říš
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
míru	mír	k1gInSc2	mír
a	a	k8xC	a
zelená	zelená	k1gFnSc1	zelená
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
vegetaci	vegetace	k1gFnSc4	vegetace
místní	místní	k2eAgFnSc2d1	místní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červenat	k5eAaImIp3nS	červenat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
tenkých	tenký	k2eAgInPc6d1	tenký
pruzích	pruh	k1gInPc6	pruh
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
životaschopnosti	životaschopnost	k1gFnSc2	životaschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
hvězd	hvězda	k1gFnPc2	hvězda
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
měsíce	měsíc	k1gInPc4	měsíc
islámského	islámský	k2eAgInSc2d1	islámský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
je	být	k5eAaImIp3nS	být
vyjádřením	vyjádření	k1gNnSc7	vyjádření
muslimské	muslimský	k2eAgFnSc2d1	muslimská
víry	víra	k1gFnSc2	víra
obyvatel	obyvatel	k1gMnPc2	obyvatel
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Uzbekistanu	Uzbekistan	k1gInSc2	Uzbekistan
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
</s>
</p>
<p>
<s>
Uzbecká	uzbecký	k2eAgFnSc1d1	Uzbecká
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
