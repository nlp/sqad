<s>
Boloňská	boloňský	k2eAgFnSc1d1	Boloňská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Alma	alma	k1gFnSc1	alma
Mater	Matra	k1gFnPc2	Matra
Studiorum	Studiorum	k1gInSc4	Studiorum
Università	Università	k1gFnSc4	Università
di	di	k?	di
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gMnSc1	Universitas
Bononiensis	Bononiensis	k1gFnSc2	Bononiensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
městě	město	k1gNnSc6	město
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
.	.	kIx.	.
</s>
