<p>
<s>
Boloňská	boloňský	k2eAgFnSc1d1	Boloňská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Alma	alma	k1gFnSc1	alma
Mater	Matra	k1gFnPc2	Matra
Studiorum	Studiorum	k1gInSc4	Studiorum
Università	Università	k1gFnSc4	Università
di	di	k?	di
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Universitas	Universitas	k1gMnSc1	Universitas
Bononiensis	Bononiensis	k1gFnSc2	Bononiensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
městě	město	k1gNnSc6	město
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
pramenů	pramen	k1gInPc2	pramen
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
roku	rok	k1gInSc6	rok
1088	[number]	k4	1088
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
údaje	údaj	k1gInPc1	údaj
uvádí	uvádět	k5eAaImIp3nP	uvádět
rok	rok	k1gInSc4	rok
1119	[number]	k4	1119
<g/>
,	,	kIx,	,
i	i	k8xC	i
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
univerzitou	univerzita	k1gFnSc7	univerzita
nejstarší	starý	k2eAgFnSc1d3	nejstarší
<g/>
.	.	kIx.	.
<g/>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
známa	znám	k2eAgFnSc1d1	známa
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
světského	světský	k2eAgNnSc2d1	světské
a	a	k8xC	a
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
000	[number]	k4	000
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
během	během	k7c2	během
akademického	akademický	k2eAgInSc2d1	akademický
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
ze	z	k7c2	z
400	[number]	k4	400
000	[number]	k4	000
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
na	na	k7c6	na
23	[number]	k4	23
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Coimbra	Coimbra	k1gMnSc1	Coimbra
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
asociace	asociace	k1gFnSc1	asociace
sdružující	sdružující	k2eAgFnSc2d1	sdružující
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
evropské	evropský	k2eAgFnSc2d1	Evropská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
absolventi	absolvent	k1gMnPc1	absolvent
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
absolventy	absolvent	k1gMnPc4	absolvent
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
Dante	Dante	k1gMnSc1	Dante
Alighieri	Alighier	k1gFnSc2	Alighier
<g/>
,	,	kIx,	,
Francesco	Francesco	k1gMnSc1	Francesco
Petrarca	Petrarca	k1gMnSc1	Petrarca
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
,	,	kIx,	,
Guglielmo	Guglielma	k1gFnSc5	Guglielma
Marconi	Marcoň	k1gFnSc5	Marcoň
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Becket	Becket	k1gMnSc1	Becket
a	a	k8xC	a
Umberto	Umberta	k1gFnSc5	Umberta
Eco	Eco	k1gMnSc7	Eco
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boloňská	boloňský	k2eAgFnSc1d1	Boloňská
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
