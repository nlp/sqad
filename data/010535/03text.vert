<p>
<s>
Rozvodí	rozvodí	k1gNnSc1	rozvodí
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
sousedícími	sousedící	k2eAgNnPc7d1	sousedící
povodími	povodí	k1gNnPc7	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodí	rozvodí	k1gNnSc6	rozvodí
vede	vést	k5eAaImIp3nS	vést
rozvodnice	rozvodnice	k1gFnSc1	rozvodnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
topografických	topografický	k2eAgInPc6d1	topografický
vrcholech	vrchol	k1gInPc6	vrchol
a	a	k8xC	a
horských	horský	k2eAgInPc6d1	horský
hřebenech	hřeben	k1gInPc6	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
rozvodí	rozvodí	k1gNnSc4	rozvodí
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
neznatelné	znatelný	k2eNgFnPc1d1	neznatelná
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
rozvodí	rozvodí	k1gNnPc1	rozvodí
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kontinentální	kontinentální	k2eAgNnPc1d1	kontinentální
rozvodí	rozvodí	k1gNnPc1	rozvodí
<g/>
.	.	kIx.	.
</s>
<s>
Rozvodí	rozvodí	k1gNnPc1	rozvodí
jsou	být	k5eAaImIp3nP	být
geograficky	geograficky	k6eAd1	geograficky
důležité	důležitý	k2eAgInPc4d1	důležitý
body	bod	k1gInPc4	bod
a	a	k8xC	a
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
politické	politický	k2eAgFnPc4d1	politická
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
pomyslné	pomyslný	k2eAgFnPc1d1	pomyslná
čáry	čára	k1gFnPc1	čára
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
povodími	povodí	k1gNnPc7	povodí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvodí	rozvodí	k1gNnPc1	rozvodí
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
směry	směr	k1gInPc4	směr
toků	tok	k1gInPc2	tok
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
povodí	povodí	k1gNnPc1	povodí
propojují	propojovat	k5eAaImIp3nP	propojovat
pomocí	pomocí	k7c2	pomocí
uměle	uměle	k6eAd1	uměle
vybudovaných	vybudovaný	k2eAgInPc2d1	vybudovaný
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozvodí	rozvodí	k1gNnSc1	rozvodí
Česka	Česko	k1gNnSc2	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Územím	území	k1gNnSc7	území
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
procházejí	procházet	k5eAaImIp3nP	procházet
tři	tři	k4xCgNnPc4	tři
hlavní	hlavní	k2eAgNnPc4d1	hlavní
evropská	evropský	k2eAgNnPc4d1	Evropské
rozvodí	rozvodí	k1gNnPc4	rozvodí
oddělující	oddělující	k2eAgFnSc2d1	oddělující
úmoří	úmoří	k1gNnSc2	úmoří
Severního	severní	k2eAgNnSc2d1	severní
<g/>
,	,	kIx,	,
Baltského	baltský	k2eAgNnSc2d1	Baltské
a	a	k8xC	a
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tři	tři	k4xCgNnPc1	tři
rozvodí	rozvodí	k1gNnPc1	rozvodí
se	se	k3xPyFc4	se
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Klepý	Klepý	k2eAgMnSc1d1	Klepý
(	(	kIx(	(
<g/>
1143	[number]	k4	1143
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Trójmorski	Trójmorske	k1gFnSc4	Trójmorske
Wierch	Wiercha	k1gFnPc2	Wiercha
<g/>
,	,	kIx,	,
ležícím	ležící	k2eAgNnSc6d1	ležící
na	na	k7c6	na
česko-polské	českoolský	k2eAgFnSc6d1	česko-polská
hranici	hranice	k1gFnSc6	hranice
v	v	k7c6	v
hřebeni	hřeben	k1gInSc6	hřeben
Králického	králický	k2eAgInSc2d1	králický
Sněžníku	Sněžník	k1gInSc2	Sněžník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
povodí	povodí	k1gNnPc1	povodí
</s>
</p>
<p>
<s>
bifurkace	bifurkace	k1gFnSc1	bifurkace
</s>
</p>
<p>
<s>
říční	říční	k2eAgNnPc1d1	říční
pirátství	pirátství	k1gNnPc1	pirátství
</s>
</p>
<p>
<s>
úmoří	úmoří	k1gNnSc1	úmoří
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rozvodí	rozvodí	k1gNnSc2	rozvodí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rozvodí	rozvodit	k5eAaImIp3nS	rozvodit
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
