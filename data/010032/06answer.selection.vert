<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Charles-Augustin	Charles-Augustin	k1gMnSc1	Charles-Augustin
de	de	k?	de
Coulomb	coulomb	k1gInSc1	coulomb
jej	on	k3xPp3gMnSc4	on
publikoval	publikovat	k5eAaBmAgInS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
tak	tak	k6eAd1	tak
základy	základ	k1gInPc4	základ
elektrostatiky	elektrostatika	k1gFnSc2	elektrostatika
<g/>
.	.	kIx.	.
</s>
