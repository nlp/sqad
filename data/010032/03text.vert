<p>
<s>
Coulombův	Coulombův	k2eAgInSc1d1	Coulombův
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
zákon	zákon	k1gInSc1	zákon
popisující	popisující	k2eAgFnSc2d1	popisující
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
mezi	mezi	k7c7	mezi
elektricky	elektricky	k6eAd1	elektricky
nabitými	nabitý	k2eAgFnPc7d1	nabitá
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Charles-Augustin	Charles-Augustin	k1gMnSc1	Charles-Augustin
de	de	k?	de
Coulomb	coulomb	k1gInSc1	coulomb
jej	on	k3xPp3gMnSc4	on
publikoval	publikovat	k5eAaBmAgInS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
tak	tak	k6eAd1	tak
základy	základ	k1gInPc4	základ
elektrostatiky	elektrostatika	k1gFnSc2	elektrostatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
elektrické	elektrický	k2eAgFnSc2d1	elektrická
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
působí	působit	k5eAaImIp3nP	působit
dvě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
velikosti	velikost	k1gFnSc2	velikost
nábojů	náboj	k1gInPc2	náboj
Q	Q	kA	Q
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Q2	Q2	k1gFnSc1	Q2
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc6	mocnina
jejich	jejich	k3xOp3gFnPc4	jejich
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
r.	r.	kA	r.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ε	ε	k?	ε
<g/>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
permitivita	permitivita	k1gFnSc1	permitivita
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
a	a	k8xC	a
ε	ε	k?	ε
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
<g/>
.	.	kIx.	.
<g/>
Ekvivalentní	ekvivalentní	k2eAgInSc1d1	ekvivalentní
s	s	k7c7	s
Coulombovým	Coulombův	k2eAgInSc7d1	Coulombův
zákonem	zákon	k1gInSc7	zákon
je	být	k5eAaImIp3nS	být
Gaussův	Gaussův	k2eAgInSc1d1	Gaussův
zákon	zákon	k1gInSc1	zákon
elektrostatiky	elektrostatika	k1gFnSc2	elektrostatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
principu	princip	k1gInSc2	princip
superpozice	superpozice	k1gFnSc2	superpozice
lze	lze	k6eAd1	lze
Coulombův	Coulombův	k2eAgInSc1d1	Coulombův
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
bodový	bodový	k2eAgInSc4d1	bodový
náboj	náboj	k1gInSc4	náboj
q	q	k?	q
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
r	r	kA	r
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
N	N	kA	N
bodových	bodový	k2eAgInPc2d1	bodový
nábojů	náboj	k1gInPc2	náboj
Qi	Qi	k1gFnSc2	Qi
zapsat	zapsat	k5eAaPmF	zapsat
vektorovým	vektorový	k2eAgInSc7d1	vektorový
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
q	q	k?	q
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
Q_	Q_	k1gFnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Elektřina	elektřina	k1gFnSc1	elektřina
</s>
</p>
<p>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
</s>
</p>
