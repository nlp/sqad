<s>
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
(	(	kIx(	(
<g/>
136199	[number]	k4	136199
<g/>
)	)	kIx)	)
Eris	Eris	k1gFnSc1	Eris
I	i	k8xC	i
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
přirozený	přirozený	k2eAgInSc1d1	přirozený
satelit	satelit	k1gInSc1	satelit
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
Eris	Eris	k1gFnSc1	Eris
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
známá	známý	k2eAgFnSc1d1	známá
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objeven	objevit	k5eAaPmNgInS	objevit
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
týmem	tým	k1gInSc7	tým
vedeným	vedený	k2eAgMnSc7d1	vedený
americkým	americký	k2eAgMnSc7d1	americký
astronomem	astronom	k1gMnSc7	astronom
Michaelem	Michael	k1gMnSc7	Michael
Brownem	Brown	k1gMnSc7	Brown
na	na	k7c6	na
W.	W.	kA	W.
M.	M.	kA	M.
Keck	Keck	k1gMnSc1	Keck
Observatory	Observator	k1gMnPc4	Observator
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
obě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
dostala	dostat	k5eAaPmAgNnP	dostat
svůj	svůj	k3xOyFgInSc4	svůj
definitivní	definitivní	k2eAgInSc4d1	definitivní
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
nesla	nést	k5eAaImAgFnS	nést
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
předběžné	předběžný	k2eAgNnSc4d1	předběžné
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
UB	UB	kA	UB
<g/>
313	[number]	k4	313
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Eris	Eris	k1gFnSc1	Eris
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
řecké	řecký	k2eAgFnSc6d1	řecká
bohyni	bohyně	k1gFnSc6	bohyně
sváru	svár	k1gInSc2	svár
a	a	k8xC	a
Dysnomia	Dysnomium	k1gNnSc2	Dysnomium
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
dceři	dcera	k1gFnSc6	dcera
<g/>
,	,	kIx,	,
bohyni	bohyně	k1gFnSc6	bohyně
anarchie	anarchie	k1gFnSc2	anarchie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
astronomové	astronom	k1gMnPc1	astronom
Keckovy	Keckův	k2eAgFnSc2d1	Keckova
observatoře	observatoř	k1gFnSc2	observatoř
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
nově	nově	k6eAd1	nově
instalovaného	instalovaný	k2eAgInSc2d1	instalovaný
systému	systém	k1gInSc2	systém
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
optiky	optika	k1gFnSc2	optika
čtyři	čtyři	k4xCgNnPc4	čtyři
nejjasnější	jasný	k2eAgNnPc4d3	nejjasnější
transneptunická	transneptunický	k2eAgNnPc4d1	transneptunické
tělesa	těleso	k1gNnPc4	těleso
(	(	kIx(	(
<g/>
Pluto	plut	k2eAgNnSc4d1	Pluto
<g/>
,	,	kIx,	,
Makemake	Makemake	k1gNnSc4	Makemake
<g/>
,	,	kIx,	,
Haumeu	Haumea	k1gFnSc4	Haumea
a	a	k8xC	a
Eris	Eris	k1gFnSc1	Eris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Eris	Eris	k1gFnSc1	Eris
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
předběžné	předběžný	k2eAgNnSc4d1	předběžné
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
UB	UB	kA	UB
<g/>
313	[number]	k4	313
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Eris	Eris	k1gFnSc1	Eris
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
neměla	mít	k5eNaImAgNnP	mít
ještě	ještě	k6eAd1	ještě
definitivní	definitivní	k2eAgNnSc4d1	definitivní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
ji	on	k3xPp3gFnSc4	on
běžně	běžně	k6eAd1	běžně
nazývali	nazývat	k5eAaImAgMnP	nazývat
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
Xena	Xena	k1gFnSc1	Xena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
válečnice	válečnice	k1gFnSc2	válečnice
z	z	k7c2	z
oblíbeného	oblíbený	k2eAgInSc2d1	oblíbený
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
pak	pak	k6eAd1	pak
objevitelé	objevitel	k1gMnPc1	objevitel
nový	nový	k2eAgInSc4d1	nový
měsíc	měsíc	k1gInSc4	měsíc
označovali	označovat	k5eAaImAgMnP	označovat
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
Gabrielle	Gabrielle	k1gFnSc1	Gabrielle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
seriálové	seriálový	k2eAgFnSc2d1	seriálová
společnice	společnice	k1gFnSc2	společnice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
měření	měření	k1gNnPc2	měření
je	být	k5eAaImIp3nS	být
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
o	o	k7c4	o
4,43	[number]	k4	4,43
magnitudy	magnitudy	k6eAd1	magnitudy
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
by	by	kYmCp3nS	by
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
průměr	průměr	k1gInSc4	průměr
350	[number]	k4	350
až	až	k9	až
490	[number]	k4	490
km	km	kA	km
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Michael	Michael	k1gMnSc1	Michael
Brown	Brown	k1gMnSc1	Brown
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
jasnost	jasnost	k1gFnSc1	jasnost
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgMnSc1d2	nižší
a	a	k8xC	a
průměr	průměr	k1gInSc1	průměr
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
satelit	satelit	k1gInSc1	satelit
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
nejen	nejen	k6eAd1	nejen
Keckovými	Keckův	k2eAgInPc7d1	Keckův
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
a	a	k8xC	a
svá	svůj	k3xOyFgNnPc4	svůj
pozorování	pozorování	k1gNnPc4	pozorování
obíhajícího	obíhající	k2eAgNnSc2d1	obíhající
tělesa	těleso	k1gNnSc2	těleso
využili	využít	k5eAaPmAgMnP	využít
jak	jak	k6eAd1	jak
k	k	k7c3	k
odhadu	odhad	k1gInSc3	odhad
charakteristik	charakteristika	k1gFnPc2	charakteristika
jeho	jeho	k3xOp3gFnSc2	jeho
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k9	také
k	k	k7c3	k
výpočtu	výpočet	k1gInSc3	výpočet
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Eris	Eris	k1gFnSc1	Eris
(	(	kIx(	(
<g/>
1,67	[number]	k4	1,67
<g/>
×	×	k?	×
<g/>
1022	[number]	k4	1022
kg	kg	kA	kg
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1,27	[number]	k4	1,27
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dysmonia	Dysmonium	k1gNnPc1	Dysmonium
se	se	k3xPyFc4	se
kolem	kolem	k6eAd1	kolem
Eris	Eris	k1gFnSc1	Eris
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
kruhové	kruhový	k2eAgFnSc6d1	kruhová
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
37	[number]	k4	37
350	[number]	k4	350
±	±	k?	±
140	[number]	k4	140
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
nalezli	naleznout	k5eAaPmAgMnP	naleznout
satelity	satelit	k1gInPc4	satelit
na	na	k7c6	na
oběžných	oběžný	k2eAgFnPc6d1	oběžná
dráhách	dráha	k1gFnPc6	dráha
třech	tři	k4xCgInPc2	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
nejjasnějších	jasný	k2eAgNnPc2d3	nejjasnější
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
méně	málo	k6eAd2	málo
jasných	jasný	k2eAgInPc2d1	jasný
členů	člen	k1gInPc2	člen
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
však	však	k9	však
zatím	zatím	k6eAd1	zatím
byly	být	k5eAaImAgInP	být
satelity	satelit	k1gInPc1	satelit
nalezeny	nalézt	k5eAaBmNgInP	nalézt
jen	jen	k9	jen
v	v	k7c4	v
10	[number]	k4	10
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgInSc2	tento
nepoměru	nepoměr	k1gInSc2	nepoměr
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
kolize	kolize	k1gFnPc1	kolize
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgNnPc7d1	velké
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bývaly	bývat	k5eAaImAgFnP	bývat
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc4	srážka
těles	těleso	k1gNnPc2	těleso
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
přesahujícím	přesahující	k2eAgInSc6d1	přesahující
1000	[number]	k4	1000
km	km	kA	km
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
vymrštění	vymrštění	k1gNnSc3	vymrštění
velkého	velký	k2eAgInSc2d1	velký
objemu	objem	k1gInSc2	objem
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
shlukl	shluknout	k5eAaPmAgMnS	shluknout
v	v	k7c4	v
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgFnPc3	jaký
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
historii	historie	k1gFnSc6	historie
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
Měsíc	měsíc	k1gInSc1	měsíc
obíhající	obíhající	k2eAgInSc1d1	obíhající
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgMnS	vybrat
jméno	jméno	k1gNnSc4	jméno
Dysnomia	Dysnomius	k1gMnSc2	Dysnomius
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obecně	obecně	k6eAd1	obecně
přijatého	přijatý	k2eAgInSc2d1	přijatý
zvyku	zvyk	k1gInSc2	zvyk
se	se	k3xPyFc4	se
měsíce	měsíc	k1gInPc1	měsíc
pojmenovávají	pojmenovávat	k5eAaImIp3nP	pojmenovávat
po	po	k7c6	po
méně	málo	k6eAd2	málo
významných	významný	k2eAgFnPc6d1	významná
bozích	bůh	k1gMnPc6	bůh
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
bohy	bůh	k1gMnPc7	bůh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
mateřské	mateřský	k2eAgNnSc1d1	mateřské
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
Dysnomia	Dysnomium	k1gNnSc2	Dysnomium
je	být	k5eAaImIp3nS	být
mýtickou	mýtický	k2eAgFnSc7d1	mýtická
dcerou	dcera	k1gFnSc7	dcera
bohyně	bohyně	k1gFnSc2	bohyně
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
níž	jenž	k3xRgFnSc2	jenž
měsíc	měsíc	k1gInSc4	měsíc
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Brown	Brown	k1gMnSc1	Brown
se	se	k3xPyFc4	se
však	však	k9	však
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
další	další	k2eAgInPc4d1	další
skryté	skrytý	k2eAgInPc4d1	skrytý
významy	význam	k1gInPc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgInSc1d1	řecký
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
dysnomia	dysnomia	k1gFnSc1	dysnomia
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládaný	překládaný	k2eAgInSc4d1	překládaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
anarchie	anarchie	k1gFnSc1	anarchie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vykládat	vykládat	k5eAaImF	vykládat
jako	jako	k9	jako
nedostatek	nedostatek	k1gInSc4	nedostatek
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
lawlessness	lawlessness	k6eAd1	lawlessness
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
přezdívka	přezdívka	k1gFnSc1	přezdívka
mateřského	mateřský	k2eAgNnSc2d1	mateřské
tělesa	těleso	k1gNnSc2	těleso
Eris	Eris	k1gFnSc1	Eris
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Xena	Xena	k1gMnSc1	Xena
<g/>
"	"	kIx"	"
po	po	k7c6	po
známé	známý	k2eAgFnSc6d1	známá
seriálové	seriálový	k2eAgFnSc6d1	seriálová
bojovnici	bojovnice	k1gFnSc6	bojovnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
poctou	pocta	k1gFnSc7	pocta
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
herečku	herečka	k1gFnSc4	herečka
Lucy	Luca	k1gFnSc2	Luca
Lawlessovou	Lawlessová	k1gFnSc7	Lawlessová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
její	její	k3xOp3gFnSc4	její
postavu	postava	k1gFnSc4	postava
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
<g/>
.	.	kIx.	.
</s>
<s>
Brown	Brown	k1gMnSc1	Brown
rovněž	rovněž	k9	rovněž
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
hrál	hrát	k5eAaImAgInS	hrát
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
Dysnomia	Dysnomium	k1gNnSc2	Dysnomium
začíná	začínat	k5eAaImIp3nS	začínat
stejným	stejný	k2eAgNnSc7d1	stejné
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Diane	Dian	k1gInSc5	Dian
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
první	první	k4xOgNnPc1	první
písmena	písmeno	k1gNnPc1	písmeno
Pluta	plut	k2eAgNnPc1d1	Pluto
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
iniciálám	iniciála	k1gFnPc3	iniciála
Pervicala	Pervicala	k1gFnSc2	Pervicala
Lowella	Lowella	k1gMnSc1	Lowella
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
její	její	k3xOp3gMnSc1	její
objevitel	objevitel	k1gMnSc1	objevitel
Clyde	Clyd	k1gInSc5	Clyd
Tombaugh	Tombaugh	k1gInSc4	Tombaugh
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
první	první	k4xOgNnPc4	první
písmena	písmeno	k1gNnPc4	písmeno
Charona	Charon	k1gMnSc2	Charon
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
začátku	začátek	k1gInSc3	začátek
křestního	křestní	k2eAgNnSc2d1	křestní
jména	jméno	k1gNnSc2	jméno
Charlene	Charlen	k1gInSc5	Charlen
<g/>
,	,	kIx,	,
jména	jméno	k1gNnSc2	jméno
manželky	manželka	k1gFnSc2	manželka
jeho	jeho	k3xOp3gMnSc2	jeho
objevitele	objevitel	k1gMnSc2	objevitel
Jamese	Jamese	k1gFnSc2	Jamese
Christyho	Christy	k1gMnSc2	Christy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
(	(	kIx(	(
<g/>
moon	moon	k1gInSc1	moon
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
