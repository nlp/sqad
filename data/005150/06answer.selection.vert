<s>
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
(	(	kIx(	(
<g/>
136199	[number]	k4	136199
<g/>
)	)	kIx)	)
Eris	Eris	k1gFnSc1	Eris
I	i	k8xC	i
Dysnomia	Dysnomia	k1gFnSc1	Dysnomia
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
přirozený	přirozený	k2eAgInSc1d1	přirozený
satelit	satelit	k1gInSc1	satelit
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
Eris	Eris	k1gFnSc1	Eris
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
známá	známý	k2eAgFnSc1d1	známá
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
