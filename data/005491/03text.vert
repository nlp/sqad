<s>
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruské	ruský	k2eAgNnSc1d1	ruské
město	město	k1gNnSc1	město
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ob	Ob	k1gInSc4	Ob
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Západosibiřské	západosibiřský	k2eAgFnSc2d1	Západosibiřská
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
administrativní	administrativní	k2eAgNnSc1d1	administrativní
centrum	centrum	k1gNnSc1	centrum
Sibiřského	sibiřský	k2eAgInSc2d1	sibiřský
federálního	federální	k2eAgInSc2d1	federální
okruhu	okruh	k1gInSc2	okruh
a	a	k8xC	a
Novosibirské	novosibirský	k2eAgFnSc2d1	Novosibirská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
třetí	třetí	k4xOgNnSc1	třetí
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Sibiře	Sibiř	k1gFnSc2	Sibiř
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
obchodním	obchodní	k2eAgInSc7d1	obchodní
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgInSc7d1	kulturní
<g/>
,	,	kIx,	,
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
<g/>
,	,	kIx,	,
dopravním	dopravní	k2eAgInSc7d1	dopravní
a	a	k8xC	a
vědeckým	vědecký	k2eAgInSc7d1	vědecký
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
teprve	teprve	k9	teprve
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
Transsibiřské	transsibiřský	k2eAgFnSc2d1	Transsibiřská
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
znak	znak	k1gInSc1	znak
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
vzniknul	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
drobnými	drobný	k2eAgFnPc7d1	drobná
úpravami	úprava	k1gFnPc7	úprava
znaku	znak	k1gInSc2	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zeleno-stříbrným	zelenotříbrný	k2eAgInSc7d1	zeleno-stříbrný
heraldickým	heraldický	k2eAgInSc7d1	heraldický
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
rozděleným	rozdělený	k2eAgInSc7d1	rozdělený
šikmou	šikmý	k2eAgFnSc7d1	šikmá
modrou	modrý	k2eAgFnSc7d1	modrá
linkou	linka	k1gFnSc7	linka
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnSc4d1	představující
řeku	řeka	k1gFnSc4	řeka
Ob.	Ob.	k1gFnSc2	Ob.
Zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
stříbrno-zelená	stříbrnoelený	k2eAgFnSc1d1	stříbrno-zelený
tenká	tenký	k2eAgFnSc1d1	tenká
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgFnSc4d1	znamenající
Transsibiřskou	transsibiřský	k2eAgFnSc4d1	Transsibiřská
magistrálu	magistrála	k1gFnSc4	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
půlkruh	půlkruh	k1gInSc1	půlkruh
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
železniční	železniční	k2eAgInSc1d1	železniční
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Ob	Ob	k1gInSc4	Ob
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgMnSc3	který
město	město	k1gNnSc1	město
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
štítem	štít	k1gInSc7	štít
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hradební	hradební	k2eAgFnSc1d1	hradební
koruna	koruna	k1gFnSc1	koruna
s	s	k7c7	s
pěti	pět	k4xCc7	pět
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Štítonoši	štítonoš	k1gMnPc1	štítonoš
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
černí	černý	k2eAgMnPc1d1	černý
soboli	sobol	k1gMnPc1	sobol
s	s	k7c7	s
červenými	červený	k2eAgInPc7d1	červený
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
stříbrnými	stříbrný	k2eAgInPc7d1	stříbrný
nosy	nos	k1gInPc7	nos
<g/>
,	,	kIx,	,
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
spáry	spár	k1gInPc7	spár
<g/>
.	.	kIx.	.
</s>
<s>
Podnoží	podnoží	k1gNnSc1	podnoží
je	být	k5eAaImIp3nS	být
tvořeno	tvořen	k2eAgNnSc1d1	tvořeno
stužkou	stužka	k1gFnSc7	stužka
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
motivem	motiv	k1gInSc7	motiv
jako	jako	k8xS	jako
na	na	k7c6	na
štítu	štít	k1gInSc6	štít
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
doplněno	doplnit	k5eAaPmNgNnS	doplnit
červeným	červený	k2eAgInSc7d1	červený
lukem	luk	k1gInSc7	luk
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
zkříženými	zkřížený	k2eAgInPc7d1	zkřížený
šípy	šíp	k1gInPc7	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Soboli	sobol	k1gMnPc1	sobol
a	a	k8xC	a
luk	luk	k1gInSc1	luk
s	s	k7c7	s
šípy	šíp	k1gInPc7	šíp
jsou	být	k5eAaImIp3nP	být
převzaty	převzít	k5eAaPmNgFnP	převzít
z	z	k7c2	z
historického	historický	k2eAgInSc2d1	historický
znaku	znak	k1gInSc2	znak
Sibiřské	sibiřský	k2eAgFnSc2d1	sibiřská
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
zeleným	zelený	k2eAgNnSc7d1	zelené
a	a	k8xC	a
bílým	bílý	k2eAgNnSc7d1	bílé
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
rozděleným	rozdělený	k2eAgInSc7d1	rozdělený
šikmou	šikmý	k2eAgFnSc7d1	šikmá
modrou	modrý	k2eAgFnSc7d1	modrá
linkou	linka	k1gFnSc7	linka
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnSc4d1	představující
řeku	řeka	k1gFnSc4	řeka
Ob.	Ob.	k1gMnPc2	Ob.
Zelené	Zelené	k2eAgNnSc1d1	Zelené
pole	pole	k1gNnSc1	pole
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
přírodní	přírodní	k2eAgNnSc4d1	přírodní
bohatství	bohatství	k1gNnSc4	bohatství
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgNnSc1d1	bílé
pole	pole	k1gNnSc1	pole
čistotu	čistota	k1gFnSc4	čistota
s	s	k7c7	s
sibiřské	sibiřský	k2eAgFnPc1d1	sibiřská
sněhy	sníh	k1gInPc4	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
ruským	ruský	k2eAgMnSc7d1	ruský
osídlením	osídlení	k1gNnSc7	osídlení
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
byl	být	k5eAaImAgInS	být
Nikolský	Nikolský	k2eAgInSc1d1	Nikolský
pogost	pogost	k1gInSc1	pogost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Krivoščokovo	Krivoščokův	k2eAgNnSc4d1	Krivoščokův
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Obu	Ob	k1gInSc2	Ob
<g/>
,	,	kIx,	,
asi	asi	k9	asi
3	[number]	k4	3
kilometry	kilometr	k1gInPc7	kilometr
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Iňy	Iňy	k1gFnSc2	Iňy
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
Brockhause	Brockhause	k1gFnSc2	Brockhause
a	a	k8xC	a
Efrona	Efron	k1gMnSc2	Efron
žilo	žít	k5eAaImAgNnS	žít
roku	rok	k1gInSc3	rok
1893	[number]	k4	1893
v	v	k7c6	v
Krivoščokově	Krivoščokův	k2eAgFnSc6d1	Krivoščokův
685	[number]	k4	685
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
fungovala	fungovat	k5eAaImAgFnS	fungovat
zde	zde	k6eAd1	zde
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
říční	říční	k2eAgNnPc1d1	říční
přístaviště	přístaviště	k1gNnPc1	přístaviště
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
padlo	padnout	k5eAaImAgNnS	padnout
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
přímo	přímo	k6eAd1	přímo
přes	přes	k7c4	přes
Krivoščokovo	Krivoščokův	k2eAgNnSc4d1	Krivoščokův
povede	vést	k5eAaImIp3nS	vést
Velká	velký	k2eAgFnSc1d1	velká
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
říkalo	říkat	k5eAaImAgNnS	říkat
Transsibiřské	transsibiřský	k2eAgFnSc3d1	Transsibiřská
magistrále	magistrála	k1gFnSc3	magistrála
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
odešla	odejít	k5eAaPmAgFnS	odejít
na	na	k7c4	na
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
Obu	Ob	k1gInSc2	Ob
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založili	založit	k5eAaPmAgMnP	založit
vesnici	vesnice	k1gFnSc4	vesnice
Krivoščokovskij	Krivoščokovskij	k1gFnSc2	Krivoščokovskij
Vysjolok	Vysjolok	k1gInSc1	Vysjolok
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
si	se	k3xPyFc3	se
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1893	[number]	k4	1893
rozbila	rozbít	k5eAaPmAgFnS	rozbít
tábor	tábor	k1gInSc4	tábor
první	první	k4xOgFnSc1	první
skupina	skupina	k1gFnSc1	skupina
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
stavět	stavět	k5eAaImF	stavět
železniční	železniční	k2eAgInSc4d1	železniční
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
datum	datum	k1gNnSc4	datum
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
vysídlenců	vysídlenec	k1gMnPc2	vysídlenec
brzy	brzy	k6eAd1	brzy
splynula	splynout	k5eAaPmAgNnP	splynout
s	s	k7c7	s
kolonií	kolonie	k1gFnSc7	kolonie
stavitelů	stavitel	k1gMnPc2	stavitel
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1904	[number]	k4	1904
potom	potom	k8xC	potom
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
nařídil	nařídit	k5eAaPmAgMnS	nařídit
vystavět	vystavět	k5eAaPmF	vystavět
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
strategickém	strategický	k2eAgNnSc6d1	strategické
místě	místo	k1gNnSc6	místo
neújezdní	újezdní	k2eNgNnSc1d1	újezdní
město	město	k1gNnSc1	město
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
carova	carův	k2eAgNnSc2d1	carovo
jména	jméno	k1gNnSc2	jméno
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1909	[number]	k4	1909
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
veliký	veliký	k2eAgInSc1d1	veliký
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Zničil	zničit	k5eAaPmAgMnS	zničit
794	[number]	k4	794
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bez	bez	k7c2	bez
střechy	střecha	k1gFnSc2	střecha
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
celkové	celkový	k2eAgFnSc2d1	celková
škody	škoda	k1gFnSc2	škoda
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
rublů	rubl	k1gInPc2	rubl
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
ještě	ještě	k6eAd1	ještě
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
epidemie	epidemie	k1gFnSc1	epidemie
tyfu	tyf	k1gInSc2	tyf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
zavedla	zavést	k5eAaPmAgFnS	zavést
místní	místní	k2eAgFnSc1d1	místní
samospráva	samospráva	k1gFnSc1	samospráva
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Ruském	ruský	k2eAgNnSc6d1	ruské
impériu	impérium	k1gNnSc6	impérium
zatím	zatím	k6eAd1	zatím
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Jaroslavli	Jaroslavli	k1gFnSc6	Jaroslavli
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
speciální	speciální	k2eAgFnSc1d1	speciální
komise	komise	k1gFnSc1	komise
řešila	řešit	k5eAaImAgFnS	řešit
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakého	jaký	k3yIgInSc2	jaký
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
Transsibiřské	transsibiřský	k2eAgFnSc6d1	Transsibiřská
magistrále	magistrála	k1gFnSc6	magistrála
vystavět	vystavět	k5eAaPmF	vystavět
odbočku	odbočka	k1gFnSc4	odbočka
na	na	k7c4	na
Altaj	Altaj	k1gInSc4	Altaj
do	do	k7c2	do
Semipalatinsku	Semipalatinsk	k1gInSc2	Semipalatinsk
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
Vladimir	Vladimir	k1gMnSc1	Vladimir
Žernakov	Žernakov	k1gInSc4	Žernakov
věnoval	věnovat	k5eAaPmAgMnS	věnovat
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
úsilí	úsilí	k1gNnSc4	úsilí
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
novou	nový	k2eAgFnSc7d1	nová
křižovatkou	křižovatka	k1gFnSc7	křižovatka
stalo	stát	k5eAaPmAgNnS	stát
právě	právě	k9	právě
jeho	jeho	k3xOp3gNnSc1	jeho
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
rok	rok	k1gInSc1	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
pro	pro	k7c4	pro
Novonikolajevsk	Novonikolajevsk	k1gInSc4	Novonikolajevsk
osudovým	osudový	k2eAgInSc7d1	osudový
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nezůstal	zůstat	k5eNaPmAgInS	zůstat
tuctovým	tuctový	k2eAgNnSc7d1	tuctové
městem	město	k1gNnSc7	město
založeným	založený	k2eAgNnSc7d1	založené
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
Magistrály	magistrála	k1gFnSc2	magistrála
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
změnil	změnit	k5eAaPmAgMnS	změnit
se	se	k3xPyFc4	se
ve	v	k7c4	v
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
dopravní	dopravní	k2eAgFnSc4d1	dopravní
křižovatku	křižovatka	k1gFnSc4	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
nastal	nastat	k5eAaPmAgInS	nastat
překotný	překotný	k2eAgInSc4d1	překotný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
trati	trať	k1gFnSc2	trať
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
-	-	kIx~	-
Semipalatinsk	Semipalatinsk	k1gInSc1	Semipalatinsk
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
ještě	ještě	k6eAd1	ještě
ani	ani	k8xC	ani
dokončena	dokončen	k2eAgFnSc1d1	dokončena
a	a	k8xC	a
již	již	k6eAd1	již
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
70	[number]	k4	70
tisíc	tisíc	k4xCgInSc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
fungovalo	fungovat	k5eAaImAgNnS	fungovat
7	[number]	k4	7
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
vysvěcená	vysvěcený	k2eAgFnSc1d1	vysvěcená
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
městského	městský	k2eAgNnSc2d1	Městské
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
Sovět	sovět	k1gInSc1	sovět
dělnických	dělnický	k2eAgMnPc2d1	dělnický
<g/>
,	,	kIx,	,
vojenských	vojenský	k2eAgMnPc2d1	vojenský
a	a	k8xC	a
rolnických	rolnický	k2eAgMnPc2d1	rolnický
delegátů	delegát	k1gMnPc2	delegát
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
vtrhnul	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
městské	městský	k2eAgFnSc2d1	městská
správy	správa	k1gFnSc2	správa
vojenský	vojenský	k2eAgInSc1d1	vojenský
oddíl	oddíl	k1gInSc1	oddíl
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Alexandra	Alexandr	k1gMnSc2	Alexandr
Iosifoviče	Iosifovič	k1gMnSc2	Iosifovič
Petuchova	Petuchův	k2eAgMnSc2d1	Petuchův
-	-	kIx~	-
moc	moc	k1gFnSc1	moc
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
převzal	převzít	k5eAaPmAgInS	převzít
místní	místní	k2eAgInSc1d1	místní
sovět	sovět	k1gInSc1	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
záhy	záhy	k6eAd1	záhy
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
rudý	rudý	k2eAgInSc1d1	rudý
teror	teror	k1gInSc1	teror
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nezaměřoval	zaměřovat	k5eNaImAgMnS	zaměřovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
buržoazii	buržoazie	k1gFnSc4	buržoazie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
bývalé	bývalý	k2eAgMnPc4d1	bývalý
politické	politický	k2eAgMnPc4d1	politický
spojence	spojenec	k1gMnPc4	spojenec
(	(	kIx(	(
<g/>
revolučně	revolučně	k6eAd1	revolučně
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nyní	nyní	k6eAd1	nyní
bolševikům	bolševik	k1gMnPc3	bolševik
překáželi	překážet	k5eAaImAgMnP	překážet
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1918	[number]	k4	1918
však	však	k9	však
obsadil	obsadit	k5eAaPmAgInS	obsadit
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
7	[number]	k4	7
<g/>
.	.	kIx.	.
střelecký	střelecký	k2eAgInSc1d1	střelecký
pluk	pluk	k1gInSc1	pluk
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
kapitána	kapitán	k1gMnSc2	kapitán
Radoly	Radola	k1gFnSc2	Radola
Gajdy	Gajda	k1gMnSc2	Gajda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bojové	bojový	k2eAgFnSc6d1	bojová
operaci	operace	k1gFnSc6	operace
jim	on	k3xPp3gMnPc3	on
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
také	také	k9	také
členové	člen	k1gMnPc1	člen
místního	místní	k2eAgInSc2d1	místní
protibolševického	protibolševický	k2eAgInSc2d1	protibolševický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
lidové	lidový	k2eAgNnSc1d1	lidové
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Tomsku	Tomsko	k1gNnSc6	Tomsko
svrhlo	svrhnout	k5eAaPmAgNnS	svrhnout
guberniální	guberniální	k2eAgInSc4d1	guberniální
bolševický	bolševický	k2eAgInSc4d1	bolševický
sovět	sovět	k1gInSc4	sovět
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
záměr	záměr	k1gInSc1	záměr
vytvořit	vytvořit	k5eAaPmF	vytvořit
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
Sibiřskou	sibiřský	k2eAgFnSc4d1	sibiřská
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
měl	mít	k5eAaImAgMnS	mít
stanout	stanout	k5eAaPmF	stanout
představitel	představitel	k1gMnSc1	představitel
protimonarchistické	protimonarchistický	k2eAgFnSc2d1	protimonarchistický
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
inteligence	inteligence	k1gFnSc2	inteligence
Grigorij	Grigorij	k1gMnSc1	Grigorij
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Potapin	Potapin	k1gMnSc1	Potapin
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Novonikolajevska	Novonikolajevsko	k1gNnSc2	Novonikolajevsko
plukovník	plukovník	k1gMnSc1	plukovník
Grišin-Almazov	Grišin-Almazov	k1gInSc1	Grišin-Almazov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
Západosibiřského	západosibiřský	k2eAgInSc2d1	západosibiřský
vojenského	vojenský	k2eAgInSc2d1	vojenský
okruhu	okruh	k1gInSc2	okruh
republiky	republika	k1gFnSc2	republika
Autonomní	autonomní	k2eAgFnSc4d1	autonomní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
správa	správa	k1gFnSc1	správa
Novonikolajevska	Novonikolajevsko	k1gNnSc2	Novonikolajevsko
nechala	nechat	k5eAaPmAgFnS	nechat
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1918	[number]	k4	1918
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
zastřelit	zastřelit	k5eAaPmF	zastřelit
představitele	představitel	k1gMnSc2	představitel
městského	městský	k2eAgInSc2d1	městský
bolševického	bolševický	k2eAgInSc2d1	bolševický
sovětu	sovět	k1gInSc2	sovět
za	za	k7c4	za
zvěrstva	zvěrstvo	k1gNnPc4	zvěrstvo
<g/>
,	,	kIx,	,
kterých	který	k3yQgNnPc2	který
se	se	k3xPyFc4	se
dopustili	dopustit	k5eAaPmAgMnP	dopustit
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
předal	předat	k5eAaPmAgInS	předat
Západosibiřský	západosibiřský	k2eAgInSc1d1	západosibiřský
komisariát	komisariát	k1gInSc1	komisariát
své	svůj	k3xOyFgFnSc2	svůj
kompetence	kompetence	k1gFnSc2	kompetence
Dočasné	dočasný	k2eAgFnSc3d1	dočasná
sibiřské	sibiřský	k2eAgFnSc3d1	sibiřská
vládě	vláda	k1gFnSc3	vláda
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Vologodského	Vologodský	k2eAgNnSc2d1	Vologodský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vládla	vládnout	k5eAaImAgFnS	vládnout
z	z	k7c2	z
Tomska	Tomsk	k1gInSc2	Tomsk
a	a	k8xC	a
Charbinu	Charbina	k1gFnSc4	Charbina
<g/>
.	.	kIx.	.
</s>
<s>
Vojensky	vojensky	k6eAd1	vojensky
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
město	město	k1gNnSc1	město
stále	stále	k6eAd1	stále
ovládali	ovládat	k5eAaImAgMnP	ovládat
českoslovenští	československý	k2eAgMnPc1d1	československý
legionáři	legionář	k1gMnPc1	legionář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1919	[number]	k4	1919
Autonomní	autonomní	k2eAgFnSc1d1	autonomní
Sibiř	Sibiř	k1gFnSc1	Sibiř
padla	padnout	k5eAaImAgFnS	padnout
<g/>
,	,	kIx,	,
vojska	vojsko	k1gNnSc2	vojsko
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Omsk	Omsk	k1gInSc4	Omsk
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
vojska	vojsko	k1gNnSc2	vojsko
Sibiřské	sibiřský	k2eAgFnSc2d1	sibiřská
armády	armáda	k1gFnSc2	armáda
složila	složit	k5eAaPmAgFnS	složit
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Tomsk	Tomsk	k1gInSc1	Tomsk
a	a	k8xC	a
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
padly	padnout	k5eAaImAgInP	padnout
po	po	k7c6	po
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1920	[number]	k4	1920
zbylo	zbýt	k5eAaPmAgNnS	zbýt
v	v	k7c6	v
Novonikolajevsku	Novonikolajevsko	k1gNnSc6	Novonikolajevsko
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nefungovaly	fungovat	k5eNaImAgInP	fungovat
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc1	žádný
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
vlaky	vlak	k1gInPc1	vlak
nejezdily	jezdit	k5eNaImAgInP	jezdit
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
hladověli	hladovět	k5eAaImAgMnP	hladovět
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
Leninem	Lenin	k1gMnSc7	Lenin
zřízený	zřízený	k2eAgMnSc1d1	zřízený
orgán	orgán	k1gMnSc1	orgán
vojenské	vojenský	k2eAgFnSc2d1	vojenská
diktatury	diktatura	k1gFnSc2	diktatura
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
Sibrevkom	Sibrevkom	k1gInSc1	Sibrevkom
a	a	k8xC	a
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
centrem	centrum	k1gNnSc7	centrum
Tomské	Tomský	k2eAgFnSc2d1	Tomský
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
západosibiřskému	západosibiřský	k2eAgInSc3d1	západosibiřský
povstání	povstání	k1gNnPc1	povstání
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
požadovalo	požadovat	k5eAaImAgNnS	požadovat
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
revoluci	revoluce	k1gFnSc4	revoluce
bez	bez	k7c2	bez
bolševiků	bolševik	k1gMnPc2	bolševik
a	a	k8xC	a
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Vážně	vážně	k6eAd1	vážně
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povstalci	povstalec	k1gMnPc1	povstalec
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
Sibrevkom	Sibrevkom	k1gInSc4	Sibrevkom
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
promptně	promptně	k6eAd1	promptně
evakuován	evakuovat	k5eAaBmNgInS	evakuovat
do	do	k7c2	do
Tomska	Tomsk	k1gInSc2	Tomsk
a	a	k8xC	a
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
status	status	k1gInSc4	status
guberniálního	guberniální	k2eAgNnSc2d1	guberniální
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Novonikolajevsk	Novonikolajevsk	k1gInSc1	Novonikolajevsk
však	však	k9	však
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
Celoruský	Celoruský	k2eAgInSc1d1	Celoruský
ústřední	ústřední	k2eAgInSc1d1	ústřední
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
RSFR	RSFR	kA	RSFR
proto	proto	k8xC	proto
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1921	[number]	k4	1921
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
Novonikolajevska	Novonikolajevsko	k1gNnSc2	Novonikolajevsko
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
dělnické	dělnický	k2eAgNnSc1d1	dělnické
administrativně-politické	administrativněolitický	k2eAgNnSc1d1	administrativně-politický
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
části	část	k1gFnSc2	část
Tomské	Tomský	k2eAgFnSc2d1	Tomský
a	a	k8xC	a
Omské	omský	k2eAgFnSc2d1	Omská
gubernie	gubernie	k1gFnSc2	gubernie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
Novonikolajevská	Novonikolajevský	k2eAgFnSc1d1	Novonikolajevský
gubernie	gubernie	k1gFnSc1	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgInS	přesunout
Sibrevkom	Sibrevkom	k1gInSc1	Sibrevkom
<g/>
,	,	kIx,	,
z	z	k7c2	z
Omska	Omsk	k1gInSc2	Omsk
byly	být	k5eAaImAgFnP	být
přestěhovány	přestěhovat	k5eAaPmNgInP	přestěhovat
orgány	orgán	k1gInPc1	orgán
Ruské	ruský	k2eAgFnSc2d1	ruská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
redakce	redakce	k1gFnSc2	redakce
novin	novina	k1gFnPc2	novina
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
Novonikolajevska	Novonikolajevsko	k1gNnSc2	Novonikolajevsko
ještě	ještě	k6eAd1	ještě
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nově	nově	k6eAd1	nově
zřízeného	zřízený	k2eAgInSc2d1	zřízený
Sibiřského	sibiřský	k2eAgInSc2d1	sibiřský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
bývalou	bývalý	k2eAgFnSc4d1	bývalá
Tomskou	Tomská	k1gFnSc4	Tomská
<g/>
,	,	kIx,	,
Omskou	omský	k2eAgFnSc4d1	Omská
<g/>
,	,	kIx,	,
Novonikolajevskou	Novonikolajevský	k2eAgFnSc4d1	Novonikolajevský
<g/>
,	,	kIx,	,
Altajskou	altajský	k2eAgFnSc4d1	Altajská
a	a	k8xC	a
Jenisejskou	jenisejský	k2eAgFnSc4d1	jenisejský
gubernii	gubernie	k1gFnSc4	gubernie
a	a	k8xC	a
také	také	k9	také
Ojrotskou	Ojrotský	k2eAgFnSc4d1	Ojrotský
autonomní	autonomní	k2eAgFnSc4d1	autonomní
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1925	[number]	k4	1925
probíhal	probíhat	k5eAaImAgInS	probíhat
1	[number]	k4	1
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
sovětů	sovět	k1gInPc2	sovět
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
řešena	řešit	k5eAaImNgFnS	řešit
otázka	otázka	k1gFnSc1	otázka
přejmenování	přejmenování	k1gNnSc2	přejmenování
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nesoucího	nesoucí	k2eAgNnSc2d1	nesoucí
jméno	jméno	k1gNnSc4	jméno
svrženého	svržený	k2eAgMnSc2d1	svržený
cara	car	k1gMnSc2	car
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
10	[number]	k4	10
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1925	[number]	k4	1925
informovaly	informovat	k5eAaBmAgFnP	informovat
noviny	novina	k1gFnPc1	novina
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
Sibiř	Sibiř	k1gFnSc4	Sibiř
o	o	k7c4	o
přejmenování	přejmenování	k1gNnSc4	přejmenování
Novonikolajevska	Novonikolajevsko	k1gNnSc2	Novonikolajevsko
na	na	k7c4	na
Novosibirsk	Novosibirsk	k1gInSc4	Novosibirsk
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
celá	celý	k2eAgFnSc1d1	celá
věc	věc	k1gFnSc1	věc
ještě	ještě	k9	ještě
nebyla	být	k5eNaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zcela	zcela	k6eAd1	zcela
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
otiskli	otisknout	k5eAaPmAgMnP	otisknout
i	i	k9	i
s	s	k7c7	s
pravopisnou	pravopisný	k2eAgFnSc7d1	pravopisná
chybou	chyba	k1gFnSc7	chyba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
rolničtí	rolnický	k2eAgMnPc1d1	rolnický
delegáti	delegát	k1gMnPc1	delegát
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
učinili	učinit	k5eAaImAgMnP	učinit
-	-	kIx~	-
správně	správně	k6eAd1	správně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgNnPc2d1	tehdejší
pravidel	pravidlo	k1gNnPc2	pravidlo
měl	mít	k5eAaImAgMnS	mít
název	název	k1gInSc4	název
Novosibirsk	Novosibirsk	k1gInSc4	Novosibirsk
zapisovat	zapisovat	k5eAaImF	zapisovat
se	s	k7c7	s
spojovníkem	spojovník	k1gInSc7	spojovník
(	(	kIx(	(
<g/>
Н	Н	k?	Н
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celoruský	Celoruský	k2eAgInSc1d1	Celoruský
ústřední	ústřední	k2eAgInSc1d1	ústřední
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
sice	sice	k8xC	sice
již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
tuto	tento	k3xDgFnSc4	tento
chybu	chyba	k1gFnSc4	chyba
opravil	opravit	k5eAaPmAgMnS	opravit
a	a	k8xC	a
příslušný	příslušný	k2eAgInSc4d1	příslušný
zákon	zákon	k1gInSc4	zákon
z	z	k7c2	z
března	březen	k1gInSc2	březen
uváděl	uvádět	k5eAaImAgMnS	uvádět
Novosibirsk	Novosibirsk	k1gInSc4	Novosibirsk
se	s	k7c7	s
spojovníkem	spojovník	k1gInSc7	spojovník
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
chybná	chybný	k2eAgFnSc1d1	chybná
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
stačila	stačit	k5eAaBmAgFnS	stačit
uchytit	uchytit	k5eAaPmF	uchytit
a	a	k8xC	a
název	název	k1gInSc1	název
se	s	k7c7	s
spojovníkem	spojovník	k1gInSc7	spojovník
se	se	k3xPyFc4	se
tak	tak	k9	tak
prakticky	prakticky	k6eAd1	prakticky
nikdy	nikdy	k6eAd1	nikdy
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
přejmenování	přejmenování	k1gNnSc6	přejmenování
nastal	nastat	k5eAaPmAgInS	nastat
doslova	doslova	k6eAd1	doslova
raketový	raketový	k2eAgInSc1d1	raketový
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
proto	proto	k8xC	proto
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Anatolij	Anatolij	k1gMnSc1	Anatolij
Lunačarskij	Lunačarskij	k1gMnSc1	Lunačarskij
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
sibiřské	sibiřský	k2eAgNnSc4d1	sibiřské
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
bylo	být	k5eAaImAgNnS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
i	i	k9	i
na	na	k7c4	na
levý	levý	k2eAgInSc4d1	levý
břeh	břeh	k1gInSc4	břeh
Obu	Ob	k1gInSc2	Ob
<g/>
,	,	kIx,	,
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
centrem	centr	k1gInSc7	centr
nově	nově	k6eAd1	nově
zřízeného	zřízený	k2eAgInSc2d1	zřízený
Západosibiřského	západosibiřský	k2eAgInSc2d1	západosibiřský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
vznikala	vznikat	k5eAaImAgFnS	vznikat
řada	řada	k1gFnSc1	řada
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
konstruktivistických	konstruktivistický	k2eAgFnPc2d1	konstruktivistická
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
budova	budova	k1gFnSc1	budova
Státní	státní	k2eAgFnPc1d1	státní
banky	banka	k1gFnPc1	banka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
však	však	k9	však
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
přestavěny	přestavěn	k2eAgInPc4d1	přestavěn
či	či	k8xC	či
přeprojektovány	přeprojektován	k2eAgInPc4d1	přeprojektován
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc4	případ
i	i	k8xC	i
Paláce	palác	k1gInPc4	palác
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vznikalo	vznikat	k5eAaImAgNnS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931-1941	[number]	k4	1931-1941
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Novosibirské	novosibirský	k2eAgNnSc1d1	novosibirský
státní	státní	k2eAgNnSc1d1	státní
akademické	akademický	k2eAgNnSc1d1	akademické
divadlo	divadlo	k1gNnSc1	divadlo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přehoupl	přehoupnout	k5eAaPmAgInS	přehoupnout
přes	přes	k7c4	přes
hranici	hranice	k1gFnSc4	hranice
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
Západosibiřský	západosibiřský	k2eAgInSc1d1	západosibiřský
kraj	kraj	k1gInSc1	kraj
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
Novosibirskou	novosibirský	k2eAgFnSc4d1	Novosibirská
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
Altajský	altajský	k2eAgInSc4d1	altajský
kraj	kraj	k1gInSc4	kraj
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
později	pozdě	k6eAd2	pozdě
vydělila	vydělit	k5eAaPmAgFnS	vydělit
Kemerovská	Kemerovský	k2eAgFnSc1d1	Kemerovská
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
Tomská	Tomský	k2eAgFnSc1d1	Tomský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
Letecký	letecký	k2eAgInSc1d1	letecký
závod	závod	k1gInSc1	závod
č.	č.	k?	č.
153	[number]	k4	153
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Novosibirský	novosibirský	k2eAgInSc1d1	novosibirský
letecký	letecký	k2eAgInSc1d1	letecký
závod	závod	k1gInSc1	závod
V.	V.	kA	V.
P.	P.	kA	P.
Čkalova	Čkalův	k2eAgFnSc1d1	Čkalova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgMnS	specializovat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejmodernějších	moderní	k2eAgInPc2d3	nejmodernější
stíhacích	stíhací	k2eAgInPc2d1	stíhací
letounů	letoun	k1gInPc2	letoun
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Polikarpova	Polikarpův	k2eAgFnSc1d1	Polikarpova
I-	I-	k1gFnSc1	I-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
situovaná	situovaný	k2eAgFnSc1d1	situovaná
bezpečně	bezpečně	k6eAd1	bezpečně
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
dodavatelů	dodavatel	k1gMnPc2	dodavatel
letecké	letecký	k2eAgFnSc2d1	letecká
techniky	technika	k1gFnSc2	technika
Rudé	rudý	k2eAgFnSc2d1	rudá
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
závod	závod	k1gInSc1	závod
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
až	až	k9	až
33	[number]	k4	33
stíhaček	stíhačka	k1gFnPc2	stíhačka
Jakovlev	Jakovlev	k1gFnSc1	Jakovlev
Jak-	Jak-	k1gFnSc1	Jak-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Jak-	Jak-	k1gFnSc1	Jak-
<g/>
7	[number]	k4	7
a	a	k8xC	a
Jak-	Jak-	k1gMnSc1	Jak-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zde	zde	k6eAd1	zde
pracovaly	pracovat	k5eAaImAgFnP	pracovat
především	především	k9	především
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
leté	letý	k2eAgFnPc1d1	letá
děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
nedalekých	daleký	k2eNgFnPc2d1	nedaleká
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
za	za	k7c4	za
700	[number]	k4	700
g	g	kA	g
chleba	chléb	k1gInSc2	chléb
na	na	k7c6	na
den	den	k1gInSc1	den
dovedly	dovést	k5eAaPmAgInP	dovést
zajistit	zajistit	k5eAaPmF	zajistit
dodávky	dodávka	k1gFnPc4	dodávka
jednoho	jeden	k4xCgInSc2	jeden
leteckého	letecký	k2eAgInSc2d1	letecký
pluku	pluk	k1gInSc2	pluk
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirské	novosibirský	k2eAgFnPc1d1	Novosibirská
děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941-1945	[number]	k4	1941-1945
dodaly	dodat	k5eAaPmAgFnP	dodat
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
550	[number]	k4	550
pluků	pluk	k1gInPc2	pluk
bojových	bojový	k2eAgNnPc2d1	bojové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
významně	významně	k6eAd1	významně
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Evakuace	evakuace	k1gFnSc1	evakuace
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
dalším	další	k2eAgInSc7d1	další
mocným	mocný	k2eAgInSc7d1	mocný
impulzem	impulz	k1gInSc7	impulz
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
asi	asi	k9	asi
400	[number]	k4	400
tisíc	tisíc	k4xCgInSc1	tisíc
a	a	k8xC	a
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
se	se	k3xPyFc4	se
mílovými	mílový	k2eAgInPc7d1	mílový
kroky	krok	k1gInPc4	krok
blížil	blížit	k5eAaImAgMnS	blížit
k	k	k7c3	k
hranici	hranice	k1gFnSc4	hranice
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
dosáhnul	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
růstu	růst	k1gInSc2	růst
města	město	k1gNnSc2	město
neochabovalo	ochabovat	k5eNaImAgNnS	ochabovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
projektováním	projektování	k1gNnSc7	projektování
novosibirského	novosibirský	k2eAgNnSc2d1	novosibirský
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
metrem	metro	k1gNnSc7	metro
v	v	k7c6	v
asijské	asijský	k2eAgFnSc6d1	asijská
části	část	k1gFnSc6	část
současného	současný	k2eAgNnSc2d1	současné
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
metrem	metro	k1gNnSc7	metro
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
rodící	rodící	k2eAgNnSc1d1	rodící
se	se	k3xPyFc4	se
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
zabezpečeno	zabezpečit	k5eAaPmNgNnS	zabezpečit
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
О	О	k?	О
vybudována	vybudován	k2eAgFnSc1d1	vybudována
gigantická	gigantický	k2eAgFnSc1d1	gigantická
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
přehrada	přehrada	k1gFnSc1	přehrada
s	s	k7c7	s
elektrárnou	elektrárna	k1gFnSc7	elektrárna
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
400	[number]	k4	400
megawattů	megawatt	k1gInPc2	megawatt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
postaveno	postavit	k5eAaPmNgNnS	postavit
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
komplex	komplex	k1gInSc4	komplex
pro	pro	k7c4	pro
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
Akademgorodok	Akademgorodok	k1gInSc1	Akademgorodok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
závod	závod	k1gInSc4	závod
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
produkující	produkující	k2eAgFnSc4d1	produkující
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
především	především	k9	především
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgFnPc4d1	jaderná
elektrárny	elektrárna	k1gFnPc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Akademgorodok	Akademgorodok	k1gInSc1	Akademgorodok
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
a	a	k8xC	a
studentská	studentský	k2eAgFnSc1d1	studentská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
mnoha	mnoho	k4c2	mnoho
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
Novosibirské	novosibirský	k2eAgFnSc2d1	Novosibirská
státní	státní	k2eAgFnSc2d1	státní
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
Novosibirsku	Novosibirsk	k1gInSc6	Novosibirsk
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
hluboká	hluboký	k2eAgFnSc1d1	hluboká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1991-1998	[number]	k4	1991-1998
se	se	k3xPyFc4	se
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
třetinu	třetina	k1gFnSc4	třetina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pokles	pokles	k1gInSc1	pokles
nejvíce	hodně	k6eAd3	hodně
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
sofistikované	sofistikovaný	k2eAgInPc4d1	sofistikovaný
technologické	technologický	k2eAgInPc4d1	technologický
obory	obor	k1gInPc4	obor
jako	jako	k8xC	jako
radioelektroniku	radioelektronika	k1gFnSc4	radioelektronika
<g/>
,	,	kIx,	,
mikroelektroniku	mikroelektronika	k1gFnSc4	mikroelektronika
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
letecký	letecký	k2eAgInSc1d1	letecký
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
po	po	k7c6	po
desetiletích	desetiletí	k1gNnPc6	desetiletí
strmého	strmý	k2eAgInSc2d1	strmý
růstu	růst	k1gInSc2	růst
poprvé	poprvé	k6eAd1	poprvé
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
transformace	transformace	k1gFnSc1	transformace
na	na	k7c4	na
tržní	tržní	k2eAgNnSc4d1	tržní
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
malých	malý	k2eAgFnPc2d1	malá
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
větší	veliký	k2eAgFnSc3d2	veliký
roli	role	k1gFnSc3	role
začaly	začít	k5eAaPmAgFnP	začít
hrát	hrát	k5eAaImF	hrát
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
opět	opět	k6eAd1	opět
prosperovat	prosperovat	k5eAaImF	prosperovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
3	[number]	k4	3
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
po	po	k7c6	po
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
překročilo	překročit	k5eAaPmAgNnS	překročit
hranici	hranice	k1gFnSc4	hranice
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Akademgorodku	Akademgorodek	k1gInSc6	Akademgorodek
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
vědecko-technologický	vědeckoechnologický	k2eAgInSc1d1	vědecko-technologický
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
inovativních	inovativní	k2eAgFnPc2d1	inovativní
technologií	technologie	k1gFnPc2	technologie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výroby	výroba	k1gFnPc1	výroba
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
biotechnologií	biotechnologie	k1gFnPc2	biotechnologie
a	a	k8xC	a
biomedicíny	biomedicína	k1gFnSc2	biomedicína
<g/>
,	,	kIx,	,
nanotechnologií	nanotechnologie	k1gFnPc2	nanotechnologie
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Západosibiřské	západosibiřský	k2eAgFnSc2d1	Západosibiřská
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
Obu	Ob	k1gInSc2	Ob
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
55	[number]	k4	55
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
s	s	k7c7	s
Omskem	Omsk	k1gInSc7	Omsk
<g/>
,	,	kIx,	,
Moskvou	Moskva	k1gFnSc7	Moskva
<g/>
,	,	kIx,	,
Kodaní	Kodaň	k1gFnSc7	Kodaň
či	či	k8xC	či
Glasgow	Glasgow	k1gNnSc7	Glasgow
<g/>
.	.	kIx.	.
</s>
<s>
Podloží	podloží	k1gNnSc1	podloží
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
silným	silný	k2eAgInSc7d1	silný
kamenným	kamenný	k2eAgInSc7d1	kamenný
podkladem	podklad	k1gInSc7	podklad
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
leží	ležet	k5eAaImIp3nS	ležet
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vrstva	vrstva	k1gFnSc1	vrstva
sedimentů	sediment	k1gInPc2	sediment
<g/>
:	:	kIx,	:
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
oblázků	oblázek	k1gInPc2	oblázek
a	a	k8xC	a
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
započalo	započnout	k5eAaPmAgNnS	započnout
pomalé	pomalý	k2eAgNnSc1d1	pomalé
zdvihání	zdvihání	k1gNnSc1	zdvihání
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
procesu	proces	k1gInSc3	proces
zde	zde	k6eAd1	zde
občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
slabým	slabý	k2eAgNnPc3d1	slabé
zemětřesením	zemětřesení	k1gNnPc3	zemětřesení
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
2-3	[number]	k4	2-3
stupně	stupeň	k1gInSc2	stupeň
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
škály	škála	k1gFnSc2	škála
(	(	kIx(	(
<g/>
silnější	silný	k2eAgMnSc1d2	silnější
je	být	k5eAaImIp3nS	být
vyloučeno	vyloučen	k2eAgNnSc1d1	vyloučeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Přiobské	Přiobský	k2eAgFnSc6d1	Přiobský
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Ob.	Ob.	k1gFnSc2	Ob.
Levobřežní	levobřežní	k2eAgFnSc1d1	levobřežní
část	část	k1gFnSc1	část
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
ploský	ploský	k2eAgInSc1d1	ploský
reliéf	reliéf	k1gInSc1	reliéf
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k6eAd1	poblíž
náměstí	náměstí	k1gNnSc1	náměstí
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
-	-	kIx~	-
151	[number]	k4	151
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Pravobřežní	pravobřežní	k2eAgFnSc1d1	pravobřežní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
rozčleněna	rozčlenit	k5eAaPmNgFnS	rozčlenit
mnoha	mnoho	k4c7	mnoho
roklemi	rokle	k1gFnPc7	rokle
a	a	k8xC	a
stržemi	strž	k1gFnPc7	strž
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
okrajovou	okrajový	k2eAgFnSc7d1	okrajová
částí	část	k1gFnSc7	část
Salairské	Salairský	k2eAgFnSc2d1	Salairský
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
pravobřežní	pravobřežní	k2eAgFnSc2d1	pravobřežní
části	část	k1gFnSc2	část
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
214	[number]	k4	214
m.	m.	k?	m.
Přes	přes	k7c4	přes
Novosibirsk	Novosibirsk	k1gInSc4	Novosibirsk
protéká	protékat	k5eAaImIp3nS	protékat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
světových	světový	k2eAgInPc2d1	světový
veletoků	veletok	k1gInPc2	veletok
<g/>
,	,	kIx,	,
Ob.	Ob.	k1gFnSc2	Ob.
Jeho	jeho	k3xOp3gFnSc1	jeho
šířka	šířka	k1gFnSc1	šířka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
činí	činit	k5eAaImIp3nS	činit
750-850	[number]	k4	750-850
m	m	kA	m
a	a	k8xC	a
hloubka	hloubka	k1gFnSc1	hloubka
okolo	okolo	k7c2	okolo
3	[number]	k4	3
m.	m.	k?	m.
Zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
obyčejně	obyčejně	k6eAd1	obyčejně
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
ledy	led	k1gInPc1	led
povolí	povolit	k5eAaPmIp3nP	povolit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Iňa	Iňa	k1gFnSc1	Iňa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
městu	město	k1gNnSc3	město
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Vybudována	vybudován	k2eAgFnSc1d1	vybudována
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
plocha	plocha	k1gFnSc1	plocha
činí	činit	k5eAaImIp3nS	činit
1070	[number]	k4	1070
km2	km2	k4	km2
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
18	[number]	k4	18
km	km	kA	km
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
185	[number]	k4	185
km	km	kA	km
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
až	až	k9	až
25	[number]	k4	25
m.	m.	k?	m.
Podnebí	podnebí	k1gNnSc1	podnebí
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
rovinaté	rovinatý	k2eAgFnSc6d1	rovinatá
části	část	k1gFnSc6	část
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
asijské	asijský	k2eAgFnSc2d1	asijská
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
teplotní	teplotní	k2eAgInPc4d1	teplotní
výkyvy	výkyv	k1gInPc4	výkyv
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
krátkodobě	krátkodobě	k6eAd1	krátkodobě
i	i	k9	i
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
velmi	velmi	k6eAd1	velmi
citelné	citelný	k2eAgInPc1d1	citelný
-	-	kIx~	-
nejsou	být	k5eNaImIp3nP	být
zde	zde	k6eAd1	zde
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
zbrzdily	zbrzdit	k5eAaPmAgInP	zbrzdit
ledové	ledový	k2eAgInPc1d1	ledový
větry	vítr	k1gInPc1	vítr
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
či	či	k8xC	či
teplé	teplý	k2eAgFnPc1d1	teplá
z	z	k7c2	z
jihozápadu	jihozápad	k1gInSc2	jihozápad
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
kumuloval	kumulovat	k5eAaImAgInS	kumulovat
teplo	teplo	k1gNnSc4	teplo
či	či	k8xC	či
chlad	chlad	k1gInSc4	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
slunečné	slunečný	k2eAgNnSc1d1	slunečné
<g/>
,	,	kIx,	,
za	za	k7c4	za
rok	rok	k1gInSc4	rok
průměrně	průměrně	k6eAd1	průměrně
2088	[number]	k4	2088
slunečných	slunečný	k2eAgFnPc2d1	Slunečná
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
asi	asi	k9	asi
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
Dubrovník	Dubrovník	k1gInSc1	Dubrovník
2600	[number]	k4	2600
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
10	[number]	k4	10
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
rajónů	rajón	k1gInPc2	rajón
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgNnSc7	třetí
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Milióntý	milióntý	k4xOgInSc4	milióntý
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
třetím	třetí	k4xOgMnSc7	třetí
ruským	ruský	k2eAgMnSc7d1	ruský
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
po	po	k7c6	po
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
překonalo	překonat	k5eAaPmAgNnS	překonat
hranici	hranice	k1gFnSc4	hranice
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
do	do	k7c2	do
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
každodenně	každodenně	k6eAd1	každodenně
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
nejméně	málo	k6eAd3	málo
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
připadalo	připadat	k5eAaPmAgNnS	připadat
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
13,2	[number]	k4	13,2
narozených	narozený	k2eAgFnPc2d1	narozená
<g/>
,	,	kIx,	,
12,3	[number]	k4	12,3
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
9,6	[number]	k4	9,6
svateb	svatba	k1gFnPc2	svatba
a	a	k8xC	a
5,0	[number]	k4	5,0
rozvodů	rozvod	k1gInPc2	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
přirozeného	přirozený	k2eAgInSc2d1	přirozený
přírůstu	přírůst	k1gInSc2	přírůst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
populaci	populace	k1gFnSc4	populace
především	především	k6eAd1	především
migrace	migrace	k1gFnSc2	migrace
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
národnostní	národnostní	k2eAgNnSc1d1	národnostní
složení	složení	k1gNnSc1	složení
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
následující	následující	k2eAgFnSc2d1	následující
<g/>
:	:	kIx,	:
Rusové	Rusová	k1gFnSc2	Rusová
92,82	[number]	k4	92,82
%	%	kIx~	%
(	(	kIx(	(
<g/>
1	[number]	k4	1
269	[number]	k4	269
979	[number]	k4	979
<g/>
)	)	kIx)	)
Ukrajinci	Ukrajinec	k1gMnSc3	Ukrajinec
0,92	[number]	k4	0,92
%	%	kIx~	%
(	(	kIx(	(
<g/>
12	[number]	k4	12
570	[number]	k4	570
<g/>
)	)	kIx)	)
Uzbekové	Uzbek	k1gMnPc1	Uzbek
0,75	[number]	k4	0,75
%	%	kIx~	%
(	(	kIx(	(
<g/>
10	[number]	k4	10
323	[number]	k4	323
<g/>
)	)	kIx)	)
Tataři	Tatar	k1gMnPc1	Tatar
0,73	[number]	k4	0,73
%	%	kIx~	%
(	(	kIx(	(
<g/>
9	[number]	k4	9
985	[number]	k4	985
<g/>
)	)	kIx)	)
Němci	Němec	k1gMnPc1	Němec
<g />
.	.	kIx.	.
</s>
<s>
0,63	[number]	k4	0,63
%	%	kIx~	%
(	(	kIx(	(
<g/>
8	[number]	k4	8
649	[number]	k4	649
<g/>
)	)	kIx)	)
Tádžikové	Tádžik	k1gMnPc1	Tádžik
0,61	[number]	k4	0,61
%	%	kIx~	%
(	(	kIx(	(
<g/>
8	[number]	k4	8
396	[number]	k4	396
<g/>
)	)	kIx)	)
Arméni	Armén	k1gMnPc1	Armén
0,47	[number]	k4	0,47
%	%	kIx~	%
(	(	kIx(	(
<g/>
6	[number]	k4	6
446	[number]	k4	446
<g/>
)	)	kIx)	)
Kyrgyzové	Kyrgyzová	k1gFnSc2	Kyrgyzová
0,44	[number]	k4	0,44
%	%	kIx~	%
(	(	kIx(	(
<g/>
6	[number]	k4	6
0	[number]	k4	0
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
Azerové	Azerová	k1gFnSc2	Azerová
0,41	[number]	k4	0,41
%	%	kIx~	%
(	(	kIx(	(
<g/>
5	[number]	k4	5
652	[number]	k4	652
<g/>
)	)	kIx)	)
Bělorusové	Bělorus	k1gMnPc1	Bělorus
0,24	[number]	k4	0,24
%	%	kIx~	%
(	(	kIx(	(
<g/>
3	[number]	k4	3
277	[number]	k4	277
<g/>
)	)	kIx)	)
Rudý	rudý	k2eAgInSc1d1	rudý
prospekt	prospekt	k1gInSc1	prospekt
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
6,7	[number]	k4	6,7
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
od	od	k7c2	od
nábřeží	nábřeží	k1gNnSc2	nábřeží
Obu	Ob	k1gInSc2	Ob
přes	přes	k7c4	přes
hlavní	hlavní	k2eAgNnSc4d1	hlavní
Leninovo	Leninův	k2eAgNnSc4d1	Leninovo
náměstí	náměstí	k1gNnSc4	náměstí
až	až	k9	až
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
bývalému	bývalý	k2eAgNnSc3d1	bývalé
letišti	letiště	k1gNnSc3	letiště
Severnyj	Severnyj	k1gFnSc1	Severnyj
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
domy	dům	k1gInPc1	dům
z	z	k7c2	z
carské	carský	k2eAgFnSc2d1	carská
éry	éra	k1gFnSc2	éra
a	a	k8xC	a
také	také	k9	také
řada	řada	k1gFnSc1	řada
památek	památka	k1gFnPc2	památka
sovětského	sovětský	k2eAgInSc2d1	sovětský
konstruktivismu	konstruktivismus	k1gInSc2	konstruktivismus
<g/>
,	,	kIx,	,
např.	např.	kA	např.
budova	budova	k1gFnSc1	budova
Státní	státní	k2eAgFnSc1d1	státní
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
trastu	trast	k1gInSc2	trast
Zapsibzoloto	Zapsibzolota	k1gFnSc5	Zapsibzolota
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
Krajispolkomu	Krajispolkom	k1gInSc2	Krajispolkom
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
Aeroflotu	aeroflot	k1gInSc2	aeroflot
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirské	novosibirský	k2eAgNnSc1d1	novosibirský
státní	státní	k2eAgNnSc1d1	státní
akademické	akademický	k2eAgNnSc1d1	akademické
divadlo	divadlo	k1gNnSc1	divadlo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
-	-	kIx~	-
největší	veliký	k2eAgFnSc1d3	veliký
divadelní	divadelní	k2eAgFnSc1d1	divadelní
budova	budova	k1gFnSc1	budova
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
konstruktivistický	konstruktivistický	k2eAgInSc1d1	konstruktivistický
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
výstavby	výstavba	k1gFnSc2	výstavba
přeprojektován	přeprojektovat	k5eAaPmNgInS	přeprojektovat
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Novosibirsk-Glavnyj	Novosibirsk-Glavnyj	k1gFnSc1	Novosibirsk-Glavnyj
-	-	kIx~	-
monumentální	monumentální	k2eAgFnSc1d1	monumentální
nádražní	nádražní	k2eAgFnSc1d1	nádražní
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
konstruktivistický	konstruktivistický	k2eAgInSc4d1	konstruktivistický
projekt	projekt	k1gInSc4	projekt
byl	být	k5eAaImAgMnS	být
zcela	zcela	k6eAd1	zcela
přepracován	přepracovat	k5eAaPmNgMnS	přepracovat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
stalinistického	stalinistický	k2eAgNnSc2d1	stalinistické
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hala	hala	k1gFnSc1	hala
tak	tak	k6eAd1	tak
připomíná	připomínat	k5eAaImIp3nS	připomínat
vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
zdobena	zdobit	k5eAaImNgFnS	zdobit
toskánskými	toskánský	k2eAgInPc7d1	toskánský
pilastry	pilastr	k1gInPc7	pilastr
<g/>
,	,	kIx,	,
dojem	dojem	k1gInSc1	dojem
umocňuje	umocňovat	k5eAaImIp3nS	umocňovat
velká	velký	k2eAgFnSc1d1	velká
atika	atika	k1gFnSc1	atika
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
tvar	tvar	k1gInSc1	tvar
budovy	budova	k1gFnSc2	budova
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
parní	parní	k2eAgFnSc4d1	parní
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
-	-	kIx~	-
první	první	k4xOgFnSc1	první
kamenná	kamenný	k2eAgFnSc1d1	kamenná
budova	budova	k1gFnSc1	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
vysvěcená	vysvěcený	k2eAgFnSc1d1	vysvěcená
již	již	k6eAd1	již
6	[number]	k4	6
let	léto	k1gNnPc2	léto
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hlavní	hlavní	k2eAgFnSc2d1	hlavní
třídy	třída	k1gFnSc2	třída
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
<g/>
,	,	kIx,	,
Rudého	rudý	k2eAgInSc2d1	rudý
prospektu	prospekt	k1gInSc2	prospekt
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
tržiště	tržiště	k1gNnSc1	tržiště
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Novosibirské	novosibirský	k2eAgNnSc4d1	novosibirský
státní	státní	k2eAgNnSc4d1	státní
regionální	regionální	k2eAgNnSc4d1	regionální
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
)	)	kIx)	)
-	-	kIx~	-
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
architektonickou	architektonický	k2eAgFnSc7d1	architektonická
památkou	památka	k1gFnSc7	památka
federálního	federální	k2eAgInSc2d1	federální
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
divadlem	divadlo	k1gNnSc7	divadlo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
tvoří	tvořit	k5eAaImIp3nP	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
náměstí	náměstí	k1gNnSc2	náměstí
Novosibirsku	Novosibirsk	k1gInSc6	Novosibirsk
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc4	náměstí
Lenina	Lenin	k1gMnSc2	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Stobytový	stobytový	k2eAgInSc1d1	stobytový
dům	dům	k1gInSc1	dům
-	-	kIx~	-
bytový	bytový	k2eAgInSc1d1	bytový
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
vystavěný	vystavěný	k2eAgInSc1d1	vystavěný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Andreje	Andrej	k1gMnSc2	Andrej
Krjačkova	Krjačkův	k2eAgMnSc2d1	Krjačkův
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
oceněn	ocenit	k5eAaPmNgInS	ocenit
zlatou	zlatá	k1gFnSc7	zlatá
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
Lenina	Lenin	k1gMnSc2	Lenin
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
)	)	kIx)	)
-	-	kIx~	-
vystavěn	vystavěn	k2eAgMnSc1d1	vystavěn
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
revolučního	revoluční	k2eAgMnSc2d1	revoluční
vůdce	vůdce	k1gMnSc2	vůdce
Lenina	Lenin	k1gMnSc2	Lenin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
relativně	relativně	k6eAd1	relativně
strohý	strohý	k2eAgInSc4d1	strohý
objekt	objekt	k1gInSc4	objekt
připomínající	připomínající	k2eAgNnSc1d1	připomínající
Leninovo	Leninův	k2eAgNnSc1d1	Leninovo
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přestavěn	přestavěn	k2eAgInSc4d1	přestavěn
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
slouží	sloužit	k5eAaImIp3nP	sloužit
místní	místní	k2eAgFnSc4d1	místní
filharmonii	filharmonie	k1gFnSc4	filharmonie
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
obchodní	obchodní	k2eAgFnSc2d1	obchodní
komory	komora	k1gFnSc2	komora
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Novosibirské	novosibirský	k2eAgNnSc4d1	novosibirský
státní	státní	k2eAgNnSc4d1	státní
akademické	akademický	k2eAgNnSc4d1	akademické
dramatické	dramatický	k2eAgNnSc4d1	dramatické
divadlo	divadlo	k1gNnSc4	divadlo
Rudý	rudý	k2eAgInSc1d1	rudý
plamen	plamen	k1gInSc1	plamen
<g/>
)	)	kIx)	)
-	-	kIx~	-
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
ruského	ruský	k2eAgInSc2d1	ruský
klasicismu	klasicismus	k1gInSc2	klasicismus
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
formálnímu	formální	k2eAgNnSc3d1	formální
i	i	k8xC	i
neformálnímu	formální	k2eNgNnSc3d1	neformální
setkávání	setkávání	k1gNnSc3	setkávání
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
,	,	kIx,	,
konaly	konat	k5eAaImAgInP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
nejrůznější	různý	k2eAgFnPc1d3	nejrůznější
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
jako	jako	k8xC	jako
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
či	či	k8xC	či
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Dům	dům	k1gInSc4	dům
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
scházel	scházet	k5eAaImAgMnS	scházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
místní	místní	k2eAgInSc1d1	místní
Sovět	sovět	k1gInSc1	sovět
dělnických	dělnický	k2eAgMnPc2d1	dělnický
<g/>
,	,	kIx,	,
vojenských	vojenský	k2eAgMnPc2d1	vojenský
a	a	k8xC	a
rolnických	rolnický	k2eAgMnPc2d1	rolnický
delegátů	delegát	k1gMnPc2	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
divadelní	divadelní	k2eAgInSc1d1	divadelní
soubor	soubor	k1gInSc1	soubor
Rudý	rudý	k2eAgInSc1d1	rudý
plamen	plamen	k1gInSc1	plamen
z	z	k7c2	z
Oděsy	Oděsa	k1gFnSc2	Oděsa
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirské	novosibirský	k2eAgNnSc1d1	novosibirský
muzeum	muzeum	k1gNnSc1	muzeum
železniční	železniční	k2eAgFnSc2d1	železniční
techniky	technika	k1gFnSc2	technika
-	-	kIx~	-
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
expozice	expozice	k1gFnSc1	expozice
parních	parní	k2eAgFnPc2d1	parní
<g/>
,	,	kIx,	,
dieselových	dieselový	k2eAgFnPc2d1	dieselová
i	i	k8xC	i
elektrických	elektrický	k2eAgFnPc2d1	elektrická
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
,	,	kIx,	,
vagónů	vagón	k1gInPc2	vagón
a	a	k8xC	a
také	také	k9	také
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
zde	zde	k6eAd1	zde
na	na	k7c4	na
60	[number]	k4	60
ha	ha	kA	ha
chovali	chovat	k5eAaImAgMnP	chovat
okolo	okolo	k6eAd1	okolo
11	[number]	k4	11
tisíc	tisíc	k4xCgInSc4	tisíc
zvířat	zvíře	k1gNnPc2	zvíře
770	[number]	k4	770
různých	různý	k2eAgInPc2d1	různý
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
-	-	kIx~	-
největší	veliký	k2eAgFnSc1d3	veliký
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
asijské	asijský	k2eAgFnSc6d1	asijská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Akademgorodku	Akademgorodka	k1gFnSc4	Akademgorodka
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Dětská	dětský	k2eAgFnSc1d1	dětská
železnice	železnice	k1gFnSc1	železnice
-	-	kIx~	-
dětská	dětský	k2eAgFnSc1d1	dětská
železnice	železnice	k1gFnSc1	železnice
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
5,3	[number]	k4	5,3
km	km	kA	km
s	s	k7c7	s
5	[number]	k4	5
stanicemi	stanice	k1gFnPc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Otevřena	otevřen	k2eAgFnSc1d1	otevřena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
oblast	oblast	k1gFnSc1	oblast
obecně	obecně	k6eAd1	obecně
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
těm	ten	k3xDgInPc3	ten
bohatším	bohatý	k2eAgInPc3d2	bohatší
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
z	z	k7c2	z
85	[number]	k4	85
subjektů	subjekt	k1gInPc2	subjekt
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
produkovala	produkovat	k5eAaImAgFnS	produkovat
30	[number]	k4	30
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třikrát	třikrát	k6eAd1	třikrát
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
úrovně	úroveň	k1gFnSc2	úroveň
socioekonomického	socioekonomický	k2eAgInSc2d1	socioekonomický
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
server	server	k1gInSc1	server
Ekspert	Ekspert	k1gInSc1	Ekspert
Online	Onlin	k1gInSc5	Onlin
ocenil	ocenit	k5eAaPmAgMnS	ocenit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vědecké	vědecký	k2eAgInPc4d1	vědecký
a	a	k8xC	a
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Sibiře	Sibiř	k1gFnSc2	Sibiř
jako	jako	k9	jako
6	[number]	k4	6
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
město	město	k1gNnSc1	město
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInSc1d1	finanční
server	server	k1gInSc1	server
banki-	banki-	k?	banki-
<g/>
v.	v.	k?	v.
<g/>
ru	ru	k?	ru
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
jako	jako	k8xC	jako
město	město	k1gNnSc1	město
s	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
úrovní	úroveň	k1gFnSc7	úroveň
života	život	k1gInSc2	život
v	v	k7c6	v
Ruské	ruský	k2eAgFnSc6d1	ruská
federaci	federace	k1gFnSc6	federace
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc4	svaz
podnikatelů	podnikatel	k1gMnPc2	podnikatel
Klub	klub	k1gInSc1	klub
liderov	liderovo	k1gNnPc2	liderovo
potom	potom	k6eAd1	potom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
ocenil	ocenit	k5eAaPmAgMnS	ocenit
investiční	investiční	k2eAgNnSc4d1	investiční
klima	klima	k1gNnSc4	klima
v	v	k7c6	v
Novosibirské	novosibirský	k2eAgFnSc6d1	Novosibirská
oblasti	oblast	k1gFnSc6	oblast
jako	jako	k9	jako
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
30	[number]	k4	30
hodnocených	hodnocený	k2eAgInPc2d1	hodnocený
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
hluboká	hluboký	k2eAgFnSc1d1	hluboká
diverzifikace	diverzifikace	k1gFnSc1	diverzifikace
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
giganty	gigant	k1gInPc1	gigant
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zdědila	zdědit	k5eAaPmAgNnP	zdědit
mnohá	mnohý	k2eAgNnPc1d1	mnohé
postsovětská	postsovětský	k2eAgNnPc1d1	postsovětské
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
malému	malý	k2eAgNnSc3d1	malé
a	a	k8xC	a
střednímu	střední	k2eAgNnSc3d1	střední
byznysu	byznys	k1gInSc2	byznys
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
zejména	zejména	k9	zejména
sektor	sektor	k1gInSc1	sektor
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
logistika	logistika	k1gFnSc1	logistika
a	a	k8xC	a
vědecký	vědecký	k2eAgInSc1d1	vědecký
výzkum	výzkum	k1gInSc1	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
technologicky	technologicky	k6eAd1	technologicky
vyspělých	vyspělý	k2eAgFnPc2d1	vyspělá
odvětví	odvětví	k1gNnPc2	odvětví
ekonomiky	ekonomika	k1gFnSc2	ekonomika
hraje	hrát	k5eAaImIp3nS	hrát
novosibirský	novosibirský	k2eAgInSc1d1	novosibirský
Akademgorodok	Akademgorodok	k1gInSc1	Akademgorodok
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
prezidium	prezidium	k1gNnSc1	prezidium
Sibiřského	sibiřský	k2eAgNnSc2d1	sibiřské
oddělení	oddělení	k1gNnSc2	oddělení
Ruské	ruský	k2eAgFnSc2d1	ruská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
fyzicko-matematické	fyzickoatematický	k2eAgNnSc1d1	fyzicko-matematický
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
vědecko-technologický	vědeckoechnologický	k2eAgInSc1d1	vědecko-technologický
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
podnikatelský	podnikatelský	k2eAgInSc1d1	podnikatelský
inkubátor	inkubátor	k1gInSc1	inkubátor
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
technologických	technologický	k2eAgFnPc2d1	technologická
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
pobočky	pobočka	k1gFnPc4	pobočka
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
globální	globální	k2eAgFnPc1d1	globální
firmy	firma	k1gFnPc1	firma
jako	jako	k8xS	jako
Intel	Intel	kA	Intel
<g/>
,	,	kIx,	,
IBM	IBM	kA	IBM
či	či	k8xC	či
Schlumberger	Schlumberger	k1gInSc4	Schlumberger
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
ale	ale	k8xC	ale
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
startupů	startup	k1gInPc2	startup
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Novosoft	Novosoft	k1gInSc1	Novosoft
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejsilnější	silný	k2eAgNnPc4d3	nejsilnější
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
odvětví	odvětví	k1gNnPc4	odvětví
patří	patřit	k5eAaImIp3nS	patřit
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
plynařství	plynařství	k1gNnSc1	plynařství
<g/>
,	,	kIx,	,
vodárenství	vodárenství	k1gNnSc1	vodárenství
<g/>
,	,	kIx,	,
metalurgie	metalurgie	k1gFnSc1	metalurgie
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
strojírenství	strojírenství	k1gNnSc2	strojírenství
-	-	kIx~	-
tyto	tento	k3xDgInPc1	tento
obory	obor	k1gInPc1	obor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
představovaly	představovat	k5eAaImAgFnP	představovat
94	[number]	k4	94
%	%	kIx~	%
veškeré	veškerý	k3xTgFnSc2	veškerý
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
132	[number]	k4	132
071	[number]	k4	071
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
43	[number]	k4	43
402	[number]	k4	402
živnostníků	živnostník	k1gMnPc2	živnostník
<g/>
.	.	kIx.	.
</s>
<s>
Letecký	letecký	k2eAgInSc1d1	letecký
průmysl	průmysl	k1gInSc1	průmysl
představuje	představovat	k5eAaImIp3nS	představovat
Novosibirský	novosibirský	k2eAgInSc1d1	novosibirský
letecký	letecký	k2eAgInSc1d1	letecký
závod	závod	k1gInSc1	závod
V.	V.	kA	V.
P.	P.	kA	P.
Čkalova	Čkalův	k2eAgFnSc1d1	Čkalova
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgFnSc1d1	spadající
do	do	k7c2	do
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
letecké	letecký	k2eAgFnSc2d1	letecká
korporace	korporace	k1gFnSc2	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
letouny	letoun	k1gInPc4	letoun
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gMnPc2	Su-
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
části	část	k1gFnSc2	část
trupu	trup	k1gInSc2	trup
dopravního	dopravní	k2eAgInSc2d1	dopravní
Suchoje	Suchoj	k1gInSc2	Suchoj
Superjetu	Superjet	k1gInSc2	Superjet
100	[number]	k4	100
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
modernizace	modernizace	k1gFnSc1	modernizace
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirský	novosibirský	k2eAgInSc1d1	novosibirský
závod	závod	k1gInSc1	závod
chemických	chemický	k2eAgInPc2d1	chemický
koncentrátů	koncentrát	k1gInPc2	koncentrát
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgMnSc7d1	tradiční
výrobcem	výrobce	k1gMnSc7	výrobce
palivových	palivový	k2eAgFnPc2d1	palivová
kazet	kazeta	k1gFnPc2	kazeta
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgFnPc4d1	jaderná
elektrárny	elektrárna	k1gFnPc4	elektrárna
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
českých	český	k2eAgMnPc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
:	:	kIx,	:
protínají	protínat	k5eAaImIp3nP	protínat
ho	on	k3xPp3gNnSc4	on
důležité	důležitý	k2eAgFnPc1d1	důležitá
železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
a	a	k8xC	a
dálnice	dálnice	k1gFnSc1	dálnice
spojující	spojující	k2eAgFnSc1d1	spojující
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
,	,	kIx,	,
Dálný	dálný	k2eAgInSc1d1	dálný
východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
nejenom	nejenom	k6eAd1	nejenom
Transsibiřská	transsibiřský	k2eAgFnSc1d1	Transsibiřská
magistrála	magistrála	k1gFnSc1	magistrála
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
jeho	jeho	k3xOp3gInSc2	jeho
zrodu	zrod	k1gInSc2	zrod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začíná	začínat	k5eAaImIp3nS	začínat
tu	tu	k6eAd1	tu
i	i	k9	i
Turkestánsko-sibiřská	turkestánskoibiřský	k2eAgFnSc1d1	turkestánsko-sibiřský
magistrála	magistrála	k1gFnSc1	magistrála
a	a	k8xC	a
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Kemerovo	Kemerův	k2eAgNnSc4d1	Kemerovo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
umožnila	umožnit	k5eAaPmAgFnS	umožnit
dopravu	doprava	k1gFnSc4	doprava
uhlí	uhlí	k1gNnSc2	uhlí
z	z	k7c2	z
Kuzněcké	kuzněcký	k2eAgFnSc2d1	kuzněcký
uhelné	uhelný	k2eAgFnSc2d1	uhelná
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Monumentální	monumentální	k2eAgFnSc1d1	monumentální
budova	budova	k1gFnSc1	budova
novosibirského	novosibirský	k2eAgNnSc2d1	novosibirský
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýraznějším	výrazný	k2eAgFnPc3d3	nejvýraznější
architektonickým	architektonický	k2eAgFnPc3d1	architektonická
památkám	památka	k1gFnPc3	památka
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Novosibirsk	Novosibirsk	k1gInSc4	Novosibirsk
prochází	procházet	k5eAaImIp3nS	procházet
federální	federální	k2eAgFnSc1d1	federální
dálnice	dálnice	k1gFnSc1	dálnice
R254	R254	k1gMnSc1	R254
Irtyš	Irtyš	k1gMnSc1	Irtyš
(	(	kIx(	(
<g/>
z	z	k7c2	z
Čeljabinsku	Čeljabinsk	k1gInSc2	Čeljabinsk
do	do	k7c2	do
Omska	Omsk	k1gInSc2	Omsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R255	R255	k1gFnSc1	R255
Sibiř	Sibiř	k1gFnSc1	Sibiř
(	(	kIx(	(
<g/>
z	z	k7c2	z
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
do	do	k7c2	do
Irkutsku	Irkutsk	k1gInSc2	Irkutsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R384	R384	k1gFnSc1	R384
(	(	kIx(	(
<g/>
z	z	k7c2	z
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
do	do	k7c2	do
Jurgy	Jurga	k1gFnSc2	Jurga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R256	R256	k1gMnSc1	R256
Čujský	Čujský	k2eAgInSc4d1	Čujský
trakt	trakt	k1gInSc4	trakt
(	(	kIx(	(
<g/>
z	z	k7c2	z
Novosibirsku	Novosibirsk	k1gInSc2	Novosibirsk
na	na	k7c4	na
mongolskou	mongolský	k2eAgFnSc4d1	mongolská
hranici	hranice	k1gFnSc4	hranice
<g/>
)	)	kIx)	)
a	a	k8xC	a
regionální	regionální	k2eAgFnSc1d1	regionální
silnice	silnice	k1gFnSc1	silnice
R380	R380	k1gFnSc1	R380
do	do	k7c2	do
Barnaulu	Barnaul	k1gInSc2	Barnaul
<g/>
.	.	kIx.	.
</s>
<s>
Dálniční	dálniční	k2eAgInSc1d1	dálniční
obchvat	obchvat	k1gInSc1	obchvat
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
hotov	hotov	k2eAgMnSc1d1	hotov
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řeku	řeka	k1gFnSc4	řeka
Ob	Ob	k1gInSc1	Ob
překlenují	překlenovat	k5eAaImIp3nP	překlenovat
v	v	k7c6	v
hranicích	hranice	k1gFnPc6	hranice
města	město	k1gNnSc2	město
4	[number]	k4	4
automobilové	automobilový	k2eAgInPc4d1	automobilový
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
řeku	řeka	k1gFnSc4	řeka
Iňu	Iňu	k1gFnSc2	Iňu
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Letecké	letecký	k2eAgNnSc1d1	letecké
spojení	spojení	k1gNnSc1	spojení
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Tolmačovo	Tolmačův	k2eAgNnSc1d1	Tolmačův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Novosibirsku	Novosibirsk	k1gInSc6	Novosibirsk
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
165	[number]	k4	165
km	km	kA	km
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
11	[number]	k4	11
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
267	[number]	k4	267
km	km	kA	km
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
14	[number]	k4	14
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirské	novosibirský	k2eAgNnSc1d1	novosibirský
metro	metro	k1gNnSc1	metro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
provozovalo	provozovat	k5eAaImAgNnS	provozovat
15,9	[number]	k4	15,9
km	km	kA	km
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
13	[number]	k4	13
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
2	[number]	k4	2
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
obsloužilo	obsloužit	k5eAaPmAgNnS	obsloužit
80,7	[number]	k4	80,7
milionů	milion	k4xCgInPc2	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
sídlilo	sídlit	k5eAaImAgNnS	sídlit
37	[number]	k4	37
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
14	[number]	k4	14
poboček	pobočka	k1gFnPc2	pobočka
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámější	známý	k2eAgInSc1d3	nejznámější
místní	místní	k2eAgFnSc7d1	místní
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
je	být	k5eAaImIp3nS	být
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
žebříčcích	žebříček	k1gInPc6	žebříček
pravidelně	pravidelně	k6eAd1	pravidelně
objevuje	objevovat	k5eAaImIp3nS	objevovat
mezi	mezi	k7c7	mezi
čtyřmi	čtyři	k4xCgFnPc7	čtyři
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
univerzitami	univerzita	k1gFnPc7	univerzita
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
ústavů	ústav	k1gInPc2	ústav
Novosibirské	novosibirský	k2eAgFnSc2d1	Novosibirská
státní	státní	k2eAgFnSc2d1	státní
univerzity	univerzita	k1gFnSc2	univerzita
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
Akademgorodku	Akademgorodek	k1gInSc6	Akademgorodek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
nachází	nacházet	k5eAaImIp3nS	nacházet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
a	a	k8xC	a
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
fungovalo	fungovat	k5eAaImAgNnS	fungovat
přes	přes	k7c4	přes
60	[number]	k4	60
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Novosibirské	novosibirský	k2eAgNnSc1d1	novosibirský
státní	státní	k2eAgNnSc1d1	státní
akademické	akademický	k2eAgNnSc1d1	akademické
divadlo	divadlo	k1gNnSc1	divadlo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
baletu	balet	k1gInSc2	balet
-	-	kIx~	-
největší	veliký	k2eAgFnSc1d3	veliký
divadelní	divadelní	k2eAgFnSc1d1	divadelní
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Novosibirské	novosibirský	k2eAgFnSc2d1	Novosibirská
státní	státní	k2eAgFnSc2d1	státní
dramatické	dramatický	k2eAgFnSc2d1	dramatická
divadlo	divadlo	k1gNnSc4	divadlo
Starý	starý	k2eAgInSc1d1	starý
dům	dům	k1gInSc1	dům
Novosibirské	novosibirský	k2eAgFnSc2d1	Novosibirská
akademické	akademický	k2eAgFnPc1d1	akademická
divadlo	divadlo	k1gNnSc4	divadlo
mládeže	mládež	k1gFnSc2	mládež
Globus	globus	k1gInSc4	globus
Novosibirské	novosibirský	k2eAgNnSc4d1	novosibirský
státní	státní	k2eAgNnSc4d1	státní
akademické	akademický	k2eAgNnSc4d1	akademické
dramatické	dramatický	k2eAgNnSc4d1	dramatické
divadlo	divadlo	k1gNnSc4	divadlo
Rudý	rudý	k2eAgInSc1d1	rudý
plamen	plamen	k1gInSc1	plamen
Novosibirské	novosibirský	k2eAgFnSc2d1	Novosibirská
městské	městský	k2eAgFnSc2d1	městská
dramatické	dramatický	k2eAgFnSc2d1	dramatická
divadlo	divadlo	k1gNnSc4	divadlo
Sergeje	Sergej	k1gMnSc2	Sergej
Afanasjeva	Afanasjevo	k1gNnSc2	Afanasjevo
Novosibirské	novosibirský	k2eAgNnSc4d1	novosibirský
městské	městský	k2eAgNnSc4d1	Městské
dramatické	dramatický	k2eAgNnSc4d1	dramatické
divadlo	divadlo	k1gNnSc4	divadlo
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Novosibirské	novosibirský	k2eAgNnSc1d1	novosibirský
divadlo	divadlo	k1gNnSc1	divadlo
muzikálové	muzikálový	k2eAgFnSc2d1	muzikálová
komedie	komedie	k1gFnSc2	komedie
Novosibirské	novosibirský	k2eAgNnSc4d1	novosibirský
oblastní	oblastní	k2eAgNnSc4d1	oblastní
<g />
.	.	kIx.	.
</s>
<s>
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
státní	státní	k2eAgFnSc1d1	státní
filharmonie	filharmonie	k1gFnSc1	filharmonie
Státní	státní	k2eAgInSc1d1	státní
koncertní	koncertní	k2eAgInSc1d1	koncertní
sál	sál	k1gInSc1	sál
Arnolda	Arnold	k1gMnSc2	Arnold
Kacy	Kaca	k1gFnSc2	Kaca
Novosibirské	novosibirský	k2eAgNnSc4d1	novosibirský
státní	státní	k2eAgNnSc4d1	státní
umělecké	umělecký	k2eAgNnSc4d1	umělecké
muzeum	muzeum	k1gNnSc4	muzeum
Novosibirské	novosibirský	k2eAgNnSc4d1	novosibirský
státní	státní	k2eAgNnSc4d1	státní
regionální	regionální	k2eAgNnSc4d1	regionální
muzeum	muzeum	k1gNnSc4	muzeum
Muzeum	muzeum	k1gNnSc4	muzeum
kozácké	kozácký	k2eAgFnSc2d1	kozácká
slávy	sláva	k1gFnSc2	sláva
Novosibirské	novosibirský	k2eAgNnSc1d1	novosibirský
muzeum	muzeum	k1gNnSc1	muzeum
železniční	železniční	k2eAgFnSc2d1	železniční
techniky	technika	k1gFnSc2	technika
N.	N.	kA	N.
A.	A.	kA	A.
Akulinina	Akulinin	k2eAgMnSc4d1	Akulinin
Muzeum	muzeum	k1gNnSc4	muzeum
Sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
březová	březový	k2eAgFnSc1d1	Březová
kůra	kůra	k1gFnSc1	kůra
-	-	kIx~	-
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
umělecké	umělecký	k2eAgInPc4d1	umělecký
předměty	předmět	k1gInPc4	předmět
z	z	k7c2	z
březové	březový	k2eAgFnSc2d1	Březová
kůry	kůra	k1gFnSc2	kůra
Muzeum	muzeum	k1gNnSc1	muzeum
N.	N.	kA	N.
K.	K.	kA	K.
Rericha	Rerich	k1gMnSc2	Rerich
Muzeum	muzeum	k1gNnSc1	muzeum
Slunce	slunce	k1gNnSc2	slunce
-	-	kIx~	-
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
různá	různý	k2eAgNnPc4d1	různé
vyobrazení	vyobrazení	k1gNnPc4	vyobrazení
slunce	slunce	k1gNnSc2	slunce
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kulturách	kultura	k1gFnPc6	kultura
Historicko-architektonický	historickorchitektonický	k2eAgInSc1d1	historicko-architektonický
skanzen	skanzen	k1gInSc1	skanzen
-	-	kIx~	-
expozice	expozice	k1gFnSc1	expozice
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
stavitelství	stavitelství	k1gNnSc2	stavitelství
Sibiře	Sibiř	k1gFnSc2	Sibiř
Státní	státní	k2eAgFnSc1d1	státní
veřejná	veřejný	k2eAgFnSc1d1	veřejná
vědecko-technická	vědeckoechnický	k2eAgFnSc1d1	vědecko-technická
knihovna	knihovna	k1gFnSc1	knihovna
SO	So	kA	So
RAV	RAV	kA	RAV
-	-	kIx~	-
největší	veliký	k2eAgFnSc1d3	veliký
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
asijské	asijský	k2eAgFnSc6d1	asijská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
státní	státní	k2eAgFnSc1d1	státní
oblastní	oblastní	k2eAgFnSc1d1	oblastní
vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
oblastní	oblastní	k2eAgFnSc1d1	oblastní
dětská	dětský	k2eAgFnSc1d1	dětská
knihovna	knihovna	k1gFnSc1	knihovna
A.	A.	kA	A.
M.	M.	kA	M.
Gorkého	Gorkij	k1gMnSc2	Gorkij
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
oblastní	oblastní	k2eAgFnSc1d1	oblastní
knihovna	knihovna	k1gFnSc1	knihovna
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
</s>
<s>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
je	být	k5eAaImIp3nS	být
nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Novosibirsku	Novosibirsk	k1gInSc6	Novosibirsk
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
HK	HK	kA	HK
Sibir	Sibir	k1gInSc1	Sibir
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Kontinentální	kontinentální	k2eAgFnSc3d1	kontinentální
hokejové	hokejový	k2eAgFnSc3d1	hokejová
lize	liga	k1gFnSc3	liga
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
aréna	aréna	k1gFnSc1	aréna
je	být	k5eAaImIp3nS	být
Ledový	ledový	k2eAgInSc4d1	ledový
palác	palác	k1gInSc4	palác
sportu	sport	k1gInSc2	sport
"	"	kIx"	"
<g/>
Sibir	Sibir	k1gInSc1	Sibir
<g/>
"	"	kIx"	"
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
7	[number]	k4	7
384	[number]	k4	384
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
FK	FK	kA	FK
Sibir	Sibir	k1gInSc1	Sibir
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Ruské	ruský	k2eAgFnSc6d1	ruská
druhé	druhý	k4xOgFnSc6	druhý
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
D-	D-	k1gFnSc1	D-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Ruskou	ruský	k2eAgFnSc4d1	ruská
Premier	Premier	k1gInSc1	Premier
Ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
hrál	hrát	k5eAaImAgMnS	hrát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Čkalovec	Čkalovec	k1gMnSc1	Čkalovec
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
klub	klub	k1gInSc1	klub
–	–	k?	–
BK	BK	kA	BK
Sibirtelekom-Lokomotiv	Sibirtelekom-Lokomotiv	k1gFnSc2	Sibirtelekom-Lokomotiv
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Superlize	superliga	k1gFnSc6	superliga
"	"	kIx"	"
<g/>
A	a	k9	a
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
D-	D-	k1gFnSc1	D-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
klub	klub	k1gInSc1	klub
–	–	k?	–
BK	BK	kA	BK
Dinamo-Energija	Dinamo-Energija	k1gFnSc1	Dinamo-Energija
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Superlize	superliga	k1gFnSc6	superliga
"	"	kIx"	"
<g/>
A	a	k9	a
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
D-	D-	k1gFnSc1	D-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HK	HK	kA	HK
Sibselmaš	Sibselmaš	k1gInSc1	Sibselmaš
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
–	–	k?	–
ruský	ruský	k2eAgMnSc1d1	ruský
mistr	mistr	k1gMnSc1	mistr
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
Vyšší	vysoký	k2eAgFnSc6d2	vyšší
lize	liga	k1gFnSc6	liga
(	(	kIx(	(
<g/>
D-	D-	k1gFnSc1	D-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
