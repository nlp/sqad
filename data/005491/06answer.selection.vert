<s>
Jakožto	jakožto	k8xS	jakožto
třetí	třetí	k4xOgNnSc1	třetí
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Sibiře	Sibiř	k1gFnSc2	Sibiř
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
obchodním	obchodní	k2eAgInSc7d1	obchodní
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgInSc7d1	kulturní
<g/>
,	,	kIx,	,
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
<g/>
,	,	kIx,	,
dopravním	dopravní	k2eAgInSc7d1	dopravní
a	a	k8xC	a
vědeckým	vědecký	k2eAgInSc7d1	vědecký
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
teprve	teprve	k9	teprve
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
Transsibiřské	transsibiřský	k2eAgFnSc2d1	Transsibiřská
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
.	.	kIx.	.
</s>
