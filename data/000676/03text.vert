<p>
<s>
Špenát	špenát	k1gInSc1	špenát
(	(	kIx(	(
<g/>
Spinacia	Spinacia	k1gFnSc1	Spinacia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
laskavcovitých	laskavcovitý	k2eAgFnPc2d1	laskavcovitý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starších	starý	k2eAgInPc6d2	starší
taxonomických	taxonomický	k2eAgInPc6d1	taxonomický
systémech	systém	k1gInPc6	systém
byla	být	k5eAaImAgFnS	být
řazena	řazen	k2eAgFnSc1d1	řazena
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
merlíkovité	merlíkovitý	k2eAgFnSc2d1	merlíkovitý
(	(	kIx(	(
<g/>
Chenopodiaceae	Chenopodiacea	k1gFnSc2	Chenopodiacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Špenát	špenát	k1gInSc1	špenát
je	být	k5eAaImIp3nS	být
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
druhem	druh	k1gInSc7	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
špenát	špenát	k1gInSc1	špenát
setý	setý	k2eAgInSc1d1	setý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
díky	díky	k7c3	díky
křižáckým	křižácký	k2eAgFnPc3d1	křižácká
výpravám	výprava	k1gFnPc3	výprava
kolem	kolem	k7c2	kolem
16.	[number]	k4	16.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
první	první	k4xOgFnSc2	první
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
rostlině	rostlina	k1gFnSc6	rostlina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
10.	[number]	k4	10.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Špenát	špenát	k1gInSc1	špenát
je	on	k3xPp3gMnPc4	on
zdraví	zdravit	k5eAaImIp3nS	zdravit
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
vitamínů	vitamín	k1gInPc2	vitamín
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
)	)	kIx)	)
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
špenát	špenát	k1gInSc1	špenát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
špenát	špenát	k1gInSc1	špenát
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Spinacia	Spinacium	k1gNnSc2	Spinacium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Špenát	špenát	k1gInSc1	špenát
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
člověka	člověk	k1gMnSc2	člověk
</s>
</p>
