<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
statistické	statistický	k2eAgFnSc6d1	statistická
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc6d1	kvantová
statistice	statistika	k1gFnSc6	statistika
(	(	kIx(	(
<g/>
Boseho-Einsteinovo	Boseho-Einsteinův	k2eAgNnSc1d1	Boseho-Einsteinův
rozdělení	rozdělení	k1gNnSc1	rozdělení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diskusi	diskuse	k1gFnSc4	diskuse
o	o	k7c4	o
interpretaci	interpretace	k1gFnSc4	interpretace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
(	(	kIx(	(
<g/>
diskuse	diskuse	k1gFnSc1	diskuse
s	s	k7c7	s
Bohrem	Bohro	k1gNnSc7	Bohro
<g/>
,	,	kIx,	,
EPR	EPR	kA	EPR
paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
