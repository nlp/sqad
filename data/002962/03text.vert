<s>
Šnekov	Šnekov	k1gInSc1	Šnekov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schneckendorf	Schneckendorf	k1gInSc1	Schneckendorf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
součást	součást	k1gFnSc1	součást
obce	obec	k1gFnSc2	obec
Březina	Březina	k1gFnSc1	Březina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
1,5	[number]	k4	1,5
km	km	kA	km
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Březiny	Březina	k1gFnSc2	Březina
a	a	k8xC	a
2	[number]	k4	2
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Křenov	Křenovo	k1gNnPc2	Křenovo
<g/>
,	,	kIx,	,
ves	ves	k1gFnSc1	ves
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
severojižním	severojižní	k2eAgInSc7d1	severojižní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Šnekovem	Šnekov	k1gInSc7	Šnekov
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
368	[number]	k4	368
z	z	k7c2	z
Letovic	Letovice	k1gFnPc2	Letovice
do	do	k7c2	do
Moravské	moravský	k2eAgFnSc2d1	Moravská
Třebové	Třebová	k1gFnSc2	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Wenzlausdorf	Wenzlausdorf	k1gInSc1	Wenzlausdorf
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1365	[number]	k4	1365
patřil	patřit	k5eAaImAgInS	patřit
pod	pod	k7c7	pod
Moravskou	moravský	k2eAgFnSc7d1	Moravská
Třebovou	Třebová	k1gFnSc7	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1408	[number]	k4	1408
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
známa	známo	k1gNnSc2	známo
pod	pod	k7c7	pod
německým	německý	k2eAgInSc7d1	německý
názvem	název	k1gInSc7	název
Dorfflss	Dorfflss	k1gInSc1	Dorfflss
bei	bei	k?	bei
Krönau	Krönaus	k1gInSc2	Krönaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1465	[number]	k4	1465
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
formu	forma	k1gFnSc4	forma
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
Dorflik	Dorflik	k1gMnSc1	Dorflik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nazývána	nazývat	k5eAaImNgFnS	nazývat
staročesky	staročesky	k6eAd1	staročesky
"	"	kIx"	"
<g/>
Dorfflik	Dorfflik	k1gMnSc1	Dorfflik
prope	propat	k5eAaPmIp3nS	propat
Chrzenowe	Chrzenowe	k1gFnSc4	Chrzenowe
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Crenaw	Crenaw	k1gFnSc1	Crenaw
Dorfflss	Dorfflssa	k1gFnPc2	Dorfflssa
dobey	dobea	k1gFnSc2	dobea
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
urbáři	urbář	k1gInSc6	urbář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1657	[number]	k4	1657
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Schneckendörfl	Schneckendörfl	k1gInSc1	Schneckendörfl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
bydlelo	bydlet	k5eAaImAgNnS	bydlet
ve	v	k7c6	v
33	[number]	k4	33
domech	dům	k1gInPc6	dům
238	[number]	k4	238
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
122	[number]	k4	122
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
116	[number]	k4	116
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřících	patřící	k2eAgFnPc2d1	patřící
ke	k	k7c3	k
křenovské	křenovský	k2eAgFnSc3d1	křenovská
farnosti	farnost	k1gFnSc3	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Obcí	obec	k1gFnSc7	obec
protéká	protékat	k5eAaImIp3nS	protékat
Malonínský	Malonínský	k2eAgInSc1d1	Malonínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
pramenící	pramenící	k2eAgFnSc1d1	pramenící
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Mühlbusch	Mühlbuscha	k1gFnPc2	Mühlbuscha
(	(	kIx(	(
<g/>
508	[number]	k4	508
m	m	kA	m
<g/>
)	)	kIx)	)
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Janůvky	Janůvka	k1gFnSc2	Janůvka
<g/>
.	.	kIx.	.
</s>
