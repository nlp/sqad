<p>
<s>
Kelpie	Kelpie	k1gFnSc1	Kelpie
je	být	k5eAaImIp3nS	být
nadpřirozená	nadpřirozený	k2eAgFnSc1d1	nadpřirozená
bytost	bytost	k1gFnSc1	bytost
z	z	k7c2	z
keltské	keltský	k2eAgFnSc2d1	keltská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
bere	brát	k5eAaImIp3nS	brát
podobu	podoba	k1gFnSc4	podoba
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
půlky	půlka	k1gFnSc2	půlka
těla	tělo	k1gNnSc2	tělo
rybí	rybí	k2eAgInSc1d1	rybí
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
však	však	k9	však
přeměnit	přeměnit	k5eAaPmF	přeměnit
i	i	k9	i
do	do	k7c2	do
lidské	lidský	k2eAgFnSc2d1	lidská
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
obývá	obývat	k5eAaImIp3nS	obývat
a	a	k8xC	a
straší	strašit	k5eAaImIp3nS	strašit
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
a	a	k8xC	a
jezerech	jezero	k1gNnPc6	jezero
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
jména	jméno	k1gNnSc2	jméno
těchto	tento	k3xDgMnPc2	tento
legendárních	legendární	k2eAgMnPc2d1	legendární
koní	kůň	k1gMnPc2	kůň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
cailpeach	cailpeach	k1gInSc4	cailpeach
nebo	nebo	k8xC	nebo
colpach	colpach	k1gInSc4	colpach
ze	z	k7c2	z
skotské	skotský	k2eAgFnSc2d1	skotská
galštiny	galština	k1gFnSc2	galština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
každé	každý	k3xTgNnSc1	každý
větší	veliký	k2eAgNnSc1d2	veliký
jezero	jezero	k1gNnSc1	jezero
či	či	k8xC	či
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
kelpie	kelpie	k1gFnSc2	kelpie
spojovány	spojovat	k5eAaImNgInP	spojovat
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
pojí	pojíst	k5eAaPmIp3nS	pojíst
s	s	k7c7	s
Loch	loch	k1gInSc4	loch
Ness	Nessa	k1gFnPc2	Nessa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
obdobné	obdobný	k2eAgFnPc4d1	obdobná
bytosti	bytost	k1gFnPc4	bytost
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
mytologiích	mytologie	k1gFnPc6	mytologie
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
slovanský	slovanský	k2eAgMnSc1d1	slovanský
vodník	vodník	k1gMnSc1	vodník
<g/>
,	,	kIx,	,
germánský	germánský	k2eAgMnSc1d1	germánský
nixe	nixe	k6eAd1	nixe
nebo	nebo	k8xC	nebo
japonská	japonský	k2eAgNnPc1d1	Japonské
kappa	kappa	k1gNnPc1	kappa
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
vodní	vodní	k2eAgMnPc1d1	vodní
duchové	duch	k1gMnPc1	duch
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
zlé	zlý	k2eAgFnPc4d1	zlá
a	a	k8xC	a
často	často	k6eAd1	často
lidojedé	lidojedý	k2eAgNnSc1d1	lidojedý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
umění	umění	k1gNnSc6	umění
mají	mít	k5eAaImIp3nP	mít
kelpie	kelpie	k1gFnSc2	kelpie
mnohá	mnohý	k2eAgNnPc4d1	mnohé
vyobrazení	vyobrazení	k1gNnPc4	vyobrazení
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
třicetimetrové	třicetimetrový	k2eAgFnPc1d1	třicetimetrová
ocelové	ocelový	k2eAgFnPc1d1	ocelová
sochy	socha	k1gFnPc1	socha
ve	v	k7c6	v
Falkirku	Falkirko	k1gNnSc6	Falkirko
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
The	The	k1gFnSc2	The
Kelpies	Kelpies	k1gMnSc1	Kelpies
(	(	kIx(	(
<g/>
Kelpie	Kelpie	k1gFnSc1	Kelpie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokončeny	dokončen	k2eAgFnPc1d1	dokončena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Folklor	folklor	k1gInSc1	folklor
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
jsou	být	k5eAaImIp3nP	být
kelpie	kelpie	k1gFnPc1	kelpie
popisovány	popisován	k2eAgFnPc1d1	popisována
jako	jako	k8xC	jako
silní	silný	k2eAgMnPc1d1	silný
a	a	k8xC	a
mocní	mocný	k2eAgMnPc1d1	mocný
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
braly	brát	k5eAaImAgFnP	brát
podobu	podoba	k1gFnSc4	podoba
vodního	vodní	k2eAgMnSc2d1	vodní
býka	býk	k1gMnSc2	býk
nebo	nebo	k8xC	nebo
telete	tele	k1gNnSc2	tele
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
useň	useň	k1gFnSc1	useň
byla	být	k5eAaImAgFnS	být
povětšinou	povětšinou	k6eAd1	povětšinou
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgInPc1	některý
příběhy	příběh	k1gInPc1	příběh
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
bílých	bílý	k2eAgMnPc6d1	bílý
koních	kůň	k1gMnPc6	kůň
<g/>
,	,	kIx,	,
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
se	se	k3xPyFc4	se
jevily	jevit	k5eAaImAgFnP	jevit
jako	jako	k8xS	jako
ztracení	ztracený	k2eAgMnPc1d1	ztracený
koně	kůň	k1gMnPc1	kůň
nebo	nebo	k8xC	nebo
poníci	poník	k1gMnPc1	poník
–	–	k?	–
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
se	se	k3xPyFc4	se
však	však	k9	však
daly	dát	k5eAaPmAgFnP	dát
díky	díky	k7c3	díky
neustále	neustále	k6eAd1	neustále
mokré	mokrý	k2eAgFnSc3d1	mokrá
hřívě	hříva	k1gFnSc3	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kůže	kůže	k1gFnSc1	kůže
vypadala	vypadat	k5eAaImAgFnS	vypadat
jako	jako	k9	jako
tulení	tulení	k2eAgFnSc1d1	tulení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hladká	hladkat	k5eAaImIp3nS	hladkat
a	a	k8xC	a
na	na	k7c4	na
omak	omak	k1gInSc4	omak
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
kelpie	kelpie	k1gFnPc1	kelpie
uměly	umět	k5eAaImAgFnP	umět
přeměnit	přeměnit	k5eAaPmF	přeměnit
v	v	k7c4	v
krásné	krásný	k2eAgFnPc4d1	krásná
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lákaly	lákat	k5eAaImAgFnP	lákat
muže	muž	k1gMnPc4	muž
do	do	k7c2	do
pastí	past	k1gFnPc2	past
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
uměly	umět	k5eAaImAgFnP	umět
vzít	vzít	k5eAaPmF	vzít
i	i	k9	i
podobu	podoba	k1gFnSc4	podoba
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
odvíjelo	odvíjet	k5eAaImAgNnS	odvíjet
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
koho	kdo	k3yInSc4	kdo
chtěly	chtít	k5eAaImAgFnP	chtít
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
nalákat	nalákat	k5eAaPmF	nalákat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
uměly	umět	k5eAaImAgInP	umět
vytvářet	vytvářet	k5eAaImF	vytvářet
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
je	on	k3xPp3gFnPc4	on
udržovaly	udržovat	k5eAaImAgInP	udržovat
skryté	skrytý	k2eAgInPc1d1	skrytý
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
poutníků	poutník	k1gMnPc2	poutník
–	–	k?	–
nad	nad	k7c4	nad
vodu	voda	k1gFnSc4	voda
vystrkovaly	vystrkovat	k5eAaImAgFnP	vystrkovat
pouze	pouze	k6eAd1	pouze
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
kelpiích	kelpie	k1gFnPc6	kelpie
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
region	region	k1gInSc1	region
od	od	k7c2	od
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
verze	verze	k1gFnPc1	verze
<g/>
,	,	kIx,	,
než	než	k8xS	než
výše	vysoce	k6eAd2	vysoce
popsaná	popsaný	k2eAgFnSc1d1	popsaná
<g/>
,	,	kIx,	,
popisují	popisovat	k5eAaImIp3nP	popisovat
kelpie	kelpie	k1gFnPc1	kelpie
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
zelené	zelené	k1gNnSc1	zelené
jako	jako	k8xC	jako
sklo	sklo	k1gNnSc1	sklo
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
srstí	srst	k1gFnSc7	srst
a	a	k8xC	a
ocasem	ocas	k1gInSc7	ocas
otočeným	otočený	k2eAgInSc7d1	otočený
k	k	k7c3	k
zádům	záda	k1gNnPc3	záda
do	do	k7c2	do
velkého	velký	k2eAgNnSc2d1	velké
kola	kolo	k1gNnSc2	kolo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
formě	forma	k1gFnSc6	forma
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
pak	pak	k6eAd1	pak
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
kapala	kapat	k5eAaImAgFnS	kapat
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
měly	mít	k5eAaImAgFnP	mít
rákosí	rákosí	k1gNnSc4	rákosí
ve	v	k7c6	v
vlasech	vlas	k1gInPc6	vlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgMnSc1d1	vodní
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
forma	forma	k1gFnSc1	forma
kelpie	kelpie	k1gFnSc2	kelpie
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
lákaly	lákat	k5eAaImAgFnP	lákat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
utopily	utopit	k5eAaPmAgInP	utopit
a	a	k8xC	a
sežraly	sežrat	k5eAaPmAgInP	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
koně	kůň	k1gMnSc2	kůň
povzbuzovaly	povzbuzovat	k5eAaImAgFnP	povzbuzovat
děti	dítě	k1gFnPc1	dítě
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
jejich	jejich	k3xOp3gFnSc1	jejich
oběť	oběť	k1gFnSc1	oběť
padla	padnout	k5eAaPmAgFnS	padnout
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
kůže	kůže	k1gFnSc1	kůže
začala	začít	k5eAaPmAgFnS	začít
lepit	lepit	k5eAaImF	lepit
a	a	k8xC	a
kůň	kůň	k1gMnSc1	kůň
odnesl	odnést	k5eAaPmAgMnS	odnést
dítě	dítě	k1gNnSc4	dítě
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
zhltl	zhltnout	k5eAaPmAgInS	zhltnout
–	–	k?	–
kromě	kromě	k7c2	kromě
srdce	srdce	k1gNnSc2	srdce
nebo	nebo	k8xC	nebo
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
příběhem	příběh	k1gInSc7	příběh
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
devíti	devět	k4xCc6	devět
dětech	dítě	k1gFnPc6	dítě
nalákaných	nalákaný	k2eAgInPc2d1	nalákaný
na	na	k7c4	na
hřbet	hřbet	k1gInSc4	hřbet
kelpie	kelpie	k1gFnSc2	kelpie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
desáté	desátý	k4xOgNnSc4	desátý
dítě	dítě	k1gNnSc4	dítě
se	se	k3xPyFc4	se
drželo	držet	k5eAaImAgNnS	držet
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Kelpie	Kelpie	k1gFnSc1	Kelpie
toto	tento	k3xDgNnSc4	tento
dítě	dítě	k1gNnSc4	dítě
pronásledovala	pronásledovat	k5eAaImAgFnS	pronásledovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
jí	on	k3xPp3gFnSc3	on
uniklo	uniknout	k5eAaPmAgNnS	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
příběhů	příběh	k1gInPc2	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
desáté	desátý	k4xOgNnSc1	desátý
dítě	dítě	k1gNnSc1	dítě
pohladilo	pohladit	k5eAaPmAgNnS	pohladit
kelpii	kelpie	k1gFnSc4	kelpie
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
přilepila	přilepit	k5eAaPmAgFnS	přilepit
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
vytáhlo	vytáhnout	k5eAaPmAgNnS	vytáhnout
nůž	nůž	k1gInSc4	nůž
a	a	k8xC	a
končetinu	končetina	k1gFnSc4	končetina
si	se	k3xPyFc3	se
uřízlo	uříznout	k5eAaPmAgNnS	uříznout
a	a	k8xC	a
ránu	ráno	k1gNnSc6	ráno
vypálilo	vypálit	k5eAaPmAgNnS	vypálit
dřevem	dřevo	k1gNnSc7	dřevo
z	z	k7c2	z
nedalekého	daleký	k2eNgInSc2d1	nedaleký
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Desáté	desátý	k4xOgNnSc1	desátý
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
pomoci	pomoct	k5eAaPmF	pomoct
svým	svůj	k3xOyFgNnSc7	svůj
devíti	devět	k4xCc3	devět
přátelům	přítel	k1gMnPc3	přítel
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
kelpie	kelpie	k1gFnSc1	kelpie
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
pod	pod	k7c4	pod
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Loch	loch	k1gInSc1	loch
Ness	Ness	k1gInSc1	Ness
==	==	k?	==
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
každá	každý	k3xTgFnSc1	každý
větší	veliký	k2eAgFnSc1d2	veliký
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
kelpie	kelpie	k1gFnSc1	kelpie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
o	o	k7c4	o
kelpie	kelpie	k1gFnPc4	kelpie
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Loch	loch	k1gInSc4	loch
Ness	Nessa	k1gFnPc2	Nessa
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
příběhů	příběh	k1gInPc2	příběh
o	o	k7c6	o
mýtických	mýtický	k2eAgFnPc6d1	mýtická
duších	duše	k1gFnPc6	duše
a	a	k8xC	a
monstrech	monstrum	k1gNnPc6	monstrum
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
ke	k	k7c3	k
zprávám	zpráva	k1gFnPc3	zpráva
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgFnPc2	který
tam	tam	k6eAd1	tam
svatý	svatý	k1gMnSc1	svatý
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
porazil	porazit	k5eAaPmAgMnS	porazit
monstrum	monstrum	k1gNnSc4	monstrum
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
kelpie	kelpie	k1gFnPc4	kelpie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
tam	tam	k6eAd1	tam
kelpie	kelpie	k1gFnSc1	kelpie
strašila	strašit	k5eAaImAgFnS	strašit
v	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Loch	loch	k1gInSc4	loch
Ness	Nessa	k1gFnPc2	Nessa
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyla	být	k5eNaImAgFnS	být
osedlána	osedlat	k5eAaPmNgFnS	osedlat
vlastním	vlastní	k2eAgNnSc7d1	vlastní
sedlem	sedlo	k1gNnSc7	sedlo
a	a	k8xC	a
ohlávkou	ohlávka	k1gFnSc7	ohlávka
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
horal	horal	k1gMnSc1	horal
James	James	k1gMnSc1	James
MacGrigor	MacGrigor	k1gMnSc1	MacGrigor
kelpie	kelpie	k1gFnSc2	kelpie
překvapil	překvapit	k5eAaPmAgMnS	překvapit
a	a	k8xC	a
přesekl	přeseknout	k5eAaPmAgMnS	přeseknout
jí	jíst	k5eAaImIp3nS	jíst
ohlávku	ohlávka	k1gFnSc4	ohlávka
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc4	zdroj
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
které	který	k3yQgFnSc2	který
do	do	k7c2	do
čtyřiadvaceti	čtyřiadvacet	k4xCc2	čtyřiadvacet
hodin	hodina	k1gFnPc2	hodina
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Kelpie	Kelpie	k1gFnSc1	Kelpie
uměla	umět	k5eAaImAgFnS	umět
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
pokusila	pokusit	k5eAaPmAgFnS	pokusit
s	s	k7c7	s
MacGrigorem	MacGrigor	k1gInSc7	MacGrigor
smlouvat	smlouvat	k5eAaImF	smlouvat
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
ohlávky	ohlávka	k1gFnSc2	ohlávka
<g/>
.	.	kIx.	.
</s>
<s>
Šla	jít	k5eAaImAgFnS	jít
za	za	k7c7	za
MacGrigorem	MacGrigor	k1gInSc7	MacGrigor
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
domů	domů	k6eAd1	domů
a	a	k8xC	a
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
MacGrigor	MacGrigor	k1gMnSc1	MacGrigor
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
vejít	vejít	k5eAaPmF	vejít
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
ohlávku	ohlávka	k1gFnSc4	ohlávka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ho	on	k3xPp3gInSc4	on
kříž	kříž	k1gInSc4	kříž
nad	nad	k7c7	nad
dveřmi	dveře	k1gFnPc7	dveře
nepustí	pustit	k5eNaPmIp3nP	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
MacGrigor	MacGrigor	k1gInSc1	MacGrigor
tvora	tvor	k1gMnSc2	tvor
přechytračil	přechytračit	k5eAaPmAgInS	přechytračit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ohlávku	ohlávka	k1gFnSc4	ohlávka
prohodil	prohodit	k5eAaPmAgInS	prohodit
dovnitř	dovnitř	k6eAd1	dovnitř
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Kelpie	Kelpie	k1gFnSc1	Kelpie
se	se	k3xPyFc4	se
smířila	smířit	k5eAaPmAgFnS	smířit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
osudem	osud	k1gInSc7	osud
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
s	s	k7c7	s
kletbami	kletba	k1gFnPc7	kletba
a	a	k8xC	a
nadávkami	nadávka	k1gFnPc7	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
je	být	k5eAaImIp3nS	být
pokračován	pokračován	k2eAgInSc1d1	pokračován
dalšími	další	k2eAgInPc7d1	další
příběhy	příběh	k1gInPc7	příběh
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
ohlávka	ohlávka	k1gFnSc1	ohlávka
dědila	dědit	k5eAaImAgFnS	dědit
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Prý	prý	k9	prý
měla	mít	k5eAaImAgFnS	mít
kouzelné	kouzelný	k2eAgFnPc4d1	kouzelná
léčivé	léčivý	k2eAgFnPc4d1	léčivá
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vložila	vložit	k5eAaPmAgFnS	vložit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
proneslo	pronést	k5eAaPmAgNnS	pronést
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Syna	syn	k1gMnSc2	syn
i	i	k8xC	i
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k1gMnSc2	svatý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ohlávka	ohlávka	k1gFnSc1	ohlávka
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zkrocení	zkrocení	k1gNnSc4	zkrocení
kelpie	kelpie	k1gFnSc2	kelpie
používá	používat	k5eAaImIp3nS	používat
napříč	napříč	k6eAd1	napříč
mnoho	mnoho	k4c4	mnoho
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kelpie	kelpie	k1gFnSc1	kelpie
lidem	lid	k1gInSc7	lid
neublížila	ublížit	k5eNaPmAgFnS	ublížit
a	a	k8xC	a
vzdálila	vzdálit	k5eAaPmAgFnS	vzdálit
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
bezostyšně	bezostyšně	k6eAd1	bezostyšně
využívána	využívat	k5eAaPmNgFnS	využívat
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
nošení	nošení	k1gNnSc3	nošení
kamení	kamení	k1gNnSc2	kamení
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vykonání	vykonání	k1gNnSc6	vykonání
práce	práce	k1gFnSc2	práce
byly	být	k5eAaImAgFnP	být
obvykle	obvykle	k6eAd1	obvykle
propuštěny	propuštěn	k2eAgFnPc1d1	propuštěna
a	a	k8xC	a
podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
se	se	k3xPyFc4	se
už	už	k6eAd1	už
znovu	znovu	k6eAd1	znovu
neobjevily	objevit	k5eNaPmAgFnP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
byly	být	k5eAaImAgFnP	být
kelpie	kelpie	k1gFnPc1	kelpie
přemoženy	přemožen	k2eAgFnPc1d1	přemožena
i	i	k9	i
jen	jen	k9	jen
pouhou	pouhý	k2eAgFnSc7d1	pouhá
lidskou	lidský	k2eAgFnSc7d1	lidská
sílou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
ohlávky	ohlávka	k1gFnSc2	ohlávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzaly	vzít	k5eAaPmAgFnP	vzít
lidskou	lidský	k2eAgFnSc4d1	lidská
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Folklorista	folklorista	k1gMnSc1	folklorista
Gary	Gara	k1gFnSc2	Gara
R.	R.	kA	R.
Varner	Varner	k1gMnSc1	Varner
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
víra	víra	k1gFnSc1	víra
ve	v	k7c4	v
vodní	vodní	k2eAgMnPc4d1	vodní
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pozřívali	pozřívat	k5eAaImAgMnP	pozřívat
zejména	zejména	k9	zejména
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
zrodit	zrodit	k5eAaPmF	zrodit
z	z	k7c2	z
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
podstupovány	podstupován	k2eAgInPc1d1	podstupován
vodním	vodní	k2eAgInSc7d1	vodní
božstvům	božstvo	k1gNnPc3	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
koňmi	kůň	k1gMnPc7	kůň
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
koňských	koňský	k2eAgFnPc2d1	koňská
obětí	oběť	k1gFnPc2	oběť
prováděných	prováděný	k2eAgFnPc2d1	prováděná
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
o	o	k7c6	o
zlých	zlý	k2eAgFnPc6d1	zlá
vodních	vodní	k2eAgFnPc6d1	vodní
duších	duše	k1gFnPc6	duše
sloužily	sloužit	k5eAaImAgFnP	sloužit
k	k	k7c3	k
praktickému	praktický	k2eAgInSc3d1	praktický
účelu	účel	k1gInSc3	účel
–	–	k?	–
udržení	udržení	k1gNnSc2	udržení
dětí	dítě	k1gFnPc2	dítě
od	od	k7c2	od
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
k	k	k7c3	k
varování	varování	k1gNnSc3	varování
dospívajících	dospívající	k2eAgFnPc2d1	dospívající
žen	žena	k1gFnPc2	žena
před	před	k7c7	před
přitažlivými	přitažlivý	k2eAgMnPc7d1	přitažlivý
cizími	cizí	k2eAgMnPc7d1	cizí
mládenci	mládenec	k1gMnPc7	mládenec
<g/>
.	.	kIx.	.
</s>
<s>
Démoni	démon	k1gMnPc1	démon
a	a	k8xC	a
duchové	duch	k1gMnPc1	duch
byli	být	k5eAaImAgMnP	být
také	také	k6eAd1	také
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
logické	logický	k2eAgNnSc4d1	logické
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
utopili	utopit	k5eAaPmAgMnP	utopit
děti	dítě	k1gFnPc4	dítě
i	i	k8xC	i
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
omylem	omylem	k6eAd1	omylem
spadli	spadnout	k5eAaPmAgMnP	spadnout
do	do	k7c2	do
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
nebo	nebo	k8xC	nebo
rozbouřených	rozbouřený	k2eAgFnPc2d1	rozbouřená
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historik	historik	k1gMnSc1	historik
a	a	k8xC	a
symbolog	symbolog	k1gMnSc1	symbolog
Charles	Charles	k1gMnSc1	Charles
Milton	Milton	k1gInSc4	Milton
Smith	Smith	k1gMnSc1	Smith
předložil	předložit	k5eAaPmAgMnS	předložit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c4	o
kelpie	kelpie	k1gFnPc4	kelpie
kvůli	kvůli	k7c3	kvůli
vodním	vodní	k2eAgFnPc3d1	vodní
smrštím	smršť	k1gFnPc3	smršť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nad	nad	k7c7	nad
skotskými	skotský	k2eAgNnPc7d1	skotské
jezery	jezero	k1gNnPc7	jezero
objevují	objevovat	k5eAaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pohybu	pohyb	k1gInSc2	pohyb
totiž	totiž	k9	totiž
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
živé	živý	k2eAgFnPc1d1	živá
bytosti	bytost	k1gFnPc1	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
Walter	Walter	k1gMnSc1	Walter
Scott	Scott	k1gMnSc1	Scott
naráží	narážet	k5eAaPmIp3nS	narážet
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
epické	epický	k2eAgFnSc6d1	epická
básni	báseň	k1gFnSc6	báseň
Jezerní	jezerní	k2eAgFnSc1d1	jezerní
panna	panna	k1gFnSc1	panna
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
na	na	k7c4	na
podobné	podobný	k2eAgNnSc4d1	podobné
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současná	současný	k2eAgFnSc1d1	současná
kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc4d1	Rowlingová
Fantastická	fantastický	k2eAgNnPc4d1	fantastické
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	on	k3xPp3gNnSc4	on
najít	najít	k5eAaPmF	najít
jsou	být	k5eAaImIp3nP	být
kelpie	kelpie	k1gFnPc1	kelpie
popisovány	popisován	k2eAgFnPc1d1	popisována
jako	jako	k8xC	jako
kožoměnci	kožoměnec	k1gMnPc1	kožoměnec
přirozeně	přirozeně	k6eAd1	přirozeně
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněno	zmíněn	k2eAgNnSc1d1	zmíněno
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lochnesská	Lochnesský	k2eAgFnSc1d1	Lochnesský
příšera	příšera	k1gFnSc1	příšera
je	být	k5eAaImIp3nS	být
gigantická	gigantický	k2eAgFnSc1d1	gigantická
kelpie	kelpie	k1gFnSc1	kelpie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
podobou	podoba	k1gFnSc7	podoba
je	být	k5eAaImIp3nS	být
mořský	mořský	k2eAgMnSc1d1	mořský
had	had	k1gMnSc1	had
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Daň	daň	k1gFnSc1	daň
peklu	peklo	k1gNnSc3	peklo
autorky	autorka	k1gFnSc2	autorka
Holly	Holla	k1gFnSc2	Holla
Blackové	Blacková	k1gFnSc2	Blacková
(	(	kIx(	(
<g/>
známá	známý	k2eAgFnSc1d1	známá
mj.	mj.	kA	mj.
díky	díky	k7c3	díky
knize	kniha	k1gFnSc3	kniha
Kronika	kronika	k1gFnSc1	kronika
rodu	rod	k1gInSc2	rod
Spiderwicků	Spiderwicek	k1gMnPc2	Spiderwicek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kelpie	kelpie	k1gFnSc1	kelpie
vykreslena	vykreslen	k2eAgFnSc1d1	vykreslena
jako	jako	k8xC	jako
kůň	kůň	k1gMnSc1	kůň
i	i	k9	i
jako	jako	k9	jako
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
formě	forma	k1gFnSc6	forma
používá	používat	k5eAaImIp3nS	používat
hypnózy	hypnóza	k1gFnPc4	hypnóza
k	k	k7c3	k
utopení	utopení	k1gNnSc3	utopení
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
Stormwatch	Stormwatcha	k1gFnPc2	Stormwatcha
skupiny	skupina	k1gFnSc2	skupina
Jethro	Jethra	k1gFnSc5	Jethra
Tull	Tull	k1gMnSc1	Tull
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skladbu	skladba	k1gFnSc4	skladba
Kelpie	Kelpie	k1gFnSc2	Kelpie
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
ochotný	ochotný	k2eAgMnSc1d1	ochotný
následovat	následovat	k5eAaImF	následovat
kelpii	kelpie	k1gFnSc4	kelpie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nejprve	nejprve	k6eAd1	nejprve
zjeví	zjevit	k5eAaPmIp3nS	zjevit
jako	jako	k9	jako
krásná	krásný	k2eAgFnSc1d1	krásná
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
manze	manz	k1gInSc6	manz
Berserk	berserk	k1gMnSc1	berserk
je	být	k5eAaImIp3nS	být
kelpie	kelpie	k1gFnSc1	kelpie
směsí	směs	k1gFnSc7	směs
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
žáby	žába	k1gFnSc2	žába
<g/>
.	.	kIx.	.
</s>
<s>
Kelpie	Kelpie	k1gFnPc1	Kelpie
zde	zde	k6eAd1	zde
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
trollího	trollí	k2eAgInSc2d1	trollí
útoku	útok	k1gInSc2	útok
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Pokémon	Pokémon	k1gMnSc1	Pokémon
je	být	k5eAaImIp3nS	být
Pokémon	Pokémon	k1gInSc4	Pokémon
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
Keldeo	Keldeo	k1gMnSc1	Keldeo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
a	a	k8xC	a
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
kelpií	kelpie	k1gFnPc2	kelpie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interpretace	interpretace	k1gFnSc1	interpretace
kelpie	kelpie	k1gFnSc1	kelpie
je	být	k5eAaImIp3nS	být
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
v	v	k7c6	v
průvodci	průvodce	k1gMnPc1	průvodce
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
postavy	postava	k1gFnSc2	postava
Arthura	Arthura	k1gFnSc1	Arthura
Spiderwicka	Spiderwicka	k1gFnSc1	Spiderwicka
ze	z	k7c2	z
série	série	k1gFnSc2	série
knih	kniha	k1gFnPc2	kniha
Kronika	kronika	k1gFnSc1	kronika
rodu	rod	k1gInSc2	rod
Spiderwicků	Spiderwicek	k1gMnPc2	Spiderwicek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ciriina	Ciriin	k2eAgFnSc1d1	Ciriin
Kelpie	Kelpie	k1gFnSc1	Kelpie
z	z	k7c2	z
polské	polský	k2eAgFnSc2d1	polská
fantasy	fantas	k1gInPc1	fantas
ságy	sága	k1gFnSc2	sága
o	o	k7c6	o
Zaklínači	zaklínač	k1gMnSc6	zaklínač
od	od	k7c2	od
A.	A.	kA	A.
Sapkowského	Sapkowského	k2eAgFnSc2d1	Sapkowského
</s>
</p>
<p>
<s>
kelpie	kelpie	k1gFnSc1	kelpie
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
creepypasty	creepypast	k1gInPc7	creepypast
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kelpie	Kelpie	k1gFnSc2	Kelpie
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgFnPc4d1	související
stránky	stránka	k1gFnPc4	stránka
===	===	k?	===
</s>
</p>
<p>
<s>
Australská	australský	k2eAgFnSc1d1	australská
kelpie	kelpie	k1gFnSc1	kelpie
</s>
</p>
<p>
<s>
Boobrie	Boobrie	k1gFnSc1	Boobrie
</s>
</p>
<p>
<s>
Eachy	Eacha	k1gFnPc1	Eacha
</s>
</p>
<p>
<s>
Kappa	kappa	k1gNnSc1	kappa
</s>
</p>
<p>
<s>
Each	Each	k1gInSc1	Each
uisge	uisgat	k5eAaPmIp3nS	uisgat
</s>
</p>
<p>
<s>
Nixe	Nixe	k6eAd1	Nixe
</s>
</p>
<p>
<s>
Ceffyl	Ceffyl	k1gInSc1	Ceffyl
dŵ	dŵ	k?	dŵ
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kelpie	Kelpie	k1gFnSc2	Kelpie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
