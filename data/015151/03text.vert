<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1910	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1936	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1911	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1865	#num#	k4
</s>
<s>
Marlborough	Marlborough	k1gMnSc1
House	house	k1gNnSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1936	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
70	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Sandringham	Sandringham	k6eAd1
House	house	k1gNnSc1
<g/>
,	,	kIx,
Norfolk	Norfolk	k1gInSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
<g/>
,	,	kIx,
Windsorský	windsorský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Tecku	Teck	k1gInSc2
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
EduardAlbert	EduardAlbert	k1gMnSc1
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
<g/>
)	)	kIx)
MarieHenryJiříJan	MarieHenryJiříJan	k1gMnSc1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Sachsen-Coburg	Sachsen-Coburg	k1gInSc1
und	und	k?
Gotha	Gotha	k1gFnSc1
Windsorská	windsorský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
George	George	k1gInSc1
V	V	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1865	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1936	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
britských	britský	k2eAgFnPc2d1
dominií	dominie	k1gFnPc2
a	a	k8xC
indický	indický	k2eAgMnSc1d1
císař	císař	k1gMnSc1
od	od	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
z	z	k7c2
windsorské	windsorský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
vytvořil	vytvořit	k5eAaPmAgMnS
přejmenováním	přejmenování	k1gNnSc7
britské	britský	k2eAgFnSc2d1
větve	větev	k1gFnSc2
dynastie	dynastie	k1gFnSc2
Sachsen-Coburg	Sachsen-Coburg	k1gMnSc1
und	und	k?
Gotha	Gotha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
dvanácti	dvanáct	k4xCc2
let	léto	k1gNnPc2
sloužil	sloužit	k5eAaImAgInS
u	u	k7c2
britského	britský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
náhlé	náhlý	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
staršího	starý	k2eAgMnSc2d2
bratra	bratr	k1gMnSc2
Alberta	Albert	k1gMnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
princem	princ	k1gMnSc7
waleským	waleský	k2eAgFnPc3d1
–	–	k?
následníkem	následník	k1gMnSc7
trůnu	trůn	k1gInSc2
–	–	k?
a	a	k8xC
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
bratrovou	bratrová	k1gFnSc7
snoubenkou	snoubenka	k1gFnSc7
Marií	Maria	k1gFnSc7
z	z	k7c2
Tecku	Teck	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
jako	jako	k9
královský	královský	k2eAgInSc4d1
pár	pár	k1gInSc4
občas	občas	k6eAd1
podnikli	podniknout	k5eAaPmAgMnP
cestu	cesta	k1gFnSc4
po	po	k7c6
zemích	zem	k1gFnPc6
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
upřednostňoval	upřednostňovat	k5eAaImAgMnS
konvenční	konvenční	k2eAgInSc4d1
styl	styl	k1gInSc4
života	život	k1gInSc2
a	a	k8xC
trávil	trávit	k5eAaImAgMnS
čas	čas	k1gInSc4
se	s	k7c7
sbírkami	sbírka	k1gFnPc7
poštovních	poštovní	k2eAgFnPc2d1
známek	známka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1910	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jediným	jediný	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
navštívil	navštívit	k5eAaPmAgMnS
císařský	císařský	k2eAgInSc4d1
dvůr	dvůr	k1gInSc4
v	v	k7c6
Dillí	Dillí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
korunován	korunován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tuto	tento	k3xDgFnSc4
příležitost	příležitost	k1gFnSc4
byla	být	k5eAaImAgFnS
zhotovena	zhotoven	k2eAgFnSc1d1
indická	indický	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
zřekl	zřeknout	k5eAaPmAgMnS
všech	všecek	k3xTgInPc2
svých	svůj	k3xOyFgInPc2
německých	německý	k2eAgInPc2d1
titulů	titul	k1gInPc2
a	a	k8xC
změnil	změnit	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
svého	svůj	k3xOyFgInSc2
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
"	"	kIx"
<g/>
Sachsen-Coburg	Sachsen-Coburg	k1gInSc1
und	und	k?
Gotha	Gotha	k1gFnSc1
<g/>
"	"	kIx"
na	na	k7c4
rod	rod	k1gInSc4
"	"	kIx"
<g/>
Windsor	Windsor	k1gInSc4
<g/>
"	"	kIx"
(	(	kIx(
<g/>
podle	podle	k7c2
stejnojmenného	stejnojmenný	k2eAgInSc2d1
sídelního	sídelní	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
po	po	k7c6
přijetí	přijetí	k1gNnSc6
příslušného	příslušný	k2eAgInSc2d1
zákona	zákon	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
oddělení	oddělení	k1gNnSc3
správy	správa	k1gFnSc2
dominií	dominie	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
byl	být	k5eAaImAgMnS
vládcem	vládce	k1gMnSc7
dominií	dominie	k1gFnPc2
jako	jako	k8xS,k8xC
oddělených	oddělený	k2eAgNnPc2d1
království	království	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
připravilo	připravit	k5eAaPmAgNnS
cestu	cesta	k1gFnSc4
ke	k	k7c3
vzniku	vznik	k1gInSc3
Commonwealthu	Commonwealth	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
panování	panování	k1gNnSc2
bylo	být	k5eAaImAgNnS
poznamenáno	poznamenat	k5eAaPmNgNnS
rozmachem	rozmach	k1gInSc7
socialismu	socialismus	k1gInSc2
<g/>
,	,	kIx,
komunismu	komunismus	k1gInSc2
<g/>
,	,	kIx,
fašismu	fašismus	k1gInSc2
<g/>
,	,	kIx,
irského	irský	k2eAgInSc2d1
republikanismu	republikanismus	k1gInSc2
a	a	k8xC
zvolením	zvolení	k1gNnSc7
prvního	první	k4xOgMnSc2
britského	britský	k2eAgMnSc2d1
labouristického	labouristický	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
zůstalo	zůstat	k5eAaPmAgNnS
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
ještě	ještě	k6eAd1
relativně	relativně	k6eAd1
nedotčené	dotčený	k2eNgInPc1d1
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
se	se	k3xPyFc4
britský	britský	k2eAgInSc1d1
vliv	vliv	k1gInSc1
ještě	ještě	k6eAd1
rozrostl	rozrůst	k5eAaPmAgInS
o	o	k7c4
mandátní	mandátní	k2eAgNnSc4d1
území	území	k1gNnSc4
v	v	k7c6
Asii	Asie	k1gFnSc6
a	a	k8xC
Africe	Afrika	k1gFnSc6
odňatá	odňatý	k2eAgFnSc1d1
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
Turecku	Turecko	k1gNnSc6
a	a	k8xC
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
brzké	brzký	k2eAgFnSc6d1
abdikaci	abdikace	k1gFnSc6
nahradil	nahradit	k5eAaPmAgMnS
druhorozený	druhorozený	k2eAgMnSc1d1
Albert	Albert	k1gMnSc1
(	(	kIx(
<g/>
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1865	#num#	k4
v	v	k7c4
Marlborough	Marlborough	k1gInSc4
House	house	k1gNnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
jako	jako	k9
druhý	druhý	k4xOgMnSc1
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Eduarda	Eduard	k1gMnSc2
<g/>
,	,	kIx,
prince	princ	k1gMnSc2
waleského	waleský	k2eAgMnSc2d1
(	(	kIx(
<g/>
pozdějšího	pozdní	k2eAgMnSc2d2
krále	král	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
manželky	manželka	k1gFnPc4
Alexandry	Alexandra	k1gFnSc2
Dánské	dánský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
měl	mít	k5eAaImAgMnS
staršího	starý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
<g/>
,	,	kIx,
neočekávalo	očekávat	k5eNaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
někdy	někdy	k6eAd1
stát	stát	k5eAaImF,k5eAaPmF
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Albert	Albert	k1gMnSc1
byl	být	k5eAaImAgMnS
jen	jen	k9
o	o	k7c4
sedmnáct	sedmnáct	k4xCc4
měsíců	měsíc	k1gInPc2
starší	starý	k2eAgMnSc1d2
a	a	k8xC
tak	tak	k6eAd1
byli	být	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
princové	princ	k1gMnPc1
vychováváni	vychovávat	k5eAaImNgMnP
společně	společně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1877	#num#	k4
strávili	strávit	k5eAaPmAgMnP
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
na	na	k7c6
výcvikové	výcvikový	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
Brittania	Brittanium	k1gNnSc2
v	v	k7c6
Dartmouthu	Dartmouth	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1879	#num#	k4
oba	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
sloužili	sloužit	k5eAaImAgMnP
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
na	na	k7c6
lodi	loď	k1gFnSc6
Bacchante	Bacchant	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
lodi	loď	k1gFnSc6
procestovali	procestovat	k5eAaPmAgMnP
různé	různý	k2eAgFnPc4d1
kolonie	kolonie	k1gFnPc4
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
v	v	k7c6
Karibiku	Karibik	k1gInSc6
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
navštívili	navštívit	k5eAaPmAgMnP
Jižní	jižní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
<g/>
,	,	kIx,
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc4
a	a	k8xC
Dálný	dálný	k2eAgInSc4d1
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
se	se	k3xPyFc4
bratři	bratr	k1gMnPc1
rozdělili	rozdělit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Albert	Albert	k1gMnSc1
začal	začít	k5eAaPmAgMnS
navštěvovat	navštěvovat	k5eAaImF
Trinity	Trinit	k2eAgFnPc4d1
College	Colleg	k1gFnPc4
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Jiří	Jiří	k1gMnSc1
dále	daleko	k6eAd2
pokračoval	pokračovat	k5eAaImAgMnS
ve	v	k7c6
službě	služba	k1gFnSc6
u	u	k7c2
královského	královský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
portrét	portrét	k1gInSc1
Jiřího	Jiří	k1gMnSc2
manželky	manželka	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
TeckuJako	TeckuJako	k1gNnSc1
mladý	mladý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
a	a	k8xC
námořník	námořník	k1gMnSc1
sloužil	sloužit	k5eAaImAgMnS
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
svého	svůj	k3xOyFgMnSc2
strýce	strýc	k1gMnSc2
prince	princ	k1gMnSc2
Alfréda	Alfréd	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
Maltě	Malta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
zamiloval	zamilovat	k5eAaPmAgMnS
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
dcery	dcera	k1gFnSc2
a	a	k8xC
své	svůj	k3xOyFgFnSc2
sestřenice	sestřenice	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
babička	babička	k1gFnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
i	i	k8xC
strýc	strýc	k1gMnSc1
s	s	k7c7
tímto	tento	k3xDgInSc7
vztahem	vztah	k1gInSc7
souhlasili	souhlasit	k5eAaImAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
obě	dva	k4xCgFnPc1
matky	matka	k1gFnPc1
byly	být	k5eAaImAgFnP
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
nátlak	nátlak	k1gInSc4
své	svůj	k3xOyFgFnSc2
matky	matka	k1gFnSc2
Marie	Maria	k1gFnSc2
Jiřího	Jiří	k1gMnSc2
nabídku	nabídka	k1gFnSc4
odmítla	odmítnout	k5eAaPmAgFnS
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
královnou	královna	k1gFnSc7
Rumunska	Rumunsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1891	#num#	k4
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc1
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Albert	Albert	k1gMnSc1
zasnoubil	zasnoubit	k5eAaPmAgMnS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
druhou	druhý	k4xOgFnSc7
sestřenicí	sestřenice	k1gFnSc7
Marií	Maria	k1gFnSc7
z	z	k7c2
Tecku	Teck	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Albert	Albert	k1gMnSc1
po	po	k7c6
půl	půl	k1xP
roce	rok	k1gInSc6
zemřel	zemřít	k5eAaPmAgInS
na	na	k7c4
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
korunním	korunní	k2eAgMnSc7d1
princem	princ	k1gMnSc7
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
ukončila	ukončit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc4
námořní	námořní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
a	a	k8xC
očekávalo	očekávat	k5eAaImAgNnS
se	se	k3xPyFc4
od	od	k7c2
něj	on	k3xPp3gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
více	hodně	k6eAd2
zapojí	zapojit	k5eAaPmIp3nS
do	do	k7c2
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
stále	stále	k6eAd1
považovala	považovat	k5eAaImAgFnS
princeznu	princezna	k1gFnSc4
Marii	Maria	k1gFnSc3
za	za	k7c4
vhodnou	vhodný	k2eAgFnSc4d1
kandidátku	kandidátka	k1gFnSc4
na	na	k7c4
sňatek	sňatek	k1gInSc4
s	s	k7c7
budoucím	budoucí	k2eAgMnSc7d1
králem	král	k1gMnSc7
a	a	k8xC
přesvědčila	přesvědčit	k5eAaPmAgFnS
Jiřího	Jiří	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Marii	Maria	k1gFnSc4
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
ruku	ruka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
sňatek	sňatek	k1gInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1893	#num#	k4
v	v	k7c6
královské	královský	k2eAgFnSc6d1
kapli	kaple	k1gFnSc6
St	St	kA
James	James	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Palace	Palace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
manželství	manželství	k1gNnSc1
bylo	být	k5eAaImAgNnS
úspěšné	úspěšný	k2eAgNnSc1d1
a	a	k8xC
oba	dva	k4xCgMnPc1
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
respektovali	respektovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Dvojice	dvojice	k1gFnSc1
žila	žít	k5eAaImAgFnS
v	v	k7c6
malém	malý	k2eAgInSc6d1
domě	dům	k1gInSc6
v	v	k7c6
Norfolku	Norfolek	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInSc1
styl	styl	k1gInSc1
života	život	k1gInSc2
podobal	podobat	k5eAaImAgInS
více	hodně	k6eAd2
životu	život	k1gInSc3
příslušníků	příslušník	k1gMnPc2
střední	střední	k2eAgFnSc2d1
společenské	společenský	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
životu	život	k1gInSc2
členů	člen	k1gMnPc2
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
upřednostňoval	upřednostňovat	k5eAaImAgMnS
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
rodičů	rodič	k1gMnPc2
<g/>
,	,	kIx,
jednoduchý	jednoduchý	k2eAgInSc4d1
a	a	k8xC
klidný	klidný	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
známý	známý	k2eAgMnSc1d1
filatelista	filatelista	k1gMnSc1
a	a	k8xC
sehrál	sehrát	k5eAaPmAgMnS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
vybudování	vybudování	k1gNnSc6
královské	královský	k2eAgFnSc2d1
filatelistické	filatelistický	k2eAgFnSc2d1
sbírky	sbírka	k1gFnSc2
do	do	k7c2
úrovně	úroveň	k1gFnSc2
nejrozsáhlejší	rozsáhlý	k2eAgFnSc2d3
sbírky	sbírka	k1gFnSc2
známek	známka	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
i	i	k9
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
plnili	plnit	k5eAaImAgMnP
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
povinností	povinnost	k1gFnPc2
členů	člen	k1gInPc2
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
se	se	k3xPyFc4
Jiřího	Jiří	k1gMnSc2
otec	otec	k1gMnSc1
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1901	#num#	k4
Jiří	Jiří	k1gMnSc1
s	s	k7c7
Marií	Maria	k1gFnSc7
procestovali	procestovat	k5eAaPmAgMnP
několik	několik	k4yIc4
zemí	zem	k1gFnPc2
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštívili	navštívit	k5eAaPmAgMnP
Austrálii	Austrálie	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zahájil	zahájit	k5eAaPmAgMnS
první	první	k4xOgNnSc4
jednání	jednání	k1gNnSc4
australského	australský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
zastávkami	zastávka	k1gFnPc7
byly	být	k5eAaImAgFnP
země	zem	k1gFnPc1
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
a	a	k8xC
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1901	#num#	k4
byl	být	k5eAaImAgMnS
Jiřímu	Jiří	k1gMnSc3
udělen	udělen	k2eAgInSc4d1
titul	titul	k1gInSc4
prince	princ	k1gMnSc2
z	z	k7c2
Walesu	Wales	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
přístupu	přístup	k1gInSc2
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
k	k	k7c3
němu	on	k3xPp3gNnSc3
samotnému	samotný	k2eAgNnSc3d1
<g/>
,	,	kIx,
si	se	k3xPyFc3
přál	přát	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
připravil	připravit	k5eAaPmAgInS
na	na	k7c4
budoucí	budoucí	k2eAgFnSc4d1
roli	role	k1gFnSc4
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
tak	tak	k9
měl	mít	k5eAaImAgMnS
přístup	přístup	k1gInSc4
ke	k	k7c3
státním	státní	k2eAgMnPc3d1
dokumentům	dokument	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1910	#num#	k4
král	král	k1gMnSc1
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
Jiří	Jiří	k1gMnSc1
po	po	k7c6
něm	on	k3xPp3gInSc6
nastoupil	nastoupit	k5eAaPmAgInS
trůn	trůn	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunovace	korunovace	k1gFnSc1
královského	královský	k2eAgInSc2d1
páru	pár	k1gInSc2
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1911	#num#	k4
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oslavy	oslava	k1gFnPc4
korunovace	korunovace	k1gFnSc2
byly	být	k5eAaImAgFnP
pojaty	pojmout	k5eAaPmNgFnP
jako	jako	k8xS,k8xC
oslava	oslava	k1gFnSc1
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
oba	dva	k4xCgMnPc1
navštívili	navštívit	k5eAaPmAgMnP
indické	indický	k2eAgNnSc4d1
Dillí	Dillí	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jako	jako	k9
indický	indický	k2eAgMnSc1d1
císař	císař	k1gMnSc1
a	a	k8xC
císařovna	císařovna	k1gFnSc1
účastnili	účastnit	k5eAaImAgMnP
slavnostního	slavnostní	k2eAgNnSc2d1
zasedání	zasedání	k1gNnSc2
indických	indický	k2eAgMnPc2d1
hodnostářů	hodnostář	k1gMnPc2
a	a	k8xC
princů	princ	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
té	ten	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
měl	mít	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
na	na	k7c6
hlavě	hlava	k1gFnSc6
nasazenu	nasazen	k2eAgFnSc4d1
indickou	indický	k2eAgFnSc4d1
císařskou	císařský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
speciálně	speciálně	k6eAd1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
příležitost	příležitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
oba	dva	k4xCgMnPc1
cestovali	cestovat	k5eAaImAgMnP
po	po	k7c6
Indii	Indie	k1gFnSc6
a	a	k8xC
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
jako	jako	k8xS,k8xC
nadšený	nadšený	k2eAgMnSc1d1
lovec	lovec	k1gMnSc1
využil	využít	k5eAaPmAgMnS
příležitosti	příležitost	k1gFnSc2
k	k	k7c3
lovu	lov	k1gInSc3
tygrů	tygr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Mikuláš	Mikuláš	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1914	#num#	k4
až	až	k9
1918	#num#	k4
byla	být	k5eAaImAgFnS
Británie	Británie	k1gFnSc1
ve	v	k7c6
válečném	válečný	k2eAgInSc6d1
stavu	stav	k1gInSc6
s	s	k7c7
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
pro	pro	k7c4
Brity	Brit	k1gMnPc4
symbolizoval	symbolizovat	k5eAaImAgMnS
válečné	válečný	k2eAgFnPc4d1
hrůzy	hrůza	k1gFnPc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Jiřího	Jiří	k1gMnSc2
prvním	první	k4xOgMnSc7
bratrancem	bratranec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
byla	být	k5eAaImAgFnS
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
britského	britský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dcerou	dcera	k1gFnSc7
vévody	vévoda	k1gMnSc2
z	z	k7c2
Tecku	Teck	k1gInSc2
<g/>
,	,	kIx,
následníka	následník	k1gMnSc2
německého	německý	k2eAgInSc2d1
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Württemberků	Württemberk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1917	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
nařízení	nařízení	k1gNnSc2
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
změnil	změnit	k5eAaPmAgInS
jméno	jméno	k1gNnSc4
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
z	z	k7c2
německy	německy	k6eAd1
znějícího	znějící	k2eAgInSc2d1
Sachsen-Coburg	Sachsen-Coburg	k1gMnSc1
und	und	k?
Gotha	Gotha	k1gFnSc1
na	na	k7c4
Windsor	Windsor	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
podpořil	podpořit	k5eAaPmAgMnS
britské	britský	k2eAgNnSc4d1
vlastenectví	vlastenectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
on	on	k3xPp3gMnSc1
a	a	k8xC
členové	člen	k1gMnPc1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
britskými	britský	k2eAgMnPc7d1
občany	občan	k1gMnPc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
zřekli	zřeknout	k5eAaPmAgMnP
používání	používání	k1gNnSc3
německých	německý	k2eAgMnPc2d1
titulů	titul	k1gInPc2
a	a	k8xC
převzali	převzít	k5eAaPmAgMnP
britská	britský	k2eAgNnPc4d1
příjmení	příjmení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
prosadil	prosadit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
příbuzní	příbuzný	k1gMnPc1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
bojující	bojující	k2eAgFnSc2d1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
zbaveni	zbavit	k5eAaPmNgMnP
britských	britský	k2eAgInPc2d1
šlechtických	šlechtický	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
bratranec	bratranec	k1gMnSc1
Mikuláš	Mikuláš	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
s	s	k7c7
nímž	jenž	k3xRgMnSc7
Jiřího	Jiří	k1gMnSc2
pojila	pojit	k5eAaImAgFnS
i	i	k8xC
nápadná	nápadný	k2eAgFnSc1d1
fyzická	fyzický	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
<g/>
)	)	kIx)
svržen	svrhnout	k5eAaPmNgMnS
Říjnovou	říjnový	k2eAgFnSc7d1
revolucí	revoluce	k1gFnSc7
<g/>
,	,	kIx,
zvažovala	zvažovat	k5eAaImAgFnS
britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
poskytnout	poskytnout	k5eAaPmF
jemu	on	k3xPp3gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
rodině	rodina	k1gFnSc3
azyl	azyl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhoršené	zhoršený	k2eAgFnPc4d1
životní	životní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
britského	britský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
a	a	k8xC
obavy	obava	k1gFnPc1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
revoluce	revoluce	k1gFnSc1
zavlečena	zavlečen	k2eAgFnSc1d1
i	i	k9
do	do	k7c2
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
však	však	k9
vedla	vést	k5eAaImAgFnS
krále	král	k1gMnSc4
Jiřího	Jiří	k1gMnSc4
k	k	k7c3
přesvědčení	přesvědčení	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
přítomnost	přítomnost	k1gFnSc1
Romanovců	Romanovec	k1gMnPc2
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
shledána	shledat	k5eAaPmNgFnS
nepříhodnou	příhodný	k2eNgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britská	britský	k2eAgFnSc1d1
tajná	tajný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
vypracovala	vypracovat	k5eAaPmAgFnS
plán	plán	k1gInSc4
na	na	k7c4
záchranu	záchrana	k1gFnSc4
carské	carský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
sílící	sílící	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
bolševiků	bolševik	k1gMnPc2
a	a	k8xC
zhoršující	zhoršující	k2eAgMnSc1d1
se	se	k3xPyFc4
podmínky	podmínka	k1gFnSc2
války	válka	k1gFnSc2
způsobily	způsobit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc1
plány	plán	k1gInPc1
nebyly	být	k5eNaImAgInP
realizovány	realizován	k2eAgInPc4d1
<g/>
,	,	kIx,
car	car	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
zajat	zajat	k2eAgInSc1d1
bolševiky	bolševik	k1gMnPc7
a	a	k8xC
s	s	k7c7
celou	celý	k2eAgFnSc7d1
nejbližší	blízký	k2eAgFnSc7d3
rodinou	rodina	k1gFnSc7
jimi	on	k3xPp3gMnPc7
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1918	#num#	k4
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
byla	být	k5eAaImAgFnS
carova	carův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
Marie	Marie	k1gFnSc1
Fjodorovna	Fjodorovna	k1gFnSc1
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
širší	široký	k2eAgFnSc2d2
carské	carský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
zachránění	zachránění	k1gNnSc2
z	z	k7c2
Krymu	Krym	k1gInSc2
britskou	britský	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Jiří	Jiří	k1gMnSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
vnukové	vnuk	k1gMnPc1
Eduard	Eduard	k1gMnSc1
a	a	k8xC
Jiří	Jiří	k1gMnSc1
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
po	po	k7c6
ní	on	k3xPp3gFnSc6
byly	být	k5eAaImAgFnP
svrženy	svržen	k2eAgFnPc1d1
mnohé	mnohý	k2eAgFnPc1d1
evropské	evropský	k2eAgFnPc1d1
monarchie	monarchie	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc4
příslušníci	příslušník	k1gMnPc1
byli	být	k5eAaImAgMnP
Jiřího	Jiří	k1gMnSc4
příbuznými	příbuzný	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
politické	politický	k2eAgInPc4d1
nepokoje	nepokoj	k1gInPc4
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
generální	generální	k2eAgFnSc2d1
stávky	stávka	k1gFnSc2
roku	rok	k1gInSc2
1926	#num#	k4
vyjádřil	vyjádřit	k5eAaPmAgInS
znepokojení	znepokojení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
stávka	stávka	k1gFnSc1
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
revolucionáři	revolucionář	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
varoval	varovat	k5eAaImAgMnS
vládu	vláda	k1gFnSc4
před	před	k7c7
použitím	použití	k1gNnSc7
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
poznamenala	poznamenat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnSc4
zdraví	zdraví	k1gNnSc4
a	a	k8xC
kouření	kouřený	k2eAgMnPc1d1
tento	tento	k3xDgInSc4
stav	stav	k1gInSc4
dále	daleko	k6eAd2
zhoršovalo	zhoršovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouho	dlouho	k6eAd1
trpěl	trpět	k5eAaImAgInS
dýchacími	dýchací	k2eAgInPc7d1
problémy	problém	k1gInPc7
a	a	k8xC
záněty	zánět	k1gInPc7
průdušek	průduška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1928	#num#	k4
vážně	vážně	k6eAd1
onemocněl	onemocnět	k5eAaPmAgInS
a	a	k8xC
po	po	k7c4
následující	následující	k2eAgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
převzal	převzít	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
státní	státní	k2eAgInSc1d1
povinnosti	povinnost	k1gFnSc2
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiřího	Jiří	k1gMnSc2
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
sice	sice	k8xC
zlepšil	zlepšit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
úplně	úplně	k6eAd1
se	se	k3xPyFc4
už	už	k6eAd1
nezotavil	zotavit	k5eNaPmAgInS
nikdy	nikdy	k6eAd1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1936	#num#	k4
v	v	k7c4
Sandringham	Sandringham	k1gInSc4
House	house	k1gNnSc4
zemřel	zemřít	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1932	#num#	k4
souhlasil	souhlasit	k5eAaImAgInS
s	s	k7c7
pronesením	pronesení	k1gNnSc7
vánočního	vánoční	k2eAgInSc2d1
projevu	projev	k1gInSc2
v	v	k7c6
rozhlasu	rozhlas	k1gInSc2
<g/>
;	;	kIx,
tato	tento	k3xDgFnSc1
akce	akce	k1gFnSc1
se	se	k3xPyFc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
opakuje	opakovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyl	být	k5eNaImAgMnS
příznivcem	příznivec	k1gMnSc7
této	tento	k3xDgFnSc2
novoty	novota	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
svým	svůj	k3xOyFgNnSc7
okolím	okolí	k1gNnSc7
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
si	se	k3xPyFc3
občané	občan	k1gMnPc1
přejí	přát	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obával	obávat	k5eAaImAgMnS
se	se	k3xPyFc4
nárůstu	nárůst	k1gInSc3
vlivu	vliv	k1gInSc2
nacistické	nacistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
vyzval	vyzvat	k5eAaPmAgMnS
britského	britský	k2eAgMnSc4d1
velvyslance	velvyslanec	k1gMnSc4
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
činnost	činnost	k1gFnSc1
nacistů	nacista	k1gMnPc2
sledoval	sledovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oslavy	oslava	k1gFnPc4
stříbrného	stříbrný	k1gInSc2
výročí	výročí	k1gNnSc3
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
byly	být	k5eAaImAgInP
demonstrací	demonstrace	k1gFnSc7
náklonnosti	náklonnost	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
osobě	osoba	k1gFnSc3
<g/>
,	,	kIx,
k	k	k7c3
oblíbenému	oblíbený	k2eAgMnSc3d1
králi	král	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1894	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
král	král	k1gMnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
indický	indický	k2eAgMnSc1d1
<g/>
;	;	kIx,
po	po	k7c6
své	svůj	k3xOyFgFnSc6
abdikaci	abdikace	k1gFnSc6
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Windsoru	Windsor	k1gInSc2
∞	∞	k?
Wallis	Wallis	k1gInSc1
Simpson	Simpson	k1gInSc1
</s>
<s>
Albert	Albert	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1895	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
král	král	k1gMnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
a	a	k8xC
poslední	poslední	k2eAgMnSc1d1
císař	císař	k1gMnSc1
indický	indický	k2eAgMnSc1d1
<g/>
,	,	kIx,
∞	∞	k?
lady	lad	k1gInPc1
Elizabeth	Elizabeth	k1gFnSc7
Bowes-Lyon	Bowes-Lyon	k1gNnSc4
</s>
<s>
Mary	Mary	k1gFnSc1
<g/>
,	,	kIx,
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Harewoodu	Harewood	k1gInSc2
<g/>
,	,	kIx,
Princess	Princess	k1gInSc1
Royal	Royal	k1gInSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1897	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1965	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
hrabě	hrabě	k1gMnSc1
Henry	Henry	k1gMnSc1
Lascelles	Lascelles	k1gMnSc1
<g/>
,	,	kIx,
Earl	earl	k1gMnSc1
of	of	k?
Harewood	Harewood	k1gInSc1
</s>
<s>
Henry	Henry	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Gloucesteru	Gloucester	k1gInSc2
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1900	#num#	k4
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1974	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
lady	lad	k1gInPc1
Alice	Alice	k1gFnSc1
Montagu-Douglas-Scott	Montagu-Douglas-Scott	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
George	Georg	k1gMnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1902	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1942	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
princezna	princezna	k1gFnSc1
Marina	Marina	k1gFnSc1
Řecká	řecký	k2eAgFnSc1d1
a	a	k8xC
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
John	John	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1905	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
epileptický	epileptický	k2eAgInSc4d1
záchvat	záchvat	k1gInSc4
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Tituly	titul	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
Jiřího	Jiří	k1gMnSc2
V.	V.	kA
<g/>
.	.	kIx.
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Ernest	Ernest	k1gMnSc1
I.	I.	kA
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgInSc5d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
princ	princ	k1gMnSc1
Albert	Albert	k1gMnSc1
</s>
<s>
Augustus	Augustus	k1gInSc1
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Zvěřínská	Meklenbursko-Zvěřínský	k2eAgFnSc1d1
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Šlesvicko-Holštýnsko-Sonderburský-Beck	Šlesvicko-Holštýnsko-Sonderburský-Beck	k1gMnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Šlesvicko-Holštýnsko-Sonderbursko-Glücksburský	Šlesvicko-Holštýnsko-Sonderbursko-Glücksburský	k2eAgMnSc1d1
</s>
<s>
Frederika	Frederika	k1gFnSc1
Schliebenská	Schliebenský	k2eAgFnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Karolina	Karolinum	k1gNnSc2
Hesensko-Kasselská	hesensko-kasselský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
a	a	k8xC
Norská	norský	k2eAgFnSc1d1
</s>
<s>
Alexandra	Alexandra	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Nasavsko-Usingenská	Nasavsko-Usingenský	k2eAgNnPc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Hesensko-Kasselská	hesensko-kasselský	k2eAgFnSc1d1
</s>
<s>
Frederik	Frederik	k1gMnSc1
Dánský	dánský	k2eAgMnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Meklenbursko-Zvěřínská	Meklenbursko-Zvěřínský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
George	Georg	k1gMnSc2
V	v	k7c6
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jiří	Jiří	k1gMnSc1
V.	V.	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://thepeerage.com/p10067.htm#i100661	http://thepeerage.com/p10067.htm#i100661	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
82172	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118690469	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
8336	#num#	k4
5509	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80139232	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
268389473	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80139232	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
