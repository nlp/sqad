<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
britským	britský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
z	z	k7c2
windsorské	windsorský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
vytvořil	vytvořit	k5eAaPmAgMnS
přejmenováním	přejmenování	k1gNnSc7
britské	britský	k2eAgFnSc2d1
větve	větev	k1gFnSc2
dynastie	dynastie	k1gFnSc2
Sachsen-Coburg	Sachsen-Coburg	k1gMnSc1
und	und	k?
Gotha	Gotha	k1gFnSc1
<g/>
.	.	kIx.
</s>