<p>
<s>
Kořenová	kořenový	k2eAgFnSc1d1	kořenová
zelenina	zelenina	k1gFnSc1	zelenina
je	být	k5eAaImIp3nS	být
zelenina	zelenina	k1gFnSc1	zelenina
(	(	kIx(	(
<g/>
v	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
i	i	k8xC	i
skupina	skupina	k1gFnSc1	skupina
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
podzemní	podzemní	k2eAgFnSc2d1	podzemní
části	část	k1gFnSc2	část
–	–	k?	–
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
hlízy	hlíza	k1gFnPc4	hlíza
nebo	nebo	k8xC	nebo
bulvy	bulva	k1gFnPc4	bulva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
využívané	využívaný	k2eAgFnPc1d1	využívaná
jako	jako	k9	jako
potrava	potrava	k1gFnSc1	potrava
či	či	k8xC	či
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
a	a	k8xC	a
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
kořenovou	kořenový	k2eAgFnSc7d1	kořenová
zeleninou	zelenina	k1gFnSc7	zelenina
je	být	k5eAaImIp3nS	být
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
petržel	petržel	k1gFnSc1	petržel
kořenová	kořenový	k2eAgFnSc1d1	kořenová
<g/>
,	,	kIx,	,
křen	křen	k1gInSc1	křen
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kořenová	kořenový	k2eAgFnSc1d1	kořenová
zelenina	zelenina	k1gFnSc1	zelenina
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgFnSc4d1	podzemní
část	část	k1gFnSc4	část
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Kořenová	kořenový	k2eAgFnSc1d1	kořenová
zelenina	zelenina	k1gFnSc1	zelenina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgFnPc4d3	nejrozšířenější
zeleniny	zelenina	k1gFnPc4	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
kořenům	kořen	k1gInPc3	kořen
<g/>
,	,	kIx,	,
kořenovým	kořenový	k2eAgFnPc3d1	kořenová
hlízám	hlíza	k1gFnPc3	hlíza
a	a	k8xC	a
bulvám	bulva	k1gFnPc3	bulva
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
petržel	petržel	k1gFnSc1	petržel
<g/>
,	,	kIx,	,
celer	celer	k1gInSc1	celer
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
také	také	k9	také
listy	list	k1gInPc1	list
(	(	kIx(	(
<g/>
nať	nať	k1gFnSc1	nať
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
dietetickou	dietetický	k2eAgFnSc4d1	dietetická
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pěstovaným	pěstovaný	k2eAgInPc3d1	pěstovaný
druhům	druh	k1gInPc3	druh
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořenová	kořenový	k2eAgFnSc1d1	kořenová
zelenina	zelenina	k1gFnSc1	zelenina
je	být	k5eAaImIp3nS	být
bohatým	bohatý	k2eAgInSc7d1	bohatý
zdrojem	zdroj	k1gInSc7	zdroj
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
,	,	kIx,	,
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
,	,	kIx,	,
éterických	éterický	k2eAgInPc2d1	éterický
olejů	olej	k1gInPc2	olej
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc2	člověk
důležitých	důležitý	k2eAgFnPc2d1	důležitá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
výživu	výživa	k1gFnSc4	výživa
nepostradatelná	postradatelný	k2eNgFnSc1d1	nepostradatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
významné	významný	k2eAgFnSc2d1	významná
kořenové	kořenový	k2eAgFnSc2d1	kořenová
zeleniny	zelenina	k1gFnSc2	zelenina
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Listová	listový	k2eAgFnSc1d1	listová
zelenina	zelenina	k1gFnSc1	zelenina
</s>
</p>
<p>
<s>
Plodová	plodový	k2eAgFnSc1d1	plodová
zelenina	zelenina	k1gFnSc1	zelenina
</s>
</p>
<p>
<s>
Košťálová	košťálový	k2eAgFnSc1d1	košťálová
zelenina	zelenina	k1gFnSc1	zelenina
</s>
</p>
<p>
<s>
Cibulová	cibulový	k2eAgFnSc1d1	cibulová
zelenina	zelenina	k1gFnSc1	zelenina
</s>
</p>
