<p>
<s>
Pero	pero	k1gNnSc1	pero
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
psací	psací	k2eAgFnSc2d1	psací
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ptačí	ptačí	k2eAgInSc1d1	ptačí
brk	brk	k1gInSc1	brk
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
až	až	k6eAd1	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
novověku	novověk	k1gInSc2	novověk
byl	být	k5eAaImAgMnS	být
seřezáván	seřezáván	k2eAgMnSc1d1	seřezáván
do	do	k7c2	do
širokého	široký	k2eAgInSc2d1	široký
hrotu	hrot	k1gInSc2	hrot
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
psát	psát	k5eAaImF	psát
pomocí	pomocí	k7c2	pomocí
silných	silný	k2eAgInPc2d1	silný
a	a	k8xC	a
vlasových	vlasový	k2eAgInPc2d1	vlasový
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
seřezávat	seřezávat	k5eAaImF	seřezávat
šikmo	šikmo	k6eAd1	šikmo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stopa	stopa	k1gFnSc1	stopa
byla	být	k5eAaImAgFnS	být
vlasová	vlasový	k2eAgFnSc1d1	vlasová
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
psaní	psaní	k1gNnSc1	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Stínování	stínování	k1gNnSc1	stínování
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
provádělo	provádět	k5eAaImAgNnS	provádět
přítlakem	přítlak	k1gInSc7	přítlak
pera	pero	k1gNnSc2	pero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někde	někde	k6eAd1	někde
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
pero	pero	k1gNnSc1	pero
rákosové	rákosový	k2eAgNnSc1d1	rákosové
<g/>
.	.	kIx.	.
</s>
<s>
Rákos	rákos	k1gInSc1	rákos
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Calamus	Calamus	k1gInSc1	Calamus
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
tak	tak	k9	tak
jméno	jméno	k1gNnSc4	jméno
kalamáři	kalamář	k1gInSc3	kalamář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
využívat	využívat	k5eAaPmF	využívat
ocelové	ocelový	k2eAgNnSc1d1	ocelové
pero	pero	k1gNnSc1	pero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
ocelového	ocelový	k2eAgNnSc2d1	ocelové
perka	perko	k1gNnSc2	perko
(	(	kIx(	(
<g/>
špičky	špička	k1gFnSc2	špička
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
násadky	násadka	k1gFnSc2	násadka
(	(	kIx(	(
<g/>
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Perku	perko	k1gNnSc3	perko
s	s	k7c7	s
násadkou	násadka	k1gFnSc7	násadka
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
pero	pero	k1gNnSc1	pero
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Údaj	údaj	k1gInSc1	údaj
ze	z	k7c2	z
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
učitele	učitel	k1gMnSc2	učitel
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
počal	počnout	k5eAaPmAgMnS	počnout
choditi	chodit	k5eAaImF	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
se	se	k3xPyFc4	se
brkovým	brkový	k2eAgNnPc3d1	brkový
perům	pero	k1gNnPc3	pero
odzvánělo	odzvánět	k5eAaImAgNnS	odzvánět
a	a	k8xC	a
počalo	počnout	k5eAaPmAgNnS	počnout
se	se	k3xPyFc4	se
psát	psát	k5eAaImF	psát
ocelovými	ocelový	k2eAgFnPc7d1	ocelová
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
ocelová	ocelový	k2eAgFnSc1d1	ocelová
perka	perko	k1gNnPc1	perko
vyráběna	vyrábět	k5eAaImNgNnP	vyrábět
asi	asi	k9	asi
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
let	léto	k1gNnPc2	léto
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ocelová	ocelový	k2eAgNnPc4d1	ocelové
perka	perko	k1gNnPc4	perko
byla	být	k5eAaImAgFnS	být
levná	levný	k2eAgFnSc1d1	levná
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
jich	on	k3xPp3gFnPc2	on
měl	mít	k5eAaImAgMnS	mít
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
mohl	moct	k5eAaImAgInS	moct
při	při	k7c6	při
rozskřípání	rozskřípání	k1gNnSc6	rozskřípání
ihned	ihned	k6eAd1	ihned
vyměnit	vyměnit	k5eAaPmF	vyměnit
<g/>
.	.	kIx.	.
</s>
<s>
Nasazovala	nasazovat	k5eAaImAgFnS	nasazovat
se	se	k3xPyFc4	se
do	do	k7c2	do
násadky	násadka	k1gFnSc2	násadka
<g/>
,	,	kIx,	,
namáčela	namáčet	k5eAaImAgFnS	namáčet
se	se	k3xPyFc4	se
do	do	k7c2	do
inkoustu	inkoust	k1gInSc2	inkoust
v	v	k7c6	v
kalamáři	kalamář	k1gInSc6	kalamář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
každým	každý	k3xTgMnSc7	každý
žákem	žák	k1gMnSc7	žák
zasazen	zasazen	k2eAgMnSc1d1	zasazen
v	v	k7c6	v
otvoru	otvor	k1gInSc6	otvor
v	v	k7c6	v
lavici	lavice	k1gFnSc6	lavice
<g/>
.	.	kIx.	.
</s>
<s>
Zámožnější	zámožný	k2eAgMnPc1d2	zámožnější
žáci	žák	k1gMnPc1	žák
měli	mít	k5eAaImAgMnP	mít
pera	pero	k1gNnSc2	pero
plnicí	plnicí	k2eAgMnPc1d1	plnicí
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zlevňovala	zlevňovat	k5eAaImAgFnS	zlevňovat
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	let	k1gInSc1	let
padesátých	padesátý	k4xOgNnPc2	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
v	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
propisovací	propisovací	k2eAgFnPc1d1	propisovací
tužky	tužka	k1gFnPc1	tužka
<g/>
,	,	kIx,	,
ocelová	ocelový	k2eAgNnPc1d1	ocelové
pera	pero	k1gNnPc1	pero
v	v	k7c6	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
postupně	postupně	k6eAd1	postupně
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
ale	ale	k9	ale
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
kaligrafické	kaligrafický	k2eAgInPc4d1	kaligrafický
účely	účel	k1gInPc4	účel
používají	používat	k5eAaImIp3nP	používat
redispera	redispero	k1gNnSc2	redispero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plnicí	plnicí	k2eAgNnPc1d1	plnicí
pera	pero	k1gNnPc1	pero
začala	začít	k5eAaPmAgNnP	začít
být	být	k5eAaImF	být
hojně	hojně	k6eAd1	hojně
využívána	využívat	k5eAaPmNgFnS	využívat
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc1	jeho
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktérem	konstruktér	k1gMnSc7	konstruktér
moderního	moderní	k2eAgNnSc2d1	moderní
pera	pero	k1gNnSc2	pero
byl	být	k5eAaImAgMnS	být
Lewis	Lewis	k1gFnSc4	Lewis
Edson	Edson	k1gMnSc1	Edson
Waterman	Waterman	k1gMnSc1	Waterman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
nejhojněji	hojně	k6eAd3	hojně
užívaným	užívaný	k2eAgInSc7d1	užívaný
typem	typ	k1gInSc7	typ
pera	pero	k1gNnSc2	pero
je	být	k5eAaImIp3nS	být
kuličkové	kuličkový	k2eAgNnSc1d1	kuličkové
pero	pero	k1gNnSc1	pero
(	(	kIx(	(
<g/>
propiska	propiska	k1gFnSc1	propiska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
širokého	široký	k2eAgNnSc2d1	široké
rozšíření	rozšíření	k1gNnSc2	rozšíření
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
technických	technický	k2eAgInPc2d1	technický
problémů	problém	k1gInPc2	problém
(	(	kIx(	(
<g/>
zadrhávání	zadrhávání	k1gNnSc1	zadrhávání
kuličky	kulička	k1gFnPc1	kulička
<g/>
,	,	kIx,	,
samovolné	samovolný	k2eAgNnSc1d1	samovolné
vytékání	vytékání	k1gNnSc1	vytékání
psací	psací	k2eAgFnSc2d1	psací
pasty	pasta	k1gFnSc2	pasta
<g/>
)	)	kIx)	)
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
novověké	novověký	k2eAgFnSc2d1	novověká
latinské	latinský	k2eAgFnSc2d1	Latinská
paleografie	paleografie	k1gFnSc2	paleografie
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zřetelem	zřetel	k1gInSc7	zřetel
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
s.	s.	k?	s.
30	[number]	k4	30
<g/>
n.	n.	k?	n.
</s>
</p>
<p>
<s>
HERZOG	HERZOG	kA	HERZOG
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
<g/>
Mé	můj	k3xOp1gFnSc2	můj
paměti	paměť	k1gFnSc2	paměť
-vzpomínky	zpomínka	k1gFnSc2	-vzpomínka
učitele-	učitele-	k?	učitele-
Brno	Brno	k1gNnSc1	Brno
1938	[number]	k4	1938
nákladem	náklad	k1gInSc7	náklad
vlastním	vlastní	k2eAgInSc7d1	vlastní
<g/>
,	,	kIx,	,
Typia	Typia	k1gFnSc1	Typia
Brno	Brno	k1gNnSc1	Brno
str	str	kA	str
<g/>
.30	.30	k4	.30
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Protokolární	protokolární	k2eAgNnSc1d1	protokolární
pero	pero	k1gNnSc1	pero
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pero	pero	k1gNnSc4	pero
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Péro	péro	k1gNnSc4	péro
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pero	pero	k1gNnSc4	pero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
