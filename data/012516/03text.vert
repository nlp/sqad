<p>
<s>
Tango	tango	k1gNnSc1	tango
je	být	k5eAaImIp3nS	být
tanec	tanec	k1gInSc1	tanec
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
Uruguaye	Uruguay	k1gFnSc2	Uruguay
a	a	k8xC	a
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
jeho	jeho	k3xOp3gInPc1	jeho
další	další	k2eAgInPc1d1	další
styly	styl	k1gInPc1	styl
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
ve	v	k7c6	v
sportovním	sportovní	k2eAgInSc6d1	sportovní
tanci	tanec	k1gInSc6	tanec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
standardní	standardní	k2eAgInPc4d1	standardní
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
původu	původ	k1gInSc3	původ
a	a	k8xC	a
povaze	povaha	k1gFnSc3	povaha
tance	tanec	k1gInSc2	tanec
bylo	být	k5eAaImAgNnS	být
tango	tango	k1gNnSc1	tango
dříve	dříve	k6eAd2	dříve
řazeno	řadit	k5eAaImNgNnS	řadit
mezi	mezi	k7c4	mezi
latinskoamerické	latinskoamerický	k2eAgInPc4d1	latinskoamerický
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tango	tango	k1gNnSc1	tango
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjednodušeno	zjednodušen	k2eAgNnSc1d1	zjednodušeno
a	a	k8xC	a
přizpůsobeno	přizpůsoben	k2eAgNnSc1d1	přizpůsobeno
potřebám	potřeba	k1gFnPc3	potřeba
tradičního	tradiční	k2eAgInSc2d1	tradiční
společenského	společenský	k2eAgInSc2d1	společenský
tance	tanec	k1gInSc2	tanec
a	a	k8xC	a
začleněno	začleněn	k2eAgNnSc1d1	začleněno
do	do	k7c2	do
repertoáru	repertoár	k1gInSc2	repertoár
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
soutěží	soutěž	k1gFnPc2	soutěž
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
tanci	tanec	k1gInSc6	tanec
–	–	k?	–
tanečním	taneční	k2eAgInPc3d1	taneční
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžní	soutěžní	k2eAgNnSc1d1	soutěžní
tango	tango	k1gNnSc1	tango
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
evropského	evropský	k2eAgInSc2d1	evropský
stylu	styl	k1gInSc2	styl
–	–	k?	–
anglického	anglický	k2eAgNnSc2d1	anglické
tanga	tango	k1gNnSc2	tango
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Styly	styl	k1gInPc1	styl
tanga	tango	k1gNnSc2	tango
a	a	k8xC	a
tangové	tangový	k2eAgFnSc2d1	Tangová
hudby	hudba	k1gFnSc2	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Styly	styl	k1gInPc1	styl
tanga	tango	k1gNnSc2	tango
===	===	k?	===
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Argentino	Argentina	k1gFnSc5	Argentina
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Oriental	Oriental	k1gMnSc1	Oriental
(	(	kIx(	(
<g/>
uruguayo	uruguayo	k1gMnSc1	uruguayo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Canyengue	Canyengu	k1gFnSc2	Canyengu
(	(	kIx(	(
<g/>
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
jako	jako	k9	jako
příměr	příměr	k1gInSc4	příměr
dosti	dosti	k6eAd1	dosti
dobře	dobře	k6eAd1	dobře
použít	použít	k5eAaPmF	použít
tanec	tanec	k1gInSc4	tanec
Pražské	pražský	k2eAgNnSc1d1	Pražské
podskalí	podskalí	k1gNnSc1	podskalí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Liso	Lisa	k1gFnSc5	Lisa
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Salon	salon	k1gInSc1	salon
(	(	kIx(	(
<g/>
poměrně	poměrně	k6eAd1	poměrně
elegantní	elegantní	k2eAgInSc1d1	elegantní
taneční	taneční	k2eAgInSc1d1	taneční
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
partneři	partner	k1gMnPc1	partner
nesdílejí	sdílet	k5eNaImIp3nP	sdílet
společné	společný	k2eAgNnSc4d1	společné
centrum	centrum	k1gNnSc4	centrum
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Orillero	Orillero	k1gNnSc1	Orillero
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
stylu	styl	k1gInSc2	styl
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
okrajových	okrajový	k2eAgFnPc2d1	okrajová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Capital	Capital	k1gMnSc1	Capital
Federal	Federal	k1gMnSc1	Federal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Milonguero	Milonguero	k1gNnSc1	Milonguero
(	(	kIx(	(
<g/>
Tango	tango	k1gNnSc1	tango
Apilado	Apilada	k1gFnSc5	Apilada
<g/>
;	;	kIx,	;
silueta	silueta	k1gFnSc1	silueta
tanečníků	tanečník	k1gMnPc2	tanečník
v	v	k7c6	v
páru	pár	k1gInSc6	pár
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Nuevo	Nuevo	k1gNnSc1	Nuevo
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
spadá	spadat	k5eAaImIp3nS	spadat
většina	většina	k1gFnSc1	většina
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neotango	Neotango	k1gNnSc1	Neotango
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgInSc1d1	moderní
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
tančený	tančený	k2eAgInSc1d1	tančený
většinou	většinou	k6eAd1	většinou
na	na	k7c4	na
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
–	–	k?	–
nejen	nejen	k6eAd1	nejen
frontální	frontální	k2eAgFnSc2d1	frontální
–	–	k?	–
tanečního	taneční	k2eAgNnSc2d1	taneční
držení	držení	k1gNnSc2	držení
a	a	k8xC	a
taneční	taneční	k2eAgFnSc2d1	taneční
figury	figura	k1gFnSc2	figura
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
tanečníků	tanečník	k1gMnPc2	tanečník
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
s	s	k7c7	s
těžištěm	těžiště	k1gNnSc7	těžiště
mimo	mimo	k7c4	mimo
stabilní	stabilní	k2eAgFnSc4d1	stabilní
polohu	poloha	k1gFnSc4	poloha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Show	show	k1gNnSc1	show
Tango	tango	k1gNnSc1	tango
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
jako	jako	k8xS	jako
Fantasia	Fantasia	k1gFnSc1	Fantasia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ballroom	Ballroom	k1gInSc1	Ballroom
Tango	tango	k1gNnSc1	tango
(	(	kIx(	(
<g/>
tango	tango	k1gNnSc1	tango
jako	jako	k8xS	jako
společenský	společenský	k2eAgInSc1d1	společenský
tanec	tanec	k1gInSc1	tanec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finské	finský	k2eAgNnSc1d1	finské
Tango	tango	k1gNnSc1	tango
</s>
</p>
<p>
<s>
Filipino	Filipin	k2eAgNnSc1d1	Filipino
Tango	tango	k1gNnSc1	tango
</s>
</p>
<p>
<s>
===	===	k?	===
Styly	styl	k1gInPc7	styl
tangové	tangový	k2eAgFnSc2d1	Tangová
hudby	hudba	k1gFnSc2	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
</s>
</p>
<p>
<s>
Vals	Vals	k1gInSc1	Vals
/	/	kIx~	/
Vals	Vals	k1gInSc1	Vals
criollo	criollo	k1gNnSc1	criollo
(	(	kIx(	(
<g/>
valčíková	valčíkový	k2eAgFnSc1d1	Valčíková
forma	forma	k1gFnSc1	forma
tanga	tango	k1gNnSc2	tango
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milonga	Milonga	k1gFnSc1	Milonga
(	(	kIx(	(
<g/>
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
tanec	tanec	k1gInSc1	tanec
s	s	k7c7	s
většinou	většinou	k6eAd1	většinou
rychlejším	rychlý	k2eAgNnSc7d2	rychlejší
tempem	tempo	k1gNnSc7	tempo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Canyengue	Canyenguat	k5eAaPmIp3nS	Canyenguat
(	(	kIx(	(
<g/>
starší	starý	k2eAgInSc4d2	starší
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
se	se	k3xPyFc4	se
výraznou	výrazný	k2eAgFnSc7d1	výrazná
rytmikou	rytmika	k1gFnSc7	rytmika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Candombe	Candombat	k5eAaPmIp3nS	Candombat
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
uruguayský	uruguayský	k2eAgInSc4d1	uruguayský
styl	styl	k1gInSc4	styl
používaný	používaný	k2eAgInSc4d1	používaný
při	při	k7c6	při
karnevalech	karneval	k1gInPc6	karneval
a	a	k8xC	a
přehlídkách	přehlídka	k1gFnPc6	přehlídka
<g/>
,	,	kIx,	,
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
africkými	africký	k2eAgInPc7d1	africký
kořeny	kořen	k1gInPc7	kořen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
tanec	tanec	k1gInSc4	tanec
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
milongové	milongový	k2eAgFnSc3d1	milongový
hudbě	hudba	k1gFnSc3	hudba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Electrotango	Electrotango	k1gMnSc1	Electrotango
–	–	k?	–
např.	např.	kA	např.
Bajofondo	Bajofondo	k6eAd1	Bajofondo
<g/>
,	,	kIx,	,
Gotan	Gotan	k1gMnSc1	Gotan
Project	Project	k1gMnSc1	Project
</s>
</p>
<p>
<s>
Alternative	Alternativ	k1gInSc5	Alternativ
Tango	tango	k1gNnSc1	tango
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
původně	původně	k6eAd1	původně
nebyla	být	k5eNaImAgFnS	být
tangová	tangový	k2eAgFnSc1d1	Tangová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
přizpůsobená	přizpůsobený	k2eAgFnSc1d1	přizpůsobená
pro	pro	k7c4	pro
tango	tango	k1gNnSc4	tango
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Figury	figura	k1gFnSc2	figura
==	==	k?	==
</s>
</p>
<p>
<s>
Figury	figura	k1gFnPc1	figura
podle	podle	k7c2	podle
ČSTS	ČSTS	kA	ČSTS
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chůze	chůze	k1gFnSc1	chůze
(	(	kIx(	(
<g/>
Walks	Walks	k1gInSc1	Walks
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Postupový	postupový	k2eAgInSc1d1	postupový
krok	krok	k1gInSc1	krok
(	(	kIx(	(
<g/>
Progresive	Progresiev	k1gFnPc1	Progresiev
Side	Sid	k1gFnSc2	Sid
Step	step	k1gFnSc1	step
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Postupová	postupový	k2eAgFnSc1d1	postupová
spojka	spojka	k1gFnSc1	spojka
(	(	kIx(	(
<g/>
Progressive	Progressiev	k1gFnSc2	Progressiev
Link	Link	k1gInSc1	Link
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zavřená	zavřený	k2eAgFnSc1d1	zavřená
promenáda	promenáda	k1gFnSc1	promenáda
(	(	kIx(	(
<g/>
Closed	Closed	k1gInSc1	Closed
Promenade	Promenad	k1gInSc5	Promenad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolébková	kolébkový	k2eAgFnSc1d1	kolébková
otáčka	otáčka	k1gFnSc1	otáčka
(	(	kIx(	(
<g/>
Rock	rock	k1gInSc1	rock
Turn	Turn	k1gNnSc1	Turn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
partnerka	partnerka	k1gFnSc1	partnerka
mimo	mimo	k6eAd1	mimo
<g/>
,	,	kIx,	,
zavřené	zavřený	k2eAgNnSc1d1	zavřené
zakončení	zakončení	k1gNnSc1	zakončení
(	(	kIx(	(
<g/>
Open	Open	k1gNnSc1	Open
Reverse	reverse	k1gFnPc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
,	,	kIx,	,
Partner	partner	k1gMnSc1	partner
Outside	Outsid	k1gInSc5	Outsid
<g/>
,	,	kIx,	,
Closed	Closed	k1gMnSc1	Closed
Finish	Finish	k1gMnSc1	Finish
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
partnerka	partnerka	k1gFnSc1	partnerka
vyrovnaně	vyrovnaně	k6eAd1	vyrovnaně
<g/>
,	,	kIx,	,
zavřené	zavřený	k2eAgNnSc1d1	zavřené
zakončení	zakončení	k1gNnSc1	zakončení
(	(	kIx(	(
<g/>
Open	Open	k1gNnSc1	Open
Reverse	reverse	k1gFnPc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
,	,	kIx,	,
Partner	partner	k1gMnSc1	partner
in	in	k?	in
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
Closed	Closed	k1gMnSc1	Closed
Finish	Finish	k1gMnSc1	Finish
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Korta	Korta	k1gFnSc1	Korta
vzad	vzad	k6eAd1	vzad
(	(	kIx(	(
<g/>
Back	Back	k1gInSc4	Back
Corté	Cortý	k2eAgFnPc1d1	Cortý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
partnerka	partnerka	k1gFnSc1	partnerka
mimo	mimo	k6eAd1	mimo
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgNnSc1d1	otevřené
zakončení	zakončení	k1gNnSc1	zakončení
(	(	kIx(	(
<g/>
Open	Open	k1gNnSc1	Open
Reverse	reverse	k1gFnPc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
,	,	kIx,	,
Partner	partner	k1gMnSc1	partner
Outside	Outsid	k1gInSc5	Outsid
<g/>
,	,	kIx,	,
Open	Open	k1gMnSc1	Open
Finish	Finish	k1gMnSc1	Finish
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
partnerka	partnerka	k1gFnSc1	partnerka
vyrovnaně	vyrovnaně	k6eAd1	vyrovnaně
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgNnSc1d1	otevřené
zakončení	zakončení	k1gNnSc1	zakončení
(	(	kIx(	(
<g/>
Open	Open	k1gNnSc1	Open
Reverse	reverse	k1gFnPc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
,	,	kIx,	,
Partner	partner	k1gMnSc1	partner
in	in	k?	in
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
Open	Open	k1gMnSc1	Open
Finish	Finish	k1gMnSc1	Finish
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
postupovým	postupový	k2eAgInSc7d1	postupový
úkrokem	úkrok	k1gInSc7	úkrok
(	(	kIx(	(
<g/>
Progressive	Progressiev	k1gFnPc1	Progressiev
Side	Sid	k1gFnSc2	Sid
Step	step	k1gFnSc4	step
Reverse	reverse	k1gFnSc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
promenáda	promenáda	k1gFnSc1	promenáda
(	(	kIx(	(
<g/>
Open	Open	k1gInSc1	Open
Promenade	Promenad	k1gInSc5	Promenad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolébky	kolébka	k1gFnPc1	kolébka
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
a	a	k8xC	a
levou	levý	k2eAgFnSc4d1	levá
nohu	noha	k1gFnSc4	noha
(	(	kIx(	(
<g/>
Left	Left	k2eAgInSc1d1	Left
Feet	Feet	k1gInSc1	Feet
and	and	k?	and
Right	Right	k2eAgInSc1d1	Right
Feet	Feet	k2eAgInSc1d1	Feet
Rocks	Rocks	k1gInSc1	Rocks
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Twistová	Twistový	k2eAgFnSc1d1	Twistový
otáčka	otáčka	k1gFnSc1	otáčka
vpravo	vpravo	k6eAd1	vpravo
(	(	kIx(	(
<g/>
Natural	Natural	k?	Natural
Twist	twist	k1gInSc1	twist
Turn	Turn	k1gInSc1	Turn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Promenádní	promenádní	k2eAgFnSc1d1	promenádní
otáčka	otáčka	k1gFnSc1	otáčka
vpravo	vpravo	k6eAd1	vpravo
(	(	kIx(	(
<g/>
Natural	Natural	k?	Natural
Promenade	Promenad	k1gInSc5	Promenad
Turn	Turn	k1gNnSc4	Turn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Promenádní	promenádní	k2eAgFnSc7d1	promenádní
otáčkou	otáčka	k1gFnSc7	otáčka
vpravo	vpravo	k6eAd1	vpravo
do	do	k7c2	do
kolébkové	kolébkový	k2eAgFnSc2d1	kolébková
otáčky	otáčka	k1gFnSc2	otáčka
(	(	kIx(	(
<g/>
Natural	Natural	k?	Natural
Promenade	Promenad	k1gInSc5	Promenad
Turn	Turna	k1gFnPc2	Turna
to	ten	k3xDgNnSc1	ten
Rock	rock	k1gInSc1	rock
Turn	Turn	k1gInSc4	Turn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Promenádní	promenádní	k2eAgFnSc1d1	promenádní
spojka	spojka	k1gFnSc1	spojka
(	(	kIx(	(
<g/>
Promenade	Promenad	k1gInSc5	Promenad
Link	Linko	k1gNnPc2	Linko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čtyřkrok	Čtyřkrok	k1gInSc1	Čtyřkrok
(	(	kIx(	(
<g/>
Four	Four	k1gInSc1	Four
Step	step	k1gInSc1	step
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čtyřkrok	Čtyřkrok	k1gInSc1	Čtyřkrok
se	se	k3xPyFc4	se
slow	slow	k?	slow
navíc	navíc	k6eAd1	navíc
(	(	kIx(	(
<g/>
Four	Four	k1gInSc1	Four
Step	step	k1gInSc1	step
with	witha	k1gFnPc2	witha
Extra	extra	k2eAgFnSc2d1	extra
Slow	Slow	k1gFnSc2	Slow
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Promenáda	promenáda	k1gFnSc1	promenáda
otevřená	otevřený	k2eAgFnSc1d1	otevřená
vzad	vzad	k6eAd1	vzad
(	(	kIx(	(
<g/>
Back	Back	k1gMnSc1	Back
Open	Open	k1gMnSc1	Open
Promenade	Promenad	k1gInSc5	Promenad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výkruty	výkrut	k1gInPc1	výkrut
mimo	mimo	k7c4	mimo
(	(	kIx(	(
<g/>
Outside	Outsid	k1gInSc5	Outsid
Swiwels	Swiwels	k1gInSc1	Swiwels
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spádná	spádný	k2eAgFnSc1d1	spádná
promenáda	promenáda	k1gFnSc1	promenáda
(	(	kIx(	(
<g/>
Fallaway	Fallaway	k1gInPc1	Fallaway
Promenade	Promenad	k1gInSc5	Promenad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
čtyřkrokem	čtyřkrokem	k6eAd1	čtyřkrokem
(	(	kIx(	(
<g/>
Four	Four	k1gInSc1	Four
Step	step	k1gInSc1	step
Change	change	k1gFnSc1	change
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přítahový	Přítahový	k2eAgInSc1d1	Přítahový
příklep	příklep	k1gInSc1	příklep
(	(	kIx(	(
<g/>
Brush	Brush	k1gMnSc1	Brush
Tap	Tap	k1gMnSc1	Tap
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spádný	spádný	k2eAgInSc1d1	spádný
čtyřkrok	čtyřkrok	k1gInSc1	čtyřkrok
(	(	kIx(	(
<g/>
Fallaway	Fallawaa	k1gFnSc2	Fallawaa
Four	Four	k1gInSc1	Four
Step	step	k1gInSc1	step
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
(	(	kIx(	(
<g/>
Basic	Basic	kA	Basic
Reverse	reverse	k1gFnSc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
(	(	kIx(	(
<g/>
Winnese	Winnese	k1gFnSc1	Winnese
Waltz	waltz	k1gInSc1	waltz
Reverse	reverse	k1gFnSc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obchvat	obchvat	k1gInSc1	obchvat
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Chase	chasa	k1gFnSc6	chasa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spádná	spádný	k2eAgFnSc1d1	spádná
otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
vkluzná	vkluzný	k2eAgFnSc1d1	vkluzný
pivota	pivot	k1gMnSc2	pivot
(	(	kIx(	(
<g/>
Fallaway	Fallawa	k2eAgFnPc1d1	Fallawa
Reverse	reverse	k1gFnPc1	reverse
and	and	k?	and
Slip	slip	k1gInSc1	slip
Pivot	pivot	k1gInSc1	pivot
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pětikrok	Pětikrok	k1gInSc1	Pětikrok
(	(	kIx(	(
<g/>
Five	Five	k1gFnSc1	Five
Step	step	k1gFnSc1	step
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Minipětikrok	Minipětikrok	k1gInSc1	Minipětikrok
(	(	kIx(	(
<g/>
Mini	mini	k2eAgFnSc1d1	mini
Five	Five	k1gFnSc1	Five
Step	step	k1gFnSc1	step
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Technika	technika	k1gFnSc1	technika
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Argentinské	argentinský	k2eAgNnSc1d1	argentinské
tango	tango	k1gNnSc1	tango
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tango	tango	k1gNnSc4	tango
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
