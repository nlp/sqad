<s>
Čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
z	z	k7c2	z
výslovnosti	výslovnost	k1gFnSc2	výslovnost
čínského	čínský	k2eAgMnSc4d1	čínský
茶	茶	k?	茶
-	-	kIx~	-
čcha	čcha	k1gMnSc1	čcha
v	v	k7c6	v
mandarínském	mandarínský	k2eAgInSc6d1	mandarínský
a	a	k8xC	a
kantonském	kantonský	k2eAgInSc6d1	kantonský
dialektu	dialekt	k1gInSc6	dialekt
<g/>
;	;	kIx,	;
v	v	k7c6	v
dialektu	dialekt	k1gInSc6	dialekt
min	mina	k1gFnPc2	mina
významného	významný	k2eAgInSc2d1	významný
přístavu	přístav	k1gInSc2	přístav
Sia-men	Siaen	k1gInSc1	Sia-men
čteno	číst	k5eAaImNgNnS	číst
te	te	k?	te
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
pojmenování	pojmenování	k1gNnSc1	pojmenování
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgInSc1d1	kulturní
nápoj	nápoj	k1gInSc1	nápoj
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
obvykle	obvykle	k6eAd1	obvykle
louhováním	louhování	k1gNnSc7	louhování
lístků	lístek	k1gInPc2	lístek
rostliny	rostlina	k1gFnSc2	rostlina
čajovníku	čajovník	k1gInSc2	čajovník
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
