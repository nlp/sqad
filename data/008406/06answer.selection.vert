<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
zvaný	zvaný	k2eAgInSc1d1	zvaný
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
Pudinkový	pudinkový	k2eAgInSc1d1	pudinkový
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
objevitel	objevitel	k1gMnSc1	objevitel
elektronu	elektron	k1gInSc2	elektron
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
Joseph	Joseph	k1gMnSc1	Joseph
John	John	k1gMnSc1	John
Thomson	Thomson	k1gMnSc1	Thomson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
