<p>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
zvaný	zvaný	k2eAgInSc1d1	zvaný
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
Pudinkový	pudinkový	k2eAgInSc1d1	pudinkový
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
objevitel	objevitel	k1gMnSc1	objevitel
elektronu	elektron	k1gInSc2	elektron
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
Joseph	Joseph	k1gMnSc1	Joseph
John	John	k1gMnSc1	John
Thomson	Thomson	k1gMnSc1	Thomson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Thomsonova	Thomsonův	k2eAgInSc2d1	Thomsonův
modelu	model	k1gInSc2	model
se	se	k3xPyFc4	se
atom	atom	k1gInSc1	atom
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
však	však	k9	však
Thomson	Thomson	k1gInSc1	Thomson
nazývá	nazývat	k5eAaImIp3nS	nazývat
jen	jen	k9	jen
částicemi	částice	k1gFnPc7	částice
-	-	kIx~	-
i	i	k8xC	i
když	když	k8xS	když
George	Georg	k1gMnSc4	Georg
Johnstone	Johnston	k1gInSc5	Johnston
Stoney	Stonea	k1gMnSc2	Stonea
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
nazývat	nazývat	k5eAaImF	nazývat
elektrické	elektrický	k2eAgFnSc2d1	elektrická
částice	částice	k1gFnSc2	částice
elektrony	elektron	k1gInPc1	elektron
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
polévkou	polévka	k1gFnSc7	polévka
kladného	kladný	k2eAgInSc2d1	kladný
náboje	náboj	k1gInSc2	náboj
vyvažující	vyvažující	k2eAgMnSc1d1	vyvažující
tak	tak	k8xS	tak
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nP	kdyby
byly	být	k5eAaImAgFnP	být
záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgFnPc4d1	nabitá
rozinky	rozinka	k1gFnPc4	rozinka
obklopené	obklopený	k2eAgFnPc4d1	obklopená
kladně	kladně	k6eAd1	kladně
nabitým	nabitý	k2eAgInSc7d1	nabitý
pudinkem	pudink	k1gInSc7	pudink
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
umístěny	umístit	k5eAaPmNgFnP	umístit
různě	různě	k6eAd1	různě
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
různé	různý	k2eAgFnPc4d1	různá
struktury	struktura	k1gFnPc4	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
vyvrácen	vyvrátit	k5eAaPmNgInS	vyvrátit
experimenty	experiment	k1gInPc1	experiment
Hanse	Hans	k1gMnSc2	Hans
Geigera	Geiger	k1gMnSc2	Geiger
a	a	k8xC	a
Ernesta	Ernest	k1gMnSc2	Ernest
Mardsena	Mardsen	k2eAgFnSc1d1	Mardsen
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
interpretoval	interpretovat	k5eAaBmAgInS	interpretovat
jako	jako	k9	jako
pozorování	pozorování	k1gNnSc4	pozorování
velmi	velmi	k6eAd1	velmi
malého	malý	k2eAgNnSc2d1	malé
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc2	atom
nesoucího	nesoucí	k2eAgInSc2d1	nesoucí
velmi	velmi	k6eAd1	velmi
vysoký	vysoký	k2eAgInSc1d1	vysoký
kladný	kladný	k2eAgInSc1d1	kladný
náboj	náboj	k1gInSc1	náboj
(	(	kIx(	(
<g/>
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
pro	pro	k7c4	pro
vyvážení	vyvážení	k1gNnSc4	vyvážení
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
elektronů	elektron	k1gInPc2	elektron
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
Rutherfordovu	Rutherfordův	k2eAgInSc3d1	Rutherfordův
modelu	model	k1gInSc3	model
atomu	atom	k1gInSc2	atom
a	a	k8xC	a
následně	následně	k6eAd1	následně
(	(	kIx(	(
<g/>
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
co	co	k9	co
Henry	Henry	k1gMnSc1	Henry
Moseley	Moselea	k1gFnSc2	Moselea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jaderný	jaderný	k2eAgInSc1d1	jaderný
náboj	náboj	k1gInSc1	náboj
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
atomovému	atomový	k2eAgNnSc3d1	atomové
číslu	číslo	k1gNnSc3	číslo
<g/>
)	)	kIx)	)
k	k	k7c3	k
předpokladu	předpoklad	k1gInSc3	předpoklad
Antonia	Antonio	k1gMnSc2	Antonio
van	vana	k1gFnPc2	vana
den	den	k1gInSc1	den
Broeka	Broeek	k1gInSc2	Broeek
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
nábojem	náboj	k1gInSc7	náboj
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
představil	představit	k5eAaPmAgInS	představit
Niels	Niels	k1gInSc1	Niels
Bohr	Bohr	k1gInSc1	Bohr
první	první	k4xOgMnSc1	první
kvantový	kvantový	k2eAgInSc1d1	kvantový
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
sestávající	sestávající	k2eAgNnSc1d1	sestávající
z	z	k7c2	z
kladného	kladný	k2eAgInSc2d1	kladný
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
atomovému	atomový	k2eAgNnSc3d1	atomové
číslu	číslo	k1gNnSc3	číslo
<g/>
)	)	kIx)	)
obklopené	obklopený	k2eAgMnPc4d1	obklopený
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
umístěných	umístěný	k2eAgInPc2d1	umístěný
v	v	k7c6	v
orbitálních	orbitální	k2eAgFnPc6d1	orbitální
hladinách	hladina	k1gFnPc6	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
srovnáván	srovnávat	k5eAaImNgInS	srovnávat
(	(	kIx(	(
<g/>
ne	ne	k9	ne
samotným	samotný	k2eAgMnSc7d1	samotný
Thomsonem	Thomson	k1gMnSc7	Thomson
<g/>
)	)	kIx)	)
ke	k	k7c3	k
korintskému	korintský	k2eAgInSc3d1	korintský
pudinku	pudink	k1gInSc3	pudink
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
představa	představa	k1gFnSc1	představa
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
statické	statický	k2eAgFnPc1d1	statická
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
podle	podle	k7c2	podle
Thomsona	Thomsona	k1gFnSc1	Thomsona
nebyly	být	k5eNaImAgInP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Thomsonov	Thomsonov	k1gInSc4	Thomsonov
model	model	k1gInSc1	model
atómu	atómat	k5eAaPmIp1nS	atómat
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
