<s>
Argon	argon	k1gInSc1	argon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ar	ar	k1gInSc1	ar
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Argon	argon	k1gInSc1	argon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
patřící	patřící	k2eAgInSc4d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
