<s>
Argon	argon	k1gInSc1	argon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ar	ar	k1gInSc1	ar
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Argon	argon	k1gInSc1	argon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
patřící	patřící	k2eAgInSc4d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
nereaktivní	reaktivní	k2eNgFnSc1d1	nereaktivní
<g/>
,	,	kIx,	,
úplně	úplně	k6eAd1	úplně
inertní	inertní	k2eAgNnPc1d1	inertní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
litru	litr	k1gInSc6	litr
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
33,6	[number]	k4	33,6
ml	ml	kA	ml
argonu	argon	k1gInSc2	argon
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
rozpustnější	rozpustný	k2eAgMnSc1d2	rozpustnější
než	než	k8xS	než
kyslík	kyslík	k1gInSc1	kyslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
nepolárních	polární	k2eNgNnPc6d1	nepolární
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Argon	argon	k1gInSc4	argon
lze	lze	k6eAd1	lze
adsorbovat	adsorbovat	k5eAaBmF	adsorbovat
na	na	k7c6	na
aktivním	aktivní	k2eAgNnSc6d1	aktivní
uhlí	uhlí	k1gNnSc6	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Argon	argon	k1gInSc1	argon
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
snadno	snadno	k6eAd1	snadno
ionizuje	ionizovat	k5eAaBmIp3nS	ionizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
ionizovaném	ionizovaný	k2eAgInSc6d1	ionizovaný
stavu	stav	k1gInSc6	stav
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
osvětlovací	osvětlovací	k2eAgFnSc6d1	osvětlovací
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Argon	argon	k1gInSc1	argon
září	září	k1gNnSc2	září
při	při	k7c6	při
větší	veliký	k2eAgFnSc6d2	veliký
koncentraci	koncentrace	k1gFnSc6	koncentrace
červeně	červeň	k1gFnSc2	červeň
<g/>
,	,	kIx,	,
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
přechází	přecházet	k5eAaImIp3nP	přecházet
přes	přes	k7c4	přes
fialovou	fialový	k2eAgFnSc4d1	fialová
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
až	až	k6eAd1	až
k	k	k7c3	k
bílé	bílý	k2eAgFnSc3d1	bílá
barvě	barva	k1gFnSc3	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
první	první	k4xOgFnSc1	první
sloučenina	sloučenina	k1gFnSc1	sloučenina
argonu	argon	k1gInSc2	argon
-	-	kIx~	-
HArF	harfa	k1gFnPc2	harfa
<g/>
.	.	kIx.	.
</s>
<s>
Syntéza	syntéza	k1gFnSc1	syntéza
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
reakcí	reakce	k1gFnSc7	reakce
argonu	argon	k1gInSc2	argon
s	s	k7c7	s
fluorovodíkem	fluorovodík	k1gInSc7	fluorovodík
v	v	k7c6	v
matrici	matrice	k1gFnSc6	matrice
z	z	k7c2	z
jodidu	jodid	k1gInSc2	jodid
cesného	cesný	k2eAgInSc2d1	cesný
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
8	[number]	k4	8
K.	K.	kA	K.
Sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
40	[number]	k4	40
K.	K.	kA	K.
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
a	a	k8xC	a
Joseph	Joseph	k1gMnSc1	Joseph
Priestley	Priestlea	k1gFnSc2	Priestlea
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
přítomnost	přítomnost	k1gFnSc4	přítomnost
argonu	argon	k1gInSc2	argon
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
odstranit	odstranit	k5eAaPmF	odstranit
kyslík	kyslík	k1gInSc4	kyslík
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
rozžhavenou	rozžhavený	k2eAgFnSc7d1	rozžhavená
mědí	měď	k1gFnSc7	měď
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
rozpuštěním	rozpuštění	k1gNnPc3	rozpuštění
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
dusík	dusík	k1gInSc4	dusík
odstranili	odstranit	k5eAaPmAgMnP	odstranit
působením	působení	k1gNnSc7	působení
elektrických	elektrický	k2eAgMnPc2d1	elektrický
výbojů	výboj	k1gInPc2	výboj
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
směs	směs	k1gFnSc4	směs
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
při	při	k7c6	při
čemž	což	k3yQnSc6	což
vznikají	vznikat	k5eAaImIp3nP	vznikat
oxidy	oxid	k1gInPc1	oxid
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nP	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
zůstal	zůstat	k5eAaPmAgInS	zůstat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
argon	argon	k1gInSc4	argon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
další	další	k2eAgInPc1d1	další
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
argonu	argon	k1gInSc2	argon
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
připisován	připisovat	k5eAaImNgInS	připisovat
lordu	lord	k1gMnSc3	lord
Rayleighovi	Rayleigh	k1gMnSc3	Rayleigh
a	a	k8xC	a
Williamu	William	k1gInSc2	William
Ramsayovi	Ramsaya	k1gMnSc3	Ramsaya
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prvek	prvek	k1gInSc4	prvek
objevili	objevit	k5eAaPmAgMnP	objevit
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
Henry	henry	k1gInSc7	henry
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
a	a	k8xC	a
Joseph	Josepha	k1gFnPc2	Josepha
Priestley	Priestlea	k1gFnSc2	Priestlea
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
zkoumání	zkoumání	k1gNnSc2	zkoumání
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
ho	on	k3xPp3gMnSc4	on
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
netečnosti	netečnost	k1gFnSc2	netečnost
argon	argon	k1gInSc1	argon
-	-	kIx~	-
líný	líný	k2eAgMnSc1d1	líný
<g/>
.	.	kIx.	.
</s>
<s>
Argon	argon	k1gInSc1	argon
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
její	její	k3xOp3gInSc1	její
1	[number]	k4	1
%	%	kIx~	%
(	(	kIx(	(
<g/>
ve	v	k7c6	v
100	[number]	k4	100
l	l	kA	l
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
934	[number]	k4	934
ml	ml	kA	ml
argonu	argon	k1gInSc2	argon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
získáván	získávat	k5eAaImNgMnS	získávat
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
argon	argon	k1gInSc1	argon
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
způsobem	způsob	k1gInSc7	způsob
popsaným	popsaný	k2eAgFnPc3d1	popsaná
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
vývoji	vývoj	k1gInSc6	vývoj
nebo	nebo	k8xC	nebo
frakční	frakční	k2eAgFnSc7d1	frakční
adsorpcí	adsorpce	k1gFnSc7	adsorpce
na	na	k7c4	na
aktivní	aktivní	k2eAgNnSc4d1	aktivní
uhlí	uhlí	k1gNnSc4	uhlí
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kapalného	kapalný	k2eAgInSc2d1	kapalný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Inertních	inertní	k2eAgFnPc2d1	inertní
vlastností	vlastnost	k1gFnPc2	vlastnost
argonu	argon	k1gInSc2	argon
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k9	především
při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
atmosféru	atmosféra	k1gFnSc4	atmosféra
kolem	kolem	k7c2	kolem
roztaveného	roztavený	k2eAgInSc2d1	roztavený
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vzniku	vznik	k1gInSc2	vznik
oxidů	oxid	k1gInPc2	oxid
a	a	k8xC	a
nitridů	nitrid	k1gInPc2	nitrid
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zhoršování	zhoršování	k1gNnSc4	zhoršování
mechanických	mechanický	k2eAgFnPc2d1	mechanická
vlastností	vlastnost	k1gFnPc2	vlastnost
svaru	svar	k1gInSc2	svar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
se	se	k3xPyFc4	se
ochranná	ochranný	k2eAgFnSc1d1	ochranná
atmosféra	atmosféra	k1gFnSc1	atmosféra
argonu	argon	k1gInSc2	argon
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
při	při	k7c6	při
tavení	tavení	k1gNnSc6	tavení
slitin	slitina	k1gFnPc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
krystalů	krystal	k1gInPc2	krystal
superčistého	superčistý	k2eAgInSc2d1	superčistý
křemíku	křemík	k1gInSc2	křemík
a	a	k8xC	a
germania	germanium	k1gNnSc2	germanium
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
součástek	součástka	k1gFnPc2	součástka
pro	pro	k7c4	pro
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
techniku	technika	k1gFnSc4	technika
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
velmi	velmi	k6eAd1	velmi
čistého	čistý	k2eAgInSc2d1	čistý
argonu	argon	k1gInSc2	argon
<g/>
.	.	kIx.	.
</s>
<s>
Argon	argon	k1gInSc1	argon
se	se	k3xPyFc4	se
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
ochranná	ochranný	k2eAgFnSc1d1	ochranná
atmosféra	atmosféra	k1gFnSc1	atmosféra
žárovek	žárovka	k1gFnPc2	žárovka
a	a	k8xC	a
jako	jako	k9	jako
prostředí	prostředí	k1gNnSc2	prostředí
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
směsi	směs	k1gFnSc6	směs
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
sáčků	sáček	k1gInPc2	sáček
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
brambůrek	brambůrek	k1gInSc1	brambůrek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
takto	takto	k6eAd1	takto
ochráněny	ochráněn	k2eAgInPc1d1	ochráněn
před	před	k7c7	před
zvlhnutím	zvlhnutí	k1gNnSc7	zvlhnutí
a	a	k8xC	a
před	před	k7c7	před
rozmačkáním	rozmačkání	k1gNnSc7	rozmačkání
<g/>
.	.	kIx.	.
</s>
<s>
Čistého	čistý	k2eAgInSc2d1	čistý
argonu	argon	k1gInSc2	argon
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
výbojkách	výbojka	k1gFnPc6	výbojka
<g/>
,	,	kIx,	,
elektrických	elektrický	k2eAgInPc6d1	elektrický
obloucích	oblouk	k1gInPc6	oblouk
a	a	k8xC	a
doutnavých	doutnavý	k2eAgFnPc6d1	doutnavý
trubicích	trubice	k1gFnPc6	trubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
koncentrace	koncentrace	k1gFnSc2	koncentrace
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
fialovou	fialový	k2eAgFnSc4d1	fialová
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc4d1	modrá
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
přínos	přínos	k1gInSc1	přínos
pro	pro	k7c4	pro
analytickou	analytický	k2eAgFnSc4d1	analytická
chemii	chemie	k1gFnSc4	chemie
znamenal	znamenat	k5eAaImAgInS	znamenat
objev	objev	k1gInSc1	objev
a	a	k8xC	a
technické	technický	k2eAgNnSc1d1	technické
zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
udržitelným	udržitelný	k2eAgNnSc7d1	udržitelné
plazmatem	plazma	k1gNnSc7	plazma
<g/>
,	,	kIx,	,
indukčně	indukčně	k6eAd1	indukčně
vázaným	vázaný	k2eAgNnSc7d1	vázané
plazmatem	plazma	k1gNnSc7	plazma
<g/>
,	,	kIx,	,
označovaným	označovaný	k2eAgMnSc7d1	označovaný
obvykle	obvykle	k6eAd1	obvykle
zkratkou	zkratka	k1gFnSc7	zkratka
ICP	ICP	kA	ICP
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
médium	médium	k1gNnSc1	médium
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
tohoto	tento	k3xDgNnSc2	tento
plazmatu	plazma	k1gNnSc2	plazma
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
právě	právě	k9	právě
čistý	čistý	k2eAgInSc1d1	čistý
argon	argon	k1gInSc1	argon
<g/>
.	.	kIx.	.
</s>
<s>
Proudící	proudící	k2eAgInSc1d1	proudící
plyn	plyn	k1gInSc1	plyn
o	o	k7c6	o
průtoku	průtok	k1gInSc6	průtok
10	[number]	k4	10
-	-	kIx~	-
20	[number]	k4	20
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
hořáku	hořák	k1gInSc6	hořák
buzen	buzen	k2eAgInSc4d1	buzen
vysokofrekvenčním	vysokofrekvenční	k2eAgInSc7d1	vysokofrekvenční
proudem	proud	k1gInSc7	proud
o	o	k7c6	o
frekvenci	frekvence	k1gFnSc6	frekvence
řádově	řádově	k6eAd1	řádově
desítek	desítka	k1gFnPc2	desítka
MHz	Mhz	kA	Mhz
a	a	k8xC	a
příkonu	příkon	k1gInSc2	příkon
0,5	[number]	k4	0,5
-	-	kIx~	-
2	[number]	k4	2
kWh	kwh	kA	kwh
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
udržet	udržet	k5eAaPmF	udržet
argonové	argonový	k2eAgNnSc4d1	argonový
plazma	plazma	k1gNnSc4	plazma
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
6	[number]	k4	6
-	-	kIx~	-
8000	[number]	k4	8000
K	K	kA	K
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
neomezenou	omezený	k2eNgFnSc4d1	neomezená
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
medium	medium	k1gNnSc1	medium
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
analytických	analytický	k2eAgFnPc6d1	analytická
technikách	technika	k1gFnPc6	technika
<g/>
:	:	kIx,	:
ICP-OES	ICP-OES	k1gFnSc1	ICP-OES
neboli	neboli	k8xC	neboli
optická	optický	k2eAgFnSc1d1	optická
emisní	emisní	k2eAgFnSc1d1	emisní
spektrometrie	spektrometrie	k1gFnSc1	spektrometrie
s	s	k7c7	s
indukčně	indukčně	k6eAd1	indukčně
vázaným	vázaný	k2eAgNnSc7d1	vázané
plazmatem	plazma	k1gNnSc7	plazma
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
vychází	vycházet	k5eAaImIp3nP	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
nad	nad	k7c7	nad
6	[number]	k4	6
000	[number]	k4	000
K	K	kA	K
je	být	k5eAaImIp3nS	být
vybuzena	vybuzen	k2eAgFnSc1d1	vybuzen
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
emisních	emisní	k2eAgFnPc2d1	emisní
čar	čára	k1gFnPc2	čára
ve	v	k7c6	v
spektrech	spektrum	k1gNnPc6	spektrum
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Analyzovaný	analyzovaný	k2eAgInSc1d1	analyzovaný
roztok	roztok	k1gInSc1	roztok
je	být	k5eAaImIp3nS	být
dávkován	dávkovat	k5eAaImNgInS	dávkovat
do	do	k7c2	do
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
odpaří	odpařit	k5eAaPmIp3nS	odpařit
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
disociaci	disociace	k1gFnSc3	disociace
všech	všecek	k3xTgFnPc2	všecek
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitním	kvalitní	k2eAgInSc7d1	kvalitní
monochromátorem	monochromátor	k1gInSc7	monochromátor
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
monitorovány	monitorován	k2eAgInPc1d1	monitorován
úseky	úsek	k1gInPc1	úsek
emisního	emisní	k2eAgNnSc2d1	emisní
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
emisní	emisní	k2eAgFnPc1d1	emisní
linie	linie	k1gFnPc1	linie
analyzovaných	analyzovaný	k2eAgInPc2d1	analyzovaný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Změřená	změřený	k2eAgFnSc1d1	změřená
intenzita	intenzita	k1gFnSc1	intenzita
emitovaného	emitovaný	k2eAgNnSc2d1	emitované
záření	záření	k1gNnSc2	záření
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
emisní	emisní	k2eAgFnSc6d1	emisní
line	linout	k5eAaImIp3nS	linout
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
koncentraci	koncentrace	k1gFnSc4	koncentrace
měřeného	měřený	k2eAgInSc2d1	měřený
prvku	prvek	k1gInSc2	prvek
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
ICP-MS	ICP-MS	k?	ICP-MS
neboli	neboli	k8xC	neboli
hmotnostní	hmotnostní	k2eAgFnSc2d1	hmotnostní
spektrometrie	spektrometrie	k1gFnSc2	spektrometrie
s	s	k7c7	s
indukčně	indukčně	k6eAd1	indukčně
vázaným	vázaný	k2eAgNnSc7d1	vázané
plazmatem	plazma	k1gNnSc7	plazma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
plazmatu	plazma	k1gNnSc3	plazma
dostanou	dostat	k5eAaPmIp3nP	dostat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysokou	vysoký	k2eAgFnSc7d1	vysoká
energií	energie	k1gFnSc7	energie
toho	ten	k3xDgNnSc2	ten
prostředí	prostředí	k1gNnSc2	prostředí
ionizována	ionizovat	k5eAaBmNgFnS	ionizovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
iontů	ion	k1gInPc2	ion
M	M	kA	M
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
ionty	ion	k1gInPc4	ion
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
komplikovaným	komplikovaný	k2eAgInSc7d1	komplikovaný
systémem	systém	k1gInSc7	systém
přechodových	přechodový	k2eAgFnPc2d1	přechodová
komor	komora	k1gFnPc2	komora
převedeny	převést	k5eAaPmNgInP	převést
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
o	o	k7c6	o
tlaku	tlak	k1gInSc6	tlak
řádově	řádově	k6eAd1	řádově
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
Torr	torr	k1gInSc4	torr
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
klasického	klasický	k2eAgInSc2d1	klasický
kvadrupolového	kvadrupolový	k2eAgInSc2d1	kvadrupolový
analyzátoru	analyzátor	k1gInSc2	analyzátor
<g/>
.	.	kIx.	.
</s>
<s>
Analyzátor	analyzátor	k1gMnSc1	analyzátor
provede	provést	k5eAaPmIp3nS	provést
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
až	až	k8xS	až
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
skenů	sken	k1gMnPc2	sken
počtu	počet	k1gInSc2	počet
iontů	ion	k1gInPc2	ion
na	na	k7c6	na
zvolených	zvolený	k2eAgFnPc6d1	zvolená
hodnotách	hodnota	k1gFnPc6	hodnota
hmotností	hmotnost	k1gFnPc2	hmotnost
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
vyhodnotí	vyhodnotit	k5eAaPmIp3nP	vyhodnotit
obsahy	obsah	k1gInPc1	obsah
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
měřeném	měřený	k2eAgInSc6d1	měřený
roztoku	roztok	k1gInSc6	roztok
na	na	k7c6	na
základě	základ	k1gInSc6	základ
získané	získaný	k2eAgFnSc2d1	získaná
intenzity	intenzita	k1gFnSc2	intenzita
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
