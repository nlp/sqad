<s>
Co	co	k3yQnSc1	co
dodává	dodávat	k5eAaImIp3nS	dodávat
jazyku	jazyk	k1gInSc3	jazyk
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
jemné	jemný	k2eAgNnSc4d1	jemné
odstínění	odstínění	k1gNnSc4	odstínění
významů	význam	k1gInPc2	význam
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kontextovém	kontextový	k2eAgNnSc6d1	kontextové
a	a	k8xC	a
stylistickém	stylistický	k2eAgNnSc6d1	stylistické
zabarvení	zabarvení	k1gNnSc6	zabarvení
<g/>
?	?	kIx.	?
</s>
