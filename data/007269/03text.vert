<s>
Pentax	Pentax	k1gInSc1	Pentax
je	být	k5eAaImIp3nS	být
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
značka	značka	k1gFnSc1	značka
pro	pro	k7c4	pro
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
fototechniky	fototechnika	k1gFnSc2	fototechnika
a	a	k8xC	a
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
patřící	patřící	k2eAgFnSc1d1	patřící
pod	pod	k7c4	pod
japonský	japonský	k2eAgInSc4d1	japonský
koncern	koncern	k1gInSc4	koncern
Ricoh	Ricoha	k1gFnPc2	Ricoha
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
divizi	divize	k1gFnSc4	divize
Ricoh	Ricoha	k1gFnPc2	Ricoha
Imaging	Imaging	k1gInSc1	Imaging
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
společnost	společnost	k1gFnSc1	společnost
založil	založit	k5eAaPmAgMnS	založit
Kumao	Kumao	k1gMnSc1	Kumao
Kajiwara	Kajiwar	k1gMnSc2	Kajiwar
jakožto	jakožto	k8xS	jakožto
Asahi	Asah	k1gMnSc6	Asah
Kogaku	Kogak	k1gMnSc6	Kogak
Goshi	Gosh	k1gMnSc6	Gosh
Kaisha	Kaisha	k1gFnSc1	Kaisha
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Toshima	Toshimum	k1gNnSc2	Toshimum
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
produkcí	produkce	k1gFnSc7	produkce
brýlových	brýlový	k2eAgFnPc2d1	brýlová
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Asahi	Asahi	k1gNnSc4	Asahi
Optical	Optical	k1gFnSc2	Optical
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
旭	旭	k?	旭
Asahi	Asah	k1gFnSc2	Asah
Kō	Kō	k1gMnSc1	Kō
Kō	Kō	k1gMnSc1	Kō
Kabushiki-gaisha	Kabushikiaisha	k1gMnSc1	Kabushiki-gaisha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
firma	firma	k1gFnSc1	firma
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
i	i	k9	i
objektivy	objektiv	k1gInPc4	objektiv
pro	pro	k7c4	pro
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
a	a	k8xC	a
kinematografii	kinematografie	k1gFnSc4	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
plnila	plnit	k5eAaImAgFnS	plnit
hlavně	hlavně	k9	hlavně
armádní	armádní	k2eAgFnPc4d1	armádní
zakázky	zakázka	k1gFnPc4	zakázka
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
nuceně	nuceně	k6eAd1	nuceně
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
předválečných	předválečný	k2eAgFnPc6d1	předválečná
aktivitách	aktivita	k1gFnPc6	aktivita
výrobou	výroba	k1gFnSc7	výroba
dalekohledů	dalekohled	k1gInPc2	dalekohled
a	a	k8xC	a
objektivů	objektiv	k1gInPc2	objektiv
pro	pro	k7c4	pro
amatérské	amatérský	k2eAgInPc4d1	amatérský
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
firem	firma	k1gFnPc2	firma
Konishiroku	Konishirok	k1gInSc2	Konishirok
a	a	k8xC	a
Chiyoda	Chiyod	k1gMnSc2	Chiyod
Kō	Kō	k1gMnSc2	Kō
Seikō	Seikō	k1gMnSc2	Seikō
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Konica	Konica	kA	Konica
a	a	k8xC	a
Minolta	Minolta	kA	Minolta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
znamenalo	znamenat	k5eAaImAgNnS	znamenat
návrat	návrat	k1gInSc4	návrat
japonského	japonský	k2eAgInSc2d1	japonský
fotografického	fotografický	k2eAgInSc2d1	fotografický
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
z	z	k7c2	z
konce	konec	k1gInSc2	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
znovuzrozeného	znovuzrozený	k2eAgNnSc2d1	znovuzrozené
odvětví	odvětví	k1gNnSc2	odvětví
japonského	japonský	k2eAgInSc2d1	japonský
průmyslu	průmysl	k1gInSc2	průmysl
mířila	mířit	k5eAaImAgFnS	mířit
k	k	k7c3	k
okupačním	okupační	k2eAgFnPc3d1	okupační
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výrobky	výrobek	k1gInPc1	výrobek
získaly	získat	k5eAaPmAgInP	získat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
reputaci	reputace	k1gFnSc4	reputace
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
příliv	příliv	k1gInSc4	příliv
žurnalistů	žurnalist	k1gMnPc2	žurnalist
a	a	k8xC	a
fotografů	fotograf	k1gMnPc2	fotograf
na	na	k7c4	na
dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
ohromeni	ohromit	k5eAaPmNgMnP	ohromit
kvalitou	kvalita	k1gFnSc7	kvalita
objektivů	objektiv	k1gInPc2	objektiv
firem	firma	k1gFnPc2	firma
jako	jako	k8xS	jako
např.	např.	kA	např.
Nikon	Nikon	k1gInSc1	Nikon
nebo	nebo	k8xC	nebo
Canon	Canon	kA	Canon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
značky	značka	k1gFnSc2	značka
Leica	Leic	k1gInSc2	Leic
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
měli	mít	k5eAaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
těla	tělo	k1gNnPc4	tělo
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
nahrazovali	nahrazovat	k5eAaImAgMnP	nahrazovat
své	svůj	k3xOyFgFnPc4	svůj
Leicy	Leica	k1gFnPc4	Leica
a	a	k8xC	a
Contaxy	Contax	k1gInPc4	Contax
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
Asahi	Asahe	k1gFnSc4	Asahe
Optical	Optical	k1gFnPc2	Optical
představil	představit	k5eAaPmAgMnS	představit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Asahiflex	Asahiflex	k1gInSc4	Asahiflex
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
japonskou	japonský	k2eAgFnSc4d1	japonská
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
používající	používající	k2eAgFnSc4d1	používající
35	[number]	k4	35
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Pentax	Pentax	k1gInSc1	Pentax
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
spojením	spojení	k1gNnSc7	spojení
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
Pentaprism	Pentaprism	k1gInSc1	Pentaprism
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
pětiboký	pětiboký	k2eAgInSc4d1	pětiboký
optický	optický	k2eAgInSc4d1	optický
hranol	hranol	k1gInSc4	hranol
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Contax	Contax	k1gInSc1	Contax
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
registrovanou	registrovaný	k2eAgFnSc7d1	registrovaná
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
východoněmecké	východoněmecký	k2eAgFnSc2d1	východoněmecká
VEB	Veba	k1gFnPc2	Veba
Zeiss	Zeissa	k1gFnPc2	Zeissa
Ikon	ikon	k1gInSc1	ikon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
později	pozdě	k6eAd2	pozdě
prodala	prodat	k5eAaPmAgFnS	prodat
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Pentax	Pentax	k1gInSc1	Pentax
<g/>
"	"	kIx"	"
firmě	firma	k1gFnSc3	firma
Asahi	Asahi	k1gNnSc2	Asahi
Optical	Optical	k1gFnSc2	Optical
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
dobou	doba	k1gFnSc7	doba
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
Asahi	Asah	k1gFnSc2	Asah
Optical	Optical	k1gFnSc1	Optical
známa	znám	k2eAgFnSc1d1	známa
hlavně	hlavně	k6eAd1	hlavně
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
produktům	produkt	k1gInPc3	produkt
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
fototechniky	fototechnika	k1gFnSc2	fototechnika
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
změnila	změnit	k5eAaPmAgFnS	změnit
název	název	k1gInSc4	název
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
na	na	k7c4	na
Pentax	Pentax	k1gInSc4	Pentax
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
firem	firma	k1gFnPc2	firma
produkujících	produkující	k2eAgFnPc2d1	produkující
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
<g/>
,	,	kIx,	,
dalekohledy	dalekohled	k1gInPc4	dalekohled
<g/>
,	,	kIx,	,
brýlové	brýlový	k2eAgFnPc4d1	brýlová
čočky	čočka	k1gFnPc4	čočka
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
měla	mít	k5eAaImAgFnS	mít
firma	firma	k1gFnSc1	firma
okolo	okolo	k7c2	okolo
6000	[number]	k4	6000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2006	[number]	k4	2006
Pentax	Pentax	k1gInSc1	Pentax
zahájil	zahájit	k5eAaPmAgInS	zahájit
proces	proces	k1gInSc4	proces
integrace	integrace	k1gFnSc2	integrace
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
Hoya	Hoyum	k1gNnSc2	Hoyum
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Primárním	primární	k2eAgInSc7d1	primární
cílem	cíl	k1gInSc7	cíl
firmy	firma	k1gFnSc2	firma
Hoya	Hoyus	k1gMnSc2	Hoyus
bylo	být	k5eAaImAgNnS	být
posílit	posílit	k5eAaPmF	posílit
business	business	k1gInSc4	business
točící	točící	k2eAgFnSc2d1	točící
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
lékařského	lékařský	k2eAgNnSc2d1	lékařské
vybavení	vybavení	k1gNnSc2	vybavení
získáním	získání	k1gNnSc7	získání
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
zkušeností	zkušenost	k1gFnPc2	zkušenost
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
endoskopů	endoskop	k1gInPc2	endoskop
<g/>
,	,	kIx,	,
očních	oční	k2eAgInPc2d1	oční
implantátů	implantát	k1gInPc2	implantát
<g/>
,	,	kIx,	,
lékařských	lékařský	k2eAgInPc2d1	lékařský
mikroskopů	mikroskop	k1gInPc2	mikroskop
<g/>
,	,	kIx,	,
biokompatibilních	biokompatibilní	k2eAgInPc2d1	biokompatibilní
keramických	keramický	k2eAgInPc2d1	keramický
materiálů	materiál	k1gInPc2	materiál
apod.	apod.	kA	apod.
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
spekulace	spekulace	k1gFnPc1	spekulace
ohledně	ohledně	k7c2	ohledně
možného	možný	k2eAgInSc2d1	možný
prodeje	prodej	k1gInSc2	prodej
divize	divize	k1gFnSc2	divize
specializující	specializující	k2eAgFnSc2d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
spojení	spojení	k1gNnSc4	spojení
obou	dva	k4xCgFnPc2	dva
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
akcií	akcie	k1gFnPc2	akcie
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
firmy	firma	k1gFnSc2	firma
Pentax	Pentax	k1gInSc1	Pentax
Fumio	Fumio	k6eAd1	Fumio
Urano	Urano	k6eAd1	Urano
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc4	funkce
převzal	převzít	k5eAaPmAgInS	převzít
Takashi	Takashe	k1gFnSc4	Takashe
Watanuki	Watanuki	k1gNnSc2	Watanuki
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Watanuki	Watanuk	k1gFnPc4	Watanuk
dříve	dříve	k6eAd2	dříve
proti	proti	k7c3	proti
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
Hoyou	Hoya	k1gFnSc7	Hoya
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Pentax	Pentax	k1gInSc1	Pentax
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijal	přijmout	k5eAaPmAgMnS	přijmout
"	"	kIx"	"
<g/>
s	s	k7c7	s
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
"	"	kIx"	"
vylepšenou	vylepšený	k2eAgFnSc4d1	vylepšená
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Šestého	šestý	k4xOgInSc2	šestý
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
Hoya	Hoya	k1gFnSc1	Hoya
naplnila	naplnit	k5eAaPmAgFnS	naplnit
veřejný	veřejný	k2eAgInSc4d1	veřejný
tendr	tendr	k1gInSc4	tendr
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
90,59	[number]	k4	90,59
<g/>
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáctého	čtrnáctý	k4xOgInSc2	čtrnáctý
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Pentax	Pentax	k1gInSc4	Pentax
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
Hoyi	Hoy	k1gFnSc2	Hoy
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
pak	pak	k6eAd1	pak
Hoya	Hoy	k2eAgFnSc1d1	Hoya
a	a	k8xC	a
Pentax	Pentax	k1gInSc4	Pentax
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
spojení	spojení	k1gNnSc1	spojení
dokončeno	dokončen	k2eAgNnSc1d1	dokončeno
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
zavření	zavření	k1gNnSc6	zavření
továrny	továrna	k1gFnSc2	továrna
Pentax	Pentax	k1gInSc1	Pentax
v	v	k7c6	v
Tokyu	Tokyum	k1gNnSc6	Tokyum
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
výroby	výroba	k1gFnSc2	výroba
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
profesionální	profesionální	k2eAgInSc4d1	profesionální
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
DA	DA	kA	DA
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
i	i	k9	i
amatérské	amatérský	k2eAgInPc1d1	amatérský
(	(	kIx(	(
<g/>
DA	DA	kA	DA
<g/>
,	,	kIx,	,
D-FA	D-FA	k1gFnSc1	D-FA
<g/>
)	)	kIx)	)
objektivy	objektiv	k1gInPc1	objektiv
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
digitální	digitální	k2eAgFnPc1d1	digitální
zrcadlovky	zrcadlovka	k1gFnPc1	zrcadlovka
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
japonská	japonský	k2eAgFnSc1d1	japonská
společnost	společnost	k1gFnSc1	společnost
Ricoh	Ricoha	k1gFnPc2	Ricoha
<g/>
,	,	kIx,	,
též	též	k9	též
historický	historický	k2eAgMnSc1d1	historický
výrobce	výrobce	k1gMnSc1	výrobce
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
a	a	k8xC	a
fototechniky	fototechnika	k1gFnSc2	fototechnika
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
známý	známý	k1gMnSc1	známý
spíše	spíše	k9	spíše
jako	jako	k9	jako
výrobce	výrobce	k1gMnSc1	výrobce
tiskáren	tiskárna	k1gFnPc2	tiskárna
a	a	k8xC	a
kopírek	kopírka	k1gFnPc2	kopírka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
mimoevropský	mimoevropský	k2eAgInSc4d1	mimoevropský
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
odkoupí	odkoupit	k5eAaPmIp3nP	odkoupit
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Hoya	Hoy	k2eAgFnSc1d1	Hoya
její	její	k3xOp3gFnSc3	její
divizi	divize	k1gFnSc3	divize
Pentax	Pentax	k1gInSc4	Pentax
Imaging	Imaging	k1gInSc1	Imaging
System	Syst	k1gInSc7	Syst
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgInSc2	první
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
převod	převod	k1gInSc1	převod
dokončen	dokončit	k5eAaPmNgInS	dokončit
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
PENTAX	PENTAX	kA	PENTAX
RICOH	RICOH	kA	RICOH
IMAGING	IMAGING	kA	IMAGING
COMPANY	COMPANY	kA	COMPANY
<g/>
,	,	kIx,	,
LTD	ltd	kA	ltd
<g/>
,	,	kIx,	,
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
ve	v	k7c6	v
stoprocentním	stoprocentní	k2eAgNnSc6d1	stoprocentní
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
Ricoh	Ricoha	k1gFnPc2	Ricoha
<g/>
.	.	kIx.	.
</s>
<s>
Ricoh	Ricoh	k1gMnSc1	Ricoh
zároveň	zároveň	k6eAd1	zároveň
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
dále	daleko	k6eAd2	daleko
rozšířit	rozšířit	k5eAaPmF	rozšířit
současnou	současný	k2eAgFnSc4d1	současná
nabídku	nabídka	k1gFnSc4	nabídka
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
nad	nad	k7c4	nad
již	již	k6eAd1	již
existující	existující	k2eAgFnSc4d1	existující
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc4	prodej
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
digitální	digitální	k2eAgInPc4d1	digitální
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
<g/>
,	,	kIx,	,
dalekohledy	dalekohled	k1gInPc4	dalekohled
nebo	nebo	k8xC	nebo
objektivy	objektiv	k1gInPc4	objektiv
pro	pro	k7c4	pro
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
kamery	kamera	k1gFnPc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
historie	historie	k1gFnSc2	historie
obou	dva	k4xCgInPc2	dva
firem	firma	k1gFnPc2	firma
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
Ricohu	Ricoh	k1gInSc2	Ricoh
může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k9	jako
celkem	celkem	k6eAd1	celkem
logický	logický	k2eAgInSc4d1	logický
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
společnosti	společnost	k1gFnPc1	společnost
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
používaly	používat	k5eAaImAgFnP	používat
stejný	stejný	k2eAgInSc4d1	stejný
M42	M42	k1gFnPc6	M42
závit	závit	k1gInSc1	závit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
bajonet	bajonet	k1gInSc1	bajonet
K.	K.	kA	K.
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
netají	tajit	k5eNaImIp3nS	tajit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Hoyi	Hoy	k1gFnSc2	Hoy
hodlá	hodlat	k5eAaImIp3nS	hodlat
do	do	k7c2	do
fotografické	fotografický	k2eAgFnSc2d1	fotografická
divize	divize	k1gFnSc2	divize
investovat	investovat	k5eAaBmF	investovat
nemalé	malý	k2eNgFnPc4d1	nemalá
částky	částka	k1gFnPc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
známa	znám	k2eAgFnSc1d1	známa
díky	díky	k7c3	díky
fotoaparátům	fotoaparát	k1gInPc3	fotoaparát
značky	značka	k1gFnSc2	značka
"	"	kIx"	"
<g/>
Pentax	Pentax	k1gInSc1	Pentax
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zrcadlovkou	zrcadlovka	k1gFnSc7	zrcadlovka
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Asahi	Asah	k1gFnPc4	Asah
Pentax	Pentax	k1gInSc1	Pentax
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhá	druhý	k4xOgFnSc1	druhý
sériově	sériově	k6eAd1	sériově
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
značky	značka	k1gFnSc2	značka
Pentax	Pentax	k1gInSc1	Pentax
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
firma	firma	k1gFnSc1	firma
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Pentax	Pentax	k1gInSc4	Pentax
Corporation	Corporation	k1gInSc1	Corporation
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
firma	firma	k1gFnSc1	firma
velice	velice	k6eAd1	velice
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
a	a	k8xC	a
populární	populární	k2eAgInSc4d1	populární
model	model	k1gInSc4	model
jednooké	jednooký	k2eAgFnSc2d1	jednooká
zrcadlovky	zrcadlovka	k1gFnSc2	zrcadlovka
na	na	k7c4	na
kinofilm	kinofilm	k1gInSc4	kinofilm
Pentax	Pentax	k1gInSc1	Pentax
Spotmatic	Spotmatice	k1gFnPc2	Spotmatice
(	(	kIx(	(
<g/>
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
<g/>
)	)	kIx)	)
s	s	k7c7	s
výměnnými	výměnný	k2eAgInPc7d1	výměnný
objektivy	objektiv	k1gInPc7	objektiv
se	s	k7c7	s
závitem	závit	k1gInSc7	závit
M	M	kA	M
<g/>
42	[number]	k4	42
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
přešla	přejít	k5eAaPmAgFnS	přejít
firma	firma	k1gFnSc1	firma
ze	z	k7c2	z
závitu	závit	k1gInSc2	závit
M42	M42	k1gFnSc2	M42
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
bajonetový	bajonetový	k2eAgInSc4d1	bajonetový
systém	systém	k1gInSc4	systém
výměny	výměna	k1gFnSc2	výměna
objektivů	objektiv	k1gInPc2	objektiv
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xS	jako
bajonet	bajonet	k1gInSc1	bajonet
Pentax	Pentax	k1gInSc1	Pentax
K.	K.	kA	K.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
uvedla	uvést	k5eAaPmAgFnS	uvést
firma	firma	k1gFnSc1	firma
na	na	k7c4	na
trh	trh	k1gInSc4	trh
jednookou	jednooký	k2eAgFnSc4d1	jednooká
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
na	na	k7c4	na
střední	střední	k2eAgInSc4d1	střední
formát	formát	k1gInSc4	formát
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
120	[number]	k4	120
<g/>
/	/	kIx~	/
<g/>
220	[number]	k4	220
<g/>
)	)	kIx)	)
Pentax	Pentax	k1gInSc1	Pentax
6	[number]	k4	6
<g/>
x	x	k?	x
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
úpravami	úprava	k1gFnPc7	úprava
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
spojila	spojit	k5eAaPmAgFnS	spojit
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Hoya	Hoy	k1gInSc2	Hoy
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
nadále	nadále	k6eAd1	nadále
vyvíjeny	vyvíjet	k5eAaImNgInP	vyvíjet
a	a	k8xC	a
prodávány	prodávat	k5eAaImNgInP	prodávat
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Pentax	Pentax	k1gInSc4	Pentax
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
změnit	změnit	k5eAaPmF	změnit
ani	ani	k8xC	ani
akvizice	akvizice	k1gFnSc1	akvizice
společností	společnost	k1gFnPc2	společnost
Ricoh	Ricoha	k1gFnPc2	Ricoha
<g/>
,	,	kIx,	,
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
zůstane	zůstat	k5eAaPmIp3nS	zůstat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
zrcadlovky	zrcadlovka	k1gFnPc4	zrcadlovka
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
pro	pro	k7c4	pro
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
digitální	digitální	k2eAgFnPc4d1	digitální
zrcadlovky	zrcadlovka	k1gFnPc4	zrcadlovka
s	s	k7c7	s
APS-C	APS-C	k1gMnPc7	APS-C
snímači	snímač	k1gInPc7	snímač
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Pentax	Pentax	k1gInSc4	Pentax
K-5	K-5	k1gFnSc2	K-5
nebo	nebo	k8xC	nebo
Pentax	Pentax	k1gInSc4	Pentax
K-	K-	k1gMnPc1	K-
<g/>
r.	r.	kA	r.
Firma	firma	k1gFnSc1	firma
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
také	také	k9	také
jeden	jeden	k4xCgInSc4	jeden
digitální	digitální	k2eAgInSc4d1	digitální
středoformátový	středoformátový	k2eAgInSc4d1	středoformátový
model	model	k1gInSc4	model
s	s	k7c7	s
názvem	název	k1gInSc7	název
645	[number]	k4	645
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
TIPA	TIPA	kA	TIPA
2011	[number]	k4	2011
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
profesionální	profesionální	k2eAgFnSc1d1	profesionální
digitální	digitální	k2eAgFnSc1d1	digitální
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pentax	Pentax	k1gInSc4	Pentax
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
anglické	anglický	k2eAgFnSc2d1	anglická
stránky	stránka	k1gFnSc2	stránka
firmy	firma	k1gFnSc2	firma
Oficiální	oficiální	k2eAgInSc4d1	oficiální
Pentax	Pentax	k1gInSc4	Pentax
fotogalerie	fotogalerie	k1gFnSc2	fotogalerie
Největší	veliký	k2eAgNnSc1d3	veliký
fórum	fórum	k1gNnSc1	fórum
pro	pro	k7c4	pro
česko-slovenské	českolovenský	k2eAgMnPc4d1	česko-slovenský
fanoušky	fanoušek	k1gMnPc4	fanoušek
Pentaxu	Pentax	k1gInSc2	Pentax
Největší	veliký	k2eAgInSc4d3	veliký
Pentax	Pentax	k1gInSc4	Pentax
komunitní	komunitní	k2eAgFnSc2d1	komunitní
stránky	stránka	k1gFnSc2	stránka
včetně	včetně	k7c2	včetně
fóra	fórum	k1gNnSc2	fórum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
českého	český	k2eAgMnSc2d1	český
distributora	distributor	k1gMnSc2	distributor
-	-	kIx~	-
PenTec	PenTec	k1gInSc1	PenTec
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Český	český	k2eAgInSc4d1	český
Klub	klub	k1gInSc4	klub
příznivců	příznivec	k1gMnPc2	příznivec
PENTAXu	PENTAXus	k1gInSc2	PENTAXus
Web	web	k1gInSc4	web
plný	plný	k2eAgInSc4d1	plný
informací	informace	k1gFnSc7	informace
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
odkazů	odkaz	k1gInPc2	odkaz
</s>
