<p>
<s>
Rejsek	rejsek	k1gInSc1	rejsek
horský	horský	k2eAgInSc1d1	horský
(	(	kIx(	(
<g/>
Sorex	Sorex	k1gInSc1	Sorex
alpinus	alpinus	k1gInSc1	alpinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drobným	drobný	k2eAgMnSc7d1	drobný
zástupcem	zástupce	k1gMnSc7	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
rejskovití	rejskovitý	k2eAgMnPc1d1	rejskovitý
(	(	kIx(	(
<g/>
Soricidae	Soricidae	k1gNnSc7	Soricidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývajícím	obývající	k2eAgInSc6d1	obývající
především	především	k9	především
horské	horský	k2eAgFnPc4d1	horská
polohy	poloha	k1gFnPc4	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Hlava	hlava	k1gFnSc1	hlava
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
měří	měřit	k5eAaImIp3nS	měřit
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
mm	mm	kA	mm
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
53	[number]	k4	53
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
mm	mm	kA	mm
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
5-14	[number]	k4	5-14
g.	g.	k?	g.
Tlama	tlama	k1gFnSc1	tlama
se	se	k3xPyFc4	se
dopředu	dopředu	k6eAd1	dopředu
zašpičaťuje	zašpičaťovat	k5eAaImIp3nS	zašpičaťovat
<g/>
.	.	kIx.	.
</s>
<s>
Špičky	špička	k1gFnPc1	špička
zubů	zub	k1gInPc2	zub
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
velkými	velký	k2eAgFnPc7d1	velká
zadními	zadní	k2eAgFnPc7d1	zadní
tlapkami	tlapka	k1gFnPc7	tlapka
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
tělo	tělo	k1gNnSc1	tělo
má	mít	k5eAaImIp3nS	mít
jak	jak	k6eAd1	jak
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
a	a	k8xC	a
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
jednobarevný	jednobarevný	k2eAgInSc4d1	jednobarevný
černošedý	černošedý	k2eAgInSc4d1	černošedý
tón	tón	k1gInSc4	tón
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
našich	naši	k1gMnPc2	naši
rejsků	rejsek	k1gInPc2	rejsek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
rozlišení	rozlišení	k1gNnSc1	rozlišení
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
spodní	spodní	k2eAgFnSc4d1	spodní
stoličku	stolička	k1gFnSc4	stolička
má	mít	k5eAaImIp3nS	mít
dvouhrotovou	dvouhrotový	k2eAgFnSc4d1	dvouhrotový
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
rejsci	rejsek	k1gMnPc1	rejsek
mají	mít	k5eAaImIp3nP	mít
zub	zub	k1gInSc4	zub
jednohrotový	jednohrotový	k2eAgInSc4d1	jednohrotový
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
krátce	krátce	k6eAd1	krátce
osrstěný	osrstěný	k2eAgInSc1d1	osrstěný
<g/>
,	,	kIx,	,
dvoubarevný	dvoubarevný	k2eAgInSc1d1	dvoubarevný
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
stejně	stejně	k6eAd1	stejně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
jako	jako	k8xS	jako
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
světlejší	světlý	k2eAgNnPc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
horské	horský	k2eAgFnPc4d1	horská
oblasti	oblast	k1gFnPc4	oblast
Evropy	Evropa	k1gFnSc2	Evropa
od	od	k7c2	od
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
přes	přes	k7c4	přes
Alpy	Alpy	k1gFnPc4	Alpy
a	a	k8xC	a
pohoří	pohoří	k1gNnSc4	pohoří
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c4	po
Karpaty	Karpaty	k1gInPc4	Karpaty
a	a	k8xC	a
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
mapování	mapování	k1gNnSc6	mapování
výskytu	výskyt	k1gInSc2	výskyt
savců	savec	k1gMnPc2	savec
byl	být	k5eAaImAgInS	být
zjištěn	zjištěn	k2eAgInSc1d1	zjištěn
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
jejím	její	k3xOp3gNnSc6	její
podhůří	podhůří	k1gNnSc6	podhůří
<g/>
,	,	kIx,	,
v	v	k7c6	v
Blanském	blanský	k2eAgInSc6d1	blanský
lese	les	k1gInSc6	les
a	a	k8xC	a
Novohradských	novohradský	k2eAgFnPc6d1	Novohradská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
pak	pak	k6eAd1	pak
od	od	k7c2	od
Lužických	lužický	k2eAgFnPc2d1	Lužická
hor	hora	k1gFnPc2	hora
přes	přes	k7c4	přes
Jizerské	jizerský	k2eAgFnPc4d1	Jizerská
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
Orlické	orlický	k2eAgFnPc1d1	Orlická
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Králický	králický	k2eAgInSc1d1	králický
Sněžník	Sněžník	k1gInSc1	Sněžník
<g/>
,	,	kIx,	,
Hrubý	hrubý	k2eAgInSc1d1	hrubý
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
,	,	kIx,	,
Nízký	nízký	k2eAgInSc1d1	nízký
Jeseník	Jeseník	k1gInSc1	Jeseník
a	a	k8xC	a
Oderské	oderský	k2eAgInPc1d1	oderský
vrchy	vrch	k1gInPc1	vrch
po	po	k7c4	po
Beskydy	Beskydy	k1gFnPc4	Beskydy
<g/>
,	,	kIx,	,
Javorníky	Javorník	k1gInPc4	Javorník
a	a	k8xC	a
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
také	také	k9	také
ve	v	k7c6	v
Žďárských	Žďárských	k2eAgInPc6d1	Žďárských
vrších	vrch	k1gInPc6	vrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
lese	les	k1gInSc6	les
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prostředí	prostředí	k1gNnSc2	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
v	v	k7c6	v
hustých	hustý	k2eAgInPc6d1	hustý
travních	travní	k2eAgInPc6d1	travní
porostech	porost	k1gInPc6	porost
<g/>
,	,	kIx,	,
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
nad	nad	k7c7	nad
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
vlhká	vlhký	k2eAgNnPc4d1	vlhké
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
jehličnaté	jehličnatý	k2eAgInPc4d1	jehličnatý
lesy	les	k1gInPc4	les
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
mezi	mezi	k7c7	mezi
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
2500	[number]	k4	2500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
a	a	k8xC	a
potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
rejsek	rejsek	k1gInSc1	rejsek
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
vrhá	vrhat	k5eAaImIp3nS	vrhat
jednou	jeden	k4xCgFnSc7	jeden
až	až	k6eAd1	až
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
zhruba	zhruba	k6eAd1	zhruba
pět	pět	k4xCc4	pět
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ANDĚRA	ANDĚRA	kA	ANDĚRA
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
HORÁČEK	Horáček	k1gMnSc1	Horáček
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Poznáváme	poznávat	k5eAaImIp1nP	poznávat
naše	náš	k3xOp1gMnPc4	náš
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rejsek	rejsek	k1gMnSc1	rejsek
horský	horský	k2eAgMnSc1d1	horský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Sorex	Sorex	k1gInSc1	Sorex
alpinus	alpinus	k1gInSc1	alpinus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Rejsek	rejsek	k1gInSc4	rejsek
horský	horský	k2eAgInSc4d1	horský
na	na	k7c4	na
BioLib	BioLib	k1gInSc4	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
