<s>
Jindřich	Jindřich	k1gMnSc1
Bláha	Bláha	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Jindřich	Jindřich	k1gMnSc1
Bláha	Bláha	k1gMnSc1
</s>
<s>
Zastupitel	zastupitel	k1gMnSc1
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2004	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
</s>
<s>
Starosta	Starosta	k1gMnSc1
města	město	k1gNnSc2
Soběslav	Soběslav	k1gMnSc1
Úřadující	úřadující	k2eAgMnSc1d1
</s>
<s>
Ve	v	k7c4
funkci	funkce	k1gFnSc4
od	od	k7c2
<g/>
:	:	kIx,
<g/>
1994	#num#	k4
</s>
<s>
Zastupitel	zastupitel	k1gMnSc1
města	město	k1gNnSc2
Soběslav	Soběslav	k1gFnSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
letech	let	k1gInPc6
1993	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
také	také	k9
místostarosta	místostarosta	k1gMnSc1
města	město	k1gNnSc2
<g/>
)	)	kIx)
Úřadující	úřadující	k2eAgInSc1d1
</s>
<s>
Ve	v	k7c4
funkci	funkce	k1gFnSc4
od	od	k7c2
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
ODS	ODS	kA
(	(	kIx(
<g/>
od	od	k7c2
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nestraník	nestraník	k1gMnSc1
v	v	k7c6
zastupitelstvu	zastupitelstvo	k1gNnSc6
</s>
<s>
nezávislý	závislý	k2eNgMnSc1d1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1959	#num#	k4
(	(	kIx(
<g/>
61	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
TáborČeskoslovensko	TáborČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Choť	choť	k1gFnSc4
</s>
<s>
Romana	Romana	k1gFnSc1
Bláhová	bláhový	k2eAgFnSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
syn	syn	k1gMnSc1
Jiří	Jiří	k1gMnSc1
a	a	k8xC
dcera	dcera	k1gFnSc1
Hana	Hana	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Bláha	Bláha	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
28	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1959	#num#	k4
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
až	až	k9
2008	#num#	k4
zastupitel	zastupitel	k1gMnSc1
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
starosta	starosta	k1gMnSc1
města	město	k1gNnSc2
Soběslavi	Soběslav	k1gFnSc2
na	na	k7c6
Táborsku	Táborsko	k1gNnSc6
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
ODS	ODS	kA
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Od	od	k7c2
dětství	dětství	k1gNnSc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
Soběslavi	Soběslav	k1gFnSc6
<g/>
,	,	kIx,
maturitní	maturitní	k2eAgFnSc4d1
zkoušku	zkouška	k1gFnSc4
složil	složit	k5eAaPmAgMnS
na	na	k7c6
Střední	střední	k2eAgFnSc6d1
průmyslové	průmyslový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
vystudoval	vystudovat	k5eAaPmAgMnS
České	český	k2eAgFnSc3d1
vysoké	vysoká	k1gFnSc3
učení	učení	k1gNnSc2
technické	technický	k2eAgInPc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
elektronické	elektronický	k2eAgInPc4d1
počítače	počítač	k1gInPc4
(	(	kIx(
<g/>
promoval	promovat	k5eAaBmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
a	a	k8xC
získala	získat	k5eAaPmAgFnS
titul	titul	k1gInSc4
Ing.	ing.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roční	roční	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
službě	služba	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
podniku	podnik	k1gInSc2
Elitex	Elitex	k1gInSc1
Soběslav	Soběslav	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Lada	Lada	k1gFnSc1
Soběslav	Soběslav	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vyráběl	vyrábět	k5eAaImAgMnS
šicí	šicí	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podniku	podnik	k1gInSc6
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
technik	technik	k1gMnSc1
<g/>
,	,	kIx,
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
v	v	k7c6
posledním	poslední	k2eAgInSc6d1
roce	rok	k1gInSc6
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
i	i	k9
jako	jako	k8xS,k8xC
ředitel	ředitel	k1gMnSc1
marketingu	marketing	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Bláha	Bláha	k1gMnSc1
žije	žít	k5eAaImIp3nS
v	v	k7c6
Soběslavi	Soběslav	k1gFnSc6
na	na	k7c4
Táborsku	Táborska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
Romana	Romana	k1gFnSc1
je	být	k5eAaImIp3nS
učitelkou	učitelka	k1gFnSc7
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
stupni	stupeň	k1gInPc7
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Soběslavi	Soběslav	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
spolu	spolu	k6eAd1
dvě	dva	k4xCgFnPc1
děti	dítě	k1gFnPc1
–	–	k?
syna	syn	k1gMnSc4
Jiřího	Jiří	k1gMnSc4
a	a	k8xC
dceru	dcera	k1gFnSc4
Hanu	Hana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volném	volný	k2eAgInSc6d1
čase	čas	k1gInSc6
sportuje	sportovat	k5eAaImIp3nS
(	(	kIx(
<g/>
tenis	tenis	k1gInSc4
a	a	k8xC
fotbal	fotbal	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rád	rád	k6eAd1
čte	číst	k5eAaImIp3nS
a	a	k8xC
navštěvuje	navštěvovat	k5eAaImIp3nS
kulturní	kulturní	k2eAgFnPc4d1
akce	akce	k1gFnPc4
(	(	kIx(
<g/>
koncerty	koncert	k1gInPc4
a	a	k8xC
divadla	divadlo	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politické	politický	k2eAgNnSc1d1
působení	působení	k1gNnSc1
</s>
<s>
Do	do	k7c2
komunální	komunální	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
vstoupil	vstoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgInS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
zvolen	zvolit	k5eAaPmNgInS
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
města	město	k1gNnSc2
Soběslavi	Soběslav	k1gFnSc2
na	na	k7c6
Táborsku	Táborsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
místostarostou	místostarosta	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
mandát	mandát	k1gInSc1
zastupitele	zastupitel	k1gMnSc4
obhájil	obhájit	k5eAaPmAgInS
jako	jako	k8xS,k8xC
člen	člen	k1gInSc1
ODS	ODS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
starostou	starosta	k1gMnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnSc1
zastupitele	zastupitel	k1gMnSc4
i	i	k9
starosta	starosta	k1gMnSc1
pak	pak	k6eAd1
obhájil	obhájit	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1998	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
a	a	k8xC
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
krajských	krajský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgInS
za	za	k7c4
ODS	ODS	kA
zvolen	zvolit	k5eAaPmNgMnS
zastupitelem	zastupitel	k1gMnSc7
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
mandát	mandát	k1gInSc4
nepodařilo	podařit	k5eNaPmAgNnS
obhájit	obhájit	k5eAaPmF
(	(	kIx(
<g/>
skončil	skončit	k5eAaPmAgMnS
jako	jako	k9
druhý	druhý	k4xOgMnSc1
náhradník	náhradník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Neuspěl	uspět	k5eNaPmAgMnS
ani	ani	k8xC
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
PČR	PČR	kA
v	v	k7c6
letech	let	k1gInPc6
2006	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
a	a	k8xC
2013	#num#	k4
kandidoval	kandidovat	k5eAaImAgInS
za	za	k7c4
ODS	ODS	kA
v	v	k7c6
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
jednou	jednou	k6eAd1
neuspěl	uspět	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Senátu	senát	k1gInSc2
PČR	PČR	kA
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
kandidoval	kandidovat	k5eAaImAgMnS
za	za	k7c2
ODS	ODS	kA
v	v	k7c6
obvodu	obvod	k1gInSc6
č.	č.	k?
13	#num#	k4
–	–	k?
Tábor	Tábor	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Se	s	k7c7
ziskem	zisk	k1gInSc7
14,34	14,34	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
skončil	skončit	k5eAaPmAgInS
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
do	do	k7c2
druhého	druhý	k4xOgNnSc2
kola	kolo	k1gNnSc2
nepostoupil	postoupit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ing.	ing.	kA
Jindřich	Jindřich	k1gMnSc1
Bláha	Bláha	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Detail	detail	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ing.	ing.	kA
Jindřich	Jindřich	k1gMnSc1
Bláha	Bláha	k1gMnSc1
<g/>
,	,	kIx,
kandidát	kandidát	k1gMnSc1
do	do	k7c2
senátu	senát	k1gInSc2
za	za	k7c4
ODS	ODS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ODS	ODS	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
18.11	18.11	k4
<g/>
.	.	kIx.
-	-	kIx~
19.11	19.11	k4
<g/>
.1994	.1994	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Tábor	Tábor	k1gInSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Soběslav	Soběslav	k1gFnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Tábor	Tábor	k1gInSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Soběslav	Soběslav	k1gFnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Tábor	Tábor	k1gInSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Soběslav	Soběslav	k1gFnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Tábor	Tábor	k1gInSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Soběslav	Soběslav	k1gFnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Tábor	Tábor	k1gInSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Soběslav	Soběslav	k1gFnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
<g/>
:	:	kIx,
Tábor	Tábor	k1gInSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Soběslav	Soběslav	k1gFnSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Sdružení	sdružení	k1gNnSc1
ODS	ODS	kA
a	a	k8xC
NK	NK	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
6.11	6.11	k4
<g/>
.2004	.2004	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Strana	strana	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
17.10	17.10	k4
<g/>
.	.	kIx.
–	–	k?
18.10	18.10	k4
<g/>
.2008	.2008	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
12.10	12.10	k4
<g/>
.	.	kIx.
–	–	k?
13.10	13.10	k4
<g/>
.2012	.2012	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
0	#num#	k4
<g/>
2.06	2.06	k4
<g/>
.	.	kIx.
–	–	k?
0	#num#	k4
<g/>
3.06	3.06	k4
<g/>
.2006	.2006	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Strana	strana	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
28.05	28.05	k4
<g/>
.	.	kIx.
–	–	k?
29.05	29.05	k4
<g/>
.2010	.2010	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Strana	strana	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnPc4d1
republiky	republika	k1gFnPc4
konané	konaný	k2eAgFnPc4d1
ve	v	k7c6
dnech	den	k1gInPc6
25.10	25.10	k4
<g/>
.	.	kIx.
–	–	k?
26.10	26.10	k4
<g/>
.2013	.2013	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Strana	strana	k1gFnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc1
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kandidáti	kandidát	k1gMnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ODS	ODS	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výsledky	výsledek	k1gInPc1
voleb	volba	k1gFnPc2
v	v	k7c6
obvodě	obvod	k1gInSc6
Tábor	Tábor	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
166047	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
296785362	#num#	k4
</s>
