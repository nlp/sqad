<s>
Název	název	k1gInSc1	název
Žižkův	Žižkův	k2eAgInSc4d1	Žižkův
buk	buk	k1gInSc4	buk
nese	nést	k5eAaImIp3nS	nést
několik	několik	k4yIc4	několik
významných	významný	k2eAgInPc2d1	významný
a	a	k8xC	a
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
spjatých	spjatý	k2eAgInPc2d1	spjatý
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
již	již	k6eAd1	již
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
nebo	nebo	k8xC	nebo
historických	historický	k2eAgInPc2d1	historický
záznamů	záznam	k1gInPc2	záznam
<g/>
)	)	kIx)	)
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
:	:	kIx,	:
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
buk	buk	k1gInSc1	buk
(	(	kIx(	(
<g/>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
–	–	k?	–
okres	okres	k1gInSc1	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Malčín	Malčína	k1gFnPc2	Malčína
<g/>
)	)	kIx)	)
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
buk	buk	k1gInSc1	buk
(	(	kIx(	(
<g/>
Libáň	Libáň	k1gFnSc1	Libáň
<g/>
)	)	kIx)	)
–	–	k?	–
okres	okres	k1gInSc1	okres
Jičín	Jičín	k1gInSc1	Jičín
(	(	kIx(	(
<g/>
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
