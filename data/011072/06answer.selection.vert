<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Radiščev	Radiščev	k1gMnSc1	Radiščev
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
А	А	k?	А
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1749	[number]	k4	1749
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
myslitel	myslitel	k1gMnSc1	myslitel
a	a	k8xC	a
revolucionář	revolucionář	k1gMnSc1	revolucionář
období	období	k1gNnSc2	období
osvícenství	osvícenství	k1gNnSc2	osvícenství
a	a	k8xC	a
preromantismu	preromantismus	k1gInSc2	preromantismus
<g/>
.	.	kIx.	.
</s>
