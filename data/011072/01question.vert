<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
ruský	ruský	k2eAgMnSc1d1	ruský
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
myslitel	myslitel	k1gMnSc1	myslitel
a	a	k8xC	a
revolucionář	revolucionář	k1gMnSc1	revolucionář
období	období	k1gNnSc2	období
osvícenství	osvícenství	k1gNnSc2	osvícenství
a	a	k8xC	a
preromantismu	preromantismus	k1gInSc2	preromantismus
<g/>
?	?	kIx.	?
</s>
