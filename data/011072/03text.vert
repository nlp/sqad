<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Radiščev	Radiščev	k1gMnSc1	Radiščev
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
А	А	k?	А
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1749	[number]	k4	1749
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
myslitel	myslitel	k1gMnSc1	myslitel
a	a	k8xC	a
revolucionář	revolucionář	k1gMnSc1	revolucionář
období	období	k1gNnSc2	období
osvícenství	osvícenství	k1gNnSc2	osvícenství
a	a	k8xC	a
preromantismu	preromantismus	k1gInSc2	preromantismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Radiščev	Radiščet	k5eAaPmDgInS	Radiščet
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
v	v	k7c6	v
zámožné	zámožný	k2eAgFnSc6d1	zámožná
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
kadetské	kadetský	k2eAgFnSc6d1	kadetská
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
absolvování	absolvování	k1gNnSc6	absolvování
jej	on	k3xPp3gMnSc4	on
carevna	carevna	k1gFnSc1	carevna
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
vyslala	vyslat	k5eAaPmAgFnS	vyslat
roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
mladíky	mladík	k1gMnPc7	mladík
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
věnoval	věnovat	k5eAaPmAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vzdělání	vzdělání	k1gNnSc1	vzdělání
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
skvělé	skvělý	k2eAgFnSc2d1	skvělá
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
ho	on	k3xPp3gMnSc4	on
oslovily	oslovit	k5eAaPmAgFnP	oslovit
zejména	zejména	k9	zejména
osvícenské	osvícenský	k2eAgFnPc1d1	osvícenská
myšlenky	myšlenka	k1gFnPc1	myšlenka
francouzských	francouzský	k2eAgMnPc2d1	francouzský
filozofů	filozof	k1gMnPc2	filozof
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
stalo	stát	k5eAaPmAgNnS	stát
osudným	osudný	k2eAgInSc7d1	osudný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Lipska	Lipsko	k1gNnSc2	Lipsko
roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Radiščev	Radiščev	k1gMnSc1	Radiščev
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
svojí	svůj	k3xOyFgFnSc7	svůj
nestranností	nestrannost	k1gFnSc7	nestrannost
a	a	k8xC	a
mírností	mírnost	k1gFnSc7	mírnost
vůči	vůči	k7c3	vůči
podřízeným	podřízený	k2eAgMnPc3d1	podřízený
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
sloužil	sloužit	k5eAaImAgInS	sloužit
ve	v	k7c6	v
vojsku	vojsko	k1gNnSc6	vojsko
jako	jako	k8xS	jako
vojenský	vojenský	k2eAgMnSc1d1	vojenský
prokurátor	prokurátor	k1gMnSc1	prokurátor
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
petrohradské	petrohradský	k2eAgFnSc2d1	Petrohradská
celnice	celnice	k1gFnSc2	celnice
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
přitom	přitom	k6eAd1	přitom
poznal	poznat	k5eAaPmAgMnS	poznat
nevolnické	nevolnický	k2eAgInPc4d1	nevolnický
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
osudy	osud	k1gInPc4	osud
vojenských	vojenský	k2eAgMnPc2d1	vojenský
zběhů	zběh	k1gMnPc2	zběh
i	i	k8xC	i
problematiku	problematika	k1gFnSc4	problematika
kupectva	kupectvo	k1gNnSc2	kupectvo
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
rozpor	rozpor	k1gInSc4	rozpor
mezi	mezi	k7c7	mezi
carevnou	carevna	k1gFnSc7	carevna
hlásaným	hlásaný	k2eAgNnSc7d1	hlásané
osvícenstvím	osvícenství	k1gNnSc7	osvícenství
a	a	k8xC	a
realitou	realita	k1gFnSc7	realita
ruského	ruský	k2eAgInSc2d1	ruský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgNnSc7d1	jediné
východiskem	východisko	k1gNnSc7	východisko
je	být	k5eAaImIp3nS	být
zrušení	zrušení	k1gNnSc1	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
zabývat	zabývat	k5eAaImF	zabývat
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
prvními	první	k4xOgFnPc7	první
důležitějšími	důležitý	k2eAgFnPc7d2	důležitější
pracemi	práce	k1gFnPc7	práce
byla	být	k5eAaImAgFnS	být
óda	óda	k1gFnSc1	óda
Volnost	volnost	k1gFnSc1	volnost
(	(	kIx(	(
<g/>
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
В	В	k?	В
<g/>
)	)	kIx)	)
a	a	k8xC	a
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
próza	próza	k1gFnSc1	próza
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
z	z	k7c2	z
Lipska	Lipsko	k1gNnSc2	Lipsko
Život	život	k1gInSc1	život
Fjodora	Fjodor	k1gMnSc2	Fjodor
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
Ušakova	Ušakův	k2eAgMnSc2d1	Ušakův
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
Ж	Ж	k?	Ж
Ф	Ф	k?	Ф
В	В	k?	В
У	У	k?	У
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
si	se	k3xPyFc3	se
Radiščev	Radiščev	k1gMnSc1	Radiščev
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nejprve	nejprve	k6eAd1	nejprve
anonymně	anonymně	k6eAd1	anonymně
vydal	vydat	k5eAaPmAgInS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
Dopis	dopis	k1gInSc4	dopis
příteli	přítel	k1gMnSc3	přítel
žijícímu	žijící	k2eAgMnSc3d1	žijící
v	v	k7c6	v
Tobolsku	Tobolsek	k1gInSc2	Tobolsek
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
,	,	kIx,	,
П	П	k?	П
к	к	k?	к
д	д	k?	д
<g/>
,	,	kIx,	,
ж	ж	k?	ж
в	в	k?	в
Т	Т	k?	Т
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
napsal	napsat	k5eAaBmAgMnS	napsat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
zásluhy	zásluha	k1gFnSc2	zásluha
cara	car	k1gMnSc2	car
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
své	svůj	k3xOyFgNnSc4	svůj
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
intelektuální	intelektuální	k2eAgInSc4d1	intelektuální
cestopis	cestopis	k1gInSc4	cestopis
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
П	П	k?	П
и	и	k?	и
П	П	k?	П
в	в	k?	в
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
smělé	smělý	k2eAgFnPc1d1	smělá
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
nevolnictví	nevolnictví	k1gNnSc6	nevolnictví
<g/>
,	,	kIx,	,
samoděržaví	samoděržaví	k1gNnSc6	samoděržaví
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
jevech	jev	k1gInPc6	jev
společenského	společenský	k2eAgMnSc2d1	společenský
i	i	k8xC	i
státního	státní	k2eAgInSc2d1	státní
života	život	k1gInSc2	život
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
měla	mít	k5eAaImAgFnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
knihu	kniha	k1gFnSc4	kniha
přečetla	přečíst	k5eAaPmAgFnS	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jí	jíst	k5eAaImIp3nS	jíst
tak	tak	k6eAd1	tak
pobouřena	pobouřen	k2eAgFnSc1d1	pobouřena
<g/>
,	,	kIx,	,
že	že	k8xS	že
přikázala	přikázat	k5eAaPmAgFnS	přikázat
náklad	náklad	k1gInSc4	náklad
zabavit	zabavit	k5eAaPmF	zabavit
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
a	a	k8xC	a
proti	proti	k7c3	proti
Radiščevovi	Radiščeva	k1gMnSc3	Radiščeva
zahájit	zahájit	k5eAaPmF	zahájit
trestní	trestní	k2eAgNnSc4d1	trestní
stíhání	stíhání	k1gNnSc4	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Radiščev	Radiščet	k5eAaPmDgInS	Radiščet
byl	být	k5eAaImAgMnS	být
brzy	brzy	k6eAd1	brzy
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
následně	následně	k6eAd1	následně
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
trest	trest	k1gInSc4	trest
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
milostí	milost	k1gFnPc2	milost
carevny	carevna	k1gFnSc2	carevna
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
desetileté	desetiletý	k2eAgNnSc4d1	desetileté
vyhnanství	vyhnanství	k1gNnSc4	vyhnanství
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
v	v	k7c6	v
Ilimsku	Ilimsek	k1gInSc6	Ilimsek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgMnS	napsat
filozofický	filozofický	k2eAgInSc4d1	filozofický
traktát	traktát	k1gInSc4	traktát
O	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrtelnosti	smrtelnost	k1gFnSc6	smrtelnost
a	a	k8xC	a
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
–	–	k?	–
<g/>
1795	[number]	k4	1795
<g/>
,	,	kIx,	,
О	О	k?	О
ч	ч	k?	ч
<g/>
,	,	kIx,	,
о	о	k?	о
е	е	k?	е
с	с	k?	с
и	и	k?	и
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
však	však	k9	však
také	také	k9	také
zabaven	zabavit	k5eAaPmNgInS	zabavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krutý	krutý	k2eAgInSc1d1	krutý
Radiščevův	Radiščevův	k2eAgInSc1d1	Radiščevův
osud	osud	k1gInSc1	osud
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
účast	účast	k1gFnSc4	účast
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
již	již	k9	již
roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
mu	on	k3xPp3gMnSc3	on
nový	nový	k2eAgMnSc1d1	nový
car	car	k1gMnSc1	car
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
dovolil	dovolit	k5eAaPmAgMnS	dovolit
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
policejním	policejní	k2eAgInSc7d1	policejní
dozorem	dozor	k1gInSc7	dozor
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
statku	statek	k1gInSc6	statek
v	v	k7c6	v
kalužské	kalužský	k2eAgFnSc6d1	kalužský
gubernii	gubernie	k1gFnSc6	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Plné	plný	k2eAgFnSc2d1	plná
svobody	svoboda	k1gFnSc2	svoboda
však	však	k9	však
Radiščev	Radiščev	k1gMnSc1	Radiščev
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
teprve	teprve	k6eAd1	teprve
až	až	k9	až
za	za	k7c4	za
dalšího	další	k2eAgMnSc4d1	další
cara	car	k1gMnSc4	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
opět	opět	k6eAd1	opět
přijal	přijmout	k5eAaPmAgMnS	přijmout
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
jej	on	k3xPp3gInSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
do	do	k7c2	do
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
sestavování	sestavování	k1gNnSc4	sestavování
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
Radiščev	Radiščev	k1gFnSc1	Radiščev
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
projektech	projekt	k1gInPc6	projekt
právních	právní	k2eAgFnPc2d1	právní
reforem	reforma	k1gFnPc2	reforma
opět	opět	k6eAd1	opět
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
proti	proti	k7c3	proti
nevolnictví	nevolnictví	k1gNnSc3	nevolnictví
<g/>
,	,	kIx,	,
přivedla	přivést	k5eAaPmAgFnS	přivést
jej	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
hrozba	hrozba	k1gFnSc1	hrozba
nového	nový	k2eAgNnSc2d1	nové
pronásledování	pronásledování	k1gNnSc2	pronásledování
k	k	k7c3	k
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
a	a	k8xC	a
literárně-kritické	literárněritický	k2eAgFnSc2d1	literárně-kritická
práce	práce	k1gFnSc2	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
řecké	řecký	k2eAgFnSc6d1	řecká
historii	historie	k1gFnSc6	historie
(	(	kIx(	(
<g/>
Р	Р	k?	Р
о	о	k?	о
г	г	k?	г
и	и	k?	и
<g/>
,	,	kIx,	,
1771	[number]	k4	1771
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
knihy	kniha	k1gFnSc2	kniha
francouzského	francouzský	k2eAgMnSc2d1	francouzský
filozofa	filozof	k1gMnSc2	filozof
Gabriela	Gabriela	k1gFnSc1	Gabriela
Bonnota	Bonnota	k1gFnSc1	Bonnota
de	de	k?	de
Mablyho	Mably	k1gMnSc2	Mably
doplněný	doplněný	k2eAgInSc4d1	doplněný
o	o	k7c4	o
Radiščevovy	Radiščevův	k2eAgFnPc4d1	Radiščevův
vlastní	vlastní	k2eAgFnPc4d1	vlastní
společensky	společensky	k6eAd1	společensky
kritické	kritický	k2eAgFnPc4d1	kritická
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
o	o	k7c6	o
Lomonosovovi	Lomonosova	k1gMnSc6	Lomonosova
(	(	kIx(	(
<g/>
С	С	k?	С
о	о	k?	о
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofická	filozofický	k2eAgFnSc1d1	filozofická
stať	stať	k1gFnSc1	stať
zařazená	zařazený	k2eAgFnSc1d1	zařazená
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
autorovy	autorův	k2eAgFnSc2d1	autorova
knihy	kniha	k1gFnSc2	kniha
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Beseda	beseda	k1gFnSc1	beseda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
vlasti	vlast	k1gFnSc2	vlast
(	(	kIx(	(
<g/>
Б	Б	k?	Б
о	о	k?	о
т	т	k?	т
<g/>
,	,	kIx,	,
ч	ч	k?	ч
е	е	k?	е
с	с	k?	с
О	О	k?	О
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofická	filozofický	k2eAgFnSc1d1	filozofická
stať	stať	k1gFnSc1	stať
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
O	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrtelnosti	smrtelnost	k1gFnSc6	smrtelnost
a	a	k8xC	a
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
(	(	kIx(	(
<g/>
О	О	k?	О
ч	ч	k?	ч
<g/>
,	,	kIx,	,
о	о	k?	о
е	е	k?	е
с	с	k?	с
и	и	k?	и
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofický	filozofický	k2eAgInSc4d1	filozofický
traktát	traktát	k1gInSc4	traktát
napsáný	napsáný	k2eAgInSc4d1	napsáný
v	v	k7c6	v
letech	let	k1gInPc6	let
1792	[number]	k4	1792
<g/>
–	–	k?	–
<g/>
1795	[number]	k4	1795
v	v	k7c6	v
sibiřském	sibiřský	k2eAgNnSc6d1	sibiřské
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
a	a	k8xC	a
vydaný	vydaný	k2eAgInSc4d1	vydaný
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Radiščev	Radiščev	k1gFnSc1	Radiščev
zabývá	zabývat	k5eAaImIp3nS	zabývat
vývojem	vývoj	k1gInSc7	vývoj
člověka	člověk	k1gMnSc2	člověk
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
říši	říš	k1gFnSc3	říš
živočišné	živočišný	k2eAgFnSc3d1	živočišná
i	i	k8xC	i
rostlinné	rostlinný	k2eAgFnSc3d1	rostlinná
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
pohyb	pohyb	k1gInSc4	pohyb
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
otázku	otázka	k1gFnSc4	otázka
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
i	i	k9	i
otázku	otázka	k1gFnSc4	otázka
smrtelnosti	smrtelnost	k1gFnSc3	smrtelnost
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
duše	duše	k1gFnSc2	duše
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
nakonec	nakonec	k6eAd1	nakonec
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
duše	duše	k1gFnSc2	duše
je	být	k5eAaImIp3nS	být
vědecky	vědecky	k6eAd1	vědecky
nedokazatelná	dokazatelný	k2eNgFnSc1d1	nedokazatelná
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
v	v	k7c4	v
ni	on	k3xPp3gFnSc4	on
pouze	pouze	k6eAd1	pouze
věřit	věřit	k5eAaImF	věřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc4	pomník
daktylotrochejskému	daktylotrochejský	k2eAgMnSc3d1	daktylotrochejský
bohatýrovi	bohatýr	k1gMnSc3	bohatýr
(	(	kIx(	(
<g/>
П	П	k?	П
д	д	k?	д
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literárně-kritické	literárněritický	k2eAgNnSc1d1	literárně-kritický
dílo	dílo	k1gNnSc1	dílo
napsané	napsaný	k2eAgNnSc1d1	napsané
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
a	a	k8xC	a
vydané	vydaný	k2eAgNnSc4d1	vydané
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1811	[number]	k4	1811
<g/>
,	,	kIx,	,
shrnující	shrnující	k2eAgInPc4d1	shrnující
Radiščevovy	Radiščevův	k2eAgInPc4d1	Radiščevův
názory	názor	k1gInPc4	názor
na	na	k7c4	na
básnictví	básnictví	k1gNnSc4	básnictví
(	(	kIx(	(
<g/>
inspirace	inspirace	k1gFnSc1	inspirace
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
poezii	poezie	k1gFnSc6	poezie
a	a	k8xC	a
v	v	k7c6	v
hrdinských	hrdinský	k2eAgInPc6d1	hrdinský
obrazech	obraz	k1gInPc6	obraz
ze	z	k7c2	z
staroruské	staroruský	k2eAgFnSc2d1	staroruská
minulosti	minulost	k1gFnSc2	minulost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poezie	poezie	k1gFnSc2	poezie
===	===	k?	===
</s>
</p>
<p>
<s>
Stvoření	stvoření	k1gNnSc1	stvoření
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Т	Т	k?	Т
м	м	k?	м
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1779	[number]	k4	1779
až	až	k9	až
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Volnost	volnost	k1gFnSc1	volnost
(	(	kIx(	(
<g/>
В	В	k?	В
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
óda	óda	k1gFnSc1	óda
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
podávající	podávající	k2eAgFnSc6d1	podávající
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
historii	historie	k1gFnSc4	historie
lidského	lidský	k2eAgInSc2d1	lidský
zápasu	zápas	k1gInSc2	zápas
za	za	k7c4	za
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
v	v	k7c6	v
autorově	autorův	k2eAgFnSc6d1	autorova
knize	kniha	k1gFnSc6	kniha
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
vydána	vydán	k2eAgFnSc1d1	vydána
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
menší	malý	k2eAgFnPc4d2	menší
básně	báseň	k1gFnPc4	báseň
Chceš	chtít	k5eAaImIp2nS	chtít
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jsem	být	k5eAaImIp1nS	být
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Т	Т	k?	Т
х	х	k?	х
з	з	k?	з
к	к	k?	к
я	я	k?	я
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
publikováno	publikovat	k5eAaBmNgNnS	publikovat
až	až	k6eAd1	až
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
a	a	k8xC	a
Modlitba	modlitba	k1gFnSc1	modlitba
(	(	kIx(	(
<g/>
М	М	k?	М
<g/>
,	,	kIx,	,
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bova	Bova	k1gFnSc1	Bova
(	(	kIx(	(
<g/>
Б	Б	k?	Б
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaBmNgNnS	napsat
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k6eAd1	až
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
bohatýrská	bohatýrský	k2eAgFnSc1d1	bohatýrská
zkazka	zkazka	k1gFnSc1	zkazka
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
menší	malý	k2eAgFnPc1d2	menší
básně	báseň	k1gFnPc1	báseň
Jeřábi	jeřáb	k1gMnPc1	jeřáb
(	(	kIx(	(
<g/>
Ж	Ж	k?	Ж
<g/>
,	,	kIx,	,
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Idyla	idyla	k1gFnSc1	idyla
(	(	kIx(	(
<g/>
И	И	k?	И
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Óda	óda	k1gFnSc1	óda
na	na	k7c4	na
mého	můj	k3xOp1gMnSc4	můj
přítele	přítel	k1gMnSc4	přítel
(	(	kIx(	(
<g/>
О	О	k?	О
к	к	k?	к
д	д	k?	д
м	м	k?	м
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sapfické	sapfický	k2eAgFnPc1d1	sapfická
sloky	sloka	k1gFnPc1	sloka
(	(	kIx(	(
<g/>
С	С	k?	С
с	с	k?	с
<g/>
,	,	kIx,	,
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osmnácté	osmnáctý	k4xOgNnSc1	osmnáctý
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
О	О	k?	О
с	с	k?	с
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaBmNgNnS	napsat
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
rozčarování	rozčarování	k1gNnSc1	rozčarování
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
revolučního	revoluční	k2eAgNnSc2d1	revoluční
dění	dění	k1gNnSc2	dění
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
zpívané	zpívaný	k2eAgFnPc1d1	zpívaná
o	o	k7c6	o
závodech	závod	k1gInPc6	závod
pro	pro	k7c4	pro
čest	čest	k1gFnSc4	čest
starých	stará	k1gFnPc2	stará
slovanských	slovanský	k2eAgMnPc2d1	slovanský
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
,	,	kIx,	,
п	п	k?	п
н	н	k?	н
с	с	k?	с
в	в	k?	в
ч	ч	k?	ч
д	д	k?	д
с	с	k?	с
б	б	k?	б
<g/>
,	,	kIx,	,
nedokončeno	dokončit	k5eNaPmNgNnS	dokončit
<g/>
,	,	kIx,	,
publikováno	publikovat	k5eAaBmNgNnS	publikovat
až	až	k6eAd1	až
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gNnSc1	poema
útočící	útočící	k2eAgNnSc1d1	útočící
proti	proti	k7c3	proti
únikovému	únikový	k2eAgInSc3d1	únikový
sentimentalismu	sentimentalismus	k1gInSc3	sentimentalismus
i	i	k9	i
proti	proti	k7c3	proti
lživědeckým	lživědecký	k2eAgInPc3d1	lživědecký
názorům	názor	k1gInPc3	názor
o	o	k7c6	o
historické	historický	k2eAgFnSc6d1	historická
úloze	úloha	k1gFnSc6	úloha
Slovanstva	Slovanstvo	k1gNnSc2	Slovanstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
píseň	píseň	k1gFnSc1	píseň
(	(	kIx(	(
<g/>
П	П	k?	П
и	и	k?	и
<g/>
,	,	kIx,	,
publikováno	publikovat	k5eAaBmNgNnS	publikovat
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
Próza	próza	k1gFnSc1	próza
===	===	k?	===
</s>
</p>
<p>
<s>
Deník	deník	k1gInSc1	deník
jednoho	jeden	k4xCgInSc2	jeden
týdne	týden	k1gInSc2	týden
(	(	kIx(	(
<g/>
Д	Д	k?	Д
о	о	k?	о
н	н	k?	н
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
1773	[number]	k4	1773
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k6eAd1	až
1811	[number]	k4	1811
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
sentimentálních	sentimentální	k2eAgFnPc2d1	sentimentální
próz	próza	k1gFnPc2	próza
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
kritiku	kritika	k1gFnSc4	kritika
Jeana-Jacquese	Jeana-Jacquese	k1gFnSc2	Jeana-Jacquese
Rousseaua	Rousseau	k1gMnSc2	Rousseau
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
Fjodora	Fjodor	k1gMnSc2	Fjodor
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
Ušakova	Ušakův	k2eAgMnSc2d1	Ušakův
(	(	kIx(	(
<g/>
Ж	Ж	k?	Ж
Ф	Ф	k?	Ф
В	В	k?	В
У	У	k?	У
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
povídka	povídka	k1gFnSc1	povídka
líčící	líčící	k2eAgFnSc2d1	líčící
pětiletý	pětiletý	k2eAgInSc4d1	pětiletý
pobyt	pobyt	k1gInSc4	pobyt
skupiny	skupina	k1gFnSc2	skupina
ruských	ruský	k2eAgMnPc2d1	ruský
studentů	student	k1gMnPc2	student
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
Radiščevův	Radiščevův	k2eAgMnSc1d1	Radiščevův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
nadějný	nadějný	k2eAgMnSc1d1	nadějný
filozof	filozof	k1gMnSc1	filozof
F.	F.	kA	F.
V.	V.	kA	V.
Ušakov	Ušakov	k1gInSc4	Ušakov
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gNnSc7	jehož
vedením	vedení	k1gNnSc7	vedení
poznává	poznávat	k5eAaImIp3nS	poznávat
Radiščev	Radiščev	k1gFnSc1	Radiščev
francouzské	francouzský	k2eAgMnPc4d1	francouzský
osvícence	osvícenec	k1gMnPc4	osvícenec
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
touha	touha	k1gFnSc1	touha
změnit	změnit	k5eAaPmF	změnit
společenské	společenský	k2eAgInPc4d1	společenský
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dopis	dopis	k1gInSc1	dopis
příteli	přítel	k1gMnSc3	přítel
žijícímu	žijící	k2eAgMnSc3d1	žijící
v	v	k7c6	v
Tobolsku	Tobolsek	k1gInSc2	Tobolsek
(	(	kIx(	(
<g/>
П	П	k?	П
к	к	k?	к
д	д	k?	д
<g/>
,	,	kIx,	,
ж	ж	k?	ж
в	в	k?	в
Т	Т	k?	Т
<g/>
,	,	kIx,	,
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Radiščev	Radiščev	k1gMnSc1	Radiščev
napsal	napsat	k5eAaBmAgMnS	napsat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
odhalení	odhalení	k1gNnSc2	odhalení
pomníku	pomník	k1gInSc2	pomník
cara	car	k1gMnSc2	car
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
carovy	carův	k2eAgFnPc4d1	carova
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c6	o
modernizaci	modernizace	k1gFnSc6	modernizace
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
(	(	kIx(	(
<g/>
П	П	k?	П
и	и	k?	и
П	П	k?	П
в	в	k?	в
М	М	k?	М
<g/>
,	,	kIx,	,
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Radiščevovo	Radiščevův	k2eAgNnSc4d1	Radiščevův
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
intelektuální	intelektuální	k2eAgInSc4d1	intelektuální
cestopis	cestopis	k1gInSc4	cestopis
obsahující	obsahující	k2eAgInSc4d1	obsahující
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
různých	různý	k2eAgFnPc2d1	různá
alegorií	alegorie	k1gFnPc2	alegorie
<g/>
,	,	kIx,	,
črt	črta	k1gFnPc2	črta
a	a	k8xC	a
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
životě	život	k1gInSc6	život
v	v	k7c6	v
rozlehlé	rozlehlý	k2eAgFnSc6d1	rozlehlá
ruské	ruský	k2eAgFnSc6d1	ruská
říši	říš	k1gFnSc6	říš
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
kritiku	kritika	k1gFnSc4	kritika
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
i	i	k8xC	i
samoděržaví	samoděržaví	k1gNnSc2	samoděržaví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápisky	zápisek	k1gInPc1	zápisek
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
(	(	kIx(	(
<g/>
З	З	k?	З
п	п	k?	п
в	в	k?	в
С	С	k?	С
<g/>
,	,	kIx,	,
publikováno	publikovat	k5eAaBmNgNnS	publikovat
až	až	k6eAd1	až
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorův	autorův	k2eAgInSc1d1	autorův
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zápisky	zápisek	k1gInPc1	zápisek
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
(	(	kIx(	(
<g/>
З	З	k?	З
п	п	k?	п
и	и	k?	и
С	С	k?	С
<g/>
,	,	kIx,	,
publikováno	publikovat	k5eAaBmNgNnS	publikovat
až	až	k6eAd1	až
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorův	autorův	k2eAgInSc1d1	autorův
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Slovanské	slovanský	k2eAgNnSc1d1	slovanské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Nataša	Nataša	k1gFnSc1	Nataša
Musiolová	Musiolový	k2eAgFnSc1d1	Musiolová
a	a	k8xC	a
Luděk	Luděk	k1gMnSc1	Luděk
Kubišta	Kubišta	k1gMnSc1	Kubišta
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prózy	próza	k1gFnPc4	próza
Život	život	k1gInSc1	život
Fjodora	Fjodor	k1gMnSc2	Fjodor
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
Ušakova	Ušakův	k2eAgFnSc1d1	Ušakův
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
Deník	deník	k1gInSc1	deník
jednoho	jeden	k4xCgInSc2	jeden
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
Dopis	dopis	k1gInSc1	dopis
příteli	přítel	k1gMnSc3	přítel
žijícímu	žijící	k2eAgMnSc3d1	žijící
v	v	k7c6	v
Tobolsku	Tobolsek	k1gInSc3	Tobolsek
<g/>
,	,	kIx,	,
Beseda	beseda	k1gFnSc1	beseda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
Nepřekročitelná	překročitelný	k2eNgFnSc1d1	nepřekročitelná
hráz	hráz	k1gFnSc1	hráz
nás	my	k3xPp1nPc4	my
dělí	dělit	k5eAaImIp3nS	dělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
československo-sovětského	československoovětský	k2eAgInSc2d1	československo-sovětský
institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luděk	Luděk	k1gMnSc1	Luděk
Kubišta	Kubišta	k1gMnSc1	Kubišta
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Zumr	Zumr	k1gMnSc1	Zumr
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorovy	autorův	k2eAgFnPc4d1	autorova
menší	malý	k2eAgFnPc4d2	menší
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
ódu	óda	k1gFnSc4	óda
Volnost	volnost	k1gFnSc4	volnost
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc4	báseň
Osmnácté	osmnáctý	k4xOgNnSc4	osmnáctý
století	století	k1gNnSc4	století
<g/>
,	,	kIx,	,
poemy	poema	k1gFnPc4	poema
Bova	Bovum	k1gNnSc2	Bovum
a	a	k8xC	a
Písně	píseň	k1gFnPc1	píseň
zpívané	zpívaný	k2eAgFnPc1d1	zpívaná
o	o	k7c6	o
závodech	závod	k1gInPc6	závod
pro	pro	k7c4	pro
čest	čest	k1gFnSc4	čest
starých	stará	k1gFnPc2	stará
slovanských	slovanský	k2eAgMnPc2d1	slovanský
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
filozofický	filozofický	k2eAgInSc4d1	filozofický
traktát	traktát	k1gInSc4	traktát
O	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrtelnosti	smrtelnost	k1gFnSc6	smrtelnost
a	a	k8xC	a
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Nataša	Nataša	k1gFnSc1	Nataša
Musiolová	Musiolový	k2eAgFnSc1d1	Musiolová
a	a	k8xC	a
Luděk	Luděk	k1gMnSc1	Luděk
Kubišta	Kubišta	k1gMnSc1	Kubišta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
:	:	kIx,	:
illustrovaná	illustrovaný	k2eAgFnSc1d1	illustrovaná
encyklopaedie	encyklopaedie	k1gFnSc1	encyklopaedie
obecných	obecný	k2eAgFnPc2d1	obecná
vědomostí	vědomost	k1gFnPc2	vědomost
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
1072	[number]	k4	1072
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
277218	[number]	k4	277218
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Článek	článek	k1gInSc1	článek
"	"	kIx"	"
<g/>
Radiščev	Radiščev	k1gFnSc1	Radiščev
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Radiščev	Radiščev	k1gFnSc7	Radiščev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Radiščev	Radiščev	k1gMnSc1	Radiščev
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
-	-	kIx~	-
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
básně	báseň	k1gFnPc1	báseň
-	-	kIx~	-
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
</s>
</p>
