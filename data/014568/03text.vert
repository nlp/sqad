<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
stranaDemocratic	stranaDemocratice	k1gFnPc2
Party	parta	k1gFnSc2
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1828	#num#	k4
1792	#num#	k4
(	(	kIx(
<g/>
historické	historický	k2eAgFnPc1d1
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
</s>
<s>
Tom	Tom	k1gMnSc1
Perez	Perez	k1gMnSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Andrew	Andrew	k?
Jackson	Jackson	k1gMnSc1
Lídr	lídr	k1gMnSc1
v	v	k7c6
Senátu	senát	k1gInSc6
</s>
<s>
Chuck	Chuck	k1gMnSc1
Schumer	Schumer	k1gMnSc1
<g/>
(	(	kIx(
<g/>
vůdce	vůdce	k1gMnSc1
většiny	většina	k1gFnSc2
<g/>
)	)	kIx)
Lídr	lídr	k1gMnSc1
ve	v	k7c6
Sněmovně	sněmovna	k1gFnSc6
rep	rep	k?
<g/>
.	.	kIx.
</s>
<s>
Nancy	Nancy	k1gFnSc4
Pelosi	Pelose	k1gFnSc4
<g/>
(	(	kIx(
<g/>
vůdce	vůdce	k1gMnSc1
většiny	většina	k1gFnSc2
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc4
</s>
<s>
430	#num#	k4
South	South	k1gMnSc1
Capitol	Capitola	k1gFnPc2
Street	Street	k1gMnSc1
SE	se	k3xPyFc4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
,	,	kIx,
20003	#num#	k4
Ideologie	ideologie	k1gFnSc1
</s>
<s>
neoliberalismus	neoliberalismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
sociální	sociální	k2eAgMnSc1d1
liberalismusamerický	liberalismusamerický	k2eAgMnSc1d1
liberalismusfrakce	liberalismusfrakce	k1gFnPc4
<g/>
:	:	kIx,
<g/>
sociální	sociální	k2eAgInSc1d1
demokracieprogresivismusDemokratický	demokracieprogresivismusDemokratický	k2eAgInSc1d1
socialismus	socialismus	k1gInSc1
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
střed	střed	k1gInSc1
až	až	k8xS
středolevice	středolevice	k1gFnSc1
Studentská	studentská	k1gFnSc1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
demokratů	demokrat	k1gMnPc2
Ameriky	Amerika	k1gFnSc2
Mládežnická	mládežnický	k2eAgNnPc4d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
Ameriky	Amerika	k1gFnSc2
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
80	#num#	k4
097	#num#	k4
731	#num#	k4
Barvy	barva	k1gFnPc1
</s>
<s>
modrá	modrat	k5eAaImIp3nS
Oficiální	oficiální	k2eAgNnSc1d1
web	web	k1gInSc4
</s>
<s>
http://www.democrats.org/	http://www.democrats.org/	k?
Zisk	zisk	k1gInSc1
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Sněmovna	sněmovna	k1gFnSc1
reprezentantů	reprezentant	k1gMnPc2
</s>
<s>
222	#num#	k4
<g/>
/	/	kIx~
<g/>
435	#num#	k4
</s>
<s>
Senát	senát	k1gInSc1
</s>
<s>
50	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
</s>
<s>
Guvernéři	guvernér	k1gMnPc1
</s>
<s>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Democratic	Democratice	k1gFnPc2
Party	party	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
Republikánské	republikánský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stranu	strana	k1gFnSc4
pod	pod	k7c7
současným	současný	k2eAgInSc7d1
názvem	název	k1gInSc7
založil	založit	k5eAaPmAgMnS
kolem	kolem	k7c2
roku	rok	k1gInSc2
1828	#num#	k4
Andrew	Andrew	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
kořeny	kořen	k1gInPc1
strany	strana	k1gFnSc2
sahají	sahat	k5eAaImIp3nP
do	do	k7c2
roku	rok	k1gInSc2
1792	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnSc1d2
prezident	prezident	k1gMnSc1
USA	USA	kA
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
Demokraticko-republikánskou	demokraticko-republikánský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
těchto	tento	k3xDgNnPc2
měřítek	měřítko	k1gNnPc2
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
nejstarší	starý	k2eAgFnSc1d3
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
politických	politický	k2eAgInPc2d1
postojů	postoj	k1gInPc2
jsou	být	k5eAaImIp3nP
demokraté	demokrat	k1gMnPc1
v	v	k7c6
USA	USA	kA
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
středolevici	středolevice	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
jsou	být	k5eAaImIp3nP
však	však	k9
považováni	považován	k2eAgMnPc1d1
spíše	spíše	k9
za	za	k7c4
středovou	středový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
měly	mít	k5eAaImAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
celkem	celkem	k6eAd1
15	#num#	k4
prezidentů	prezident	k1gMnPc2
z	z	k7c2
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1
znaku	znak	k1gInSc2
</s>
<s>
Znakem	znak	k1gInSc7
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
osel	osel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
oslovi	osel	k1gMnSc3
přirovnávali	přirovnávat	k5eAaImAgMnP
kritici	kritik	k1gMnPc1
už	už	k6eAd1
demokratického	demokratický	k2eAgMnSc4d1
kandidáta	kandidát	k1gMnSc4
na	na	k7c4
prezidenta	prezident	k1gMnSc4
Andrewa	Andrewus	k1gMnSc4
Jacksona	Jackson	k1gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1828	#num#	k4
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
nelíbily	líbit	k5eNaImAgFnP
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
údajně	údajně	k6eAd1
populistické	populistický	k2eAgInPc4d1
názory	názor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužem	muž	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
ale	ale	k9
oslíka	oslík	k1gMnSc4
navždy	navždy	k6eAd1
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
Demokratickou	demokratický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
karikaturista	karikaturista	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Nast	Nast	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1870	#num#	k4
totiž	totiž	k9
použil	použít	k5eAaPmAgMnS
osla	osel	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
znázornil	znázornit	k5eAaPmAgInS
protiválečné	protiválečný	k2eAgMnPc4d1
politické	politický	k2eAgMnPc4d1
sympatizanty	sympatizant	k1gMnPc4
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgMnPc7
nesouhlasil	souhlasit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narážel	narážet	k5eAaPmAgMnS,k5eAaImAgMnS
tím	ten	k3xDgNnSc7
hlavně	hlavně	k9
na	na	k7c4
některé	některý	k3yIgInPc4
prodemokratické	prodemokratický	k2eAgInPc4d1
magazíny	magazín	k1gInPc4
a	a	k8xC
deníky	deník	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
tradice	tradice	k1gFnSc1
pomalu	pomalu	k6eAd1
přešla	přejít	k5eAaPmAgFnS
i	i	k9
ke	k	k7c3
straně	strana	k1gFnSc3
samotné	samotný	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvou	barva	k1gFnSc7
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
ji	on	k3xPp3gFnSc4
televize	televize	k1gFnSc1
označila	označit	k5eAaPmAgFnS
v	v	k7c6
průzkumu	průzkum	k1gInSc6
a	a	k8xC
GOP	GOP	kA
přidělila	přidělit	k5eAaPmAgFnS
barvu	barva	k1gFnSc4
červenou	červený	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
modré	modré	k1gNnSc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
konzervativní	konzervativní	k2eAgMnPc1d1
a	a	k8xC
červené	červený	k2eAgFnPc1d1
levicové	levicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
politické	politický	k2eAgFnPc1d1
formace	formace	k1gFnPc1
si	se	k3xPyFc3
tyto	tento	k3xDgFnPc1
barvy	barva	k1gFnPc1
přivlastnily	přivlastnit	k5eAaPmAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Omezení	omezení	k1gNnSc1
politiky	politika	k1gFnSc2
rasové	rasový	k2eAgFnSc2d1
segregace	segregace	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
demokraté	demokrat	k1gMnPc1
prosadili	prosadit	k5eAaPmAgMnP
Zákon	zákon	k1gInSc4
o	o	k7c4
imigraci	imigrace	k1gFnSc4
a	a	k8xC
občanství	občanství	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zrušil	zrušit	k5eAaPmAgInS
rasová	rasový	k2eAgNnPc4d1
omezení	omezení	k1gNnPc4
pro	pro	k7c4
imigraci	imigrace	k1gFnSc4
z	z	k7c2
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
Afriky	Afrika	k1gFnSc2
a	a	k8xC
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
vedl	vést	k5eAaImAgInS
ke	k	k7c3
změně	změna	k1gFnSc3
etnického	etnický	k2eAgNnSc2d1
složení	složení	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Většina	většina	k1gFnSc1
demokratů	demokrat	k1gMnPc2
podporovala	podporovat	k5eAaImAgFnS
povinné	povinný	k2eAgNnSc4d1
rozvážení	rozvážení	k1gNnSc4
dětí	dítě	k1gFnPc2
odlišných	odlišný	k2eAgFnPc2d1
ras	rasa	k1gFnPc2
do	do	k7c2
škol	škola	k1gFnPc2
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
čtvrtích	čtvrt	k1gFnPc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
podpořena	podpořit	k5eAaPmNgFnS
rasová	rasový	k2eAgFnSc1d1
diverzita	diverzita	k1gFnSc1
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
desegregation	desegregation	k1gInSc1
busing	busing	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Demokraté	demokrat	k1gMnPc1
také	také	k9
tradičně	tradičně	k6eAd1
podporují	podporovat	k5eAaImIp3nP
pozitivní	pozitivní	k2eAgFnSc4d1
diskriminaci	diskriminace	k1gFnSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
affirmative	affirmativ	k1gInSc5
action	action	k1gInSc4
<g/>
)	)	kIx)
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
<g/>
,	,	kIx,
dle	dle	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
při	při	k7c6
přijímání	přijímání	k1gNnSc6
studentů	student	k1gMnPc2
na	na	k7c4
univerzity	univerzita	k1gFnPc4
rozhoduje	rozhodovat	k5eAaImIp3nS
podle	podle	k7c2
rasového	rasový	k2eAgInSc2d1
klíče	klíč	k1gInSc2
a	a	k8xC
zvýhodňují	zvýhodňovat	k5eAaImIp3nP
se	se	k3xPyFc4
Afroameričané	Afroameričan	k1gMnPc1
a	a	k8xC
Hispánci	Hispánek	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
krize	krize	k1gFnSc1
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
pokusu	pokus	k1gInSc3
posílit	posílit	k5eAaPmF
vliv	vliv	k1gInSc4
prostých	prostý	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
oproti	oproti	k7c3
stranickým	stranický	k2eAgFnPc3d1
špičkám	špička	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgInSc3
účelu	účel	k1gInSc3
vznikly	vzniknout	k5eAaPmAgFnP
kvóty	kvóta	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
zajistily	zajistit	k5eAaPmAgFnP
přítomnost	přítomnost	k1gFnSc4
slabších	slabý	k2eAgInPc2d2
a	a	k8xC
menších	malý	k2eAgFnPc2d2
sociálních	sociální	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
na	na	k7c6
Národním	národní	k2eAgInSc6d1
konventu	konvent	k1gInSc6
a	a	k8xC
zároveň	zároveň	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
již	již	k6eAd1
musel	muset	k5eAaImAgMnS
každý	každý	k3xTgMnSc1
zástupce	zástupce	k1gMnSc1
být	být	k5eAaImF
přímo	přímo	k6eAd1
zavázán	zavázán	k2eAgMnSc1d1
hlasovat	hlasovat	k5eAaImF
pro	pro	k7c4
konkrétního	konkrétní	k2eAgMnSc4d1
kandidáta	kandidát	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgInS
umenšen	umenšen	k2eAgInSc4d1
vliv	vliv	k1gInSc4
vůdců	vůdce	k1gMnPc2
a	a	k8xC
na	na	k7c6
konventu	konvent	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
se	s	k7c7
prezidentským	prezidentský	k2eAgMnSc7d1
kandidátem	kandidát	k1gMnSc7
demokratů	demokrat	k1gMnPc2
stal	stát	k5eAaPmAgInS
poměrně	poměrně	k6eAd1
neznámý	známý	k2eNgMnSc1d1
lokální	lokální	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Jimmy	Jimma	k1gFnSc2
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
projevil	projevit	k5eAaPmAgInS
organizační	organizační	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
a	a	k8xC
bez	bez	k7c2
stranické	stranický	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
zvládl	zvládnout	k5eAaPmAgMnS
kampaň	kampaň	k1gFnSc4
v	v	k7c6
primárkách	primárky	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
vyhrocení	vyhrocení	k1gNnSc3
vnitřních	vnitřní	k2eAgInPc2d1
stranických	stranický	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
opětovná	opětovný	k2eAgFnSc1d1
změna	změna	k1gFnSc1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zvýšila	zvýšit	k5eAaPmAgFnS
vliv	vliv	k1gInSc4
politických	politický	k2eAgMnPc2d1
organizátorů	organizátor	k1gMnPc2
a	a	k8xC
vůdců	vůdce	k1gMnPc2
při	při	k7c6
výběru	výběr	k1gInSc6
prezidentských	prezidentský	k2eAgMnPc2d1
kandidátů	kandidát	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
redukci	redukce	k1gFnSc3
délky	délka	k1gFnSc2
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
v	v	k7c6
primárkách	primárky	k1gFnPc6
z	z	k7c2
dvaceti	dvacet	k4xCc2
na	na	k7c4
patnáct	patnáct	k4xCc4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ideologie	ideologie	k1gFnSc1
a	a	k8xC
politika	politika	k1gFnSc1
</s>
<s>
Ideologické	ideologický	k2eAgNnSc1d1
ukotvení	ukotvení	k1gNnSc1
</s>
<s>
Andrew	Andrew	k?
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
moderní	moderní	k2eAgFnSc2d1
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c6
americké	americký	k2eAgFnSc6d1
politické	politický	k2eAgFnSc6d1
ose	osa	k1gFnSc6
zaujímá	zaujímat	k5eAaImIp3nS
středolevicové	středolevicový	k2eAgInPc4d1
a	a	k8xC
liberální	liberální	k2eAgInPc4d1
postoje	postoj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
má	mít	k5eAaImIp3nS
úměrnou	úměrný	k2eAgFnSc4d1
vnitřní	vnitřní	k2eAgFnSc4d1
rozmanitost	rozmanitost	k1gFnSc4
<g/>
;	;	kIx,
tradičním	tradiční	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
ideologie	ideologie	k1gFnSc1
sociálního	sociální	k2eAgInSc2d1
liberalismu	liberalismus	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
postupně	postupně	k6eAd1
prosazuje	prosazovat	k5eAaImIp3nS
směr	směr	k1gInSc4
zvaný	zvaný	k2eAgInSc1d1
progresivismus	progresivismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
i	i	k9
sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgInPc1d1
a	a	k8xC
zelené	zelený	k2eAgInPc1d1
proudy	proud	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
Demokratické	demokratický	k2eAgFnSc6d1
straně	strana	k1gFnSc6
začínají	začínat	k5eAaImIp3nP
objevovat	objevovat	k5eAaImF
i	i	k9
radikálně	radikálně	k6eAd1
socialistické	socialistický	k2eAgInPc1d1
proudy	proud	k1gInPc1
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gMnSc7
nejznámějším	známý	k2eAgMnSc7d3
představitelem	představitel	k1gMnSc7
je	být	k5eAaImIp3nS
senátor	senátor	k1gMnSc1
Bernie	Bernie	k1gFnSc2
Sanders	Sanders	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
však	však	k9
není	být	k5eNaImIp3nS
zapsaným	zapsaný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
tváří	tvář	k1gFnPc2
radikálně	radikálně	k6eAd1
progresivistického	progresivistický	k2eAgNnSc2d1
křídla	křídlo	k1gNnSc2
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
poslankyně	poslankyně	k1gFnSc1
Sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
Alexandria	Alexandrium	k1gNnSc2
Ocasio-Cortezová	Ocasio-Cortezová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
skupiny	skupina	k1gFnSc2
čtyř	čtyři	k4xCgFnPc2
žen	žena	k1gFnPc2
ve	v	k7c6
Sněmovně	sněmovna	k1gFnSc6
zvané	zvaný	k2eAgFnSc2d1
The	The	k1gFnSc2
Squad	Squad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Demokraté	demokrat	k1gMnPc1
oproti	oproti	k7c3
republikánům	republikán	k1gMnPc3
kladou	klást	k5eAaImIp3nP
větší	veliký	k2eAgInPc4d2
důraz	důraz	k1gInSc4
na	na	k7c6
řešení	řešení	k1gNnSc6
sociálních	sociální	k2eAgInPc2d1
problémů	problém	k1gInPc2
(	(	kIx(
<g/>
dostupné	dostupný	k2eAgNnSc4d1
bydlení	bydlení	k1gNnSc4
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc4
nezaměstnanosti	nezaměstnanost	k1gFnSc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
nejchudších	chudý	k2eAgInPc2d3
atd.	atd.	kA
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
na	na	k7c4
liberální	liberální	k2eAgNnPc4d1
témata	téma	k1gNnPc4
(	(	kIx(
<g/>
legalizace	legalizace	k1gFnSc1
interrupcí	interrupce	k1gFnPc2
a	a	k8xC
homosexuálních	homosexuální	k2eAgInPc2d1
sňatků	sňatek	k1gInPc2
<g/>
,	,	kIx,
pozitivní	pozitivní	k2eAgFnSc2d1
diskriminace	diskriminace	k1gFnSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosazují	prosazovat	k5eAaImIp3nP
progresivní	progresivní	k2eAgFnSc4d1
daň	daň	k1gFnSc4
a	a	k8xC
větší	veliký	k2eAgInPc4d2
zásahy	zásah	k1gInPc4
státu	stát	k1gInSc2
do	do	k7c2
hospodářství	hospodářství	k1gNnSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
ne	ne	k9
tak	tak	k6eAd1
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
levicové	levicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporují	podporovat	k5eAaImIp3nP
imigraci	imigrace	k1gFnSc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
amnestie	amnestie	k1gFnSc2
pro	pro	k7c4
nelegální	legální	k2eNgMnPc4d1
přistěhovalce	přistěhovalec	k1gMnPc4
<g/>
)	)	kIx)
a	a	k8xC
angažují	angažovat	k5eAaBmIp3nP
se	se	k3xPyFc4
ve	v	k7c6
vyšší	vysoký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
v	v	k7c6
obraně	obrana	k1gFnSc6
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
práv	právo	k1gNnPc2
národnostních	národnostní	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
a	a	k8xC
LGBT	LGBT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladou	klást	k5eAaImIp3nP
velký	velký	k2eAgInSc4d1
důraz	důraz	k1gInSc4
na	na	k7c4
zelené	zelený	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
–	–	k?
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc1
emisí	emise	k1gFnPc2
skleníkových	skleníkový	k2eAgInPc2d1
plynů	plyn	k1gInPc2
a	a	k8xC
podpora	podpora	k1gFnSc1
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
V	v	k7c6
zahraniční	zahraniční	k2eAgFnSc6d1
politice	politika	k1gFnSc6
chtějí	chtít	k5eAaImIp3nP
krom	krom	k7c2
udržování	udržování	k1gNnSc2
dobrých	dobrý	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
a	a	k8xC
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
<g/>
,	,	kIx,
prohlubovat	prohlubovat	k5eAaImF
vztahy	vztah	k1gInPc1
i	i	k9
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
zejména	zejména	k9
s	s	k7c7
Francií	Francie	k1gFnSc7
<g/>
,	,	kIx,
Německem	Německo	k1gNnSc7
a	a	k8xC
se	s	k7c7
zeměmi	zem	k1gFnPc7
ze	z	k7c2
Střední	střední	k2eAgFnSc2d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
s	s	k7c7
Mexikem	Mexiko	k1gNnSc7
<g/>
,	,	kIx,
či	či	k8xC
Brazílií	Brazílie	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zahraničněpolitických	zahraničněpolitický	k2eAgFnPc6d1
a	a	k8xC
bezpečnostních	bezpečnostní	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
boj	boj	k1gInSc1
proti	proti	k7c3
komunismu	komunismus	k1gInSc3
a	a	k8xC
islámskému	islámský	k2eAgInSc3d1
terorismu	terorismus	k1gInSc3
<g/>
,	,	kIx,
nepřátelství	nepřátelství	k1gNnSc1
k	k	k7c3
Rusku	Rusko	k1gNnSc3
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
masové	masový	k2eAgNnSc1d1
sledování	sledování	k1gNnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
NSA	NSA	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
s	s	k7c7
Republikány	republikán	k1gMnPc7
výrazně	výrazně	k6eAd1
neodlišují	odlišovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
existuje	existovat	k5eAaImIp3nS
spíše	spíše	k9
formálně	formálně	k6eAd1
než	než	k8xS
fakticky	fakticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okresní	okresní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
de	de	k?
facto	fact	k2eAgNnSc4d1
neexistují	existovat	k5eNaImIp3nP
a	a	k8xC
oblastní	oblastní	k2eAgInPc1d1
a	a	k8xC
státní	státní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
mají	mít	k5eAaImIp3nP
charakter	charakter	k1gInSc4
koalic	koalice	k1gFnPc2
držitelů	držitel	k1gMnPc2
moci	moct	k5eAaImF
<g/>
,	,	kIx,
tedy	tedy	k9
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
disponují	disponovat	k5eAaBmIp3nP
nějakou	nějaký	k3yIgFnSc7
funkcí	funkce	k1gFnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
strana	strana	k1gFnSc1
u	u	k7c2
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
stranický	stranický	k2eAgInSc1d1
orgán	orgán	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Demokratický	demokratický	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
(	(	kIx(
<g/>
význam	význam	k1gInSc1
„	„	k?
<g/>
national	nationat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
“	“	k?
je	být	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
„	„	k?
<g/>
federální	federální	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Democratic	Democratice	k1gFnPc2
National	National	k1gMnPc2
Committee	Committe	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
působí	působit	k5eAaImIp3nS
zástupci	zástupce	k1gMnSc3
ze	z	k7c2
všech	všecek	k3xTgInPc2
států	stát	k1gInPc2
Unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
výboru	výbor	k1gInSc2
je	být	k5eAaImIp3nS
vypracovat	vypracovat	k5eAaPmF
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
jednání	jednání	k1gNnSc4
Národního	národní	k2eAgNnSc2d1
(	(	kIx(
<g/>
federálního	federální	k2eAgMnSc2d1
<g/>
)	)	kIx)
konventu	konvent	k1gInSc2
(	(	kIx(
<g/>
National	National	k1gFnSc1
Convention	Convention	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
zajišťovat	zajišťovat	k5eAaImF
další	další	k2eAgFnPc4d1
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
zejména	zejména	k9
shánění	shánění	k1gNnSc1
příspěvků	příspěvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Předsedu	předseda	k1gMnSc4
strany	strana	k1gFnSc2
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
prezident	prezident	k1gMnSc1
USA	USA	kA
za	za	k7c4
Demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
demokraté	demokrat	k1gMnPc1
v	v	k7c6
opozici	opozice	k1gFnSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
volen	volit	k5eAaImNgInS
stranickými	stranický	k2eAgMnPc7d1
představiteli	představitel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
neměl	mít	k5eNaImAgMnS
žádný	žádný	k3yNgInSc4
vztah	vztah	k1gInSc4
k	k	k7c3
případným	případný	k2eAgMnPc3d1
uchazečům	uchazeč	k1gMnPc3
o	o	k7c4
funkci	funkce	k1gFnSc4
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Strany	strana	k1gFnPc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
státech	stát	k1gInPc6
</s>
<s>
Minnesota	Minnesota	k1gFnSc1
<g/>
:	:	kIx,
Minnesotská	minnesotský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
Progresivní	progresivní	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
občas	občas	k6eAd1
účastní	účastnit	k5eAaImIp3nP
i	i	k9
mezinárodních	mezinárodní	k2eAgInPc2d1
sjezdů	sjezd	k1gInPc2
u	u	k7c2
liberálních	liberální	k2eAgFnPc2d1
a	a	k8xC
také	také	k9
sociálně	sociálně	k6eAd1
demokratických	demokratický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Voličská	voličský	k2eAgFnSc1d1
základna	základna	k1gFnSc1
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1
demokratických	demokratický	k2eAgMnPc2d1
kandidátů	kandidát	k1gMnPc2
Baracka	Baracko	k1gNnSc2
Obamy	Obama	k1gFnSc2
a	a	k8xC
Hillary	Hillara	k1gFnSc2
Clintonové	Clintonová	k1gFnSc2
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
průzkumů	průzkum	k1gInPc2
se	se	k3xPyFc4
více	hodně	k6eAd2
obyvatel	obyvatel	k1gMnSc1
identifikuje	identifikovat	k5eAaBmIp3nS
s	s	k7c7
demokraty	demokrat	k1gMnPc7
než	než	k8xS
republikány	republikán	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2
mají	mít	k5eAaImIp3nP
více	hodně	k6eAd2
ve	v	k7c6
městech	město	k1gNnPc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
než	než	k8xS
na	na	k7c6
venkově	venkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
americká	americký	k2eAgFnSc1d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
např.	např.	kA
New	New	k1gFnSc4
York	York	k1gInSc1
<g/>
,	,	kIx,
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
,	,	kIx,
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
nebo	nebo	k8xC
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
považována	považován	k2eAgFnSc1d1
za	za	k7c2
tradiční	tradiční	k2eAgFnSc2d1
bašty	bašta	k1gFnSc2
demokratů	demokrat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
republikány	republikán	k1gMnPc7
měli	mít	k5eAaImAgMnP
demokraté	demokrat	k1gMnPc1
tradičně	tradičně	k6eAd1
o	o	k7c4
něco	něco	k6eAd1
větší	veliký	k2eAgFnSc4d2
podporu	podpora	k1gFnSc4
u	u	k7c2
nejchudší	chudý	k2eAgFnSc2d3
až	až	k8xS
střední	střední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
a	a	k8xC
u	u	k7c2
absolventů	absolvent	k1gMnPc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
také	také	k9
u	u	k7c2
etnických	etnický	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Afroameričané	Afroameričan	k1gMnPc1
a	a	k8xC
u	u	k7c2
hispánského	hispánský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimku	výjimek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
imigranti	imigrant	k1gMnPc1
z	z	k7c2
Kuby	Kuba	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
zvláště	zvláště	k6eAd1
početní	početní	k2eAgInSc4d1
na	na	k7c6
Floridě	Florida	k1gFnSc6
a	a	k8xC
přistěhovalci	přistěhovalec	k1gMnPc1
z	z	k7c2
Venezuely	Venezuela	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
většinově	většinově	k6eAd1
tíhnou	tíhnout	k5eAaImIp3nP
k	k	k7c3
republikánům	republikán	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraznou	výrazný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
mají	mít	k5eAaImIp3nP
demokraté	demokrat	k1gMnPc1
u	u	k7c2
rostoucí	rostoucí	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
asijských	asijský	k2eAgMnPc2d1
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podporu	podpora	k1gFnSc4
Američanů	Američan	k1gMnPc2
původem	původ	k1gInSc7
ze	z	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
z	z	k7c2
kontinentální	kontinentální	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
pro	pro	k7c4
demokraty	demokrat	k1gMnPc4
či	či	k8xC
republikány	republikán	k1gMnPc4
nelze	lze	k6eNd1
vyčíslit	vyčíslit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
původem	původ	k1gInSc7
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
Walesu	Wales	k1gInSc2
a	a	k8xC
Skotska	Skotsko	k1gNnSc2
a	a	k8xC
také	také	k9
z	z	k7c2
Irska	Irsko	k1gNnSc2
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
největší	veliký	k2eAgInSc1d3
poměrně	poměrně	k6eAd1
homogenní	homogenní	k2eAgFnSc7d1
etnickou	etnický	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepříliš	příliš	k6eNd1
velkou	velký	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
má	mít	k5eAaImIp3nS
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
u	u	k7c2
početných	početný	k2eAgMnPc2d1
Američanů	Američan	k1gMnPc2
německého	německý	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
dobře	dobře	k6eAd1
situovaní	situovaný	k2eAgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
také	také	k9
u	u	k7c2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
předkové	předek	k1gMnPc1
nebo	nebo	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
sami	sám	k3xTgMnPc1
přišli	přijít	k5eAaPmAgMnP
ze	z	k7c2
zemí	zem	k1gFnPc2
Střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
včetně	včetně	k7c2
Čechů	Čech	k1gMnPc2
a	a	k8xC
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SIROTA	sirota	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yes	Yes	k1gFnSc1
<g/>
,	,	kIx,
let	let	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
wipe	wipe	k1gInSc1
out	out	k?
Trump	Trump	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
But	But	k1gFnPc2
take	take	k6eAd1
neoliberal	oliberat	k5eNaPmAgInS,k5eNaImAgInS
Democrats	Democrats	k1gInSc1
with	with	k1gInSc1
him	him	k?
<g/>
,	,	kIx,
too	too	k?
<g/>
.	.	kIx.
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JANDA	Janda	k1gMnSc1
<g/>
,	,	kIx,
KENNETH	KENNETH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
challenge	challeng	k1gFnSc2
of	of	k?
democracy	democraca	k1gFnSc2
:	:	kIx,
American	American	k1gMnSc1
government	government	k1gMnSc1
in	in	k?
global	globat	k5eAaImAgMnS
politics	politics	k6eAd1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
th	th	k?
ed	ed	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Belmont	Belmont	k1gMnSc1
<g/>
,	,	kIx,
Calif	Calif	k1gMnSc1
<g/>
.	.	kIx.
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wadsworth	Wadsworth	k1gInSc1
Cengage	Cengage	k1gNnSc2
Learning	Learning	k1gInSc1
xxxviii	xxxviie	k1gFnSc4
<g/>
,	,	kIx,
697	#num#	k4
<g/>
,	,	kIx,
110	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
495	#num#	k4
<g/>
-	-	kIx~
<g/>
91309	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
495	#num#	k4
<g/>
-	-	kIx~
<g/>
91309	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
OCLC	OCLC	kA
751863473	#num#	k4
↑	↑	k?
http://politickymarketing.com/loga-a-barvy-politickych-stran-svazujici-barvy-14	http://politickymarketing.com/loga-a-barvy-politickych-stran-svazujici-barvy-14	k4
<g/>
↑	↑	k?
Jennifer	Jennifer	k1gInSc1
Ludden	Luddna	k1gFnPc2
<g/>
.	.	kIx.
1965	#num#	k4
immigration	immigration	k1gInSc1
law	law	k?
changed	changed	k1gInSc1
face	fac	k1gMnSc2
of	of	k?
America	Americus	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
record	record	k6eAd1
on	on	k3xPp3gInSc1
school	school	k1gInSc1
desegregation	desegregation	k1gInSc1
busing	busing	k1gInSc1
<g/>
,	,	kIx,
explained	explained	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vox	Vox	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kamala	Kamala	k1gFnSc1
Harris	Harris	k1gFnSc1
<g/>
'	'	kIx"
support	support	k1gInSc1
of	of	k?
public	publicum	k1gNnPc2
university	universita	k1gFnSc2
affirmative	affirmativ	k1gInSc5
action	action	k1gInSc1
defies	defies	k1gMnSc1
the	the	k?
will	will	k1gMnSc1
of	of	k?
California	Californium	k1gNnSc2
voters	votersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
How	How	k1gFnSc2
Americans	Americansa	k1gFnPc2
Feel	Feel	k1gMnSc1
About	About	k1gMnSc1
Affirmative	Affirmativ	k1gInSc5
Action	Action	k1gInSc1
In	In	k1gMnSc1
Higher	Highra	k1gFnPc2
Education	Education	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Poll	Poll	k1gInSc1
<g/>
:	:	kIx,
Obama	Obama	k?
won	won	k1gInSc1
71	#num#	k4
<g/>
%	%	kIx~
of	of	k?
Asian	Asian	k1gInSc1
vote	votat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politico	Politico	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Libertariánská	Libertariánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Seznam	seznam	k1gInSc4
prezidentů	prezident	k1gMnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
New	New	k?
Deal	Deal	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.democrats.org	www.democrats.org	k1gInSc1
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
strany	strana	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
stranického	stranický	k2eAgInSc2d1
znaku	znak	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
Demokratických	demokratický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
v	v	k7c6
USA	USA	kA
</s>
<s>
Analýza	analýza	k1gFnSc1
<g/>
:	:	kIx,
Liberalismus	liberalismus	k1gInSc1
a	a	k8xC
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
2004	#num#	k4
</s>
<s>
Demokraté	demokrat	k1gMnPc1
2020	#num#	k4
</s>
<s>
Demokraté	demokrat	k1gMnPc1
pro	pro	k7c4
Ameriku	Amerika	k1gFnSc4
</s>
<s>
Demokraté	demokrat	k1gMnPc1
pro	pro	k7c4
život	život	k1gInSc4
v	v	k7c6
Americe	Amerika	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20050512027	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
110671-5	110671-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8593	#num#	k4
3424	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79054058	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
159808480	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79054058	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
