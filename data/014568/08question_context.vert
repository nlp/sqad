<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Democratic	Democratice	k1gFnPc2
Party	party	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
Republikánské	republikánský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>