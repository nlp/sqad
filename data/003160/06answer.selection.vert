<s>
Rovník	rovník	k1gInSc1	rovník
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
nebeského	nebeský	k2eAgNnSc2d1	nebeské
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
polokoule	polokoule	k1gFnPc4	polokoule
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
