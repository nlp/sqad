<p>
<s>
Zásoby	zásoba	k1gFnPc1	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
odhadovaly	odhadovat	k5eAaImAgFnP	odhadovat
na	na	k7c4	na
1,1	[number]	k4	1,1
bilionu	bilion	k4xCgInSc2	bilion
kubických	kubický	k2eAgInPc2d1	kubický
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zemi	zem	k1gFnSc4	zem
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
26	[number]	k4	26
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
prokázanými	prokázaný	k2eAgFnPc7d1	prokázaná
zásobami	zásoba	k1gFnPc7	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
před	před	k7c7	před
připojením	připojení	k1gNnSc7	připojení
poloostrovu	poloostrov	k1gInSc3	poloostrov
Krym	Krym	k1gInSc1	Krym
k	k	k7c3	k
Ruské	ruský	k2eAgFnSc3d1	ruská
federaci	federace	k1gFnSc3	federace
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
ukrajinské	ukrajinský	k2eAgFnPc1d1	ukrajinská
zásoby	zásoba	k1gFnPc1	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
na	na	k7c4	na
5,4	[number]	k4	5,4
bilionu	bilion	k4xCgInSc2	bilion
kubických	kubický	k2eAgInPc2d1	kubický
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Domácí	domácí	k2eAgFnSc1d1	domácí
produkce	produkce	k1gFnSc1	produkce
==	==	k?	==
</s>
</p>
<p>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
domácí	domácí	k2eAgFnSc2d1	domácí
produkce	produkce	k1gFnSc2	produkce
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vytěžilo	vytěžit	k5eAaPmAgNnS	vytěžit
68,1	[number]	k4	68,1
miliardy	miliarda	k4xCgFnSc2	miliarda
kubických	kubický	k2eAgMnPc2d1	kubický
metrů	metr	k1gMnPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
produkce	produkce	k1gFnSc1	produkce
pomaly	pomaly	k?	pomaly
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
v	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
letech	léto	k1gNnPc6	léto
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
na	na	k7c4	na
asi	asi	k9	asi
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
kubických	kubický	k2eAgInPc2d1	kubický
metrů	metr	k1gInPc2	metr
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
snaží	snažit	k5eAaImIp3nS	snažit
zvýšit	zvýšit	k5eAaPmF	zvýšit
produkci	produkce	k1gFnSc4	produkce
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
Černém	černý	k2eAgNnSc6d1	černé
moři	moře	k1gNnSc6	moře
z	z	k7c2	z
1	[number]	k4	1
miliardy	miliarda	k4xCgFnPc1	miliarda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c4	na
3	[number]	k4	3
miliardy	miliarda	k4xCgFnPc1	miliarda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
tu	tu	k6eAd1	tu
produkce	produkce	k1gFnSc1	produkce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
kubických	kubický	k2eAgMnPc2d1	kubický
metrů	metr	k1gMnPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
produkce	produkce	k1gFnSc2	produkce
bude	být	k5eAaImBp3nS	být
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
netradičních	tradiční	k2eNgInPc2d1	netradiční
plynových	plynový	k2eAgInPc2d1	plynový
depozitů	depozit	k1gInPc2	depozit
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
6-11	[number]	k4	6-11
miliard	miliarda	k4xCgFnPc2	miliarda
kubických	kubický	k2eAgInPc2d1	kubický
metrů	metr	k1gInPc2	metr
břidlicového	břidlicový	k2eAgInSc2d1	břidlicový
plynu	plyn	k1gInSc2	plyn
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
přírodního	přírodní	k2eAgInSc2d1	přírodní
plynu	plyn	k1gInSc2	plyn
na	na	k7c4	na
technologii	technologie	k1gFnSc4	technologie
zplyňování	zplyňování	k1gNnSc1	zplyňování
uhlí	uhlí	k1gNnSc2	uhlí
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Natural	Natural	k?	Natural
gas	gas	k?	gas
in	in	k?	in
Ukraine	Ukrain	k1gInSc5	Ukrain
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
