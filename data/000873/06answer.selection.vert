<s>
Tantal	tantal	k1gInSc1	tantal
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
švédským	švédský	k2eAgMnSc7d1	švédský
chemikem	chemik	k1gMnSc7	chemik
Andersem	Anders	k1gMnSc7	Anders
G.	G.	kA	G.
Ekebergem	Ekeberg	k1gMnSc7	Ekeberg
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc1d1	čistý
prvek	prvek	k1gInSc1	prvek
byl	být	k5eAaImAgInS	být
izolován	izolovat	k5eAaBmNgInS	izolovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
Jönsem	Jönso	k1gNnSc7	Jönso
Berzeliem	Berzelium	k1gNnSc7	Berzelium
<g/>
.	.	kIx.	.
</s>
