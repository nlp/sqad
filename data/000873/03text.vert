<s>
Tantal	tantal	k1gInSc1	tantal
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ta	ten	k3xDgFnSc1	ten
latinsky	latinsky	k6eAd1	latinsky
Tantalum	Tantalum	k?	Tantalum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
<g/>
,	,	kIx,	,
modro-šedý	modro-šedý	k2eAgInSc4d1	modro-šedý
<g/>
,	,	kIx,	,
lesklý	lesklý	k2eAgInSc4d1	lesklý
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc4d1	přechodný
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
korozivzdorný	korozivzdorný	k2eAgMnSc1d1	korozivzdorný
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
implantátů	implantát	k1gInPc2	implantát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
inertní	inertní	k2eAgInSc4d1	inertní
vůči	vůči	k7c3	vůči
organickým	organický	k2eAgFnPc3d1	organická
tělesným	tělesný	k2eAgFnPc3d1	tělesná
tkáním	tkáň	k1gFnPc3	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tantal	tantal	k1gInSc1	tantal
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
švédským	švédský	k2eAgMnSc7d1	švédský
chemikem	chemik	k1gMnSc7	chemik
Andersem	Anders	k1gMnSc7	Anders
G.	G.	kA	G.
Ekebergem	Ekeberg	k1gMnSc7	Ekeberg
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc1d1	čistý
prvek	prvek	k1gInSc1	prvek
byl	být	k5eAaImAgInS	být
izolován	izolovat	k5eAaBmNgInS	izolovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
Jönsem	Jönso	k1gNnSc7	Jönso
Berzeliem	Berzelium	k1gNnSc7	Berzelium
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
získal	získat	k5eAaPmAgInS	získat
podle	podle	k7c2	podle
bájného	bájný	k2eAgMnSc2d1	bájný
krále	král	k1gMnSc2	král
Tantala	Tantal	k1gMnSc2	Tantal
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyjádřením	vyjádření	k1gNnSc7	vyjádření
jeho	jeho	k3xOp3gFnSc2	jeho
význačné	význačný	k2eAgFnSc2d1	význačná
vlastnosti	vlastnost	k1gFnSc2	vlastnost
-	-	kIx~	-
chemické	chemický	k2eAgFnSc2d1	chemická
netečnosti	netečnost	k1gFnSc2	netečnost
<g/>
.	.	kIx.	.
</s>
<s>
Tantal	tantal	k1gInSc1	tantal
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
mimořádně	mimořádně	k6eAd1	mimořádně
obtížně	obtížně	k6eAd1	obtížně
tavitelné	tavitelný	k2eAgInPc1d1	tavitelný
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
wolfram	wolfram	k1gInSc1	wolfram
a	a	k8xC	a
rhenium	rhenium	k1gNnSc1	rhenium
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgInSc4d2	vyšší
bod	bod	k1gInSc4	bod
tavení	tavení	k1gNnSc2	tavení
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
podobný	podobný	k2eAgInSc1d1	podobný
prvku	prvek	k1gInSc2	prvek
niobu	niob	k1gInSc2	niob
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
jej	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
v	v	k7c6	v
minerálech	minerál	k1gInPc6	minerál
a	a	k8xC	a
rudách	ruda	k1gFnPc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Tantal	tantal	k1gInSc1	tantal
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
kujný	kujný	k2eAgInSc1d1	kujný
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
pevný	pevný	k2eAgInSc4d1	pevný
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
odolný	odolný	k2eAgMnSc1d1	odolný
proti	proti	k7c3	proti
kyselinám	kyselina	k1gFnPc3	kyselina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dobrým	dobrý	k2eAgInSc7d1	dobrý
vodičem	vodič	k1gInSc7	vodič
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
chemicky	chemicky	k6eAd1	chemicky
inertní	inertní	k2eAgFnSc1d1	inertní
<g/>
,	,	kIx,	,
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
s	s	k7c7	s
lučavkou	lučavka	k1gFnSc7	lučavka
královskou	královský	k2eAgFnSc7d1	královská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
4,48	[number]	k4	4,48
K	K	kA	K
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
supravodičem	supravodič	k1gInSc7	supravodič
I	i	k8xC	i
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
(	(	kIx(	(
<g/>
HF	HF	kA	HF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyselých	kyselý	k2eAgInPc6d1	kyselý
roztocích	roztok	k1gInPc6	roztok
obsahujících	obsahující	k2eAgInPc2d1	obsahující
fluoridové	fluoridový	k2eAgInPc4d1	fluoridový
ionty	ion	k1gInPc4	ion
a	a	k8xC	a
kapalném	kapalný	k2eAgInSc6d1	kapalný
oxidu	oxid	k1gInSc6	oxid
sírovém	sírový	k2eAgInSc6d1	sírový
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
tantal	tantal	k1gInSc1	tantal
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
alkalickým	alkalický	k2eAgNnSc7d1	alkalické
tavením	tavení	k1gNnSc7	tavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Ta	ten	k3xDgFnSc1	ten
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
a	a	k8xC	a
Ta	ten	k3xDgFnSc1	ten
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Tantal	tantal	k1gInSc1	tantal
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
obsah	obsah	k1gInSc4	obsah
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
2	[number]	k4	2
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1	koncentrace
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nelze	lze	k6eNd1	lze
změřit	změřit	k5eAaPmF	změřit
ani	ani	k9	ani
nejcitlivějšími	citlivý	k2eAgFnPc7d3	nejcitlivější
analytickými	analytický	k2eAgFnPc7d1	analytická
metodami	metoda	k1gFnPc7	metoda
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
0,000	[number]	k4	0,000
0025	[number]	k4	0025
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
tantalu	tantal	k1gInSc2	tantal
na	na	k7c4	na
1	[number]	k4	1
500	[number]	k4	500
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Tantal	Tantal	k1gMnSc1	Tantal
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
minerálu	minerál	k1gInSc6	minerál
tantalitu	tantalit	k1gInSc2	tantalit
((	((	k?	((
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
euxenitu	euxenita	k1gFnSc4	euxenita
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
<g/>
Th	Th	k1gFnSc1	Th
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
Ti	ty	k3xPp2nSc3	ty
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ložiska	ložisko	k1gNnPc1	ložisko
rud	rudo	k1gNnPc2	rudo
s	s	k7c7	s
prakticky	prakticky	k6eAd1	prakticky
využitelným	využitelný	k2eAgInSc7d1	využitelný
obsahem	obsah	k1gInSc7	obsah
tantalu	tantal	k1gInSc2	tantal
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
přírodních	přírodní	k2eAgFnPc6d1	přírodní
rudách	ruda	k1gFnPc6	ruda
jej	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
niob	niob	k1gInSc1	niob
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
chemické	chemický	k2eAgNnSc1d1	chemické
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
separaci	separace	k1gFnSc4	separace
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
kovů	kov	k1gInPc2	kov
používá	používat	k5eAaImIp3nS	používat
krystalizace	krystalizace	k1gFnSc1	krystalizace
jejich	jejich	k3xOp3gInPc2	jejich
fluorokomplexů	fluorokomplex	k1gInPc2	fluorokomplex
nebo	nebo	k8xC	nebo
frakční	frakční	k2eAgFnSc1d1	frakční
destilace	destilace	k1gFnSc1	destilace
pětimocných	pětimocný	k2eAgInPc2d1	pětimocný
chloridů	chlorid	k1gInPc2	chlorid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přečištění	přečištění	k1gNnSc6	přečištění
sloučenin	sloučenina	k1gFnPc2	sloučenina
se	se	k3xPyFc4	se
elementární	elementární	k2eAgInSc1d1	elementární
kovový	kovový	k2eAgInSc1d1	kovový
tantal	tantal	k1gInSc1	tantal
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
použití	použití	k1gNnSc1	použití
nachází	nacházet	k5eAaImIp3nS	nacházet
tantal	tantal	k1gInSc4	tantal
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
elektronických	elektronický	k2eAgFnPc2d1	elektronická
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
tantalových	tantalový	k2eAgInPc2d1	tantalový
kondenzátorů	kondenzátor	k1gInPc2	kondenzátor
<g/>
.	.	kIx.	.
</s>
<s>
Tantalové	tantalový	k2eAgInPc1d1	tantalový
elektrolytické	elektrolytický	k2eAgInPc1d1	elektrolytický
kondenzátory	kondenzátor	k1gInPc1	kondenzátor
využívají	využívat	k5eAaPmIp3nP	využívat
schopnosti	schopnost	k1gFnPc4	schopnost
tantalu	tantal	k1gInSc2	tantal
vytvořit	vytvořit	k5eAaPmF	vytvořit
odolnou	odolný	k2eAgFnSc4d1	odolná
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
vrstvu	vrstva	k1gFnSc4	vrstva
oxidu	oxid	k1gInSc2	oxid
<g/>
,	,	kIx,	,
tantalová	tantalový	k2eAgFnSc1d1	tantalová
fólie	fólie	k1gFnSc1	fólie
tvoří	tvořit	k5eAaImIp3nS	tvořit
první	první	k4xOgFnSc4	první
desku	deska	k1gFnSc4	deska
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gNnSc1	jeho
dielektrikum	dielektrikum	k1gNnSc1	dielektrikum
a	a	k8xC	a
elektrolyt	elektrolyt	k1gInSc1	elektrolyt
tvoří	tvořit	k5eAaImIp3nS	tvořit
druhou	druhý	k4xOgFnSc4	druhý
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
oxidická	oxidický	k2eAgFnSc1d1	oxidická
vrstva	vrstva	k1gFnSc1	vrstva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
(	(	kIx(	(
<g/>
tenčí	tenký	k2eAgFnSc1d2	tenčí
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
typech	typ	k1gInPc6	typ
kondenzátorů	kondenzátor	k1gInPc2	kondenzátor
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
objemu	objem	k1gInSc2	objem
získat	získat	k5eAaPmF	získat
velkou	velký	k2eAgFnSc4d1	velká
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využití	využití	k1gNnSc3	využití
těchto	tento	k3xDgInPc2	tento
kondenzátorů	kondenzátor	k1gInPc2	kondenzátor
v	v	k7c6	v
telefonech	telefon	k1gInPc6	telefon
<g/>
,	,	kIx,	,
počítačích	počítač	k1gInPc6	počítač
atd.	atd.	kA	atd.
Tantal	tantal	k1gInSc4	tantal
se	se	k3xPyFc4	se
také	také	k9	také
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
slitinách	slitina	k1gFnPc6	slitina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
získávají	získávat	k5eAaImIp3nP	získávat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgInPc1d1	pevný
a	a	k8xC	a
kujné	kujný	k2eAgInPc1d1	kujný
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vysoce	vysoce	k6eAd1	vysoce
namáhaných	namáhaný	k2eAgFnPc2d1	namáhaná
součástek	součástka	k1gFnPc2	součástka
v	v	k7c6	v
leteckých	letecký	k2eAgInPc6d1	letecký
turbomotorech	turbomotor	k1gInPc6	turbomotor
<g/>
,	,	kIx,	,
ponorkách	ponorka	k1gFnPc6	ponorka
<g/>
,	,	kIx,	,
atomových	atomový	k2eAgInPc6d1	atomový
reaktorech	reaktor	k1gInPc6	reaktor
a	a	k8xC	a
chemických	chemický	k2eAgInPc6d1	chemický
reaktorech	reaktor	k1gInPc6	reaktor
pro	pro	k7c4	pro
speciální	speciální	k2eAgFnPc4d1	speciální
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
absolutně	absolutně	k6eAd1	absolutně
inertní	inertní	k2eAgFnSc1d1	inertní
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
slitin	slitina	k1gFnPc2	slitina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
tělních	tělní	k2eAgInPc2d1	tělní
implantátů	implantát	k1gInPc2	implantát
<g/>
.	.	kIx.	.
</s>
<s>
Karbid	karbid	k1gInSc1	karbid
tantalu	tantal	k1gInSc2	tantal
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgInPc2d3	nejtvrdší
známých	známý	k2eAgInPc2d1	známý
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgFnPc2d1	speciální
brusných	brusný	k2eAgFnPc2d1	brusná
směsí	směs	k1gFnPc2	směs
a	a	k8xC	a
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
povrchů	povrch	k1gInPc2	povrch
vrtných	vrtný	k2eAgNnPc2d1	vrtné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
tantalu	tantal	k1gInSc2	tantal
je	být	k5eAaImIp3nS	být
složkou	složka	k1gFnSc7	složka
speciálních	speciální	k2eAgNnPc2d1	speciální
skel	sklo	k1gNnPc2	sklo
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
optických	optický	k2eAgFnPc2d1	optická
součástek	součástka	k1gFnPc2	součástka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čočky	čočka	k1gFnSc2	čočka
filmových	filmový	k2eAgFnPc2d1	filmová
kamer	kamera	k1gFnPc2	kamera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
silně	silně	k6eAd1	silně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
tantal	tantal	k1gInSc1	tantal
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
izotopy	izotop	k1gInPc4	izotop
<g/>
.	.	kIx.	.
181	[number]	k4	181
<g/>
Ta	ten	k3xDgFnSc1	ten
je	on	k3xPp3gInPc4	on
stabilní	stabilní	k2eAgInPc4d1	stabilní
a	a	k8xC	a
180	[number]	k4	180
<g/>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1015	[number]	k4	1015
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tantal	tantal	k1gInSc1	tantal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tantal	tantal	k1gInSc1	tantal
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
