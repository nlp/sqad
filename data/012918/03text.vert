<p>
<s>
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
filiální	filiální	k2eAgInSc1d1	filiální
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
architektonického	architektonický	k2eAgNnSc2d1	architektonické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pětilodní	pětilodní	k2eAgFnSc4d1	pětilodní
gotickou	gotický	k2eAgFnSc4d1	gotická
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1	okolnost
fundace	fundace	k1gFnSc2	fundace
chrámu	chrám	k1gInSc2	chrám
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
svaté	svatý	k2eAgFnSc2d1	svatá
panny	panna	k1gFnSc2	panna
Barbory	Barbora	k1gFnSc2	Barbora
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
unikátní	unikátní	k2eAgFnPc1d1	unikátní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
ostrohu	ostroh	k1gInSc2	ostroh
nad	nad	k7c7	nad
říčkou	říčka	k1gFnSc7	říčka
Vrchlicí	vrchlice	k1gFnSc7	vrchlice
stávala	stávat	k5eAaImAgFnS	stávat
kaplička	kaplička	k1gFnSc1	kaplička
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
patronce	patronka	k1gFnSc3	patronka
horníků	horník	k1gMnPc2	horník
<g/>
,	,	kIx,	,
svaté	svatý	k2eAgFnSc3d1	svatá
panně	panna	k1gFnSc3	panna
Barboře	Barbora	k1gFnSc3	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popud	popud	k1gInSc4	popud
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
měšťané	měšťan	k1gMnPc1	měšťan
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
náklady	náklad	k1gInPc4	náklad
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
architektonicky	architektonicky	k6eAd1	architektonicky
směle	směle	k6eAd1	směle
konkuroval	konkurovat	k5eAaImAgMnS	konkurovat
pražské	pražský	k2eAgFnSc3d1	Pražská
katedrále	katedrála	k1gFnSc3	katedrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
panny	panna	k1gFnSc2	panna
Barbory	Barbora	k1gFnSc2	Barbora
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
úctu	úcta	k1gFnSc4	úcta
měšťanů	měšťan	k1gMnPc2	měšťan
k	k	k7c3	k
Božímu	boží	k2eAgNnSc3d1	boží
Tělu	tělo	k1gNnSc3	tělo
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
dobové	dobový	k2eAgNnSc1d1	dobové
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Kutnou	kutný	k2eAgFnSc7d1	Kutná
Horou	hora	k1gFnSc7	hora
a	a	k8xC	a
také	také	k9	také
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
od	od	k7c2	od
sedleckého	sedlecký	k2eAgInSc2d1	sedlecký
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
právě	právě	k9	právě
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
vybudován	vybudován	k2eAgInSc1d1	vybudován
za	za	k7c7	za
městskými	městský	k2eAgFnPc7d1	městská
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
sedleckou	sedlecký	k2eAgFnSc4d1	Sedlecká
duchovní	duchovní	k2eAgFnSc4d1	duchovní
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
pražské	pražský	k2eAgFnSc2d1	Pražská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
započala	započnout	k5eAaPmAgFnS	započnout
roku	rok	k1gInSc2	rok
1388	[number]	k4	1388
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
mít	mít	k5eAaImF	mít
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
délku	délka	k1gFnSc4	délka
lodi	loď	k1gFnSc2	loď
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
současný	současný	k2eAgInSc4d1	současný
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
několika	několik	k4yIc6	několik
etapách	etapa	k1gFnPc6	etapa
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
souvisela	souviset	k5eAaImAgFnS	souviset
s	s	k7c7	s
prosperitou	prosperita	k1gFnSc7	prosperita
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
kutnohorských	kutnohorský	k2eAgMnPc2d1	kutnohorský
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
definitivně	definitivně	k6eAd1	definitivně
zastaveny	zastavit	k5eAaPmNgFnP	zastavit
roku	rok	k1gInSc2	rok
1558	[number]	k4	1558
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
západě	západ	k1gInSc6	západ
uzavřen	uzavřen	k2eAgInSc1d1	uzavřen
pouze	pouze	k6eAd1	pouze
provizorní	provizorní	k2eAgFnSc7d1	provizorní
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
zásahy	zásah	k1gInPc1	zásah
měly	mít	k5eAaImAgInP	mít
pouze	pouze	k6eAd1	pouze
udržovací	udržovací	k2eAgInSc4d1	udržovací
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
kališníky	kališník	k1gMnPc4	kališník
vydrancován	vydrancován	k2eAgMnSc1d1	vydrancován
<g/>
,	,	kIx,	,
zpovědnice	zpovědnice	k1gFnPc1	zpovědnice
byly	být	k5eAaImAgFnP	být
roztřískány	roztřískán	k2eAgFnPc1d1	roztřískán
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
roztrhány	roztrhán	k2eAgInPc1d1	roztrhán
<g/>
,	,	kIx,	,
chrámový	chrámový	k2eAgInSc1d1	chrámový
poklad	poklad	k1gInSc1	poklad
pak	pak	k6eAd1	pak
rozkraden	rozkrást	k5eAaPmNgInS	rozkrást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
pobělohorské	pobělohorský	k2eAgFnSc2d1	pobělohorská
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc4	kostel
zbaven	zbaven	k2eAgInSc4d1	zbaven
utrakvistických	utrakvistický	k2eAgInPc2d1	utrakvistický
symbolů	symbol	k1gInPc2	symbol
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
předán	předat	k5eAaPmNgInS	předat
jezuitům	jezuita	k1gMnPc3	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gNnSc2	on
pak	pak	k9	pak
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
kolej	kolej	k1gFnSc4	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
kostela	kostel	k1gInSc2	kostel
provedli	provést	k5eAaPmAgMnP	provést
barokní	barokní	k2eAgFnPc4d1	barokní
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
barokního	barokní	k2eAgInSc2d1	barokní
krovu	krov	k1gInSc2	krov
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
historické	historický	k2eAgNnSc4d1	historické
foto	foto	k1gNnSc4	foto
ve	v	k7c6	v
fotogalerii	fotogalerie	k1gFnSc6	fotogalerie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1884	[number]	k4	1884
a	a	k8xC	a
1905	[number]	k4	1905
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
popudu	popud	k1gInSc2	popud
místního	místní	k2eAgInSc2d1	místní
archeologického	archeologický	k2eAgInSc2d1	archeologický
spolku	spolek	k1gInSc2	spolek
Vocel	Vocela	k1gFnPc2	Vocela
provedena	provést	k5eAaPmNgFnS	provést
puristická	puristický	k2eAgFnSc1d1	puristická
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
nejen	nejen	k6eAd1	nejen
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
klenební	klenební	k2eAgNnSc4d1	klenební
pole	pole	k1gNnSc4	pole
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
nové	nový	k2eAgNnSc1d1	nové
neogotické	ogotický	k2eNgNnSc1d1	neogotické
průčelí	průčelí	k1gNnSc1	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
krov	krov	k1gInSc1	krov
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
krovem	krov	k1gInSc7	krov
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
trojice	trojice	k1gFnSc2	trojice
gotizujících	gotizující	k2eAgFnPc2d1	gotizující
stanových	stanový	k2eAgFnPc2d1	stanová
střech	střecha	k1gFnPc2	střecha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
zhruba	zhruba	k6eAd1	zhruba
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
původnímu	původní	k2eAgNnSc3d1	původní
gotickému	gotický	k2eAgNnSc3d1	gotické
řešení	řešení	k1gNnSc3	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architekti	architekt	k1gMnPc1	architekt
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
výstavby	výstavba	k1gFnSc2	výstavba
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
fázi	fáze	k1gFnSc4	fáze
výstavby	výstavba	k1gFnSc2	výstavba
prováděla	provádět	k5eAaImAgNnP	provádět
huť	huť	k1gFnSc4	huť
stavitele	stavitel	k1gMnSc2	stavitel
Svatovítské	svatovítský	k2eAgFnSc2d1	Svatovítská
katedrály	katedrála	k1gFnSc2	katedrála
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Parléř	Parléř	k1gMnSc1	Parléř
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
nejstarší	starý	k2eAgFnSc4d3	nejstarší
část	část	k1gFnSc4	část
stavby	stavba	k1gFnSc2	stavba
s	s	k7c7	s
věncem	věnec	k1gInSc7	věnec
obvodových	obvodový	k2eAgFnPc2d1	obvodová
kaplí	kaple	k1gFnPc2	kaple
podle	podle	k7c2	podle
vzorů	vzor	k1gInPc2	vzor
francouzských	francouzský	k2eAgFnPc2d1	francouzská
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
budované	budovaný	k2eAgNnSc1d1	budované
trojlodí	trojlodí	k1gNnSc1	trojlodí
bylo	být	k5eAaImAgNnS	být
brzy	brzy	k6eAd1	brzy
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
o	o	k7c6	o
široké	široký	k2eAgFnSc6d1	široká
vnější	vnější	k2eAgFnSc6d1	vnější
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
šedesát	šedesát	k4xCc4	šedesát
let	léto	k1gNnPc2	léto
přerušena	přerušen	k2eAgFnSc1d1	přerušena
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
téměř	téměř	k6eAd1	téměř
poloviny	polovina	k1gFnSc2	polovina
současné	současný	k2eAgFnSc2d1	současná
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc1d1	střední
trojlodí	trojlodí	k1gNnSc1	trojlodí
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
zaklenuto	zaklenut	k2eAgNnSc1d1	zaklenuto
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
získala	získat	k5eAaPmAgFnS	získat
klenbu	klenba	k1gFnSc4	klenba
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
krytá	krytý	k2eAgFnSc1d1	krytá
proti	proti	k7c3	proti
dešti	dešť	k1gInSc3	dešť
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jí	on	k3xPp3gFnSc3	on
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nesvědčilo	svědčit	k5eNaImAgNnS	svědčit
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
na	na	k7c6	na
kostele	kostel	k1gInSc6	kostel
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
obnoveny	obnovit	k5eAaPmNgInP	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1482	[number]	k4	1482
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
zde	zde	k6eAd1	zde
pracovali	pracovat	k5eAaImAgMnP	pracovat
místní	místní	k2eAgMnPc1d1	místní
stavitelé	stavitel	k1gMnPc1	stavitel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
intencích	intence	k1gFnPc6	intence
zakladatele	zakladatel	k1gMnSc2	zakladatel
stavby	stavba	k1gFnSc2	stavba
Jana	Jan	k1gMnSc2	Jan
Parléře	Parléř	k1gMnSc2	Parléř
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
budována	budovat	k5eAaImNgFnS	budovat
z	z	k7c2	z
pískovce	pískovec	k1gInSc2	pískovec
<g/>
,	,	kIx,	,
získávaného	získávaný	k2eAgMnSc2d1	získávaný
z	z	k7c2	z
nedalekých	daleký	k2eNgInPc2d1	nedaleký
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1489	[number]	k4	1489
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1506	[number]	k4	1506
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
dostavbě	dostavba	k1gFnSc6	dostavba
kostela	kostel	k1gInSc2	kostel
Matěj	Matěj	k1gMnSc1	Matěj
Rejsek	rejsek	k1gMnSc1	rejsek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dostavěl	dostavět	k5eAaPmAgMnS	dostavět
chór	chór	k1gInSc4	chór
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
provedl	provést	k5eAaPmAgInS	provést
triforium	triforium	k1gNnSc4	triforium
<g/>
,	,	kIx,	,
zónu	zóna	k1gFnSc4	zóna
bazilikálních	bazilikální	k2eAgNnPc2d1	bazilikální
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
utvářenou	utvářený	k2eAgFnSc4d1	utvářená
síťovou	síťový	k2eAgFnSc4d1	síťová
žebrovou	žebrový	k2eAgFnSc4d1	žebrová
klenbu	klenba	k1gFnSc4	klenba
(	(	kIx(	(
<g/>
dokončena	dokončen	k2eAgFnSc1d1	dokončena
1499	[number]	k4	1499
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
část	část	k1gFnSc4	část
vnějšího	vnější	k2eAgInSc2d1	vnější
opěrného	opěrný	k2eAgInSc2d1	opěrný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInPc1d1	stavební
zásahy	zásah	k1gInPc1	zásah
Matyáše	Matyáš	k1gMnSc2	Matyáš
Rejska	rejsek	k1gMnSc2	rejsek
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
uplatněním	uplatnění	k1gNnSc7	uplatnění
množství	množství	k1gNnSc2	množství
velmi	velmi	k6eAd1	velmi
bohatého	bohatý	k2eAgInSc2d1	bohatý
pozdně	pozdně	k6eAd1	pozdně
gotického	gotický	k2eAgInSc2d1	gotický
dekoru	dekor	k1gInSc2	dekor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Rejskova	rejskův	k2eAgInSc2d1	rejskův
návrhu	návrh	k1gInSc2	návrh
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1509	[number]	k4	1509
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
kostel	kostel	k1gInSc4	kostel
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
především	především	k6eAd1	především
dokončit	dokončit	k5eAaPmF	dokončit
bazilikálně	bazilikálně	k6eAd1	bazilikálně
převýšenou	převýšený	k2eAgFnSc4d1	převýšená
hlavní	hlavní	k2eAgFnSc4d1	hlavní
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
respektovaného	respektovaný	k2eAgMnSc2d1	respektovaný
architekta	architekt	k1gMnSc2	architekt
Benedikta	Benedikt	k1gMnSc2	Benedikt
Rejta	Rejt	k1gInSc2	Rejt
roku	rok	k1gInSc2	rok
1512	[number]	k4	1512
však	však	k9	však
znamenal	znamenat	k5eAaImAgInS	znamenat
radikální	radikální	k2eAgFnSc4d1	radikální
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Rejt	Rejt	k1gMnSc1	Rejt
kromě	kromě	k7c2	kromě
vrchní	vrchní	k2eAgFnSc2d1	vrchní
části	část	k1gFnSc2	část
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
ještě	ještě	k9	ještě
dvě	dva	k4xCgFnPc4	dva
shodně	shodně	k6eAd1	shodně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
boční	boční	k2eAgFnPc1d1	boční
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
zónou	zóna	k1gFnSc7	zóna
mezilodních	mezilodní	k2eAgFnPc2d1	mezilodní
arkád	arkáda	k1gFnPc2	arkáda
otevírají	otevírat	k5eAaImIp3nP	otevírat
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
empor	empora	k1gFnPc2	empora
založených	založený	k2eAgFnPc2d1	založená
o	o	k7c6	o
1,15	[number]	k4	1,15
m	m	kA	m
níž	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
úroveň	úroveň	k1gFnSc4	úroveň
triforia	triforium	k1gNnSc2	triforium
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
kostela	kostel	k1gInSc2	kostel
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nad	nad	k7c7	nad
původní	původní	k2eAgFnSc7d1	původní
částí	část	k1gFnSc7	část
kostela	kostel	k1gInSc2	kostel
nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc1d1	gotický
halový	halový	k2eAgInSc1d1	halový
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
zaklenutý	zaklenutý	k2eAgInSc1d1	zaklenutý
krouženou	kroužený	k2eAgFnSc7d1	kroužená
žebrovou	žebrový	k2eAgFnSc7d1	žebrová
klenbou	klenba	k1gFnSc7	klenba
a	a	k8xC	a
silně	silně	k6eAd1	silně
prosvětlený	prosvětlený	k2eAgInSc4d1	prosvětlený
množstvím	množství	k1gNnSc7	množství
rozměrných	rozměrný	k2eAgNnPc2d1	rozměrné
oken	okno	k1gNnPc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Iluzi	iluze	k1gFnSc4	iluze
"	"	kIx"	"
<g/>
kostela	kostel	k1gInSc2	kostel
nad	nad	k7c7	nad
kostelem	kostel	k1gInSc7	kostel
<g/>
"	"	kIx"	"
ještě	ještě	k6eAd1	ještě
zdůraznilo	zdůraznit	k5eAaPmAgNnS	zdůraznit
zcela	zcela	k6eAd1	zcela
nezvyklé	zvyklý	k2eNgNnSc1d1	nezvyklé
umístění	umístění	k1gNnSc1	umístění
oltářů	oltář	k1gInPc2	oltář
i	i	k8xC	i
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
etáži	etáž	k1gFnSc6	etáž
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Reidova	Reidův	k2eAgInSc2d1	Reidův
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
stavělo	stavět	k5eAaImAgNnS	stavět
i	i	k9	i
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
postupným	postupný	k2eAgInSc7d1	postupný
poklesem	pokles	k1gInSc7	pokles
těžby	těžba	k1gFnSc2	těžba
stříbra	stříbro	k1gNnSc2	stříbro
opět	opět	k6eAd1	opět
docházely	docházet	k5eAaImAgFnP	docházet
finanční	finanční	k2eAgInPc4d1	finanční
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1558	[number]	k4	1558
práce	práce	k1gFnSc2	práce
definitivně	definitivně	k6eAd1	definitivně
zastaveny	zastavit	k5eAaPmNgFnP	zastavit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
poslední	poslední	k2eAgFnSc2d1	poslední
části	část	k1gFnSc2	část
soulodí	soulodí	k1gNnSc2	soulodí
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c6	o
rozsahu	rozsah	k1gInSc6	rozsah
dalších	další	k2eAgInPc2d1	další
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
klenebních	klenební	k2eAgFnPc2d1	klenební
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Program	program	k1gInSc1	program
záchrany	záchrana	k1gFnSc2	záchrana
architektonického	architektonický	k2eAgNnSc2d1	architektonické
dědictví	dědictví	k1gNnSc2	dědictví
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Programu	program	k1gInSc2	program
záchrany	záchrana	k1gFnSc2	záchrana
architektonického	architektonický	k2eAgNnSc2d1	architektonické
dědictví	dědictví	k1gNnSc2	dědictví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995-2014	[number]	k4	1995-2014
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
památky	památka	k1gFnSc2	památka
čerpáno	čerpat	k5eAaImNgNnS	čerpat
6	[number]	k4	6
500	[number]	k4	500
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektonický	architektonický	k2eAgInSc4d1	architektonický
popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Exteriér	exteriér	k1gInSc4	exteriér
===	===	k?	===
</s>
</p>
<p>
<s>
Orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
pojata	pojat	k2eAgFnSc1d1	pojata
jako	jako	k8xC	jako
pětilodní	pětilodní	k2eAgFnSc1d1	pětilodní
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zamýšlená	zamýšlený	k2eAgNnPc4d1	zamýšlené
jako	jako	k8xC	jako
bazilikální	bazilikální	k2eAgNnPc4d1	bazilikální
trojlodí	trojlodí	k1gNnPc4	trojlodí
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
byly	být	k5eAaImAgFnP	být
hned	hned	k9	hned
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
stavby	stavba	k1gFnSc2	stavba
přistavěny	přistavěn	k2eAgFnPc4d1	přistavěna
dvě	dva	k4xCgFnPc4	dva
vnější	vnější	k2eAgFnPc4d1	vnější
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukčně	konstrukčně	k6eAd1	konstrukčně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pilířový	pilířový	k2eAgInSc4d1	pilířový
skelet	skelet	k1gInSc4	skelet
<g/>
.	.	kIx.	.
</s>
<s>
Pilíře	pilíř	k1gInPc1	pilíř
vnějších	vnější	k2eAgFnPc2d1	vnější
lodí	loď	k1gFnPc2	loď
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
z	z	k7c2	z
obvodového	obvodový	k2eAgInSc2d1	obvodový
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
původním	původní	k2eAgMnPc3d1	původní
plánům	plán	k1gInPc3	plán
byly	být	k5eAaImAgFnP	být
boční	boční	k2eAgFnPc4d1	boční
lodi	loď	k1gFnPc4	loď
hlavního	hlavní	k2eAgNnSc2d1	hlavní
trojlodí	trojlodí	k1gNnSc2	trojlodí
navýšeny	navýšen	k2eAgMnPc4d1	navýšen
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
arkádami	arkáda	k1gFnPc7	arkáda
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořen	k2eAgNnSc1d1	vytvořeno
síňové	síňový	k2eAgNnSc1d1	síňové
trojlodí	trojlodí	k1gNnSc1	trojlodí
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
zastřešené	zastřešený	k2eAgInPc1d1	zastřešený
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
trojicí	trojice	k1gFnSc7	trojice
stanových	stanový	k2eAgFnPc2d1	stanová
střech	střecha	k1gFnPc2	střecha
křivkového	křivkový	k2eAgInSc2d1	křivkový
spádu	spád	k1gInSc2	spád
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněným	zmíněný	k2eAgNnSc7d1	zmíněné
navýšením	navýšení	k1gNnSc7	navýšení
se	se	k3xPyFc4	se
kostel	kostel	k1gInSc1	kostel
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
halový	halový	k2eAgInSc4d1	halový
typ	typ	k1gInSc4	typ
<g/>
,	,	kIx,	,
bazilikálně	bazilikálně	k6eAd1	bazilikálně
převyšující	převyšující	k2eAgFnPc1d1	převyšující
pouze	pouze	k6eAd1	pouze
přistavěné	přistavěný	k2eAgFnPc1d1	přistavěná
postranní	postranní	k2eAgFnPc1d1	postranní
lodi	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
ukončen	ukončit	k5eAaPmNgInS	ukončit
protáhlým	protáhlý	k2eAgInSc7d1	protáhlý
chórem	chór	k1gInSc7	chór
s	s	k7c7	s
polygonálním	polygonální	k2eAgInSc7d1	polygonální
ochozovým	ochozový	k2eAgInSc7d1	ochozový
závěrem	závěr	k1gInSc7	závěr
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
polovině	polovina	k1gFnSc6	polovina
obestavěným	obestavěný	k2eAgInSc7d1	obestavěný
obvodovými	obvodový	k2eAgFnPc7d1	obvodová
kaplemi	kaple	k1gFnPc7	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnPc1	kaple
mají	mít	k5eAaImIp3nP	mít
valbovou	valbový	k2eAgFnSc4d1	valbová
střechu	střecha	k1gFnSc4	střecha
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednotný	jednotný	k2eAgInSc4d1	jednotný
blok	blok	k1gInSc4	blok
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
vnější	vnější	k2eAgFnPc4d1	vnější
lodi	loď	k1gFnPc4	loď
pětilodní	pětilodní	k2eAgMnSc1d1	pětilodní
a	a	k8xC	a
nad	nad	k7c4	nad
kaple	kaple	k1gFnPc4	kaple
chóru	chór	k1gInSc2	chór
je	být	k5eAaImIp3nS	být
vyveden	vyveden	k2eAgInSc1d1	vyveden
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
opěrný	opěrný	k2eAgInSc1d1	opěrný
pilířový	pilířový	k2eAgInSc1d1	pilířový
systém	systém	k1gInSc1	systém
se	s	k7c7	s
symetricky	symetricky	k6eAd1	symetricky
rozmístěnými	rozmístěný	k2eAgInPc7d1	rozmístěný
násobnými	násobný	k2eAgInPc7d1	násobný
pilíři	pilíř	k1gInPc7	pilíř
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
zdobenými	zdobený	k2eAgFnPc7d1	zdobená
poměrně	poměrně	k6eAd1	poměrně
masivními	masivní	k2eAgFnPc7d1	masivní
fiálami	fiála	k1gFnPc7	fiála
<g/>
,	,	kIx,	,
ukončenými	ukončený	k2eAgFnPc7d1	ukončená
kytkou	kytka	k1gFnSc7	kytka
<g/>
.	.	kIx.	.
</s>
<s>
Odvodnění	odvodnění	k1gNnSc1	odvodnění
je	být	k5eAaImIp3nS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
pomocí	pomocí	k7c2	pomocí
figurálně	figurálně	k6eAd1	figurálně
ztvárněných	ztvárněný	k2eAgInPc2d1	ztvárněný
chrličů	chrlič	k1gInPc2	chrlič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
klenebním	klenební	k2eAgNnSc6d1	klenební
poli	pole	k1gNnSc6	pole
přetíná	přetínat	k5eAaImIp3nS	přetínat
chór	chór	k1gInSc1	chór
příčná	příčný	k2eAgFnSc1d1	příčná
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgFnPc1d1	stejná
výšky	výška	k1gFnPc1	výška
jako	jako	k8xS	jako
ochozové	ochozový	k2eAgFnPc1d1	ochozová
kaple	kaple	k1gFnPc1	kaple
a	a	k8xC	a
šířky	šířka	k1gFnPc1	šířka
zarovnané	zarovnaný	k2eAgFnPc1d1	zarovnaná
s	s	k7c7	s
vnějšími	vnější	k2eAgFnPc7d1	vnější
bočními	boční	k2eAgFnPc7d1	boční
loděmi	loď	k1gFnPc7	loď
<g/>
,	,	kIx,	,
zdůrazněná	zdůrazněný	k2eAgFnSc1d1	zdůrazněná
navenek	navenek	k6eAd1	navenek
pouze	pouze	k6eAd1	pouze
dvojicí	dvojice	k1gFnSc7	dvojice
masivních	masivní	k2eAgInPc2d1	masivní
pilířů	pilíř	k1gInPc2	pilíř
s	s	k7c7	s
chrliči	chrlič	k1gInPc7	chrlič
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgNnSc1d1	vstupní
průčelí	průčelí	k1gNnSc1	průčelí
je	být	k5eAaImIp3nS	být
pětidílné	pětidílný	k2eAgNnSc1d1	pětidílné
<g/>
,	,	kIx,	,
pseudogotické	pseudogotický	k2eAgNnSc1d1	pseudogotické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
ho	on	k3xPp3gInSc4	on
odlehčují	odlehčovat	k5eAaImIp3nP	odlehčovat
3	[number]	k4	3
vysoká	vysoká	k1gFnSc1	vysoká
okna	okno	k1gNnSc2	okno
zakončená	zakončený	k2eAgFnSc1d1	zakončená
lomeným	lomený	k2eAgInSc7d1	lomený
obloukem	oblouk	k1gInSc7	oblouk
s	s	k7c7	s
plaménkovou	plaménkový	k2eAgFnSc7d1	plaménková
kružbou	kružba	k1gFnSc7	kružba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
symetrii	symetrie	k1gFnSc4	symetrie
narušuje	narušovat	k5eAaImIp3nS	narušovat
umístění	umístění	k1gNnSc4	umístění
oken	okno	k1gNnPc2	okno
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Interiér	interiér	k1gInSc1	interiér
===	===	k?	===
</s>
</p>
<p>
<s>
Prostor	prostor	k1gInSc1	prostor
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
příčné	příčný	k2eAgFnSc2d1	příčná
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
chóru	chór	k1gInSc2	chór
v	v	k7c6	v
první	první	k4xOgFnSc6	první
etáži	etáž	k1gFnSc6	etáž
obíhá	obíhat	k5eAaImIp3nS	obíhat
arkáda	arkáda	k1gFnSc1	arkáda
zalomených	zalomený	k2eAgInPc2d1	zalomený
profilovaných	profilovaný	k2eAgInPc2d1	profilovaný
oblouků	oblouk	k1gInPc2	oblouk
na	na	k7c6	na
hladkých	hladký	k2eAgInPc6d1	hladký
polygonálních	polygonální	k2eAgInPc6d1	polygonální
pilířích	pilíř	k1gInPc6	pilíř
s	s	k7c7	s
trnoží	trnož	k1gFnSc7	trnož
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
plynule	plynule	k6eAd1	plynule
přechází	přecházet	k5eAaImIp3nS	přecházet
žebra	žebro	k1gNnPc4	žebro
bez	bez	k7c2	bez
přípor	přípora	k1gFnPc2	přípora
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
arkádou	arkáda	k1gFnSc7	arkáda
obíhá	obíhat	k5eAaImIp3nS	obíhat
římsa	římsa	k1gFnSc1	římsa
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgNnPc4d1	následované
úzkým	úzký	k2eAgInSc7d1	úzký
pásem	pás	k1gInSc7	pás
volné	volný	k2eAgFnSc2d1	volná
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
ukončené	ukončený	k2eAgFnSc2d1	ukončená
v	v	k7c6	v
zábradlí	zábradlí	k1gNnSc6	zábradlí
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
dostavěného	dostavěný	k2eAgNnSc2d1	dostavěné
síňového	síňový	k2eAgNnSc2d1	síňové
trojlodí	trojlodí	k1gNnSc2	trojlodí
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
ochoz	ochoz	k1gInSc1	ochoz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zeď	zeď	k1gFnSc1	zeď
je	být	k5eAaImIp3nS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
velkými	velký	k2eAgInPc7d1	velký
okny	okno	k1gNnPc7	okno
s	s	k7c7	s
plaménkovou	plaménkový	k2eAgFnSc7d1	plaménková
kružbou	kružba	k1gFnSc7	kružba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kouta	kout	k1gInSc2	kout
spojení	spojení	k1gNnSc2	spojení
příčné	příčný	k2eAgFnSc2d1	příčná
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
chóru	chór	k1gInSc2	chór
ústí	ústit	k5eAaImIp3nS	ústit
dvě	dva	k4xCgNnPc4	dva
vřetenová	vřetenový	k2eAgNnPc4d1	vřetenové
schodiště	schodiště	k1gNnPc4	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Chór	chór	k1gInSc1	chór
je	být	k5eAaImIp3nS	být
zaklenut	zaklenout	k5eAaPmNgInS	zaklenout
síťovou	síťový	k2eAgFnSc7d1	síťová
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
zaklenuta	zaklenut	k2eAgNnPc4d1	zaklenuto
pozdně	pozdně	k6eAd1	pozdně
gotickou	gotický	k2eAgFnSc7d1	gotická
krouženou	kroužený	k2eAgFnSc7d1	kroužená
žebrovou	žebrový	k2eAgFnSc7d1	žebrová
klenbou	klenba	k1gFnSc7	klenba
o	o	k7c6	o
sedmi	sedm	k4xCc6	sedm
polích	pole	k1gNnPc6	pole
(	(	kIx(	(
<g/>
6	[number]	k4	6
původních	původní	k2eAgMnPc2d1	původní
a	a	k8xC	a
1	[number]	k4	1
dostavěném	dostavěný	k2eAgMnSc6d1	dostavěný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
Benedikt	Benedikt	k1gMnSc1	Benedikt
Rejt	Rejt	k1gMnSc1	Rejt
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
boční	boční	k2eAgFnPc1d1	boční
lodi	loď	k1gFnPc1	loď
jsou	být	k5eAaImIp3nP	být
zaklenuty	zaklenut	k2eAgFnPc1d1	zaklenuta
též	též	k9	též
krouženou	kroužený	k2eAgFnSc7d1	kroužená
klenbou	klenba	k1gFnSc7	klenba
<g/>
,	,	kIx,	,
jiného	jiný	k2eAgInSc2d1	jiný
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
vzor	vzor	k1gInSc1	vzor
žeber	žebro	k1gNnPc2	žebro
kroužené	kroužený	k2eAgFnSc2d1	kroužená
klenby	klenba	k1gFnSc2	klenba
vizuálně	vizuálně	k6eAd1	vizuálně
podobá	podobat	k5eAaImIp3nS	podobat
klenbě	klenba	k1gFnSc3	klenba
Vladislavského	vladislavský	k2eAgInSc2d1	vladislavský
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
konstrukční	konstrukční	k2eAgNnSc1d1	konstrukční
řešení	řešení	k1gNnSc1	řešení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
klenba	klenba	k1gFnSc1	klenba
Barbory	Barbora	k1gFnSc2	Barbora
nesena	nesen	k2eAgFnSc1d1	nesena
na	na	k7c6	na
subtilních	subtilní	k2eAgInPc6d1	subtilní
pilířích	pilíř	k1gInPc6	pilíř
a	a	k8xC	a
vnějším	vnější	k2eAgInSc7d1	vnější
opěrným	opěrný	k2eAgInSc7d1	opěrný
systémem	systém	k1gInSc7	systém
jsou	být	k5eAaImIp3nP	být
podporovány	podporován	k2eAgFnPc1d1	podporována
pouze	pouze	k6eAd1	pouze
klenby	klenba	k1gFnPc4	klenba
bočních	boční	k2eAgFnPc2d1	boční
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
klenba	klenba	k1gFnSc1	klenba
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
plošší	plochý	k2eAgMnSc1d2	plošší
a	a	k8xC	a
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
ve	v	k7c6	v
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
mohutné	mohutný	k2eAgFnPc4d1	mohutná
stěny	stěna	k1gFnPc4	stěna
a	a	k8xC	a
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
klenbu	klenba	k1gFnSc4	klenba
podepřít	podepřít	k5eAaPmF	podepřít
opěráky	opěrák	k1gInPc4	opěrák
<g/>
.	.	kIx.	.
</s>
<s>
Kutnohorská	kutnohorský	k2eAgFnSc1d1	Kutnohorská
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rozvrhu	rozvrh	k1gInSc6	rozvrh
bohatší	bohatý	k2eAgMnPc1d2	bohatší
a	a	k8xC	a
motivicky	motivicky	k6eAd1	motivicky
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
základní	základní	k2eAgInSc4d1	základní
vzorec	vzorec	k1gInSc4	vzorec
klenby	klenba	k1gFnSc2	klenba
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
barokní	barokní	k2eAgFnSc4d1	barokní
architekturu	architektura	k1gFnSc4	architektura
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
krouženou	kroužený	k2eAgFnSc4d1	kroužená
klenbu	klenba	k1gFnSc4	klenba
zřejmě	zřejmě	k6eAd1	zřejmě
viděl	vidět	k5eAaImAgMnS	vidět
Jan	Jan	k1gMnSc1	Jan
Santini	Santin	k2eAgMnPc1d1	Santin
před	před	k7c7	před
započetím	započetí	k1gNnSc7	započetí
barokní	barokní	k2eAgFnSc2d1	barokní
obnovy	obnova	k1gFnSc2	obnova
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
klášterního	klášterní	k2eAgInSc2d1	klášterní
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
Sedlci	Sedlec	k1gInSc6	Sedlec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výzdoba	výzdoba	k1gFnSc1	výzdoba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
kněžišti	kněžiště	k1gNnSc6	kněžiště
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgNnSc1d1	gotické
pastoforium	pastoforium	k1gNnSc1	pastoforium
pocházející	pocházející	k2eAgNnSc1d1	pocházející
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Matyáše	Matyáš	k1gMnSc2	Matyáš
Rejska	rejsek	k1gMnSc2	rejsek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1510	[number]	k4	1510
<g/>
.	.	kIx.	.
</s>
<s>
Chórové	chórový	k2eAgFnPc1d1	chórová
lavice	lavice	k1gFnPc1	lavice
jsou	být	k5eAaImIp3nP	být
zdobeny	zdobit	k5eAaImNgFnP	zdobit
řezbami	řezba	k1gFnPc7	řezba
řezbáře	řezbář	k1gMnSc2	řezbář
mistra	mistr	k1gMnSc2	mistr
Jakuba	Jakub	k1gMnSc2	Jakub
Nymburského	nymburský	k2eAgMnSc2d1	nymburský
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1480	[number]	k4	1480
<g/>
–	–	k?	–
<g/>
1490	[number]	k4	1490
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
a	a	k8xC	a
ojedinělé	ojedinělý	k2eAgNnSc1d1	ojedinělé
v	v	k7c6	v
našem	náš	k3xOp1gNnSc6	náš
středověkém	středověký	k2eAgNnSc6d1	středověké
umění	umění	k1gNnSc6	umění
jsou	být	k5eAaImIp3nP	být
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
pozdně	pozdně	k6eAd1	pozdně
gotické	gotický	k2eAgFnPc4d1	gotická
fresky	freska	k1gFnPc4	freska
s	s	k7c7	s
báňskou	báňský	k2eAgFnSc7d1	báňská
tematikou	tematika	k1gFnSc7	tematika
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kaplích	kaple	k1gFnPc6	kaple
(	(	kIx(	(
<g/>
Hašplířská	Hašplířský	k2eAgFnSc1d1	Hašplířský
kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
Hašplířské	Hašplířský	k2eAgInPc1d1	Hašplířský
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
práce	práce	k1gFnSc2	práce
u	u	k7c2	u
hašplu	hašpl	k1gInSc2	hašpl
neboli	neboli	k8xC	neboli
rumpálu	rumpál	k1gInSc2	rumpál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mincovní	mincovní	k2eAgFnSc6d1	mincovní
kapli	kaple	k1gFnSc6	kaple
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
středověká	středověký	k2eAgFnSc1d1	středověká
technika	technika	k1gFnSc1	technika
ražby	ražba	k1gFnSc2	ražba
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Nejunikátnější	unikátní	k2eAgFnSc1d3	nejunikátnější
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
malířská	malířský	k2eAgFnSc1d1	malířská
výzdoba	výzdoba	k1gFnSc1	výzdoba
Smíškovské	smíškovský	k2eAgFnSc2d1	smíškovský
kaple	kaple	k1gFnSc2	kaple
(	(	kIx(	(
<g/>
1485	[number]	k4	1485
<g/>
–	–	k?	–
<g/>
1492	[number]	k4	1492
<g/>
)	)	kIx)	)
zobrazující	zobrazující	k2eAgFnSc2d1	zobrazující
typologicky	typologicky	k6eAd1	typologicky
pojaté	pojatý	k2eAgFnSc2d1	pojatá
scény	scéna	k1gFnSc2	scéna
Královna	královna	k1gFnSc1	královna
ze	z	k7c2	z
Sáby	Sába	k1gFnSc2	Sába
přichází	přicházet	k5eAaImIp3nS	přicházet
ke	k	k7c3	k
králi	král	k1gMnSc3	král
Šalomounovi	Šalomoun	k1gMnSc3	Šalomoun
<g/>
,	,	kIx,	,
Trajánův	Trajánův	k2eAgInSc1d1	Trajánův
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Vidění	vidění	k1gNnSc1	vidění
Tiburtské	Tiburtský	k2eAgFnSc2d1	Tiburtský
Sybily	Sybila	k1gFnSc2	Sybila
a	a	k8xC	a
Ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
kaple	kaple	k1gFnSc2	kaple
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
tyto	tento	k3xDgInPc4	tento
výjevy	výjev	k1gInPc4	výjev
ještě	ještě	k6eAd1	ještě
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
malba	malba	k1gFnSc1	malba
"	"	kIx"	"
<g/>
litterati	litterat	k5eAaPmF	litterat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připravujících	připravující	k2eAgInPc2d1	připravující
liturgické	liturgický	k2eAgNnSc4d1	liturgické
náčiní	náčiní	k1gNnSc4	náčiní
k	k	k7c3	k
bohoslužbě	bohoslužba	k1gFnSc3	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tyto	tento	k3xDgFnPc4	tento
malby	malba	k1gFnPc4	malba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nejen	nejen	k6eAd1	nejen
velmi	velmi	k6eAd1	velmi
schopným	schopný	k2eAgMnSc7d1	schopný
tvůrcem	tvůrce	k1gMnSc7	tvůrce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
i	i	k9	i
dobře	dobře	k6eAd1	dobře
poučen	poučit	k5eAaPmNgMnS	poučit
o	o	k7c6	o
soudobé	soudobý	k2eAgFnSc6d1	soudobá
italské	italský	k2eAgFnSc6d1	italská
malířské	malířský	k2eAgFnSc6d1	malířská
produkci	produkce	k1gFnSc6	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Architektonicky	architektonicky	k6eAd1	architektonicky
nejzdobnější	zdobný	k2eAgFnSc7d3	nejzdobnější
částí	část	k1gFnSc7	část
exteriéru	exteriér	k1gInSc2	exteriér
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
kamenické	kamenický	k2eAgFnPc1d1	kamenická
a	a	k8xC	a
sochařské	sochařský	k2eAgFnPc1d1	sochařská
práce	práce	k1gFnPc1	práce
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
části	část	k1gFnSc6	část
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
opěrném	opěrný	k2eAgInSc6d1	opěrný
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
fiál	fiála	k1gFnPc2	fiála
s	s	k7c7	s
kytkou	kytka	k1gFnSc7	kytka
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
mnohá	mnohé	k1gNnPc4	mnohé
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flory	flora	k1gFnSc2	flora
<g/>
,	,	kIx,	,
satirických	satirický	k2eAgFnPc2d1	satirická
figur	figura	k1gFnPc2	figura
<g/>
,	,	kIx,	,
démonů	démon	k1gMnPc2	démon
či	či	k8xC	či
mytických	mytický	k2eAgMnPc2d1	mytický
tvorů	tvor	k1gMnPc2	tvor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BRANIŠ	BRANIŠ	kA	BRANIŠ
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
v	v	k7c6	v
Hoře	hora	k1gFnSc6	hora
Kutné	kutný	k2eAgFnPc1d1	Kutná
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
popis	popis	k1gInSc1	popis
původní	původní	k2eAgFnSc2d1	původní
stavby	stavba	k1gFnSc2	stavba
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1620	[number]	k4	1620
/	/	kIx~	/
dle	dle	k7c2	dle
souvěkých	souvěký	k2eAgInPc2d1	souvěký
pramenův	pramenův	k2eAgInSc1d1	pramenův
a	a	k8xC	a
vlastních	vlastní	k2eAgNnPc2d1	vlastní
studií	studio	k1gNnPc2	studio
napsal	napsat	k5eAaBmAgMnS	napsat
Josef	Josef	k1gMnSc1	Josef
Braniš	Braniš	k1gMnSc1	Braniš
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Jiří	Jiří	k1gMnSc1	Jiří
Zach	Zach	k1gMnSc1	Zach
<g/>
.	.	kIx.	.
</s>
<s>
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Šolc	Šolc	k1gMnSc1	Šolc
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
127	[number]	k4	127
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Neuwirth	Neuwirtha	k1gFnPc2	Neuwirtha
<g/>
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Baubeginn	Baubeginn	k1gInSc1	Baubeginn
der	drát	k5eAaImRp2nS	drát
Frohnleichnams-	Frohnleichnams-	k1gFnSc2	Frohnleichnams-
und	und	k?	und
Barbarakirche	Barbarakirche	k1gInSc1	Barbarakirche
in	in	k?	in
Kuttenberg	Kuttenberg	k1gInSc1	Kuttenberg
=	=	kIx~	=
Studien	Studien	k1gInSc1	Studien
zur	zur	k?	zur
Geschichte	Geschicht	k1gInSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
Gotik	gotik	k1gMnSc1	gotik
in	in	k?	in
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aus	Aus	k?	Aus
dem	dem	k?	dem
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Jahrgange	Jahrgange	k6eAd1	Jahrgange
der	drát	k5eAaImRp2nS	drát
"	"	kIx"	"
<g/>
Mittheilungen	Mittheilungen	k1gInSc1	Mittheilungen
des	des	k1gNnSc1	des
Vereines	Vereines	k1gInSc1	Vereines
für	für	k?	für
Geschichte	Geschicht	k1gInSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
Deutschen	Deutschen	k1gInSc1	Deutschen
in	in	k?	in
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
"	"	kIx"	"
besonders	besonders	k1gInSc1	besonders
abgedruckt	abgedruckt	k1gInSc1	abgedruckt
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
u.	u.	k?	u.
k.	k.	k?	k.
Hofdruckerei	Hofdruckerei	k1gNnPc2	Hofdruckerei
A.	A.	kA	A.
Haase	Haase	k1gFnSc2	Haase
<g/>
,	,	kIx,	,
Prag	Prag	k1gMnSc1	Prag
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
https://www.academia.edu/19772685/	[url]	k4	https://www.academia.edu/19772685/
</s>
</p>
<p>
<s>
KALINA	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
Ried	Ried	k1gInSc1	Ried
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
záalpské	záalpský	k2eAgFnSc2d1	záalpský
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
sakrální	sakrální	k2eAgFnSc1d1	sakrální
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Barbora	Barbora	k1gFnSc1	Barbora
z	z	k7c2	z
Nikomédie	Nikomédie	k1gFnSc2	Nikomédie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chrám	chrám	k1gInSc1	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Barbory	Barbora	k1gFnSc2	Barbora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Filiální	filiální	k2eAgInSc1d1	filiální
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
Biskupství	biskupství	k1gNnSc2	biskupství
královéhradecké	královéhradecký	k2eAgFnSc2d1	Královéhradecká
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
–	–	k?	–
arciděkanství	arciděkanství	k1gNnSc2	arciděkanství
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
města	město	k1gNnSc2	město
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
</s>
</p>
<p>
<s>
Památky	památka	k1gFnPc1	památka
v	v	k7c6	v
Městské	městský	k2eAgFnSc3d1	městská
památkové	památkový	k2eAgFnSc3d1	památková
rezervaci	rezervace	k1gFnSc3	rezervace
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
Krásy	krása	k1gFnPc1	krása
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
fotogalerie	fotogalerie	k1gFnSc2	fotogalerie
</s>
</p>
