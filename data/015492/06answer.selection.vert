<s>
Smlouva	smlouva	k1gFnSc1
o	o	k7c6
obchodu	obchod	k1gInSc6
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Arms	Arms	k1gInSc1
Trade	Trad	k1gInSc5
Treaty	Treat	k2eAgInPc1d1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
ATT	ATT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc4d1
dokument	dokument	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
jehož	jehož	k3xOyRp3gInSc4
vznik	vznik	k1gInSc4
hlasovalo	hlasovat	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
ve	v	k7c6
Valném	valný	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
OSN	OSN	kA
153	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
s	s	k7c7
cílem	cíl	k1gInSc7
ustavit	ustavit	k5eAaPmF
odpovědnou	odpovědný	k2eAgFnSc4d1
globální	globální	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
mezinárodním	mezinárodní	k2eAgInSc7d1
obchodem	obchod	k1gInSc7
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
<g/>
.	.	kIx.
</s>