<s>
Smlouva	smlouva	k1gFnSc1
o	o	k7c6
obchodu	obchod	k1gInSc6
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
</s>
<s>
Smlouva	smlouva	k1gFnSc1
o	o	k7c6
obchodu	obchod	k1gInSc6
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Arms	Arms	k1gInSc1
Trade	Trad	k1gInSc5
Treaty	Treat	k2eAgInPc1d1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
ATT	ATT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc4d1
dokument	dokument	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
jehož	jehož	k3xOyRp3gInSc4
vznik	vznik	k1gInSc4
hlasovalo	hlasovat	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
ve	v	k7c6
Valném	valný	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
OSN	OSN	kA
153	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
s	s	k7c7
cílem	cíl	k1gInSc7
ustavit	ustavit	k5eAaPmF
odpovědnou	odpovědný	k2eAgFnSc4d1
globální	globální	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
mezinárodním	mezinárodní	k2eAgInSc7d1
obchodem	obchod	k1gInSc7
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
například	například	k6eAd1
podle	podle	k7c2
organizace	organizace	k1gFnSc2
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc2
zabývající	zabývající	k2eAgInPc1d1
se	se	k3xPyFc4
lidskými	lidský	k2eAgNnPc7d1
právy	právo	k1gNnPc7
dosud	dosud	k6eAd1
regulován	regulovat	k5eAaImNgInS
nedostatečně	dostatečně	k6eNd1
<g/>
,	,	kIx,
a	a	k8xC
zbraně	zbraň	k1gFnPc1
tak	tak	k9
mohou	moct	k5eAaImIp3nP
proudit	proudit	k5eAaImF,k5eAaPmF
na	na	k7c4
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
bezpráví	bezpráví	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
upozorňují	upozorňovat	k5eAaImIp3nP
na	na	k7c4
vliv	vliv	k1gInSc4
nekontrolovaného	kontrolovaný	k2eNgInSc2d1
obchodu	obchod	k1gInSc2
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
na	na	k7c4
světovou	světový	k2eAgFnSc4d1
chudobu	chudoba	k1gFnSc4
a	a	k8xC
udržitelný	udržitelný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Státy	stát	k1gInPc1
podporující	podporující	k2eAgInPc1d1
přijetí	přijetí	k1gNnSc4
Smlouvy	smlouva	k1gFnSc2
o	o	k7c6
obchodu	obchod	k1gInSc6
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
se	se	k3xPyFc4
v	v	k7c6
rezoluci	rezoluce	k1gFnSc6
Valného	valný	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
OSN	OSN	kA
číslo	číslo	k1gNnSc1
61	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
shodly	shodnout	k5eAaPmAgFnP,k5eAaBmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
transfery	transfer	k1gInPc4
konvenčních	konvenční	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
jsou	být	k5eAaImIp3nP
faktorem	faktor	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
přispívá	přispívat	k5eAaImIp3nS
ke	k	k7c3
konfliktům	konflikt	k1gInPc3
<g/>
,	,	kIx,
vyhánění	vyhánění	k1gNnPc4
lidí	člověk	k1gMnPc2
z	z	k7c2
jejich	jejich	k3xOp3gInPc2
domovů	domov	k1gInPc2
<g/>
,	,	kIx,
zločinu	zločin	k1gInSc3
a	a	k8xC
terorismu	terorismus	k1gInSc3
<g/>
,	,	kIx,
a	a	k8xC
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
tudíž	tudíž	k8xC
podrývá	podrývat	k5eAaImIp3nS
mír	mír	k1gInSc4
<g/>
,	,	kIx,
bezpečnost	bezpečnost	k1gFnSc4
<g/>
,	,	kIx,
stabilitu	stabilita	k1gFnSc4
a	a	k8xC
udržitelný	udržitelný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Smlouva	smlouva	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
nezabývá	zabývat	k5eNaImIp3nS
vnitrostátním	vnitrostátní	k2eAgInSc7d1
obchodem	obchod	k1gInSc7
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
<g/>
,	,	kIx,
nepřináší	přinášet	k5eNaImIp3nS
tedy	tedy	k9
státům	stát	k1gInPc3
žádné	žádný	k3yNgFnPc4
povinnosti	povinnost	k1gFnPc4
ve	v	k7c6
věci	věc	k1gFnSc6
domácí	domácí	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
zbraní	zbraň	k1gFnPc2
nebo	nebo	k8xC
regulace	regulace	k1gFnSc2
civilního	civilní	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přijetí	přijetí	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
na	na	k7c6
základě	základ	k1gInSc6
rezoluce	rezoluce	k1gFnSc2
Valného	valný	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
OSN	OSN	kA
konzultace	konzultace	k1gFnSc1
o	o	k7c6
„	„	k?
<g/>
uskutečnitelnosti	uskutečnitelnost	k1gFnSc6
<g/>
,	,	kIx,
rozsahu	rozsah	k1gInSc6
a	a	k8xC
parametrech	parametr	k1gInPc6
<g/>
“	“	k?
budoucího	budoucí	k2eAgInSc2d1
dokumentu	dokument	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
názory	názor	k1gInPc4
zaslalo	zaslat	k5eAaPmAgNnS
OSN	OSN	kA
asi	asi	k9
100	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expertní	expertní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
složená	složený	k2eAgFnSc1d1
ze	z	k7c2
zástupců	zástupce	k1gMnPc2
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Indonésie	Indonésie	k1gFnSc2
<g/>
,	,	kIx,
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
ukončila	ukončit	k5eAaPmAgFnS
svá	svůj	k3xOyFgNnPc4
jednání	jednání	k1gNnPc4
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
zprávu	zpráva	k1gFnSc4
projedná	projednat	k5eAaPmIp3nS
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Státy	stát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jednání	jednání	k1gNnSc4
podpořily	podpořit	k5eAaPmAgFnP
</s>
<s>
Albánie	Albánie	k1gFnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
<g/>
,	,	kIx,
Benin	Benin	k1gInSc1
<g/>
,	,	kIx,
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
<g/>
,	,	kIx,
Ekvádor	Ekvádor	k1gInSc1
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Guatemala	Guatemala	k1gFnSc1
<g/>
,	,	kIx,
Guinea	Guinea	k1gFnSc1
<g/>
,	,	kIx,
Guinea-Bissau	Guinea-Bissaa	k1gFnSc4
<g/>
,	,	kIx,
Haiti	Haiti	k1gNnSc4
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
<g/>
,	,	kIx,
Irsko	Irsko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Kamerun	Kamerun	k1gInSc1
<g/>
,	,	kIx,
Keňa	Keňa	k1gFnSc1
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc1
<g/>
,	,	kIx,
Kostarika	Kostarika	k1gFnSc1
<g/>
,	,	kIx,
Republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
<g/>
,	,	kIx,
Kypr	Kypr	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
Libérie	Libérie	k1gFnSc1
<g/>
,	,	kIx,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
<g/>
,	,	kIx,
Malawi	Malawi	k1gNnSc1
<g/>
,	,	kIx,
Malta	Malta	k1gFnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
Moldávie	Moldávie	k1gFnSc1
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Nizozemí	Nizozemí	k1gNnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
,	,	kIx,
Niger	Niger	k1gInSc1
<g/>
,	,	kIx,
Nigérie	Nigérie	k1gFnSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Panama	Panama	k1gFnSc1
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc1
<g/>
,	,	kIx,
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
<g/>
,	,	kIx,
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Rwanda	Rwanda	k1gFnSc1
<g/>
,	,	kIx,
Salvador	Salvador	k1gInSc1
<g/>
,	,	kIx,
Senegal	Senegal	k1gInSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
<g/>
,	,	kIx,
Togo	Togo	k1gNnSc1
<g/>
,	,	kIx,
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
<g/>
,	,	kIx,
Uganda	Uganda	k1gFnSc1
<g/>
,	,	kIx,
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
<g/>
,	,	kIx,
Tanzanie	Tanzanie	k1gFnSc1
<g/>
,	,	kIx,
Uruguay	Uruguay	k1gFnSc1
<g/>
,	,	kIx,
Zambie	Zambie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Státy	stát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
zdržely	zdržet	k5eAaPmAgInP
hlasování	hlasování	k1gNnSc1
</s>
<s>
Bahrajn	Bahrajn	k1gNnSc1
<g/>
,	,	kIx,
Bělorusko	Bělorusko	k1gNnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Irák	Irák	k1gInSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
<g/>
,	,	kIx,
Jemen	Jemen	k1gInSc1
<g/>
,	,	kIx,
Kuvajt	Kuvajt	k1gInSc1
<g/>
,	,	kIx,
Libye	Libye	k1gFnSc1
<g/>
,	,	kIx,
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
Nepál	Nepál	k1gInSc1
<g/>
,	,	kIx,
Omán	Omán	k1gInSc1
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc1
<g/>
,	,	kIx,
Katar	katar	k1gInSc1
<g/>
,	,	kIx,
Ruská	ruský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
,	,	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
,	,	kIx,
Súdán	Súdán	k1gInSc1
<g/>
,	,	kIx,
Sýrie	Sýrie	k1gFnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
,	,	kIx,
Venezuela	Venezuela	k1gFnSc1
<g/>
,	,	kIx,
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Státy	stát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
hlasovaly	hlasovat	k5eAaImAgInP
proti	proti	k7c3
</s>
<s>
Proti	proti	k7c3
hlasovaly	hlasovat	k5eAaImAgFnP
pouze	pouze	k6eAd1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pět	pět	k4xCc1
zlatých	zlatý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
obchodu	obchod	k1gInSc2
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
</s>
<s>
Ke	k	k7c3
vzniku	vznik	k1gInSc3
Smlouvy	smlouva	k1gFnSc2
o	o	k7c6
obchodu	obchod	k1gInSc6
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
vyzývá	vyzývat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
800	#num#	k4
neziskových	ziskový	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
regulací	regulace	k1gFnSc7
obchodu	obchod	k1gInSc2
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
nebo	nebo	k8xC
související	související	k2eAgFnSc7d1
problematikou	problematika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gFnPc3
například	například	k6eAd1
získat	získat	k5eAaPmF
více	hodně	k6eAd2
než	než	k8xS
milion	milion	k4xCgInSc4
podpisů	podpis	k1gInPc2
pod	pod	k7c4
globální	globální	k2eAgFnSc4d1
fotografickou	fotografický	k2eAgFnSc4d1
petici	petice	k1gFnSc4
Milion	milion	k4xCgInSc4
Faces	Faces	k1gInSc4
podporující	podporující	k2eAgNnSc1d1
přijetí	přijetí	k1gNnSc1
ATT.	ATT.	k1gFnSc2
Připojila	připojit	k5eAaPmAgFnS
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
například	například	k6eAd1
i	i	k9
zpěvačka	zpěvačka	k1gFnSc1
Dido	Dido	k1gMnSc1
<g/>
,	,	kIx,
hudebník	hudebník	k1gMnSc1
Manu	manout	k5eAaImIp1nS
Chao	Chao	k1gMnSc1
nebo	nebo	k8xC
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
Rady	rada	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Terry	Terra	k1gFnSc2
Davis	Davis	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pět	pět	k4xCc1
zlatých	zlatý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
</s>
<s>
Koalice	koalice	k1gFnSc1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
společně	společně	k6eAd1
vytvořily	vytvořit	k5eAaPmAgFnP
kampaň	kampaň	k1gFnSc4
Control	Controla	k1gFnPc2
Arms	Armsa	k1gFnPc2
<g/>
,	,	kIx,
prosazuje	prosazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
součástí	součást	k1gFnSc7
dokumentu	dokument	k1gInSc2
stalo	stát	k5eAaPmAgNnS
„	„	k?
<g/>
pět	pět	k4xCc1
zlatých	zlatý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
obchodu	obchod	k1gInSc2
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
nich	on	k3xPp3gNnPc2
státy	stát	k1gInPc1
nesmí	smět	k5eNaImIp3nP
povolit	povolit	k5eAaPmF
žádné	žádný	k3yNgInPc4
transfery	transfer	k1gInPc4
konvenčních	konvenční	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
nebo	nebo	k8xC
munice	munice	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
<g/>
:	:	kIx,
</s>
<s>
budou	být	k5eAaImBp3nP
nebo	nebo	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
k	k	k7c3
vážnému	vážný	k2eAgNnSc3d1
porušování	porušování	k1gNnSc3
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
nebo	nebo	k8xC
mezinárodního	mezinárodní	k2eAgNnSc2d1
humanitárního	humanitární	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
;	;	kIx,
</s>
<s>
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
ohrožovat	ohrožovat	k5eAaImF
trvale	trvale	k6eAd1
udržitelný	udržitelný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
nebo	nebo	k8xC
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
přispívat	přispívat	k5eAaImF
korupci	korupce	k1gFnSc4
<g/>
;	;	kIx,
</s>
<s>
by	by	kYmCp3nP
mohly	moct	k5eAaImAgFnP
vyvolávat	vyvolávat	k5eAaImF
nebo	nebo	k8xC
zhoršovat	zhoršovat	k5eAaImF
ozbrojený	ozbrojený	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
;	;	kIx,
</s>
<s>
by	by	kYmCp3nP
mohly	moct	k5eAaImAgInP
přispívat	přispívat	k5eAaImF
k	k	k7c3
ozbrojenému	ozbrojený	k2eAgInSc3d1
zločinu	zločin	k1gInSc3
<g/>
;	;	kIx,
</s>
<s>
by	by	kYmCp3nP
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
zneužity	zneužít	k5eAaPmNgInP
k	k	k7c3
terorismu	terorismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.amnesty.org/en/campaigns/control-arms	http://www.amnesty.org/en/campaigns/control-arms	k6eAd1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.controlarms.org	www.controlarms.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://disarmament.un.org/cab/ATT/Resolution_61_89.pdf%5B%5D	http://disarmament.un.org/cab/ATT/Resolution_61_89.pdf%5B%5D	k4
<g/>
↑	↑	k?
http://disarmament.un.org/yearbook-2006/e-YB/e-YB2006/Output/VotingPat.html%5B%5D	http://disarmament.un.org/yearbook-2006/e-YB/e-YB2006/Output/VotingPat.html%5B%5D	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.controlarms.org	www.controlarms.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.controlarms.org	www.controlarms.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Video	video	k1gNnSc1
kampaně	kampaň	k1gFnSc2
Control	Controla	k1gFnPc2
Arms	Armsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Organizace	organizace	k1gFnSc1
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
</s>
