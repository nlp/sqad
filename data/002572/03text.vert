<s>
William	William	k6eAd1	William
Seward	Seward	k1gInSc1	Seward
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc1	Missouri
-	-	kIx~	-
1997	[number]	k4	1997
Lawrence	Lawrenec	k1gInSc2	Lawrenec
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc1	Kansas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
William	William	k1gInSc1	William
S.	S.	kA	S.
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
American	American	k1gMnSc1	American
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Letters	Letters	k1gInSc1	Letters
a	a	k8xC	a
Commandeur	Commandeur	k1gMnSc1	Commandeur
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Ordre	ordre	k1gInSc1	ordre
des	des	k1gNnSc1	des
Arts	Arts	k1gInSc1	Arts
et	et	k?	et
des	des	k1gNnSc7	des
Lettres	Lettresa	k1gFnPc2	Lettresa
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kultovní	kultovní	k2eAgMnSc1d1	kultovní
americký	americký	k2eAgMnSc1d1	americký
romanopisec	romanopisec	k1gMnSc1	romanopisec
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
duchovním	duchovní	k2eAgMnSc7d1	duchovní
otcem	otec	k1gMnSc7	otec
beat	beat	k1gInSc1	beat
generation	generation	k1gInSc4	generation
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
kyberpunku	kyberpunk	k1gInSc2	kyberpunk
<g/>
,	,	kIx,	,
vynálezcem	vynálezce	k1gMnSc7	vynálezce
slova	slovo	k1gNnSc2	slovo
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
literárním	literární	k2eAgMnSc7d1	literární
experimentátorem	experimentátor	k1gMnSc7	experimentátor
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgMnSc7d1	poslední
spisovatelem	spisovatel	k1gMnSc7	spisovatel
amerických	americký	k2eAgFnPc2d1	americká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgInSc2	jenž
dílu	díl	k1gInSc2	díl
hrozil	hrozit	k5eAaImAgInS	hrozit
zákaz	zákaz	k1gInSc1	zákaz
další	další	k2eAgFnSc2d1	další
publikace	publikace	k1gFnSc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
klientem	klient	k1gMnSc7	klient
tzv.	tzv.	kA	tzv.
substituční	substituční	k2eAgFnSc2d1	substituční
metadonové	metadonový	k2eAgFnSc2d1	metadonová
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
opiátová	opiátový	k2eAgFnSc1d1	opiátová
závislost	závislost	k1gFnSc1	závislost
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
částečně	částečně	k6eAd1	částečně
autobiografickém	autobiografický	k2eAgNnSc6d1	autobiografické
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
překladatelem	překladatel	k1gMnSc7	překladatel
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Rauvolf	Rauvolf	k1gMnSc1	Rauvolf
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k1gInSc1	Burroughs
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1914	[number]	k4	1914
jako	jako	k8xC	jako
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc6d1	situovaná
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Seward	Seward	k1gInSc1	Seward
Burroughs	Burroughs	k1gInSc4	Burroughs
I.	I.	kA	I.
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
Burroughs	Burroughs	k1gInSc1	Burroughs
Adding	Adding	k1gInSc1	Adding
Machine	Machin	k1gInSc5	Machin
Co	co	k3yQnSc4	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Burroughs	Burroughs	k1gInSc1	Burroughs
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
firmu	firma	k1gFnSc4	firma
držící	držící	k2eAgInSc4d1	držící
patent	patent	k1gInSc4	patent
na	na	k7c4	na
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
pokladnu	pokladna	k1gFnSc4	pokladna
<g/>
,	,	kIx,	,
postaru	postaru	k6eAd1	postaru
<g/>
:	:	kIx,	:
krámskou	krámský	k2eAgFnSc4d1	Krámská
kasu	kasa	k1gFnSc4	kasa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Laura	Laura	k1gFnSc1	Laura
Hammon	Hammon	k1gInSc1	Hammon
Lee	Lea	k1gFnSc3	Lea
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
ministra	ministr	k1gMnSc2	ministr
pocházejícího	pocházející	k2eAgMnSc2d1	pocházející
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
generála	generál	k1gMnSc2	generál
Roberta	Robert	k1gMnSc2	Robert
E.	E.	kA	E.
Lee	Lea	k1gFnSc3	Lea
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
jižanská	jižanský	k2eAgNnPc1d1	jižanské
vojska	vojsko	k1gNnPc1	vojsko
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Mortimer	Mortimer	k1gMnSc1	Mortimer
Perry	Perra	k1gFnSc2	Perra
Burroughs	Burroughs	k1gInSc1	Burroughs
vedl	vést	k5eAaImAgInS	vést
starožitnictví	starožitnictví	k1gNnSc3	starožitnictví
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c4	v
Palm	Palm	k1gInSc4	Palm
Beach	Beacha	k1gFnPc2	Beacha
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
trpěl	trpět	k5eAaImAgMnS	trpět
úzkostnými	úzkostný	k2eAgInPc7d1	úzkostný
stavy	stav	k1gInPc7	stav
a	a	k8xC	a
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
pohlavně	pohlavně	k6eAd1	pohlavně
zneužit	zneužít	k5eAaPmNgMnS	zneužít
partnerem	partner	k1gMnSc7	partner
své	svůj	k3xOyFgFnSc2	svůj
chůvy	chůva	k1gFnSc2	chůva
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vzápětí	vzápětí	k6eAd1	vzápětí
propuštěna	propustit	k5eAaPmNgFnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
styl	styl	k1gInSc1	styl
vyprávění	vyprávění	k1gNnSc2	vyprávění
plný	plný	k2eAgInSc1d1	plný
fantaskních	fantaskní	k2eAgFnPc2d1	fantaskní
příšer	příšera	k1gFnPc2	příšera
a	a	k8xC	a
prolínání	prolínání	k1gNnSc4	prolínání
reality	realita	k1gFnSc2	realita
s	s	k7c7	s
výmyslem	výmysl	k1gInSc7	výmysl
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
promítl	promítnout	k5eAaPmAgInS	promítnout
i	i	k9	i
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
budoucího	budoucí	k2eAgMnSc2d1	budoucí
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
dětství	dětství	k1gNnSc6	dětství
jej	on	k3xPp3gInSc4	on
nesmírně	smírně	k6eNd1	smírně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
kniha	kniha	k1gFnSc1	kniha
You	You	k1gFnSc1	You
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Win	Win	k1gFnSc4	Win
amerického	americký	k2eAgMnSc2d1	americký
tuláka	tulák	k1gMnSc2	tulák
a	a	k8xC	a
zlodějíčka	zlodějíček	k1gMnSc2	zlodějíček
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
přezdíval	přezdívat	k5eAaImAgMnS	přezdívat
Jack	Jack	k1gMnSc1	Jack
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
páchal	páchat	k5eAaImAgMnS	páchat
drobnou	drobný	k2eAgFnSc4d1	drobná
kriminalitu	kriminalita	k1gFnSc4	kriminalita
<g/>
,	,	kIx,	,
vloupával	vloupávat	k5eAaImAgMnS	vloupávat
se	se	k3xPyFc4	se
s	s	k7c7	s
kamarády	kamarád	k1gMnPc7	kamarád
do	do	k7c2	do
cizích	cizí	k2eAgInPc2d1	cizí
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
základní	základní	k2eAgMnSc1d1	základní
John	John	k1gMnSc1	John
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
esej	esej	k1gInSc1	esej
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
John	John	k1gMnSc1	John
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
Review	Review	k1gMnSc1	Review
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
však	však	k9	však
jako	jako	k9	jako
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
dílo	dílo	k1gNnSc4	dílo
udával	udávat	k5eAaImAgMnS	udávat
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
povídku	povídka	k1gFnSc4	povídka
Biografie	biografie	k1gFnSc2	biografie
vlka	vlk	k1gMnSc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
rodičů	rodič	k1gMnPc2	rodič
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
internátní	internátní	k2eAgFnSc6d1	internátní
The	The	k1gFnSc6	The
Los	los	k1gMnSc1	los
Alamos	Alamos	k1gMnSc1	Alamos
Ranch	Ranch	k1gMnSc1	Ranch
School	School	k1gInSc4	School
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
režim	režim	k1gInSc1	režim
školy	škola	k1gFnSc2	škola
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
noční	noční	k2eAgInSc4d1	noční
můrou	můra	k1gFnSc7	můra
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
bavilo	bavit	k5eAaImAgNnS	bavit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
střelecký	střelecký	k2eAgInSc1d1	střelecký
výcvik	výcvik	k1gInSc1	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
prožil	prožít	k5eAaPmAgMnS	prožít
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
nenaplněnou	naplněný	k2eNgFnSc4d1	nenaplněná
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
chlapci	chlapec	k1gMnSc3	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
nakonec	nakonec	k6eAd1	nakonec
vyslyšela	vyslyšet	k5eAaPmAgFnS	vyslyšet
jeho	jeho	k3xOp3gFnPc4	jeho
úpěnlivé	úpěnlivý	k2eAgFnPc4d1	úpěnlivá
prosby	prosba	k1gFnPc4	prosba
a	a	k8xC	a
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
jej	on	k3xPp3gMnSc4	on
vzala	vzít	k5eAaPmAgFnS	vzít
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
verzí	verze	k1gFnPc2	verze
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
různě	různě	k6eAd1	různě
líčených	líčený	k2eAgInPc2d1	líčený
důvodů	důvod	k1gInPc2	důvod
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
svěřil	svěřit	k5eAaPmAgMnS	svěřit
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
homosexualitou	homosexualita	k1gFnSc7	homosexualita
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
přijala	přijmout	k5eAaPmAgFnS	přijmout
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k1gMnSc1	střední
vzdělání	vzdělání	k1gNnSc2	vzdělání
ukončil	ukončit	k5eAaPmAgMnS	ukončit
na	na	k7c4	na
Taylor	Taylor	k1gInSc4	Taylor
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Harvardské	harvardský	k2eAgNnSc1d1	Harvardské
období	období	k1gNnSc1	období
přivedlo	přivést	k5eAaPmAgNnS	přivést
Burroughse	Burroughse	k1gFnPc4	Burroughse
do	do	k7c2	do
newyorské	newyorský	k2eAgFnSc2d1	newyorská
gay	gay	k1gMnSc1	gay
subkultury	subkultura	k1gFnSc2	subkultura
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ale	ale	k9	ale
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
pohrdal	pohrdat	k5eAaImAgMnS	pohrdat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Z	z	k7c2	z
místnosti	místnost	k1gFnSc2	místnost
plné	plný	k2eAgFnSc2d1	plná
uječených	uječený	k2eAgMnPc2d1	uječený
buzíků	buzík	k1gMnPc2	buzík
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
padala	padat	k5eAaImAgFnS	padat
hrůza	hrůza	k1gFnSc1	hrůza
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
autobiografické	autobiografický	k2eAgFnSc6d1	autobiografická
novele	novela	k1gFnSc6	novela
Teplouš	teplouš	k1gMnSc1	teplouš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
však	však	k9	však
všechna	všechen	k3xTgNnPc4	všechen
tehdy	tehdy	k6eAd1	tehdy
vyhlášená	vyhlášený	k2eAgNnPc4d1	vyhlášené
místa	místo	k1gNnPc4	místo
-	-	kIx~	-
Harlem	Harl	k1gInSc7	Harl
i	i	k9	i
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gInSc2	Villag
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
zámožného	zámožný	k2eAgMnSc2d1	zámožný
spolustudenta	spolustudent	k1gMnSc2	spolustudent
Richarda	Richard	k1gMnSc2	Richard
Sterna	sternum	k1gNnSc2	sternum
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
málem	málem	k6eAd1	málem
stal	stát	k5eAaPmAgInS	stát
první	první	k4xOgFnSc7	první
obětí	oběť	k1gFnSc7	oběť
jeho	jeho	k3xOp3gFnPc4	jeho
posedlosti	posedlost	k1gFnPc4	posedlost
střelnými	střelný	k2eAgFnPc7d1	střelná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Kulka	kulka	k1gFnSc1	kulka
z	z	k7c2	z
nezajištěného	zajištěný	k2eNgInSc2d1	nezajištěný
revolveru	revolver	k1gInSc2	revolver
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gNnSc4	on
minula	minout	k5eAaImAgFnS	minout
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
předjímala	předjímat	k5eAaImAgFnS	předjímat
události	událost	k1gFnPc4	událost
příští	příští	k2eAgMnSc1d1	příští
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
harvardských	harvardský	k2eAgFnPc2d1	Harvardská
studií	studie	k1gFnPc2	studie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
další	další	k2eAgFnSc1d1	další
nedochovaná	dochovaný	k2eNgFnSc1d1	nedochovaná
povídka	povídka	k1gFnSc1	povídka
Poslední	poslední	k2eAgFnSc1d1	poslední
záblesk	záblesk	k1gInSc4	záblesk
soumraku	soumrak	k1gInSc2	soumrak
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Twilight	Twilight	k1gInSc1	Twilight
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Last	Last	k1gMnSc1	Last
Gleaming	Gleaming	k1gInSc1	Gleaming
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
napsal	napsat	k5eAaBmAgInS	napsat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dávným	dávný	k2eAgMnSc7d1	dávný
přítelem	přítel	k1gMnSc7	přítel
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
Kellsem	Kells	k1gMnSc7	Kells
Elvinsem	Elvins	k1gMnSc7	Elvins
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
cynický	cynický	k2eAgInSc1d1	cynický
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
potápějícím	potápějící	k2eAgMnSc6d1	potápějící
se	se	k3xPyFc4	se
Titanicu	Titanicus	k1gInSc6	Titanicus
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
utíká	utíkat	k5eAaImIp3nS	utíkat
s	s	k7c7	s
revolverem	revolver	k1gInSc7	revolver
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
kapitán	kapitán	k1gMnSc1	kapitán
převlečený	převlečený	k2eAgInSc4d1	převlečený
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
povídku	povídka	k1gFnSc4	povídka
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
časopisu	časopis	k1gInSc2	časopis
Esquire	Esquir	k1gMnSc5	Esquir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
odmítnuti	odmítnout	k5eAaPmNgMnP	odmítnout
.	.	kIx.	.
</s>
<s>
Graduoval	graduovat	k5eAaBmAgMnS	graduovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
anglická	anglický	k2eAgFnSc1d1	anglická
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
znalcem	znalec	k1gMnSc7	znalec
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
citovat	citovat	k5eAaBmF	citovat
nekonečné	konečný	k2eNgFnPc4d1	nekonečná
pasáže	pasáž	k1gFnPc4	pasáž
zpaměti	zpaměti	k6eAd1	zpaměti
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
mu	on	k3xPp3gMnSc3	on
přiřkli	přiřknout	k5eAaPmAgMnP	přiřknout
výnos	výnos	k1gInSc4	výnos
z	z	k7c2	z
majetků	majetek	k1gInPc2	majetek
Cobbleston	Cobbleston	k1gInSc4	Cobbleston
Gardens	Gardensa	k1gFnPc2	Gardensa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nemalá	malý	k2eNgFnSc1d1	nemalá
suma	suma	k1gFnSc1	suma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
žít	žít	k5eAaImF	žít
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
a	a	k8xC	a
potloukat	potloukat	k5eAaImF	potloukat
se	se	k3xPyFc4	se
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
polosvěta	polosvět	k1gInSc2	polosvět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
fascinoval	fascinovat	k5eAaBmAgMnS	fascinovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
Harvardu	Harvard	k1gInSc2	Harvard
se	se	k3xPyFc4	se
Burroughs	Burroughs	k1gInSc1	Burroughs
dále	daleko	k6eAd2	daleko
formálně	formálně	k6eAd1	formálně
nevzdělával	vzdělávat	k5eNaImAgMnS	vzdělávat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
flirtoval	flirtovat	k5eAaImAgMnS	flirtovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
obory	obor	k1gInPc7	obor
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
studoval	studovat	k5eAaImAgMnS	studovat
medicínu	medicína	k1gFnSc4	medicína
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
byl	být	k5eAaImAgMnS	být
spíše	spíše	k9	spíše
jedním	jeden	k4xCgInSc7	jeden
homosexuálním	homosexuální	k2eAgInSc7d1	homosexuální
večírkem	večírek	k1gInSc7	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Kupoval	kupovat	k5eAaImAgMnS	kupovat
si	se	k3xPyFc3	se
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
parní	parní	k2eAgFnPc4d1	parní
lázně	lázeň	k1gFnPc4	lázeň
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
víru	vír	k1gInSc6	vír
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
divoký	divoký	k2eAgInSc1d1	divoký
rytmus	rytmus	k1gInSc1	rytmus
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
odrazil	odrazit	k5eAaPmAgInS	odrazit
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
pobýval	pobývat	k5eAaImAgMnS	pobývat
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
potkal	potkat	k5eAaPmAgInS	potkat
také	také	k9	také
s	s	k7c7	s
Ilse	Ilse	k1gFnSc7	Ilse
Klapper	Klappra	k1gFnPc2	Klappra
<g/>
,	,	kIx,	,
Židovkou	Židovka	k1gFnSc7	Židovka
utíkající	utíkající	k2eAgMnPc1d1	utíkající
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
neznali	neznat	k5eAaImAgMnP	neznat
<g/>
,	,	kIx,	,
Burroughs	Burroughs	k1gInSc1	Burroughs
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
navzdory	navzdory	k7c3	navzdory
protestům	protest	k1gInPc3	protest
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
obávající	obávající	k2eAgFnSc2d1	obávající
se	se	k3xPyFc4	se
finančních	finanční	k2eAgFnPc2d1	finanční
ztrát	ztráta	k1gFnPc2	ztráta
způsobených	způsobený	k2eAgFnPc2d1	způsobená
rozvodem	rozvod	k1gInSc7	rozvod
<g/>
,	,	kIx,	,
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
jí	jíst	k5eAaImIp3nS	jíst
tak	tak	k6eAd1	tak
získat	získat	k5eAaPmF	získat
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
vycestovat	vycestovat	k5eAaPmF	vycestovat
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
zůstali	zůstat	k5eAaPmAgMnP	zůstat
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
bombardování	bombardování	k1gNnSc6	bombardování
Pearl	Pearl	k1gInSc4	Pearl
Harboru	Harbor	k1gInSc2	Harbor
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
zařazen	zařadit	k5eAaPmNgInS	zařadit
to	ten	k3xDgNnSc1	ten
pěchotní	pěchotní	k2eAgFnPc1d1	pěchotní
třídy	třída	k1gFnPc1	třída
1	[number]	k4	1
<g/>
-A	-A	k?	-A
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gNnSc2	on
silně	silně	k6eAd1	silně
frustrovalo	frustrovat	k5eAaImAgNnS	frustrovat
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
využila	využít	k5eAaPmAgFnS	využít
své	svůj	k3xOyFgInPc4	svůj
kontakty	kontakt	k1gInPc4	kontakt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
<g/>
:	:	kIx,	:
Burroughsovým	Burroughsův	k2eAgInSc7d1	Burroughsův
velkým	velký	k2eAgInSc7d1	velký
snem	sen	k1gInSc7	sen
bylo	být	k5eAaImAgNnS	být
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
agentem	agent	k1gMnSc7	agent
nebo	nebo	k8xC	nebo
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
ne	ne	k9	ne
pěšákem	pěšák	k1gMnSc7	pěšák
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
nicméně	nicméně	k8xC	nicméně
selhal	selhat	k5eAaPmAgInS	selhat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
u	u	k7c2	u
výběrové	výběrový	k2eAgFnSc2d1	výběrová
komise	komise	k1gFnSc2	komise
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
vyučujícím	vyučující	k2eAgMnSc7d1	vyučující
z	z	k7c2	z
Los	los	k1gInSc1	los
Alamos	Alamos	k1gInSc4	Alamos
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
nenáviděli	nenávidět	k5eAaImAgMnP	nenávidět
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
prohlásit	prohlásit	k5eAaPmF	prohlásit
jej	on	k3xPp3gInSc4	on
za	za	k7c2	za
mentálně	mentálně	k6eAd1	mentálně
nezpůsobilého	způsobilý	k2eNgInSc2d1	nezpůsobilý
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předchozího	předchozí	k2eAgInSc2d1	předchozí
psychiatrického	psychiatrický	k2eAgInSc2d1	psychiatrický
nálezu	nález	k1gInSc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
Burroughs	Burroughs	k1gInSc4	Burroughs
v	v	k7c6	v
zoufalém	zoufalý	k2eAgInSc6d1	zoufalý
gestu	gest	k1gInSc6	gest
z	z	k7c2	z
neopětované	opětovaný	k2eNgFnSc2d1	neopětovaná
lásky	láska	k1gFnSc2	láska
uřízl	uříznout	k5eAaPmAgInS	uříznout
po	po	k7c6	po
samurajském	samurajský	k2eAgInSc6d1	samurajský
způsobu	způsob	k1gInSc6	způsob
poslední	poslední	k2eAgInSc4d1	poslední
článek	článek	k1gInSc4	článek
malíku	malík	k1gInSc2	malík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
stačilo	stačit	k5eAaBmAgNnS	stačit
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
diagnózy	diagnóza	k1gFnSc2	diagnóza
paranoidní	paranoidní	k2eAgFnSc2d1	paranoidní
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
čekal	čekat	k5eAaImAgMnS	čekat
v	v	k7c6	v
Jeffersonových	Jeffersonův	k2eAgFnPc6d1	Jeffersonova
kasárnách	kasárny	k1gFnPc6	kasárny
poblíž	poblíž	k7c2	poblíž
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
na	na	k7c4	na
uznání	uznání	k1gNnSc4	uznání
služby	služba	k1gFnSc2	služba
neschopným	schopný	k2eNgInPc3d1	neschopný
a	a	k8xC	a
poté	poté	k6eAd1	poté
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
-	-	kIx~	-
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
-	-	kIx~	-
nejrůznější	různý	k2eAgNnPc4d3	nejrůznější
povolání	povolání	k1gNnPc4	povolání
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
barmana	barman	k1gMnSc2	barman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
deratizátora	deratizátor	k1gMnSc4	deratizátor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
odrazí	odrazit	k5eAaPmIp3nS	odrazit
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Hubitel	hubitel	k1gMnSc1	hubitel
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Během	během	k7c2	během
chicagského	chicagský	k2eAgNnSc2d1	Chicagské
období	období	k1gNnSc2	období
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
propadl	propadnout	k5eAaPmAgMnS	propadnout
opiátové	opiátový	k2eAgFnSc3d1	opiátová
závislosti	závislost	k1gFnSc3	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
téměř	téměř	k6eAd1	téměř
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
<g/>
:	:	kIx,	:
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
prodej	prodej	k1gInSc4	prodej
od	od	k7c2	od
jakéhosi	jakýsi	k3yIgMnSc2	jakýsi
drobného	drobný	k2eAgMnSc2d1	drobný
zloděje	zloděj	k1gMnSc2	zloděj
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
vojenských	vojenský	k2eAgFnPc2d1	vojenská
morfiových	morfiový	k2eAgFnPc2d1	morfiový
syret	syreta	k1gFnPc2	syreta
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
prodal	prodat	k5eAaPmAgMnS	prodat
a	a	k8xC	a
něco	něco	k3yInSc1	něco
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
morfinu	morfin	k1gInSc2	morfin
jej	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
a	a	k8xC	a
Allena	Allen	k1gMnSc4	Allen
Ginsberga	Ginsberg	k1gMnSc4	Ginsberg
<g/>
)	)	kIx)	)
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
Herbert	Herbert	k1gMnSc1	Herbert
Huncke	Hunck	k1gInSc2	Hunck
<g/>
,	,	kIx,	,
kultovní	kultovní	k2eAgFnSc1d1	kultovní
postava	postava	k1gFnSc1	postava
Beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
těžká	těžký	k2eAgFnSc1d1	těžká
závislost	závislost	k1gFnSc1	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
Burroughs	Burroughs	k1gInSc1	Burroughs
později	pozdě	k6eAd2	pozdě
chválil	chválit	k5eAaImAgInS	chválit
apomorfinovou	apomorfinový	k2eAgFnSc4d1	apomorfinový
léčbu	léčba	k1gFnSc4	léčba
<g/>
,	,	kIx,	,
přízrak	přízrak	k1gInSc1	přízrak
fetování	fetování	k1gNnSc1	fetování
jej	on	k3xPp3gMnSc4	on
nadosmrti	nadosmrti	k6eAd1	nadosmrti
nepustil	pustit	k5eNaPmAgMnS	pustit
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
relapsu	relaps	k1gInSc6	relaps
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
halucinogenů	halucinogen	k1gInPc2	halucinogen
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
příležitostnou	příležitostný	k2eAgFnSc4d1	příležitostná
konzumaci	konzumace	k1gFnSc4	konzumace
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
psychofarmak	psychofarmakum	k1gNnPc2	psychofarmakum
vnímal	vnímat	k5eAaImAgMnS	vnímat
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
procesu	proces	k1gInSc2	proces
osvobozování	osvobozování	k1gNnSc2	osvobozování
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
faktem	fakt	k1gInSc7	fakt
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
narkomanie	narkomanie	k1gFnSc1	narkomanie
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gNnPc3	jeho
prokletím	prokletí	k1gNnPc3	prokletí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
dvou	dva	k4xCgMnPc2	dva
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
Luciena	Lucien	k1gMnSc2	Lucien
Carra	Carr	k1gMnSc2	Carr
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Kammerera	Kammerer	k1gMnSc2	Kammerer
opustil	opustit	k5eAaPmAgMnS	opustit
Chicago	Chicago	k1gNnSc4	Chicago
a	a	k8xC	a
přesunul	přesunout	k5eAaPmAgMnS	přesunout
se	se	k3xPyFc4	se
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
začal	začít	k5eAaPmAgInS	začít
Burroughs	Burroughs	k1gInSc1	Burroughs
žít	žít	k5eAaImF	žít
s	s	k7c7	s
Joan	Joano	k1gNnPc2	Joano
Vollmer-Adamsovou	Vollmer-Adamsová	k1gFnSc7	Vollmer-Adamsová
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sdílel	sdílet	k5eAaImAgMnS	sdílet
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
Kerouacem	Kerouace	k1gMnSc7	Kerouace
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
Edie	Edie	k1gFnSc7	Edie
Parkerovou	Parkerová	k1gFnSc7	Parkerová
<g/>
.	.	kIx.	.
</s>
<s>
Kerouac	Kerouac	k1gInSc1	Kerouac
a	a	k8xC	a
Burroughs	Burroughs	k1gInSc1	Burroughs
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
problémů	problém	k1gInPc2	problém
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
kvůli	kvůli	k7c3	kvůli
křivému	křivý	k2eAgNnSc3d1	křivé
svědectví	svědectví	k1gNnSc3	svědectví
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
vraždy	vražda	k1gFnSc2	vražda
<g/>
:	:	kIx,	:
Lucien	Lucien	k2eAgInSc1d1	Lucien
Carr	Carr	k1gInSc1	Carr
zabil	zabít	k5eAaPmAgInS	zabít
v	v	k7c6	v
hádce	hádka	k1gFnSc6	hádka
Davida	David	k1gMnSc2	David
Kammerera	Kammerer	k1gMnSc2	Kammerer
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k6eAd1	Burroughs
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
příležitostně	příležitostně	k6eAd1	příležitostně
prodával	prodávat	k5eAaImAgInS	prodávat
heroin	heroin	k1gInSc1	heroin
a	a	k8xC	a
stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
potřebu	potřeba	k1gFnSc4	potřeba
peněz	peníze	k1gInPc2	peníze
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
návykem	návyk	k1gInSc7	návyk
řešil	řešit	k5eAaImAgInS	řešit
i	i	k9	i
další	další	k2eAgInSc1d1	další
drobnou	drobný	k2eAgFnSc7d1	drobná
kriminalitou	kriminalita	k1gFnSc7	kriminalita
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
okrádáním	okrádání	k1gNnSc7	okrádání
opilců	opilec	k1gMnPc2	opilec
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
a	a	k8xC	a
konče	konče	k7c7	konče
falšováním	falšování	k1gNnSc7	falšování
receptů	recept	k1gInPc2	recept
na	na	k7c4	na
opiáty	opiát	k1gInPc4	opiát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Vollmerová	Vollmerová	k1gFnSc1	Vollmerová
užívala	užívat	k5eAaImAgFnS	užívat
drogy	droga	k1gFnPc4	droga
-	-	kIx~	-
její	její	k3xOp3gInSc1	její
návyk	návyk	k1gInSc1	návyk
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
amfetaminového	amfetaminový	k2eAgInSc2d1	amfetaminový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
užívala	užívat	k5eAaImAgFnS	užívat
převážně	převážně	k6eAd1	převážně
Benzedrin	Benzedrin	k1gInSc4	Benzedrin
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
závislost	závislost	k1gFnSc1	závislost
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgNnP	pohybovat
<g/>
,	,	kIx,	,
přiměly	přimět	k5eAaPmAgFnP	přimět
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
Adamse	Adams	k1gMnSc4	Adams
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
neprodleně	prodleně	k6eNd1	prodleně
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Burroughsovou	Burroughsův	k2eAgFnSc7d1	Burroughsova
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Novomanžel	novomanžel	k1gMnSc1	novomanžel
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
zatčen	zatknout	k5eAaPmNgMnS	zatknout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
St.	st.	kA	st.
Louis	louis	k1gInSc2	louis
<g/>
.	.	kIx.	.
</s>
<s>
Drogy	droga	k1gFnPc1	droga
působily	působit	k5eAaImAgFnP	působit
Vollmerové	Vollmerový	k2eAgInPc4d1	Vollmerový
problémy	problém	k1gInPc4	problém
psychotického	psychotický	k2eAgInSc2d1	psychotický
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jí	jíst	k5eAaImIp3nS	jíst
odebrána	odebrán	k2eAgFnSc1d1	odebrána
dcera	dcera	k1gFnSc1	dcera
Julie	Julie	k1gFnSc1	Julie
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
byly	být	k5eAaImAgFnP	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
Burroughs	Burroughs	k1gInSc1	Burroughs
odkroutil	odkroutit	k5eAaPmAgInS	odkroutit
své	svůj	k3xOyFgFnPc4	svůj
"	"	kIx"	"
<g/>
domácí	domácí	k1gFnPc1	domácí
vězení	vězení	k1gNnSc2	vězení
<g/>
"	"	kIx"	"
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
ženu	žena	k1gFnSc4	žena
z	z	k7c2	z
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
William	William	k1gInSc4	William
Seward	Seward	k1gInSc1	Seward
Burroughs	Burroughs	k1gInSc1	Burroughs
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Billy	Bill	k1gMnPc4	Bill
Burroughs	Burroughs	k1gInSc1	Burroughs
jistou	jistý	k2eAgFnSc4d1	jistá
proslulost	proslulost	k1gFnSc4	proslulost
jako	jako	k8xC	jako
nadaný	nadaný	k2eAgMnSc1d1	nadaný
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
o	o	k7c4	o
šestnáct	šestnáct	k4xCc4	šestnáct
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Burroughs	Burroughs	k1gInSc1	Burroughs
úzce	úzko	k6eAd1	úzko
sblížil	sblížit	k5eAaPmAgInS	sblížit
s	s	k7c7	s
kontroverzním	kontroverzní	k2eAgMnSc7d1	kontroverzní
rakouským	rakouský	k2eAgMnSc7d1	rakouský
vědcem	vědec	k1gMnSc7	vědec
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Reichem	Reichem	k?	Reichem
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
orgonovou	orgonový	k2eAgFnSc7d1	orgonová
teorií	teorie	k1gFnSc7	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Reich	Reich	k?	Reich
byl	být	k5eAaImAgMnS	být
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
prosazující	prosazující	k2eAgFnSc4d1	prosazující
myšlenku	myšlenka	k1gFnSc4	myšlenka
jakýchsi	jakýsi	k3yIgFnPc2	jakýsi
částic	částice	k1gFnPc2	částice
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
orgonů	orgon	k1gInPc2	orgon
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
orgony	orgon	k1gInPc7	orgon
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nabita	nabit	k2eAgFnSc1d1	nabita
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
částice	částice	k1gFnPc4	částice
životní	životní	k2eAgFnSc2d1	životní
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Burroughs	Burroughs	k1gInSc1	Burroughs
měl	mít	k5eAaImAgInS	mít
jeden	jeden	k4xCgInSc4	jeden
orgonový	orgonový	k2eAgInSc4d1	orgonový
akumulátor	akumulátor	k1gInSc4	akumulátor
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
začala	začít	k5eAaPmAgFnS	začít
americká	americký	k2eAgFnSc1d1	americká
FDA	FDA	kA	FDA
(	(	kIx(	(
<g/>
Food	Food	k1gMnSc1	Food
And	Anda	k1gFnPc2	Anda
Drug	Druga	k1gFnPc2	Druga
Administration	Administration	k1gInSc1	Administration
<g/>
)	)	kIx)	)
Reicha	Reicha	k1gFnSc1	Reicha
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
za	za	k7c4	za
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
orgonovými	orgonův	k2eAgInPc7d1	orgonův
akumulátory	akumulátor	k1gInPc7	akumulátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
teoretických	teoretický	k2eAgFnPc2d1	teoretická
prací	práce	k1gFnPc2	práce
spálena	spálen	k2eAgFnSc1d1	spálena
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Reicha	Reich	k1gMnSc2	Reich
se	se	k3xPyFc4	se
Burroughs	Burroughs	k1gInSc1	Burroughs
nikdy	nikdy	k6eAd1	nikdy
nevzdal	vzdát	k5eNaPmAgInS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zatčení	zatčení	k1gNnSc4	zatčení
během	během	k7c2	během
domovní	domovní	k2eAgFnSc2d1	domovní
prohlídky	prohlídka	k1gFnSc2	prohlídka
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
policie	policie	k1gFnSc1	policie
našla	najít	k5eAaPmAgFnS	najít
dopisy	dopis	k1gInPc4	dopis
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Allenem	Allen	k1gMnSc7	Allen
Ginsbergem	Ginsberg	k1gMnSc7	Ginsberg
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
marihuanou	marihuana	k1gFnSc7	marihuana
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k1gInSc1	Burroughs
utekl	utéct	k5eAaPmAgInS	utéct
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
ho	on	k3xPp3gInSc4	on
následovala	následovat	k5eAaImAgFnS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Plánovali	plánovat	k5eAaImAgMnP	plánovat
tam	tam	k6eAd1	tam
zůstat	zůstat	k5eAaPmF	zůstat
nejméně	málo	k6eAd3	málo
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
dokud	dokud	k6eAd1	dokud
nevyprší	vypršet	k5eNaPmIp3nS	vypršet
promlčecí	promlčecí	k2eAgFnSc1d1	promlčecí
lhůta	lhůta	k1gFnSc1	lhůta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
Bounty	Bounta	k1gFnSc2	Bounta
Baru	bar	k1gInSc2	bar
v	v	k7c6	v
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc1	city
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
Joan	Joana	k1gFnPc2	Joana
Vollmerovou	Vollmerová	k1gFnSc4	Vollmerová
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
jejich	jejich	k3xOp3gMnSc2	jejich
syna	syn	k1gMnSc2	syn
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
<g/>
,	,	kIx,	,
když	když	k8xS	když
chtěl	chtít	k5eAaImAgMnS	chtít
sehrát	sehrát	k5eAaPmF	sehrát
variaci	variace	k1gFnSc4	variace
na	na	k7c4	na
Viléma	Vilém	k1gMnSc4	Vilém
Tella	Tell	k1gMnSc4	Tell
a	a	k8xC	a
hodlal	hodlat	k5eAaImAgMnS	hodlat
sestřelit	sestřelit	k5eAaPmF	sestřelit
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
hlavy	hlava	k1gFnSc2	hlava
šampusku	šampuska	k1gFnSc4	šampuska
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
večírku	večírek	k1gInSc6	večírek
u	u	k7c2	u
Johna	John	k1gMnSc2	John
Halyho	Haly	k1gMnSc4	Haly
<g/>
,	,	kIx,	,
Američana	Američan	k1gMnSc4	Američan
z	z	k7c2	z
Milwaukee	Milwauke	k1gFnSc2	Milwauke
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
podal	podat	k5eAaPmAgInS	podat
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
to	ten	k3xDgNnSc1	ten
Burroughs	Burroughs	k1gInSc4	Burroughs
popřel	popřít	k5eAaPmAgMnS	popřít
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabitá	nabitý	k2eAgFnSc1d1	nabitá
zbraň	zbraň	k1gFnSc1	zbraň
upadla	upadnout	k5eAaPmAgFnS	upadnout
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
a	a	k8xC	a
samovolně	samovolně	k6eAd1	samovolně
vystřelila	vystřelit	k5eAaPmAgFnS	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
zaslal	zaslat	k5eAaPmAgMnS	zaslat
značnou	značný	k2eAgFnSc4d1	značná
sumu	suma	k1gFnSc4	suma
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
dostupného	dostupný	k2eAgMnSc4d1	dostupný
právníka	právník	k1gMnSc4	právník
i	i	k9	i
mexické	mexický	k2eAgMnPc4d1	mexický
úřady	úřada	k1gMnPc4	úřada
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c4	na
kauci	kauce	k1gFnSc4	kauce
už	už	k6eAd1	už
po	po	k7c6	po
třinácti	třináct	k4xCc2	třináct
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
verze	verze	k1gFnSc1	verze
obhajoby	obhajoba	k1gFnSc2	obhajoba
zněla	znět	k5eAaImAgFnS	znět
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
výstřelu	výstřel	k1gInSc3	výstřel
došlo	dojít	k5eAaPmAgNnS	dojít
náhodně	náhodně	k6eAd1	náhodně
při	při	k7c6	při
upadnutí	upadnutí	k1gNnSc6	upadnutí
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
očekával	očekávat	k5eAaImAgInS	očekávat
výsledek	výsledek	k1gInSc1	výsledek
balistické	balistický	k2eAgFnSc2d1	balistická
expertizy	expertiza	k1gFnSc2	expertiza
<g/>
,	,	kIx,	,
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
Burroughsův	Burroughsův	k2eAgMnSc1d1	Burroughsův
advokát	advokát	k1gMnSc1	advokát
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
(	(	kIx(	(
<g/>
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
také	také	k6eAd1	také
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
člověka	člověk	k1gMnSc4	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k1gInSc1	Burroughs
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
celou	celý	k2eAgFnSc4d1	celá
věc	věc	k1gFnSc4	věc
"	"	kIx"	"
<g/>
skončit	skončit	k5eAaPmF	skončit
<g/>
"	"	kIx"	"
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
za	za	k7c4	za
těžké	těžký	k2eAgNnSc4d1	těžké
ublížení	ublížení	k1gNnSc4	ublížení
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
smrti	smrt	k1gFnSc2	smrt
ke	k	k7c3	k
dvěma	dva	k4xCgNnPc3	dva
letům	léto	k1gNnPc3	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
byl	být	k5eAaImAgInS	být
promlčen	promlčet	k5eAaPmNgInS	promlčet
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
manželů	manžel	k1gMnPc2	manžel
Burroughsových	Burroughsová	k1gFnPc6	Burroughsová
byly	být	k5eAaImAgFnP	být
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
do	do	k7c2	do
výchovy	výchova	k1gFnSc2	výchova
prarodičům	prarodič	k1gMnPc3	prarodič
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrt	smrt	k1gFnSc1	smrt
Joan	Joana	k1gFnPc2	Joana
byla	být	k5eAaImAgFnS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělalo	udělat	k5eAaPmAgNnS	udělat
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
nucen	nutit	k5eAaImNgMnS	nutit
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
se	se	k3xPyFc4	se
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nestal	stát	k5eNaPmAgInS	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
nebýt	být	k5eNaImF	být
Joaniny	Joanin	k1gInPc4	Joanin
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Žiji	žít	k5eAaImIp1nS	žít
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
potřebě	potřeba	k1gFnSc6	potřeba
se	se	k3xPyFc4	se
opanovat	opanovat	k5eAaPmF	opanovat
<g/>
.	.	kIx.	.
</s>
<s>
Joanina	Joanin	k2eAgFnSc1d1	Joanin
smrt	smrt	k1gFnSc1	smrt
mě	já	k3xPp1nSc4	já
přivedla	přivést	k5eAaPmAgFnS	přivést
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
se	s	k7c7	s
Zlým	zlý	k1gMnSc7	zlý
Duchem	duch	k1gMnSc7	duch
a	a	k8xC	a
vrhla	vrhnout	k5eAaPmAgFnS	vrhnout
mě	já	k3xPp1nSc4	já
do	do	k7c2	do
celoživotního	celoživotní	k2eAgInSc2d1	celoživotní
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
nemám	mít	k5eNaImIp1nS	mít
jiné	jiný	k2eAgFnPc4d1	jiná
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
než	než	k8xS	než
vypsat	vypsat	k5eAaPmF	vypsat
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
začal	začít	k5eAaPmAgInS	začít
systematicky	systematicky	k6eAd1	systematicky
psát	psát	k5eAaImF	psát
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Kerouacem	Kerouaec	k1gInSc7	Kerouaec
na	na	k7c6	na
tajemném	tajemný	k2eAgInSc6d1	tajemný
románu	román	k1gInSc6	román
And	Anda	k1gFnPc2	Anda
the	the	k?	the
Hippos	Hippos	k1gMnSc1	Hippos
Were	Wer	k1gFnSc2	Wer
Boiled	Boiled	k1gMnSc1	Boiled
in	in	k?	in
Their	Their	k1gInSc1	Their
Tanks	Tanks	k1gInSc1	Tanks
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
ve	v	k7c6	v
výběru	výběr	k1gInSc6	výběr
Word	Word	kA	Word
Virus	virus	k1gInSc1	virus
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
jej	on	k3xPp3gMnSc4	on
označoval	označovat	k5eAaImAgInS	označovat
za	za	k7c4	za
nepříliš	příliš	k6eNd1	příliš
zdařilé	zdařilý	k2eAgNnSc4d1	zdařilé
dílko	dílko	k1gNnSc4	dílko
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
raná	raný	k2eAgNnPc1d1	rané
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
knihy	kniha	k1gFnSc2	kniha
Teplouš	teplouš	k1gMnSc1	teplouš
a	a	k8xC	a
Feťák	Feťák	k?	Feťák
<g/>
,	,	kIx,	,
prokazatelně	prokazatelně	k6eAd1	prokazatelně
napsal	napsat	k5eAaBmAgInS	napsat
už	už	k6eAd1	už
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
Joan	Joana	k1gFnPc2	Joana
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
Feťák	Feťák	k?	Feťák
publikovalo	publikovat	k5eAaBmAgNnS	publikovat
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
komentáři	komentář	k1gInPc7	komentář
a	a	k8xC	a
upozorněním	upozornění	k1gNnSc7	upozornění
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydavatel	vydavatel	k1gMnSc1	vydavatel
ani	ani	k8xC	ani
vědecké	vědecký	k2eAgFnPc1d1	vědecká
autority	autorita	k1gFnPc1	autorita
se	se	k3xPyFc4	se
neshodují	shodovat	k5eNaImIp3nP	shodovat
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Ace	Ace	k1gFnSc2	Ace
Original	Original	k1gFnSc2	Original
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
použil	použít	k5eAaPmAgMnS	použít
pseudonym	pseudonym	k1gInSc4	pseudonym
William	William	k1gInSc1	William
Lee	Lea	k1gFnSc3	Lea
<g/>
.	.	kIx.	.
</s>
<s>
Teplouš	teplouš	k1gMnSc1	teplouš
byl	být	k5eAaImAgMnS	být
publikován	publikován	k2eAgMnSc1d1	publikován
-	-	kIx~	-
zřejmě	zřejmě	k6eAd1	zřejmě
kvůli	kvůli	k7c3	kvůli
intimnímu	intimní	k2eAgInSc3d1	intimní
obsahu	obsah	k1gInSc3	obsah
-	-	kIx~	-
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Raný	raný	k2eAgInSc1d1	raný
Burroughs	Burroughs	k1gInSc1	Burroughs
používal	používat	k5eAaImAgInS	používat
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nazval	nazvat	k5eAaBmAgInS	nazvat
faktualismus	faktualismus	k1gInSc1	faktualismus
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtenáře	čtenář	k1gMnSc2	čtenář
nezajímá	zajímat	k5eNaImIp3nS	zajímat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
fakta	faktum	k1gNnSc2	faktum
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
holé	holý	k2eAgFnPc1d1	holá
<g/>
,	,	kIx,	,
na	na	k7c4	na
maximální	maximální	k2eAgFnSc4d1	maximální
možnou	možný	k2eAgFnSc4d1	možná
míru	míra	k1gFnSc4	míra
omezené	omezený	k2eAgNnSc1d1	omezené
sdělení	sdělení	k1gNnSc1	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
ranou	raný	k2eAgFnSc4d1	raná
lakonickou	lakonický	k2eAgFnSc4d1	lakonická
formu	forma	k1gFnSc4	forma
však	však	k9	však
opustil	opustit	k5eAaPmAgMnS	opustit
poměrně	poměrně	k6eAd1	poměrně
záhy	záhy	k6eAd1	záhy
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zastřelení	zastřelení	k1gNnSc6	zastřelení
manželky	manželka	k1gFnSc2	manželka
nakonec	nakonec	k6eAd1	nakonec
neodcestoval	odcestovat	k5eNaPmAgMnS	odcestovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
-	-	kIx~	-
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
potloukal	potloukat	k5eAaImAgMnS	potloukat
mezi	mezi	k7c7	mezi
Indiány	Indián	k1gMnPc7	Indián
i	i	k8xC	i
podivnými	podivný	k2eAgMnPc7d1	podivný
euroamerickými	euroamerický	k2eAgMnPc7d1	euroamerický
ztroskotanci	ztroskotanec	k1gMnPc7	ztroskotanec
hledaje	hledat	k5eAaImSgInS	hledat
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgFnPc6	který
slyšel	slyšet	k5eAaImAgMnS	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
probouzet	probouzet	k5eAaImF	probouzet
telepatické	telepatický	k2eAgFnPc4d1	telepatická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
stavy	stav	k1gInPc4	stav
podřízenosti	podřízenost	k1gFnSc2	podřízenost
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
jednak	jednak	k8xC	jednak
načerpal	načerpat	k5eAaPmAgMnS	načerpat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
metafor	metafora	k1gFnPc2	metafora
a	a	k8xC	a
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
Allenu	Allen	k1gMnSc3	Allen
Ginsbergovi	Ginsberg	k1gMnSc3	Ginsberg
objevuje	objevovat	k5eAaImIp3nS	objevovat
novou	nový	k2eAgFnSc4d1	nová
techniku	technika	k1gFnSc4	technika
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
techniku	technika	k1gFnSc4	technika
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Korespondence	korespondence	k1gFnSc1	korespondence
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Dopisy	dopis	k1gInPc4	dopis
o	o	k7c6	o
yagé	yagý	k2eAgFnSc6d1	yagý
(	(	kIx(	(
<g/>
San	San	k1gFnSc6	San
Francisco	Francisco	k6eAd1	Francisco
City	city	k1gNnSc1	city
Light	Lighta	k1gFnPc2	Lighta
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
divoké	divoký	k2eAgNnSc1d1	divoké
povídání	povídání	k1gNnSc1	povídání
<g/>
,	,	kIx,	,
prorocká	prorocký	k2eAgFnSc1d1	prorocká
zvěst	zvěst	k1gFnSc1	zvěst
a	a	k8xC	a
pábení	pábení	k1gNnSc1	pábení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
popírá	popírat	k5eAaImIp3nS	popírat
klasickou	klasický	k2eAgFnSc4d1	klasická
syžetovou	syžetový	k2eAgFnSc4d1	syžetová
formu	forma	k1gFnSc4	forma
kauzálních	kauzální	k2eAgFnPc2d1	kauzální
souvislostí	souvislost	k1gFnPc2	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k6eAd1	Burroughs
však	však	k9	však
přesto	přesto	k8xC	přesto
stále	stále	k6eAd1	stále
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc3	jeho
psaní	psaní	k1gNnSc4	psaní
cosi	cosi	k3yInSc1	cosi
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
bylo	být	k5eAaImAgNnS	být
další	další	k2eAgNnSc1d1	další
zlomovou	zlomový	k2eAgFnSc7d1	zlomová
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Brionem	Brion	k1gMnSc7	Brion
Gysinem	Gysin	k1gMnSc7	Gysin
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Řím	Řím	k1gInSc4	Řím
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Tangeru	Tanger	k1gInSc2	Tanger
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
proměna	proměna	k1gFnSc1	proměna
ze	z	k7c2	z
spisovatele	spisovatel	k1gMnSc2	spisovatel
v	v	k7c6	v
"	"	kIx"	"
<g/>
možná	možná	k9	možná
jediného	jediný	k2eAgMnSc2d1	jediný
žijícího	žijící	k2eAgMnSc2d1	žijící
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
génia	génius	k1gMnSc4	génius
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Norman	Norman	k1gMnSc1	Norman
Mailer	Mailer	k1gMnSc1	Mailer
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
započala	započnout	k5eAaPmAgFnS	započnout
<g/>
.	.	kIx.	.
</s>
<s>
Brion	Brion	k1gMnSc1	Brion
Gysin	Gysin	k1gMnSc1	Gysin
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
Burroughse	Burroughse	k1gFnSc1	Burroughse
metodu	metoda	k1gFnSc4	metoda
střihu	střih	k1gInSc2	střih
<g/>
.	.	kIx.	.
</s>
<s>
Napsané	napsaný	k2eAgInPc4d1	napsaný
texty	text	k1gInPc4	text
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c4	na
čtvrtky	čtvrtka	k1gFnPc4	čtvrtka
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
skládal	skládat	k5eAaImAgMnS	skládat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
-	-	kIx~	-
vznikala	vznikat	k5eAaImAgFnS	vznikat
tak	tak	k6eAd1	tak
četná	četný	k2eAgFnSc1d1	četná
zaskakující	zaskakující	k2eAgFnSc1d1	zaskakující
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
<g/>
,	,	kIx,	,
otevírala	otevírat	k5eAaImAgFnS	otevírat
se	se	k3xPyFc4	se
brána	brána	k1gFnSc1	brána
k	k	k7c3	k
podvědomí	podvědomí	k1gNnSc3	podvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jako	jako	k9	jako
lingvista	lingvista	k1gMnSc1	lingvista
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
cítil	cítit	k5eAaImAgMnS	cítit
velkou	velký	k2eAgFnSc4d1	velká
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
samotnému	samotný	k2eAgInSc3d1	samotný
<g/>
,	,	kIx,	,
cítě	cítit	k5eAaImSgMnS	cítit
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
prostředek	prostředek	k1gInSc4	prostředek
ovládání	ovládání	k1gNnSc1	ovládání
a	a	k8xC	a
obávaje	obávat	k5eAaImSgInS	obávat
se	se	k3xPyFc4	se
kontroly	kontrola	k1gFnSc2	kontrola
lidského	lidský	k2eAgInSc2d1	lidský
mozku	mozek	k1gInSc2	mozek
skrze	skrze	k?	skrze
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Kladl	klást	k5eAaImAgMnS	klást
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
fragmenty	fragment	k1gInPc1	fragment
<g/>
,	,	kIx,	,
útržky	útržek	k1gInPc1	útržek
konverzací	konverzace	k1gFnPc2	konverzace
<g/>
,	,	kIx,	,
novinové	novinový	k2eAgInPc4d1	novinový
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
citáty	citát	k1gInPc4	citát
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
smíchal	smíchat	k5eAaPmAgInS	smíchat
v	v	k7c4	v
mozaiku	mozaika	k1gFnSc4	mozaika
bez	bez	k7c2	bez
zřejmé	zřejmý	k2eAgFnSc2d1	zřejmá
ústřední	ústřední	k2eAgFnSc2d1	ústřední
ideje	idea	k1gFnSc2	idea
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
pak	pak	k6eAd1	pak
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
stránky	stránka	k1gFnSc2	stránka
takto	takto	k6eAd1	takto
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
textu	text	k1gInSc2	text
použil	použít	k5eAaPmAgInS	použít
třeba	třeba	k6eAd1	třeba
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
větu	věta	k1gFnSc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
střihy	střih	k1gInPc1	střih
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
klasických	klasický	k2eAgFnPc2d1	klasická
koláží	koláž	k1gFnPc2	koláž
a	a	k8xC	a
dada	dada	k1gNnSc2	dada
především	především	k9	především
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
nezříká	zříkat	k5eNaImIp3nS	zříkat
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
nehledá	hledat	k5eNaImIp3nS	hledat
poetiku	poetika	k1gFnSc4	poetika
nonsensu	nonsens	k1gInSc2	nonsens
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
cosi	cosi	k3yInSc1	cosi
nového	nový	k2eAgMnSc2d1	nový
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
racionálně	racionálně	k6eAd1	racionálně
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
divokou	divoký	k2eAgFnSc7d1	divoká
obrazností	obraznost	k1gFnSc7	obraznost
<g/>
,	,	kIx,	,
cynickým	cynický	k2eAgInSc7d1	cynický
humorem	humor	k1gInSc7	humor
<g/>
,	,	kIx,	,
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
výbavou	výbava	k1gFnSc7	výbava
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
všechny	všechen	k3xTgFnPc4	všechen
úrovně	úroveň	k1gFnPc4	úroveň
angličtiny	angličtina	k1gFnSc2	angličtina
od	od	k7c2	od
shakespearovské	shakespearovský	k2eAgFnSc2d1	shakespearovská
až	až	k9	až
po	po	k7c4	po
jazyk	jazyk	k1gInSc4	jazyk
ulice	ulice	k1gFnSc2	ulice
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rodí	rodit	k5eAaImIp3nS	rodit
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
obdoby	obdoba	k1gFnPc4	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
střihu	střih	k1gInSc2	střih
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
prostředkem	prostředek	k1gInSc7	prostředek
odtržení	odtržení	k1gNnSc2	odtržení
se	se	k3xPyFc4	se
od	od	k7c2	od
romantických	romantický	k2eAgFnPc2d1	romantická
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
útlocitu	útlocit	k1gInSc2	útlocit
<g/>
,	,	kIx,	,
osobních	osobní	k2eAgFnPc2d1	osobní
asociací	asociace	k1gFnPc2	asociace
a	a	k8xC	a
pout	pout	k1gInSc4	pout
k	k	k7c3	k
vlastním	vlastní	k2eAgNnPc3d1	vlastní
slovům	slovo	k1gNnPc3	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
střihu	střih	k1gInSc2	střih
taktéž	taktéž	k?	taktéž
měla	mít	k5eAaImAgFnS	mít
ohrozit	ohrozit	k5eAaPmF	ohrozit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
čemu	co	k3yRnSc3	co
autor	autor	k1gMnSc1	autor
říkal	říkat	k5eAaImAgMnS	říkat
Ovládací	ovládací	k2eAgFnPc4d1	ovládací
mašinerie	mašinerie	k1gFnPc4	mašinerie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pomocí	pomocí	k7c2	pomocí
represí	represe	k1gFnPc2	represe
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
hierarchie	hierarchie	k1gFnSc2	hierarchie
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
moc	moc	k6eAd1	moc
menšině	menšina	k1gFnSc3	menšina
a	a	k8xC	a
podrobení	podrobení	k1gNnSc3	podrobení
většiny	většina	k1gFnSc2	většina
-	-	kIx~	-
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zakotvit	zakotvit	k5eAaPmF	zakotvit
pomocí	pomocí	k7c2	pomocí
institucí	instituce	k1gFnPc2	instituce
jako	jako	k8xC	jako
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
masmédia	masmédium	k1gNnSc2	masmédium
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k6eAd1	Burroughs
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
masmédia	masmédium	k1gNnPc1	masmédium
dokáží	dokázat	k5eAaPmIp3nP	dokázat
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
běh	běh	k1gInSc4	běh
událostí	událost	k1gFnPc2	událost
různými	různý	k2eAgFnPc7d1	různá
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zdůrazňováním	zdůrazňování	k1gNnSc7	zdůrazňování
a	a	k8xC	a
úpravami	úprava	k1gFnPc7	úprava
jistých	jistý	k2eAgFnPc2d1	jistá
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
propagací	propagace	k1gFnSc7	propagace
či	či	k8xC	či
vybranou	vybraný	k2eAgFnSc7d1	vybraná
zábavou	zábava	k1gFnSc7	zábava
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Stříhání	stříhání	k1gNnSc1	stříhání
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
znamená	znamenat	k5eAaImIp3nS	znamenat
rozkouskování	rozkouskování	k1gNnSc4	rozkouskování
tohoto	tento	k3xDgInSc2	tento
škodlivého	škodlivý	k2eAgInSc2d1	škodlivý
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
paralyzování	paralyzování	k1gNnSc2	paralyzování
síly	síla	k1gFnSc2	síla
těchto	tento	k3xDgNnPc2	tento
médií	médium	k1gNnPc2	médium
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zohlednit	zohlednit	k5eAaPmF	zohlednit
<g/>
,	,	kIx,	,
že	že	k8xS	že
střihu	střih	k1gInSc2	střih
podobná	podobný	k2eAgFnSc1d1	podobná
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
děl	dít	k5eAaImAgMnS	dít
už	už	k9	už
před	před	k7c7	před
Burroughsem	Burroughso	k1gNnSc7	Burroughso
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
obeznámen	obeznámen	k2eAgMnSc1d1	obeznámen
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Zárodky	zárodek	k1gInPc1	zárodek
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ho	on	k3xPp3gNnSc4	on
proslavily	proslavit	k5eAaPmAgFnP	proslavit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
objevují	objevovat	k5eAaImIp3nP	objevovat
už	už	k6eAd1	už
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Briona	Brion	k1gMnSc2	Brion
Gysina	Gysin	k1gMnSc2	Gysin
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
známostí	známost	k1gFnSc7	známost
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
přikládal	přikládat	k5eAaImAgInS	přikládat
váhu	váha	k1gFnSc4	váha
<g/>
,	,	kIx,	,
s	s	k7c7	s
Ianem	Ianus	k1gMnSc7	Ianus
Sommervillem	Sommervill	k1gMnSc7	Sommervill
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Burroughs	Burroughs	k1gInSc1	Burroughs
svůj	svůj	k3xOyFgInSc4	svůj
marocký	marocký	k2eAgInSc4d1	marocký
pobyt	pobyt	k1gInSc4	pobyt
dramatizuje	dramatizovat	k5eAaBmIp3nS	dramatizovat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
tam	tam	k6eAd1	tam
nic	nic	k3yNnSc1	nic
nechybělo	chybět	k5eNaImAgNnS	chybět
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozval	pozvat	k5eAaPmAgMnS	pozvat
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Nesnesli	snést	k5eNaPmAgMnP	snést
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
a	a	k8xC	a
Billy	Bill	k1gMnPc4	Bill
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Palm	Palma	k1gFnPc2	Palma
Beach	Beacha	k1gFnPc2	Beacha
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
monumentální	monumentální	k2eAgInSc4d1	monumentální
rukopis	rukopis	k1gInSc4	rukopis
Smečka	smečka	k1gFnSc1	smečka
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Word	Word	kA	Word
Hoard	Hoard	k1gMnSc1	Hoard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ginsbergem	Ginsberg	k1gInSc7	Ginsberg
a	a	k8xC	a
Kerouacem	Kerouaec	k1gInSc7	Kerouaec
vybrán	vybrán	k2eAgInSc1d1	vybrán
text	text	k1gInSc1	text
knihy	kniha	k1gFnSc2	kniha
Nahý	nahý	k2eAgInSc4d1	nahý
oběd	oběd	k1gInSc4	oběd
(	(	kIx(	(
<g/>
a	a	k8xC	a
následně	následně	k6eAd1	následně
byly	být	k5eAaImAgFnP	být
zbylé	zbylý	k2eAgInPc4d1	zbylý
materiály	materiál	k1gInPc4	materiál
použity	použit	k2eAgInPc4d1	použit
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Hebká	hebký	k2eAgFnSc1d1	hebká
mašinka	mašinka	k1gFnSc1	mašinka
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lístek	lístek	k1gInSc1	lístek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
explodoval	explodovat	k5eAaBmAgInS	explodovat
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Nova	nova	k1gFnSc1	nova
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narativní	narativní	k2eAgFnSc1d1	narativní
rovina	rovina	k1gFnSc1	rovina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Nahém	nahý	k2eAgInSc6d1	nahý
obědě	oběd	k1gInSc6	oběd
poprvé	poprvé	k6eAd1	poprvé
zcela	zcela	k6eAd1	zcela
rozbita	rozbit	k2eAgFnSc1d1	rozbita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dějištěm	dějiště	k1gNnSc7	dějiště
je	být	k5eAaImIp3nS	být
Interzóna	Interzóna	k1gFnSc1	Interzóna
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
halucinovaně	halucinovaně	k6eAd1	halucinovaně
a	a	k8xC	a
s	s	k7c7	s
jízlivou	jízlivý	k2eAgFnSc7d1	jízlivá
nadsázkou	nadsázka	k1gFnSc7	nadsázka
pojatá	pojatý	k2eAgFnSc1d1	pojatá
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
zóna	zóna	k1gFnSc1	zóna
Tanger	Tangra	k1gFnPc2	Tangra
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
promítají	promítat	k5eAaImIp3nP	promítat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
všechny	všechen	k3xTgFnPc4	všechen
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
Burroughs	Burroughs	k1gInSc1	Burroughs
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
i	i	k8xC	i
zcela	zcela	k6eAd1	zcela
fiktivní	fiktivní	k2eAgInPc1d1	fiktivní
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
lze	lze	k6eAd1	lze
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
kompletní	kompletní	k2eAgFnSc4d1	kompletní
mytologii	mytologie	k1gFnSc4	mytologie
-	-	kIx~	-
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
mytologii	mytologie	k1gFnSc6	mytologie
jsou	být	k5eAaImIp3nP	být
aplikovány	aplikovat	k5eAaBmNgFnP	aplikovat
feťácké	feťácký	k2eAgFnPc1d1	feťácká
zkušenosti	zkušenost	k1gFnPc1	zkušenost
podle	podle	k7c2	podle
způsobů	způsob	k1gInPc2	způsob
hmyzí	hmyzí	k2eAgFnSc2d1	hmyzí
říše	říš	k1gFnSc2	říš
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
postavy	postava	k1gFnPc1	postava
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
mají	mít	k5eAaImIp3nP	mít
drogové	drogový	k2eAgInPc1d1	drogový
návyky	návyk	k1gInPc1	návyk
<g/>
,	,	kIx,	,
sadistické	sadistický	k2eAgFnPc1d1	sadistická
a	a	k8xC	a
kyberpunkové	kyberpunkový	k2eAgFnPc1d1	kyberpunková
scény	scéna	k1gFnPc1	scéna
se	se	k3xPyFc4	se
prolínají	prolínat	k5eAaImIp3nP	prolínat
s	s	k7c7	s
halucinovanou	halucinovaný	k2eAgFnSc7d1	halucinovaný
pornografií	pornografie	k1gFnSc7	pornografie
<g/>
,	,	kIx,	,
lyrikou	lyrika	k1gFnSc7	lyrika
i	i	k8xC	i
sžíravou	sžíravý	k2eAgFnSc7d1	sžíravá
satirou	satira	k1gFnSc7	satira
<g/>
.	.	kIx.	.
</s>
<s>
Burroughsovi	Burroughsův	k2eAgMnPc1d1	Burroughsův
příznivci	příznivec	k1gMnPc1	příznivec
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
proroctvích	proroctví	k1gNnPc6	proroctví
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Nahý	nahý	k2eAgInSc4d1	nahý
oběd	oběd	k1gInSc4	oběd
poprvé	poprvé	k6eAd1	poprvé
publikoval	publikovat	k5eAaBmAgMnS	publikovat
majitel	majitel	k1gMnSc1	majitel
pornografického	pornografický	k2eAgNnSc2d1	pornografické
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Olympia	Olympia	k1gFnSc1	Olympia
Press	Press	k1gInSc1	Press
Maurice	Maurika	k1gFnSc6	Maurika
Girodia	Girodium	k1gNnSc2	Girodium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
zpřístupnilo	zpřístupnit	k5eAaPmAgNnS	zpřístupnit
světu	svět	k1gInSc3	svět
Jamese	Jamese	k1gFnSc1	Jamese
Joyce	Joyce	k1gFnSc2	Joyce
i	i	k8xC	i
Vladimira	Vladimiro	k1gNnSc2	Vladimiro
Nabokova	Nabokův	k2eAgFnSc1d1	Nabokova
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
značnou	značný	k2eAgFnSc4d1	značná
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
intelektuálních	intelektuální	k2eAgInPc6d1	intelektuální
kruzích	kruh	k1gInPc6	kruh
Evropy	Evropa	k1gFnSc2	Evropa
ještě	ještě	k9	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
obscenitu	obscenita	k1gFnSc4	obscenita
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zakázat	zakázat	k5eAaPmF	zakázat
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Londýn	Londýn	k1gInSc4	Londýn
a	a	k8xC	a
navázal	navázat	k5eAaPmAgInS	navázat
tam	tam	k6eAd1	tam
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
Anthony	Anthon	k1gMnPc7	Anthon
Burgessem	Burgess	k1gMnSc7	Burgess
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
autorem	autor	k1gMnSc7	autor
slavné	slavný	k2eAgFnSc2d1	slavná
knihy	kniha	k1gFnSc2	kniha
Mechanický	mechanický	k2eAgInSc1d1	mechanický
pomeranč	pomeranč	k1gInSc1	pomeranč
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozdějších	pozdní	k2eAgInPc2d2	pozdější
londýnských	londýnský	k2eAgInPc2d1	londýnský
pobytů	pobyt	k1gInPc2	pobyt
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
Burroughsovy	Burroughsův	k2eAgMnPc4d1	Burroughsův
podporovatele	podporovatel	k1gMnPc4	podporovatel
(	(	kIx(	(
<g/>
platil	platit	k5eAaImAgMnS	platit
mu	on	k3xPp3gInSc3	on
nájem	nájem	k1gInSc4	nájem
<g/>
)	)	kIx)	)
mj.	mj.	kA	mj.
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gMnSc2	McCartnea
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
státu	stát	k1gInSc2	stát
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nahý	nahý	k2eAgInSc4d1	nahý
oběd	oběd	k1gInSc4	oběd
není	být	k5eNaImIp3nS	být
obscenita	obscenita	k1gFnSc1	obscenita
<g/>
,	,	kIx,	,
a	a	k8xC	a
takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
skončil	skončit	k5eAaPmAgInS	skončit
poslední	poslední	k2eAgMnSc1d1	poslední
-	-	kIx~	-
a	a	k8xC	a
možná	možná	k9	možná
největší	veliký	k2eAgInSc1d3	veliký
-	-	kIx~	-
proces	proces	k1gInSc1	proces
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
lavici	lavice	k1gFnSc6	lavice
obžalovaných	obžalovaná	k1gFnPc2	obžalovaná
seděla	sedět	k5eAaImAgFnS	sedět
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Představme	představit	k5eAaPmRp1nP	představit
si	se	k3xPyFc3	se
životní	životní	k2eAgFnSc4d1	životní
formu	forma	k1gFnSc4	forma
B	B	kA	B
parazitující	parazitující	k2eAgFnSc1d1	parazitující
na	na	k7c6	na
životní	životní	k2eAgFnSc6d1	životní
formě	forma	k1gFnSc6	forma
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
říká	říkat	k5eAaImIp3nS	říkat
Burroughs	Burroughs	k1gInSc4	Burroughs
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
předjímá	předjímat	k5eAaImIp3nS	předjímat
tak	tak	k6eAd1	tak
memetiku	memetika	k1gFnSc4	memetika
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
je	být	k5eAaImIp3nS	být
mentální	mentální	k2eAgFnSc1d1	mentální
nákaza	nákaza	k1gFnSc1	nákaza
<g/>
,	,	kIx,	,
možná	možná	k9	možná
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
šířící	šířící	k2eAgFnSc1d1	šířící
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
intelektuála	intelektuál	k1gMnSc2	intelektuál
je	být	k5eAaImIp3nS	být
odhalovat	odhalovat	k5eAaImF	odhalovat
působení	působení	k1gNnSc4	působení
tohoto	tento	k3xDgInSc2	tento
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
viru	vir	k1gInSc2	vir
<g/>
,	,	kIx,	,
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
jeho	on	k3xPp3gMnSc4	on
nositele	nositel	k1gMnSc4	nositel
a	a	k8xC	a
zastavit	zastavit	k5eAaPmF	zastavit
mimozemskou	mimozemský	k2eAgFnSc4d1	mimozemská
invazi	invaze	k1gFnSc4	invaze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Místě	místo	k1gNnSc6	místo
slepých	slepý	k2eAgFnPc2d1	slepá
cest	cesta	k1gFnPc2	cesta
Burroughs	Burroughs	k1gInSc4	Burroughs
agituje	agitovat	k5eAaImIp3nS	agitovat
s	s	k7c7	s
nesmírnou	smírný	k2eNgFnSc7d1	nesmírná
přesvědčivostí	přesvědčivost	k1gFnSc7	přesvědčivost
(	(	kIx(	(
<g/>
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
přihlásí	přihlásit	k5eAaPmIp3nS	přihlásit
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
hlásit	hlásit	k5eAaImF	hlásit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
kopce	kopec	k1gInPc4	kopec
a	a	k8xC	a
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
do	do	k7c2	do
Západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kdokoliv	kdokoliv	k3yInSc1	kdokoliv
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
postaví	postavit	k5eAaPmIp3nS	postavit
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
ZABIJTE	zabít	k5eAaPmRp2nP	zabít
HO	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
trestaneckou	trestanecký	k2eAgFnSc7d1	trestanecká
kolonií	kolonie	k1gFnSc7	kolonie
a	a	k8xC	a
nikomu	nikdo	k3yNnSc3	nikdo
není	být	k5eNaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
ji	on	k3xPp3gFnSc4	on
opustit	opustit	k5eAaPmF	opustit
-	-	kIx~	-
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
ven	ven	k6eAd1	ven
proto	proto	k8xC	proto
budete	být	k5eAaImBp2nP	být
muset	muset	k5eAaImF	muset
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Zabijte	zabít	k5eAaPmRp2nP	zabít
stráže	stráž	k1gFnPc4	stráž
a	a	k8xC	a
jděte	jít	k5eAaImRp2nP	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
téže	týž	k3xTgFnSc2	týž
knihy	kniha	k1gFnSc2	kniha
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jistěže	jistěže	k9	jistěže
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
ale	ale	k8xC	ale
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
bacilonosiči	bacilonosič	k1gMnPc7	bacilonosič
<g/>
,	,	kIx,	,
nositeli	nositel	k1gMnPc7	nositel
viru	vir	k1gInSc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
lze	lze	k6eAd1	lze
ovládnout	ovládnout	k5eAaPmF	ovládnout
žlutou	žlutý	k2eAgFnSc4d1	žlutá
zimnici	zimnice	k1gFnSc4	zimnice
<g/>
?	?	kIx.	?
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgMnSc1d1	nutný
vybít	vybít	k5eAaPmF	vybít
moskyty	moskyt	k1gMnPc4	moskyt
<g/>
,	,	kIx,	,
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
teďko	teďko	k?	teďko
poslouchejte	poslouchat	k5eAaImRp2nP	poslouchat
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
bacilonosiči	bacilonosič	k1gMnPc1	bacilonosič
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgMnPc1d2	silnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Krucinál	Krucinál	k?	Krucinál
<g/>
,	,	kIx,	,
jen	jen	k9	jen
se	se	k3xPyFc4	se
koukněte	kouknout	k5eAaPmRp2nP	kouknout
na	na	k7c4	na
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
nedílná	dílný	k2eNgFnSc1d1	nedílná
součást	součást	k1gFnSc1	součást
Programu	program	k1gInSc2	program
sráčocidy	sráčocida	k1gFnSc2	sráčocida
budou	být	k5eAaImBp3nP	být
dominantní	dominantní	k2eAgMnPc1d1	dominantní
bacilonosiči	bacilonosič	k1gMnPc1	bacilonosič
zaměřeni	zaměřit	k5eAaPmNgMnP	zaměřit
a	a	k8xC	a
zabiti	zabít	k5eAaPmNgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Pánové	pán	k1gMnPc1	pán
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
a	a	k8xC	a
frekventanti	frekventant	k1gMnPc1	frekventant
kurzu	kurz	k1gInSc2	kurz
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přijdou	přijít	k5eAaPmIp3nP	přijít
po	po	k7c4	po
vás	vy	k3xPp2nPc4	vy
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
elitou	elita	k1gFnSc7	elita
<g/>
,	,	kIx,	,
řídícími	řídící	k2eAgInPc7d1	řídící
mozky	mozek	k1gInPc7	mozek
úžasných	úžasný	k2eAgInPc2d1	úžasný
SS	SS	kA	SS
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
W.	W.	kA	W.
S.	S.	kA	S.
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
,	,	kIx,	,
archanděl	archanděl	k1gMnSc1	archanděl
apokalypsy	apokalypsa	k1gFnSc2	apokalypsa
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Beat	beat	k1gInSc4	beat
generation	generation	k1gInSc4	generation
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
byla	být	k5eAaImAgFnS	být
Gertrude	Gertrud	k1gInSc5	Gertrud
Steinová	Steinová	k1gFnSc1	Steinová
pro	pro	k7c4	pro
Ztracenou	ztracený	k2eAgFnSc4d1	ztracená
generaci	generace	k1gFnSc4	generace
-	-	kIx~	-
teoretikem	teoretik	k1gMnSc7	teoretik
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Očistil	očistit	k5eAaPmAgMnS	očistit
své	svůj	k3xOyFgNnSc4	svůj
vnímání	vnímání	k1gNnSc4	vnímání
a	a	k8xC	a
použil	použít	k5eAaPmAgMnS	použít
metodu	metoda	k1gFnSc4	metoda
střihu	střih	k1gInSc2	střih
jako	jako	k8xC	jako
způsob	způsob	k1gInSc4	způsob
objektivního	objektivní	k2eAgNnSc2d1	objektivní
reprodukování	reprodukování	k1gNnSc2	reprodukování
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
často	často	k6eAd1	často
nechápali	chápat	k5eNaImAgMnP	chápat
jeho	jeho	k3xOp3gInSc4	jeho
přínos	přínos	k1gInSc4	přínos
a	a	k8xC	a
posuzovali	posuzovat	k5eAaImAgMnP	posuzovat
jeho	jeho	k3xOp3gFnPc4	jeho
knihy	kniha	k1gFnPc4	kniha
jako	jako	k8xS	jako
nepodařenou	podařený	k2eNgFnSc4d1	nepodařená
sci-fi	scii	k1gFnSc4	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Marshall	Marshall	k1gMnSc1	Marshall
MacLuhan	MacLuhan	k1gMnSc1	MacLuhan
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
kritizovat	kritizovat	k5eAaImF	kritizovat
oděv	oděv	k1gInSc4	oděv
a	a	k8xC	a
slovní	slovní	k2eAgInSc4d1	slovní
projev	projev	k1gInSc4	projev
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
klepe	klepat	k5eAaImIp3nS	klepat
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nás	my	k3xPp1nPc4	my
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
hoří	hořet	k5eAaImIp3nS	hořet
střecha	střecha	k1gFnSc1	střecha
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jakkoli	jakkoli	k8xS	jakkoli
obsah	obsah	k1gInSc1	obsah
Burroughsova	Burroughsův	k2eAgNnSc2d1	Burroughsovo
díla	dílo	k1gNnSc2	dílo
zůstane	zůstat	k5eAaPmIp3nS	zůstat
obscénní	obscénní	k2eAgFnSc1d1	obscénní
pro	pro	k7c4	pro
puritány	puritán	k1gMnPc4	puritán
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
poselství	poselství	k1gNnSc4	poselství
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
rozluštitelné	rozluštitelný	k2eAgNnSc1d1	rozluštitelné
a	a	k8xC	a
varující	varující	k2eAgNnSc1d1	varující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gNnSc3	on
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
pomohl	pomoct	k5eAaPmAgMnS	pomoct
najít	najít	k5eAaPmF	najít
práci	práce	k1gFnSc4	práce
učitele	učitel	k1gMnSc2	učitel
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
navázal	navázat	k5eAaPmAgMnS	navázat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
s	s	k7c7	s
Andy	Anda	k1gFnSc2	Anda
Warholem	Warhol	k1gMnSc7	Warhol
<g/>
,	,	kIx,	,
Johnem	John	k1gMnSc7	John
Giornem	Giorn	k1gInSc7	Giorn
<g/>
,	,	kIx,	,
Patti	Patt	k1gMnPc1	Patt
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Susan	Susan	k1gMnSc1	Susan
Sontag	Sontag	k1gMnSc1	Sontag
<g/>
,	,	kIx,	,
Dennisem	Dennis	k1gInSc7	Dennis
Hopperem	Hopper	k1gMnSc7	Hopper
<g/>
,	,	kIx,	,
Terry	Terra	k1gMnSc2	Terra
Southernem	Southern	k1gInSc7	Southern
a	a	k8xC	a
Mickem	Micek	k1gMnSc7	Micek
Jaggerem	Jagger	k1gMnSc7	Jagger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
scientologické	scientologický	k2eAgFnSc2d1	Scientologická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
záhy	záhy	k6eAd1	záhy
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
bezprecedentní	bezprecedentní	k2eAgFnSc1d1	bezprecedentní
kritika	kritika	k1gFnSc1	kritika
této	tento	k3xDgFnSc2	tento
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
úvod	úvod	k1gInSc4	úvod
napsaný	napsaný	k2eAgInSc4d1	napsaný
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
Roberta	Robert	k1gMnSc2	Robert
Kaufmana	Kaufman	k1gMnSc2	Kaufman
Inside	Insid	k1gInSc5	Insid
Scientology	scientolog	k1gMnPc4	scientolog
vyprovokovala	vyprovokovat	k5eAaPmAgFnS	vyprovokovat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
bitvu	bitva	k1gFnSc4	bitva
s	s	k7c7	s
příznivci	příznivec	k1gMnPc7	příznivec
scientologie	scientologie	k1gFnSc2	scientologie
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
také	také	k6eAd1	také
dianetika	dianetik	k1gMnSc2	dianetik
<g/>
)	)	kIx)	)
vedenou	vedený	k2eAgFnSc4d1	vedená
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
jater	játra	k1gNnPc2	játra
jeho	jeho	k3xOp3gNnPc2	jeho
syn	syn	k1gMnSc1	syn
Billy	Bill	k1gMnPc7	Bill
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
poměrně	poměrně	k6eAd1	poměrně
dobrým	dobrý	k2eAgMnSc7d1	dobrý
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Billy	Bill	k1gMnPc4	Bill
na	na	k7c6	na
otce	otka	k1gFnSc6	otka
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
magazínu	magazín	k1gInSc2	magazín
Esquire	Esquir	k1gInSc5	Esquir
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
otcovy	otcův	k2eAgInPc1d1	otcův
způsoby	způsob	k1gInPc1	způsob
podepsaly	podepsat	k5eAaPmAgInP	podepsat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vlastním	vlastní	k2eAgInSc6d1	vlastní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
mj.	mj.	kA	mj.
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Tangeru	Tanger	k1gInSc6	Tanger
sexuálně	sexuálně	k6eAd1	sexuálně
obtěžován	obtěžovat	k5eAaImNgMnS	obtěžovat
otcovými	otcův	k2eAgMnPc7d1	otcův
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
jej	on	k3xPp3gInSc4	on
k	k	k7c3	k
podobným	podobný	k2eAgNnPc3d1	podobné
tvrzením	tvrzení	k1gNnPc3	tvrzení
vedla	vést	k5eAaImAgFnS	vést
narkomanie	narkomanie	k1gFnSc2	narkomanie
a	a	k8xC	a
potřeba	potřeba	k6eAd1	potřeba
vydělat	vydělat	k5eAaPmF	vydělat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
zodpovědět	zodpovědět	k5eAaPmF	zodpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Skutečností	skutečnost	k1gFnSc7	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
dcera	dcera	k1gFnSc1	dcera
Jacka	Jacek	k1gMnSc2	Jacek
Kerouaca	Kerouacus	k1gMnSc2	Kerouacus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
živila	živit	k5eAaImAgFnS	živit
prostitucí	prostituce	k1gFnSc7	prostituce
a	a	k8xC	a
ani	ani	k8xC	ani
dalším	další	k2eAgMnPc3d1	další
lidem	člověk	k1gMnPc3	člověk
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
představitelů	představitel	k1gMnPc2	představitel
beat-generation	beateneration	k1gInSc1	beat-generation
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
nevedlo	vést	k5eNaImAgNnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
všemu	všecek	k3xTgNnSc3	všecek
napsal	napsat	k5eAaPmAgMnS	napsat
W.	W.	kA	W.
S.	S.	kA	S.
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
II	II	kA	II
<g/>
.	.	kIx.	.
k	k	k7c3	k
souhrnnému	souhrnný	k2eAgNnSc3d1	souhrnné
vydání	vydání	k1gNnSc3	vydání
synova	synův	k2eAgNnSc2d1	synovo
díla	dílo	k1gNnSc2	dílo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
doslov	doslov	k1gInSc1	doslov
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Burroughs	Burroughs	k1gInSc1	Burroughs
stal	stát	k5eAaPmAgInS	stát
kulturním	kulturní	k2eAgInSc7d1	kulturní
gigantem	gigant	k1gInSc7	gigant
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
nejširší	široký	k2eAgFnSc7d3	nejširší
škálu	škála	k1gFnSc4	škála
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
od	od	k7c2	od
hip	hip	k0	hip
hopových	hopových	k2eAgFnSc1d1	hopových
Disposable	Disposable	k1gFnSc1	Disposable
Heroes	Heroesa	k1gFnPc2	Heroesa
of	of	k?	of
Hiphoprisy	Hiphopris	k1gInPc1	Hiphopris
přes	přes	k7c4	přes
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
po	po	k7c4	po
skupinu	skupina	k1gFnSc4	skupina
Ministry	ministr	k1gMnPc4	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Guse	Gus	k1gFnSc2	Gus
van	vana	k1gFnPc2	vana
Santa	Santa	k1gMnSc1	Santa
Drugstore	Drugstor	k1gMnSc5	Drugstor
Cowboy	Cowbo	k2eAgInPc1d1	Cowbo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
kněze	kněz	k1gMnSc4	kněz
závislého	závislý	k2eAgMnSc4d1	závislý
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
ctitele	ctitel	k1gMnPc4	ctitel
se	se	k3xPyFc4	se
počítal	počítat	k5eAaImAgMnS	počítat
Bono	bona	k1gFnSc5	bona
Vox	Vox	k1gMnSc7	Vox
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
skupina	skupina	k1gFnSc1	skupina
Sonic	Sonic	k1gMnSc1	Sonic
Youth	Youth	k1gMnSc1	Youth
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
jako	jako	k9	jako
recitátor	recitátor	k1gMnSc1	recitátor
na	na	k7c4	na
cd	cd	kA	cd
Dead	Dead	k1gInSc4	Dead
City	City	k1gFnSc2	City
Radio	radio	k1gNnSc4	radio
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
texty	text	k1gInPc4	text
k	k	k7c3	k
experimentálnímu	experimentální	k2eAgNnSc3d1	experimentální
albu	album	k1gNnSc3	album
The	The	k1gFnSc2	The
Black	Black	k1gInSc1	Black
Rider	Rider	k1gInSc1	Rider
Toma	Tom	k1gMnSc2	Tom
Waitse	Waits	k1gMnSc2	Waits
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
American	Americana	k1gFnPc2	Americana
Academy	Academa	k1gFnSc2	Academa
and	and	k?	and
Institute	institut	k1gInSc5	institut
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Letters	Letters	k1gInSc1	Letters
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
Lawrence	Lawrence	k1gFnSc2	Lawrence
v	v	k7c6	v
Kansasu	Kansas	k1gInSc6	Kansas
se	se	k3xPyFc4	se
Burroughs	Burroughs	k1gInSc1	Burroughs
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
a	a	k8xC	a
dožil	dožít	k5eAaPmAgMnS	dožít
tam	tam	k6eAd1	tam
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
mnoha	mnoho	k4c2	mnoho
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hrály	hrát	k5eAaImAgFnP	hrát
značnou	značný	k2eAgFnSc4d1	značná
roli	role	k1gFnSc4	role
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
soukromé	soukromý	k2eAgFnSc6d1	soukromá
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
a	a	k8xC	a
velkých	velký	k2eAgInPc2d1	velký
demižonů	demižon	k1gInPc2	demižon
laciné	laciný	k2eAgFnSc2d1	laciná
vodky	vodka	k1gFnSc2	vodka
z	z	k7c2	z
blízkého	blízký	k2eAgInSc2d1	blízký
supermarketu	supermarket	k1gInSc2	supermarket
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
realizovat	realizovat	k5eAaBmF	realizovat
jako	jako	k9	jako
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
když	když	k8xS	když
objevil	objevit	k5eAaPmAgMnS	objevit
techniku	technika	k1gFnSc4	technika
brokovnicové	brokovnicový	k2eAgFnSc2d1	brokovnicový
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
do	do	k7c2	do
plechovek	plechovka	k1gFnPc2	plechovka
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
před	před	k7c7	před
plátnem	plátno	k1gNnSc7	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
-	-	kIx~	-
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
neuvěření	neuvěření	k1gNnSc3	neuvěření
-	-	kIx~	-
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
tantiémách	tantiéma	k1gFnPc6	tantiéma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
osobnostmi	osobnost	k1gFnPc7	osobnost
opiátové	opiátový	k2eAgFnSc2d1	opiátová
subkultury	subkultura	k1gFnSc2	subkultura
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Nickem	Nicek	k1gMnSc7	Nicek
Cavem	Cav	k1gMnSc7	Cav
a	a	k8xC	a
Tomem	Tom	k1gMnSc7	Tom
Waitsem	Waits	k1gMnSc7	Waits
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
sbírku	sbírka	k1gFnSc4	sbírka
krátkých	krátká	k1gFnPc2	krátká
próz	próza	k1gFnPc2	próza
Smack	Smack	k1gInSc1	Smack
my	my	k3xPp1nPc1	my
Crack	Cracko	k1gNnPc2	Cracko
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vydanou	vydaný	k2eAgFnSc4d1	vydaná
jako	jako	k8xC	jako
mluvené	mluvený	k2eAgNnSc4d1	mluvené
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
David	David	k1gMnSc1	David
Cronenberg	Cronenberg	k1gMnSc1	Cronenberg
rozpačitou	rozpačitý	k2eAgFnSc4d1	rozpačitá
filmovou	filmový	k2eAgFnSc4d1	filmová
adaptaci	adaptace	k1gFnSc4	adaptace
Nahého	nahý	k2eAgInSc2d1	nahý
oběda	oběd	k1gInSc2	oběd
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgInS	být
spokojen	spokojen	k2eAgMnSc1d1	spokojen
ani	ani	k8xC	ani
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnPc1	jeho
příznivci	příznivec	k1gMnPc1	příznivec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ale	ale	k9	ale
přesto	přesto	k6eAd1	přesto
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
další	další	k2eAgFnSc4d1	další
vlnu	vlna	k1gFnSc4	vlna
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Burroughs	Burroughs	k1gInSc1	Burroughs
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gMnSc2	The
Illuminates	Illuminatesa	k1gFnPc2	Illuminatesa
of	of	k?	of
Thanateros	Thanaterosa	k1gFnPc2	Thanaterosa
založené	založený	k2eAgFnPc4d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
britskými	britský	k2eAgMnPc7d1	britský
okultisty	okultista	k1gMnPc7	okultista
Rayem	Ray	k1gMnSc7	Ray
Shervinem	Shervin	k1gMnSc7	Shervin
a	a	k8xC	a
Peterem	Peter	k1gMnSc7	Peter
Carollem	Caroll	k1gMnSc7	Caroll
<g/>
,	,	kIx,	,
vyznavačů	vyznavač	k1gMnPc2	vyznavač
magie	magie	k1gFnSc1	magie
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Burroughs	Burroughs	k1gInSc1	Burroughs
zemřel	zemřít	k5eAaPmAgInS	zemřít
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
v	v	k7c4	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
následkem	následkem	k7c2	následkem
srdečního	srdeční	k2eAgNnSc2d1	srdeční
selhání	selhání	k1gNnSc2	selhání
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
předchozího	předchozí	k2eAgInSc2d1	předchozí
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Lawrence	Lawrence	k1gFnSc2	Lawrence
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kansas	Kansas	k1gInSc1	Kansas
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgNnSc1d1	duchovní
cvičení	cvičení	k1gNnSc1	cvičení
tibetské	tibetský	k2eAgFnSc2d1	tibetská
školy	škola	k1gFnSc2	škola
Ňingma	Ňingmum	k1gNnSc2	Ňingmum
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gNnSc7	jeho
tělem	tělo	k1gNnSc7	tělo
po	po	k7c4	po
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
prováděl	provádět	k5eAaImAgMnS	provádět
jeho	jeho	k3xOp3gNnSc4	jeho
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
John	John	k1gMnSc1	John
Giorno	Giorno	k1gNnSc4	Giorno
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
kolem	kolem	k7c2	kolem
desáté	desátá	k1gFnSc2	desátá
hodiny	hodina	k1gFnSc2	hodina
se	se	k3xPyFc4	se
dostavili	dostavit	k5eAaPmAgMnP	dostavit
James	James	k1gMnSc1	James
Grauerholz	Grauerholz	k1gMnSc1	Grauerholz
a	a	k8xC	a
Ira	Ir	k1gMnSc4	Ir
Silverberg	Silverberg	k1gInSc4	Silverberg
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
připravili	připravit	k5eAaPmAgMnP	připravit
Burroughse	Burroughse	k1gFnPc4	Burroughse
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
zásvětí	zásvětí	k1gNnSc2	zásvětí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
osmatřicítku	osmatřicítka	k1gFnSc4	osmatřicítka
s	s	k7c7	s
pěti	pět	k4xCc7	pět
náboji	náboj	k1gInPc7	náboj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Burroughs	Burroughs	k1gInSc4	Burroughs
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
situaci	situace	k1gFnSc6	situace
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
člověk	člověk	k1gMnSc1	člověk
dost	dost	k6eAd1	dost
ozbrojen	ozbrojen	k2eAgMnSc1d1	ozbrojen
<g/>
,	,	kIx,	,
dávku	dávka	k1gFnSc4	dávka
heroinu	heroin	k1gInSc2	heroin
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
marihuany	marihuana	k1gFnPc1	marihuana
<g/>
,	,	kIx,	,
zlatou	zlatý	k2eAgFnSc4d1	zlatá
minci	mince	k1gFnSc4	mince
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
,	,	kIx,	,
na	na	k7c4	na
klopu	klopa	k1gFnSc4	klopa
řád	řád	k1gInSc1	řád
francouzské	francouzský	k2eAgFnSc2d1	francouzská
i	i	k8xC	i
americké	americký	k2eAgFnSc2d1	americká
Akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
vycházkovou	vycházkový	k2eAgFnSc4d1	vycházková
hůl	hůl	k1gFnSc4	hůl
s	s	k7c7	s
kordem	kord	k1gInSc7	kord
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
klobouk	klobouk	k1gInSc4	klobouk
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc4d1	červený
šátek	šátek	k1gInSc4	šátek
<g/>
,	,	kIx,	,
vestu	vesta	k1gFnSc4	vesta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Briona	Brion	k1gMnSc2	Brion
Gysina	Gysin	k1gMnSc2	Gysin
<g/>
,	,	kIx,	,
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
předmětů	předmět	k1gInPc2	předmět
včetně	včetně	k7c2	včetně
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
propisovačky	propisovačka	k1gFnSc2	propisovačka
<g/>
.	.	kIx.	.
</s>
<s>
Uložen	uložen	k2eAgInSc1d1	uložen
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
hrobky	hrobka	k1gFnSc2	hrobka
v	v	k7c6	v
Bellefontaine	Bellefontain	k1gInSc5	Bellefontain
Cemetry	Cemetr	k1gInPc7	Cemetr
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Missouri	Missouri	k1gFnSc2	Missouri
na	na	k7c4	na
38.690	[number]	k4	38.690
<g/>
310	[number]	k4	310
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
90.231	[number]	k4	90.231
<g/>
720	[number]	k4	720
<g/>
°	°	k?	°
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
Burroughsovy	Burroughsův	k2eAgInPc1d1	Burroughsův
rané	raný	k2eAgInPc1d1	raný
literární	literární	k2eAgInPc1d1	literární
počiny	počin	k1gInPc1	počin
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Word	Word	kA	Word
Virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
editory	editor	k1gInPc1	editor
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Grauerholz	Grauerholz	k1gMnSc1	Grauerholz
a	a	k8xC	a
Silverberg	Silverberg	k1gMnSc1	Silverberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc4	vydání
sbírky	sbírka	k1gFnSc2	sbírka
časopiseckých	časopisecký	k2eAgNnPc2d1	časopisecké
dílek	dílko	k1gNnPc2	dílko
z	z	k7c2	z
pozdního	pozdní	k2eAgNnSc2d1	pozdní
období	období	k1gNnSc2	období
pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Evil	Evil	k1gMnSc1	Evil
River	River	k1gMnSc1	River
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
odkládá	odkládat	k5eAaImIp3nS	odkládat
<g/>
.	.	kIx.	.
</s>
<s>
Feťák	Feťák	k?	Feťák
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Junkie	Junkie	k1gFnSc1	Junkie
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Nahý	nahý	k2eAgInSc1d1	nahý
oběd	oběd	k1gInSc1	oběd
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Naked	Naked	k1gInSc1	Naked
Lunch	lunch	k1gInSc1	lunch
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7287-063-7	[number]	k4	80-7287-063-7
Hebká	hebký	k2eAgFnSc1d1	hebká
mašinka	mašinka	k1gFnSc1	mašinka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
Soft	Soft	k?	Soft
Machine	Machin	k1gMnSc5	Machin
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Lístek	lístek	k1gInSc1	lístek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
explodoval	explodovat	k5eAaBmAgInS	explodovat
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Ticket	Ticket	k1gMnSc1	Ticket
That	That	k1gMnSc1	That
<g />
.	.	kIx.	.
</s>
<s>
Exploded	Exploded	k1gMnSc1	Exploded
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Dopisy	dopis	k1gInPc1	dopis
o	o	k7c4	o
yagé	yagé	k1gNnSc4	yagé
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnPc1	The
Yagé	Yagé	k1gNnSc2	Yagé
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Nova	nova	k1gFnSc1	nova
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Divocí	divoký	k2eAgMnPc1d1	divoký
hoši	hoch	k1gMnPc1	hoch
<g/>
:	:	kIx,	:
kniha	kniha	k1gFnSc1	kniha
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc3	The
Wild	Wilda	k1gFnPc2	Wilda
Boys	boy	k1gMnPc1	boy
<g/>
:	:	kIx,	:
A	a	k9	a
Book	Book	k1gMnSc1	Book
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
Dead	Dead	k1gMnSc1	Dead
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Hubitel	hubitel	k1gMnSc1	hubitel
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Exterminator	Exterminator	k1gInSc1	Exterminator
<g/>
!	!	kIx.	!
</s>
<s>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Města	město	k1gNnSc2	město
rudé	rudý	k2eAgFnSc2d1	rudá
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Cities	Cities	k1gInSc1	Cities
of	of	k?	of
the	the	k?	the
Red	Red	k1gFnSc1	Red
Night	Nightum	k1gNnPc2	Nightum
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Místo	místo	k7c2	místo
slepých	slepý	k2eAgFnPc2d1	slepá
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc1	The
Place	plac	k1gInSc6	plac
of	of	k?	of
Dead	Dead	k1gInSc1	Dead
Roads	Roadsa	k1gFnPc2	Roadsa
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7207-426-1	[number]	k4	80-7207-426-1
Teplouš	teplouš	k1gMnSc1	teplouš
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Queer	Queer	k1gInSc1	Queer
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g />
.	.	kIx.	.
</s>
<s>
80-7287-080-7	[number]	k4	80-7287-080-7
Západní	západní	k2eAgFnPc4d1	západní
země	zem	k1gFnPc4	zem
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Western	Western	kA	Western
Lands	Lands	k1gInSc1	Lands
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-14-009456-3	[number]	k4	0-14-009456-3
Ohyzdný	ohyzdný	k2eAgMnSc1d1	ohyzdný
duch	duch	k1gMnSc1	duch
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
angl.	angl.	k?	angl.
Ghost	Ghost	k1gInSc1	Ghost
of	of	k?	of
Chance	Chanec	k1gInSc2	Chanec
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Škola	škola	k1gFnSc1	škola
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
kniha	kniha	k1gFnSc1	kniha
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
My	my	k3xPp1nPc1	my
Education	Education	k1gInSc4	Education
<g/>
:	:	kIx,	:
A	a	k9	a
Book	Book	k1gInSc1	Book
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
