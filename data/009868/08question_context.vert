<s>
Korsika	Korsika	k1gFnSc1	Korsika
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
spadající	spadající	k2eAgInSc1d1	spadající
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
ostrova	ostrov	k1gInSc2	ostrov
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
8680	[number]	k4	8680
km2	km2	k4	km2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
zaťatou	zaťatý	k2eAgFnSc4d1	zaťatá
pěst	pěst	k1gFnSc4	pěst
se	s	k7c7	s
vztyčeným	vztyčený	k2eAgInSc7d1	vztyčený
palcem	palec	k1gInSc7	palec
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
183	[number]	k4	183
km	km	kA	km
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
83	[number]	k4	83
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
ostrova	ostrov	k1gInSc2	ostrov
od	od	k7c2	od
evropské	evropský	k2eAgFnSc2d1	Evropská
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
180	[number]	k4	180
km	km	kA	km
<g/>
,	,	kIx,	,
od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
80	[number]	k4	80
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Obýván	obýván	k2eAgMnSc1d1	obýván
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
322	[number]	k4	322
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
Monte	Mont	k1gMnSc5	Mont
Cinto	cinta	k1gMnSc5	cinta
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2706	[number]	k4	2706
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
