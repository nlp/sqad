<s>
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Jeffrey	Jeffrea	k1gMnSc2
JordanOsobní	JordanOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1963	#num#	k4
(	(	kIx(
<g/>
58	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Stát	stát	k1gInSc1
</s>
<s>
USA	USA	kA
Výška	výška	k1gFnSc1
</s>
<s>
198	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
98	#num#	k4
kg	kg	kA
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Air	Air	k?
Jordan	Jordan	k1gMnSc1
Klubové	klubový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Současný	současný	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Kariéra	kariéra	k1gFnSc1
ukončena	ukončit	k5eAaPmNgFnS
r.	r.	kA
2003	#num#	k4
Číslo	číslo	k1gNnSc4
</s>
<s>
23	#num#	k4
a	a	k8xC
45	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
Shooting	Shooting	k1gInSc1
guard	guard	k1gInSc1
(	(	kIx(
<g/>
nižší	nízký	k2eAgNnSc1d2
křídlo	křídlo	k1gNnSc1
<g/>
)	)	kIx)
Předchozí	předchozí	k2eAgInPc1d1
týmy	tým	k1gInPc1
</s>
<s>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
Washington	Washington	k1gInSc1
Wizards	Wizards	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
Draft	draft	k1gInSc1
NBA	NBA	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
jako	jako	k8xS,k8xC
3	#num#	k4
<g/>
.	.	kIx.
celkovětýmem	celkovětým	k1gInSc7
Chicago	Chicago	k1gNnSc1
Bulls	Bulls	k1gInSc1
Univerzita	univerzita	k1gFnSc1
</s>
<s>
University	universita	k1gFnPc1
of	of	k?
North	North	k1gMnSc1
Carolina	Carolin	k2eAgNnSc2d1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
Vítěz	vítězit	k5eAaImRp2nS
NBA	NBA	kA
6	#num#	k4
<g/>
×	×	k?
MVP	MVP	kA
NBA	NBA	kA
finále	finále	k1gNnSc4
5	#num#	k4
<g/>
×	×	k?
MVP	MVP	kA
sezóny	sezóna	k1gFnSc2
NBA	NBA	kA
NBA	NBA	kA
nováček	nováček	k1gMnSc1
roku	rok	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Basketbal	basketbal	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
LOH	LOH	kA
1984	#num#	k4
</s>
<s>
basketbal	basketbal	k1gInSc4
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
LOH	LOH	kA
1992	#num#	k4
</s>
<s>
basketbal	basketbal	k1gInSc4
</s>
<s>
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
Brooklyn	Brooklyn	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
americký	americký	k2eAgMnSc1d1
profesionální	profesionální	k2eAgMnSc1d1
basketbalista	basketbalista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
basketbalistů	basketbalista	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Hrál	hrát	k5eAaImAgInS
v	v	k7c6
NBA	NBA	kA
za	za	k7c4
tým	tým	k1gInSc4
Chicago	Chicago	k1gNnSc4
Bulls	Bulls	k1gInSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
spolumajitelem	spolumajitel	k1gMnSc7
týmu	tým	k1gInSc2
Washington	Washington	k1gInSc1
Wizards	Wizards	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
nečekaně	nečekaně	k6eAd1
znovu	znovu	k6eAd1
nastoupil	nastoupit	k5eAaPmAgInS
i	i	k9
jako	jako	k9
hráč	hráč	k1gMnSc1
a	a	k8xC
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
něj	on	k3xPp3gMnSc4
do	do	k7c2
sezóny	sezóna	k1gFnSc2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
po	po	k7c6
jejím	její	k3xOp3gInSc6
závěru	závěr	k1gInSc6
ukončil	ukončit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
profesionální	profesionální	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4
tým	tým	k1gInSc4
dovedl	dovést	k5eAaPmAgMnS
šestkrát	šestkrát	k4xCNnPc1
do	do	k7c2
finále	finále	k1gNnSc2
NBA	NBA	kA
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
a	a	k8xC
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
všech	všecek	k3xTgNnPc6
šesti	šest	k4xCc6
případech	případ	k1gInPc6
Bulls	Bulls	k1gInSc1
finále	finále	k1gNnSc6
vyhráli	vyhrát	k5eAaPmAgMnP
a	a	k8xC
Jordan	Jordan	k1gMnSc1
byl	být	k5eAaImAgMnS
vyhlášen	vyhlásit	k5eAaPmNgMnS
nejužitečnějším	užitečný	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
finálové	finálový	k2eAgFnSc2d1
série	série	k1gFnSc2
(	(	kIx(
<g/>
MVP	MVP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
byl	být	k5eAaImAgInS
také	také	k6eAd1
pětkrát	pětkrát	k6eAd1
vyhlášen	vyhlásit	k5eAaPmNgMnS
nejužitečnějším	užitečný	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
NBA	NBA	kA
za	za	k7c4
celou	celý	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
<g/>
:	:	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
a	a	k8xC
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
průměr	průměr	k1gInSc1
30,12	30,12	k4
bodu	bod	k1gInSc2
na	na	k7c4
zápas	zápas	k1gInSc4
jej	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nS
v	v	k7c6
průměrné	průměrný	k2eAgFnSc6d1
produktivitě	produktivita	k1gFnSc6
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
historických	historický	k2eAgFnPc6d1
tabulkách	tabulka	k1gFnPc6
NBA	NBA	kA
(	(	kIx(
<g/>
druhý	druhý	k4xOgInSc1
Wilt	Wilt	k2eAgInSc1d1
Chamberlain	Chamberlain	k1gInSc1
zaostává	zaostávat	k5eAaImIp3nS
o	o	k7c4
0,06	0,06	k4
bodu	bod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jeho	jeho	k3xOp3gFnSc6
univerzálnosti	univerzálnost	k1gFnSc6
a	a	k8xC
přínosu	přínos	k1gInSc6
pro	pro	k7c4
tým	tým	k1gInSc4
svědčí	svědčit	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
také	také	k9
jednou	jednou	k6eAd1
vyhlášen	vyhlásit	k5eAaPmNgMnS
nejlépe	dobře	k6eAd3
bránícím	bránící	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
NBA	NBA	kA
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
držitele	držitel	k1gMnSc4
dvou	dva	k4xCgFnPc6
zlatých	zlatý	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
z	z	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
–	–	k?
z	z	k7c2
let	léto	k1gNnPc2
1984	#num#	k4
a	a	k8xC
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
kariéře	kariéra	k1gFnSc6
je	být	k5eAaImIp3nS
odskok	odskok	k1gInSc1
od	od	k7c2
basketbalu	basketbal	k1gInSc2
k	k	k7c3
profesionálnímu	profesionální	k2eAgInSc3d1
baseballu	baseball	k1gInSc3
<g/>
,	,	kIx,
když	když	k8xS
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
po	po	k7c4
dvě	dva	k4xCgNnPc4
léta	léto	k1gNnPc4
nastupoval	nastupovat	k5eAaImAgInS
za	za	k7c4
tým	tým	k1gInSc4
Chicago	Chicago	k1gNnSc4
White	Whit	k1gMnSc5
Sox	Sox	k1gMnSc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
moc	moc	k6eAd1
neprosadil	prosadit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
jak	jak	k6eAd1
k	k	k7c3
basketbalu	basketbal	k1gInSc3
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
k	k	k7c3
Bulls	Bulls	k1gInSc4
a	a	k8xC
pomohl	pomoct	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
k	k	k7c3
dalším	další	k2eAgFnPc3d1
třem	tři	k4xCgFnPc3
titulům	titul	k1gInPc3
vítěze	vítěz	k1gMnPc4
NBA	NBA	kA
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Brooklynu	Brooklyn	k1gInSc6
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
NY	NY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc6
se	se	k3xPyFc4
rodina	rodina	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Wilmingtonu	Wilmington	k1gInSc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Karolíně	Karolína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgMnS
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Emsley	Emslea	k1gMnSc2
A.	A.	kA
Laney	Lanea	k1gMnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
započal	započnout	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
sportovní	sportovní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
za	za	k7c4
školu	škola	k1gFnSc4
hrál	hrát	k5eAaImAgMnS
americký	americký	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
<g/>
,	,	kIx,
baseball	baseball	k1gInSc4
a	a	k8xC
basketbal	basketbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1981	#num#	k4
získal	získat	k5eAaPmAgInS
sportovní	sportovní	k2eAgNnSc4d1
stipendium	stipendium	k1gNnSc4
pro	pro	k7c4
studium	studium	k1gNnSc4
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
at	at	k?
Chapel	Chapel	k1gMnSc1
Hill	Hill	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
poté	poté	k6eAd1
studoval	studovat	k5eAaImAgMnS
obor	obor	k1gInSc4
kulturní	kulturní	k2eAgFnSc2d1
geografie	geografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
univerzitě	univerzita	k1gFnSc6
se	se	k3xPyFc4
hned	hned	k6eAd1
první	první	k4xOgFnSc4
sezónu	sezóna	k1gFnSc4
1981	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
ukázal	ukázat	k5eAaPmAgInS
jako	jako	k9
velmi	velmi	k6eAd1
talentovaný	talentovaný	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezónu	sezóna	k1gFnSc4
končil	končit	k5eAaImAgInS
s	s	k7c7
průměrem	průměr	k1gInSc7
13,4	13,4	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
a	a	k8xC
úspěšností	úspěšnost	k1gFnSc7
53,4	53,4	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgMnS
vyhlášen	vyhlásit	k5eAaPmNgMnS
univerzitním	univerzitní	k2eAgMnSc7d1
basketbalovým	basketbalový	k2eAgMnSc7d1
nováčkem	nováček	k1gMnSc7
východního	východní	k2eAgNnSc2d1
konference	konference	k1gFnSc2
USA	USA	kA
(	(	kIx(
<g/>
ACC	ACC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oné	onen	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
s	s	k7c7
týmem	tým	k1gInSc7
vyhráli	vyhrát	k5eAaPmAgMnP
univerzitní	univerzitní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
NCAA	NCAA	kA
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
univerzitu	univerzita	k1gFnSc4
hrál	hrát	k5eAaImAgMnS
ještě	ještě	k9
dvě	dva	k4xCgFnPc4
sezóny	sezóna	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
vždy	vždy	k6eAd1
průměr	průměr	k1gInSc1
20	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
a	a	k8xC
úspěšnost	úspěšnost	k1gFnSc4
okolo	okolo	k7c2
54	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1983	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
Naismithův	Naismithův	k2eAgInSc1d1
pohár	pohár	k1gInSc1
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
i	i	k9
Woodenův	Woodenův	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
přerušil	přerušit	k5eAaPmAgMnS
studium	studium	k1gNnSc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
byl	být	k5eAaImAgInS
draftován	draftovat	k5eAaImNgInS
týmem	tým	k1gInSc7
Chicago	Chicago	k1gNnSc4
Bulls	Bullsa	k1gFnPc2
do	do	k7c2
NBA	NBA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byl	být	k5eAaImAgInS
i	i	k9
ve	v	k7c6
vítězném	vítězný	k2eAgInSc6d1
týmu	tým	k1gInSc6
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
dokončil	dokončit	k5eAaPmAgInS
roku	rok	k1gInSc2
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Profesionální	profesionální	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
v	v	k7c6
NBA	NBA	kA
si	se	k3xPyFc3
udržel	udržet	k5eAaPmAgInS
průměr	průměr	k1gInSc1
28,2	28,2	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
s	s	k7c7
úspěšností	úspěšnost	k1gFnSc7
51,5	51,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
byl	být	k5eAaImAgMnS
diváky	divák	k1gMnPc4
zvolen	zvolit	k5eAaPmNgMnS
do	do	k7c2
all-star	all-stara	k1gFnPc2
týmu	tým	k1gInSc2
a	a	k8xC
objevil	objevit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c6
obálce	obálka	k1gFnSc6
prestižního	prestižní	k2eAgInSc2d1
sportovního	sportovní	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
Sports	Sportsa	k1gFnPc2
Illustrated	Illustrated	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
zvolení	zvolení	k1gNnSc1
do	do	k7c2
all-star	all-stara	k1gFnPc2
týmu	tým	k1gInSc2
však	však	k9
vzbudilo	vzbudit	k5eAaPmAgNnS
kontroverzi	kontroverze	k1gFnSc4
mezi	mezi	k7c7
zkušenějšími	zkušený	k2eAgMnPc7d2
hráči	hráč	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
dokonce	dokonce	k9
odmítali	odmítat	k5eAaImAgMnP
Jordanovi	Jordanův	k2eAgMnPc1d1
během	během	k7c2
hry	hra	k1gFnSc2
přihrát	přihrát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordan	Jordan	k1gMnSc1
sice	sice	k8xC
vyhrál	vyhrát	k5eAaPmAgMnS
ocenění	ocenění	k1gNnSc4
nováček	nováček	k1gMnSc1
roku	rok	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc1
tým	tým	k1gInSc1
vypadl	vypadnout	k5eAaPmAgInS
již	již	k6eAd1
v	v	k7c6
prvním	první	k4xOgNnSc6
kole	kolo	k1gNnSc6
playoff	playoff	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
druhé	druhý	k4xOgFnSc6
sezóně	sezóna	k1gFnSc6
si	se	k3xPyFc3
zlomil	zlomit	k5eAaPmAgMnS
nohu	noha	k1gFnSc4
a	a	k8xC
odehrál	odehrát	k5eAaPmAgInS
jen	jen	k9
18	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
,	,	kIx,
toho	ten	k3xDgMnSc4
využil	využít	k5eAaPmAgMnS
pro	pro	k7c4
dostudování	dostudování	k1gNnSc4
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Následujících	následující	k2eAgFnPc2d1
sedm	sedm	k4xCc1
sezón	sezóna	k1gFnPc2
byl	být	k5eAaImAgInS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jeho	jeho	k3xOp3gInSc1
průměr	průměr	k1gInSc1
neklesl	klesnout	k5eNaPmAgInS
pod	pod	k7c4
30	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezónách	sezóna	k1gFnPc6
1986	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
a	a	k8xC
1989	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
třikrát	třikrát	k6eAd1
překonal	překonat	k5eAaPmAgMnS
hranici	hranice	k1gFnSc4
1000	#num#	k4
košů	koš	k1gInPc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1986	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
teprve	teprve	k6eAd1
druhým	druhý	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
pokořil	pokořit	k5eAaPmAgMnS
hranici	hranice	k1gFnSc4
3000	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
sezónu	sezóna	k1gFnSc4
<g/>
,	,	kIx,
prvním	první	k4xOgMnSc6
byl	být	k5eAaImAgMnS
Wilt	Wilt	k2eAgInSc4d1
Chamberlain	Chamberlain	k1gInSc4
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
3041	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
hranici	hranice	k1gFnSc4
již	již	k6eAd1
znovu	znovu	k6eAd1
nepřekročil	překročit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
sedmi	sedm	k4xCc2
sezónách	sezóna	k1gFnPc6
jeho	jeho	k3xOp3gMnPc3
průměr	průměr	k1gInSc1
neklesl	klesnout	k5eNaPmAgInS
pod	pod	k7c4
943	#num#	k4
košů	koš	k1gInPc2
a	a	k8xC
2400	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
a	a	k8xC
1993	#num#	k4
dovedl	dovést	k5eAaPmAgInS
tým	tým	k1gInSc1
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
k	k	k7c3
vítězství	vítězství	k1gNnSc3
v	v	k7c4
playoff	playoff	k1gInSc4
NBA	NBA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
v	v	k7c6
těchto	tento	k3xDgNnPc6
letech	léto	k1gNnPc6
stal	stát	k5eAaPmAgMnS
třikrát	třikrát	k6eAd1
nejužitečnějším	užitečný	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
finále	finále	k1gNnSc2
playoff	playoff	k1gMnSc1
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1991	#num#	k4
a	a	k8xC
1992	#num#	k4
i	i	k8xC
nejužitečnějším	užitečný	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
celé	celý	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
NBA	NBA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
dále	daleko	k6eAd2
úspěšně	úspěšně	k6eAd1
reprezentoval	reprezentovat	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
vlast	vlast	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
s	s	k7c7
tzv.	tzv.	kA
Dream	Dream	k1gInSc1
Team	team	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1993	#num#	k4
si	se	k3xPyFc3
prošel	projít	k5eAaPmAgInS
obdobím	období	k1gNnSc7
kontroverze	kontroverze	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgMnS
obviňován	obviňovat	k5eAaImNgMnS
z	z	k7c2
gamblerství	gamblerství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byl	být	k5eAaImAgMnS
navíc	navíc	k6eAd1
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
zavražděn	zavraždit	k5eAaPmNgMnS
při	při	k7c6
loupežném	loupežný	k2eAgNnSc6d1
přepadení	přepadení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordan	Jordan	k1gMnSc1
kvůli	kvůli	k7c3
stresu	stres	k1gInSc3
přerušil	přerušit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1994	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
baseballovým	baseballový	k2eAgInSc7d1
týmem	tým	k1gInSc7
Chicago	Chicago	k1gNnSc4
White	Whit	k1gMnSc5
Sox	Sox	k1gMnSc5
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
hrát	hrát	k5eAaImF
za	za	k7c4
jejich	jejich	k3xOp3gInPc4
týmy	tým	k1gInPc4
v	v	k7c6
Minor	minor	k2eAgFnSc6d1
league	league	k1gFnSc6
baseball	baseball	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečně	skutečně	k6eAd1
nastoupil	nastoupit	k5eAaPmAgMnS
za	za	k7c4
tým	tým	k1gInSc4
Birmingham	Birmingham	k1gInSc4
Barons	Baronsa	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
za	za	k7c4
Scottsdale	Scottsdala	k1gFnSc3
Scorpions	Scorpions	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bez	bez	k7c2
Jordana	Jordan	k1gMnSc2
Chicago	Chicago	k1gNnSc4
Bulls	Bullsa	k1gFnPc2
vypadli	vypadnout	k5eAaPmAgMnP
v	v	k7c6
druhém	druhý	k4xOgNnSc6
kole	kolo	k1gNnSc6
playoff	playoff	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1995	#num#	k4
dvěma	dva	k4xCgInPc7
slovy	slovo	k1gNnPc7
(	(	kIx(
<g/>
„	„	k?
<g/>
Jsem	být	k5eAaImIp1nS
zpět	zpět	k6eAd1
<g/>
“	“	k?
<g/>
)	)	kIx)
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
do	do	k7c2
NBA	NBA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
probíhající	probíhající	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
ještě	ještě	k9
stihl	stihnout	k5eAaPmAgMnS
dohrát	dohrát	k5eAaPmF
sedmnáct	sedmnáct	k4xCc4
zápasů	zápas	k1gInPc2
s	s	k7c7
průměrem	průměr	k1gInSc7
26,9	26,9	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
a	a	k8xC
úspěšností	úspěšnost	k1gFnSc7
41,1	41,1	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc4
však	však	k9
znovu	znovu	k6eAd1
v	v	k7c4
playoff	playoff	k1gInSc4
vypadl	vypadnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
následující	následující	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
proto	proto	k8xC
Jordan	Jordan	k1gMnSc1
začal	začít	k5eAaPmAgMnS
znovu	znovu	k6eAd1
přísně	přísně	k6eAd1
trénovat	trénovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1995	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
se	se	k3xPyFc4
Jordan	Jordan	k1gMnSc1
i	i	k9
Bulls	Bulls	k1gInSc4
předvedli	předvést	k5eAaPmAgMnP
znamenitě	znamenitě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordan	Jordan	k1gMnSc1
byl	být	k5eAaImAgMnS
znovu	znovu	k6eAd1
nejlepším	dobrý	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
s	s	k7c7
30,4	30,4	k4
body	bod	k1gInPc7
na	na	k7c4
zápas	zápas	k1gInSc4
a	a	k8xC
úspěšností	úspěšnost	k1gFnSc7
49,5	49,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
zase	zase	k9
stanovil	stanovit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
s	s	k7c7
72	#num#	k4
výhrami	výhra	k1gFnPc7
a	a	k8xC
jen	jen	k9
10	#num#	k4
porážkami	porážka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
také	také	k6eAd1
znovu	znovu	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
playoff	playoff	k1gMnSc1
NBA	NBA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordan	Jordan	k1gMnSc1
byl	být	k5eAaImAgMnS
již	již	k6eAd1
počtvrté	počtvrté	k4xO
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
nejužitečnějším	užitečný	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
finálových	finálový	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
překonal	překonat	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
Magica	Magicus	k1gMnSc2
Johnsona	Johnson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
tým	tým	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
69	#num#	k4
zápasů	zápas	k1gInPc2
a	a	k8xC
Jordan	Jordan	k1gMnSc1
zaznamenal	zaznamenat	k5eAaPmAgMnS
skóre	skóre	k1gNnPc4
29,6	29,6	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
s	s	k7c7
úspěšností	úspěšnost	k1gFnSc7
48,6	48,6	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulls	Bulls	k1gInSc1
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
znovu	znovu	k6eAd1
vyhráli	vyhrát	k5eAaPmAgMnP
finále	finále	k1gNnSc4
playoff	playoff	k1gMnSc1
a	a	k8xC
Jordan	Jordan	k1gMnSc1
získal	získat	k5eAaPmAgMnS
trofej	trofej	k1gFnSc4
pro	pro	k7c4
nejužitečnějšího	užitečný	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
poslední	poslední	k2eAgFnSc7d1
sezónou	sezóna	k1gFnSc7
v	v	k7c4
Chicago	Chicago	k1gNnSc4
Bulls	Bullsa	k1gFnPc2
byla	být	k5eAaImAgFnS
1997	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
udržel	udržet	k5eAaPmAgInS
28,7	28,7	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
a	a	k8xC
úspěšnost	úspěšnost	k1gFnSc4
46,5	46,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
již	již	k9
35	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
playoff	playoff	k1gInSc4
získal	získat	k5eAaPmAgMnS
Jordan	Jordan	k1gMnSc1
s	s	k7c7
týmem	tým	k1gInSc7
již	již	k9
šestý	šestý	k4xOgInSc1
pohár	pohár	k1gInSc1
pro	pro	k7c4
nejlepší	dobrý	k2eAgInSc4d3
tým	tým	k1gInSc4
a	a	k8xC
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
pro	pro	k7c4
nejužitečnějšího	užitečný	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1999	#num#	k4
mu	on	k3xPp3gNnSc3
vypršela	vypršet	k5eAaPmAgFnS
smlouva	smlouva	k1gFnSc1
a	a	k8xC
i	i	k9
proto	proto	k8xC
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
již	již	k6eAd1
podruhé	podruhé	k6eAd1
přerušit	přerušit	k5eAaPmF
kariéru	kariéra	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
přesněji	přesně	k6eAd2
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
na	na	k7c4
99	#num#	k4
%	%	kIx~
k	k	k7c3
hráčské	hráčský	k2eAgFnSc3d1
kariéře	kariéra	k1gFnSc3
nevrátí	vrátit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
do	do	k7c2
světa	svět	k1gInSc2
NBA	NBA	kA
vrátil	vrátit	k5eAaPmAgMnS
jako	jako	k9
spolumajitel	spolumajitel	k1gMnSc1
týmu	tým	k1gInSc2
Washington	Washington	k1gInSc1
Wizards	Wizards	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
však	však	k9
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
hodlá	hodlat	k5eAaImIp3nS
hrát	hrát	k5eAaImF
za	za	k7c4
tým	tým	k1gInSc4
Wizards	Wizardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
60	#num#	k4
zápasech	zápas	k1gInPc6
však	však	k9
kvůli	kvůli	k7c3
zranění	zranění	k1gNnSc3
kolene	kolen	k1gInSc5
z	z	k7c2
týmu	tým	k1gInSc2
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
zaznamenal	zaznamenat	k5eAaPmAgInS
22,9	22,9	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
poslední	poslední	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
odehrál	odehrát	k5eAaPmAgMnS
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
týmu	tým	k1gInSc2
Wizards	Wizardsa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
již	již	k9
o	o	k7c4
čtrnáctou	čtrnáctý	k4xOgFnSc4
sezónu	sezóna	k1gFnSc4
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
Jordan	Jordan	k1gMnSc1
zlomil	zlomit	k5eAaPmAgMnS
dosavadní	dosavadní	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
držel	držet	k5eAaImAgInS
Kareem	Kareum	k1gNnSc7
Abdul-Jabbar	Abdul-Jabbara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezónu	sezóna	k1gFnSc4
ukončil	ukončit	k5eAaPmAgInS
s	s	k7c7
průměrem	průměr	k1gInSc7
20	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
již	již	k6eAd1
definitivně	definitivně	k6eAd1
ukončil	ukončit	k5eAaPmAgMnS
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Momentálně	momentálně	k6eAd1
je	být	k5eAaImIp3nS
majitelem	majitel	k1gMnSc7
týmu	tým	k1gInSc2
Charlotte	Charlott	k1gInSc5
Hornets	Hornets	k1gInSc4
(	(	kIx(
<g/>
Dříve	dříve	k6eAd2
Charlotte	Charlott	k1gInSc5
Bobcats	Bobcats	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
Charlotte	Charlott	k1gInSc5
Hornets	Hornetsa	k1gFnPc2
byl	být	k5eAaImAgInS
klub	klub	k1gInSc1
přejmenován	přejmenovat	k5eAaPmNgInS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
14	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Týmy	tým	k1gInPc1
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
a	a	k8xC
Washington	Washington	k1gInSc1
Wizards	Wizardsa	k1gFnPc2
z	z	k7c2
úcty	úcta	k1gFnSc2
vyřadily	vyřadit	k5eAaPmAgInP
číslo	číslo	k1gNnSc4
23	#num#	k4
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
nabídky	nabídka	k1gFnSc2
čísel	číslo	k1gNnPc2
dresů	dres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1989	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Juanitou	Juanita	k1gFnSc7
Vanoy	Vanoa	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
má	mít	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
(	(	kIx(
<g/>
Jeffrey	Jeffrea	k1gMnSc2
Michael	Michael	k1gMnSc1
a	a	k8xC
Marcus	Marcus	k1gMnSc1
James	James	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
dceru	dcera	k1gFnSc4
(	(	kIx(
<g/>
Jasmine	Jasmin	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pár	pár	k4xCyI
se	se	k3xPyFc4
rozvedl	rozvést	k5eAaPmAgMnS
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Juanita	Juanita	k1gFnSc1
získala	získat	k5eAaPmAgFnS
vyrovnání	vyrovnání	k1gNnSc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
168	#num#	k4
milionů	milion	k4xCgInPc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2006	#num#	k4
ho	on	k3xPp3gMnSc4
jeho	jeho	k3xOp3gFnSc1
bývalá	bývalý	k2eAgFnSc1d1
milenka	milenka	k1gFnSc1
Karla	Karla	k1gFnSc1
Knafel	Knaflo	k1gNnPc2
nařkla	nařknout	k5eAaPmAgFnS
z	z	k7c2
otcovství	otcovství	k1gNnSc2
svého	svůj	k3xOyFgNnSc2
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
testy	testa	k1gFnPc1
však	však	k9
prokázaly	prokázat	k5eAaPmAgFnP
opak	opak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvíce	nejvíce	k6eAd1,k6eAd3
známých	známý	k2eAgFnPc2d1
mediálních	mediální	k2eAgFnPc2d1
tváří	tvář	k1gFnPc2
<g/>
,	,	kIx,
spolupracoval	spolupracovat	k5eAaImAgInS
s	s	k7c7
firmami	firma	k1gFnPc7
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Nike	Nike	k1gFnSc1
<g/>
,	,	kIx,
Coca-Cola	coca-cola	k1gFnSc1
<g/>
,	,	kIx,
Chevrolet	chevrolet	k1gInSc1
nebo	nebo	k8xC
McDonald	McDonald	k1gInSc1
<g/>
'	'	kIx"
<g/>
s.	s.	k?
Oděvní	oděvní	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Nike	Nike	k1gFnSc2
vytvořila	vytvořit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc4
vlastní	vlastní	k2eAgFnSc4d1
veleúspěšnou	veleúspěšný	k2eAgFnSc4d1
podznačku	podznačka	k1gFnSc4
pojmenovanou	pojmenovaný	k2eAgFnSc4d1
„	„	k?
<g/>
Air	Air	k1gFnSc2
Jordan	Jordan	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordan	Jordan	k1gMnSc1
podniká	podnikat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgMnS
blízkým	blízký	k2eAgMnSc7d1
přítelem	přítel	k1gMnSc7
zpěváka	zpěvák	k1gMnSc2
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
se	se	k3xPyFc4
dokonce	dokonce	k9
objevil	objevit	k5eAaPmAgMnS
po	po	k7c6
boku	bok	k1gInSc6
Jacksona	Jackson	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
hudební	hudební	k2eAgFnSc6d1
klipu	klip	k1gInSc3
k	k	k7c3
singlu	singl	k1gInSc3
Jam	jáma	k1gFnPc2
z	z	k7c2
alba	album	k1gNnSc2
Dangerous	Dangerous	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klip	klip	k1gInSc1
má	mít	k5eAaImIp3nS
basketbalovou	basketbalový	k2eAgFnSc4d1
tématiku	tématika	k1gFnSc4
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
konci	konec	k1gInSc6
se	se	k3xPyFc4
Jordan	Jordan	k1gMnSc1
učí	učit	k5eAaImIp3nS
od	od	k7c2
Jacksona	Jackson	k1gMnSc2
jeho	jeho	k3xOp3gNnSc1
ikonické	ikonický	k2eAgInPc4d1
taneční	taneční	k2eAgInPc4d1
kroky	krok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1996	#num#	k4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgInS
ve	v	k7c6
filmu	film	k1gInSc6
Space	Space	k1gFnSc2
Jam	jáma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
AIR	AIR	kA
JORDAN	Jordan	k1gMnSc1
</s>
<s>
Přestože	přestože	k8xS
skončil	skončit	k5eAaPmAgMnS
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
aktivní	aktivní	k2eAgFnSc7d1
profesionální	profesionální	k2eAgFnSc7d1
kariérou	kariéra	k1gFnSc7
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
značka	značka	k1gFnSc1
stále	stále	k6eAd1
žije	žít	k5eAaImIp3nS
a	a	k8xC
nabírá	nabírat	k5eAaImIp3nS
na	na	k7c6
popularitě	popularita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Nike	Nike	k1gFnSc7
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgInS
před	před	k7c7
téměř	téměř	k6eAd1
třiceti	třicet	k4xCc7
lety	léto	k1gNnPc7
a	a	k8xC
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
vydali	vydat	k5eAaPmAgMnP
desítky	desítka	k1gFnPc4
modelů	model	k1gInPc2
ve	v	k7c6
stovkách	stovka	k1gFnPc6
barevných	barevný	k2eAgInPc2d1
provedení	provedení	k1gNnSc4
tenisek	teniska	k1gFnPc2
Jordan	Jordan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
značka	značka	k1gFnSc1
Nike	Nike	k1gFnSc1
výrazně	výrazně	k6eAd1
zlevnila	zlevnit	k5eAaPmAgFnS
a	a	k8xC
zvětšil	zvětšit	k5eAaPmAgInS
se	se	k3xPyFc4
její	její	k3xOp3gInSc4
celkový	celkový	k2eAgInSc4d1
obrat	obrat	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
paradoxně	paradoxně	k6eAd1
stal	stát	k5eAaPmAgMnS
nejvýdělečnějším	výdělečný	k2eAgMnSc7d3
sportovním	sportovní	k2eAgMnSc7d1
penzistou	penzista	k1gMnSc7
roku	rok	k1gInSc2
2015	#num#	k4
s	s	k7c7
astronomickým	astronomický	k2eAgInSc7d1
ročním	roční	k2eAgInSc7d1
příjmem	příjem	k1gInSc7
2	#num#	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Statistiky	statistika	k1gFnPc1
</s>
<s>
Jordanův	Jordanův	k2eAgInSc4d1
dres	dres	k1gInSc4
v	v	k7c6
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
</s>
<s>
SezónaZápasyKošeBodyBody	SezónaZápasyKošeBodyBod	k1gInPc1
na	na	k7c4
zápasTým	zápasTý	k1gMnSc7
</s>
<s>
1981	#num#	k4
<g/>
–	–	k?
<g/>
823519146013,5	823519146013,5	k4
<g/>
University	universita	k1gFnSc2
of	of	k?
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
</s>
<s>
1982	#num#	k4
<g/>
–	–	k?
<g/>
833628272120	#num#	k4
<g/>
University	universita	k1gFnSc2
of	of	k?
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
</s>
<s>
1983	#num#	k4
<g/>
–	–	k?
<g/>
843124760719,6	843124760719,6	k4
<g/>
University	universita	k1gFnSc2
of	of	k?
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
</s>
<s>
SezónaZápasyKošeBodyBody	SezónaZápasyKošeBodyBod	k1gInPc1
na	na	k7c4
zápasTým	zápasTí	k1gMnPc3
</s>
<s>
1984	#num#	k4
<g/>
–	–	k?
<g/>
8582837231328,2	8582837231328,2	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
861815040822,7	861815040822,7	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
87821098304137,1	87821098304137,1	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1987	#num#	k4
<g/>
–	–	k?
<g/>
88821069286835	#num#	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1988	#num#	k4
<g/>
–	–	k?
<g/>
8981966263332,5	8981966263332,5	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
90821034275333,6	90821034275333,6	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
9182990258031,5	9182990258031,5	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
9280943240430,1	9280943240430,1	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
9378992254132,6	9378992254132,6	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
951716645726,9	951716645726,9	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1995	#num#	k4
<g/>
–	–	k?
<g/>
9682916249130,4	9682916249130,4	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1996	#num#	k4
<g/>
–	–	k?
<g/>
9782920243129,6	9782920243129,6	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
9882881235728,7	9882881235728,7	k4
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
</s>
<s>
2001	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
260551137522,9	260551137522,9	k4
<g/>
Washington	Washington	k1gInSc1
Wizards	Wizards	k1gInSc4
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
382679164020	#num#	k4
<g/>
Washington	Washington	k1gInSc1
Wizards	Wizards	k1gInSc4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
1072121923229230,1	1072121923229230,1	k4
<g/>
-	-	kIx~
</s>
<s>
Trofeje	trofej	k1gFnPc1
a	a	k8xC
ocenění	ocenění	k1gNnSc1
</s>
<s>
Jordanovy	Jordanův	k2eAgInPc1d1
úspěchy	úspěch	k1gInPc1
vytesány	vytesán	k2eAgInPc1d1
před	před	k7c7
halou	hala	k1gFnSc7
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
</s>
<s>
Uveden	uveden	k2eAgMnSc1d1
do	do	k7c2
Síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
Naismith	Naismitha	k1gFnPc2
Memorial	Memorial	k1gInSc1
–	–	k?
2009	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
Olympijská	olympijský	k2eAgFnSc1d1
zlatá	zlatý	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
–	–	k?
1984	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
Vítěz	vítěz	k1gMnSc1
NBA	NBA	kA
–	–	k?
(	(	kIx(
<g/>
Chicago	Chicago	k1gNnSc1
Bulls	Bullsa	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
Nejužitečnější	užitečný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
finále	finále	k1gNnSc2
NBA	NBA	kA
–	–	k?
1991	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
Nejužitečnější	užitečný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
sezóny	sezóna	k1gFnSc2
NBA	NBA	kA
–	–	k?
1988	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
NBA	NBA	kA
–	–	k?
1987	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
×	×	k?
Vybrán	vybrán	k2eAgMnSc1d1
do	do	k7c2
All-Star	All-Stara	k1gFnPc2
turnaje	turnaj	k1gInSc2
–	–	k?
1985	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
Nejužitečnější	užitečný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
zápasu	zápas	k1gInSc2
All-Star	All-Star	k1gInSc1
–	–	k?
1988	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
×	×	k?
Vybrán	vybrat	k5eAaPmNgMnS
do	do	k7c2
týmu	tým	k1gInSc2
nejlepších	dobrý	k2eAgMnPc2d3
hráčů	hráč	k1gMnPc2
NBA	NBA	kA
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
Vítěz	vítěz	k1gMnSc1
NBA	NBA	kA
Slam	slam	k1gInSc4
Dunk	Dunka	k1gFnPc2
turnaje	turnaj	k1gInSc2
–	–	k?
1987	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
</s>
<s>
Nováček	Nováček	k1gMnSc1
roku	rok	k1gInSc2
–	–	k?
1984	#num#	k4
</s>
<s>
Obránce	obránce	k1gMnSc1
roku	rok	k1gInSc2
–	–	k?
1987	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VAVRDA	VAVRDA	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
53	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
nehraje	hrát	k5eNaImIp3nS
už	už	k6eAd1
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
Jordan	Jordan	k1gMnSc1
vydělává	vydělávat	k5eAaImIp3nS
2	#num#	k4
miliardy	miliarda	k4xCgFnSc2
ročně	ročně	k6eAd1
<g/>
..	..	k?
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
3.4	3.4	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
3.4	3.4	k4
<g/>
.2016	.2016	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NBA	NBA	kA
History	Histor	k1gInPc4
<g/>
:	:	kIx,
Jordan	Jordan	k1gMnSc1
bio	bio	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NBA	NBA	kA
Player	Player	k1gInSc1
Stats	Stats	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Basketball-Reference	Basketball-Reference	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
–	–	k?
Basketbal	basketbal	k1gInSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1984	#num#	k4
–	–	k?
turnaj	turnaj	k1gInSc1
mužů	muž	k1gMnPc2
USA	USA	kA
</s>
<s>
Steve	Steve	k1gMnSc1
Alford	Alford	k1gMnSc1
•	•	k?
Patrick	Patrick	k1gMnSc1
Ewing	Ewing	k1gMnSc1
•	•	k?
Vern	Vern	k1gInSc1
Fleming	Fleming	k1gInSc1
•	•	k?
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
•	•	k?
Joe	Joe	k1gMnSc5
Kleine	Klein	k1gMnSc5
•	•	k?
Jon	Jon	k1gMnSc1
Koncak	Koncak	k1gMnSc1
•	•	k?
Chris	Chris	k1gFnSc2
Mullin	Mullina	k1gFnPc2
•	•	k?
Sam	Sam	k1gMnSc1
Perkins	Perkinsa	k1gFnPc2
•	•	k?
Alvin	Alvin	k1gMnSc1
Robertson	Robertson	k1gMnSc1
•	•	k?
Wayman	Wayman	k1gMnSc1
Tisdale	Tisdala	k1gFnSc3
•	•	k?
Jeff	Jeff	k1gMnSc1
Turner	turner	k1gMnSc1
•	•	k?
Leon	Leona	k1gFnPc2
Wood	Wood	k1gMnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
José	José	k6eAd1
Manuel	Manuel	k1gMnSc1
Beiran	Beirana	k1gFnPc2
•	•	k?
José	Josá	k1gFnSc2
Luis	Luisa	k1gFnPc2
Llorente	Llorent	k1gInSc5
•	•	k?
Fernando	Fernanda	k1gFnSc5
Arcega	Arceg	k1gMnSc2
•	•	k?
José	Josá	k1gFnSc2
Maria	Maria	k1gFnSc1
Margall	Margall	k1gMnSc1
•	•	k?
Andrés	Andrés	k1gInSc1
Jiménez	Jiménez	k1gInSc1
•	•	k?
Fernando	Fernanda	k1gFnSc5
Romay	Romay	k1gInPc1
•	•	k?
Fernando	Fernanda	k1gFnSc5
Martín	Martín	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Corbalán	Corbalán	k2eAgMnSc1d1
•	•	k?
Ignacio	Ignacio	k6eAd1
Solozábal	Solozábal	k1gInSc1
•	•	k?
Juan	Juan	k1gMnSc1
Domingo	Domingo	k1gMnSc1
De	De	k?
la	la	k1gNnPc2
Cruz	Cruz	k1gInSc1
•	•	k?
José	Josá	k1gFnSc2
Manuel	Manuel	k1gMnSc1
López	López	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Antonio	Antonio	k1gMnSc1
San	San	k1gMnSc1
Epifanio	Epifanio	k1gMnSc1
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Dražen	dražen	k2eAgMnSc1d1
Petrović	Petrović	k1gMnSc1
•	•	k?
Aleksandar	Aleksandar	k1gInSc1
Petrovic	Petrovice	k1gFnPc2
•	•	k?
Nebojsa	nebojsa	k1gMnSc1
Zorkic	Zorkic	k1gMnSc1
•	•	k?
Rajko	rajka	k1gFnSc5
Zizic	Zizic	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Sunara	Sunar	k1gMnSc2
•	•	k?
Emir	Emir	k1gMnSc1
Mutapcic	Mutapcic	k1gMnSc1
•	•	k?
Sabit	Sabit	k1gMnSc1
Hadzic	Hadzic	k1gMnSc1
•	•	k?
Andro	Andro	k1gNnSc4
Knego	Knego	k6eAd1
•	•	k?
Ratko	Ratko	k1gNnSc1
Radovanovic	Radovanovice	k1gFnPc2
•	•	k?
Mihovil	Mihovil	k1gFnSc2
Nakic-Vojnovic	Nakic-Vojnovice	k1gFnPc2
•	•	k?
Dražen	dražen	k2eAgMnSc1d1
Dalipagić	Dalipagić	k1gMnSc1
•	•	k?
Branko	branka	k1gFnSc5
Vukicevic	Vukicevice	k1gFnPc2
</s>
<s>
Medailisté	medailista	k1gMnPc1
–	–	k?
Basketbal	basketbal	k1gInSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1992	#num#	k4
–	–	k?
turnaj	turnaj	k1gInSc1
mužů	muž	k1gMnPc2
USA	USA	kA
</s>
<s>
Charles	Charles	k1gMnSc1
Barkley	Barklea	k1gFnSc2
•	•	k?
Larry	Larra	k1gFnSc2
Bird	Bird	k1gMnSc1
•	•	k?
Clyde	Clyd	k1gInSc5
Drexler	Drexler	k1gInSc4
•	•	k?
Patrick	Patrick	k1gInSc1
Ewing	Ewing	k1gInSc1
•	•	k?
Earvin	Earvina	k1gFnPc2
'	'	kIx"
<g/>
Magic	Magic	k1gMnSc1
<g/>
'	'	kIx"
Johnson	Johnson	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
•	•	k?
Christian	Christian	k1gMnSc1
Laettner	Laettner	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Malone	Malon	k1gInSc5
•	•	k?
Chris	Chris	k1gInSc1
Mullin	Mullin	k1gInSc1
•	•	k?
Scottie	Scottie	k1gFnSc2
Pippen	Pippen	k2eAgMnSc1d1
•	•	k?
David	David	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Stockton	Stockton	k1gInSc4
Chorvatsko	Chorvatsko	k1gNnSc4
</s>
<s>
Vladan	Vladana	k1gFnPc2
Alanovič	Alanovič	k1gMnSc1
•	•	k?
Franjo	Franjo	k1gMnSc1
Arapovič	Arapovič	k1gMnSc1
•	•	k?
Danko	Danka	k1gFnSc5
Cvjetičanin	Cvjetičanin	k2eAgInSc1d1
•	•	k?
Alan	alan	k1gInSc1
Gregov	Gregov	k1gInSc1
•	•	k?
Arijan	Arijan	k1gInSc1
Komazec	Komazec	k1gMnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kukoč	Kukoč	k1gMnSc1
•	•	k?
Aramis	Aramis	k1gInSc1
Naglič	Naglič	k1gMnSc1
•	•	k?
Velimir	Velimir	k1gMnSc1
Perasovič	Perasovič	k1gMnSc1
•	•	k?
Dražen	dražen	k2eAgMnSc1d1
Petrović	Petrović	k1gMnSc1
•	•	k?
Dino	Dino	k1gMnSc1
Rada	Rada	k1gMnSc1
•	•	k?
Zan	Zan	k1gMnSc1
Tabak	Tabak	k1gMnSc1
•	•	k?
Stojko	Stojka	k1gMnSc5
Vrankovič	Vrankovič	k1gMnSc1
Litva	Litva	k1gFnSc1
</s>
<s>
Romanas	Romanas	k1gMnSc1
Brazdauskis	Brazdauskis	k1gFnSc2
•	•	k?
Valdemaras	Valdemaras	k1gMnSc1
Chomicius	Chomicius	k1gMnSc1
•	•	k?
Darius	Darius	k1gMnSc1
Dimavicius	Dimavicius	k1gMnSc1
•	•	k?
Gintaras	Gintaras	k1gMnSc1
Einikis	Einikis	k1gFnSc2
•	•	k?
Sergejus	Sergejus	k1gInSc1
Jovaisa	Jovais	k1gMnSc2
•	•	k?
Arturas	Arturas	k1gMnSc1
Karnisovas	Karnisovas	k1gMnSc1
•	•	k?
Gintaras	Gintaras	k1gMnSc1
Krapikas	Krapikas	k1gMnSc1
•	•	k?
Rimas	Rimas	k1gInSc1
Kurtinaitis	Kurtinaitis	k1gFnSc1
•	•	k?
Šarū	Šarū	k1gInSc1
Marčiulionis	Marčiulionis	k1gFnSc2
•	•	k?
Alvydas	Alvydas	k1gMnSc1
Pazdrazdis	Pazdrazdis	k1gFnSc2
•	•	k?
Arvydas	Arvydas	k1gMnSc1
Sabonis	Sabonis	k1gFnSc2
•	•	k?
Arunas	Arunas	k1gMnSc1
Visockas	Visockas	k1gMnSc1
</s>
<s>
Basketbal	basketbal	k1gInSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1992	#num#	k4
–	–	k?
Americký	americký	k2eAgInSc1d1
mužský	mužský	k2eAgInSc1d1
basketbalový	basketbalový	k2eAgInSc1d1
tým	tým	k1gInSc1
–	–	k?
„	„	k?
<g/>
Dream	Dream	k1gInSc1
Team	team	k1gInSc1
<g/>
“	“	k?
</s>
<s>
Charles	Charles	k1gMnSc1
Barkley	Barklea	k1gFnSc2
•	•	k?
Larry	Larra	k1gFnSc2
Bird	Bird	k1gMnSc1
•	•	k?
Clyde	Clyd	k1gInSc5
Drexler	Drexler	k1gMnSc1
•	•	k?
Patrick	Patrick	k1gMnSc1
Ewing	Ewing	k1gMnSc1
•	•	k?
Magic	Magic	k1gMnSc1
Johnson	Johnson	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
•	•	k?
Christian	Christian	k1gMnSc1
Laettner	Laettner	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Malone	Malon	k1gInSc5
•	•	k?
Chris	Chris	k1gInSc1
Mullin	Mullin	k1gInSc1
•	•	k?
Scottie	Scottie	k1gFnSc2
Pippen	Pippen	k2eAgMnSc1d1
•	•	k?
David	David	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Stockton	Stockton	k1gInSc1
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Chuck	Chuck	k1gMnSc1
Daly	dát	k5eAaPmAgFnP
</s>
<s>
Associated	Associated	k1gMnSc1
Press	Pressa	k1gFnPc2
sportovec	sportovec	k1gMnSc1
roku	rok	k1gInSc2
-	-	kIx~
muži	muž	k1gMnPc1
</s>
<s>
1931	#num#	k4
Pepper	Pepper	k1gMnSc1
Martin	Martin	k1gMnSc1
•	•	k?
1932	#num#	k4
Gene	gen	k1gInSc5
Sarazen	Sarazen	k2eAgInSc4d1
•	•	k?
1933	#num#	k4
Carl	Carl	k1gInSc1
Hubbell	Hubbell	k1gInSc4
•	•	k?
1934	#num#	k4
Dizzy	Dizza	k1gFnSc2
Dean	Deana	k1gFnPc2
•	•	k?
1935	#num#	k4
Joe	Joe	k1gFnSc1
Louis	louis	k1gInSc2
•	•	k?
1936	#num#	k4
Jesse	Jesse	k1gFnSc2
Owens	Owensa	k1gFnPc2
•	•	k?
1937	#num#	k4
Don	Don	k1gInSc1
Budge	Budge	k1gInSc4
•	•	k?
1938	#num#	k4
Don	Don	k1gInSc1
Budge	Budge	k1gInSc4
•	•	k?
1939	#num#	k4
Nile	Nil	k1gInSc5
Kinnick	Kinnick	k1gInSc4
•	•	k?
1940	#num#	k4
Tom	Tom	k1gMnSc1
Harmon	Harmon	k1gMnSc1
•	•	k?
1941	#num#	k4
Joe	Joe	k1gFnSc6
DiMaggio	DiMaggio	k1gNnSc1
•	•	k?
1942	#num#	k4
Frank	frank	k1gInSc1
Sinkwich	Sinkwich	k1gInSc4
•	•	k?
1943	#num#	k4
Gunder	Gunder	k1gInSc1
Hägg	Hägg	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1944	#num#	k4
Byron	Byrona	k1gFnPc2
Nelson	Nelson	k1gMnSc1
•	•	k?
1945	#num#	k4
Byron	Byrona	k1gFnPc2
Nelson	Nelson	k1gMnSc1
•	•	k?
1946	#num#	k4
Glenn	Glenn	k1gInSc1
Davis	Davis	k1gInSc4
•	•	k?
1947	#num#	k4
Johnny	Johnna	k1gFnSc2
Lujack	Lujacka	k1gFnPc2
•	•	k?
1948	#num#	k4
Lou	Lou	k1gFnSc1
Boudreau	Boudreaus	k1gInSc2
•	•	k?
1949	#num#	k4
Leon	Leona	k1gFnPc2
Hart	Harta	k1gFnPc2
•	•	k?
1950	#num#	k4
Jim	on	k3xPp3gMnPc3
Konstanty	konstanta	k1gFnSc2
•	•	k?
1951	#num#	k4
Dick	Dick	k1gInSc1
Kazmaier	Kazmaier	k1gInSc4
•	•	k?
1952	#num#	k4
Bob	bob	k1gInSc1
Mathias	Mathias	k1gInSc4
•	•	k?
1953	#num#	k4
Ben	Ben	k1gInSc1
Hogan	Hogan	k1gInSc4
•	•	k?
1954	#num#	k4
Willie	Willie	k1gFnSc2
Mays	Maysa	k1gFnPc2
•	•	k?
1955	#num#	k4
Howard	Howard	k1gInSc1
Cassady	Cassada	k1gFnSc2
•	•	k?
1956	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Mickey	Mickea	k1gFnSc2
Mantle	Mantle	k?
•	•	k?
1957	#num#	k4
Ted	Ted	k1gMnSc1
Williams	Williams	k1gInSc1
•	•	k?
1958	#num#	k4
Herb	Herb	k1gInSc1
Elliott	Elliott	k1gInSc4
•	•	k?
1959	#num#	k4
Ingemar	Ingemar	k1gInSc1
Johansson	Johansson	k1gInSc4
•	•	k?
1960	#num#	k4
Rafer	Rafer	k1gInSc1
Johnson	Johnson	k1gInSc4
•	•	k?
1961	#num#	k4
Roger	Roger	k1gInSc1
Maris	Maris	k1gInSc4
•	•	k?
1962	#num#	k4
Maury	Maur	k1gMnPc4
Wills	Willsa	k1gFnPc2
•	•	k?
1963	#num#	k4
Sandy	Sanda	k1gFnSc2
Koufax	Koufax	k1gInSc1
•	•	k?
1964	#num#	k4
Don	Don	k1gInSc1
Schollander	Schollander	k1gInSc4
•	•	k?
1965	#num#	k4
Sandy	Sanda	k1gFnSc2
Koufax	Koufax	k1gInSc1
•	•	k?
1966	#num#	k4
Frank	Frank	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
1967	#num#	k4
Carl	Carl	k1gInSc1
Yastrzemski	Yastrzemsk	k1gFnSc2
•	•	k?
1968	#num#	k4
Denny	Denna	k1gFnSc2
McLain	McLaina	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1969	#num#	k4
Tom	Tom	k1gMnSc1
Seaver	Seaver	k1gMnSc1
•	•	k?
1970	#num#	k4
George	George	k1gFnSc1
Blanda	Blanda	k1gFnSc1
•	•	k?
1971	#num#	k4
Lee	Lea	k1gFnSc6
Trevino	Trevin	k2eAgNnSc1d1
•	•	k?
1972	#num#	k4
Mark	Mark	k1gMnSc1
Spitz	Spitz	k1gMnSc1
•	•	k?
1973	#num#	k4
O.	O.	kA
J.	J.	kA
Simpson	Simpson	k1gInSc1
•	•	k?
1974	#num#	k4
Muhammad	Muhammad	k1gInSc1
Ali	Ali	k1gFnSc2
•	•	k?
1975	#num#	k4
Fred	Fred	k1gMnSc1
Lynn	Lynn	k1gMnSc1
•	•	k?
1976	#num#	k4
Bruce	Bruce	k1gFnSc2
Jenner	Jennra	k1gFnPc2
•	•	k?
1977	#num#	k4
Steve	Steve	k1gMnSc1
Cauthen	Cauthen	k1gInSc1
•	•	k?
1978	#num#	k4
Ron	ron	k1gInSc1
Guidry	Guidr	k1gInPc1
•	•	k?
1979	#num#	k4
Willie	Willie	k1gFnSc2
Stargell	Stargella	k1gFnPc2
•	•	k?
1980	#num#	k4
Hokejový	hokejový	k2eAgInSc1d1
tým	tým	k1gInSc1
USA	USA	kA
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1981	#num#	k4
John	John	k1gMnSc1
McEnroe	McEnro	k1gInSc2
•	•	k?
1982	#num#	k4
Wayne	Wayn	k1gInSc5
Gretzky	Gretzek	k1gInPc7
•	•	k?
1983	#num#	k4
Carl	Carl	k1gInSc1
Lewis	Lewis	k1gInSc4
•	•	k?
1984	#num#	k4
Carl	Carl	k1gInSc1
Lewis	Lewis	k1gInSc4
•	•	k?
1985	#num#	k4
Dwight	Dwightum	k1gNnPc2
Gooden	Goodna	k1gFnPc2
•	•	k?
1986	#num#	k4
Larry	Larra	k1gFnSc2
Bird	Birda	k1gFnPc2
•	•	k?
1987	#num#	k4
Ben	Ben	k1gInSc1
Johnson	Johnson	k1gInSc4
•	•	k?
1988	#num#	k4
Orel	Orel	k1gMnSc1
Hershiser	Hershiser	k1gMnSc1
•	•	k?
1989	#num#	k4
Joe	Joe	k1gFnSc1
Montana	Montana	k1gFnSc1
•	•	k?
1990	#num#	k4
Joe	Joe	k1gFnSc1
Montana	Montana	k1gFnSc1
•	•	k?
1991	#num#	k4
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
•	•	k?
1992	#num#	k4
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
•	•	k?
1993	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
•	•	k?
1994	#num#	k4
George	Georg	k1gInSc2
Foreman	Foreman	k1gMnSc1
•	•	k?
1995	#num#	k4
Cal	Cal	k1gMnSc2
Ripken	Ripken	k1gInSc4
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc4
<g/>
.	.	kIx.
•	•	k?
1996	#num#	k4
Michael	Michael	k1gMnSc1
Johnson	Johnson	k1gMnSc1
•	•	k?
1997	#num#	k4
Tiger	Tiger	k1gInSc1
Woods	Woods	k1gInSc4
•	•	k?
1998	#num#	k4
Mark	Mark	k1gMnSc1
McGwire	McGwir	k1gInSc5
•	•	k?
1999	#num#	k4
Tiger	Tiger	k1gInSc1
Woods	Woods	k1gInSc4
•	•	k?
2000	#num#	k4
Tiger	Tiger	k1gInSc1
Woods	Woods	k1gInSc4
•	•	k?
2001	#num#	k4
Barry	Barra	k1gFnSc2
Bonds	Bondsa	k1gFnPc2
•	•	k?
2002	#num#	k4
Lance	lance	k1gNnSc2
Armstrong	Armstrong	k1gMnSc1
•	•	k?
2003	#num#	k4
Lance	lance	k1gNnSc2
Armstrong	Armstrong	k1gMnSc1
•	•	k?
2004	#num#	k4
Lance	lance	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
Armstrong	Armstrong	k1gMnSc1
•	•	k?
2005	#num#	k4
Lance	lance	k1gNnSc2
Armstrong	Armstrong	k1gMnSc1
•	•	k?
2006	#num#	k4
Tiger	Tiger	k1gInSc1
Woods	Woods	k1gInSc4
•	•	k?
2007	#num#	k4
Tom	Tom	k1gMnSc1
Brady	brada	k1gFnSc2
•	•	k?
2008	#num#	k4
Michael	Michaela	k1gFnPc2
Phelps	Phelpsa	k1gFnPc2
•	•	k?
2009	#num#	k4
Jimmie	Jimmie	k1gFnSc2
Johnson	Johnsona	k1gFnPc2
•	•	k?
2010	#num#	k4
Drew	Drew	k1gFnSc2
Brees	Breesa	k1gFnPc2
•	•	k?
2011	#num#	k4
Aaron	Aaron	k1gInSc1
Rodgers	Rodgers	k1gInSc4
•	•	k?
2012	#num#	k4
Michael	Michaela	k1gFnPc2
Phelps	Phelpsa	k1gFnPc2
•	•	k?
2013	#num#	k4
LeBron	LeBron	k1gInSc1
James	James	k1gInSc4
•	•	k?
2014	#num#	k4
Madison	Madison	k1gInSc1
Bumgarner	Bumgarner	k1gInSc4
•	•	k?
2015	#num#	k4
Stephen	Stephen	k1gInSc1
Curry	Curra	k1gFnSc2
•	•	k?
2016	#num#	k4
LeBron	LeBron	k1gInSc1
James	James	k1gInSc4
•	•	k?
2017	#num#	k4
José	Josá	k1gFnSc2
Altuve	Altuev	k1gFnSc2
•	•	k?
2018	#num#	k4
LeBron	LeBron	k1gInSc1
James	James	k1gInSc4
•	•	k?
2019	#num#	k4
Kawhi	Kawhi	k1gNnPc2
Leonard	Leonarda	k1gFnPc2
•	•	k?
2020	#num#	k4
LeBron	LeBron	k1gMnSc1
James	James	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20080215004	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119184486	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1455	#num#	k4
4934	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
86020198	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
114945073	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
86020198	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
