<s>
Anthony	Anthon	k1gInPc1	Anthon
Kiedis	Kiedis	k1gFnSc2	Kiedis
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Rapids	Rapids	k1gInSc1	Rapids
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Red	Red	k1gMnSc1	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnSc3	Chil
Peppers	Peppersa	k1gFnPc2	Peppersa
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
Michael	Michael	k1gMnSc1	Michael
Flea	Flea	k1gMnSc1	Flea
Balzary	Balzara	k1gFnSc2	Balzara
založili	založit	k5eAaPmAgMnP	založit
tuto	tento	k3xDgFnSc4	tento
kalifornskou	kalifornský	k2eAgFnSc4d1	kalifornská
punkfunkrockovou	punkfunkrockový	k2eAgFnSc4d1	punkfunkrockový
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
první	první	k4xOgFnSc7	první
písničkou	písnička	k1gFnSc7	písnička
byla	být	k5eAaImAgFnS	být
Out	Out	k1gFnSc1	Out
in	in	k?	in
L.A.	L.A.	k1gMnSc1	L.A.
Nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
ji	on	k3xPp3gFnSc4	on
Anthony	Anthon	k1gInPc4	Anthon
(	(	kIx(	(
<g/>
nespal	spát	k5eNaImAgMnS	spát
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
píseň	píseň	k1gFnSc4	píseň
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
-	-	kIx~	-
chtěl	chtít	k5eAaImAgMnS	chtít
překvapit	překvapit	k5eAaPmF	překvapit
zbytek	zbytek	k1gInSc4	zbytek
kapely	kapela	k1gFnSc2	kapela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kiedis	Kiedis	k1gInSc1	Kiedis
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
energickým	energický	k2eAgInSc7d1	energický
projevem	projev	k1gInSc7	projev
při	při	k7c6	při
živých	živý	k2eAgInPc6d1	živý
vystoupeních	vystoupení	k1gNnPc6	vystoupení
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
mírně	mírně	k6eAd1	mírně
tajemných	tajemný	k2eAgInPc2d1	tajemný
textů	text	k1gInPc2	text
této	tento	k3xDgFnSc2	tento
světoznámé	světoznámý	k2eAgFnSc2d1	světoznámá
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
během	během	k7c2	během
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc1	jeho
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
kouřil	kouřit	k5eAaImAgMnS	kouřit
marihuanu	marihuana	k1gFnSc4	marihuana
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgFnP	přidat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Bral	brát	k5eAaImAgMnS	brát
kokain	kokain	k1gInSc4	kokain
i	i	k8xC	i
heroin	heroin	k1gInSc4	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
skupiny	skupina	k1gFnSc2	skupina
Hillel	Hillel	k1gMnSc1	Hillel
Slovak	Slovak	k1gMnSc1	Slovak
<g/>
,	,	kIx,	,
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
na	na	k7c4	na
pět	pět	k4xCc4	pět
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
přestal	přestat	k5eAaPmAgMnS	přestat
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
přestal	přestat	k5eAaPmAgInS	přestat
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgInPc4	svůj
velmi	velmi	k6eAd1	velmi
naturalistické	naturalistický	k2eAgInPc4d1	naturalistický
memoáry	memoáry	k1gInPc4	memoáry
s	s	k7c7	s
názvem	název	k1gInSc7	název
Scar	Scar	k1gInSc4	Scar
Tissue	Tissu	k1gInSc2	Tissu
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
panictví	panictví	k1gNnSc6	panictví
přišel	přijít	k5eAaPmAgMnS	přijít
asi	asi	k9	asi
ve	v	k7c6	v
12	[number]	k4	12
letech	let	k1gInPc6	let
s	s	k7c7	s
otcovou	otcův	k2eAgFnSc7d1	otcova
18	[number]	k4	18
<g/>
letou	letý	k2eAgFnSc7d1	letá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
prý	prý	k9	prý
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
chuť	chuť	k1gFnSc1	chuť
ochutnat	ochutnat	k5eAaPmF	ochutnat
všechny	všechen	k3xTgFnPc4	všechen
holky	holka	k1gFnPc4	holka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
nedávna	nedávno	k1gNnSc2	nedávno
stál	stát	k5eAaImAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
fanklubu	fanklub	k1gInSc2	fanklub
RHCP	RHCP	kA	RHCP
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
Anthony	Anthona	k1gFnSc2	Anthona
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nosí	nosit	k5eAaImIp3nS	nosit
starostlivě	starostlivě	k6eAd1	starostlivě
ve	v	k7c6	v
vaku	vak	k1gInSc6	vak
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
jógu	jóga	k1gFnSc4	jóga
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zdravým	zdravý	k2eAgInSc7d1	zdravý
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
Anthonymu	Anthonym	k1gInSc6	Anthonym
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Everly	Everla	k1gFnSc2	Everla
Bear	Bear	k1gMnSc1	Bear
<g/>
.	.	kIx.	.
</s>
<s>
Matkou	matka	k1gFnSc7	matka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jeho	jeho	k3xOp3gFnSc1	jeho
bývalá	bývalý	k2eAgFnSc1d1	bývalá
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
Heather	Heathra	k1gFnPc2	Heathra
Christie	Christie	k1gFnSc2	Christie
<g/>
.	.	kIx.	.
</s>
<s>
Anthony	Anthon	k1gInPc4	Anthon
měl	mít	k5eAaImAgInS	mít
po	po	k7c4	po
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
hnědé	hnědý	k2eAgInPc1d1	hnědý
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
nad	nad	k7c4	nad
zadek	zadek	k1gInSc4	zadek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
Californication	Californication	k1gInSc1	Californication
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
ostříhal	ostříhat	k5eAaImAgMnS	ostříhat
a	a	k8xC	a
obarvil	obarvit	k5eAaPmAgMnS	obarvit
na	na	k7c4	na
blond	blond	k1gInSc4	blond
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
opět	opět	k6eAd1	opět
narůst	narůst	k5eAaPmF	narůst
po	po	k7c4	po
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
krátké	krátký	k2eAgInPc4d1	krátký
vlasy	vlas	k1gInPc4	vlas
s	s	k7c7	s
patkou	patka	k1gFnSc7	patka
a	a	k8xC	a
knír	knír	k1gInSc1	knír
<g/>
.	.	kIx.	.
</s>
