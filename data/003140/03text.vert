<s>
Divize	divize	k1gFnSc1	divize
je	být	k5eAaImIp3nS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
asi	asi	k9	asi
dvacet	dvacet	k4xCc1	dvacet
tisíc	tisíc	k4xCgInSc1	tisíc
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
armád	armáda	k1gFnPc2	armáda
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
pluků	pluk	k1gInPc2	pluk
nebo	nebo	k8xC	nebo
brigád	brigáda	k1gFnPc2	brigáda
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
několik	několik	k4yIc4	několik
divizí	divize	k1gFnPc2	divize
tvoří	tvořit	k5eAaImIp3nS	tvořit
armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
jednotka	jednotka	k1gFnSc1	jednotka
běžné	běžný	k2eAgFnSc2d1	běžná
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
vojenské	vojenský	k2eAgFnPc4d1	vojenská
operace	operace	k1gFnPc4	operace
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
u	u	k7c2	u
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
nebo	nebo	k8xC	nebo
u	u	k7c2	u
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
divize	divize	k1gFnPc1	divize
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
značí	značit	k5eAaImIp3nS	značit
prostou	prostý	k2eAgFnSc7d1	prostá
řadovou	řadový	k2eAgFnSc7d1	řadová
číslovkou	číslovka	k1gFnSc7	číslovka
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
divize	divize	k1gFnSc2	divize
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
maršál	maršál	k1gMnSc1	maršál
Maurice	Maurika	k1gFnSc3	Maurika
de	de	k?	de
Saxe	sax	k1gInSc5	sax
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Mes	Mes	k1gMnSc1	Mes
Réveries	Réveries	k1gMnSc1	Réveries
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
(	(	kIx(	(
<g/>
maršál	maršál	k1gMnSc1	maršál
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
vydáním	vydání	k1gNnSc7	vydání
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
divize	divize	k1gFnSc1	divize
byly	být	k5eAaImAgFnP	být
zavedeny	zavést	k5eAaPmNgFnP	zavést
až	až	k9	až
v	v	k7c6	v
sedmileté	sedmiletý	k2eAgFnSc6d1	sedmiletá
válce	válka	k1gFnSc6	válka
maršálem	maršál	k1gMnSc7	maršál
de	de	k?	de
Broglie	Broglie	k1gFnSc1	Broglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
přešla	přejít	k5eAaPmAgFnS	přejít
trvale	trvale	k6eAd1	trvale
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
získal	získat	k5eAaPmAgInS	získat
francouzský	francouzský	k2eAgInSc1d1	francouzský
model	model	k1gInSc1	model
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
užívající	užívající	k2eAgMnPc1d1	užívající
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
divize	divize	k1gFnPc4	divize
<g/>
,	,	kIx,	,
věhlas	věhlas	k1gInSc4	věhlas
a	a	k8xC	a
divize	divize	k1gFnPc4	divize
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
armád	armáda	k1gFnPc2	armáda
včetně	včetně	k7c2	včetně
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Velitelem	velitel	k1gMnSc7	velitel
divize	divize	k1gFnSc2	divize
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
generálmajor	generálmajor	k1gMnSc1	generálmajor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
hodnost	hodnost	k1gFnSc4	hodnost
divizní	divizní	k2eAgMnSc1d1	divizní
generál	generál	k1gMnSc1	generál
<g/>
.	.	kIx.	.
</s>
