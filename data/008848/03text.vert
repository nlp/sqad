<p>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
sirovodík	sirovodík	k1gInSc1	sirovodík
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
tvořit	tvořit	k5eAaImF	tvořit
rozkladem	rozklad	k1gInSc7	rozklad
organického	organický	k2eAgInSc2d1	organický
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
bioplynu	bioplyn	k1gInSc2	bioplyn
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
často	často	k6eAd1	často
musí	muset	k5eAaImIp3nS	muset
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
síranů	síran	k1gInPc2	síran
při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sulfanová	Sulfanový	k2eAgFnSc1d1	Sulfanový
řada	řada	k1gFnSc1	řada
==	==	k?	==
</s>
</p>
<p>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
je	být	k5eAaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgFnSc7d3	nejjednodušší
sloučeninou	sloučenina	k1gFnSc7	sloučenina
síry	síra	k1gFnSc2	síra
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nejstálejší	stálý	k2eAgFnSc1d3	nejstálejší
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
sloučenina	sloučenina	k1gFnSc1	sloučenina
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
členem	člen	k1gInSc7	člen
homologické	homologický	k2eAgFnSc2d1	homologická
řady	řada	k1gFnSc2	řada
sulfanů	sulfan	k1gMnPc2	sulfan
<g/>
.	.	kIx.	.
</s>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
vzorec	vzorec	k1gInSc1	vzorec
těchto	tento	k3xDgFnPc2	tento
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
Sn	Sn	k1gFnSc2	Sn
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
např.	např.	kA	např.
vzorec	vzorec	k1gInSc1	vzorec
disulfanu	disulfan	k1gInSc2	disulfan
je	být	k5eAaImIp3nS	být
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
trisulfanu	trisulfanout	k5eAaPmIp1nS	trisulfanout
H2S3	H2S3	k1gMnSc1	H2S3
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Soli	sůl	k1gFnPc1	sůl
odpovídajících	odpovídající	k2eAgFnPc2d1	odpovídající
kyselin	kyselina	k1gFnPc2	kyselina
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
polysulfidy	polysulfida	k1gFnPc1	polysulfida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
sulfidu	sulfid	k1gInSc2	sulfid
železnatého	železnatý	k2eAgMnSc4d1	železnatý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
připravit	připravit	k5eAaPmF	připravit
přímým	přímý	k2eAgNnSc7d1	přímé
sloučením	sloučení	k1gNnSc7	sloučení
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
FeS	fes	k1gNnSc1	fes
+	+	kIx~	+
2	[number]	k4	2
HCl	HCl	k1gMnSc1	HCl
→	→	k?	→
H2S	H2S	k1gMnSc1	H2S
+	+	kIx~	+
FeCl	FeCl	k1gMnSc1	FeCl
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
+	+	kIx~	+
S	s	k7c7	s
→	→	k?	→
H2S	H2S	k1gFnSc7	H2S
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnSc3	vlastnost
sulfanu	sulfan	k1gInSc2	sulfan
==	==	k?	==
</s>
</p>
<p>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
zapáchající	zapáchající	k2eAgMnSc1d1	zapáchající
po	po	k7c6	po
zkažených	zkažený	k2eAgNnPc6d1	zkažené
vejcích	vejce	k1gNnPc6	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
zkapalňuje	zkapalňovat	k5eAaImIp3nS	zkapalňovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kapalinách	kapalina	k1gFnPc6	kapalina
včetně	včetně	k7c2	včetně
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
sulfanová	sulfanová	k1gFnSc1	sulfanová
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
sirovodíková	sirovodíkový	k2eAgFnSc1d1	sirovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
vzorec	vzorec	k1gInSc1	vzorec
sulfanu	sulfanout	k5eAaPmIp1nS	sulfanout
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
slabá	slabý	k2eAgFnSc1d1	slabá
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
soli	sůl	k1gFnPc4	sůl
dvojího	dvojí	k4xRgInSc2	dvojí
typu	typ	k1gInSc2	typ
-	-	kIx~	-
sulfidy	sulfid	k1gInPc4	sulfid
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
a	a	k8xC	a
hydrogensulfidy	hydrogensulfid	k1gInPc1	hydrogensulfid
(	(	kIx(	(
<g/>
HS-	HS-	k1gFnSc1	HS-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bakteriích	bakterie	k1gFnPc6	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
sopkách	sopka	k1gFnPc6	sopka
<g/>
,	,	kIx,	,
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
pevná	pevný	k2eAgFnSc1d1	pevná
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
bakterie	bakterie	k1gFnSc1	bakterie
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
pod	pod	k7c4	pod
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
spalování	spalování	k1gNnSc2	spalování
sulfanu	sulfana	k1gFnSc4	sulfana
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
tzv.	tzv.	kA	tzv.
spalování	spalování	k1gNnSc2	spalování
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
tj.	tj.	kA	tj.
za	za	k7c2	za
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
spalování	spalování	k1gNnSc1	spalování
nedokonalé	dokonalý	k2eNgFnSc2d1	nedokonalá
tj.	tj.	kA	tj.
za	za	k7c2	za
nedostatečného	dostatečný	k2eNgInSc2d1	nedostatečný
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalým	dokonalý	k2eAgNnSc7d1	dokonalé
spalováním	spalování	k1gNnSc7	spalování
sulfanu	sulfan	k1gInSc2	sulfan
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2	[number]	k4	2
H2S	H2S	k1gFnSc1	H2S
+	+	kIx~	+
3	[number]	k4	3
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
2	[number]	k4	2
SO	So	kA	So
<g/>
2	[number]	k4	2
<g/>
Při	při	k7c6	při
nedokonalém	dokonalý	k2eNgNnSc6d1	nedokonalé
spalování	spalování	k1gNnSc6	spalování
vzniká	vznikat	k5eAaImIp3nS	vznikat
síra	síra	k1gFnSc1	síra
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2	[number]	k4	2
H2S	H2S	k1gFnSc1	H2S
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
2	[number]	k4	2
S	s	k7c7	s
+	+	kIx~	+
2	[number]	k4	2
H2OV	H2OV	k1gFnSc6	H2OV
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
se	se	k3xPyFc4	se
sulfan	sulfan	k1gInSc1	sulfan
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jím	jíst	k5eAaImIp1nS	jíst
totiž	totiž	k9	totiž
vysrážet	vysrážet	k5eAaPmF	vysrážet
nerozpustné	rozpustný	k2eNgInPc1d1	nerozpustný
sulfidy	sulfid	k1gInPc1	sulfid
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
sulfid	sulfid	k1gInSc1	sulfid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
a	a	k8xC	a
dokázat	dokázat	k5eAaPmF	dokázat
tak	tak	k6eAd1	tak
přítomnost	přítomnost	k1gFnSc4	přítomnost
daných	daný	k2eAgInPc2d1	daný
kovových	kovový	k2eAgInPc2d1	kovový
kationtů	kation	k1gInPc2	kation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
sopečných	sopečný	k2eAgInPc6d1	sopečný
plynech	plyn	k1gInPc6	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sopečný	sopečný	k2eAgInSc4d1	sopečný
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vyvrhován	vyvrhovat	k5eAaImNgInS	vyvrhovat
při	při	k7c6	při
sopečné	sopečný	k2eAgFnSc6d1	sopečná
erupci	erupce	k1gFnSc6	erupce
<g/>
;	;	kIx,	;
při	při	k7c6	při
silných	silný	k2eAgFnPc6d1	silná
erupcích	erupce	k1gFnPc6	erupce
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
v	v	k7c6	v
pyroklastickém	pyroklastický	k2eAgNnSc6d1	pyroklastický
mračnu	mračno	k1gNnSc6	mračno
až	až	k9	až
do	do	k7c2	do
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
drobných	drobný	k2eAgFnPc2d1	drobná
kapiček	kapička	k1gFnPc2	kapička
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
aerosolu	aerosol	k1gInSc6	aerosol
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
aerosol	aerosol	k1gInSc1	aerosol
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přetrvat	přetrvat	k5eAaPmF	přetrvat
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
roky	rok	k1gInPc4	rok
a	a	k8xC	a
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
velice	velice	k6eAd1	velice
efektivní	efektivní	k2eAgFnSc1d1	efektivní
zábrana	zábrana	k1gFnSc1	zábrana
před	před	k7c7	před
dopadajícím	dopadající	k2eAgNnSc7d1	dopadající
slunečním	sluneční	k2eAgNnSc7d1	sluneční
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
oteplovat	oteplovat	k5eAaImF	oteplovat
a	a	k8xC	a
v	v	k7c4	v
dalších	další	k2eAgInPc2d1	další
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
;	;	kIx,	;
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
částic	částice	k1gFnPc2	částice
aerosolu	aerosol	k1gInSc2	aerosol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
pro	pro	k7c4	pro
analýzu	analýza	k1gFnSc4	analýza
iontů	ion	k1gInPc2	ion
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
hutnictví	hutnictví	k1gNnSc6	hutnictví
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
kovových	kovový	k2eAgInPc2d1	kovový
sulfidů	sulfid	k1gInPc2	sulfid
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
uplatnění	uplatnění	k1gNnSc4	uplatnění
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
olejových	olejový	k2eAgInPc2d1	olejový
doplňků	doplněk	k1gInPc2	doplněk
a	a	k8xC	a
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
izotopů	izotop	k1gInPc2	izotop
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Toxicita	toxicita	k1gFnSc1	toxicita
===	===	k?	===
</s>
</p>
<p>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
je	být	k5eAaImIp3nS	být
prudce	prudko	k6eAd1	prudko
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
dávkách	dávka	k1gFnPc6	dávka
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
otravy	otrava	k1gFnSc2	otrava
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
smrti	smrt	k1gFnSc2	smrt
bez	bez	k7c2	bez
morfologických	morfologický	k2eAgFnPc2d1	morfologická
změn	změna	k1gFnPc2	změna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
účinky	účinek	k1gInPc1	účinek
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
jako	jako	k8xC	jako
u	u	k7c2	u
kyanovodíku	kyanovodík	k1gInSc2	kyanovodík
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
látky	látka	k1gFnPc1	látka
inhibují	inhibovat	k5eAaBmIp3nP	inhibovat
enzym	enzym	k1gInSc4	enzym
cytochrom	cytochrom	k1gInSc1	cytochrom
C	C	kA	C
oxidázu	oxidáza	k1gFnSc4	oxidáza
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
tak	tak	k8xS	tak
tkáním	tkáň	k1gFnPc3	tkáň
využívat	využívat	k5eAaPmF	využívat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
především	především	k9	především
v	v	k7c6	v
CNS	CNS	kA	CNS
paralýzou	paralýza	k1gFnSc7	paralýza
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
<g/>
Sulfan	Sulfan	k1gInSc1	Sulfan
má	mít	k5eAaImIp3nS	mít
dráždivý	dráždivý	k2eAgInSc1d1	dráždivý
i	i	k8xC	i
dusivý	dusivý	k2eAgInSc1d1	dusivý
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Dráždí	dráždit	k5eAaImIp3nP	dráždit
dýchací	dýchací	k2eAgNnSc4d1	dýchací
ústrojí	ústrojí	k1gNnSc4	ústrojí
a	a	k8xC	a
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
podráždění	podráždění	k1gNnSc1	podráždění
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
při	při	k7c6	při
dlouhodobější	dlouhodobý	k2eAgFnSc6d2	dlouhodobější
expozici	expozice	k1gFnSc6	expozice
již	již	k9	již
u	u	k7c2	u
koncentrací	koncentrace	k1gFnPc2	koncentrace
10,5	[number]	k4	10,5
<g/>
-	-	kIx~	-
<g/>
21,0	[number]	k4	21,0
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
očích	oko	k1gNnPc6	oko
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
keratokonjunktivitidu	keratokonjunktivitida	k1gFnSc4	keratokonjunktivitida
<g/>
,	,	kIx,	,
podráždění	podráždění	k1gNnSc4	podráždění
dýchacího	dýchací	k2eAgInSc2d1	dýchací
traktu	trakt	k1gInSc2	trakt
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
edému	edém	k1gInSc3	edém
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
koncentracích	koncentrace	k1gFnPc6	koncentrace
1	[number]	k4	1
000-2	[number]	k4	000-2
000	[number]	k4	000
ppm	ppm	k?	ppm
se	se	k3xPyFc4	se
sulfan	sulfan	k1gInSc1	sulfan
rychle	rychle	k6eAd1	rychle
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nejprve	nejprve	k6eAd1	nejprve
zrychlené	zrychlený	k2eAgNnSc1d1	zrychlené
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
vystřídáno	vystřídat	k5eAaPmNgNnS	vystřídat
zástavou	zástava	k1gFnSc7	zástava
dechu	dech	k1gInSc2	dech
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
koncentrace	koncentrace	k1gFnPc1	koncentrace
okamžitě	okamžitě	k6eAd1	okamžitě
paralyzují	paralyzovat	k5eAaBmIp3nP	paralyzovat
dýchací	dýchací	k2eAgNnSc4d1	dýchací
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
resuscitace	resuscitace	k1gFnSc2	resuscitace
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
spontánní	spontánní	k2eAgFnSc2d1	spontánní
obnovy	obnova	k1gFnSc2	obnova
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
udušením	udušení	k1gNnSc7	udušení
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
koncentracích	koncentrace	k1gFnPc6	koncentrace
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
ppm	ppm	k?	ppm
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
edém	edém	k1gInSc1	edém
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Čichem	čich	k1gInSc7	čich
jsou	být	k5eAaImIp3nP	být
rozpoznatelné	rozpoznatelný	k2eAgInPc1d1	rozpoznatelný
již	již	k6eAd1	již
koncentrace	koncentrace	k1gFnPc1	koncentrace
0,0005	[number]	k4	0,0005
<g/>
-	-	kIx~	-
<g/>
0,13	[number]	k4	0,13
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
individuální	individuální	k2eAgFnSc2d1	individuální
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
koncentrace	koncentrace	k1gFnPc1	koncentrace
rychle	rychle	k6eAd1	rychle
paralyzují	paralyzovat	k5eAaBmIp3nP	paralyzovat
čichové	čichový	k2eAgFnPc1d1	čichová
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zápach	zápach	k1gInSc1	zápach
plynu	plyn	k1gInSc2	plyn
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svoji	svůj	k3xOyFgFnSc4	svůj
varovnou	varovný	k2eAgFnSc4d1	varovná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Signalizační	signalizační	k2eAgFnSc1d1	signalizační
molekula	molekula	k1gFnSc1	molekula
===	===	k?	===
</s>
</p>
<p>
<s>
Sirovodík	sirovodík	k1gInSc1	sirovodík
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
dusnatým	dusnatý	k2eAgInSc7d1	dusnatý
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
k	k	k7c3	k
gasotransmitterům	gasotransmitter	k1gMnPc3	gasotransmitter
<g/>
.	.	kIx.	.
</s>
<s>
Sirovodík	sirovodík	k1gInSc1	sirovodík
proto	proto	k8xC	proto
působí	působit	k5eAaImIp3nS	působit
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
oxid	oxid	k1gInSc1	oxid
dusnatý	dusnatý	k2eAgInSc1d1	dusnatý
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
relaxant	relaxant	k1gInSc1	relaxant
na	na	k7c4	na
hladkosvalové	hladkosvalové	k2eAgFnPc4d1	hladkosvalové
buňky	buňka	k1gFnPc4	buňka
ve	v	k7c6	v
stěnách	stěna	k1gFnPc6	stěna
cév	céva	k1gFnPc2	céva
(	(	kIx(	(
<g/>
vasorelaxační	vasorelaxační	k2eAgInSc1d1	vasorelaxační
účinek	účinek	k1gInSc1	účinek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
patrný	patrný	k2eAgInSc1d1	patrný
byl	být	k5eAaImAgInS	být
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
uvolnění	uvolnění	k1gNnSc4	uvolnění
napětí	napětí	k1gNnSc2	napětí
hladké	hladký	k2eAgFnSc2d1	hladká
svaloviny	svalovina	k1gFnSc2	svalovina
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
(	(	kIx(	(
<g/>
myorelaxační	myorelaxační	k2eAgInSc1d1	myorelaxační
účinek	účinek	k1gInSc1	účinek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sirovodík	sirovodík	k1gInSc1	sirovodík
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
prokrvení	prokrvení	k1gNnSc1	prokrvení
penisu	penis	k1gInSc2	penis
<g/>
,	,	kIx,	,
přežití	přežití	k1gNnSc4	přežití
při	při	k7c6	při
infarktu	infarkt	k1gInSc6	infarkt
a	a	k8xC	a
ztrátě	ztráta	k1gFnSc3	ztráta
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
léčit	léčit	k5eAaImF	léčit
poruchy	porucha	k1gFnSc2	porucha
erekce	erekce	k1gFnSc2	erekce
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
typy	typ	k1gInPc4	typ
migrén	migréna	k1gFnPc2	migréna
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
srdečně	srdečně	k6eAd1	srdečně
cévní	cévní	k2eAgFnPc4d1	cévní
obtíže	obtíž	k1gFnPc4	obtíž
nebo	nebo	k8xC	nebo
pomáhat	pomáhat	k5eAaImF	pomáhat
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
prevenci	prevence	k1gFnSc6	prevence
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
pro	pro	k7c4	pro
humánní	humánní	k2eAgNnSc4d1	humánní
využití	využití	k1gNnSc4	využití
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
toxicita	toxicita	k1gFnSc1	toxicita
plynu	plyn	k1gInSc2	plyn
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgNnPc2	některý
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
snáší	snášet	k5eAaImIp3nS	snášet
relativně	relativně	k6eAd1	relativně
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sulfan	Sulfana	k1gFnPc2	Sulfana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
