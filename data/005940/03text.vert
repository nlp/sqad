<s>
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
působící	působící	k2eAgMnSc1d1	působící
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
kouzelník	kouzelník	k1gMnSc1	kouzelník
Žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgMnSc1d1	amatérský
bojovník	bojovník	k1gMnSc1	bojovník
muay	muaa	k1gFnSc2	muaa
thai	tha	k1gFnSc2	tha
<g/>
,	,	kIx,	,
příležitostný	příležitostný	k2eAgMnSc1d1	příležitostný
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
měl	mít	k5eAaImAgMnS	mít
až	až	k9	až
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
spíše	spíše	k9	spíše
špatný	špatný	k2eAgInSc4d1	špatný
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
pražskou	pražský	k2eAgFnSc4d1	Pražská
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
v	v	k7c6	v
hudebně-dramatickém	hudebněramatický	k2eAgInSc6d1	hudebně-dramatický
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
studentkou	studentka	k1gFnSc7	studentka
režie	režie	k1gFnSc2	režie
na	na	k7c6	na
FAMU	FAMU	kA	FAMU
Mirjam	Mirjam	k1gFnSc1	Mirjam
Müller	Müller	k1gMnSc1	Müller
<g/>
,	,	kIx,	,
rodilou	rodilý	k2eAgFnSc7d1	rodilá
Němkou	Němka	k1gFnSc7	Němka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
–	–	k?	–
starší	starý	k2eAgFnSc3d2	starší
Anastázii	Anastázie	k1gFnSc3	Anastázie
(	(	kIx(	(
<g/>
*	*	kIx~	*
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvojčata	dvojče	k1gNnPc1	dvojče
Rozálii	Rozálie	k1gFnSc4	Rozálie
a	a	k8xC	a
Roxanu	Roxana	k1gFnSc4	Roxana
(	(	kIx(	(
<g/>
*	*	kIx~	*
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Landa	Landa	k1gMnSc1	Landa
hrál	hrát	k5eAaImAgMnS	hrát
například	například	k6eAd1	například
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Proč	proč	k6eAd1	proč
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Černých	Černých	k2eAgMnPc6d1	Černých
baronech	baron	k1gMnPc6	baron
<g/>
,	,	kIx,	,
Copak	copak	k6eAd1	copak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
vojáka	voják	k1gMnSc4	voják
<g/>
...	...	k?	...
a	a	k8xC	a
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
seriálu	seriál	k1gInSc6	seriál
Alles	Alles	k1gMnSc1	Alles
ausser	ausser	k1gMnSc1	ausser
Mord	mord	k1gInSc4	mord
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
také	také	k9	také
ve	v	k7c6	v
filmech	film	k1gInPc6	film
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Mirjam	Mirjam	k1gFnSc4	Mirjam
Landové	Landové	k2eAgInSc2d1	Landové
Kvaska	Kvask	k1gInSc2	Kvask
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tacho	Tac	k1gMnSc2	Tac
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
Emanuela	Emanuel	k1gMnSc4	Emanuel
Moravce	Moravec	k1gMnSc4	Moravec
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
České	český	k2eAgNnSc1d1	české
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
epizoda	epizoda	k1gFnSc1	epizoda
Den	den	k1gInSc1	den
po	po	k7c6	po
Mnichovu	Mnichov	k1gInSc6	Mnichov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
hrál	hrát	k5eAaImAgInS	hrát
například	například	k6eAd1	například
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
O	o	k7c6	o
myších	myš	k1gFnPc6	myš
a	a	k8xC	a
lidech	lid	k1gInPc6	lid
společně	společně	k6eAd1	společně
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Matáskem	Matásek	k1gMnSc7	Matásek
<g/>
,	,	kIx,	,
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Dracula	Draculum	k1gNnSc2	Draculum
<g/>
,	,	kIx,	,
Krysař	krysař	k1gMnSc1	krysař
a	a	k8xC	a
odehrál	odehrát	k5eAaPmAgMnS	odehrát
několik	několik	k4yIc4	několik
představení	představení	k1gNnPc2	představení
v	v	k7c6	v
muzikálovém	muzikálový	k2eAgNnSc6d1	muzikálové
dramatu	drama	k1gNnSc6	drama
Tajemství	tajemství	k1gNnSc1	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Tajemství	tajemství	k1gNnSc4	tajemství
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
draka	drak	k1gMnSc2	drak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
a	a	k8xC	a
derniéru	derniéra	k1gFnSc4	derniéra
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
Mahenově	Mahenův	k2eAgNnSc6d1	Mahenovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
postavu	postava	k1gFnSc4	postava
Merkuria	Merkurium	k1gNnSc2	Merkurium
(	(	kIx(	(
<g/>
v	v	k7c6	v
alternaci	alternace	k1gFnSc6	alternace
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Traplem	Trapl	k1gMnSc7	Trapl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Orlík	Orlík	k1gInSc1	Orlík
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
založil	založit	k5eAaPmAgInS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Davidem	David	k1gMnSc7	David
Matáskem	Matásek	k1gMnSc7	Matásek
kapelu	kapela	k1gFnSc4	kapela
Orlík	Orlík	k1gInSc1	Orlík
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
název	název	k1gInSc4	název
převzali	převzít	k5eAaPmAgMnP	převzít
od	od	k7c2	od
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
vinárny	vinárna	k1gFnSc2	vinárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
první	první	k4xOgFnSc4	první
studiovou	studiový	k2eAgFnSc4d1	studiová
nahrávku	nahrávka	k1gFnSc4	nahrávka
Oi	Oi	k1gFnSc2	Oi
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
velkou	velká	k1gFnSc4	velká
oblibu	obliba	k1gFnSc4	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
byla	být	k5eAaImAgFnS	být
i	i	k9	i
mezi	mezi	k7c7	mezi
příslušníky	příslušník	k1gMnPc7	příslušník
hnutí	hnutí	k1gNnSc2	hnutí
skinheads	skinheads	k1gInSc1	skinheads
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
některých	některý	k3yIgFnPc2	některý
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
liga	liga	k1gFnSc1	liga
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Bílej	Bílej	k?	Bílej
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
nesouhlasné	souhlasný	k2eNgInPc4d1	nesouhlasný
postoje	postoj	k1gInPc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
a	a	k8xC	a
posledním	poslední	k2eAgNnSc7d1	poslední
albem	album	k1gNnSc7	album
byla	být	k5eAaImAgFnS	být
Demise	demise	k1gFnSc1	demise
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
rozešla	rozejít	k5eAaPmAgFnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
sólovou	sólový	k2eAgFnSc4d1	sólová
kariéru	kariéra	k1gFnSc4	kariéra
začal	začít	k5eAaPmAgInS	začít
deskou	deska	k1gFnSc7	deska
Valčík	valčík	k1gInSc1	valčík
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovanou	následovaný	k2eAgFnSc4d1	následovaná
alby	alba	k1gFnPc1	alba
Chcíply	chcípnout	k5eAaPmAgFnP	chcípnout
dobrý	dobrý	k2eAgMnSc1d1	dobrý
víly	víla	k1gFnPc4	víla
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pozdrav	pozdrav	k1gInSc1	pozdrav
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Konec	konec	k1gInSc1	konec
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
výběrové	výběrový	k2eAgNnSc1d1	výběrové
album	album	k1gNnSc1	album
Best	Best	k1gMnSc1	Best
of	of	k?	of
Landa	Landa	k1gMnSc1	Landa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
Landa	Landa	k1gMnSc1	Landa
nahrál	nahrát	k5eAaBmAgMnS	nahrát
9	[number]	k4	9
<g/>
mm	mm	kA	mm
argumentů	argument	k1gInPc2	argument
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
album	album	k1gNnSc1	album
Neofolk	Neofolk	k1gInSc1	Neofolk
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
výběrová	výběrový	k2eAgFnSc1d1	výběrová
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
taktéž	taktéž	k?	taktéž
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
činností	činnost	k1gFnPc2	činnost
řádu	řád	k1gInSc2	řád
Ordo	orda	k1gFnSc5	orda
Lumen	lumen	k1gInSc4	lumen
Templi	Templi	k1gFnPc1	Templi
ve	v	k7c6	v
vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
chrámu	chrám	k1gInSc6	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Večer	večer	k6eAd1	večer
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
tvořeného	tvořený	k2eAgInSc2d1	tvořený
cover	cover	k1gInSc1	cover
verzemi	verze	k1gFnPc7	verze
písní	píseň	k1gFnPc2	píseň
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
vyšel	vyjít	k5eAaPmAgInS	vyjít
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
satiricky	satiricky	k6eAd1	satiricky
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
také	také	k9	také
severočeský	severočeský	k2eAgMnSc1d1	severočeský
umělec	umělec	k1gMnSc1	umělec
Xavier	Xavier	k1gMnSc1	Xavier
Baumaxa	Baumaxa	k1gMnSc1	Baumaxa
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
skladbě	skladba	k1gFnSc6	skladba
Nazijazz	Nazijazz	k1gMnSc1	Nazijazz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
činností	činnost	k1gFnSc7	činnost
řádu	řád	k1gInSc2	řád
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Landa	Landa	k1gMnSc1	Landa
i	i	k9	i
na	na	k7c4	na
a	a	k8xC	a
charitativním	charitativní	k2eAgInSc6d1	charitativní
Večeru	večer	k1gInSc6	večer
sjednocení	sjednocení	k1gNnSc1	sjednocení
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Markéty	Markéta	k1gFnSc2	Markéta
na	na	k7c6	na
Břevnově	Břevnov	k1gInSc6	Břevnov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Landa	Landa	k1gMnSc1	Landa
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
turné	turné	k1gNnSc4	turné
Vltava	Vltava	k1gFnSc1	Vltava
Tour	Tour	k1gInSc4	Tour
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
turné	turné	k1gNnSc4	turné
Bouře	bouř	k1gFnSc2	bouř
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ostravice	Ostravice	k1gFnSc1	Ostravice
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Kalich	kalich	k1gInSc1	kalich
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
koncertů	koncert	k1gInPc2	koncert
Vltava	Vltava	k1gFnSc1	Vltava
Tour	Tour	k1gInSc1	Tour
a	a	k8xC	a
Bouře	bouře	k1gFnSc1	bouře
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
série	série	k1gFnSc1	série
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
Československo	Československo	k1gNnSc4	Československo
Tour	Tour	k1gInSc1	Tour
2008	[number]	k4	2008
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
90	[number]	k4	90
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Landa	Landa	k1gMnSc1	Landa
vydal	vydat	k5eAaPmAgMnS	vydat
album	album	k1gNnSc4	album
Nigredo	Nigredo	k1gNnSc4	Nigredo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
éru	éra	k1gFnSc4	éra
turné	turné	k1gNnSc2	turné
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
Vozová	vozový	k2eAgFnSc1d1	vozová
hradba	hradba	k1gFnSc1	hradba
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dilema	dilema	k1gNnSc1	dilema
pyrotechnikovo	pyrotechnikův	k2eAgNnSc1d1	pyrotechnikův
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letos	letos	k6eAd1	letos
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
turné	turné	k1gNnSc1	turné
Žito	žit	k2eAgNnSc1d1	žito
tour	tour	k1gInSc4	tour
2014	[number]	k4	2014
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
městech	město	k1gNnPc6	město
po	po	k7c6	po
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
plánuje	plánovat	k5eAaImIp3nS	plánovat
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
jediný	jediný	k2eAgInSc4d1	jediný
Velekoncert	Velekoncert	k1gInSc4	Velekoncert
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
letišti	letiště	k1gNnSc6	letiště
Letňany	Letňan	k1gMnPc4	Letňan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
plánuje	plánovat	k5eAaImIp3nS	plánovat
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
několika	několik	k4yIc3	několik
filmům	film	k1gInPc3	film
a	a	k8xC	a
divadelním	divadelní	k2eAgNnSc7d1	divadelní
představením	představení	k1gNnSc7	představení
–	–	k?	–
Udělení	udělení	k1gNnSc4	udělení
milosti	milost	k1gFnSc2	milost
se	se	k3xPyFc4	se
zamítá	zamítat	k5eAaImIp3nS	zamítat
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gambit	gambit	k1gInSc1	gambit
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ex	ex	k6eAd1	ex
Offo	Offo	k6eAd1	Offo
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
k	k	k7c3	k
snímku	snímek	k1gInSc3	snímek
Kvaska	Kvask	k1gInSc2	Kvask
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
autorem	autor	k1gMnSc7	autor
muzikálů	muzikál	k1gInPc2	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
dirty	dirta	k1gFnSc2	dirta
muzikál	muzikál	k1gInSc1	muzikál
Krysař	krysař	k1gMnSc1	krysař
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Ta	ten	k3xDgFnSc1	ten
Fantastika	fantastika	k1gFnSc1	fantastika
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
i	i	k9	i
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
nastudování	nastudování	k1gNnSc3	nastudování
divadla	divadlo	k1gNnSc2	divadlo
Kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
počinem	počin	k1gInSc7	počin
je	být	k5eAaImIp3nS	být
muzikálové	muzikálový	k2eAgNnSc1d1	muzikálové
drama	drama	k1gNnSc1	drama
Tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Kalich	kalich	k1gInSc1	kalich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dopsal	dopsat	k5eAaPmAgInS	dopsat
s	s	k7c7	s
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Soukupem	Soukup	k1gMnSc7	Soukup
další	další	k2eAgFnSc4d1	další
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
muzikál	muzikál	k1gInSc4	muzikál
Touha	touha	k1gFnSc1	touha
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
a	a	k8xC	a
režie	režie	k1gFnSc2	režie
Mirjam	Mirjam	k1gFnSc2	Mirjam
Landy	Landa	k1gMnSc2	Landa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
filmu	film	k1gInSc2	film
Kvaska	Kvask	k1gInSc2	Kvask
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sepsal	sepsat	k5eAaPmAgMnS	sepsat
pro	pro	k7c4	pro
brněnské	brněnský	k2eAgNnSc4d1	brněnské
Mahenovo	Mahenův	k2eAgNnSc4d1	Mahenovo
divadlo	divadlo	k1gNnSc4	divadlo
hudbu	hudba	k1gFnSc4	hudba
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Tajemství	tajemství	k1gNnSc2	tajemství
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
derniéra	derniéra	k1gFnSc1	derniéra
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
Mozartovo	Mozartův	k2eAgNnSc4d1	Mozartovo
Requiem	Requium	k1gNnSc7	Requium
do	do	k7c2	do
rockové	rockový	k2eAgFnSc2d1	rocková
podoby	podoba	k1gFnSc2	podoba
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Rockquiem	Rockquium	k1gNnSc7	Rockquium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Obecním	obecní	k2eAgInSc6d1	obecní
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
jezdil	jezdit	k5eAaImAgInS	jezdit
autokros	autokros	k1gInSc1	autokros
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
až	až	k9	až
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
závodění	závodění	k1gNnSc3	závodění
trucků	truck	k1gInPc2	truck
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nováčkem	nováček	k1gInSc7	nováček
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jezdí	jezdit	k5eAaImIp3nS	jezdit
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
rallye	rallye	k1gFnSc4	rallye
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
N	N	kA	N
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
automobilismem	automobilismus	k1gInSc7	automobilismus
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
Nadace	nadace	k1gFnSc1	nadace
Malina	malina	k1gFnSc1	malina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
s	s	k7c7	s
Romanem	Roman	k1gMnSc7	Roman
Krestou	Kresta	k1gMnSc7	Kresta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nadace	nadace	k1gFnSc1	nadace
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
osvětou	osvěta	k1gFnSc7	osvěta
a	a	k8xC	a
prevencí	prevence	k1gFnSc7	prevence
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Vydali	vydat	k5eAaPmAgMnP	vydat
i	i	k9	i
několik	několik	k4yIc4	několik
výukových	výukový	k2eAgInPc2d1	výukový
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Bourá	bourat	k5eAaImIp3nS	bourat
jen	jen	k9	jen
blb	blb	k1gMnSc1	blb
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Auto	auto	k1gNnSc1	auto
je	být	k5eAaImIp3nS	být
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
Tacho	Tac	k1gMnSc4	Tac
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Ordo	orda	k1gFnSc5	orda
Lumen	lumen	k1gInSc4	lumen
Templi	Templi	k1gFnSc1	Templi
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
neveřejným	veřejný	k2eNgInSc7d1	neveřejný
koncertem	koncert	k1gInSc7	koncert
Večer	večer	k1gInSc1	večer
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
tematicky	tematicky	k6eAd1	tematicky
navázal	navázat	k5eAaPmAgInS	navázat
Večer	večer	k1gInSc1	večer
sjednocení	sjednocení	k1gNnSc2	sjednocení
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Markéty	Markéta	k1gFnSc2	Markéta
na	na	k7c6	na
Břevnově	Břevnov	k1gInSc6	Břevnov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
z	z	k7c2	z
dětských	dětský	k2eAgInPc2d1	dětský
domovů	domov	k1gInPc2	domov
Pyšely	Pyšely	k1gInPc4	Pyšely
<g/>
,	,	kIx,	,
Racek	racek	k1gMnSc1	racek
a	a	k8xC	a
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výtěžku	výtěžek	k1gInSc2	výtěžek
koncertu	koncert	k1gInSc2	koncert
finančně	finančně	k6eAd1	finančně
přispěl	přispět	k5eAaPmAgMnS	přispět
jménem	jméno	k1gNnSc7	jméno
řádu	řád	k1gInSc2	řád
OLT	OLT	kA	OLT
na	na	k7c4	na
dětské	dětský	k2eAgNnSc4d1	dětské
hřiště	hřiště	k1gNnSc4	hřiště
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
starých	starý	k2eAgFnPc2d1	stará
českých	český	k2eAgFnPc2d1	Česká
pověstí	pověst	k1gFnPc2	pověst
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Landa	Landa	k1gMnSc1	Landa
ale	ale	k8xC	ale
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
řád	řád	k1gInSc1	řád
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
bývalých	bývalý	k2eAgMnPc2d1	bývalý
členů	člen	k1gMnPc2	člen
se	se	k3xPyFc4	se
Ordo	orda	k1gFnSc5	orda
Lumen	lumen	k1gNnSc4	lumen
Templi	Templi	k1gFnSc3	Templi
de	de	k?	de
facto	facto	k1gNnSc1	facto
rozpadl	rozpadnout	k5eAaPmAgMnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Činností	činnost	k1gFnPc2	činnost
sdružení	sdružení	k1gNnSc2	sdružení
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
také	také	k9	také
odborná	odborný	k2eAgFnSc1d1	odborná
religionistická	religionistický	k2eAgFnSc1d1	religionistická
veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
představení	představení	k1gNnSc4	představení
hudebního	hudební	k2eAgNnSc2d1	hudební
dramatu	drama	k1gNnSc2	drama
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vedle	vedle	k7c2	vedle
herců	herc	k1gInPc2	herc
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
osobně	osobně	k6eAd1	osobně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
nejznámější	známý	k2eAgFnPc1d3	nejznámější
písně	píseň	k1gFnPc1	píseň
tvoří	tvořit	k5eAaImIp3nP	tvořit
osu	osa	k1gFnSc4	osa
celého	celý	k2eAgNnSc2d1	celé
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
zde	zde	k6eAd1	zde
v	v	k7c6	v
roli	role	k1gFnSc6	role
Merkuria	Merkurium	k1gNnSc2	Merkurium
–	–	k?	–
starověkého	starověký	k2eAgInSc2d1	starověký
symbolu	symbol	k1gInSc2	symbol
sjednocení	sjednocení	k1gNnSc2	sjednocení
duchovních	duchovní	k2eAgFnPc2d1	duchovní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
výstižně	výstižně	k6eAd1	výstižně
významný	významný	k2eAgMnSc1d1	významný
psycholog	psycholog	k1gMnSc1	psycholog
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
písní	píseň	k1gFnPc2	píseň
jsou	být	k5eAaImIp3nP	být
diváci	divák	k1gMnPc1	divák
"	"	kIx"	"
<g/>
Tajemství	tajemství	k1gNnSc1	tajemství
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
Draka	drak	k1gMnSc2	drak
<g/>
"	"	kIx"	"
vtaženi	vtáhnout	k5eAaPmNgMnP	vtáhnout
do	do	k7c2	do
detektivního	detektivní	k2eAgInSc2d1	detektivní
příběhu	příběh	k1gInSc2	příběh
protkaného	protkaný	k2eAgInSc2d1	protkaný
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
tajemství	tajemství	k1gNnSc4	tajemství
starověkých	starověký	k2eAgFnPc2d1	starověká
i	i	k8xC	i
novověkých	novověký	k2eAgFnPc2d1	novověká
mystických	mystický	k2eAgFnPc2d1	mystická
legend	legenda	k1gFnPc2	legenda
a	a	k8xC	a
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
inscenaci	inscenace	k1gFnSc6	inscenace
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
i	i	k9	i
autorská	autorský	k2eAgFnSc1d1	autorská
dvojice	dvojice	k1gFnSc1	dvojice
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Plachý	Plachý	k1gMnSc1	Plachý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
oznámil	oznámit	k5eAaPmAgMnS	oznámit
ukončení	ukončení	k1gNnSc4	ukončení
své	svůj	k3xOyFgFnSc2	svůj
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Přestal	přestat	k5eAaPmAgMnS	přestat
používat	používat	k5eAaImF	používat
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
říkat	říkat	k5eAaImF	říkat
kouzelník	kouzelník	k1gMnSc1	kouzelník
Žito	žito	k1gNnSc1	žito
44	[number]	k4	44
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
kontroverzi	kontroverze	k1gFnSc4	kontroverze
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
jeho	jeho	k3xOp3gInSc1	jeho
projev	projev	k1gInSc1	projev
na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
reakcí	reakce	k1gFnPc2	reakce
zde	zde	k6eAd1	zde
nepokrytě	pokrytě	k6eNd1	pokrytě
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
dalších	další	k2eAgFnPc2d1	další
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
projev	projev	k1gInSc4	projev
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prožívá	prožívat	k5eAaImIp3nS	prožívat
silnou	silný	k2eAgFnSc4d1	silná
krizi	krize	k1gFnSc4	krize
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
kouzelník	kouzelník	k1gMnSc1	kouzelník
Žito	žito	k1gNnSc4	žito
si	se	k3xPyFc3	se
stanovil	stanovit	k5eAaPmAgInS	stanovit
sedm	sedm	k4xCc4	sedm
"	"	kIx"	"
<g/>
kejklí	kejkle	k1gFnPc2	kejkle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
chce	chtít	k5eAaImIp3nS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
:	:	kIx,	:
plavat	plavat	k5eAaImF	plavat
freedive	freediev	k1gFnPc4	freediev
s	s	k7c7	s
tygřími	tygří	k2eAgMnPc7d1	tygří
žraloky	žralok	k1gMnPc7	žralok
v	v	k7c6	v
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rallye	rallye	k1gNnSc6	rallye
se	se	k3xPyFc4	se
umístit	umístit	k5eAaPmF	umístit
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
podplavat	podplavat	k5eAaPmF	podplavat
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
Blue	Blue	k1gFnSc4	Blue
Hole	hole	k6eAd1	hole
dokončit	dokončit	k5eAaPmF	dokončit
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
thaiboxu	thaibox	k1gInSc6	thaibox
proti	proti	k7c3	proti
profesionálnímu	profesionální	k2eAgMnSc3d1	profesionální
boxerovi	boxer	k1gMnSc3	boxer
dostat	dostat	k5eAaPmF	dostat
<g />
.	.	kIx.	.
</s>
<s>
Sigmu	Sigmat	k5eAaPmIp1nS	Sigmat
Olomouc	Olomouc	k1gFnSc1	Olomouc
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
uspořádat	uspořádat	k5eAaPmF	uspořádat
koncert	koncert	k1gInSc4	koncert
pro	pro	k7c4	pro
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
položit	položit	k5eAaPmF	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
chrámu	chrámat	k5eAaImIp1nS	chrámat
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
draka	drak	k1gMnSc4	drak
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
přinesla	přinést	k5eAaPmAgFnS	přinést
média	médium	k1gNnSc2	médium
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Landa	Landa	k1gMnSc1	Landa
napadl	napadnout	k5eAaPmAgMnS	napadnout
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
vraceli	vracet	k5eAaImAgMnP	vracet
v	v	k7c4	v
půl	půl	k1xP	půl
druhé	druhý	k4xOgFnPc4	druhý
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
oslavy	oslava	k1gFnSc2	oslava
narozenin	narozeniny	k1gFnPc2	narozeniny
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
poblíž	poblíž	k6eAd1	poblíž
jeho	on	k3xPp3gInSc2	on
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Landy	Landa	k1gMnSc2	Landa
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
preventivní	preventivní	k2eAgFnSc4d1	preventivní
obranu	obrana	k1gFnSc4	obrana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ho	on	k3xPp3gMnSc4	on
skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
vulgárně	vulgárně	k6eAd1	vulgárně
urážet	urážet	k5eAaImF	urážet
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
podnapilém	podnapilý	k2eAgInSc6d1	podnapilý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
a	a	k8xC	a
Landa	Landa	k1gMnSc1	Landa
nebyl	být	k5eNaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Frýba	Frýba	k1gMnSc1	Frýba
for	forum	k1gNnPc2	forum
president	president	k1gMnSc1	president
(	(	kIx(	(
<g/>
OI	OI	kA	OI
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Demise	demise	k1gFnSc2	demise
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Valčík	valčík	k1gInSc1	valčík
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Chcíply	chcípnout	k5eAaPmAgFnP	chcípnout
dobrý	dobrý	k2eAgMnSc1d1	dobrý
víly	víla	k1gFnPc4	víla
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Krysař	krysař	k1gMnSc1	krysař
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
muzikálu	muzikál	k1gInSc2	muzikál
<g/>
)	)	kIx)	)
Pozdrav	pozdrav	k1gInSc1	pozdrav
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Konec	konec	k1gInSc1	konec
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Best	Best	k1gMnSc1	Best
of	of	k?	of
Landa	Landa	k1gMnSc1	Landa
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
mm	mm	kA	mm
argumentů	argument	k1gInPc2	argument
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Krysař	krysař	k1gMnSc1	krysař
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
dirty	dirta	k1gFnPc1	dirta
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
Landa	Landa	k1gMnSc1	Landa
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
písních	píseň	k1gFnPc6	píseň
<g/>
)	)	kIx)	)
Vltava	Vltava	k1gFnSc1	Vltava
tour	tour	k1gMnSc1	tour
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Best	Best	k1gMnSc1	Best
of	of	k?	of
Landa	Landa	k1gMnSc1	Landa
2	[number]	k4	2
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Neofolk	Neofolka	k1gFnPc2	Neofolka
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Večer	večer	k6eAd1	večer
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Komplet	komplet	k1gInSc1	komplet
<g/>
/	/	kIx~	/
<g/>
Best	Best	k1gInSc1	Best
of	of	k?	of
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Tajemství	tajemství	k1gNnSc1	tajemství
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
<g />
.	.	kIx.	.
</s>
<s>
muzikálu	muzikál	k1gInSc2	muzikál
na	na	k7c4	na
CD	CD	kA	CD
<g/>
)	)	kIx)	)
Bouře	bouře	k1gFnSc1	bouře
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
koncert	koncert	k1gInSc4	koncert
na	na	k7c6	na
DVD	DVD	kA	DVD
nebo	nebo	k8xC	nebo
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
CD	CD	kA	CD
<g/>
)	)	kIx)	)
Kvaska	Kvasek	k1gMnSc2	Kvasek
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
filmu	film	k1gInSc3	film
<g/>
)	)	kIx)	)
Tajemství	tajemství	k1gNnSc1	tajemství
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc1	záznam
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
muzikálu	muzikál	k1gInSc2	muzikál
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
Touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
soundtrack	soundtrack	k1gInSc1	soundtrack
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
muzikálu	muzikál	k1gInSc3	muzikál
<g/>
)	)	kIx)	)
Nigredo	Nigredo	k1gNnSc1	Nigredo
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Platinum	Platinum	k1gInSc1	Platinum
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
3	[number]	k4	3
CD	CD	kA	CD
Tacho	Tac	k1gMnSc2	Tac
(	(	kIx(	(
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
filmu	film	k1gInSc3	film
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Československo	Československo	k1gNnSc4	Československo
Tour	Toura	k1gFnPc2	Toura
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
záznam	záznam	k1gInSc1	záznam
na	na	k7c4	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Vozová	vozový	k2eAgFnSc1d1	vozová
<g />
.	.	kIx.	.
</s>
<s>
hradba	hradba	k1gFnSc1	hradba
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
tour	tour	k1gMnSc1	tour
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
záznam	záznam	k1gInSc1	záznam
na	na	k7c4	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
<g/>
:	:	kIx,	:
Best	Best	k1gInSc1	Best
of	of	k?	of
3	[number]	k4	3
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Klíč	klíč	k1gInSc1	klíč
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Žito	žito	k1gNnSc4	žito
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Švédská	švédský	k2eAgFnSc1d1	švédská
trojka	trojka	k1gFnSc1	trojka
–	–	k?	–
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Landa	Landa	k1gMnSc1	Landa
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Večerníček	večerníček	k1gInSc1	večerníček
<g/>
)	)	kIx)	)
Dracula	Dracula	k1gFnSc1	Dracula
<g/>
/	/	kIx~	/
<g/>
výběr	výběr	k1gInSc1	výběr
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
původní	původní	k2eAgNnSc4d1	původní
obsazení	obsazení	k1gNnSc4	obsazení
muzikálu	muzikál	k1gInSc2	muzikál
–	–	k?	–
2	[number]	k4	2
písně	píseň	k1gFnPc1	píseň
<g/>
)	)	kIx)	)
Citová	citový	k2eAgFnSc1d1	citová
investice	investice	k1gFnSc1	investice
–	–	k?	–
Hapka	Hapka	k1gMnSc1	Hapka
&	&	k?	&
Horáček	Horáček	k1gMnSc1	Horáček
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Landa	Landa	k1gMnSc1	Landa
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
–	–	k?	–
2	[number]	k4	2
písně	píseň	k1gFnPc1	píseň
<g/>
)	)	kIx)	)
Medium	medium	k1gNnSc1	medium
–	–	k?	–
Kurtizány	kurtizána	k1gFnSc2	kurtizána
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
avenue	avenue	k1gFnSc6	avenue
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Landa	Landa	k1gMnSc1	Landa
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
–	–	k?	–
1	[number]	k4	1
píseň	píseň	k1gFnSc1	píseň
<g/>
)	)	kIx)	)
Krysař	krysař	k1gMnSc1	krysař
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
soundtrack	soundtrack	k1gInSc1	soundtrack
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
filmu	film	k1gInSc2	film
–	–	k?	–
písně	píseň	k1gFnSc2	píseň
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
Daniela	Daniel	k1gMnSc2	Daniel
Landy	Landa	k1gMnSc2	Landa
<g/>
)	)	kIx)	)
Rockquiem	Rockquium	k1gNnSc7	Rockquium
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInSc1d1	speciální
projekt	projekt	k1gInSc1	projekt
hudebně	hudebně	k6eAd1	hudebně
scénické	scénický	k2eAgFnSc2d1	scénická
show	show	k1gFnSc2	show
<g/>
)	)	kIx)	)
Alimania	Alimanium	k1gNnSc2	Alimanium
–	–	k?	–
Jakub	Jakub	k1gMnSc1	Jakub
Mohamed	Mohamed	k1gMnSc1	Mohamed
Ali	Ali	k1gMnSc1	Ali
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
O.S.	O.S.	k?	O.S.
<g/>
G.	G.	kA	G.
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Landa	Landa	k1gMnSc1	Landa
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
spoluautor	spoluautor	k1gMnSc1	spoluautor
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
Strážce	strážce	k1gMnSc1	strážce
plamene	plamen	k1gInSc2	plamen
–	–	k?	–
Hapka	Hapka	k1gMnSc1	Hapka
&	&	k?	&
Horáček	Horáček	k1gMnSc1	Horáček
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Landa	Landa	k1gMnSc1	Landa
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
–	–	k?	–
2	[number]	k4	2
písně	píseň	k1gFnSc2	píseň
<g/>
)	)	kIx)	)
Buccaneer	Buccaneer	k1gMnSc1	Buccaneer
-	-	kIx~	-
Vláďa	Vláďa	k1gMnSc1	Vláďa
Šafránek	Šafránek	k1gMnSc1	Šafránek
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Landa	Landa	k1gMnSc1	Landa
napsal	napsat	k5eAaPmAgMnS	napsat
text	text	k1gInSc4	text
písně	píseň	k1gFnSc2	píseň
Pod	pod	k7c7	pod
pirátskou	pirátský	k2eAgFnSc7d1	pirátská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
)	)	kIx)	)
Třetí	třetí	k4xOgNnSc1	třetí
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
Případ	případ	k1gInSc1	případ
Kolman	Kolman	k1gMnSc1	Kolman
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
Copak	copak	k9	copak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
vojáka	voják	k1gMnSc4	voják
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
desátník	desátník	k1gMnSc1	desátník
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
Proč	proč	k6eAd1	proč
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
Kainovo	Kainův	k2eAgNnSc1d1	Kainovo
znamení	znamení	k1gNnSc1	znamení
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Albín	Albín	k1gMnSc1	Albín
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
Jen	jen	k6eAd1	jen
o	o	k7c6	o
rodinných	rodinný	k2eAgFnPc6d1	rodinná
záležitostech	záležitost	k1gFnPc6	záležitost
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
dozorce	dozorce	k1gMnSc1	dozorce
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
Tichá	Tichá	k1gFnSc1	Tichá
bolest	bolest	k1gFnSc1	bolest
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Vrábel	Vrábel	k1gMnSc1	Vrábel
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
Černí	černit	k5eAaImIp3nP	černit
baroni	baron	k1gMnPc1	baron
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
svobodník	svobodník	k1gMnSc1	svobodník
Halík	Halík	k1gMnSc1	Halík
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
Alles	Alles	k1gMnSc1	Alles
Außer	Außer	k1gMnSc1	Außer
Mord	morda	k1gFnPc2	morda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Tödlicher	Tödlichra	k1gFnPc2	Tödlichra
Irrtum	Irrtum	k1gNnSc4	Irrtum
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
Alles	Allesa	k1gFnPc2	Allesa
außer	außra	k1gFnPc2	außra
Mord	mord	k1gInSc1	mord
<g/>
:	:	kIx,	:
Hals	Hals	k1gMnSc1	Hals
über	über	k1gMnSc1	über
Kopf	Kopf	k1gMnSc1	Kopf
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
Vltava	Vltava	k1gFnSc1	Vltava
tour	toura	k1gFnPc2	toura
Večer	večer	k6eAd1	večer
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
2004	[number]	k4	2004
Bouře	bouře	k1gFnSc1	bouře
2006	[number]	k4	2006
Peklo	peklo	k1gNnSc4	peklo
s	s	k7c7	s
Landou	Landa	k1gMnSc7	Landa
2007	[number]	k4	2007
–	–	k?	–
internetová	internetový	k2eAgFnSc1d1	internetová
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
Kvaska	Kvask	k1gInSc2	Kvask
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Franta	Franta	k1gMnSc1	Franta
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
2007	[number]	k4	2007
Tajemství	tajemství	k1gNnSc1	tajemství
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Temný	temný	k2eAgMnSc1d1	temný
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
–	–	k?	–
záznam	záznam	k1gInSc1	záznam
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
muzikálu	muzikál	k1gInSc2	muzikál
Tacho	Tac	k1gMnSc2	Tac
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Alex	Alex	k1gMnSc1	Alex
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
České	český	k2eAgNnSc1d1	české
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
po	po	k7c6	po
Mnichovu	Mnichov	k1gInSc6	Mnichov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Emanuel	Emanuel	k1gMnSc1	Emanuel
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
Jó	jó	k0	jó
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
<g/>
!	!	kIx.	!
</s>
<s>
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
Valčík	valčík	k1gInSc1	valčík
<g/>
)	)	kIx)	)
Přízraky	přízrak	k1gInPc7	přízrak
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
Valčík	valčík	k1gInSc1	valčík
<g/>
)	)	kIx)	)
Motýlek	motýlek	k1gInSc1	motýlek
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
Chcíply	chcípnout	k5eAaPmAgFnP	chcípnout
dobrý	dobrý	k2eAgMnSc1d1	dobrý
víly	víla	k1gFnPc4	víla
<g/>
)	)	kIx)	)
Ztracení	ztracený	k2eAgMnPc1d1	ztracený
hoši	hoch	k1gMnPc1	hoch
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
Chcíply	chcípnout	k5eAaPmAgFnP	chcípnout
dobrý	dobrý	k2eAgMnSc1d1	dobrý
víly	víla	k1gFnPc4	víla
<g/>
)	)	kIx)	)
Včera	včera	k6eAd1	včera
mě	já	k3xPp1nSc2	já
někdo	někdo	k3yInSc1	někdo
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
Chcíply	chcípnout	k5eAaPmAgFnP	chcípnout
dobrý	dobrý	k2eAgMnSc1d1	dobrý
víly	víla	k1gFnPc4	víla
<g/>
)	)	kIx)	)
Pozdrav	pozdrav	k1gInSc1	pozdrav
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Pozdrav	pozdrav	k1gInSc1	pozdrav
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Pozdrav	pozdrav	k1gInSc1	pozdrav
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
<g/>
)	)	kIx)	)
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
<g/>
)	)	kIx)	)
Na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
ani	ani	k8xC	ani
mrak	mrak	k1gInSc1	mrak
ani	ani	k8xC	ani
loď	loď	k1gFnSc1	loď
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Konec	konec	k1gInSc1	konec
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
sta	sto	k4xCgNnPc4	sto
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
<g/>
)	)	kIx)	)
Vltava	Vltava	k1gFnSc1	Vltava
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
mm	mm	kA	mm
<g />
.	.	kIx.	.
</s>
<s>
argumentů	argument	k1gInPc2	argument
<g/>
)	)	kIx)	)
Tajemství	tajemství	k1gNnSc1	tajemství
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
mm	mm	kA	mm
argumentů	argument	k1gInPc2	argument
<g/>
)	)	kIx)	)
Quantum	Quantum	k1gNnSc1	Quantum
tarantulí	tarantule	k1gFnPc2	tarantule
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
mm	mm	kA	mm
argumentů	argument	k1gInPc2	argument
<g/>
)	)	kIx)	)
Morituri	Morituri	k1gNnSc1	Morituri
te	te	k?	te
salutant	salutant	k1gMnSc1	salutant
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
Vlatava	Vlatava	k1gFnSc1	Vlatava
tour	tour	k1gMnSc1	tour
<g/>
)	)	kIx)	)
Karavana	karavana	k1gFnSc1	karavana
mraků	mrak	k1gInPc2	mrak
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
Večer	večer	k6eAd1	večer
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Karla	Karel	k1gMnSc2	Karel
Kryla	Kryl	k1gMnSc2	Kryl
pro	pro	k7c4	pro
Český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
<g/>
)	)	kIx)	)
Protestsong	protestsong	k1gInSc1	protestsong
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
Neofolk	Neofolk	k1gInSc1	Neofolk
<g/>
)	)	kIx)	)
Volám	volat	k5eAaImIp1nS	volat
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
Tajemství	tajemství	k1gNnSc1	tajemství
<g/>
)	)	kIx)	)
Touha	touha	k1gFnSc1	touha
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
Kvaska	Kvask	k1gInSc2	Kvask
<g/>
)	)	kIx)	)
Díkůvzdání	díkůvzdání	k1gNnSc4	díkůvzdání
<g/>
,	,	kIx,	,
Otevřete	otevřít	k5eAaPmRp2nP	otevřít
okno	okno	k1gNnSc4	okno
aby	aby	kYmCp3nP	aby
duše	duše	k1gFnSc1	duše
mohla	moct	k5eAaImAgFnS	moct
ven	ven	k6eAd1	ven
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
Hapka	Hapka	k1gMnSc1	Hapka
<g/>
,	,	kIx,	,
Horáček	Horáček	k1gMnSc1	Horáček
–	–	k?	–
Strážce	strážce	k1gMnSc1	strážce
plamene	plamen	k1gInSc2	plamen
<g/>
)	)	kIx)	)
Tacho	Tac	k1gMnSc2	Tac
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
Tacho	Tac	k1gMnSc4	Tac
<g/>
)	)	kIx)	)
Bohemia	bohemia	k1gFnSc1	bohemia
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
klaun	klaun	k1gMnSc1	klaun
2013	[number]	k4	2013
Lagu	Lagus	k1gInSc2	Lagus
2013	[number]	k4	2013
Uruz	Uruz	k1gInSc1	Uruz
2014	[number]	k4	2014
Šmouha	šmouha	k1gFnSc1	šmouha
2014	[number]	k4	2014
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Neoficiální	neoficiální	k2eAgFnSc2d1	neoficiální
stránky	stránka	k1gFnSc2	stránka
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
13	[number]	k4	13
<g/>
.	.	kIx.	.
komnata	komnata	k1gFnSc1	komnata
</s>
