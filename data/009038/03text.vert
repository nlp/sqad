<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
asociací	asociace	k1gFnPc2	asociace
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
Football	Football	k1gInSc1	Football
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnPc4d1	hlavní
řídící	řídící	k2eAgFnPc4d1	řídící
organizace	organizace	k1gFnPc4	organizace
světového	světový	k2eAgInSc2d1	světový
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
futsalu	futsal	k1gInSc2	futsal
a	a	k8xC	a
plážového	plážový	k2eAgInSc2d1	plážový
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
sídlo	sídlo	k1gNnSc1	sídlo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Curychu	Curych	k1gInSc6	Curych
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Gianni	Gianň	k1gMnPc7	Gianň
Infantino	Infantin	k2eAgNnSc1d1	Infantino
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnSc4	její
hlavní	hlavní	k2eAgFnPc1d1	hlavní
náplně	náplň	k1gFnPc1	náplň
patří	patřit	k5eAaImIp3nP	patřit
pořádání	pořádání	k1gNnSc4	pořádání
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
první	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIFA	FIFA	kA	FIFA
má	mít	k5eAaImIp3nS	mít
209	[number]	k4	209
členských	členský	k2eAgFnPc2d1	členská
asociací	asociace	k1gFnPc2	asociace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
méně	málo	k6eAd2	málo
než	než	k8xS	než
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
asociace	asociace	k1gFnSc1	asociace
atletických	atletický	k2eAgFnPc2d1	atletická
federací	federace	k1gFnPc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
suverénními	suverénní	k2eAgInPc7d1	suverénní
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
růstem	růst	k1gInSc7	růst
popularity	popularita	k1gFnSc2	popularita
fotbalu	fotbal	k1gInSc2	fotbal
i	i	k9	i
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
objevila	objevit	k5eAaPmAgFnS	objevit
potřeba	potřeba	k1gFnSc1	potřeba
existence	existence	k1gFnSc1	existence
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
národní	národní	k2eAgInPc4d1	národní
svazy	svaz	k1gInPc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
FIFA	FIFA	kA	FIFA
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
založení	založení	k1gNnSc6	založení
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
národní	národní	k2eAgFnPc1d1	národní
asociace	asociace	k1gFnPc1	asociace
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
reprezentováno	reprezentovat	k5eAaImNgNnS	reprezentovat
Madridským	madridský	k2eAgInSc7d1	madridský
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
klubem	klub	k1gInSc7	klub
<g/>
;	;	kIx,	;
Španělská	španělský	k2eAgFnSc1d1	španělská
federace	federace	k1gFnSc1	federace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k9	až
9	[number]	k4	9
let	léto	k1gNnPc2	léto
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
FIFA	FIFA	kA	FIFA
-	-	kIx~	-
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
Německá	německý	k2eAgFnSc1d1	německá
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
asociace	asociace	k1gFnSc1	asociace
skrze	skrze	k?	skrze
telegram	telegram	k1gInSc4	telegram
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
FIFA	FIFA	kA	FIFA
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Robert	Robert	k1gMnSc1	Robert
Guérin	Guérin	k1gInSc4	Guérin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
Danielem	Daniel	k1gMnSc7	Daniel
Burley	Burlea	k1gMnSc2	Burlea
Woolfallem	Woolfallo	k1gNnSc7	Woolfallo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
měla	mít	k5eAaImAgFnS	mít
FIFA	FIFA	kA	FIFA
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
byl	být	k5eAaImAgInS	být
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
turnaj	turnaj	k1gInSc1	turnaj
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1908	[number]	k4	1908
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Možnost	možnost	k1gFnSc1	možnost
členství	členství	k1gNnSc2	členství
ve	v	k7c6	v
FIFA	FIFA	kA	FIFA
i	i	k9	i
pro	pro	k7c4	pro
mimoevropské	mimoevropský	k2eAgInPc4d1	mimoevropský
státy	stát	k1gInPc4	stát
se	se	k3xPyFc4	se
aplikovala	aplikovat	k5eAaBmAgFnS	aplikovat
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
Argentina	Argentina	k1gFnSc1	Argentina
a	a	k8xC	a
Chile	Chile	k1gNnSc1	Chile
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
FIFA	FIFA	kA	FIFA
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
1	[number]	k4	1
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebylo	být	k5eNaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
federace	federace	k1gFnSc1	federace
bude	být	k5eAaImBp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
fotbalistů	fotbalista	k1gMnPc2	fotbalista
bylo	být	k5eAaImAgNnS	být
posláno	poslat	k5eAaPmNgNnS	poslat
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
státy	stát	k1gInPc7	stát
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
jen	jen	k9	jen
minimum	minimum	k1gNnSc4	minimum
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc2	vedení
federace	federace	k1gFnSc2	federace
ujal	ujmout	k5eAaPmAgMnS	ujmout
Carl	Carl	k1gMnSc1	Carl
Hirschmann	Hirschmann	k1gMnSc1	Hirschmann
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
FIFA	FIFA	kA	FIFA
zachránil	zachránit	k5eAaPmAgMnS	zachránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
odstoupení	odstoupení	k1gNnSc4	odstoupení
tzv.	tzv.	kA	tzv.
Domácích	domácí	k2eAgInPc2d1	domácí
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
soutěžit	soutěžit	k5eAaImF	soutěžit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
dosavadními	dosavadní	k2eAgMnPc7d1	dosavadní
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgInPc1d1	domácí
národy	národ	k1gInPc1	národ
obnovily	obnovit	k5eAaPmAgInP	obnovit
své	své	k1gNnSc4	své
členství	členství	k1gNnSc2	členství
ve	v	k7c6	v
FIFA	FIFA	kA	FIFA
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
prezidentů	prezident	k1gMnPc2	prezident
FIFA	FIFA	kA	FIFA
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
FIFA	FIFA	kA	FIFA
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
primárně	primárně	k6eAd1	primárně
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
podle	podle	k7c2	podle
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
kongres	kongres	k1gInSc1	kongres
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc2	svůj
zástupce	zástupce	k1gMnSc2	zástupce
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
209	[number]	k4	209
členských	členský	k2eAgFnPc2d1	členská
asociací	asociace	k1gFnPc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
nebo	nebo	k8xC	nebo
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
zasedá	zasedat	k5eAaImIp3nS	zasedat
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
konají	konat	k5eAaImIp3nP	konat
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
mimořádná	mimořádný	k2eAgNnPc4d1	mimořádné
zasedání	zasedání	k1gNnPc4	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
volí	volit	k5eAaImIp3nS	volit
prezidenta	prezident	k1gMnSc4	prezident
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
generálního	generální	k2eAgMnSc4d1	generální
sekretáře	sekretář	k1gMnSc4	sekretář
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
je	být	k5eAaImIp3nS	být
automaticky	automaticky	k6eAd1	automaticky
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
zasedáními	zasedání	k1gNnPc7	zasedání
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
národní	národní	k2eAgFnPc1d1	národní
asociace	asociace	k1gFnPc1	asociace
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
sdruženy	sdružit	k5eAaPmNgInP	sdružit
do	do	k7c2	do
šesti	šest	k4xCc2	šest
regionálních	regionální	k2eAgFnPc2d1	regionální
konfederací	konfederace	k1gFnPc2	konfederace
podle	podle	k7c2	podle
světadílu	světadíl	k1gInSc2	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
národní	národní	k2eAgFnSc1d1	národní
asociace	asociace	k1gFnSc1	asociace
mohla	moct	k5eAaImAgFnS	moct
stát	stát	k5eAaPmF	stát
členem	člen	k1gInSc7	člen
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nejprve	nejprve	k6eAd1	nejprve
členem	člen	k1gInSc7	člen
své	svůj	k3xOyFgFnSc2	svůj
regionální	regionální	k2eAgFnSc2d1	regionální
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
AFC	AFC	kA	AFC
–	–	k?	–
Asian	Asian	k1gInSc1	Asian
Football	Football	k1gMnSc1	Football
Confederation	Confederation	k1gInSc1	Confederation
(	(	kIx(	(
<g/>
Asie	Asie	k1gFnSc1	Asie
<g/>
)	)	kIx)	)
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
CAF	CAF	kA	CAF
–	–	k?	–
Confédération	Confédération	k1gInSc1	Confédération
Africaine	Africain	k1gInSc5	Africain
de	de	k?	de
Football	Football	k1gInSc1	Football
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CONCACAF	CONCACAF	kA	CONCACAF
–	–	k?	–
Confederation	Confederation	k1gInSc1	Confederation
of	of	k?	of
North	North	k1gMnSc1	North
<g/>
,	,	kIx,	,
Central	Central	k1gMnSc1	Central
American	American	k1gMnSc1	American
and	and	k?	and
Caribbean	Caribbean	k1gInSc1	Caribbean
Association	Association	k1gInSc1	Association
Football	Football	k1gInSc1	Football
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
Karibik	Karibik	k1gInSc1	Karibik
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
CONMEBOL	CONMEBOL	kA	CONMEBOL
–	–	k?	–
Confederación	Confederación	k1gInSc1	Confederación
Sudamericana	Sudamericana	k1gFnSc1	Sudamericana
de	de	k?	de
Fútbol	Fútbol	k1gInSc1	Fútbol
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OFC	OFC	kA	OFC
–	–	k?	–
Oceania	Oceanium	k1gNnSc2	Oceanium
Football	Footballa	k1gFnPc2	Footballa
Confederation	Confederation	k1gInSc1	Confederation
(	(	kIx(	(
<g/>
Oceánie	Oceánie	k1gFnSc1	Oceánie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
UEFA	UEFA	kA	UEFA
–	–	k?	–
Union	union	k1gInSc1	union
of	of	k?	of
European	European	k1gInSc1	European
Football	Football	k1gMnSc1	Football
Associations	Associations	k1gInSc1	Associations
(	(	kIx(	(
<g/>
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
)	)	kIx)	)
<g/>
31	[number]	k4	31
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
AFC	AFC	kA	AFC
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
členem	člen	k1gInSc7	člen
OFC	OFC	kA	OFC
<g/>
.2	.2	k4	.2
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Surinam	Surinam	k1gInSc1	Surinam
a	a	k8xC	a
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
CONCACAF	CONCACAF	kA	CONCACAF
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.3	.3	k4	.3
Vzhledem	vzhledem	k7c3	vzhledem
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
jasnému	jasný	k2eAgNnSc3d1	jasné
vedení	vedení	k1gNnSc3	vedení
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gInPc4	člen
UEFA	UEFA	kA	UEFA
také	také	k9	také
státy	stát	k1gInPc1	stát
jako	jako	k8xC	jako
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
větší	veliký	k2eAgFnSc7d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc7d2	menší
částí	část	k1gFnSc7	část
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
také	také	k9	také
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
členy	člen	k1gInPc7	člen
AFC	AFC	kA	AFC
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Kypr	Kypr	k1gInSc1	Kypr
leží	ležet	k5eAaImIp3nS	ležet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
je	on	k3xPp3gNnPc4	on
pojí	pojit	k5eAaImIp3nP	pojit
historické	historický	k2eAgInPc4d1	historický
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInPc4d1	kulturní
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
důvody	důvod	k1gInPc4	důvod
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
Kazachstán	Kazachstán	k1gInSc4	Kazachstán
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
bývalými	bývalý	k2eAgInPc7d1	bývalý
členy	člen	k1gInPc7	člen
AFC	AFC	kA	AFC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
FIFA	FIFA	kA	FIFA
209	[number]	k4	209
národních	národní	k2eAgFnPc2d1	národní
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
asociací	asociace	k1gFnPc2	asociace
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
mužských	mužský	k2eAgInPc2d1	mužský
reprezentačních	reprezentační	k2eAgInPc2d1	reprezentační
týmů	tým	k1gInPc2	tým
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
seznam	seznam	k1gInSc4	seznam
mužských	mužský	k2eAgFnPc2d1	mužská
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
reprezentací	reprezentace	k1gFnPc2	reprezentace
a	a	k8xC	a
seznam	seznam	k1gInSc1	seznam
kódů	kód	k1gInPc2	kód
zemí	zem	k1gFnPc2	zem
podle	podle	k7c2	podle
FIFA	FIFA	kA	FIFA
a	a	k8xC	a
129	[number]	k4	129
ženských	ženský	k2eAgInPc2d1	ženský
reprezentačních	reprezentační	k2eAgInPc2d1	reprezentační
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
FIFA	FIFA	kA	FIFA
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
členů	člen	k1gMnPc2	člen
než	než	k8xS	než
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
FIFA	FIFA	kA	FIFA
uznává	uznávat	k5eAaImIp3nS	uznávat
23	[number]	k4	23
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
samostatnými	samostatný	k2eAgInPc7d1	samostatný
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
osm	osm	k4xCc1	osm
suverénních	suverénní	k2eAgInPc2d1	suverénní
států	stát	k1gInPc2	stát
není	být	k5eNaImIp3nS	být
členem	člen	k1gInSc7	člen
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Monako	Monako	k1gNnSc1	Monako
<g/>
,	,	kIx,	,
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
,	,	kIx,	,
Mikronésie	Mikronésie	k1gFnSc1	Mikronésie
<g/>
,	,	kIx,	,
Marshallovy	Marshallův	k2eAgInPc4d1	Marshallův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Kiribati	Kiribati	k1gFnSc4	Kiribati
<g/>
,	,	kIx,	,
Tuvalu	Tuvala	k1gFnSc4	Tuvala
<g/>
,	,	kIx,	,
Palau	Palaa	k1gFnSc4	Palaa
a	a	k8xC	a
Nauru	Naura	k1gFnSc4	Naura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
vydáván	vydávat	k5eAaImNgInS	vydávat
žebříček	žebříček	k1gInSc1	žebříček
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
turnajích	turnaj	k1gInPc6	turnaj
<g/>
,	,	kIx,	,
kvalifikacích	kvalifikace	k1gFnPc6	kvalifikace
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
a	a	k8xC	a
v	v	k7c6	v
přátelských	přátelský	k2eAgInPc6d1	přátelský
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
žebříček	žebříček	k1gInSc1	žebříček
FIFA	FIFA	kA	FIFA
se	se	k3xPyFc4	se
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žebříček	žebříček	k1gInSc1	žebříček
slouží	sloužit	k5eAaImIp3nS	sloužit
například	například	k6eAd1	například
pro	pro	k7c4	pro
sestavování	sestavování	k1gNnSc4	sestavování
losovacích	losovací	k2eAgInPc2d1	losovací
košů	koš	k1gInPc2	koš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc4	ocenění
FIFA	FIFA	kA	FIFA
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
individuálním	individuální	k2eAgNnSc7d1	individuální
oceněním	ocenění	k1gNnSc7	ocenění
FIFA	FIFA	kA	FIFA
byl	být	k5eAaImAgMnS	být
titul	titul	k1gInSc4	titul
FIFA	FIFA	kA	FIFA
World	World	k1gInSc1	World
Player	Player	k1gInSc1	Player
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
a	a	k8xC	a
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
hráče	hráč	k1gMnSc4	hráč
trenéři	trenér	k1gMnPc1	trenér
národních	národní	k2eAgInPc2d1	národní
reprezentačních	reprezentační	k2eAgInPc2d1	reprezentační
týmů	tým	k1gInPc2	tým
<g/>
;	;	kIx,	;
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
tři	tři	k4xCgMnPc4	tři
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
ohodnotil	ohodnotit	k5eAaPmAgMnS	ohodnotit
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
a	a	k8xC	a
5	[number]	k4	5
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
FIFA	FIFA	kA	FIFA
omezila	omezit	k5eAaPmAgFnS	omezit
libovolný	libovolný	k2eAgInSc4d1	libovolný
výběr	výběr	k1gInSc4	výběr
kandidátů	kandidát	k1gMnPc2	kandidát
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
35	[number]	k4	35
nominovaných	nominovaný	k2eAgMnPc2d1	nominovaný
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
mužská	mužský	k2eAgFnSc1d1	mužská
kategorie	kategorie	k1gFnSc1	kategorie
této	tento	k3xDgFnSc2	tento
trofeje	trofej	k1gFnSc2	trofej
sloučena	sloučen	k2eAgFnSc1d1	sloučena
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
výhercem	výherce	k1gMnSc7	výherce
samostatného	samostatný	k2eAgNnSc2d1	samostatné
ocenění	ocenění	k1gNnSc2	ocenění
Fotbalisty	fotbalista	k1gMnSc2	fotbalista
roku	rok	k1gInSc2	rok
FIFA	FIFA	kA	FIFA
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Lionel	Lionel	k1gMnSc1	Lionel
Messi	Messe	k1gFnSc4	Messe
<g/>
.	.	kIx.	.
</s>
<s>
FIFA	FIFA	kA	FIFA
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
uděluje	udělovat	k5eAaImIp3nS	udělovat
samostatně	samostatně	k6eAd1	samostatně
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soutěže	soutěž	k1gFnSc2	soutěž
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
FIFA	FIFA	kA	FIFA
==	==	k?	==
</s>
</p>
<p>
<s>
Mužské	mužský	k2eAgNnSc1d1	mužské
turnajeMistrovství	turnajeMistrovství	k1gNnSc1	turnajeMistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
</s>
</p>
<p>
<s>
Konfederační	konfederační	k2eAgInSc1d1	konfederační
pohár	pohár	k1gInSc1	pohár
FIFA	FIFA	kA	FIFA
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
17	[number]	k4	17
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
klubů	klub	k1gInPc2	klub
</s>
</p>
<p>
<s>
Blue	Blue	k1gInSc1	Blue
Stars	Stars	k1gInSc1	Stars
<g/>
/	/	kIx~	/
<g/>
FIFA	FIFA	kA	FIFA
Youth	Youth	k1gInSc1	Youth
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
plážovém	plážový	k2eAgInSc6d1	plážový
fotbaleŽenské	fotbaleŽenský	k2eAgInPc1d1	fotbaleŽenský
turnajeMistrovství	turnajeMistrovství	k1gNnSc4	turnajeMistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
žen	žena	k1gFnPc2	žena
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
žen	žena	k1gFnPc2	žena
do	do	k7c2	do
17	[number]	k4	17
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
==	==	k?	==
Sponzoři	sponzor	k1gMnPc5	sponzor
==	==	k?	==
</s>
</p>
<p>
<s>
Adidas	Adidas	k1gMnSc1	Adidas
</s>
</p>
<p>
<s>
Coca-Cola	cocaola	k1gFnSc1	coca-cola
</s>
</p>
<p>
<s>
Emirates	Emirates	k1gMnSc1	Emirates
</s>
</p>
<p>
<s>
Hyundai-Kia	Hyundai-Kia	k1gFnSc1	Hyundai-Kia
Motors	Motorsa	k1gFnPc2	Motorsa
</s>
</p>
<p>
<s>
Sony	Sony	kA	Sony
</s>
</p>
<p>
<s>
Visa	viso	k1gNnPc1	viso
</s>
</p>
<p>
<s>
==	==	k?	==
Korupce	korupce	k1gFnSc1	korupce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skandál	skandál	k1gInSc1	skandál
2012	[number]	k4	2012
===	===	k?	===
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
musela	muset	k5eAaImAgFnS	muset
FIFA	FIFA	kA	FIFA
z	z	k7c2	z
nařízení	nařízení	k1gNnSc2	nařízení
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgFnPc6	který
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
FIFA	FIFA	kA	FIFA
Joao	Joao	k1gMnSc1	Joao
Havelange	Havelang	k1gFnSc2	Havelang
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
Ricardo	Ricardo	k1gNnSc1	Ricardo
Teixeira	Teixeiro	k1gNnSc2	Teixeiro
brali	brát	k5eAaImAgMnP	brát
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
úplatky	úplatek	k1gInPc1	úplatek
spojené	spojený	k2eAgInPc1d1	spojený
prodejem	prodej	k1gInSc7	prodej
televizních	televizní	k2eAgNnPc2d1	televizní
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
přenosy	přenos	k1gInPc4	přenos
z	z	k7c2	z
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Teixeira	Teixeira	k1gMnSc1	Teixeira
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
marketingového	marketingový	k2eAgMnSc2d1	marketingový
partnera	partner	k1gMnSc2	partner
FIFA	FIFA	kA	FIFA
společnosti	společnost	k1gFnSc2	společnost
ISL	ISL	kA	ISL
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
až	až	k9	až
1997	[number]	k4	1997
nejméně	málo	k6eAd3	málo
12,74	[number]	k4	12,74
milionu	milion	k4xCgInSc2	milion
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
<s>
Havelange	Havelange	k6eAd1	Havelange
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
obdržel	obdržet	k5eAaPmAgMnS	obdržet
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
<s>
FIFA	FIFA	kA	FIFA
původně	původně	k6eAd1	původně
chtěla	chtít	k5eAaImAgFnS	chtít
uveřejnění	uveřejnění	k1gNnSc3	uveřejnění
dokumentu	dokument	k1gInSc2	dokument
soudní	soudní	k2eAgFnSc7d1	soudní
cestou	cesta	k1gFnSc7	cesta
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
změnila	změnit	k5eAaPmAgFnS	změnit
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skandály	skandál	k1gInPc7	skandál
2015	[number]	k4	2015
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
americkými	americký	k2eAgMnPc7d1	americký
a	a	k8xC	a
švýcarskými	švýcarský	k2eAgMnPc7d1	švýcarský
obviněno	obvinit	k5eAaPmNgNnS	obvinit
5	[number]	k4	5
sportovních	sportovní	k2eAgInPc2d1	sportovní
marketingových	marketingový	k2eAgInPc2d1	marketingový
manažerů	manažer	k1gInPc2	manažer
a	a	k8xC	a
9	[number]	k4	9
vysoký	vysoký	k2eAgInSc4d1	vysoký
funkcionářů	funkcionář	k1gMnPc2	funkcionář
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
,	,	kIx,	,
vydírání	vydírání	k1gNnSc2	vydírání
<g/>
,	,	kIx,	,
defraudace	defraudace	k1gFnSc2	defraudace
či	či	k8xC	či
praní	praní	k1gNnSc2	praní
špinavých	špinavý	k2eAgInPc2d1	špinavý
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obviněným	obviněný	k1gMnSc7	obviněný
byli	být	k5eAaImAgMnP	být
viceprezidenti	viceprezident	k1gMnPc1	viceprezident
FIFA	FIFA	kA	FIFA
Jeffrey	Jeffrea	k1gMnSc2	Jeffrea
Webb	Webb	k1gMnSc1	Webb
z	z	k7c2	z
Kajmanských	Kajmanský	k2eAgInPc2d1	Kajmanský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
Eugenio	Eugenio	k6eAd1	Eugenio
Figueredo	Figueredo	k1gNnSc1	Figueredo
z	z	k7c2	z
Uruguaye	Uruguay	k1gFnSc2	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Obvinění	obvinění	k1gNnSc1	obvinění
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
získání	získání	k1gNnSc1	získání
vysílacích	vysílací	k2eAgNnPc2d1	vysílací
a	a	k8xC	a
marketingových	marketingový	k2eAgNnPc2d1	marketingové
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
CONCACAF	CONCACAF	kA	CONCACAF
<g/>
,	,	kIx,	,
volby	volba	k1gFnPc1	volba
předsedy	předseda	k1gMnSc2	předseda
FIFA	FIFA	kA	FIFA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
pořadatelství	pořadatelství	k1gNnSc4	pořadatelství
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
a	a	k8xC	a
2022	[number]	k4	2022
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
obviněných	obviněný	k1gMnPc2	obviněný
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
nezákonně	zákonně	k6eNd1	zákonně
obohatit	obohatit	k5eAaPmF	obohatit
o	o	k7c4	o
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
úplatky	úplatek	k1gInPc1	úplatek
měly	mít	k5eAaImAgInP	mít
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výslechu	výslech	k1gInSc6	výslech
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vydání	vydání	k1gNnSc6	vydání
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
se	se	k3xPyFc4	se
bývalý	bývalý	k2eAgMnSc1d1	bývalý
viceprezident	viceprezident	k1gMnSc1	viceprezident
FIFA	FIFA	kA	FIFA
Figueredo	Figueredo	k1gNnSc4	Figueredo
přiznal	přiznat	k5eAaPmAgMnS	přiznat
k	k	k7c3	k
dlouholeté	dlouholetý	k2eAgFnSc3d1	dlouholetá
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
korupci	korupce	k1gFnSc3	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
Juana	Juan	k1gMnSc2	Juan
Gómeze	Gómeze	k1gFnSc2	Gómeze
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
úplatcích	úplatek	k1gInPc6	úplatek
získal	získat	k5eAaPmAgMnS	získat
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
sumy	suma	k1gFnPc4	suma
peněz	peníze	k1gInPc2	peníze
<g/>
"	"	kIx"	"
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
poskytováním	poskytování	k1gNnSc7	poskytování
marketingových	marketingový	k2eAgFnPc2d1	marketingová
a	a	k8xC	a
vysílacích	vysílací	k2eAgNnPc2d1	vysílací
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
odhalil	odhalit	k5eAaPmAgInS	odhalit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
korupční	korupční	k2eAgFnSc4d1	korupční
síť	síť	k1gFnSc4	síť
uvnitř	uvnitř	k7c2	uvnitř
CONMEBOL	CONMEBOL	kA	CONMEBOL
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
minimálně	minimálně	k6eAd1	minimálně
deset	deset	k4xCc1	deset
prezidentů	prezident	k1gMnPc2	prezident
národních	národní	k2eAgInPc2d1	národní
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
svazů	svaz	k1gInPc2	svaz
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
etická	etický	k2eAgFnSc1d1	etická
komise	komise	k1gFnSc1	komise
FIFA	FIFA	kA	FIFA
udělila	udělit	k5eAaPmAgFnS	udělit
zákaz	zákaz	k1gInSc4	zákaz
činnost	činnost	k1gFnSc1	činnost
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
8	[number]	k4	8
let	léto	k1gNnPc2	léto
a	a	k8xC	a
pokutu	pokuta	k1gFnSc4	pokuta
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
franků	frank	k1gInPc2	frank
za	za	k7c4	za
korupci	korupce	k1gFnSc4	korupce
bývalému	bývalý	k2eAgMnSc3d1	bývalý
prezidentovi	prezident	k1gMnSc3	prezident
FIFA	FIFA	kA	FIFA
Seppu	Sepp	k1gMnSc3	Sepp
Blatterovi	Blatter	k1gMnSc3	Blatter
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
předal	předat	k5eAaPmAgMnS	předat
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
franků	frank	k1gInPc2	frank
předsedovi	předseda	k1gMnSc3	předseda
UEFA	UEFA	kA	UEFA
Michelu	Michel	k1gMnSc3	Michel
Platinimu	Platinim	k1gMnSc3	Platinim
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
poradenskou	poradenský	k2eAgFnSc4d1	poradenská
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
však	však	k9	však
neexistuje	existovat	k5eNaImIp3nS	existovat
písemná	písemný	k2eAgFnSc1d1	písemná
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
korupčního	korupční	k2eAgInSc2d1	korupční
skandálu	skandál	k1gInSc2	skandál
FIFA	FIFA	kA	FIFA
byli	být	k5eAaImAgMnP	být
zapleteni	zaplést	k5eAaPmNgMnP	zaplést
i	i	k9	i
princ	princ	k1gMnSc1	princ
William	William	k1gInSc1	William
a	a	k8xC	a
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
David	David	k1gMnSc1	David
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Darby	Darba	k1gFnSc2	Darba
<g/>
,	,	kIx,	,
Africa	Africa	k1gMnSc1	Africa
<g/>
,	,	kIx,	,
Football	Football	k1gMnSc1	Football
and	and	k?	and
Fifa	Fifa	k1gMnSc1	Fifa
<g/>
:	:	kIx,	:
Politics	Politics	k1gInSc1	Politics
<g/>
,	,	kIx,	,
Colonialism	Colonialism	k1gInSc1	Colonialism
and	and	k?	and
Resistance	Resistance	k1gFnSc1	Resistance
(	(	kIx(	(
<g/>
Sport	sport	k1gInSc1	sport
in	in	k?	in
the	the	k?	the
Global	globat	k5eAaImAgMnS	globat
Society	societa	k1gFnPc4	societa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Frank	frank	k1gInSc1	frank
Cass	Cass	k1gInSc1	Cass
Publishers	Publishers	k1gInSc1	Publishers
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7146-8029-X	[number]	k4	0-7146-8029-X
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Sugden	Sugdna	k1gFnPc2	Sugdna
<g/>
,	,	kIx,	,
FIFA	FIFA	kA	FIFA
and	and	k?	and
the	the	k?	the
Contest	Contest	k1gFnSc1	Contest
For	forum	k1gNnPc2	forum
World	World	k1gMnSc1	World
Football	Football	k1gMnSc1	Football
<g/>
,	,	kIx,	,
Polity	polit	k2eAgInPc4d1	polit
Press	Press	k1gInSc4	Press
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7456-1661-5	[number]	k4	0-7456-1661-5
</s>
</p>
<p>
<s>
Jim	on	k3xPp3gMnPc3	on
Trecker	Trecker	k1gMnSc1	Trecker
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Miers	Miers	k1gInSc1	Miers
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Brett	Brett	k1gMnSc1	Brett
Whitesell	Whitesell	k1gMnSc1	Whitesell
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Women	Women	k1gInSc1	Women
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Soccer	Soccra	k1gFnPc2	Soccra
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
and	and	k?	and
the	the	k?	the
Fifa	Fif	k1gInSc2	Fif
World	Worlda	k1gFnPc2	Worlda
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
Universe	Universe	k1gFnSc1	Universe
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Revised	Revised	k1gInSc1	Revised
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7893-0527-5	[number]	k4	0-7893-0527-5
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
mužských	mužský	k2eAgFnPc2d1	mužská
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
reprezentací	reprezentace	k1gFnPc2	reprezentace
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kódů	kód	k1gInPc2	kód
zemí	zem	k1gFnPc2	zem
podle	podle	k7c2	podle
FIFA	FIFA	kA	FIFA
</s>
</p>
<p>
<s>
Žebříček	žebříček	k1gInSc1	žebříček
FIFA	FIFA	kA	FIFA
</s>
</p>
<p>
<s>
Žebříček	žebříček	k1gInSc1	žebříček
FIFA	FIFA	kA	FIFA
žen	žena	k1gFnPc2	žena
</s>
</p>
<p>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
FIFA	FIFA	kA	FIFA
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
FIFA	FIFA	kA	FIFA
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
FIFA	FIFA	kA	FIFA
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
fotbalu	fotbal	k1gInSc3	fotbal
</s>
</p>
