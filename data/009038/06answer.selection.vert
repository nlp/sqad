<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
asociací	asociace	k1gFnPc2	asociace
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
Football	Football	k1gInSc1	Football
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnPc4d1	hlavní
řídící	řídící	k2eAgFnPc4d1	řídící
organizace	organizace	k1gFnPc4	organizace
světového	světový	k2eAgInSc2d1	světový
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
futsalu	futsal	k1gInSc2	futsal
a	a	k8xC	a
plážového	plážový	k2eAgInSc2d1	plážový
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
