<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
obdélníku	obdélník	k1gInSc2	obdélník
se	s	k7c7	s
třemi	tři	k4xCgNnPc7	tři
stejně	stejně	k6eAd1	stejně
širokými	široký	k2eAgInPc7d1	široký
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
červeným	červený	k2eAgInSc7d1	červený
a	a	k8xC	a
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
bílý	bílý	k2eAgInSc1d1	bílý
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
a	a	k8xC	a
osmicípá	osmicípý	k2eAgFnSc1d1	osmicípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
šířky	šířka	k1gFnSc2	šířka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
ázerbájdžánské	ázerbájdžánský	k2eAgInPc1d1	ázerbájdžánský
Turky	turek	k1gInPc1	turek
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
pokroku	pokrok	k1gInSc2	pokrok
a	a	k8xC	a
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
islám	islám	k1gInSc1	islám
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vypůjčen	vypůjčit	k5eAaPmNgInS	vypůjčit
z	z	k7c2	z
turecké	turecký	k2eAgFnSc2d1	turecká
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
cípů	cíp	k1gInPc2	cíp
hvězdy	hvězda	k1gFnSc2	hvězda
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
osm	osm	k4xCc1	osm
hlavních	hlavní	k2eAgInPc2d1	hlavní
tureckých	turecký	k2eAgInPc2d1	turecký
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
vzor	vzor	k1gInSc1	vzor
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
autora	autor	k1gMnSc4	autor
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
básník	básník	k1gMnSc1	básník
Ali	Ali	k1gMnSc1	Ali
Bej	bej	k1gMnSc1	bej
Husejn	Husejn	k1gMnSc1	Husejn
Zade	zad	k1gInSc5	zad
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vlajku	vlajka	k1gFnSc4	vlajka
jej	on	k3xPp3gInSc2	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
vláda	vláda	k1gFnSc1	vláda
Ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1	Ázerbájdžánská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
sovětské	sovětský	k2eAgFnSc2d1	sovětská
moci	moc	k1gFnSc2	moc
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
vlajka	vlajka	k1gFnSc1	vlajka
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
sovětským	sovětský	k2eAgInSc7d1	sovětský
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
</s>
</p>
<p>
<s>
Ázerbájdžánská	ázerbájdžánský	k2eAgFnSc1d1	Ázerbájdžánská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ázerbájdžánská	ázerbájdžánský	k2eAgFnSc1d1	Ázerbájdžánská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
