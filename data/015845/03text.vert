<s>
Továrna	továrna	k1gFnSc1
Walter	Walter	k1gMnSc1
(	(	kIx(
<g/>
stavba	stavba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Továrna	továrna	k1gFnSc1
Walter	Walter	k1gMnSc1
(	(	kIx(
<g/>
stavba	stavba	k1gFnSc1
<g/>
)	)	kIx)
Továrna	továrna	k1gFnSc1
J.	J.	kA
Walter	Walter	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
funkcionalismus	funkcionalismus	k1gInSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1913	#num#	k4
Přestavba	přestavba	k1gFnSc1
</s>
<s>
1928	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
2015	#num#	k4
Stavebník	stavebník	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Walter	Walter	k1gMnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Klikatá	klikatý	k2eAgFnSc1d1
329	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
5	#num#	k4
-	-	kIx~
Jinonice	Jinonice	k1gFnPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Klikatá	klikatý	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
21,66	21,66	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
19,61	19,61	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
103726	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Továrna	továrna	k1gFnSc1
Walter	Walter	k1gMnSc1
(	(	kIx(
<g/>
Továrna	továrna	k1gFnSc1
automobilů	automobil	k1gInPc2
J.	J.	kA
Walter	Walter	k1gMnSc1
a	a	k8xC
spol	spol	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1948	#num#	k4
Motorlet	motorlet	k1gInSc1
n.	n.	k?
p.	p.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zaniklý	zaniklý	k2eAgInSc1d1
průmyslový	průmyslový	k2eAgInSc1d1
areál	areál	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
5	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
západně	západně	k6eAd1
od	od	k7c2
nádraží	nádraží	k1gNnSc2
Praha-Jinonice	Praha-Jinonice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediná	jediný	k2eAgFnSc1d1
dochovaná	dochovaný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
31	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
chráněna	chránit	k5eAaImNgFnS
jako	jako	k9
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tovární	tovární	k2eAgInSc4d1
areál	areál	k1gInSc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
automobilů	automobil	k1gInPc2
byl	být	k5eAaImAgInS
v	v	k7c6
Jinonicích	Jinonice	k1gFnPc6
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
letech	let	k1gInPc6
1911	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
zde	zde	k6eAd1
závod	závod	k1gInSc1
se	s	k7c7
slévárnou	slévárna	k1gFnSc7
<g/>
,	,	kIx,
karosárnou	karosárna	k1gFnSc7
<g/>
,	,	kIx,
smaltovnou	smaltovna	k1gFnSc7
<g/>
,	,	kIx,
lakovnou	lakovna	k1gFnSc7
a	a	k8xC
brzdicí	brzdicí	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
od	od	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
vyráběl	vyrábět	k5eAaImAgInS
také	také	k9
hvězdicové	hvězdicový	k2eAgInPc4d1
letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Stavbu	stavba	k1gFnSc4
i	i	k8xC
pozdější	pozdní	k2eAgFnPc4d2
přístavby	přístavba	k1gFnPc4
provedla	provést	k5eAaPmAgFnS
Českomoravská	českomoravský	k2eAgFnSc1d1
stavební	stavební	k2eAgFnSc1d1
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
;	;	kIx,
roku	rok	k1gInSc2
1937	#num#	k4
postavila	postavit	k5eAaPmAgFnS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
administrativní	administrativní	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
a	a	k8xC
dostavěla	dostavět	k5eAaPmAgFnS
poslední	poslední	k2eAgNnSc4d1
patro	patro	k1gNnSc4
montážní	montážní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
budovy	budova	k1gFnPc1
ve	v	k7c6
funkcionalistickém	funkcionalistický	k2eAgInSc6d1
stylu	styl	k1gInSc6
měly	mít	k5eAaImAgInP
trámové	trámový	k2eAgInPc1d1
stropy	strop	k1gInPc1
s	s	k7c7
průvlaky	průvlak	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Montážní	montážní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
</s>
<s>
památkově	památkově	k6eAd1
chráněná	chráněný	k2eAgFnSc1d1
dochovaná	dochovaný	k2eAgFnSc1d1
budova	budova	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
areálu	areál	k1gInSc2
Waltrovky	waltrovka	k1gFnSc2
stojí	stát	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
budova	budova	k1gFnSc1
čp.	čp.	k?
329	#num#	k4
postavená	postavený	k2eAgFnSc1d1
podle	podle	k7c2
projektu	projekt	k1gInSc2
arch	archa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Františka	František	k1gMnSc2
Bartoše	Bartoš	k1gMnSc2
(	(	kIx(
<g/>
projekt	projekt	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
montážní	montážní	k2eAgFnSc1d1
<g/>
,	,	kIx,
výrobní	výrobní	k2eAgFnSc1d1
a	a	k8xC
skladovací	skladovací	k2eAgFnSc1d1
hala	hala	k1gFnSc1
pro	pro	k7c4
osobní	osobní	k2eAgInPc4d1
a	a	k8xC
nákladní	nákladní	k2eAgInPc4d1
automobily	automobil	k1gInPc4
<g/>
,	,	kIx,
letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
a	a	k8xC
další	další	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Budova	budova	k1gFnSc1
postavená	postavený	k2eAgFnSc1d1
na	na	k7c6
obdélném	obdélný	k2eAgInSc6d1
půdorysu	půdorys	k1gInSc6
ve	v	k7c6
funkcionalistickém	funkcionalistický	k2eAgInSc6d1
slohu	sloh	k1gInSc6
je	být	k5eAaImIp3nS
podsklepená	podsklepený	k2eAgFnSc1d1
šestipodlažní	šestipodlažní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
s	s	k7c7
nejvyšším	vysoký	k2eAgNnSc7d3
ustupujícím	ustupující	k2eAgNnSc7d1
patrem	patro	k1gNnSc7
a	a	k8xC
plochou	plochý	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
železobetonový	železobetonový	k2eAgInSc1d1
skelet	skelet	k1gInSc1
má	mít	k5eAaImIp3nS
obdélná	obdélný	k2eAgNnPc4d1
pole	pole	k1gNnPc4
na	na	k7c6
rastru	rastr	k1gInSc6
4	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
jej	on	k3xPp3gNnSc2
osmiboké	osmiboký	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
s	s	k7c7
hřibovitými	hřibovitý	k2eAgFnPc7d1
hlavicemi	hlavice	k1gFnPc7
a	a	k8xC
ploché	plochý	k2eAgInPc1d1
stropy	strop	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okna	okno	k1gNnPc1
jsou	být	k5eAaImIp3nP
zároveň	zároveň	k6eAd1
výplněmi	výplň	k1gFnPc7
nosné	nosný	k2eAgFnPc1d1
konstrukce	konstrukce	k1gFnPc1
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
okenními	okenní	k2eAgInPc7d1
rámy	rám	k1gInPc7
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgFnP
v	v	k7c6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
-	-	kIx~
jsou	být	k5eAaImIp3nP
kovové	kovový	k2eAgInPc1d1
s	s	k7c7
rastrem	rastr	k1gInSc7
skleněných	skleněný	k2eAgFnPc2d1
tabulek	tabulka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
šachta	šachta	k1gFnSc1
nákladního	nákladní	k2eAgInSc2d1
výtahu	výtah	k1gInSc2
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
vystupuje	vystupovat	k5eAaImIp3nS
trojramenné	trojramenný	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
nad	nad	k7c4
rovinu	rovina	k1gFnSc4
střechy	střecha	k1gFnSc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
kubusu	kubus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pilířové	pilířový	k2eAgNnSc1d1
<g/>
,	,	kIx,
se	s	k7c7
schodnicemi	schodnice	k1gFnPc7
obloženými	obložený	k2eAgFnPc7d1
bílým	bílý	k1gMnSc7
mramorem	mramor	k1gInSc7
<g/>
;	;	kIx,
osvětleno	osvětlen	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
vertikálními	vertikální	k2eAgNnPc7d1
okny	okno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
kolmými	kolmý	k2eAgInPc7d1
pruty	prut	k1gInPc7
dochovaného	dochovaný	k2eAgNnSc2d1
zábradlí	zábradlí	k1gNnSc2
jsou	být	k5eAaImIp3nP
vloženy	vložen	k2eAgFnPc1d1
obdélné	obdélný	k2eAgFnPc1d1
a	a	k8xC
kosodélné	kosodélný	k2eAgFnPc1d1
terče	terč	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Patra	patro	k1gNnSc2
byla	být	k5eAaImAgFnS
propojena	propojit	k5eAaPmNgFnS
velkým	velký	k2eAgInSc7d1
výtahem	výtah	k1gInSc7
pro	pro	k7c4
dopravu	doprava	k1gFnSc4
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
menším	malý	k2eAgInSc7d2
výtahem	výtah	k1gInSc7
pro	pro	k7c4
dopravu	doprava	k1gFnSc4
materiálu	materiál	k1gInSc2
a	a	k8xC
osobním	osobní	k2eAgInSc7d1
výtahem	výtah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schodištěm	schodiště	k1gNnSc7
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
rohovým	rohový	k2eAgInSc7d1
třípatrovým	třípatrový	k2eAgInSc7d1
domem	dům	k1gInSc7
č.	č.	k?
<g/>
p.	p.	k?
346	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
kancelářské	kancelářský	k2eAgFnPc1d1
místnosti	místnost	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
části	část	k1gFnSc6
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
patra	patro	k1gNnSc2
kanceláře	kancelář	k1gFnSc2
pro	pro	k7c4
administrativu	administrativa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
budově	budova	k1gFnSc6
přiléhala	přiléhat	k5eAaImAgNnP
další	další	k2eAgNnPc1d1
křídla	křídlo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
byla	být	k5eAaImAgNnP
při	při	k7c6
demolici	demolice	k1gFnSc6
celého	celý	k2eAgInSc2d1
areálu	areál	k1gInSc2
zbořena	zbořit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iranhala	Iranhat	k5eAaBmAgFnS,k5eAaImAgFnS,k5eAaPmAgFnS
</s>
<s>
Výrobní	výrobní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
7	#num#	k4
zvaná	zvaný	k2eAgFnSc1d1
„	„	k?
<g/>
Iranhala	Iranhal	k1gMnSc2
<g/>
“	“	k?
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1942	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
na	na	k7c6
části	část	k1gFnSc6
pozemku	pozemek	k1gInSc2
bývalého	bývalý	k2eAgNnSc2d1
zahradnictví	zahradnictví	k1gNnSc2
Františka	František	k1gMnSc2
Buzka	Buzek	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
společnost	společnost	k1gFnSc1
Walter	Walter	k1gMnSc1
odkoupila	odkoupit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Halu	halo	k1gNnSc3
od	od	k7c2
vlastní	vlastní	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
původně	původně	k6eAd1
oddělovala	oddělovat	k5eAaImAgFnS
veřejná	veřejný	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
dalším	další	k2eAgNnSc6d1
rozšiřování	rozšiřování	k1gNnSc6
areálu	areál	k1gInSc2
se	se	k3xPyFc4
ze	z	k7c2
silnice	silnice	k1gFnSc2
stala	stát	k5eAaPmAgFnS
hlavní	hlavní	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
podniku	podnik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
v	v	k7c4
10.30	10.30	k4
hodin	hodina	k1gFnPc2
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
haly	hala	k1gFnSc2
hořet	hořet	k5eAaImF
a	a	k8xC
oheň	oheň	k1gInSc1
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgInS
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
haly	hala	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oheň	oheň	k1gInSc4
a	a	k8xC
následně	následně	k6eAd1
voda	voda	k1gFnSc1
značně	značně	k6eAd1
poškodily	poškodit	k5eAaPmAgInP
188	#num#	k4
obráběcích	obráběcí	k2eAgInPc2d1
strojů	stroj	k1gInPc2
-	-	kIx~
třetinu	třetina	k1gFnSc4
všech	všecek	k3xTgInPc2
obráběcích	obráběcí	k2eAgInPc2d1
strojů	stroj	k1gInPc2
v	v	k7c6
hale	hala	k1gFnSc6
<g/>
;	;	kIx,
15	#num#	k4
neopravitelných	opravitelný	k2eNgMnPc2d1
bylo	být	k5eAaImAgNnS
následně	následně	k6eAd1
sešrotováno	sešrotován	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Energetická	energetický	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
</s>
<s>
areál	areál	k1gInSc1
bývalé	bývalý	k2eAgFnSc2d1
Walterovy	Walterův	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
a	a	k8xC
komín	komín	k1gInSc1
energetické	energetický	k2eAgFnSc2d1
ústředny	ústředna	k1gFnSc2
</s>
<s>
Roku	rok	k1gInSc2
1950	#num#	k4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
n.	n.	k?
p.	p.	k?
Motorlet	motorlet	k1gInSc1
postavena	postaven	k2eAgFnSc1d1
kotelna	kotelna	k1gFnSc1
na	na	k7c6
pozemku	pozemek	k1gInSc6
při	při	k7c6
železniční	železniční	k2eAgFnSc6d1
trati	trať	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
od	od	k7c2
vlastní	vlastní	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
oddělovala	oddělovat	k5eAaImAgFnS
silnice	silnice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
výrobním	výrobní	k2eAgFnPc3d1
halám	hala	k1gFnPc3
vedl	vést	k5eAaImAgInS
z	z	k7c2
kotelny	kotelna	k1gFnSc2
teplovodní	teplovodní	k2eAgInSc4d1
kanál	kanál	k1gInSc4
pod	pod	k7c7
ulicí	ulice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kotelna	kotelna	k1gFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
několika	několik	k4yIc3
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
uměle	uměle	k6eAd1
vytvořeném	vytvořený	k2eAgInSc6d1
náspu	násep	k1gInSc6
s	s	k7c7
tovární	tovární	k2eAgFnSc7d1
vlečkou	vlečka	k1gFnSc7
zajížděly	zajíždět	k5eAaImAgInP
vagony	vagon	k1gInPc1
s	s	k7c7
uhlím	uhlí	k1gNnSc7
v	v	k7c6
úrovni	úroveň	k1gFnSc6
třetího	třetí	k4xOgNnSc2
nadzemního	nadzemní	k2eAgNnSc2d1
podlaží	podlaží	k1gNnSc2
přímo	přímo	k6eAd1
do	do	k7c2
objektu	objekt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
přes	přes	k7c4
mohutné	mohutný	k2eAgFnPc4d1
železobetonové	železobetonový	k2eAgFnPc4d1
násypky	násypka	k1gFnPc4
umístěné	umístěný	k2eAgFnPc4d1
pod	pod	k7c7
vagony	vagon	k1gInPc7
bylo	být	k5eAaImAgNnS
uhlí	uhlí	k1gNnSc1
dopravováno	dopravovat	k5eAaImNgNnS
po	po	k7c6
dopravních	dopravní	k2eAgInPc6d1
pásech	pás	k1gInPc6
až	až	k9
ke	k	k7c3
kotlům	kotel	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
budově	budova	k1gFnSc6
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
se	se	k3xPyFc4
později	pozdě	k6eAd2
nacházela	nacházet	k5eAaImAgFnS
kogenerační	kogenerační	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
a	a	k8xC
v	v	k7c4
další	další	k2eAgMnPc4d1
rozvodna	rozvodna	k1gFnSc1
vysokého	vysoký	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgInS
do	do	k7c2
areálu	areál	k1gInSc2
přiveden	přiveden	k2eAgInSc4d1
zemní	zemní	k2eAgInSc4d1
plyn	plyn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pára	pára	k1gFnSc1
vedla	vést	k5eAaImAgFnS
do	do	k7c2
továrny	továrna	k1gFnSc2
kolektorem	kolektor	k1gInSc7
umístěným	umístěný	k2eAgInSc7d1
hluboko	hluboko	k1gNnSc4
pod	pod	k7c7
Radlickou	radlický	k2eAgFnSc7d1
ulicí	ulice	k1gFnSc7
<g/>
,	,	kIx,
kouř	kouř	k1gInSc1
z	z	k7c2
plynových	plynový	k2eAgInPc2d1
kotlů	kotel	k1gInPc2
odváděl	odvádět	k5eAaImAgInS
120	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc1d1
železobetonový	železobetonový	k2eAgInSc1d1
komín	komín	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
:	:	kIx,
Továrna	továrna	k1gFnSc1
Walter	Walter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalogové	katalogový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
:	:	kIx,
1698938122	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číslo	číslo	k1gNnSc1
ÚSKP	ÚSKP	kA
<g/>
:	:	kIx,
103726	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohlášeno	prohlásit	k5eAaPmNgNnS
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
Antonín	Antonín	k1gMnSc1
Dittmayer	Dittmayer	k1gMnSc1
<g/>
:	:	kIx,
Náš	náš	k3xOp1gMnSc1
betoňák	betoňák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
WalterJinonice	WalterJinonice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
bicyklu	bicykl	k1gInSc2
k	k	k7c3
letadlovým	letadlový	k2eAgInPc3d1
motorům	motor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
jednoho	jeden	k4xCgNnSc2
století	století	k1gNnSc2
podniku	podnik	k1gInSc2
Walter	Walter	k1gMnSc1
v	v	k7c6
Praze-Jinonicích	Praze-Jinonice	k1gFnPc6
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
webcache	webcache	k6eAd1
11.12	11.12	k4
<g/>
.2019	.2019	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Antonín	Antonín	k1gMnSc1
Dittmayer	Dittmayer	k1gMnSc1
<g/>
:	:	kIx,
Iranhala	Iranhal	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
WalterJinonice	WalterJinonice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
bicyklu	bicykl	k1gInSc2
k	k	k7c3
letadlovým	letadlový	k2eAgInPc3d1
motorům	motor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
jednoho	jeden	k4xCgNnSc2
století	století	k1gNnSc2
podniku	podnik	k1gInSc2
Walter	Walter	k1gMnSc1
v	v	k7c6
Praze-Jinonicích	Praze-Jinonice	k1gFnPc6
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
webcache	webcache	k6eAd1
17.1	17.1	k4
<g/>
.2020	.2020	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Ing.	ing.	kA
Svatopluk	Svatopluk	k1gMnSc1
Zídek	Zídek	k1gMnSc1
<g/>
:	:	kIx,
Přeměna	přeměna	k1gFnSc1
kotelny	kotelna	k1gFnSc2
Walter	Walter	k1gMnSc1
na	na	k7c4
administrativní	administrativní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
iMateriály	iMateriál	k1gInPc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BERAN	Beran	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
VALCHÁŘOVÁ	VALCHÁŘOVÁ	kA
<g/>
,	,	kIx,
Vladislava	Vladislava	k1gFnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražský	pražský	k2eAgInSc1d1
industriál	industriál	k1gInSc1
<g/>
:	:	kIx,
technické	technický	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
a	a	k8xC
průmyslová	průmyslový	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Prahy	Praha	k1gFnSc2
<g/>
:	:	kIx,
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rozš	rozš	k1gMnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
<g/>
,	,	kIx,
©	©	k?
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
303	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3586	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
16	#num#	k4
<g/>
-Jihozápadní	-Jihozápadní	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
č.	č.	k?
316	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
265	#num#	k4
<g/>
-	-	kIx~
<g/>
266	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Waltrovka	waltrovka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.walterjinonice.cz/	http://www.walterjinonice.cz/	k?
</s>
<s>
Orientační	orientační	k2eAgInSc1d1
plán	plán	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
s	s	k7c7
okolím	okolí	k1gNnSc7
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
listy	list	k1gInPc1
č.	č.	k?
<g/>
47	#num#	k4
<g/>
,	,	kIx,
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ortofotomapy	Ortofotomapa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Energetická	energetický	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
Walter	Walter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Solvayovylomy	Solvayovylom	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technické	technický	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladislav	Vladislav	k1gMnSc1
Konvička	konvička	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavná	slavný	k2eAgFnSc1d1
Waltrovka	waltrovka	k1gFnSc1
mizí	mizet	k5eAaImIp3nS
<g/>
,	,	kIx,
nahradí	nahradit	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
vilky	vilka	k1gFnPc1
<g/>
,	,	kIx,
byty	byt	k1gInPc1
a	a	k8xC
budova	budova	k1gFnSc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
vrtule	vrtule	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hana	Hana	k1gFnSc1
Treutlerová	Treutlerová	k1gFnSc1
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
