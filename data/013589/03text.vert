<s>
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
318	#num#	k4
<g/>
/	/	kIx~
<g/>
A	a	k9
<g/>
319	#num#	k4
<g/>
/	/	kIx~
<g/>
A	a	k9
<g/>
320	#num#	k4
<g/>
/	/	kIx~
<g/>
A	a	k9
<g/>
321	#num#	k4
Airbus	airbus	k1gInSc1
A320-214	A320-214	k1gFnSc2
společnosti	společnost	k1gFnSc2
ČSAUrčení	ČSAUrčení	k1gNnSc2
</s>
<s>
úzkotrupý	úzkotrupý	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Airbus	airbus	k1gInSc1
První	první	k4xOgInSc1
let	léto	k1gNnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1987	#num#	k4
Zařazeno	zařazen	k2eAgNnSc4d1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1988	#num#	k4
(	(	kIx(
<g/>
Air	Air	k1gMnSc4
France	Franc	k1gMnSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Charakter	charakter	k1gInSc1
</s>
<s>
Ve	v	k7c6
službě	služba	k1gFnSc6
Uživatel	uživatel	k1gMnSc1
</s>
<s>
American	American	k1gMnSc1
AirlineseasyJetChina	AirlineseasyJetChina	k1gMnSc1
Southern	Southern	k1gMnSc1
AirlinesChina	AirlinesChina	k1gMnSc1
Eastern	Eastern	k1gMnSc1
Airlines	Airlines	k1gMnSc1
Výroba	výroba	k1gFnSc1
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
10	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
Varianty	varianta	k1gFnSc2
</s>
<s>
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
318	#num#	k4
<g/>
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
319	#num#	k4
<g/>
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
320	#num#	k4
<g/>
Airbus	airbus	k1gInSc1
A321	A321	k1gFnSc2
Další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
je	být	k5eAaImIp3nS
řada	řada	k1gFnSc1
civilních	civilní	k2eAgInPc2d1
dvoumotorových	dvoumotorový	k2eAgInPc2d1
proudových	proudový	k2eAgInPc2d1
dopravních	dopravní	k2eAgInPc2d1
letounů	letoun	k1gInPc2
s	s	k7c7
úzkým	úzký	k2eAgInSc7d1
trupem	trup	k1gInSc7
pro	pro	k7c4
krátké	krátký	k2eAgFnPc4d1
a	a	k8xC
střední	střední	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
vyráběných	vyráběný	k2eAgNnPc2d1
firmou	firma	k1gFnSc7
Airbus	airbus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A320	A320	k1gFnPc2
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
v	v	k7c6
březnu	březen	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
vzlétl	vzlétnout	k5eAaPmAgInS
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1987	#num#	k4
a	a	k8xC
v	v	k7c6
dubnu	duben	k1gInSc6
1988	#num#	k4
byl	být	k5eAaImAgInS
pravidelně	pravidelně	k6eAd1
nasazen	nasadit	k5eAaPmNgInS
společností	společnost	k1gFnSc7
Air	Air	k1gFnPc2
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
prvním	první	k4xOgInSc7
členem	člen	k1gInSc7
rodiny	rodina	k1gFnSc2
následovala	následovat	k5eAaImAgFnS
delší	dlouhý	k2eAgFnSc1d2
varianta	varianta	k1gFnSc1
A321	A321	k1gFnSc1
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
dodávka	dodávka	k1gFnSc1
v	v	k7c6
lednu	leden	k1gInSc6
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kratší	krátký	k2eAgFnSc1d2
A319	A319	k1gFnSc1
(	(	kIx(
<g/>
duben	duben	k1gInSc1
1996	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ještě	ještě	k9
kratší	krátký	k2eAgFnSc1d2
A318	A318	k1gFnSc1
(	(	kIx(
<g/>
červenec	červenec	k1gInSc1
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečná	konečný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Toulouse	Toulouse	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Hamburku	Hamburk	k1gInSc6
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Tchien-ťinu	Tchien-ťin	k2eAgFnSc4d1
v	v	k7c6
Číně	Čína	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
a	a	k8xC
od	od	k7c2
dubna	duben	k1gInSc2
2016	#num#	k4
v	v	k7c6
Mobile	mobile	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
státě	stát	k1gInSc6
Alabama	Alabamum	k1gNnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
šest	šest	k4xCc1
míst	místo	k1gNnPc2
vedle	vedle	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
rozdělených	rozdělený	k2eAgInPc2d1
jednou	jeden	k4xCgFnSc7
uličkou	ulička	k1gFnSc7
a	a	k8xC
je	být	k5eAaImIp3nS
poháněn	pohánět	k5eAaImNgInS
dvouproudovými	dvouproudový	k2eAgInPc7d1
motory	motor	k1gInPc7
CFM56	CFM56	k1gFnSc2
nebo	nebo	k8xC
IAE	IAE	kA
V2500	V2500	k1gFnSc1
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
typu	typ	k1gInSc2
A318	A318	k1gFnSc2
poháněného	poháněný	k2eAgInSc2d1
motory	motor	k1gInPc4
CFM56	CFM56	k1gFnSc1
nebo	nebo	k8xC
PW	PW	kA
<g/>
6000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Série	série	k1gFnSc1
byla	být	k5eAaImAgFnS
průkopníkem	průkopník	k1gMnSc7
jako	jako	k9
první	první	k4xOgFnSc2
civilní	civilní	k2eAgFnSc2d1
dopravní	dopravní	k2eAgFnSc2d1
letoun	letoun	k1gInSc4
s	s	k7c7
plně	plně	k6eAd1
digitálním	digitální	k2eAgInSc7d1
řídícím	řídící	k2eAgInSc7d1
systémem	systém	k1gInSc7
fly-by-wire	fly-by-wir	k1gInSc5
a	a	k8xC
první	první	k4xOgFnSc4
s	s	k7c7
tzv.	tzv.	kA
přirozenou	přirozený	k2eAgFnSc7d1
nestabilitou	nestabilita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jako	jako	k8xC,k8xS
první	první	k4xOgInSc1
dopravní	dopravní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
používá	používat	k5eAaImIp3nS
boční	boční	k2eAgNnPc4d1
side-sticky	side-sticky	k6eAd1
místo	místo	k7c2
tradičních	tradiční	k2eAgFnPc2d1
řídicích	řídicí	k2eAgFnPc2d1
pák	páka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varianty	varianta	k1gFnPc1
nabízejí	nabízet	k5eAaImIp3nP
maximální	maximální	k2eAgFnSc4d1
vzletovou	vzletový	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
od	od	k7c2
68	#num#	k4
do	do	k7c2
93,5	93,5	k4
t	t	k?
(	(	kIx(
<g/>
150	#num#	k4
000	#num#	k4
až	až	k9
206	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pokryly	pokrýt	k5eAaPmAgFnP
dolet	dolet	k1gInSc4
5	#num#	k4
740	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
940	#num#	k4
km	km	kA
(	(	kIx(
<g/>
3	#num#	k4
100	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
750	#num#	k4
nmi	nmi	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
31,4	31,4	k4
m	m	kA
(	(	kIx(
<g/>
103	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
dlouhý	dlouhý	k2eAgMnSc1d1
A318	A318	k1gMnSc1
obvykle	obvykle	k6eAd1
pojme	pojmout	k5eAaPmIp3nS
107	#num#	k4
až	až	k9
132	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A319	A319	k1gFnSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
124	#num#	k4
<g/>
–	–	k?
<g/>
156	#num#	k4
míst	místo	k1gNnPc2
je	být	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
33,8	33,8	k4
m	m	kA
(	(	kIx(
<g/>
111	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A320	A320	k?
je	být	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
37,6	37,6	k4
m	m	kA
(	(	kIx(
<g/>
123	#num#	k4
ft	ft	kA
<g/>
)	)	kIx)
a	a	k8xC
pojme	pojmout	k5eAaPmIp3nS
150	#num#	k4
až	až	k9
186	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
44,5	44,5	k4
m	m	kA
(	(	kIx(
<g/>
146	#num#	k4
ft	ft	kA
<g/>
)	)	kIx)
dlouhý	dlouhý	k2eAgMnSc1d1
A321	A321	k?
nabízí	nabízet	k5eAaImIp3nS
185	#num#	k4
až	až	k9
230	#num#	k4
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
business	business	k1gInSc4
jety	jet	k2eAgMnPc4d1
známé	známý	k1gMnPc4
jako	jako	k8xC,k8xS
Airbus	airbus	k1gInSc4
Corporate	Corporat	k1gInSc5
Jets	Jets	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2010	#num#	k4
společnost	společnost	k1gFnSc1
Airbus	airbus	k1gInSc1
ohlásila	ohlásit	k5eAaPmAgFnS
vylepšený	vylepšený	k2eAgInSc4d1
model	model	k1gInSc4
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
(	(	kIx(
<g/>
new	new	k?
engine	enginout	k5eAaPmIp3nS
option	option	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
první	první	k4xOgInSc1
let	let	k1gInSc1
vykonal	vykonat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
a	a	k8xC
do	do	k7c2
služby	služba	k1gFnSc2
u	u	k7c2
společnosti	společnost	k1gFnSc2
Lufthansa	Lufthans	k1gMnSc2
vstoupil	vstoupit	k5eAaPmAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
účinnějším	účinný	k2eAgInPc3d2
motorům	motor	k1gInPc3
a	a	k8xC
vylepšením	vylepšení	k1gNnSc7
včetně	včetně	k7c2
sharkletů	sharklet	k1gInPc2
nabízí	nabízet	k5eAaImIp3nS
až	až	k9
o	o	k7c4
15	#num#	k4
<g/>
%	%	kIx~
nižší	nízký	k2eAgFnSc2d2
spotřebu	spotřeba	k1gFnSc4
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgFnSc1d1
A320	A320	k1gFnSc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
A	a	k9
<g/>
320	#num#	k4
<g/>
ceo	ceo	k?
(	(	kIx(
<g/>
current	current	k1gMnSc1
engine	enginout	k5eAaPmIp3nS
option	option	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2019	#num#	k4
překonal	překonat	k5eAaPmAgInS
Boeing	boeing	k1gInSc4
737	#num#	k4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
nejprodávanějším	prodávaný	k2eAgNnSc7d3
dopravním	dopravní	k2eAgNnSc7d1
letadlem	letadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prosinci	prosinec	k1gInSc3
2019	#num#	k4
bylo	být	k5eAaImAgNnS
dodáno	dodat	k5eAaPmNgNnS
celkem	celkem	k6eAd1
9	#num#	k4
247	#num#	k4
letadel	letadlo	k1gNnPc2
více	hodně	k6eAd2
než	než	k8xS
330	#num#	k4
provozovatelům	provozovatel	k1gMnPc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
nízkonákladových	nízkonákladový	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
<g/>
.	.	kIx.
8	#num#	k4
796	#num#	k4
letadel	letadlo	k1gNnPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Airlines	Airlines	k1gMnSc1
byl	být	k5eAaImAgMnS
největším	veliký	k2eAgMnSc7d3
operátorem	operátor	k1gMnSc7
se	s	k7c7
412	#num#	k4
letadly	letadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevyřízené	vyřízený	k2eNgFnPc4d1
objednávky	objednávka	k1gFnPc4
byly	být	k5eAaImAgFnP
na	na	k7c6
čísle	číslo	k1gNnSc6
6	#num#	k4
068	#num#	k4
ks	ks	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
dohromady	dohromady	k6eAd1
dělá	dělat	k5eAaImIp3nS
15	#num#	k4
315	#num#	k4
objednávek	objednávka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
<g/>
320	#num#	k4
<g/>
ceo	ceo	k?
zpočátku	zpočátku	k6eAd1
konkuroval	konkurovat	k5eAaImAgInS
typům	typ	k1gInPc3
MD-80	MD-80	k1gFnPc2
a	a	k8xC
Boeing	boeing	k1gInSc4
737	#num#	k4
Classic	Classice	k1gFnPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
MD-90	MD-90	k1gFnSc4
a	a	k8xC
Boeing	boeing	k1gInSc4
737	#num#	k4
Next	Next	k2eAgInSc4d1
Generation	Generation	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Boeing	boeing	k1gInSc4
737	#num#	k4
MAX	Max	k1gMnSc1
je	být	k5eAaImIp3nS
odpovědí	odpověď	k1gFnSc7
na	na	k7c4
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
A320-100	A320-100	k4
Air	Air	k1gMnSc1
Inter	Inter	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
několika	několik	k4yIc2
málo	málo	k6eAd1
A320-100	A320-100	k1gFnPc2
</s>
<s>
Prototyp	prototyp	k1gInSc1
Airbusu	airbus	k1gInSc2
A320	A320	k1gFnPc2
v	v	k7c6
září	září	k1gNnSc6
1988	#num#	k4
<g/>
,	,	kIx,
upravený	upravený	k2eAgInSc1d1
jako	jako	k8xS,k8xC
A320-200	A320-200	k1gFnSc1
</s>
<s>
Po	po	k7c6
úspěchu	úspěch	k1gInSc6
Airbusu	airbus	k1gInSc2
A300	A300	k1gFnSc2
začal	začít	k5eAaPmAgInS
Airbus	airbus	k1gInSc1
vyvíjet	vyvíjet	k5eAaImF
náhradu	náhrada	k1gFnSc4
za	za	k7c4
tehdy	tehdy	k6eAd1
nejpopulárnější	populární	k2eAgInSc4d3
dopravní	dopravní	k2eAgInSc4d1
letoun	letoun	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
–	–	k?
Boeing	boeing	k1gInSc1
727	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Airbus	airbus	k1gInSc1
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
stejnou	stejný	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nižší	nízký	k2eAgInPc1d2
provozní	provozní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
a	a	k8xC
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
několika	několik	k4yIc6
verzích	verze	k1gFnPc6
pro	pro	k7c4
různý	různý	k2eAgInSc4d1
počet	počet	k1gInSc4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
použité	použitý	k2eAgFnPc4d1
u	u	k7c2
A320	A320	k1gFnSc2
byly	být	k5eAaImAgInP
symbolem	symbol	k1gInSc7
dvougeneračního	dvougenerační	k2eAgInSc2d1
technického	technický	k2eAgInSc2d1
skoku	skok	k1gInSc2
oproti	oproti	k7c3
plně	plně	k6eAd1
analogovému	analogový	k2eAgInSc3d1
Boeingu	boeing	k1gInSc3
727	#num#	k4
a	a	k8xC
generačního	generační	k2eAgInSc2d1
vůči	vůči	k7c3
Boeingům	boeing	k1gInPc3
737	#num#	k4
řad	řada	k1gFnPc2
-300	-300	k4
<g/>
/	/	kIx~
<g/>
400	#num#	k4
<g/>
/	/	kIx~
<g/>
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A320	A320	k1gMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
celosvětovou	celosvětový	k2eAgFnSc7d1
náhradou	náhrada	k1gFnSc7
za	za	k7c4
727	#num#	k4
a	a	k8xC
nejstarší	starý	k2eAgFnPc1d3
varianty	varianta	k1gFnPc1
737	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
nárůstech	nárůst	k1gInPc6
cen	cena	k1gFnPc2
ropy	ropa	k1gFnSc2
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
potřeboval	potřebovat	k5eAaImAgInS
Airbus	airbus	k1gInSc1
minimalizovat	minimalizovat	k5eAaBmF
náklady	náklad	k1gInPc4
na	na	k7c4
palivo	palivo	k1gNnSc4
u	u	k7c2
A	A	kA
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
toho	ten	k3xDgMnSc4
dosáhl	dosáhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
použil	použít	k5eAaPmAgMnS
u	u	k7c2
svého	svůj	k3xOyFgInSc2
nového	nový	k2eAgInSc2d1
typu	typ	k1gInSc2
množství	množství	k1gNnSc2
nejmodernějších	moderní	k2eAgFnPc2d3
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
systém	systém	k1gInSc1
řízení	řízení	k1gNnSc2
letu	let	k1gInSc2
fly-by-wire	fly-by-wir	k1gInSc5
<g/>
,	,	kIx,
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
použití	použití	k1gNnSc1
kompozitních	kompozitní	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
změnu	změna	k1gFnSc4
těžiště	těžiště	k1gNnSc2
letounu	letoun	k1gInSc2
pomocí	pomocí	k7c2
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
skleněný	skleněný	k2eAgInSc4d1
kokpit	kokpit	k1gInSc4
a	a	k8xC
dvoučlennou	dvoučlenný	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
těmto	tento	k3xDgNnPc3
vylepšením	vylepšení	k1gNnSc7
dosáhl	dosáhnout	k5eAaPmAgInS
A320	A320	k1gFnSc4
o	o	k7c4
50	#num#	k4
<g/>
%	%	kIx~
nižší	nízký	k2eAgFnSc2d2
spotřeby	spotřeba	k1gFnSc2
než	než	k8xS
Boeing	boeing	k1gInSc4
727	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
A320	A320	k1gFnSc2
jsou	být	k5eAaImIp3nP
používány	používán	k2eAgInPc1d1
motory	motor	k1gInPc1
dvou	dva	k4xCgMnPc2
dodavatelů	dodavatel	k1gMnPc2
<g/>
:	:	kIx,
CFM	CFM	kA
International	International	k1gMnSc1
CFM56	CFM56	k1gMnSc1
a	a	k8xC
International	International	k1gMnSc1
Aero	aero	k1gNnSc1
Engines	Engines	k1gMnSc1
V	v	k7c6
<g/>
2500	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
A320	A320	k4
má	mít	k5eAaImIp3nS
počítačový	počítačový	k2eAgInSc1d1
palubní	palubní	k2eAgInSc1d1
systém	systém	k1gInSc1
údržby	údržba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systémy	systém	k1gInPc1
avioniky	avionika	k1gFnSc2
jsou	být	k5eAaImIp3nP
navrženy	navrhnout	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
je	on	k3xPp3gNnSc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
snadno	snadno	k6eAd1
modernizovat	modernizovat	k5eAaBmF
na	na	k7c6
novější	nový	k2eAgFnSc6d2
verzi	verze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
úplně	úplně	k6eAd1
prvních	první	k4xOgFnPc2
A320	A320	k1gFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
většina	většina	k1gFnSc1
strojů	stroj	k1gInPc2
modernizována	modernizován	k2eAgFnSc1d1
na	na	k7c4
nejnovější	nový	k2eAgInSc4d3
standard	standard	k1gInSc4
avioniky	avionika	k1gFnSc2
a	a	k8xC
být	být	k5eAaImF
tak	tak	k6eAd1
moderní	moderní	k2eAgInSc1d1
i	i	k9
po	po	k7c6
dvaceti	dvacet	k4xCc6
letech	léto	k1gNnPc6
ve	v	k7c6
službě	služba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pilotní	pilotní	k2eAgFnSc1d1
kabina	kabina	k1gFnSc1
je	být	k5eAaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
Elektronickým	elektronický	k2eAgInSc7d1
letovým	letový	k2eAgInSc7d1
informačním	informační	k2eAgInSc7d1
systémem	systém	k1gInSc7
EFIS	EFIS	kA
a	a	k8xC
bočními	boční	k2eAgFnPc7d1
„	„	k?
<g/>
sidesticky	sidesticky	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
což	což	k3yRnSc4,k3yQnSc4
jsou	být	k5eAaImIp3nP
laicky	laicky	k6eAd1
řečeno	říct	k5eAaPmNgNnS
joysticky	joysticky	k6eAd1
–	–	k?
místo	místo	k7c2
klasických	klasický	k2eAgInPc2d1
řídících	řídící	k2eAgInPc2d1
sloupků	sloupek	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chování	chování	k1gNnSc1
systému	systém	k1gInSc2
fly-by-wire	fly-by-wir	k1gInSc5
(	(	kIx(
<g/>
vybaveného	vybavený	k2eAgInSc2d1
ochranou	ochrana	k1gFnSc7
proti	proti	k7c3
překročení	překročení	k1gNnSc3
letové	letový	k2eAgFnSc2d1
obálky	obálka	k1gFnSc2
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
tehdy	tehdy	k6eAd1
(	(	kIx(
<g/>
na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
)	)	kIx)
zcela	zcela	k6eAd1
novou	nový	k2eAgFnSc7d1
zkušeností	zkušenost	k1gFnSc7
pro	pro	k7c4
mnoho	mnoho	k4c4
pilotů	pilot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mohlo	moct	k5eAaImAgNnS
přispět	přispět	k5eAaPmF
k	k	k7c3
některým	některý	k3yIgFnPc3
tehdejším	tehdejší	k2eAgFnPc3d1
nehodám	nehoda	k1gFnPc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
nehody	nehoda	k1gFnSc2
letu	let	k1gInSc2
296	#num#	k4
Air	Air	k1gMnSc2
France	Franc	k1gMnSc2
během	během	k7c2
letecké	letecký	k2eAgFnSc2d1
přehlídky	přehlídka	k1gFnSc2
v	v	k7c6
Habsheimu	Habsheimo	k1gNnSc6
s	s	k7c7
3	#num#	k4
mrtvými	mrtvý	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nehoda	nehoda	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
pilot	pilot	k1gMnSc1
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
nízký	nízký	k2eAgInSc4d1
průlet	průlet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřování	vyšetřování	k1gNnSc1
nehody	nehoda	k1gFnSc2
bylo	být	k5eAaImAgNnS
ztěžováno	ztěžovat	k5eAaImNgNnS
zásahy	zásah	k1gInPc7
zaměstnanců	zaměstnanec	k1gMnPc2
Airbusu	airbus	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
manipulace	manipulace	k1gFnSc2
s	s	k7c7
důkazy	důkaz	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečlivý	pečlivý	k2eAgInSc4d1
pilotní	pilotní	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
a	a	k8xC
úpravy	úprava	k1gFnPc4
systému	systém	k1gInSc2
fly-by-wire	fly-by-wir	k1gInSc5
velmi	velmi	k6eAd1
omezily	omezit	k5eAaPmAgFnP
podobné	podobný	k2eAgInPc4d1
incidenty	incident	k1gInPc4
<g/>
;	;	kIx,
A320	A320	k1gFnPc4
má	mít	k5eAaImIp3nS
dnes	dnes	k6eAd1
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgInPc1d1
záznamy	záznam	k1gInPc1
o	o	k7c6
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
</s>
<s>
Kokpit	kokpit	k1gInSc1
A321	A321	k1gFnSc2
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
i	i	k9
pro	pro	k7c4
A	A	kA
<g/>
318	#num#	k4
<g/>
,	,	kIx,
A319	A319	k1gFnSc1
a	a	k8xC
A	A	kA
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
uspořádání	uspořádání	k1gNnSc1
bylo	být	k5eAaImAgNnS
začleněno	začlenit	k5eAaPmNgNnS
i	i	k9
do	do	k7c2
A	A	kA
<g/>
330	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
340	#num#	k4
<g/>
,	,	kIx,
A350	A350	k1gFnSc1
a	a	k8xC
A	A	kA
<g/>
380	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
„	„	k?
<g/>
standardizace	standardizace	k1gFnSc1
<g/>
“	“	k?
umožňuje	umožňovat	k5eAaImIp3nS
pilotům	pilot	k1gMnPc3
rychlý	rychlý	k2eAgInSc4d1
přechod	přechod	k1gInSc4
mezi	mezi	k7c7
typy	typ	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1988	#num#	k4
byl	být	k5eAaImAgInS
typu	typ	k1gInSc3
udělen	udělen	k2eAgInSc1d1
certifikát	certifikát	k1gInSc1
od	od	k7c2
JAA	JAA	kA
a	a	k8xC
A320	A320	k1gMnSc1
mohl	moct	k5eAaImAgMnS
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c6
březnu	březen	k1gInSc6
1988	#num#	k4
u	u	k7c2
Air	Air	k1gMnSc2
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
rodina	rodina	k1gFnSc1
verzí	verze	k1gFnPc2
A320	A320	k1gFnSc1
rychle	rychle	k6eAd1
rozrůstala	rozrůstat	k5eAaImAgFnS
<g/>
:	:	kIx,
výroba	výroba	k1gFnSc1
185	#num#	k4
<g/>
místného	místný	k2eAgInSc2d1
A321	A321	k1gMnSc2
byla	být	k5eAaImAgNnP
zahájena	zahájit	k5eAaPmNgNnP
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
,	,	kIx,
124	#num#	k4
<g/>
místného	místný	k2eAgNnSc2d1
A319	A319	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
a	a	k8xC
107	#num#	k4
<g/>
místného	místný	k2eAgNnSc2d1
A318	A318	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
letouny	letoun	k1gInPc7
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
má	mít	k5eAaImIp3nS
A320	A320	k1gMnSc1
širší	široký	k2eAgFnSc4d2
kabinu	kabina	k1gFnSc4
<g/>
,	,	kIx,
větší	veliký	k2eAgInSc4d2
zavazadlový	zavazadlový	k2eAgInSc4d1
prostor	prostor	k1gInSc4
v	v	k7c6
nadhlavní	nadhlavní	k2eAgFnSc6d1
části	část	k1gFnSc6
a	a	k8xC
plně	plně	k6eAd1
digitální	digitální	k2eAgInSc4d1
fly-by-wire	fly-by-wir	k1gInSc5
řídící	řídící	k2eAgFnSc2d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
má	mít	k5eAaImIp3nS
letoun	letoun	k1gInSc1
značný	značný	k2eAgInSc1d1
nákladový	nákladový	k2eAgInSc1d1
prostor	prostor	k1gInSc1
s	s	k7c7
rozměrnými	rozměrný	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
usnadňují	usnadňovat	k5eAaImIp3nP
nakládku	nakládka	k1gFnSc4
a	a	k8xC
vykládku	vykládka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
těmto	tento	k3xDgFnPc3
vlastnostem	vlastnost	k1gFnPc3
si	se	k3xPyFc3
A320	A320	k1gFnPc1
objednaly	objednat	k5eAaPmAgFnP
letecké	letecký	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
jako	jako	k8xS,k8xC
Northwest	Northwest	k1gMnSc1
Airlines	Airlines	k1gMnSc1
(	(	kIx(
<g/>
první	první	k4xOgMnSc1
zákazník	zákazník	k1gMnSc1
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
United	United	k1gMnSc1
Airlines	Airlines	k1gMnSc1
a	a	k8xC
British	British	k1gMnSc1
Airways	Airwaysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nízké	nízký	k2eAgInPc1d1
náklady	náklad	k1gInPc1
na	na	k7c4
provoz	provoz	k1gInSc4
a	a	k8xC
údržbu	údržba	k1gFnSc4
jsou	být	k5eAaImIp3nP
magnetem	magnet	k1gInSc7
pro	pro	k7c4
nízkonákladové	nízkonákladový	k2eAgMnPc4d1
dopravce	dopravce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Jet	jet	k2eAgInSc4d1
Blue	Blue	k1gInSc4
si	se	k3xPyFc3
objednal	objednat	k5eAaPmAgMnS
až	až	k9
233	#num#	k4
strojů	stroj	k1gInPc2
z	z	k7c2
rodiny	rodina	k1gFnSc2
A320	A320	k1gFnSc2
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
flotilu	flotila	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgMnPc4d1
nízkonákladové	nízkonákladový	k2eAgMnPc4d1
dopravce	dopravce	k1gMnPc4
s	s	k7c7
významnými	významný	k2eAgFnPc7d1
objednávkami	objednávka	k1gFnPc7
patří	patřit	k5eAaImIp3nS
easyJet	easyJet	k1gInSc1
a	a	k8xC
AirAsia	AirAsia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Už	už	k6eAd1
skoro	skoro	k6eAd1
dvě	dva	k4xCgNnPc4
desetiletí	desetiletí	k1gNnPc4
stroje	stroj	k1gInSc2
rodiny	rodina	k1gFnSc2
A320	A320	k1gFnPc2
soupeří	soupeřit	k5eAaImIp3nS
na	na	k7c6
trhu	trh	k1gInSc6
s	s	k7c7
konkurencí	konkurence	k1gFnSc7
letounů	letoun	k1gInPc2
Boeing	boeing	k1gInSc4
737	#num#	k4
Classics	Classicsa	k1gFnPc2
(	(	kIx(
<g/>
-	-	kIx~
<g/>
300	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
400	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
500	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Boeing	boeing	k1gInSc4
737	#num#	k4
Next-Generation	Next-Generation	k1gInSc4
(	(	kIx(
<g/>
-	-	kIx~
<g/>
600	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
700	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
800	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
900	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Boeing	boeing	k1gInSc4
757	#num#	k4
<g/>
,	,	kIx,
Boeing	boeing	k1gInSc1
717	#num#	k4
<g/>
,	,	kIx,
McDonnell	McDonnell	k1gMnSc1
Douglas	Douglas	k1gMnSc1
MD-80	MD-80	k1gMnSc1
a	a	k8xC
McDonnell	McDonnell	k1gMnSc1
Douglas	Douglas	k1gMnSc1
MD-	MD-	k1gMnSc1
<g/>
90	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Airbus	airbus	k1gInSc1
A319	A319	k1gFnSc2
používá	používat	k5eAaImIp3nS
Escadron	Escadron	k1gInSc1
de	de	k?
transport	transport	k1gInSc1
<g/>
,	,	kIx,
d	d	k?
<g/>
'	'	kIx"
<g/>
entraînement	entraînement	k1gMnSc1
et	et	k?
de	de	k?
calibrage	calibragat	k5eAaPmIp3nS
–	–	k?
letka	letka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
starosti	starost	k1gFnSc6
přepravu	přeprava	k1gFnSc4
oficiálních	oficiální	k2eAgMnPc2d1
francouzských	francouzský	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Airbus	airbus	k1gInSc1
A319CJ	A319CJ	k1gFnSc2
v	v	k7c6
počtu	počet	k1gInSc6
2	#num#	k4
kusů	kus	k1gInPc2
používá	používat	k5eAaImIp3nS
rovněž	rovněž	k9
241	#num#	k4
<g/>
.	.	kIx.
dopravní	dopravní	k2eAgFnSc1d1
letka	letka	k1gFnSc1
Vzdušných	vzdušný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Armády	armáda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
zajišťující	zajišťující	k2eAgInSc4d1
transport	transport	k1gInSc4
vojáků	voják	k1gMnPc2
a	a	k8xC
oficiálních	oficiální	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
české	český	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
</s>
<s>
První	první	k4xOgFnPc1
úvahy	úvaha	k1gFnPc1
o	o	k7c4
zařazení	zařazení	k1gNnSc4
letadel	letadlo	k1gNnPc2
rodiny	rodina	k1gFnSc2
A320	A320	k1gFnSc2
do	do	k7c2
flotily	flotila	k1gFnSc2
ČSA	ČSA	kA
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
krátce	krátce	k6eAd1
po	po	k7c6
Sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
potřebou	potřeba	k1gFnSc7
obnovy	obnova	k1gFnSc2
a	a	k8xC
modernizace	modernizace	k1gFnSc2
flotily	flotila	k1gFnSc2
na	na	k7c6
středně	středně	k6eAd1
dlouhých	dlouhý	k2eAgFnPc6d1
a	a	k8xC
krátkých	krátký	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
byly	být	k5eAaImAgFnP
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
obsluhovány	obsluhován	k2eAgInPc1d1
sovětskými	sovětský	k2eAgInPc7d1
Tupolevy	Tupolev	k1gInPc7
134	#num#	k4
a	a	k8xC
Tupolevy	Tupoleva	k1gFnPc1
154	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
bylo	být	k5eAaImAgNnS
uvažováno	uvažován	k2eAgNnSc1d1
o	o	k7c6
dlouhodobém	dlouhodobý	k2eAgInSc6d1
leasingu	leasing	k1gInSc6
5	#num#	k4
letadel	letadlo	k1gNnPc2
A	a	k8xC
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
opuštěn	opustit	k5eAaPmNgInS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byly	být	k5eAaImAgFnP
jako	jako	k8xS,k8xC
náhrada	náhrada	k1gFnSc1
Tupolevů	Tupolev	k1gInPc2
vybrány	vybrán	k2eAgInPc4d1
americké	americký	k2eAgInPc4d1
Boeingy	boeing	k1gInPc4
737	#num#	k4
verzí	verze	k1gFnPc2
-400	-400	k4
a	a	k8xC
-500	-500	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbusy	airbus	k1gInPc4
A320	A320	k1gFnSc3
family	famila	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
minimálně	minimálně	k6eAd1
na	na	k7c6
letišti	letiště	k1gNnSc6
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2
objevovaly	objevovat	k5eAaImAgFnP
a	a	k8xC
stále	stále	k6eAd1
objevují	objevovat	k5eAaImIp3nP
na	na	k7c6
pravidelných	pravidelný	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
zahraničních	zahraniční	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Lufthansa	Lufthans	k1gMnSc4
nebo	nebo	k8xC
Air	Air	k1gMnSc4
France	Franc	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
vypsaly	vypsat	k5eAaPmAgFnP
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
výběrové	výběrový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
na	na	k7c4
12	#num#	k4
nových	nový	k2eAgNnPc2d1
středně	středně	k6eAd1
dálkových	dálkový	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
nahradila	nahradit	k5eAaPmAgFnS
zastarávající	zastarávající	k2eAgInPc4d1
Boeingy	boeing	k1gInPc4
737	#num#	k4
Classic	Classice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc1
nabídl	nabídnout	k5eAaPmAgInS
další	další	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
modelu	model	k1gInSc2
737	#num#	k4
–	–	k?
Next	Next	k2eAgInSc4d1
Generation	Generation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízel	nabízet	k5eAaImAgMnS
6	#num#	k4
Boeingů	boeing	k1gInPc2
737-700	737-700	k4
(	(	kIx(
<g/>
kratší	krátký	k2eAgFnSc1d2
verze	verze	k1gFnSc1
pro	pro	k7c4
cca	cca	kA
150	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
stejný	stejný	k2eAgInSc1d1
počet	počet	k1gInSc1
Boeingů	boeing	k1gInPc2
737-800	737-800	k4
(	(	kIx(
<g/>
delší	dlouhý	k2eAgFnSc1d2
verze	verze	k1gFnSc1
pro	pro	k7c4
189	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
do	do	k7c2
soutěže	soutěž	k1gFnSc2
nabídl	nabídnout	k5eAaPmAgMnS
modely	model	k1gInPc4
A319	A319	k1gMnPc2
(	(	kIx(
<g/>
144	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
A320	A320	k1gMnSc1
(	(	kIx(
<g/>
180	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabídla	nabídnout	k5eAaPmAgFnS
Airbusu	airbus	k1gInSc2
vycházela	vycházet	k5eAaImAgNnP
celkově	celkově	k6eAd1
výhodněji	výhodně	k6eAd2
a	a	k8xC
v	v	k7c6
říjnu	říjen	k1gInSc6
2004	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
podepsání	podepsání	k1gNnSc3
smlouvy	smlouva	k1gFnSc2
na	na	k7c4
12	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
i	i	k8xC
dodávku	dodávka	k1gFnSc4
2	#num#	k4
náhradních	náhradní	k2eAgInPc2d1
motorů	motor	k1gInPc2
CFM	CFM	kA
56	#num#	k4
<g/>
,	,	kIx,
náhradní	náhradní	k2eAgInPc4d1
díly	díl	k1gInPc4
a	a	k8xC
zajištění	zajištění	k1gNnSc4
výcviku	výcvik	k1gInSc2
posádek	posádka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
v	v	k7c6
barvách	barva	k1gFnPc6
ČSA	ČSA	kA
přiletěl	přiletět	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesl	nést	k5eAaImAgMnS
registraci	registrace	k1gFnSc4
OK-GEA	OK-GEA	k1gMnSc1
a	a	k8xC
pojmenován	pojmenován	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
"	"	kIx"
<g/>
Rožnov	Rožnov	k1gInSc1
pod	pod	k7c7
Radhoštěm	Radhošť	k1gInSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgInS
do	do	k7c2
výcvikových	výcvikový	k2eAgInPc2d1
letů	let	k1gInPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
i	i	k9
do	do	k7c2
běžných	běžný	k2eAgInPc2d1
obchodních	obchodní	k2eAgInPc2d1
letů	let	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
rozrostla	rozrůst	k5eAaPmAgFnS
o	o	k7c4
další	další	k2eAgNnSc4d1
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
registrace	registrace	k1gFnSc1
OK-GEB	OK-GEB	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
pojmenován	pojmenován	k2eAgMnSc1d1
"	"	kIx"
<g/>
Strakonice	Strakonice	k1gFnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgNnPc4
letadla	letadlo	k1gNnPc4
již	již	k9
sloužila	sloužit	k5eAaImAgFnS
u	u	k7c2
jiných	jiný	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
a	a	k8xC
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2005	#num#	k4
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
o	o	k7c4
2	#num#	k4
Airbusy	airbus	k1gInPc4
A321-200	A321-200	k1gMnSc1
s	s	k7c7
registrací	registrace	k1gFnSc7
OK-CED	OK-CED	k1gFnSc2
a	a	k8xC
OK-CEC	OK-CEC	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tato	tento	k3xDgNnPc1
letadla	letadlo	k1gNnPc1
sloužila	sloužit	k5eAaImAgNnP
již	již	k6eAd1
dříve	dříve	k6eAd2
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
"	"	kIx"
<g/>
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
OK-CEC	OK-CEC	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
Nové	Nové	k2eAgNnSc1d1
město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
OK-CED	OK-CED	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
dodávky	dodávka	k1gFnPc1
nově	nově	k6eAd1
vyrobených	vyrobený	k2eAgFnPc2d1
A320	A320	k1gFnPc2
přímo	přímo	k6eAd1
od	od	k7c2
Airbusu	airbus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
letadla	letadlo	k1gNnPc4
registrací	registrace	k1gFnPc2
OK-LEE	OK-LEE	k1gFnSc1
(	(	kIx(
<g/>
dodaný	dodaný	k2eAgInSc1d1
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-LEF	OK-LEF	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-LEG	OK-LEG	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byly	být	k5eAaImAgFnP
dodány	dodán	k2eAgFnPc1d1
zbývající	zbývající	k2eAgFnPc1d1
A320	A320	k1gFnPc1
a	a	k8xC
zahájeny	zahájen	k2eAgFnPc1d1
dodávky	dodávka	k1gFnPc1
menších	malý	k2eAgNnPc2d2
A	a	k8xC
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
stroje	stroj	k1gInPc4
OK-MEH	OK-MEH	k1gMnPc2
(	(	kIx(
<g/>
dodaný	dodaný	k2eAgInSc1d1
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-MEK	OK-MEK	k1gMnSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
dodaný	dodaný	k2eAgMnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-MEI	OK-MEI	k1gMnSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
dodaný	dodaný	k2eAgMnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-MEJ	OK-MEJ	k1gMnSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
dodaný	dodaný	k2eAgMnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
OK-MEL	OK-MEL	k1gMnSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
dodaný	dodaný	k2eAgMnSc1d1
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
2008	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
byly	být	k5eAaImAgFnP
dodány	dodat	k5eAaPmNgInP
další	další	k2eAgInPc1d1
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
jejichž	jejichž	k3xOyRp3gInSc1
počet	počet	k1gInSc1
se	se	k3xPyFc4
zvýšil	zvýšit	k5eAaPmAgInS
ze	z	k7c2
6	#num#	k4
na	na	k7c4
celkem	celkem	k6eAd1
9	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
stroje	stroj	k1gInPc1
registrací	registrace	k1gFnPc2
OK-NEM	OK-NEM	k1gFnPc2
(	(	kIx(
<g/>
první	první	k4xOgInSc1
stroj	stroj	k1gInSc1
dodaný	dodaný	k2eAgInSc1d1
v	v	k7c6
nových	nový	k2eAgInPc6d1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
používaných	používaný	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-NEN	OK-NEN	k1gMnSc1
<g/>
,	,	kIx,
OK-NEO	OK-NEO	k1gMnSc1
<g/>
,	,	kIx,
OK-NEP	OK-NEP	k1gMnSc1
(	(	kIx(
<g/>
dodané	dodaný	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-OER	OK-OER	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OK-PET	OK-PET	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
OK-REQ	OK-REQ	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Všechna	všechen	k3xTgNnPc1
letadla	letadlo	k1gNnPc1
se	se	k3xPyFc4
zapojila	zapojit	k5eAaPmAgFnS
do	do	k7c2
služby	služba	k1gFnSc2
po	po	k7c6
boku	bok	k1gInSc6
Boeingů	boeing	k1gInPc2
737-400	737-400	k4
a	a	k8xC
-500	-500	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
postupně	postupně	k6eAd1
ze	z	k7c2
služby	služba	k1gFnSc2
stahovány	stahovat	k5eAaImNgFnP
a	a	k8xC
podíl	podíl	k1gInSc1
Airbusů	airbus	k1gInPc2
ve	v	k7c6
flotile	flotila	k1gFnSc6
ČSA	ČSA	kA
rostl	růst	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
letadel	letadlo	k1gNnPc2
A320	A320	k1gMnPc2
family	famila	k1gFnSc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
ve	v	k7c6
flotile	flotila	k1gFnSc6
nacházelo	nacházet	k5eAaImAgNnS
19	#num#	k4
<g/>
:	:	kIx,
9	#num#	k4
<g/>
×	×	k?
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
×	×	k?
A320	A320	k1gMnPc2
a	a	k8xC
2	#num#	k4
<g/>
×	×	k?
A	a	k9
<g/>
321	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
roku	rok	k1gInSc2
2012	#num#	k4
začal	začít	k5eAaPmAgInS
počet	počet	k1gInSc1
letadel	letadlo	k1gNnPc2
ve	v	k7c6
flotile	flotila	k1gFnSc6
ČSA	ČSA	kA
<g/>
,	,	kIx,
díky	díky	k7c3
špatné	špatný	k2eAgFnSc3d1
ekonomické	ekonomický	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
dopravce	dopravce	k1gMnSc1
<g/>
,	,	kIx,
klesat	klesat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
byly	být	k5eAaImAgInP
stroje	stroj	k1gInPc1
registrací	registrace	k1gFnPc2
OK-LEE	OK-LEE	k1gFnPc2
<g/>
,	,	kIx,
LEF	Lef	k1gInSc1
<g/>
,	,	kIx,
MEH	mha	k1gFnPc2
a	a	k8xC
LEG	lego	k1gNnPc2
převedeny	převést	k5eAaPmNgInP
k	k	k7c3
nově	nova	k1gFnSc3
vzniklé	vzniklý	k2eAgFnSc2d1
dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Holidays	Holidaysa	k1gFnPc2
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
připojily	připojit	k5eAaPmAgFnP
k	k	k7c3
dříve	dříve	k6eAd2
získaným	získaný	k2eAgFnPc3d1
dalším	další	k2eAgFnPc3d1
A320	A320	k1gFnPc3
registrací	registrace	k1gFnPc2
OK-HCA	OK-HCA	k1gFnPc2
a	a	k8xC
OK-HCB	OK-HCB	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
flotily	flotila	k1gFnSc2
ČSA	ČSA	kA
však	však	k9
mizela	mizet	k5eAaImAgNnP
další	další	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
2012	#num#	k4
vykonaly	vykonat	k5eAaPmAgFnP
své	svůj	k3xOyFgInPc4
poslední	poslední	k2eAgInPc4d1
lety	let	k1gInPc4
A320	A320	k1gFnSc2
registrací	registrace	k1gFnSc7
OK-GEA	OK-GEA	k1gFnSc7
a	a	k8xC
OK-GEB	OK-GEB	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadla	letadlo	k1gNnSc2
definitivně	definitivně	k6eAd1
opustila	opustit	k5eAaPmAgFnS
flotilu	flotila	k1gFnSc4
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
rovněž	rovněž	k6eAd1
vyřadila	vyřadit	k5eAaPmAgFnS
ČSA	ČSA	kA
obě	dva	k4xCgFnPc1
A	a	k9
<g/>
321	#num#	k4
<g/>
,	,	kIx,
OK-CEC	OK-CEC	k1gFnSc1
a	a	k8xC
OK-CED	OK-CED	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
nebylo	být	k5eNaImAgNnS
využití	využití	k1gNnSc1
(	(	kIx(
<g/>
charterové	charterový	k2eAgInPc1d1
lety	let	k1gInPc1
přešly	přejít	k5eAaPmAgInP
pod	pod	k7c4
Holidays	Holidays	k1gInSc4
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
rovněž	rovněž	k9
opustily	opustit	k5eAaPmAgInP
flotilu	flotila	k1gFnSc4
poslední	poslední	k2eAgInPc4d1
Boeingy	boeing	k1gInPc4
737-500	737-500	k4
a	a	k8xC
středně	středně	k6eAd1
dálková	dálkový	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
skládala	skládat	k5eAaImAgFnS
jen	jen	k6eAd1
z	z	k7c2
Airbusů	airbus	k1gInPc2
A320	A320	k1gFnSc2
family	famila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
provozovaly	provozovat	k5eAaImAgFnP
ČSA	ČSA	kA
2	#num#	k4
<g/>
×	×	k?
A320	A320	k1gMnSc1
(	(	kIx(
<g/>
OK-MEI	OK-MEI	k1gMnSc1
a	a	k8xC
OK-MEJ	OK-MEJ	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
9	#num#	k4
<g/>
×	×	k?
A	a	k9
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tento	tento	k3xDgInSc1
stav	stav	k1gInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
konečný	konečný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
rozhodnutí	rozhodnutí	k1gNnSc1
všechny	všechen	k3xTgFnPc4
zbývající	zbývající	k2eAgFnPc4d1
A320	A320	k1gFnPc4
z	z	k7c2
flotily	flotila	k1gFnSc2
vyřadit	vyřadit	k5eAaPmF
z	z	k7c2
důvodu	důvod	k1gInSc2
jejich	jejich	k3xOp3gFnSc2
ekonomické	ekonomický	k2eAgFnSc2d1
neúnosnosti	neúnosnost	k1gFnSc2
-	-	kIx~
málokdy	málokdy	k6eAd1
se	se	k3xPyFc4
je	on	k3xPp3gInPc4
podařilo	podařit	k5eAaPmAgNnS
naplnit	naplnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
letní	letní	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
2014	#num#	k4
si	se	k3xPyFc3
ČSA	ČSA	kA
pronajaly	pronajmout	k5eAaPmAgFnP
jednu	jeden	k4xCgFnSc4
A320	A320	k1gFnSc4
od	od	k7c2
malajské	malajský	k2eAgFnSc2d1
nízkonákladové	nízkonákladový	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
AirAsia	AirAsia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadlo	letadlo	k1gNnSc1
létalo	létat	k5eAaImAgNnS
v	v	k7c6
plných	plný	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
AirAsia	AirAsium	k1gNnSc2
<g/>
,	,	kIx,
jen	jen	k9
s	s	k7c7
českou	český	k2eAgFnSc7d1
registrací	registrace	k1gFnSc7
OK-NES	OK-NES	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadlo	letadlo	k1gNnSc1
létalo	létat	k5eAaImAgNnS
především	především	k6eAd1
charterové	charterový	k2eAgFnPc4d1
linky	linka	k1gFnPc4
pro	pro	k7c4
Travel	Travel	k1gInSc4
Service	Service	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
2014	#num#	k4
vykonaly	vykonat	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
zbývající	zbývající	k2eAgFnPc1d1
A320	A320	k1gFnPc1
(	(	kIx(
<g/>
OK-LEE	OK-LEE	k1gFnPc1
<g/>
,	,	kIx,
LEF	Lef	k1gInSc1
<g/>
,	,	kIx,
LEG	lego	k1gNnPc2
<g/>
,	,	kIx,
MEH	mha	k1gFnPc2
<g/>
,	,	kIx,
MEI	MEI	kA
a	a	k8xC
MEJ	MEJ	kA
<g/>
)	)	kIx)
své	svůj	k3xOyFgInPc4
poslední	poslední	k2eAgInPc4d1
lety	let	k1gInPc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
nabídnuty	nabídnout	k5eAaPmNgFnP
k	k	k7c3
prodeji	prodej	k1gInSc3
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
prodány	prodán	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Airbus	airbus	k1gInSc1
A319	A319	k1gFnSc2
ČSA	ČSA	kA
(	(	kIx(
<g/>
OK-MEL	OK-MEL	k1gMnSc1
<g/>
)	)	kIx)
vytlačován	vytlačovat	k5eAaImNgMnS
ze	z	k7c2
stojánky	stojánka	k1gFnSc2
na	na	k7c6
letišti	letiště	k1gNnSc6
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
červenec	červenec	k1gInSc1
2019	#num#	k4
</s>
<s>
Jediným	jediný	k2eAgInSc7d1
typem	typ	k1gInSc7
na	na	k7c6
středně	středně	k6eAd1
dlouhé	dlouhý	k2eAgFnSc6d1
tratě	trata	k1gFnSc6
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgInS
Airbus	airbus	k1gInSc1
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
pro	pro	k7c4
přibližně	přibližně	k6eAd1
140	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
letadel	letadlo	k1gNnPc2
se	se	k3xPyFc4
opět	opět	k6eAd1
částečně	částečně	k6eAd1
snížil	snížit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
byly	být	k5eAaImAgInP
stroje	stroj	k1gInPc1
registrací	registrace	k1gFnPc2
OK-PET	OK-PET	k1gFnPc2
a	a	k8xC
OK-OER	OK-OER	k1gFnPc1
pronajaty	pronajat	k2eAgFnPc1d1
společnosti	společnost	k1gFnSc2
Saudia	Saudium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejich	jejich	k3xOp3gInSc6
návratu	návrat	k1gInSc6
zpět	zpět	k6eAd1
k	k	k7c3
ČSA	ČSA	kA
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
opět	opět	k6eAd1
ustálila	ustálit	k5eAaPmAgFnS
na	na	k7c6
9	#num#	k4
letadlech	letadlo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
další	další	k2eAgFnPc1d1
změny	změna	k1gFnPc1
přišly	přijít	k5eAaPmAgFnP
poměrně	poměrně	k6eAd1
brzo	brzo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
opustil	opustit	k5eAaPmAgMnS
flotilu	flotila	k1gFnSc4
A319	A319	k1gMnSc1
OK-OER	OK-OER	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
letní	letní	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
se	se	k3xPyFc4
však	však	k9
do	do	k7c2
flotily	flotila	k1gFnSc2
vrátil	vrátit	k5eAaPmAgInS
Airbus	airbus	k1gInSc1
A321	A321	k1gFnSc2
registrace	registrace	k1gFnSc2
OY-RUU	OY-RUU	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
pronajat	pronajmout	k5eAaPmNgInS
od	od	k7c2
dánského	dánský	k2eAgMnSc2d1
přepravce	přepravce	k1gMnSc2
Danish	Danisha	k1gFnPc2
Air	Air	k1gFnSc2
Transport	transport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
bylo	být	k5eAaImAgNnS
dohodnuto	dohodnout	k5eAaPmNgNnS
akcionářem	akcionář	k1gMnSc7
ČSA	ČSA	kA
<g/>
,	,	kIx,
společností	společnost	k1gFnPc2
Smartwings	Smartwingsa	k1gFnPc2
<g/>
,	,	kIx,
pronájem	pronájem	k1gInSc4
většiny	většina	k1gFnSc2
letadel	letadlo	k1gNnPc2
německému	německý	k2eAgNnSc3d1
dopravci	dopravce	k1gMnPc1
Eurowings	Eurowings	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
dubna	duben	k1gInSc2
2019	#num#	k4
opustil	opustit	k5eAaPmAgInS
flotilu	flotila	k1gFnSc4
stroj	stroj	k1gInSc1
OK-PET	OK-PET	k1gFnPc1
<g/>
,	,	kIx,
OK-MEK	OK-MEK	k1gFnPc1
a	a	k8xC
OK-MEL	OK-MEL	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgMnSc1
jmenovaný	jmenovaný	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
do	do	k7c2
flotily	flotila	k1gFnSc2
znovu	znovu	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
a	a	k8xC
v	v	k7c6
letní	letní	k2eAgFnSc6d1
sezoně	sezona	k1gFnSc6
2019	#num#	k4
byl	být	k5eAaImAgMnS
jediným	jediný	k2eAgMnSc7d1
A319	A319	k1gMnSc7
ve	v	k7c6
flotile	flotila	k1gFnSc6
ČSA	ČSA	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
létal	létat	k5eAaImAgInS
z	z	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2020	#num#	k4
byl	být	k5eAaImAgInS
ukončen	ukončit	k5eAaPmNgInS
pronájem	pronájem	k1gInSc1
A319	A319	k1gFnSc2
společností	společnost	k1gFnPc2
Eurowings	Eurowings	k1gInSc1
<g/>
,	,	kIx,
do	do	k7c2
flotily	flotila	k1gFnSc2
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
vrátit	vrátit	k5eAaPmF
až	až	k9
4	#num#	k4
A320	A320	k1gFnPc2
a	a	k8xC
již	již	k6eAd1
v	v	k7c6
únoru	únor	k1gInSc6
přiletěl	přiletět	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
první	první	k4xOgFnSc2
z	z	k7c2
nich	on	k3xPp3gFnPc2
<g/>
,	,	kIx,
A320	A320	k1gFnSc1
registrace	registrace	k1gFnSc1
OK-HEU	OK-HEU	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tento	tento	k3xDgInSc1
letoun	letoun	k1gInSc1
byl	být	k5eAaImAgInS
dříve	dříve	k6eAd2
používán	používat	k5eAaImNgInS
jinými	jiný	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
stroje	stroj	k1gInPc1
měly	mít	k5eAaImAgInP
brzo	brzo	k6eAd1
následovat	následovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
pandemie	pandemie	k1gFnSc2
covidu-	covidu-	k?
<g/>
19	#num#	k4
však	však	k9
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
nastala	nastat	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
redukce	redukce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flotilu	flotila	k1gFnSc4
v	v	k7c6
půlce	půlka	k1gFnSc6
března	březen	k1gInSc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
již	již	k6eAd1
definitivně	definitivně	k6eAd1
<g/>
,	,	kIx,
opustily	opustit	k5eAaPmAgFnP
A319	A319	k1gFnPc1
OK-MEL	OK-MEL	k1gMnPc2
a	a	k8xC
OK-NEN	OK-NEN	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obnovení	obnovení	k1gNnSc6
provozu	provoz	k1gInSc2
v	v	k7c6
květnu	květen	k1gInSc6
2020	#num#	k4
aktivně	aktivně	k6eAd1
létali	létat	k5eAaImAgMnP
jen	jen	k9
A319	A319	k1gMnSc1
OK-REQ	OK-REQ	k1gMnSc1
(	(	kIx(
<g/>
jediný	jediný	k2eAgMnSc1d1
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
ČSA	ČSA	kA
<g/>
)	)	kIx)
a	a	k8xC
A320	A320	k1gMnSc1
OK-HEU	OK-HEU	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgFnPc4d1
A319	A319	k1gFnPc4
registrací	registrace	k1gFnSc7
OK-NEM	OK-NEM	k1gFnSc7
<g/>
,	,	kIx,
OK-NEO	OK-NEO	k1gFnSc7
a	a	k8xC
OK-NEP	OK-NEP	k1gFnSc7
zůstaly	zůstat	k5eAaPmAgFnP
na	na	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
osud	osud	k1gInSc1
byl	být	k5eAaImAgInS
nejistý	jistý	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
2020	#num#	k4
opustil	opustit	k5eAaPmAgMnS
flotilu	flotila	k1gFnSc4
A319	A319	k1gMnSc1
OK-NEP	OK-NEP	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
zbarven	zbarvit	k5eAaPmNgInS
ve	v	k7c6
speciálních	speciální	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
propagující	propagující	k2eAgFnSc4d1
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
října	říjen	k1gInSc2
opustil	opustit	k5eAaPmAgMnS
ČSA	ČSA	kA
další	další	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
319	#num#	k4
<g/>
,	,	kIx,
registrace	registrace	k1gFnSc1
OK-NEM	OK-NEM	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
2020	#num#	k4
byl	být	k5eAaImAgInS
znovu	znovu	k6eAd1
uskladněn	uskladněn	k2eAgInSc1d1
stroj	stroj	k1gInSc1
A319	A319	k1gMnSc2
OK-REQ	OK-REQ	k1gMnSc2
a	a	k8xC
létal	létat	k5eAaImAgMnS
jen	jen	k9
A320	A320	k1gMnSc1
OK-HEU	OK-HEU	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
prosince	prosinec	k1gInSc2
2020	#num#	k4
opustil	opustit	k5eAaPmAgMnS
flotilu	flotila	k1gFnSc4
další	další	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
319	#num#	k4
<g/>
,	,	kIx,
registrace	registrace	k1gFnSc1
OK-NEO	OK-NEO	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
původních	původní	k2eAgInPc2d1
9	#num#	k4
letadel	letadlo	k1gNnPc2
typu	typ	k1gInSc2
A319	A319	k1gFnSc2
zbyl	zbýt	k5eAaPmAgInS
společnosti	společnost	k1gFnSc2
jediný	jediný	k2eAgInSc1d1
<g/>
,	,	kIx,
registrace	registrace	k1gFnSc1
OK-REQ	OK-REQ	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flotila	flotila	k1gFnSc1
ČSA	ČSA	kA
se	se	k3xPyFc4
zmenšila	zmenšit	k5eAaPmAgFnS
na	na	k7c4
pouhé	pouhý	k2eAgInPc4d1
2	#num#	k4
Airbusy	airbus	k1gInPc4
<g/>
,	,	kIx,
jednu	jeden	k4xCgFnSc4
A320	A320	k1gFnSc4
a	a	k8xC
jeden	jeden	k4xCgInSc1
A	A	kA
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
do	do	k7c2
vzduchu	vzduch	k1gInSc2
opět	opět	k6eAd1
vznesl	vznést	k5eAaPmAgMnS
A319	A319	k1gFnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
A320	A320	k1gFnSc2
family	famila	k1gFnSc2
vrátila	vrátit	k5eAaPmAgFnS
zpět	zpět	k6eAd1
do	do	k7c2
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
ledna	leden	k1gInSc2
2021	#num#	k4
zajistil	zajistit	k5eAaPmAgMnS
A320	A320	k1gMnSc3
transport	transport	k1gInSc4
dílů	díl	k1gInPc2
na	na	k7c4
výrobu	výroba	k1gFnSc4
aut	auto	k1gNnPc2
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
marockého	marocký	k2eAgInSc2d1
Tangeru	Tanger	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Smartwings	Smartwings	k1gInSc1
</s>
<s>
Společnost	společnost	k1gFnSc1
Travel	Travlo	k1gNnPc2
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Smartwings	Smartwings	k1gInSc1
<g/>
)	)	kIx)
sice	sice	k8xC
stojí	stát	k5eAaImIp3nS
na	na	k7c6
letadlech	letadlo	k1gNnPc6
Boeing	boeing	k1gInSc4
737	#num#	k4
Next	Next	k2eAgInSc4d1
Generation	Generation	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
i	i	k9
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
flotile	flotila	k1gFnSc6
se	se	k3xPyFc4
čas	čas	k1gInSc1
od	od	k7c2
času	čas	k1gInSc2
Airbusy	airbus	k1gInPc1
A320	A320	k1gFnSc4
objevily	objevit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
pronajaté	pronajatý	k2eAgInPc4d1
letouny	letoun	k1gInPc4
na	na	k7c4
letní	letní	k2eAgFnPc4d1
sezóny	sezóna	k1gFnPc4
od	od	k7c2
různých	různý	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadla	letadlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
bázována	bázován	k2eAgFnSc1d1
na	na	k7c6
letištích	letiště	k1gNnPc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Ostravě	Ostrava	k1gFnSc6
či	či	k8xC
Pardubicích	Pardubice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
-	-	kIx~
Seagle	Seagle	k1gInSc1
Air	Air	k1gFnSc2
</s>
<s>
V	v	k7c6
historii	historie	k1gFnSc6
leteckých	letecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
se	se	k3xPyFc4
letadla	letadlo	k1gNnSc2
typu	typ	k1gInSc2
A320	A320	k1gFnSc2
objevila	objevit	k5eAaPmAgFnS
ve	v	k7c6
flotile	flotila	k1gFnSc6
jen	jen	k6eAd1
společnosti	společnost	k1gFnSc2
Seagle	Seagle	k1gFnSc2
Air	Air	k1gFnSc2
v	v	k7c6
počtu	počet	k1gInSc6
2	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
provozovala	provozovat	k5eAaImAgFnS
leteckou	letecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
a	a	k8xC
nepravidelnou	pravidelný	k2eNgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
přepravu	přeprava	k1gFnSc4
cestujících	cestující	k1gMnPc2
a	a	k8xC
nákladů	náklad	k1gInPc2
letadly	letadlo	k1gNnPc7
LET-410	LET-410	k1gFnPc3
Turbolet	Turbolet	k1gInSc4
(	(	kIx(
<g/>
například	například	k6eAd1
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
poštu	pošta	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krachu	krach	k1gInSc6
Slovenských	slovenský	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
v	v	k7c6
únoru	únor	k1gInSc6
2007	#num#	k4
využila	využít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
šance	šance	k1gFnSc1
<g/>
,	,	kIx,
zaměstnala	zaměstnat	k5eAaPmAgFnS
80	#num#	k4
<g/>
%	%	kIx~
bývalých	bývalý	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
Slovenských	slovenský	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
a	a	k8xC
nakoupila	nakoupit	k5eAaPmAgNnP
velká	velký	k2eAgNnPc1d1
dopravní	dopravní	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
2	#num#	k4
letech	léto	k1gNnPc6
byly	být	k5eAaImAgInP
dodány	dodat	k5eAaPmNgInP
4	#num#	k4
Boeingy	boeing	k1gInPc1
737-300	737-300	k4
a	a	k8xC
v	v	k7c6
dubnu	duben	k1gInSc6
respektive	respektive	k9
v	v	k7c6
červnu	červen	k1gInSc6
2009	#num#	k4
byly	být	k5eAaImAgFnP
dodány	dodán	k2eAgFnPc1d1
i	i	k9
2	#num#	k4
Airbusy	airbus	k1gInPc4
A	a	k8xC
<g/>
320	#num#	k4
<g/>
,	,	kIx,
registrované	registrovaný	k2eAgFnPc4d1
OM-HLD	OM-HLD	k1gFnPc4
a	a	k8xC
OM-HLE	OM-HLE	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staly	stát	k5eAaPmAgInP
se	s	k7c7
prvními	první	k4xOgFnPc7
(	(	kIx(
<g/>
a	a	k8xC
zatím	zatím	k6eAd1
i	i	k9
posledními	poslední	k2eAgFnPc7d1
<g/>
)	)	kIx)
A320	A320	k1gFnPc4
zaregistrované	zaregistrovaný	k2eAgFnPc4d1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
společnosti	společnost	k1gFnSc2
však	však	k9
dlouho	dlouho	k6eAd1
nevydržely	vydržet	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnosti	společnost	k1gFnSc3
Seagle	Seagle	k1gFnSc2
Air	Air	k1gFnSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
2009	#num#	k4
ukončila	ukončit	k5eAaPmAgFnS
všechny	všechen	k3xTgInPc4
lety	let	k1gInPc4
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
letadla	letadlo	k1gNnPc1
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
zpět	zpět	k6eAd1
k	k	k7c3
leasingovým	leasingový	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
obou	dva	k4xCgNnPc2
A	a	k9
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Technologie	technologie	k1gFnSc1
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1
montáž	montáž	k1gFnSc1
letounů	letoun	k1gMnPc2
Airbus	airbus	k1gInSc4
A321	A321	k1gFnPc2
v	v	k7c6
Hamburku	Hamburk	k1gInSc6
</s>
<s>
Některé	některý	k3yIgFnPc1
moderní	moderní	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
použité	použitý	k2eAgFnPc4d1
v	v	k7c6
A	A	kA
<g/>
320	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
první	první	k4xOgFnSc4
plně	plně	k6eAd1
digitální	digitální	k2eAgFnSc4d1
fly-by-wire	fly-by-wir	k1gInSc5
řídící	řídící	k2eAgInSc4d1
systém	systém	k1gInSc4
v	v	k7c6
civilním	civilní	k2eAgNnSc6d1
dopravním	dopravní	k2eAgNnSc6d1
letadle	letadlo	k1gNnSc6
a	a	k8xC
první	první	k4xOgFnSc4
s	s	k7c7
tzv.	tzv.	kA
přirozenou	přirozený	k2eAgFnSc7d1
nestabilitou	nestabilita	k1gFnSc7
</s>
<s>
první	první	k4xOgNnSc4
civilní	civilní	k2eAgNnSc4d1
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
používá	používat	k5eAaImIp3nS
boční	boční	k2eAgFnSc7d1
„	„	k?
<g/>
sidesticky	sidesticky	k6eAd1
<g/>
“	“	k?
místo	místo	k1gNnSc4
tradičních	tradiční	k2eAgFnPc2d1
řídicích	řídicí	k2eAgFnPc2d1
pák	páka	k1gFnPc2
(	(	kIx(
<g/>
sloupků	sloupek	k1gInPc2
<g/>
/	/	kIx~
<g/>
beranů	beran	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
dvoučlenná	dvoučlenný	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
(	(	kIx(
<g/>
Boeing	boeing	k1gInSc1
727	#num#	k4
má	mít	k5eAaImIp3nS
tříčlennou	tříčlenný	k2eAgFnSc4d1
<g/>
,	,	kIx,
737	#num#	k4
dvoučlennou	dvoučlenný	k2eAgFnSc7d1
<g/>
)	)	kIx)
</s>
<s>
plně	plně	k6eAd1
skleněný	skleněný	k2eAgInSc4d1
kokpit	kokpit	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
ne	ne	k9
hybridní	hybridní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
jako	jako	k8xS,k8xC
u	u	k7c2
A	A	kA
<g/>
310	#num#	k4
<g/>
,	,	kIx,
Boeingu	boeing	k1gInSc2
757	#num#	k4
a	a	k8xC
Boeingu	boeing	k1gInSc2
767	#num#	k4
</s>
<s>
první	první	k4xOgInSc1
dopravní	dopravní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
s	s	k7c7
významným	významný	k2eAgInSc7d1
podílem	podíl	k1gInSc7
kompozitních	kompozitní	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
v	v	k7c6
konstrukci	konstrukce	k1gFnSc6
</s>
<s>
centrální	centrální	k2eAgInSc1d1
diagnostický	diagnostický	k2eAgInSc1d1
systém	systém	k1gInSc1
umožňující	umožňující	k2eAgInSc1d1
zjištění	zjištění	k1gNnSc4
problému	problém	k1gInSc2
z	z	k7c2
pilotní	pilotní	k2eAgFnSc2d1
kabiny	kabina	k1gFnSc2
</s>
<s>
Varianty	varianta	k1gFnPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Komentář	komentář	k1gInSc1
<g/>
:	:	kIx,
Sekce	sekce	k1gFnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
nejvíce	nejvíce	k6eAd1,k6eAd3
letadel	letadlo	k1gNnPc2
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
provozuje	provozovat	k5eAaImIp3nS
<g/>
...	...	k?
<g/>
"	"	kIx"
nebo	nebo	k8xC
„	„	k?
<g/>
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
drží	držet	k5eAaImIp3nS
rekord	rekord	k1gInSc4
<g/>
"	"	kIx"
už	už	k6eAd1
nejsou	být	k5eNaImIp3nP
aktuální	aktuální	k2eAgInSc4d1
</s>
<s>
Airbus	airbus	k1gInSc1
A320-232	A320-232	k1gFnSc2
z	z	k7c2
podhledu	podhled	k1gInSc2
</s>
<s>
A320	A320	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
základem	základ	k1gInSc7
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
verzí	verze	k1gFnPc2
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
rozdílnou	rozdílný	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
–	–	k?
o	o	k7c4
něco	něco	k3yInSc4
menší	malý	k2eAgFnPc1d2
A	a	k8xC
<g/>
319	#num#	k4
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
menší	malý	k2eAgFnSc4d2
A318	A318	k1gFnSc4
a	a	k8xC
o	o	k7c4
trochu	trocha	k1gFnSc4
větší	veliký	k2eAgFnSc4d2
A	a	k8xC
<g/>
321	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc1
kapacita	kapacita	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
100	#num#	k4
do	do	k7c2
220	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkurují	konkurovat	k5eAaImIp3nP
si	se	k3xPyFc3
s	s	k7c7
Boeingy	boeing	k1gInPc1
737	#num#	k4
<g/>
,	,	kIx,
757	#num#	k4
a	a	k8xC
717	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
pilota	pilot	k1gMnSc4
je	být	k5eAaImIp3nS
ovládání	ovládání	k1gNnSc1
všech	všecek	k3xTgFnPc2
verzí	verze	k1gFnPc2
A320	A320	k1gFnSc2
stejné	stejný	k2eAgFnSc2d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
vzít	vzít	k5eAaPmF
do	do	k7c2
úvahy	úvaha	k1gFnSc2
o	o	k7c4
něco	něco	k3yInSc4
větší	veliký	k2eAgFnSc1d2
či	či	k8xC
menší	malý	k2eAgFnSc1d2
délka	délka	k1gFnSc1
a	a	k8xC
hmotnost	hmotnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Technicky	technicky	k6eAd1
se	se	k3xPyFc4
název	název	k1gInSc1
„	„	k?
<g/>
A	a	k8xC
<g/>
320	#num#	k4
<g/>
“	“	k?
vztahuje	vztahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
původní	původní	k2eAgFnSc6d1
„	„	k?
<g/>
středně	středně	k6eAd1
velkou	velký	k2eAgFnSc4d1
<g/>
“	“	k?
verzi	verze	k1gFnSc4
letounu	letoun	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
neformálně	formálně	k6eNd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
označení	označení	k1gNnSc3
celé	celý	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
A	a	k9
<g/>
318	#num#	k4
<g/>
/	/	kIx~
<g/>
A	a	k9
<g/>
319	#num#	k4
<g/>
/	/	kIx~
<g/>
A	a	k9
<g/>
320	#num#	k4
<g/>
/	/	kIx~
<g/>
A	a	k9
<g/>
321	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
varianty	varianta	k1gFnPc1
splňují	splňovat	k5eAaImIp3nP
požadavky	požadavek	k1gInPc7
ETOPS	ETOPS	kA
(	(	kIx(
<g/>
schopnost	schopnost	k1gFnSc4
letět	letět	k5eAaImF
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
s	s	k7c7
jedním	jeden	k4xCgInSc7
motorem	motor	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
A320	A320	k4
</s>
<s>
Irské	irský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
<g/>
,	,	kIx,
Aer	aero	k1gNnPc2
Lingus	Lingus	k1gInSc4
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
přes	přes	k7c4
třicet	třicet	k4xCc4
letounů	letoun	k1gInPc2
A320-200	A320-200	k1gFnSc2
</s>
<s>
A320	A320	k4
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
varianty	varianta	k1gFnPc4
A320-100	A320-100	k1gFnSc2
a	a	k8xC
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A320-200	A320-200	k1gMnSc1
je	být	k5eAaImIp3nS
konečnou	konečná	k1gFnSc4
verzí	verze	k1gFnPc2
<g/>
,	,	kIx,
strojů	stroj	k1gInPc2
A320-100	A320-100	k1gMnPc2
se	se	k3xPyFc4
vyrobilo	vyrobit	k5eAaPmAgNnS
jen	jen	k9
několik	několik	k4yIc1
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
již	již	k6eAd1
jen	jen	k9
jeden	jeden	k4xCgInSc4
kus	kus	k1gInSc4
<g/>
,	,	kIx,
první	první	k4xOgInSc4
prototyp	prototyp	k1gInSc4
A	a	k9
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A320-200	A320-200	k1gMnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
koncích	konec	k1gInPc6
křídel	křídlo	k1gNnPc2
malé	malý	k2eAgFnSc2d1
trojúhelníkové	trojúhelníkový	k2eAgFnSc2d1
winglety	wingleta	k1gFnSc2
(	(	kIx(
<g/>
wingtip	wingtip	k1gInSc1
fence	fenka	k1gFnSc3
<g/>
)	)	kIx)
a	a	k8xC
zvýšenou	zvýšený	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
palivových	palivový	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
oproti	oproti	k7c3
A320-100	A320-100	k1gFnSc3
(	(	kIx(
<g/>
pro	pro	k7c4
větší	veliký	k2eAgInSc4d2
dolet	dolet	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
další	další	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
jsou	být	k5eAaImIp3nP
minimální	minimální	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typický	typický	k2eAgInSc4d1
dolet	dolet	k1gInSc4
se	s	k7c7
150	#num#	k4
pasažéry	pasažér	k1gMnPc7
je	být	k5eAaImIp3nS
u	u	k7c2
A320-200	A320-200	k1gFnSc2
asi	asi	k9
5	#num#	k4
400	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohon	pohon	k1gInSc1
obstarávají	obstarávat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
dvouproudové	dvouproudový	k2eAgInPc4d1
motory	motor	k1gInPc4
CFMI	CFMI	kA
CFM56-5	CFM56-5	k1gFnSc1
nebo	nebo	k8xC
IAE	IAE	kA
V2500	V2500	k1gFnPc4
s	s	k7c7
tahem	tah	k1gInSc7
113	#num#	k4
kN	kN	k?
až	až	k9
120	#num#	k4
kN	kN	k?
<g/>
.	.	kIx.
</s>
<s>
A319	A319	k4
</s>
<s>
Airbus	airbus	k1gInSc1
A319	A319	k1gFnSc2
ve	v	k7c6
službách	služba	k1gFnPc6
Aeroflotu	aeroflot	k1gInSc2
</s>
<s>
Zkrácená	zkrácený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
A320	A320	k1gFnSc2
s	s	k7c7
minimálními	minimální	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
skoro	skoro	k6eAd1
shodnou	shodný	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
palivových	palivový	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
jako	jako	k8xS,k8xC
A320-200	A320-200	k1gFnPc2
a	a	k8xC
méně	málo	k6eAd2
pasažéry	pasažér	k1gMnPc7
se	se	k3xPyFc4
dolet	dolet	k1gInSc4
<g/>
,	,	kIx,
se	s	k7c7
124	#num#	k4
cestujícími	cestující	k1gMnPc7
ve	v	k7c6
dvou	dva	k4xCgFnPc6
třídách	třída	k1gFnPc6
<g/>
,	,	kIx,
prodloužil	prodloužit	k5eAaPmAgInS
na	na	k7c4
7	#num#	k4
200	#num#	k4
km	km	kA
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
třídě	třída	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A320	A320	k1gFnPc2
a	a	k8xC
A319	A319	k1gFnPc2
jsou	být	k5eAaImIp3nP
nejpopulárnější	populární	k2eAgFnPc1d3
varianty	varianta	k1gFnPc1
v	v	k7c6
rodině	rodina	k1gFnSc6
A	a	k9
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
si	se	k3xPyFc3
easyJet	easyJet	k1gMnSc1
objednal	objednat	k5eAaPmAgMnS
Airbusy	airbus	k1gInPc4
A319	A319	k1gMnPc2
s	s	k7c7
menšími	malý	k2eAgFnPc7d2
kuchyňkami	kuchyňka	k1gFnPc7
(	(	kIx(
<g/>
easyJet	easyJet	k1gInSc1
během	během	k7c2
letů	let	k1gInPc2
nenabízí	nabízet	k5eNaImIp3nS
jídlo	jídlo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
156	#num#	k4
sedadly	sedadlo	k1gNnPc7
v	v	k7c6
jediné	jediný	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhovělo	vyhovět	k5eAaPmAgNnS
bezpečnostním	bezpečnostní	k2eAgInPc3d1
požadavkům	požadavek	k1gInPc3
na	na	k7c4
rychlost	rychlost	k1gFnSc4
evakuace	evakuace	k1gFnSc2
<g/>
,	,	kIx,
musely	muset	k5eAaImAgInP
být	být	k5eAaImF
přidány	přidat	k5eAaPmNgInP
další	další	k2eAgInPc1d1
nouzové	nouzový	k2eAgInPc1d1
východy	východ	k1gInPc1
nad	nad	k7c4
křídlo	křídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohon	pohon	k1gInSc1
zajišťují	zajišťovat	k5eAaImIp3nP
stejné	stejný	k2eAgInPc4d1
motory	motor	k1gInPc4
jako	jako	k8xS,k8xC
u	u	k7c2
A	A	kA
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Certifikace	certifikace	k1gFnSc1
u	u	k7c2
JAA	JAA	kA
a	a	k8xC
vstup	vstup	k1gInSc4
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
u	u	k7c2
Swissairu	Swissair	k1gInSc2
<g/>
,	,	kIx,
proběhly	proběhnout	k5eAaPmAgInP
v	v	k7c6
dubnu	duben	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obrovská	obrovský	k2eAgFnSc1d1
objednávka	objednávka	k1gFnSc1
easyJetu	easyJet	k1gInSc2
na	na	k7c4
120	#num#	k4
kusů	kus	k1gInPc2
A319	A319	k1gFnSc1
plus	plus	k1gInSc1
120	#num#	k4
opcí	opce	k1gFnPc2
patřila	patřit	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
mezi	mezi	k7c4
největší	veliký	k2eAgFnPc4d3
objednávky	objednávka	k1gFnPc4
dopravních	dopravní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
od	od	k7c2
leteckých	letecký	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc4d1
srovnatelnou	srovnatelný	k2eAgFnSc4d1
objednávku	objednávka	k1gFnSc4
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
hlavní	hlavní	k2eAgMnSc1d1
konkurent	konkurent	k1gMnSc1
easyJetu	easyJet	k1gMnSc3
Ryanair	Ryanair	k1gInSc4
na	na	k7c4
stroje	stroj	k1gInPc4
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
drží	držet	k5eAaImIp3nS
Northwest	Northwest	k1gMnSc1
rekord	rekord	k1gInSc1
v	v	k7c6
nejkratší	krátký	k2eAgFnSc6d3
pravidelné	pravidelný	k2eAgFnSc6d1
lince	linka	k1gFnSc6
A319	A319	k1gFnSc1
z	z	k7c2
Bishop	Bishop	k1gInSc4
International	International	k1gFnSc4
Airport	Airport	k1gInSc4
ve	v	k7c4
Flintu	flinta	k1gFnSc4
<g/>
,	,	kIx,
Michigan	Michigan	k1gInSc4
do	do	k7c2
Detroitu	Detroit	k1gInSc2
(	(	kIx(
<g/>
Detroit	Detroit	k1gInSc1
Metro	metro	k1gNnSc1
Airport	Airport	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
vzdálenost	vzdálenost	k1gFnSc1
pouhých	pouhý	k2eAgInPc2d1
92	#num#	k4
km	km	kA
(	(	kIx(
<g/>
57	#num#	k4
mil	míle	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
A319CJ	A319CJ	k4
</s>
<s>
Český	český	k2eAgInSc1d1
vládní	vládní	k2eAgInSc1d1
Airbus	airbus	k1gInSc1
A319CJ	A319CJ	k1gMnPc2
doprovázen	doprovázen	k2eAgMnSc1d1
stíhačkou	stíhačka	k1gFnSc7
Saab	Saab	k1gInSc4
Gripen	Gripen	k2eAgInSc4d1
při	při	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
Olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Soči	Soči	k1gNnSc6
</s>
<s>
A319CJ	A319CJ	k4
(	(	kIx(
<g/>
Corporate	Corporat	k1gInSc5
Jet	jet	k5eAaImF
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
business	business	k1gInSc4
jet	jet	k5eAaImF
verze	verze	k1gFnSc2
A	a	k9
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
instalované	instalovaný	k2eAgFnSc2d1
další	další	k2eAgFnSc2d1
palivové	palivový	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
v	v	k7c6
nákladovém	nákladový	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
a	a	k8xC
prodloužený	prodloužený	k2eAgInSc4d1
dolet	dolet	k1gInSc4
na	na	k7c4
12	#num#	k4
000	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
případném	případný	k2eAgInSc6d1
dalším	další	k2eAgInSc6d1
prodeji	prodej	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zpětně	zpětně	k6eAd1
přestavěn	přestavět	k5eAaPmNgInS
na	na	k7c4
standardní	standardní	k2eAgFnSc4d1
A319	A319	k1gFnSc4
pouhým	pouhý	k2eAgNnSc7d1
odstraněním	odstranění	k1gNnSc7
přidaných	přidaný	k2eAgFnPc2d1
palivových	palivový	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
maximalizovat	maximalizovat	k5eAaBmF
svou	svůj	k3xOyFgFnSc4
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
ACJ	ACJ	kA
neboli	neboli	k8xC
Airbus	airbus	k1gInSc1
Corporate	Corporat	k1gInSc5
Jet	jet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Letoun	letoun	k1gMnSc1
může	moct	k5eAaImIp3nS
pojmout	pojmout	k5eAaPmF
až	až	k9
39	#num#	k4
pasažérů	pasažér	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
přání	přání	k1gNnSc4
zákazníka	zákazník	k1gMnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dodán	dodat	k5eAaPmNgMnS
v	v	k7c6
jakékoliv	jakýkoliv	k3yIgFnSc6
konfiguraci	konfigurace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnPc4
uživatele	uživatel	k1gMnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Daimler	Daimler	k1gInSc1
AG	AG	kA
a	a	k8xC
PrivatAir	PrivatAir	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A319CJ	A319CJ	k1gFnSc1
si	se	k3xPyFc3
konkuruje	konkurovat	k5eAaImIp3nS
s	s	k7c7
dalšími	další	k2eAgInPc7d1
firemními	firemní	k2eAgInPc7d1
letouny	letoun	k1gInPc7
jako	jako	k9
Gulfstream	Gulfstream	k1gInSc1
V	V	kA
<g/>
,	,	kIx,
na	na	k7c6
Boeingu	boeing	k1gInSc6
737	#num#	k4
založeném	založený	k2eAgInSc6d1
BBJ1	BBJ1	k1gMnSc1
a	a	k8xC
Bombardier	Bombardier	k1gMnSc1
Global	globat	k5eAaImAgMnS
Express	express	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
poháněn	pohánět	k5eAaImNgInS
stejnými	stejný	k2eAgInPc7d1
motory	motor	k1gInPc7
jako	jako	k8xC,k8xS
A	a	k9
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
podepsalo	podepsat	k5eAaPmAgNnS
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
nákupu	nákup	k1gInSc6
dvou	dva	k4xCgInPc2
letounů	letoun	k1gInPc2
A	a	k9
<g/>
319	#num#	k4
<g/>
CJ	CJ	kA
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
postupně	postupně	k6eAd1
nahradily	nahradit	k5eAaPmAgFnP
v	v	k7c6
dopravním	dopravní	k2eAgNnSc6d1
letectvu	letectvo	k1gNnSc6
Armády	armáda	k1gFnSc2
ČR	ČR	kA
zastaralé	zastaralý	k2eAgInPc4d1
stroje	stroj	k1gInPc4
Tupolev	Tupolev	k1gFnSc2
Tu-	Tu-	k1gFnSc3
<g/>
154	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
letoun	letoun	k1gMnSc1
byl	být	k5eAaImAgMnS
dodán	dodat	k5eAaPmNgMnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2006	#num#	k4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
pak	pak	k9
v	v	k7c6
září	září	k1gNnSc6
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
nesou	nést	k5eAaImIp3nP
oba	dva	k4xCgInPc4
stroje	stroj	k1gInPc4
jména	jméno	k1gNnSc2
válečných	válečný	k2eAgMnPc2d1
veteránů	veterán	k1gMnPc2
z	z	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
generála	generál	k1gMnSc2
Karla	Karel	k1gMnSc2
Janouška	Janoušek	k1gMnSc2
a	a	k8xC
generála	generál	k1gMnSc2
Josefa	Josef	k1gMnSc2
Ocelky	ocelka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A319LR	A319LR	k4
</s>
<s>
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
má	mít	k5eAaImIp3nS
všech	všecek	k3xTgNnPc2
48	#num#	k4
sedadel	sedadlo	k1gNnPc2
v	v	k7c6
obchodní	obchodní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
speciálně	speciálně	k6eAd1
navržena	navrhnout	k5eAaPmNgFnS
pro	pro	k7c4
mezikontinentální	mezikontinentální	k2eAgInPc4d1
lety	let	k1gInPc4
pouze	pouze	k6eAd1
touto	tento	k3xDgFnSc7
třídou	třída	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
<g/>
319	#num#	k4
<g/>
LR	LR	kA
<g/>
,	,	kIx,
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
A	A	kA
<g/>
319	#num#	k4
<g/>
CJ	CJ	kA
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
jen	jen	k9
čtyři	čtyři	k4xCgFnPc1
přidané	přidaný	k2eAgFnPc1d1
palivové	palivový	k2eAgFnPc1d1
nádrže	nádrž	k1gFnPc1
místo	místo	k7c2
šesti	šest	k4xCc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typický	typický	k2eAgInSc4d1
dolet	dolet	k1gInSc4
je	být	k5eAaImIp3nS
8	#num#	k4
300	#num#	k4
km	km	kA
<g/>
,	,	kIx,
nejdelší	dlouhý	k2eAgInSc1d3
v	v	k7c6
rodině	rodina	k1gFnSc6
A320	A320	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
A	a	k9
<g/>
319	#num#	k4
<g/>
CJ	CJ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lufthansa	Lufthansa	k1gFnSc1
provozuje	provozovat	k5eAaImIp3nS
linku	linka	k1gFnSc4
(	(	kIx(
<g/>
jen	jen	k9
s	s	k7c7
business	business	k1gInSc4
třídou	třída	k1gFnSc7
<g/>
)	)	kIx)
mezi	mezi	k7c7
Německem	Německo	k1gNnSc7
a	a	k8xC
USA	USA	kA
s	s	k7c7
flotilou	flotila	k1gFnSc7
strojů	stroj	k1gInPc2
A319LR	A319LR	k1gFnPc2
patřících	patřící	k2eAgFnPc2d1
švýcarskému	švýcarský	k2eAgMnSc3d1
PrivatAiru	PrivatAir	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
např.	např.	kA
Qatar	Qatara	k1gFnPc2
Airways	Airwaysa	k1gFnPc2
mají	mít	k5eAaImIp3nP
své	svůj	k3xOyFgMnPc4
A319LR	A319LR	k1gMnPc4
vybaveny	vybaven	k2eAgMnPc4d1
standardním	standardní	k2eAgInSc7d1
počtem	počet	k1gInSc7
110	#num#	k4
sedadel	sedadlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
A321	A321	k4
</s>
<s>
Přistávající	přistávající	k2eAgMnSc1d1
A321	A321	k1gMnSc1
společnosti	společnost	k1gFnSc2
Condor	Condor	k1gMnSc1
</s>
<s>
A321	A321	k4
je	být	k5eAaImIp3nS
prodloužená	prodloužený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
A320	A320	k1gFnSc2
s	s	k7c7
minimálními	minimální	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plocha	plocha	k1gFnSc1
křídla	křídlo	k1gNnSc2
byla	být	k5eAaImAgFnS
mírně	mírně	k6eAd1
zvětšena	zvětšit	k5eAaPmNgFnS
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
oproti	oproti	k7c3
A320	A320	k1gFnSc3
dvouštěrbinové	dvouštěrbinový	k2eAgFnSc2d1
klapky	klapka	k1gFnSc2
a	a	k8xC
podvozek	podvozek	k1gInSc1
je	být	k5eAaImIp3nS
zesílen	zesílit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohon	pohon	k1gInSc1
zajišťují	zajišťovat	k5eAaImIp3nP
silnější	silný	k2eAgFnPc1d2
verze	verze	k1gFnPc1
motorů	motor	k1gInPc2
CFM56	CFM56	k1gFnSc1
a	a	k8xC
V	V	kA
<g/>
2500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
dopravci	dopravce	k1gMnPc1
upřednostňují	upřednostňovat	k5eAaImIp3nP
A321	A321	k1gFnSc4
před	před	k7c7
Boeingem	boeing	k1gInSc7
757	#num#	k4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gFnSc3
podobnosti	podobnost	k1gFnPc1
s	s	k7c7
A	A	kA
<g/>
318	#num#	k4
<g/>
,	,	kIx,
A319	A319	k1gFnSc1
a	a	k8xC
A	A	kA
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Certifikací	certifikace	k1gFnPc2
úspěšně	úspěšně	k6eAd1
prošel	projít	k5eAaPmAgMnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1993	#num#	k4
u	u	k7c2
JAA	JAA	kA
<g/>
.	.	kIx.
</s>
<s>
Typický	typický	k2eAgInSc1d1
dolet	dolet	k1gInSc1
se	s	k7c7
186	#num#	k4
pasažéry	pasažér	k1gMnPc7
je	být	k5eAaImIp3nS
pro	pro	k7c4
A321-100	A321-100	k1gFnSc4
asi	asi	k9
4	#num#	k4
300	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poháněn	poháněn	k2eAgInSc1d1
je	on	k3xPp3gMnPc4
dvěma	dva	k4xCgInPc7
dvouproudovými	dvouproudový	k2eAgInPc7d1
motory	motor	k1gInPc7
CFM56-5	CFM56-5	k1gFnSc2
nebo	nebo	k8xC
IAE	IAE	kA
V2500	V2500	k1gFnPc4
s	s	k7c7
tahem	tah	k1gInSc7
138	#num#	k4
kN	kN	k?
<g/>
.	.	kIx.
</s>
<s>
A321-200	A321-200	k4
s	s	k7c7
větší	veliký	k2eAgFnSc7d2
kapacitou	kapacita	k1gFnSc7
palivových	palivový	k2eAgFnPc2d1
nádrží	nádrž	k1gFnPc2
má	mít	k5eAaImIp3nS
se	s	k7c7
186	#num#	k4
pasažéry	pasažér	k1gMnPc7
dolet	dolet	k1gInSc4
asi	asi	k9
5	#num#	k4
500	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A321-200	A321-200	k1gFnPc2
je	být	k5eAaImIp3nS
poháněn	pohánět	k5eAaImNgInS
dvěma	dva	k4xCgInPc7
motory	motor	k1gInPc7
CFM56-5	CFM56-5	k1gFnPc2
nebo	nebo	k8xC
IAE	IAE	kA
V2500	V2500	k1gFnPc4
s	s	k7c7
tahem	tah	k1gInSc7
147	#num#	k4
kN	kN	k?
<g/>
.	.	kIx.
</s>
<s>
A318	A318	k4
</s>
<s>
Airbus	airbus	k1gInSc1
A318	A318	k1gMnPc2
u	u	k7c2
British	Britisha	k1gFnPc2
Airways	Airwaysa	k1gFnPc2
</s>
<s>
A	a	k9
<g/>
318	#num#	k4
<g/>
,	,	kIx,
také	také	k9
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Mini-Airbus	Mini-Airbus	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejmenší	malý	k2eAgMnSc1d3
člen	člen	k1gMnSc1
rodiny	rodina	k1gFnSc2
A	A	kA
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
vývoje	vývoj	k1gInSc2
byl	být	k5eAaImAgInS
označován	označovat	k5eAaImNgInS
jako	jako	k9
A	A	kA
<g/>
319	#num#	k4
<g/>
M	M	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
napovídá	napovídat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
A	A	kA
<g/>
319	#num#	k4
<g/>
.	.	kIx.
„	„	k?
<g/>
M	M	kA
<g/>
3	#num#	k4
<g/>
“	“	k?
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
minus	minus	k1gInSc1
tři	tři	k4xCgInPc4
trupové	trupový	k2eAgInPc4d1
díly	díl	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
šest	šest	k4xCc4
metrů	metr	k1gInPc2
kratší	krátký	k2eAgFnSc1d2
a	a	k8xC
o	o	k7c4
14	#num#	k4
tun	tuna	k1gFnPc2
lehčí	lehký	k2eAgMnSc1d2
než	než	k8xS
A	a	k9
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
konfiguraci	konfigurace	k1gFnSc6
se	s	k7c7
dvěma	dva	k4xCgFnPc7
třídami	třída	k1gFnPc7
má	mít	k5eAaImIp3nS
A318	A318	k1gFnSc4
109	#num#	k4
sedadel	sedadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
konstruován	konstruovat	k5eAaImNgMnS
jako	jako	k8xS,k8xC
náhrada	náhrada	k1gFnSc1
starších	starý	k2eAgFnPc2d2
verzí	verze	k1gFnPc2
Boeingu	boeing	k1gInSc2
737	#num#	k4
a	a	k8xC
Douglasu	Douglas	k1gInSc6
DC-	DC-	k1gFnPc2
<g/>
9	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
konkuruje	konkurovat	k5eAaImIp3nS
také	také	k9
modelům	model	k1gInPc3
Boeing	boeing	k1gInSc4
737-700	737-700	k4
a	a	k8xC
Boeing	boeing	k1gInSc4
717	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
A318	A318	k4
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
několika	několik	k4yIc6
verzích	verze	k1gFnPc6
s	s	k7c7
různou	různý	k2eAgFnSc7d1
maximální	maximální	k2eAgFnSc7d1
vzletovou	vzletový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
(	(	kIx(
<g/>
MTOW	MTOW	kA
<g/>
)	)	kIx)
od	od	k7c2
59	#num#	k4
tun	tuna	k1gFnPc2
(	(	kIx(
<g/>
dolet	dolet	k1gInSc4
2750	#num#	k4
km	km	kA
<g/>
)	)	kIx)
po	po	k7c4
68	#num#	k4
tun	tuna	k1gFnPc2
(	(	kIx(
<g/>
6000	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nižší	nízký	k2eAgInSc1d2
MTOW	MTOW	kA
umožňuje	umožňovat	k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc4d1
provoz	provoz	k1gInSc4
na	na	k7c6
regionálních	regionální	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
(	(	kIx(
<g/>
při	při	k7c6
kratším	krátký	k2eAgInSc6d2
doletu	dolet	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
vyšší	vysoký	k2eAgFnSc2d2
MTOW	MTOW	kA
dovoluje	dovolovat	k5eAaImIp3nS
A318	A318	k1gFnSc1
doplňovat	doplňovat	k5eAaImF
další	další	k2eAgInPc4d1
stroje	stroj	k1gInPc4
rodiny	rodina	k1gFnSc2
A320	A320	k1gMnPc2
na	na	k7c6
okrajových	okrajový	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lehčí	lehký	k2eAgFnSc1d2
A318	A318	k1gFnSc1
má	mít	k5eAaImIp3nS
o	o	k7c4
10	#num#	k4
<g/>
%	%	kIx~
větší	veliký	k2eAgInSc4d2
dolet	dolet	k1gInSc4
než	než	k8xS
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mu	on	k3xPp3gMnSc3
dovoluje	dovolovat	k5eAaImIp3nS
obsluhovat	obsluhovat	k5eAaImF
některé	některý	k3yIgFnPc4
linky	linka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
A320	A320	k1gFnSc4
obsluhovat	obsluhovat	k5eAaImF
nemůže	moct	k5eNaImIp3nS
<g/>
,	,	kIx,
např.	např.	kA
Londýn	Londýn	k1gInSc1
<g/>
–	–	k?
<g/>
Jeruzalém	Jeruzalém	k1gInSc1
a	a	k8xC
Singapur	Singapur	k1gInSc1
<g/>
–	–	k?
<g/>
Tokio	Tokio	k1gNnSc1
<g/>
,	,	kIx,
British	British	k1gInSc1
Airways	Airwaysa	k1gFnPc2
jej	on	k3xPp3gMnSc4
provozovaly	provozovat	k5eAaImAgInP
i	i	k9
na	na	k7c6
transatlantické	transatlantický	k2eAgFnSc6d1
lince	linka	k1gFnSc6
z	z	k7c2
Londýna	Londýn	k1gInSc2
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
s	s	k7c7
mezipřistáním	mezipřistání	k1gNnSc7
v	v	k7c6
Shannonu	Shannon	k1gInSc6
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
do	do	k7c2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Největší	veliký	k2eAgNnSc1d3
využití	využití	k1gNnSc1
ale	ale	k9
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
krátkých	krátký	k2eAgInPc6d1
<g/>
,	,	kIx,
méně	málo	k6eAd2
obsazených	obsazený	k2eAgNnPc6d1
letech	léto	k1gNnPc6
mezi	mezi	k7c7
městy	město	k1gNnPc7
střední	střední	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
svého	svůj	k3xOyFgInSc2
vývoje	vývoj	k1gInSc2
narazil	narazit	k5eAaPmAgMnS
A318	A318	k1gFnSc4
na	na	k7c4
několik	několik	k4yIc4
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc6
byl	být	k5eAaImAgInS
pokles	pokles	k1gInSc1
poptávky	poptávka	k1gFnSc2
po	po	k7c6
nových	nový	k2eAgNnPc6d1
letadlech	letadlo	k1gNnPc6
v	v	k7c6
období	období	k1gNnSc6
po	po	k7c4
11	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
byl	být	k5eAaImAgInS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
nové	nový	k2eAgInPc1d1
motory	motor	k1gInPc1
Pratt	Pratta	k1gFnPc2
&	&	k?
Whitney	Whitney	k1gInPc1
spotřebovávaly	spotřebovávat	k5eAaImAgInP
více	hodně	k6eAd2
paliva	palivo	k1gNnSc2
než	než	k8xS
se	se	k3xPyFc4
očekávalo	očekávat	k5eAaImAgNnS
a	a	k8xC
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
CFMI	CFMI	kA
připraveny	připravit	k5eAaPmNgInP
nové	nový	k2eAgInPc1d1
<g/>
,	,	kIx,
úspornější	úsporný	k2eAgInPc1d2
motory	motor	k1gInPc1
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
zákazníků	zákazník	k1gMnPc2
se	se	k3xPyFc4
od	od	k7c2
A318	A318	k1gFnSc2
už	už	k6eAd1
odvrátilo	odvrátit	k5eAaPmAgNnS
<g/>
,	,	kIx,
např.	např.	kA
Air	Air	k1gFnSc1
China	China	k1gFnSc1
<g/>
,	,	kIx,
Trans	trans	k1gInSc1
World	World	k1gMnSc1
Airlines	Airlines	k1gMnSc1
(	(	kIx(
<g/>
divize	divize	k1gFnSc1
American	American	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
British	British	k1gInSc4
Airways	Airwaysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
Airbus	airbus	k1gInSc4
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
A318	A318	k1gMnPc1
na	na	k7c6
trhu	trh	k1gInSc6
prosadí	prosadit	k5eAaPmIp3nS
jako	jako	k8xC,k8xS
alternativa	alternativa	k1gFnSc1
k	k	k7c3
regionálním	regionální	k2eAgInPc3d1
dopravním	dopravní	k2eAgInPc3d1
proudovým	proudový	k2eAgInPc3d1
letounům	letoun	k1gInPc3
<g/>
,	,	kIx,
zákony	zákon	k1gInPc4
jak	jak	k8xC,k8xS
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
jej	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nS
do	do	k7c2
stejné	stejný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
jako	jako	k8xS,k8xC
větší	veliký	k2eAgNnPc4d2
letadla	letadlo	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
protože	protože	k8xS
tomu	ten	k3xDgNnSc3
odpovídají	odpovídat	k5eAaImIp3nP
i	i	k9
přistávací	přistávací	k2eAgInPc1d1
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
<g/>
,	,	kIx,
regionální	regionální	k2eAgMnPc1d1
operátoři	operátor	k1gMnPc1
jej	on	k3xPp3gInSc4
nepoužívají	používat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Pohon	pohon	k1gInSc4
zajišťují	zajišťovat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
motory	motor	k1gInPc4
CFM56-5	CFM56-5	k1gFnPc2
nebo	nebo	k8xC
Pratt	Pratta	k1gFnPc2
&	&	k?
Whitney	Whitnea	k1gMnSc2
PW6000	PW6000	k1gMnSc2
s	s	k7c7
tahem	tah	k1gInSc7
96	#num#	k4
až	až	k8xS
106	#num#	k4
kN	kN	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
A318	A318	k1gMnPc2
byly	být	k5eAaImAgInP
dodány	dodat	k5eAaPmNgInP
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
společnostem	společnost	k1gFnPc3
Frontier	Frontira	k1gFnPc2
Airlines	Airlinesa	k1gFnPc2
a	a	k8xC
Air	Air	k1gFnPc2
France	Franc	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
za	za	k7c4
jeden	jeden	k4xCgInSc4
A318	A318	k1gFnPc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
39	#num#	k4
do	do	k7c2
45	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
a	a	k8xC
provozní	provozní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
jsou	být	k5eAaImIp3nP
asi	asi	k9
2500	#num#	k4
až	až	k9
3000	#num#	k4
dolarů	dolar	k1gInPc2
za	za	k7c4
hodinu	hodina	k1gFnSc4
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objednávky	objednávka	k1gFnPc4
A318	A318	k1gFnSc2
jsou	být	k5eAaImIp3nP
nižší	nízký	k2eAgFnPc1d2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
plánovalo	plánovat	k5eAaImAgNnS
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
přispěla	přispět	k5eAaPmAgFnS
těžká	těžký	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
firem	firma	k1gFnPc2
jako	jako	k8xS,k8xC
Bombardier	Bombardira	k1gFnPc2
a	a	k8xC
Embraer	Embrara	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vyrábějí	vyrábět	k5eAaImIp3nP
menší	malý	k2eAgNnPc4d2
letadla	letadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
objednáno	objednat	k5eAaPmNgNnS
a	a	k8xC
dodáno	dodat	k5eAaPmNgNnS
80	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Airbus	airbus	k1gInSc1
A	a	k8xC
<g/>
320	#num#	k4
<g/>
neo	neo	k?
<g/>
.	.	kIx.
</s>
<s>
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
společnosti	společnost	k1gFnSc2
Scandinavian	Scandinavian	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
Airbus	airbus	k1gInSc1
navázal	navázat	k5eAaPmAgInS
na	na	k7c4
úspěch	úspěch	k1gInSc4
předchozích	předchozí	k2eAgFnPc2d1
řad	řada	k1gFnPc2
novou	nový	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
od	od	k7c2
té	ten	k3xDgFnSc2
starší	starý	k2eAgFnSc2d2
liší	lišit	k5eAaImIp3nP
především	především	k6eAd1
novými	nový	k2eAgInPc7d1
motory	motor	k1gInPc7
<g/>
,	,	kIx,
lepšími	dobrý	k2eAgFnPc7d2
aerodynamickými	aerodynamický	k2eAgFnPc7d1
vlažnostmi	vlažnost	k1gFnPc7
<g/>
,	,	kIx,
širší	široký	k2eAgInSc1d2
kabinou	kabina	k1gFnSc7
<g/>
,	,	kIx,
lepšími	dobrý	k2eAgFnPc7d2
zvukovými	zvukový	k2eAgFnPc7d1
izolačními	izolační	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
<g/>
,	,	kIx,
větším	veliký	k2eAgInSc7d2
počtem	počet	k1gInSc7
míst	místo	k1gNnPc2
a	a	k8xC
celkovými	celkový	k2eAgInPc7d1
sníženými	snížený	k2eAgInPc7d1
náklady	náklad	k1gInPc7
na	na	k7c4
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neo	Neo	k1gFnSc1
v	v	k7c6
názvu	název	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
new	new	k?
engine	enginout	k5eAaPmIp3nS
option	option	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
překladu	překlad	k1gInSc6
nová	nový	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
motoru	motor	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
řady	řada	k1gFnSc2
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
je	být	k5eAaImIp3nS
totiž	totiž	k9
možno	možno	k6eAd1
si	se	k3xPyFc3
vybrat	vybrat	k5eAaPmF
z	z	k7c2
motorů	motor	k1gInPc2
CFMI	CFMI	kA
LEAP-1A	LEAP-1A	k1gFnSc2
nebo	nebo	k8xC
Pratt	Pratt	k2eAgMnSc1d1
&	&	k?
Whitney	Whitney	k1gInPc4
PW	PW	kA
<g/>
1000	#num#	k4
<g/>
G.	G.	kA
První	první	k4xOgInSc4
let	let	k1gInSc4
verze	verze	k1gFnSc2
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
<g/>
,	,	kIx,
verze	verze	k1gFnSc1
A	a	k9
<g/>
319	#num#	k4
<g/>
neo	neo	k?
poprvé	poprvé	k6eAd1
vzlétla	vzlétnout	k5eAaPmAgFnS
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
a	a	k8xC
verze	verze	k1gFnPc1
A	a	k9
<g/>
321	#num#	k4
<g/>
neo	neo	k?
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
A	A	kA
<g/>
318	#num#	k4
<g/>
neo	neo	k?
není	být	k5eNaImIp3nS
v	v	k7c6
plánu	plán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
již	již	k9
přes	přes	k7c4
100	#num#	k4
kusů	kus	k1gInPc2
tohoto	tento	k3xDgInSc2
nového	nový	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvními	první	k4xOgFnPc7
provozovateli	provozovatel	k1gMnSc3
byly	být	k5eAaImAgFnP
společnosti	společnost	k1gFnPc1
Lufthansa	Lufthansa	k1gFnSc1
<g/>
,	,	kIx,
GoAir	GoAir	k1gInSc1
<g/>
,	,	kIx,
IndiGo	indigo	k1gNnSc1
<g/>
,	,	kIx,
Pegasus	Pegasus	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
,	,	kIx,
Scandinavian	Scandinavian	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
,	,	kIx,
AirAsia	AirAsia	k1gFnSc1
a	a	k8xC
Frontier	Frontier	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
řada	řada	k1gFnSc1
konkuruje	konkurovat	k5eAaImIp3nS
především	především	k9
letounům	letoun	k1gMnPc3
Boeing	boeing	k1gInSc4
737	#num#	k4
MAX	max	kA
<g/>
.	.	kIx.
</s>
<s>
Nehody	nehoda	k1gFnPc1
a	a	k8xC
incidenty	incident	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
Údaje	údaj	k1gInPc1
k	k	k7c3
lednu	leden	k1gInSc3
2018	#num#	k4
souhrn	souhrn	k1gInSc1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
typy	typ	k1gInPc4
rodiny	rodina	k1gFnSc2
A	a	k9
<g/>
320	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ztráty	ztráta	k1gFnPc1
letounu	letoun	k1gInSc2
<g/>
:	:	kIx,
41	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
celkem	celek	k1gInSc7
1393	#num#	k4
mrtvými	mrtvý	k2eAgNnPc7d1
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
<g/>
:	:	kIx,
83	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Únosy	únos	k1gInPc1
<g/>
:	:	kIx,
15	#num#	k4
</s>
<s>
Specifikace	specifikace	k1gFnSc1
</s>
<s>
Letouny	letoun	k1gInPc4
Airbus	airbus	k1gInSc1
A32X	A32X	k1gMnPc2
</s>
<s>
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
za	za	k7c2
letu	let	k1gInSc2
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
A	a	k9
<g/>
318	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Posádka	posádka	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
Exit	exit	k1gInSc1
limit	limit	k1gInSc1
EASA	EASA	kA
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
/	/	kIx~
<g/>
FAA	FAA	kA
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
136	#num#	k4
</s>
<s>
160	#num#	k4
</s>
<s>
195	#num#	k4
<g/>
/	/	kIx~
<g/>
190	#num#	k4
</s>
<s>
230	#num#	k4
</s>
<s>
1	#num#	k4
třída	třída	k1gFnSc1
<g/>
,	,	kIx,
maximální	maximální	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
132	#num#	k4
u	u	k7c2
29	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
in	in	k?
(	(	kIx(
<g/>
74	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
sedadla	sedadlo	k1gNnPc4
</s>
<s>
156	#num#	k4
u	u	k7c2
28	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
in	in	k?
(	(	kIx(
<g/>
71	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
sedadla	sedadlo	k1gNnPc4
</s>
<s>
186	#num#	k4
u	u	k7c2
29	#num#	k4
in	in	k?
(	(	kIx(
<g/>
74	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
sedadla	sedadlo	k1gNnPc4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
236	#num#	k4
při	při	k7c6
28	#num#	k4
in	in	k?
(	(	kIx(
<g/>
71	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
pitch	pitch	k1gMnSc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
třída	třída	k1gFnSc1
<g/>
,	,	kIx,
typická	typický	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
117	#num#	k4
u	u	k7c2
32	#num#	k4
in	in	k?
(	(	kIx(
<g/>
81	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
sedadla	sedadlo	k1gNnPc4
</s>
<s>
134	#num#	k4
u	u	k7c2
32	#num#	k4
in	in	k?
(	(	kIx(
<g/>
81	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
sedadla	sedadlo	k1gNnPc4
</s>
<s>
164	#num#	k4
u	u	k7c2
32	#num#	k4
in	in	k?
(	(	kIx(
<g/>
81	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
sedadla	sedadlo	k1gNnPc4
</s>
<s>
199	#num#	k4
u	u	k7c2
32	#num#	k4
in	in	k?
(	(	kIx(
<g/>
81	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
sedadla	sedadlo	k1gNnPc4
</s>
<s>
2	#num#	k4
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
typická	typický	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
107	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
F	F	kA
@	@	kIx~
38	#num#	k4
in	in	k?
<g/>
,	,	kIx,
99Y	99Y	k4
@	@	kIx~
32	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
124	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
F	F	kA
@	@	kIx~
38	#num#	k4
in	in	k?
<g/>
,	,	kIx,
116Y	116Y	k4
@	@	kIx~
32	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
150	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
F	F	kA
@	@	kIx~
36	#num#	k4
in	in	k?
<g/>
,	,	kIx,
138Y	138Y	k4
@	@	kIx~
32	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
185	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
F	F	kA
@	@	kIx~
36	#num#	k4
in	in	k?
<g/>
,	,	kIx,
169Y	169Y	k4
@	@	kIx~
32	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
Objem	objem	k1gInSc1
nákladového	nákladový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
</s>
<s>
21,20	21,20	k4
m	m	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
749	#num#	k4
cu	cu	k?
ft	ft	k?
<g/>
)	)	kIx)
</s>
<s>
27,70	27,70	k4
m	m	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
978	#num#	k4
cu	cu	k?
ft	ft	k?
<g/>
)	)	kIx)
</s>
<s>
37,40	37,40	k4
m	m	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
321	#num#	k4
cu	cu	k?
ft	ft	k?
<g/>
)	)	kIx)
</s>
<s>
51,70	51,70	k4
m	m	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
826	#num#	k4
cu	cu	k?
ft	ft	k?
<g/>
)	)	kIx)
</s>
<s>
ULD	ULD	kA
(	(	kIx(
<g/>
Universal	Universal	k1gFnPc2
loading	loading	k1gInSc1
device	device	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
X	X	kA
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
LD3-45	LD3-45	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
LD3-45	LD3-45	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
×	×	k?
LD3-45	LD3-45	k1gFnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
31,44	31,44	k4
m	m	kA
(	(	kIx(
<g/>
103	#num#	k4
ft	ft	k?
2	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
33,84	33,84	k4
m	m	kA
(	(	kIx(
<g/>
111	#num#	k4
ft	ft	k?
0	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
37,57	37,57	k4
m	m	kA
(	(	kIx(
<g/>
123	#num#	k4
ft	ft	k?
3	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
44,51	44,51	k4
m	m	kA
(	(	kIx(
<g/>
146	#num#	k4
ft	ft	k?
0	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
Rozvor	rozvor	k1gInSc1
</s>
<s>
10,25	10,25	k4
m	m	kA
(	(	kIx(
<g/>
33	#num#	k4
ft	ft	k?
8	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
11,04	11,04	k4
m	m	kA
(	(	kIx(
<g/>
36	#num#	k4
ft	ft	k?
3	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
12,64	12,64	k4
m	m	kA
(	(	kIx(
<g/>
41	#num#	k4
ft	ft	k?
6	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
16,91	16,91	k4
m	m	kA
(	(	kIx(
<g/>
55	#num#	k4
ft	ft	k?
6	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
Rozchod	rozchod	k1gInSc1
</s>
<s>
7,59	7,59	k4
m	m	kA
(	(	kIx(
<g/>
24	#num#	k4
ft	ft	k?
11	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
</s>
<s>
34,10	34,10	k4
m	m	kA
(	(	kIx(
<g/>
111	#num#	k4
ft	ft	k?
11	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
35,8	35,8	k4
m	m	kA
(	(	kIx(
<g/>
117	#num#	k4
ft	ft	k?
5	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
křídel	křídlo	k1gNnPc2
</s>
<s>
124	#num#	k4
m²	m²	k?
(	(	kIx(
<g/>
1,330	1,330	k4
sq	sq	k?
ft	ft	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
10,3	10,3	k4
štíhlost	štíhlost	k1gFnSc1
křídla	křídlo	k1gNnSc2
</s>
<s>
128	#num#	k4
m²	m²	k?
(	(	kIx(
<g/>
1,380	1,380	k4
sq	sq	k?
ft	ft	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
10	#num#	k4
ŠK	ŠK	kA
</s>
<s>
Šípovitost	Šípovitost	k1gFnSc1
křídla	křídlo	k1gNnSc2
</s>
<s>
25	#num#	k4
stupňů	stupeň	k1gInPc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výška	výška	k1gFnSc1
ocasu	ocas	k1gInSc2
</s>
<s>
12,56	12,56	k4
m	m	kA
(	(	kIx(
<g/>
41	#num#	k4
ft	ft	k?
2	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
11,76	11,76	k4
m	m	kA
(	(	kIx(
<g/>
38	#num#	k4
ft	ft	k?
7	#num#	k4
in	in	k?
<g/>
)	)	kIx)
</s>
<s>
Trup	trup	k1gMnSc1
</s>
<s>
4,14	4,14	k4
m	m	kA
(	(	kIx(
<g/>
13	#num#	k4
ft	ft	k?
7	#num#	k4
in	in	k?
<g/>
)	)	kIx)
výška	výška	k1gFnSc1
<g/>
,	,	kIx,
3,95	3,95	k4
m	m	kA
(	(	kIx(
<g/>
13	#num#	k4
ft	ft	k?
0	#num#	k4
in	in	k?
<g/>
)	)	kIx)
šířka	šířka	k1gFnSc1
<g/>
,	,	kIx,
3,70	3,70	k4
m	m	kA
(	(	kIx(
<g/>
12	#num#	k4
ft	ft	k?
2	#num#	k4
in	in	k?
<g/>
)	)	kIx)
šířka	šířka	k1gFnSc1
kabiny	kabina	k1gFnSc2
</s>
<s>
MTOW	MTOW	kA
</s>
<s>
68	#num#	k4
t	t	k?
(	(	kIx(
<g/>
150	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
75,5	75,5	k4
t	t	k?
(	(	kIx(
<g/>
166	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
78	#num#	k4
t	t	k?
(	(	kIx(
<g/>
172	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
93,5	93,5	k4
t	t	k?
(	(	kIx(
<g/>
206	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
Max	max	kA
<g/>
.	.	kIx.
užitečné	užitečný	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
</s>
<s>
15	#num#	k4
t	t	k?
(	(	kIx(
<g/>
33	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
17,7	17,7	k4
t	t	k?
(	(	kIx(
<g/>
39	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
19,9	19,9	k4
t	t	k?
(	(	kIx(
<g/>
44	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
25,3	25,3	k4
t	t	k?
(	(	kIx(
<g/>
56	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
Množství	množství	k1gNnSc1
paliva	palivo	k1gNnSc2
</s>
<s>
24	#num#	k4
210	#num#	k4
L6	L6	k1gFnPc2
400	#num#	k4
US	US	kA
gal	gal	k?
</s>
<s>
24	#num#	k4
210	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
190	#num#	k4
L6	L6	k1gFnPc2
400	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
980	#num#	k4
US	US	kA
gal	gal	k?
</s>
<s>
24	#num#	k4
210	#num#	k4
<g/>
–	–	k?
<g/>
27	#num#	k4
200	#num#	k4
L6	L6	k1gFnPc2
400	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
190	#num#	k4
US	US	kA
gal	gal	k?
</s>
<s>
24	#num#	k4
0	#num#	k4
<g/>
50	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
030	#num#	k4
L6	L6	k1gFnPc2
350	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
930	#num#	k4
US	US	kA
gal	gal	k?
</s>
<s>
Operační	operační	k2eAgFnSc1d1
prázdná	prázdný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
39,5	39,5	k4
t	t	k?
(	(	kIx(
<g/>
87	#num#	k4
100	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
40,8	40,8	k4
t	t	k?
(	(	kIx(
<g/>
89	#num#	k4
900	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
42,6	42,6	k4
t	t	k?
(	(	kIx(
<g/>
93	#num#	k4
900	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
48,5	48,5	k4
t	t	k?
(	(	kIx(
<g/>
107	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
34,5	34,5	k4
t	t	k?
(	(	kIx(
<g/>
76	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
35,4	35,4	k4
t	t	k?
(	(	kIx(
<g/>
78	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
37,23	37,23	k4
t	t	k?
(	(	kIx(
<g/>
82	#num#	k4
100	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
47,5	47,5	k4
t	t	k?
(	(	kIx(
<g/>
105	#num#	k4
000	#num#	k4
lb	lb	k?
<g/>
)	)	kIx)
</s>
<s>
Rychlost	rychlost	k1gFnSc1
</s>
<s>
Cestovní	cestovní	k2eAgFnPc1d1
<g/>
:	:	kIx,
Mach	Mach	k1gMnSc1
0,78	0,78	k4
(	(	kIx(
<g/>
447	#num#	k4
kn	kn	k?
<g/>
;	;	kIx,
829	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
MMO	MMO	kA
<g/>
:	:	kIx,
Mach	Mach	k1gMnSc1
0,82	0,82	k4
(	(	kIx(
<g/>
470	#num#	k4
kn	kn	k?
<g/>
;	;	kIx,
871	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dolet	dolet	k1gInSc1
s	s	k7c7
obvyklou	obvyklý	k2eAgFnSc7d1
zátěží	zátěž	k1gFnSc7
</s>
<s>
3	#num#	k4
100	#num#	k4
nmi	nmi	k?
<g/>
,	,	kIx,
5	#num#	k4
750	#num#	k4
km	km	kA
</s>
<s>
3	#num#	k4
750	#num#	k4
nmi	nmi	k?
<g/>
,	,	kIx,
6	#num#	k4
950	#num#	k4
km	km	kA
</s>
<s>
3	#num#	k4
300	#num#	k4
nmi	nmi	k?
<g/>
,	,	kIx,
6	#num#	k4
100	#num#	k4
km	km	kA
</s>
<s>
3	#num#	k4
200	#num#	k4
nmi	nmi	k?
<g/>
,	,	kIx,
5	#num#	k4
950	#num#	k4
km	km	kA
</s>
<s>
Dolet	dolet	k1gInSc1
ACJ	ACJ	kA
</s>
<s>
4	#num#	k4
200	#num#	k4
nmi	nmi	k?
<g/>
,	,	kIx,
7	#num#	k4
800	#num#	k4
km	km	kA
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
000	#num#	k4
nmi	nmi	k?
<g/>
,	,	kIx,
11	#num#	k4
100	#num#	k4
km	km	kA
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
300	#num#	k4
nmi	nmi	k?
<g/>
,	,	kIx,
7	#num#	k4
800	#num#	k4
km	km	kA
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Délka	délka	k1gFnSc1
vzletové	vzletový	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
MTOW	MTOW	kA
<g/>
,	,	kIx,
SL	SL	kA
<g/>
,	,	kIx,
ISA	ISA	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
780	#num#	k4
m	m	kA
(	(	kIx(
<g/>
5	#num#	k4
840	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
850	#num#	k4
m	m	kA
(	(	kIx(
<g/>
6	#num#	k4
070	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
100	#num#	k4
m	m	kA
(	(	kIx(
<g/>
6	#num#	k4
900	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Délka	délka	k1gFnSc1
přistávací	přistávací	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
MLW	MLW	kA
<g/>
,	,	kIx,
SL	SL	kA
<g/>
,	,	kIx,
ISA	ISA	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
230	#num#	k4
m	m	kA
(	(	kIx(
<g/>
4	#num#	k4
040	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
360	#num#	k4
m	m	kA
(	(	kIx(
<g/>
4	#num#	k4
460	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
500	#num#	k4
m	m	kA
(	(	kIx(
<g/>
4	#num#	k4
900	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dostup	dostup	k1gInSc1
</s>
<s>
39,100	39,100	k4
<g/>
–	–	k?
<g/>
41,000	41,000	k4
ft	ft	k?
(	(	kIx(
<g/>
11,918	11,918	k4
<g/>
–	–	k?
<g/>
12,497	12,497	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Motory	motor	k1gInPc1
(	(	kIx(
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CFM	CFM	kA
International	International	k1gMnSc1
CFM	CFM	kA
<g/>
56	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
68,3	68,3	k4
in	in	k?
(	(	kIx(
<g/>
1,73	1,73	k4
m	m	kA
<g/>
)	)	kIx)
dmychadlo	dmychadlo	k1gNnSc1
</s>
<s>
PW	PW	kA
<g/>
6000	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
56,5	56,5	k4
in	in	k?
(	(	kIx(
<g/>
1,44	1,44	k4
m	m	kA
<g/>
)	)	kIx)
dmychadlo	dmychadlo	k1gNnSc1
</s>
<s>
IAE	IAE	kA
V	v	k7c6
<g/>
2500	#num#	k4
<g/>
A	a	k9
<g/>
5	#num#	k4
<g/>
,	,	kIx,
63,5	63,5	k4
in	in	k?
(	(	kIx(
<g/>
1,61	1,61	k4
m	m	kA
<g/>
)	)	kIx)
dmychadlo	dmychadlo	k1gNnSc1
</s>
<s>
Tah	tah	k1gInSc1
(	(	kIx(
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
96	#num#	k4
<g/>
–	–	k?
<g/>
106	#num#	k4
kN	kN	k?
(	(	kIx(
<g/>
22	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
000	#num#	k4
lbf	lbf	k?
<g/>
)	)	kIx)
</s>
<s>
98	#num#	k4
<g/>
–	–	k?
<g/>
120	#num#	k4
kN	kN	k?
(	(	kIx(
<g/>
22	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
27	#num#	k4
000	#num#	k4
lbf	lbf	k?
<g/>
)	)	kIx)
</s>
<s>
133	#num#	k4
<g/>
–	–	k?
<g/>
147	#num#	k4
kN	kN	k?
(	(	kIx(
<g/>
30	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
000	#num#	k4
lbf	lbf	k?
<g/>
)	)	kIx)
</s>
<s>
Motory	motor	k1gInPc1
</s>
<s>
ModelCertifikaceMotory	ModelCertifikaceMotor	k1gInPc1
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
<g/>
318	#num#	k4
<g/>
-	-	kIx~
<g/>
11123	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2003	#num#	k4
<g/>
CFM	CFM	kA
<g/>
56	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
B	B	kA
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
318	#num#	k4
<g/>
-	-	kIx~
<g/>
11223	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2003	#num#	k4
<g/>
CFM	CFM	kA
<g/>
56	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
B	B	kA
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
318	#num#	k4
<g/>
-	-	kIx~
<g/>
12121	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005PW6122A	2005PW6122A	k4
</s>
<s>
A	a	k9
<g/>
318	#num#	k4
<g/>
-	-	kIx~
<g/>
12221	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005PW6124A	2005PW6124A	k4
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
11110	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1996CFM56-5B5	1996CFM56-5B5	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
11210	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1996CFM56-5B6	1996CFM56-5B6	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
11331	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1996CFM56-5A4	1996CFM56-5A4	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
A	a	k9
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
F	F	kA
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
11431	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1996CFM56-5A5	1996CFM56-5A5	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
A	a	k9
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
F	F	kA
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
11530	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1999CFM56-5B7	1999CFM56-5B7	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
13118	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1996IAE	1996IAE	k4
Model	model	k1gInSc1
V2522-A5	V2522-A5	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
13218	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1996IAE	1996IAE	k4
Model	model	k1gInSc1
V2524-A5	V2524-A5	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
13330	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1999IAE	1999IAE	k4
Model	model	k1gInSc1
V2527M-A5	V2527M-A5	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
11126	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1988CFM56-5A1	1988CFM56-5A1	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
A	a	k9
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
F	F	kA
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
2118	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1988CFM56-5A1	1988CFM56-5A1	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
A	a	k9
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
F	F	kA
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
21220	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990CFM56-5A3	1990CFM56-5A3	k4
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
21410	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1995CFM56-5B4	1995CFM56-5B4	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
21522	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2006CFM56-5B5	2006CFM56-5B5	k4
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
21614	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2006CFM56-5B6	2006CFM56-5B6	k4
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
23120	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1989IAE	1989IAE	k4
Model	model	k1gInSc1
V2500-A1	V2500-A1	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
23228	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1993IAE	1993IAE	k4
Model	modla	k1gFnPc2
V2527-A5	V2527-A5	k1gFnPc2
</s>
<s>
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
23312	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1996IAE	1996IAE	k4
Model	model	k1gInSc1
V2527E-A5	V2527E-A5	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
11127	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1995CFM56-5B1	1995CFM56-5B1	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
11215	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1995CFM56-5B2	1995CFM56-5B2	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
13117	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1993IAE	1993IAE	k4
Model	model	k1gInSc1
V2530-A5	V2530-A5	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
21120	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1997CFM56-5B3	1997CFM56-5B3	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
21231	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2001CFM56-5B1	2001CFM56-5B1	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
21331	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2001CFM56-5B2	2001CFM56-5B2	k4
nebo	nebo	k8xC
5	#num#	k4
<g/>
B	B	kA
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
23120	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1997IAE	1997IAE	k4
Model	model	k1gInSc1
V2533-A5	V2533-A5	k1gFnSc2
</s>
<s>
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
23231	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2001IAE	2001IAE	k4
Model	model	k1gInSc1
V2530-A5	V2530-A5	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Šablona	šablona	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Cite	cit	k1gInSc5
magazine	magazinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Smlouva	smlouva	k1gFnSc1
s	s	k7c7
firmou	firma	k1gFnSc7
AIRBUS	airbus	k1gInSc4
podepsána	podepsat	k5eAaPmNgFnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-03-09	2006-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
army	arma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Nebeský	nebeský	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
<g/>
“	“	k?
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
do	do	k7c2
vzduchu	vzduch	k1gInSc2
s	s	k7c7
českými	český	k2eAgMnPc7d1
politiky	politik	k1gMnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-10-20	2007-10-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
British	British	k1gInSc1
Airways	Airways	k1gInSc1
confirms	confirms	k1gInSc1
end	end	k?
of	of	k?
all	all	k?
business	business	k1gInSc1
class	classa	k1gFnPc2
LCY-JFK	LCY-JFK	k1gFnSc2
service	service	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-08-03	2020-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
businesstraveller	businesstraveller	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
319	#num#	k4
<g/>
/	/	kIx~
<g/>
320	#num#	k4
<g/>
/	/	kIx~
<g/>
321	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aviation	Aviation	k1gInSc4
Safety	Safeta	k1gFnSc2
Network	network	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
A318	A318	k1gFnSc1
Dimensions	Dimensions	k1gInSc1
&	&	k?
key	key	k?
data	datum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
A319	A319	k1gFnSc1
Dimensions	Dimensions	k1gInSc1
&	&	k?
key	key	k?
data	datum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
A320	A320	k1gFnSc1
Dimensions	Dimensions	k1gInSc1
&	&	k?
key	key	k?
data	datum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
A321	A321	k1gFnSc1
Dimensions	Dimensions	k1gInSc1
&	&	k?
key	key	k?
data	datum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Type	typ	k1gInSc5
Certificate	Certificat	k1gInSc5
Data	datum	k1gNnPc1
Sheet	Sheeta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EASA	EASA	kA
<g/>
,	,	kIx,
28	#num#	k4
June	jun	k1gMnSc5
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
15	#num#	k4
September	September	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Type	typ	k1gInSc5
Certificate	Certificat	k1gInSc5
Data	datum	k1gNnPc1
Sheet	Sheeta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FAA	FAA	kA
<g/>
,	,	kIx,
August	August	k1gMnSc1
12	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
All	All	k1gMnSc1
About	About	k1gMnSc1
the	the	k?
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
Family	Famila	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
High-density	High-densit	k1gInPc4
A	A	kA
<g/>
320	#num#	k4
<g/>
s	s	k7c7
for	forum	k1gNnPc2
easyJet	easyJet	k1gMnSc1
will	will	k1gMnSc1
retain	retain	k1gMnSc1
seat	seat	k1gMnSc1
pitch	pitch	k1gMnSc1
<g/>
,	,	kIx,
assures	assures	k1gMnSc1
airline	airlin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Runway	runway	k1gFnSc1
Girl	girl	k1gFnSc1
Network	network	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
March	March	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Airbus	airbus	k1gInSc1
Studies	Studies	k1gInSc1
236	#num#	k4
<g/>
-Seat	-Seat	k1gInSc4
A	a	k9
<g/>
321	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aviation	Aviation	k1gInSc1
Week	Week	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
10	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Airbus	airbus	k1gInSc1
Aircraft	Aircrafta	k1gFnPc2
Data	datum	k1gNnSc2
File	File	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elsevier	Elsevira	k1gFnPc2
<g/>
,	,	kIx,
July	Jula	k1gFnSc2
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
A320	A320	k1gFnPc2
Family	Famila	k1gFnSc2
Technology	technolog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
ACJ318	ACJ318	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
1	#num#	k4
December	December	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
ACJ319	ACJ319	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
5	#num#	k4
January	Januara	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
ACJ320	ACJ320	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airbus	airbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
6	#num#	k4
December	December	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Airbus	airbus	k1gInSc1
A320	A320	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Detaily	detail	k1gInPc1
o	o	k7c6
letounech	letoun	k1gInPc6
rodiny	rodina	k1gFnSc2
Airbus	airbus	k1gInSc4
A320	A320	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Historie	historie	k1gFnSc1
a	a	k8xC
fotografie	fotografie	k1gFnSc1
Airbusu	airbus	k1gInSc2
A	a	k9
<g/>
319	#num#	k4
<g/>
,	,	kIx,
A320	A320	k1gFnSc1
a	a	k8xC
A321	A321	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Fotografie	fotografia	k1gFnPc1
Airbus	airbus	k1gInSc4
A-319CJ	A-319CJ	k1gFnSc2
ve	v	k7c6
vysokém	vysoký	k2eAgNnSc6d1
rozlišení	rozlišení	k1gNnSc6
(	(	kIx(
<g/>
HD	HD	kA
<g/>
)	)	kIx)
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
<g/>
zDL	zDL	k?
letiště	letiště	k1gNnSc2
Praha	Praha	k1gFnSc1
Kbely	Kbely	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Airbus	airbus	k1gInSc1
A318	A318	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Aircraft-Info	Aircraft-Info	k6eAd1
<g/>
.	.	kIx.
<g/>
net	net	k?
–	–	k?
Airbus	airbus	k1gInSc4
A320	A320	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
vyrobených	vyrobený	k2eAgInPc2d1
Airbusů	airbus	k1gInPc2
A	a	k9
<g/>
318	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
A321	A321	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Airliners	Airliners	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
–	–	k?
Článek	článek	k1gInSc1
o	o	k7c4
A320	A320	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Interaktivní	interaktivní	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
kokpitu	kokpit	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gMnSc1
A320	A320	k1gMnSc1
Project	Project	k1gMnSc1
Website	Websit	k1gInSc5
(	(	kIx(
<g/>
Simulátor	simulátor	k1gInSc4
A	a	k9
<g/>
320	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Databáze	databáze	k1gFnSc1
nehod	nehoda	k1gFnPc2
A320	A320	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
30	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Vojenské	vojenský	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yRgNnSc7,k3yQgNnSc7
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
svézt	svézt	k5eAaPmF
i	i	k8xC
vy	vy	k3xPp2nPc1
–	–	k?
jako	jako	k9
pacient	pacient	k1gMnSc1
–	–	k?
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
článek	článek	k1gInSc1
a	a	k8xC
fotogalerie	fotogalerie	k1gFnSc1
A319	A319	k1gFnSc2
CJ	CJ	kA
Armády	armáda	k1gFnSc2
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Letouny	letoun	k1gInPc4
Airbus	airbus	k1gInSc1
Civilní	civilní	k2eAgNnSc5d1
</s>
<s>
A	a	k9
<g/>
220	#num#	k4
<g/>
-	-	kIx~
<g/>
100	#num#	k4
<g/>
,	,	kIx,
A220-300	A220-300	k1gMnSc1
•	•	k?
A300	A300	k1gMnSc1
•	•	k?
A310	A310	k1gMnSc1
•	•	k?
A	a	k9
<g/>
318	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
A321	A321	k1gFnSc1
•	•	k?
A	a	k9
<g/>
319	#num#	k4
<g/>
neo	neo	k?
<g/>
,	,	kIx,
A	A	kA
<g/>
320	#num#	k4
<g/>
neo	neo	k?
<g/>
,	,	kIx,
A	A	kA
<g/>
321	#num#	k4
<g/>
neo	neo	k?
•	•	k?
A330	A330	k1gMnSc1
•	•	k?
A	a	k9
<g/>
330	#num#	k4
<g/>
neo	neo	k?
•	•	k?
A340	A340	k1gMnSc1
•	•	k?
A350	A350	k1gMnSc1
•	•	k?
A380	A380	k1gMnSc1
•	•	k?
Beluga	Beluga	k1gFnSc1
•	•	k?
Beluga	Beluga	k1gFnSc1
XL	XL	kA
Vojenské	vojenský	k2eAgFnSc2d1
</s>
<s>
A310	A310	k4
MRTT	MRTT	kA
•	•	k?
CC-150	CC-150	k1gMnSc1
Polaris	Polaris	k1gFnSc2
•	•	k?
A330	A330	k1gFnSc2
MRTT	MRTT	kA
•	•	k?
A400M	A400M	k1gMnSc1
Atlas	Atlas	k1gMnSc1
•	•	k?
C212	C212	k1gMnSc1
•	•	k?
CN235	CN235	k1gMnSc1
•	•	k?
C295	C295	k1gMnSc1
•	•	k?
HC-144	HC-144	k1gMnSc1
•	•	k?
KC-45	KC-45	k1gMnSc1
•	•	k?
Mako	mako	k1gNnSc4
<g/>
/	/	kIx~
<g/>
HEAT	HEAT	kA
•	•	k?
Eurofighter	Eurofighter	k1gMnSc1
Typhoon	Typhoon	k1gMnSc1
</s>
<s>
Vojenská	vojenský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
Stíhací	stíhací	k2eAgInSc4d1
a	a	k8xC
víceúčelové	víceúčelový	k2eAgInPc4d1
bojové	bojový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
JAS-39	JAS-39	k4
</s>
<s>
MiG-	MiG-	k?
<g/>
21	#num#	k4
</s>
<s>
MiG-	MiG-	k?
<g/>
23	#num#	k4
<g/>
MF	MF	kA
<g/>
/	/	kIx~
<g/>
ML	ml	kA
</s>
<s>
MiG-	MiG-	k?
<g/>
29	#num#	k4
Bitevní	bitevní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
a	a	k8xC
bombardéry	bombardér	k1gInPc4
</s>
<s>
L-159	L-159	k4
</s>
<s>
MiG-	MiG-	k?
<g/>
23	#num#	k4
<g/>
BN	BN	kA
</s>
<s>
Su-	Su-	k?
<g/>
22	#num#	k4
</s>
<s>
Su-	Su-	k?
<g/>
25	#num#	k4
Dopravní	dopravní	k2eAgInPc1d1
a	a	k8xC
transportní	transportní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
A319	A319	k4
</s>
<s>
An-	An-	k?
<g/>
24	#num#	k4
</s>
<s>
An-	An-	k?
<g/>
26	#num#	k4
</s>
<s>
C-295	C-295	k4
</s>
<s>
CL-601	CL-601	k4
</s>
<s>
Jak-	Jak-	k?
<g/>
40	#num#	k4
</s>
<s>
L-410	L-410	k4
</s>
<s>
Tu-	Tu-	k?
<g/>
134	#num#	k4
</s>
<s>
Tu-	Tu-	k?
<g/>
154	#num#	k4
Vrtulníky	vrtulník	k1gInPc4
</s>
<s>
Enstrom	Enstrom	k1gInSc1
480	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
2	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
8	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
17	#num#	k4
</s>
<s>
Mi-	Mi-	k?
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
Mi-	Mi-	k1gFnSc2
<g/>
35	#num#	k4
</s>
<s>
W-3	W-3	k4
Cvičné	cvičný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
EV-97	EV-97	k4
</s>
<s>
L-29	L-29	k4
</s>
<s>
L-39	L-39	k4
</s>
<s>
L-39MS	L-39MS	k4
</s>
<s>
L-159T	L-159T	k4
</s>
<s>
Z-43	Z-43	k4
</s>
<s>
Z-142	Z-142	k4
Speciální	speciální	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
An-	An-	k?
<g/>
30	#num#	k4
</s>
<s>
Av-	Av-	k?
<g/>
14	#num#	k4
<g/>
FG	FG	kA
</s>
<s>
L-410FG	L-410FG	k4
Bezpilotní	bezpilotní	k2eAgInSc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
Raven	Raven	k1gInSc1
</s>
<s>
ScanEagle	ScanEagle	k6eAd1
</s>
<s>
Skylark	Skylark	k1gInSc1
</s>
<s>
Sojka	Sojka	k1gMnSc1
III	III	kA
</s>
<s>
Wasp	Wasp	k1gInSc4
Experimentální	experimentální	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
a	a	k8xC
prototypy	prototyp	k1gInPc4
</s>
<s>
L-39NG	L-39NG	k4
</s>
<s>
L-610	L-610	k4
Související	související	k2eAgInSc1d1
<g/>
:	:	kIx,
</s>
<s>
Centrum	centrum	k1gNnSc1
leteckého	letecký	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
</s>
<s>
Historie	historie	k1gFnSc1
letecké	letecký	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
AČR	AČR	kA
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
AČR	AČR	kA
</s>
<s>
Označení	označení	k1gNnSc1
letadel	letadlo	k1gNnPc2
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Itálie	Itálie	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
1	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
</s>
<s>
Q-1	Q-1	k4
</s>
<s>
G-2	G-2	k4
</s>
<s>
H-	H-	k?
<g/>
3	#num#	k4
<g/>
D	D	kA
<g/>
/	/	kIx~
<g/>
H-	H-	k1gMnSc1
<g/>
3	#num#	k4
<g/>
F	F	kA
</s>
<s>
G-4	G-4	k4
</s>
<s>
AV-8	AV-8	k4
</s>
<s>
Q-9	Q-9	k4
</s>
<s>
Q-10	Q-10	k4
</s>
<s>
A-11	A-11	k4
</s>
<s>
Q-11	Q-11	k4
</s>
<s>
Q-12	Q-12	k4
</s>
<s>
F-16	F-16	k4
</s>
<s>
G-17	G-17	k4
</s>
<s>
G-21	G-21	k4
</s>
<s>
Q-24	Q-24	k4
</s>
<s>
Q-25	Q-25	k4
</s>
<s>
Q-26	Q-26	k4
</s>
<s>
C-27	C-27	k4
</s>
<s>
Q-27	Q-27	k4
</s>
<s>
F-35	F-35	k4
</s>
<s>
C-	C-	k?
<g/>
42	#num#	k4
<g/>
/	/	kIx~
<g/>
P-	P-	k1gFnSc2
<g/>
42	#num#	k4
</s>
<s>
H-47	H-47	k4
</s>
<s>
C-50	C-50	k4
</s>
<s>
P-72	P-72	k4
</s>
<s>
H-90	H-90	k4
101	#num#	k4
<g/>
–	–	k?
<g/>
200	#num#	k4
</s>
<s>
H-101	H-101	k4
</s>
<s>
G-103	G-103	k4
</s>
<s>
H-109	H-109	k4
</s>
<s>
H-129	H-129	k4
</s>
<s>
C-130	C-130	k4
</s>
<s>
H-139	H-139	k4
</s>
<s>
U-166	U-166	k4
</s>
<s>
C-180	C-180	k4
</s>
<s>
A-200	A-200	k4
201	#num#	k4
<g/>
–	–	k?
<g/>
400	#num#	k4
</s>
<s>
H-205	H-205	k4
</s>
<s>
H-206	H-206	k4
</s>
<s>
U-208	U-208	k4
</s>
<s>
H-212	H-212	k4
</s>
<s>
C-222	C-222	k4
</s>
<s>
C-228	C-228	k4
</s>
<s>
T-260	T-260	k4
</s>
<s>
C-319	C-319	k4
</s>
<s>
T-339	T-339	k4
</s>
<s>
T-345	T-345	k4
</s>
<s>
T-346	T-346	k4
401	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
</s>
<s>
H-412	H-412	k4
</s>
<s>
H-500	H-500	k4
</s>
<s>
KC-707	KC-707	k4
</s>
<s>
KC-767	KC-767	k4
</s>
<s>
C-900	C-900	k4
</s>
<s>
P-1150	P-1150	k4
</s>
<s>
F-2000	F-2000	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
