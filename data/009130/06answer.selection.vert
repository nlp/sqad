<s>
Eucharistickou	eucharistický	k2eAgFnSc4d1	eucharistická
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
<g/>
,	,	kIx,	,
mši	mše	k1gFnSc4	mše
nebo	nebo	k8xC	nebo
Večeři	večeře	k1gFnSc4	večeře
Páně	páně	k2eAgNnSc2d1	páně
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jen	jen	k9	jen
zvlášť	zvlášť	k6eAd1	zvlášť
pověřená	pověřený	k2eAgFnSc1d1	pověřená
duchovní	duchovní	k2eAgFnSc1d1	duchovní
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
