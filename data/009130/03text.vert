<p>
<s>
Liturgie	liturgie	k1gFnSc1	liturgie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
λ	λ	k?	λ
veřejná	veřejný	k2eAgFnSc1d1	veřejná
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
z	z	k7c2	z
λ	λ	k?	λ
veřejný	veřejný	k2eAgInSc1d1	veřejný
a	a	k8xC	a
ε	ε	k?	ε
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
dnes	dnes	k6eAd1	dnes
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgNnSc4d1	veřejné
shromáždění	shromáždění	k1gNnSc4	shromáždění
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
toho	ten	k3xDgNnSc2	ten
kterého	který	k3yQgNnSc2	který
náboženství	náboženství	k1gNnSc2	náboženství
či	či	k8xC	či
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlům	pravidlo	k1gNnPc3	pravidlo
pro	pro	k7c4	pro
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ritus	ritus	k1gInSc1	ritus
nebo	nebo	k8xC	nebo
také	také	k9	také
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
soustavnému	soustavný	k2eAgNnSc3d1	soustavné
studiu	studio	k1gNnSc3	studio
liturgie	liturgie	k1gFnSc2	liturgie
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
odvětví	odvětví	k1gNnSc1	odvětví
teologie	teologie	k1gFnSc2	teologie
s	s	k7c7	s
názvem	název	k1gInSc7	název
liturgika	liturgika	k1gFnSc1	liturgika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
znamenala	znamenat	k5eAaImAgFnS	znamenat
liturgie	liturgie	k1gFnSc1	liturgie
břemeno	břemeno	k1gNnSc4	břemeno
či	či	k8xC	či
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
celé	celý	k2eAgFnSc2d1	celá
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ukládala	ukládat	k5eAaImAgFnS	ukládat
zejména	zejména	k9	zejména
bohatým	bohatý	k2eAgMnSc7d1	bohatý
občanům	občan	k1gMnPc3	občan
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
například	například	k6eAd1	například
o	o	k7c6	o
vystrojení	vystrojení	k1gNnSc6	vystrojení
veřejného	veřejný	k2eAgInSc2d1	veřejný
svátku	svátek	k1gInSc2	svátek
či	či	k8xC	či
oslavy	oslava	k1gFnSc2	oslava
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
o	o	k7c6	o
postavení	postavení	k1gNnSc6	postavení
a	a	k8xC	a
vyzbrojení	vyzbrojení	k1gNnSc6	vyzbrojení
válečné	válečný	k2eAgFnSc2d1	válečná
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Liturgie	liturgie	k1gFnSc1	liturgie
tak	tak	k9	tak
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
nahrazovala	nahrazovat	k5eAaImAgFnS	nahrazovat
placení	placení	k1gNnSc4	placení
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
však	však	k9	však
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
používal	používat	k5eAaImAgInS	používat
i	i	k9	i
pro	pro	k7c4	pro
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
neplacený	placený	k2eNgInSc4d1	neplacený
úřad	úřad	k1gInSc4	úřad
a	a	k8xC	a
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
liturgie	liturgie	k1gFnSc1	liturgie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
veřejné	veřejný	k2eAgFnPc1d1	veřejná
služby	služba	k1gFnPc1	služba
převzaly	převzít	k5eAaPmAgFnP	převzít
pojem	pojem	k1gInSc4	pojem
už	už	k6eAd1	už
starověké	starověký	k2eAgFnSc2d1	starověká
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
si	se	k3xPyFc3	se
budovaly	budovat	k5eAaImAgFnP	budovat
i	i	k8xC	i
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
ritus	ritus	k1gInSc1	ritus
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
pravidla	pravidlo	k1gNnPc4	pravidlo
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
čase	čas	k1gInSc6	čas
konání	konání	k1gNnSc2	konání
<g/>
,	,	kIx,	,
o	o	k7c6	o
osobách	osoba	k1gFnPc6	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
liturgii	liturgie	k1gFnSc4	liturgie
vykonávat	vykonávat	k5eAaImF	vykonávat
a	a	k8xC	a
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
mohou	moct	k5eAaImIp3nP	moct
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
účastnit	účastnit	k5eAaImF	účastnit
<g/>
,	,	kIx,	,
o	o	k7c6	o
potřebném	potřebný	k2eAgNnSc6d1	potřebné
oblečení	oblečení	k1gNnSc6	oblečení
(	(	kIx(	(
<g/>
rouchu	roucho	k1gNnSc6	roucho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náčiní	náčiní	k1gNnSc4	náčiní
a	a	k8xC	a
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
o	o	k7c6	o
postupu	postup	k1gInSc6	postup
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
<g/>
,	,	kIx,	,
o	o	k7c6	o
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
zpěvu	zpěv	k1gInSc6	zpěv
<g/>
,	,	kIx,	,
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
vznikala	vznikat	k5eAaImAgNnP	vznikat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
stával	stávat	k5eAaImAgInS	stávat
bohatším	bohatý	k2eAgInSc7d2	bohatší
a	a	k8xC	a
složitějším	složitý	k2eAgInSc7d2	složitější
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zařazovaly	zařazovat	k5eAaImAgFnP	zařazovat
další	další	k2eAgInPc1d1	další
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgInPc1d1	hlavní
typy	typ	k1gInPc1	typ
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
liturgie	liturgie	k1gFnSc2	liturgie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
hlavní	hlavní	k2eAgFnPc1d1	hlavní
složky	složka	k1gFnPc1	složka
a	a	k8xC	a
obsahy	obsah	k1gInPc1	obsah
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
i	i	k8xC	i
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
poměrně	poměrně	k6eAd1	poměrně
ustálily	ustálit	k5eAaPmAgFnP	ustálit
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
také	také	k9	také
sjednocovaly	sjednocovat	k5eAaImAgFnP	sjednocovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozdělení	rozdělení	k1gNnSc2	rozdělení
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
velkého	velký	k2eAgNnSc2d1	velké
schizmatu	schizma	k1gNnSc2	schizma
(	(	kIx(	(
<g/>
1054	[number]	k4	1054
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
liturgie	liturgie	k1gFnSc2	liturgie
ustálily	ustálit	k5eAaPmAgFnP	ustálit
v	v	k7c4	v
dosti	dosti	k6eAd1	dosti
odlišné	odlišný	k2eAgFnSc6d1	odlišná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jazykový	jazykový	k2eAgInSc1d1	jazykový
úzus	úzus	k1gInSc1	úzus
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
liturgií	liturgie	k1gFnSc7	liturgie
obvykle	obvykle	k6eAd1	obvykle
rozumí	rozumět	k5eAaImIp3nS	rozumět
především	především	k9	především
pravidla	pravidlo	k1gNnPc1	pravidlo
a	a	k8xC	a
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
liturgie	liturgie	k1gFnSc1	liturgie
<g/>
"	"	kIx"	"
i	i	k8xC	i
každé	každý	k3xTgNnSc4	každý
jednotlivé	jednotlivý	k2eAgNnSc4d1	jednotlivé
shromáždění	shromáždění	k1gNnSc4	shromáždění
(	(	kIx(	(
<g/>
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
církve	církev	k1gFnPc1	církev
mají	mít	k5eAaImIp3nP	mít
liturgii	liturgie	k1gFnSc4	liturgie
bohatší	bohatý	k2eAgFnSc4d2	bohatší
a	a	k8xC	a
složitější	složitý	k2eAgFnSc4d2	složitější
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
a	a	k8xC	a
anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
reformované	reformovaný	k2eAgFnSc2d1	reformovaná
církve	církev	k1gFnSc2	církev
velmi	velmi	k6eAd1	velmi
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kvakeři	kvaker	k1gMnPc1	kvaker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nějaká	nějaký	k3yIgFnSc1	nějaký
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
shromáždění	shromáždění	k1gNnPc1	shromáždění
s	s	k7c7	s
typizovaným	typizovaný	k2eAgInSc7d1	typizovaný
průběhem	průběh	k1gInSc7	průběh
a	a	k8xC	a
obsahem	obsah	k1gInSc7	obsah
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
církvích	církev	k1gFnPc6	církev
i	i	k8xC	i
v	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
náboženských	náboženský	k2eAgNnPc6d1	náboženské
společenstvích	společenství	k1gNnPc6	společenství
<g/>
,	,	kIx,	,
v	v	k7c6	v
islámu	islám	k1gInSc6	islám
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c6	o
liturgii	liturgie	k1gFnSc6	liturgie
nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgNnPc1d1	křesťanské
liturgická	liturgický	k2eAgNnPc1d1	liturgické
shromáždění	shromáždění	k1gNnPc1	shromáždění
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
jen	jen	k9	jen
modlitby	modlitba	k1gFnPc1	modlitba
<g/>
,	,	kIx,	,
zpěvy	zpěv	k1gInPc1	zpěv
a	a	k8xC	a
čtení	čtení	k1gNnSc1	čtení
<g/>
;	;	kIx,	;
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
</s>
</p>
<p>
<s>
Denní	denní	k2eAgFnSc1d1	denní
modlitba	modlitba	k1gFnSc1	modlitba
církve	církev	k1gFnSc2	církev
neboli	neboli	k8xC	neboli
liturgie	liturgie	k1gFnSc2	liturgie
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
společná	společný	k2eAgFnSc1d1	společná
modlitba	modlitba	k1gFnSc1	modlitba
breviáře	breviář	k1gInSc2	breviář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eucharistická	eucharistický	k2eAgFnSc1d1	eucharistická
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
<g/>
,	,	kIx,	,
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
liturgii	liturgie	k1gFnSc6	liturgie
mše	mše	k1gFnSc2	mše
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
božská	božská	k1gFnSc1	božská
liturgie	liturgie	k1gFnPc1	liturgie
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgMnSc1d1	zahrnující
také	také	k9	také
eucharistickou	eucharistický	k2eAgFnSc4d1	eucharistická
oběť	oběť	k1gFnSc4	oběť
a	a	k8xC	a
hostinu	hostina	k1gFnSc4	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Eucharistickou	eucharistický	k2eAgFnSc4d1	eucharistická
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
<g/>
,	,	kIx,	,
mši	mše	k1gFnSc4	mše
nebo	nebo	k8xC	nebo
Večeři	večeře	k1gFnSc4	večeře
Páně	páně	k2eAgNnSc2d1	páně
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jen	jen	k9	jen
zvlášť	zvlášť	k6eAd1	zvlášť
pověřená	pověřený	k2eAgFnSc1d1	pověřená
duchovní	duchovní	k2eAgFnSc1d1	duchovní
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgInPc1d1	jistý
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
západě	západ	k1gInSc6	západ
už	už	k6eAd1	už
v	v	k7c6	v
reformaci	reformace	k1gFnSc6	reformace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
katolická	katolický	k2eAgFnSc1d1	katolická
liturgie	liturgie	k1gFnSc1	liturgie
definitivně	definitivně	k6eAd1	definitivně
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
protestantských	protestantský	k2eAgFnPc6d1	protestantská
církvích	církev	k1gFnPc6	církev
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
zjednodušila	zjednodušit	k5eAaPmAgFnS	zjednodušit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
rozlišení	rozlišení	k1gNnSc1	rozlišení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
reformami	reforma	k1gFnPc7	reforma
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
anglikánské	anglikánský	k2eAgFnPc1d1	anglikánská
a	a	k8xC	a
luterské	luterský	k2eAgFnPc1d1	luterská
církve	církev	k1gFnPc1	církev
nepřijaly	přijmout	k5eNaPmAgFnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravoslavných	pravoslavný	k2eAgFnPc6d1	pravoslavná
církvích	církev	k1gFnPc6	církev
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
slouží	sloužit	k5eAaImIp3nS	sloužit
Liturgie	liturgie	k1gFnSc1	liturgie
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Zlatoústého	zlatoústý	k2eAgMnSc2d1	zlatoústý
<g/>
,	,	kIx,	,
nejobvyklejší	obvyklý	k2eAgFnSc1d3	nejobvyklejší
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
byzantského	byzantský	k2eAgInSc2d1	byzantský
ritu	rit	k1gInSc2	rit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
liturgií	liturgie	k1gFnPc2	liturgie
podává	podávat	k5eAaImIp3nS	podávat
heslo	heslo	k1gNnSc1	heslo
Ritus	ritus	k1gInSc1	ritus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Liturgy	Liturg	k1gInPc4	Liturg
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Berger	Berger	k1gMnSc1	Berger
<g/>
:	:	kIx,	:
Liturgický	liturgický	k2eAgInSc1d1	liturgický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
2008	[number]	k4	2008
-	-	kIx~	-
588	[number]	k4	588
str	str	kA	str
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7021-965-2	[number]	k4	978-80-7021-965-2
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Donghi	Dongh	k1gFnPc1	Dongh
<g/>
,	,	kIx,	,
Gesta	gesto	k1gNnPc1	gesto
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
<g/>
:	:	kIx,	:
úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
symbolické	symbolický	k2eAgFnSc2d1	symbolická
mluvy	mluva	k1gFnSc2	mluva
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
-	-	kIx~	-
93	[number]	k4	93
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7192-008-8	[number]	k4	80-7192-008-8
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Guardini	Guardin	k2eAgMnPc1d1	Guardin
<g/>
,	,	kIx,	,
O	o	k7c6	o
duchu	duch	k1gMnSc6	duch
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
-	-	kIx~	-
59	[number]	k4	59
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-85795-02-7	[number]	k4	80-85795-02-7
</s>
</p>
<p>
<s>
NOSEK	Nosek	k1gMnSc1	Nosek
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
;	;	kIx,	;
DAMOHORSKÁ	DAMOHORSKÁ	kA	DAMOHORSKÁ
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
synagogální	synagogální	k2eAgFnSc2d1	synagogální
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
986	[number]	k4	986
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
<g/>
,	,	kIx,	,
Duch	duch	k1gMnSc1	duch
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
-	-	kIx~	-
205	[number]	k4	205
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7364-032-5	[number]	k4	80-7364-032-5
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Liturgie	liturgie	k1gFnSc1	liturgie
a	a	k8xC	a
život	život	k1gInSc1	život
<g/>
:	:	kIx,	:
co	co	k3yRnSc4	co
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
vědět	vědět	k5eAaImF	vědět
o	o	k7c4	o
mši	mše	k1gFnSc4	mše
<g/>
,	,	kIx,	,
o	o	k7c6	o
církevním	církevní	k2eAgInSc6d1	církevní
roku	rok	k1gInSc6	rok
a	a	k8xC	a
smyslu	smysl	k1gInSc6	smysl
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
-	-	kIx~	-
183	[number]	k4	183
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7021-140-7	[number]	k4	80-7021-140-7
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
</s>
</p>
<p>
<s>
Liturgika	liturgika	k1gFnSc1	liturgika
</s>
</p>
<p>
<s>
Ritus	ritus	k1gInSc1	ritus
</s>
</p>
<p>
<s>
Liturgická	liturgický	k2eAgFnSc1d1	liturgická
barva	barva	k1gFnSc1	barva
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
liturgie	liturgie	k1gFnSc2	liturgie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc4d1	Česká
stránky	stránka	k1gFnPc4	stránka
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
liturgii	liturgie	k1gFnSc3	liturgie
</s>
</p>
<p>
<s>
Katecheze	katecheze	k1gFnSc1	katecheze
o	o	k7c4	o
liturgii	liturgie	k1gFnSc4	liturgie
a	a	k8xC	a
mši	mše	k1gFnSc4	mše
svaté	svatý	k2eAgFnSc2d1	svatá
</s>
</p>
<p>
<s>
http://liturgickahudba.szm.sk	[url]	k1gInSc1	http://liturgickahudba.szm.sk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnPc5	Encyclopedium
</s>
</p>
<p>
<s>
Orthodox	Orthodox	k1gInSc1	Orthodox
Tradition	Tradition	k1gInSc1	Tradition
and	and	k?	and
the	the	k?	the
Liturgy	Liturg	k1gInPc7	Liturg
</s>
</p>
<p>
<s>
Jewish	Jewish	k1gInSc1	Jewish
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
<g/>
:	:	kIx,	:
Liturgy	Liturg	k1gInPc7	Liturg
</s>
</p>
<p>
<s>
Contemporary	Contemporara	k1gFnPc1	Contemporara
Christian	Christian	k1gMnSc1	Christian
Liturgy	Liturg	k1gInPc1	Liturg
Website	Websit	k1gInSc5	Websit
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
liturgické	liturgický	k2eAgFnPc4d1	liturgická
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc6	teorie
a	a	k8xC	a
praxi	praxe	k1gFnSc6	praxe
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
tridentinská	tridentinský	k2eAgFnSc1d1	tridentinský
latinská	latinský	k2eAgFnSc1d1	Latinská
liturgie	liturgie	k1gFnSc1	liturgie
</s>
</p>
<p>
<s>
Liturgia	Liturgia	k1gFnSc1	Liturgia
<g/>
:	:	kIx,	:
forum	forum	k1gNnSc1	forum
</s>
</p>
