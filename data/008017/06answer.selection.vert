<s>
Kréta	Kréta	k1gFnSc1	Kréta
je	být	k5eAaImIp3nS	být
hornatý	hornatý	k2eAgInSc4d1	hornatý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Psiloritis	Psiloritis	k1gFnSc1	Psiloritis
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Idi	Idi	k1gMnSc2	Idi
Ori	Ori	k1gMnSc2	Ori
má	mít	k5eAaImIp3nS	mít
2456	[number]	k4	2456
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Páhnes	Páhnesa	k1gFnPc2	Páhnesa
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Levka	Levek	k1gMnSc2	Levek
Ori	Ori	k1gMnSc2	Ori
má	mít	k5eAaImIp3nS	mít
2453	[number]	k4	2453
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
