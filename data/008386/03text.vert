<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
Prostějov	Prostějov	k1gInSc1	Prostějov
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
prostějovského	prostějovský	k2eAgMnSc2d1	prostějovský
zubního	zubní	k2eAgMnSc2d1	zubní
lékaře	lékař	k1gMnSc2	lékař
MUDr.	MUDr.	kA	MUDr.
Antonína	Antonín	k1gMnSc2	Antonín
Valenty	Valenta	k1gMnSc2	Valenta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matrice	matrika	k1gFnSc6	matrika
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
jako	jako	k9	jako
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
,	,	kIx,	,
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
Edvard	Edvard	k1gMnSc1	Edvard
přijal	přijmout	k5eAaPmAgMnS	přijmout
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
vsi	ves	k1gFnSc2	ves
Roštína	Roštín	k1gInSc2	Roštín
u	u	k7c2	u
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
z	z	k7c2	z
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
rodiny	rodina	k1gFnSc2	rodina
Chmelařů	Chmelař	k1gMnPc2	Chmelař
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
mu	on	k3xPp3gMnSc3	on
nejprve	nejprve	k6eAd1	nejprve
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
souchotiny	souchotiny	k1gFnPc1	souchotiny
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
tutéž	týž	k3xTgFnSc4	týž
nemoc	nemoc	k1gFnSc4	nemoc
i	i	k9	i
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
<g/>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sestrami	sestra	k1gFnPc7	sestra
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
ujal	ujmout	k5eAaPmAgMnS	ujmout
strýc	strýc	k1gMnSc1	strýc
František	František	k1gMnSc1	František
Kovářík	kovářík	k1gMnSc1	kovářík
(	(	kIx(	(
<g/>
manžel	manžel	k1gMnSc1	manžel
sestry	sestra	k1gFnSc2	sestra
zemřelé	zemřelý	k2eAgFnSc2d1	zemřelá
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
prostějovské	prostějovský	k2eAgFnSc2d1	prostějovská
firmy	firma	k1gFnSc2	firma
F.	F.	kA	F.
a	a	k8xC	a
J.	J.	kA	J.
Kovařík	Kovařík	k1gMnSc1	Kovařík
(	(	kIx(	(
<g/>
po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
Wikov	Wikov	k1gInSc1	Wikov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
předválečný	předválečný	k2eAgMnSc1d1	předválečný
ministr	ministr	k1gMnSc1	ministr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
maturoval	maturovat	k5eAaBmAgMnS	maturovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
na	na	k7c6	na
prostějovské	prostějovský	k2eAgFnSc6d1	prostějovská
reálce	reálka	k1gFnSc6	reálka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
místním	místní	k2eAgNnSc6d1	místní
gymnáziu	gymnázium	k1gNnSc6	gymnázium
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Valentovou	Valentová	k1gFnSc7	Valentová
sestrou	sestra	k1gFnSc7	sestra
Olgou	Olga	k1gFnSc7	Olga
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Wolker	Wolker	k1gMnSc1	Wolker
<g/>
;	;	kIx,	;
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
Valenta	Valenta	k1gMnSc1	Valenta
redigoval	redigovat	k5eAaImAgMnS	redigovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
Sborníku	sborník	k1gInSc2	sborník
českého	český	k2eAgNnSc2d1	české
studentstva	studentstvo	k1gNnSc2	studentstvo
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
lásky	láska	k1gFnSc2	láska
v	v	k7c6	v
pěstounské	pěstounský	k2eAgFnSc6d1	pěstounská
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
sebevražda	sebevražda	k1gFnSc1	sebevražda
sedmnáctileté	sedmnáctiletý	k2eAgFnPc1d1	sedmnáctiletá
sestry	sestra	k1gFnPc1	sestra
Věry	Věra	k1gFnSc2	Věra
se	se	k3xPyFc4	se
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
autobiografických	autobiografický	k2eAgInPc6d1	autobiografický
ohlasech	ohlas	k1gInPc6	ohlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
techniku	technika	k1gFnSc4	technika
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
svého	svůj	k3xOyFgMnSc2	svůj
pěstouna	pěstoun	k1gMnSc2	pěstoun
Františka	František	k1gMnSc2	František
Kováříka	kovářík	k1gMnSc2	kovářík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
očekával	očekávat	k5eAaImAgMnS	očekávat
jeho	jeho	k3xOp3gFnSc4	jeho
angažovanost	angažovanost	k1gFnSc4	angažovanost
v	v	k7c6	v
prostějovském	prostějovský	k2eAgInSc6d1	prostějovský
podniku	podnik	k1gInSc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
však	však	k9	však
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
semestry	semestr	k1gInPc4	semestr
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
příležitostných	příležitostný	k2eAgNnPc6d1	příležitostné
zaměstnáních	zaměstnání	k1gNnPc6	zaměstnání
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
do	do	k7c2	do
brněnské	brněnský	k2eAgFnSc2d1	brněnská
redakce	redakce	k1gFnSc2	redakce
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
je	být	k5eAaImIp3nS	být
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
redakci	redakce	k1gFnSc6	redakce
i	i	k8xC	i
v	v	k7c6	v
olomoucké	olomoucký	k2eAgFnSc6d1	olomoucká
a	a	k8xC	a
pražské	pražský	k2eAgFnSc6d1	Pražská
pobočce	pobočka	k1gFnSc6	pobočka
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
stenograf	stenograf	k1gMnSc1	stenograf
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
jako	jako	k8xS	jako
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
redaktor	redaktor	k1gMnSc1	redaktor
kulturní	kulturní	k2eAgFnSc2d1	kulturní
a	a	k8xC	a
literární	literární	k2eAgFnSc2d1	literární
části	část	k1gFnSc2	část
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
úvodníkář	úvodníkář	k1gMnSc1	úvodníkář
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
finanční	finanční	k2eAgFnSc2d1	finanční
podpory	podpora	k1gFnSc2	podpora
Jana	Jan	k1gMnSc2	Jan
Antonína	Antonín	k1gMnSc2	Antonín
Bati	Baťa	k1gMnSc2	Baťa
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
světě	svět	k1gInSc6	svět
jako	jako	k8xC	jako
reportér	reportér	k1gMnSc1	reportér
a	a	k8xC	a
také	také	k9	také
J.	J.	kA	J.
A.	A.	kA	A.
Baťu	Baťa	k1gMnSc4	Baťa
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
novinářské	novinářský	k2eAgFnSc2d1	novinářská
činnosti	činnost	k1gFnSc2	činnost
stal	stát	k5eAaPmAgInS	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Voznice	voznice	k1gFnSc2	voznice
u	u	k7c2	u
Příbrami	Příbram	k1gFnSc2	Příbram
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
pronajatu	pronajat	k2eAgFnSc4d1	pronajata
Oldřich	Oldřich	k1gMnSc1	Oldřich
Nový	nový	k2eAgMnSc1d1	nový
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
redaktorem	redaktor	k1gMnSc7	redaktor
týdeníku	týdeník	k1gInSc2	týdeník
Dnešek	dnešek	k1gInSc1	dnešek
a	a	k8xC	a
Knihovny	knihovna	k1gFnPc1	knihovna
Svobodných	svobodný	k2eAgFnPc2d1	svobodná
novin	novina	k1gFnPc2	novina
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
redaktorem	redaktor	k1gMnSc7	redaktor
Svobodných	svobodný	k2eAgFnPc2d1	svobodná
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
okupace	okupace	k1gFnSc2	okupace
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
takto	takto	k6eAd1	takto
přejmenovány	přejmenován	k2eAgFnPc4d1	přejmenována
a	a	k8xC	a
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
názvu	název	k1gInSc3	název
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
Svobodných	svobodný	k2eAgFnPc2d1	svobodná
novin	novina	k1gFnPc2	novina
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
držen	držet	k5eAaImNgMnS	držet
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
ho	on	k3xPp3gNnSc4	on
následně	následně	k6eAd1	následně
zprostil	zprostit	k5eAaPmAgMnS	zprostit
obžaloby	obžaloba	k1gFnPc4	obžaloba
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Edvardu	Edvard	k1gMnSc6	Edvard
</s>
</p>
<p>
<s>
a	a	k8xC	a
syny	syn	k1gMnPc4	syn
Martina	Martin	k1gMnSc4	Martin
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
ovdověl	ovdovět	k5eAaPmAgMnS	ovdovět
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
trpěl	trpět	k5eAaImAgMnS	trpět
chorobou	choroba	k1gFnSc7	choroba
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
ucha	ucho	k1gNnSc2	ucho
(	(	kIx(	(
<g/>
Meniè	Meniè	k1gFnSc1	Meniè
choroba	choroba	k1gFnSc1	choroba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
ohluchnutí	ohluchnutí	k1gNnSc3	ohluchnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Valenta	Valenta	k1gMnSc1	Valenta
zemřel	zemřít	k5eAaPmAgMnS	zemřít
21	[number]	k4	21
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1978	[number]	k4	1978
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
umístěny	umístěn	k2eAgInPc1d1	umístěn
na	na	k7c6	na
prostějovském	prostějovský	k2eAgInSc6d1	prostějovský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
hrobu	hrob	k1gInSc2	hrob
jeho	on	k3xPp3gMnSc2	on
přítele	přítel	k1gMnSc2	přítel
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svého	svůj	k3xOyFgMnSc2	svůj
rodáka	rodák	k1gMnSc2	rodák
pojmenovalo	pojmenovat	k5eAaPmAgNnS	pojmenovat
město	město	k1gNnSc1	město
Prostějov	Prostějov	k1gInSc4	Prostějov
ulici	ulice	k1gFnSc4	ulice
Edvarda	Edvard	k1gMnSc2	Edvard
Valenty	Valenta	k1gMnSc2	Valenta
a	a	k8xC	a
Základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
Edvarda	Edvard	k1gMnSc2	Edvard
Valenty	Valenta	k1gMnSc2	Valenta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
významným	významný	k2eAgInSc7d1	významný
spisovatelským	spisovatelský	k2eAgInSc7d1	spisovatelský
počinem	počin	k1gInSc7	počin
je	být	k5eAaImIp3nS	být
knižní	knižní	k2eAgNnSc4d1	knižní
zpracování	zpracování	k1gNnSc4	zpracování
příběhů	příběh	k1gInPc2	příběh
českého	český	k2eAgMnSc2d1	český
polárníka	polárník	k1gMnSc2	polárník
Jana	Jan	k1gMnSc2	Jan
Welzla	Welzla	k1gFnSc2	Welzla
z	z	k7c2	z
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Bedřichem	Bedřich	k1gMnSc7	Bedřich
Golombkem	Golombek	k1gMnSc7	Golombek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
stěžejním	stěžejní	k2eAgNnSc7d1	stěžejní
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
psychologický	psychologický	k2eAgInSc1d1	psychologický
román	román	k1gInSc1	román
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c7	za
zeleným	zelený	k2eAgNnSc7d1	zelené
světlem	světlo	k1gNnSc7	světlo
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
jím	on	k3xPp3gInSc7	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
linii	linie	k1gFnSc4	linie
prózy	próza	k1gFnSc2	próza
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
na	na	k7c4	na
díla	dílo	k1gNnPc4	dílo
soudobá	soudobý	k2eAgNnPc4d1	soudobé
–	–	k?	–
Ptáčníkův	Ptáčníkův	k2eAgInSc1d1	Ptáčníkův
Ročník	ročník	k1gInSc1	ročník
jednadvacet	jednadvacet	k1gNnSc2	jednadvacet
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
a	a	k8xC	a
Otčenáškův	Otčenáškův	k2eAgInSc4d1	Otčenáškův
román	román	k1gInSc4	román
Občan	občan	k1gMnSc1	občan
Brych	Brych	k1gMnSc1	Brych
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
románu	román	k1gInSc2	román
kritikou	kritika	k1gFnSc7	kritika
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vydání	vydání	k1gNnSc2	vydání
rozporné	rozporný	k2eAgFnSc2d1	rozporná
–	–	k?	–
od	od	k7c2	od
nadšení	nadšení	k1gNnSc2	nadšení
k	k	k7c3	k
odmítání	odmítání	k1gNnSc3	odmítání
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nedoceněný	doceněný	k2eNgInSc1d1	nedoceněný
je	být	k5eAaImIp3nS	být
Valentův	Valentův	k2eAgInSc1d1	Valentův
posmrtně	posmrtně	k6eAd1	posmrtně
vydaný	vydaný	k2eAgInSc1d1	vydaný
román	román	k1gInSc1	román
Žít	žít	k5eAaImF	žít
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Soupis	soupis	k1gInSc1	soupis
publikovaných	publikovaný	k2eAgNnPc2d1	publikované
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Zrána	zrána	k6eAd1	zrána
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Žikeš	Žikeš	k1gMnSc1	Žikeš
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trh	trh	k1gInSc1	trh
před	před	k7c7	před
svatým	svatý	k2eAgInSc7d1	svatý
Mikulášem	mikuláš	k1gInSc7	mikuláš
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Žikeš	Žikeš	k1gMnSc1	Žikeš
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Voříškova	Voříškův	k2eAgNnPc1d1	Voříškův
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
k	k	k7c3	k
obrázkům	obrázek	k1gInPc3	obrázek
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Sekory	Sekor	k1gInPc4	Sekor
<g/>
,	,	kIx,	,
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
knižní	knižní	k2eAgNnSc1d1	knižní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Třicet	třicet	k4xCc1	třicet
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
Edvarda	Edvard	k1gMnSc2	Edvard
Valenty	Valenta	k1gMnSc2	Valenta
a	a	k8xC	a
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Golombka	Golombek	k1gMnSc2	Golombek
<g/>
,	,	kIx,	,
Fr.	Fr.	k1gMnSc2	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
polárních	polární	k2eAgInPc2d1	polární
pokladů	poklad	k1gInPc2	poklad
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
Edvarda	Edvard	k1gMnSc2	Edvard
Valenty	Valenta	k1gMnSc2	Valenta
a	a	k8xC	a
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Golombka	Golombek	k1gMnSc2	Golombek
<g/>
,	,	kIx,	,
Fr.	Fr.	k1gMnSc2	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trampoty	trampota	k1gFnPc1	trampota
eskymáckého	eskymácký	k2eAgMnSc2d1	eskymácký
náčelníka	náčelník	k1gMnSc2	náčelník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
Nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
léta	léto	k1gNnSc2	léto
Eskymo	eskymo	k1gNnSc1	eskymo
Welzla	Welzla	k1gFnSc2	Welzla
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
Edvarda	Edvard	k1gMnSc2	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
a	a	k8xC	a
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Golombek	Golombka	k1gFnPc2	Golombka
<g/>
,	,	kIx,	,
Fr.	Fr.	k1gMnSc1	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
</s>
</p>
<p>
<s>
Ledové	ledový	k2eAgFnPc1d1	ledová
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
Edvarda	Edvard	k1gMnSc2	Edvard
Valenty	Valenta	k1gMnSc2	Valenta
a	a	k8xC	a
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Golombka	Golombek	k1gMnSc2	Golombek
<g/>
,	,	kIx,	,
Fr.	Fr.	k1gMnSc2	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
Fr.	Fr.	k1gMnSc1	Fr.
<g/>
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Královnino	královnin	k2eAgNnSc1d1	královnino
tajemství	tajemství	k1gNnSc1	tajemství
(	(	kIx(	(
<g/>
il	il	k?	il
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Milén	Milén	k1gInSc1	Milén
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
sborníku	sborník	k1gInSc2	sborník
Grafické	grafický	k2eAgFnSc2d1	grafická
práce	práce	k1gFnSc2	práce
Hollar	Hollara	k1gFnPc2	Hollara
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tajemný	tajemný	k2eAgMnSc1d1	tajemný
Zlíňan	Zlíňan	k1gMnSc1	Zlíňan
(	(	kIx(	(
<g/>
vydal	vydat	k5eAaPmAgMnS	vydat
A.	A.	kA	A.
Drégr	Drégr	k1gMnSc1	Drégr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
(	(	kIx(	(
<g/>
il	il	k?	il
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Brázdil	brázdit	k5eAaImAgMnS	brázdit
<g/>
,	,	kIx,	,
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
polárních	polární	k2eAgInPc2d1	polární
pokladů	poklad	k1gInPc2	poklad
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Eskymo	eskymo	k1gNnSc4	eskymo
Welzl	Welzl	k1gMnSc2	Welzl
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
Edvarda	Edvard	k1gMnSc2	Edvard
Valenty	Valenta	k1gMnSc2	Valenta
a	a	k8xC	a
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Golombka	Golombek	k1gMnSc2	Golombek
<g/>
,	,	kIx,	,
Fr.	Fr.	k1gMnSc2	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
jsem	být	k5eAaImIp1nS	být
potkal	potkat	k5eAaPmAgMnS	potkat
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
americké	americký	k2eAgFnSc2d1	americká
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
Fr.	Fr.	k1gMnSc1	Fr.
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Strýček	strýček	k1gMnSc1	strýček
Eskymák	Eskymák	k1gMnSc1	Eskymák
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vyprávění	vyprávění	k1gNnSc2	vyprávění
Eskymo	eskymo	k1gNnSc1	eskymo
Welzla	Welzla	k1gFnSc2	Welzla
<g/>
,	,	kIx,	,
Čin	čin	k1gInSc1	čin
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
housle	housle	k1gFnPc1	housle
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
cestě	cesta	k1gFnSc6	cesta
Emila	Emil	k1gMnSc2	Emil
Holuba	Holub	k1gMnSc2	Holub
<g/>
,	,	kIx,	,
ELK	ELK	kA	ELK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kouty	kout	k1gInPc1	kout
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
ELK	ELK	kA	ELK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kvas	kvas	k1gInSc1	kvas
(	(	kIx(	(
<g/>
ELK	ELK	kA	ELK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Novoroční	novoroční	k2eAgFnSc7d1	novoroční
Arizonou	Arizona	k1gFnSc7	Arizona
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Kuchař	Kuchař	k1gMnSc1	Kuchař
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Světem	svět	k1gInSc7	svět
pro	pro	k7c4	pro
nic	nic	k3yNnSc1	nic
za	za	k7c4	za
nic	nic	k3yNnSc4	nic
(	(	kIx(	(
<g/>
příběhy	příběh	k1gInPc4	příběh
z	z	k7c2	z
potulek	potulka	k1gFnPc2	potulka
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Kuchař	Kuchař	k1gMnSc1	Kuchař
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
(	(	kIx(	(
<g/>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Mrskoš	Mrskoš	k1gMnSc1	Mrskoš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c7	za
zeleným	zelený	k2eAgNnSc7d1	zelené
světlem	světlo	k1gNnSc7	světlo
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejkrásnější	krásný	k2eAgFnPc4d3	nejkrásnější
země	zem	k1gFnPc4	zem
(	(	kIx(	(
<g/>
příběhy	příběh	k1gInPc4	příběh
z	z	k7c2	z
potulek	potulka	k1gFnPc2	potulka
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trám	trám	k1gInSc1	trám
(	(	kIx(	(
<g/>
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dlouhán	dlouhán	k1gMnSc1	dlouhán
v	v	k7c6	v
okně	okno	k1gNnSc6	okno
(	(	kIx(	(
<g/>
detektivní	detektivní	k2eAgInSc1d1	detektivní
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
poklad	poklad	k1gInSc4	poklad
(	(	kIx(	(
<g/>
dřevoryty	dřevoryt	k1gInPc4	dřevoryt
Michael	Michael	k1gMnSc1	Michael
Florian	Florian	k1gMnSc1	Florian
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
samé	samý	k3xTgNnSc4	samý
psaní	psaní	k1gNnSc4	psaní
(	(	kIx(	(
<g/>
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
brněnských	brněnský	k2eAgFnPc2d1	brněnská
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnSc2	kresba
Eduard	Eduard	k1gMnSc1	Eduard
Milén	Milén	k1gInSc1	Milén
<g/>
,	,	kIx,	,
Symposium	symposium	k1gNnSc1	symposium
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žil	žít	k5eAaImAgMnS	žít
jsem	být	k5eAaImIp1nS	být
s	s	k7c7	s
miliardářem	miliardář	k1gMnSc7	miliardář
(	(	kIx(	(
<g/>
samizdatová	samizdatový	k2eAgFnSc1d1	samizdatová
Petlice	petlice	k1gFnSc1	petlice
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žít	žít	k5eAaImF	žít
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
:	:	kIx,	:
samizdatová	samizdatový	k2eAgFnSc1d1	samizdatová
Petlice	petlice	k1gFnSc1	petlice
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
;	;	kIx,	;
potom	potom	k6eAd1	potom
exilový	exilový	k2eAgInSc1d1	exilový
Index	index	k1gInSc1	index
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
nového	nový	k2eAgInSc2d1	nový
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ukradený	ukradený	k2eAgInSc1d1	ukradený
vynález	vynález	k1gInSc1	vynález
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Včera	včera	k6eAd1	včera
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
tenkrát	tenkrát	k6eAd1	tenkrát
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Filmografie	filmografie	k1gFnSc2	filmografie
===	===	k?	===
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
a	a	k8xC	a
Nilskou	nilský	k2eAgFnSc7d1	nilská
deltou	delta	k1gFnSc7	delta
do	do	k7c2	do
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
(	(	kIx(	(
<g/>
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
dokumentům	dokument	k1gInPc3	dokument
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Případ	případ	k1gInSc1	případ
ještě	ještě	k9	ještě
nekončí	končit	k5eNaImIp3nS	končit
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c7	za
zeleným	zelený	k2eAgNnSc7d1	zelené
světlem	světlo	k1gNnSc7	světlo
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
námětu	námět	k1gInSc2	námět
TV	TV	kA	TV
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
natočili	natočit	k5eAaBmAgMnP	natočit
prostějovští	prostějovský	k2eAgMnPc1d1	prostějovský
amatérští	amatérský	k2eAgMnPc1d1	amatérský
filmaři	filmař	k1gMnPc1	filmař
dokument	dokument	k1gInSc4	dokument
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Život	život	k1gInSc1	život
samé	samý	k3xTgNnSc4	samý
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novinářská	novinářský	k2eAgFnSc1d1	novinářská
práce	práce	k1gFnSc1	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Publikace	publikace	k1gFnSc1	publikace
v	v	k7c6	v
periodikách	periodikum	k1gNnPc6	periodikum
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
orlice	orlice	k1gFnSc1	orlice
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
,	,	kIx,	,
Niva	niva	k1gFnSc1	niva
<g/>
,	,	kIx,	,
Topičův	topičův	k2eAgInSc1d1	topičův
sborník	sborník	k1gInSc1	sborník
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
popularitě	popularita	k1gFnSc6	popularita
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
polozapomenutých	polozapomenutý	k2eAgMnPc2d1	polozapomenutý
autorů	autor	k1gMnPc2	autor
Edvarda	Edvard	k1gMnSc2	Edvard
Valenty	Valenta	k1gMnSc2	Valenta
a	a	k8xC	a
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Golombka	Golombek	k1gMnSc2	Golombek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
o	o	k7c6	o
popularitě	popularita	k1gFnSc6	popularita
jejich	jejich	k3xOp3gNnSc2	jejich
díla	dílo	k1gNnSc2	dílo
o	o	k7c6	o
Eskymu	eskymo	k1gNnSc6	eskymo
Welzlovi	Welzlův	k2eAgMnPc1d1	Welzlův
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
román	román	k1gInSc4	román
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
v	v	k7c6	v
části	část	k1gFnSc6	část
Pan	Pan	k1gMnSc1	Pan
Golombek	Golombek	k1gMnSc1	Golombek
a	a	k8xC	a
Pan	Pan	k1gMnSc1	Pan
Valenta	Valenta	k1gMnSc1	Valenta
nechal	nechat	k5eAaPmAgMnS	nechat
své	svůj	k3xOyFgMnPc4	svůj
redakční	redakční	k2eAgMnPc4d1	redakční
kolegy	kolega	k1gMnPc4	kolega
<g/>
,	,	kIx,	,
coby	coby	k?	coby
reportéry	reportér	k1gMnPc7	reportér
<g/>
,	,	kIx,	,
zpovídat	zpovídat	k5eAaImF	zpovídat
kapitána	kapitán	k1gMnSc2	kapitán
van	van	k1gInSc4	van
Tocha	Toch	k1gMnSc4	Toch
(	(	kIx(	(
<g/>
Vantocha	Vantoch	k1gMnSc4	Vantoch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
van	vana	k1gFnPc2	vana
Toch	Toch	k1gInSc1	Toch
navíc	navíc	k6eAd1	navíc
hovoří	hovořit	k5eAaImIp3nS	hovořit
jazykem	jazyk	k1gInSc7	jazyk
podobným	podobný	k2eAgInSc7d1	podobný
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
Eskymo	eskymo	k1gNnSc4	eskymo
Welzla	Welzla	k1gFnSc2	Welzla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BLAŽÍČEK	Blažíček	k1gMnSc1	Blažíček
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
po	po	k7c6	po
r.	r.	kA	r.
1945	[number]	k4	1945
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MRAVCOVÁ	MRAVCOVÁ	kA	MRAVCOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
po	po	k7c6	po
r.	r.	kA	r.
1945	[number]	k4	1945
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Bedřich	Bedřich	k1gMnSc1	Bedřich
Golombek	Golombek	k1gMnSc1	Golombek
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OPATRNÁ	opatrný	k2eAgFnSc1d1	opatrná
<g/>
,	,	kIx,	,
Denisa	Denisa	k1gFnSc1	Denisa
<g/>
.	.	kIx.	.
</s>
<s>
EDVARD	Edvard	k1gMnSc1	Edvard
VALENTA	Valenta	k1gMnSc1	Valenta
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
37	[number]	k4	37
s.	s.	k?	s.
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Daniel	Daniel	k1gMnSc1	Daniel
Jakubíček	Jakubíček	k1gMnSc1	Jakubíček
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
</s>
</p>
<p>
<s>
Jdi	jít	k5eAaImRp2nS	jít
za	za	k7c7	za
zeleným	zelený	k2eAgNnSc7d1	zelené
světlem	světlo	k1gNnSc7	světlo
–	–	k?	–
studie	studie	k1gFnSc1	studie
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
