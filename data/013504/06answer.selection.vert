<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
ale	ale	k8xC	ale
proslul	proslout	k5eAaPmAgMnS	proslout
během	během	k7c2	během
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1952	[number]	k4	1952
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
běh	běh	k1gInSc4	běh
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6,72	[number]	k4	6,72
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
:	:	kIx,	:
<g/>
17,0	[number]	k4	17,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
maratón	maratón	k1gInSc4	maratón
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3,2	[number]	k4	3,2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tehdy	tehdy	k6eAd1	tehdy
běžel	běžet	k5eAaImAgMnS	běžet
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
