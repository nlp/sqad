<s>
Peeling	Peeling	k1gInSc1	Peeling
je	být	k5eAaImIp3nS	být
obrušování	obrušování	k1gNnSc4	obrušování
mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
buněk	buňka	k1gFnPc2	buňka
kůže	kůže	k1gFnSc2	kůže
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
odstranění	odstranění	k1gNnSc2	odstranění
odumřelých	odumřelý	k2eAgFnPc2d1	odumřelá
části	část	k1gFnSc6	část
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
pomocí	pomocí	k7c2	pomocí
krému	krém	k1gInSc2	krém
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hrubé	hrubý	k2eAgFnPc4d1	hrubá
částečky	částečka	k1gFnPc4	částečka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
procedura	procedura	k1gFnSc1	procedura
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgFnS	mít
provádět	provádět	k5eAaImF	provádět
častěji	často	k6eAd2	často
než	než	k8xS	než
jedenkrát	jedenkrát	k6eAd1	jedenkrát
týdně	týdně	k6eAd1	týdně
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
hrozí	hrozit	k5eAaImIp3nS	hrozit
poškození	poškození	k1gNnSc4	poškození
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
obličejový	obličejový	k2eAgInSc4d1	obličejový
i	i	k8xC	i
tělový	tělový	k2eAgInSc4d1	tělový
peeling	peeling	k1gInSc4	peeling
<g/>
.	.	kIx.	.
</s>
<s>
Peeling	Peeling	k1gInSc1	Peeling
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
ekologické	ekologický	k2eAgFnPc4d1	ekologická
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Znečišťuje	znečišťovat	k5eAaImIp3nS	znečišťovat
životní	životní	k2eAgNnSc1d1	životní
prostředí	prostředí	k1gNnSc1	prostředí
plastovými	plastový	k2eAgFnPc7d1	plastová
mikrogranulemi	mikrogranule	k1gFnPc7	mikrogranule
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
součástí	součást	k1gFnSc7	součást
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
web	web	k1gInSc1	web
o	o	k7c6	o
peelingu	peeling	k1gInSc6	peeling
</s>
