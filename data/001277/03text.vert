<s>
Radon	radon	k1gInSc1	radon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Rn	Rn	k1gFnSc1	Rn
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Radonum	Radonum	k1gInSc1	Radonum
je	být	k5eAaImIp3nS	být
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
nereaktivní	reaktivní	k2eNgInSc1d1	nereaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
radia	radio	k1gNnSc2	radio
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nestálosti	nestálost	k1gFnSc3	nestálost
postupně	postupně	k6eAd1	postupně
zaniká	zanikat	k5eAaImIp3nS	zanikat
dalším	další	k2eAgInSc7d1	další
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přibližně	přibližně	k6eAd1	přibližně
dvacet	dvacet	k4xCc1	dvacet
nestabilních	stabilní	k2eNgInPc2d1	nestabilní
izotopů	izotop	k1gInPc2	izotop
radonu	radon	k1gInSc2	radon
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
krypton	krypton	k1gInSc4	krypton
a	a	k8xC	a
xenon	xenon	k1gInSc4	xenon
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
<g/>
,	,	kIx,	,
chlorem	chlor	k1gInSc7	chlor
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
silnými	silný	k2eAgInPc7d1	silný
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
činidly	činidlo	k1gNnPc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
51	[number]	k4	51
%	%	kIx~	%
svého	své	k1gNnSc2	své
objemu	objem	k1gInSc2	objem
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
nepolárních	polární	k2eNgNnPc6d1	nepolární
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
zachytit	zachytit	k5eAaPmF	zachytit
na	na	k7c6	na
aktivním	aktivní	k2eAgNnSc6d1	aktivní
uhlí	uhlí	k1gNnSc6	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
snadno	snadno	k6eAd1	snadno
ionizuje	ionizovat	k5eAaBmIp3nS	ionizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
ionizovaném	ionizovaný	k2eAgInSc6d1	ionizovaný
stavu	stav	k1gInSc6	stav
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
využívat	využívat	k5eAaPmF	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
osvětlovací	osvětlovací	k2eAgFnSc2d1	osvětlovací
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
ve	v	k7c6	v
výbojce	výbojka	k1gFnSc6	výbojka
vydává	vydávat	k5eAaImIp3nS	vydávat
jasně	jasně	k6eAd1	jasně
bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Ernstem	Ernst	k1gMnSc7	Ernst
Dornem	Dorn	k1gMnSc7	Dorn
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
radia	radio	k1gNnSc2	radio
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
jako	jako	k8xC	jako
radiová	radiový	k2eAgFnSc1d1	radiová
emanace	emanace	k1gFnSc1	emanace
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
charakterisoval	charakterisovat	k5eAaBmAgInS	charakterisovat
radiovou	radiový	k2eAgFnSc4d1	radiová
emanci	emanek	k1gMnPc1	emanek
jejím	její	k3xOp3gNnSc7	její
spektrem	spektrum	k1gNnSc7	spektrum
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
určil	určit	k5eAaPmAgInS	určit
její	její	k3xOp3gFnSc4	její
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
i	i	k9	i
atomovou	atomový	k2eAgFnSc4d1	atomová
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
název	název	k1gInSc4	název
svítící	svítící	k2eAgFnPc1d1	svítící
-	-	kIx~	-
niton	niton	k1gMnSc1	niton
Nt	Nt	k1gMnSc1	Nt
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
prvku	prvek	k1gInSc2	prvek
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
,	,	kIx,	,
až	až	k8xS	až
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
přijat	přijmout	k5eAaPmNgInS	přijmout
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
radon	radon	k1gInSc1	radon
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
radonu	radon	k1gInSc2	radon
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
jsou	být	k5eAaImIp3nP	být
nesmírně	smírně	k6eNd1	smírně
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
detekce	detekce	k1gFnSc2	detekce
těch	ten	k3xDgFnPc2	ten
nejcitlivějších	citlivý	k2eAgFnPc2d3	nejcitlivější
analytických	analytický	k2eAgFnPc2d1	analytická
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
vývěrech	vývěr	k1gInPc6	vývěr
podzemních	podzemní	k2eAgFnPc2d1	podzemní
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
rozpadu	rozpad	k1gInSc2	rozpad
jader	jádro	k1gNnPc2	jádro
radia	radio	k1gNnSc2	radio
<g/>
,	,	kIx,	,
thoria	thorium	k1gNnSc2	thorium
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
však	však	k9	však
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
dávkách	dávka	k1gFnPc6	dávka
vyvěrat	vyvěrat	k5eAaImF	vyvěrat
sám	sám	k3xTgInSc4	sám
z	z	k7c2	z
podloží	podloží	k1gNnSc2	podloží
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
plynné	plynný	k2eAgFnSc6d1	plynná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
radon	radon	k1gInSc1	radon
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
do	do	k7c2	do
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
roztok	roztok	k1gInSc1	roztok
radnaté	radnatý	k2eAgFnSc2d1	radnatý
soli	sůl	k1gFnSc2	sůl
nechá	nechat	k5eAaPmIp3nS	nechat
stát	stát	k5eAaImF	stát
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
láhvi	láhev	k1gFnSc6	láhev
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
ustanoví	ustanovit	k5eAaPmIp3nS	ustanovit
rovnováha	rovnováha	k1gFnSc1	rovnováha
s	s	k7c7	s
radiem	radio	k1gNnSc7	radio
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
emanancí	emanance	k1gFnSc7	emanance
(	(	kIx(	(
<g/>
minerálu	minerál	k1gInSc2	minerál
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
radonu	radon	k1gInSc2	radon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
dá	dát	k5eAaPmIp3nS	dát
oddestilovat	oddestilovat	k5eAaPmF	oddestilovat
nebo	nebo	k8xC	nebo
vyvařit	vyvařit	k5eAaPmF	vyvařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
geologii	geologie	k1gFnSc6	geologie
slouží	sloužit	k5eAaImIp3nS	sloužit
studium	studium	k1gNnSc4	studium
obsahu	obsah	k1gInSc2	obsah
izotopů	izotop	k1gInPc2	izotop
radonu	radon	k1gInSc2	radon
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
vodách	voda	k1gFnPc6	voda
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
jejich	jejich	k3xOp3gInSc2	jejich
původu	původ	k1gInSc2	původ
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Medicínské	medicínský	k2eAgNnSc1d1	medicínské
využití	využití	k1gNnSc1	využití
radonu	radon	k1gInSc2	radon
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gInPc2	jeho
izotopů	izotop	k1gInPc2	izotop
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
alfa-zářiče	alfaářič	k1gInPc1	alfa-zářič
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
krátkým	krátký	k2eAgInSc7d1	krátký
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgInSc1d3	nejznámější
izotop	izotop	k1gInSc1	izotop
222	[number]	k4	222
<g/>
Rn	Rn	k1gFnPc2	Rn
má	mít	k5eAaImIp3nS	mít
poločas	poločas	k1gInSc1	poločas
rozpadu	rozpad	k1gInSc2	rozpad
3,82	[number]	k4	3,82
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
izotopy	izotop	k1gInPc1	izotop
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
<g/>
:	:	kIx,	:
220	[number]	k4	220
<g/>
Rn	Rn	k1gFnSc1	Rn
54,5	[number]	k4	54,5
s	s	k7c7	s
a	a	k8xC	a
219	[number]	k4	219
<g/>
Rn	Rn	k1gFnSc1	Rn
3,92	[number]	k4	3,92
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
krátkodobé	krátkodobý	k2eAgNnSc4d1	krátkodobé
lokální	lokální	k2eAgNnSc4d1	lokální
ozařování	ozařování	k1gNnSc4	ozařování
vybraných	vybraný	k2eAgFnPc2d1	vybraná
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Radonová	radonový	k2eAgFnSc1d1	radonová
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
voda	voda	k1gFnSc1	voda
obsahující	obsahující	k2eAgInSc1d1	obsahující
rozpuštěný	rozpuštěný	k2eAgInSc1d1	rozpuštěný
radon	radon	k1gInSc1	radon
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
balneologii	balneologie	k1gFnSc6	balneologie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
jáchymovských	jáchymovský	k2eAgFnPc6d1	Jáchymovská
lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
dopravována	dopravovat	k5eAaImNgFnS	dopravovat
potrubím	potrubí	k1gNnSc7	potrubí
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
uranového	uranový	k2eAgInSc2d1	uranový
dolu	dol	k1gInSc2	dol
Svornost	svornost	k1gFnSc1	svornost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejím	její	k3xOp3gInSc7	její
nejmohutnějším	mohutný	k2eAgInSc7d3	nejmohutnější
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgInSc1d1	podzemní
pramen	pramen	k1gInSc1	pramen
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
akademiku	akademik	k1gMnSc6	akademik
Běhounkovi	běhounek	k1gMnSc6	běhounek
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
měrnou	měrný	k2eAgFnSc7d1	měrná
aktivitou	aktivita	k1gFnSc7	aktivita
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
kBq	kBq	k?	kBq
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc4d1	silná
radonové	radonový	k2eAgFnPc4d1	radonová
vody	voda	k1gFnPc4	voda
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
kBq	kBq	k?	kBq
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
Agricola	Agricola	k1gFnSc1	Agricola
(	(	kIx(	(
<g/>
navrtaný	navrtaný	k2eAgInSc1d1	navrtaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
měrnou	měrný	k2eAgFnSc4d1	měrná
aktivitu	aktivita	k1gFnSc4	aktivita
ještě	ještě	k9	ještě
přibližně	přibližně	k6eAd1	přibližně
dvakrát	dvakrát	k6eAd1	dvakrát
tak	tak	k6eAd1	tak
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
vydatnost	vydatnost	k1gFnSc1	vydatnost
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Vydatnost	vydatnost	k1gFnSc1	vydatnost
všech	všecek	k3xTgInPc2	všecek
Jáchymovských	jáchymovský	k2eAgInPc2d1	jáchymovský
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
pramenů	pramen	k1gInPc2	pramen
činí	činit	k5eAaImIp3nS	činit
řádově	řádově	k6eAd1	řádově
500	[number]	k4	500
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jáchymovských	jáchymovský	k2eAgFnPc6d1	Jáchymovská
lázních	lázeň	k1gFnPc6	lázeň
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
používají	používat	k5eAaImIp3nP	používat
koupele	koupel	k1gFnPc1	koupel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
aktivita	aktivita	k1gFnSc1	aktivita
radonové	radonový	k2eAgFnSc2d1	radonová
vody	voda	k1gFnSc2	voda
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
(	(	kIx(	(
<g/>
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
odvětrání	odvětrání	k1gNnSc2	odvětrání
a	a	k8xC	a
rozpadu	rozpad	k1gInSc2	rozpad
během	během	k7c2	během
postupného	postupný	k2eAgNnSc2d1	postupné
přečerpávání	přečerpávání	k1gNnSc2	přečerpávání
<g/>
)	)	kIx)	)
na	na	k7c4	na
4,5	[number]	k4	4,5
kBq	kBq	k?	kBq
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Typická	typický	k2eAgFnSc1d1	typická
délka	délka	k1gFnSc1	délka
pobytu	pobyt	k1gInSc2	pobyt
pacienta	pacient	k1gMnSc2	pacient
ve	v	k7c6	v
vaně	vana	k1gFnSc6	vana
s	s	k7c7	s
radonovou	radonový	k2eAgFnSc7d1	radonová
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
dvacet	dvacet	k4xCc4	dvacet
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
výskyt	výskyt	k1gInSc1	výskyt
radonu	radon	k1gInSc2	radon
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
lokalitě	lokalita	k1gFnSc6	lokalita
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přináší	přinášet	k5eAaImIp3nS	přinášet
nárůst	nárůst	k1gInSc4	nárůst
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
výskytu	výskyt	k1gInSc2	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
samotné	samotný	k2eAgInPc1d1	samotný
izotopy	izotop	k1gInPc1	izotop
radonu	radon	k1gInSc2	radon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
produkty	produkt	k1gInPc1	produkt
jeho	jeho	k3xOp3gFnSc2	jeho
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
radonu	radon	k1gInSc2	radon
kovy	kov	k1gInPc4	kov
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
tvoří	tvořit	k5eAaImIp3nP	tvořit
shluky	shluk	k1gInPc1	shluk
s	s	k7c7	s
aerosolovými	aerosolový	k2eAgFnPc7d1	aerosolová
částicemi	částice	k1gFnPc7	částice
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vázané	vázaný	k2eAgInPc1d1	vázaný
produkty	produkt	k1gInPc1	produkt
přeměny	přeměna	k1gFnSc2	přeměna
radonu	radon	k1gInSc2	radon
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
při	při	k7c6	při
vdechnutí	vdechnutí	k1gNnSc6	vdechnutí
zachyceny	zachycen	k2eAgInPc1d1	zachycen
v	v	k7c6	v
dýchacím	dýchací	k2eAgNnSc6d1	dýchací
ústrojí	ústrojí	k1gNnSc6	ústrojí
a	a	k8xC	a
volně	volně	k6eAd1	volně
se	se	k3xPyFc4	se
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
radon	radon	k1gInSc1	radon
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
produkty	produkt	k1gInPc1	produkt
jeho	jeho	k3xOp3gFnSc2	jeho
přeměny	přeměna	k1gFnSc2	přeměna
polonium	polonium	k1gNnSc1	polonium
218	[number]	k4	218
<g/>
Po	Po	kA	Po
a	a	k8xC	a
214	[number]	k4	214
<g/>
Po	Po	kA	Po
emitují	emitovat	k5eAaBmIp3nP	emitovat
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
radioaktivní	radioaktivní	k2eAgFnSc6d1	radioaktivní
přeměně	přeměna	k1gFnSc6	přeměna
částice	částice	k1gFnSc2	částice
alfa	alfa	k1gNnSc1	alfa
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
ionizační	ionizační	k2eAgFnSc3d1	ionizační
schopnosti	schopnost	k1gFnSc3	schopnost
způsobit	způsobit	k5eAaPmF	způsobit
porušení	porušení	k1gNnSc4	porušení
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
reparace	reparace	k1gFnSc1	reparace
DNA	DNA	kA	DNA
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
nekontrolovatelné	kontrolovatelný	k2eNgNnSc4d1	nekontrolovatelné
množení	množení	k1gNnSc4	množení
buněk	buňka	k1gFnPc2	buňka
-	-	kIx~	-
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
vést	vést	k5eAaImF	vést
k	k	k7c3	k
více	hodně	k6eAd2	hodně
nádorům	nádor	k1gInPc3	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
základová	základový	k2eAgFnSc1d1	základová
část	část	k1gFnSc1	část
domu	dům	k1gInSc2	dům
starší	starý	k2eAgFnSc1d2	starší
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
provedená	provedený	k2eAgFnSc1d1	provedená
(	(	kIx(	(
<g/>
špatná	špatný	k2eAgFnSc1d1	špatná
izolace	izolace	k1gFnSc1	izolace
základů	základ	k1gInPc2	základ
<g/>
,	,	kIx,	,
popraskaná	popraskaný	k2eAgFnSc1d1	popraskaná
podlaha	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
prkenná	prkenný	k2eAgFnSc1d1	prkenná
podlaha	podlaha	k1gFnSc1	podlaha
bez	bez	k7c2	bez
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
utěsněné	utěsněný	k2eAgInPc1d1	utěsněný
prostupy	prostup	k1gInPc1	prostup
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
nasávání	nasávání	k1gNnSc3	nasávání
radonu	radon	k1gInSc2	radon
do	do	k7c2	do
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
prostředí	prostředí	k1gNnSc2	prostředí
objektu	objekt	k1gInSc2	objekt
(	(	kIx(	(
<g/>
především	především	k9	především
přízemí	přízemí	k1gNnSc2	přízemí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
komínového	komínový	k2eAgInSc2d1	komínový
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
způsobí	způsobit	k5eAaPmIp3nS	způsobit
podtlak	podtlak	k1gInSc1	podtlak
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
a	a	k8xC	a
radon	radon	k1gInSc1	radon
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
plyny	plyn	k1gInPc7	plyn
aktivně	aktivně	k6eAd1	aktivně
nasáván	nasáván	k2eAgMnSc1d1	nasáván
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
možným	možný	k2eAgInSc7d1	možný
zdrojem	zdroj	k1gInSc7	zdroj
radonu	radon	k1gInSc2	radon
je	být	k5eAaImIp3nS	být
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
škvárobetonové	škvárobetonový	k2eAgFnPc1d1	škvárobetonový
tvárnice	tvárnice	k1gFnPc1	tvárnice
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
rynholecké	rynholecký	k2eAgFnSc2d1	rynholecký
škváry	škvára	k1gFnSc2	škvára
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
aktivity	aktivita	k1gFnPc4	aktivita
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
všech	všecek	k3xTgInPc2	všecek
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
dodávaných	dodávaný	k2eAgInPc2d1	dodávaný
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trh	trh	k1gInSc4	trh
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Státního	státní	k2eAgInSc2d1	státní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
SÚJB	SÚJB	kA	SÚJB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
geologické	geologický	k2eAgFnSc3d1	geologická
stavbě	stavba	k1gFnSc3	stavba
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
zemím	zem	k1gFnPc3	zem
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
koncentrací	koncentrace	k1gFnSc7	koncentrace
radonu	radon	k1gInSc2	radon
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
(	(	kIx(	(
<g/>
118	[number]	k4	118
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
WHO	WHO	kA	WHO
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
limit	limit	k1gInSc4	limit
100	[number]	k4	100
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1	koncentrace
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
prostoru	prostor	k1gInSc6	prostor
bývá	bývat	k5eAaImIp3nS	bývat
kolem	kolem	k7c2	kolem
10	[number]	k4	10
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
koncentraci	koncentrace	k1gFnSc4	koncentrace
radonu	radon	k1gInSc2	radon
v	v	k7c6	v
domě	dům	k1gInSc6	dům
lze	lze	k6eAd1	lze
dočasně	dočasně	k6eAd1	dočasně
snížit	snížit	k5eAaPmF	snížit
častějším	častý	k2eAgNnSc7d2	častější
větráním	větrání	k1gNnSc7	větrání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
známá	známý	k2eAgNnPc4d1	známé
účinná	účinný	k2eAgNnPc4d1	účinné
technická	technický	k2eAgNnPc4d1	technické
opatření	opatření	k1gNnPc4	opatření
proti	proti	k7c3	proti
pronikání	pronikání	k1gNnSc3	pronikání
radonu	radon	k1gInSc2	radon
do	do	k7c2	do
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
u	u	k7c2	u
stavěných	stavěný	k2eAgInPc2d1	stavěný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
existujících	existující	k2eAgFnPc2d1	existující
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
usiluje	usilovat	k5eAaImIp3nS	usilovat
již	již	k6eAd1	již
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
o	o	k7c6	o
snížení	snížení	k1gNnSc6	snížení
ozáření	ozáření	k1gNnSc6	ozáření
obyvatel	obyvatel	k1gMnPc2	obyvatel
od	od	k7c2	od
radonu	radon	k1gInSc2	radon
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
radonový	radonový	k2eAgInSc1d1	radonový
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
Radonový	radonový	k2eAgInSc1d1	radonový
program	program	k1gInSc1	program
ČR	ČR	kA	ČR
na	na	k7c4	na
léta	léto	k1gNnPc4	léto
2009	[number]	k4	2009
až	až	k8xS	až
2019	[number]	k4	2019
-	-	kIx~	-
Akční	akční	k2eAgInSc1d1	akční
plán	plán	k1gInSc1	plán
<g/>
)	)	kIx)	)
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
řadu	řada	k1gFnSc4	řada
aktivit	aktivita	k1gFnPc2	aktivita
koordinovaných	koordinovaný	k2eAgFnPc2d1	koordinovaná
Státním	státní	k2eAgInSc7d1	státní
úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
měření	měření	k1gNnSc1	měření
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
a	a	k8xC	a
školských	školský	k2eAgNnPc6d1	školské
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
informativní	informativní	k2eAgNnSc1d1	informativní
měření	měření	k1gNnSc1	měření
objemové	objemový	k2eAgFnSc2d1	objemová
aktivity	aktivita	k1gFnSc2	aktivita
radonu	radon	k1gInSc2	radon
v	v	k7c6	v
domě	dům	k1gInSc6	dům
či	či	k8xC	či
bytě	byt	k1gInSc6	byt
pomocí	pomoc	k1gFnPc2	pomoc
stopových	stopový	k2eAgInPc2d1	stopový
detektorů	detektor	k1gInPc2	detektor
RAMARn	RAMARna	k1gFnPc2	RAMARna
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
v	v	k7c6	v
ČR	ČR	kA	ČR
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
radiační	radiační	k2eAgFnSc2d1	radiační
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Radiační	radiační	k2eAgFnSc2d1	radiační
hormeze	hormeze	k1gFnSc2	hormeze
Ionizující	ionizující	k2eAgFnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
radon	radon	k1gInSc1	radon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
radon	radon	k1gInSc1	radon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Radonový	radonový	k2eAgInSc1d1	radonový
program	program	k1gInSc1	program
ČR	ČR	kA	ČR
Radon	radon	k1gInSc1	radon
-	-	kIx~	-
podceňovaný	podceňovaný	k2eAgInSc1d1	podceňovaný
zabiják	zabiják	k1gInSc1	zabiják
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
Zdraví	zdraví	k1gNnSc1	zdraví
"	"	kIx"	"
<g/>
v	v	k7c6	v
cajku	cajk	k1gInSc6	cajk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
</s>
