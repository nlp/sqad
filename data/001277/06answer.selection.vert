<s>
Radon	radon	k1gInSc1	radon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Rn	Rn	k1gFnSc1	Rn
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Radonum	Radonum	k1gInSc1	Radonum
je	být	k5eAaImIp3nS	být
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
<g/>
.	.	kIx.	.
</s>
