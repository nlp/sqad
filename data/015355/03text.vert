<s>
Reaktance	reaktance	k1gFnSc1
</s>
<s>
Reaktance	reaktance	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
jalový	jalový	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
imaginární	imaginární	k2eAgFnSc7d1
částí	část	k1gFnSc7
impedance	impedance	k1gFnSc2
(	(	kIx(
<g/>
celkového	celkový	k2eAgInSc2d1
odporu	odpor	k1gInSc2
<g/>
)	)	kIx)
součástky	součástka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reaktance	reaktance	k1gFnSc1
indukčního	indukční	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
se	se	k3xPyFc4
též	též	k9
nazývá	nazývat	k5eAaImIp3nS
induktance	induktance	k1gFnSc1
(	(	kIx(
<g/>
indukční	indukční	k2eAgInSc1d1
odpor	odpor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
reaktance	reaktance	k1gFnSc1
kapacitního	kapacitní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
kapacitance	kapacitance	k1gFnSc2
(	(	kIx(
<g/>
kapacitní	kapacitní	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
hrozí	hrozit	k5eAaImIp3nS
záměna	záměna	k1gFnSc1
s	s	k7c7
anglickými	anglický	k2eAgInPc7d1
termíny	termín	k1gInPc7
„	„	k?
<g/>
inductance	inductanka	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
=	=	kIx~
indukčnost	indukčnost	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
capacitance	capacitance	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
=	=	kIx~
elektrická	elektrický	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
lépe	dobře	k6eAd2
používat	používat	k5eAaImF
termínů	termín	k1gInPc2
induktivní	induktivní	k2eAgFnSc2d1
a	a	k8xC
kapacitní	kapacitní	k2eAgFnSc2d1
reaktance	reaktance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
norma	norma	k1gFnSc1
ČSN	ČSN	kA
ISO	ISO	kA
31-5	31-5	k4
Veličiny	veličina	k1gFnPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
Elektřina	elektřina	k1gFnSc1
a	a	k8xC
magnetismus	magnetismus	k1gInSc1
již	již	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
pouze	pouze	k6eAd1
termíny	termín	k1gInPc7
kapacitní	kapacitní	k2eAgFnSc2d1
a	a	k8xC
induktivní	induktivní	k2eAgFnSc2d1
reaktance	reaktance	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
tohoto	tento	k3xDgNnSc2
hlediska	hledisko	k1gNnSc2
lze	lze	k6eAd1
proto	proto	k8xC
termíny	termín	k1gInPc1
kapacitance	kapacitance	k1gFnSc2
a	a	k8xC
induktance	induktance	k1gFnSc2
považovat	považovat	k5eAaImF
za	za	k7c4
zastaralé	zastaralý	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Značka	značka	k1gFnSc1
<g/>
:	:	kIx,
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
X	X	kA
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Jednotka	jednotka	k1gFnSc1
SI	si	k1gNnSc2
<g/>
:	:	kIx,
ohm	ohm	k1gInSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
Ω	Ω	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
stejné	stejný	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
pro	pro	k7c4
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
Analýza	analýza	k1gFnSc1
</s>
<s>
Ve	v	k7c6
fázorovém	fázorový	k2eAgNnSc6d1
vyjádření	vyjádření	k1gNnSc6
se	se	k3xPyFc4
reaktance	reaktance	k1gFnSc1
používá	používat	k5eAaImIp3nS
k	k	k7c3
určení	určení	k1gNnSc3
změny	změna	k1gFnSc2
amplitudy	amplituda	k1gFnSc2
a	a	k8xC
fáze	fáze	k1gFnSc2
sinusového	sinusový	k2eAgInSc2d1
střídavého	střídavý	k2eAgInSc2d1
proudu	proud	k1gInSc2
procházejícího	procházející	k2eAgInSc2d1
elementem	element	k1gInSc7
střídavého	střídavý	k2eAgInSc2d1
elektrického	elektrický	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reaktance	reaktance	k1gFnSc1
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
X	X	kA
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
odpor	odpor	k1gInSc1
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
R	R	kA
<g/>
}}	}}	k?
</s>
<s>
elementu	element	k1gInSc3
dohromady	dohromady	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
impedanci	impedance	k1gFnSc4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
</s>
<s>
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
R	R	kA
</s>
<s>
+	+	kIx~
</s>
<s>
j	j	k?
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
R	R	kA
<g/>
+	+	kIx~
<g/>
jX	jX	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
impedance	impedance	k1gFnSc1
<g/>
;	;	kIx,
měří	měřit	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ohmech	ohm	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
R	R	kA
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
odpor	odpor	k1gInSc4
<g/>
;	;	kIx,
měří	měřit	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ohmech	ohm	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
{	{	kIx(
<g/>
X	X	kA
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
reaktance	reaktance	k1gFnSc1
<g/>
;	;	kIx,
měří	měřit	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ohmech	ohm	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
scriptstyle	scriptstyl	k1gInSc5
j	j	k?
<g/>
\	\	kIx~
<g/>
;	;	kIx,
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
;	;	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Induktance	Induktance	k1gFnSc1
</s>
<s>
Kapacitance	Kapacitance	k1gFnSc1
</s>
<s>
Impedance	impedance	k1gFnSc1
</s>
<s>
Admitance	admitance	k1gFnSc1
</s>
<s>
Elektřina	elektřina	k1gFnSc1
</s>
<s>
Kondenzátor	kondenzátor	k1gInSc1
</s>
<s>
Cívka	cívka	k1gFnSc1
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1
odpor	odpor	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
