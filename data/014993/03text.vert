<s>
Šalomoun	Šalomoun	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
izraelském	izraelský	k2eAgMnSc6d1
králi	král	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Šalomoun	Šalomoun	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Šalomoun	Šalomoun	k1gMnSc1
Narození	narození	k1gNnSc2
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
panovník	panovník	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
judaismus	judaismus	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Naamahfaraonova	Naamahfaraonův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Královna	královna	k1gFnSc1
ze	z	k7c2
Sáby	Sába	k1gFnSc2
Děti	dítě	k1gFnPc1
</s>
<s>
RechabeámMenelik	RechabeámMenelik	k1gMnSc1
I.	I.	kA
<g/>
BasmatTafat	BasmatTafat	k1gMnSc4
Rodiče	rodič	k1gMnSc4
</s>
<s>
David	David	k1gMnSc1
a	a	k8xC
Batšeba	Batšeba	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Nathan	Nathan	k1gMnSc1
<g/>
,	,	kIx,
Adonijáš	Adonijáš	k1gMnSc1
<g/>
,	,	kIx,
Amnon	Amnon	k1gMnSc1
a	a	k8xC
Abímelek	Abímelek	k1gInSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Abijám	Abija	k1gFnPc3
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
vnuk	vnuk	k1gMnSc1
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
izraelský	izraelský	k2eAgMnSc1d1
král	král	k1gMnSc1
(	(	kIx(
<g/>
970	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
–	–	k?
<g/>
931	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šalomoun	Šalomoun	k1gMnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ש	ש	k?
<g/>
ְ	ְ	k?
<g/>
ׁ	ׁ	k?
<g/>
ל	ל	k?
<g/>
ֹ	ֹ	k?
<g/>
מ	מ	k?
<g/>
ֹ	ֹ	k?
<g/>
ה	ה	k?
<g/>
,	,	kIx,
Šelomo	Šeloma	k1gFnSc5
či	či	k8xC
Šlomo	Šloma	k1gFnSc5
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
„	„	k?
<g/>
Pokoj	pokoj	k1gInSc1
jeho	jeho	k3xOp3gNnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
přepisováno	přepisován	k2eAgNnSc1d1
též	též	k9
jako	jako	k8xC,k8xS
Šalamoun	Šalamoun	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
králem	král	k1gMnSc7
jednotného	jednotný	k2eAgNnSc2d1
Izraelského	izraelský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládl	vládnout	k5eAaImAgMnS
přibližně	přibližně	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
970	#num#	k4
<g/>
–	–	k?
<g/>
931	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
(	(	kIx(
<g/>
Další	další	k2eAgNnSc1d1
možné	možný	k2eAgNnSc1d1
datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnSc2
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
v	v	k7c6
r.	r.	kA
933	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Gans	Gans	k1gInSc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k8xS,k8xC
datum	datum	k1gNnSc4
počátku	počátek	k1gInSc2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
rok	rok	k1gInSc1
2924	#num#	k4
od	od	k7c2
stvoření	stvoření	k1gNnSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídá	odpovídat	k5eAaImIp3nS
roku	rok	k1gInSc2
838	#num#	k4
nebo	nebo	k8xC
837	#num#	k4
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
království	království	k1gNnSc1
největšího	veliký	k2eAgInSc2d3
hospodářského	hospodářský	k2eAgInSc2d1
a	a	k8xC
územního	územní	k2eAgInSc2d1
rozmachu	rozmach	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
rozporů	rozpor	k1gInPc2
mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnPc4
syny	syn	k1gMnPc4
rozdělilo	rozdělit	k5eAaPmAgNnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
:	:	kIx,
na	na	k7c4
severní	severní	k2eAgNnSc4d1
Izraelské	izraelský	k2eAgNnSc4d1
království	království	k1gNnSc4
a	a	k8xC
jižní	jižní	k2eAgNnSc4d1
Judské	judský	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šalomoun	Šalomoun	k1gMnSc1
také	také	k9
nechal	nechat	k5eAaPmAgMnS
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
vystavět	vystavět	k5eAaPmF
Chrám	chrám	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
zničení	zničení	k1gNnSc2
roku	rok	k1gInSc2
586	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
střediskem	středisko	k1gNnSc7
židovského	židovský	k2eAgInSc2d1
kultu	kult	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šalomounovu	Šalomounův	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
popisuje	popisovat	k5eAaImIp3nS
biblická	biblický	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
královská	královský	k2eAgFnSc1d1
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paralipomenon	paralipomenon	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
</s>
<s>
Šalomounova	Šalomounův	k2eAgFnSc1d1
moudrost	moudrost	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
biblického	biblický	k2eAgNnSc2d1
vyprávění	vyprávění	k1gNnSc2
byl	být	k5eAaImAgMnS
Šalomoun	Šalomoun	k1gMnSc1
známý	známý	k1gMnSc1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
moudrost	moudrost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
obdivovala	obdivovat	k5eAaImAgFnS
i	i	k9
Královna	královna	k1gFnSc1
ze	z	k7c2
Sáby	Sába	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
židovských	židovský	k2eAgFnPc6d1
tradicích	tradice	k1gFnPc6
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
autora	autor	k1gMnSc4
tří	tři	k4xCgFnPc2
biblických	biblický	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
:	:	kIx,
knihy	kniha	k1gFnPc1
Přísloví	přísloví	k1gNnSc2
<g/>
,	,	kIx,
knihy	kniha	k1gFnSc2
Kazatel	kazatel	k1gMnSc1
a	a	k8xC
Písně	píseň	k1gFnPc1
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
moderní	moderní	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
toto	tento	k3xDgNnSc4
autorství	autorství	k1gNnSc4
mnohdy	mnohdy	k6eAd1
zpochybňuje	zpochybňovat	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Šalomounově	Šalomounův	k2eAgFnSc6d1
moudrosti	moudrost	k1gFnSc6
svědčí	svědčit	k5eAaImIp3nS
nejen	nejen	k6eAd1
rozkvět	rozkvět	k1gInSc1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc2
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
sídelního	sídelní	k2eAgNnSc2d1
města	město	k1gNnSc2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
40	#num#	k4
let	léto	k1gNnPc2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
nepoznala	poznat	k5eNaPmAgFnS
země	země	k1gFnSc1
válku	válek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Známé	známý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
Šalomounovo	Šalomounův	k2eAgNnSc1d1
rozhodnutí	rozhodnutí	k1gNnSc1
při	při	k7c6
sporu	spor	k1gInSc6
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
ženami	žena	k1gFnPc7
o	o	k7c4
dítě	dítě	k1gNnSc4
<g/>
:	:	kIx,
</s>
<s>
Dvěma	dva	k4xCgFnPc3
ženám	žena	k1gFnPc3
se	se	k3xPyFc4
současně	současně	k6eAd1
narodily	narodit	k5eAaPmAgFnP
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
však	však	k9
zemřelo	zemřít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
matka	matka	k1gFnSc1
si	se	k3xPyFc3
tajně	tajně	k6eAd1
přivlastnila	přivlastnit	k5eAaPmAgFnS
dítě	dítě	k1gNnSc4
druhé	druhý	k4xOgFnSc2
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
ta	ten	k3xDgFnSc1
potom	potom	k6eAd1
žádala	žádat	k5eAaImAgFnS
vrácení	vrácení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
spor	spor	k1gInSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
až	až	k9
ke	k	k7c3
králi	král	k1gMnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
obě	dva	k4xCgFnPc1
ženy	žena	k1gFnPc1
trvaly	trvat	k5eAaImAgFnP
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šalomoun	Šalomoun	k1gMnSc1
tehdy	tehdy	k6eAd1
přikázal	přikázat	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Rozetněte	rozetnout	k5eAaPmRp2nP
dítě	dítě	k1gNnSc4
vedví	vedví	k6eAd1
a	a	k8xC
dejte	dát	k5eAaPmRp2nP
každé	každý	k3xTgNnSc4
ženě	žena	k1gFnSc3
po	po	k7c6
půlce	půlka	k1gFnSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
žen	žena	k1gFnPc2
chladně	chladně	k6eAd1
souhlasila	souhlasit	k5eAaImAgFnS
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
úpěnlivě	úpěnlivě	k6eAd1
prosila	prosit	k5eAaImAgFnS,k5eAaPmAgFnS
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
dítě	dítě	k1gNnSc4
nechají	nechat	k5eAaPmIp3nP
žít	žít	k5eAaImF
a	a	k8xC
ať	ať	k8xS,k8xC
ho	on	k3xPp3gMnSc4
dají	dát	k5eAaPmIp3nP
raději	rád	k6eAd2
její	její	k3xOp3gFnSc4
sokyni	sokyně	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k8xS,k8xC
král	král	k1gMnSc1
Šalomoun	Šalomoun	k1gMnSc1
hned	hned	k6eAd1
poznal	poznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
z	z	k7c2
žen	žena	k1gFnPc2
je	být	k5eAaImIp3nS
pravou	pravý	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
jí	jíst	k5eAaImIp3nS
dítě	dítě	k1gNnSc4
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
také	také	k9
pochází	pocházet	k5eAaImIp3nS
sousloví	sousloví	k1gNnSc1
šalamounské	šalamounský	k2eAgNnSc1d1
rozhodnutí	rozhodnutí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Totožný	totožný	k2eAgInSc1d1
příběh	příběh	k1gInSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
sepsán	sepsat	k5eAaPmNgInS
ve	v	k7c6
4	#num#	k4
stol.	stol.	k?
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
buddhistických	buddhistický	k2eAgInPc6d1
příbězích	příběh	k1gInPc6
Džataky	Džatak	k1gInPc7
pod	pod	k7c7
číslem	číslo	k1gNnSc7
546	#num#	k4
<g/>
,	,	kIx,
tzv.	tzv.	kA
Mahómmaggadžátaka	Mahómmaggadžátak	k1gMnSc2
(	(	kIx(
<g/>
Jak	jak	k6eAd1
Bódhisatta	Bódhisatta	k1gMnSc1
překonal	překonat	k5eAaPmAgMnS
všechny	všechen	k3xTgMnPc4
mudrce	mudrc	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozporuplnost	rozporuplnost	k1gFnSc1
hodnocení	hodnocení	k1gNnSc2
</s>
<s>
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
jen	jen	k9
jedna	jeden	k4xCgFnSc1
stránka	stránka	k1gFnSc1
vyobrazení	vyobrazení	k1gNnSc4
Šalomouna	Šalomoun	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šalomoun	Šalomoun	k1gMnSc1
je	být	k5eAaImIp3nS
sice	sice	k8xC
skutečně	skutečně	k6eAd1
vylíčen	vylíčen	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
moudrý	moudrý	k2eAgMnSc1d1
<g/>
,	,	kIx,
spravedlivý	spravedlivý	k2eAgInSc1d1
a	a	k8xC
zbožný	zbožný	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
biblický	biblický	k2eAgInSc1d1
text	text	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
popis	popis	k1gInSc4
Šalomounova	Šalomounův	k2eAgInSc2d1
drsného	drsný	k2eAgInSc2d1
boje	boj	k1gInSc2
o	o	k7c4
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Bible	bible	k1gFnSc1
sice	sice	k8xC
líčí	líčit	k5eAaImIp3nS
jeho	jeho	k3xOp3gInPc4
úspěchy	úspěch	k1gInPc4
v	v	k7c6
diplomacii	diplomacie	k1gFnSc6
<g/>
,	,	kIx,
obchodu	obchod	k1gInSc3
a	a	k8xC
výstavbě	výstavba	k1gFnSc3
Jeruzaléma	Jeruzalém	k1gInSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Chrámu	chrám	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
za	za	k7c4
cenu	cena	k1gFnSc4
nucených	nucený	k2eAgFnPc2d1
prací	práce	k1gFnPc2
poddaných	poddaný	k1gMnPc2
<g/>
,	,	kIx,
přejímání	přejímání	k1gNnSc3
cizích	cizí	k2eAgInPc2d1
vzorů	vzor	k1gInPc2
a	a	k8xC
kultury	kultura	k1gFnSc2
a	a	k8xC
že	že	k8xS
měl	mít	k5eAaImAgInS
dokonce	dokonce	k9
také	také	k9
harém	harém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šalomounova	Šalomounův	k2eAgFnSc1d1
doba	doba	k1gFnSc1
tak	tak	k6eAd1
sice	sice	k8xC
byla	být	k5eAaImAgFnS
dobou	doba	k1gFnSc7
vzestupu	vzestup	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
vnitřně	vnitřně	k6eAd1
se	se	k3xPyFc4
již	již	k6eAd1
izraelská	izraelský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
počínala	počínat	k5eAaImAgFnS
rozkládat	rozkládat	k5eAaImF
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
–	–	k?
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
království	království	k1gNnSc1
rozdělilo	rozdělit	k5eAaPmAgNnS
na	na	k7c4
Severoizraelské	Severoizraelský	k2eAgNnSc4d1
a	a	k8xC
Judské	judský	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paralipomenon	paralipomenon	k1gNnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HELLER	HELLER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkladový	výkladový	k2eAgInSc1d1
slovník	slovník	k1gInSc1
biblických	biblický	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Advent-Orion	Advent-Orion	k1gInSc1
<g/>
/	/	kIx~
<g/>
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7172	#num#	k4
<g/>
-	-	kIx~
<g/>
865	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
725	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
451	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Universum	universum	k1gNnSc1
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
655	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
1071	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
Šalomoun	Šalomoun	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
260	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GANS	GANS	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ratolest	ratolest	k1gFnSc1
Davidova	Davidův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2535	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
1	#num#	k4
Kr	Kr	k1gFnSc1
3,16	3,16	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
↑	↑	k?
HORÁČEK	Horáček	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Džátaky	Džátak	k1gInPc4
-	-	kIx~
Legendární	legendární	k2eAgInPc1d1
životopisy	životopis	k1gInPc1
Gautamy	Gautam	k1gInPc7
Buddhy	Buddha	k1gMnSc2
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
Džátakatthavannana	Džátakatthavannana	k1gFnSc1
<g/>
.	.	kIx.
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
filosofie	filosofie	k1gFnSc2
a	a	k8xC
religionistiky	religionistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc2
Jiří	Jiří	k1gMnSc1
Holba	holba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Džátaky	Džátak	k1gInPc4
<g/>
:	:	kIx,
příběhy	příběh	k1gInPc4
z	z	k7c2
minulých	minulý	k2eAgInPc2d1
životů	život	k1gInPc2
Buddhy	Buddha	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Dušan	Dušan	k1gMnSc1
Zbavitel	zbavitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rozš	rozš	k5eAaPmIp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
DharmaGaia	DharmaGaia	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
230	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prameny	pramen	k1gInPc1
buddhismu	buddhismus	k1gInSc2
<g/>
;	;	kIx,
sv.	sv.	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86685	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
1	#num#	k4
Kr	Kr	k1gFnSc1
2,46	2,46	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Pak	pak	k6eAd1
dal	dát	k5eAaPmAgMnS
král	král	k1gMnSc1
příkaz	příkaz	k1gInSc4
Benajášovi	Benajáš	k1gMnSc3
<g/>
,	,	kIx,
synu	syn	k1gMnSc3
Jójadovu	Jójadův	k2eAgMnSc3d1
<g/>
,	,	kIx,
a	a	k8xC
ten	ten	k3xDgMnSc1
vyšel	vyjít	k5eAaPmAgMnS
a	a	k8xC
skolil	skolit	k5eAaPmAgMnS,k5eAaImAgMnS
ho	on	k3xPp3gMnSc4
[	[	kIx(
<g/>
Šimeího	Šimeí	k1gMnSc4
<g/>
]	]	kIx)
<g/>
;	;	kIx,
tak	tak	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
království	království	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
ruce	ruka	k1gFnSc6
Šalomounově	Šalomounův	k2eAgFnSc6d1
upevnilo	upevnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
↑	↑	k?
RENDTORFF	RENDTORFF	kA
<g/>
,	,	kIx,
Rolf	Rolf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hebrejská	hebrejský	k2eAgFnSc1d1
bible	bible	k1gFnSc1
a	a	k8xC
dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
starozákonní	starozákonní	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
634	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
59	#num#	k4
<g/>
-	-	kIx~
<g/>
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Královna	královna	k1gFnSc1
ze	z	k7c2
Sáby	Sába	k1gFnSc2
</s>
<s>
Menelik	Menelik	k1gMnSc1
I.	I.	kA
Etiopský	etiopský	k2eAgMnSc5d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
královská	královský	k2eAgFnSc1d1
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
chrám	chrám	k1gInSc1
</s>
<s>
Izraelské	izraelský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Judské	judský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Šalomounovská	Šalomounovský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šalomoun	Šalomoun	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Šalomoun	Šalomoun	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Šalamoun	Šalamoun	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Králové	Král	k1gMnPc1
Izraelského	izraelský	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Král	Král	k1gMnSc1
David	David	k1gMnSc1
</s>
<s>
970	#num#	k4
<g/>
–	–	k?
<g/>
931	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Šalomoun	Šalomoun	k1gMnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Judské	judský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
RechabeámSeveroizraelské	RechabeámSeveroizraelský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
Jarobeám	Jarobea	k1gFnPc3
I.	I.	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bible	bible	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000720339	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118605100	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0804	#num#	k4
0267	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80024516	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
100963493	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80024516	#num#	k4
</s>
