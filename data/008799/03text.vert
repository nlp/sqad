<p>
<s>
Hasicí	hasicí	k2eAgInSc1d1	hasicí
přístroj	přístroj	k1gInSc1	přístroj
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
hasičák	hasičák	k1gInSc1	hasičák
nebo	nebo	k8xC	nebo
minimax	minimax	k1gInSc1	minimax
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
věcných	věcný	k2eAgInPc2d1	věcný
prostředků	prostředek	k1gInPc2	prostředek
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
operativnímu	operativní	k2eAgNnSc3d1	operativní
zdolávání	zdolávání	k1gNnSc3	zdolávání
požárů	požár	k1gInPc2	požár
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
MV	MV	kA	MV
246	[number]	k4	246
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
stanovení	stanovení	k1gNnSc6	stanovení
podmínek	podmínka	k1gFnPc2	podmínka
požární	požární	k2eAgFnSc2d1	požární
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
výkonu	výkon	k1gInSc2	výkon
státního	státní	k2eAgInSc2d1	státní
požárního	požární	k2eAgInSc2d1	požární
dozoru	dozor	k1gInSc2	dozor
(	(	kIx(	(
<g/>
vyhláška	vyhláška	k1gFnSc1	vyhláška
o	o	k7c6	o
požární	požární	k2eAgFnSc6d1	požární
prevenci	prevence	k1gFnSc6	prevence
<g/>
)	)	kIx)	)
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vyhrazený	vyhrazený	k2eAgInSc4d1	vyhrazený
druh	druh	k1gInSc4	druh
věcného	věcný	k2eAgInSc2d1	věcný
prostředku	prostředek	k1gInSc2	prostředek
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jenž	k3xRgNnSc4	jenž
projektování	projektování	k1gNnSc4	projektování
<g/>
,	,	kIx,	,
instalaci	instalace	k1gFnSc4	instalace
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
opravu	oprava	k1gFnSc4	oprava
jsou	být	k5eAaImIp3nP	být
kladeny	kladen	k2eAgInPc1d1	kladen
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
požadavky	požadavek	k1gInPc1	požadavek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Projektování	projektování	k1gNnSc1	projektování
a	a	k8xC	a
instalace	instalace	k1gFnSc1	instalace
==	==	k?	==
</s>
</p>
<p>
<s>
Vybavení	vybavení	k1gNnSc1	vybavení
prostor	prostora	k1gFnPc2	prostora
hasicími	hasicí	k2eAgInPc7d1	hasicí
přístroji	přístroj	k1gInPc7	přístroj
určuje	určovat	k5eAaImIp3nS	určovat
požárně	požárně	k6eAd1	požárně
bezpečnostní	bezpečnostní	k2eAgNnSc1d1	bezpečnostní
řešení	řešení	k1gNnSc1	řešení
stavby	stavba	k1gFnSc2	stavba
nebo	nebo	k8xC	nebo
obdobná	obdobný	k2eAgFnSc1d1	obdobná
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ověřené	ověřený	k2eAgFnSc2d1	ověřená
projektové	projektový	k2eAgFnSc2d1	projektová
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
prostor	prostor	k1gInSc1	prostor
a	a	k8xC	a
zařízení	zařízení	k1gNnSc1	zařízení
právnických	právnický	k2eAgFnPc2d1	právnická
a	a	k8xC	a
podnikajících	podnikající	k2eAgFnPc2d1	podnikající
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
nebylo	být	k5eNaImAgNnS	být
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
stanoveny	stanovit	k5eAaPmNgInP	stanovit
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
povinné	povinný	k2eAgNnSc1d1	povinné
vybavení	vybavení	k1gNnSc1	vybavení
hasicími	hasicí	k2eAgInPc7d1	hasicí
přístroji	přístroj	k1gInPc7	přístroj
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gMnPc1	jejich
vlastníci	vlastník	k1gMnPc1	vlastník
(	(	kIx(	(
<g/>
provozovatelé	provozovatel	k1gMnPc1	provozovatel
<g/>
)	)	kIx)	)
povinni	povinen	k2eAgMnPc1d1	povinen
zajistit	zajistit	k5eAaPmF	zajistit
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
§	§	k?	§
2	[number]	k4	2
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
5	[number]	k4	5
vyhlášky	vyhláška	k1gFnSc2	vyhláška
MV	MV	kA	MV
č.	č.	k?	č.
246	[number]	k4	246
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
instalují	instalovat	k5eAaBmIp3nP	instalovat
alespoň	alespoň	k9	alespoň
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
hasební	hasební	k2eAgFnSc7d1	hasební
schopností	schopnost	k1gFnSc7	schopnost
na	na	k7c6	na
každých	každý	k3xTgInPc6	každý
započatých	započatý	k2eAgInPc6d1	započatý
200	[number]	k4	200
m2	m2	k4	m2
z	z	k7c2	z
půdorysné	půdorysný	k2eAgFnSc2d1	půdorysná
plochy	plocha	k1gFnSc2	plocha
podlaží	podlaží	k1gNnSc2	podlaží
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
vyhlášku	vyhláška	k1gFnSc4	vyhláška
MV	MV	kA	MV
23	[number]	k4	23
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
o	o	k7c6	o
technických	technický	k2eAgFnPc6d1	technická
podmínkách	podmínka	k1gFnPc6	podmínka
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
staveb	stavba	k1gFnPc2	stavba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
že	že	k8xS	že
minimálně	minimálně	k6eAd1	minimálně
jedním	jeden	k4xCgInSc7	jeden
hasicím	hasicí	k2eAgInSc7d1	hasicí
přístrojem	přístroj	k1gInSc7	přístroj
s	s	k7c7	s
hasební	hasební	k2eAgFnSc7d1	hasební
schopností	schopnost	k1gFnSc7	schopnost
34A	[number]	k4	34A
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
se	se	k3xPyFc4	se
instalují	instalovat	k5eAaBmIp3nP	instalovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
snadno	snadno	k6eAd1	snadno
viditelné	viditelný	k2eAgFnPc1d1	viditelná
a	a	k8xC	a
volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgNnSc1d1	přístupné
<g/>
.	.	kIx.	.
</s>
<s>
Hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
se	se	k3xPyFc4	se
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
riziko	riziko	k1gNnSc4	riziko
vzniku	vznik	k1gInSc2	vznik
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přenosné	přenosný	k2eAgInPc1d1	přenosný
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
svislé	svislý	k2eAgFnSc6d1	svislá
stavební	stavební	k2eAgFnSc6d1	stavební
konstrukci	konstrukce	k1gFnSc6	konstrukce
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
rukojeť	rukojeť	k1gFnSc4	rukojeť
max	max	kA	max
<g/>
.	.	kIx.	.
150	[number]	k4	150
cm	cm	kA	cm
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
umístěny	umístěn	k2eAgInPc1d1	umístěn
na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
stavební	stavební	k2eAgFnSc6d1	stavební
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
způsobem	způsob	k1gInSc7	způsob
zajištěny	zajistit	k5eAaPmNgFnP	zajistit
proti	proti	k7c3	proti
pádu	pád	k1gInSc3	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Provoz	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
,	,	kIx,	,
údržba	údržba	k1gFnSc1	údržba
a	a	k8xC	a
opravy	oprava	k1gFnPc1	oprava
==	==	k?	==
</s>
</p>
<p>
<s>
Provozuschopnost	provozuschopnost	k1gFnSc1	provozuschopnost
hasicího	hasicí	k2eAgInSc2d1	hasicí
přístroje	přístroj	k1gInSc2	přístroj
se	se	k3xPyFc4	se
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
dokladem	doklad	k1gInSc7	doklad
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
kontrole	kontrola	k1gFnSc6	kontrola
<g/>
,	,	kIx,	,
kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
štítkem	štítek	k1gInSc7	štítek
a	a	k8xC	a
plombou	plomba	k1gFnSc7	plomba
spouštěcí	spouštěcí	k2eAgFnSc2d1	spouštěcí
armatury	armatura	k1gFnSc2	armatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legislativa	legislativa	k1gFnSc1	legislativa
určuje	určovat	k5eAaImIp3nS	určovat
povinnost	povinnost	k1gFnSc4	povinnost
provádění	provádění	k1gNnSc3	provádění
každoročních	každoroční	k2eAgFnPc2d1	každoroční
kontrol	kontrola	k1gFnPc2	kontrola
provozuschopnosti	provozuschopnost	k1gFnSc2	provozuschopnost
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
periodických	periodický	k2eAgFnPc2d1	periodická
kontrol	kontrola	k1gFnPc2	kontrola
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
a	a	k8xC	a
pěnových	pěnový	k2eAgInPc2d1	pěnový
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kontroly	kontrola	k1gFnPc4	kontrola
<g/>
,	,	kIx,	,
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
opravy	oprava	k1gFnPc4	oprava
vyhrazených	vyhrazený	k2eAgInPc2d1	vyhrazený
druhů	druh	k1gInPc2	druh
věcných	věcný	k2eAgInPc2d1	věcný
prostředků	prostředek	k1gInPc2	prostředek
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
pouze	pouze	k6eAd1	pouze
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
splní	splnit	k5eAaPmIp3nS	splnit
podmínky	podmínka	k1gFnPc4	podmínka
stanovené	stanovený	k2eAgFnPc4d1	stanovená
právními	právní	k2eAgInPc7d1	právní
předpisy	předpis	k1gInPc7	předpis
<g/>
,	,	kIx,	,
normativními	normativní	k2eAgInPc7d1	normativní
požadavky	požadavek	k1gInPc7	požadavek
a	a	k8xC	a
průvodní	průvodní	k2eAgFnSc7d1	průvodní
dokumentací	dokumentace	k1gFnSc7	dokumentace
výrobce	výrobce	k1gMnSc2	výrobce
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
typu	typ	k1gInSc2	typ
hasicího	hasicí	k2eAgInSc2d1	hasicí
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pouze	pouze	k6eAd1	pouze
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přihlédnout	přihlédnout	k5eAaPmF	přihlédnout
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
HP	HP	kA	HP
spadá	spadat	k5eAaImIp3nS	spadat
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
18	[number]	k4	18
<g/>
/	/	kIx~	/
<g/>
1979	[number]	k4	1979
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Českého	český	k2eAgInSc2d1	český
úřadu	úřad	k1gInSc2	úřad
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
Českého	český	k2eAgInSc2d1	český
báňského	báňský	k2eAgInSc2d1	báňský
úřadu	úřad	k1gInSc2	úřad
mezi	mezi	k7c4	mezi
vyhrazená	vyhrazený	k2eAgNnPc4d1	vyhrazené
tlaková	tlakový	k2eAgNnPc4d1	tlakové
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
dodržet	dodržet	k5eAaPmF	dodržet
další	další	k2eAgNnPc4d1	další
opatření	opatření	k1gNnPc4	opatření
vztahující	vztahující	k2eAgNnPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
vyhrazeným	vyhrazený	k2eAgNnPc3d1	vyhrazené
tlakovým	tlakový	k2eAgNnPc3d1	tlakové
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vhodné	vhodný	k2eAgNnSc1d1	vhodné
uchycení	uchycení	k1gNnSc1	uchycení
HP	HP	kA	HP
<g/>
,	,	kIx,	,
provádění	provádění	k1gNnSc2	provádění
revizí	revize	k1gFnPc2	revize
a	a	k8xC	a
tlakových	tlakový	k2eAgFnPc2d1	tlaková
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obvykle	obvykle	k6eAd1	obvykle
zajistí	zajistit	k5eAaPmIp3nS	zajistit
revizní	revizní	k2eAgMnSc1d1	revizní
technik	technik	k1gMnSc1	technik
oprávněný	oprávněný	k2eAgMnSc1d1	oprávněný
k	k	k7c3	k
revizím	revize	k1gFnPc3	revize
HP	HP	kA	HP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
hasicí	hasicí	k2eAgFnSc2d1	hasicí
látky	látka	k1gFnSc2	látka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vodní	vodní	k2eAgFnPc1d1	vodní
===	===	k?	===
</s>
</p>
<p>
<s>
Hasicí	hasicí	k2eAgFnSc7d1	hasicí
látkou	látka	k1gFnSc7	látka
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
uhličitan	uhličitan	k1gInSc4	uhličitan
draselný	draselný	k2eAgInSc4d1	draselný
<g/>
,	,	kIx,	,
chránící	chránící	k2eAgInSc4d1	chránící
proti	proti	k7c3	proti
zamrznutí	zamrznutí	k1gNnSc3	zamrznutí
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
hasit	hasit	k5eAaImF	hasit
elektrická	elektrický	k2eAgNnPc1d1	elektrické
zařízení	zařízení	k1gNnPc1	zařízení
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
,	,	kIx,	,
hasebním	hasební	k2eAgInSc7d1	hasební
účinkem	účinek	k1gInSc7	účinek
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
především	především	k9	především
ochlazování	ochlazování	k1gNnSc1	ochlazování
(	(	kIx(	(
<g/>
chladicí	chladicí	k2eAgInSc1d1	chladicí
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInPc1d1	vodní
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
hašení	hašení	k1gNnSc4	hašení
požárů	požár	k1gInPc2	požár
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
hasení	hasení	k1gNnSc3	hasení
hořících	hořící	k2eAgInPc2d1	hořící
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
k	k	k7c3	k
hasení	hasení	k1gNnSc3	hasení
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
ani	ani	k8xC	ani
k	k	k7c3	k
hašení	hašení	k1gNnSc3	hašení
hořících	hořící	k2eAgInPc2d1	hořící
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
byl	být	k5eAaImAgInS	být
přístroj	přístroj	k1gInSc1	přístroj
Minimax	minimax	k1gInSc1	minimax
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pěnový	pěnový	k2eAgMnSc1d1	pěnový
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
vody	voda	k1gFnSc2	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
pěnidlo	pěnidlo	k1gNnSc1	pěnidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
při	při	k7c6	při
provzdušnění	provzdušnění	k1gNnSc6	provzdušnění
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pěnu	pěna	k1gFnSc4	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pěnidlu	pěnidlo	k1gNnSc3	pěnidlo
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
snazšímu	snadný	k2eAgNnSc3d2	snazší
smáčení	smáčení	k1gNnSc3	smáčení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
pěna	pěna	k1gFnSc1	pěna
izoluje	izolovat	k5eAaBmIp3nS	izolovat
hořící	hořící	k2eAgFnPc4d1	hořící
látky	látka	k1gFnPc4	látka
od	od	k7c2	od
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
–	–	k?	–
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
dusivého	dusivý	k2eAgInSc2d1	dusivý
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
voda	voda	k1gFnSc1	voda
hasí	hasit	k5eAaImIp3nS	hasit
pevné	pevný	k2eAgFnPc4d1	pevná
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
hašení	hašení	k1gNnSc3	hašení
hořlavých	hořlavý	k2eAgFnPc2d1	hořlavá
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
pěnidla	pěnidlo	k1gNnSc2	pěnidlo
typu	typ	k1gInSc2	typ
AR	ar	k1gInSc1	ar
(	(	kIx(	(
<g/>
alcohol	alcohol	k1gInSc1	alcohol
resistant	resistant	k1gMnSc1	resistant
<g/>
)	)	kIx)	)
jimi	on	k3xPp3gInPc7	on
lze	lze	k6eAd1	lze
hasit	hasit	k5eAaImF	hasit
i	i	k9	i
polární	polární	k2eAgFnPc4d1	polární
kapaliny	kapalina	k1gFnPc4	kapalina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
líh	líh	k1gInSc4	líh
nebo	nebo	k8xC	nebo
aceton	aceton	k1gInSc4	aceton
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
standardní	standardní	k2eAgFnPc4d1	standardní
pěnu	pěna	k1gFnSc4	pěna
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
pěnidel	pěnidlo	k1gNnPc2	pěnidlo
typu	typ	k1gInSc2	typ
AFFF	AFFF	kA	AFFF
významně	významně	k6eAd1	významně
působí	působit	k5eAaImIp3nS	působit
izolační	izolační	k2eAgInSc1d1	izolační
efekt	efekt	k1gInSc1	efekt
(	(	kIx(	(
<g/>
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
výstupu	výstup	k1gInSc2	výstup
hořlavých	hořlavý	k2eAgFnPc2d1	hořlavá
par	para	k1gFnPc2	para
a	a	k8xC	a
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
pásma	pásmo	k1gNnSc2	pásmo
hoření	hoření	k1gNnSc2	hoření
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
přístupu	přístup	k1gInSc2	přístup
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přístrojem	přístroj	k1gInSc7	přístroj
nelze	lze	k6eNd1	lze
hasit	hasit	k5eAaImF	hasit
elektrická	elektrický	k2eAgNnPc1d1	elektrické
zařízení	zařízení	k1gNnPc1	zařízení
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pěna	pěna	k1gFnSc1	pěna
je	být	k5eAaImIp3nS	být
vodivá	vodivý	k2eAgFnSc1d1	vodivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
na	na	k7c4	na
hašení	hašení	k1gNnSc4	hašení
požárů	požár	k1gInPc2	požár
třídy	třída	k1gFnSc2	třída
F	F	kA	F
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
styku	styk	k1gInSc6	styk
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
hořícím	hořící	k2eAgInSc7d1	hořící
tukem	tuk	k1gInSc7	tuk
nebo	nebo	k8xC	nebo
olejem	olej	k1gInSc7	olej
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
rozšíření	rozšíření	k1gNnSc3	rozšíření
požáru	požár	k1gInSc2	požár
až	až	k8xS	až
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Práškový	práškový	k2eAgMnSc1d1	práškový
===	===	k?	===
</s>
</p>
<p>
<s>
Hasivem	Hasiv	k1gInSc7	Hasiv
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc1d1	speciální
nebo	nebo	k8xC	nebo
univerzální	univerzální	k2eAgInSc1d1	univerzální
jemný	jemný	k2eAgInSc1d1	jemný
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
hnaný	hnaný	k2eAgInSc1d1	hnaný
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
účinné	účinný	k2eAgNnSc1d1	účinné
hasivo	hasivo	k1gNnSc1	hasivo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
nevodivost	nevodivost	k1gFnSc1	nevodivost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
hasit	hasit	k5eAaImF	hasit
i	i	k8xC	i
elektrická	elektrický	k2eAgNnPc1d1	elektrické
zařízení	zařízení	k1gNnPc1	zařízení
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Hasebním	hasební	k2eAgInSc7d1	hasební
efektem	efekt	k1gInSc7	efekt
je	být	k5eAaImIp3nS	být
stěnový	stěnový	k2eAgInSc1d1	stěnový
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
přístroje	přístroj	k1gInPc1	přístroj
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
prach	prach	k1gInSc4	prach
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgNnPc1d1	elektronické
zařízení	zařízení	k1gNnPc1	zařízení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
hasivo	hasivo	k1gNnSc1	hasivo
třídy	třída	k1gFnSc2	třída
BC	BC	kA	BC
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
sodný	sodný	k2eAgInSc1d1	sodný
–	–	k?	–
NaHCO	NaHCO	k1gFnSc7	NaHCO
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
draselný	draselný	k2eAgInSc1d1	draselný
–	–	k?	–
KHCO3	KHCO3	k1gFnPc2	KHCO3
</s>
</p>
<p>
<s>
síran	síran	k1gInSc1	síran
draselný	draselný	k2eAgInSc1d1	draselný
–	–	k?	–
K2SO4	K2SO4	k1gFnSc7	K2SO4
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
–	–	k?	–
CaCO	CaCO	k1gFnSc7	CaCO
<g/>
3	[number]	k4	3
<g/>
Hasící	hasící	k2eAgFnPc4d1	hasící
směsi	směs	k1gFnPc4	směs
BC	BC	kA	BC
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Monnex	Monnex	k1gInSc1	Monnex
-	-	kIx~	-
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
draselný	draselný	k2eAgInSc1d1	draselný
–	–	k?	–
KHCO3	KHCO3	k1gFnSc1	KHCO3
a	a	k8xC	a
močovina	močovina	k1gFnSc1	močovina
H2N-CO-NH2	H2N-CO-NH2	k1gFnSc2	H2N-CO-NH2
</s>
</p>
<p>
<s>
Totalit	totalita	k1gFnPc2	totalita
2000	[number]	k4	2000
–	–	k?	–
vyroben	vyroben	k2eAgMnSc1d1	vyroben
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
síranu	síran	k1gInSc2	síran
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
použít	použít	k5eAaPmF	použít
i	i	k9	i
k	k	k7c3	k
hašení	hašení	k1gNnSc3	hašení
hořících	hořící	k2eAgInPc2d1	hořící
plastů	plast	k1gInPc2	plast
</s>
</p>
<p>
<s>
Totalit	totalita	k1gFnPc2	totalita
Super	super	k1gInSc2	super
–	–	k?	–
vyroben	vyroben	k2eAgInSc1d1	vyroben
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
hydrogenuhličitanu	hydrogenuhličitat	k5eAaPmIp1nS	hydrogenuhličitat
sodnéhoJako	sodnéhoJako	k6eAd1	sodnéhoJako
hasivo	hasivo	k1gNnSc1	hasivo
třídy	třída	k1gFnSc2	třída
ABC	ABC	kA	ABC
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hydrogenfosforečnan	Hydrogenfosforečnan	k1gMnSc1	Hydrogenfosforečnan
amonný	amonný	k2eAgMnSc1d1	amonný
–	–	k?	–
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
HPO	HPO	kA	HPO
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
Dihydrogenfosforečnan	Dihydrogenfosforečnan	k1gMnSc1	Dihydrogenfosforečnan
amonný	amonný	k2eAgMnSc1d1	amonný
–	–	k?	–
NH4H2PO4	NH4H2PO4	k1gMnSc1	NH4H2PO4
</s>
</p>
<p>
<s>
Síran	síran	k1gInSc1	síran
amonný	amonný	k2eAgInSc1d1	amonný
–	–	k?	–
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
Hasící	hasící	k2eAgFnSc2d1	hasící
směsi	směs	k1gFnSc2	směs
ABC	ABC	kA	ABC
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Neutrex	Neutrex	k1gInSc1	Neutrex
ABC	ABC	kA	ABC
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
složka	složka	k1gFnSc1	složka
dihydrogenfosforečnan	dihydrogenfosforečnana	k1gFnPc2	dihydrogenfosforečnana
amonný	amonný	k2eAgInSc1d1	amonný
</s>
</p>
<p>
<s>
Totalit	totalita	k1gFnPc2	totalita
GJako	GJako	k1gNnSc1	GJako
hasivo	hasivo	k1gNnSc4	hasivo
při	při	k7c6	při
hašení	hašení	k1gNnSc6	hašení
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
–	–	k?	–
NaCl	NaCl	k1gInSc1	NaCl
</s>
</p>
<p>
<s>
Tetraboritan	tetraboritan	k1gInSc1	tetraboritan
(	(	kIx(	(
<g/>
di	di	k?	di
<g/>
)	)	kIx)	)
<g/>
sodný	sodný	k2eAgInSc4d1	sodný
(	(	kIx(	(
<g/>
borax	borax	k1gInSc4	borax
<g/>
;	;	kIx,	;
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uhličitan	uhličitan	k1gInSc4	uhličitan
sodný	sodný	k2eAgInSc4d1	sodný
–	–	k?	–
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
Hasicí	hasicí	k2eAgFnSc4d1	hasicí
směs	směs	k1gFnSc4	směs
na	na	k7c4	na
kovy	kov	k1gInPc4	kov
je	být	k5eAaImIp3nS	být
především	především	k9	především
Totalit	totalita	k1gFnPc2	totalita
M.	M.	kA	M.
</s>
</p>
<p>
<s>
===	===	k?	===
Sněhový	sněhový	k2eAgMnSc1d1	sněhový
===	===	k?	===
</s>
</p>
<p>
<s>
Hasivem	Hasiv	k1gInSc7	Hasiv
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Hasivo	Hasivo	k1gNnSc1	Hasivo
má	mít	k5eAaImIp3nS	mít
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
tlakové	tlakový	k2eAgFnSc2d1	tlaková
nádoby	nádoba	k1gFnSc2	nádoba
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
–	–	k?	–
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
hubice	hubice	k1gFnSc2	hubice
asi	asi	k9	asi
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
hubici	hubice	k1gFnSc4	hubice
držet	držet	k5eAaImF	držet
jen	jen	k9	jen
za	za	k7c4	za
držadlo	držadlo	k1gNnSc4	držadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
jím	jíst	k5eAaImIp1nS	jíst
hasit	hasit	k5eAaImF	hasit
sypké	sypký	k2eAgInPc4d1	sypký
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
proud	proud	k1gInSc1	proud
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
prudký	prudký	k2eAgInSc1d1	prudký
a	a	k8xC	a
hořící	hořící	k2eAgInSc1d1	hořící
sypký	sypký	k2eAgInSc1d1	sypký
materiál	materiál	k1gInSc1	materiál
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
rozfouknut	rozfouknout	k5eAaPmNgInS	rozfouknout
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgMnSc1d1	vhodný
pro	pro	k7c4	pro
hašení	hašení	k1gNnPc4	hašení
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
dřeva	dřevo	k1gNnSc2	dřevo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hašení	hašení	k1gNnSc3	hašení
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
nejen	nejen	k6eAd1	nejen
sníh	sníh	k1gInSc4	sníh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
z	z	k7c2	z
hubice	hubice	k1gFnSc2	hubice
uniká	unikat	k5eAaImIp3nS	unikat
po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Přístrojem	přístroj	k1gInSc7	přístroj
lze	lze	k6eAd1	lze
hasit	hasit	k5eAaImF	hasit
elektronická	elektronický	k2eAgNnPc1d1	elektronické
zařízení	zařízení	k1gNnPc1	zařízení
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
do	do	k7c2	do
1	[number]	k4	1
kV	kV	k?	kV
<g/>
,	,	kIx,	,
hořlavé	hořlavý	k2eAgFnPc1d1	hořlavá
kapaliny	kapalina	k1gFnPc1	kapalina
i	i	k8xC	i
hořící	hořící	k2eAgInPc1d1	hořící
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Halonový	Halonový	k2eAgMnSc1d1	Halonový
===	===	k?	===
</s>
</p>
<p>
<s>
Hasivem	Hasiv	k1gInSc7	Hasiv
jsou	být	k5eAaImIp3nP	být
halonové	halonový	k2eAgInPc1d1	halonový
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejúčinnější	účinný	k2eAgFnSc4d3	nejúčinnější
hasební	hasební	k2eAgFnSc4d1	hasební
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Hasebním	hasební	k2eAgInSc7d1	hasební
účinkem	účinek	k1gInSc7	účinek
je	být	k5eAaImIp3nS	být
fyzikálně-chemický	fyzikálněhemický	k2eAgInSc1d1	fyzikálně-chemický
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Hasivo	Hasivo	k1gNnSc1	Hasivo
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
ozónovou	ozónový	k2eAgFnSc4d1	ozónová
vrstvu	vrstva	k1gFnSc4	vrstva
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používat	používat	k5eAaImF	používat
ve	v	k7c6	v
špatně	špatně	k6eAd1	špatně
větratelných	větratelný	k2eAgFnPc6d1	větratelná
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
na	na	k7c4	na
žhoucí	žhoucí	k2eAgFnPc4d1	žhoucí
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
na	na	k7c4	na
požáry	požár	k1gInPc4	požár
lehkých	lehký	k2eAgInPc2d1	lehký
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc4	některý
hasiva	hasivo	k1gNnPc4	hasivo
do	do	k7c2	do
halonových	halonův	k2eAgInPc2d1	halonův
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
(	(	kIx(	(
<g/>
Halon	halon	k1gInSc1	halon
1211	[number]	k4	1211
<g/>
,	,	kIx,	,
1301	[number]	k4	1301
<g/>
,	,	kIx,	,
hasivo	hasivo	k1gNnSc1	hasivo
FM	FM	kA	FM
<g/>
100	[number]	k4	100
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
(	(	kIx(	(
<g/>
hasivo	hasivo	k1gNnSc4	hasivo
Halotron	Halotron	k1gInSc1	Halotron
I	i	k9	i
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
jejich	jejich	k3xOp3gFnPc1	jejich
velmi	velmi	k6eAd1	velmi
účinné	účinný	k2eAgFnPc1d1	účinná
náhrady	náhrada	k1gFnPc1	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nevyrábějí	vyrábět	k5eNaImIp3nP	vyrábět
a	a	k8xC	a
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
je	on	k3xPp3gInPc4	on
přístroje	přístroj	k1gInPc4	přístroj
plněné	plněný	k2eAgFnSc2d1	plněná
CO2	CO2	k1gFnSc2	CO2
(	(	kIx(	(
<g/>
sněhové	sněhový	k2eAgFnPc1d1	sněhová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novými	nový	k2eAgFnPc7d1	nová
náhradami	náhrada	k1gFnPc7	náhrada
jsou	být	k5eAaImIp3nP	být
hasicí	hasicí	k2eAgInPc4d1	hasicí
přístroje	přístroj	k1gInPc4	přístroj
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
hexafluoropropanu	hexafluoropropan	k1gInSc2	hexafluoropropan
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hasivo	hasivo	k1gNnSc1	hasivo
není	být	k5eNaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
žádnými	žádný	k3yNgFnPc7	žádný
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
úmluvami	úmluva	k1gFnPc7	úmluva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
netoxické	toxický	k2eNgNnSc1d1	netoxické
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
při	při	k7c6	při
hašení	hašení	k1gNnSc6	hašení
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
výhodu	výhoda	k1gFnSc4	výhoda
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
nejúčinnějším	účinný	k2eAgInSc7d3	nejúčinnější
hasivem	hasiv	k1gInSc7	hasiv
<g/>
.	.	kIx.	.
</s>
<s>
Hasí	hasit	k5eAaImIp3nP	hasit
všechny	všechen	k3xTgFnPc4	všechen
běžné	běžný	k2eAgFnPc4d1	běžná
hořlaviny	hořlavina	k1gFnPc4	hořlavina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
žhnutí	žhnutí	k1gNnSc4	žhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
čisté	čistý	k2eAgNnSc1d1	čisté
hasivo	hasivo	k1gNnSc1	hasivo
(	(	kIx(	(
<g/>
halotron	halotron	k1gInSc1	halotron
uhasí	uhasit	k5eAaPmIp3nS	uhasit
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozptýlí	rozptýlit	k5eAaPmIp3nS	rozptýlit
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
nezůstanou	zůstat	k5eNaPmIp3nP	zůstat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
stopy	stopa	k1gFnPc4	stopa
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vhodný	vhodný	k2eAgInSc1d1	vhodný
např.	např.	kA	např.
pro	pro	k7c4	pro
archivy	archiv	k1gInPc4	archiv
-	-	kIx~	-
bez	bez	k7c2	bez
rizika	riziko	k1gNnSc2	riziko
promáčení	promáčení	k1gNnSc2	promáčení
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Nezůstávají	zůstávat	k5eNaImIp3nP	zůstávat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
žádná	žádný	k3yNgNnPc4	žádný
rezidua	reziduum	k1gNnPc4	reziduum
a	a	k8xC	a
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
teplotní	teplotní	k2eAgInSc4d1	teplotní
šok	šok	k1gInSc4	šok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
poměrně	poměrně	k6eAd1	poměrně
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
5	[number]	k4	5
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tetrachlórový	tetrachlórový	k2eAgMnSc1d1	tetrachlórový
====	====	k?	====
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
plněné	plněný	k2eAgInPc1d1	plněný
tetrachlormethanem	tetrachlormethan	k1gInSc7	tetrachlormethan
<g/>
,	,	kIx,	,
při	při	k7c6	při
hašení	hašení	k1gNnSc6	hašení
však	však	k9	však
vznikají	vznikat	k5eAaImIp3nP	vznikat
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
zplodiny	zplodina	k1gFnPc4	zplodina
obsahující	obsahující	k2eAgInSc4d1	obsahující
fosgen	fosgen	k1gInSc4	fosgen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
podle	podle	k7c2	podle
provedení	provedení	k1gNnSc2	provedení
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
provedení	provedení	k1gNnSc2	provedení
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
přenosné	přenosný	k2eAgInPc4d1	přenosný
hasicí	hasicí	k2eAgInPc4d1	hasicí
přístroje	přístroj	k1gInPc4	přístroj
</s>
</p>
<p>
<s>
pojízdné	pojízdný	k2eAgInPc1d1	pojízdný
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
</s>
</p>
<p>
<s>
přívěsné	přívěsný	k2eAgInPc1d1	přívěsný
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
</s>
</p>
<p>
<s>
==	==	k?	==
Hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
podle	podle	k7c2	podle
tříd	třída	k1gFnPc2	třída
požárů	požár	k1gInPc2	požár
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
hasicí	hasicí	k2eAgInPc4d1	hasicí
přístroje	přístroj	k1gInPc4	přístroj
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
na	na	k7c4	na
požáry	požár	k1gInPc4	požár
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
se	se	k3xPyFc4	se
požáry	požár	k1gInPc1	požár
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
tříd	třída	k1gFnPc2	třída
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A	a	k9	a
–	–	k?	–
pevné	pevný	k2eAgFnSc2d1	pevná
látky	látka	k1gFnSc2	látka
–	–	k?	–
vhodné	vhodný	k2eAgInPc1d1	vhodný
jsou	být	k5eAaImIp3nP	být
vodní	vodní	k2eAgInPc1d1	vodní
<g/>
,	,	kIx,	,
pěnové	pěnový	k2eAgInPc1d1	pěnový
a	a	k8xC	a
práškové	práškový	k2eAgInPc1d1	práškový
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
</s>
</p>
<p>
<s>
B	B	kA	B
–	–	k?	–
hořlavé	hořlavý	k2eAgFnSc2d1	hořlavá
kapaliny	kapalina	k1gFnSc2	kapalina
–	–	k?	–
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
pěnové	pěnový	k2eAgInPc4d1	pěnový
<g/>
,	,	kIx,	,
práškové	práškový	k2eAgInPc4d1	práškový
a	a	k8xC	a
sněhové	sněhový	k2eAgInPc4d1	sněhový
hasicí	hasicí	k2eAgInPc4d1	hasicí
přístroje	přístroj	k1gInPc4	přístroj
</s>
</p>
<p>
<s>
C	C	kA	C
–	–	k?	–
plyny	plyn	k1gInPc1	plyn
–	–	k?	–
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
práškové	práškový	k2eAgInPc1d1	práškový
a	a	k8xC	a
sněhové	sněhový	k2eAgInPc1d1	sněhový
hasicí	hasicí	k2eAgInPc1d1	hasicí
přístroje	přístroj	k1gInPc1	přístroj
</s>
</p>
<p>
<s>
D	D	kA	D
–	–	k?	–
hořlavé	hořlavý	k2eAgInPc4d1	hořlavý
kovy	kov	k1gInPc4	kov
(	(	kIx(	(
<g/>
lehké	lehký	k2eAgInPc4d1	lehký
a	a	k8xC	a
alkalické	alkalický	k2eAgInPc4d1	alkalický
kovy	kov	k1gInPc4	kov
<g/>
)	)	kIx)	)
–	–	k?	–
hasí	hasit	k5eAaImIp3nS	hasit
se	s	k7c7	s
speciálními	speciální	k2eAgInPc7d1	speciální
prášky	prášek	k1gInPc7	prášek
</s>
</p>
<p>
<s>
E	E	kA	E
–	–	k?	–
elektrické	elektrický	k2eAgNnSc4d1	elektrické
zařízení	zařízení	k1gNnSc4	zařízení
pod	pod	k7c7	pod
proudem	proud	k1gInSc7	proud
–	–	k?	–
hasí	hasit	k5eAaImIp3nS	hasit
se	se	k3xPyFc4	se
do	do	k7c2	do
1000	[number]	k4	1000
V	v	k7c6	v
práškovým	práškový	k2eAgInSc7d1	práškový
nebo	nebo	k8xC	nebo
sněhovým	sněhový	k2eAgInSc7d1	sněhový
přístrojem	přístroj	k1gInSc7	přístroj
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
1000	[number]	k4	1000
V	V	kA	V
speciálními	speciální	k2eAgInPc7d1	speciální
přístroji	přístroj	k1gInPc7	přístroj
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
F	F	kA	F
–	–	k?	–
jedlé	jedlý	k2eAgInPc4d1	jedlý
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
a	a	k8xC	a
živočišné	živočišný	k2eAgInPc4d1	živočišný
oleje	olej	k1gInPc4	olej
a	a	k8xC	a
tuky	tuk	k1gInPc4	tuk
v	v	k7c6	v
kuchyňských	kuchyňský	k2eAgNnPc6d1	kuchyňské
zařízeních	zařízení	k1gNnPc6	zařízení
–	–	k?	–
hasí	hasit	k5eAaImIp3nS	hasit
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
hasivemNa	hasivemNa	k6eAd1	hasivemNa
hasicích	hasicí	k2eAgInPc6d1	hasicí
přístrojích	přístroj	k1gInPc6	přístroj
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
hasicí	hasicí	k2eAgFnSc1d1	hasicí
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
"	"	kIx"	"
<g/>
34	[number]	k4	34
A	A	kA	A
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hasicí	hasicí	k2eAgInSc4d1	hasicí
přístroj	přístroj	k1gInSc4	přístroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
hašení	hašení	k1gNnSc4	hašení
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
uhasit	uhasit	k5eAaPmF	uhasit
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
požáru	požár	k1gInSc3	požár
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
hranice	hranice	k1gFnSc2	hranice
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
3,4	[number]	k4	3,4
m	m	kA	m
<g/>
,	,	kIx,	,
široké	široký	k2eAgFnSc2d1	široká
0,5	[number]	k4	0,5
m	m	kA	m
a	a	k8xC	a
vysoké	vysoká	k1gFnPc1	vysoká
0,56	[number]	k4	0,56
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hasicí	hasicí	k2eAgInSc4d1	hasicí
přístroj	přístroj	k1gInSc4	přístroj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
