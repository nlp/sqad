<p>
<s>
Ulysses	Ulysses	k1gInSc1	Ulysses
Simpson	Simpson	k1gNnSc1	Simpson
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Hiram	Hiram	k1gInSc1	Hiram
Ulysses	Ulysses	k1gInSc1	Ulysses
Grant	grant	k1gInSc1	grant
<g/>
;	;	kIx,	;
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1822	[number]	k4	1822
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
Unie	unie	k1gFnSc2	unie
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1869	[number]	k4	1869
až	až	k9	až
1877	[number]	k4	1877
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
18	[number]	k4	18
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojenským	vojenský	k2eAgMnSc7d1	vojenský
historikem	historik	k1gMnSc7	historik
J.	J.	kA	J.
F.	F.	kA	F.
C.	C.	kA	C.
Fullerem	Fuller	k1gInSc7	Fuller
byl	být	k5eAaImAgInS	být
Grant	grant	k1gInSc1	grant
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
největšího	veliký	k2eAgMnSc4d3	veliký
generála	generál	k1gMnSc4	generál
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
stratégů	stratég	k1gMnPc2	stratég
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mnoho	mnoho	k4c4	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
velícím	velící	k2eAgMnSc7d1	velící
generálem	generál	k1gMnSc7	generál
všech	všecek	k3xTgFnPc2	všecek
unijních	unijní	k2eAgFnPc2d1	unijní
armád	armáda	k1gFnPc2	armáda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisována	připisován	k2eAgFnSc1d1	připisována
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
úloha	úloha	k1gFnSc1	úloha
ve	v	k7c6	v
vítězství	vítězství	k1gNnSc6	vítězství
Unie	unie	k1gFnSc2	unie
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
kritickému	kritický	k2eAgInSc3d1	kritický
pohledu	pohled	k1gInSc2	pohled
mnoha	mnoho	k4c2	mnoho
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
považují	považovat	k5eAaImIp3nP	považovat
jeho	jeho	k3xOp3gNnSc4	jeho
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejslabších	slabý	k2eAgInPc2d3	nejslabší
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
měla	mít	k5eAaImAgFnS	mít
bezesporu	bezesporu	k9	bezesporu
stabilizující	stabilizující	k2eAgInSc4d1	stabilizující
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
kritickém	kritický	k2eAgNnSc6d1	kritické
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgMnPc2d1	významný
veřejně	veřejně	k6eAd1	veřejně
působících	působící	k2eAgMnPc2d1	působící
současníků	současník	k1gMnPc2	současník
byl	být	k5eAaImAgInS	být
uznáván	uznáván	k2eAgMnSc1d1	uznáván
jak	jak	k8xS	jak
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
státech	stát	k1gInPc6	stát
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
necitlivost	necitlivost	k1gFnSc1	necitlivost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
vzbuzení	vzbuzení	k1gNnSc4	vzbuzení
pocitu	pocit	k1gInSc2	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Sever	sever	k1gInSc1	sever
mstí	mstít	k5eAaImIp3nS	mstít
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
nové	nový	k2eAgNnSc4d1	nové
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
celá	celý	k2eAgFnSc1d1	celá
federace	federace	k1gFnSc1	federace
vyrovnávala	vyrovnávat	k5eAaImAgFnS	vyrovnávat
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nastolil	nastolit	k5eAaPmAgMnS	nastolit
nové	nový	k2eAgInPc4d1	nový
problémy	problém	k1gInPc4	problém
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
rovnoprávnosti	rovnoprávnost	k1gFnSc2	rovnoprávnost
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
nakonec	nakonec	k6eAd1	nakonec
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
občanská	občanský	k2eAgNnPc4d1	občanské
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
administrativa	administrativa	k1gFnSc1	administrativa
zamořena	zamořen	k2eAgFnSc1d1	zamořena
korupcí	korupce	k1gFnSc7	korupce
a	a	k8xC	a
častými	častý	k2eAgInPc7d1	častý
skandály	skandál	k1gInPc7	skandál
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
bezúhonný	bezúhonný	k2eAgInSc4d1	bezúhonný
<g/>
.	.	kIx.	.
</s>
<s>
Historiky	historik	k1gMnPc4	historik
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
spíš	spíš	k9	spíš
vyčítáno	vyčítán	k2eAgNnSc4d1	vyčítáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
razantněji	razantně	k6eAd2	razantně
nepostavil	postavit	k5eNaPmAgMnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnPc1d2	novější
studie	studie	k1gFnPc1	studie
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
jeho	jeho	k3xOp3gFnPc6	jeho
administrativa	administrativa	k1gFnSc1	administrativa
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
podpora	podpora	k1gFnSc1	podpora
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
černochů	černoch	k1gMnPc2	černoch
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
možnosti	možnost	k1gFnPc4	možnost
obsazovat	obsazovat	k5eAaImF	obsazovat
veřejné	veřejný	k2eAgFnPc4d1	veřejná
pozice	pozice	k1gFnPc4	pozice
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
především	především	k6eAd1	především
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
velmi	velmi	k6eAd1	velmi
nepopulární	populární	k2eNgMnSc1d1	nepopulární
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
získaly	získat	k5eAaPmAgFnP	získat
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Point	pointa	k1gFnPc2	pointa
Pleasant	Pleasanta	k1gFnPc2	Pleasanta
v	v	k7c6	v
Clermontském	Clermontský	k2eAgInSc6d1	Clermontský
okrese	okres	k1gInSc6	okres
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
40	[number]	k4	40
km	km	kA	km
od	od	k7c2	od
Cincinnati	Cincinnati	k1gFnSc2	Cincinnati
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Jesse	Jesse	k1gFnSc2	Jesse
Root	Root	k1gMnSc1	Root
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
1794	[number]	k4	1794
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
koželuhem	koželuh	k1gMnSc7	koželuh
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Hannah	Hannaha	k1gFnPc2	Hannaha
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Simpson	Simpson	k1gInSc1	Simpson
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
–	–	k?	–
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Georgetown	Georgetowna	k1gFnPc2	Georgetowna
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brown	Browna	k1gFnPc2	Browna
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
až	až	k9	až
do	do	k7c2	do
věku	věk	k1gInSc2	věk
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stěží	stěží	k6eAd1	stěží
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
ve	v	k7c4	v
West	West	k1gInSc4	West
Pointu	pointa	k1gFnSc4	pointa
(	(	kIx(	(
<g/>
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
se	se	k3xPyFc4	se
vešel	vejít	k5eAaPmAgInS	vejít
do	do	k7c2	do
požadavku	požadavek	k1gInSc2	požadavek
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
kongresmana	kongresman	k1gMnSc2	kongresman
Thomase	Thomas	k1gMnSc2	Thomas
L.	L.	kA	L.
Hamera	Hamer	k1gMnSc2	Hamer
kadetem	kadet	k1gMnSc7	kadet
této	tento	k3xDgFnSc2	tento
vojenské	vojenský	k2eAgFnSc2d1	vojenská
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Hamer	Hamry	k1gInPc2	Hamry
jej	on	k3xPp3gInSc4	on
chybně	chybně	k6eAd1	chybně
nominoval	nominovat	k5eAaBmAgMnS	nominovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Ulysses	Ulysses	k1gInSc1	Ulysses
Simpson	Simpson	k1gInSc1	Simpson
Grant	grant	k1gInSc4	grant
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
změně	změna	k1gFnSc3	změna
jména	jméno	k1gNnPc4	jméno
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
,	,	kIx,	,
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
porazit	porazit	k5eAaPmF	porazit
státní	státní	k2eAgFnSc4d1	státní
byrokracii	byrokracie	k1gFnSc4	byrokracie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studií	studie	k1gFnPc2	studie
dál	daleko	k6eAd2	daleko
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užíval	užívat	k5eAaImAgInS	užívat
jen	jen	k9	jen
střední	střední	k2eAgFnSc4d1	střední
iniciálu	iniciála	k1gFnSc4	iniciála
S.	S.	kA	S.
</s>
</p>
<p>
<s>
Akademii	akademie	k1gFnSc4	akademie
ve	v	k7c4	v
West	West	k1gInSc4	West
Pointu	pointa	k1gFnSc4	pointa
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
39	[number]	k4	39
spolužáky	spolužák	k1gMnPc7	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
získal	získat	k5eAaPmAgInS	získat
reputaci	reputace	k1gFnSc4	reputace
nebojácného	bojácný	k2eNgMnSc2d1	nebojácný
a	a	k8xC	a
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
jezdce	jezdec	k1gMnSc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
pít	pít	k5eAaImF	pít
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
během	během	k7c2	během
války	válka	k1gFnSc2	válka
začal	začít	k5eAaPmAgMnS	začít
i	i	k9	i
kouřit	kouřit	k5eAaImF	kouřit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
doutníků	doutník	k1gInPc2	doutník
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
zato	zato	k6eAd1	zato
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
5	[number]	k4	5
let	léto	k1gNnPc2	léto
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vykouřit	vykouřit	k5eAaPmF	vykouřit
přes	přes	k7c4	přes
10	[number]	k4	10
000	[number]	k4	000
doutníků	doutník	k1gInPc2	doutník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
rakovině	rakovina	k1gFnSc3	rakovina
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
postihla	postihnout	k5eAaPmAgFnS	postihnout
v	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
letech	let	k1gInPc6	let
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1848	[number]	k4	1848
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Julií	Julie	k1gFnSc7	Julie
Boggs	Boggsa	k1gFnPc2	Boggsa
Dentovou	Dentová	k1gFnSc7	Dentová
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Fredericka	Fredericka	k1gFnSc1	Fredericka
Dent	Dent	k1gMnSc1	Dent
Granta	Granta	k1gMnSc1	Granta
<g/>
,	,	kIx,	,
Ulysses	Ulysses	k1gMnSc1	Ulysses
S.	S.	kA	S.
(	(	kIx(	(
<g/>
Buck	Buck	k1gMnSc1	Buck
<g/>
)	)	kIx)	)
Granta	Granta	k1gMnSc1	Granta
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ellen	Ellen	k1gInSc1	Ellen
(	(	kIx(	(
<g/>
Nellie	Nellie	k1gFnSc2	Nellie
<g/>
)	)	kIx)	)
Grantovou	grantový	k2eAgFnSc7d1	Grantová
a	a	k8xC	a
Jesse	Jesse	k1gFnSc1	Jesse
Root	Roota	k1gFnPc2	Roota
Granta	Grant	k1gInSc2	Grant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mexická	mexický	k2eAgFnSc1d1	mexická
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
mexicko-americké	mexickomerický	k2eAgFnSc6d1	mexicko-americká
válce	válka	k1gFnSc6	válka
pod	pod	k7c7	pod
generály	generál	k1gMnPc7	generál
Zachary	Zachara	k1gFnSc2	Zachara
Taylorem	Taylor	k1gMnSc7	Taylor
a	a	k8xC	a
Winfieldem	Winfield	k1gMnSc7	Winfield
Scottem	Scott	k1gMnSc7	Scott
<g/>
,	,	kIx,	,
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
bitev	bitva	k1gFnPc2	bitva
u	u	k7c2	u
Resaca	Resac	k1gInSc2	Resac
de	de	k?	de
la	la	k1gNnSc1	la
Palma	palma	k1gFnSc1	palma
<g/>
,	,	kIx,	,
Palo	Pala	k1gMnSc5	Pala
Alta	Altum	k1gNnPc4	Altum
<g/>
,	,	kIx,	,
Monterrey	Monterrea	k1gFnPc4	Monterrea
a	a	k8xC	a
Veracruz	Veracruz	k1gInSc4	Veracruz
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
<g/>
:	:	kIx,	:
u	u	k7c2	u
Molina	molino	k1gNnSc2	molino
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
a	a	k8xC	a
Chapultepeca	Chapultepecum	k1gNnSc2	Chapultepecum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
skončila	skončit	k5eAaPmAgFnS	skončit
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
přeložen	přeložit	k5eAaPmNgInS	přeložit
na	na	k7c4	na
nová	nový	k2eAgNnPc4d1	nové
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
Fort	Fort	k?	Fort
Vencouveru	Vencouver	k1gInSc2	Vencouver
v	v	k7c6	v
teritoriu	teritorium	k1gNnSc6	teritorium
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
proviantní	proviantní	k2eAgMnSc1d1	proviantní
důstojník	důstojník	k1gMnSc1	důstojník
u	u	k7c2	u
4	[number]	k4	4
<g/>
.	.	kIx.	.
jízdního	jízdní	k2eAgInSc2d1	jízdní
regimentu	regiment	k1gInSc2	regiment
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
ho	on	k3xPp3gMnSc4	on
nemohla	moct	k5eNaImAgFnS	moct
doprovázet	doprovázet	k5eAaImF	doprovázet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc4	jeho
plat	plat	k1gInSc4	plat
by	by	kYmCp3nS	by
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
uživit	uživit	k5eAaPmF	uživit
rodinu	rodina	k1gFnSc4	rodina
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gInSc2	on
odjezdu	odjezd	k1gInSc2	odjezd
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
měsíci	měsíc	k1gInSc6	měsíc
těhotenství	těhotenství	k1gNnSc2	těhotenství
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
druhým	druhý	k4xOgNnSc7	druhý
dítětem	dítě	k1gNnSc7	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
a	a	k8xC	a
pověřen	pověřit	k5eAaPmNgInS	pověřit
velením	velení	k1gNnSc7	velení
setniny	setnina	k1gFnSc2	setnina
F	F	kA	F
4	[number]	k4	4
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
ve	v	k7c6	v
Fort	Fort	k?	Fort
Humboltu	Humbolt	k1gInSc6	Humbolt
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gInSc1	jeho
plat	plat	k1gInSc1	plat
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přijela	přijet	k5eAaPmAgFnS	přijet
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
obchodně	obchodně	k6eAd1	obchodně
podnikat	podnikat	k5eAaImF	podnikat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nějak	nějak	k6eAd1	nějak
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
své	svůj	k3xOyFgInPc4	svůj
příjmy	příjem	k1gInPc4	příjem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
pít	pít	k5eAaImF	pít
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
jeho	jeho	k3xOp3gInSc2	jeho
výkonu	výkon	k1gInSc2	výkon
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
před	před	k7c4	před
možnost	možnost	k1gFnSc4	možnost
buď	buď	k8xC	buď
čelit	čelit	k5eAaImF	čelit
soudu	soud	k1gInSc3	soud
nebo	nebo	k8xC	nebo
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1854	[number]	k4	1854
tedy	tedy	k9	tedy
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
a	a	k8xC	a
7	[number]	k4	7
let	léto	k1gNnPc2	léto
vedl	vést	k5eAaImAgInS	vést
civilní	civilní	k2eAgInSc1d1	civilní
život	život	k1gInSc1	život
jako	jako	k8xS	jako
farmář	farmář	k1gMnSc1	farmář
<g/>
,	,	kIx,	,
realitní	realitní	k2eAgMnSc1d1	realitní
agent	agent	k1gMnSc1	agent
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
jako	jako	k9	jako
pomocník	pomocník	k1gMnSc1	pomocník
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
koženým	kožený	k2eAgNnSc7d1	kožené
zbožím	zboží	k1gNnSc7	zboží
vlastněným	vlastněný	k2eAgNnSc7d1	vlastněné
jeho	jeho	k3xOp3gMnPc7	jeho
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
bratrem	bratr	k1gMnSc7	bratr
v	v	k7c6	v
Galeně	Galena	k1gFnSc6	Galena
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
zadlužil	zadlužit	k5eAaPmAgMnS	zadlužit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Začátek	začátek	k1gInSc1	začátek
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
10	[number]	k4	10
dní	den	k1gInPc2	den
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Fort	Fort	k?	Fort
Sumteru	Sumter	k1gInSc2	Sumter
<g/>
,	,	kIx,	,
dorazil	dorazit	k5eAaPmAgMnS	dorazit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
naverboval	naverbovat	k5eAaPmAgMnS	naverbovat
<g/>
,	,	kIx,	,
do	do	k7c2	do
Springfieldu	Springfield	k1gInSc2	Springfield
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
bývalý	bývalý	k2eAgMnSc1d1	bývalý
žák	žák	k1gMnSc1	žák
z	z	k7c2	z
West	Westa	k1gFnPc2	Westa
Pointu	pointa	k1gFnSc4	pointa
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
dostal	dostat	k5eAaPmAgMnS	dostat
důležitější	důležitý	k2eAgInPc4d2	Důležitější
úkoly	úkol	k1gInPc4	úkol
a	a	k8xC	a
povýšil	povýšit	k5eAaPmAgMnS	povýšit
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc2	plukovník
21	[number]	k4	21
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
(	(	kIx(	(
<g/>
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgMnS	být
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
brigádního	brigádní	k2eAgMnSc4d1	brigádní
generála	generál	k1gMnSc4	generál
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1862	[number]	k4	1862
získal	získat	k5eAaPmAgMnS	získat
Unii	unie	k1gFnSc4	unie
její	její	k3xOp3gFnSc2	její
první	první	k4xOgFnSc2	první
velké	velká	k1gFnSc2	velká
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
když	když	k8xS	když
dobyl	dobýt	k5eAaPmAgInS	dobýt
Fort	Fort	k?	Fort
Henry	henry	k1gInSc1	henry
a	a	k8xC	a
Fort	Fort	k?	Fort
Donelson	Donelson	k1gMnSc1	Donelson
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
<g/>
.	.	kIx.	.
</s>
<s>
Grant	grant	k1gInSc1	grant
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
nejen	nejen	k6eAd1	nejen
zajal	zajmout	k5eAaPmAgInS	zajmout
posádku	posádka	k1gFnSc4	posádka
Fort	Fort	k?	Fort
Donelson	Donelson	k1gMnSc1	Donelson
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
12	[number]	k4	12
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
požadoval	požadovat	k5eAaImAgMnS	požadovat
"	"	kIx"	"
<g/>
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
a	a	k8xC	a
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1	bezpodmínečná
kapitulaci	kapitulace	k1gFnSc4	kapitulace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gNnSc4	on
na	na	k7c6	na
Severu	sever	k1gInSc6	sever
rychle	rychle	k6eAd1	rychle
zpopularizovala	zpopularizovat	k5eAaPmAgFnS	zpopularizovat
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
nakrátko	nakrátko	k6eAd1	nakrátko
stal	stát	k5eAaPmAgMnS	stát
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
1862	[number]	k4	1862
byl	být	k5eAaImAgInS	být
překvapen	překvapit	k5eAaPmNgInS	překvapit
generálem	generál	k1gMnSc7	generál
Albertem	Albert	k1gMnSc7	Albert
Sidney	Sidne	k2eAgFnPc1d1	Sidne
Johnstonem	Johnston	k1gInSc7	Johnston
a	a	k8xC	a
přinucen	přinutit	k5eAaPmNgInS	přinutit
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Shilohu	Shiloh	k1gInSc2	Shiloh
<g/>
.	.	kIx.	.
</s>
<s>
Grantovi	Grant	k1gMnSc3	Grant
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
během	během	k7c2	během
jižanského	jižanský	k2eAgInSc2d1	jižanský
útoku	útok	k1gInSc2	útok
své	svůj	k3xOyFgFnSc2	svůj
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
převzít	převzít	k5eAaPmF	převzít
aktivitu	aktivita	k1gFnSc4	aktivita
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zvítězit	zvítězit	k5eAaPmF	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Shilohu	Shiloh	k1gInSc2	Shiloh
se	se	k3xPyFc4	se
s	s	k7c7	s
25	[number]	k4	25
000	[number]	k4	000
oběťmi	oběť	k1gFnPc7	oběť
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
stala	stát	k5eAaPmAgFnS	stát
nejkrvavější	krvavý	k2eAgFnSc7d3	nejkrvavější
bitvou	bitva	k1gFnSc7	bitva
celé	celý	k2eAgFnSc2d1	celá
války	válka	k1gFnSc2	válka
a	a	k8xC	a
celých	celý	k2eAgFnPc2d1	celá
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
amerických	americký	k2eAgFnPc2d1	americká
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
překonal	překonat	k5eAaPmAgInS	překonat
celkové	celkový	k2eAgNnSc4d1	celkové
množství	množství	k1gNnSc4	množství
mrtvých	mrtvý	k1gMnPc2	mrtvý
v	v	k7c6	v
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
válce	válka	k1gFnSc6	válka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
a	a	k8xC	a
mexické	mexický	k2eAgInPc4d1	mexický
válce	válec	k1gInPc4	válec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Henry	Henry	k1gMnSc1	Henry
W.	W.	kA	W.
Halleck	Halleck	k1gMnSc1	Halleck
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
nadřízený	nadřízený	k1gMnSc1	nadřízený
generál	generál	k1gMnSc1	generál
ho	on	k3xPp3gInSc4	on
zprostil	zprostit	k5eAaPmAgMnS	zprostit
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
velení	velení	k1gNnSc2	velení
a	a	k8xC	a
převzal	převzít	k5eAaPmAgMnS	převzít
velení	velení	k1gNnSc4	velení
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
generála	generál	k1gMnSc4	generál
Williama	William	k1gMnSc2	William
T.	T.	kA	T.
Shermana	Sherman	k1gMnSc2	Sherman
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
nerezignoval	rezignovat	k5eNaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
armádní	armádní	k2eAgFnSc2d1	armádní
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgMnS	být
Halleck	Halleck	k1gMnSc1	Halleck
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
všech	všecek	k3xTgFnPc2	všecek
unionistických	unionistický	k2eAgFnPc2d1	unionistická
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Vicksburgu	Vicksburg	k1gInSc2	Vicksburg
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
získat	získat	k5eAaPmF	získat
pevnost	pevnost	k1gFnSc4	pevnost
Vicksburg	Vicksburg	k1gInSc4	Vicksburg
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
zbývajících	zbývající	k2eAgInPc2d1	zbývající
opěrných	opěrný	k2eAgInPc2d1	opěrný
bodů	bod	k1gInPc2	bod
Konfederace	konfederace	k1gFnSc2	konfederace
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
dalším	další	k1gNnSc7	další
byl	být	k5eAaImAgInS	být
Port	port	k1gInSc1	port
Hudson	Hudson	k1gInSc1	Hudson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vicksburg	Vicksburg	k1gInSc4	Vicksburg
byla	být	k5eAaImAgFnS	být
pevnost	pevnost	k1gFnSc1	pevnost
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
říční	říční	k2eAgFnSc6d1	říční
ostrožně	ostrožna	k1gFnSc6	ostrožna
a	a	k8xC	a
obklopená	obklopený	k2eAgFnSc1d1	obklopená
mrtvými	mrtvý	k1gMnPc7	mrtvý
říčními	říční	k2eAgNnPc7d1	říční
rameny	rameno	k1gNnPc7	rameno
<g/>
;	;	kIx,	;
přes	přes	k7c4	přes
složitý	složitý	k2eAgInSc4d1	složitý
terén	terén	k1gInSc4	terén
bylo	být	k5eAaImAgNnS	být
takřka	takřka	k6eAd1	takřka
nemožné	možný	k2eNgNnSc1d1	nemožné
se	se	k3xPyFc4	se
k	k	k7c3	k
městu	město	k1gNnSc3	město
dostat	dostat	k5eAaPmF	dostat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zásobovat	zásobovat	k5eAaImF	zásobovat
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1863	[number]	k4	1863
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
město	město	k1gNnSc1	město
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdokonalejších	dokonalý	k2eAgFnPc2d3	nejdokonalejší
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
vojenské	vojenský	k2eAgFnSc6d1	vojenská
historii	historie	k1gFnSc6	historie
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedl	vést	k5eAaImAgInS	vést
své	svůj	k3xOyFgInPc4	svůj
oddíly	oddíl	k1gInPc4	oddíl
podél	podél	k7c2	podél
západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
a	a	k8xC	a
na	na	k7c6	na
vhodném	vhodný	k2eAgNnSc6d1	vhodné
místě	místo	k1gNnSc6	místo
Mississippi	Mississippi	k1gFnSc2	Mississippi
překročil	překročit	k5eAaPmAgInS	překročit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
amerického	americký	k2eAgNnSc2d1	americké
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přivezlo	přivézt	k5eAaPmAgNnS	přivézt
k	k	k7c3	k
Vicksburgu	Vicksburg	k1gInSc3	Vicksburg
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Toto	tento	k3xDgNnSc1	tento
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Normandii	Normandie	k1gFnSc6	Normandie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
největší	veliký	k2eAgFnSc1d3	veliký
obojživelná	obojživelný	k2eAgFnSc1d1	obojživelná
operace	operace	k1gFnSc1	operace
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
vojenské	vojenský	k2eAgFnSc6d1	vojenská
historii	historie	k1gFnSc6	historie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
muži	muž	k1gMnPc7	muž
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
odřízl	odříznout	k5eAaPmAgMnS	odříznout
od	od	k7c2	od
zásobovacích	zásobovací	k2eAgInPc2d1	zásobovací
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Přesunoval	přesunovat	k5eAaImAgInS	přesunovat
se	se	k3xPyFc4	se
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedal	dát	k5eNaPmAgMnS	dát
veliteli	velitel	k1gMnPc7	velitel
Jižanů	Jižan	k1gMnPc2	Jižan
Johnu	John	k1gMnSc6	John
C.	C.	kA	C.
Pembertonovi	Pembertonův	k2eAgMnPc1d1	Pembertonův
příležitost	příležitost	k1gFnSc4	příležitost
soustředit	soustředit	k5eAaPmF	soustředit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
zaútočit	zaútočit	k5eAaPmF	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
Grant	grant	k1gInSc1	grant
obsadil	obsadit	k5eAaPmAgInS	obsadit
město	město	k1gNnSc4	město
Jackson	Jacksona	k1gFnPc2	Jacksona
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
odříznout	odříznout	k5eAaPmF	odříznout
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
do	do	k7c2	do
Vicksburgu	Vicksburg	k1gInSc2	Vicksburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Champion	Champion	k1gInSc4	Champion
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
zde	zde	k6eAd1	zde
porazil	porazit	k5eAaPmAgMnS	porazit
Vicksburské	Vicksburský	k2eAgInPc4d1	Vicksburský
oddíly	oddíl	k1gInPc4	oddíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgInP	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
za	za	k7c4	za
opevnění	opevnění	k1gNnSc4	opevnění
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
obléhal	obléhat	k5eAaImAgInS	obléhat
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
se	se	k3xPyFc4	se
Pemberton	Pemberton	k1gInSc1	Pemberton
vzdal	vzdát	k5eAaPmAgInS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Konfederaci	konfederace	k1gFnSc4	konfederace
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
o	o	k7c4	o
den	den	k1gInSc4	den
dřív	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
byl	být	k5eAaImAgInS	být
územně	územně	k6eAd1	územně
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
a	a	k8xC	a
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
oslaben	oslabit	k5eAaPmNgInS	oslabit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Chattanoogy	Chattanooga	k1gFnSc2	Chattanooga
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1863	[number]	k4	1863
Konfederace	konfederace	k1gFnSc1	konfederace
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Chickamauga	Chickamaug	k1gMnSc2	Chickamaug
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
poražené	poražený	k2eAgNnSc1d1	poražené
unijní	unijní	k2eAgNnSc1d1	unijní
vojsko	vojsko	k1gNnSc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Williama	William	k1gMnSc2	William
Rosencranse	Rosencrans	k1gMnSc2	Rosencrans
stáhlo	stáhnout	k5eAaPmAgNnS	stáhnout
do	do	k7c2	do
města	město	k1gNnSc2	město
Chattanooga	Chattanoog	k1gMnSc2	Chattanoog
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
–	–	k?	–
končila	končit	k5eAaImAgFnS	končit
zde	zde	k6eAd1	zde
železnice	železnice	k1gFnSc1	železnice
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Tennessee	Tennessee	k1gFnSc6	Tennessee
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
brána	brána	k1gFnSc1	brána
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Georgie	Georgie	k1gFnSc2	Georgie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
promptně	promptně	k6eAd1	promptně
obleženo	oblehnout	k5eAaPmNgNnS	oblehnout
vítěznými	vítězný	k2eAgInPc7d1	vítězný
konfederačními	konfederační	k2eAgInPc7d1	konfederační
vojsky	vojsky	k6eAd1	vojsky
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Braxton	Braxton	k1gInSc1	Braxton
Bragga	Bragga	k1gFnSc1	Bragga
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgInS	být
Grant	grant	k1gInSc1	grant
pověřen	pověřit	k5eAaPmNgInS	pověřit
velením	velení	k1gNnSc7	velení
obleženého	obležený	k2eAgNnSc2d1	obležené
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Rosencranse	Rosencransa	k1gFnSc3	Rosencransa
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
Thomasem	Thomas	k1gMnSc7	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
Grantův	Grantův	k2eAgMnSc1d1	Grantův
hlavní	hlavní	k2eAgMnSc1d1	hlavní
stratég	stratég	k1gMnSc1	stratég
(	(	kIx(	(
<g/>
chief	chief	k1gMnSc1	chief
engineer	engineer	k1gMnSc1	engineer
<g/>
)	)	kIx)	)
William	William	k1gInSc1	William
F.	F.	kA	F.
"	"	kIx"	"
<g/>
Baldy	balda	k1gMnSc2	balda
<g/>
"	"	kIx"	"
Smith	Smith	k1gMnSc1	Smith
začal	začít	k5eAaPmAgMnS	začít
realizovat	realizovat	k5eAaBmF	realizovat
plán	plán	k1gInSc4	plán
známý	známý	k2eAgInSc4d1	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Cracker	Cracker	k1gInSc1	Cracker
Line	linout	k5eAaImIp3nS	linout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1863	[number]	k4	1863
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Wauhatchie	Wauhatchie	k1gFnSc2	Wauhatchie
jižní	jižní	k2eAgInPc1d1	jižní
oddíly	oddíl	k1gInPc1	oddíl
a	a	k8xC	a
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
řeku	řeka	k1gFnSc4	řeka
Tennessee	Tennesse	k1gInSc2	Tennesse
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
do	do	k7c2	do
Chattanoogy	Chattanooga	k1gFnSc2	Chattanooga
připlouvaly	připlouvat	k5eAaImAgFnP	připlouvat
zásoby	zásoba	k1gFnPc1	zásoba
a	a	k8xC	a
posily	posila	k1gFnPc1	posila
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
otevření	otevření	k1gNnSc1	otevření
obležení	obležení	k1gNnSc2	obležení
zvedlo	zvednout	k5eAaPmAgNnS	zvednout
morálku	morálka	k1gFnSc4	morálka
obléhaného	obléhaný	k2eAgNnSc2d1	obléhané
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
přešel	přejít	k5eAaPmAgMnS	přejít
Grant	grant	k1gInSc4	grant
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Chattanoogy	Chattanooga	k1gFnSc2	Chattanooga
nezačala	začít	k5eNaPmAgFnS	začít
pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
armádu	armáda	k1gFnSc4	armáda
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Sherman	Sherman	k1gMnSc1	Sherman
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
úspěšně	úspěšně	k6eAd1	úspěšně
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
konfederačních	konfederační	k2eAgNnPc2d1	konfederační
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
špatném	špatný	k2eAgNnSc6d1	špatné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
porazit	porazit	k5eAaPmF	porazit
jedinou	jediný	k2eAgFnSc7d1	jediná
jižní	jižní	k2eAgFnSc7d1	jižní
divizí	divize	k1gFnSc7	divize
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
špatný	špatný	k2eAgInSc4d1	špatný
start	start	k1gInSc4	start
předstíraným	předstíraný	k2eAgInSc7d1	předstíraný
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
střed	střed	k1gInSc4	střed
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Thomase	Thomas	k1gMnSc2	Thomas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
odvést	odvést	k5eAaPmF	odvést
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
Shermana	Sherman	k1gMnSc2	Sherman
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
si	se	k3xPyFc3	se
nebyl	být	k5eNaImAgMnS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hooker	Hooker	k1gInSc1	Hooker
posílený	posílený	k2eAgInSc1d1	posílený
potomackou	potomacký	k2eAgFnSc7d1	potomacký
armádou	armáda	k1gFnSc7	armáda
nenapadl	napadnout	k5eNaPmAgInS	napadnout
konfederátní	konfederátní	k2eAgFnPc4d1	konfederátní
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
levém	levý	k2eAgNnSc6d1	levé
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Hookerovi	Hooker	k1gMnSc3	Hooker
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
prolomit	prolomit	k5eAaPmF	prolomit
jižanské	jižanský	k2eAgNnSc4d1	jižanské
levé	levý	k2eAgNnSc4d1	levé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Thomasovi	Thomasův	k2eAgMnPc1d1	Thomasův
muži	muž	k1gMnPc1	muž
byli	být	k5eAaImAgMnP	být
stejně	stejně	k6eAd1	stejně
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
předstíraný	předstíraný	k2eAgInSc1d1	předstíraný
útok	útok	k1gInSc1	útok
změnil	změnit	k5eAaPmAgInS	změnit
ve	v	k7c4	v
skutečný	skutečný	k2eAgInSc4d1	skutečný
(	(	kIx(	(
<g/>
Thomasovi	Thomasův	k2eAgMnPc1d1	Thomasův
muži	muž	k1gMnPc1	muž
měli	mít	k5eAaImAgMnP	mít
zaujmout	zaujmout	k5eAaPmF	zaujmout
pozice	pozice	k1gFnPc4	pozice
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Misionářského	misionářský	k2eAgInSc2d1	misionářský
hřebenu	hřeben	k1gInSc2	hřeben
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vojáci	voják	k1gMnPc1	voják
rozpálení	rozpálený	k2eAgMnPc1d1	rozpálený
porážkou	porážka	k1gFnSc7	porážka
u	u	k7c2	u
Chickamaugy	Chickamauga	k1gFnSc2	Chickamauga
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
sápat	sápat	k5eAaImF	sápat
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
kopce	kopec	k1gInSc2	kopec
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc7	jejich
velkým	velký	k2eAgNnSc7d1	velké
štěstím	štěstí	k1gNnSc7	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
konfederačním	konfederační	k2eAgMnSc7d1	konfederační
vojákům	voják	k1gMnPc3	voják
nepodařilo	podařit	k5eNaPmAgNnS	podařit
sklonit	sklonit	k5eAaPmF	sklonit
děla	dělo	k1gNnPc4	dělo
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
mohli	moct	k5eAaImAgMnP	moct
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
;	;	kIx,	;
když	když	k8xS	když
unionističtí	unionistický	k2eAgMnPc1d1	unionistický
vojáci	voják	k1gMnPc1	voják
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
kopce	kopec	k1gInSc2	kopec
<g/>
,	,	kIx,	,
dávali	dávat	k5eAaImAgMnP	dávat
se	se	k3xPyFc4	se
Jižani	Jižan	k1gMnPc1	Jižan
okamžitě	okamžitě	k6eAd1	okamžitě
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
na	na	k7c4	na
Thomase	Thomas	k1gMnPc4	Thomas
hněval	hněvat	k5eAaImAgMnS	hněvat
<g/>
,	,	kIx,	,
že	že	k8xS	že
překročil	překročit	k5eAaPmAgMnS	překročit
daný	daný	k2eAgInSc4d1	daný
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
vítězství	vítězství	k1gNnSc1	vítězství
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
ústup	ústup	k1gInSc4	ústup
jižanských	jižanský	k2eAgNnPc2d1	jižanské
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
Unii	unie	k1gFnSc4	unie
možnost	možnost	k1gFnSc4	možnost
napadnout	napadnout	k5eAaPmF	napadnout
Atlantu	Atlanta	k1gFnSc4	Atlanta
v	v	k7c6	v
Georgii	Georgie	k1gFnSc6	Georgie
–	–	k?	–
srdce	srdce	k1gNnSc2	srdce
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vůle	vůle	k1gFnSc1	vůle
k	k	k7c3	k
boji	boj	k1gInSc3	boj
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
vítězit	vítězit	k5eAaImF	vítězit
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
Lincolna	Lincoln	k1gMnSc4	Lincoln
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
povýšen	povýšen	k2eAgMnSc1d1	povýšen
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1864	[number]	k4	1864
na	na	k7c4	na
"	"	kIx"	"
<g/>
lieutenant	lieutenant	k1gMnSc1	lieutenant
general	generat	k5eAaPmAgMnS	generat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
generálporučík	generálporučík	k1gMnSc1	generálporučík
–	–	k?	–
nová	nový	k2eAgFnSc1d1	nová
hodnost	hodnost	k1gFnSc1	hodnost
schválená	schválený	k2eAgFnSc1d1	schválená
kongresem	kongres	k1gInSc7	kongres
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
osobně	osobně	k6eAd1	osobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
10	[number]	k4	10
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
všech	všecek	k3xTgFnPc2	všecek
severských	severský	k2eAgFnPc2d1	severská
armád	armáda	k1gFnPc2	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
armád	armáda	k1gFnPc2	armáda
a	a	k8xC	a
vítězná	vítězný	k2eAgFnSc1d1	vítězná
strategie	strategie	k1gFnSc1	strategie
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
bojový	bojový	k2eAgInSc1d1	bojový
styl	styl	k1gInSc1	styl
nazval	nazvat	k5eAaPmAgInS	nazvat
jeden	jeden	k4xCgInSc4	jeden
spolugenerál	spolugenerál	k1gInSc4	spolugenerál
"	"	kIx"	"
<g/>
buldočím	buldočí	k2eAgInSc7d1	buldočí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
vynikajícím	vynikající	k2eAgMnPc3d1	vynikající
stratégům	stratég	k1gMnPc3	stratég
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
u	u	k7c2	u
Vicksburgu	Vicksburg	k1gInSc2	Vicksburg
a	a	k8xC	a
v	v	k7c6	v
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
generálu	generál	k1gMnSc3	generál
Robertu	Robert	k1gMnSc3	Robert
E.	E.	kA	E.
Leeovi	Leea	k1gMnSc3	Leea
<g/>
,	,	kIx,	,
nebál	bát	k5eNaImAgMnS	bát
se	se	k3xPyFc4	se
přímých	přímý	k2eAgInPc2d1	přímý
útoků	útok	k1gInPc2	útok
nebo	nebo	k8xC	nebo
těsného	těsný	k2eAgNnSc2d1	těsné
obléhání	obléhání	k1gNnSc2	obléhání
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
jižanské	jižanský	k2eAgFnPc4d1	jižanská
síly	síla	k1gFnPc4	síla
samy	sám	k3xTgFnPc1	sám
spouštěly	spouštět	k5eAaImAgFnP	spouštět
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
útok	útok	k1gInSc1	útok
nebo	nebo	k8xC	nebo
obležení	obležení	k1gNnSc1	obležení
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgMnS	odmítat
ustoupit	ustoupit	k5eAaPmF	ustoupit
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nechat	nechat	k5eAaPmF	nechat
vytlačit	vytlačit	k5eAaPmF	vytlačit
z	z	k7c2	z
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
taktika	taktika	k1gFnSc1	taktika
často	často	k6eAd1	často
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vysokým	vysoký	k2eAgFnPc3d1	vysoká
ztrátám	ztráta	k1gFnPc3	ztráta
jak	jak	k6eAd1	jak
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ovšem	ovšem	k9	ovšem
i	i	k9	i
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
protivníka	protivník	k1gMnSc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
strategii	strategie	k1gFnSc4	strategie
byl	být	k5eAaImAgInS	být
popisován	popisovat	k5eAaImNgInS	popisovat
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
řezník	řezník	k1gMnSc1	řezník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
ostatní	ostatní	k2eAgMnPc1d1	ostatní
generálové	generál	k1gMnPc1	generál
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
oddílech	oddíl	k1gInPc6	oddíl
podobně	podobně	k6eAd1	podobně
velké	velký	k2eAgFnPc1d1	velká
ztráty	ztráta	k1gFnPc1	ztráta
<g/>
,	,	kIx,	,
úspěch	úspěch	k1gInSc1	úspěch
ovšem	ovšem	k9	ovšem
mnohem	mnohem	k6eAd1	mnohem
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1864	[number]	k4	1864
pověřil	pověřit	k5eAaPmAgInS	pověřit
bezprostředním	bezprostřední	k2eAgNnSc7d1	bezprostřední
velením	velení	k1gNnSc7	velení
západních	západní	k2eAgFnPc2d1	západní
armád	armáda	k1gFnPc2	armáda
generálmajora	generálmajor	k1gMnSc2	generálmajor
William	William	k1gInSc1	William
T.	T.	kA	T.
Shermana	Sherman	k1gMnSc4	Sherman
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
velitelství	velitelství	k1gNnSc4	velitelství
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
už	už	k6eAd1	už
dlouho	dlouho	k6eAd1	dlouho
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
probíhající	probíhající	k2eAgFnSc4d1	probíhající
snahu	snaha	k1gFnSc4	snaha
zničit	zničit	k5eAaPmF	zničit
Leeovu	Leeův	k2eAgFnSc4d1	Leeova
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
dalším	další	k2eAgInSc7d1	další
bezprostředním	bezprostřední	k2eAgInSc7d1	bezprostřední
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
podmíněným	podmíněný	k2eAgMnSc7d1	podmíněný
ovšem	ovšem	k9	ovšem
splněním	splnění	k1gNnSc7	splnění
toho	ten	k3xDgNnSc2	ten
prvního	první	k4xOgNnSc2	první
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
obsazení	obsazení	k1gNnSc1	obsazení
Richmondu	Richmond	k1gInSc2	Richmond
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
použil	použít	k5eAaPmAgInS	použít
složitou	složitý	k2eAgFnSc4d1	složitá
koordinovanou	koordinovaný	k2eAgFnSc4d1	koordinovaná
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
centrum	centrum	k1gNnSc4	centrum
Konfederace	konfederace	k1gFnSc2	konfederace
z	z	k7c2	z
více	hodně	k6eAd2	hodně
směrů	směr	k1gInPc2	směr
<g/>
:	:	kIx,	:
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
George	George	k1gInSc1	George
G.	G.	kA	G.
Meade	Mead	k1gInSc5	Mead
a	a	k8xC	a
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k2eAgInSc4d1	Franklin
Butler	Butler	k1gInSc4	Butler
proti	proti	k7c3	proti
Leeovi	Leea	k1gMnSc3	Leea
blízko	blízko	k7c2	blízko
Richmondu	Richmond	k1gInSc2	Richmond
<g/>
;	;	kIx,	;
Franz	Franz	k1gInSc1	Franz
Sigel	sigla	k1gFnPc2	sigla
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Shenandoahu	Shenandoah	k1gInSc2	Shenandoah
<g/>
;	;	kIx,	;
Sherman	Sherman	k1gMnSc1	Sherman
měl	mít	k5eAaImAgMnS	mít
napadnout	napadnout	k5eAaPmF	napadnout
Georgii	Georgie	k1gFnSc4	Georgie
<g/>
,	,	kIx,	,
porazit	porazit	k5eAaPmF	porazit
Josepha	Joseph	k1gMnSc4	Joseph
E.	E.	kA	E.
Jonhstona	Jonhston	k1gMnSc4	Jonhston
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
Atlantu	Atlanta	k1gFnSc4	Atlanta
<g/>
;	;	kIx,	;
George	George	k1gFnSc1	George
Crook	Crook	k1gInSc1	Crook
a	a	k8xC	a
William	William	k1gInSc1	William
W.	W.	kA	W.
Averell	Averell	k1gInSc4	Averell
měli	mít	k5eAaImAgMnP	mít
přerušit	přerušit	k5eAaPmF	přerušit
železniční	železniční	k2eAgNnSc4d1	železniční
spojení	spojení	k1gNnSc4	spojení
ze	z	k7c2	z
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
;	;	kIx,	;
úkolem	úkol	k1gInSc7	úkol
Nathaniela	Nathaniel	k1gMnSc2	Nathaniel
Bankse	Banks	k1gMnSc2	Banks
bylo	být	k5eAaImAgNnS	být
získání	získání	k1gNnSc1	získání
Mobile	mobile	k1gNnSc2	mobile
v	v	k7c6	v
Alabamě	Alabama	k1gFnSc6	Alabama
<g/>
.	.	kIx.	.
</s>
<s>
Grant	grant	k1gInSc4	grant
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
generálem	generál	k1gMnSc7	generál
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
tak	tak	k6eAd1	tak
komplexní	komplexní	k2eAgFnSc4d1	komplexní
koordinovanou	koordinovaný	k2eAgFnSc4d1	koordinovaná
strategii	strategie	k1gFnSc4	strategie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
pochopil	pochopit	k5eAaPmAgMnS	pochopit
důležitost	důležitost	k1gFnSc4	důležitost
protivníkovy	protivníkův	k2eAgFnSc2d1	protivníkova
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
–	–	k?	–
použil	použít	k5eAaPmAgInS	použít
tak	tak	k9	tak
koncept	koncept	k1gInSc1	koncept
totální	totální	k2eAgFnSc2d1	totální
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Overland	Overlando	k1gNnPc2	Overlando
Campaign	Campaigna	k1gFnPc2	Campaigna
===	===	k?	===
</s>
</p>
<p>
<s>
Overland	Overland	k1gInSc4	Overland
Campaign	Campaigna	k1gFnPc2	Campaigna
začala	začít	k5eAaPmAgFnS	začít
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
když	když	k8xS	když
potomacká	potomacký	k2eAgFnSc1d1	potomacký
armáda	armáda	k1gFnSc1	armáda
o	o	k7c4	o
115	[number]	k4	115
000	[number]	k4	000
mužích	muž	k1gMnPc6	muž
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
asi	asi	k9	asi
65	[number]	k4	65
000	[number]	k4	000
mužům	muž	k1gMnPc3	muž
Leeovým	Leeův	k2eAgMnPc3d1	Leeův
<g/>
)	)	kIx)	)
překročila	překročit	k5eAaPmAgFnS	překročit
řeku	řeka	k1gFnSc4	řeka
Rapidan	Rapidana	k1gFnPc2	Rapidana
(	(	kIx(	(
<g/>
Rappahannock	Rappahannock	k1gInSc1	Rappahannock
<g/>
)	)	kIx)	)
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dosud	dosud	k6eAd1	dosud
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
Divočina	divočina	k1gFnSc1	divočina
(	(	kIx(	(
<g/>
Wilderness	Wilderness	k1gInSc1	Wilderness
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Lee	Lea	k1gFnSc3	Lea
poslal	poslat	k5eAaPmAgInS	poslat
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
Armádu	armáda	k1gFnSc4	armáda
Severní	severní	k2eAgFnSc2d1	severní
Virgínie	Virgínie	k1gFnSc2	Virgínie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
oslabit	oslabit	k5eAaPmF	oslabit
početní	početní	k2eAgFnSc4d1	početní
převahu	převaha	k1gFnSc4	převaha
seveřanů	seveřan	k1gMnPc2	seveřan
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Divočině	divočina	k1gFnSc6	divočina
(	(	kIx(	(
<g/>
Battle	Battle	k1gFnSc1	Battle
of	of	k?	of
Wilderness	Wilderness	k1gInSc1	Wilderness
<g/>
)	)	kIx)	)
probíhala	probíhat	k5eAaImAgFnS	probíhat
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přestávce	přestávka	k1gFnSc3	přestávka
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
jednomu	jeden	k4xCgNnSc3	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgNnPc2d1	klíčové
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
stáhnout	stáhnout	k5eAaPmF	stáhnout
z	z	k7c2	z
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgMnS	umožnit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dosud	dosud	k6eAd1	dosud
dělali	dělat	k5eAaImAgMnP	dělat
všichni	všechen	k3xTgMnPc1	všechen
velící	velící	k2eAgMnPc1d1	velící
generálové	generál	k1gMnPc1	generál
severních	severní	k2eAgFnPc2d1	severní
armád	armáda	k1gFnPc2	armáda
–	–	k?	–
ústup	ústup	k1gInSc1	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
rozkazu	rozkaz	k1gInSc2	rozkaz
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
nařídil	nařídit	k5eAaPmAgInS	nařídit
obejít	obejít	k5eAaPmF	obejít
Leeho	Lee	k1gMnSc4	Lee
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
směřovat	směřovat	k5eAaImF	směřovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Siegelova	Siegelův	k2eAgFnSc1d1	Siegelova
kampaň	kampaň	k1gFnSc1	kampaň
v	v	k7c6	v
Shenandoahu	Shenandoah	k1gInSc6	Shenandoah
i	i	k8xC	i
Butlerova	Butlerův	k2eAgFnSc1d1	Butlerova
kampaň	kampaň	k1gFnSc1	kampaň
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
James	James	k1gInSc4	James
neuspěly	uspět	k5eNaPmAgFnP	uspět
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
Lee	Lea	k1gFnSc3	Lea
schopný	schopný	k2eAgMnSc1d1	schopný
doplnit	doplnit	k5eAaPmF	doplnit
své	své	k1gNnSc4	své
jednotky	jednotka	k1gFnSc2	jednotka
vojáky	voják	k1gMnPc7	voják
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1864	[number]	k4	1864
se	se	k3xPyFc4	se
Lee	Lea	k1gFnSc3	Lea
s	s	k7c7	s
Grantem	grant	k1gInSc7	grant
utkali	utkat	k5eAaPmAgMnP	utkat
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Spotsylvanského	Spotsylvanský	k2eAgInSc2d1	Spotsylvanský
dvora	dvůr	k1gInSc2	dvůr
(	(	kIx(	(
<g/>
Battle	Battle	k1gFnSc1	Battle
of	of	k?	of
Spotsylvania	Spotsylvanium	k1gNnPc1	Spotsylvanium
Court	Court	k1gInSc1	Court
House	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
napsal	napsat	k5eAaPmAgMnS	napsat
proslavenou	proslavený	k2eAgFnSc4d1	proslavená
zprávu	zpráva	k1gFnSc4	zpráva
obsahující	obsahující	k2eAgFnSc4d1	obsahující
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Navrhuji	navrhovat	k5eAaImIp1nS	navrhovat
bojovat	bojovat	k5eAaImF	bojovat
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
trvat	trvat	k5eAaImF	trvat
celé	celý	k2eAgNnSc4d1	celé
léto	léto	k1gNnSc4	léto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
hromadný	hromadný	k2eAgInSc4d1	hromadný
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
téměř	téměř	k6eAd1	téměř
prolomil	prolomit	k5eAaPmAgInS	prolomit
Leeovu	Leeův	k2eAgFnSc4d1	Leeova
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
zas	zas	k6eAd1	zas
stočil	stočit	k5eAaPmAgInS	stočit
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rostoucí	rostoucí	k2eAgFnPc4d1	rostoucí
ztráty	ztráta	k1gFnPc4	ztráta
unionistických	unionistický	k2eAgFnPc2d1	unionistická
armád	armáda	k1gFnPc2	armáda
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
měnila	měnit	k5eAaImAgFnS	měnit
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
prospěchu	prospěch	k1gInSc3	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Leeových	Leeův	k2eAgInPc2d1	Leeův
úspěchů	úspěch	k1gInPc2	úspěch
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
ofenzívě	ofenzíva	k1gFnSc6	ofenzíva
obsahující	obsahující	k2eAgInPc4d1	obsahující
překvapivé	překvapivý	k2eAgInPc4d1	překvapivý
přesuny	přesun	k1gInPc4	přesun
a	a	k8xC	a
náhlé	náhlý	k2eAgInPc4d1	náhlý
útoky	útok	k1gInPc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
byl	být	k5eAaImAgMnS	být
přinucen	přinutit	k5eAaPmNgMnS	přinutit
bojovat	bojovat	k5eAaImF	bojovat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
v	v	k7c6	v
defenzívě	defenzíva	k1gFnSc6	defenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
po	po	k7c6	po
strašné	strašný	k2eAgFnSc6d1	strašná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Cold	Colda	k1gFnPc2	Colda
Harbor	Harbora	k1gFnPc2	Harbora
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
dokázal	dokázat	k5eAaPmAgMnS	dokázat
stupňovat	stupňovat	k5eAaImF	stupňovat
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
překročit	překročit	k5eAaPmF	překročit
řeku	řeka	k1gFnSc4	řeka
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
Petersburgu	Petersburg	k1gInSc3	Petersburg
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
dorazil	dorazit	k5eAaPmAgMnS	dorazit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zajistit	zajistit	k5eAaPmF	zajistit
tuto	tento	k3xDgFnSc4	tento
významnou	významný	k2eAgFnSc4d1	významná
železniční	železniční	k2eAgFnSc4d1	železniční
křižovatku	křižovatka	k1gFnSc4	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
vinou	vina	k1gFnSc7	vina
přehnaně	přehnaně	k6eAd1	přehnaně
opatrného	opatrný	k2eAgNnSc2d1	opatrné
podřízeného	podřízený	k2eAgNnSc2d1	podřízené
Williama	Williamum	k1gNnSc2	Williamum
F.	F.	kA	F.
"	"	kIx"	"
<g/>
Baldy	balda	k1gMnSc2	balda
<g/>
"	"	kIx"	"
Smithe	Smith	k1gMnSc2	Smith
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
strávilo	strávit	k5eAaPmAgNnS	strávit
vojsko	vojsko	k1gNnSc1	vojsko
marnými	marný	k2eAgInPc7d1	marný
útoky	útok	k1gInPc7	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
dorazily	dorazit	k5eAaPmAgInP	dorazit
i	i	k9	i
Leeovy	Leeův	k2eAgInPc1d1	Leeův
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
posílily	posílit	k5eAaPmAgFnP	posílit
obránce	obránce	k1gMnPc4	obránce
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nezbylo	zbýt	k5eNaPmAgNnS	zbýt
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
než	než	k8xS	než
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
obléháním	obléhání	k1gNnSc7	obléhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
se	se	k3xPyFc4	se
k	k	k7c3	k
plánu	plán	k1gInSc3	plán
navrženému	navržený	k2eAgInSc3d1	navržený
Ambrosem	Ambros	k1gMnSc7	Ambros
Burnsidem	Burnsid	k1gMnSc7	Burnsid
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozbřeskem	rozbřesk	k1gInSc7	rozbřesk
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
nechal	nechat	k5eAaPmAgMnS	nechat
vyhodit	vyhodit	k5eAaPmF	vyhodit
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
doly	dol	k1gInPc4	dol
pod	pod	k7c7	pod
konfederačními	konfederační	k2eAgFnPc7d1	konfederační
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
prováděným	prováděný	k2eAgFnPc3d1	prováděná
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
Meade	Mead	k1gInSc5	Mead
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
ubezpečovali	ubezpečovat	k5eAaImAgMnP	ubezpečovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jednotky	jednotka	k1gFnSc2	jednotka
složené	složený	k2eAgFnSc6d1	složená
z	z	k7c2	z
černých	černý	k2eAgMnPc2d1	černý
Američanů	Američan	k1gMnPc2	Američan
vedly	vést	k5eAaImAgFnP	vést
útok	útok	k1gInSc1	útok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
následující	následující	k2eAgInSc1d1	následující
útok	útok	k1gInSc1	útok
chabý	chabý	k2eAgInSc1d1	chabý
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
koordinovaný	koordinovaný	k2eAgInSc1d1	koordinovaný
<g/>
.	.	kIx.	.
</s>
<s>
Konfederační	konfederační	k2eAgFnPc1d1	konfederační
jednotky	jednotka	k1gFnPc1	jednotka
využily	využít	k5eAaPmAgFnP	využít
možnosti	možnost	k1gFnPc4	možnost
se	se	k3xPyFc4	se
přeskupit	přeskupit	k5eAaPmF	přeskupit
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
protiútok	protiútok	k1gInSc4	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
kráter	kráter	k1gInSc4	kráter
(	(	kIx(	(
<g/>
Battle	Battle	k1gFnSc1	Battle
of	of	k?	of
the	the	k?	the
Crater	Crater	k1gInSc1	Crater
<g/>
)	)	kIx)	)
a	a	k8xC	a
unionisté	unionista	k1gMnPc1	unionista
tak	tak	k6eAd1	tak
ztratili	ztratit	k5eAaPmAgMnP	ztratit
další	další	k2eAgFnSc4d1	další
příležitost	příležitost	k1gFnSc4	příležitost
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
postupovalo	postupovat	k5eAaImAgNnS	postupovat
léto	léto	k1gNnSc1	léto
a	a	k8xC	a
Shermanovy	Shermanův	k2eAgFnPc1d1	Shermanova
armády	armáda	k1gFnPc1	armáda
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
postoupit	postoupit	k5eAaPmF	postoupit
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
a	a	k8xC	a
Georgii	Georgie	k1gFnSc6	Georgie
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
nastávající	nastávající	k1gFnSc4	nastávající
události	událost	k1gFnSc3	událost
ovládat	ovládat	k5eAaImF	ovládat
politika	politik	k1gMnSc4	politik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
totiž	totiž	k9	totiž
připadaly	připadat	k5eAaImAgFnP	připadat
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Unii	unie	k1gFnSc4	unie
se	se	k3xPyFc4	se
věci	věc	k1gFnPc1	věc
ještě	ještě	k9	ještě
zhoršily	zhoršit	k5eAaPmAgFnP	zhoršit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Leeovi	Leea	k1gMnSc3	Leea
podařilo	podařit	k5eAaPmAgNnS	podařit
oddělit	oddělit	k5eAaPmF	oddělit
malou	malý	k2eAgFnSc4d1	malá
armádu	armáda	k1gFnSc4	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generálmajora	generálmajor	k1gMnSc2	generálmajor
Jubal	Jubal	k1gInSc1	Jubal
A.	A.	kA	A.
Earlyho	Early	k1gMnSc2	Early
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
odlákat	odlákat	k5eAaPmF	odlákat
Granta	Granta	k1gMnSc1	Granta
<g/>
.	.	kIx.	.
</s>
<s>
Early	earl	k1gMnPc4	earl
skrz	skrz	k6eAd1	skrz
Shenandoahské	Shenandoahské	k2eAgNnSc2d1	Shenandoahské
údolí	údolí	k1gNnSc2	údolí
prošel	projít	k5eAaPmAgInS	projít
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
blízkosti	blízkost	k1gFnSc2	blízkost
Washingtonu	Washington	k1gInSc2	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
sice	sice	k8xC	sice
schopný	schopný	k2eAgMnSc1d1	schopný
obsadit	obsadit	k5eAaPmF	obsadit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotná	samotný	k2eAgFnSc1d1	samotná
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
zřejmě	zřejmě	k6eAd1	zřejmě
znamenala	znamenat	k5eAaImAgFnS	znamenat
další	další	k2eAgNnSc4d1	další
snížení	snížení	k1gNnSc4	snížení
šancí	šance	k1gFnPc2	šance
na	na	k7c4	na
Lincolnovo	Lincolnův	k2eAgNnSc4d1	Lincolnovo
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
září	září	k1gNnSc2	září
přineslo	přinést	k5eAaPmAgNnS	přinést
jeho	jeho	k3xOp3gFnSc4	jeho
úsilí	úsilí	k1gNnSc1	úsilí
první	první	k4xOgNnSc4	první
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
když	když	k8xS	když
Sherman	Sherman	k1gMnSc1	Sherman
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Atlantu	Atlanta	k1gFnSc4	Atlanta
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
poslal	poslat	k5eAaPmAgMnS	poslat
Philipa	Philip	k1gMnSc4	Philip
Sheridana	Sheridan	k1gMnSc4	Sheridan
do	do	k7c2	do
Shenandoahského	Shenandoahský	k2eAgNnSc2d1	Shenandoahský
údolí	údolí	k1gNnSc2	údolí
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	se	k3xPyFc4	se
s	s	k7c7	s
Earlym	Earlym	k1gInSc1	Earlym
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
severu	sever	k1gInSc6	sever
si	se	k3xPyFc3	se
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
jasněji	jasně	k6eAd2	jasně
začali	začít	k5eAaPmAgMnP	začít
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vítězství	vítězství	k1gNnSc3	vítězství
a	a	k8xC	a
Lincoln	Lincoln	k1gMnSc1	Lincoln
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
Sherman	Sherman	k1gMnSc1	Sherman
začal	začít	k5eAaPmAgMnS	začít
svůj	svůj	k3xOyFgInSc4	svůj
pochod	pochod	k1gInSc4	pochod
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
i	i	k8xC	i
Sheridan	Sheridana	k1gFnPc2	Sheridana
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
strategii	strategie	k1gFnSc6	strategie
totální	totální	k2eAgFnSc2d1	totální
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
1865	[number]	k4	1865
konečně	konečně	k6eAd1	konečně
jeho	jeho	k3xOp3gInSc1	jeho
neustávající	ustávající	k2eNgInSc1d1	neustávající
tlak	tlak	k1gInSc1	tlak
donutil	donutit	k5eAaPmAgInS	donutit
Leeho	Lee	k1gMnSc4	Lee
evakuovat	evakuovat	k5eAaBmF	evakuovat
Richmond	Richmond	k1gInSc4	Richmond
a	a	k8xC	a
po	po	k7c6	po
devítidenním	devítidenní	k2eAgInSc6d1	devítidenní
ústupu	ústup	k1gInSc6	ústup
se	se	k3xPyFc4	se
Lee	Lea	k1gFnSc6	Lea
vzdal	vzdát	k5eAaPmAgMnS	vzdát
u	u	k7c2	u
Appomattoxu	Appomattox	k1gInSc2	Appomattox
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Nabídl	nabídnout	k5eAaPmAgInS	nabídnout
mu	on	k3xPp3gNnSc3	on
velkorysé	velkorysý	k2eAgFnPc1d1	velkorysá
podmínky	podmínka	k1gFnPc1	podmínka
kapitulace	kapitulace	k1gFnSc2	kapitulace
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
uvolnily	uvolnit	k5eAaPmAgInP	uvolnit
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
armádami	armáda	k1gFnPc7	armáda
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgInP	umožnit
jižanským	jižanský	k2eAgMnPc3d1	jižanský
vojákům	voják	k1gMnPc3	voják
uchovat	uchovat	k5eAaPmF	uchovat
si	se	k3xPyFc3	se
alespoň	alespoň	k9	alespoň
zdání	zdání	k1gNnSc1	zdání
hrdosti	hrdost	k1gFnSc2	hrdost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
menší	malý	k2eAgFnPc1d2	menší
akce	akce	k1gFnPc1	akce
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Kirby	Kirb	k1gInPc7	Kirb
Smith	Smitha	k1gFnPc2	Smitha
vzdal	vzdát	k5eAaPmAgMnS	vzdát
u	u	k7c2	u
Trans-Mississippi	Trans-Mississipp	k1gFnSc2	Trans-Mississipp
Departmentu	department	k1gInSc2	department
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
Leeově	Leeův	k2eAgFnSc6d1	Leeova
kapitulaci	kapitulace	k1gFnSc6	kapitulace
byl	být	k5eAaImAgInS	být
poctěn	poctěn	k2eAgInSc1d1	poctěn
kráčet	kráčet	k5eAaImF	kráčet
vedle	vedle	k7c2	vedle
rakve	rakev	k1gFnSc2	rakev
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Lincoln	Lincoln	k1gMnSc1	Lincoln
byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
svojí	svůj	k3xOyFgFnSc7	svůj
podporou	podpora	k1gFnSc7	podpora
jeho	jeho	k3xOp3gFnSc3	jeho
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Shilohu	Shiloh	k1gInSc2	Shiloh
řekl	říct	k5eAaPmAgInS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemůžu	Nemůžu	k?	Nemůžu
ho	on	k3xPp3gMnSc4	on
propustit	propustit	k5eAaPmF	propustit
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
totiž	totiž	k9	totiž
bojuje	bojovat	k5eAaImIp3nS	bojovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
mu	on	k3xPp3gMnSc3	on
Kongres	kongres	k1gInSc1	kongres
udělil	udělit	k5eAaPmAgInS	udělit
hodnost	hodnost	k1gFnSc4	hodnost
"	"	kIx"	"
<g/>
General	General	k1gFnSc1	General
of	of	k?	of
the	the	k?	the
Army	Arma	k1gFnSc2	Arma
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dnešnímu	dnešní	k2eAgMnSc3d1	dnešní
čtyřhvězdičkovému	čtyřhvězdičkový	k2eAgMnSc3d1	čtyřhvězdičkový
generálovi	generál	k1gMnSc3	generál
–	–	k?	–
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
ho	on	k3xPp3gNnSc4	on
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
Andrew	Andrew	k1gMnSc1	Andrew
Johnson	Johnson	k1gMnSc1	Johnson
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentství	prezidentství	k1gNnSc2	prezidentství
==	==	k?	==
</s>
</p>
<p>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
18	[number]	k4	18
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
volebních	volební	k2eAgNnPc2d1	volební
období	období	k1gNnPc2	období
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1869	[number]	k4	1869
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k8xC	jako
republikánský	republikánský	k2eAgMnSc1d1	republikánský
prezidentský	prezidentský	k2eAgMnSc1d1	prezidentský
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c6	na
republikánském	republikánský	k2eAgNnSc6d1	republikánské
národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1868	[number]	k4	1868
bez	bez	k7c2	bez
skutečné	skutečný	k2eAgFnSc2d1	skutečná
konkurence	konkurence	k1gFnSc2	konkurence
–	–	k?	–
odpůrci	odpůrce	k1gMnPc1	odpůrce
otroctví	otroctví	k1gNnSc2	otroctví
a	a	k8xC	a
republikáni	republikán	k1gMnPc1	republikán
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
vzhlíželi	vzhlížet	k5eAaImAgMnP	vzhlížet
jako	jako	k9	jako
k	k	k7c3	k
vítězi	vítěz	k1gMnSc3	vítěz
od	od	k7c2	od
Vicksburgu	Vicksburg	k1gInSc2	Vicksburg
a	a	k8xC	a
válečnému	válečný	k2eAgMnSc3d1	válečný
hrdinovi	hrdina	k1gMnSc3	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
voleb	volba	k1gFnPc2	volba
získal	získat	k5eAaPmAgInS	získat
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
demokratickému	demokratický	k2eAgMnSc3d1	demokratický
protihráči	protihráč	k1gMnSc3	protihráč
Horatio	Horatio	k1gMnSc1	Horatio
Seymourovi	Seymour	k1gMnSc3	Seymour
většinu	většina	k1gFnSc4	většina
3	[number]	k4	3
012	[number]	k4	012
833	[number]	k4	833
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
5	[number]	k4	5
716	[number]	k4	716
082	[number]	k4	082
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
přinesl	přinést	k5eAaPmAgInS	přinést
jen	jen	k9	jen
minimální	minimální	k2eAgFnPc4d1	minimální
politické	politický	k2eAgFnPc4d1	politická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
dle	dle	k7c2	dle
mnohých	mnohý	k2eAgMnPc2d1	mnohý
kritiků	kritik	k1gMnPc2	kritik
i	i	k9	i
malý	malý	k2eAgInSc1d1	malý
talent	talent	k1gInSc1	talent
politické	politický	k2eAgFnSc2d1	politická
záležitosti	záležitost	k1gFnSc2	záležitost
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skandály	skandál	k1gInPc1	skandál
jeho	on	k3xPp3gNnSc2	on
prezidenství	prezidenství	k1gNnSc2	prezidenství
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
zaplaveno	zaplavit	k5eAaPmNgNnS	zaplavit
skandály	skandál	k1gInPc4	skandál
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
Sanbornský	Sanbornský	k2eAgInSc4d1	Sanbornský
incident	incident	k1gInSc4	incident
v	v	k7c4	v
Treasury	Treasur	k1gInPc4	Treasur
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
zástupcem	zástupce	k1gMnSc7	zástupce
Cyrus	Cyrus	k1gMnSc1	Cyrus
I.	I.	kA	I.
Scofieldem	Scofieldo	k1gNnSc7	Scofieldo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
skandál	skandál	k1gInSc1	skandál
zvaný	zvaný	k2eAgInSc1d1	zvaný
Whiskey	Whiske	k1gMnPc7	Whiske
Ring	ring	k1gInSc4	ring
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vláda	vláda	k1gFnSc1	vláda
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
milióny	milión	k4xCgInPc1	milión
dolarů	dolar	k1gInPc2	dolar
vybraných	vybraný	k2eAgFnPc2d1	vybraná
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Orville	Orville	k1gInSc1	Orville
E.	E.	kA	E.
Babcock	Babcock	k1gInSc1	Babcock
<g/>
,	,	kIx,	,
sekretář	sekretář	k1gMnSc1	sekretář
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
této	tento	k3xDgFnSc2	tento
kliky	klika	k1gFnSc2	klika
a	a	k8xC	a
unikl	uniknout	k5eAaPmAgMnS	uniknout
usvědčení	usvědčení	k1gNnSc4	usvědčení
pouze	pouze	k6eAd1	pouze
díky	díky	k7c3	díky
prezidentovu	prezidentův	k2eAgInSc3d1	prezidentův
pardonu	pardon	k1gInSc3	pardon
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
William	William	k1gInSc1	William
W.	W.	kA	W.
Belknap	Belknap	k1gInSc1	Belknap
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odhalilo	odhalit	k5eAaPmAgNnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bral	brát	k5eAaImAgMnS	brát
úplatky	úplatek	k1gInPc7	úplatek
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
Native	Natiev	k1gFnSc2	Natiev
American	Americana	k1gFnPc2	Americana
trading	trading	k1gInSc1	trading
posts	posts	k6eAd1	posts
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
neexistují	existovat	k5eNaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
korupci	korupce	k1gFnSc4	korupce
svých	svůj	k3xOyFgMnPc2	svůj
podřízených	podřízený	k1gMnPc2	podřízený
<g/>
,	,	kIx,	,
nezaujal	zaujmout	k5eNaPmAgMnS	zaujmout
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
činnosti	činnost	k1gFnSc3	činnost
jasné	jasný	k2eAgNnSc1d1	jasné
a	a	k8xC	a
pevné	pevný	k2eAgNnSc1d1	pevné
stanovisko	stanovisko	k1gNnSc1	stanovisko
ani	ani	k8xC	ani
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
usvědčeni	usvědčit	k5eAaPmNgMnP	usvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
ruku	ruka	k1gFnSc4	ruka
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
svých	svůj	k3xOyFgMnPc2	svůj
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
si	se	k3xPyFc3	se
vůdce	vůdce	k1gMnSc1	vůdce
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
když	když	k8xS	když
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
úřadů	úřad	k1gInPc2	úřad
dal	dát	k5eAaPmAgMnS	dát
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
politickým	politický	k2eAgMnPc3d1	politický
podpůrcům	podpůrce	k1gMnPc3	podpůrce
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterými	který	k3yIgMnPc7	který
stál	stát	k5eAaImAgMnS	stát
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
neosvědčili	osvědčit	k5eNaPmAgMnP	osvědčit
či	či	k8xC	či
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
byla	být	k5eAaImAgFnS	být
vznesena	vznesen	k2eAgNnPc4d1	vzneseno
závažná	závažný	k2eAgNnPc4d1	závažné
obvinění	obvinění	k1gNnPc4	obvinění
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
výjimek	výjimka	k1gFnPc2	výjimka
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
vybraný	vybraný	k2eAgInSc1d1	vybraný
Hamilton	Hamilton	k1gInSc1	Hamilton
Fish	Fisha	k1gFnPc2	Fisha
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
neschopnost	neschopnost	k1gFnSc4	neschopnost
zajistit	zajistit	k5eAaPmF	zajistit
si	se	k3xPyFc3	se
politické	politický	k2eAgFnSc6d1	politická
spojence	spojenka	k1gFnSc6	spojenka
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
skandály	skandál	k1gInPc1	skandál
dostaly	dostat	k5eAaPmAgInP	dostat
mimo	mimo	k7c4	mimo
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průběh	průběh	k1gInSc1	průběh
prezidentství	prezidentství	k1gNnSc1	prezidentství
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
skandály	skandál	k1gInPc4	skandál
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgNnSc1d1	americké
přenést	přenést	k5eAaPmF	přenést
přes	přes	k7c4	přes
citlivé	citlivý	k2eAgNnSc4d1	citlivé
období	období	k1gNnSc4	období
tzv.	tzv.	kA	tzv.
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
jen	jen	k9	jen
omezenou	omezený	k2eAgFnSc4d1	omezená
přítomnost	přítomnost	k1gFnSc4	přítomnost
severních	severní	k2eAgFnPc2d1	severní
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c6	na
území	území	k1gNnSc6	území
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
–	–	k?	–
ty	ten	k3xDgFnPc1	ten
měly	mít	k5eAaImAgFnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
dodržování	dodržování	k1gNnSc4	dodržování
práv	právo	k1gNnPc2	právo
jižanských	jižanský	k2eAgMnPc2d1	jižanský
černochů	černoch	k1gMnPc2	černoch
a	a	k8xC	a
zamezit	zamezit	k5eAaPmF	zamezit
násilným	násilný	k2eAgInPc3d1	násilný
činům	čin	k1gInPc3	čin
Ku	k	k7c3	k
Klux	Klux	k1gInSc1	Klux
Klanu	klan	k1gInSc6	klan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1869	[number]	k4	1869
a	a	k8xC	a
1871	[number]	k4	1871
podepsal	podepsat	k5eAaPmAgInS	podepsat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zaručovaly	zaručovat	k5eAaImAgFnP	zaručovat
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
černochům	černoch	k1gMnPc3	černoch
a	a	k8xC	a
potíraly	potírat	k5eAaImAgFnP	potírat
vůdce	vůdce	k1gMnPc4	vůdce
Ku	k	k7c3	k
Klux	Klux	k1gInSc1	Klux
Klanu	klan	k1gInSc6	klan
<g/>
.	.	kIx.	.
</s>
<s>
Patnáctý	patnáctý	k4xOgInSc1	patnáctý
dodatek	dodatek	k1gInSc1	dodatek
americké	americký	k2eAgFnSc2d1	americká
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zaručoval	zaručovat	k5eAaImAgMnS	zaručovat
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
černochům	černoch	k1gMnPc3	černoch
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
přijato	přijmout	k5eAaPmNgNnS	přijmout
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
byla	být	k5eAaImAgFnS	být
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
jeho	jeho	k3xOp3gFnSc2	jeho
administrativy	administrativa	k1gFnSc2	administrativa
Washingtonská	washingtonský	k2eAgFnSc1d1	Washingtonská
smlouva	smlouva	k1gFnSc1	smlouva
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgNnP	být
vyjednána	vyjednat	k5eAaPmNgNnP	vyjednat
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
mála	málo	k4c2	málo
dobrých	dobrý	k2eAgFnPc2d1	dobrá
voleb	volba	k1gFnPc2	volba
při	při	k7c6	při
obsazování	obsazování	k1gNnSc6	obsazování
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Hamiltonem	Hamilton	k1gInSc7	Hamilton
Fishem	Fish	k1gInSc7	Fish
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kongres	kongres	k1gInSc1	kongres
neschválil	schválit	k5eNaPmAgInS	schválit
koupi	koupě	k1gFnSc4	koupě
Santo	Santo	k1gNnSc1	Santo
Dominga	Doming	k1gMnSc2	Doming
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
o	o	k7c4	o
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
funkci	funkce	k1gFnSc4	funkce
usilovat	usilovat	k5eAaImF	usilovat
potřetí	potřetí	k4xO	potřetí
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
podpora	podpora	k1gFnSc1	podpora
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
zřejmě	zřejmě	k6eAd1	zřejmě
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
)	)	kIx)	)
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
záměru	záměr	k1gInSc2	záměr
upustil	upustit	k5eAaPmAgInS	upustit
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
se	se	k3xPyFc4	se
např.	např.	kA	např.
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
i	i	k9	i
Kongres	kongres	k1gInSc1	kongres
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
během	během	k7c2	během
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zmanipulovaných	zmanipulovaný	k2eAgFnPc2d1	zmanipulovaná
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
(	(	kIx(	(
<g/>
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Hayesem	Hayes	k1gMnSc7	Hayes
a	a	k8xC	a
Tildenem	Tilden	k1gMnSc7	Tilden
<g/>
)	)	kIx)	)
uklidnil	uklidnit	k5eAaPmAgInS	uklidnit
národ	národ	k1gInSc4	národ
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svolal	svolat	k5eAaPmAgMnS	svolat
federální	federální	k2eAgMnSc1d1	federální
komisi	komise	k1gFnSc3	komise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Willardský	Willardský	k2eAgInSc4d1	Willardský
hotel	hotel	k1gInSc4	hotel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
unikl	uniknout	k5eAaPmAgInS	uniknout
stresu	stres	k1gInSc2	stres
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
foyer	foyer	k1gNnSc6	foyer
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
lobby	lobby	k1gFnSc2	lobby
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
obvykle	obvykle	k6eAd1	obvykle
snažilo	snažit	k5eAaImAgNnS	snažit
zastihnout	zastihnout	k5eAaPmF	zastihnout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
zatracenými	zatracený	k2eAgMnPc7d1	zatracený
lobbisty	lobbista	k1gMnPc7	lobbista
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
those	those	k1gFnSc1	those
damn	damn	k1gMnSc1	damn
lobbysts	lobbysts	k1gInSc1	lobbysts
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
snad	snad	k9	snad
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
moderní	moderní	k2eAgInSc1d1	moderní
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
lobbista	lobbista	k1gMnSc1	lobbista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vládní	vládní	k2eAgInPc1d1	vládní
úřady	úřad	k1gInPc1	úřad
založené	založený	k2eAgInPc1d1	založený
během	běh	k1gInSc7	běh
Grantovy	Grantovy	k?	Grantovy
administrativy	administrativa	k1gFnSc2	administrativa
===	===	k?	===
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
(	(	kIx(	(
<g/>
Department	department	k1gInSc1	department
of	of	k?	of
Justice	justice	k1gFnSc2	justice
<g/>
)	)	kIx)	)
1870	[number]	k4	1870
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pošt	pošta	k1gFnPc2	pošta
(	(	kIx(	(
<g/>
Post	post	k1gInSc1	post
Office	Office	kA	Office
Department	department	k1gInSc1	department
<g/>
)	)	kIx)	)
1872	[number]	k4	1872
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
generálního	generální	k2eAgMnSc2d1	generální
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
(	(	kIx(	(
<g/>
Solicitor	Solicitor	k1gMnSc1	Solicitor
General	General	k1gMnSc1	General
<g/>
)	)	kIx)	)
1870	[number]	k4	1870
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Advisory	Advisor	k1gInPc1	Advisor
Board	Board	k1gInSc4	Board
on	on	k3xPp3gMnSc1	on
Civil	civil	k1gMnSc1	civil
Service	Service	k1gFnSc2	Service
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
předchůdce	předchůdce	k1gMnSc2	předchůdce
úřadu	úřad	k1gInSc2	úřad
"	"	kIx"	"
<g/>
Civil	civil	k1gMnSc1	civil
Service	Service	k1gFnSc2	Service
Commission	Commission	k1gInSc1	Commission
<g/>
"	"	kIx"	"
zavedeného	zavedený	k2eAgMnSc2d1	zavedený
1883	[number]	k4	1883
prezidentem	prezident	k1gMnSc7	prezident
Chesterem	Chester	k1gMnSc7	Chester
A.	A.	kA	A.
Arthurem	Arthur	k1gMnSc7	Arthur
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Office	Office	kA	Office
of	of	k?	of
Personnel	Personnel	k1gInSc1	Personnel
Management	management	k1gInSc1	management
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Office	Office	kA	Office
of	of	k?	of
the	the	k?	the
Surgeon	Surgeon	k1gMnSc1	Surgeon
General	General	k1gMnSc1	General
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
po	po	k7c6	po
prezidentském	prezidentský	k2eAgNnSc6d1	prezidentské
období	období	k1gNnSc6	období
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
svého	svůj	k3xOyFgNnSc2	svůj
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
mj.	mj.	kA	mj.
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
napsal	napsat	k5eAaBmAgMnS	napsat
paměti	paměť	k1gFnSc3	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
dokončení	dokončení	k1gNnSc6	dokončení
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
karcinom	karcinom	k1gInSc4	karcinom
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ulysses	Ulyssesa	k1gFnPc2	Ulyssesa
S.	S.	kA	S.
Grant	grant	k1gInSc1	grant
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
