<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
v	v	k7c6	v
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
moc	moc	k6eAd1	moc
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
