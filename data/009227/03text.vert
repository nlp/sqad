<p>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
<g/>
,	,	kIx,	,
také	také	k9	také
Ruská	ruský	k2eAgFnSc1d1	ruská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
и	и	k?	и
<g/>
,	,	kIx,	,
v	v	k7c6	v
předreformní	předreformní	k2eAgFnSc6d1	předreformní
podobě	podoba	k1gFnSc6	podoba
Pо	Pо	k1gFnSc2	Pо
И	И	k?	И
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
existující	existující	k2eAgInSc4d1	existující
v	v	k7c6	v
letech	let	k1gInPc6	let
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgFnSc4	třetí
územně	územně	k6eAd1	územně
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3	nejrozsáhlejší
říši	říše	k1gFnSc4	říše
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
<g/>
–	–	k?	–
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
carem	car	k1gMnSc7	car
Petrem	Petr	k1gMnSc7	Petr
I.	I.	kA	I.
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
imperátora	imperátor	k1gMnSc2	imperátor
<g/>
;	;	kIx,	;
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nově	nově	k6eAd1	nově
založený	založený	k2eAgInSc4d1	založený
Petrohrad	Petrohrad	k1gInSc4	Petrohrad
(	(	kIx(	(
<g/>
Sankt-Petěrburg	Sankt-Petěrburg	k1gInSc4	Sankt-Petěrburg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
v	v	k7c6	v
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
moc	moc	k6eAd1	moc
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
činila	činit	k5eAaImAgFnS	činit
rozloha	rozloha	k1gFnSc1	rozloha
impéria	impérium	k1gNnSc2	impérium
22	[number]	k4	22
400	[number]	k4	400
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
<g/>
:	:	kIx,	:
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
17	[number]	k4	17
075	[number]	k4	075
200	[number]	k4	200
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
128	[number]	k4	128
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
byli	být	k5eAaImAgMnP	být
Rusové	Rus	k1gMnPc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
81	[number]	k4	81
gubernií	gubernie	k1gFnPc2	gubernie
<g/>
,	,	kIx,	,
20	[number]	k4	20
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
1	[number]	k4	1
okruh	okruh	k1gInSc1	okruh
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Územní	územní	k2eAgInSc4d1	územní
vývoj	vývoj	k1gInSc4	vývoj
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Moskevské	moskevský	k2eAgNnSc1d1	moskevské
knížectví	knížectví	k1gNnSc1	knížectví
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
stalo	stát	k5eAaPmAgNnS	stát
říší	říš	k1gFnSc7	říš
(	(	kIx(	(
<g/>
impériem	impérium	k1gNnSc7	impérium
<g/>
)	)	kIx)	)
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
imperiální	imperiální	k2eAgFnSc1d1	imperiální
expanze	expanze	k1gFnSc1	expanze
součástí	součást	k1gFnPc2	součást
dějin	dějiny	k1gFnPc2	dějiny
předchozího	předchozí	k2eAgInSc2d1	předchozí
státního	státní	k2eAgInSc2d1	státní
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
Rusko	Rusko	k1gNnSc1	Rusko
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
<g/>
,	,	kIx,	,
Moskevského	moskevský	k2eAgNnSc2d1	moskevské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
vládci	vládce	k1gMnPc1	vládce
(	(	kIx(	(
<g/>
nejprve	nejprve	k6eAd1	nejprve
knížata	kníže	k1gMnPc1wR	kníže
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
velkoknížata	velkokníže	k1gMnPc1wR	velkokníže
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
carové	car	k1gMnPc1	car
<g/>
)	)	kIx)	)
zahájili	zahájit	k5eAaPmAgMnP	zahájit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
expanzivní	expanzivní	k2eAgFnSc4d1	expanzivní
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
z	z	k7c2	z
nevýznamného	významný	k2eNgInSc2d1	nevýznamný
údělu	úděl	k1gInSc2	úděl
stalo	stát	k5eAaPmAgNnS	stát
srdce	srdce	k1gNnSc1	srdce
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zvláště	zvláště	k6eAd1	zvláště
významnou	významný	k2eAgFnSc7d1	významná
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
považovat	považovat	k5eAaImF	považovat
osobnost	osobnost	k1gFnSc4	osobnost
Ivana	Ivan	k1gMnSc2	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1462	[number]	k4	1462
<g/>
–	–	k?	–
<g/>
1505	[number]	k4	1505
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ivana	Ivan	k1gMnSc2	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1533	[number]	k4	1533
<g/>
–	–	k?	–
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
území	území	k1gNnSc4	území
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
Rusi	Rus	k1gFnSc2	Rus
o	o	k7c4	o
severoruské	severoruský	k2eAgInPc4d1	severoruský
městské	městský	k2eAgInPc4d1	městský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
Veliký	veliký	k2eAgInSc1d1	veliký
Novgorod	Novgorod	k1gInSc1	Novgorod
a	a	k8xC	a
Pskov	Pskov	k1gInSc1	Pskov
(	(	kIx(	(
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
poslední	poslední	k2eAgNnSc4d1	poslední
území	území	k1gNnSc4	území
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
tatarských	tatarský	k2eAgInPc2d1	tatarský
chanátů	chanát	k1gInPc2	chanát
v	v	k7c6	v
Kazani	Kazaň	k1gFnSc6	Kazaň
(	(	kIx(	(
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ivana	Ivan	k1gMnSc2	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
také	také	k9	také
započala	započnout	k5eAaPmAgFnS	započnout
ruská	ruský	k2eAgFnSc1d1	ruská
kolonizace	kolonizace	k1gFnSc1	kolonizace
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1639	[number]	k4	1639
stanuli	stanout	k5eAaPmAgMnP	stanout
Rusové	Rus	k1gMnPc1	Rus
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
následně	následně	k6eAd1	následně
založili	založit	k5eAaPmAgMnP	založit
město	město	k1gNnSc4	město
Vladivostok	Vladivostok	k1gInSc4	Vladivostok
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
také	také	k9	také
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
kolonizovalo	kolonizovat	k5eAaBmAgNnS	kolonizovat
a	a	k8xC	a
kontrolovalo	kontrolovat	k5eAaImAgNnS	kontrolovat
severoamerickou	severoamerický	k2eAgFnSc4d1	severoamerická
Aljašku	Aljaška	k1gFnSc4	Aljaška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
územní	územní	k2eAgFnSc2d1	územní
expanze	expanze	k1gFnSc2	expanze
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
Ruska	Ruska	k1gFnSc1	Ruska
významná	významný	k2eAgFnSc1d1	významná
také	také	k9	také
ideologická	ideologický	k2eAgFnSc1d1	ideologická
prestiž	prestiž	k1gFnSc1	prestiž
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Moskvě	Moskva	k1gFnSc6	Moskva
zajistil	zajistit	k5eAaPmAgMnS	zajistit
právě	právě	k9	právě
Ivan	Ivan	k1gMnSc1	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
svatbou	svatba	k1gFnSc7	svatba
s	s	k7c7	s
byzantskou	byzantský	k2eAgFnSc7d1	byzantská
princeznou	princezna	k1gFnSc7	princezna
Sofií	Sofia	k1gFnSc7	Sofia
a	a	k8xC	a
přijetím	přijetí	k1gNnSc7	přijetí
byzantského	byzantský	k2eAgMnSc2d1	byzantský
orla	orel	k1gMnSc2	orel
jako	jako	k8xC	jako
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
titulu	titul	k1gInSc2	titul
cara	car	k1gMnSc2	car
(	(	kIx(	(
<g/>
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
caesar	caesar	k1gInSc1	caesar
=	=	kIx~	=
císař	císař	k1gMnSc1	císař
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1472	[number]	k4	1472
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
imperiální	imperiální	k2eAgFnSc4d1	imperiální
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
ideologii	ideologie	k1gFnSc4	ideologie
Moskvy	Moskva	k1gFnSc2	Moskva
–	–	k?	–
Třetího	třetí	k4xOgInSc2	třetí
Říma	Řím	k1gInSc2	Řím
navázal	navázat	k5eAaPmAgInS	navázat
také	také	k9	také
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ruskou	ruský	k2eAgFnSc4d1	ruská
koncepci	koncepce	k1gFnSc4	koncepce
říše	říš	k1gFnSc2	říš
více	hodně	k6eAd2	hodně
pozápadnil	pozápadnit	k5eAaPmAgMnS	pozápadnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
územním	územní	k2eAgMnPc3d1	územní
ziskům	zisk	k1gInPc3	zisk
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
patřilo	patřit	k5eAaImAgNnS	patřit
získání	získání	k1gNnSc4	získání
levobřežní	levobřežní	k2eAgFnSc2d1	levobřežní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
Malá	malý	k2eAgFnSc1d1	malá
Rus	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
následné	následný	k2eAgNnSc4d1	následné
postupné	postupný	k2eAgNnSc4d1	postupné
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
jako	jako	k8xC	jako
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zisky	zisk	k1gInPc1	zisk
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
orientaci	orientace	k1gFnSc4	orientace
Moskvy	Moskva	k1gFnSc2	Moskva
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
konkrétněji	konkrétně	k6eAd2	konkrétně
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
a	a	k8xC	a
k	k	k7c3	k
Pobaltí	Pobaltí	k1gNnSc3	Pobaltí
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
strategický	strategický	k2eAgInSc4d1	strategický
význam	význam	k1gInSc4	význam
si	se	k3xPyFc3	se
ruští	ruský	k2eAgMnPc1d1	ruský
carové	car	k1gMnPc1	car
dobře	dobře	k6eAd1	dobře
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stranou	strana	k1gFnSc7	strana
nezůstávaly	zůstávat	k5eNaImAgFnP	zůstávat
ani	ani	k8xC	ani
oblasti	oblast	k1gFnPc1	oblast
jihovýchodní	jihovýchodní	k2eAgFnPc1d1	jihovýchodní
kolem	kolo	k1gNnSc7	kolo
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
kontrolované	kontrolovaný	k2eAgInPc1d1	kontrolovaný
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
nebo	nebo	k8xC	nebo
jejími	její	k3xOp3gMnPc7	její
klienty	klient	k1gMnPc7	klient
<g/>
.	.	kIx.	.
</s>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
Černým	Černý	k1gMnSc7	Černý
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
kontrola	kontrola	k1gFnSc1	kontrola
Bosporu	Bospor	k1gInSc2	Bospor
a	a	k8xC	a
Dardanel	Dardanely	k1gFnPc2	Dardanely
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
klíčových	klíčový	k2eAgMnPc2d1	klíčový
momentů	moment	k1gInPc2	moment
ruské	ruský	k2eAgFnSc2d1	ruská
imperiální	imperiální	k2eAgFnSc2d1	imperiální
expanze	expanze	k1gFnSc2	expanze
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
postupně	postupně	k6eAd1	postupně
získalo	získat	k5eAaPmAgNnS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
pobřežím	pobřeží	k1gNnSc7	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
dobytí	dobytí	k1gNnSc1	dobytí
Azova	Azov	k1gInSc2	Azov
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
vlaštovkou	vlaštovka	k1gFnSc7	vlaštovka
<g/>
)	)	kIx)	)
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
regionu	region	k1gInSc6	region
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
připojením	připojení	k1gNnSc7	připojení
Gruzie	Gruzie	k1gFnSc2	Gruzie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
také	také	k9	také
významně	významně	k6eAd1	významně
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Petra	Petr	k1gMnSc4	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgMnSc4d1	veliký
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
zcela	zcela	k6eAd1	zcela
připojeno	připojen	k2eAgNnSc1d1	připojeno
Pobaltí	Pobaltí	k1gNnSc1	Pobaltí
(	(	kIx(	(
<g/>
dobyto	dobyt	k2eAgNnSc1d1	dobyto
na	na	k7c6	na
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
Kateřiny	Kateřina	k1gFnPc4	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliké	veliký	k2eAgNnSc1d1	veliké
získalo	získat	k5eAaPmAgNnS	získat
Rusko	Rusko	k1gNnSc1	Rusko
východní	východní	k2eAgFnSc2d1	východní
oblasti	oblast	k1gFnSc2	oblast
polsko-litevské	polskoitevský	k2eAgFnSc2d1	polsko-litevská
Rzeczi	Rzecze	k1gFnSc4	Rzecze
Pospolité	pospolitý	k2eAgInPc1d1	pospolitý
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
východního	východní	k2eAgNnSc2d1	východní
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
Krym	Krym	k1gInSc4	Krym
s	s	k7c7	s
přilehlou	přilehlý	k2eAgFnSc7d1	přilehlá
částí	část	k1gFnSc7	část
jižní	jižní	k2eAgFnSc2d1	jižní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgInP	založit
přístavy	přístav	k1gInPc1	přístav
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
,	,	kIx,	,
Cherson	Cherson	k1gInSc1	Cherson
a	a	k8xC	a
Oděsa	Oděsa	k1gFnSc1	Oděsa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
Finské	finský	k2eAgFnSc2d1	finská
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
také	také	k6eAd1	také
připojeno	připojit	k5eAaPmNgNnS	připojit
Finsko	Finsko	k1gNnSc1	Finsko
mající	mající	k2eAgMnSc1d1	mající
status	status	k1gInSc4	status
autonomního	autonomní	k2eAgNnSc2d1	autonomní
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ruská	ruský	k2eAgFnSc1d1	ruská
expanze	expanze	k1gFnSc1	expanze
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ruské	ruský	k2eAgNnSc1d1	ruské
teritorium	teritorium	k1gNnSc1	teritorium
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
ještě	ještě	k9	ještě
o	o	k7c4	o
několik	několik	k4yIc4	několik
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
vedle	vedle	k7c2	vedle
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
Zakavkazska	Zakavkazsko	k1gNnSc2	Zakavkazsko
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
Besarábii	Besarábie	k1gFnSc6	Besarábie
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
Chiva	Chiva	k1gFnSc1	Chiva
a	a	k8xC	a
Buchara	Buchara	k1gFnSc1	Buchara
připojeny	připojit	k5eAaPmNgFnP	připojit
v	v	k7c6	v
letech	let	k1gInPc6	let
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Středoasijská	středoasijský	k2eAgFnSc1d1	středoasijská
expanze	expanze	k1gFnSc1	expanze
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
ruské	ruský	k2eAgFnSc2d1	ruská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
soupeření	soupeření	k1gNnSc2	soupeření
v	v	k7c6	v
regionu	region	k1gInSc6	region
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
Persie	Persie	k1gFnSc2	Persie
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úpadek	úpadek	k1gInSc4	úpadek
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
ještě	ještě	k6eAd1	ještě
rostlo	růst	k5eAaImAgNnS	růst
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
prohluboval	prohlubovat	k5eAaImAgMnS	prohlubovat
jeho	on	k3xPp3gInSc4	on
úpadek	úpadek	k1gInSc4	úpadek
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
konzervatismu	konzervatismus	k1gInSc2	konzervatismus
v	v	k7c6	v
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
míra	míra	k1gFnSc1	míra
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
absolutismus	absolutismus	k1gInSc1	absolutismus
<g/>
,	,	kIx,	,
zaostalost	zaostalost	k1gFnSc1	zaostalost
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
dědictví	dědictví	k1gNnSc2	dědictví
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
,	,	kIx,	,
národnostní	národnostní	k2eAgFnSc1d1	národnostní
otázka	otázka	k1gFnSc1	otázka
a	a	k8xC	a
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
obecně	obecně	k6eAd1	obecně
pomalá	pomalý	k2eAgFnSc1d1	pomalá
modernizace	modernizace	k1gFnSc1	modernizace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
prohlubování	prohlubování	k1gNnSc3	prohlubování
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
krize	krize	k1gFnSc2	krize
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
projevů	projev	k1gInPc2	projev
krize	krize	k1gFnSc2	krize
byla	být	k5eAaImAgFnS	být
porážka	porážka	k1gFnSc1	porážka
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
Krymské	krymský	k2eAgFnSc6d1	Krymská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
a	a	k8xC	a
prodej	prodej	k1gInSc4	prodej
Aljašky	Aljaška	k1gFnSc2	Aljaška
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
americkým	americký	k2eAgInPc3d1	americký
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začaly	začít	k5eAaPmAgInP	začít
sociálně-politické	sociálněolitický	k2eAgInPc1d1	sociálně-politický
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
potýkalo	potýkat	k5eAaImAgNnS	potýkat
<g/>
,	,	kIx,	,
eskalovat	eskalovat	k5eAaImF	eskalovat
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
sociálně-politické	sociálněolitický	k2eAgFnSc6d1	sociálně-politická
krizi	krize	k1gFnSc6	krize
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
hlavními	hlavní	k2eAgInPc7d1	hlavní
jmenovateli	jmenovatel	k1gInPc7	jmenovatel
byly	být	k5eAaImAgFnP	být
sílící	sílící	k2eAgFnPc1d1	sílící
snahy	snaha	k1gFnPc1	snaha
především	především	k9	především
středních	střední	k2eAgFnPc2d1	střední
vrstev	vrstva	k1gFnPc2	vrstva
a	a	k8xC	a
dělnického	dělnický	k2eAgNnSc2d1	dělnické
hnutí	hnutí	k1gNnSc2	hnutí
o	o	k7c4	o
uvolnění	uvolnění	k1gNnSc4	uvolnění
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
odpor	odpor	k1gInSc4	odpor
neruských	ruský	k2eNgInPc2d1	neruský
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Čuvaši	Čuvaš	k1gMnPc5	Čuvaš
<g/>
,	,	kIx,	,
Litevci	Litevec	k1gMnPc5	Litevec
<g/>
,	,	kIx,	,
Finové	Fin	k1gMnPc5	Fin
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
stále	stále	k6eAd1	stále
agresívnější	agresívní	k2eAgFnSc3d2	agresívnější
rusifikaci	rusifikace	k1gFnSc3	rusifikace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
se	se	k3xPyFc4	se
střetlo	střetnout	k5eAaPmAgNnS	střetnout
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
a	a	k8xC	a
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
v	v	k7c6	v
rusko-japonské	ruskoaponský	k2eAgFnSc6d1	rusko-japonská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
vliv	vliv	k1gInSc1	vliv
na	na	k7c6	na
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgNnSc1d1	ruské
válečné	válečný	k2eAgNnSc1d1	válečné
loďstvo	loďstvo	k1gNnSc1	loďstvo
bylo	být	k5eAaImAgNnS	být
rozdrceno	rozdrcen	k2eAgNnSc1d1	rozdrceno
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Cušimy	Cušima	k1gFnSc2	Cušima
<g/>
)	)	kIx)	)
a	a	k8xC	a
velmocenská	velmocenský	k2eAgFnSc1d1	velmocenská
prestiž	prestiž	k1gFnSc1	prestiž
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgFnPc1d1	následná
dvě	dva	k4xCgFnPc1	dva
ruské	ruský	k2eAgFnPc1d1	ruská
revoluce	revoluce	k1gFnPc1	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
a	a	k8xC	a
1917	[number]	k4	1917
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
na	na	k7c4	na
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
krizi	krize	k1gFnSc4	krize
samoděržaví	samoděržaví	k1gNnSc2	samoděržaví
<g/>
,	,	kIx,	,
despocie	despocie	k1gFnSc2	despocie
<g/>
,	,	kIx,	,
tyranie	tyranie	k1gFnSc2	tyranie
<g/>
,	,	kIx,	,
krutovlády	krutovláda	k1gFnSc2	krutovláda
(	(	kIx(	(
<g/>
carský	carský	k2eAgInSc1d1	carský
režim	režim	k1gInSc1	režim
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
sociálního	sociální	k2eAgNnSc2d1	sociální
a	a	k8xC	a
politického	politický	k2eAgNnSc2d1	politické
uspořádání	uspořádání	k1gNnSc2	uspořádání
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
ranou	rána	k1gFnSc7	rána
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Impérium	impérium	k1gNnSc4	impérium
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naplno	naplno	k6eAd1	naplno
odhalila	odhalit	k5eAaPmAgFnS	odhalit
jeho	jeho	k3xOp3gNnSc4	jeho
nefunkční	funkční	k2eNgNnSc4d1	nefunkční
společenské	společenský	k2eAgNnSc4d1	společenské
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
technologickou	technologický	k2eAgFnSc4d1	technologická
zaostalost	zaostalost	k1gFnSc4	zaostalost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
car	car	k1gMnSc1	car
nebral	brát	k5eNaImAgMnS	brát
ohledy	ohled	k1gInPc4	ohled
na	na	k7c4	na
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
násilná	násilný	k2eAgFnSc1d1	násilná
vláda	vláda	k1gFnSc1	vláda
znamenala	znamenat	k5eAaImAgFnS	znamenat
otřesy	otřes	k1gInPc4	otřes
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
zaostalá	zaostalý	k2eAgFnSc1d1	zaostalá
<g/>
,	,	kIx,	,
během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ruská	ruský	k2eAgFnSc1d1	ruská
carská	carský	k2eAgFnSc1d1	carská
armáda	armáda	k1gFnSc1	armáda
rozložila	rozložit	k5eAaPmAgFnS	rozložit
a	a	k8xC	a
Ruská	ruský	k2eAgFnSc1d1	ruská
říše	říše	k1gFnSc1	říše
společensky	společensky	k6eAd1	společensky
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
i	i	k9	i
vojensky	vojensky	k6eAd1	vojensky
zkolabovala	zkolabovat	k5eAaPmAgFnS	zkolabovat
<g/>
.	.	kIx.	.
</s>
<s>
Oslabený	oslabený	k2eAgInSc1d1	oslabený
carský	carský	k2eAgInSc1d1	carský
režim	režim	k1gInSc1	režim
padl	padnout	k5eAaPmAgInS	padnout
ve	v	k7c6	v
víru	vír	k1gInSc6	vír
druhé	druhý	k4xOgFnSc2	druhý
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
rozvrácenou	rozvrácený	k2eAgFnSc4d1	rozvrácená
zemi	zem	k1gFnSc4	zem
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Krátkou	krátký	k2eAgFnSc7d1	krátká
epizodou	epizoda	k1gFnSc7	epizoda
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
Ruské	ruský	k2eAgFnSc2d1	ruská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc1	organizace
vlády	vláda	k1gFnSc2	vláda
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Gosudar-Imperátor	Gosudar-Imperátor	k1gInSc4	Gosudar-Imperátor
===	===	k?	===
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
říše	říše	k1gFnSc1	říše
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svojí	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
absolutistickým	absolutistický	k2eAgInSc7d1	absolutistický
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
založeným	založený	k2eAgNnSc7d1	založené
na	na	k7c6	na
principu	princip	k1gInSc6	princip
samoděržaví	samoděržaví	k1gNnSc2	samoděržaví
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
1905	[number]	k4	1905
a	a	k8xC	a
Říjnovém	říjnový	k2eAgInSc6d1	říjnový
manifestu	manifest	k1gInSc6	manifest
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
(	(	kIx(	(
<g/>
neoficiální	oficiální	k2eNgInSc1d1	neoficiální
titul	titul	k1gInSc1	titul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
gosudar-imperátor	gosudarmperátor	k1gInSc1	gosudar-imperátor
absolutistickým	absolutistický	k2eAgMnSc7d1	absolutistický
panovníkem	panovník	k1gMnSc7	panovník
s	s	k7c7	s
neomezenou	omezený	k2eNgFnSc7d1	neomezená
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
samoděržaví	samoděržaví	k1gNnSc2	samoděržaví
vycházel	vycházet	k5eAaImAgInS	vycházet
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
byzantské	byzantský	k2eAgFnSc2d1	byzantská
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odrážel	odrážet	k5eAaImAgMnS	odrážet
také	také	k6eAd1	také
prvky	prvek	k1gInPc4	prvek
východního	východní	k2eAgInSc2d1	východní
despotismu	despotismus	k1gInSc2	despotismus
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
gosudar-imperátor	gosudarmperátor	k1gInSc1	gosudar-imperátor
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukách	ruka	k1gFnPc6	ruka
držel	držet	k5eAaImAgInS	držet
veškerou	veškerý	k3xTgFnSc4	veškerý
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
potom	potom	k6eAd1	potom
delegoval	delegovat	k5eAaBmAgMnS	delegovat
na	na	k7c4	na
jemu	on	k3xPp3gMnSc3	on
podřízené	podřízený	k2eAgInPc1d1	podřízený
orgány	orgán	k1gInPc1	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samoděržavný	samoděržavný	k2eAgInSc1d1	samoděržavný
princip	princip	k1gInSc1	princip
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
určitým	určitý	k2eAgInSc7d1	určitý
derivátem	derivát	k1gInSc7	derivát
několika	několik	k4yIc2	několik
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
(	(	kIx(	(
<g/>
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
částečně	částečně	k6eAd1	částečně
<g/>
)	)	kIx)	)
samoděržaví	samoděržaví	k1gNnSc1	samoděržaví
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
pozápadnit	pozápadnit	k5eAaPmF	pozápadnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užíval	užívat	k5eAaImAgInS	užívat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
stále	stále	k6eAd1	stále
despotických	despotický	k2eAgFnPc2d1	despotická
metod	metoda	k1gFnPc2	metoda
svých	svůj	k3xOyFgMnPc2	svůj
moskevských	moskevský	k2eAgMnPc2d1	moskevský
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
samoděržaví	samoděržaví	k1gNnSc1	samoděržaví
samozřejmě	samozřejmě	k6eAd1	samozřejmě
proměňovalo	proměňovat	k5eAaImAgNnS	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
moderní	moderní	k2eAgFnSc4d1	moderní
interpretaci	interpretace	k1gFnSc4	interpretace
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
nebo	nebo	k8xC	nebo
měly	mít	k5eAaImAgFnP	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
pozvolnému	pozvolný	k2eAgInSc3d1	pozvolný
přechodu	přechod	k1gInSc3	přechod
od	od	k7c2	od
despoticky	despoticky	k6eAd1	despoticky
a	a	k8xC	a
tradičně	tradičně	k6eAd1	tradičně
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
státu	stát	k1gInSc2	stát
k	k	k7c3	k
právnímu	právní	k2eAgInSc3d1	právní
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
vlády	vláda	k1gFnSc2	vláda
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Alexandra	Alexandr	k1gMnSc2	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
vnuka	vnuk	k1gMnSc4	vnuk
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
tradičnímu	tradiční	k2eAgNnSc3d1	tradiční
konzervativnímu	konzervativní	k2eAgNnSc3d1	konzervativní
pojetí	pojetí	k1gNnSc3	pojetí
samoděržaví	samoděržaví	k1gNnSc2	samoděržaví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vidělo	vidět	k5eAaImAgNnS	vidět
v	v	k7c6	v
gosudaru-imperátorovi	gosudarumperátor	k1gMnSc6	gosudaru-imperátor
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
neomezené	omezený	k2eNgNnSc1d1	neomezené
moci	moct	k5eAaImF	moct
jedinou	jediný	k2eAgFnSc4d1	jediná
záruku	záruka	k1gFnSc4	záruka
politické	politický	k2eAgFnSc2d1	politická
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
dostávaly	dostávat	k5eAaImAgFnP	dostávat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
politickými	politický	k2eAgFnPc7d1	politická
i	i	k8xC	i
sociálními	sociální	k2eAgFnPc7d1	sociální
změnami	změna	k1gFnPc7	změna
uvnitř	uvnitř	k7c2	uvnitř
ruské	ruský	k2eAgFnSc2d1	ruská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ruských	ruský	k2eAgMnPc2d1	ruský
gosudarů-imperátorů	gosudarůmperátor	k1gMnPc2	gosudarů-imperátor
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1721	[number]	k4	1721
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1725	[number]	k4	1725
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
car	car	k1gMnSc1	car
1682	[number]	k4	1682
<g/>
–	–	k?	–
<g/>
1722	[number]	k4	1722
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
I.	I.	kA	I.
Ruská	ruský	k2eAgFnSc1d1	ruská
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
–	–	k?	–
<g/>
1727	[number]	k4	1727
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
(	(	kIx(	(
<g/>
1727	[number]	k4	1727
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Ivanovna	Ivanovna	k1gFnSc1	Ivanovna
(	(	kIx(	(
<g/>
1730	[number]	k4	1730
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1741	[number]	k4	1741
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliká	veliký	k2eAgFnSc1d1	veliká
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
–	–	k?	–
<g/>
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
–	–	k?	–
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alexandrovič	Alexandrovič	k1gInSc1	Alexandrovič
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vládní	vládní	k2eAgFnSc1d1	vládní
rada	rada	k1gFnSc1	rada
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vládní	vládní	k2eAgFnSc7d1	vládní
reformou	reforma	k1gFnSc7	reforma
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgMnSc2d1	veliký
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
nahradit	nahradit	k5eAaPmF	nahradit
bojarský	bojarský	k2eAgInSc4d1	bojarský
sněm	sněm	k1gInSc4	sněm
a	a	k8xC	a
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
napomoci	napomoct	k5eAaPmF	napomoct
lepší	dobrý	k2eAgFnSc3d2	lepší
organizaci	organizace	k1gFnSc3	organizace
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
Ruskem	Rusko	k1gNnSc7	Rusko
podle	podle	k7c2	podle
západních	západní	k2eAgInPc2d1	západní
vzorů	vzor	k1gInPc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
rada	rada	k1gFnSc1	rada
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
výkonným	výkonný	k2eAgInSc7d1	výkonný
<g/>
,	,	kIx,	,
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
i	i	k8xC	i
soudním	soudní	k2eAgInSc7d1	soudní
orgánem	orgán	k1gInSc7	orgán
ruských	ruský	k2eAgMnPc2d1	ruský
monarchů	monarcha	k1gMnPc2	monarcha
a	a	k8xC	a
předsedal	předsedat	k5eAaImAgMnS	předsedat
jí	jíst	k5eAaImIp3nS	jíst
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
oberprokurátor	oberprokurátor	k1gInSc4	oberprokurátor
Nejsvětějšího	nejsvětější	k2eAgInSc2d1	nejsvětější
synodu	synod	k1gInSc2	synod
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
členů	člen	k1gInPc2	člen
Rady	rada	k1gFnSc2	rada
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
odlišného	odlišný	k2eAgInSc2d1	odlišný
kontextu	kontext	k1gInSc2	kontext
po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
a	a	k8xC	a
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
===	===	k?	===
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
poradní	poradní	k2eAgInSc1d1	poradní
orgán	orgán	k1gInSc1	orgán
ruských	ruský	k2eAgMnPc2d1	ruský
panovníků	panovník	k1gMnPc2	panovník
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reformy	reforma	k1gFnSc2	reforma
ruské	ruský	k2eAgFnSc2d1	ruská
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
funkce	funkce	k1gFnSc1	funkce
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
byly	být	k5eAaImAgInP	být
spojeny	spojen	k2eAgInPc1d1	spojen
se	s	k7c7	s
zákonodárstvím	zákonodárství	k1gNnSc7	zákonodárství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ustanovení	ustanovení	k1gNnSc2	ustanovení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
se	se	k3xPyFc4	se
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
radě	rada	k1gFnSc6	rada
měly	mít	k5eAaImAgInP	mít
především	především	k6eAd1	především
probírat	probírat	k5eAaImF	probírat
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
zákonných	zákonný	k2eAgFnPc2d1	zákonná
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
členů	člen	k1gMnPc2	člen
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
nebyla	být	k5eNaImAgFnS	být
volena	volit	k5eAaImNgFnS	volit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
carem	car	k1gMnSc7	car
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
volena	volit	k5eAaImNgFnS	volit
z	z	k7c2	z
gubernií	gubernie	k1gFnPc2	gubernie
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
delegace	delegace	k1gFnSc2	delegace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Nejsvětější	nejsvětější	k2eAgInSc1d1	nejsvětější
synod	synod	k1gInSc1	synod
měl	mít	k5eAaImAgInS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
6	[number]	k4	6
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
finský	finský	k2eAgInSc1d1	finský
Senát	senát	k1gInSc1	senát
na	na	k7c4	na
2	[number]	k4	2
místa	místo	k1gNnSc2	místo
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
transformaci	transformace	k1gFnSc3	transformace
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
v	v	k7c4	v
horní	horní	k2eAgFnSc4d1	horní
komoru	komora	k1gFnSc4	komora
ruské	ruský	k2eAgFnSc2d1	ruská
Dumy	duma	k1gFnSc2	duma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Duma	duma	k1gFnSc1	duma
===	===	k?	===
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
neměla	mít	k5eNaImAgFnS	mít
Ruská	ruský	k2eAgFnSc1d1	ruská
říše	říše	k1gFnSc1	říše
jako	jako	k8xC	jako
absolutisticky	absolutisticky	k6eAd1	absolutisticky
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
stát	stát	k1gInSc4	stát
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
moderního	moderní	k2eAgInSc2d1	moderní
parlamentu	parlament	k1gInSc2	parlament
nebo	nebo	k8xC	nebo
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc3	Rusko
oficiálně	oficiálně	k6eAd1	oficiálně
neexistovaly	existovat	k5eNaImAgFnP	existovat
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
Duma	duma	k1gFnSc1	duma
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
až	až	k9	až
po	po	k7c6	po
ruské	ruský	k2eAgFnSc6d1	ruská
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Manifestu	manifest	k1gInSc2	manifest
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
Duma	duma	k1gFnSc1	duma
svolána	svolán	k2eAgFnSc1d1	svolána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
také	také	k9	také
legalizována	legalizován	k2eAgFnSc1d1	legalizována
existence	existence	k1gFnSc1	existence
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
garantována	garantován	k2eAgNnPc4d1	garantováno
základní	základní	k2eAgNnPc4d1	základní
občanská	občanský	k2eAgNnPc4d1	občanské
práva	právo	k1gNnPc4	právo
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
Říjnovým	říjnový	k2eAgInSc7d1	říjnový
manifestem	manifest	k1gInSc7	manifest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
byly	být	k5eAaImAgFnP	být
zvoleny	zvolit	k5eAaPmNgInP	zvolit
a	a	k8xC	a
svolány	svolat	k5eAaPmNgInP	svolat
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
Dumy	duma	k1gFnPc4	duma
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nově	nově	k6eAd1	nově
etablující	etablující	k2eAgMnSc1d1	etablující
se	se	k3xPyFc4	se
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Ruska	Rusko	k1gNnSc2	Rusko
byl	být	k5eAaImAgInS	být
relativně	relativně	k6eAd1	relativně
nestabilní	stabilní	k2eNgInSc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
ruské	ruský	k2eAgFnPc1d1	ruská
Dumy	duma	k1gFnPc1	duma
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
I.	I.	kA	I.
Duma	duma	k1gFnSc1	duma
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
–	–	k?	–
červen	červen	k1gInSc1	červen
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Duma	duma	k1gFnSc1	duma
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
–	–	k?	–
červen	červen	k1gInSc1	červen
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Duma	duma	k1gFnSc1	duma
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Duma	duma	k1gFnSc1	duma
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
Volební	volební	k2eAgInSc1d1	volební
systém	systém	k1gInSc1	systém
Ruska	Rusko	k1gNnSc2	Rusko
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
kuriálním	kuriální	k2eAgInSc6d1	kuriální
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zaručoval	zaručovat	k5eAaImAgInS	zaručovat
dominantní	dominantní	k2eAgInSc4d1	dominantní
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
zastoupení	zastoupení	k1gNnSc3	zastoupení
v	v	k7c6	v
Dumě	duma	k1gFnSc6	duma
pro	pro	k7c4	pro
velkostatkáře	velkostatkář	k1gMnPc4	velkostatkář
a	a	k8xC	a
městské	městský	k2eAgFnPc4d1	městská
elity	elita	k1gFnPc4	elita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgFnPc1d2	menší
pro	pro	k7c4	pro
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
rolníky	rolník	k1gMnPc4	rolník
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInPc1d3	veliký
sociální	sociální	k2eAgFnSc4d1	sociální
skupinu	skupina	k1gFnSc4	skupina
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
zástupce	zástupce	k1gMnPc4	zástupce
neruských	ruský	k2eNgInPc2d1	neruský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
zastoupené	zastoupený	k2eAgFnPc1d1	zastoupená
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
předrevolučních	předrevoluční	k2eAgFnPc6d1	předrevoluční
ruských	ruský	k2eAgFnPc6d1	ruská
Dumách	duma	k1gFnPc6	duma
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sociálně-demokratická	Sociálněemokratický	k2eAgFnSc1d1	Sociálně-demokratická
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
Eseři	eser	k1gMnPc1	eser
(	(	kIx(	(
<g/>
Sociálně-revoluční	Sociálněevoluční	k2eAgFnSc1d1	Sociálně-revoluční
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
</s>
</p>
<p>
<s>
Trudovici	trudovik	k1gMnPc1	trudovik
</s>
</p>
<p>
<s>
Progresisté	progresista	k1gMnPc1	progresista
</s>
</p>
<p>
<s>
Kadeti	kadet	k1gMnPc1	kadet
(	(	kIx(	(
<g/>
Konstitucionálně-demokratická	Konstitucionálněemokratický	k2eAgFnSc1d1	Konstitucionálně-demokratický
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Autonomisté	autonomista	k1gMnPc1	autonomista
</s>
</p>
<p>
<s>
Okťabristé	okťabrista	k1gMnPc1	okťabrista
</s>
</p>
<p>
<s>
Nacionalisté	nacionalista	k1gMnPc1	nacionalista
</s>
</p>
<p>
<s>
Krajní	krajní	k2eAgFnSc1d1	krajní
pravice	pravice	k1gFnSc1	pravice
</s>
</p>
<p>
<s>
===	===	k?	===
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Ministerstva	ministerstvo	k1gNnPc1	ministerstvo
podle	podle	k7c2	podle
evropského	evropský	k2eAgInSc2d1	evropský
(	(	kIx(	(
<g/>
francouzského	francouzský	k2eAgInSc2d1	francouzský
<g/>
)	)	kIx)	)
vzoru	vzor	k1gInSc2	vzor
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
zřízena	zřídit	k5eAaPmNgFnS	zřídit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Alexandra	Alexandr	k1gMnSc2	Alexandr
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
<g/>
.	.	kIx.	.
</s>
<s>
Ministři	ministr	k1gMnPc1	ministr
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
za	za	k7c4	za
služebníky	služebník	k1gMnPc4	služebník
cara	car	k1gMnSc2	car
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgNnSc4	jenž
gosudar-imperátor	gosudarmperátor	k1gMnSc1	gosudar-imperátor
delegoval	delegovat	k5eAaBmAgMnS	delegovat
část	část	k1gFnSc4	část
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
neomezených	omezený	k2eNgFnPc2d1	neomezená
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
tato	tento	k3xDgNnPc4	tento
ministerstva	ministerstvo	k1gNnPc4	ministerstvo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
pozemních	pozemní	k2eAgFnPc2d1	pozemní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
lidové	lidový	k2eAgFnSc2d1	lidová
osvěty	osvěta	k1gFnSc2	osvěta
<g/>
.	.	kIx.	.
<g/>
Záležitosti	záležitost	k1gFnPc1	záležitost
kultu	kult	k1gInSc2	kult
byly	být	k5eAaImAgFnP	být
přenechány	přenechán	k2eAgFnPc1d1	přenechána
Nejsvětějšímu	nejsvětější	k2eAgInSc3d1	nejsvětější
synodu	synod	k1gInSc3	synod
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
nestál	stát	k5eNaImAgMnS	stát
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oberprokurátor	oberprokurátor	k1gInSc1	oberprokurátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
(	(	kIx(	(
<g/>
po	po	k7c6	po
první	první	k4xOgFnSc6	první
ruské	ruský	k2eAgFnSc6d1	ruská
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
Rusko	Rusko	k1gNnSc1	Rusko
ještě	ještě	k9	ještě
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
;	;	kIx,	;
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
pozemních	pozemní	k2eAgFnPc2d1	pozemní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
v	v	k7c4	v
jedno	jeden	k4xCgNnSc4	jeden
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
záležitosti	záležitost	k1gFnSc6	záležitost
carského	carský	k2eAgInSc2d1	carský
dvora	dvůr	k1gInSc2	dvůr
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgMnS	starat
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
ministr	ministr	k1gMnSc1	ministr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Funkce	funkce	k1gFnSc1	funkce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
poprvé	poprvé	k6eAd1	poprvé
oficiálně	oficiálně	k6eAd1	oficiálně
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zřízení	zřízení	k1gNnSc2	zřízení
kolektivního	kolektivní	k2eAgInSc2d1	kolektivní
vládního	vládní	k2eAgInSc2d1	vládní
orgánu	orgán	k1gInSc2	orgán
rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
neoficiálně	neoficiálně	k6eAd1	neoficiálně
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
často	často	k6eAd1	často
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
mělo	mít	k5eAaImAgNnS	mít
Rusko	Rusko	k1gNnSc1	Rusko
stabilně	stabilně	k6eAd1	stabilně
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
funkce	funkce	k1gFnPc4	funkce
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Witte	Witt	k1gInSc5	Witt
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Goremykin	Goremykin	k1gMnSc1	Goremykin
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
–	–	k?	–
červenec	červenec	k1gInSc1	červenec
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pjotr	Pjotr	k1gMnSc1	Pjotr
Stolypin	Stolypin	k1gMnSc1	Stolypin
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gInSc1	Vladimir
Kokovcov	Kokovcov	k1gInSc1	Kokovcov
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Goremykin	Goremykin	k1gMnSc1	Goremykin
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Boris	Boris	k1gMnSc1	Boris
Strumer	Strumer	k1gMnSc1	Strumer
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
–	–	k?	–
listopad	listopad	k1gInSc1	listopad
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Trepov	Trepov	k1gInSc1	Trepov
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Golicyn	Golicyn	k1gMnSc1	Golicyn
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
–	–	k?	–
březen	březen	k1gInSc1	březen
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
===	===	k?	===
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc4d1	moderní
státní	státní	k2eAgFnSc4d1	státní
správu	správa	k1gFnSc4	správa
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
zavedly	zavést	k5eAaPmAgFnP	zavést
reformy	reforma	k1gFnPc1	reforma
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
1708	[number]	k4	1708
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
praxe	praxe	k1gFnSc1	praxe
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
stále	stále	k6eAd1	stále
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
archaickém	archaický	k2eAgInSc6d1	archaický
principu	princip	k1gInSc6	princip
kormljenie	kormljenie	k1gFnSc2	kormljenie
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
také	také	k9	také
postrádalo	postrádat	k5eAaImAgNnS	postrádat
centralizovaně	centralizovaně	k6eAd1	centralizovaně
organizovanou	organizovaný	k2eAgFnSc4d1	organizovaná
a	a	k8xC	a
standardizovanou	standardizovaný	k2eAgFnSc4d1	standardizovaná
organizaci	organizace	k1gFnSc4	organizace
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
i	i	k8xC	i
profesionální	profesionální	k2eAgMnPc4d1	profesionální
státní	státní	k2eAgMnPc4d1	státní
úředníky	úředník	k1gMnPc4	úředník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
také	také	k9	také
nechal	nechat	k5eAaPmAgMnS	nechat
rozdělit	rozdělit	k5eAaPmF	rozdělit
Rusko	Rusko	k1gNnSc4	Rusko
na	na	k7c4	na
několik	několik	k4yIc4	několik
menších	malý	k2eAgInPc2d2	menší
a	a	k8xC	a
větších	veliký	k2eAgInPc2d2	veliký
správních	správní	k2eAgInPc2d1	správní
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgMnPc1d3	nejmenší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
volosť	volostit	k5eAaPmRp2nS	volostit
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
následoval	následovat	k5eAaImAgInS	následovat
ujezd	ujezd	k6eAd1	ujezd
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
у	у	k?	у
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
gubernie	gubernie	k1gFnSc1	gubernie
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
г	г	k?	г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
ještě	ještě	k6eAd1	ještě
reformou	reforma	k1gFnSc7	reforma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1718	[number]	k4	1718
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
na	na	k7c4	na
provincie	provincie	k1gFnPc4	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgInSc7d1	významný
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
utvoření	utvoření	k1gNnSc1	utvoření
profesionální	profesionální	k2eAgFnSc2d1	profesionální
třídy	třída	k1gFnSc2	třída
státních	státní	k2eAgMnPc2d1	státní
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1722	[number]	k4	1722
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zavedení	zavedení	k1gNnSc1	zavedení
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
Tabulky	tabulka	k1gFnSc2	tabulka
hodností	hodnost	k1gFnSc7	hodnost
další	další	k2eAgFnSc7d1	další
krok	krok	k1gInSc4	krok
v	v	k7c6	v
systemizaci	systemizace	k1gFnSc6	systemizace
a	a	k8xC	a
standardizaci	standardizace	k1gFnSc6	standardizace
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
četné	četný	k2eAgFnPc4d1	četná
změny	změna	k1gFnPc4	změna
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
především	především	k9	především
za	za	k7c4	za
Kateřiny	Kateřina	k1gFnPc4	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliké	veliký	k2eAgNnSc1d1	veliké
<g/>
)	)	kIx)	)
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zůstal	zůstat	k5eAaPmAgInS	zůstat
princip	princip	k1gInSc1	princip
volosti	volost	k1gFnSc2	volost
–	–	k?	–
ujezdu	ujezd	k1gInSc2	ujezd
–	–	k?	–
gubernie	gubernie	k1gFnSc2	gubernie
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1914	[number]	k4	1914
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
81	[number]	k4	81
gubernii	gubernium	k1gNnPc7	gubernium
<g/>
,	,	kIx,	,
20	[number]	k4	20
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
na	na	k7c4	na
1	[number]	k4	1
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc4d2	veliký
města	město	k1gNnPc4	město
jako	jako	k9	jako
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
Oděsa	Oděsa	k1gFnSc1	Oděsa
<g/>
,	,	kIx,	,
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
,	,	kIx,	,
Rostov	Rostov	k1gInSc1	Rostov
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
administrativu	administrativa	k1gFnSc4	administrativa
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
stojící	stojící	k2eAgFnSc4d1	stojící
mimo	mimo	k7c4	mimo
systém	systém	k1gInSc4	systém
gubernií	gubernie	k1gFnPc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Vilno	Vilno	k1gNnSc1	Vilno
nebo	nebo	k8xC	nebo
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
generální	generální	k2eAgMnPc4d1	generální
guvernéry	guvernér	k1gMnPc4	guvernér
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
klasické	klasický	k2eAgFnPc4d1	klasická
administrativní	administrativní	k2eAgFnPc4d1	administrativní
a	a	k8xC	a
policejní	policejní	k2eAgFnPc4d1	policejní
správy	správa	k1gFnPc4	správa
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
odlišně	odlišně	k6eAd1	odlišně
na	na	k7c4	na
12	[number]	k4	12
okruhů	okruh	k1gInPc2	okruh
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
о	о	k?	о
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obecného	obecný	k2eAgNnSc2d1	obecné
hlediska	hledisko	k1gNnSc2	hledisko
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
relativně	relativně	k6eAd1	relativně
přísně	přísně	k6eAd1	přísně
centralizovaným	centralizovaný	k2eAgInSc7d1	centralizovaný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
rozloze	rozloha	k1gFnSc3	rozloha
a	a	k8xC	a
rozdílnosti	rozdílnost	k1gFnSc3	rozdílnost
jím	jíst	k5eAaImIp1nS	jíst
kontrolovaných	kontrolovaný	k2eAgNnPc2d1	kontrolované
teritorií	teritorium	k1gNnPc2	teritorium
však	však	k9	však
byla	být	k5eAaImAgFnS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
správa	správa	k1gFnSc1	správa
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
flota	flota	k1gFnSc1	flota
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
mělo	mít	k5eAaImAgNnS	mít
Rusko	Rusko	k1gNnSc1	Rusko
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
armádu	armáda	k1gFnSc4	armáda
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgFnPc7d1	Evropská
velmocemi	velmoc	k1gFnPc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Neznamenalo	znamenat	k5eNaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
mělo	mít	k5eAaImAgNnS	mít
armádu	armáda	k1gFnSc4	armáda
technologicky	technologicky	k6eAd1	technologicky
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
pilířů	pilíř	k1gInPc2	pilíř
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neustálé	neustálý	k2eAgFnSc3d1	neustálá
expanzi	expanze	k1gFnSc3	expanze
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
území	území	k1gNnSc2	území
v	v	k7c6	v
době	doba	k1gFnSc6	doba
existence	existence	k1gFnSc2	existence
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
byly	být	k5eAaImAgFnP	být
funkce	funkce	k1gFnPc1	funkce
armády	armáda	k1gFnSc2	armáda
nejenom	nejenom	k6eAd1	nejenom
čistě	čistě	k6eAd1	čistě
vojenské	vojenský	k2eAgFnPc1d1	vojenská
ale	ale	k8xC	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
také	také	k9	také
správní	správní	k2eAgInPc1d1	správní
a	a	k8xC	a
policejní	policejní	k2eAgInPc1d1	policejní
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
bylo	být	k5eAaImAgNnS	být
propojení	propojení	k1gNnSc4	propojení
armády	armáda	k1gFnSc2	armáda
se	s	k7c7	s
státní	státní	k2eAgFnSc7d1	státní
správou	správa	k1gFnSc7	správa
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Cela	cela	k1gFnSc1	cela
řada	řada	k1gFnSc1	řada
ruských	ruský	k2eAgMnPc2d1	ruský
politiků	politik	k1gMnPc2	politik
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
nebo	nebo	k8xC	nebo
flotile	flotila	k1gFnSc6	flotila
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
vysoké	vysoký	k2eAgFnPc4d1	vysoká
vojenské	vojenský	k2eAgFnPc4d1	vojenská
hodnosti	hodnost	k1gFnPc4	hodnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
moderní	moderní	k2eAgFnSc2d1	moderní
evropské	evropský	k2eAgFnSc2d1	Evropská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přisuzovat	přisuzovat	k5eAaImF	přisuzovat
opět	opět	k6eAd1	opět
Petrovi	Petr	k1gMnSc3	Petr
I.	I.	kA	I.
Velikému	veliký	k2eAgMnSc3d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
evropskými	evropský	k2eAgInPc7d1	evropský
vzory	vzor	k1gInPc7	vzor
nejprve	nejprve	k6eAd1	nejprve
při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
Preobraženského	Preobraženský	k2eAgInSc2d1	Preobraženský
pluku	pluk	k1gInSc2	pluk
(	(	kIx(	(
<g/>
1685	[number]	k4	1685
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
novoty	novota	k1gFnPc4	novota
patřilo	patřit	k5eAaImAgNnS	patřit
například	například	k6eAd1	například
zavedení	zavedení	k1gNnSc1	zavedení
vojenských	vojenský	k2eAgInPc2d1	vojenský
stejnokrojů	stejnokroj	k1gInPc2	stejnokroj
(	(	kIx(	(
<g/>
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
<s>
Preobraženský	Preobraženský	k2eAgInSc1d1	Preobraženský
pluk	pluk	k1gInSc1	pluk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
později	pozdě	k6eAd2	pozdě
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
reorganizaci	reorganizace	k1gFnSc4	reorganizace
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
prvních	první	k4xOgInPc6	první
neúspěších	neúspěch	k1gInPc6	neúspěch
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Narvy	Narva	k1gFnSc2	Narva
<g/>
,	,	kIx,	,
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
krokem	krok	k1gInSc7	krok
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
reformách	reforma	k1gFnPc6	reforma
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
přestala	přestat	k5eAaPmAgFnS	přestat
mít	mít	k5eAaImF	mít
svůj	svůj	k3xOyFgInSc4	svůj
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
polofeudální	polofeudální	k2eAgInSc4d1	polofeudální
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
kdykoliv	kdykoliv	k6eAd1	kdykoliv
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
potřebám	potřeba	k1gFnPc3	potřeba
gosudara-imperátora	gosudaramperátor	k1gMnSc4	gosudara-imperátor
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byly	být	k5eAaImAgInP	být
přitom	přitom	k6eAd1	přitom
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
flotila	flotila	k1gFnSc1	flotila
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
osobní	osobní	k2eAgInSc4d1	osobní
majetek	majetek	k1gInSc4	majetek
gosudara-imperátora	gosudaramperátor	k1gMnSc2	gosudara-imperátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
stálé	stálý	k2eAgFnSc2d1	stálá
armády	armáda	k1gFnSc2	armáda
samozřejmě	samozřejmě	k6eAd1	samozřejmě
znamenala	znamenat	k5eAaImAgFnS	znamenat
nutnost	nutnost	k1gFnSc4	nutnost
zajištění	zajištění	k1gNnSc2	zajištění
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
přílivu	příliv	k1gInSc2	příliv
rekrutů	rekrut	k1gMnPc2	rekrut
a	a	k8xC	a
finančních	finanční	k2eAgInPc2d1	finanční
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
první	první	k4xOgFnSc1	první
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
povinnost	povinnost	k1gFnSc1	povinnost
odvodu	odvod	k1gInSc2	odvod
rekrutů	rekrut	k1gMnPc2	rekrut
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
ruské	ruský	k2eAgMnPc4d1	ruský
obyvatele	obyvatel	k1gMnPc4	obyvatel
byla	být	k5eAaImAgFnS	být
uzákoněna	uzákoněn	k2eAgFnSc1d1	uzákoněna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
flotila	flotila	k1gFnSc1	flotila
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
značně	značně	k6eAd1	značně
mnohonárodním	mnohonárodní	k2eAgNnSc7d1	mnohonárodní
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
masa	maso	k1gNnSc2	maso
často	často	k6eAd1	často
nevzdělaných	vzdělaný	k2eNgMnPc2d1	nevzdělaný
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
mnohonárodní	mnohonárodní	k2eAgFnSc1d1	mnohonárodní
elita	elita	k1gFnSc1	elita
ruských	ruský	k2eAgMnPc2d1	ruský
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
místních	místní	k2eAgFnPc2d1	místní
aristokracií	aristokracie	k1gFnPc2	aristokracie
nebo	nebo	k8xC	nebo
starých	starý	k2eAgFnPc2d1	stará
vojenských	vojenský	k2eAgFnPc2d1	vojenská
rodin	rodina	k1gFnPc2	rodina
(	(	kIx(	(
<g/>
pobaltští	pobaltský	k2eAgMnPc1d1	pobaltský
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Gruzíni	Gruzín	k1gMnPc1	Gruzín
a	a	k8xC	a
Tataři	Tatar	k1gMnPc1	Tatar
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nejčastějším	častý	k2eAgInPc3d3	nejčastější
neruským	ruský	k2eNgInPc3d1	neruský
členům	člen	k1gInPc3	člen
důstojnického	důstojnický	k2eAgInSc2d1	důstojnický
sboru	sbor	k1gInSc2	sbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
imperiálnímu	imperiální	k2eAgInSc3d1	imperiální
charakteru	charakter	k1gInSc3	charakter
nebyl	být	k5eNaImAgMnS	být
jiný	jiný	k2eAgInSc4d1	jiný
etnický	etnický	k2eAgInSc4d1	etnický
původ	původ	k1gInSc4	původ
ani	ani	k8xC	ani
náboženství	náboženství	k1gNnSc4	náboženství
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
ve	v	k7c6	v
flotile	flotila	k1gFnSc6	flotila
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
inkorporace	inkorporace	k1gFnSc2	inkorporace
lokálních	lokální	k2eAgFnPc2d1	lokální
elit	elita	k1gFnPc2	elita
do	do	k7c2	do
imperiální	imperiální	k2eAgFnSc2d1	imperiální
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
hodností	hodnost	k1gFnSc7	hodnost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
Tabulky	tabulka	k1gFnSc2	tabulka
hodností	hodnost	k1gFnPc2	hodnost
generál-polní	generálolní	k2eAgMnSc1d1	generál-polní
maršál	maršál	k1gMnSc1	maršál
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgNnPc4d1	pozemní
vojska	vojsko	k1gNnPc4	vojsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
generál-admirál	generáldmirál	k1gInSc1	generál-admirál
(	(	kIx(	(
<g/>
flotila	flotila	k1gFnSc1	flotila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
hodností	hodnost	k1gFnSc7	hodnost
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
vojenských	vojenský	k2eAgFnPc2d1	vojenská
hodností	hodnost	k1gFnPc2	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
vojenské	vojenský	k2eAgFnPc1d1	vojenská
hodnosti	hodnost	k1gFnPc1	hodnost
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
čtrnáct	čtrnáct	k4xCc4	čtrnáct
hodnostních	hodnostní	k2eAgFnPc2d1	hodnostní
tříd	třída	k1gFnPc2	třída
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
pěchoty	pěchota	k1gFnSc2	pěchota
řadový	řadový	k2eAgMnSc1d1	řadový
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
u	u	k7c2	u
flotily	flotila	k1gFnSc2	flotila
námořník	námořník	k1gMnSc1	námořník
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soudnictví	soudnictví	k1gNnSc2	soudnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Základy	základ	k1gInPc1	základ
moderního	moderní	k2eAgNnSc2d1	moderní
soudnictví	soudnictví	k1gNnSc2	soudnictví
položil	položit	k5eAaPmAgInS	položit
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc1d1	veliký
svými	svůj	k3xOyFgMnPc7	svůj
reformami	reforma	k1gFnPc7	reforma
politické	politický	k2eAgFnSc2d1	politická
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
moderní	moderní	k2eAgFnSc1d1	moderní
reforma	reforma	k1gFnSc1	reforma
soudnictví	soudnictví	k1gNnSc2	soudnictví
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
reforma	reforma	k1gFnSc1	reforma
provedla	provést	k5eAaPmAgFnS	provést
několik	několik	k4yIc4	několik
významných	významný	k2eAgInPc2d1	významný
kroků	krok	k1gInPc2	krok
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
budování	budování	k1gNnSc3	budování
moderního	moderní	k2eAgInSc2d1	moderní
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Suděbnyj	Suděbnyj	k1gMnSc1	Suděbnyj
Ustav	ustavit	k5eAaPmRp2nS	ustavit
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
především	především	k6eAd1	především
zajistil	zajistit	k5eAaPmAgMnS	zajistit
oddělení	oddělení	k1gNnSc4	oddělení
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
správní	správní	k2eAgFnSc2d1	správní
a	a	k8xC	a
učinil	učinit	k5eAaImAgMnS	učinit
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
sféru	sféra	k1gFnSc4	sféra
výkonu	výkon	k1gInSc2	výkon
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
reformy	reforma	k1gFnSc2	reforma
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
nezávislost	nezávislost	k1gFnSc1	nezávislost
soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgNnSc4d1	veřejné
konání	konání	k1gNnSc4	konání
soudních	soudní	k2eAgNnPc2d1	soudní
líčení	líčení	k1gNnPc2	líčení
a	a	k8xC	a
především	především	k6eAd1	především
rovnost	rovnost	k1gFnSc1	rovnost
všech	všecek	k3xTgFnPc2	všecek
sociálních	sociální	k2eAgFnPc2d1	sociální
skupin	skupina	k1gFnPc2	skupina
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
soudnictví	soudnictví	k1gNnSc2	soudnictví
Alexandra	Alexandr	k1gMnSc2	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
britských	britský	k2eAgFnPc2d1	britská
a	a	k8xC	a
francouzských	francouzský	k2eAgFnPc2d1	francouzská
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zvláště	zvláště	k6eAd1	zvláště
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
inovaci	inovace	k1gFnSc4	inovace
lze	lze	k6eAd1	lze
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
rovnosti	rovnost	k1gFnSc2	rovnost
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
)	)	kIx)	)
považovat	považovat	k5eAaImF	považovat
především	především	k9	především
volitelnost	volitelnost	k1gFnSc1	volitelnost
soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tato	tento	k3xDgFnSc1	tento
reforma	reforma	k1gFnSc1	reforma
také	také	k9	také
zavedla	zavést	k5eAaPmAgFnS	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
struktura	struktura	k1gFnSc1	struktura
soudnictví	soudnictví	k1gNnSc2	soudnictví
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
standardizována	standardizován	k2eAgFnSc1d1	standardizována
<g/>
:	:	kIx,	:
každý	každý	k3xTgInSc1	každý
soudní	soudní	k2eAgInSc1d1	soudní
tribunál	tribunál	k1gInSc1	tribunál
měl	mít	k5eAaImAgInS	mít
svůj	svůj	k3xOyFgInSc4	svůj
odvolací	odvolací	k2eAgInSc4d1	odvolací
soud	soud	k1gInSc4	soud
a	a	k8xC	a
na	na	k7c6	na
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
stupni	stupeň	k1gInSc6	stupeň
potom	potom	k6eAd1	potom
stál	stát	k5eAaImAgInS	stát
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kasační	kasační	k2eAgInSc1d1	kasační
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Soudci	soudce	k1gMnPc1	soudce
v	v	k7c6	v
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
instanci	instance	k1gFnSc6	instance
byli	být	k5eAaImAgMnP	být
voleni	volit	k5eAaImNgMnP	volit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
soudci	soudce	k1gMnPc1	soudce
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
instancích	instance	k1gFnPc6	instance
byli	být	k5eAaImAgMnP	být
delegováni	delegován	k2eAgMnPc1d1	delegován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samospráva	samospráva	k1gFnSc1	samospráva
===	===	k?	===
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc4d1	lokální
samosprávu	samospráva	k1gFnSc4	samospráva
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
liberálních	liberální	k2eAgFnPc2d1	liberální
reforem	reforma	k1gFnPc2	reforma
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
zřízením	zřízení	k1gNnSc7	zřízení
místních	místní	k2eAgNnPc2d1	místní
zastupitelských	zastupitelský	k2eAgNnPc2d1	zastupitelské
shromáždění	shromáždění	k1gNnPc2	shromáždění
zemstev	zemstvo	k1gNnPc2	zemstvo
ve	v	k7c6	v
34	[number]	k4	34
evropských	evropský	k2eAgFnPc6d1	Evropská
guberniích	gubernie	k1gFnPc6	gubernie
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zemstev	zemstvo	k1gNnPc2	zemstvo
byli	být	k5eAaImAgMnP	být
voleni	volen	k2eAgMnPc1d1	volen
zástupci	zástupce	k1gMnPc1	zástupce
velkostatkářů	velkostatkář	k1gMnPc2	velkostatkář
<g/>
,	,	kIx,	,
měst	město	k1gNnPc2	město
a	a	k8xC	a
rolníků	rolník	k1gMnPc2	rolník
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
nerovného	rovný	k2eNgNnSc2d1	nerovné
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velkostatkářům	velkostatkář	k1gMnPc3	velkostatkář
byl	být	k5eAaImAgInS	být
garantován	garantován	k2eAgInSc4d1	garantován
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
místní	místní	k2eAgFnSc1d1	místní
samospráva	samospráva	k1gFnSc1	samospráva
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
organizována	organizovat	k5eAaBmNgFnS	organizovat
vzestupně	vzestupně	k6eAd1	vzestupně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejnižším	nízký	k2eAgInSc6d3	nejnižší
stupni	stupeň	k1gInSc6	stupeň
byla	být	k5eAaImAgFnS	být
venkovská	venkovský	k2eAgFnSc1d1	venkovská
samospráva	samospráva	k1gFnSc1	samospráva
miru	mir	k1gInSc2	mir
a	a	k8xC	a
volosti	volost	k1gFnSc2	volost
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zemstva	zemstvo	k1gNnSc2	zemstvo
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
stupně	stupeň	k1gInPc1	stupeň
byly	být	k5eAaImAgInP	být
vzájemně	vzájemně	k6eAd1	vzájemně
provázány	provázán	k2eAgInPc1d1	provázán
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
klíčový	klíčový	k2eAgInSc4d1	klíčový
vliv	vliv	k1gInSc4	vliv
měla	mít	k5eAaImAgFnS	mít
zemstva	zemstvo	k1gNnPc4	zemstvo
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
především	především	k6eAd1	především
místní	místní	k2eAgFnSc4d1	místní
školskou	školská	k1gFnSc4	školská
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
ale	ale	k8xC	ale
i	i	k9	i
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
politiku	politika	k1gFnSc4	politika
či	či	k8xC	či
poštu	pošta	k1gFnSc4	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
delegovaly	delegovat	k5eAaBmAgFnP	delegovat
vesnické	vesnický	k2eAgFnPc4d1	vesnická
komunity	komunita	k1gFnSc2	komunita
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
zemstva	zemstvo	k1gNnPc1	zemstvo
stala	stát	k5eAaPmAgNnP	stát
baštou	bašta	k1gFnSc7	bašta
ruských	ruský	k2eAgMnPc2d1	ruský
liberálů	liberál	k1gMnPc2	liberál
a	a	k8xC	a
konstitucionalistů	konstitucionalista	k1gMnPc2	konstitucionalista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byla	být	k5eAaImAgNnP	být
zemstva	zemstvo	k1gNnPc1	zemstvo
zvláště	zvláště	k6eAd1	zvláště
zainteresována	zainteresován	k2eAgNnPc1d1	zainteresováno
v	v	k7c6	v
uvádění	uvádění	k1gNnSc6	uvádění
agrární	agrární	k2eAgFnSc2d1	agrární
reformy	reforma	k1gFnSc2	reforma
Petra	Petra	k1gFnSc1	Petra
Stolypina	Stolypina	k1gFnSc1	Stolypina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
chtěla	chtít	k5eAaImAgFnS	chtít
podstatně	podstatně	k6eAd1	podstatně
modernizovat	modernizovat	k5eAaBmF	modernizovat
ruskou	ruský	k2eAgFnSc4d1	ruská
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
samospráva	samospráva	k1gFnSc1	samospráva
organizována	organizovat	k5eAaBmNgFnS	organizovat
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
jako	jako	k8xC	jako
zemstva	zemstvo	k1gNnSc2	zemstvo
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgInPc1d1	městský
zastupitelské	zastupitelský	k2eAgInPc1d1	zastupitelský
orgány	orgán	k1gInPc1	orgán
(	(	kIx(	(
<g/>
dumy	duma	k1gFnPc1	duma
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
složeny	složit	k5eAaPmNgFnP	složit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
</s>
</p>
<p>
<s>
volebního	volební	k2eAgInSc2d1	volební
censu	census	k1gInSc2	census
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
příslušela	příslušet	k5eAaImAgFnS	příslušet
ve	v	k7c6	v
městech	město	k1gNnPc6	město
starostovi	starosta	k1gMnSc3	starosta
a	a	k8xC	a
městské	městský	k2eAgFnSc3d1	městská
radě	rada	k1gFnSc3	rada
(	(	kIx(	(
<g/>
uprava	uprava	k1gFnSc1	uprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
byla	být	k5eAaImAgFnS	být
nicméně	nicméně	k8xC	nicméně
ruská	ruský	k2eAgFnSc1d1	ruská
samospráva	samospráva	k1gFnSc1	samospráva
omezena	omezit	k5eAaPmNgFnS	omezit
novými	nový	k2eAgFnPc7d1	nová
zákonnými	zákonný	k2eAgFnPc7d1	zákonná
úpravami	úprava	k1gFnPc7	úprava
vlády	vláda	k1gFnSc2	vláda
Alexandra	Alexandr	k1gMnSc2	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jiným	jiný	k2eAgInSc7d1	jiný
tyto	tento	k3xDgFnPc1	tento
úpravy	úprava	k1gFnPc1	úprava
znamenaly	znamenat	k5eAaImAgFnP	znamenat
podřízení	podřízení	k1gNnSc4	podřízení
zemstev	zemstvo	k1gNnPc2	zemstvo
a	a	k8xC	a
městských	městský	k2eAgFnPc2d1	městská
dum	duma	k1gFnPc2	duma
gubernátorovi	gubernátorův	k2eAgMnPc1d1	gubernátorův
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
přímo	přímo	k6eAd1	přímo
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
zemstva	zemstvo	k1gNnPc1	zemstvo
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
sověty	sovět	k1gInPc7	sovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společnost	společnost	k1gFnSc1	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
imperiální	imperiální	k2eAgInSc4d1	imperiální
stát	stát	k1gInSc4	stát
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
během	během	k7c2	během
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
sociálně	sociálně	k6eAd1	sociálně
stratifikovanou	stratifikovaný	k2eAgFnSc7d1	stratifikovaná
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
mnohonárodní	mnohonárodní	k2eAgFnSc1d1	mnohonárodní
a	a	k8xC	a
mnohokonfesionální	mnohokonfesionální	k2eAgFnSc1d1	mnohokonfesionální
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
měly	mít	k5eAaImAgFnP	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
proporcionálně	proporcionálně	k6eAd1	proporcionálně
nejmenší	malý	k2eAgFnPc1d3	nejmenší
skupiny	skupina	k1gFnPc1	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
složené	složený	k2eAgFnSc2d1	složená
ze	z	k7c2	z
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
byrokratů	byrokrat	k1gMnPc2	byrokrat
<g/>
,	,	kIx,	,
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
církevních	církevní	k2eAgMnPc2d1	církevní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
také	také	k9	také
bohatých	bohatý	k2eAgMnPc2d1	bohatý
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
skupinou	skupina	k1gFnSc7	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byli	být	k5eAaImAgMnP	být
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Rusko	Rusko	k1gNnSc1	Rusko
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
dominantně	dominantně	k6eAd1	dominantně
agrárním	agrární	k2eAgInSc7d1	agrární
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
pilíři	pilíř	k1gInPc7	pilíř
imperiální	imperiální	k2eAgFnSc2d1	imperiální
moci	moc	k1gFnSc2	moc
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
byli	být	k5eAaImAgMnP	být
tyto	tento	k3xDgFnPc4	tento
sociální	sociální	k2eAgFnPc4d1	sociální
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
aristokracie	aristokracie	k1gFnSc1	aristokracie
</s>
</p>
<p>
<s>
byrokracie	byrokracie	k1gFnSc1	byrokracie
</s>
</p>
<p>
<s>
armáda	armáda	k1gFnSc1	armáda
</s>
</p>
<p>
<s>
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Aristokracie	aristokracie	k1gFnSc2	aristokracie
===	===	k?	===
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
šlechta	šlechta	k1gFnSc1	šlechta
(	(	kIx(	(
<g/>
dvorjanstvo	dvorjanstvo	k1gNnSc1	dvorjanstvo
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
д	д	k?	д
<g/>
)	)	kIx)	)
odrážela	odrážet	k5eAaImAgFnS	odrážet
mnohonárodní	mnohonárodní	k2eAgFnSc4d1	mnohonárodní
podstatu	podstata	k1gFnSc4	podstata
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
centru	centrum	k1gNnSc6	centrum
stály	stát	k5eAaImAgFnP	stát
staré	starý	k2eAgFnPc1d1	stará
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
ruské	ruský	k2eAgFnPc1d1	ruská
šlechtické	šlechtický	k2eAgFnPc1d1	šlechtická
rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vedle	vedle	k7c2	vedle
nich	on	k3xPp3gInPc2	on
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
významné	významný	k2eAgFnPc1d1	významná
tatarské	tatarský	k2eAgFnPc1d1	tatarská
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnPc1d1	německá
<g/>
,	,	kIx,	,
švédské	švédský	k2eAgFnPc1d1	švédská
<g/>
,	,	kIx,	,
gruzínské	gruzínský	k2eAgFnPc1d1	gruzínská
a	a	k8xC	a
polské	polský	k2eAgFnPc1d1	polská
šlechtické	šlechtický	k2eAgFnPc1d1	šlechtická
rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
inkorporovány	inkorporovat	k5eAaBmNgFnP	inkorporovat
do	do	k7c2	do
ruské	ruský	k2eAgFnSc2d1	ruská
aristokracie	aristokracie	k1gFnSc2	aristokracie
a	a	k8xC	a
přijaty	přijmout	k5eAaPmNgFnP	přijmout
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
ruských	ruský	k2eAgMnPc2d1	ruský
gosudarů-imperátorů	gosudarůmperátor	k1gMnPc2	gosudarů-imperátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
proměnou	proměna	k1gFnSc7	proměna
prošla	projít	k5eAaPmAgFnS	projít
ruská	ruský	k2eAgFnSc1d1	ruská
šlechta	šlechta	k1gFnSc1	šlechta
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochopil	pochopit	k5eAaPmAgInS	pochopit
nutnost	nutnost	k1gFnSc4	nutnost
její	její	k3xOp3gFnSc2	její
westernizace	westernizace	k1gFnSc2	westernizace
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
šlechtu	šlechta	k1gFnSc4	šlechta
obměnil	obměnit	k5eAaPmAgMnS	obměnit
šlechtou	šlechta	k1gFnSc7	šlechta
novou	nový	k2eAgFnSc7d1	nová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
původu	původ	k1gInSc6	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
zásluhách	zásluha	k1gFnPc6	zásluha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předpetrské	předpetrský	k2eAgFnSc6d1	předpetrský
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
šlechta	šlechta	k1gFnSc1	šlechta
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
významným	významný	k2eAgMnSc7d1	významný
politických	politický	k2eAgMnPc2d1	politický
aktérem	aktér	k1gMnSc7	aktér
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
pevně	pevně	k6eAd1	pevně
svázána	svázat	k5eAaPmNgFnS	svázat
s	s	k7c7	s
carským	carský	k2eAgInSc7d1	carský
despotismem	despotismus	k1gInSc7	despotismus
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
kladl	klást	k5eAaImAgMnS	klást
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
provázání	provázání	k1gNnSc1	provázání
s	s	k7c7	s
efektivně	efektivně	k6eAd1	efektivně
spravovaným	spravovaný	k2eAgInSc7d1	spravovaný
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
závislost	závislost	k1gFnSc4	závislost
aristokracie	aristokracie	k1gFnSc2	aristokracie
na	na	k7c4	na
samoděržaví	samoděržaví	k1gNnSc4	samoděržaví
mu	on	k3xPp3gMnSc3	on
vyhovovala	vyhovovat	k5eAaImAgFnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
emancipaci	emancipace	k1gFnSc3	emancipace
ruské	ruský	k2eAgFnSc2d1	ruská
šlechty	šlechta	k1gFnSc2	šlechta
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Kateřiny	Kateřina	k1gFnSc2	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliké	veliký	k2eAgNnSc1d1	veliké
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1762	[number]	k4	1762
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
některá	některý	k3yIgNnPc4	některý
významná	významný	k2eAgNnPc4d1	významné
privilegia	privilegium	k1gNnPc4	privilegium
pro	pro	k7c4	pro
ruské	ruský	k2eAgNnSc4d1	ruské
dvorjanstvo	dvorjanstvo	k1gNnSc4	dvorjanstvo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
osvobození	osvobození	k1gNnSc1	osvobození
od	od	k7c2	od
povinné	povinný	k2eAgFnSc2d1	povinná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
platné	platný	k2eAgInPc1d1	platný
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
vycestovat	vycestovat	k5eAaPmF	vycestovat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
pasu	pas	k1gInSc2	pas
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
privilegium	privilegium	k1gNnSc1	privilegium
volného	volný	k2eAgInSc2d1	volný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stará	starý	k2eAgFnSc1d1	stará
šlechta	šlechta	k1gFnSc1	šlechta
(	(	kIx(	(
<g/>
potomci	potomek	k1gMnPc1	potomek
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
a	a	k8xC	a
starých	starý	k2eAgFnPc2d1	stará
bojarských	bojarský	k2eAgFnPc2d1	bojarská
rodin	rodina	k1gFnPc2	rodina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
titulární	titulární	k2eAgFnSc1d1	titulární
šlechta	šlechta	k1gFnSc1	šlechta
(	(	kIx(	(
<g/>
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
tituly	titul	k1gInPc7	titul
<g/>
:	:	kIx,	:
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dědičná	dědičný	k2eAgFnSc1d1	dědičná
šlechta	šlechta	k1gFnSc1	šlechta
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
šlechtické	šlechtický	k2eAgInPc1d1	šlechtický
tituly	titul	k1gInPc1	titul
dědily	dědit	k5eAaImAgInP	dědit
u	u	k7c2	u
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
osobní	osobní	k2eAgNnSc1d1	osobní
šlechtictví	šlechtictví	k1gNnSc1	šlechtictví
(	(	kIx(	(
<g/>
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
titul	titul	k1gInSc1	titul
byl	být	k5eAaImAgInS	být
udělen	udělit	k5eAaPmNgInS	udělit
pouze	pouze	k6eAd1	pouze
konkrétní	konkrétní	k2eAgFnSc3d1	konkrétní
osobě	osoba	k1gFnSc3	osoba
a	a	k8xC	a
nedědil	dědit	k5eNaImAgMnS	dědit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nemajetná	majetný	k2eNgFnSc1d1	nemajetná
šlechta	šlechta	k1gFnSc1	šlechta
(	(	kIx(	(
<g/>
šlechtictví	šlechtictví	k1gNnSc4	šlechtictví
získané	získaný	k2eAgNnSc4d1	získané
službou	služba	k1gFnSc7	služba
bez	bez	k7c2	bez
majetku	majetek	k1gInSc2	majetek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prošla	projít	k5eAaPmAgFnS	projít
ruská	ruský	k2eAgFnSc1d1	ruská
šlechta	šlechta	k1gFnSc1	šlechta
významnými	významný	k2eAgFnPc7d1	významná
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
nové	nový	k2eAgFnSc2d1	nová
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
službou	služba	k1gFnSc7	služba
a	a	k8xC	a
zásluhami	zásluha	k1gFnPc7	zásluha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
její	její	k3xOp3gFnPc4	její
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistických	statistický	k2eAgInPc2d1	statistický
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1850	[number]	k4	1850
až	až	k8xS	až
1900	[number]	k4	1900
počet	počet	k1gInSc1	počet
celoruské	celoruský	k2eAgFnSc2d1	celoruský
šlechty	šlechta	k1gFnSc2	šlechta
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
80	[number]	k4	80
%	%	kIx~	%
z	z	k7c2	z
1	[number]	k4	1
miliónu	milión	k4xCgInSc2	milión
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
1,8	[number]	k4	1,8
miliónu	milión	k4xCgInSc2	milión
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
přibližnou	přibližný	k2eAgFnSc4d1	přibližná
etnickou	etnický	k2eAgFnSc4d1	etnická
strukturu	struktura	k1gFnSc4	struktura
ruské	ruský	k2eAgFnSc2d1	ruská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
A.	A.	kA	A.
Kappelera	Kappeler	k1gMnSc2	Kappeler
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
40	[number]	k4	40
%	%	kIx~	%
šlechty	šlechta	k1gFnSc2	šlechta
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
šlechta	šlechta	k1gFnSc1	šlechta
tvořila	tvořit	k5eAaImAgFnS	tvořit
ne	ne	k9	ne
méně	málo	k6eAd2	málo
než	než	k8xS	než
29	[number]	k4	29
%	%	kIx~	%
(	(	kIx(	(
<g/>
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
byli	být	k5eAaImAgMnP	být
zahrnuti	zahrnut	k2eAgMnPc1d1	zahrnut
společně	společně	k6eAd1	společně
s	s	k7c7	s
běloruskou	běloruský	k2eAgFnSc7d1	Běloruská
a	a	k8xC	a
litevskou	litevský	k2eAgFnSc7d1	Litevská
šlechtou	šlechta	k1gFnSc7	šlechta
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
39	[number]	k4	39
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5,9	[number]	k4	5,9
%	%	kIx~	%
ruské	ruský	k2eAgFnSc2d1	ruská
šlechty	šlechta	k1gFnSc2	šlechta
byli	být	k5eAaImAgMnP	být
Gruzíni	Gruzín	k1gMnPc1	Gruzín
<g/>
,	,	kIx,	,
4,8	[number]	k4	4,8
%	%	kIx~	%
tatarská	tatarský	k2eAgFnSc1d1	tatarská
šlechta	šlechta	k1gFnSc1	šlechta
a	a	k8xC	a
kolem	kolem	k7c2	kolem
2	[number]	k4	2
%	%	kIx~	%
německá	německý	k2eAgFnSc1d1	německá
pobaltská	pobaltský	k2eAgFnSc1d1	pobaltská
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
<g/>
Ruská	ruský	k2eAgFnSc1d1	ruská
šlechta	šlechta	k1gFnSc1	šlechta
měla	mít	k5eAaImAgFnS	mít
samozřejmě	samozřejmě	k6eAd1	samozřejmě
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
politický	politický	k2eAgInSc4d1	politický
vývoj	vývoj	k1gInSc4	vývoj
i	i	k8xC	i
situaci	situace	k1gFnSc4	situace
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dalších	další	k2eAgInPc2d1	další
odhadů	odhad	k1gInPc2	odhad
bylo	být	k5eAaImAgNnS	být
71	[number]	k4	71
%	%	kIx~	%
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
nejvyšších	vysoký	k2eAgInPc6d3	Nejvyšší
postech	post	k1gInPc6	post
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Byrokracie	byrokracie	k1gFnSc1	byrokracie
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
===	===	k?	===
</s>
</p>
<p>
<s>
Významným	významný	k2eAgInSc7d1	významný
pilířem	pilíř	k1gInSc7	pilíř
ruského	ruský	k2eAgInSc2d1	ruský
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
sociální	sociální	k2eAgFnSc1d1	sociální
vrstva	vrstva	k1gFnSc1	vrstva
ruské	ruský	k2eAgFnSc2d1	ruská
byrokracie	byrokracie	k1gFnSc2	byrokracie
<g/>
.	.	kIx.	.
</s>
<s>
Představy	představa	k1gFnPc1	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
carské	carský	k2eAgNnSc1d1	carské
Rusko	Rusko	k1gNnSc1	Rusko
mělo	mít	k5eAaImAgNnS	mít
přebujelou	přebujelý	k2eAgFnSc4d1	přebujelá
byrokracii	byrokracie	k1gFnSc4	byrokracie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mylné	mylný	k2eAgFnPc1d1	mylná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
tvořila	tvořit	k5eAaImAgFnS	tvořit
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
pouhých	pouhý	k2eAgInPc2d1	pouhý
0,75	[number]	k4	0,75
%	%	kIx~	%
všeho	všecek	k3xTgNnSc2	všecek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Ruska	Ruska	k1gFnSc1	Ruska
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
ruské	ruský	k2eAgInPc1d1	ruský
moderní	moderní	k2eAgFnSc4d1	moderní
byrokracii	byrokracie	k1gFnSc4	byrokracie
nepoložil	položit	k5eNaPmAgMnS	položit
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
systematizoval	systematizovat	k5eAaBmAgMnS	systematizovat
a	a	k8xC	a
reorganizoval	reorganizovat	k5eAaBmAgMnS	reorganizovat
celou	celý	k2eAgFnSc4d1	celá
státní	státní	k2eAgFnSc4d1	státní
správu	správa	k1gFnSc4	správa
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
instituci	instituce	k1gFnSc4	instituce
státního	státní	k2eAgMnSc2d1	státní
úředníka	úředník	k1gMnSc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
hodností	hodnost	k1gFnPc2	hodnost
(	(	kIx(	(
<g/>
1722	[number]	k4	1722
<g/>
)	)	kIx)	)
potom	potom	k6eAd1	potom
přesně	přesně	k6eAd1	přesně
stratifikovala	stratifikovat	k5eAaBmAgFnS	stratifikovat
ruskou	ruský	k2eAgFnSc7d1	ruská
byrokracií	byrokracie	k1gFnSc7	byrokracie
do	do	k7c2	do
celkem	celkem	k6eAd1	celkem
14	[number]	k4	14
hodnostních	hodnostní	k2eAgFnPc2d1	hodnostní
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Orlando	Orlando	k6eAd1	Orlando
Figges	Figges	k1gInSc1	Figges
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
carskému	carský	k2eAgNnSc3d1	carské
Rusku	Rusko	k1gNnSc3	Rusko
rozhodně	rozhodně	k6eAd1	rozhodně
nechyběla	chybět	k5eNaImAgFnS	chybět
profesionální	profesionální	k2eAgFnSc1d1	profesionální
a	a	k8xC	a
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
byrokracie	byrokracie	k1gFnSc1	byrokracie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInSc1	její
problém	problém	k1gInSc1	problém
spočíval	spočívat	k5eAaImAgInS	spočívat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruská	ruský	k2eAgFnSc1d1	ruská
byrokracie	byrokracie	k1gFnSc1	byrokracie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
smyslu	smysl	k1gInSc6	smysl
hybridem	hybrid	k1gInSc7	hybrid
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
smísila	smísit	k5eAaPmAgFnS	smísit
tradice	tradice	k1gFnSc1	tradice
ruského	ruský	k2eAgInSc2d1	ruský
patrimonialismu	patrimonialismus	k1gInSc2	patrimonialismus
s	s	k7c7	s
pruským	pruský	k2eAgInSc7d1	pruský
systémem	systém	k1gInSc7	systém
racionalizované	racionalizovaný	k2eAgFnSc2d1	racionalizovaná
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruská	ruský	k2eAgFnSc1d1	ruská
byrokracie	byrokracie	k1gFnSc1	byrokracie
často	často	k6eAd1	často
a	a	k8xC	a
jednoduše	jednoduše	k6eAd1	jednoduše
podléhala	podléhat	k5eAaImAgFnS	podléhat
blahovůli	blahovůle	k1gFnSc4	blahovůle
carského	carský	k2eAgInSc2d1	carský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
postrádala	postrádat	k5eAaImAgFnS	postrádat
některé	některý	k3yIgFnPc4	některý
hlavní	hlavní	k2eAgFnPc4d1	hlavní
charakteristiky	charakteristika	k1gFnPc4	charakteristika
moderní	moderní	k2eAgFnSc2d1	moderní
byrokracie	byrokracie	k1gFnSc2	byrokracie
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
řádné	řádný	k2eAgFnPc1d1	řádná
procedury	procedura	k1gFnPc1	procedura
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
stanovené	stanovený	k2eAgInPc1d1	stanovený
institucionální	institucionální	k2eAgInPc1d1	institucionální
vztahy	vztah	k1gInPc1	vztah
nebo	nebo	k8xC	nebo
řádné	řádný	k2eAgInPc1d1	řádný
právní	právní	k2eAgInPc1d1	právní
postupy	postup	k1gInPc1	postup
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
problémem	problém	k1gInSc7	problém
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
byrokracie	byrokracie	k1gFnSc1	byrokracie
neměla	mít	k5eNaImAgFnS	mít
pevně	pevně	k6eAd1	pevně
zakotvené	zakotvený	k2eAgNnSc4d1	zakotvené
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
regionech	region	k1gInPc6	region
sehrávala	sehrávat	k5eAaImAgFnS	sehrávat
administrativní	administrativní	k2eAgFnSc4d1	administrativní
roli	role	k1gFnSc4	role
ruská	ruský	k2eAgFnSc1d1	ruská
carská	carský	k2eAgFnSc1d1	carská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
<g/>
Armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
pozemní	pozemní	k2eAgFnSc1d1	pozemní
i	i	k8xC	i
flotila	flotila	k1gFnSc1	flotila
<g/>
)	)	kIx)	)
tvořila	tvořit	k5eAaImAgFnS	tvořit
poslední	poslední	k2eAgFnSc4d1	poslední
sekulární	sekulární	k2eAgFnSc4d1	sekulární
skupinu	skupina	k1gFnSc4	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Ruska	Rusko	k1gNnSc2	Rusko
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
politický	politický	k2eAgInSc4d1	politický
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
situaci	situace	k1gFnSc4	situace
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
byl	být	k5eAaImAgInS	být
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc4d2	veliký
čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
Rusko	Rusko	k1gNnSc1	Rusko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1721	[number]	k4	1721
expandovalo	expandovat	k5eAaImAgNnS	expandovat
a	a	k8xC	a
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
své	svůj	k3xOyFgNnSc1	svůj
teritorium	teritorium	k1gNnSc1	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
měla	mít	k5eAaImAgFnS	mít
armáda	armáda	k1gFnSc1	armáda
neopomenutelné	opomenutelný	k2eNgFnSc2d1	neopomenutelná
represivní	represivní	k2eAgFnSc2d1	represivní
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tvořili	tvořit	k5eAaImAgMnP	tvořit
vojáci	voják	k1gMnPc1	voják
celkem	celkem	k6eAd1	celkem
1	[number]	k4	1
%	%	kIx~	%
všeho	všecek	k3xTgNnSc2	všecek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Ruska	Ruska	k1gFnSc1	Ruska
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
byrokracie	byrokracie	k1gFnSc1	byrokracie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
armáda	armáda	k1gFnSc1	armáda
mnohonárodní	mnohonárodní	k2eAgFnSc1d1	mnohonárodní
sociální	sociální	k2eAgFnSc7d1	sociální
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
vojáci	voják	k1gMnPc1	voják
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
konců	konec	k1gInPc2	konec
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
posty	post	k1gInPc4	post
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
ruské	ruský	k2eAgFnSc2d1	ruská
aristokracie	aristokracie	k1gFnSc2	aristokracie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
monarchii	monarchie	k1gFnSc4	monarchie
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Prusko	Prusko	k1gNnSc1	Prusko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
[	[	kIx(	[
<g/>
-Uhersko	-Uhersko	k1gNnSc1	-Uhersko
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c6	na
nejvyšších	vysoký	k2eAgInPc6d3	Nejvyšší
stupních	stupeň	k1gInPc6	stupeň
ruské	ruský	k2eAgFnSc2d1	ruská
vládního	vládní	k2eAgInSc2d1	vládní
systému	systém	k1gInSc2	systém
k	k	k7c3	k
mísení	mísení	k1gNnSc3	mísení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
a	a	k8xC	a
civilních	civilní	k2eAgFnPc2d1	civilní
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
na	na	k7c6	na
úrovní	úroveň	k1gFnSc7	úroveň
gubernátorů	gubernátor	k1gMnPc2	gubernátor
<g/>
,	,	kIx,	,
ministrů	ministr	k1gMnPc2	ministr
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
===	===	k?	===
</s>
</p>
<p>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
měly	mít	k5eAaImAgInP	mít
v	v	k7c6	v
carském	carský	k2eAgNnSc6d1	carské
Rusku	Rusko	k1gNnSc6	Rusko
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
státního	státní	k2eAgNnSc2d1	státní
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
dávalo	dávat	k5eAaImAgNnS	dávat
moci	moc	k1gFnSc2	moc
ruských	ruský	k2eAgMnPc2d1	ruský
autokratů	autokrat	k1gMnPc2	autokrat
a	a	k8xC	a
celému	celý	k2eAgInSc3d1	celý
Ruskému	ruský	k2eAgInSc3d1	ruský
státu	stát	k1gInSc3	stát
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
dimenzi	dimenze	k1gFnSc4	dimenze
a	a	k8xC	a
ideologické	ideologický	k2eAgNnSc4d1	ideologické
ospravedlnění	ospravedlnění	k1gNnSc4	ospravedlnění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
role	role	k1gFnSc2	role
této	tento	k3xDgFnSc2	tento
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
podívat	podívat	k5eAaImF	podívat
se	se	k3xPyFc4	se
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
ruských	ruský	k2eAgFnPc2d1	ruská
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
zmínit	zmínit	k5eAaPmF	zmínit
vztah	vztah	k1gInSc4	vztah
státu	stát	k1gInSc2	stát
a	a	k8xC	a
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
době	doba	k1gFnSc6	doba
starších	starý	k2eAgFnPc2d2	starší
ruských	ruský	k2eAgFnPc2d1	ruská
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
zejména	zejména	k9	zejména
instituci	instituce	k1gFnSc4	instituce
caesaropapismu	caesaropapismus	k1gInSc2	caesaropapismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
západního	západní	k2eAgInSc2d1	západní
katolicismu	katolicismus	k1gInSc2	katolicismus
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neoddělila	oddělit	k5eNaPmAgFnS	oddělit
od	od	k7c2	od
světské	světský	k2eAgFnSc2d1	světská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přímo	přímo	k6eAd1	přímo
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
sekulární	sekulární	k2eAgFnSc7d1	sekulární
mocí	moc	k1gFnSc7	moc
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
vládce	vládce	k1gMnSc1	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prvek	prvek	k1gInSc1	prvek
plně	plně	k6eAd1	plně
využil	využít	k5eAaPmAgInS	využít
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
církev	církev	k1gFnSc4	církev
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
svázal	svázat	k5eAaPmAgMnS	svázat
s	s	k7c7	s
ruským	ruský	k2eAgInSc7d1	ruský
státem	stát	k1gInSc7	stát
zřízením	zřízení	k1gNnSc7	zřízení
Nejsvětějšího	nejsvětější	k2eAgInSc2d1	nejsvětější
synodu	synod	k1gInSc2	synod
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
oberprokurátora	oberprokurátor	k1gMnSc2	oberprokurátor
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
instituci	instituce	k1gFnSc6	instituce
zasedali	zasedat	k5eAaImAgMnP	zasedat
církevní	církevní	k2eAgMnPc1d1	církevní
představitelé	představitel	k1gMnPc1	představitel
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
představitelé	představitel	k1gMnPc1	představitel
světské	světský	k2eAgFnSc2d1	světská
moci	moc	k1gFnSc2	moc
státu	stát	k1gInSc2	stát
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1897	[number]	k4	1897
tvořili	tvořit	k5eAaImAgMnP	tvořit
členové	člen	k1gMnPc1	člen
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
asi	asi	k9	asi
kolem	kolem	k7c2	kolem
65	[number]	k4	65
%	%	kIx~	%
všeho	všecek	k3xTgNnSc2	všecek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Ruska	Ruska	k1gFnSc1	Ruska
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
měla	mít	k5eAaImAgFnS	mít
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
zastávala	zastávat	k5eAaImAgFnS	zastávat
spíše	spíše	k9	spíše
konzervativní	konzervativní	k2eAgInPc4d1	konzervativní
postoje	postoj	k1gInPc4	postoj
vůči	vůči	k7c3	vůči
modernizaci	modernizace	k1gFnSc3	modernizace
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
její	její	k3xOp3gNnSc4	její
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
v	v	k7c6	v
praktické	praktický	k2eAgFnSc6d1	praktická
politice	politika	k1gFnSc6	politika
během	během	k7c2	během
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
viditelně	viditelně	k6eAd1	viditelně
upadalo	upadat	k5eAaPmAgNnS	upadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
inteligence	inteligence	k1gFnSc2	inteligence
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
mělo	mít	k5eAaImAgNnS	mít
Rusko	Rusko	k1gNnSc1	Rusko
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stále	stále	k6eAd1	stále
úzkou	úzký	k2eAgFnSc4d1	úzká
skupinu	skupina	k1gFnSc4	skupina
středních	střední	k2eAgFnPc2d1	střední
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
tvořily	tvořit	k5eAaImAgFnP	tvořit
ruské	ruský	k2eAgFnPc1d1	ruská
střední	střední	k2eAgFnPc1d1	střední
třídy	třída	k1gFnPc1	třída
kolem	kolem	k7c2	kolem
12	[number]	k4	12
%	%	kIx~	%
všeho	všecek	k3xTgNnSc2	všecek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
byla	být	k5eAaImAgFnS	být
podstatně	podstatně	k6eAd1	podstatně
diferencovaná	diferencovaný	k2eAgFnSc1d1	diferencovaná
a	a	k8xC	a
zahrnout	zahrnout	k5eAaPmF	zahrnout
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
dají	dát	k5eAaPmIp3nP	dát
podnikatelé	podnikatel	k1gMnPc1	podnikatel
a	a	k8xC	a
rentiéři	rentiér	k1gMnPc1	rentiér
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
svobodná	svobodný	k2eAgNnPc1d1	svobodné
povolání	povolání	k1gNnPc1	povolání
<g/>
,	,	kIx,	,
inteligence	inteligence	k1gFnPc1	inteligence
a	a	k8xC	a
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
nebo	nebo	k8xC	nebo
středních	střední	k2eAgFnPc2d1	střední
vrstev	vrstva	k1gFnPc2	vrstva
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
projevů	projev	k1gInPc2	projev
ruské	ruský	k2eAgFnSc2d1	ruská
zaostalosti	zaostalost	k1gFnSc2	zaostalost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
právě	právě	k9	právě
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
výrazné	výrazný	k2eAgFnSc2d1	výrazná
proměny	proměna	k1gFnSc2	proměna
a	a	k8xC	a
početní	početní	k2eAgInSc1d1	početní
nárůst	nárůst	k1gInSc1	nárůst
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
podnikatelů	podnikatel	k1gMnPc2	podnikatel
a	a	k8xC	a
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
246	[number]	k4	246
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
podnikatelských	podnikatelský	k2eAgFnPc2d1	podnikatelská
a	a	k8xC	a
obchodnických	obchodnický	k2eAgFnPc2d1	obchodnická
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
buržoazie	buržoazie	k1gFnSc1	buržoazie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
trojnásobné	trojnásobný	k2eAgNnSc4d1	trojnásobné
číslo	číslo	k1gNnSc4	číslo
–	–	k?	–
600	[number]	k4	600
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
ruské	ruský	k2eAgFnSc2d1	ruská
buržoazie	buržoazie	k1gFnSc2	buržoazie
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
nejen	nejen	k6eAd1	nejen
její	její	k3xOp3gFnSc4	její
relativní	relativní	k2eAgFnSc4d1	relativní
nepočetnost	nepočetnost	k1gFnSc4	nepočetnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
malý	malý	k2eAgInSc4d1	malý
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
konzervativní	konzervativní	k2eAgInPc4d1	konzervativní
a	a	k8xC	a
antiliberální	antiliberální	k2eAgInPc4d1	antiliberální
názory	názor	k1gInPc4	názor
či	či	k8xC	či
negativní	negativní	k2eAgInPc4d1	negativní
postoje	postoj	k1gInPc4	postoj
k	k	k7c3	k
technologickým	technologický	k2eAgFnPc3d1	technologická
inovacím	inovace	k1gFnPc3	inovace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
začala	začít	k5eAaPmAgFnS	začít
dramaticky	dramaticky	k6eAd1	dramaticky
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vlivné	vlivný	k2eAgMnPc4d1	vlivný
politiky	politik	k1gMnPc4	politik
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
okťabristé	okťabrista	k1gMnPc1	okťabrista
Alexandr	Alexandr	k1gMnSc1	Alexandr
Gučkov	Gučkov	k1gInSc1	Gučkov
a	a	k8xC	a
Alexandr	Alexandr	k1gMnSc1	Alexandr
Protopopov	Protopopov	k1gInSc1	Protopopov
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
pocházející	pocházející	k2eAgInPc1d1	pocházející
z	z	k7c2	z
bohatých	bohatý	k2eAgFnPc2d1	bohatá
podnikatelských	podnikatelský	k2eAgFnPc2d1	podnikatelská
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
inteligence	inteligence	k1gFnSc1	inteligence
resp.	resp.	kA	resp.
intelektuální	intelektuální	k2eAgFnSc1d1	intelektuální
elita	elita	k1gFnSc1	elita
(	(	kIx(	(
<g/>
inteligencia	inteligencia	k1gFnSc1	inteligencia
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
и	и	k?	и
<g/>
)	)	kIx)	)
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejdramatičtějších	dramatický	k2eAgInPc2d3	nejdramatičtější
nárůstů	nárůst	k1gInPc2	nárůst
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
k	k	k7c3	k
ní	on	k3xPp3gFnSc6	on
patřilo	patřit	k5eAaImAgNnS	patřit
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
400	[number]	k4	400
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
odhady	odhad	k1gInPc1	odhad
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
dynamické	dynamický	k2eAgFnPc4d1	dynamická
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
docházelo	docházet	k5eAaImAgNnS	docházet
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
i	i	k9	i
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
carismu	carismus	k1gInSc2	carismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zmínit	zmínit	k5eAaPmF	zmínit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
sociální	sociální	k2eAgInSc4d1	sociální
původ	původ	k1gInSc4	původ
členů	člen	k1gMnPc2	člen
inteligence	inteligence	k1gFnSc2	inteligence
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
různorodý	různorodý	k2eAgInSc1d1	různorodý
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
členů	člen	k1gInPc2	člen
ruské	ruský	k2eAgFnSc2d1	ruská
aristokracie	aristokracie	k1gFnSc2	aristokracie
nebo	nebo	k8xC	nebo
středních	střední	k2eAgFnPc2d1	střední
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
městských	městský	k2eAgFnPc2d1	městská
elit	elita	k1gFnPc2	elita
<g/>
,	,	kIx,	,
podnikatelů	podnikatel	k1gMnPc2	podnikatel
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inteligence	inteligence	k1gFnSc1	inteligence
patřila	patřit	k5eAaImAgFnS	patřit
často	často	k6eAd1	často
k	k	k7c3	k
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
kritiků	kritik	k1gMnPc2	kritik
konzervativního	konzervativní	k2eAgInSc2d1	konzervativní
režimu	režim	k1gInSc2	režim
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rolnictvo	rolnictvo	k1gNnSc1	rolnictvo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
tvořili	tvořit	k5eAaImAgMnP	tvořit
77,1	[number]	k4	77,1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Ruska	Rusko	k1gNnSc2	Rusko
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzovalo	potvrzovat	k5eAaImAgNnS	potvrzovat
postavení	postavení	k1gNnSc4	postavení
Ruska	Rusko	k1gNnSc2	Rusko
jako	jako	k8xS	jako
agrárního	agrární	k2eAgInSc2d1	agrární
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
obrovská	obrovský	k2eAgFnSc1d1	obrovská
masa	masa	k1gFnSc1	masa
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
nebyla	být	k5eNaImAgFnS	být
zcela	zcela	k6eAd1	zcela
homogenní	homogenní	k2eAgFnSc1d1	homogenní
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
oblastech	oblast	k1gFnPc6	oblast
Ruska	Rusko	k1gNnSc2	Rusko
neexistovalo	existovat	k5eNaImAgNnS	existovat
nevolnictví	nevolnictví	k1gNnSc4	nevolnictví
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
zrušením	zrušení	k1gNnSc7	zrušení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
tamní	tamní	k2eAgMnPc1d1	tamní
rolníci	rolník	k1gMnPc1	rolník
hospodařili	hospodařit	k5eAaImAgMnP	hospodařit
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
zde	zde	k6eAd1	zde
obščina	obščina	k1gFnSc1	obščina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Rusku	Rusko	k1gNnSc6	Rusko
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
rolnictvo	rolnictvo	k1gNnSc1	rolnictvo
bylo	být	k5eAaImAgNnS	být
hluboce	hluboko	k6eAd1	hluboko
zakořeněné	zakořeněný	k2eAgNnSc1d1	zakořeněné
v	v	k7c6	v
nevolnickém	nevolnický	k2eAgInSc6d1	nevolnický
systému	systém	k1gInSc6	systém
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
se	se	k3xPyFc4	se
přizpůsobovalo	přizpůsobovat	k5eAaImAgNnS	přizpůsobovat
požadavkům	požadavek	k1gInPc3	požadavek
doby	doba	k1gFnSc2	doba
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
flexibilitu	flexibilita	k1gFnSc4	flexibilita
a	a	k8xC	a
inovace	inovace	k1gFnPc4	inovace
i	i	k9	i
v	v	k7c6	v
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
dominovala	dominovat	k5eAaImAgFnS	dominovat
obščina	obščina	k1gFnSc1	obščina
a	a	k8xC	a
postoje	postoj	k1gInPc1	postoj
k	k	k7c3	k
soukromému	soukromý	k2eAgNnSc3d1	soukromé
vlastnictví	vlastnictví	k1gNnSc3	vlastnictví
půdy	půda	k1gFnSc2	půda
byly	být	k5eAaImAgInP	být
spíše	spíše	k9	spíše
negativní	negativní	k2eAgInPc1d1	negativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masa	masa	k1gFnSc1	masa
rolnictva	rolnictvo	k1gNnSc2	rolnictvo
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
lidským	lidský	k2eAgInSc7d1	lidský
zdrojem	zdroj	k1gInSc7	zdroj
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
byl	být	k5eAaImAgInS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
První	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
plně	plně	k6eAd1	plně
svázána	svázat	k5eAaPmNgFnS	svázat
s	s	k7c7	s
rolnictvem	rolnictvo	k1gNnSc7	rolnictvo
<g/>
.	.	kIx.	.
16-20	[number]	k4	16-20
%	%	kIx~	%
ruského	ruský	k2eAgInSc2d1	ruský
národního	národní	k2eAgInSc2d1	národní
produktu	produkt	k1gInSc2	produkt
představoval	představovat	k5eAaImAgInS	představovat
export	export	k1gInSc1	export
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
tohoto	tento	k3xDgNnSc2	tento
obilí	obilí	k1gNnSc2	obilí
pocházely	pocházet	k5eAaImAgFnP	pocházet
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
ruského	ruský	k2eAgNnSc2d1	ruské
rolnictva	rolnictvo	k1gNnSc2	rolnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
z	z	k7c2	z
rolnictva	rolnictvo	k1gNnSc2	rolnictvo
se	se	k3xPyFc4	se
také	také	k9	také
rekrutovali	rekrutovat	k5eAaImAgMnP	rekrutovat
zemědělští	zemědělský	k2eAgMnPc1d1	zemědělský
a	a	k8xC	a
průmysloví	průmyslový	k2eAgMnPc1d1	průmyslový
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
počty	počet	k1gInPc1	počet
se	se	k3xPyFc4	se
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dělnictvo	dělnictvo	k1gNnSc1	dělnictvo
===	===	k?	===
</s>
</p>
<p>
<s>
Industrializace	industrializace	k1gFnPc1	industrializace
a	a	k8xC	a
společenské	společenský	k2eAgFnPc1d1	společenská
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
přinášela	přinášet	k5eAaImAgFnS	přinášet
<g/>
,	,	kIx,	,
stály	stát	k5eAaImAgFnP	stát
za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
nové	nový	k2eAgFnSc2d1	nová
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
–	–	k?	–
dělnictva	dělnictvo	k1gNnSc2	dělnictvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rekrutovalo	rekrutovat	k5eAaImAgNnS	rekrutovat
především	především	k9	především
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
rolnického	rolnický	k2eAgNnSc2d1	rolnické
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
se	se	k3xPyFc4	se
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
zemědělském	zemědělský	k2eAgInSc6d1	zemědělský
sektoru	sektor	k1gInSc6	sektor
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
závislých	závislý	k2eAgMnPc2d1	závislý
na	na	k7c6	na
mzdě	mzda	k1gFnSc6	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistických	statistický	k2eAgInPc2d1	statistický
odhadů	odhad	k1gInPc2	odhad
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ruska	Rusko	k1gNnSc2	Rusko
závislých	závislý	k2eAgNnPc2d1	závislé
na	na	k7c6	na
mzdě	mzda	k1gFnSc6	mzda
–	–	k?	–
tedy	tedy	k8xC	tedy
zemědělských	zemědělský	k2eAgMnPc2d1	zemědělský
a	a	k8xC	a
průmyslových	průmyslový	k2eAgMnPc2d1	průmyslový
dělníků	dělník	k1gMnPc2	dělník
–	–	k?	–
ze	z	k7c2	z
4	[number]	k4	4
miliónů	milión	k4xCgInPc2	milión
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
na	na	k7c4	na
17,4	[number]	k4	17,4
milióny	milión	k4xCgInPc4	milión
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
kolem	kolem	k6eAd1	kolem
asi	asi	k9	asi
12,8	[number]	k4	12,8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
bylo	být	k5eAaImAgNnS	být
číslo	číslo	k1gNnSc1	číslo
relativně	relativně	k6eAd1	relativně
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
sledované	sledovaný	k2eAgFnSc2d1	sledovaná
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
asi	asi	k9	asi
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
ruského	ruský	k2eAgNnSc2d1	ruské
dělnictva	dělnictvo	k1gNnSc2	dělnictvo
ještě	ještě	k9	ještě
úzce	úzko	k6eAd1	úzko
svázána	svázat	k5eAaPmNgFnS	svázat
s	s	k7c7	s
venkovem	venkov	k1gInSc7	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
námezdní	námezdní	k2eAgMnPc4d1	námezdní
sezónní	sezónní	k2eAgMnPc4d1	sezónní
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sezóny	sezóna	k1gFnSc2	sezóna
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
jako	jako	k8xC	jako
rolníci	rolník	k1gMnPc1	rolník
a	a	k8xC	a
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
nechali	nechat	k5eAaPmAgMnP	nechat
najímat	najímat	k5eAaImF	najímat
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
měnit	měnit	k5eAaImF	měnit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
etablovala	etablovat	k5eAaBmAgFnS	etablovat
skupina	skupina	k1gFnSc1	skupina
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
městských	městský	k2eAgMnPc2d1	městský
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
identifikovali	identifikovat	k5eAaBmAgMnP	identifikovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sociální	sociální	k2eAgFnSc7d1	sociální
skupinou	skupina	k1gFnSc7	skupina
a	a	k8xC	a
ztratili	ztratit	k5eAaPmAgMnP	ztratit
vazby	vazba	k1gFnPc4	vazba
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
skutečností	skutečnost	k1gFnSc7	skutečnost
souviselo	souviset	k5eAaImAgNnS	souviset
také	také	k9	také
zformování	zformování	k1gNnSc1	zformování
prvního	první	k4xOgNnSc2	první
dělnického	dělnický	k2eAgNnSc2d1	dělnické
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
sociálně-demokratické	sociálněemokratický	k2eAgFnSc2d1	sociálně-demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
ilegální	ilegální	k2eAgInPc1d1	ilegální
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
Dumě	duma	k1gFnSc6	duma
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
třetí	třetí	k4xOgMnSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
až	až	k9	až
nové	nový	k2eAgFnSc2d1	nová
volební	volební	k2eAgFnSc2d1	volební
úpravy	úprava	k1gFnSc2	úprava
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
snížili	snížit	k5eAaPmAgMnP	snížit
na	na	k7c4	na
14	[number]	k4	14
poslanců	poslanec	k1gMnPc2	poslanec
ve	v	k7c4	v
třetí	třetí	k4xOgNnSc4	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgFnSc3	čtvrtý
Dumě	duma	k1gFnSc3	duma
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nový	nový	k2eAgInSc1d1	nový
trend	trend	k1gInSc1	trend
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
na	na	k7c4	na
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
zpolitizování	zpolitizování	k1gNnSc4	zpolitizování
alespoň	alespoň	k9	alespoň
části	část	k1gFnSc3	část
dělnického	dělnický	k2eAgNnSc2d1	dělnické
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
měst	město	k1gNnPc2	město
a	a	k8xC	a
korespondoval	korespondovat	k5eAaImAgInS	korespondovat
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kultura	kultura	k1gFnSc1	kultura
imperiálního	imperiální	k2eAgNnSc2d1	imperiální
období	období	k1gNnSc2	období
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
období	období	k1gNnPc2	období
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ruská	ruský	k2eAgFnSc1d1	ruská
kultura	kultura	k1gFnSc1	kultura
dostávala	dostávat	k5eAaImAgFnS	dostávat
do	do	k7c2	do
stále	stále	k6eAd1	stále
intenzívnějších	intenzívní	k2eAgInPc2d2	intenzívnější
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
či	či	k8xC	či
západní	západní	k2eAgFnSc7d1	západní
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Modernizační	modernizační	k2eAgFnPc1d1	modernizační
linie	linie	k1gFnPc1	linie
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
pomyslnému	pomyslný	k2eAgNnSc3d1	pomyslné
"	"	kIx"	"
<g/>
otevření	otevření	k1gNnSc1	otevření
oken	okno	k1gNnPc2	okno
<g/>
"	"	kIx"	"
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
projevovalo	projevovat	k5eAaImAgNnS	projevovat
především	především	k9	především
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
elit	elita	k1gFnPc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ruské	ruský	k2eAgFnPc1d1	ruská
elity	elita	k1gFnPc1	elita
(	(	kIx(	(
<g/>
především	především	k9	především
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
,	,	kIx,	,
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
)	)	kIx)	)
staly	stát	k5eAaPmAgFnP	stát
frankofonní	frankofonní	k2eAgFnPc1d1	frankofonní
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
četba	četba	k1gFnSc1	četba
osvícenské	osvícenský	k2eAgFnSc2d1	osvícenská
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
například	například	k6eAd1	například
carevna	carevna	k1gFnSc1	carevna
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliká	veliký	k2eAgFnSc1d1	veliká
si	se	k3xPyFc3	se
osobně	osobně	k6eAd1	osobně
dopisovala	dopisovat	k5eAaImAgFnS	dopisovat
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
filosofem	filosof	k1gMnSc7	filosof
osvícenství	osvícenství	k1gNnSc2	osvícenství
Voltairem	Voltairma	k1gFnPc2	Voltairma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
prozápadní	prozápadní	k2eAgFnSc1d1	prozápadní
orientace	orientace	k1gFnSc1	orientace
elit	elita	k1gFnPc2	elita
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
autochtonním	autochtonní	k2eAgInSc7d1	autochtonní
vývojem	vývoj	k1gInSc7	vývoj
ruské	ruský	k2eAgFnSc2d1	ruská
lidové	lidový	k2eAgFnSc2d1	lidová
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
směsí	směs	k1gFnSc7	směs
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
<g/>
,	,	kIx,	,
skandinávských	skandinávský	k2eAgFnPc2d1	skandinávská
<g/>
,	,	kIx,	,
finských	finský	k2eAgFnPc2d1	finská
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
asijských	asijský	k2eAgInPc2d1	asijský
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
či	či	k8xC	či
pohanských	pohanský	k2eAgInPc2d1	pohanský
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
tento	tento	k3xDgInSc1	tento
rozpor	rozpor	k1gInSc1	rozpor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ústředním	ústřední	k2eAgInSc7d1	ústřední
bodem	bod	k1gInSc7	bod
diskuzí	diskuze	k1gFnPc2	diskuze
o	o	k7c4	o
chápání	chápání	k1gNnSc4	chápání
a	a	k8xC	a
pojímání	pojímání	k1gNnSc4	pojímání
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
zformovala	zformovat	k5eAaPmAgFnS	zformovat
skupina	skupina	k1gFnSc1	skupina
tzv.	tzv.	kA	tzv.
zapadniků	zapadnik	k1gMnPc2	zapadnik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
viděli	vidět	k5eAaImAgMnP	vidět
Ruska	Rusko	k1gNnSc2	Rusko
jako	jako	k8xS	jako
nedílnou	dílný	k2eNgFnSc4d1	nedílná
součást	součást	k1gFnSc4	součást
evropského	evropský	k2eAgInSc2d1	evropský
<g/>
,	,	kIx,	,
civilizovaného	civilizovaný	k2eAgInSc2d1	civilizovaný
světa	svět	k1gInSc2	svět
a	a	k8xC	a
přihlašovali	přihlašovat	k5eAaImAgMnP	přihlašovat
se	se	k3xPyFc4	se
k	k	k7c3	k
tradici	tradice	k1gFnSc3	tradice
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
skupina	skupina	k1gFnSc1	skupina
tzv.	tzv.	kA	tzv.
slavjanofilů	slavjanofil	k1gMnPc2	slavjanofil
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chápali	chápat	k5eAaImAgMnP	chápat
Rusko	Rusko	k1gNnSc4	Rusko
jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
kulturně-politický	kulturněolitický	k2eAgInSc4d1	kulturně-politický
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
euroasijské	euroasijský	k2eAgFnSc2d1	euroasijská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
stavěli	stavět	k5eAaImAgMnP	stavět
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
kriticky	kriticky	k6eAd1	kriticky
k	k	k7c3	k
západnímu	západní	k2eAgNnSc3d1	západní
pojetí	pojetí	k1gNnSc3	pojetí
Ruska	Rusko	k1gNnSc2	Rusko
i	i	k8xC	i
k	k	k7c3	k
Západu	západ	k1gInSc3	západ
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
známý	známý	k2eAgInSc1d1	známý
intelektuální	intelektuální	k2eAgInSc1d1	intelektuální
spor	spor	k1gInSc1	spor
dobře	dobře	k6eAd1	dobře
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
rozporuplnou	rozporuplný	k2eAgFnSc4d1	rozporuplná
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
poreformní	poreformní	k2eAgNnSc1d1	poreformní
Rusko	Rusko	k1gNnSc1	Rusko
ocitalo	ocitat	k5eAaImAgNnS	ocitat
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
společnosti	společnost	k1gFnSc6	společnost
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
pochyb	pochyba	k1gFnPc2	pochyba
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruská	ruský	k2eAgFnSc1d1	ruská
kultura	kultura	k1gFnSc1	kultura
imperiálního	imperiální	k2eAgNnSc2d1	imperiální
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nP	zasloužit
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
:	:	kIx,	:
Alexandr	Alexandr	k1gMnSc1	Alexandr
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Jurjevič	Jurjevič	k1gMnSc1	Jurjevič
Lermontov	Lermontov	k1gInSc1	Lermontov
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
<g/>
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
,	,	kIx,	,
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
historici	historik	k1gMnPc1	historik
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Karamzin	Karamzin	k1gMnSc1	Karamzin
a	a	k8xC	a
Sergej	Sergej	k1gMnSc1	Sergej
Solovjev	Solovjev	k1gMnSc1	Solovjev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
připomenout	připomenout	k5eAaPmF	připomenout
například	například	k6eAd1	například
Petra	Petra	k1gFnSc1	Petra
Iljiče	Iljič	k1gMnSc2	Iljič
Čajkovského	Čajkovský	k2eAgMnSc2d1	Čajkovský
<g/>
,	,	kIx,	,
Michaila	Michail	k1gMnSc2	Michail
Ivanoviče	Ivanovič	k1gMnSc4	Ivanovič
Glinku	Glinka	k1gMnSc4	Glinka
<g/>
,	,	kIx,	,
Sergeje	Sergej	k1gMnSc4	Sergej
Prokofjeva	Prokofjev	k1gMnSc4	Prokofjev
<g/>
,	,	kIx,	,
Modesta	Modest	k1gMnSc4	Modest
Petroviče	Petrovič	k1gMnSc4	Petrovič
Musorgského	Musorgský	k1gMnSc4	Musorgský
či	či	k8xC	či
Nikolaje	Nikolaj	k1gMnSc4	Nikolaj
Andrejevič	Andrejevič	k1gInSc4	Andrejevič
Rimského-Korsakova	Rimského-Korsakův	k2eAgFnSc1d1	Rimského-Korsakova
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
malířství	malířství	k1gNnSc2	malířství
připomeneme	připomenout	k5eAaPmIp1nP	připomenout
například	například	k6eAd1	například
Ilju	Ilja	k1gFnSc4	Ilja
Repina	Repino	k1gNnSc2	Repino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
těchto	tento	k3xDgFnPc2	tento
osobností	osobnost	k1gFnPc2	osobnost
se	se	k3xPyFc4	se
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
nevědomky	nevědomky	k6eAd1	nevědomky
odrážel	odrážet	k5eAaImAgInS	odrážet
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
konflikt	konflikt	k1gInSc1	konflikt
o	o	k7c6	o
pojetí	pojetí	k1gNnSc6	pojetí
a	a	k8xC	a
chápání	chápání	k1gNnSc6	chápání
Ruska	Rusko	k1gNnSc2	Rusko
jako	jako	k8xC	jako
kultury	kultura	k1gFnSc2	kultura
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
civilizačními	civilizační	k2eAgInPc7d1	civilizační
světy	svět	k1gInPc7	svět
Západem	západ	k1gInSc7	západ
(	(	kIx(	(
<g/>
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
zájem	zájem	k1gInSc1	zájem
badatelů	badatel	k1gMnPc2	badatel
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
bohatou	bohatý	k2eAgFnSc4d1	bohatá
a	a	k8xC	a
obohacující	obohacující	k2eAgFnSc4d1	obohacující
směs	směs	k1gFnSc4	směs
kulturních	kulturní	k2eAgFnPc2d1	kulturní
praktik	praktika	k1gFnPc2	praktika
<g/>
,	,	kIx,	,
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
formovaly	formovat	k5eAaImAgFnP	formovat
základy	základ	k1gInPc4	základ
moderní	moderní	k2eAgFnSc2d1	moderní
národní	národní	k2eAgFnSc2d1	národní
kultury	kultura	k1gFnSc2	kultura
řady	řada	k1gFnSc2	řada
evropských	evropský	k2eAgInPc2d1	evropský
i	i	k8xC	i
neevropských	evropský	k2eNgInPc2d1	neevropský
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
<g/>
,	,	kIx,	,
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
<g/>
,	,	kIx,	,
estonská	estonský	k2eAgFnSc1d1	Estonská
<g/>
,	,	kIx,	,
litevská	litevský	k2eAgFnSc1d1	Litevská
<g/>
,	,	kIx,	,
lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
<g/>
,	,	kIx,	,
finská	finský	k2eAgFnSc1d1	finská
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
<g/>
,	,	kIx,	,
moldavská	moldavský	k2eAgFnSc1d1	Moldavská
<g/>
,	,	kIx,	,
gruzínská	gruzínský	k2eAgFnSc1d1	gruzínská
<g/>
,	,	kIx,	,
tatarská	tatarský	k2eAgFnSc1d1	tatarská
<g/>
,	,	kIx,	,
arménská	arménský	k2eAgFnSc1d1	arménská
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
imperiálního	imperiální	k2eAgNnSc2d1	imperiální
období	období	k1gNnSc2	období
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
===	===	k?	===
</s>
</p>
<p>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
zformován	zformovat	k5eAaPmNgInS	zformovat
podle	podle	k7c2	podle
evropského	evropský	k2eAgInSc2d1	evropský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
měla	mít	k5eAaImAgFnS	mít
kontrolu	kontrola	k1gFnSc4	kontrola
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
především	především	k6eAd1	především
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
organizace	organizace	k1gFnSc1	organizace
orientovala	orientovat	k5eAaBmAgFnS	orientovat
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
státních	státní	k2eAgFnPc2d1	státní
elit	elita	k1gFnPc2	elita
(	(	kIx(	(
<g/>
aristokratů	aristokrat	k1gMnPc2	aristokrat
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc1d1	veliký
založil	založit	k5eAaPmAgMnS	založit
několik	několik	k4yIc4	několik
kadetek	kadetka	k1gFnPc2	kadetka
orientovaných	orientovaný	k2eAgFnPc2d1	orientovaná
na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
důstojníku	důstojník	k1gMnSc3	důstojník
pro	pro	k7c4	pro
ruskou	ruský	k2eAgFnSc4d1	ruská
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
flotilu	flotila	k1gFnSc4	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
vznikala	vznikat	k5eAaImAgFnS	vznikat
první	první	k4xOgNnPc4	první
gymnázia	gymnázium	k1gNnPc4	gymnázium
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
)	)	kIx)	)
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
Zájem	zájem	k1gInSc1	zájem
státu	stát	k1gInSc2	stát
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
širších	široký	k2eAgFnPc2d2	širší
vrstev	vrstva	k1gFnPc2	vrstva
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
existující	existující	k2eAgFnSc4d1	existující
legislativu	legislativa	k1gFnSc4	legislativa
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Kateřiny	Kateřina	k1gFnSc2	Kateřina
I.	I.	kA	I.
Veliké	veliký	k2eAgInPc1d1	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
organizovala	organizovat	k5eAaBmAgFnS	organizovat
základní	základní	k2eAgFnSc4d1	základní
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
školství	školství	k1gNnSc1	školství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
poprvé	poprvé	k6eAd1	poprvé
konsolidováno	konsolidovat	k5eAaBmNgNnS	konsolidovat
za	za	k7c4	za
Kateřiny	Kateřina	k1gFnPc4	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliké	veliký	k2eAgNnSc1d1	veliké
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
rakouským	rakouský	k2eAgInSc7d1	rakouský
vzorem	vzor	k1gInSc7	vzor
a	a	k8xC	a
pozvala	pozvat	k5eAaPmAgFnS	pozvat
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
rakouské	rakouský	k2eAgMnPc4d1	rakouský
specialisty	specialista	k1gMnPc4	specialista
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
školství	školství	k1gNnSc1	školství
bylo	být	k5eAaImAgNnS	být
organizováno	organizovat	k5eAaBmNgNnS	organizovat
do	do	k7c2	do
několika	několik	k4yIc2	několik
stupňů	stupeň	k1gInPc2	stupeň
<g/>
:	:	kIx,	:
farní	farní	k2eAgFnSc2d1	farní
školy	škola	k1gFnSc2	škola
–	–	k?	–
městské	městský	k2eAgFnSc2d1	městská
školy	škola	k1gFnSc2	škola
–	–	k?	–
normální	normální	k2eAgFnSc2d1	normální
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
ruské	ruský	k2eAgNnSc1d1	ruské
školství	školství	k1gNnSc1	školství
(	(	kIx(	(
<g/>
základní	základní	k2eAgMnSc1d1	základní
také	také	k9	také
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
centrálně	centrálně	k6eAd1	centrálně
reorganizováno	reorganizován	k2eAgNnSc4d1	reorganizováno
a	a	k8xC	a
podřízeno	podřízen	k2eAgNnSc4d1	podřízeno
autoritě	autorita	k1gFnSc3	autorita
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
osvěty	osvěta	k1gFnSc2	osvěta
vedeného	vedený	k2eAgNnSc2d1	vedené
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
hrabětem	hrabě	k1gMnSc7	hrabě
Sergejem	Sergej	k1gMnSc7	Sergej
Uvarovem	Uvarov	k1gInSc7	Uvarov
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
vzdělání	vzdělání	k1gNnSc4	vzdělání
rolnického	rolnický	k2eAgNnSc2d1	rolnické
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
zintenzivňovat	zintenzivňovat	k5eAaImF	zintenzivňovat
během	běh	k1gInSc7	běh
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
(	(	kIx(	(
<g/>
zrušení	zrušení	k1gNnSc3	zrušení
nevolnictví	nevolnictví	k1gNnSc3	nevolnictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
financování	financování	k1gNnSc4	financování
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
si	se	k3xPyFc3	se
rozdělovaly	rozdělovat	k5eAaImAgInP	rozdělovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
tři	tři	k4xCgFnPc4	tři
instituce	instituce	k1gFnPc1	instituce
<g/>
:	:	kIx,	:
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
zemstva	zemstvo	k1gNnPc1	zemstvo
a	a	k8xC	a
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
se	se	k3xPyFc4	se
také	také	k9	také
stávala	stávat	k5eAaImAgFnS	stávat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
rusifikační	rusifikační	k2eAgFnPc4d1	rusifikační
snahy	snaha	k1gFnPc4	snaha
v	v	k7c6	v
neruských	ruský	k2eNgFnPc6d1	neruská
oblastech	oblast	k1gFnPc6	oblast
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
bylo	být	k5eAaImAgNnS	být
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
autonomní	autonomní	k2eAgMnSc1d1	autonomní
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
navštěvovalo	navštěvovat	k5eAaImAgNnS	navštěvovat
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
Rusku	Rusko	k1gNnSc6	Rusko
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
jen	jen	k9	jen
58	[number]	k4	58
%	%	kIx~	%
školou	škola	k1gFnSc7	škola
povinných	povinný	k2eAgFnPc2d1	povinná
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
<g/>
Ruské	ruský	k2eAgNnSc1d1	ruské
střední	střední	k2eAgNnSc1d1	střední
školství	školství	k1gNnSc1	školství
prošlo	projít	k5eAaPmAgNnS	projít
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
řadou	řada	k1gFnSc7	řada
významných	významný	k2eAgFnPc2d1	významná
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zcela	zcela	k6eAd1	zcela
specifickou	specifický	k2eAgFnSc4d1	specifická
funkci	funkce	k1gFnSc4	funkce
sehrávala	sehrávat	k5eAaImAgFnS	sehrávat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
původním	původní	k2eAgInSc7d1	původní
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
vzdělání	vzdělání	k1gNnSc1	vzdělání
elit	elita	k1gFnPc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
celoevropskými	celoevropský	k2eAgInPc7d1	celoevropský
trendy	trend	k1gInPc7	trend
bylo	být	k5eAaImAgNnS	být
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c4	v
Rusko	Rusko	k1gNnSc4	Rusko
reorganizováno	reorganizován	k2eAgNnSc4d1	reorganizováno
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
klasického	klasický	k2eAgNnSc2d1	klasické
gymnázia	gymnázium	k1gNnSc2	gymnázium
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
klasických	klasický	k2eAgInPc2d1	klasický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
progymnázia	progymnázia	k1gFnSc1	progymnázia
zaměřeného	zaměřený	k2eAgMnSc4d1	zaměřený
více	hodně	k6eAd2	hodně
na	na	k7c4	na
moderní	moderní	k2eAgInPc4d1	moderní
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Klíčové	klíčový	k2eAgNnSc1d1	klíčové
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
zákonných	zákonný	k2eAgFnPc2d1	zákonná
úprav	úprava	k1gFnPc2	úprava
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
odepřen	odepřen	k2eAgInSc1d1	odepřen
dětem	dítě	k1gFnPc3	dítě
z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
služek	služka	k1gFnPc2	služka
<g/>
,	,	kIx,	,
rolníků	rolník	k1gMnPc2	rolník
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
absolvování	absolvování	k1gNnSc1	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
bylo	být	k5eAaImAgNnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
vstupenkou	vstupenka	k1gFnSc7	vstupenka
na	na	k7c4	na
12	[number]	k4	12
ruských	ruský	k2eAgFnPc2d1	ruská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
gymnázii	gymnázium	k1gNnPc7	gymnázium
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
také	také	k6eAd1	také
profesní	profesní	k2eAgNnSc4d1	profesní
střední	střední	k2eAgNnSc4d1	střední
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
reálek	reálka	k1gFnPc2	reálka
<g/>
,	,	kIx,	,
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
a	a	k8xC	a
komerčních	komerční	k2eAgFnPc2d1	komerční
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
navazovala	navazovat	k5eAaImAgFnS	navazovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
polytechnik	polytechnika	k1gFnPc2	polytechnika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
založeno	založit	k5eAaPmNgNnS	založit
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
nových	nový	k2eAgFnPc2d1	nová
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1916	[number]	k4	1916
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
samozřejmě	samozřejmě	k6eAd1	samozřejmě
neodpovídalo	odpovídat	k5eNaImAgNnS	odpovídat
skutečné	skutečný	k2eAgFnSc3d1	skutečná
velikosti	velikost	k1gFnSc3	velikost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
univerzity	univerzita	k1gFnPc4	univerzita
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
,	,	kIx,	,
obnovena	obnoven	k2eAgFnSc1d1	obnovena
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Děrptu	Děrpta	k1gFnSc4	Děrpta
(	(	kIx(	(
<g/>
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazani	Kazaň	k1gFnSc6	Kazaň
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Charkově	Charkov	k1gInSc6	Charkov
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Varšavě	Varšava	k1gFnSc3	Varšava
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kyjevě	Kyjev	k1gInSc6	Kyjev
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Novorossijsku	Novorossijsk	k1gInSc2	Novorossijsk
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomsku	Tomska	k1gFnSc4	Tomska
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saratovu	Saratův	k2eAgFnSc4d1	Saratova
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rostově	Rostův	k2eAgInSc6d1	Rostův
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
a	a	k8xC	a
Permu	perm	k1gInSc2	perm
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
reforem	reforma	k1gFnPc2	reforma
Alexandra	Alexandr	k1gMnSc2	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
státní	státní	k2eAgFnSc1d1	státní
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
univerzitami	univerzita	k1gFnPc7	univerzita
zmírněna	zmírnit	k5eAaPmNgFnS	zmírnit
a	a	k8xC	a
posíleny	posílen	k2eAgFnPc1d1	posílena
některé	některý	k3yIgFnPc1	některý
akademické	akademický	k2eAgFnPc1d1	akademická
svobody	svoboda	k1gFnPc1	svoboda
i	i	k8xC	i
přístup	přístup	k1gInSc1	přístup
lidových	lidový	k2eAgFnPc2d1	lidová
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c4	na
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
změnil	změnit	k5eAaPmAgInS	změnit
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
konzervativního	konzervativní	k2eAgInSc2d1	konzervativní
režimu	režim	k1gInSc2	režim
Alexandra	Alexandr	k1gMnSc2	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
znovu	znovu	k6eAd1	znovu
podřídil	podřídit	k5eAaPmAgMnS	podřídit
univerzity	univerzita	k1gFnPc4	univerzita
plné	plný	k2eAgFnSc3d1	plná
ideologické	ideologický	k2eAgFnSc3d1	ideologická
kontrole	kontrola	k1gFnSc3	kontrola
ruského	ruský	k2eAgInSc2d1	ruský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
FLOROVSKIJ	FLOROVSKIJ	kA	FLOROVSKIJ
<g/>
,	,	kIx,	,
Antonij	Antonij	k1gMnSc1	Antonij
Vasiľjevič	Vasiľjevič	k1gMnSc1	Vasiľjevič
<g/>
.	.	kIx.	.
</s>
<s>
Česko-ruské	českouský	k2eAgInPc1d1	česko-ruský
obchodní	obchodní	k2eAgInPc1d1	obchodní
styky	styk	k1gInPc1	styk
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
X.	X.	kA	X.
<g/>
-XVIII	-XVIII	k?	-XVIII
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
394	[number]	k4	394
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOSKINS	HOSKINS	kA	HOSKINS
<g/>
,	,	kIx,	,
Geoffey	Geoffey	k1gInPc1	Geoffey
<g/>
:	:	kIx,	:
Russia	Russia	k1gFnSc1	Russia
<g/>
.	.	kIx.	.
</s>
<s>
People	People	k6eAd1	People
and	and	k?	and
Empire	empir	k1gInSc5	empir
1552	[number]	k4	1552
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
Mass	Massa	k1gFnPc2	Massa
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
674	[number]	k4	674
<g/>
-	-	kIx~	-
<g/>
78119	[number]	k4	78119
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAPPELER	KAPPELER	kA	KAPPELER
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gInSc1	Andreas
<g/>
:	:	kIx,	:
Russland	Russland	k1gInSc1	Russland
als	als	k?	als
Vielvökerreich	Vielvökerreich	k1gInSc1	Vielvökerreich
<g/>
.	.	kIx.	.
</s>
<s>
Entstehung	Entstehung	k1gMnSc1	Entstehung
<g/>
,	,	kIx,	,
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
,	,	kIx,	,
Zerfall	Zerfalla	k1gFnPc2	Zerfalla
<g/>
.	.	kIx.	.
</s>
<s>
München	München	k1gInSc1	München
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
406	[number]	k4	406
<g/>
-	-	kIx~	-
<g/>
36472	[number]	k4	36472
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
М	М	k?	М
<g/>
,	,	kIx,	,
Б	Б	k?	Б
Н	Н	k?	Н
<g/>
:	:	kIx,	:
С	С	k?	С
и	и	k?	и
Р	Р	k?	Р
п	п	k?	п
и	и	k?	и
(	(	kIx(	(
<g/>
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
н	н	k?	н
XX	XX	kA	XX
<g/>
.	.	kIx.	.
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
Г	Г	k?	Г
л	л	k?	л
<g/>
,	,	kIx,	,
д	д	k?	д
с	с	k?	с
<g/>
,	,	kIx,	,
г	г	k?	г
о	о	k?	о
и	и	k?	и
п	п	k?	п
г	г	k?	г
<g/>
.	.	kIx.	.
С	С	k?	С
П	П	k?	П
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
5-86007-185-X	[number]	k4	5-86007-185-X
V	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
MIRONOV	MIRONOV	kA	MIRONOV
<g/>
,	,	kIx,	,
Boris	Boris	k1gMnSc1	Boris
N.	N.	kA	N.
A	a	k9	a
Social	Social	k1gInSc1	Social
History	Histor	k1gInPc1	Histor
of	of	k?	of
Imperial	Imperial	k1gInSc4	Imperial
Russia	Russius	k1gMnSc2	Russius
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k1gMnSc1	West
View	View	k1gMnSc1	View
Press	Press	k1gInSc4	Press
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8133	[number]	k4	8133
<g/>
-	-	kIx~	-
<g/>
3684	[number]	k4	3684
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PIPES	PIPES	kA	PIPES
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
:	:	kIx,	:
Rusko	Rusko	k1gNnSc1	Rusko
za	za	k7c2	za
starého	starý	k2eAgInSc2d1	starý
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Argo	Argo	k1gNnSc1	Argo
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
406	[number]	k4	406
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
559	[number]	k4	559
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STELLNER	STELLNER	kA	STELLNER
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
dynastische	dynastischat	k5eAaPmIp3nS	dynastischat
Politik	politik	k1gMnSc1	politik
des	des	k1gNnSc4	des
russischen	russischen	k2eAgInSc1d1	russischen
Imperiums	Imperiums	k1gInSc1	Imperiums
im	im	k?	im
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Jahrhundert	Jahrhundert	k1gMnSc1	Jahrhundert
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ruské	ruský	k2eAgFnSc2d1	ruská
impérium	impérium	k1gNnSc4	impérium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Moskvě	Moskva	k1gFnSc3	Moskva
pod	pod	k7c7	pod
sněhem	sníh	k1gInSc7	sníh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
В	В	k?	В
С	С	k?	С
Р	Р	k?	Р
о	о	k?	о
к	к	k?	к
<g/>
.	.	kIx.	.
М	М	k?	М
1993	[number]	k4	1993
</s>
</p>
