<s>
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
</s>
<s>
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
obchodní	obchodní	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Charakteristika	charakteristikon	k1gNnSc2
firmy	firma	k1gFnSc2
Majitel	majitel	k1gMnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
https://www.postovnisporitelna.cz/	https://www.postovnisporitelna.cz/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Era	Era	k?
svět	svět	k1gInSc1
na	na	k7c6
Jungmannově	Jungmannov	k1gInSc6
nám.	nám.	k?
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2012	#num#	k4
</s>
<s>
Vkladní	vkladní	k2eAgFnSc1d1
knížka	knížka	k1gFnSc1
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
z	z	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
je	být	k5eAaImIp3nS
obchodní	obchodní	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
ČSOB	ČSOB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
původně	původně	k6eAd1
jako	jako	k8xC,k8xS
samostatná	samostatný	k2eAgFnSc1d1
banka	banka	k1gFnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Poštovní	poštovní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
současný	současný	k2eAgInSc4d1
název	název	k1gInSc4
užívá	užívat	k5eAaImIp3nS
až	až	k9
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
ji	on	k3xPp3gFnSc4
převzala	převzít	k5eAaPmAgFnS
Investiční	investiční	k2eAgFnSc1d1
a	a	k8xC
poštovní	poštovní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
skupiny	skupina	k1gFnSc2
ČSOB	ČSOB	kA
vlastněné	vlastněný	k2eAgFnPc1d1
belgickou	belgický	k2eAgFnSc4d1
KBC	KBC	kA
Bank	bank	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služby	služba	k1gFnPc4
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
jsou	být	k5eAaImIp3nP
poskytovány	poskytovat	k5eAaImNgFnP
na	na	k7c6
přepážkách	přepážka	k1gFnPc6
České	český	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
a	a	k8xC
na	na	k7c6
Finančních	finanční	k2eAgNnPc6d1
centrech	centrum	k1gNnPc6
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
rozsáhlé	rozsáhlý	k2eAgFnSc3d1
pobočkové	pobočkový	k2eAgFnSc3d1
síti	síť	k1gFnSc3
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
aktuálně	aktuálně	k6eAd1
nejlépe	dobře	k6eAd3
dostupnou	dostupný	k2eAgFnSc7d1
bankou	banka	k1gFnSc7
na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
začala	začít	k5eAaPmAgFnS
pod	pod	k7c7
jednou	jeden	k4xCgFnSc7
střechou	střecha	k1gFnSc7
s	s	k7c7
Poštovní	poštovní	k2eAgFnSc7d1
spořitelnou	spořitelna	k1gFnSc7
působit	působit	k5eAaImF
také	také	k9
značka	značka	k1gFnSc1
Era	Era	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSOB	ČSOB	kA
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
prodloužit	prodloužit	k5eAaPmF
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
Českou	český	k2eAgFnSc7d1
poštou	pošta	k1gFnSc7
na	na	k7c4
dalších	další	k2eAgNnPc2d1
10	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
rozšířila	rozšířit	k5eAaPmAgFnS
tak	tak	k9
nabídku	nabídka	k1gFnSc4
produktů	produkt	k1gInPc2
a	a	k8xC
služeb	služba	k1gFnPc2
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
na	na	k7c6
poštách	pošta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
Poštovní	poštovní	k2eAgFnSc1d1
půjčka	půjčka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
slavila	slavit	k5eAaImAgFnS
u	u	k7c2
klientů	klient	k1gMnPc2
úspěch	úspěch	k1gInSc4
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
Poštovní	poštovní	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
představila	představit	k5eAaPmAgFnS
na	na	k7c6
trhu	trh	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
pak	pak	k6eAd1
značka	značka	k1gFnSc1
spustila	spustit	k5eAaPmAgFnS
prodej	prodej	k1gInSc4
Poštovního	poštovní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
a	a	k8xC
Poštovních	poštovní	k2eAgInPc2d1
fondů	fond	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
rozšířené	rozšířený	k2eAgFnSc2d1
a	a	k8xC
aktualizované	aktualizovaný	k2eAgFnSc2d1
nabídky	nabídka	k1gFnSc2
produktů	produkt	k1gInPc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
přišla	přijít	k5eAaPmAgFnS
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
i	i	k9
s	s	k7c7
novým	nový	k2eAgInSc7d1
marketingovým	marketingový	k2eAgInSc7d1
konceptem	koncept	k1gInSc7
s	s	k7c7
poštovními	poštovní	k2eAgMnPc7d1
skřítky	skřítek	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvojem	rozvoj	k1gInSc7
služeb	služba	k1gFnPc2
a	a	k8xC
značky	značka	k1gFnSc2
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
zanikly	zaniknout	k5eAaPmAgInP
důvody	důvod	k1gInPc1
pro	pro	k7c4
oddělení	oddělení	k1gNnSc4
značky	značka	k1gFnSc2
Era	Era	k1gMnPc7
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
sjednocení	sjednocení	k1gNnSc3
obsluhy	obsluha	k1gFnSc2
a	a	k8xC
produktů	produkt	k1gInPc2
obou	dva	k4xCgInPc2
značek	značka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
představuje	představovat	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
80	#num#	k4
Finančních	finanční	k2eAgNnPc2d1
center	centrum	k1gNnPc2
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
3	#num#	k4
000	#num#	k4
poboček	pobočka	k1gFnPc2
České	český	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Své	svůj	k3xOyFgFnPc4
služby	služba	k1gFnPc4
poskytuje	poskytovat	k5eAaImIp3nS
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
přibližně	přibližně	k6eAd1
2	#num#	k4
milionům	milion	k4xCgInPc3
klientů	klient	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
poskytování	poskytování	k1gNnSc6
drobného	drobný	k2eAgNnSc2d1
bankovnictví	bankovnictví	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zajišťuje	zajišťovat	k5eAaImIp3nS
komplexní	komplexní	k2eAgFnPc4d1
služby	služba	k1gFnPc4
jak	jak	k8xC,k8xS
fyzickým	fyzický	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
podnikatelským	podnikatelský	k2eAgInPc3d1
subjektům	subjekt	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Veškeré	veškerý	k3xTgFnPc4
informace	informace	k1gFnPc4
o	o	k7c6
nabídce	nabídka	k1gFnSc6
produktů	produkt	k1gInPc2
a	a	k8xC
služeb	služba	k1gFnPc2
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
na	na	k7c6
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
také	také	k9
možnost	možnost	k1gFnSc1
zřídit	zřídit	k5eAaPmF
vkladní	vkladní	k2eAgFnSc4d1
knížku	knížka	k1gFnSc4
dětskou	dětský	k2eAgFnSc4d1
pro	pro	k7c4
děti	dítě	k1gFnPc4
již	již	k6eAd1
od	od	k7c2
narození	narození	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsluhovat	obsluhovat	k5eAaImF
takovou	takový	k3xDgFnSc4
vkladní	vkladní	k2eAgFnSc4d1
knížku	knížka	k1gFnSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
jak	jak	k8xC,k8xS
na	na	k7c6
Finančních	finanční	k2eAgNnPc6d1
centrech	centrum	k1gNnPc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
na	na	k7c6
pobočkách	pobočka	k1gFnPc6
České	český	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vklad	vklad	k1gInSc1
na	na	k7c4
takovou	takový	k3xDgFnSc4
knížku	knížka	k1gFnSc4
může	moct	k5eAaImIp3nS
realizovat	realizovat	k5eAaBmF
rodič	rodič	k1gMnSc1
i	i	k9
třetí	třetí	k4xOgFnSc1
osoba	osoba	k1gFnSc1
bezhotovostně	bezhotovostně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
sloučení	sloučení	k1gNnSc3
s	s	k7c7
ČSOB	ČSOB	kA
a	a	k8xC
značka	značka	k1gFnSc1
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
zanikne	zaniknout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
poštovních	poštovní	k2eAgFnPc2d1
spořitelen	spořitelna	k1gFnPc2
</s>
<s>
Šekový	šekový	k2eAgInSc1d1
vplatní	vplatní	k2eAgInSc1d1
lístek	lístek	k1gInSc1
Poštovní	poštovní	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
</s>
<s>
Historie	historie	k1gFnSc1
poštovních	poštovní	k2eAgFnPc2d1
spořitelen	spořitelna	k1gFnPc2
sahá	sahat	k5eAaImIp3nS
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
předválečném	předválečný	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
fungovala	fungovat	k5eAaImAgFnS
poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
rakousko-uherskou	rakousko-uherský	k2eAgFnSc4d1
předchůdkyni	předchůdkyně	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československá	československý	k2eAgFnSc1d1
poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
převzala	převzít	k5eAaPmAgFnS
v	v	k7c6
novém	nový	k2eAgInSc6d1
československém	československý	k2eAgInSc6d1
státě	stát	k1gInSc6
šekovou	šekový	k2eAgFnSc4d1
službu	služba	k1gFnSc4
bývalého	bývalý	k2eAgInSc2d1
rakouského	rakouský	k2eAgInSc2d1
poštovního	poštovní	k2eAgInSc2d1
spořitelního	spořitelní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
zřízena	zřídit	k5eAaPmNgFnS
jen	jen	k9
jako	jako	k9
správní	správní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
v	v	k7c6
oboru	obor	k1gInSc6
působnosti	působnost	k1gFnSc2
poštovní	poštovní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonem	zákon	k1gInSc7
číslo	číslo	k1gNnSc1
140	#num#	k4
<g/>
/	/	kIx~
<g/>
1919	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
Poštovní	poštovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
šekový	šekový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
provádět	provádět	k5eAaImF
bezhotovostní	bezhotovostní	k2eAgFnPc4d1
platby	platba	k1gFnPc4
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
hospodářskými	hospodářský	k2eAgInPc7d1
subjekty	subjekt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
sehrál	sehrát	k5eAaPmAgMnS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
stabilizaci	stabilizace	k1gFnSc6
měnových	měnový	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
v	v	k7c6
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc6d1
ČSR	ČSR	kA
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
snížil	snížit	k5eAaPmAgInS
potřebu	potřeba	k1gFnSc4
oběživa	oběživo	k1gNnSc2
na	na	k7c4
platby	platba	k1gFnPc4
v	v	k7c6
hotovosti	hotovost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Poštovního	poštovní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
šekového	šekový	k2eAgNnSc2d1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
soustřeďovala	soustřeďovat	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
část	část	k1gFnSc1
příjmů	příjem	k1gInPc2
a	a	k8xC
výplat	výplata	k1gFnPc2
státu	stát	k1gInSc2
na	na	k7c6
ústředním	ústřední	k2eAgNnSc6d1
kontě	konto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
byl	být	k5eAaImAgInS
zákonem	zákon	k1gInSc7
č.	č.	k?
143	#num#	k4
<g/>
/	/	kIx~
<g/>
1930	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
přeměněn	přeměněn	k2eAgInSc1d1
Poštovní	poštovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
šekový	šekový	k2eAgInSc1d1
na	na	k7c4
Poštovní	poštovní	k2eAgFnSc4d1
spořitelnu	spořitelna	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
jako	jako	k9
nový	nový	k2eAgInSc4d1
atribut	atribut	k1gInSc4
jeho	jeho	k3xOp3gFnSc2
činnosti	činnost	k1gFnSc2
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
spořitelní	spořitelní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
instituce	instituce	k1gFnPc4
byla	být	k5eAaImAgFnS
rovněž	rovněž	k9
důležitým	důležitý	k2eAgInSc7d1
činitelem	činitel	k1gInSc7
a	a	k8xC
místem	místo	k1gNnSc7
pro	pro	k7c4
upisování	upisování	k1gNnSc4
vnitřních	vnitřní	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
půjček	půjčka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
k	k	k7c3
existenci	existence	k1gFnSc3
této	tento	k3xDgFnSc2
instituce	instituce	k1gFnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
Archivu	archiv	k1gInSc6
ČNB	ČNB	kA
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
země	zem	k1gFnPc4
s	s	k7c7
nejrozvinutějším	rozvinutý	k2eAgNnSc7d3
poštovním	poštovní	k2eAgNnSc7d1
bankovnictvím	bankovnictví	k1gNnSc7
patří	patřit	k5eAaImIp3nS
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
Japonsko	Japonsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
forma	forma	k1gFnSc1
poskytování	poskytování	k1gNnSc2
bankovních	bankovní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
je	být	k5eAaImIp3nS
též	též	k9
velmi	velmi	k6eAd1
populární	populární	k2eAgFnSc1d1
v	v	k7c6
rozvojových	rozvojový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgInPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Senegal	Senegal	k1gInSc1
<g/>
,	,	kIx,
Keňa	Keňa	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Írán	Írán	k1gInSc1
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Představení	představení	k1gNnPc2
Ery	Ery	k1gFnPc2
|	|	kIx~
Era	Era	k1gFnPc2
jednoduše	jednoduše	k6eAd1
<g/>
.	.	kIx.
erasvet	erasvet	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c6
ČSOB	ČSOB	kA
a	a	k8xC
skupině	skupina	k1gFnSc6
|	|	kIx~
ČSOB	ČSOB	kA
<g/>
.	.	kIx.
www.csob.cz	www.csob.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poštovní	poštovní	k2eAgFnSc1d1
spořitelna	spořitelna	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
