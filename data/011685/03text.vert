<p>
<s>
Zápas	zápas	k1gInSc1	zápas
řecko-římský	řecko-římský	k2eAgInSc1d1	řecko-římský
ve	v	k7c6	v
váhové	váhový	k2eAgFnSc6d1	váhová
kategorii	kategorie	k1gFnSc6	kategorie
+100	+100	k4	+100
kg	kg	kA	kg
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1984	[number]	k4	1984
v	v	k7c6	v
Anaheimském	Anaheimský	k2eAgInSc6d1	Anaheimský
Convention	Convention	k1gInSc1	Convention
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
medaile	medaile	k1gFnPc4	medaile
se	se	k3xPyFc4	se
utkalo	utkat	k5eAaPmAgNnS	utkat
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
zápasníků	zápasník	k1gMnPc2	zápasník
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
dodatečně	dodatečně	k6eAd1	dodatečně
měnilo	měnit	k5eAaImAgNnS	měnit
pro	pro	k7c4	pro
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
dopingový	dopingový	k2eAgInSc4d1	dopingový
nález	nález	k1gInSc4	nález
u	u	k7c2	u
poraženého	poražený	k2eAgMnSc2d1	poražený
finalisty	finalista	k1gMnSc2	finalista
Johanssona	Johansson	k1gMnSc2	Johansson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medailisté	medailista	k1gMnPc5	medailista
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Turnajové	turnajový	k2eAgInPc1d1	turnajový
výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Zápasníci	zápasník	k1gMnPc1	zápasník
jsou	být	k5eAaImIp3nP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
aplikován	aplikován	k2eAgInSc4d1	aplikován
systém	systém	k1gInSc4	systém
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
porážky	porážka	k1gFnPc4	porážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LegendaTF	LegendaTF	k?	LegendaTF
–	–	k?	–
Lopatkové	lopatkový	k2eAgNnSc4d1	lopatkové
vítězství	vítězství	k1gNnSc4	vítězství
</s>
</p>
<p>
<s>
ST	St	kA	St
–	–	k?	–
Vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c4	na
technickou	technický	k2eAgFnSc4d1	technická
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
12	[number]	k4	12
bodů	bod	k1gInPc2	bod
rozdíl	rozdíl	k1gInSc4	rozdíl
</s>
</p>
<p>
<s>
PP	PP	kA	PP
–	–	k?	–
Vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c4	na
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
1-7	[number]	k4	1-7
bodů	bod	k1gInPc2	bod
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
poražený	poražený	k2eAgMnSc1d1	poražený
s	s	k7c7	s
body	bod	k1gInPc7	bod
</s>
</p>
<p>
<s>
PO	Po	kA	Po
–	–	k?	–
Vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c4	na
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
1-7	[number]	k4	1-7
bodů	bod	k1gInPc2	bod
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
poražený	poražený	k2eAgInSc4d1	poražený
bez	bez	k7c2	bez
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
SP	SP	kA	SP
–	–	k?	–
Vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c4	na
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
8-11	[number]	k4	8-11
bodů	bod	k1gInPc2	bod
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
poražený	poražený	k2eAgMnSc1d1	poražený
s	s	k7c7	s
body	bod	k1gInPc7	bod
</s>
</p>
<p>
<s>
SO	So	kA	So
–	–	k?	–
Vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c4	na
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
8-11	[number]	k4	8-11
bodů	bod	k1gInPc2	bod
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
poražený	poražený	k2eAgInSc4d1	poražený
bez	bez	k7c2	bez
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
P0	P0	k4	P0
–	–	k?	–
Vítězství	vítězství	k1gNnSc4	vítězství
pro	pro	k7c4	pro
pasivitu	pasivita	k1gFnSc4	pasivita
<g/>
,	,	kIx,	,
protivník	protivník	k1gMnSc1	protivník
bez	bez	k7c2	bez
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
P1	P1	k4	P1
–	–	k?	–
Vítězství	vítězství	k1gNnSc4	vítězství
pro	pro	k7c4	pro
pasivitu	pasivita	k1gFnSc4	pasivita
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
1-7	[number]	k4	1-7
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
PS	PS	kA	PS
–	–	k?	–
Vítězství	vítězství	k1gNnSc1	vítězství
pro	pro	k7c4	pro
pasivitu	pasivita	k1gFnSc4	pasivita
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
8-11	[number]	k4	8-11
bodů	bod	k1gInPc2	bod
</s>
</p>
<p>
<s>
DC	DC	kA	DC
–	–	k?	–
Vítězství	vítězství	k1gNnSc1	vítězství
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
,	,	kIx,	,
skóre	skóre	k1gNnSc2	skóre
0-0	[number]	k4	0-0
</s>
</p>
<p>
<s>
PA	Pa	kA	Pa
–	–	k?	–
Vítězství	vítězství	k1gNnSc1	vítězství
pro	pro	k7c4	pro
soupeřovo	soupeřův	k2eAgNnSc4d1	soupeřovo
zranění	zranění	k1gNnSc4	zranění
</s>
</p>
<p>
<s>
DQ	DQ	kA	DQ
–	–	k?	–
Vítězství	vítězství	k1gNnSc1	vítězství
pro	pro	k7c4	pro
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
diskvalifikaci	diskvalifikace	k1gFnSc4	diskvalifikace
</s>
</p>
<p>
<s>
DNA	DNA	kA	DNA
–	–	k?	–
Nenastoupil	nastoupit	k5eNaPmAgMnS	nastoupit
</s>
</p>
<p>
<s>
L	L	kA	L
–	–	k?	–
Porážky	porážka	k1gFnSc2	porážka
</s>
</p>
<p>
<s>
ER	ER	kA	ER
–	–	k?	–
Kolo	kolo	k1gNnSc1	kolo
vyřazení	vyřazení	k1gNnSc2	vyřazení
</s>
</p>
<p>
<s>
CP	CP	kA	CP
–	–	k?	–
Klasifikační	klasifikační	k2eAgInPc4d1	klasifikační
body	bod	k1gInPc4	bod
</s>
</p>
<p>
<s>
TP	TP	kA	TP
–	–	k?	–
Technické	technický	k2eAgInPc1d1	technický
body	bod	k1gInPc1	bod
</s>
</p>
<p>
<s>
===	===	k?	===
Skupiny	skupina	k1gFnPc1	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Finálové	finálový	k2eAgNnSc1d1	finálové
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Blatnick	Blatnick	k1gMnSc1	Blatnick
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Refik	Refik	k1gMnSc1	Refik
Memišević	Memišević	k1gMnSc1	Memišević
(	(	kIx(	(
<g/>
YUG	YUG	kA	YUG
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Victor	Victor	k1gMnSc1	Victor
Dolipschi	Dolipsch	k1gFnSc2	Dolipsch
(	(	kIx(	(
<g/>
ROU	ROU	kA	ROU
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Panagiotis	Panagiotis	k1gFnSc1	Panagiotis
Poikilidis	Poikilidis	k1gFnSc1	Poikilidis
(	(	kIx(	(
<g/>
GRE	GRE	kA	GRE
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hassan	Hassan	k1gInSc1	Hassan
El-Hadad	El-Hadad	k1gInSc1	El-Hadad
(	(	kIx(	(
<g/>
EGY	ego	k1gNnPc7	ego
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masaja	Masaja	k6eAd1	Masaja
Ando	Anda	k1gFnSc5	Anda
(	(	kIx(	(
<g/>
JPN	JPN	kA	JPN
<g/>
)	)	kIx)	)
a	a	k8xC	a
Antonio	Antonio	k1gMnSc1	Antonio
La	la	k1gNnSc2	la
Penna	Penno	k1gNnSc2	Penno
(	(	kIx(	(
<g/>
ITA	ITA	kA	ITA
<g/>
)	)	kIx)	)
<g/>
DSQ	DSQ	kA	DSQ
<g/>
:	:	kIx,	:
Tomas	Tomas	k1gMnSc1	Tomas
Johansson	Johansson	k1gMnSc1	Johansson
(	(	kIx(	(
<g/>
SWE	SWE	kA	SWE
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
</s>
</p>
<p>
<s>
*	*	kIx~	*
diskvalifikován	diskvalifikován	k2eAgMnSc1d1	diskvalifikován
po	po	k7c6	po
positivním	positivní	k2eAgInSc6d1	positivní
testu	test	k1gInSc6	test
na	na	k7c4	na
Methenolon	Methenolon	k1gInSc4	Methenolon
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wrestling	Wrestling	k1gInSc1	Wrestling
at	at	k?	at
the	the	k?	the
1984	[number]	k4	1984
Summer	Summer	k1gInSc1	Summer
Olympics	Olympics	k1gInSc4	Olympics
–	–	k?	–
Men	Men	k1gFnSc2	Men
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Greco-Roman	Greco-Roman	k1gMnSc1	Greco-Roman
+100	+100	k4	+100
kg	kg	kA	kg
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
zpráva	zpráva	k1gFnSc1	zpráva
</s>
</p>
