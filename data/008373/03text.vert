<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Bernhard	Bernhard	k1gMnSc1	Bernhard
Riemann	Riemann	k1gMnSc1	Riemann
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
Breselenz	Breselenz	k1gInSc1	Breselenz
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
Selasca	Selasca	k1gFnSc1	Selasca
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
Riemannovy	Riemannův	k2eAgFnSc2d1	Riemannova
práce	práce	k1gFnSc2	práce
==	==	k?	==
</s>
</p>
<p>
<s>
Riemann	Riemann	k1gMnSc1	Riemann
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
geometrie	geometrie	k1gFnSc2	geometrie
a	a	k8xC	a
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
myšlenkách	myšlenka	k1gFnPc6	myšlenka
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
rozvinuty	rozvinout	k5eAaPmNgFnP	rozvinout
například	například	k6eAd1	například
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,	,	kIx,	,
algebraická	algebraický	k2eAgFnSc1d1	algebraická
geometrie	geometrie	k1gFnSc1	geometrie
či	či	k8xC	či
teorie	teorie	k1gFnSc1	teorie
komplexních	komplexní	k2eAgFnPc2d1	komplexní
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
matematiky	matematika	k1gFnSc2	matematika
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
topologie	topologie	k1gFnSc2	topologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reálné	reálný	k2eAgFnSc6d1	reálná
analýze	analýza	k1gFnSc6	analýza
přispěl	přispět	k5eAaPmAgInS	přispět
definicí	definice	k1gFnSc7	definice
Riemannova	Riemannův	k2eAgInSc2d1	Riemannův
integrálu	integrál	k1gInSc2	integrál
a	a	k8xC	a
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
také	také	k9	také
teorii	teorie	k1gFnSc4	teorie
trigonometrických	trigonometrický	k2eAgFnPc2d1	trigonometrická
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
jediném	jediný	k2eAgInSc6d1	jediný
článku	článek	k1gInSc6	článek
věnovaném	věnovaný	k2eAgInSc6d1	věnovaný
teorii	teorie	k1gFnSc4	teorie
čísel	číslo	k1gNnPc2	číslo
zavedl	zavést	k5eAaPmAgInS	zavést
Riemannovu	Riemannův	k2eAgFnSc4d1	Riemannova
funkci	funkce	k1gFnSc4	funkce
zeta	zet	k1gMnSc2	zet
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
její	její	k3xOp3gInSc4	její
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
rozmístění	rozmístění	k1gNnSc3	rozmístění
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
článku	článek	k1gInSc6	článek
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
řadu	řada	k1gFnSc4	řada
domněnek	domněnka	k1gFnPc2	domněnka
o	o	k7c6	o
vlastnostech	vlastnost	k1gFnPc6	vlastnost
zeta	zet	k1gMnSc2	zet
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Bernhard	Bernhard	k1gMnSc1	Bernhard
Riemann	Riemann	k1gMnSc1	Riemann
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1826	[number]	k4	1826
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
vesničce	vesnička	k1gFnSc6	vesnička
Breselenz	Breselenz	k1gMnSc1	Breselenz
poblíž	poblíž	k7c2	poblíž
Dannenbergu	Dannenberg	k1gInSc2	Dannenberg
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dětství	dětství	k1gNnSc1	dětství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chudé	chudý	k2eAgFnSc6d1	chudá
a	a	k8xC	a
zaostalé	zaostalý	k2eAgFnSc6d1	zaostalá
oblasti	oblast	k1gFnSc6	oblast
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Bernhardův	Bernhardův	k2eAgMnSc1d1	Bernhardův
otec	otec	k1gMnSc1	otec
Friedrich	Friedrich	k1gMnSc1	Friedrich
Bernhard	Bernhard	k1gMnSc1	Bernhard
Riemann	Riemann	k1gMnSc1	Riemann
byl	být	k5eAaImAgMnS	být
chudý	chudý	k2eAgMnSc1d1	chudý
luteránský	luteránský	k2eAgMnSc1d1	luteránský
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
veterán	veterán	k1gMnSc1	veterán
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
raného	raný	k2eAgNnSc2d1	rané
dětství	dětství	k1gNnSc2	dětství
projevoval	projevovat	k5eAaImAgMnS	projevovat
Riemann	Riemann	k1gInSc4	Riemann
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
matematické	matematický	k2eAgNnSc4d1	matematické
nadání	nadání	k1gNnSc4	nadání
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
měl	mít	k5eAaImAgMnS	mít
skvělé	skvělý	k2eAgFnPc4d1	skvělá
počtářské	počtářský	k2eAgFnPc4d1	počtářská
dovednosti	dovednost	k1gFnPc4	dovednost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prodělal	prodělat	k5eAaPmAgMnS	prodělat
několik	několik	k4yIc4	několik
nervových	nervový	k2eAgInPc2d1	nervový
kolapsů	kolaps	k1gInPc2	kolaps
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgInS	trpět
abnormální	abnormální	k2eAgFnSc7d1	abnormální
stydlivostí	stydlivost	k1gFnSc7	stydlivost
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
hrůzu	hrůza	k1gFnSc4	hrůza
z	z	k7c2	z
mluvení	mluvení	k1gNnSc2	mluvení
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Bernhard	Bernhard	k1gInSc4	Bernhard
ještě	ještě	k9	ještě
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
otec	otec	k1gMnSc1	otec
přijal	přijmout	k5eAaPmAgMnS	přijmout
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
jako	jako	k8xC	jako
farář	farář	k1gMnSc1	farář
v	v	k7c6	v
Quickbornu	Quickborn	k1gInSc6	Quickborn
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Breselenze	Breselenze	k1gFnSc2	Breselenze
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
centrem	centr	k1gInSc7	centr
jeho	jeho	k3xOp3gInSc2	jeho
citového	citový	k2eAgInSc2d1	citový
života	život	k1gInSc2	život
až	až	k9	až
do	do	k7c2	do
téměř	téměř	k6eAd1	téměř
třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
o	o	k7c6	o
Riemannovi	Riemann	k1gMnSc6	Riemann
nevíme	vědět	k5eNaImIp1nP	vědět
mnoho	mnoho	k4c4	mnoho
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
nezanechal	zanechat	k5eNaPmAgInS	zanechat
žádné	žádný	k3yNgInPc4	žádný
záznamy	záznam	k1gInPc4	záznam
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
usoudit	usoudit	k5eAaPmF	usoudit
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Quickborn	Quickborn	k1gNnSc1	Quickborn
bylo	být	k5eAaImAgNnS	být
jediným	jediné	k1gNnSc7	jediné
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
příležitosti	příležitost	k1gFnSc6	příležitost
rád	rád	k6eAd1	rád
vracel	vracet	k5eAaImAgMnS	vracet
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
rodinného	rodinný	k2eAgInSc2d1	rodinný
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
ale	ale	k9	ale
v	v	k7c6	v
Quickbornu	Quickborno	k1gNnSc6	Quickborno
nebylo	být	k5eNaImAgNnS	být
žádné	žádný	k3yNgNnSc1	žádný
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
svoje	svůj	k3xOyFgNnSc4	svůj
vzdělání	vzdělání	k1gNnSc4	vzdělání
až	až	k9	až
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Hannoveru	Hannover	k1gInSc6	Hannover
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žila	žít	k5eAaImAgFnS	žít
Bernhardova	Bernhardův	k2eAgFnSc1d1	Bernhardova
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
rodina	rodina	k1gFnSc1	rodina
ušetřila	ušetřit	k5eAaPmAgFnS	ušetřit
za	za	k7c4	za
ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Riemann	Riemann	k1gInSc1	Riemann
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
cítil	cítit	k5eAaImAgMnS	cítit
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
a	a	k8xC	a
tesknící	tesknící	k2eAgMnSc1d1	tesknící
po	po	k7c6	po
domově	domov	k1gInSc6	domov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
především	především	k9	především
studiem	studio	k1gNnSc7	studio
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgMnS	projevovat
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
matematice	matematika	k1gFnSc3	matematika
–	–	k?	–
pokusil	pokusit	k5eAaPmAgInS	pokusit
se	se	k3xPyFc4	se
matematicky	matematicky	k6eAd1	matematicky
dokázat	dokázat	k5eAaPmF	dokázat
korektnost	korektnost	k1gFnSc4	korektnost
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
učitele	učitel	k1gMnSc4	učitel
však	však	k9	však
překvapoval	překvapovat	k5eAaImAgInS	překvapovat
svými	svůj	k3xOyFgFnPc7	svůj
schopnostmi	schopnost	k1gFnPc7	schopnost
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
obtížných	obtížný	k2eAgFnPc2d1	obtížná
matematických	matematický	k2eAgFnPc2d1	matematická
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
babiččině	babiččin	k2eAgFnSc6d1	babiččina
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Lüneburgu	Lüneburg	k1gInSc6	Lüneburg
<g/>
.	.	kIx.	.
</s>
<s>
Blízkost	blízkost	k1gFnSc1	blízkost
domova	domov	k1gInSc2	domov
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
trávit	trávit	k5eAaImF	trávit
prázdniny	prázdniny	k1gFnPc4	prázdniny
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
činila	činit	k5eAaImAgFnS	činit
jeho	jeho	k3xOp3gNnSc4	jeho
pozdější	pozdní	k2eAgNnSc4d2	pozdější
školní	školní	k2eAgNnSc4d1	školní
léta	léto	k1gNnPc4	léto
poněkud	poněkud	k6eAd1	poněkud
šťastnějšími	šťastný	k2eAgInPc7d2	šťastnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
byl	být	k5eAaImAgInS	být
úspěšně	úspěšně	k6eAd1	úspěšně
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
filologii	filologie	k1gFnSc4	filologie
a	a	k8xC	a
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaPmF	stát
knězem	kněz	k1gMnSc7	kněz
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
tím	ten	k3xDgNnSc7	ten
zajistit	zajistit	k5eAaPmF	zajistit
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
mladého	mladý	k1gMnSc4	mladý
Riemanna	Riemann	k1gInSc2	Riemann
jednu	jeden	k4xCgFnSc4	jeden
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
domovem	domov	k1gInSc7	domov
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
matematiků	matematik	k1gMnPc2	matematik
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
možná	možná	k9	možná
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
Carla	Carl	k1gMnSc2	Carl
Friedricha	Friedrich	k1gMnSc2	Friedrich
Gausse	gauss	k1gInSc5	gauss
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
Gaussovi	Gaussův	k2eAgMnPc1d1	Gaussův
už	už	k9	už
69	[number]	k4	69
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
období	období	k1gNnSc1	období
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
minulostí	minulost	k1gFnPc2	minulost
a	a	k8xC	a
přednášel	přednášet	k5eAaImAgMnS	přednášet
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
Riemann	Riemann	k1gNnSc1	Riemann
jeho	jeho	k3xOp3gFnSc2	jeho
přednášky	přednáška	k1gFnSc2	přednáška
z	z	k7c2	z
metody	metoda	k1gFnSc2	metoda
nejmenších	malý	k2eAgInPc2d3	nejmenší
čtverců	čtverec	k1gInPc2	čtverec
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
vášeň	vášeň	k1gFnSc4	vášeň
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Riemann	Riemann	k1gInSc1	Riemann
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
okamžiku	okamžik	k1gInSc6	okamžik
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1846-1847	[number]	k4	1846-1847
musel	muset	k5eAaImAgMnS	muset
svěřit	svěřit	k5eAaPmF	svěřit
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
zajímá	zajímat	k5eAaImIp3nS	zajímat
matematika	matematika	k1gFnSc1	matematika
daleko	daleko	k6eAd1	daleko
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
teologie	teologie	k1gFnSc1	teologie
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
dal	dát	k5eAaPmAgMnS	dát
svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
matematika	matematika	k1gFnSc1	matematika
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gNnPc3	jeho
povoláním	povolání	k1gNnPc3	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
odešel	odejít	k5eAaPmAgMnS	odejít
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
učili	učít	k5eAaPmAgMnP	učít
Jacobi	Jacobe	k1gFnSc4	Jacobe
<g/>
,	,	kIx,	,
Dirichlet	Dirichlet	k1gInSc1	Dirichlet
a	a	k8xC	a
Steiner	Steiner	k1gMnSc1	Steiner
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
se	se	k3xPyFc4	se
však	však	k9	však
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Göttingenu	Göttingen	k1gInSc2	Göttingen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dospělost	dospělost	k1gFnSc4	dospělost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
Riemann	Riemann	k1gInSc1	Riemann
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
samotného	samotný	k2eAgNnSc2d1	samotné
Gausse	gauss	k1gInSc5	gauss
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
25	[number]	k4	25
let	léto	k1gNnPc2	léto
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
univerzitní	univerzitní	k2eAgFnSc4d1	univerzitní
přednášku	přednáška	k1gFnSc4	přednáška
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
vedl	vést	k5eAaImAgInS	vést
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
univerzitě	univerzita	k1gFnSc6	univerzita
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
profesorem	profesor	k1gMnSc7	profesor
řádným	řádný	k2eAgMnSc7d1	řádný
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
pracemi	práce	k1gFnPc7	práce
na	na	k7c6	na
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dnes	dnes	k6eAd1	dnes
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
<g/>
,	,	kIx,	,
a	a	k8xC	a
svojí	svojit	k5eAaImIp3nS	svojit
teorií	teorie	k1gFnSc7	teorie
vyšších	vysoký	k2eAgFnPc2d2	vyšší
dimenzí	dimenze	k1gFnPc2	dimenze
připravil	připravit	k5eAaPmAgMnS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
Alberta	Albert	k1gMnSc4	Albert
Einsteina	Einstein	k1gMnSc4	Einstein
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Průlomovým	průlomový	k2eAgInSc7d1	průlomový
okamžikem	okamžik	k1gInSc7	okamžik
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Riemanna	Riemann	k1gMnSc4	Riemann
rok	rok	k1gInSc4	rok
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
článek	článek	k1gInSc1	článek
z	z	k7c2	z
analýzy	analýza	k1gFnSc2	analýza
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnSc1	teorie
abelovských	abelovský	k2eAgFnPc2d1	abelovská
funkcí	funkce	k1gFnPc2	funkce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
okamžitě	okamžitě	k6eAd1	okamžitě
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
zásadní	zásadní	k2eAgInSc4d1	zásadní
příspěvek	příspěvek	k1gInSc4	příspěvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svými	svůj	k3xOyFgInPc7	svůj
třicátými	třicátý	k4xOgInPc7	třicátý
třetími	třetí	k4xOgFnPc7	třetí
narozeninami	narozeniny	k1gFnPc7	narozeniny
byl	být	k5eAaImAgInS	být
jmenován	jmenován	k2eAgInSc1d1	jmenován
také	také	k6eAd1	také
členem	člen	k1gMnSc7	člen
korespondentem	korespondent	k1gMnSc7	korespondent
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
učinila	učinit	k5eAaPmAgFnS	učinit
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pouze	pouze	k6eAd1	pouze
dvou	dva	k4xCgFnPc2	dva
Riemannových	Riemannův	k2eAgFnPc2d1	Riemannova
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
dobře	dobře	k6eAd1	dobře
známy	znám	k2eAgFnPc1d1	známa
<g/>
:	:	kIx,	:
na	na	k7c6	na
doktorské	doktorský	k2eAgFnSc6d1	doktorská
disertaci	disertace	k1gFnSc6	disertace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
a	a	k8xC	a
na	na	k7c6	na
článku	článek	k1gInSc6	článek
o	o	k7c6	o
abelovských	abelovský	k2eAgFnPc6d1	abelovská
funkcích	funkce	k1gFnPc6	funkce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
členství	členství	k1gNnSc1	členství
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
velkou	velký	k2eAgFnSc7d1	velká
ctí	čest	k1gFnSc7	čest
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
svoji	svůj	k3xOyFgFnSc4	svůj
vděčnost	vděčnost	k1gFnSc1	vděčnost
předložením	předložení	k1gNnSc7	předložení
práce	práce	k1gFnSc2	práce
popisující	popisující	k2eAgInSc4d1	popisující
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
nový	nový	k2eAgMnSc1d1	nový
člen	člen	k1gMnSc1	člen
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Riemann	Riemann	k1gMnSc1	Riemann
předložil	předložit	k5eAaPmAgMnS	předložit
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Über	Über	k1gMnSc1	Über
die	die	k?	die
Anzahl	Anzahl	k1gMnSc1	Anzahl
der	drát	k5eAaImRp2nS	drát
Primzahlen	Primzahlen	k2eAgMnSc1d1	Primzahlen
unter	unter	k1gMnSc1	unter
einer	einer	k1gMnSc1	einer
gegebenen	gegebenen	k2eAgMnSc1d1	gegebenen
Grösse	Grösse	k1gFnPc4	Grösse
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
počtu	počet	k1gInSc6	počet
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
daná	daný	k2eAgFnSc1d1	daná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stejné	stejný	k2eAgNnSc4d1	stejné
téma	téma	k1gNnSc4	téma
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
přednášku	přednáška	k1gFnSc4	přednáška
v	v	k7c6	v
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
triumfem	triumf	k1gInSc7	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Elise	elise	k1gFnPc1	elise
Kochovou	Kochová	k1gFnSc4	Kochová
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
v	v	k7c6	v
Selasce	Selaska	k1gFnSc6	Selaska
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
třetí	třetí	k4xOgFnSc6	třetí
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
hypotéza	hypotéza	k1gFnSc1	hypotéza
</s>
</p>
<p>
<s>
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
funkce	funkce	k1gFnSc1	funkce
zeta	zet	k1gMnSc2	zet
</s>
</p>
<p>
<s>
Riemannův	Riemannův	k2eAgInSc1d1	Riemannův
integrál	integrál	k1gInSc1	integrál
</s>
</p>
<p>
<s>
Riemannova	Riemannův	k2eAgFnSc1d1	Riemannova
koule	koule	k1gFnSc1	koule
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Bernhard	Bernhard	k1gMnSc1	Bernhard
Riemann	Riemann	k1gMnSc1	Riemann
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bernhard	Bernhard	k1gMnSc1	Bernhard
Riemann	Riemann	k1gMnSc1	Riemann
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bernhard	Bernharda	k1gFnPc2	Bernharda
Riemann	Riemann	k1gNnSc4	Riemann
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
