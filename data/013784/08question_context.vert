<s desamb="1">
Károly	Károla	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Karel	Karel	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hubert	Hubert	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Oto	Oto	k1gMnSc1
Maria	Mario	k1gMnSc4
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgInSc1d1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Karl	Karl	k1gMnSc1
Franz	Franz	k1gMnSc1
Josef	Josef	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Hubert	Hubert	k1gMnSc1
Georg	Georg	k1gMnSc1
Otto	Otto	k1gMnSc1
Maria	Maria	k1gFnSc1
von	von	k1gInSc4
Habsburg-Lothringen	Habsburg-Lothringen	k1gInSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1887	#num#	k4
<g/>
,	,	kIx,
zámek	zámek	k1gInSc1
Persenbeug	Persenbeug	k1gInSc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1922	#num#	k4
<g/>
,	,	kIx,
Monte	Mont	k1gMnSc5
<g/>
,	,	kIx,
Funchal	Funchal	k1gFnSc1
<g/>
,	,	kIx,
Madeira	Madeira	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
habsbursko-lotrinské	habsbursko-lotrinský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
poslední	poslední	k2eAgMnSc1d1
císař	císař	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
apoštolský	apoštolský	k2eAgMnSc1d1
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
atd.	atd.	kA
</s>