<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
posledním	poslední	k2eAgMnSc6d1
císaři	císař	k1gMnSc6
rakouském	rakouský	k2eAgMnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
</s>
<s>
Císař	Císař	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
apoštolský	apoštolský	k2eAgMnSc1d1
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
chorvatsko-slavonský	chorvatsko-slavonský	k2eAgMnSc1d1
<g/>
,	,	kIx,
dalmátský	dalmátský	k2eAgMnSc1d1
<g/>
,	,	kIx,
haličsko-vladiměřský	haličsko-vladiměřský	k2eAgMnSc1d1
<g/>
,	,	kIx,
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
atd.	atd.	kA
</s>
<s>
Císař	Císař	k1gMnSc1
Karel	Karel	k1gMnSc1
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1916	#num#	k4
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
<g/>
(	(	kIx(
<g/>
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Karl	Karl	k1gMnSc1
Franz	Franz	k1gMnSc1
Josef	Josef	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Hubert	Hubert	k1gMnSc1
Georg	Georg	k1gMnSc1
Maria	Maria	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1887	#num#	k4
</s>
<s>
Persenbeug	Persenbeug	k1gInSc1
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dolní	dolní	k2eAgNnSc1d1
Rakousko	Rakousko	k1gNnSc1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gMnSc5
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1922	#num#	k4
(	(	kIx(
<g/>
34	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Monte	Monit	k5eAaBmRp2nP,k5eAaImRp2nP,k5eAaPmRp2nP
<g/>
,	,	kIx,
Funchal	Funchal	k1gFnSc1
<g/>
,	,	kIx,
Madeira	Madeira	k1gFnSc1
Portugalsko	Portugalsko	k1gNnSc1
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Kostel	kostel	k1gInSc1
Nossa	Noss	k1gMnSc2
Senhora	Senhor	k1gMnSc2
na	na	k7c6
Monte	Mont	k1gMnSc5
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
–	–	k?
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Zita	Zita	k1gFnSc1
Bourbonsko-Parmská	bourbonsko-parmský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
OtoAdelheidRobertFelixKarel	OtoAdelheidRobertFelixKarel	k1gMnSc1
LudvíkRudolf	LudvíkRudolf	k1gMnSc1
SyringusŠarlotaAlžběta	SyringusŠarlotaAlžběta	k1gMnSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
</s>
<s>
Hymna	hymna	k1gFnSc1
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Arcivévoda	arcivévoda	k1gMnSc1
Ota	Ota	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Blahoslavený	blahoslavený	k2eAgMnSc1d1
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
označovaný	označovaný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Károly	Károla	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Karel	Karel	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hubert	Hubert	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Oto	Oto	k1gMnSc1
Maria	Mario	k1gMnSc4
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgInSc1d1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Karl	Karl	k1gMnSc1
Franz	Franz	k1gMnSc1
Josef	Josef	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Hubert	Hubert	k1gMnSc1
Georg	Georg	k1gMnSc1
Otto	Otto	k1gMnSc1
Maria	Maria	k1gFnSc1
von	von	k1gInSc4
Habsburg-Lothringen	Habsburg-Lothringen	k1gInSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1887	#num#	k4
<g/>
,	,	kIx,
zámek	zámek	k1gInSc1
Persenbeug	Persenbeug	k1gInSc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1922	#num#	k4
<g/>
,	,	kIx,
Monte	Mont	k1gMnSc5
<g/>
,	,	kIx,
Funchal	Funchal	k1gFnSc1
<g/>
,	,	kIx,
Madeira	Madeira	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
habsbursko-lotrinské	habsbursko-lotrinský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
poslední	poslední	k2eAgMnSc1d1
císař	císař	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
apoštolský	apoštolský	k2eAgMnSc1d1
král	král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
atd.	atd.	kA
</s>
<s>
Dětství	dětství	k1gNnSc2
a	a	k8xC
mládí	mládí	k1gNnSc2
Karel	Karel	k1gMnSc1
strávil	strávit	k5eAaPmAgMnS
převážně	převážně	k6eAd1
v	v	k7c6
Dolních	dolní	k2eAgInPc6d1
Rakousích	Rakousy	k1gInPc6
<g/>
,	,	kIx,
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
vedlejší	vedlejší	k2eAgFnSc2d1
větve	větev	k1gFnSc2
rodu	rod	k1gInSc2
se	se	k3xPyFc4
do	do	k7c2
přímé	přímý	k2eAgFnSc2d1
linie	linie	k1gFnSc2
následnictví	následnictví	k1gNnPc2
dostal	dostat	k5eAaPmAgInS
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
arcivévoda	arcivévoda	k1gMnSc1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
morganaticky	morganaticky	k6eAd1
oženil	oženit	k5eAaPmAgMnS
a	a	k8xC
tím	ten	k3xDgNnSc7
své	svůj	k3xOyFgInPc4
případné	případný	k2eAgInPc4d1
potomky	potomek	k1gMnPc7
z	z	k7c2
následnictví	následnictví	k1gNnSc2
vyloučil	vyloučit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
se	se	k3xPyFc4
Karel	Karel	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
se	s	k7c7
Zitou	Zita	k1gFnSc7
Bourbonsko-Parmskou	bourbonsko-parmský	k2eAgFnSc7d1
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
měl	mít	k5eAaImAgMnS
celkem	celkem	k6eAd1
8	#num#	k4
dětí	dítě	k1gFnPc2
včetně	včetně	k7c2
následníka	následník	k1gMnSc2
Oty	Ota	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
trůn	trůn	k1gInSc4
usedl	usednout	k5eAaPmAgMnS
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1916	#num#	k4
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
prastrýce	prastrýc	k1gMnSc2
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc2
I.	I.	kA
Fyzicky	fyzicky	k6eAd1
byl	být	k5eAaImAgInS
korunován	korunovat	k5eAaBmNgInS
pouze	pouze	k6eAd1
za	za	k7c2
uherského	uherský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
byla	být	k5eAaImAgFnS
poznamenána	poznamenat	k5eAaPmNgFnS
neustálou	neustálý	k2eAgFnSc7d1
snahou	snaha	k1gFnSc7
o	o	k7c6
zastavení	zastavení	k1gNnSc6
zuřící	zuřící	k2eAgFnSc2d1
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
vypuknutí	vypuknutí	k1gNnSc6
neměl	mít	k5eNaImAgInS
prakticky	prakticky	k6eAd1
žádný	žádný	k3yNgInSc1
podíl	podíl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc1
mírová	mírový	k2eAgNnPc1d1
jednání	jednání	k1gNnPc1
byla	být	k5eAaImAgNnP
ovšem	ovšem	k9
stejně	stejně	k6eAd1
neúspěšná	úspěšný	k2eNgNnPc1d1
jako	jako	k8xC,k8xS
jeho	jeho	k3xOp3gNnSc6
úsilí	úsilí	k1gNnSc6
o	o	k7c4
národnostní	národnostní	k2eAgInSc4d1
smír	smír	k1gInSc4
v	v	k7c6
monarchii	monarchie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1917	#num#	k4
svolal	svolat	k5eAaPmAgMnS
již	již	k9
několik	několik	k4yIc4
let	léto	k1gNnPc2
rozpuštěný	rozpuštěný	k2eAgInSc1d1
předlitavský	předlitavský	k2eAgInSc4d1
parlament	parlament	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
části	část	k1gFnSc6
monarchie	monarchie	k1gFnSc2
obnovil	obnovit	k5eAaPmAgInS
válkou	válka	k1gFnSc7
přerušený	přerušený	k2eAgInSc1d1
demokratický	demokratický	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc2
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
odejít	odejít	k5eAaPmF
do	do	k7c2
exilu	exil	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
svých	svůj	k3xOyFgInPc2
titulů	titul	k1gInPc2
a	a	k8xC
nároků	nárok	k1gInPc2
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nevzdal	vzdát	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osud	osud	k1gInSc1
Podunají	Podunají	k1gNnSc2
mu	on	k3xPp3gMnSc3
stále	stále	k6eAd1
ležel	ležet	k5eAaImAgMnS
na	na	k7c6
srdci	srdce	k1gNnSc6
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
rozdrobenost	rozdrobenost	k1gFnSc4
na	na	k7c4
malé	malý	k2eAgInPc4d1
národní	národní	k2eAgInPc4d1
státy	stát	k1gInPc4
vnímal	vnímat	k5eAaImAgInS
jako	jako	k9
rizikovou	rizikový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1921	#num#	k4
se	se	k3xPyFc4
dvakrát	dvakrát	k6eAd1
marně	marně	k6eAd1
pokusil	pokusit	k5eAaPmAgMnS
ujmout	ujmout	k5eAaPmF
vlády	vláda	k1gFnPc4
v	v	k7c6
poválečném	poválečný	k2eAgNnSc6d1
Uhersku	Uhersko	k1gNnSc6
(	(	kIx(
<g/>
tedy	tedy	k8xC
Maďarsku	Maďarsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
formálně	formálně	k6eAd1
zůstalo	zůstat	k5eAaPmAgNnS
monarchií	monarchie	k1gFnPc2
a	a	k8xC
Habsburky	Habsburk	k1gInPc4
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
oficiálně	oficiálně	k6eAd1
nesesadilo	sesadit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
musel	muset	k5eAaImAgInS
s	s	k7c7
celou	celý	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
do	do	k7c2
vyhnanství	vyhnanství	k1gNnSc2
až	až	k9
na	na	k7c4
odlehlý	odlehlý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
Madeira	Madeira	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
roku	rok	k1gInSc2
1922	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgInS
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
příkladný	příkladný	k2eAgInSc4d1
křesťanský	křesťanský	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
pro	pro	k7c4
své	své	k1gNnSc4
mírové	mírový	k2eAgFnSc2d1
snahy	snaha	k1gFnSc2
beatifikován	beatifikován	k2eAgInSc4d1
papežem	papež	k1gMnSc7
Janem	Jan	k1gMnSc7
Pavlem	Pavel	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
na	na	k7c4
něj	on	k3xPp3gMnSc4
každoročně	každoročně	k6eAd1
upomíná	upomínat	k5eAaImIp3nS
kulturní	kulturní	k2eAgFnSc1d1
akce	akce	k1gFnSc1
Audience	audience	k1gFnSc1
u	u	k7c2
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
pořádaná	pořádaný	k2eAgFnSc1d1
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Brandýse	Brandýs	k1gInSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
ještě	ještě	k9
jako	jako	k9
mladý	mladý	k2eAgMnSc1d1
arcivévoda	arcivévoda	k1gMnSc1
strávil	strávit	k5eAaPmAgMnS
několik	několik	k4yIc4
let	léto	k1gNnPc2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Číslování	číslování	k1gNnSc1
</s>
<s>
Ve	v	k7c6
většině	většina	k1gFnSc6
zemí	zem	k1gFnPc2
se	se	k3xPyFc4
čísluje	číslovat	k5eAaImIp3nS
jako	jako	k9
I.	I.	kA
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
je	být	k5eAaImIp3nS
míněno	míněn	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
mezi	mezi	k7c7
rakouskými	rakouský	k2eAgMnPc7d1
císaři	císař	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vymyká	vymykat	k5eAaImIp3nS
se	se	k3xPyFc4
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zásadně	zásadně	k6eAd1
počítá	počítat	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
uherských	uherský	k2eAgMnPc2d1
králů	král	k1gMnPc2
jako	jako	k9
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
po	po	k7c4
Karlu	Karla	k1gFnSc4
Robertovi	Robert	k1gMnSc3
<g/>
,	,	kIx,
Karlu	Karel	k1gMnSc3
z	z	k7c2
Drače	Drač	k1gInSc2
a	a	k8xC
Karlu	Karla	k1gFnSc4
VI	VI	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
navázalo	navázat	k5eAaPmAgNnS
na	na	k7c4
řadu	řada	k1gFnSc4
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
by	by	kYmCp3nS
Karlem	Karel	k1gMnSc7
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
v	v	k7c6
samotném	samotný	k2eAgNnSc6d1
Rakousku	Rakousko	k1gNnSc6
byl	být	k5eAaImAgMnS
panovníkem	panovník	k1gMnSc7
toho	ten	k3xDgNnSc2
jména	jméno	k1gNnSc2
třetím	třetí	k4xOgMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
třetím	třetí	k4xOgMnSc7
Karlem	Karel	k1gMnSc7
(	(	kIx(
<g/>
po	po	k7c4
Karlu	Karla	k1gFnSc4
IV	IV	kA
<g/>
.	.	kIx.
a	a	k8xC
Karlu	Karla	k1gFnSc4
VI	VI	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
označení	označení	k1gNnSc1
Karel	Karla	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
však	však	k9
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
prakticky	prakticky	k6eAd1
nepoužívá	používat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgNnSc2
byl	být	k5eAaImAgInS
za	za	k7c2
československé	československý	k2eAgFnSc2d1
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
často	často	k6eAd1
posměšně	posměšně	k6eAd1
nazýván	nazývat	k5eAaImNgInS
jako	jako	k9
„	„	k?
<g/>
Karel	Karel	k1gMnSc1
Poslední	poslední	k2eAgMnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Karel	Karel	k1gMnSc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
rodiči	rodič	k1gMnPc7
a	a	k8xC
mladším	mladý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c6
zámku	zámek	k1gInSc6
Persenbeugu	Persenbeug	k1gInSc2
v	v	k7c6
Dolních	dolní	k2eAgInPc6d1
Rakousích	Rakousy	k1gInPc6
jako	jako	k9
nejstarší	starý	k2eAgMnPc1d3
syn	syn	k1gMnSc1
arcivévody	arcivévoda	k1gMnSc2
Oty	Ota	k1gMnSc2
Františka	František	k1gMnSc2
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
–	–	k?
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Marie	Marie	k1gFnSc1
Josefy	Josefa	k1gFnSc2
Saské	saský	k2eAgFnSc2d1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
děd	děd	k1gMnSc1
arcivévoda	arcivévoda	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
–	–	k?
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
bratrem	bratr	k1gMnSc7
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgInS
tedy	tedy	k9
jeho	jeho	k3xOp3gMnSc7
prastrýcem	prastrýc	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc1d1
syn	syn	k1gMnSc1
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
I.	I.	kA
<g/>
,	,	kIx,
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
<g/>
,	,	kIx,
spáchal	spáchat	k5eAaPmAgInS
roku	rok	k1gInSc2
1889	#num#	k4
sebevraždu	sebevražda	k1gFnSc4
a	a	k8xC
další	další	k2eAgMnSc1d1
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
Karlův	Karlův	k2eAgMnSc1d1
strýc	strýc	k1gMnSc1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
byl	být	k5eAaImAgInS
zavražděn	zavražděn	k2eAgInSc1d1
roku	rok	k1gInSc2
1914	#num#	k4
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
děti	dítě	k1gFnPc1
však	však	k9
pocházely	pocházet	k5eAaImAgFnP
z	z	k7c2
morganatického	morganatický	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
s	s	k7c7
nerovnorodou	rovnorodý	k2eNgFnSc7d1
hraběnkou	hraběnka	k1gFnSc7
Chotkovou	Chotkův	k2eAgFnSc7d1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
po	po	k7c6
smrti	smrt	k1gFnSc6
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc2
I.	I.	kA
císařem	císař	k1gMnSc7
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Oty	Ota	k1gMnSc2
<g/>
,	,	kIx,
mladšího	mladý	k2eAgMnSc2d2
bratra	bratr	k1gMnSc2
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
do	do	k7c2
roku	rok	k1gInSc2
1916	#num#	k4
</s>
<s>
Malého	Malý	k1gMnSc4
Karla	Karel	k1gMnSc4
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
v	v	k7c6
nepřítomnosti	nepřítomnost	k1gFnSc6
otce	otec	k1gMnSc2
opatrovala	opatrovat	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
zbožná	zbožný	k2eAgFnSc1d1
matka	matka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
byl	být	k5eAaImAgMnS
nejprve	nejprve	k6eAd1
vychováván	vychovávat	k5eAaImNgMnS
domácími	domácí	k2eAgMnPc7d1
učiteli	učitel	k1gMnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgInPc4
patřil	patřit	k5eAaImAgMnS
i	i	k9
Godfried	Godfried	k1gMnSc1
Marschall	Marschall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
v	v	k7c6
letech	let	k1gInPc6
1899	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
navštěvoval	navštěvovat	k5eAaImAgInS
věhlasné	věhlasný	k2eAgNnSc4d1
prestižní	prestižní	k2eAgNnSc4d1
Skotské	skotský	k2eAgNnSc4d1
gymnázium	gymnázium	k1gNnSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgMnPc1d1
učitelé	učitel	k1gMnPc1
ho	on	k3xPp3gNnSc4
vychovávali	vychovávat	k5eAaImAgMnP
v	v	k7c6
přísně	přísně	k6eAd1
katolickém	katolický	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vídeňském	vídeňský	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
u	u	k7c2
skotských	skotská	k1gFnPc2
františkánů	františkán	k1gMnPc2
studoval	studovat	k5eAaImAgMnS
přírodovědné	přírodovědný	k2eAgInPc4d1
obory	obor	k1gInPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1905	#num#	k4
započal	započnout	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
vojenský	vojenský	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
u	u	k7c2
7	#num#	k4
<g/>
.	.	kIx.
dragounského	dragounský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
nejprve	nejprve	k6eAd1
v	v	k7c6
Chudeřicích	Chudeřice	k1gFnPc6
(	(	kIx(
<g/>
něm.	něm.	k?
Kutterschitz	Kutterschitz	k1gInSc1
<g/>
)	)	kIx)
u	u	k7c2
Bíliny	Bílina	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1906	#num#	k4
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc6
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
hodnosti	hodnost	k1gFnSc2
nadporučíka	nadporučík	k1gMnSc2
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1906	#num#	k4
přerušil	přerušit	k5eAaPmAgMnS
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nastoupil	nastoupit	k5eAaPmAgMnS
dvouleté	dvouletý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
na	na	k7c6
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1
universitě	universita	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
už	už	k6eAd1
byla	být	k5eAaImAgFnS
univerzita	univerzita	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
německou	německý	k2eAgFnSc4d1
a	a	k8xC
českou	český	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
nevznikaly	vznikat	k5eNaImAgFnP
národnostní	národnostní	k2eAgInPc4d1
rozbroje	rozbroj	k1gInPc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Karel	Karel	k1gMnSc1
vzděláván	vzdělávat	k5eAaImNgMnS
profesory	profesor	k1gMnPc7
z	z	k7c2
obou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
nedocházel	docházet	k5eNaImAgMnS
na	na	k7c4
přednášky	přednáška	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
profesoři	profesor	k1gMnPc1
jej	on	k3xPp3gMnSc4
navštěvovali	navštěvovat	k5eAaImAgMnP
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
tehdy	tehdy	k6eAd1
pobýval	pobývat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
ukončil	ukončit	k5eAaPmAgInS
právnickými	právnický	k2eAgFnPc7d1
zkouškami	zkouška	k1gFnPc7
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
doby	doba	k1gFnSc2
univerzitních	univerzitní	k2eAgNnPc2d1
studií	studio	k1gNnPc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
slavnostní	slavnostní	k2eAgNnSc4d1
vyhlášení	vyhlášení	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
plnoletosti	plnoletost	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xS,k8xC
bylo	být	k5eAaImAgNnS
zvykem	zvyk	k1gInSc7
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
vlastní	vlastní	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc7
vrchním	vrchní	k2eAgMnSc7d1
komořím	komoří	k1gMnSc7
knížete	kníže	k1gMnSc2
Zdenko	Zdenka	k1gFnSc5
Lobkowicze	Lobkowicz	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
s	s	k7c7
Karlem	Karel	k1gMnSc7
setrval	setrvat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
na	na	k7c6
Madeiře	Madeira	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
zemřel	zemřít	k5eAaPmAgMnS
Karlův	Karlův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
Otto	Otto	k1gMnSc1
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc7
opatrovníkem	opatrovník	k1gMnSc7
strýc	strýc	k1gMnSc1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
předtím	předtím	k6eAd1
roku	rok	k1gInSc2
1900	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
tzv.	tzv.	kA
morganatického	morganatický	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
(	(	kIx(
<g/>
tzn.	tzn.	kA
vzal	vzít	k5eAaPmAgInS
si	se	k3xPyFc3
manželku	manželka	k1gFnSc4
nerovného	rovný	k2eNgNnSc2d1
rodového	rodový	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
)	)	kIx)
s	s	k7c7
hraběnkou	hraběnka	k1gFnSc7
Žofií	Žofie	k1gFnSc7
Chotkovou	Chotková	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
tím	ten	k3xDgNnSc7
byly	být	k5eAaImAgFnP
děti	dítě	k1gFnPc4
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
vyloučeny	vyloučen	k2eAgInPc1d1
z	z	k7c2
následnictví	následnictví	k1gNnSc2
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tímto	tento	k3xDgMnSc7
Karel	Karel	k1gMnSc1
ve	v	k7c6
třinácti	třináct	k4xCc6
letech	léto	k1gNnPc6
po	po	k7c6
Františku	František	k1gMnSc6
Ferdinandovi	Ferdinand	k1gMnSc6
druhým	druhý	k4xOgNnSc7
v	v	k7c6
pořadí	pořadí	k1gNnSc6
v	v	k7c6
nástupnictví	nástupnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
1908	#num#	k4
nastoupil	nastoupit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
službu	služba	k1gFnSc4
u	u	k7c2
své	svůj	k3xOyFgFnSc2
vojenské	vojenský	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
souhlasným	souhlasný	k2eAgInSc7d1
zájmem	zájem	k1gInSc7
sledoval	sledovat	k5eAaImAgMnS
reformní	reformní	k2eAgInPc4d1
plány	plán	k1gInPc4
svého	svůj	k1gMnSc2
strýce	strýc	k1gMnSc2
<g/>
,	,	kIx,
arcivévody	arcivévoda	k1gMnSc2
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
<g/>
,	,	kIx,
politiky	politika	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
stranil	stranit	k5eAaImAgInS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
setkávalo	setkávat	k5eAaImAgNnS
s	s	k7c7
vřelým	vřelý	k2eAgInSc7d1
souhlasem	souhlas	k1gInSc7
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
až	až	k6eAd1
roku	rok	k1gInSc2
1911	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
vědomost	vědomost	k1gFnSc4
o	o	k7c6
důležitých	důležitý	k2eAgInPc6d1
zahraničně	zahraničně	k6eAd1
politických	politický	k2eAgInPc6d1
aktech	akt	k1gInPc6
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1913	#num#	k4
ho	on	k3xPp3gInSc4
směl	smět	k5eAaImAgMnS
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
informovat	informovat	k5eAaBmF
blíže	blízce	k6eAd2
o	o	k7c6
svých	svůj	k3xOyFgInPc6
reformních	reformní	k2eAgInPc6d1
plánech	plán	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
až	až	k9
do	do	k7c2
svého	svůj	k3xOyFgInSc2
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
nebyl	být	k5eNaImAgMnS
Karel	Karel	k1gMnSc1
účasten	účasten	k2eAgMnSc1d1
politických	politický	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnSc2
v	v	k7c6
monarchii	monarchie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zavraždění	zavraždění	k1gNnSc6
následníka	následník	k1gMnSc2
trůnu	trůn	k1gInSc2
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1914	#num#	k4
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
postoupil	postoupit	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
na	na	k7c4
místo	místo	k1gNnSc4
následníka	následník	k1gMnSc2
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
zůstal	zůstat	k5eAaPmAgInS
však	však	k9
nadále	nadále	k6eAd1
vyloučen	vyloučit	k5eAaPmNgMnS
z	z	k7c2
rozhodnutí	rozhodnutí	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
vypuknutí	vypuknutí	k1gNnSc3
války	válka	k1gFnSc2
<g/>
,	,	kIx,
i	i	k9
z	z	k7c2
vysoké	vysoký	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
1914	#num#	k4
byl	být	k5eAaImAgInS
přidělen	přidělit	k5eAaPmNgInS
k	k	k7c3
vrchnímu	vrchní	k2eAgNnSc3d1
velení	velení	k1gNnSc3
armády	armáda	k1gFnSc2
v	v	k7c6
Těšíně	Těšín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
polního	polní	k2eAgMnSc4d1
podmaršálka	podmaršálek	k1gMnSc4
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1916	#num#	k4
převzal	převzít	k5eAaPmAgInS
velení	velení	k1gNnSc4
20	#num#	k4
<g/>
.	.	kIx.
armádního	armádní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
na	na	k7c6
počátečních	počáteční	k2eAgInPc6d1
úspěších	úspěch	k1gInPc6
nešťastné	šťastný	k2eNgFnSc2d1
italské	italský	k2eAgFnSc2d1
ofenzívy	ofenzíva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mála	málo	k4c2
panovníků	panovník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
osobně	osobně	k6eAd1
zúčastnili	zúčastnit	k5eAaPmAgMnP
bojů	boj	k1gInPc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
říkal	říkat	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Ota	Ota	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
v	v	k7c6
jednom	jeden	k4xCgInSc6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
české	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
…	…	k?
<g/>
přišel	přijít	k5eAaPmAgInS
sice	sice	k8xC
pozdě	pozdě	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
aspoň	aspoň	k9
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
<g/>
…	…	k?
</s>
<s>
“	“	k?
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
děti	dítě	k1gFnPc1
</s>
<s>
Svatba	svatba	k1gFnSc1
za	za	k7c2
účasti	účast	k1gFnSc2
císaře	císař	k1gMnSc2
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1911	#num#	k4
se	se	k3xPyFc4
Karel	Karel	k1gMnSc1
ve	v	k7c6
Villa	Vill	k1gMnSc2
delle	della	k1gFnSc6
Pianore	Pianor	k1gInSc5
u	u	k7c2
Lukky	Lukka	k1gFnSc2
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
zasnoubil	zasnoubit	k5eAaPmAgMnS
se	s	k7c7
Zitou	Zita	k1gFnSc7
Bourbonsko-Parmskou	bourbonsko-parmský	k2eAgFnSc7d1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
oženil	oženit	k5eAaPmAgMnS
na	na	k7c6
zámku	zámek	k1gInSc6
Schwarzau	Schwarzaus	k1gInSc2
am	am	k?
Steinfeld	Steinfeld	k1gInSc1
(	(	kIx(
<g/>
Dolní	dolní	k2eAgNnSc1d1
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
svatebního	svatební	k2eAgInSc2d1
obřadu	obřad	k1gInSc2
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
záznam	záznam	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
provedl	provést	k5eAaPmAgMnS
zakladatel	zakladatel	k1gMnSc1
rakouského	rakouský	k2eAgInSc2d1
filmového	filmový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
hrabě	hrabě	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Kolowrat-Krakowský	Kolowrat-Krakowský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc4
volba	volba	k1gFnSc1
„	„	k?
<g/>
Italky	Italka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k6eAd1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
označována	označovat	k5eAaImNgFnS
odpůrci	odpůrce	k1gMnPc1
tohoto	tento	k3xDgInSc2
svazku	svazek	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
války	válka	k1gFnSc2
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc2
Itálií	Itálie	k1gFnSc7
roku	rok	k1gInSc2
1915	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
mínění	mínění	k1gNnPc2
kritiků	kritik	k1gMnPc2
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
nežádoucímu	žádoucí	k2eNgNnSc3d1
mezinárodnímu	mezinárodní	k2eAgNnSc3d1
připoutání	připoutání	k1gNnSc3
domu	dům	k1gInSc2
Habsbursko-Lotrinského	habsbursko-lotrinský	k2eAgInSc2d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
Zita	Zita	k1gFnSc1
pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
<g/>
)	)	kIx)
nevládnoucí	vládnoucí	k2eNgFnSc2d1
vévodské	vévodský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
ze	z	k7c2
země	zem	k1gFnSc2
nepřátelské	přátelský	k2eNgFnSc2d1
k	k	k7c3
Rakousku	Rakousko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
manželství	manželství	k1gNnSc2
vzešlo	vzejít	k5eAaPmAgNnS
osm	osm	k4xCc1
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Otto	Otto	k1gMnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1951	#num#	k4
Regina	Regina	k1gFnSc1
<g/>
,	,	kIx,
princezna	princezna	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adelheid	Adelheida	k1gFnPc2
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
Rakouský-d	Rakouský-d	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1953	#num#	k4
Markéta	Markéta	k1gFnSc1
Savojská-Aosta	Savojská-Aosta	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1930	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Felix	Felix	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
∞	∞	k?
1952	#num#	k4
Anna	Anna	k1gFnSc1
Evženie	Evženie	k1gFnSc1
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1950	#num#	k4
Yolanda	Yolanda	k1gFnSc1
z	z	k7c2
Ligne	Lign	k1gInSc5
(	(	kIx(
<g/>
*	*	kIx~
1923	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudolph	Rudolph	k1gMnSc1
Syringus	Syringus	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
1953	#num#	k4
Xenia	Xenia	k1gFnSc1
Černyšev	Černyšev	k1gFnSc1
Bezobrazova	Bezobrazův	k2eAgFnSc1d1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
1971	#num#	k4
Anna	Anna	k1gFnSc1
Gabriele	Gabriela	k1gFnSc6
princezna	princezna	k1gFnSc1
z	z	k7c2
Wredy	Wreda	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charlotte	Charlott	k1gInSc5
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1956	#num#	k4
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Meklenburský	meklenburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elisabeth	Elisabeth	k1gInSc1
Charlotte	Charlott	k1gInSc5
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
∞	∞	k?
1949	#num#	k4
Jindřich	Jindřich	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
Lichtenštejnský	lichtenštejnský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vláda	vláda	k1gFnSc1
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
a	a	k8xC
Zita	Zita	k1gMnSc1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
nejstarším	starý	k2eAgMnSc7d3
synem	syn	k1gMnSc7
<g/>
,	,	kIx,
korunním	korunní	k2eAgMnSc7d1
princem	princ	k1gMnSc7
Otou	Ota	k1gMnSc7
<g/>
,	,	kIx,
po	po	k7c6
uherské	uherský	k2eAgFnSc6d1
korunovaci	korunovace	k1gFnSc6
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1916	#num#	k4
<g/>
)	)	kIx)
usedl	usednout	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
na	na	k7c4
trůn	trůn	k1gInSc4
rakousko-uherského	rakousko-uherský	k2eAgNnSc2d1
mocnářství	mocnářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
plný	plný	k2eAgInSc1d1
titul	titul	k1gInSc1
zněl	znět	k5eAaImAgInS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Karel	Karel	k1gMnSc1
I.	I.	kA
z	z	k7c2
milosti	milost	k1gFnSc2
Boží	božit	k5eAaImIp3nS
císař	císař	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
uherský	uherský	k2eAgMnSc1d1
apoštolský	apoštolský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
dalmatský	dalmatský	k2eAgMnSc1d1
<g/>
,	,	kIx,
chorvatský	chorvatský	k2eAgMnSc1d1
<g/>
,	,	kIx,
slavonský	slavonský	k2eAgMnSc1d1
<g/>
,	,	kIx,
haličský	haličský	k2eAgMnSc1d1
<g/>
,	,	kIx,
lodoměřský	lodoměřský	k2eAgMnSc1d1
<g/>
,	,	kIx,
illyrský	illyrský	k2eAgMnSc1d1
a	a	k8xC
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
arcivévoda	arcivévoda	k1gMnSc1
rakouský	rakouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
velkovévoda	velkovévoda	k1gMnSc1
toskánský	toskánský	k2eAgMnSc1d1
a	a	k8xC
krakovský	krakovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
lotrinský	lotrinský	k2eAgInSc1d1
<g/>
,	,	kIx,
salcburský	salcburský	k2eAgInSc1d1
<g/>
,	,	kIx,
štýrský	štýrský	k2eAgInSc1d1
<g/>
,	,	kIx,
korutanský	korutanský	k2eAgInSc1d1
<g/>
,	,	kIx,
kraňský	kraňský	k2eAgInSc1d1
a	a	k8xC
bukovinský	bukovinský	k2eAgInSc1d1
<g/>
,	,	kIx,
velkokníže	velkokníže	k1gMnSc1
sedmihradský	sedmihradský	k2eAgMnSc1d1
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
horního	horní	k2eAgMnSc2d1
a	a	k8xC
dolního	dolní	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
modenský	modenský	k2eAgInSc4d1
<g/>
,	,	kIx,
parmský	parmský	k2eAgInSc4d1
<g/>
,	,	kIx,
piacenzský	piacenzský	k2eAgInSc4d1
a	a	k8xC
guastellský	guastellský	k2eAgInSc4d1
<g/>
,	,	kIx,
osvětimský	osvětimský	k2eAgInSc4d1
a	a	k8xC
zatorský	zatorský	k2eAgInSc4d1
<g/>
,	,	kIx,
těšínský	těšínský	k2eAgInSc4d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
friulský	friulský	k2eAgMnSc1d1
<g/>
,	,	kIx,
raguszský	raguszský	k2eAgMnSc1d1
a	a	k8xC
zarský	zarský	k2eAgMnSc1d1
<g/>
,	,	kIx,
okněžněný	okněžněný	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
habsburský	habsburský	k2eAgMnSc1d1
a	a	k8xC
tyrolský	tyrolský	k2eAgMnSc1d1
<g/>
,	,	kIx,
kyburský	kyburský	k2eAgMnSc1d1
<g/>
,	,	kIx,
gorizijský	gorizijský	k2eAgMnSc1d1
a	a	k8xC
gradišský	gradišský	k2eAgMnSc1d1
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
tridentský	tridentský	k2eAgMnSc1d1
a	a	k8xC
brixenský	brixenský	k2eAgMnSc1d1
<g/>
,	,	kIx,
markrabě	markrabě	k1gMnSc1
horní	horní	k2eAgFnSc2d1
a	a	k8xC
dolní	dolní	k2eAgFnSc2d1
Lužice	Lužice	k1gFnSc2
a	a	k8xC
Istrie	Istrie	k1gFnSc2
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
hohenemský	hohenemský	k2eAgMnSc1d1
<g/>
,	,	kIx,
feldkirchský	feldkirchský	k2eAgMnSc1d1
<g/>
,	,	kIx,
bregenzský	bregenzský	k2eAgMnSc1d1
<g/>
,	,	kIx,
sonnenberský	sonnenberský	k2eAgMnSc1d1
<g/>
,	,	kIx,
pán	pán	k1gMnSc1
z	z	k7c2
Terstu	Terst	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
Cattara	Cattara	k1gFnSc1
<g/>
,	,	kIx,
velkovojvoda	velkovojvoda	k1gMnSc1
Vojvodiny	Vojvodina	k1gFnSc2
srbské	srbský	k2eAgFnSc2d1
<g/>
…	…	k?
<g/>
“	“	k?
</s>
<s>
Plány	plán	k1gInPc1
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
na	na	k7c4
prosazení	prosazení	k1gNnSc4
nezbytně	zbytně	k6eNd1,k6eAd1
nutných	nutný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
v	v	k7c6
uherské	uherský	k2eAgFnSc6d1
ústavě	ústava	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
autonomie	autonomie	k1gFnSc2
pro	pro	k7c4
utlačované	utlačovaný	k2eAgFnPc4d1
národnostní	národnostní	k2eAgFnPc4d1
menšiny	menšina	k1gFnPc4
<g/>
)	)	kIx)
ještě	ještě	k6eAd1
před	před	k7c7
jeho	jeho	k3xOp3gFnSc7
korunovací	korunovace	k1gFnSc7
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
se	se	k3xPyFc4
však	však	k9
nemohly	moct	k5eNaImAgFnP
uskutečnit	uskutečnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgMnSc1d1
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
István	István	k2eAgMnSc1d1
Tisza	Tisza	k1gFnSc1
Karla	Karel	k1gMnSc4
totiž	totiž	k9
přesvědčil	přesvědčit	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
již	již	k6eAd1
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
korunovat	korunovat	k5eAaBmF
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
jako	jako	k8xS,k8xC
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
maďarsky	maďarsky	k6eAd1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Károly	Károla	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
pak	pak	k6eAd1
měl	mít	k5eAaImAgMnS
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
do	do	k7c2
budoucna	budoucno	k1gNnSc2
v	v	k7c6
uherské	uherský	k2eAgFnSc6d1
části	část	k1gFnSc6
monarchie	monarchie	k1gFnSc2
značně	značně	k6eAd1
svázané	svázaný	k2eAgFnPc4d1
ruce	ruka	k1gFnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
v	v	k7c6
korunovační	korunovační	k2eAgFnSc6d1
přísaze	přísaha	k1gFnSc6
zavázal	zavázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
hájit	hájit	k5eAaImF
integritu	integrita	k1gFnSc4
a	a	k8xC
nedotknutelnost	nedotknutelnost	k1gFnSc4
zemí	zem	k1gFnPc2
svatoštěpánské	svatoštěpánský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc7
korunovací	korunovace	k1gFnSc7
zmařil	zmařit	k5eAaPmAgInS
i	i	k9
smělé	smělý	k2eAgInPc4d1
plány	plán	k1gInPc4
svého	svůj	k1gMnSc2
strýce	strýc	k1gMnSc2
na	na	k7c4
revizi	revize	k1gFnSc4
<g/>
,	,	kIx,
či	či	k8xC
přímo	přímo	k6eAd1
odstranění	odstranění	k1gNnSc1
rakousko-uherského	rakousko-uherský	k2eAgInSc2d1
dualismu	dualismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalekosáhlé	dalekosáhlý	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
Uherska	Uhersko	k1gNnSc2
pak	pak	k6eAd1
už	už	k6eAd1
nemohly	moct	k5eNaImAgFnP
být	být	k5eAaImF
z	z	k7c2
Karlovy	Karlův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
ústavní	ústavní	k2eAgFnSc2d1
cestou	cesta	k1gFnSc7
podniknuty	podniknout	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
politických	politický	k2eAgFnPc2d1
chyb	chyba	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
rámci	rámec	k1gInSc6
proklamované	proklamovaný	k2eAgFnSc2d1
integrity	integrita	k1gFnSc2
Uherska	Uhersko	k1gNnSc2
se	se	k3xPyFc4
skrývalo	skrývat	k5eAaImAgNnS
nerovnoprávné	rovnoprávný	k2eNgNnSc1d1
začlenění	začlenění	k1gNnSc1
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
zejména	zejména	k9
Chorvatů	Chorvat	k1gMnPc2
a	a	k8xC
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vladařský	vladařský	k2eAgInSc1d1
styl	styl	k1gInSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
obhospodařoval	obhospodařovat	k5eAaImAgInS
všechny	všechen	k3xTgFnPc4
záležitosti	záležitost	k1gFnPc4
sám	sám	k3xTgMnSc1
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
pracovny	pracovna	k1gFnSc2
vídeňského	vídeňský	k2eAgInSc2d1
Hofburgu	Hofburg	k1gInSc2
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
I.	I.	kA
nepřevzal	převzít	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
pravidelně	pravidelně	k6eAd1
předsedal	předsedat	k5eAaImAgMnS
zasedáním	zasedání	k1gNnSc7
společné	společný	k2eAgFnSc2d1
Rady	rada	k1gFnSc2
ministrů	ministr	k1gMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
rozhodovala	rozhodovat	k5eAaImAgFnS
o	o	k7c6
vnějších	vnější	k2eAgFnPc6d1
a	a	k8xC
válečných	válečný	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
Otakara	Otakar	k1gMnSc4
hraběte	hrabě	k1gMnSc4
Černína	Černín	k1gMnSc4
ministrem	ministr	k1gMnSc7
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poměrně	poměrně	k6eAd1
neobvyklá	obvyklý	k2eNgFnSc1d1
byla	být	k5eAaImAgFnS
také	také	k9
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Karel	Karel	k1gMnSc1
o	o	k7c6
všech	všecek	k3xTgNnPc6
důležitých	důležitý	k2eAgNnPc6d1
rozhodnutích	rozhodnutí	k1gNnPc6
radil	radit	k5eAaImAgInS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
Zitou	Zita	k1gFnSc7
a	a	k8xC
její	její	k3xOp3gInPc4
názory	názor	k1gInPc4
bral	brát	k5eAaImAgMnS
vážně	vážně	k6eAd1
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
byl	být	k5eAaImAgInS
některými	některý	k3yIgMnPc7
lidmi	člověk	k1gMnPc7
vysmíván	vysmíván	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
u	u	k7c2
mnoha	mnoho	k4c2
porad	porada	k1gFnPc2
byla	být	k5eAaImAgFnS
Zita	Zita	k1gFnSc1
přítomna	přítomen	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
posluchačka	posluchačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1
sociálněpolitická	sociálněpolitický	k2eAgNnPc1d1
opatření	opatření	k1gNnPc1
z	z	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
Zákon	zákon	k1gInSc1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
nájemníků	nájemník	k1gMnPc2
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vytvoření	vytvoření	k1gNnSc4
Ministerstva	ministerstvo	k1gNnSc2
sociální	sociální	k2eAgFnSc2d1
péče	péče	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
)	)	kIx)
či	či	k8xC
Ministerstva	ministerstvo	k1gNnSc2
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
<g/>
)	)	kIx)
přežily	přežít	k5eAaPmAgFnP
i	i	k9
zánik	zánik	k1gInSc1
monarchie	monarchie	k1gFnSc2
a	a	k8xC
existují	existovat	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viktor	Viktor	k1gMnSc1
Mataja	Mataja	k1gMnSc1
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
sociálním	sociální	k2eAgMnSc7d1
ministrem	ministr	k1gMnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
Karel	Karel	k1gMnSc1
osobně	osobně	k6eAd1
připíná	připínat	k5eAaImIp3nS
vyznamenání	vyznamenání	k1gNnSc4
vojákům	voják	k1gMnPc3
na	na	k7c6
italské	italský	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
všech	všecek	k3xTgMnPc2
ostatních	ostatní	k2eAgMnPc2d1
válečných	válečný	k2eAgMnPc2d1
velitelů	velitel	k1gMnPc2
měl	mít	k5eAaImAgMnS
prostřednictvím	prostřednictvím	k7c2
četných	četný	k2eAgFnPc2d1
návštěv	návštěva	k1gFnPc2
bojové	bojový	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
velký	velký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c4
vedení	vedení	k1gNnSc4
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
převzal	převzít	k5eAaPmAgInS
vrchní	vrchní	k2eAgInSc1d1
velení	velení	k1gNnSc2
nad	nad	k7c7
armádou	armáda	k1gFnSc7
a	a	k8xC
hlavní	hlavní	k2eAgInSc1d1
válečný	válečný	k2eAgInSc1d1
stan	stan	k1gInSc1
přesunul	přesunout	k5eAaPmAgInS
z	z	k7c2
Těšína	Těšín	k1gInSc2
do	do	k7c2
Badenu	Baden	k1gInSc2
u	u	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1917	#num#	k4
odvolal	odvolat	k5eAaPmAgMnS
velitele	velitel	k1gMnSc4
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
Conrada	Conrada	k1gFnSc1
von	von	k1gInSc4
Hötzendorf	Hötzendorf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohl	moct	k5eAaImAgMnS
tím	ten	k3xDgInSc7
zamezit	zamezit	k5eAaPmF
podobnému	podobný	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
jako	jako	k9
v	v	k7c6
Německé	německý	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
politiku	politika	k1gFnSc4
řídili	řídit	k5eAaImAgMnP
hlavně	hlavně	k9
generálové	generál	k1gMnPc1
Paul	Paula	k1gFnPc2
von	von	k1gInSc1
Hindenburg	Hindenburg	k1gMnSc1
a	a	k8xC
Erich	Erich	k1gMnSc1
Ludendorff	Ludendorff	k1gMnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
císař	císař	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
měl	mít	k5eAaImAgInS
jen	jen	k9
malý	malý	k2eAgInSc1d1
vliv	vliv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
i	i	k9
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
před	před	k7c7
Karlovým	Karlův	k2eAgInSc7d1
nástupem	nástup	k1gInSc7
k	k	k7c3
moci	moc	k1gFnSc3
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
vojenských	vojenský	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnPc2
zcela	zcela	k6eAd1
závislé	závislý	k2eAgInPc1d1
na	na	k7c6
vrchním	vrchní	k2eAgNnSc6d1
velení	velení	k1gNnSc6
německé	německý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Díky	dík	k1gInPc1
své	svůj	k3xOyFgFnPc4
schopnosti	schopnost	k1gFnPc4
prohloubeného	prohloubený	k2eAgInSc2d1
vhledu	vhled	k1gInSc2
stále	stále	k6eAd1
zřetelněji	zřetelně	k6eAd2
rozpoznával	rozpoznávat	k5eAaImAgMnS
bezvýchodnost	bezvýchodnost	k1gFnSc4
situace	situace	k1gFnSc2
Ústředních	ústřední	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mírovém	mírový	k2eAgInSc6d1
návrhu	návrh	k1gInSc6
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1916	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
ztroskotal	ztroskotat	k5eAaPmAgInS
na	na	k7c6
odmítavém	odmítavý	k2eAgInSc6d1
postoji	postoj	k1gInSc6
ze	z	k7c2
strany	strana	k1gFnSc2
Německé	německý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
na	na	k7c6
jaře	jaro	k1gNnSc6
1917	#num#	k4
Karel	Karla	k1gFnPc2
neúspěšně	úspěšně	k6eNd1
pokusil	pokusit	k5eAaPmAgMnS
vyjádřit	vyjádřit	k5eAaPmF
konkrétní	konkrétní	k2eAgInPc4d1
mírové	mírový	k2eAgInPc4d1
cíle	cíl	k1gInPc4
přes	přes	k7c4
svého	svůj	k3xOyFgMnSc4
švagra	švagr	k1gMnSc4
Sixta	Sixtus	k1gMnSc4
Ferdinanda	Ferdinand	k1gMnSc4
Bourbon-Parma	Bourbon-Parm	k1gMnSc4
vyjednáváním	vyjednávání	k1gNnSc7
s	s	k7c7
Trojdohodou	Trojdohoda	k1gFnSc7
o	o	k7c4
dosažení	dosažení	k1gNnSc4
separátního	separátní	k2eAgInSc2d1
míru	mír	k1gInSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Sixtova	Sixtův	k2eAgFnSc1d1
aféra	aféra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
mírové	mírový	k2eAgInPc1d1
rozhovory	rozhovor	k1gInPc1
se	se	k3xPyFc4
udály	udát	k5eAaPmAgInP
v	v	k7c6
létě	léto	k1gNnSc6
1917	#num#	k4
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
záměry	záměr	k1gInPc1
však	však	k9
ztroskotaly	ztroskotat	k5eAaPmAgInP
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
touze	touha	k1gFnSc6
po	po	k7c6
vítězství	vítězství	k1gNnSc6
v	v	k7c6
poli	pole	k1gNnSc6
(	(	kIx(
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
vstoupily	vstoupit	k5eAaPmAgInP
do	do	k7c2
války	válka	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
přemrštěných	přemrštěný	k2eAgInPc6d1
požadavcích	požadavek	k1gInPc6
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
na	na	k7c6
neústupnosti	neústupnost	k1gFnSc6
Německé	německý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stále	stále	k6eAd1
více	hodně	k6eAd2
prosazovaly	prosazovat	k5eAaImAgFnP
síly	síla	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
věřily	věřit	k5eAaImAgFnP
ve	v	k7c4
vojenský	vojenský	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Císařský	císařský	k2eAgInSc1d1
monogram	monogram	k1gInSc1
Karla	Karel	k1gMnSc2
I.	I.	kA
</s>
<s>
Mírové	mírový	k2eAgFnPc4d1
snahy	snaha	k1gFnPc4
<g/>
,	,	kIx,
výhrady	výhrada	k1gFnPc4
k	k	k7c3
neomezené	omezený	k2eNgFnSc3d1
ponorkové	ponorkový	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
,	,	kIx,
zákaz	zákaz	k1gInSc1
bombardování	bombardování	k1gNnSc2
civilních	civilní	k2eAgMnPc2d1
cílů	cíl	k1gInPc2
a	a	k8xC
pozitivní	pozitivní	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
na	na	k7c4
mírové	mírový	k2eAgFnPc4d1
výzvy	výzva	k1gFnPc4
papeže	papež	k1gMnSc2
Benedikta	Benedikt	k1gMnSc2
XV	XV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
spojence	spojenec	k1gMnPc4
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
vedly	vést	k5eAaImAgFnP
ke	k	k7c3
stále	stále	k6eAd1
hlubšímu	hluboký	k2eAgInSc3d2
rozporu	rozpor	k1gInSc3
Karla	Karel	k1gMnSc2
s	s	k7c7
Německou	německý	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
s	s	k7c7
německými	německý	k2eAgInPc7d1
národními	národní	k2eAgInPc7d1
spolky	spolek	k1gInPc7
ve	v	k7c6
vlastní	vlastní	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karla	k1gFnPc2
sice	sice	k8xC
zakázal	zakázat	k5eAaPmAgInS
explicitně	explicitně	k6eAd1
jakékoli	jakýkoli	k3yIgNnSc4
nasazení	nasazení	k1gNnSc4
otravných	otravný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
v	v	k7c6
rozsahu	rozsah	k1gInSc6
moci	moc	k1gFnSc2
velení	velení	k1gNnSc2
c.	c.	k?
a.	a.	k?
k.	k.	k?
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
nemohl	moct	k5eNaImAgMnS
však	však	k9
zabránit	zabránit	k5eAaPmF
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vrchní	vrchní	k2eAgNnSc1d1
velení	velení	k1gNnSc1
Německé	německý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
bitvě	bitva	k1gFnSc6
na	na	k7c6
Soče	sočit	k5eAaImSgInS
<g/>
,	,	kIx,
v	v	k7c6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Kobaridu	Kobarid	k1gInSc2
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
Caporetto	Caporetto	k1gNnSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgInSc1d1
Kobarid	Kobarid	k1gInSc1
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
říjnu	říjen	k1gInSc6
1917	#num#	k4
nařídilo	nařídit	k5eAaPmAgNnS
použití	použití	k1gNnSc1
jedovatého	jedovatý	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
útok	útok	k1gInSc4
německé	německý	k2eAgFnSc2d1
14	#num#	k4
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
vedl	vést	k5eAaImAgMnS
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Below	Below	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
se	se	k3xPyFc4
rozcházel	rozcházet	k5eAaImAgMnS
v	v	k7c6
názoru	názor	k1gInSc6
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
poradci	poradce	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
by	by	kYmCp3nP
takový	takový	k3xDgInSc4
kurs	kurs	k1gInSc4
schvalovali	schvalovat	k5eAaImAgMnP
<g/>
,	,	kIx,
ba	ba	k9
i	i	k9
podporovali	podporovat	k5eAaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c4
něž	jenž	k3xRgNnSc4
by	by	kYmCp3nP
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
plně	plně	k6eAd1
spoléhat	spoléhat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Otakar	Otakar	k1gMnSc1
Černín	Černín	k1gMnSc1
se	se	k3xPyFc4
sice	sice	k8xC
zpočátku	zpočátku	k6eAd1
přimlouval	přimlouvat	k5eAaImAgMnS
za	za	k7c4
mírové	mírový	k2eAgInPc4d1
plány	plán	k1gInPc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
však	však	k9
přiklonil	přiklonit	k5eAaPmAgInS
k	k	k7c3
pevnějším	pevný	k2eAgFnPc3d2
vazbám	vazba	k1gFnPc3
na	na	k7c4
Německo	Německo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černín	Černín	k1gMnSc1
v	v	k7c6
proslovu	proslov	k1gInSc6
ze	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1918	#num#	k4
vytýkal	vytýkat	k5eAaImAgMnS
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
vedla	vést	k5eAaImAgFnS
tajná	tajný	k2eAgNnPc4d1
mírová	mírový	k2eAgNnPc4d1
vyjednávání	vyjednávání	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Francie	Francie	k1gFnSc1
s	s	k7c7
tímto	tento	k3xDgNnSc7
nařčením	nařčení	k1gNnSc7
nesouhlasila	souhlasit	k5eNaImAgFnS
<g/>
,	,	kIx,
zveřejnil	zveřejnit	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Georges	Georges	k1gMnSc1
Clemenceau	Clemenceaus	k1gInSc2
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
obsah	obsah	k1gInSc1
tajného	tajný	k2eAgInSc2d1
Karlova	Karlův	k2eAgInSc2d1
dopisu	dopis	k1gInSc2
švagru	švagr	k1gMnSc3
Sixtovi	Sixta	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
utrpěla	utrpět	k5eAaPmAgFnS
autorita	autorita	k1gFnSc1
císaře	císař	k1gMnSc2
enormní	enormní	k2eAgFnPc1d1
škody	škoda	k1gFnPc1
<g/>
,	,	kIx,
i	i	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
dopis	dopis	k1gInSc1
dementovat	dementovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
byl	být	k5eAaImAgMnS
označen	označit	k5eAaPmNgMnS
za	za	k7c4
„	„	k?
<g/>
pantoflového	pantoflový	k2eAgMnSc4d1
hrdinu	hrdina	k1gMnSc4
<g/>
“	“	k?
a	a	k8xC
Zita	Zita	k1gFnSc1
za	za	k7c4
„	„	k?
<g/>
italskou	italský	k2eAgFnSc4d1
zrádkyni	zrádkyně	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černín	Černín	k1gMnSc1
byl	být	k5eAaImAgMnS
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
donucen	donucen	k2eAgMnSc1d1
k	k	k7c3
odstoupení	odstoupení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
musel	muset	k5eAaImAgMnS
podstoupit	podstoupit	k5eAaPmF
nepříjemnou	příjemný	k2eNgFnSc4d1
cestu	cesta	k1gFnSc4
k	k	k7c3
císaři	císař	k1gMnSc3
Vilémovi	Vilém	k1gMnSc3
do	do	k7c2
Spa	Spa	k1gFnPc2
a	a	k8xC
přijmout	přijmout	k5eAaPmF
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
ještě	ještě	k6eAd1
pevněji	pevně	k6eAd2
připoutaly	připoutat	k5eAaPmAgFnP
k	k	k7c3
Německé	německý	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Rovněž	rovněž	k9
Karlovy	Karlův	k2eAgFnSc2d1
snahy	snaha	k1gFnSc2
z	z	k7c2
října	říjen	k1gInSc2
1918	#num#	k4
o	o	k7c4
záchranu	záchrana	k1gFnSc4
alespoň	alespoň	k9
rakouské	rakouský	k2eAgFnSc3d1
části	část	k1gFnSc3
monarchie	monarchie	k1gFnSc2
a	a	k8xC
přesun	přesun	k1gInSc1
do	do	k7c2
jakéhosi	jakýsi	k3yIgInSc2
spolkového	spolkový	k2eAgInSc2d1
státu	stát	k1gInSc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
autonomií	autonomie	k1gFnSc7
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
národnosti	národnost	k1gFnPc4
přišly	přijít	k5eAaPmAgFnP
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
seznal	seznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
situace	situace	k1gFnSc1
je	být	k5eAaImIp3nS
neudržitelná	udržitelný	k2eNgFnSc1d1
<g/>
,	,	kIx,
soustředil	soustředit	k5eAaPmAgMnS
se	se	k3xPyFc4
především	především	k9
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
blížící	blížící	k2eAgMnSc1d1
se	se	k3xPyFc4
rozpad	rozpad	k1gInSc1
monarchie	monarchie	k1gFnSc2
proběhl	proběhnout	k5eAaPmAgInS
klidně	klidně	k6eAd1
a	a	k8xC
bez	bez	k7c2
krveprolití	krveprolití	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
Národní	národní	k2eAgInSc1d1
manifest	manifest	k1gInSc1
přispěl	přispět	k5eAaPmAgInS
k	k	k7c3
pokojnému	pokojný	k2eAgNnSc3d1
převzetí	převzetí	k1gNnSc3
moci	moc	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
zakladatelé	zakladatel	k1gMnPc1
tzv.	tzv.	kA
nástupnických	nástupnický	k2eAgInPc2d1
států	stát	k1gInPc2
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
odvolat	odvolat	k5eAaPmF
na	na	k7c4
Karlovu	Karlův	k2eAgFnSc4d1
vůli	vůle	k1gFnSc4
a	a	k8xC
c.	c.	k?
k.	k.	k?
úřady	úřad	k1gInPc1
se	se	k3xPyFc4
nestavěly	stavět	k5eNaImAgInP
proti	proti	k7c3
nim	on	k3xPp3gInPc3
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
října	říjen	k1gInSc2
se	se	k3xPyFc4
vzbouřily	vzbouřit	k5eAaPmAgInP
především	především	k9
maďarské	maďarský	k2eAgInPc1d1
vojenské	vojenský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
c.	c.	k?
a	a	k8xC
k.	k.	k?
armády	armáda	k1gFnSc2
na	na	k7c6
italské	italský	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
dvou	dva	k4xCgInPc2
dnů	den	k1gInPc2
se	se	k3xPyFc4
vzpoura	vzpoura	k1gFnSc1
a	a	k8xC
pasivní	pasivní	k2eAgInSc1d1
odboj	odboj	k1gInSc1
rozšířily	rozšířit	k5eAaPmAgInP
na	na	k7c6
26	#num#	k4
z	z	k7c2
57	#num#	k4
oddílů	oddíl	k1gInPc2
<g/>
…	…	k?
<g/>
,	,	kIx,
píše	psát	k5eAaImIp3nS
Brook-Shepherd	Brook-Shepherd	k1gMnSc1
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
232	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
příměří	příměří	k1gNnSc6
s	s	k7c7
Itálií	Itálie	k1gFnSc7
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
odporovalo	odporovat	k5eAaImAgNnS
záměrům	záměr	k1gInPc3
spojenců	spojenec	k1gMnPc2
Německé	německý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gInPc4
nemuseli	muset	k5eNaImAgMnP
sami	sám	k3xTgMnPc1
podepsat	podepsat	k5eAaPmF
<g/>
,	,	kIx,
předal	předat	k5eAaPmAgMnS
císař	císař	k1gMnSc1
a	a	k8xC
král	král	k1gMnSc1
vrchní	vrchní	k1gFnSc4
velení	velení	k1gNnSc2
nad	nad	k7c7
vším	všecek	k3xTgNnSc7
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
dosud	dosud	k6eAd1
podléhalo	podléhat	k5eAaImAgNnS
c.	c.	k?
a	a	k8xC
k.	k.	k?
armádě	armáda	k1gFnSc6
starého	starý	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
<g/>
,	,	kIx,
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
na	na	k7c4
generála	generál	k1gMnSc4
Artura	Artur	k1gMnSc4
Arze	arze	k1gFnSc2
ze	z	k7c2
Straussenburgu	Straussenburg	k1gInSc2
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
polního	polní	k2eAgMnSc4d1
maršála	maršál	k1gMnSc4
Heřmana	Heřman	k1gMnSc4
Kövesse	Kövess	k1gMnSc4
z	z	k7c2
Kövessházy	Kövessháza	k1gFnSc2
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
přání	přání	k1gNnSc4
vrchním	vrchní	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
byla	být	k5eAaImAgFnS
c.	c.	k?
a	a	k8xC
k.	k.	k?
armáda	armáda	k1gFnSc1
císařem	císař	k1gMnSc7
a	a	k8xC
králem	král	k1gMnSc7
demobilizována	demobilizován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
celkovému	celkový	k2eAgNnSc3d1
vojenskému	vojenský	k2eAgNnSc3d1
zhroucení	zhroucení	k1gNnSc3
a	a	k8xC
vnitřnímu	vnitřní	k2eAgInSc3d1
rozpadu	rozpad	k1gInSc3
podunajské	podunajský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
zřekl	zřeknout	k5eAaPmAgMnS
se	se	k3xPyFc4
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
části	část	k1gFnSc6
monarchie	monarchie	k1gFnSc2
„	„	k?
<g/>
jakékoli	jakýkoli	k3yIgFnSc3
účasti	účast	k1gFnSc3
na	na	k7c6
státních	státní	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
<g/>
“	“	k?
a	a	k8xC
zprostil	zprostit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
(	(	kIx(
<g/>
mezitím	mezitím	k6eAd1
již	již	k6eAd1
nefunkční	funkční	k2eNgFnSc4d1
<g/>
)	)	kIx)
rakouskou	rakouský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Prohlášení	prohlášení	k1gNnSc1
bylo	být	k5eAaImAgNnS
navrženo	navrhnout	k5eAaPmNgNnS
c.	c.	k?
k.	k.	k?
ministry	ministr	k1gMnPc7
společně	společně	k6eAd1
se	s	k7c7
zástupci	zástupce	k1gMnPc7
německorakouské	německorakouský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
říjnu	říjen	k1gInSc6
1918	#num#	k4
nově	nově	k6eAd1
vzniklý	vzniklý	k2eAgInSc4d1
stát	stát	k1gInSc4
Německé	německý	k2eAgNnSc1d1
Rakousko	Rakousko	k1gNnSc1
prohlášen	prohlášen	k2eAgInSc4d1
republikou	republika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Podobným	podobný	k2eAgNnSc7d1
prohlášením	prohlášení	k1gNnSc7
byl	být	k5eAaImAgMnS
Karel	Karel	k1gMnSc1
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
na	na	k7c4
nátlak	nátlak	k1gInSc4
maďarských	maďarský	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
donucen	donutit	k5eAaPmNgMnS
zříci	zříct	k5eAaPmF
se	se	k3xPyFc4
svých	svůj	k3xOyFgNnPc2
panovnických	panovnický	k2eAgNnPc2d1
práv	právo	k1gNnPc2
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Viléma	Vilém	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
nepřemýšlel	přemýšlet	k5eNaImAgMnS
–	–	k?
pod	pod	k7c7
silným	silný	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
Zity	Zita	k1gFnSc2
–	–	k?
formálně	formálně	k6eAd1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
v	v	k7c6
Německém	německý	k2eAgNnSc6d1
Rakousku	Rakousko	k1gNnSc6
k	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
Zákona	zákon	k1gInSc2
ze	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1919	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
vypovězení	vypovězení	k1gNnSc6
ze	z	k7c2
země	zem	k1gFnSc2
a	a	k8xC
převzetí	převzetí	k1gNnSc2
moci	moc	k1gFnSc2
domu	dům	k1gInSc2
habsbursko-lotrinského	habsbursko-lotrinský	k2eAgInSc2d1
(	(	kIx(
<g/>
StGBl	StGBlum	k1gNnPc2
<g/>
.	.	kIx.
209	#num#	k4
<g/>
/	/	kIx~
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
zrušoval	zrušovat	k5eAaImAgInS
pro	pro	k7c4
Německé	německý	k2eAgNnSc4d1
Rakousko	Rakousko	k1gNnSc4
všechna	všechen	k3xTgNnPc1
vladařská	vladařský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
dynastie	dynastie	k1gFnSc2
a	a	k8xC
stanovoval	stanovovat	k5eAaImAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
…	…	k?
<g/>
v	v	k7c6
zájmu	zájem	k1gInSc6
bezpečnosti	bezpečnost	k1gFnSc2
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
někdejším	někdejší	k2eAgMnPc3d1
nositelům	nositel	k1gMnPc3
koruny	koruna	k1gFnSc2
a	a	k8xC
ostatním	ostatní	k2eAgMnPc3d1
příslušníkům	příslušník	k1gMnPc3
domu	dům	k1gInSc2
Habsburg-Lothringen	Habsburg-Lothringen	k1gInSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
tito	tento	k3xDgMnPc1
výslovně	výslovně	k6eAd1
nezřeknou	zřeknout	k5eNaPmIp3nP
příslušnosti	příslušnost	k1gFnPc1
k	k	k7c3
panovnickému	panovnický	k2eAgInSc3d1
domu	dům	k1gInSc3
a	a	k8xC
všech	všecek	k3xTgNnPc2
s	s	k7c7
tím	ten	k3xDgNnSc7
souvisejících	související	k2eAgInPc2d1
vladařských	vladařský	k2eAgInPc2d1
nároků	nárok	k1gInPc2
a	a	k8xC
neprohlásí	prohlásit	k5eNaPmIp3nS
se	se	k3xPyFc4
za	za	k7c4
věrné	věrný	k2eAgMnPc4d1
občany	občan	k1gMnPc4
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
nařizuje	nařizovat	k5eAaImIp3nS
vyhoštění	vyhoštění	k1gNnSc1
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Ex-císař	Ex-císař	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
předtím	předtím	k6eAd1
odcestoval	odcestovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgInS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
vyhnancem	vyhnanec	k1gMnSc7
z	z	k7c2
Německého	německý	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
zbylí	zbylý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
rodiny	rodina	k1gFnSc2
habsbursko-lotrinské	habsbursko-lotrinský	k2eAgFnSc2d1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
pro	pro	k7c4
život	život	k1gInSc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
pro	pro	k7c4
republikánské	republikánský	k2eAgNnSc4d1
Rakousko	Rakousko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vdova	vdova	k1gFnSc1
po	po	k7c6
Karlovi	Karel	k1gMnSc6
Zita	Zita	k1gFnSc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
k	k	k7c3
Rakouské	rakouský	k2eAgFnSc3d1
republice	republika	k1gFnSc3
neznala	znát	k5eNaImAgFnS,k5eAaImAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
směla	smět	k5eAaImAgFnS
znovu	znovu	k6eAd1
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
země	zem	k1gFnSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
po	po	k7c6
posledním	poslední	k2eAgInSc6d1
přezkoumání	přezkoumání	k1gNnSc2
jejího	její	k3xOp3gInSc2
případu	případ	k1gInSc2
vyjasnilo	vyjasnit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
dynastie	dynastie	k1gFnSc2
pouze	pouze	k6eAd1
přivdala	přivdat	k5eAaPmAgFnS
a	a	k8xC
nikdy	nikdy	k6eAd1
by	by	kYmCp3nS
neměla	mít	k5eNaImAgFnS
ani	ani	k9
teoretické	teoretický	k2eAgNnSc4d1
právo	právo	k1gNnSc4
skutečně	skutečně	k6eAd1
vládnout	vládnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
exilu	exil	k1gInSc6
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Císař	Císař	k1gMnSc1
Karel	Karel	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
uchýlil	uchýlit	k5eAaPmAgInS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
nejbližší	blízký	k2eAgFnSc7d3
rodinou	rodina	k1gFnSc7
na	na	k7c4
zámek	zámek	k1gInSc4
Eckartsau	Eckartsaus	k1gInSc2
na	na	k7c6
Moravském	moravský	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
tehdy	tehdy	k6eAd1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Schönbrunnu	Schönbrunn	k1gInSc2
soukromým	soukromý	k2eAgInSc7d1
habsburským	habsburský	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
V.	V.	kA
jej	on	k3xPp3gNnSc4
chtěl	chtít	k5eAaImAgMnS
ušetřit	ušetřit	k5eAaPmF
osudu	osud	k1gInSc3
ruského	ruský	k2eAgMnSc2d1
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
tam	tam	k6eAd1
ochraňovat	ochraňovat	k5eAaImF
anglickým	anglický	k2eAgMnSc7d1
podplukovníkem	podplukovník	k1gMnSc7
E.	E.	kA
L.	L.	kA
Struttem	Strutt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
hrozila	hrozit	k5eAaImAgFnS
konfiskace	konfiskace	k1gFnSc1
habsburského	habsburský	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
nenechal	nechat	k5eNaPmAgMnS
se	se	k3xPyFc4
Karel	Karel	k1gMnSc1
přesvědčit	přesvědčit	k5eAaPmF
k	k	k7c3
abdikaci	abdikace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
vnitropolitický	vnitropolitický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
v	v	k7c6
Německém	německý	k2eAgNnSc6d1
Rakousku	Rakousko	k1gNnSc6
(	(	kIx(
<g/>
Habsburský	habsburský	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
připravil	připravit	k5eAaPmAgMnS
pplk.	pplk.	kA
Strutt	Strutt	k1gMnSc1
vycestování	vycestování	k1gNnSc4
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
do	do	k7c2
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1919	#num#	k4
dvorním	dvorní	k2eAgInSc7d1
vlakem	vlak	k1gInSc7
někdejších	někdejší	k2eAgFnPc2d1
c.	c.	k?
k.	k.	k?
státních	státní	k2eAgFnPc2d1
drah	draha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
přechodem	přechod	k1gInSc7
hranic	hranice	k1gFnPc2
zopakoval	zopakovat	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
v	v	k7c6
tzv.	tzv.	kA
Feldkirchském	Feldkirchské	k1gNnSc6
manifestu	manifest	k1gInSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
své	svůj	k3xOyFgNnSc4
prohlášení	prohlášení	k1gNnSc4
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
a	a	k8xC
protestoval	protestovat	k5eAaBmAgMnS
proti	proti	k7c3
svému	svůj	k3xOyFgNnSc3
sesazení	sesazení	k1gNnSc1
coby	coby	k?
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1919	#num#	k4
schválilo	schválit	k5eAaPmAgNnS
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
Německého	německý	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
vypovězení	vypovězení	k1gNnSc2
ze	z	k7c2
země	zem	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
)	)	kIx)
a	a	k8xC
konfiskaci	konfiskace	k1gFnSc4
habsburského	habsburský	k2eAgInSc2d1
rodinného	rodinný	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
nikoli	nikoli	k9
však	však	k9
prokazatelně	prokazatelně	k6eAd1
soukromý	soukromý	k2eAgInSc1d1
majetek	majetek	k1gInSc1
jednotlivých	jednotlivý	k2eAgMnPc2d1
členů	člen	k1gMnPc2
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
švýcarském	švýcarský	k2eAgInSc6d1
exilu	exil	k1gInSc6
žil	žít	k5eAaImAgMnS
nejprve	nejprve	k6eAd1
na	na	k7c6
zámku	zámek	k1gInSc6
Wartegg	Wartegga	k1gFnPc2
u	u	k7c2
Rorschachu	Rorschach	k1gInSc2
na	na	k7c6
Bodamském	bodamský	k2eAgNnSc6d1
jezeře	jezero	k1gNnSc6
a	a	k8xC
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1919	#num#	k4
v	v	k7c6
Pranginsu	Prangins	k1gInSc6
na	na	k7c6
Ženevském	ženevský	k2eAgNnSc6d1
jezeře	jezero	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
udržoval	udržovat	k5eAaImAgMnS
horlivý	horlivý	k2eAgInSc4d1
kontakt	kontakt	k1gInSc4
s	s	k7c7
legitimistickými	legitimistický	k2eAgInPc7d1
spolky	spolek	k1gInPc7
<g/>
,	,	kIx,
především	především	k9
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
po	po	k7c6
krátkém	krátký	k2eAgNnSc6d1
republikánském	republikánský	k2eAgNnSc6d1
intermezzu	intermezzo	k1gNnSc6
byla	být	k5eAaImAgFnS
monarchie	monarchie	k1gFnSc1
znovu	znovu	k6eAd1
nastolena	nastolen	k2eAgFnSc1d1
a	a	k8xC
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1920	#num#	k4
byl	být	k5eAaImAgInS
zdánlivě	zdánlivě	k6eAd1
prohabsburský	prohabsburský	k2eAgInSc1d1
Miklós	Miklós	k1gInSc1
Horthy	Hortha	k1gFnSc2
zvolen	zvolit	k5eAaPmNgInS
říšským	říšský	k2eAgMnSc7d1
místodržitelem	místodržitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
mu	on	k3xPp3gMnSc3
sice	sice	k8xC
slíbil	slíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
bude	být	k5eAaImBp3nS
informovat	informovat	k5eAaBmF
o	o	k7c6
svých	svůj	k3xOyFgInPc6
plánech	plán	k1gInPc6
a	a	k8xC
vrátí	vrátit	k5eAaPmIp3nP
se	se	k3xPyFc4
teprve	teprve	k6eAd1
po	po	k7c4
uklidnění	uklidnění	k1gNnSc4
politické	politický	k2eAgFnSc2d1
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
věřil	věřit	k5eAaImAgMnS
spíše	spíše	k9
úsudku	úsudek	k1gInSc6
svých	svůj	k3xOyFgMnPc2
poradců	poradce	k1gMnPc2
<g/>
,	,	kIx,
především	především	k6eAd1
tomu	ten	k3xDgMnSc3
nejvyššímu	vysoký	k2eAgMnSc3d3
<g/>
,	,	kIx,
Antonu	Anton	k1gMnSc3
Lehárovi	Lehár	k1gMnSc3
(	(	kIx(
<g/>
bratrovi	bratr	k1gMnSc3
skladatele	skladatel	k1gMnSc2
Franze	Franze	k1gFnSc2
Lehára	Lehár	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
čas	čas	k1gInSc1
pro	pro	k7c4
restauraci	restaurace	k1gFnSc4
monarchie	monarchie	k1gFnSc2
pro	pro	k7c4
Habsburky	Habsburk	k1gMnPc4
teprve	teprve	k6eAd1
přijde	přijít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
francouzský	francouzský	k2eAgMnSc1d1
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Aristide	Aristid	k1gInSc5
Briand	Brianda	k1gFnPc2
vyjádřil	vyjádřit	k5eAaPmAgMnS
podporu	podpora	k1gFnSc4
<g/>
;	;	kIx,
tu	tu	k6eAd1
však	však	k9
po	po	k7c6
krachu	krach	k1gInSc6
restaurace	restaurace	k1gFnSc2
dementoval	dementovat	k5eAaBmAgInS
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
informoval	informovat	k5eAaBmAgMnS
Horthyho	Horthy	k1gMnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
okolo	okolo	k7c2
Velikonoc	Velikonoce	k1gFnPc2
1921	#num#	k4
inkognito	inkognito	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Budapešti	Budapešť	k1gFnSc2
a	a	k8xC
od	od	k7c2
zemského	zemský	k2eAgMnSc2d1
správce	správce	k1gMnSc2
důrazně	důrazně	k6eAd1
požadoval	požadovat	k5eAaImAgMnS
odstoupení	odstoupení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
narážel	narážet	k5eAaImAgInS,k5eAaPmAgInS
pouze	pouze	k6eAd1
na	na	k7c4
Horthyho	Horthy	k1gMnSc4
přísahu	přísaha	k1gFnSc4
věrnosti	věrnost	k1gFnSc3
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
bral	brát	k5eAaImAgMnS
vážně	vážně	k6eAd1
námitky	námitka	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
vnitropolitických	vnitropolitický	k2eAgInPc2d1
problémů	problém	k1gInPc2
a	a	k8xC
především	především	k9
hrozící	hrozící	k2eAgFnSc3d1
intervenci	intervence	k1gFnSc3
Dohody	dohoda	k1gFnSc2
resp.	resp.	kA
vyhlášení	vyhlášení	k1gNnSc4
války	válka	k1gFnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
nástupnických	nástupnický	k2eAgInPc2d1
států	stát	k1gInPc2
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
Rumunska	Rumunsko	k1gNnSc2
a	a	k8xC
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
týdenním	týdenní	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
v	v	k7c4
Szombathely	Szombathela	k1gFnPc4
v	v	k7c6
západním	západní	k2eAgNnSc6d1
Maďarsku	Maďarsko	k1gNnSc6
se	se	k3xPyFc4
přesvědčil	přesvědčit	k5eAaPmAgMnS
o	o	k7c6
neperspektivnosti	neperspektivnost	k1gFnSc6
svých	svůj	k3xOyFgFnPc2
snah	snaha	k1gFnPc2
<g/>
,	,	kIx,
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
tedy	tedy	k9
zpět	zpět	k6eAd1
do	do	k7c2
Švýcar	Švýcary	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
usídlil	usídlit	k5eAaPmAgMnS
na	na	k7c6
zámku	zámek	k1gInSc6
Hertenstein	Hertensteina	k1gFnPc2
u	u	k7c2
Luzernu	Luzerna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
s	s	k7c7
rodinou	rodina	k1gFnSc7
ve	v	k7c6
vyhnanství	vyhnanství	k1gNnSc6
</s>
<s>
Již	již	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1921	#num#	k4
započal	započnout	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
druhý	druhý	k4xOgInSc4
pokus	pokus	k1gInSc4
a	a	k8xC
letěl	letět	k5eAaImAgMnS
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
chotí	choť	k1gFnSc7
Zitou	Zita	k1gFnSc7
na	na	k7c6
stroji	stroj	k1gInSc6
Junkers	Junkersa	k1gFnPc2
F	F	kA
13	#num#	k4
do	do	k7c2
Sopronu	Sopron	k1gInSc2
<g/>
,	,	kIx,
opět	opět	k6eAd1
bez	bez	k7c2
informování	informování	k1gNnSc2
jemu	on	k3xPp3gMnSc3
beztak	beztak	k9
již	již	k6eAd1
podezřelého	podezřelý	k2eAgMnSc4d1
Horthyho	Horthy	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
legitimisté	legitimista	k1gMnPc1
mezitím	mezitím	k6eAd1
započali	započnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
tzv.	tzv.	kA
Freischärler	Freischärler	k1gInSc1
(	(	kIx(
<g/>
polovojenská	polovojenský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
stavěli	stavět	k5eAaImAgMnP
proti	proti	k7c3
postoupení	postoupení	k1gNnSc3
Burgenlandska	Burgenlandsk	k1gInSc2
Rakousku	Rakousko	k1gNnSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
též	též	k9
Referendum	referendum	k1gNnSc1
1921	#num#	k4
v	v	k7c6
Burgenlandsku	Burgenlandsek	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgInPc1d1
menší	malý	k2eAgInPc1d2
vojenské	vojenský	k2eAgInPc1d1
kontingenty	kontingent	k1gInPc1
se	se	k3xPyFc4
spojovaly	spojovat	k5eAaImAgInP
ve	v	k7c4
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
však	však	k9
telegram	telegram	k1gInSc4
se	s	k7c7
zprávou	zpráva	k1gFnSc7
o	o	k7c6
Karlově	Karlův	k2eAgInSc6d1
příjezdu	příjezd	k1gInSc6
dorazil	dorazit	k5eAaPmAgMnS
o	o	k7c4
den	den	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
rozhodujícím	rozhodující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
zpozdil	zpozdit	k5eAaPmAgInS
odchod	odchod	k1gInSc1
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pochodu	pochod	k1gInSc2
na	na	k7c4
Budapešť	Budapešť	k1gFnSc4
vzniklo	vzniknout	k5eAaPmAgNnS
vítězné	vítězný	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
<g/>
,	,	kIx,
ne	ne	k9
nepodobné	podobný	k2eNgFnPc1d1
slavnému	slavný	k2eAgMnSc3d1
návratu	návrat	k1gInSc6
Napoleona	Napoleon	k1gMnSc2
z	z	k7c2
Elby	Elba	k1gFnSc2
do	do	k7c2
Paříže	Paříž	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1814	#num#	k4
<g/>
,	,	kIx,
pomalé	pomalý	k2eAgNnSc1d1
tempo	tempo	k1gNnSc1
postupu	postup	k1gInSc2
však	však	k9
dalo	dát	k5eAaPmAgNnS
zpočátku	zpočátku	k6eAd1
váhavému	váhavý	k2eAgInSc3d1
Horthymu	Horthym	k1gInSc3
čas	čas	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
soustředil	soustředit	k5eAaPmAgInS
vlastní	vlastní	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
na	na	k7c4
konfliktem	konflikt	k1gInSc7
hrozící	hrozící	k2eAgNnSc1d1
vojska	vojsko	k1gNnPc1
dohody	dohoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Budaörsu	Budaörs	k1gInSc6
<g/>
,	,	kIx,
jednom	jeden	k4xCgMnSc6
z	z	k7c2
předměstí	předměstí	k1gNnSc2
Budapešti	Budapešť	k1gFnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1921	#num#	k4
k	k	k7c3
menší	malý	k2eAgFnSc3d2
potyčce	potyčka	k1gFnSc3
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
život	život	k1gInSc4
19	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
si	se	k3xPyFc3
Karel	Karel	k1gMnSc1
uvědomil	uvědomit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
snahy	snaha	k1gFnPc1
o	o	k7c4
restauraci	restaurace	k1gFnSc4
monarchie	monarchie	k1gFnSc2
by	by	kYmCp3nP
skončily	skončit	k5eAaPmAgFnP
občanskou	občanský	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgInS
i	i	k9
přes	přes	k7c4
naléhání	naléhání	k1gNnSc4
svých	svůj	k3xOyFgMnPc2
vojenských	vojenský	k2eAgMnPc2d1
poradců	poradce	k1gMnPc2
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
ústupu	ústup	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
krátké	krátký	k2eAgFnSc6d1
internaci	internace	k1gFnSc6
v	v	k7c6
opatství	opatství	k1gNnSc6
Tihany	Tihana	k1gFnSc2
na	na	k7c6
Balatonu	Balaton	k1gInSc6
byl	být	k5eAaImAgMnS
Karel	Karel	k1gMnSc1
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc6
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
chotí	choť	k1gFnSc7
Zitou	Zita	k1gFnSc7
přijat	přijmout	k5eAaPmNgMnS
na	na	k7c4
palubu	paluba	k1gFnSc4
britské	britský	k2eAgFnSc2d1
dunajské	dunajský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Glowworm	Glowworm	k1gInSc1
a	a	k8xC
odvezen	odvezen	k2eAgInSc1d1
k	k	k7c3
Černému	černý	k2eAgNnSc3d1
moři	moře	k1gNnSc3
a	a	k8xC
poté	poté	k6eAd1
pokračovali	pokračovat	k5eAaImAgMnP
na	na	k7c6
anglickém	anglický	k2eAgInSc6d1
křižníku	křižník	k1gInSc6
Cardiff	Cardiff	k1gInSc4
přes	přes	k7c4
Gibraltar	Gibraltar	k1gInSc4
na	na	k7c4
portugalský	portugalský	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
Madeira	Madeira	k1gFnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
dorazil	dorazit	k5eAaPmAgInS
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
za	za	k7c7
nimi	on	k3xPp3gMnPc7
dorazily	dorazit	k5eAaPmAgInP
teprve	teprve	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
maďarském	maďarský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
byl	být	k5eAaImAgInS
mezitím	mezitím	k6eAd1
<g/>
,	,	kIx,
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
přijat	přijat	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
zbavil	zbavit	k5eAaPmAgInS
Habsburky	Habsburk	k1gInPc4
nároku	nárok	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
první	první	k4xOgInSc1
Habsburk	Habsburk	k1gInSc1
se	se	k3xPyFc4
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
studentského	studentský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1921	#num#	k4
byl	být	k5eAaImAgMnS
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
čestného	čestný	k2eAgMnSc4d1
člena	člen	k1gMnSc4
„	„	k?
<g/>
Německé	německý	k2eAgFnSc2d1
křesťanské	křesťanský	k2eAgFnSc2d1
akademické	akademický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
Wasgonia	Wasgonium	k1gNnSc2
<g/>
“	“	k?
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
měla	mít	k5eAaImAgFnS
místo	místo	k1gNnSc4
a	a	k8xC
hlas	hlas	k1gInSc4
na	na	k7c6
všech	všecek	k3xTgNnPc6
studentských	studentský	k2eAgNnPc6d1
setkáních	setkání	k1gNnPc6
a	a	k8xC
sjezdech	sjezd	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Madeiře	Madeira	k1gFnSc6
Karel	Karel	k1gMnSc1
sídlil	sídlit	k5eAaImAgMnS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
nejdříve	dříve	k6eAd3
v	v	k7c6
hotelu	hotel	k1gInSc6
Victoria	Victorium	k1gNnSc2
ve	v	k7c6
Funchalu	Funchal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
jim	on	k3xPp3gMnPc3
byly	být	k5eAaImAgInP
odcizeny	odcizen	k2eAgInPc1d1
osobní	osobní	k2eAgInPc1d1
šperky	šperk	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
jejich	jejich	k3xOp3gMnSc7
posledním	poslední	k2eAgInSc7d1
zbylým	zbylý	k2eAgInSc7d1
prostředkem	prostředek	k1gInSc7
<g/>
,	,	kIx,
přestěhovali	přestěhovat	k5eAaPmAgMnP
se	se	k3xPyFc4
do	do	k7c2
quinty	quinta	k1gFnSc2
(	(	kIx(
<g/>
panský	panský	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c4
Monte	Mont	k1gMnSc5
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
jim	on	k3xPp3gInPc3
byla	být	k5eAaImAgFnS
dána	dán	k2eAgFnSc1d1
bezplatně	bezplatně	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
rodinou	rodina	k1gFnSc7
jistého	jistý	k2eAgMnSc2d1
bankéře	bankéř	k1gMnSc2
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
se	se	k3xPyFc4
Karel	Karel	k1gMnSc1
nachladil	nachladit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěl	chtít	k5eAaImAgMnS
šetřit	šetřit	k5eAaImF
tenčící	tenčící	k2eAgFnSc4d1
se	se	k3xPyFc4
rodinné	rodinný	k2eAgFnPc1d1
finance	finance	k1gFnPc1
<g/>
,	,	kIx,
proto	proto	k8xC
byl	být	k5eAaImAgMnS
lékař	lékař	k1gMnSc1
povolán	povolat	k5eAaPmNgMnS
teprve	teprve	k9
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
konstatoval	konstatovat	k5eAaBmAgMnS
těžký	těžký	k2eAgInSc4d1
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1922	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
I.	I.	kA
ve	v	k7c6
věku	věk	k1gInSc6
třiceti	třicet	k4xCc2
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
na	na	k7c4
30	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
hrob	hrob	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
kostele	kostel	k1gInSc6
Nossa	Noss	k1gMnSc2
Senhora	Senhor	k1gMnSc2
do	do	k7c2
Monte	Mont	k1gInSc5
na	na	k7c4
Monte	Mont	k1gInSc5
(	(	kIx(
<g/>
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Funchalu	Funchal	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
srdce	srdce	k1gNnSc1
je	být	k5eAaImIp3nS
uloženo	uložit	k5eAaPmNgNnS
v	v	k7c6
Loretánské	loretánský	k2eAgFnSc6d1
kapli	kaple	k1gFnSc6
kláštera	klášter	k1gInSc2
Muri	Mur	k1gFnSc2
ve	v	k7c6
švýcarské	švýcarský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
Muri	Mur	k1gFnSc2
v	v	k7c6
kantonu	kanton	k1gInSc6
Aargau	Aargaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
pohřbu	pohřeb	k1gInSc2
Zity	Zita	k1gFnSc2
roku	rok	k1gInSc2
1989	#num#	k4
ve	v	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
Kapucínské	kapucínský	k2eAgFnSc6d1
kryptě	krypta	k1gFnSc6
stále	stále	k6eAd1
čeká	čekat	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
hrobek	hrobka	k1gFnPc2
na	na	k7c4
posledního	poslední	k2eAgMnSc4d1
habsburského	habsburský	k2eAgMnSc4d1
císaře	císař	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
především	především	k6eAd1
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Ota	Ota	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
<g/>
,	,	kIx,
však	však	k9
převoz	převoz	k1gInSc1
ostatků	ostatek	k1gInPc2
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
neschválila	schválit	k5eNaPmAgFnS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
takový	takový	k3xDgInSc1
úmysl	úmysl	k1gInSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
urážku	urážka	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
Madeiry	Madeira	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jeho	jeho	k3xOp3gFnSc1
otci	otec	k1gMnSc3
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
posledních	poslední	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
života	život	k1gInSc2
tolik	tolik	k6eAd1
pomáhali	pomáhat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
blahoslavení	blahoslavení	k1gNnPc2
Karla	Karel	k1gMnSc2
I.	I.	kA
získal	získat	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
hrob	hrob	k1gInSc1
na	na	k7c4
Monte	Mont	k1gInSc5
ve	v	k7c6
Funchalu	Funchal	k1gInSc6
pro	pro	k7c4
tamní	tamní	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
i	i	k8xC
zahraniční	zahraniční	k2eAgMnPc4d1
turisty	turist	k1gMnPc4
další	další	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
po	po	k7c4
většinu	většina	k1gFnSc4
roku	rok	k1gInSc2
otevřen	otevřít	k5eAaPmNgInS
<g/>
,	,	kIx,
do	do	k7c2
pohřební	pohřební	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
s	s	k7c7
kovovým	kovový	k2eAgInSc7d1
sarkofágem	sarkofág	k1gInSc7
lze	lze	k6eAd1
nahlédnout	nahlédnout	k5eAaPmF
přes	přes	k7c4
ozdobnou	ozdobný	k2eAgFnSc4d1
mříž	mříž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Karlova	Karlův	k2eAgFnSc1d1
vize	vize	k1gFnSc1
budoucnosti	budoucnost	k1gFnSc2
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1920	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
spisech	spis	k1gInPc6
Karel	Karel	k1gMnSc1
velmi	velmi	k6eAd1
přesně	přesně	k6eAd1
vystihl	vystihnout	k5eAaPmAgMnS
budoucí	budoucí	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
středoevropského	středoevropský	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
na	na	k7c4
desítky	desítka	k1gFnPc4
let	léto	k1gNnPc2
dopředu	dopředu	k6eAd1
(	(	kIx(
<g/>
rozmach	rozmach	k1gInSc1
Německé	německý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
rozmíšky	rozmíška	k1gFnSc2
a	a	k8xC
krize	krize	k1gFnSc2
uvnitř	uvnitř	k7c2
malých	malý	k2eAgInPc2d1
nově	nově	k6eAd1
vzniklých	vzniklý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
vehnání	vehnání	k1gNnSc1
mladých	mladý	k2eAgFnPc2d1
republik	republika	k1gFnPc2
do	do	k7c2
područí	područí	k1gNnSc2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
nerovnováha	nerovnováha	k1gFnSc1
a	a	k8xC
roztříštěnost	roztříštěnost	k1gFnSc1
národnostní	národnostní	k2eAgFnSc1d1
<g/>
,	,	kIx,
politická	politický	k2eAgFnSc1d1
i	i	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Vztah	vztah	k1gInSc1
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
k	k	k7c3
Čechám	Čechy	k1gFnPc3
</s>
<s>
Hrobka	hrobka	k1gFnSc1
císaře	císař	k1gMnSc2
a	a	k8xC
krále	král	k1gMnSc2
Karla	Karel	k1gMnSc2
I.	I.	kA
</s>
<s>
Hovořil	hovořit	k5eAaImAgMnS
dobře	dobře	k6eAd1
česky	česky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
a	a	k8xC
rád	rád	k6eAd1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c6
Čechách	Čechy	k1gFnPc6
(	(	kIx(
<g/>
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
absolvoval	absolvovat	k5eAaPmAgMnS
v	v	k7c6
Brandýse	Brandýs	k1gInSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
pobýval	pobývat	k5eAaImAgMnS
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Zákupech	zákup	k1gInPc6
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
nebylo	být	k5eNaImAgNnS
vyhověno	vyhověn	k2eAgNnSc1d1
jeho	jeho	k3xOp3gFnPc4
žádosti	žádost	k1gFnPc4
o	o	k7c4
azyl	azyl	k1gInSc4
v	v	k7c6
nově	nově	k6eAd1
vzniklém	vzniklý	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
<g/>
,	,	kIx,
o	o	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
žádal	žádat	k5eAaImAgMnS
<g/>
,	,	kIx,
osud	osud	k1gInSc4
a	a	k8xC
vývoj	vývoj	k1gInSc4
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
mu	on	k3xPp3gMnSc3
ležel	ležet	k5eAaImAgMnS
na	na	k7c6
srdci	srdce	k1gNnSc6
a	a	k8xC
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
smrt	smrt	k1gFnSc4
nemocný	nemocný	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
modlil	modlil	k1gMnSc1
se	se	k3xPyFc4
za	za	k7c4
český	český	k2eAgInSc4d1
národ	národ	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
posledních	poslední	k2eAgInPc2d1
dnů	den	k1gInPc2
svého	svůj	k3xOyFgInSc2
nedlouhého	dlouhý	k2eNgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
na	na	k7c6
návštěvě	návštěva	k1gFnSc6
Tanvaldu	Tanvald	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
</s>
<s>
Post-mortem	Post-mort	k1gInSc7
</s>
<s>
Historikové	historik	k1gMnPc1
se	se	k3xPyFc4
rozcházejí	rozcházet	k5eAaImIp3nP
v	v	k7c6
názoru	názor	k1gInSc6
na	na	k7c4
Karla	Karel	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejkritičtějších	kritický	k2eAgNnPc2d3
je	být	k5eAaImIp3nS
Helmut	Helmut	k2eAgInSc1d1
Rumpler	Rumpler	k1gInSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
Habsburského	habsburský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
Rakouské	rakouský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
Karla	Karel	k1gMnSc4
popsal	popsat	k5eAaPmAgMnS
jako	jako	k9
"	"	kIx"
<g/>
diletanta	diletant	k1gMnSc4
<g/>
,	,	kIx,
vnitřně	vnitřně	k6eAd1
příliš	příliš	k6eAd1
slabého	slabý	k2eAgNnSc2d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
čelit	čelit	k5eAaImF
všem	všecek	k3xTgFnPc3
výzvám	výzva	k1gFnPc3
osudu	osud	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jako	jako	k9
nevalného	valný	k2eNgNnSc2d1
politika	politikum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Nicméně	nicméně	k8xC
většina	většina	k1gFnSc1
ostatních	ostatní	k2eAgMnPc2d1
vidí	vidět	k5eAaImIp3nS
Karla	Karel	k1gMnSc4
jako	jako	k9
statečnou	statečný	k2eAgFnSc4d1
a	a	k8xC
čestnou	čestný	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
císař	císař	k1gMnSc1
a	a	k8xC
král	král	k1gMnSc1
pokusila	pokusit	k5eAaPmAgFnS
zastavit	zastavit	k5eAaPmF
běsnění	běsnění	k1gNnSc4
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Herbert	Herbert	k1gMnSc1
Vivian	Viviana	k1gFnPc2
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Karel	Karel	k1gMnSc1
byl	být	k5eAaImAgMnS
velký	velký	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
<g/>
,	,	kIx,
posel	posel	k1gMnSc1
míru	mír	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
chtěl	chtít	k5eAaImAgMnS
zachránit	zachránit	k5eAaPmF
svět	svět	k1gInSc4
od	od	k7c2
let	léto	k1gNnPc2
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státník	státník	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
dával	dávat	k5eAaImAgMnS
naději	naděje	k1gFnSc4
na	na	k7c4
záchranu	záchrana	k1gFnSc4
svého	svůj	k3xOyFgInSc2
lidu	lid	k1gInSc2
navzdory	navzdory	k7c3
komplikovaným	komplikovaný	k2eAgInPc3d1
problémům	problém	k1gInPc3
v	v	k7c6
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
svůj	svůj	k3xOyFgInSc4
lid	lid	k1gInSc4
miloval	milovat	k5eAaImAgMnS
nade	nad	k7c4
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
statečný	statečný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
ušlechtilá	ušlechtilý	k2eAgFnSc1d1
duše	duše	k1gFnSc1
<g/>
,	,	kIx,
vznešený	vznešený	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
svatý	svatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
jehož	jenž	k3xRgInSc2,k3xOyRp3gInSc2
hrobu	hrob	k1gInSc2
vychází	vycházet	k5eAaImIp3nS
požehnání	požehnání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Dále	daleko	k6eAd2
například	například	k6eAd1
Anatole	Anatola	k1gFnSc6
France	Franc	k1gMnPc4
<g/>
,	,	kIx,
francouzský	francouzský	k2eAgMnSc1d1
romanopisec	romanopisec	k1gMnSc1
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Císař	Císař	k1gMnSc1
Karel	Karel	k1gMnSc1
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgMnSc1d1
slušný	slušný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
postavil	postavit	k5eAaPmAgMnS
do	do	k7c2
čela	čelo	k1gNnSc2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
ač	ač	k8xS
byl	být	k5eAaImAgInS
spíše	spíše	k9
jako	jako	k9
světec	světec	k1gMnSc1
a	a	k8xC
nikdo	nikdo	k3yNnSc1
mu	on	k3xPp3gMnSc3
nenaslouchal	naslouchat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upřímně	upřímně	k6eAd1
toužil	toužit	k5eAaImAgInS
po	po	k7c6
míru	mír	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgInS
opovrhován	opovrhován	k2eAgInSc1d1
celým	celý	k2eAgInSc7d1
světem	svět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
zde	zde	k6eAd1
veliká	veliký	k2eAgFnSc1d1
šance	šance	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
promarněna	promarněn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Všechny	všechen	k3xTgInPc1
tyto	tento	k3xDgInPc1
rozličné	rozličný	k2eAgInPc1d1
pohledy	pohled	k1gInPc1
dávají	dávat	k5eAaImIp3nP
za	za	k7c4
pravdu	pravda	k1gFnSc4
slovům	slovo	k1gNnPc3
papeže	papež	k1gMnSc4
sv.	sv.	kA
Pia	Pius	k1gMnSc4
X.	X.	kA
během	během	k7c2
audience	audience	k1gFnSc2
mladého	mladý	k2eAgMnSc2d1
Karla	Karel	k1gMnSc2
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Žehnám	žehnat	k5eAaImIp1nS
arcivévodovi	arcivévoda	k1gMnSc3
Karlovi	Karel	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
bude	být	k5eAaImBp3nS
příštím	příští	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
rakouským	rakouský	k2eAgMnSc7d1
a	a	k8xC
jenž	jenž	k3xRgMnSc1
pomůže	pomoct	k5eAaPmIp3nS
vésti	vést	k5eAaImF
své	svůj	k3xOyFgFnPc4
země	zem	k1gFnPc4
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
národy	národ	k1gInPc1
k	k	k7c3
vysoké	vysoký	k2eAgFnSc3d1
cti	čest	k1gFnSc3
a	a	k8xC
mnohému	mnohý	k2eAgNnSc3d1
požehnání	požehnání	k1gNnSc3
–	–	k?
toto	tento	k3xDgNnSc4
požehnání	požehnání	k1gNnSc4
se	se	k3xPyFc4
však	však	k9
úplně	úplně	k6eAd1
projeví	projevit	k5eAaPmIp3nS
až	až	k9
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Blahořečení	blahořečení	k1gNnSc1
a	a	k8xC
posmrtná	posmrtný	k2eAgFnSc1d1
úcta	úcta	k1gFnSc1
</s>
<s>
Blahoslavený	blahoslavený	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
Datum	datum	k1gInSc4
narození	narození	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1887	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Persenbeug	Persenbeug	k1gInSc1
<g/>
,	,	kIx,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1922	#num#	k4
Madeira	Madeira	k1gFnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
Svátek	svátek	k1gInSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Funchal	Funchat	k5eAaPmAgInS,k5eAaImAgInS
Blahořečen	blahořečen	k2eAgInSc1d1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
Uctíván	uctíván	k2eAgInSc1d1
církvemi	církev	k1gFnPc7
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
blahořečen	blahořečen	k2eAgInSc1d1
římskokatolickou	římskokatolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církev	církev	k1gFnSc1
uctila	uctít	k5eAaPmAgFnS
Karla	Karel	k1gMnSc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
svou	svůj	k3xOyFgFnSc4
křesťanskou	křesťanský	k2eAgFnSc4d1
víru	víra	k1gFnSc4
stavěl	stavět	k5eAaImAgMnS
před	před	k7c4
každé	každý	k3xTgNnSc4
své	svůj	k3xOyFgNnSc4
politické	politický	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
a	a	k8xC
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
vnímal	vnímat	k5eAaImAgInS
svou	svůj	k3xOyFgFnSc4
politickou	politický	k2eAgFnSc4d1
roli	role	k1gFnSc4
jako	jako	k8xC,k8xS
mírotvorce	mírotvorce	k1gMnSc4
během	během	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
pak	pak	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
beatifikační	beatifikační	k2eAgFnSc2d1
mše	mše	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
pronesl	pronést	k5eAaPmAgInS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3
úkolem	úkol	k1gInSc7
křesťana	křesťan	k1gMnSc2
je	být	k5eAaImIp3nS
hledání	hledání	k1gNnSc1
<g/>
,	,	kIx,
rozpoznávání	rozpoznávání	k1gNnSc1
a	a	k8xC
následování	následování	k1gNnSc1
Boží	boží	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
v	v	k7c6
každé	každý	k3xTgFnSc6
záležitosti	záležitost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanský	křesťanský	k2eAgMnSc1d1
státník	státník	k1gMnSc1
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
a	a	k8xC
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
přijímal	přijímat	k5eAaImAgMnS
tuto	tento	k3xDgFnSc4
výzvu	výzva	k1gFnSc4
každý	každý	k3xTgInSc4
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gNnPc6
očích	oko	k1gNnPc6
byla	být	k5eAaImAgFnS
válka	válka	k1gFnSc1
‚	‚	k?
<g/>
něčím	něco	k3yInSc7
děsivým	děsivý	k2eAgNnSc7d1
<g/>
‘	‘	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k7c2
vřavy	vřava	k1gFnSc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
usilovně	usilovně	k6eAd1
snažil	snažit	k5eAaImAgMnS
napomáhat	napomáhat	k5eAaBmF,k5eAaImF
mírové	mírový	k2eAgFnSc3d1
iniciativě	iniciativa	k1gFnSc3
mého	můj	k3xOp1gMnSc2
předchůdce	předchůdce	k1gMnSc2
Benedikta	Benedikt	k1gMnSc2
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Celá	celý	k2eAgFnSc1d1
kauza	kauza	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
důkazy	důkaz	k1gInPc4
o	o	k7c6
Karlových	Karlův	k2eAgInPc6d1
světeckých	světecký	k2eAgInPc6d1
činech	čin	k1gInPc6
byly	být	k5eAaImAgInP
shromážděny	shromáždit	k5eAaPmNgInP
ve	v	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
arcidiecézi	arcidiecéze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
byl	být	k5eAaImAgMnS
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
ctihodného	ctihodný	k2eAgMnSc4d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgInSc4
krok	krok	k1gInSc4
v	v	k7c6
procesu	proces	k1gInSc6
beatifikace	beatifikace	k1gFnSc2
(	(	kIx(
<g/>
blahořečení	blahořečení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liga	liga	k1gFnSc1
věřících	věřící	k1gFnPc2
zřízená	zřízený	k2eAgFnSc1d1
k	k	k7c3
podpoře	podpora	k1gFnSc3
jeho	jeho	k3xOp3gFnSc2
kauzy	kauza	k1gFnSc2
dokonce	dokonce	k9
vytvořila	vytvořit	k5eAaPmAgFnS
webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Vídeňský	vídeňský	k2eAgMnSc1d1
kardinál	kardinál	k1gMnSc1
Christoph	Christoph	k1gMnSc1
Schönborn	Schönborn	k1gMnSc1
byl	být	k5eAaImAgMnS
církevním	církevní	k2eAgMnSc7d1
patronem	patron	k1gMnSc7
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
blahořečení	blahořečení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svátek	svátek	k1gInSc1
se	se	k3xPyFc4
slaví	slavit	k5eAaImIp3nS
na	na	k7c4
den	den	k1gInSc4
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
(	(	kIx(
<g/>
datum	datum	k1gInSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
sňatku	sňatek	k1gInSc2
s	s	k7c7
princeznou	princezna	k1gFnSc7
Zitou	Zita	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
je	být	k5eAaImIp3nS
blahoslavenému	blahoslavený	k2eAgMnSc3d1
císaři	císař	k1gMnSc3
Karlovi	Karel	k1gMnSc3
zasvěceno	zasvěcen	k2eAgNnSc1d1
několik	několik	k4yIc1
pamětních	pamětní	k2eAgInPc2d1
místː	místː	k?
</s>
<s>
kaple	kaple	k1gFnSc1
bl.	bl.	k?
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
Rakouského	rakouský	k2eAgInSc2d1
vybudovaná	vybudovaný	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
v	v	k7c6
Branišově	Branišův	k2eAgFnSc6d1
u	u	k7c2
Kdyně	Kdyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
sem	sem	k6eAd1
konají	konat	k5eAaImIp3nP
poutě	pouť	k1gFnPc1
okolo	okolo	k7c2
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
(	(	kIx(
<g/>
Karlův	Karlův	k2eAgInSc1d1
svátek	svátek	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
(	(	kIx(
<g/>
vysvěcení	vysvěcení	k1gNnSc2
kaple	kaple	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
kaple	kaple	k1gFnSc1
bl.	bl.	k?
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
Rakouského	rakouský	k2eAgMnSc2d1
v	v	k7c6
kostele	kostel	k1gInSc6
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
</s>
<s>
výklenková	výklenkový	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
bl.	bl.	k?
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
Rakouského	rakouský	k2eAgMnSc2d1
u	u	k7c2
Hořoviček	Hořovička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
rekonstrukcí	rekonstrukce	k1gFnSc7
zchátralé	zchátralý	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
neznámého	známý	k2eNgNnSc2d1
zasvěcení	zasvěcení	k1gNnSc2
<g/>
,	,	kIx,
vysvěcena	vysvěcen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
kříž	kříž	k1gInSc4
blahoslaveného	blahoslavený	k2eAgMnSc2d1
Karla	Karel	k1gMnSc2
I.	I.	kA
v	v	k7c6
Tlumačově	tlumačův	k2eAgNnSc6d1
<g/>
,	,	kIx,
posvěcený	posvěcený	k2eAgMnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pramen	pramen	k1gInSc1
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
I.	I.	kA
v	v	k7c6
Návarově	návarově	k6eAd1
<g/>
,	,	kIx,
pojmenovaný	pojmenovaný	k2eAgInSc1d1
a	a	k8xC
upravený	upravený	k2eAgInSc1d1
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
slavnostní	slavnostní	k2eAgNnSc4d1
požehnání	požehnání	k1gNnSc4
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
neoficiální	neoficiální	k2eAgFnSc3d1,k2eNgFnSc3d1
<g/>
)	)	kIx)
náměstí	náměstí	k1gNnSc1
císaře	císař	k1gMnSc2
a	a	k8xC
krále	král	k1gMnSc2
Karla	Karel	k1gMnSc2
I.	I.	kA
u	u	k7c2
zámku	zámek	k1gInSc2
v	v	k7c6
Horních	horní	k2eAgFnPc6d1
Tošanovicích	Tošanovice	k1gFnPc6
<g/>
,	,	kIx,
otevřené	otevřený	k2eAgNnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
Karel	Karel	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Rakousko-Uherský	rakousko-uherský	k2eAgMnSc1d1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Bádenská	bádenský	k2eAgFnSc1d1
</s>
<s>
Ota	Ota	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc5d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Annunziata	Annunziat	k2eAgFnSc1d1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Rakousko-Těšínský	rakousko-těšínský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Izabela	Izabela	k1gFnSc1
Rakouská	rakouský	k2eAgFnSc1d1
</s>
<s>
Jindřiška	Jindřiška	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
</s>
<s>
Maximilián	Maximilián	k1gMnSc1
<g/>
,	,	kIx,
dědičný	dědičný	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Saska	Sasko	k1gNnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
Saský	saský	k2eAgMnSc5d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Parmská	parmský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
Saský	saský	k2eAgMnSc1d1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Augusta	August	k1gMnSc2
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Bádenská	bádenský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portugalský	portugalský	k2eAgInSc1d1
</s>
<s>
Maria	Mario	k1gMnSc2
Antonia	Antonio	k1gMnSc2
Koháry	Kohár	k1gInPc1
de	de	k?
Csábrág	Csábrág	k1gInSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Portugalská	portugalský	k2eAgFnSc1d1
</s>
<s>
Petr	Petr	k1gMnSc1
I.	I.	kA
Brazilský	brazilský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portugalská	portugalský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Leopoldina	Leopoldina	k1gFnSc1
Habsbursko-Lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
ve	v	k7c6
farnosti	farnost	k1gFnSc6
Persenburg	Persenburg	k1gMnSc1
<g/>
↑	↑	k?
GALANDAUER	GALANDAUER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
I.	I.	kA
Poslední	poslední	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
174	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
124	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Felix	Felix	k1gMnSc1
Habsburg	Habsburg	k1gMnSc1
im	im	k?
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lebensjahr	Lebensjahr	k1gInSc1
verstorben	verstorben	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
V	V	kA
<g/>
:	:	kIx,
wien	wien	k1gMnSc1
<g/>
.	.	kIx.
<g/>
ORF	ORF	kA
<g/>
.	.	kIx.
<g/>
at	at	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc4
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GALANDAUER	GALANDAUER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
I.	I.	kA
Poslední	poslední	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
122	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
BRÁNA	brána	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
720	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
597	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
82	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudové	osudový	k2eAgInPc1d1
okamžiky	okamžik	k1gInPc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
plato	plato	k1gNnSc1
<g/>
.	.	kIx.
<g/>
kfunigraz	kfunigraz	k1gInSc1
<g/>
.	.	kIx.
<g/>
ac	ac	k?
<g/>
.	.	kIx.
<g/>
at	at	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náš	náš	k3xOp1gMnSc1
arcivévoda	arcivévoda	k1gMnSc1
císař	císař	k1gMnSc1
a	a	k8xC
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
<g/>
:	:	kIx,
v	v	k7c6
městě	město	k1gNnSc6
Brandýse	Brandýs	k1gInSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
-	-	kIx~
Staré	Staré	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Kartuziánské	kartuziánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
134	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86953	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
134	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.emperor-charles.org	http://www.emperor-charles.org	k1gInSc1
<g/>
↑	↑	k?
HOBLÍKOVÁ	hoblíkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruina	ruina	k1gFnSc1
zmizela	zmizet	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opravená	opravený	k2eAgFnSc1d1
kaplička	kaplička	k1gFnSc1
svítí	svítit	k5eAaImIp3nS
daleko	daleko	k6eAd1
do	do	k7c2
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakovnický	rakovnický	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kříž	Kříž	k1gMnSc1
zasvěcený	zasvěcený	k2eAgMnSc1d1
blahoslavenému	blahoslavený	k2eAgMnSc3d1
Karlovi	Karel	k1gMnSc3
I.	I.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Patria	Patrium	k1gNnSc2
Moravia	Moravia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Shrine	Shrin	k1gInSc5
in	in	k?
Czech	Czech	k1gMnSc1
Countryside	Countrysid	k1gInSc5
Dedicated	Dedicated	k1gInSc1
to	ten	k3xDgNnSc4
Blessed	Blessed	k1gMnSc1
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blessed	Blessed	k1gMnSc1
Karl	Karl	k1gMnSc1
of	of	k?
Austria	Austrium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROSNER	ROSNER	kA
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Horních	horní	k2eAgFnPc6d1
Tošanovicích	Tošanovice	k1gFnPc6
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
náměstí	náměstí	k1gNnSc4
bl.	bl.	k?
Karla	Karel	k1gMnSc2
I.	I.	kA
Rakouského	rakouský	k2eAgMnSc2d1
<g/>
.	.	kIx.
www.doo.cz	www.doo.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA
<g/>
,	,	kIx,
Ivana	Ivana	k1gFnSc1
<g/>
;	;	kIx,
RAK	rak	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
VLNAS	VLNAS	kA
<g/>
,	,	kIx,
Vít	Vít	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stínu	stín	k1gInSc6
tvých	tvůj	k3xOp2gNnPc2
křídel	křídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85785	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
GALANDAUER	GALANDAUER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
I.	I.	kA
Poslední	poslední	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
176	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA
<g/>
,	,	kIx,
Brigitte	Brigitte	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životopisná	životopisný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brána	brána	k1gFnSc1
;	;	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85946	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
I.	I.	kA
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
RYANTOVÁ	RYANTOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
VOREL	Vorel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
králové	král	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
940	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
549	#num#	k4
<g/>
-	-	kIx~
<g/>
559	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgMnPc1d1
Habsburkové	Habsburk	k1gMnPc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
Zita	Zita	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
a	a	k8xC
snahy	snaha	k1gFnPc1
o	o	k7c4
záchranu	záchrana	k1gFnSc4
císařského	císařský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gInSc1
&	&	k?
Principal	Principal	k1gFnSc1
;	;	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
286	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85947	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PRECLÍK	preclík	k1gInSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masaryk	Masaryk	k1gMnSc1
a	a	k8xC
legie	legie	k1gFnSc1
<g/>
,	,	kIx,
váz	váza	k1gFnPc2
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
219	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc1
Paris	Paris	k1gMnSc1
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
Žižkova	Žižkův	k2eAgFnSc1d1
2379	#num#	k4
(	(	kIx(
<g/>
734	#num#	k4
01	#num#	k4
Karviná	Karviná	k1gFnSc1
<g/>
)	)	kIx)
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Masarykovým	Masarykův	k2eAgNnSc7d1
demokratickým	demokratický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87173	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
28	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
84	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
131	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
166	#num#	k4
</s>
<s>
TÓTH	TÓTH	kA
<g/>
,	,	kIx,
Andrej	Andrej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Czechoslovak	Czechoslovak	k1gMnSc1
Policy	Polica	k1gFnSc2
and	and	k?
the	the	k?
First	First	k1gFnSc1
Restoration	Restoration	k1gInSc1
Attempt	Attempt	k1gMnSc1
of	of	k?
Charles	Charles	k1gMnSc1
Habsburg	Habsburg	k1gMnSc1
in	in	k?
Hungary	Hungara	k1gFnSc2
in	in	k?
the	the	k?
Spring	Spring	k1gInSc4
1921	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc4
of	of	k?
International	International	k1gFnSc2
Relations	Relationsa	k1gFnPc2
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
241	#num#	k4
<g/>
-	-	kIx~
<g/>
279	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7308	#num#	k4
<g/>
-	-	kIx~
<g/>
208	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VONDRA	Vondra	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
I.	I.	kA
<g/>
:	:	kIx,
(	(	kIx(
<g/>
1887	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
133	#num#	k4
<g/>
-	-	kIx~
<g/>
135	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karel	Karla	k1gFnPc2
I.	I.	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autor	autor	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Osoba	osoba	k1gFnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Karel	Karel	k1gMnSc1
I.	I.	kA
</s>
<s>
Filmový	filmový	k2eAgInSc1d1
záznam	záznam	k1gInSc1
svatby	svatba	k1gFnSc2
Karla	Karel	k1gMnSc2
I.	I.	kA
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Projev	projev	k1gInSc1
Karla	Karel	k1gMnSc2
I.	I.	kA
o	o	k7c6
válce	válka	k1gFnSc6
</s>
<s>
Stránky	stránka	k1gFnPc1
o	o	k7c6
Karlovi	Karel	k1gMnSc6
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
příbuzných	příbuzný	k1gMnPc2
</s>
<s>
Beatifikační	Beatifikační	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
a	a	k8xC
poslední	poslední	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
–	–	k?
video	video	k1gNnSc1
z	z	k7c2
cyklu	cyklus	k1gInSc2
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Historický	historický	k2eAgInSc1d1
magazín	magazín	k1gInSc1
</s>
<s>
http://www.cisarkarel.cz	http://www.cisarkarel.cz	k1gMnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I	i	k9
<g/>
.1916	.1916	k4
–	–	k?
1918	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Karl	Karl	k1gMnSc1
Seitz	Seitz	k1gMnSc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
prezident	prezident	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.1916	.1916	k4
–	–	k?
1918	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigu	k1gFnSc2
Masaryk	Masaryk	k1gMnSc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
prezident	prezident	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.1916	.1916	k4
–	–	k?
1918	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Josef	Josef	k1gMnSc1
August	August	k1gMnSc1
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
zemský	zemský	k2eAgMnSc1d1
správce	správce	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Chorvatsko-slavonský	chorvatsko-slavonský	k2eAgMnSc1d1
král	král	k1gMnSc1
1916	#num#	k4
–	–	k?
1918	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
jiný	jiný	k2eAgInSc4d1
druh	druh	k1gInSc4
monarchie	monarchie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Dalmátský	Dalmátský	k2eAgMnSc1d1
král	král	k1gMnSc1
1916	#num#	k4
–	–	k?
1918	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
jiný	jiný	k2eAgInSc4d1
druh	druh	k1gInSc4
monarchie	monarchie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Haličsko-vladiměřský	Haličsko-vladiměřský	k2eAgMnSc1d1
král	král	k1gMnSc1
1916	#num#	k4
–	–	k?
1918	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Karel	Karel	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Hlava	hlava	k1gFnSc1
dynastie	dynastie	k1gFnSc2
1916	#num#	k4
–	–	k?
1922	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Habsburg	Habsburg	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Čeští	český	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
(	(	kIx(
<g/>
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgMnPc1
mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Krok	krok	k1gInSc1
•	•	k?
Libuše	Libuše	k1gFnSc1
Mytičtí	mytický	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Oráč	oráč	k1gMnSc1
•	•	k?
Nezamysl	Nezamysl	k1gMnSc1
•	•	k?
Mnata	Mnata	k1gFnSc1
•	•	k?
Vojen	vojna	k1gFnPc2
•	•	k?
Vnislav	Vnislav	k1gMnSc1
•	•	k?
Křesomysl	Křesomysl	k1gMnSc1
•	•	k?
Neklan	Neklan	k1gMnSc1
•	•	k?
Hostivít	Hostivít	k1gMnSc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
872	#num#	k4
<g/>
–	–	k?
<g/>
889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
I.	I.	kA
(	(	kIx(
<g/>
894	#num#	k4
<g/>
–	–	k?
<g/>
915	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
915	#num#	k4
<g/>
–	–	k?
<g/>
921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
svatý	svatý	k1gMnSc1
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
921	#num#	k4
<g/>
–	–	k?
<g/>
935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
935	#num#	k4
<g/>
–	–	k?
<g/>
972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
972	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
999	#num#	k4
<g/>
–	–	k?
<g/>
1002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
Chrabrý¹	Chrabrý¹	k1gMnSc1
(	(	kIx(
<g/>
1003	#num#	k4
<g/>
–	–	k?
<g/>
1004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladivoj	Vladivoj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1004	#num#	k4
<g/>
–	–	k?
<g/>
1012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1012	#num#	k4
<g/>
–	–	k?
<g/>
1033	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1033	#num#	k4
<g/>
–	–	k?
<g/>
1034	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
II	II	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
(	(	kIx(
<g/>
1100	#num#	k4
<g/>
–	–	k?
<g/>
1107	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Svatopluk	Svatopluk	k1gMnSc1
Olomoucký	olomoucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1109	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1109	#num#	k4
<g/>
–	–	k?
<g/>
1117	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1117	#num#	k4
<g/>
–	–	k?
<g/>
1020	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
I.	I.	kA
(	(	kIx(
<g/>
1120	#num#	k4
<g/>
–	–	k?
<g/>
1025	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1125	#num#	k4
<g/>
–	–	k?
<g/>
1140	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1140	#num#	k4
<g/>
–	–	k?
<g/>
1172	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1172	#num#	k4
<g/>
–	–	k?
<g/>
1173	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1173	#num#	k4
<g/>
–	–	k?
<g/>
1178	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1178	#num#	k4
<g/>
–	–	k?
<g/>
1189	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1191	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1192	#num#	k4
<g/>
–	–	k?
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
(	(	kIx(
<g/>
1193	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Vladislav	Vladislav	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1198	#num#	k4
<g/>
–	–	k?
<g/>
1230	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1230	#num#	k4
<g/>
–	–	k?
<g/>
1253	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1253	#num#	k4
<g/>
–	–	k?
<g/>
1278	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1278	#num#	k4
<g/>
–	–	k?
<g/>
1305	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1305	#num#	k4
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1307	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1307	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
Lucemburkové	Lucemburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1346	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1346	#num#	k4
<g/>
–	–	k?
<g/>
1378	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1378	#num#	k4
<g/>
–	–	k?
<g/>
1419	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zikmund	Zikmund	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1419	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1437	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1438	#num#	k4
<g/>
–	–	k?
<g/>
1439	#num#	k4
<g/>
)	)	kIx)
•	•	k?
interregnum	interregnum	k1gNnSc4
(	(	kIx(
<g/>
1439	#num#	k4
<g/>
–	–	k?
<g/>
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
(	(	kIx(
<g/>
1453	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1457	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Korvín²	Korvín²	k1gMnSc1
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1490	#num#	k4
<g/>
)	)	kIx)
Jagellonci	Jagellonec	k1gInSc6
<g/>
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1516	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1516	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1564	#num#	k4
<g/>
–	–	k?
<g/>
1576	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1576	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1611	#num#	k4
<g/>
–	–	k?
<g/>
1619	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Falcký³	Falcký³	k1gMnSc1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1620	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1637	#num#	k4
<g/>
–	–	k?
<g/>
1657	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgInSc1d1
•	•	k?
Leopold	Leopolda	k1gFnPc2
I.	I.	kA
(	(	kIx(
<g/>
1657	#num#	k4
<g/>
–	–	k?
<g/>
1705	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1711	#num#	k4
<g/>
–	–	k?
<g/>
1740	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský³	Bavorský³	k1gFnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
)	)	kIx)
Habsburko-Lotrinkové	Habsburko-Lotrinkový	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
¹	¹	k?
Piastovec	Piastovec	k1gInSc1
<g/>
,	,	kIx,
²	²	k?
vládce	vládce	k1gMnSc1
vedlejších	vedlejší	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
,	,	kIx,
³	³	k?
vzdorokrál	vzdorokrál	k1gMnSc1
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Markrabata	markrabě	k1gNnPc4
Malý	malý	k2eAgInSc4d1
znak	znak	k1gInSc4
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babenberkové	Babenberkové	k2eAgInSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Jindřich	Jindřich	k1gMnSc1
I.	I.	kA
•	•	k?
Adalbert	Adalbert	k1gMnSc1
•	•	k?
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
do	do	k7c2
1156	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vévodové	vévoda	k1gMnPc1
Babenberkové	Babenberková	k1gFnSc2
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1156	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopold	k1gMnSc1
V.	V.	kA
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemyslovci	Přemyslovec	k1gMnSc3
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zähringové	Zähringový	k2eAgFnPc4d1
</s>
<s>
Heřman	Heřman	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bádenský	bádenský	k2eAgMnSc1d1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Bádenský	bádenský	k2eAgInSc4d1
Habsburkové	Habsburk	k1gMnPc5
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
I.	I.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
I.	I.	kA
•	•	k?
Rudolf	Rudolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ota	Ota	k1gMnSc1
•	•	k?
Rudolf	Rudolf	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Vilém	Vilém	k1gMnSc1
•	•	k?
Albrecht	Albrecht	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
V.	V.	kA
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Arnošt	Arnošt	k1gMnSc1
Železný	Železný	k1gMnSc1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
do	do	k7c2
1453	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Arcivévodové	arcivévoda	k1gMnPc1
Habsburkové	Habsburk	k1gMnPc1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Rudolf	Rudolf	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Matyáš	Matyáš	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
do	do	k7c2
1804	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Císařové	Císařové	k2eAgFnSc1d1
Habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
od	od	k7c2
1804	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
rakouští	rakouský	k2eAgMnPc1d1
princové	princ	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
titulární	titulární	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
uvedeni	uvést	k5eAaPmNgMnP
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
šabloně	šablona	k1gFnSc6
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
1	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
Znak	znak	k1gInSc4
rakouské	rakouský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
I.	I.	kA
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
V.	V.	kA
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělský	španělský	k2eAgInSc1d1
<g/>
*	*	kIx~
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štýrský	štýrský	k2eAgInSc4d1
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Albrecht	Albrecht	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
V.	V.	kA
•	•	k?
Karel	Karel	k1gMnSc1
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Jan	Jan	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
Vilém	Vilém	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
František	František	k1gMnSc1
Tyrolský	tyrolský	k2eAgMnSc1d1
8	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
*	*	kIx~
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Leopold	Leopold	k1gMnSc1
I.	I.	kA
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Leopold	Leopold	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
František	František	k1gMnSc1
<g/>
**	**	k?
12	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
**	**	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
***	***	k?
•	•	k?
Karel	Karel	k1gMnSc1
Ambrož	Ambrož	k1gMnSc1
<g/>
***	***	k?
13	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgInSc1d1
•	•	k?
František	františek	k1gInSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
Těšínský	Těšínský	k1gMnSc1
•	•	k?
Štěpán	Štěpán	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
•	•	k?
Arnošt	Arnošt	k1gMnSc1
•	•	k?
Zikmund	Zikmund	k1gMnSc1
•	•	k?
Rainer	Rainer	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
V.	V.	kA
Modenský	modenský	k2eAgInSc1d1
<g/>
***	***	k?
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Viktor	Viktor	k1gMnSc1
<g/>
***	***	k?
14	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Mexický	mexický	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jan	Jan	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Rakousko-Těšínský	rakousko-těšínský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
•	•	k?
Evžen	Evžen	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
August	August	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Filip	Filip	k1gMnSc1
15	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Ota	Ota	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
•	•	k?
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Petr	Petr	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Leopold	Leopold	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
•	•	k?
Leo	Leo	k1gMnSc1
Karel	Karel	k1gMnSc1
•	•	k?
Vilém	Vilém	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
František	František	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Luitpold	Luitpold	k1gMnSc1
16	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
Evžen	Evžen	k1gMnSc1
•	•	k?
Gottfried	Gottfried	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Jiří	Jiří	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Rainer	Rainer	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Antonín	Antonín	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
<g/>
**	**	k?
•	•	k?
Karel	Karel	k1gMnSc1
Pius	Pius	k1gMnSc1
<g/>
**	**	k?
•	•	k?
František	František	k1gMnSc1
Karel	Karel	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Hubert	Hubert	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Theodor	Theodor	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
•	•	k?
Klement	Klement	k1gMnSc1
Salvátor	Salvátor	k1gMnSc1
<g/>
**	**	k?
17	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Habsburg	Habsburg	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
***	***	k?
•	•	k?
Felix	Felix	k1gMnSc1
Rakouský	rakouský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
*	*	kIx~
také	také	k9
španělský	španělský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
(	(	kIx(
<g/>
infant	infant	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
**	**	k?
také	také	k9
toskánský	toskánský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
***	***	k?
také	také	k9
modenský	modenský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
<g/>
,	,	kIx,
kurzívou	kurzíva	k1gFnSc7
jsou	být	k5eAaImIp3nP
rakouští	rakouský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700856	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118560077	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6635	#num#	k4
219X	219X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50046481	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
76310427	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50046481	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Novověk	novověk	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Maďarsko	Maďarsko	k1gNnSc1
</s>
