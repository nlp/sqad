<s>
Definován	definován	k2eAgInSc1d1	definován
Francouzem	Francouz	k1gMnSc7	Francouz
Davidem	David	k1gMnSc7	David
Bellem	bell	k1gInSc7	bell
<g/>
,	,	kIx,	,
parkour	parkour	k1gMnSc1	parkour
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
trénink	trénink	k1gInSc4	trénink
účinných	účinný	k2eAgMnPc2d1	účinný
pohybů	pohyb	k1gInPc2	pohyb
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
situacích	situace	k1gFnPc6	situace
(	(	kIx(	(
<g/>
a	a	k8xC	a
především	především	k9	především
kritických	kritický	k2eAgNnPc2d1	kritické
<g/>
)	)	kIx)	)
uměl	umět	k5eAaImAgInS	umět
pohybovat	pohybovat	k5eAaImF	pohybovat
klidně	klidně	k6eAd1	klidně
a	a	k8xC	a
sebejistě	sebejistě	k6eAd1	sebejistě
<g/>
.	.	kIx.	.
</s>
