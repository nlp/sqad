<s>
Parkour	Parkour	k1gMnSc1	Parkour
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Le	Le	k1gMnSc1	Le
Parkour	Parkour	k1gMnSc1	Parkour
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
zkratka	zkratka	k1gFnSc1	zkratka
PK	PK	kA	PK
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
du	du	k?	du
déplacement	déplacement	k1gInSc1	déplacement
(	(	kIx(	(
<g/>
umění	umění	k1gNnSc1	umění
pohybu	pohyb	k1gInSc2	pohyb
<g/>
/	/	kIx~	/
<g/>
umění	umění	k1gNnSc3	umění
přemístění	přemístění	k1gNnSc3	přemístění
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
disciplína	disciplína	k1gFnSc1	disciplína
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
z	z	k7c2	z
bodu	bod	k1gInSc2	bod
A	a	k8xC	a
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
B	B	kA	B
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
umět	umět	k5eAaImF	umět
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
bezpečně	bezpečně	k6eAd1	bezpečně
<g/>
,	,	kIx,	,
plynule	plynule	k6eAd1	plynule
a	a	k8xC	a
účinně	účinně	k6eAd1	účinně
(	(	kIx(	(
<g/>
efektivně	efektivně	k6eAd1	efektivně
<g/>
)	)	kIx)	)
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
překonávání	překonávání	k1gNnSc6	překonávání
libovolných	libovolný	k2eAgFnPc2d1	libovolná
překážek	překážka	k1gFnPc2	překážka
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
–	–	k?	–
od	od	k7c2	od
větví	větev	k1gFnPc2	větev
přes	přes	k7c4	přes
kameny	kámen	k1gInPc4	kámen
a	a	k8xC	a
skály	skála	k1gFnPc4	skála
až	až	k9	až
po	po	k7c6	po
zábradlí	zábradlí	k1gNnSc6	zábradlí
a	a	k8xC	a
betonové	betonový	k2eAgFnSc3d1	betonová
zdi	zeď	k1gFnSc3	zeď
–	–	k?	–
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provozován	provozovat	k5eAaImNgInS	provozovat
jak	jak	k8xC	jak
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
provozující	provozující	k2eAgMnSc1d1	provozující
parkour	parkour	k1gMnSc1	parkour
je	být	k5eAaImIp3nS	být
traceur	traceur	k1gMnSc1	traceur
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
pak	pak	k6eAd1	pak
traceuse	traceuse	k6eAd1	traceuse
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
počeštěným	počeštěný	k2eAgInSc7d1	počeštěný
výrazem	výraz	k1gInSc7	výraz
traceurka	traceurek	k1gMnSc2	traceurek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Definován	definován	k2eAgInSc1d1	definován
Francouzem	Francouz	k1gMnSc7	Francouz
Davidem	David	k1gMnSc7	David
Bellem	bell	k1gInSc7	bell
<g/>
,	,	kIx,	,
parkour	parkour	k1gMnSc1	parkour
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
trénink	trénink	k1gInSc4	trénink
účinných	účinný	k2eAgMnPc2d1	účinný
pohybů	pohyb	k1gInPc2	pohyb
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
situacích	situace	k1gFnPc6	situace
(	(	kIx(	(
<g/>
a	a	k8xC	a
především	především	k9	především
kritických	kritický	k2eAgNnPc2d1	kritické
<g/>
)	)	kIx)	)
uměl	umět	k5eAaImAgInS	umět
pohybovat	pohybovat	k5eAaImF	pohybovat
klidně	klidně	k6eAd1	klidně
a	a	k8xC	a
sebejistě	sebejistě	k6eAd1	sebejistě
<g/>
.	.	kIx.	.
</s>
<s>
Parkour	Parkoura	k1gFnPc2	Parkoura
je	být	k5eAaImIp3nS	být
fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lze	lze	k6eAd1	lze
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
přiřadit	přiřadit	k5eAaPmF	přiřadit
kategorii	kategorie	k1gFnSc4	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
extrémní	extrémní	k2eAgInSc1d1	extrémní
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
umění	umění	k1gNnSc1	umění
nebo	nebo	k8xC	nebo
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
sebeobraně	sebeobrana	k1gFnSc3	sebeobrana
v	v	k7c6	v
bojových	bojový	k2eAgNnPc6d1	bojové
uměních	umění	k1gNnPc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Davida	David	k1gMnSc2	David
Bella	Bell	k1gMnSc2	Bell
je	být	k5eAaImIp3nS	být
fyzická	fyzický	k2eAgFnSc1d1	fyzická
část	část	k1gFnSc1	část
parkouru	parkour	k1gInSc2	parkour
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
překonávání	překonávání	k1gNnSc4	překonávání
všech	všecek	k3xTgFnPc2	všecek
překážek	překážka	k1gFnPc2	překážka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kdyby	kdyby	kYmCp3nS	kdyby
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
stav	stav	k1gInSc4	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
–	–	k?	–
chcete	chtít	k5eAaImIp2nP	chtít
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vám	vy	k3xPp2nPc3	vy
pomůže	pomoct	k5eAaPmIp3nS	pomoct
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
od	od	k7c2	od
<g/>
/	/	kIx~	/
<g/>
k	k	k7c3	k
někomu	někdo	k3yInSc3	někdo
<g/>
/	/	kIx~	/
<g/>
něčemu	něco	k3yInSc3	něco
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
protivníkem	protivník	k1gMnSc7	protivník
může	moct	k5eAaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
utíkat	utíkat	k5eAaImF	utíkat
<g/>
.	.	kIx.	.
</s>
<s>
Bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnPc1	umění
připravují	připravovat	k5eAaImIp3nP	připravovat
na	na	k7c4	na
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
parkour	parkour	k1gMnSc1	parkour
je	být	k5eAaImIp3nS	být
formou	forma	k1gFnSc7	forma
tréninku	trénink	k1gInSc2	trénink
pro	pro	k7c4	pro
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
složitosti	složitost	k1gFnSc3	složitost
určení	určení	k1gNnSc2	určení
a	a	k8xC	a
zařazení	zařazení	k1gNnSc2	zařazení
se	se	k3xPyFc4	se
často	často	k6eAd1	často
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
parkour	parkour	k1gMnSc1	parkour
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kategorii	kategorie	k1gFnSc4	kategorie
–	–	k?	–
parkour	parkour	k1gMnSc1	parkour
je	být	k5eAaImIp3nS	být
parkour	parkour	k1gMnSc1	parkour
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
charakteristikou	charakteristika	k1gFnSc7	charakteristika
parkouru	parkour	k1gInSc2	parkour
je	být	k5eAaImIp3nS	být
efektivita	efektivita	k1gFnSc1	efektivita
(	(	kIx(	(
<g/>
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Traceur	Traceur	k1gMnSc1	Traceur
se	se	k3xPyFc4	se
nesnaží	snažit	k5eNaImIp3nS	snažit
pohybovat	pohybovat	k5eAaImF	pohybovat
jen	jen	k9	jen
nejrychleji	rychle	k6eAd3	rychle
jak	jak	k6eAd1	jak
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
s	s	k7c7	s
co	co	k9	co
nejmenší	malý	k2eAgFnSc7d3	nejmenší
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
nejpřímější	přímý	k2eAgFnSc7d3	nejpřímější
možnou	možný	k2eAgFnSc7d1	možná
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
protože	protože	k8xS	protože
neoficiálním	oficiální	k2eNgNnSc7d1	neoficiální
parkourovým	parkourův	k2eAgNnSc7d1	parkourův
mottem	motto	k1gNnSc7	motto
je	být	k5eAaImIp3nS	být
ê	ê	k?	ê
et	et	k?	et
durer	durer	k1gInSc1	durer
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
a	a	k8xC	a
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
a	a	k8xC	a
vydržet	vydržet	k5eAaPmF	vydržet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
efektivita	efektivita	k1gFnSc1	efektivita
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
předcházení	předcházení	k1gNnSc1	předcházení
zraněním	zranění	k1gNnSc7	zranění
krátko	krátko	k6eAd1	krátko
i	i	k9	i
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
parkour	parkour	k1gMnSc1	parkour
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
traceurův	traceurův	k2eAgInSc1d1	traceurův
proces	proces	k1gInSc1	proces
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Traceuři	Traceur	k1gMnPc1	Traceur
a	a	k8xC	a
traceurky	traceurka	k1gFnPc1	traceurka
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
myšlení	myšlení	k1gNnSc6	myšlení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
překážky	překážka	k1gFnPc4	překážka
fyzické	fyzický	k2eAgNnSc4d1	fyzické
či	či	k8xC	či
psychické	psychický	k2eAgNnSc4d1	psychické
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
motto	motto	k1gNnSc1	motto
ê	ê	k?	ê
et	et	k?	et
durer	durer	k1gInSc1	durer
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ê	ê	k?	ê
fort	fort	k?	fort
pour	pour	k1gInSc1	pour
ê	ê	k?	ê
utile	utile	k1gInSc1	utile
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
být	být	k5eAaImF	být
silným	silný	k2eAgMnPc3d1	silný
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
byl	být	k5eAaImAgInS	být
užitečným	užitečný	k2eAgInSc7d1	užitečný
podle	podle	k7c2	podle
Hébertovy	Hébertův	k2eAgFnSc2d1	Hébertův
Přirozené	přirozený	k2eAgFnSc2d1	přirozená
metody	metoda	k1gFnSc2	metoda
<g/>
)	)	kIx)	)
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
umět	umět	k5eAaImF	umět
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
bodu	bod	k1gInSc2	bod
A	a	k8xC	a
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
B	B	kA	B
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
plynule	plynule	k6eAd1	plynule
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tréninkem	trénink	k1gInSc7	trénink
traceur	traceur	k1gMnSc1	traceur
<g/>
/	/	kIx~	/
<g/>
ka	ka	k?	ka
začíná	začínat	k5eAaImIp3nS	začínat
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
pohnutkách	pohnutka	k1gFnPc6	pohnutka
<g/>
,	,	kIx,	,
důvodech	důvod	k1gInPc6	důvod
k	k	k7c3	k
tréninku	trénink	k1gInSc3	trénink
<g/>
,	,	kIx,	,
parkourové	parkourový	k2eAgFnSc6d1	parkourová
filosofii	filosofie	k1gFnSc6	filosofie
atd.	atd.	kA	atd.
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
du	du	k?	du
déplacement	déplacement	k1gMnSc1	déplacement
a	a	k8xC	a
le	le	k?	le
parcours	parcours	k1gInSc1	parcours
byly	být	k5eAaImAgFnP	být
prvními	první	k4xOgMnPc7	první
pojmy	pojem	k1gInPc7	pojem
použitými	použitý	k2eAgInPc7d1	použitý
k	k	k7c3	k
popsání	popsání	k1gNnSc3	popsání
metody	metoda	k1gFnSc2	metoda
tréninku	trénink	k1gInSc2	trénink
pohybů	pohyb	k1gInPc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
parkour	parkoura	k1gFnPc2	parkoura
IPA	IPA	kA	IPA
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
paʁ	paʁ	k?	paʁ
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
kuʁ	kuʁ	k?	kuʁ
<g/>
]	]	kIx)	]
definovali	definovat	k5eAaBmAgMnP	definovat
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Hubert	Hubert	k1gMnSc1	Hubert
Koundé	Koundý	k2eAgFnSc2d1	Koundý
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
parcours	parcours	k1gInSc1	parcours
du	du	k?	du
combatant	combatant	k1gMnSc1	combatant
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
poeticky	poeticky	k6eAd1	poeticky
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
cesta	cesta	k1gFnSc1	cesta
bojovníka	bojovník	k1gMnSc2	bojovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
vojenská	vojenský	k2eAgFnSc1d1	vojenská
výcviková	výcvikový	k2eAgFnSc1d1	výcviková
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
navržená	navržený	k2eAgFnSc1d1	navržená
Georgesem	Georges	k1gMnSc7	Georges
Hébertem	Hébert	k1gMnSc7	Hébert
<g/>
.	.	kIx.	.
</s>
<s>
Koundé	Koundý	k2eAgNnSc1d1	Koundý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sám	sám	k3xTgInSc4	sám
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
traceurem	traceur	k1gMnSc7	traceur
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
c	c	k0	c
písmenem	písmeno	k1gNnSc7	písmeno
k	k	k7c3	k
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
větší	veliký	k2eAgFnSc4d2	veliký
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
odstranil	odstranit	k5eAaPmAgMnS	odstranit
s	s	k7c7	s
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nadbytečné	nadbytečný	k2eAgNnSc1d1	nadbytečné
a	a	k8xC	a
odporuje	odporovat	k5eAaImIp3nS	odporovat
filosofii	filosofie	k1gFnSc4	filosofie
efektivity	efektivita	k1gFnSc2	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
du	du	k?	du
combatant	combatant	k1gMnSc1	combatant
bylo	být	k5eAaImAgNnS	být
úplně	úplně	k6eAd1	úplně
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
nebo	nebo	k8xC	nebo
bojový	bojový	k2eAgInSc4d1	bojový
výcvik	výcvik	k1gInSc4	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Traceur	Traceur	k1gMnSc1	Traceur
IPA	IPA	kA	IPA
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
tʁ	tʁ	k?	tʁ
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
sœ	sœ	k?	sœ
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
substantivum	substantivum	k1gNnSc1	substantivum
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
francouzského	francouzský	k2eAgNnSc2d1	francouzské
slovesa	sloveso	k1gNnSc2	sloveso
tracer	tracra	k1gFnPc2	tracra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obvykle	obvykle	k6eAd1	obvykle
znamená	znamenat	k5eAaImIp3nS	znamenat
načrtnout	načrtnout	k5eAaPmF	načrtnout
<g/>
,	,	kIx,	,
vyznačit	vyznačit	k5eAaPmF	vyznačit
<g/>
,	,	kIx,	,
vytyčit	vytyčit	k5eAaPmF	vytyčit
<g/>
;	;	kIx,	;
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
slangu	slang	k1gInSc6	slang
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
znamenat	znamenat	k5eAaImF	znamenat
také	také	k9	také
běžet	běžet	k5eAaImF	běžet
<g/>
;	;	kIx,	;
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tracer	tracer	k1gInSc1	tracer
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
následuje	následovat	k5eAaImIp3nS	následovat
svoji	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Parkour	Parkour	k1gMnSc1	Parkour
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
mnohými	mnohý	k2eAgInPc7d1	mnohý
zdroji	zdroj	k1gInPc7	zdroj
<g/>
,	,	kIx,	,
především	především	k9	především
Přirozenou	přirozený	k2eAgFnSc7d1	přirozená
metodou	metoda	k1gFnSc7	metoda
Georgese	Georgese	k1gFnSc2	Georgese
Héberta	Héberta	k1gFnSc1	Héberta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
vojáci	voják	k1gMnPc1	voják
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
se	se	k3xPyFc4	se
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
Hébertovou	Hébertový	k2eAgFnSc7d1	Hébertový
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známo	znám	k2eAgNnSc1d1	známo
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
parcours	parcours	k1gInSc1	parcours
du	du	k?	du
combattant	combattant	k1gMnSc1	combattant
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
překážkové	překážkový	k2eAgFnSc3d1	překážková
trati	trať	k1gFnSc3	trať
a	a	k8xC	a
Hébertově	Hébertův	k2eAgFnSc3d1	Hébertův
Přirozené	přirozený	k2eAgFnSc3d1	přirozená
metodě	metoda	k1gFnSc3	metoda
dostal	dostat	k5eAaPmAgMnS	dostat
díky	díky	k7c3	díky
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
Raymondu	Raymond	k1gMnSc3	Raymond
Bellovi	Bell	k1gMnSc3	Bell
<g/>
,	,	kIx,	,
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
hasiči	hasič	k1gMnSc3	hasič
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
těmto	tento	k3xDgFnPc3	tento
disciplínám	disciplína	k1gFnPc3	disciplína
věnoval	věnovat	k5eAaPmAgMnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
trénoval	trénovat	k5eAaImAgMnS	trénovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
disciplíny	disciplína	k1gFnPc4	disciplína
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnSc4	umění
či	či	k8xC	či
gymnastiku	gymnastika	k1gFnSc4	gymnastika
<g/>
,	,	kIx,	,
a	a	k8xC	a
hledal	hledat	k5eAaImAgMnS	hledat
možnost	možnost	k1gFnSc4	možnost
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestěhování	přestěhování	k1gNnSc6	přestěhování
do	do	k7c2	do
Lisses	Lissesa	k1gFnPc2	Lissesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
předměstí	předměstí	k1gNnSc6	předměstí
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přidala	přidat	k5eAaPmAgFnS	přidat
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sébastien	Sébastien	k2eAgInSc1d1	Sébastien
Foucan	Foucan	k1gInSc1	Foucan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
trénovala	trénovat	k5eAaImAgFnS	trénovat
<g/>
,	,	kIx,	,
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
definice	definice	k1gFnPc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
tak	tak	k9	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skupina	skupina	k1gFnSc1	skupina
Yamakasi	Yamakas	k1gMnPc1	Yamakas
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
Lingala	Lingala	k1gFnSc1	Lingala
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
silné	silný	k2eAgNnSc1d1	silné
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
silná	silný	k2eAgFnSc1d1	silná
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
silný	silný	k2eAgMnSc1d1	silný
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
pak	pak	k6eAd1	pak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
disciplíny	disciplína	k1gFnSc2	disciplína
–	–	k?	–
parkour	parkour	k1gMnSc1	parkour
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Yamakasi	Yamakas	k1gMnPc1	Yamakas
se	se	k3xPyFc4	se
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
ohledně	ohledně	k7c2	ohledně
show	show	k1gFnPc2	show
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gNnPc7	Dame
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
odchází	odcházet	k5eAaImIp3nS	odcházet
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
když	když	k8xS	když
Yamakasi	Yamakas	k1gMnPc1	Yamakas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
natočí	natočit	k5eAaBmIp3nP	natočit
film	film	k1gInSc1	film
Yamakasi	Yamakas	k1gMnPc1	Yamakas
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
ho	on	k3xPp3gNnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
prostituci	prostituce	k1gFnSc4	prostituce
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
odvysílán	odvysílán	k2eAgInSc4d1	odvysílán
pořad	pořad	k1gInSc4	pořad
Jump	Jump	k1gMnSc1	Jump
London	London	k1gMnSc1	London
se	s	k7c7	s
Sébastienem	Sébastien	k1gMnSc7	Sébastien
Foucanem	Foucan	k1gMnSc7	Foucan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
šíření	šíření	k1gNnSc4	šíření
parkouru	parkour	k1gInSc2	parkour
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
vzniká	vznikat	k5eAaImIp3nS	vznikat
nový	nový	k2eAgInSc4d1	nový
proud	proud	k1gInSc4	proud
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
oddělující	oddělující	k2eAgFnSc1d1	oddělující
se	se	k3xPyFc4	se
disciplína	disciplína	k1gFnSc1	disciplína
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Free	Free	k1gNnSc4	Free
running	running	k1gInSc1	running
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
založil	založit	k5eAaPmAgMnS	založit
Jerome	Jerom	k1gInSc5	Jerom
Lebret	Lebret	k1gInSc1	Lebret
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Davida	David	k1gMnSc2	David
Bella	Bell	k1gMnSc2	Bell
portál	portál	k1gInSc1	portál
Parkour	Parkoura	k1gFnPc2	Parkoura
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
(	(	kIx(	(
<g/>
známý	známý	k1gMnSc1	známý
mezi	mezi	k7c7	mezi
traceury	traceur	k1gMnPc7	traceur
jako	jako	k8xS	jako
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
fórum	fórum	k1gNnSc1	fórum
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stalo	stát	k5eAaPmAgNnS	stát
virtuálním	virtuální	k2eAgInSc7d1	virtuální
domovem	domov	k1gInSc7	domov
nejzkušenějších	zkušený	k2eAgMnPc2d3	nejzkušenější
a	a	k8xC	a
nejrespektovanějších	respektovaný	k2eAgMnPc2d3	nejrespektovanější
traceurů	traceur	k1gMnPc2	traceur
a	a	k8xC	a
traceurek	traceurka	k1gFnPc2	traceurka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
založil	založit	k5eAaPmAgMnS	založit
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
asociaci	asociace	k1gFnSc4	asociace
PAWA	PAWA	kA	PAWA
(	(	kIx(	(
<g/>
PArkour	PArkour	k1gMnSc1	PArkour
Worldwide	Worldwid	k1gInSc5	Worldwid
Association	Association	k1gInSc1	Association
–	–	k?	–
Světová	světový	k2eAgFnSc1d1	světová
parkourová	parkourový	k2eAgFnSc1d1	parkourová
asociace	asociace	k1gFnSc1	asociace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
měla	mít	k5eAaImAgFnS	mít
pomoci	pomoc	k1gFnSc2	pomoc
šířit	šířit	k5eAaImF	šířit
správnou	správný	k2eAgFnSc4d1	správná
myšlenku	myšlenka	k1gFnSc4	myšlenka
parkouru	parkour	k1gInSc2	parkour
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
brzy	brzy	k6eAd1	brzy
přestala	přestat	k5eAaPmAgFnS	přestat
plnit	plnit	k5eAaImF	plnit
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
David	David	k1gMnSc1	David
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
opustil	opustit	k5eAaPmAgMnS	opustit
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
vyjádřením	vyjádření	k1gNnSc7	vyjádření
<g/>
:	:	kIx,	:
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
dalšímu	další	k2eAgNnSc3d1	další
komerčnímu	komerční	k2eAgNnSc3d1	komerční
a	a	k8xC	a
filosofickému	filosofický	k2eAgNnSc3d1	filosofické
zneužití	zneužití	k1gNnSc3	zneužití
parkouru	parkour	k1gInSc2	parkour
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neručí	ručit	k5eNaImIp3nS	ručit
za	za	k7c4	za
žádnou	žádný	k3yNgFnSc4	žádný
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
asociaci	asociace	k1gFnSc4	asociace
nebo	nebo	k8xC	nebo
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předstírá	předstírat	k5eAaImIp3nS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgInSc1d1	oficiální
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgFnSc1d1	oficiální
jen	jen	k9	jen
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
si	se	k3xPyFc3	se
dala	dát	k5eAaPmAgFnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
vznik	vznik	k1gInSc4	vznik
skupiny	skupina	k1gFnSc2	skupina
Team	team	k1gInSc1	team
Parkour	Parkour	k1gMnSc1	Parkour
by	by	kYmCp3nS	by
D.	D.	kA	D.
Belle	bell	k1gInSc5	bell
a	a	k8xC	a
Bellových	Bellův	k2eAgFnPc2d1	Bellova
osobních	osobní	k2eAgFnPc2d1	osobní
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgFnPc2d1	důležitá
částí	část	k1gFnPc2	část
parkouru	parkour	k1gInSc2	parkour
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Yamakasi	Yamakas	k1gMnPc1	Yamakas
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nedílnou	dílný	k2eNgFnSc4d1	nedílná
součást	součást	k1gFnSc4	součást
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
Williamse	Williams	k1gMnSc2	Williams
Bella	Bell	k1gMnSc2	Bell
<g/>
:	:	kIx,	:
Dalším	další	k2eAgInSc7d1	další
aspektem	aspekt	k1gInSc7	aspekt
filosofie	filosofie	k1gFnSc2	filosofie
je	být	k5eAaImIp3nS	být
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
opakováno	opakován	k2eAgNnSc1d1	opakováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
parkouru	parkoura	k1gFnSc4	parkoura
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
věnovat	věnovat	k5eAaImF	věnovat
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
<g/>
,	,	kIx,	,
kdekoliv	kdekoliv	k6eAd1	kdekoliv
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
je	být	k5eAaImIp3nS	být
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k9	jako
možnost	možnost	k1gFnSc1	možnost
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jiní	jiný	k2eAgMnPc1d1	jiný
nemohou	moct	k5eNaImIp3nP	moct
<g/>
,	,	kIx,	,
způsobem	způsob	k1gInSc7	způsob
vyjádřeným	vyjádřený	k2eAgInSc7d1	vyjádřený
výše	vysoce	k6eAd2	vysoce
(	(	kIx(	(
<g/>
bezpečně	bezpečně	k6eAd1	bezpečně
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
plynule	plynule	k6eAd1	plynule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Châu	Châu	k6eAd1	Châu
Belle-Dinh	Belle-Dinh	k1gInSc1	Belle-Dinh
uvádí	uvádět	k5eAaImIp3nS	uvádět
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
pouhou	pouhý	k2eAgFnSc4d1	pouhá
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
definici	definice	k1gFnSc4	definice
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
učení	učení	k1gNnSc2	učení
se	se	k3xPyFc4	se
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
umění	umění	k1gNnSc3	umění
a	a	k8xC	a
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
schopnost	schopnost	k1gFnSc1	schopnost
překonat	překonat	k5eAaPmF	překonat
obavy	obava	k1gFnPc4	obava
a	a	k8xC	a
bolesti	bolest	k1gFnPc4	bolest
a	a	k8xC	a
aplikovat	aplikovat	k5eAaBmF	aplikovat
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
zvládnutí	zvládnutí	k1gNnSc3	zvládnutí
parkouru	parkour	k1gInSc2	parkour
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
i	i	k9	i
ovládání	ovládání	k1gNnSc4	ovládání
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Andreas	Andreas	k1gInSc1	Andreas
Kalteis	Kalteis	k1gFnSc2	Kalteis
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
traceur	traceur	k1gMnSc1	traceur
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
Parkour	Parkour	k1gMnSc1	Parkour
Journeys	Journeys	k1gInSc1	Journeys
<g/>
:	:	kIx,	:
Akce	akce	k1gFnSc1	akce
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
na	na	k7c6	na
portále	portál	k1gInSc6	portál
Parkour	Parkoura	k1gFnPc2	Parkoura
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zachovat	zachovat	k5eAaPmF	zachovat
filosofii	filosofie	k1gFnSc4	filosofie
parkouru	parkour	k1gInSc2	parkour
–	–	k?	–
proti	proti	k7c3	proti
soutěžím	soutěž	k1gFnPc3	soutěž
a	a	k8xC	a
rivalitě	rivalita	k1gFnSc3	rivalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parkouru	parkour	k1gInSc6	parkour
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
předtvořených	předtvořený	k2eAgFnPc2d1	předtvořený
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
než	než	k8xS	než
například	například	k6eAd1	například
v	v	k7c6	v
gymnastice	gymnastika	k1gFnSc6	gymnastika
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
parkour	parkour	k1gMnSc1	parkour
není	být	k5eNaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
jen	jen	k9	jen
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
přesně	přesně	k6eAd1	přesně
daných	daný	k2eAgInPc2d1	daný
pohybů	pohyb	k1gInPc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
překážka	překážka	k1gFnSc1	překážka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
traceur	traceur	k1gMnSc1	traceur
setká	setkat	k5eAaPmIp3nS	setkat
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
unikátní	unikátní	k2eAgFnSc4d1	unikátní
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
efektivním	efektivní	k2eAgInSc7d1	efektivní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
úhlu	úhel	k1gInSc2	úhel
náběhu	náběh	k1gInSc2	náběh
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
překážky	překážka	k1gFnSc2	překážka
apod.	apod.	kA	apod.
Parkour	Parkour	k1gMnSc1	Parkour
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
tréninkem	trénink	k1gInSc7	trénink
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
mysli	mysl	k1gFnSc2	mysl
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zlepšení	zlepšení	k1gNnSc2	zlepšení
schopnosti	schopnost	k1gFnSc2	schopnost
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
překážku	překážka	k1gFnSc4	překážka
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
;	;	kIx,	;
mnohdy	mnohdy	k6eAd1	mnohdy
technikou	technika	k1gFnSc7	technika
nepopsanou	popsaný	k2eNgFnSc7d1	nepopsaná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ani	ani	k8xC	ani
popsat	popsat	k5eAaPmF	popsat
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
závisí	záviset	k5eAaImIp3nS	záviset
efektivita	efektivita	k1gFnSc1	efektivita
na	na	k7c6	na
rychlém	rychlý	k2eAgNnSc6d1	rychlé
přesouvání	přesouvání	k1gNnSc6	přesouvání
těžiště	těžiště	k1gNnSc2	těžiště
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
pohybové	pohybový	k2eAgFnSc2d1	pohybová
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
provedení	provedení	k1gNnSc3	provedení
"	"	kIx"	"
<g/>
nemožných	možný	k2eNgFnPc6d1	nemožná
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
náročných	náročný	k2eAgInPc2d1	náročný
tělesných	tělesný	k2eAgInPc2d1	tělesný
pohybů	pohyb	k1gInPc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Absorpce	absorpce	k1gFnSc1	absorpce
a	a	k8xC	a
přesouvání	přesouvání	k1gNnSc1	přesouvání
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
–	–	k?	–
provedení	provedení	k1gNnSc6	provedení
parakotoulu	parakotoul	k1gInSc2	parakotoul
snižuje	snižovat	k5eAaImIp3nS	snižovat
náraz	náraz	k1gInSc1	náraz
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
páteř	páteř	k1gFnSc4	páteř
a	a	k8xC	a
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
traceurovi	traceur	k1gMnSc3	traceur
skoky	skok	k1gInPc4	skok
z	z	k7c2	z
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
gymnastikou	gymnastika	k1gFnSc7	gymnastika
či	či	k8xC	či
akrobacií	akrobacie	k1gFnSc7	akrobacie
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnPc4d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Davida	David	k1gMnSc2	David
Bella	Bell	k1gMnSc2	Bell
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vám	vy	k3xPp2nPc3	vy
pomůže	pomoct	k5eAaPmIp3nS	pomoct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
výsledku	výsledek	k1gInSc2	výsledek
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
či	či	k8xC	či
běhu	běh	k1gInSc6	běh
od	od	k7c2	od
<g/>
/	/	kIx~	/
<g/>
k	k	k7c3	k
někomu	někdo	k3yInSc3	někdo
<g/>
/	/	kIx~	/
<g/>
něčemu	něco	k3yInSc3	něco
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
traceur	traceur	k1gMnSc1	traceur
někam	někam	k6eAd1	někam
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
z	z	k7c2	z
A	A	kA	A
do	do	k7c2	do
B	B	kA	B
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
z	z	k7c2	z
B	B	kA	B
do	do	k7c2	do
A	A	kA	A
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
nutně	nutně	k6eAd1	nutně
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
stejných	stejný	k2eAgFnPc2d1	stejná
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgInSc3	ten
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
základních	základní	k2eAgFnPc2d1	základní
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
všestrannost	všestrannost	k1gFnSc4	všestrannost
a	a	k8xC	a
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
ovládnout	ovládnout	k5eAaPmF	ovládnout
skok	skok	k1gInSc4	skok
a	a	k8xC	a
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Parakotoul	Parakotoul	k1gInSc1	Parakotoul
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
zmírnění	zmírnění	k1gNnSc4	zmírnění
dopadu	dopad	k1gInSc2	dopad
a	a	k8xC	a
přenos	přenos	k1gInSc1	přenos
pohybové	pohybový	k2eAgFnSc2d1	pohybová
energie	energie	k1gFnSc2	energie
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zdůrazňován	zdůrazňován	k2eAgInSc1d1	zdůrazňován
jako	jako	k8xS	jako
naprosto	naprosto	k6eAd1	naprosto
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
nezkušení	zkušený	k2eNgMnPc1d1	nezkušený
traceuři	traceur	k1gMnPc1	traceur
se	se	k3xPyFc4	se
potýkají	potýkat	k5eAaImIp3nP	potýkat
s	s	k7c7	s
kloubními	kloubní	k2eAgInPc7d1	kloubní
problémy	problém	k1gInPc7	problém
kvůli	kvůli	k7c3	kvůli
skokům	skok	k1gInPc3	skok
z	z	k7c2	z
velkých	velká	k1gFnPc2	velká
výšek	výška	k1gFnPc2	výška
a	a	k8xC	a
špatným	špatný	k2eAgInPc3d1	špatný
dopadům	dopad	k1gInPc3	dopad
a	a	k8xC	a
parakotoulům	parakotoul	k1gInPc3	parakotoul
<g/>
.	.	kIx.	.
</s>
<s>
Parkour	Parkour	k1gMnSc1	Parkour
užívá	užívat	k5eAaImIp3nS	užívat
francouzského	francouzský	k2eAgNnSc2d1	francouzské
názvosloví	názvosloví	k1gNnSc2	názvosloví
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc2d1	mluvící
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
používání	používání	k1gNnSc4	používání
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
byly	být	k5eAaImAgFnP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
i	i	k8xC	i
anglické	anglický	k2eAgInPc1d1	anglický
názvy	název	k1gInPc1	název
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
názvosloví	názvosloví	k1gNnSc1	názvosloví
neexistuje	existovat	k5eNaImIp3nS	existovat
(	(	kIx(	(
<g/>
snad	snad	k9	snad
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
výrazů	výraz	k1gInPc2	výraz
dopad	dopad	k1gInSc4	dopad
namísto	namísto	k7c2	namísto
landing	landing	k1gInSc4	landing
a	a	k8xC	a
parakotoul	parakotoul	k1gInSc4	parakotoul
namísto	namísto	k7c2	namísto
roulade	roulad	k1gInSc5	roulad
či	či	k8xC	či
roll	rolla	k1gFnPc2	rolla
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnPc1d1	základní
techniky	technika	k1gFnPc1	technika
známé	známý	k2eAgFnPc1d1	známá
v	v	k7c6	v
parkouru	parkour	k1gInSc6	parkour
<g/>
:	:	kIx,	:
Back	Back	k1gMnSc1	Back
smash	smash	k1gMnSc1	smash
je	být	k5eAaImIp3nS	být
trik	trik	k1gInSc4	trik
pri	pri	k?	pri
kterém	který	k3yIgInSc6	který
udelate	udelat	k1gInSc5	udelat
konga	kong	k1gMnSc4	kong
pozadu	pozadu	k6eAd1	pozadu
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trik	trik	k1gInSc1	trik
není	být	k5eNaImIp3nS	být
moc	moc	k6eAd1	moc
známí	známý	k2eAgMnPc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
věnující	věnující	k2eAgMnPc1d1	věnující
se	se	k3xPyFc4	se
parkouru	parkour	k1gMnSc6	parkour
obvykle	obvykle	k6eAd1	obvykle
utratí	utratit	k5eAaPmIp3nS	utratit
za	za	k7c2	za
parkour	parkoura	k1gFnPc2	parkoura
jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
trénují	trénovat	k5eAaImIp3nP	trénovat
v	v	k7c6	v
jednoduchém	jednoduchý	k2eAgNnSc6d1	jednoduché
<g/>
,	,	kIx,	,
lehkém	lehký	k2eAgNnSc6d1	lehké
<g/>
,	,	kIx,	,
sportovním	sportovní	k2eAgNnSc6d1	sportovní
oblečení	oblečení	k1gNnSc6	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Vybavení	vybavení	k1gNnSc1	vybavení
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
skládá	skládat	k5eAaImIp3nS	skládat
jen	jen	k9	jen
z	z	k7c2	z
<g/>
:	:	kIx,	:
Pohodlné	pohodlný	k2eAgFnSc2d1	pohodlná
sportovní	sportovní	k2eAgInSc4d1	sportovní
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
běžecké	běžecký	k2eAgFnSc2d1	běžecká
<g/>
)	)	kIx)	)
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
,	,	kIx,	,
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
přilnavostí	přilnavost	k1gFnSc7	přilnavost
<g/>
,	,	kIx,	,
oporou	opora	k1gFnSc7	opora
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
tlumením	tlumení	k1gNnSc7	tlumení
dopadů	dopad	k1gInPc2	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
gelové	gelový	k2eAgFnPc1d1	gelová
vložky	vložka	k1gFnPc1	vložka
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
také	také	k9	také
z	z	k7c2	z
rukavic	rukavice	k1gFnPc2	rukavice
(	(	kIx(	(
<g/>
lehkých	lehký	k2eAgFnPc2d1	lehká
atletických	atletický	k2eAgFnPc2d1	atletická
či	či	k8xC	či
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
ruce	ruka	k1gFnPc4	ruka
proti	proti	k7c3	proti
oděru	oděr	k1gInSc3	oděr
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenější	zkušený	k2eAgMnPc1d2	zkušenější
traceuři	traceur	k1gMnPc1	traceur
používání	používání	k1gNnSc2	používání
rukavic	rukavice	k1gFnPc2	rukavice
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
je	on	k3xPp3gNnSc4	on
za	za	k7c4	za
nepřirozené	přirozený	k2eNgMnPc4d1	nepřirozený
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
parkour	parkour	k1gMnSc1	parkour
spřízněn	spříznit	k5eAaPmNgMnS	spříznit
s	s	k7c7	s
Přirozenou	přirozený	k2eAgFnSc7d1	přirozená
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
traceuři	traceur	k1gMnPc1	traceur
někdy	někdy	k6eAd1	někdy
trénují	trénovat	k5eAaImIp3nP	trénovat
bosí	bosý	k2eAgMnPc1d1	bosý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
uměli	umět	k5eAaImAgMnP	umět
pohybovat	pohybovat	k5eAaImF	pohybovat
efektivně	efektivně	k6eAd1	efektivně
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
holé	holý	k2eAgFnPc1d1	holá
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
botami	bota	k1gFnPc7	bota
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Parkour	Parkour	k1gMnSc1	Parkour
se	se	k3xPyFc4	se
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
dostal	dostat	k5eAaPmAgMnS	dostat
snad	snad	k9	snad
ještě	ještě	k9	ještě
před	před	k7c7	před
britským	britský	k2eAgNnSc7d1	Britské
vysíláním	vysílání	k1gNnSc7	vysílání
Jump	Jump	k1gMnSc1	Jump
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
výrazněji	výrazně	k6eAd2	výrazně
projevovat	projevovat	k5eAaImF	projevovat
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
pražská	pražský	k2eAgFnSc1d1	Pražská
Lezz	Lezz	k1gInSc4	Lezz
Kref	Kref	k1gMnSc1	Kref
a	a	k8xC	a
zlínská	zlínský	k2eAgFnSc1d1	zlínská
Streaks	Streaks	k1gInSc1	Streaks
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
osamocenosti	osamocenost	k1gFnSc3	osamocenost
a	a	k8xC	a
neexistenci	neexistence	k1gFnSc3	neexistence
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
základny	základna	k1gFnSc2	základna
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc1	skupina
postupně	postupně	k6eAd1	postupně
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
2004	[number]	k4	2004
a	a	k8xC	a
2005	[number]	k4	2005
přišla	přijít	k5eAaPmAgFnS	přijít
velká	velký	k2eAgFnSc1d1	velká
vlna	vlna	k1gFnSc1	vlna
zájmu	zájem	k1gInSc2	zájem
o	o	k7c6	o
parkour	parkour	k1gMnSc1	parkour
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měla	mít	k5eAaImAgFnS	mít
videa	video	k1gNnSc2	video
Davida	David	k1gMnSc2	David
Bella	Bell	k1gMnSc2	Bell
<g/>
,	,	kIx,	,
šířící	šířící	k2eAgInSc4d1	šířící
se	se	k3xPyFc4	se
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgFnSc2d1	známá
skupiny	skupina	k1gFnSc2	skupina
Real	Real	k1gInSc1	Real
Motion	Motion	k1gInSc1	Motion
či	či	k8xC	či
1	[number]	k4	1
<g/>
Bcrew	Bcrew	k1gFnPc2	Bcrew
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
skupin	skupina	k1gFnPc2	skupina
ovšem	ovšem	k9	ovšem
brzy	brzy	k6eAd1	brzy
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
právě	právě	k9	právě
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
parkour	parkour	k1gMnSc1	parkour
praktikovat	praktikovat	k5eAaImF	praktikovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
základnu	základna	k1gFnSc4	základna
českého	český	k2eAgInSc2d1	český
parkouru	parkour	k1gInSc2	parkour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
1	[number]	k4	1
<g/>
.	.	kIx.	.
český	český	k2eAgInSc1d1	český
parkourový	parkourový	k2eAgInSc1d1	parkourový
jam	jam	k1gInSc1	jam
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
setkání	setkání	k1gNnPc4	setkání
českých	český	k2eAgMnPc2d1	český
traceurů	traceur	k1gMnPc2	traceur
a	a	k8xC	a
freerunnerů	freerunner	k1gMnPc2	freerunner
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
ještě	ještě	k6eAd1	ještě
příliš	příliš	k6eAd1	příliš
neoddělovaly	oddělovat	k5eNaImAgInP	oddělovat
pojmy	pojem	k1gInPc4	pojem
free	fre	k1gFnSc2	fre
running	running	k1gInSc1	running
a	a	k8xC	a
parkour	parkour	k1gMnSc1	parkour
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
asi	asi	k9	asi
30	[number]	k4	30
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
české	český	k2eAgFnSc3d1	Česká
komunitě	komunita	k1gFnSc3	komunita
představuje	představovat	k5eAaImIp3nS	představovat
portál	portál	k1gInSc1	portál
Parkour	Parkoura	k1gFnPc2	Parkoura
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
od	od	k7c2	od
zanikajících	zanikající	k2eAgFnPc2d1	zanikající
Streaks	Streaksa	k1gFnPc2	Streaksa
přebírá	přebírat	k5eAaImIp3nS	přebírat
Toshiro	Toshiro	k1gNnSc1	Toshiro
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
snaží	snažit	k5eAaImIp3nP	snažit
sdružovat	sdružovat	k5eAaImF	sdružovat
a	a	k8xC	a
informovat	informovat	k5eAaBmF	informovat
parkourovou	parkourový	k2eAgFnSc4d1	parkourová
komunitu	komunita	k1gFnSc4	komunita
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ji	on	k3xPp3gFnSc4	on
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Davida	David	k1gMnSc2	David
Bella	Bell	k1gMnSc2	Bell
měla	mít	k5eAaImAgFnS	mít
komunita	komunita	k1gFnSc1	komunita
přivítat	přivítat	k5eAaPmF	přivítat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakonec	nakonec	k6eAd1	nakonec
nepřijel	přijet	k5eNaPmAgMnS	přijet
<g/>
,	,	kIx,	,
předznamenalo	předznamenat	k5eAaPmAgNnS	předznamenat
ostravské	ostravský	k2eAgNnSc1d1	ostravské
setkání	setkání	k1gNnSc1	setkání
mnoho	mnoho	k4c4	mnoho
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
české	český	k2eAgFnSc2d1	Česká
parkourové	parkourový	k2eAgFnSc2d1	parkourová
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
znovuzaložení	znovuzaložení	k1gNnSc2	znovuzaložení
a	a	k8xC	a
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
představení	představení	k1gNnSc2	představení
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
portál	portál	k1gInSc1	portál
Parkour	Parkoura	k1gFnPc2	Parkoura
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
informovat	informovat	k5eAaBmF	informovat
českou	český	k2eAgFnSc4d1	Česká
parkourovou	parkourový	k2eAgFnSc4d1	parkourová
komunitu	komunita	k1gFnSc4	komunita
o	o	k7c6	o
myšlenkách	myšlenka	k1gFnPc6	myšlenka
<g/>
,	,	kIx,	,
tréninku	trénink	k1gInSc6	trénink
a	a	k8xC	a
filosofii	filosofie	k1gFnSc3	filosofie
parkouru	parkour	k1gInSc2	parkour
a	a	k8xC	a
také	také	k9	také
o	o	k7c6	o
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
dění	dění	k1gNnSc6	dění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
zkušení	zkušený	k2eAgMnPc1d1	zkušený
čeští	český	k2eAgMnPc1d1	český
traceuři	traceur	k1gMnPc1	traceur
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Free	Free	k1gFnSc1	Free
running	running	k1gInSc1	running
<g/>
.	.	kIx.	.
</s>
<s>
Výrazy	výraz	k1gInPc1	výraz
parkour	parkoura	k1gFnPc2	parkoura
a	a	k8xC	a
free	freat	k5eAaPmIp3nS	freat
running	running	k1gInSc1	running
(	(	kIx(	(
<g/>
freerun	freerun	k1gInSc1	freerun
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
původně	původně	k6eAd1	původně
identický	identický	k2eAgInSc4d1	identický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělily	oddělit	k5eAaPmAgFnP	oddělit
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
opomíjen	opomíjet	k5eAaImNgInS	opomíjet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
a	a	k8xC	a
Sébastien	Sébastien	k2eAgInSc4d1	Sébastien
Foucan	Foucan	k1gInSc4	Foucan
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
<g/>
,	,	kIx,	,
free	free	k1gFnSc1	free
running	running	k1gInSc1	running
se	se	k3xPyFc4	se
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
na	na	k7c4	na
estetičnost	estetičnost	k1gFnSc4	estetičnost
pohybu	pohyb	k1gInSc2	pohyb
než	než	k8xS	než
na	na	k7c4	na
efektivitu	efektivita	k1gFnSc4	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Foucan	Foucan	k1gInSc1	Foucan
definuje	definovat	k5eAaBmIp3nS	definovat
free	free	k6eAd1	free
running	running	k1gInSc4	running
jako	jako	k8xC	jako
disciplínu	disciplína	k1gFnSc4	disciplína
seberozvíjení	seberozvíjení	k1gNnSc2	seberozvíjení
a	a	k8xC	a
následování	následování	k1gNnSc4	následování
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
traceuři	traceur	k1gMnPc1	traceur
a	a	k8xC	a
traceurky	traceurka	k1gFnPc1	traceurka
trénují	trénovat	k5eAaImIp3nP	trénovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uměli	umět	k5eAaImAgMnP	umět
překonávat	překonávat	k5eAaImF	překonávat
překážky	překážka	k1gFnPc4	překážka
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
efektivněji	efektivně	k6eAd2	efektivně
<g/>
,	,	kIx,	,
freerunneři	freerunner	k1gMnPc1	freerunner
(	(	kIx(	(
<g/>
runneři	runner	k1gMnPc1	runner
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
freerunners	freerunners	k1gInSc1	freerunners
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
runners	runners	k6eAd1	runners
<g/>
)	)	kIx)	)
trénují	trénovat	k5eAaImIp3nP	trénovat
a	a	k8xC	a
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
estetické	estetický	k2eAgInPc1d1	estetický
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
překonáním	překonání	k1gNnSc7	překonání
překážky	překážka	k1gFnSc2	překážka
ani	ani	k8xC	ani
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
odlišné	odlišný	k2eAgInPc1d1	odlišný
filosofické	filosofický	k2eAgInPc1d1	filosofický
přístupy	přístup	k1gInPc1	přístup
jsou	být	k5eAaImIp3nP	být
shrnuty	shrnut	k2eAgInPc1d1	shrnut
následujícími	následující	k2eAgInPc7d1	následující
citáty	citát	k1gInPc7	citát
<g/>
:	:	kIx,	:
zkušený	zkušený	k2eAgMnSc1d1	zkušený
free	free	k6eAd1	free
runner	runner	k1gMnSc1	runner
Jerome	Jerom	k1gInSc5	Jerom
Bon	bon	k1gInSc4	bon
Aoues	Aoues	k1gInSc1	Aoues
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
Jump	Jump	k1gMnSc1	Jump
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Belle	bell	k1gInSc5	bell
<g/>
,	,	kIx,	,
PAWA	PAWA	kA	PAWA
team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
parkouru	parkour	k1gInSc2	parkour
a	a	k8xC	a
free	fre	k1gFnSc2	fre
runningu	running	k1gInSc2	running
oznámením	oznámení	k1gNnSc7	oznámení
<g/>
:	:	kIx,	:
Parkour	Parkour	k1gMnSc1	Parkour
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
televizních	televizní	k2eAgFnPc6d1	televizní
reklamách	reklama	k1gFnPc6	reklama
<g/>
,	,	kIx,	,
novinových	novinový	k2eAgFnPc6d1	novinová
zprávách	zpráva	k1gFnPc6	zpráva
a	a	k8xC	a
různých	různý	k2eAgInPc6d1	různý
zábavných	zábavný	k2eAgInPc6d1	zábavný
pořadech	pořad	k1gInPc6	pořad
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
formami	forma	k1gFnPc7	forma
akrobacie	akrobacie	k1gFnSc2	akrobacie
<g/>
,	,	kIx,	,
free	freat	k5eAaPmIp3nS	freat
runningem	running	k1gInSc7	running
<g/>
,	,	kIx,	,
street	street	k1gInSc4	street
stunts	stuntsa	k1gFnPc2	stuntsa
či	či	k8xC	či
trickingem	tricking	k1gInSc7	tricking
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
:	:	kIx,	:
Yamakasi	Yamakas	k1gMnPc1	Yamakas
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
District	District	k1gInSc1	District
13	[number]	k4	13
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Great	Great	k1gInSc1	Great
Challenge	Challenge	k1gFnSc1	Challenge
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Bourne	Bourn	k1gInSc5	Bourn
Supremacy	Supremaca	k1gMnSc2	Supremaca
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Casino	Casina	k1gMnSc5	Casina
Royale	Royal	k1gMnSc5	Royal
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Breaking	Breaking	k1gInSc1	Breaking
and	and	k?	and
Entering	Entering	k1gInSc1	Entering
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Live	Live	k1gFnSc1	Live
Free	Fre	k1gFnSc2	Fre
or	or	k?	or
Die	Die	k1gMnSc1	Die
Hard	Hard	k1gMnSc1	Hard
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Hard	Hard	k1gMnSc1	Hard
4.0	[number]	k4	4.0
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Bourne	Bourn	k1gInSc5	Bourn
Ultimatum	ultimatum	k1gNnSc4	ultimatum
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
District	District	k1gMnSc1	District
13	[number]	k4	13
Ultimatum	ultimatum	k1gNnSc1	ultimatum
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
videoklipech	videoklip	k1gInPc6	videoklip
<g/>
:	:	kIx,	:
Jump	Jump	k1gMnSc1	Jump
/	/	kIx~	/
Confessions	Confessions	k1gInSc1	Confessions
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Dance	Danka	k1gFnSc3	Danka
Floor	Floora	k1gFnPc2	Floora
<g/>
,	,	kIx,	,
Madonna	Madonna	k1gFnSc1	Madonna
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
videohrách	videohra	k1gFnPc6	videohra
<g/>
:	:	kIx,	:
Mirror	Mirrora	k1gFnPc2	Mirrora
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Edge	Edge	k1gFnSc7	Edge
od	od	k7c2	od
Electronic	Electronice	k1gFnPc2	Electronice
Arts	Artsa	k1gFnPc2	Artsa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Prototype	prototyp	k1gInSc5	prototyp
od	od	k7c2	od
Ubisoft	Ubisofta	k1gFnPc2	Ubisofta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Assassin	Assassin	k1gInSc1	Assassin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Creed	Creed	k1gInSc1	Creed
od	od	k7c2	od
Ubisoft	Ubisofta	k1gFnPc2	Ubisofta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Assassin	Assassina	k1gFnPc2	Assassina
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
Creed	Creed	k1gInSc4	Creed
2	[number]	k4	2
od	od	k7c2	od
Ubisoft	Ubisofta	k1gFnPc2	Ubisofta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Assassin	Assassin	k1gInSc1	Assassin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Creed	Creed	k1gInSc1	Creed
Brotherhood	Brotherhooda	k1gFnPc2	Brotherhooda
od	od	k7c2	od
Ubisoft	Ubisofta	k1gFnPc2	Ubisofta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Assassin	Assassin	k1gInSc1	Assassin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Creed	Creed	k1gInSc1	Creed
Revelations	Revelationsa	k1gFnPc2	Revelationsa
od	od	k7c2	od
Ubisoft	Ubisofta	k1gFnPc2	Ubisofta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Assassin	Assassin	k1gInSc1	Assassin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Creed	Creed	k1gInSc1	Creed
3	[number]	k4	3
od	od	k7c2	od
Ubisoft	Ubisofta	k1gFnPc2	Ubisofta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
V	v	k7c6	v
reklamách	reklama	k1gFnPc6	reklama
<g/>
:	:	kIx,	:
na	na	k7c6	na
K-swiss	Kwissa	k1gFnPc2	K-swissa
Známými	známá	k1gFnPc7	známá
parkourovými	parkourův	k2eAgFnPc7d1	parkourův
(	(	kIx(	(
<g/>
free	free	k6eAd1	free
runningovými	runningový	k2eAgMnPc7d1	runningový
<g/>
)	)	kIx)	)
dokumenty	dokument	k1gInPc1	dokument
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Jump	Jump	k1gInSc1	Jump
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Jump	Jump	k1gMnSc1	Jump
Britain	Britain	k1gMnSc1	Britain
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Generation	Generation	k1gInSc1	Generation
Yamakasi	Yamakas	k1gMnPc1	Yamakas
Parkour	Parkour	k1gMnSc1	Parkour
Project	Project	k1gMnSc1	Project
:	:	kIx,	:
Pilgrimage	Pilgrimage	k1gFnSc1	Pilgrimage
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Parkour	Parkoura	k1gFnPc2	Parkoura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Parkour	Parkoura	k1gFnPc2	Parkoura
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
–	–	k?	–
světový	světový	k2eAgInSc1d1	světový
parkourový	parkourový	k2eAgInSc1d1	parkourový
portál	portál	k1gInSc1	portál
Parkour	Parkoura	k1gFnPc2	Parkoura
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
český	český	k2eAgInSc1d1	český
parkourový	parkourový	k2eAgInSc1d1	parkourový
portál	portál	k1gInSc1	portál
Sportmediaconcept	Sportmediaconcepta	k1gFnPc2	Sportmediaconcepta
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
/	/	kIx~	/
<g/>
parkour	parkour	k1gMnSc1	parkour
<g/>
/	/	kIx~	/
–	–	k?	–
osobní	osobní	k2eAgFnSc2d1	osobní
stránky	stránka	k1gFnSc2	stránka
D.	D.	kA	D.
Bella	Bella	k1gMnSc1	Bella
<g />
.	.	kIx.	.
</s>
<s>
Parkourpedia	Parkourpedium	k1gNnSc2	Parkourpedium
of	of	k?	of
Australian	Australian	k1gMnSc1	Australian
Parkour	Parkour	k1gMnSc1	Parkour
Association	Association	k1gInSc4	Association
Salto	salto	k1gNnSc1	salto
Mortale	Mortal	k1gMnSc5	Mortal
–	–	k?	–
web	web	k1gInSc4	web
o	o	k7c4	o
parkouru	parkoura	k1gFnSc4	parkoura
<g/>
,	,	kIx,	,
free	free	k1gFnSc4	free
runningu	running	k1gInSc2	running
a	a	k8xC	a
lezení	lezení	k1gNnSc2	lezení
AdrenalineZone	AdrenalineZon	k1gInSc5	AdrenalineZon
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
-	-	kIx~	-
vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
Parkour	Parkour	k1gMnSc1	Parkour
<g/>
/	/	kIx~	/
<g/>
Freerunning	Freerunning	k1gInSc1	Freerunning
AdrenalineZone	AdrenalineZon	k1gInSc5	AdrenalineZon
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
-	-	kIx~	-
jak	jak	k6eAd1	jak
trénovat	trénovat	k5eAaImF	trénovat
Parkour	Parkour	k1gMnSc1	Parkour
<g/>
/	/	kIx~	/
<g/>
Freerunning	Freerunning	k1gInSc1	Freerunning
Parkourista	Parkourista	k1gMnSc1	Parkourista
vidí	vidět	k5eAaImIp3nS	vidět
cesty	cesta	k1gFnPc4	cesta
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nejsou	být	k5eNaImIp3nP	být
na	na	k7c4	na
Český	český	k2eAgInSc4d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
