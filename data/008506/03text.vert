<p>
<s>
Prolog	prolog	k1gInSc1	prolog
je	být	k5eAaImIp3nS	být
logický	logický	k2eAgInSc1d1	logický
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
deklarativní	deklarativní	k2eAgInPc4d1	deklarativní
programovací	programovací	k2eAgInPc4d1	programovací
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
programátor	programátor	k1gInSc1	programátor
popisuje	popisovat	k5eAaImIp3nS	popisovat
pouze	pouze	k6eAd1	pouze
cíl	cíl	k1gInSc4	cíl
výpočtu	výpočet	k1gInSc2	výpočet
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přesný	přesný	k2eAgInSc4d1	přesný
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
se	se	k3xPyFc4	se
k	k	k7c3	k
výsledku	výsledek	k1gInSc3	výsledek
program	program	k1gInSc1	program
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ponechán	ponechat	k5eAaPmNgInS	ponechat
na	na	k7c4	na
libovůli	libovůle	k1gFnSc4	libovůle
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Prolog	prolog	k1gInSc1	prolog
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
vyjádření	vyjádření	k1gNnSc4	vyjádření
faktů	fakt	k1gInPc2	fakt
a	a	k8xC	a
logických	logický	k2eAgInPc2d1	logický
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
s	s	k7c7	s
potlačením	potlačení	k1gNnSc7	potlačení
imperativní	imperativní	k2eAgFnSc2d1	imperativní
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Prolog	prolog	k1gInSc1	prolog
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
především	především	k9	především
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
a	a	k8xC	a
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
lingvistice	lingvistika	k1gFnSc6	lingvistika
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
zpracování	zpracování	k1gNnSc1	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
navržen	navrhnout	k5eAaPmNgInS	navrhnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntaxe	syntaxe	k1gFnSc1	syntaxe
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
použitelná	použitelný	k2eAgFnSc1d1	použitelná
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
počítačově	počítačově	k6eAd1	počítačově
nepříliš	příliš	k6eNd1	příliš
gramotné	gramotný	k2eAgMnPc4d1	gramotný
lingvisty	lingvista	k1gMnPc4	lingvista
<g/>
.	.	kIx.	.
</s>
<s>
Prolog	prolog	k1gInSc1	prolog
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
predikátové	predikátový	k2eAgFnSc6d1	predikátová
logice	logika	k1gFnSc6	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
;	;	kIx,	;
konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
Hornovy	Hornův	k2eAgFnPc4d1	Hornova
klauzule	klauzule	k1gFnPc4	klauzule
<g/>
.	.	kIx.	.
</s>
<s>
Běh	běh	k1gInSc1	běh
programu	program	k1gInSc2	program
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
představován	představovat	k5eAaImNgInS	představovat
aplikací	aplikace	k1gFnSc7	aplikace
dokazovacích	dokazovací	k2eAgFnPc2d1	dokazovací
technik	technika	k1gFnPc2	technika
na	na	k7c4	na
zadané	zadaný	k2eAgFnPc4d1	zadaná
klauzule	klauzule	k1gFnPc4	klauzule
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
využívanými	využívaný	k2eAgInPc7d1	využívaný
přístupy	přístup	k1gInPc7	přístup
jsou	být	k5eAaImIp3nP	být
unifikace	unifikace	k1gFnSc2	unifikace
<g/>
,	,	kIx,	,
rekurze	rekurze	k1gFnSc2	rekurze
a	a	k8xC	a
backtracking	backtracking	k1gInSc1	backtracking
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Prolog	prolog	k1gInSc1	prolog
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
Philippem	Philipp	k1gInSc7	Philipp
Rousselem	Roussel	k1gInSc7	Roussel
jako	jako	k8xS	jako
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
"	"	kIx"	"
<g/>
PROgrammation	PROgrammation	k1gInSc1	PROgrammation
en	en	k?	en
LOGique	LOGique	k1gInSc1	LOGique
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
'	'	kIx"	'
<g/>
programování	programování	k1gNnSc1	programování
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
Alainem	Alain	k1gMnSc7	Alain
Colmerauerem	Colmerauer	k1gMnSc7	Colmerauer
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Philippem	Philipp	k1gMnSc7	Philipp
Rousselem	Roussel	k1gMnSc7	Roussel
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
procedurálního	procedurální	k2eAgInSc2d1	procedurální
výkladu	výklad	k1gInSc2	výklad
Hornovy	Hornův	k2eAgFnSc2d1	Hornova
klauzule	klauzule	k1gFnSc2	klauzule
od	od	k7c2	od
Roberta	Robert	k1gMnSc2	Robert
Kowalskiho	Kowalski	k1gMnSc2	Kowalski
<g/>
.	.	kIx.	.
</s>
<s>
Prolog	prolog	k1gInSc4	prolog
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
automatických	automatický	k2eAgInPc2d1	automatický
odvozujících	odvozující	k2eAgInPc2d1	odvozující
systémů	systém	k1gInPc2	systém
vyvíjených	vyvíjený	k2eAgInPc2d1	vyvíjený
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
až	až	k9	až
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Prologu	prolog	k1gInSc2	prolog
jsou	být	k5eAaImIp3nP	být
Q-systémy	Qystém	k1gInPc1	Q-systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc4	příklad
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
Projekt	projekt	k1gInSc1	projekt
počítačů	počítač	k1gMnPc2	počítač
páté	pátá	k1gFnSc2	pátá
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vybral	vybrat	k5eAaPmAgInS	vybrat
za	za	k7c4	za
vývojový	vývojový	k2eAgInSc4d1	vývojový
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těch	ten	k3xDgFnPc2	ten
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
Prolog	prolog	k1gInSc1	prolog
rozvětvil	rozvětvit	k5eAaPmAgInS	rozvětvit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
jako	jako	k9	jako
nové	nový	k2eAgInPc4d1	nový
logické	logický	k2eAgInPc4d1	logický
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
druhy	druh	k1gInPc7	druh
Prologu	prolog	k1gInSc2	prolog
jsou	být	k5eAaImIp3nP	být
určité	určitý	k2eAgInPc1d1	určitý
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
v	v	k7c6	v
sémantice	sémantika	k1gFnSc6	sémantika
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
v	v	k7c6	v
syntaxi	syntax	k1gFnSc6	syntax
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nebývá	bývat	k5eNaImIp3nS	bývat
problém	problém	k1gInSc4	problém
přeorientovat	přeorientovat	k5eAaPmF	přeorientovat
se	se	k3xPyFc4	se
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotnou	jednotný	k2eAgFnSc7d1	jednotná
datovou	datový	k2eAgFnSc7d1	datová
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
Prolog	prolog	k1gInSc1	prolog
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
term	term	k1gInSc1	term
-	-	kIx~	-
pojem	pojem	k1gInSc1	pojem
převzatý	převzatý	k2eAgInSc1d1	převzatý
z	z	k7c2	z
formální	formální	k2eAgFnSc2d1	formální
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
členění	členění	k1gNnSc1	členění
termů	term	k1gInPc2	term
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
term	term	k1gInSc1	term
</s>
</p>
<p>
<s>
struktura	struktura	k1gFnSc1	struktura
</s>
</p>
<p>
<s>
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
term	term	k1gInSc1	term
</s>
</p>
<p>
<s>
proměnná	proměnná	k1gFnSc1	proměnná
</s>
</p>
<p>
<s>
konstanta	konstanta	k1gFnSc1	konstanta
</s>
</p>
<p>
<s>
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
atom	atom	k1gInSc1	atom
</s>
</p>
<p>
<s>
===	===	k?	===
Atomy	atom	k1gInPc1	atom
===	===	k?	===
</s>
</p>
<p>
<s>
Atomy	atom	k1gInPc1	atom
lze	lze	k6eAd1	lze
dle	dle	k7c2	dle
konstrukce	konstrukce	k1gFnSc2	konstrukce
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
třech	tři	k4xCgFnPc2	tři
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
řetězce	řetězec	k1gInPc1	řetězec
znaků	znak	k1gInPc2	znak
začínající	začínající	k2eAgFnSc2d1	začínající
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
obsahující	obsahující	k2eAgFnSc2d1	obsahující
pouze	pouze	k6eAd1	pouze
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
číslice	číslice	k1gFnPc4	číslice
a	a	k8xC	a
podtržítko	podtržítko	k1gNnSc4	podtržítko
-	-	kIx~	-
např	např	kA	např
<g/>
:	:	kIx,	:
otec	otec	k1gMnSc1	otec
franta	franta	k1gMnSc1	franta
novy_Clen	novy_Clen	k1gInSc4	novy_Clen
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
posloupnost	posloupnost	k1gFnSc1	posloupnost
znaků	znak	k1gInPc2	znak
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
apostrofech	apostrof	k1gInPc6	apostrof
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnSc2	některý
implementace	implementace	k1gFnSc2	implementace
používají	používat	k5eAaImIp3nP	používat
uvozovky	uvozovka	k1gFnPc1	uvozovka
<g/>
)	)	kIx)	)
-	-	kIx~	-
např	např	kA	např
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Pepa	Pepa	k1gFnSc1	Pepa
26.6	[number]	k4	26.6
<g/>
.2007	.2007	k4	.2007
<g/>
'	'	kIx"	'
'	'	kIx"	'
<g/>
velký	velký	k2eAgInSc1d1	velký
les	les	k1gInSc1	les
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
atomy	atom	k1gInPc4	atom
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
speciálních	speciální	k2eAgInPc2d1	speciální
znaků	znak	k1gInPc2	znak
-	-	kIx~	-
např	např	kA	např
<g/>
:	:	kIx,	:
,	,	kIx,	,
;	;	kIx,	;
<	<	kIx(	<
<g/>
*	*	kIx~	*
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
*	*	kIx~	*
<g/>
>	>	kIx)	>
:	:	kIx,	:
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Čísla	číslo	k1gNnPc1	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
Prolog	prolog	k1gInSc1	prolog
podporoval	podporovat	k5eAaImAgInS	podporovat
pouze	pouze	k6eAd1	pouze
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
implementací	implementace	k1gFnPc2	implementace
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
reálnými	reálný	k2eAgNnPc7d1	reálné
i	i	k8xC	i
racionálními	racionální	k2eAgNnPc7d1	racionální
čísly	číslo	k1gNnPc7	číslo
a	a	k8xC	a
s	s	k7c7	s
neohraničenými	ohraničený	k2eNgNnPc7d1	neohraničené
celými	celý	k2eAgNnPc7d1	celé
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
X	X	kA	X
is	is	k?	is
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
200	[number]	k4	200
<g/>
,	,	kIx,	,
Y	Y	kA	Y
is	is	k?	is
(	(	kIx(	(
<g/>
3	[number]	k4	3
rdiv	rdiv	k1gInSc1	rdiv
8	[number]	k4	8
<g/>
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
<g/>
4	[number]	k4	4
rdiv	rdiv	k1gInSc1	rdiv
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
X	X	kA	X
=	=	kIx~	=
1606938044258990275541962092341162602522202993782792835301376	[number]	k4	1606938044258990275541962092341162602522202993782792835301376
</s>
</p>
<p>
<s>
Y	Y	kA	Y
=	=	kIx~	=
59	[number]	k4	59
rdiv	rdiv	k1gInSc1	rdiv
72	[number]	k4	72
</s>
</p>
<p>
<s>
===	===	k?	===
Proměnné	proměnný	k2eAgMnPc4d1	proměnný
===	===	k?	===
</s>
</p>
<p>
<s>
Proměnné	proměnná	k1gFnPc1	proměnná
začínají	začínat	k5eAaImIp3nP	začínat
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
nebo	nebo	k8xC	nebo
podtržítkem	podtržítek	k1gInSc7	podtržítek
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
obsahovat	obsahovat	k5eAaImF	obsahovat
speciální	speciální	k2eAgInPc4d1	speciální
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popisují	popisovat	k5eAaImIp3nP	popisovat
účastníky	účastník	k1gMnPc7	účastník
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dotazech	dotaz	k1gInPc6	dotaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
hledané	hledaný	k2eAgInPc4d1	hledaný
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
platnosti	platnost	k1gFnSc2	platnost
proměnné	proměnná	k1gFnSc2	proměnná
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
proměnná	proměnná	k1gFnSc1	proměnná
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
klauzuli	klauzule	k1gFnSc6	klauzule
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
součásti	součást	k1gFnPc1	součást
stejného	stejný	k2eAgInSc2d1	stejný
predikátu	predikát	k1gInSc2	predikát
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotu	hodnota	k1gFnSc4	hodnota
získává	získávat	k5eAaImIp3nS	získávat
pomocí	pomocí	k7c2	pomocí
srovnávání	srovnávání	k1gNnSc2	srovnávání
(	(	kIx(	(
<g/>
unifikace	unifikace	k1gFnSc2	unifikace
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
přiřazení	přiřazení	k1gNnSc6	přiřazení
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dále	daleko	k6eAd2	daleko
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
použité	použitý	k2eAgNnSc1d1	Použité
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ji	on	k3xPp3gFnSc4	on
přiřadilo	přiřadit	k5eAaPmAgNnS	přiřadit
<g/>
,	,	kIx,	,
neodvolá	odvolat	k5eNaPmIp3nS	odvolat
(	(	kIx(	(
<g/>
backtracking	backtracking	k1gInSc1	backtracking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
interpretu	interpret	k1gMnSc3	interpret
lze	lze	k6eAd1	lze
proměnné	proměnná	k1gFnPc4	proměnná
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
volné	volný	k2eAgNnSc1d1	volné
-	-	kIx~	-
jejich	jejich	k3xOp3gFnSc1	jejich
hodnota	hodnota	k1gFnSc1	hodnota
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
známá	známá	k1gFnSc1	známá
a	a	k8xC	a
interpret	interpret	k1gMnSc1	interpret
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
nalézt	nalézt	k5eAaBmF	nalézt
</s>
</p>
<p>
<s>
vázané	vázaný	k2eAgFnPc1d1	vázaná
-	-	kIx~	-
z	z	k7c2	z
dřívějších	dřívější	k2eAgInPc2d1	dřívější
kroků	krok	k1gInPc2	krok
řešení	řešení	k1gNnSc2	řešení
již	již	k6eAd1	již
plyne	plynout	k5eAaImIp3nS	plynout
její	její	k3xOp3gFnSc1	její
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
svázánaPříklad	svázánaPříklad	k1gInSc4	svázánaPříklad
použití	použití	k1gNnSc3	použití
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
:	:	kIx,	:
<g/>
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
jana	jana	k1gMnSc1	jana
<g/>
,	,	kIx,	,
<g/>
petr	petr	k1gMnSc1	petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
otto	otto	k1gMnSc1	otto
<g/>
,	,	kIx,	,
<g/>
eva	eva	k?	eva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dite	dite	k1gFnSc1	dite
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dotazy	dotaz	k1gInPc1	dotaz
<g/>
:	:	kIx,	:
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
petr	petr	k1gMnSc1	petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
X	X	kA	X
=	=	kIx~	=
jana	jana	k1gMnSc1	jana
;	;	kIx,	;
</s>
</p>
<p>
<s>
No	no	k9	no
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
dite	dit	k1gMnSc4	dit
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
jana	jana	k1gFnSc1	jana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
X	X	kA	X
=	=	kIx~	=
petr	petr	k1gMnSc1	petr
;	;	kIx,	;
</s>
</p>
<p>
<s>
No	no	k9	no
</s>
</p>
<p>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
anonymní	anonymní	k2eAgFnSc1d1	anonymní
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
jako	jako	k9	jako
podtržítko	podtržítko	k1gNnSc1	podtržítko
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hodnota	hodnota	k1gFnSc1	hodnota
není	být	k5eNaImIp3nS	být
podstatná	podstatný	k2eAgFnSc1d1	podstatná
a	a	k8xC	a
Prolog	prolog	k1gInSc1	prolog
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
výsledcích	výsledek	k1gInPc6	výsledek
nezobrazuje	zobrazovat	k5eNaImIp3nS	zobrazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
predikát	predikát	k1gInSc1	predikát
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
X	X	kA	X
je	být	k5eAaImIp3nS	být
dítě	dítě	k1gNnSc1	dítě
</s>
</p>
<p>
<s>
je_dite	je_dit	k1gMnSc5	je_dit
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
dite	dite	k1gInSc1	dite
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
_	_	kIx~	_
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktury	struktura	k1gFnSc2	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Struktury	struktura	k1gFnPc1	struktura
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
z	z	k7c2	z
funktoru	funktor	k1gInSc2	funktor
a	a	k8xC	a
argumentů	argument	k1gInPc2	argument
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
argumentů	argument	k1gInPc2	argument
udává	udávat	k5eAaImIp3nS	udávat
aritu	arit	k1gInSc3	arit
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
operátory	operátor	k1gInPc1	operátor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používány	používat	k5eAaImNgFnP	používat
také	také	k9	také
v	v	k7c6	v
infixovém	infixový	k2eAgInSc6d1	infixový
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Strukturou	struktura	k1gFnSc7	struktura
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
funktor	funktor	k1gInSc1	funktor
používá	používat	k5eAaImIp3nS	používat
infixový	infixový	k2eAgInSc4d1	infixový
operátor	operátor	k1gInSc4	operátor
:	:	kIx,	:
<g/>
-	-	kIx~	-
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
muz	muz	k?	muz
<g/>
(	(	kIx(	(
<g/>
adam	adam	k1gMnSc1	adam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
funktor	funktor	k1gInSc1	funktor
muz	muz	k?	muz
má	mít	k5eAaImIp3nS	mít
aritu	arit	k1gInSc2	arit
1	[number]	k4	1
</s>
</p>
<p>
<s>
datum	datum	k1gNnSc1	datum
<g/>
(	(	kIx(	(
<g/>
27,6	[number]	k4	27,6
<g/>
,2007	,2007	k4	,2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
funktor	funktor	k1gInSc1	funktor
datum	datum	k1gNnSc1	datum
má	mít	k5eAaImIp3nS	mít
aritu	arit	k1gInSc2	arit
3	[number]	k4	3
</s>
</p>
<p>
<s>
okamzik	okamzik	k1gInSc1	okamzik
<g/>
(	(	kIx(	(
<g/>
datum	datum	k1gNnSc1	datum
<g/>
(	(	kIx(	(
<g/>
27,6	[number]	k4	27,6
<g/>
,2007	,2007	k4	,2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
cas	cas	k?	cas
<g/>
(	(	kIx(	(
<g/>
13,54	[number]	k4	13,54
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
funktor	funktor	k1gInSc1	funktor
okamzik	okamzika	k1gFnPc2	okamzika
má	mít	k5eAaImIp3nS	mít
aritu	arit	k1gInSc2	arit
2	[number]	k4	2
</s>
</p>
<p>
<s>
prarodic	prarodic	k1gMnSc1	prarodic
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Z	Z	kA	Z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
Z	Z	kA	Z
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
programu	program	k1gInSc6	program
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
dva	dva	k4xCgInPc4	dva
stejně	stejně	k6eAd1	stejně
pojmenované	pojmenovaný	k2eAgInPc4d1	pojmenovaný
funktory	funktor	k1gInPc4	funktor
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc4d1	různý
arity	arit	k1gInPc4	arit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
struktur	struktura	k1gFnPc2	struktura
jsou	být	k5eAaImIp3nP	být
seznamy	seznam	k1gInPc4	seznam
a	a	k8xC	a
řetězce	řetězec	k1gInPc4	řetězec
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Seznamy	seznam	k1gInPc4	seznam
====	====	k?	====
</s>
</p>
<p>
<s>
Seznamy	seznam	k1gInPc1	seznam
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgInP	definovat
induktivně	induktivně	k6eAd1	induktivně
<g/>
:	:	kIx,	:
Prázdný	prázdný	k2eAgInSc1d1	prázdný
seznam	seznam	k1gInSc1	seznam
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
atomem	atom	k1gInSc7	atom
[	[	kIx(	[
]	]	kIx)	]
,	,	kIx,	,
k	k	k7c3	k
reprezentaci	reprezentace	k1gFnSc3	reprezentace
neprázdného	prázdný	k2eNgInSc2d1	neprázdný
seznamu	seznam	k1gInSc2	seznam
slouží	sloužit	k5eAaImIp3nS	sloužit
binární	binární	k2eAgInSc1d1	binární
funktor	funktor	k1gInSc1	funktor
tečka	tečka	k1gFnSc1	tečka
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
.	.	kIx.	.
</s>
<s>
Neprázdný	prázdný	k2eNgInSc1d1	neprázdný
seznam	seznam	k1gInSc1	seznam
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
tzv.	tzv.	kA	tzv.
tečkový	tečkový	k2eAgInSc1d1	tečkový
pár	pár	k1gInSc1	pár
(	(	kIx(	(
<g/>
terminologie	terminologie	k1gFnSc1	terminologie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
LISP	LISP	kA	LISP
<g/>
)	)	kIx)	)
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
<g/>
Tělo	tělo	k1gNnSc1	tělo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
prvek	prvek	k1gInSc4	prvek
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
tvořený	tvořený	k2eAgInSc1d1	tvořený
zbývajícími	zbývající	k2eAgInPc7d1	zbývající
prvky	prvek	k1gInPc7	prvek
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
zápisu	zápis	k1gInSc2	zápis
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
výčet	výčet	k1gInSc4	výčet
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
hranatých	hranatý	k2eAgFnPc6d1	hranatá
závorkách	závorka	k1gFnPc6	závorka
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
zápisy	zápis	k1gInPc1	zápis
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgInPc1d1	ekvivalentní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
]	]	kIx)	]
<g/>
)))	)))	k?	)))
.	.	kIx.	.
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
]	]	kIx)	]
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
seznamy	seznam	k1gInPc7	seznam
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívá	využívat	k5eAaImIp3nS	využívat
operátor	operátor	k1gInSc4	operátor
'	'	kIx"	'
<g/>
|	|	kIx~	|
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
částem	část	k1gFnPc3	část
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
Začátek	začátek	k1gInSc1	začátek
<g/>
|	|	kIx~	|
<g/>
Tělo	tělo	k1gNnSc1	tělo
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Začátek	začátek	k1gInSc1	začátek
je	být	k5eAaImIp3nS	být
výčet	výčet	k1gInSc4	výčet
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
seznam	seznam	k1gInSc4	seznam
<g/>
)	)	kIx)	)
prvků	prvek	k1gInPc2	prvek
tvořící	tvořící	k2eAgInSc4d1	tvořící
začátek	začátek	k1gInSc4	začátek
definovaného	definovaný	k2eAgInSc2d1	definovaný
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc4	seznam
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
výčet	výčet	k1gInSc4	výčet
<g/>
)	)	kIx)	)
tvořící	tvořící	k2eAgInSc4d1	tvořící
zbytek	zbytek	k1gInSc4	zbytek
definovaného	definovaný	k2eAgInSc2d1	definovaný
seznamu	seznam	k1gInSc2	seznam
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
se	se	k3xPyFc4	se
uvádět	uvádět	k5eAaImF	uvádět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Častým	častý	k2eAgInSc7d1	častý
zdrojem	zdroj	k1gInSc7	zdroj
chyb	chyba	k1gFnPc2	chyba
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
záměna	záměna	k1gFnSc1	záměna
operátoru	operátor	k1gInSc2	operátor
'	'	kIx"	'
<g/>
|	|	kIx~	|
<g/>
'	'	kIx"	'
a	a	k8xC	a
čárky	čárka	k1gFnSc2	čárka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Řetězce	řetězec	k1gInPc4	řetězec
====	====	k?	====
</s>
</p>
<p>
<s>
Řetězce	řetězec	k1gInPc1	řetězec
jsou	být	k5eAaImIp3nP	být
sekvence	sekvence	k1gFnPc1	sekvence
znaků	znak	k1gInPc2	znak
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
v	v	k7c6	v
uvozovkách	uvozovka	k1gFnPc6	uvozovka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgMnSc1d1	ekvivalentní
seznamu	seznam	k1gInSc2	seznam
(	(	kIx(	(
<g/>
číselných	číselný	k2eAgInPc2d1	číselný
<g/>
)	)	kIx)	)
kódů	kód	k1gInPc2	kód
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
znakové	znakový	k2eAgFnSc6d1	znaková
sadě	sada	k1gFnSc6	sada
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
systém	systém	k1gInSc1	systém
podporuje	podporovat	k5eAaImIp3nS	podporovat
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
Xs	Xs	k1gFnSc6	Xs
=	=	kIx~	=
"	"	kIx"	"
<g/>
Π	Π	k?	Π
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Xs	Xs	k?	Xs
=	=	kIx~	=
[	[	kIx(	[
<g/>
928	[number]	k4	928
<g/>
,	,	kIx,	,
961	[number]	k4	961
<g/>
,	,	kIx,	,
972	[number]	k4	972
<g/>
,	,	kIx,	,
955	[number]	k4	955
<g/>
,	,	kIx,	,
959	[number]	k4	959
<g/>
,	,	kIx,	,
947	[number]	k4	947
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Programování	programování	k1gNnSc1	programování
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
==	==	k?	==
</s>
</p>
<p>
<s>
Programování	programování	k1gNnSc1	programování
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
programování	programování	k1gNnSc2	programování
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
procedurálních	procedurální	k2eAgInPc6d1	procedurální
jazycích	jazyk	k1gInPc6	jazyk
jako	jako	k8xS	jako
například	například	k6eAd1	například
C.	C.	kA	C.
Program	program	k1gInSc1	program
popisuje	popisovat	k5eAaImIp3nS	popisovat
vztahy	vztah	k1gInPc4	vztah
definované	definovaný	k2eAgInPc4d1	definovaný
pomocí	pomocí	k7c2	pomocí
klauzulí	klauzule	k1gFnPc2	klauzule
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
Prolog	prolog	k1gInSc1	prolog
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
Hornovy	Hornův	k2eAgFnPc4d1	Hornova
klauzule	klauzule	k1gFnPc4	klauzule
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
predikátovou	predikátový	k2eAgFnSc4d1	predikátová
logiku	logika	k1gFnSc4	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
Prologu	prolog	k1gInSc2	prolog
je	být	k5eAaImIp3nS	být
databáze	databáze	k1gFnSc1	databáze
klauzulí	klauzule	k1gFnPc2	klauzule
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
fakta	faktum	k1gNnPc4	faktum
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterými	který	k3yRgFnPc7	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
klást	klást	k5eAaImF	klást
dotazy	dotaz	k1gInPc4	dotaz
formou	forma	k1gFnSc7	forma
tvrzení	tvrzení	k1gNnPc1	tvrzení
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
Prolog	prolog	k1gInSc1	prolog
zhodnocuje	zhodnocovat	k5eAaImIp3nS	zhodnocovat
jejich	jejich	k3xOp3gFnSc4	jejich
pravdivost	pravdivost	k1gFnSc4	pravdivost
(	(	kIx(	(
<g/>
dokazatelnost	dokazatelnost	k1gFnSc4	dokazatelnost
z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednoduššími	jednoduchý	k2eAgFnPc7d3	nejjednodušší
klauzulemi	klauzule	k1gFnPc7	klauzule
jsou	být	k5eAaImIp3nP	být
fakta	faktum	k1gNnPc1	faktum
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pouze	pouze	k6eAd1	pouze
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
o	o	k7c6	o
vlastnostech	vlastnost	k1gFnPc6	vlastnost
objektu	objekt	k1gInSc2	objekt
nebo	nebo	k8xC	nebo
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c4	mezi
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Složitějšími	složitý	k2eAgFnPc7d2	složitější
klauzulemi	klauzule	k1gFnPc7	klauzule
jsou	být	k5eAaImIp3nP	být
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pomocí	pomocí	k7c2	pomocí
implikace	implikace	k1gFnSc2	implikace
odvozovat	odvozovat	k5eAaImF	odvozovat
nová	nový	k2eAgNnPc4d1	nové
fakta	faktum	k1gNnPc4	faktum
<g/>
.	.	kIx.	.
</s>
<s>
Zapisují	zapisovat	k5eAaImIp3nP	zapisovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
hlavička	hlavička	k1gFnSc1	hlavička
:	:	kIx,	:
<g/>
-	-	kIx~	-
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavička	hlavička	k1gFnSc1	hlavička
definuje	definovat	k5eAaBmIp3nS	definovat
odvozovaný	odvozovaný	k2eAgInSc4d1	odvozovaný
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc4	tělo
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
,	,	kIx,	,
za	za	k7c2	za
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeden	jeden	k4xCgInSc4	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
interpretu	interpret	k1gMnSc3	interpret
podaří	podařit	k5eAaPmIp3nS	podařit
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
<g/>
,	,	kIx,	,
ověřil	ověřit	k5eAaPmAgMnS	ověřit
tím	ten	k3xDgNnSc7	ten
pravdivost	pravdivost	k1gFnSc4	pravdivost
hlavičky	hlavička	k1gFnSc2	hlavička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
lze	lze	k6eAd1	lze
do	do	k7c2	do
databáze	databáze	k1gFnSc2	databáze
uložit	uložit	k5eAaPmF	uložit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Monika	Monika	k1gFnSc1	Monika
je	být	k5eAaImIp3nS	být
dívka	dívka	k1gFnSc1	dívka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dívka	dívka	k1gFnSc1	dívka
<g/>
(	(	kIx(	(
<g/>
monika	monika	k1gFnSc1	monika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
lze	lze	k6eAd1	lze
dokazatelnost	dokazatelnost	k1gFnSc4	dokazatelnost
tohoto	tento	k3xDgInSc2	tento
faktu	fakt	k1gInSc2	fakt
prověřit	prověřit	k5eAaPmF	prověřit
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
Prolog	prolog	k1gInSc1	prolog
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
yes	yes	k?	yes
(	(	kIx(	(
<g/>
ano	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
dívka	dívka	k1gFnSc1	dívka
<g/>
(	(	kIx(	(
<g/>
monika	monika	k1gFnSc1	monika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
yes	yes	k?	yes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
zeptat	zeptat	k5eAaPmF	zeptat
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgNnPc6	který
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
dívky	dívka	k1gFnPc1	dívka
(	(	kIx(	(
<g/>
středníkem	středník	k1gInSc7	středník
požadujeme	požadovat	k5eAaImIp1nP	požadovat
další	další	k2eAgInPc1d1	další
výsledky	výsledek	k1gInPc1	výsledek
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
dívka	dívka	k1gFnSc1	dívka
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
X	X	kA	X
=	=	kIx~	=
monika	monik	k1gMnSc2	monik
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
no	no	k9	no
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
(	(	kIx(	(
<g/>
závislosti	závislost	k1gFnSc2	závislost
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
pomocí	pomocí	k7c2	pomocí
implikací	implikace	k1gFnPc2	implikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
syn	syn	k1gMnSc1	syn
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
rodič	rodič	k1gMnSc1	rodič
<g/>
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tedy	tedy	k9	tedy
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
B	B	kA	B
je	být	k5eAaImIp3nS	být
rodičem	rodič	k1gMnSc7	rodič
A	a	k8xC	a
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
A	a	k9	a
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
A	a	k9	a
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
B.	B.	kA	B.
</s>
</p>
<p>
<s>
===	===	k?	===
Predikát	predikát	k1gInSc4	predikát
===	===	k?	===
</s>
</p>
<p>
<s>
Predikát	predikát	k1gInSc4	predikát
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k8xS	jako
sadu	sada	k1gFnSc4	sada
klauzulí	klauzule	k1gFnPc2	klauzule
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
stejnou	stejný	k2eAgFnSc7d1	stejná
aritou	arita	k1gFnSc7	arita
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
fakta	faktum	k1gNnPc4	faktum
i	i	k8xC	i
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k8xS	jako
alternativy	alternativa	k1gFnPc1	alternativa
–	–	k?	–
platnost	platnost	k1gFnSc1	platnost
predikátu	predikát	k1gInSc2	predikát
lze	lze	k6eAd1	lze
dokázat	dokázat	k5eAaPmF	dokázat
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Pravdivost	pravdivost	k1gFnSc1	pravdivost
predikátu	predikát	k1gInSc2	predikát
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
dvě	dva	k4xCgFnPc1	dva
logické	logický	k2eAgFnPc1d1	logická
konstanty	konstanta	k1gFnPc1	konstanta
–	–	k?	–
true	true	k1gFnSc1	true
<g/>
,	,	kIx,	,
fail	fail	k1gInSc1	fail
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhodnocování	vyhodnocování	k1gNnSc6	vyhodnocování
pravidel	pravidlo	k1gNnPc2	pravidlo
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
základní	základní	k2eAgInPc4d1	základní
logické	logický	k2eAgInPc4d1	logický
operátory	operátor	k1gInPc4	operátor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
konjunkce	konjunkce	k1gFnSc1	konjunkce
(	(	kIx(	(
<g/>
AND	Anda	k1gFnPc2	Anda
<g/>
)	)	kIx)	)
–	–	k?	–
čárka	čárka	k1gFnSc1	čárka
'	'	kIx"	'
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
–	–	k?	–
pokud	pokud	k8xS	pokud
některá	některý	k3yIgFnSc1	některý
část	část	k1gFnSc1	část
selže	selhat	k5eAaPmIp3nS	selhat
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
se	se	k3xPyFc4	se
nevyhodnocují	vyhodnocovat	k5eNaImIp3nP	vyhodnocovat
</s>
</p>
<p>
<s>
disjunkce	disjunkce	k1gFnSc1	disjunkce
(	(	kIx(	(
<g/>
OR	OR	kA	OR
<g/>
)	)	kIx)	)
–	–	k?	–
středník	středník	k1gInSc1	středník
'	'	kIx"	'
<g/>
;	;	kIx,	;
<g/>
'	'	kIx"	'
–	–	k?	–
disjunkci	disjunkce	k1gFnSc4	disjunkce
lze	lze	k6eAd1	lze
také	také	k9	také
zapsat	zapsat	k5eAaPmF	zapsat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
že	že	k8xS	že
pravidlo	pravidlo	k1gNnSc1	pravidlo
rozepíšeme	rozepsat	k5eAaPmIp1nP	rozepsat
na	na	k7c6	na
více	hodně	k6eAd2	hodně
řádků	řádek	k1gInPc2	řádek
<g/>
,	,	kIx,	,
např	např	kA	např
<g/>
:	:	kIx,	:
<g/>
raz	razit	k5eAaImRp2nS	razit
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
dva	dva	k4xCgInPc4	dva
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tri	tri	k?	tri
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
</s>
</p>
<p>
<s>
raz	razit	k5eAaImRp2nS	razit
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
dva	dva	k4xCgInPc4	dva
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
raz	razit	k5eAaImRp2nS	razit
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
tri	tri	k?	tri
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rekurze	rekurz	k1gInSc6	rekurz
===	===	k?	===
</s>
</p>
<p>
<s>
Rekurze	rekurze	k1gFnSc1	rekurze
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
cykly	cyklus	k1gInPc4	cyklus
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používána	používán	k2eAgFnSc1d1	používána
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
predikát	predikát	k1gInSc1	predikát
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
předka	předek	k1gMnSc2	předek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
predek	predek	k1gMnSc1	predek
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
predek	predek	k1gMnSc1	predek
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Z	Z	kA	Z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
predek	predek	k1gMnSc1	predek
<g/>
(	(	kIx(	(
<g/>
Z	Z	kA	Z
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
používání	používání	k1gNnSc6	používání
rekurze	rekurze	k1gFnSc2	rekurze
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dávat	dávat	k5eAaImF	dávat
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
pořadí	pořadí	k1gNnSc4	pořadí
klauzulí	klauzule	k1gFnPc2	klauzule
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
Prolog	prolog	k1gInSc1	prolog
prochází	procházet	k5eAaImIp3nS	procházet
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
prohození	prohození	k1gNnSc1	prohození
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
efektivity	efektivita	k1gFnSc2	efektivita
algoritmu	algoritmus	k1gInSc2	algoritmus
nebo	nebo	k8xC	nebo
až	až	k9	až
k	k	k7c3	k
nekonečnému	konečný	k2eNgInSc3d1	nekonečný
cyklu	cyklus	k1gInSc3	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
prohození	prohození	k1gNnSc1	prohození
klauzulí	klauzule	k1gFnPc2	klauzule
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedeném	uvedený	k2eAgInSc6d1	uvedený
příkladu	příklad	k1gInSc6	příklad
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
nekonečný	konečný	k2eNgInSc1d1	nekonečný
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
predek	predek	k1gMnSc1	predek
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
predek	predek	k1gMnSc1	predek
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Z	Z	kA	Z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
Z	Z	kA	Z
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reverzibilita	reverzibilita	k1gFnSc1	reverzibilita
===	===	k?	===
</s>
</p>
<p>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
programování	programování	k1gNnSc1	programování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
podmnožinu	podmnožina	k1gFnSc4	podmnožina
logického	logický	k2eAgNnSc2d1	logické
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
lze	lze	k6eAd1	lze
funkce	funkce	k1gFnPc4	funkce
uvažovat	uvažovat	k5eAaImF	uvažovat
jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
relací	relace	k1gFnPc2	relace
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
díky	díky	k7c3	díky
relační	relační	k2eAgFnSc3d1	relační
povaze	povaha	k1gFnSc3	povaha
vestavěných	vestavěný	k2eAgInPc2d1	vestavěný
predikátů	predikát	k1gInPc2	predikát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gInSc4	on
používat	používat	k5eAaImF	používat
v	v	k7c6	v
několika	několik	k4yIc6	několik
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
klasická	klasický	k2eAgFnSc1d1	klasická
funkce	funkce	k1gFnSc1	funkce
length	lengtha	k1gFnPc2	lengtha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
délky	délka	k1gFnSc2	délka
seznamu	seznam	k1gInSc2	seznam
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
generovat	generovat	k5eAaImF	generovat
kostru	kostra	k1gFnSc4	kostra
seznamu	seznam	k1gInSc2	seznam
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
délky	délka	k1gFnSc2	délka
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
kostry	kostra	k1gFnSc2	kostra
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
použití	použití	k1gNnSc2	použití
length	lengtha	k1gFnPc2	lengtha
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
seznamu	seznam	k1gInSc2	seznam
(	(	kIx(	(
<g/>
ukazatele	ukazatel	k1gInSc2	ukazatel
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
proměnné	proměnná	k1gFnSc6	proměnná
si	se	k3xPyFc3	se
Prolog	prolog	k1gInSc1	prolog
značí	značit	k5eAaImIp3nS	značit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
_Gčíslo	_Gčísnout	k5eAaPmAgNnS	_Gčísnout
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
length	length	k1gMnSc1	length
<g/>
(	(	kIx(	(
<g/>
Ls	Ls	k1gMnSc1	Ls
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ls	Ls	k?	Ls
=	=	kIx~	=
[	[	kIx(	[
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
=	=	kIx~	=
0	[number]	k4	0
;	;	kIx,	;
</s>
</p>
<p>
<s>
Ls	Ls	k?	Ls
=	=	kIx~	=
[	[	kIx(	[
<g/>
_G	_G	k?	_G
<g/>
358	[number]	k4	358
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
=	=	kIx~	=
1	[number]	k4	1
;	;	kIx,	;
</s>
</p>
<p>
<s>
Ls	Ls	k?	Ls
=	=	kIx~	=
[	[	kIx(	[
<g/>
_G	_G	k?	_G
<g/>
358	[number]	k4	358
<g/>
,	,	kIx,	,
_G	_G	k?	_G
<g/>
361	[number]	k4	361
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
L	L	kA	L
=	=	kIx~	=
2	[number]	k4	2
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
append	append	k1gInSc4	append
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
dvou	dva	k4xCgInPc2	dva
seznamů	seznam	k1gInPc2	seznam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
append	append	k1gInSc4	append
<g/>
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Ls	Ls	k1gMnSc1	Ls
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ls	Ls	k?	Ls
=	=	kIx~	=
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
pro	pro	k7c4	pro
rozdělení	rozdělení	k1gNnSc4	rozdělení
jednoho	jeden	k4xCgInSc2	jeden
seznamu	seznam	k1gInSc2	seznam
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
length	length	k1gMnSc1	length
<g/>
(	(	kIx(	(
<g/>
Xs	Xs	k1gMnSc1	Xs
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
append	append	k1gMnSc1	append
<g/>
(	(	kIx(	(
<g/>
Xs	Xs	k1gMnSc1	Xs
<g/>
,	,	kIx,	,
Ys	Ys	k1gMnSc1	Ys
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Xs	Xs	k?	Xs
=	=	kIx~	=
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Ys	Ys	k?	Ys
=	=	kIx~	=
[	[	kIx(	[
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgFnSc1d1	malá
knihovna	knihovna	k1gFnSc1	knihovna
predikátů	predikát	k1gInPc2	predikát
vystačí	vystačit	k5eAaBmIp3nS	vystačit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
způsob	způsob	k1gInSc1	způsob
využití	využití	k1gNnSc2	využití
predikátů	predikát	k1gInPc2	predikát
je	být	k5eAaImIp3nS	být
testování	testování	k1gNnSc1	testování
pravdivosti	pravdivost	k1gFnSc2	pravdivost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
všechny	všechen	k3xTgInPc1	všechen
argumenty	argument	k1gInPc1	argument
<g/>
.	.	kIx.	.
</s>
<s>
Predikát	predikát	k1gInSc1	predikát
vrací	vracet	k5eAaImIp3nS	vracet
hodnotu	hodnota	k1gFnSc4	hodnota
Yes	Yes	k1gFnSc2	Yes
<g/>
,	,	kIx,	,
když	když	k8xS	když
vztah	vztah	k1gInSc1	vztah
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
No	no	k9	no
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
append	append	k1gInSc4	append
<g/>
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
x	x	k?	x
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
x	x	k?	x
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Yes	Yes	k?	Yes
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
append	append	k1gInSc4	append
<g/>
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
x	x	k?	x
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
y	y	k?	y
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
x	x	k?	x
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
No	no	k9	no
</s>
</p>
<p>
<s>
===	===	k?	===
Operátory	operátor	k1gInPc4	operátor
===	===	k?	===
</s>
</p>
<p>
<s>
Operátor	operátor	k1gInSc1	operátor
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Prologu	prolog	k1gInSc2	prolog
pouze	pouze	k6eAd1	pouze
jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
zápisu	zápis	k1gInSc2	zápis
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Strukturu	struktura	k1gFnSc4	struktura
s	s	k7c7	s
funktorem	funktor	k1gInSc7	funktor
+	+	kIx~	+
(	(	kIx(	(
<g/>
běžné	běžný	k2eAgNnSc4d1	běžné
sčítání	sčítání	k1gNnSc4	sčítání
<g/>
)	)	kIx)	)
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
správně	správně	k6eAd1	správně
zapisovat	zapisovat	k5eAaImF	zapisovat
takto	takto	k6eAd1	takto
jako	jako	k8xC	jako
strukturu	struktura	k1gFnSc4	struktura
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prolog	prolog	k1gInSc4	prolog
nám	my	k3xPp1nPc3	my
však	však	k9	však
povoluje	povolovat	k5eAaImIp3nS	povolovat
i	i	k9	i
tento	tento	k3xDgInSc1	tento
infixový	infixový	k2eAgInSc1d1	infixový
zápis	zápis	k1gInSc1	zápis
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
B.	B.	kA	B.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
vestavěných	vestavěný	k2eAgInPc2d1	vestavěný
operátorů	operátor	k1gInPc2	operátor
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
libovolné	libovolný	k2eAgInPc4d1	libovolný
vlastní	vlastní	k2eAgInPc4d1	vlastní
operátory	operátor	k1gInPc4	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
operátory	operátor	k1gInPc4	operátor
definujeme	definovat	k5eAaBmIp1nP	definovat
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
priorita	priorita	k1gFnSc1	priorita
<g/>
,	,	kIx,	,
specifikator	specifikator	k1gInSc4	specifikator
<g/>
,	,	kIx,	,
jmeno	jmeno	k6eAd1	jmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Priorita	priorita	k1gFnSc1	priorita
je	být	k5eAaImIp3nS	být
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgNnSc6	jaký
pořadí	pořadí	k1gNnSc6	pořadí
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
operátory	operátor	k1gInPc1	operátor
vyhodnocovat	vyhodnocovat	k5eAaImF	vyhodnocovat
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
výraz	výraz	k1gInSc1	výraz
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
operátorů	operátor	k1gInPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
číslo	číslo	k1gNnSc1	číslo
znamená	znamenat	k5eAaImIp3nS	znamenat
nižší	nízký	k2eAgFnSc4d2	nižší
prioritu	priorita	k1gFnSc4	priorita
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
výraz	výraz	k1gInSc1	výraz
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
operátory	operátor	k1gInPc4	operátor
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
prioritou	priorita	k1gFnSc7	priorita
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
program	program	k1gInSc4	program
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
směrem	směr	k1gInSc7	směr
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
postupovat	postupovat	k5eAaImF	postupovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
operátoru	operátor	k1gInSc6	operátor
definuje	definovat	k5eAaBmIp3nS	definovat
specifikátor	specifikátor	k1gInSc1	specifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Specifikátory	specifikátor	k1gInPc4	specifikátor
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
podle	podle	k7c2	podle
směru	směr	k1gInSc2	směr
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
operandu	operand	k1gInSc3	operand
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Infixový	infixový	k2eAgInSc1d1	infixový
binární	binární	k2eAgInSc1d1	binární
operátor	operátor	k1gInSc1	operátor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
yfx	yfx	k?	yfx
směr	směr	k1gInSc1	směr
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
zleva	zleva	k6eAd1	zleva
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
použití	použití	k1gNnSc1	použití
jsou	být	k5eAaImIp3nP	být
aritmetické	aritmetický	k2eAgInPc1d1	aritmetický
operátory	operátor	k1gInPc1	operátor
</s>
</p>
<p>
<s>
xfy	xfy	k?	xfy
směr	směr	k1gInSc1	směr
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
zprava	zprava	k6eAd1	zprava
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
operátor	operátor	k1gInSc4	operátor
is	is	k?	is
</s>
</p>
<p>
<s>
xfx	xfx	k?	xfx
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
směru	směr	k1gInSc6	směr
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nalevo	nalevo	k6eAd1	nalevo
i	i	k9	i
napravo	napravo	k6eAd1	napravo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
operátory	operátor	k1gInPc1	operátor
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
vyhodnocovací	vyhodnocovací	k2eAgFnSc7d1	vyhodnocovací
prioritou	priorita	k1gFnSc7	priorita
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
použití	použití	k1gNnSc1	použití
jsou	být	k5eAaImIp3nP	být
porovnání	porovnání	k1gNnSc1	porovnání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prefixový	prefixový	k2eAgInSc1d1	prefixový
unární	unární	k2eAgInSc1d1	unární
operátor	operátor	k1gInSc1	operátor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
fx	fx	k?	fx
-	-	kIx~	-
operátor	operátor	k1gMnSc1	operátor
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
operandem	operand	k1gInSc7	operand
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
operátor	operátor	k1gInSc4	operátor
not	nota	k1gFnPc2	nota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
i	i	k9	i
postfixový	postfixový	k2eAgInSc4d1	postfixový
unární	unární	k2eAgInSc4d1	unární
operátor	operátor	k1gInSc4	operátor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
xf	xf	k?	xf
-	-	kIx~	-
operátor	operátor	k1gMnSc1	operátor
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
operandem	operand	k1gInSc7	operand
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgInSc1	žádný
standardní	standardní	k2eAgInSc1d1	standardní
operátor	operátor	k1gInSc1	operátor
tento	tento	k3xDgInSc4	tento
specifikátor	specifikátor	k1gInSc4	specifikátor
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
<g/>
Standardně	standardně	k6eAd1	standardně
definované	definovaný	k2eAgInPc1d1	definovaný
operátory	operátor	k1gInPc1	operátor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
1200	[number]	k4	1200
<g/>
,	,	kIx,	,
xfx	xfx	k?	xfx
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
:	:	kIx,	:
<g/>
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
1200	[number]	k4	1200
<g/>
,	,	kIx,	,
fx	fx	k?	fx
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
:	:	kIx,	:
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
1100	[number]	k4	1100
<g/>
,	,	kIx,	,
xfy	xfy	k?	xfy
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
;	;	kIx,	;
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
xfy	xfy	k?	xfy
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
700	[number]	k4	700
<g/>
,	,	kIx,	,
xfx	xfx	k?	xfx
<g/>
,	,	kIx,	,
[	[	kIx(	[
=	=	kIx~	=
<g/>
,	,	kIx,	,
\	\	kIx~	\
<g/>
=	=	kIx~	=
<g/>
,	,	kIx,	,
is	is	k?	is
<g/>
,	,	kIx,	,
<	<	kIx(	<
<g/>
,	,	kIx,	,
>	>	kIx)	>
<g/>
,	,	kIx,	,
=	=	kIx~	=
<g/>
<	<	kIx(	<
<g/>
,	,	kIx,	,
>	>	kIx)	>
<g/>
=	=	kIx~	=
<g/>
,	,	kIx,	,
==	==	k?	==
<g/>
,	,	kIx,	,
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
=	=	kIx~	=
<g/>
,	,	kIx,	,
\	\	kIx~	\
<g/>
==	==	k?	==
<g/>
,	,	kIx,	,
=	=	kIx~	=
<g/>
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
,	,	kIx,	,
yfx	yfx	k?	yfx
<g/>
,	,	kIx,	,
[	[	kIx(	[
+	+	kIx~	+
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
,	,	kIx,	,
fx	fx	k?	fx
<g/>
,	,	kIx,	,
[	[	kIx(	[
+	+	kIx~	+
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
,	,	kIx,	,
not	nota	k1gFnPc2	nota
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
400	[number]	k4	400
<g/>
,	,	kIx,	,
yfx	yfx	k?	yfx
<g/>
,	,	kIx,	,
[	[	kIx(	[
*	*	kIx~	*
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
,	,	kIx,	,
div	div	k1gInSc1	div
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
-op	p	k?	-op
<g/>
(	(	kIx(	(
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
xfx	xfx	k?	xfx
<g/>
,	,	kIx,	,
mod	mod	k?	mod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Negace	negace	k1gFnSc2	negace
-	-	kIx~	-
not	nota	k1gFnPc2	nota
====	====	k?	====
</s>
</p>
<p>
<s>
Negace	negace	k1gFnSc1	negace
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
je	být	k5eAaImIp3nS	být
chápána	chápat	k5eAaImNgFnS	chápat
trochu	trochu	k6eAd1	trochu
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
v	v	k7c6	v
hovoru	hovor	k1gInSc6	hovor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
logice	logika	k1gFnSc6	logika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
výrok	výrok	k1gInSc4	výrok
A	a	k8xC	a
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
negace	negace	k1gFnSc1	negace
not	nota	k1gFnPc2	nota
A	a	k9	a
výroky	výrok	k1gInPc4	výrok
o	o	k7c6	o
témže	týž	k3xTgInSc6	týž
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výroky	výrok	k1gInPc1	výrok
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
hezká	hezký	k2eAgFnSc1d1	hezká
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
hezká	hezký	k2eAgFnSc1d1	hezká
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
hodnocením	hodnocení	k1gNnSc7	hodnocení
nějakého	nějaký	k3yIgInSc2	nějaký
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
</s>
</p>
<p>
<s>
jehezka	jehezka	k1gFnSc1	jehezka
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
%	%	kIx~	%
<g/>
je	být	k5eAaImIp3nS	být
konstatováním	konstatování	k1gNnSc7	konstatování
vlastnosti	vlastnost	k1gFnSc2	vlastnost
objektu	objekt	k1gInSc2	objekt
X	X	kA	X
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
negace	negace	k1gFnSc1	negace
</s>
</p>
<p>
<s>
not	nota	k1gFnPc2	nota
jehezka	jehezka	k1gFnSc1	jehezka
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
%	%	kIx~	%
<g/>
znamená	znamenat	k5eAaImIp3nS	znamenat
jen	jen	k9	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
programu	program	k1gInSc2	program
nelze	lze	k6eNd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
platnost	platnost	k1gFnSc4	platnost
predikátu	predikát	k1gInSc2	predikát
jehezka	jehezka	k1gFnSc1	jehezka
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
negace	negace	k1gFnSc1	negace
výrok	výrok	k1gInSc1	výrok
o	o	k7c6	o
programu	program	k1gInSc6	program
<g/>
,	,	kIx,	,
ne	ne	k9	ne
o	o	k7c6	o
objektu	objekt	k1gInSc6	objekt
X	X	kA	X
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
)	)	kIx)	)
X	X	kA	X
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hezká	hezký	k2eAgFnSc1d1	hezká
-	-	kIx~	-
ba	ba	k9	ba
překrásná	překrásný	k2eAgFnSc1d1	překrásná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
zanést	zanést	k5eAaPmF	zanést
do	do	k7c2	do
programu	program	k1gInSc2	program
-	-	kIx~	-
a	a	k8xC	a
už	už	k6eAd1	už
platí	platit	k5eAaImIp3nS	platit
not	nota	k1gFnPc2	nota
hezka	hezka	k1gMnSc1	hezka
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
program	program	k1gInSc4	program
X	X	kA	X
hezká	hezký	k2eAgFnSc1d1	hezká
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
negací	negace	k1gFnSc7	negace
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
Prologu	prolog	k1gInSc2	prolog
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
v	v	k7c6	v
soudnictví	soudnictví	k1gNnSc6	soudnictví
<g/>
:	:	kIx,	:
Komu	kdo	k3yQnSc3	kdo
jsme	být	k5eAaImIp1nP	být
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrah	vrah	k1gMnSc1	vrah
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
vrahem	vrah	k1gMnSc7	vrah
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
práva	právo	k1gNnSc2	právo
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Porovnání	porovnání	k1gNnSc1	porovnání
-	-	kIx~	-
=	=	kIx~	=
====	====	k?	====
</s>
</p>
<p>
<s>
Operátor	operátor	k1gInSc1	operátor
=	=	kIx~	=
uspěje	uspět	k5eAaPmIp3nS	uspět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc4	dva
argumenty	argument	k1gInPc4	argument
povede	povést	k5eAaPmIp3nS	povést
unifikovat	unifikovat	k5eAaImF	unifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Porovnání	porovnání	k1gNnSc1	porovnání
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
čtyř	čtyři	k4xCgInPc2	čtyři
různých	různý	k2eAgInPc2d1	různý
významů	význam	k1gInPc2	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
proměnná	proměnná	k1gFnSc1	proměnná
specifikovaná	specifikovaný	k2eAgFnSc1d1	specifikovaná
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nespecifikovaná	specifikovaný	k2eNgFnSc1d1	nespecifikovaná
<g/>
,	,	kIx,	,
stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
proměnné	proměnná	k1gFnPc1	proměnná
specifikované	specifikovaný	k2eAgFnPc1d1	specifikovaná
stejnou	stejný	k2eAgFnSc7d1	stejná
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
proměnné	proměnná	k1gFnPc1	proměnná
specifikované	specifikovaný	k2eAgFnPc1d1	specifikovaná
<g/>
,	,	kIx,	,
porovnají	porovnat	k5eAaPmIp3nP	porovnat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
integrity	integrita	k1gFnPc1	integrita
a	a	k8xC	a
atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
rovny	roven	k2eAgMnPc4d1	roven
pouze	pouze	k6eAd1	pouze
samy	sám	k3xTgInPc4	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
obě	dva	k4xCgFnPc1	dva
proměnné	proměnná	k1gFnPc1	proměnná
nespecifikovány	specifikován	k2eNgFnPc1d1	nespecifikována
<g/>
,	,	kIx,	,
stanou	stanout	k5eAaPmIp3nP	stanout
se	s	k7c7	s
sdílenými	sdílený	k2eAgMnPc7d1	sdílený
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
specifikovanou	specifikovaný	k2eAgFnSc7d1	specifikovaná
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
stejnou	stejný	k2eAgFnSc7d1	stejná
hodnotou	hodnota	k1gFnSc7	hodnota
specifikována	specifikovat	k5eAaBmNgFnS	specifikovat
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
2	[number]	k4	2
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovny	roven	k2eAgFnPc1d1	rovna
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
funktor	funktor	k1gInSc4	funktor
<g/>
,	,	kIx,	,
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
počet	počet	k1gInSc1	počet
komponent	komponenta	k1gFnPc2	komponenta
a	a	k8xC	a
korespondující	korespondující	k2eAgFnPc4d1	korespondující
komponenty	komponenta	k1gFnPc4	komponenta
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovny	roven	k2eAgFnPc1d1	rovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vyčíslení	vyčíslení	k1gNnSc2	vyčíslení
-	-	kIx~	-
is	is	k?	is
====	====	k?	====
</s>
</p>
<p>
<s>
Operátor	operátor	k1gInSc1	operátor
is	is	k?	is
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Číslo	číslo	k1gNnSc4	číslo
is	is	k?	is
Výraz	výraz	k1gInSc1	výraz
a	a	k8xC	a
vydá	vydat	k5eAaPmIp3nS	vydat
true	true	k6eAd1	true
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
volná	volný	k2eAgFnSc1d1	volná
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
)	)	kIx)	)
úspěšně	úspěšně	k6eAd1	úspěšně
unifikováno	unifikovat	k5eAaImNgNnS	unifikovat
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
Výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
proměnné	proměnná	k1gFnPc1	proměnná
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
<g/>
)	)	kIx)	)
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
Výrazu	výraz	k1gInSc6	výraz
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
splňování	splňování	k1gNnSc4	splňování
cíle	cíl	k1gInPc1	cíl
konkretizovány	konkretizován	k2eAgInPc1d1	konkretizován
na	na	k7c4	na
číselnou	číselný	k2eAgFnSc4d1	číselná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
běhové	běhový	k2eAgFnSc3d1	běhová
chybě	chyba	k1gFnSc3	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
běhové	běhový	k2eAgFnSc3d1	běhová
chybě	chyba	k1gFnSc3	chyba
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
proměnná	proměnná	k1gFnSc1	proměnná
Číslo	číslo	k1gNnSc4	číslo
již	již	k6eAd1	již
konkretizována	konkretizovat	k5eAaBmNgNnP	konkretizovat
na	na	k7c4	na
nečíselnou	číselný	k2eNgFnSc4d1	nečíselná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
funkce	funkce	k1gFnSc2	funkce
operátoru	operátor	k1gInSc2	operátor
porovnání	porovnání	k1gNnSc2	porovnání
a	a	k8xC	a
vyčíslení	vyčíslení	k1gNnSc2	vyčíslení
na	na	k7c6	na
predikátu	predikát	k1gInSc6	predikát
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
obsahu	obsah	k1gInSc2	obsah
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
obsah_kruhu	obsah_kruha	k1gFnSc4	obsah_kruha
<g/>
(	(	kIx(	(
<g/>
Polomer	Polomer	k1gInSc1	Polomer
<g/>
,	,	kIx,	,
<g/>
Obsah	obsah	k1gInSc1	obsah
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
Obsah	obsah	k1gInSc1	obsah
is	is	k?	is
3.14	[number]	k4	3.14
*	*	kIx~	*
Polomer	Polomer	k1gMnSc1	Polomer
*	*	kIx~	*
Polomer	Polomer	k1gMnSc1	Polomer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
obsah_kruhu	obsah_kruh	k1gInSc2	obsah_kruh
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
Polomer	Polomer	k1gInSc1	Polomer
<g/>
,	,	kIx,	,
<g/>
Obsah	obsah	k1gInSc1	obsah
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
Obsah	obsah	k1gInSc1	obsah
=	=	kIx~	=
3.14	[number]	k4	3.14
*	*	kIx~	*
Polomer	Polomer	k1gMnSc1	Polomer
*	*	kIx~	*
Polomer	Polomer	k1gMnSc1	Polomer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
obsah_kruhu	obsah_kruha	k1gFnSc4	obsah_kruha
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Y	Y	kA	Y
=	=	kIx~	=
78.5	[number]	k4	78.5
</s>
</p>
<p>
<s>
Yes	Yes	k?	Yes
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
obsah_kruhu	obsah_kruh	k1gInSc3	obsah_kruh
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Y	Y	kA	Y
=	=	kIx~	=
3.14	[number]	k4	3.14
<g/>
*	*	kIx~	*
<g/>
5	[number]	k4	5
<g/>
*	*	kIx~	*
<g/>
5	[number]	k4	5
</s>
</p>
<p>
<s>
Yes	Yes	k?	Yes
</s>
</p>
<p>
<s>
====	====	k?	====
Příklady	příklad	k1gInPc1	příklad
použití	použití	k1gNnSc2	použití
operátorů	operátor	k1gInPc2	operátor
====	====	k?	====
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
=	=	kIx~	=
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
no	no	k9	no
%	%	kIx~	%
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
termy	term	k1gInPc4	term
nelze	lze	k6eNd1	lze
unifikovat	unifikovat	k5eAaImF	unifikovat
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
=	=	kIx~	=
<g/>
:	:	kIx,	:
<g/>
=	=	kIx~	=
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
yes	yes	k?	yes
<g/>
.	.	kIx.	.
%	%	kIx~	%
oba	dva	k4xCgInPc1	dva
aritmetické	aritmetický	k2eAgInPc1d1	aritmetický
výrazy	výraz	k1gInPc1	výraz
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
hodnotu	hodnota	k1gFnSc4	hodnota
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
X	X	kA	X
is	is	k?	is
Y	Y	kA	Y
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
%	%	kIx~	%
běhová	běhový	k2eAgFnSc1d1	běhová
chyba	chyba	k1gFnSc1	chyba
-	-	kIx~	-
nedefinovaný	definovaný	k2eNgInSc1d1	nedefinovaný
aritmetický	aritmetický	k2eAgInSc1d1	aritmetický
výraz	výraz	k1gInSc1	výraz
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
X	X	kA	X
<g/>
<	<	kIx(	<
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
%	%	kIx~	%
běhová	běhový	k2eAgFnSc1d1	běhová
chyba	chyba	k1gFnSc1	chyba
-	-	kIx~	-
výraz	výraz	k1gInSc1	výraz
vlevo	vlevo	k6eAd1	vlevo
nelze	lze	k6eNd1	lze
vyčíslit	vyčíslit	k5eAaPmF	vyčíslit
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
X	X	kA	X
is	is	k?	is
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Y	Y	kA	Y
is	is	k?	is
2	[number]	k4	2
<g/>
*	*	kIx~	*
<g/>
X	X	kA	X
,	,	kIx,	,
Z	z	k7c2	z
is	is	k?	is
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Y	Y	kA	Y
is	is	k?	is
Z	Z	kA	Z
.	.	kIx.	.
</s>
</p>
<p>
<s>
no	no	k9	no
%	%	kIx~	%
za	za	k7c4	za
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
proměnné	proměnná	k1gFnPc4	proměnná
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
substituovala	substituovat	k5eAaBmAgNnP	substituovat
čísla	číslo	k1gNnPc1	číslo
X	X	kA	X
<g/>
=	=	kIx~	=
<g/>
5	[number]	k4	5
,	,	kIx,	,
Y	Y	kA	Y
<g/>
=	=	kIx~	=
<g/>
10	[number]	k4	10
a	a	k8xC	a
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
%	%	kIx~	%
při	při	k7c6	při
vyhodnocování	vyhodnocování	k1gNnSc6	vyhodnocování
predikátu	predikát	k1gInSc2	predikát
Y	Y	kA	Y
is	is	k?	is
Z	z	k7c2	z
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
proměnná	proměnná	k1gFnSc1	proměnná
Y	Y	kA	Y
již	již	k6eAd1	již
konkretizována	konkretizován	k2eAgFnSc1d1	konkretizována
a	a	k8xC	a
</s>
</p>
<p>
<s>
%	%	kIx~	%
její	její	k3xOp3gFnSc1	její
hodnota	hodnota	k1gFnSc1	hodnota
různá	různý	k2eAgFnSc1d1	různá
od	od	k7c2	od
hodnoty	hodnota	k1gFnSc2	hodnota
proměnné	proměnná	k1gFnSc2	proměnná
Z	z	k7c2	z
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
X	X	kA	X
is	is	k?	is
7	[number]	k4	7
,	,	kIx,	,
X	X	kA	X
is	is	k?	is
X	X	kA	X
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
.	.	kIx.	.
</s>
</p>
<p>
<s>
no	no	k9	no
%	%	kIx~	%
za	za	k7c2	za
X	X	kA	X
substituovalo	substituovat	k5eAaBmAgNnS	substituovat
7	[number]	k4	7
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
není	být	k5eNaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
8	[number]	k4	8
</s>
</p>
<p>
<s>
===	===	k?	===
Vstupy	vstup	k1gInPc1	vstup
a	a	k8xC	a
výstupy	výstup	k1gInPc1	výstup
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pohodlnější	pohodlný	k2eAgFnSc4d2	pohodlnější
práci	práce	k1gFnSc4	práce
nabízejí	nabízet	k5eAaImIp3nP	nabízet
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
implementace	implementace	k1gFnPc1	implementace
Prologu	prolog	k1gInSc2	prolog
řadu	řad	k1gInSc2	řad
předdefinovaných	předdefinovaný	k2eAgInPc2d1	předdefinovaný
predikátů	predikát	k1gInPc2	predikát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
predikáty	predikát	k1gInPc1	predikát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
každý	každý	k3xTgInSc1	každý
plnoprávný	plnoprávný	k2eAgInSc1d1	plnoprávný
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
Prolog	prolog	k1gInSc1	prolog
predikáty	predikát	k1gInPc1	predikát
pro	pro	k7c4	pro
klasický	klasický	k2eAgInSc4d1	klasický
vstup	vstup	k1gInSc4	vstup
ze	z	k7c2	z
souboru	soubor	k1gInSc2	soubor
a	a	k8xC	a
výstup	výstup	k1gInSc4	výstup
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
predikáty	predikát	k1gInPc1	predikát
už	už	k6eAd1	už
nemají	mít	k5eNaImIp3nP	mít
relační	relační	k2eAgInSc4d1	relační
význam	význam	k1gInSc4	význam
a	a	k8xC	a
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
čistě	čistě	k6eAd1	čistě
logickou	logický	k2eAgFnSc4d1	logická
podmnožinu	podmnožina	k1gFnSc4	podmnožina
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pravdivostní	pravdivostní	k2eAgFnSc1d1	pravdivostní
hodnota	hodnota	k1gFnSc1	hodnota
některých	některý	k3yIgInPc2	některý
predikátů	predikát	k1gInPc2	predikát
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
a	a	k8xC	a
výstup	výstup	k1gInSc4	výstup
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
true	true	k6eAd1	true
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
predikátem	predikát	k1gInSc7	predikát
fail	fainout	k5eAaPmAgMnS	fainout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
notoricky	notoricky	k6eAd1	notoricky
známého	známý	k2eAgInSc2d1	známý
programu	program	k1gInSc2	program
"	"	kIx"	"
<g/>
Ahoj	ahoj	k0	ahoj
světe	svět	k1gInSc5	svět
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
-	-	kIx~	-
write	wriit	k5eAaBmRp2nP	wriit
<g/>
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
Ahoj	ahoj	k0	ahoj
světe	svět	k1gInSc5	svět
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ahoj	ahoj	k0	ahoj
světe	svět	k1gInSc5	svět
</s>
</p>
<p>
<s>
Yes	Yes	k?	Yes
</s>
</p>
<p>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
implementacích	implementace	k1gFnPc6	implementace
Prologu	prolog	k1gInSc2	prolog
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
predikáty	predikát	k1gInPc1	predikát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
uživatel	uživatel	k1gMnSc1	uživatel
měl	mít	k5eAaImAgMnS	mít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
všechno	všechen	k3xTgNnSc4	všechen
nabízí	nabízet	k5eAaImIp3nS	nabízet
implementace	implementace	k1gFnSc1	implementace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
predikáty	predikát	k1gInPc1	predikát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
standardní	standardní	k2eAgFnPc4d1	standardní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c4	v
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
<g/>
)	)	kIx)	)
každé	každý	k3xTgFnSc3	každý
implementaci	implementace	k1gFnSc3	implementace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
aktuálního	aktuální	k2eAgInSc2d1	aktuální
vstupu	vstup	k1gInSc2	vstup
a	a	k8xC	a
výstupu	výstup	k1gInSc2	výstup
slouží	sloužit	k5eAaImIp3nP	sloužit
predikáty	predikát	k1gInPc1	predikát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
see	see	k?	see
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
-nastaví	astavit	k5eAaPmIp3nS	-nastavit
aktuální	aktuální	k2eAgInSc4d1	aktuální
vstup	vstup	k1gInSc4	vstup
ze	z	k7c2	z
souboru	soubor	k1gInSc2	soubor
F	F	kA	F
</s>
</p>
<p>
<s>
seen	seen	k1gMnSc1	seen
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
-uzavře	zavřít	k5eAaPmIp3nS	-uzavřít
vstupní	vstupní	k2eAgInSc4d1	vstupní
soubor	soubor	k1gInSc4	soubor
F	F	kA	F
a	a	k8xC	a
obnoví	obnovit	k5eAaPmIp3nS	obnovit
aktuální	aktuální	k2eAgInSc4d1	aktuální
vstup	vstup	k1gInSc4	vstup
z	z	k7c2	z
klávesnice	klávesnice	k1gFnSc2	klávesnice
(	(	kIx(	(
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
zavolal	zavolat	k5eAaPmAgMnS	zavolat
see	see	k?	see
<g/>
(	(	kIx(	(
<g/>
user	usrat	k5eAaPmRp2nS	usrat
<g/>
)	)	kIx)	)
)	)	kIx)	)
</s>
</p>
<p>
<s>
seeing	seeing	k1gInSc1	seeing
<g/>
(	(	kIx(	(
<g/>
-F	-F	k?	-F
<g/>
)	)	kIx)	)
<g/>
-dotaz	otaz	k1gInSc1	-dotaz
na	na	k7c4	na
aktuální	aktuální	k2eAgInSc4d1	aktuální
vstupní	vstupní	k2eAgInSc4d1	vstupní
soubor	soubor	k1gInSc4	soubor
</s>
</p>
<p>
<s>
tell	tell	k1gMnSc1	tell
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
-nastaví	astavit	k5eAaPmIp3nS	-nastavit
aktuální	aktuální	k2eAgInSc4d1	aktuální
výstup	výstup	k1gInSc4	výstup
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
F	F	kA	F
</s>
</p>
<p>
<s>
told	told	k1gMnSc1	told
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
-uzavře	zavřít	k5eAaPmIp3nS	-uzavřít
výstupní	výstupní	k2eAgInSc1d1	výstupní
soubor	soubor	k1gInSc1	soubor
F	F	kA	F
a	a	k8xC	a
obnoví	obnovit	k5eAaPmIp3nS	obnovit
aktuální	aktuální	k2eAgInSc4d1	aktuální
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
obrazovku	obrazovka	k1gFnSc4	obrazovka
(	(	kIx(	(
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
zavolal	zavolat	k5eAaPmAgMnS	zavolat
tell	tell	k1gInSc4	tell
<g/>
(	(	kIx(	(
<g/>
user	usrat	k5eAaPmRp2nS	usrat
<g/>
)	)	kIx)	)
)	)	kIx)	)
</s>
</p>
<p>
<s>
telling	telling	k1gInSc1	telling
<g/>
(	(	kIx(	(
<g/>
-F	-F	k?	-F
<g/>
)	)	kIx)	)
<g/>
-dotaz	otaz	k1gInSc1	-dotaz
na	na	k7c4	na
aktuální	aktuální	k2eAgInSc4d1	aktuální
výstupní	výstupní	k2eAgInSc4d1	výstupní
soubor	soubor	k1gInSc4	soubor
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
typem	typ	k1gInSc7	typ
vstupu	vstup	k1gInSc2	vstup
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
výstupu	výstup	k1gInSc3	výstup
<g/>
)	)	kIx)	)
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc1	vstup
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
výstup	výstup	k1gInSc4	výstup
<g/>
)	)	kIx)	)
termů	term	k1gInPc2	term
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
predikáty	predikát	k1gInPc1	predikát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
read	read	k1gInSc1	read
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
-přečte	řéct	k5eAaPmRp2nP	-přéct
z	z	k7c2	z
aktuálního	aktuální	k2eAgInSc2d1	aktuální
vstupu	vstup	k1gInSc2	vstup
jeden	jeden	k4xCgInSc4	jeden
term	termy	k1gFnPc2	termy
(	(	kIx(	(
<g/>
ukončený	ukončený	k2eAgInSc1d1	ukončený
tečkou	tečka	k1gFnSc7	tečka
<g/>
)	)	kIx)	)
a	a	k8xC	a
unifikuje	unifikovat	k5eAaImIp3nS	unifikovat
jej	on	k3xPp3gMnSc4	on
s	s	k7c7	s
argumentem	argument	k1gInSc7	argument
T	T	kA	T
</s>
</p>
<p>
<s>
write	write	k5eAaPmIp2nP	write
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
-vystoupí	ystoupit	k5eAaPmIp3nS	-vystoupit
instance	instance	k1gFnSc1	instance
termu	term	k1gInSc2	term
T	T	kA	T
s	s	k7c7	s
právě	právě	k6eAd1	právě
platnými	platný	k2eAgFnPc7d1	platná
hodnotami	hodnota	k1gFnPc7	hodnota
substitucí	substituce	k1gFnPc2	substituce
za	za	k7c4	za
proměnné	proměnná	k1gFnPc4	proměnná
v	v	k7c6	v
termu	term	k1gInSc6	term
T	T	kA	T
obsažené	obsažený	k2eAgInPc1d1	obsažený
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc4	vstup
a	a	k8xC	a
výstup	výstup	k1gInSc4	výstup
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
např.	např.	kA	např.
tyto	tento	k3xDgInPc4	tento
predikáty	predikát	k1gInPc4	predikát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
get	get	k?	get
<g/>
0	[number]	k4	0
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
Z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
-přečte	řéct	k5eAaPmRp2nP	-přéct
jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
z	z	k7c2	z
aktuálního	aktuální	k2eAgInSc2d1	aktuální
vstupu	vstup	k1gInSc2	vstup
<g/>
,	,	kIx,	,
uspěje	uspět	k5eAaPmIp3nS	uspět
pokud	pokud	k8xS	pokud
jej	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
unifikovat	unifikovat	k5eAaImF	unifikovat
s	s	k7c7	s
termem	term	k1gInSc7	term
Z	z	k7c2	z
</s>
</p>
<p>
<s>
get	get	k?	get
<g/>
(	(	kIx(	(
<g/>
Z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
-chová	hová	k1gFnSc1	-chová
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
(	(	kIx(	(
<g/>
přeskakuje	přeskakovat	k5eAaImIp3nS	přeskakovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řídicí	řídicí	k2eAgInPc4d1	řídicí
znaky	znak	k1gInPc4	znak
(	(	kIx(	(
<g/>
ASCII	ascii	kA	ascii
<	<	kIx(	<
32	[number]	k4	32
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
put	puta	k1gFnPc2	puta
<g/>
(	(	kIx(	(
<g/>
Z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
-výstup	ýstup	k1gInSc1	-výstup
znaku	znak	k1gInSc2	znak
do	do	k7c2	do
aktuálního	aktuální	k2eAgInSc2d1	aktuální
výstupu	výstup	k1gInSc2	výstup
(	(	kIx(	(
<g/>
ne	ne	k9	ne
řídicí	řídicí	k2eAgInPc4d1	řídicí
znaky	znak	k1gInPc4	znak
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tab	tab	kA	tab
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
-vystoupí	ystoupit	k5eAaPmIp3nP	-vystoupit
N	N	kA	N
mezer	mezera	k1gFnPc2	mezera
(	(	kIx(	(
<g/>
N	N	kA	N
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nl-přechod	nlřechod	k1gInSc1	nl-přechod
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
řádku	řádka	k1gFnSc4	řádka
(	(	kIx(	(
pascalské	pascalský	k2eAgNnSc4d1	pascalský
writeln	writeln	k1gNnSc4	writeln
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyhodnocování	vyhodnocování	k1gNnSc2	vyhodnocování
==	==	k?	==
</s>
</p>
<p>
<s>
Mějme	mít	k5eAaImRp1nP	mít
databázi	databáze	k1gFnSc4	databáze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
<g/>
(	(	kIx(	(
<g/>
petr	petr	k1gMnSc1	petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
<g/>
(	(	kIx(	(
<g/>
marek	marka	k1gFnPc2	marka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
<g/>
(	(	kIx(	(
<g/>
Pritel	Pritel	k1gMnSc1	Pritel
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
markuv_pritel	markuv_pritel	k1gMnSc1	markuv_pritel
<g/>
(	(	kIx(	(
<g/>
Pritel	Pritel	k1gMnSc1	Pritel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
petruv_pritel	petruv_pritel	k1gMnSc1	petruv_pritel
<g/>
(	(	kIx(	(
<g/>
Pritel	Pritel	k1gMnSc1	Pritel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
markuv_pritel	markuv_pritel	k1gMnSc1	markuv_pritel
<g/>
(	(	kIx(	(
<g/>
lenka	lenka	k1gFnSc1	lenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
markuv_pritel	markuv_pritel	k1gMnSc1	markuv_pritel
<g/>
(	(	kIx(	(
<g/>
jirka	jirka	k1gMnSc1	jirka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
petruv_pritel	petruv_pritel	k1gMnSc1	petruv_pritel
<g/>
(	(	kIx(	(
<g/>
jirka	jirka	k1gMnSc1	jirka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
petruv_pritel	petruv_pritel	k1gMnSc1	petruv_pritel
<g/>
(	(	kIx(	(
<g/>
petra	petra	k1gMnSc1	petra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
(	(	kIx(	(
<g/>
Kdo	kdo	k3yQnSc1	kdo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
získáme	získat	k5eAaPmIp1nP	získat
následující	následující	k2eAgFnPc4d1	následující
odpovědi	odpověď	k1gFnPc4	odpověď
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
=	=	kIx~	=
petr	petr	k1gMnSc1	petr
;	;	kIx,	;
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
=	=	kIx~	=
marek	marka	k1gFnPc2	marka
;	;	kIx,	;
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
=	=	kIx~	=
jirka	jirka	k6eAd1	jirka
;	;	kIx,	;
</s>
</p>
<p>
<s>
no	no	k9	no
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vyhodnocování	vyhodnocování	k1gNnSc6	vyhodnocování
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
prohledává	prohledávat	k5eAaImIp3nS	prohledávat
databáze	databáze	k1gFnPc4	databáze
od	od	k7c2	od
shora	shora	k6eAd1	shora
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
na	na	k7c6	na
hladinách	hladina	k1gFnPc6	hladina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
existují	existovat	k5eAaImIp3nP	existovat
alternativy	alternativa	k1gFnPc1	alternativa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
splnění	splnění	k1gNnSc6	splnění
cílů	cíl	k1gInPc2	cíl
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
tzv.	tzv.	kA	tzv.
srovnání	srovnání	k1gNnSc1	srovnání
(	(	kIx(	(
<g/>
unifikace	unifikace	k1gFnSc1	unifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srovná	srovnat	k5eAaPmIp3nS	srovnat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
aktuální	aktuální	k2eAgInSc4d1	aktuální
cíl	cíl	k1gInSc4	cíl
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
či	či	k8xC	či
hlavou	hlava	k1gFnSc7	hlava
pravidla	pravidlo	k1gNnSc2	pravidlo
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
srovnání	srovnání	k1gNnSc2	srovnání
je	být	k5eAaImIp3nS	být
i	i	k9	i
případný	případný	k2eAgInSc1d1	případný
vznik	vznik	k1gInSc1	vznik
vazby	vazba	k1gFnSc2	vazba
proměnné	proměnná	k1gFnSc2	proměnná
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
Prologovský	Prologovský	k2eAgInSc4d1	Prologovský
program	program	k1gInSc4	program
použít	použít	k5eAaPmF	použít
tzv.	tzv.	kA	tzv.
navracení	navracení	k1gNnSc4	navracení
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
backtracking	backtracking	k1gInSc4	backtracking
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
vazeb	vazba	k1gFnPc2	vazba
proměnných	proměnná	k1gFnPc2	proměnná
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
hledání	hledání	k1gNnSc4	hledání
jiného	jiný	k2eAgNnSc2d1	jiné
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nalezení	nalezení	k1gNnSc6	nalezení
řešení	řešení	k1gNnSc2	řešení
může	moct	k5eAaImIp3nS	moct
navracení	navracení	k1gNnSc4	navracení
vyvolat	vyvolat	k5eAaPmF	vyvolat
i	i	k9	i
uživatel	uživatel	k1gMnSc1	uživatel
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
stisknutím	stisknutí	k1gNnSc7	stisknutí
klávesy	klávesa	k1gFnSc2	klávesa
středník	středník	k1gInSc1	středník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nejprve	nejprve	k6eAd1	nejprve
Prolog	prolog	k1gInSc1	prolog
pokusí	pokusit	k5eAaPmIp3nS	pokusit
porovnat	porovnat	k5eAaPmF	porovnat
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
<g/>
(	(	kIx(	(
<g/>
Kdo	kdo	k3yRnSc1	kdo
<g/>
)	)	kIx)	)
a	a	k8xC	a
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
<g/>
(	(	kIx(	(
<g/>
petr	petr	k1gMnSc1	petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proměnná	proměnná	k1gFnSc1	proměnná
Kdo	kdo	k3yQnSc1	kdo
byla	být	k5eAaImAgFnS	být
nespecifikována	specifikován	k2eNgNnPc1d1	nespecifikováno
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
atom	atom	k1gInSc4	atom
petr	petra	k1gFnPc2	petra
<g/>
.	.	kIx.	.
</s>
<s>
Cíl	cíl	k1gInSc1	cíl
je	být	k5eAaImIp3nS	být
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
,	,	kIx,	,
Prolog	prolog	k1gInSc1	prolog
vypíše	vypsat	k5eAaPmIp3nS	vypsat
první	první	k4xOgFnSc4	první
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
máme	mít	k5eAaImIp1nP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
vyhledání	vyhledání	k1gNnSc4	vyhledání
alternativního	alternativní	k2eAgNnSc2d1	alternativní
řešení	řešení	k1gNnSc2	řešení
<g/>
,	,	kIx,	,
požádáme	požádat	k5eAaPmIp1nP	požádat
Prolog	prolog	k1gInSc4	prolog
o	o	k7c4	o
znovusplnění	znovusplnění	k1gNnSc4	znovusplnění
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
použijeme	použít	k5eAaPmIp1nP	použít
klávesu	klávesa	k1gFnSc4	klávesa
středník	středník	k1gInSc4	středník
<g/>
.	.	kIx.	.
</s>
<s>
Prolog	prolog	k1gInSc1	prolog
zruší	zrušit	k5eAaPmIp3nS	zrušit
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
atom	atom	k1gInSc4	atom
petr	petra	k1gFnPc2	petra
<g/>
,	,	kIx,	,
označí	označit	k5eAaPmIp3nS	označit
si	se	k3xPyFc3	se
již	již	k9	již
splněný	splněný	k2eAgInSc4d1	splněný
fakt	fakt	k1gInSc4	fakt
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
další	další	k2eAgFnPc4d1	další
řešení	řešení	k1gNnPc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
databáze	databáze	k1gFnSc1	databáze
se	se	k3xPyFc4	se
prohledává	prohledávat	k5eAaImIp3nS	prohledávat
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
dalším	další	k2eAgNnSc7d1	další
porovnáním	porovnání	k1gNnSc7	porovnání
bude	být	k5eAaImBp3nS	být
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
<g/>
(	(	kIx(	(
<g/>
Kdo	kdo	k3yRnSc1	kdo
<g/>
)	)	kIx)	)
a	a	k8xC	a
muj_pritel	muj_pritel	k1gMnSc1	muj_pritel
<g/>
(	(	kIx(	(
<g/>
marek	marka	k1gFnPc2	marka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
nová	nový	k2eAgFnSc1d1	nová
vazba	vazba	k1gFnSc1	vazba
proměnné	proměnná	k1gFnSc2	proměnná
Kdo	kdo	k3yQnSc1	kdo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
atom	atom	k1gInSc4	atom
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Cíl	cíl	k1gInSc1	cíl
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
,	,	kIx,	,
vypíše	vypsat	k5eAaPmIp3nS	vypsat
se	se	k3xPyFc4	se
druhá	druhý	k4xOgFnSc1	druhý
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stisku	stisk	k1gInSc6	stisk
klávesy	klávesa	k1gFnSc2	klávesa
středník	středník	k1gInSc1	středník
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
vazby	vazba	k1gFnSc2	vazba
proměnné	proměnná	k1gFnSc2	proměnná
Kdo	kdo	k3yQnSc1	kdo
na	na	k7c4	na
atom	atom	k1gInSc4	atom
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
použitého	použitý	k2eAgInSc2d1	použitý
faktu	fakt	k1gInSc2	fakt
a	a	k8xC	a
hledání	hledání	k1gNnSc2	hledání
dalšího	další	k2eAgInSc2d1	další
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
další	další	k2eAgFnSc2d1	další
odpovědi	odpověď	k1gFnSc2	odpověď
pracuje	pracovat	k5eAaImIp3nS	pracovat
Prolog	prolog	k1gInSc1	prolog
s	s	k7c7	s
pravidlem	pravidlo	k1gNnSc7	pravidlo
a	a	k8xC	a
postupuje	postupovat	k5eAaImIp3nS	postupovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nespecifikované	specifikovaný	k2eNgFnSc3d1	nespecifikovaná
proměnné	proměnná	k1gFnSc3	proměnná
Kdo	kdo	k3yRnSc1	kdo
a	a	k8xC	a
Pritel	Pritel	k1gInSc4	Pritel
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
sdílenými	sdílený	k2eAgFnPc7d1	sdílená
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
obě	dva	k4xCgFnPc1	dva
proměnné	proměnná	k1gFnPc1	proměnná
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
jedinou	jediný	k2eAgFnSc4d1	jediná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
se	se	k3xPyFc4	se
první	první	k4xOgMnSc1	první
Markův	Markův	k2eAgMnSc1d1	Markův
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
případě	případ	k1gInSc6	případ
Lenka	Lenka	k1gFnSc1	Lenka
a	a	k8xC	a
na	na	k7c6	na
proměnné	proměnná	k1gFnSc6	proměnná
Kdo	kdo	k3yInSc1	kdo
i	i	k8xC	i
Pritel	Pritel	k1gInSc1	Pritel
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
hodnota	hodnota	k1gFnSc1	hodnota
lenka	lenka	k1gFnSc1	lenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
hledá	hledat	k5eAaImIp3nS	hledat
predikát	predikát	k1gInSc1	predikát
petruv_pritel	petruv_pritel	k1gMnSc1	petruv_pritel
<g/>
(	(	kIx(	(
<g/>
lenka	lenka	k1gFnSc1	lenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
hledání	hledání	k1gNnSc1	hledání
je	být	k5eAaImIp3nS	být
však	však	k9	však
neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
navrácení	navrácení	k1gNnSc1	navrácení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
k	k	k7c3	k
predikátu	predikát	k1gInSc3	predikát
markuv_pritel	markuv_pritel	k1gMnSc1	markuv_pritel
<g/>
(	(	kIx(	(
<g/>
Pritel	Pritel	k1gMnSc1	Pritel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zruší	zrušit	k5eAaPmIp3nS	zrušit
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
proměnnou	proměnná	k1gFnSc4	proměnná
lenka	lenka	k1gFnSc2	lenka
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
jinou	jiný	k2eAgFnSc4d1	jiná
vazbu	vazba	k1gFnSc4	vazba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
případě	případ	k1gInSc6	případ
bude	být	k5eAaImBp3nS	být
atom	atom	k1gInSc1	atom
jirka	jirek	k1gInSc2	jirek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
tedy	tedy	k9	tedy
program	program	k1gInSc1	program
hledá	hledat	k5eAaImIp3nS	hledat
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
predikát	predikát	k1gInSc4	predikát
petruv_pritel	petruv_pritel	k1gMnSc1	petruv_pritel
<g/>
(	(	kIx(	(
<g/>
jirka	jirka	k1gMnSc1	jirka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
predikát	predikát	k1gInSc1	predikát
najde	najít	k5eAaPmIp3nS	najít
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
splní	splnit	k5eAaPmIp3nS	splnit
cíl	cíl	k1gInSc1	cíl
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vypsána	vypsán	k2eAgFnSc1d1	vypsána
odpověď	odpověď	k1gFnSc1	odpověď
Jirka	Jirka	k1gFnSc1	Jirka
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgNnSc1d1	další
hledání	hledání	k1gNnSc1	hledání
probíhá	probíhat	k5eAaImIp3nS	probíhat
podobně	podobně	k6eAd1	podobně
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc4d1	poslední
odpověď	odpověď	k1gFnSc4	odpověď
no	no	k9	no
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
napsaného	napsaný	k2eAgNnSc2d1	napsané
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prolog	prolog	k1gInSc1	prolog
prohledává	prohledávat	k5eAaImIp3nS	prohledávat
strom	strom	k1gInSc4	strom
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
derivační	derivační	k2eAgInSc1d1	derivační
strom	strom	k1gInSc1	strom
<g/>
)	)	kIx)	)
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
a	a	k8xC	a
na	na	k7c6	na
hladinách	hladina	k1gFnPc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
existují	existovat	k5eAaImIp3nP	existovat
alternativy	alternativa	k1gFnPc1	alternativa
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strategii	strategie	k1gFnSc4	strategie
vyhodnocování	vyhodnocování	k1gNnSc2	vyhodnocování
můžeme	moct	k5eAaImIp1nP	moct
pozměnit	pozměnit	k5eAaPmF	pozměnit
pomocí	pomocí	k7c2	pomocí
některých	některý	k3yIgInPc2	některý
vestavěných	vestavěný	k2eAgInPc2d1	vestavěný
predikátů	predikát	k1gInPc2	predikát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Selhání	selhání	k1gNnSc1	selhání
-	-	kIx~	-
fail	fail	k1gInSc1	fail
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
predikát	predikát	k1gInSc4	predikát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
vždy	vždy	k6eAd1	vždy
nutí	nutit	k5eAaImIp3nP	nutit
program	program	k1gInSc4	program
se	se	k3xPyFc4	se
vracet	vracet	k5eAaImF	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Predikát	predikát	k1gInSc1	predikát
fail	fail	k1gInSc1	fail
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
například	například	k6eAd1	například
v	v	k7c6	v
</s>
</p>
<p>
<s>
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
najít	najít	k5eAaPmF	najít
všechna	všechen	k3xTgNnPc4	všechen
řešení	řešení	k1gNnPc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
muz	muz	k?	muz
<g/>
(	(	kIx(	(
<g/>
pavel	pavel	k1gMnSc1	pavel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
muz	muz	k?	muz
<g/>
(	(	kIx(	(
<g/>
petr	petr	k1gMnSc1	petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zena	zena	k1gMnSc1	zena
<g/>
(	(	kIx(	(
<g/>
petra	petra	k1gMnSc1	petra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vypis	vypis	k1gFnSc1	vypis
<g/>
:	:	kIx,	:
<g/>
-muz	uz	k1gMnSc1	-muz
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
write	write	k5eAaPmIp2nP	write
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
nl	nl	k?	nl
<g/>
,	,	kIx,	,
<g/>
fail	fail	k1gInSc1	fail
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
vypis	vypis	k1gInSc1	vypis
vypíše	vypsat	k5eAaPmIp3nS	vypsat
všechny	všechen	k3xTgMnPc4	všechen
muže	muž	k1gMnPc4	muž
z	z	k7c2	z
databáze	databáze	k1gFnSc2	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
nepoužili	použít	k5eNaPmAgMnP	použít
predikát	predikát	k1gInSc4	predikát
fail	faila	k1gFnPc2	faila
<g/>
,	,	kIx,	,
vypsalo	vypsat	k5eAaPmAgNnS	vypsat
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
první	první	k4xOgNnSc4	první
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xC	jako
konstruktivní	konstruktivní	k2eAgNnSc1d1	konstruktivní
selhání	selhání	k1gNnSc1	selhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řez	řez	k1gInSc4	řez
-	-	kIx~	-
!	!	kIx.	!
</s>
<s>
===	===	k?	===
</s>
</p>
<p>
<s>
Řez	řez	k1gInSc1	řez
je	být	k5eAaImIp3nS	být
vestavěný	vestavěný	k2eAgInSc1d1	vestavěný
predikát	predikát	k1gInSc1	predikát
<g/>
,	,	kIx,	,
značený	značený	k2eAgInSc1d1	značený
vykřičníkem	vykřičník	k1gInSc7	vykřičník
'	'	kIx"	'
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
'	'	kIx"	'
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
zakázat	zakázat	k5eAaPmF	zakázat
programu	program	k1gInSc2	program
či	či	k8xC	či
uživateli	uživatel	k1gMnSc3	uživatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
používal	používat	k5eAaImAgInS	používat
navracení	navracení	k1gNnSc4	navracení
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
predikátu	predikát	k1gInSc2	predikát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
mít	mít	k5eAaImF	mít
nanejvýše	nanejvýše	k6eAd1	nanejvýše
jedno	jeden	k4xCgNnSc4	jeden
řešení	řešení	k1gNnSc4	řešení
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
determinovaný	determinovaný	k2eAgInSc1d1	determinovaný
predikát	predikát	k1gInSc1	predikát
<g/>
.	.	kIx.	.
</s>
<s>
Řez	řez	k1gInSc1	řez
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
jednosměrku	jednosměrka	k1gFnSc4	jednosměrka
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
predikát	predikát	k1gInSc4	predikát
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
najít	najít	k5eAaPmF	najít
řešení	řešení	k1gNnSc4	řešení
uvnitř	uvnitř	k7c2	uvnitř
řezu	řez	k1gInSc2	řez
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
predikát	predikát	k1gInSc1	predikát
opuštěn	opuštěn	k2eAgInSc1d1	opuštěn
jako	jako	k8xS	jako
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
portu	port	k1gInSc2	port
fail	faila	k1gFnPc2	faila
<g/>
)	)	kIx)	)
a	a	k8xC	a
řez	řez	k1gInSc4	řez
je	být	k5eAaImIp3nS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Řez	řez	k1gInSc1	řez
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
kvůli	kvůli	k7c3	kvůli
funkčnosti	funkčnost	k1gFnSc3	funkčnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
efektivnosti	efektivnost	k1gFnSc2	efektivnost
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
druhy	druh	k1gInPc4	druh
řezu	řez	k1gInSc2	řez
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
řez	řez	k1gInSc1	řez
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	Zelený	k1gMnSc1	Zelený
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
efektivnost	efektivnost	k1gFnSc4	efektivnost
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
bez	bez	k7c2	bez
červeného	červený	k2eAgInSc2d1	červený
by	by	kYmCp3nP	by
program	program	k1gInSc1	program
správně	správně	k6eAd1	správně
nefungoval	fungovat	k5eNaImAgInS	fungovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
použití	použití	k1gNnSc2	použití
červeného	červený	k2eAgInSc2d1	červený
řezu	řez	k1gInSc2	řez
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
max	max	kA	max
<g/>
1	[number]	k4	1
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
-X	-X	k?	-X
<g/>
>	>	kIx)	>
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
</p>
<p>
<s>
max	max	kA	max
<g/>
1	[number]	k4	1
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
použití	použití	k1gNnSc2	použití
zeleného	zelený	k2eAgInSc2d1	zelený
řezu	řez	k1gInSc2	řez
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
max	max	kA	max
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
-X	-X	k?	-X
<g/>
>	>	kIx)	>
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
</p>
<p>
<s>
max	max	kA	max
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
-Y	-Y	k?	-Y
<g/>
>	>	kIx)	>
<g/>
=	=	kIx~	=
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc1	dva
predikáty	predikát	k1gInPc1	predikát
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
1	[number]	k4	1
i	i	k8xC	i
max	max	kA	max
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vyberou	vybrat	k5eAaPmIp3nP	vybrat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
čísel	číslo	k1gNnPc2	číslo
maximum	maximum	k1gNnSc4	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Predikát	predikát	k1gInSc1	predikát
max	max	kA	max
<g/>
2	[number]	k4	2
by	by	kYmCp3nS	by
sice	sice	k8xC	sice
fungoval	fungovat	k5eAaImAgInS	fungovat
i	i	k9	i
bez	bez	k7c2	bez
řezu	řez	k1gInSc2	řez
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
maximem	maxim	k1gInSc7	maxim
X	X	kA	X
<g/>
,	,	kIx,	,
dovolil	dovolit	k5eAaPmAgInS	dovolit
by	by	kYmCp3nS	by
uživateli	uživatel	k1gMnSc3	uživatel
pomocí	pomocí	k7c2	pomocí
středníku	středník	k1gInSc2	středník
návrat	návrat	k1gInSc1	návrat
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
jiného	jiný	k2eAgNnSc2d1	jiné
řešení	řešení	k1gNnSc2	řešení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jistě	jistě	k9	jistě
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zelený	zelený	k2eAgInSc4d1	zelený
řez	řez	k1gInSc4	řez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
predikátu	predikát	k1gInSc2	predikát
max	max	kA	max
<g/>
1	[number]	k4	1
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
nastat	nastat	k5eAaPmF	nastat
podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
větší	veliký	k2eAgNnSc1d2	veliký
číslo	číslo	k1gNnSc1	číslo
bylo	být	k5eAaImAgNnS	být
X	X	kA	X
<g/>
,	,	kIx,	,
dovolil	dovolit	k5eAaPmAgInS	dovolit
by	by	kYmCp3nS	by
uživateli	uživatel	k1gMnSc3	uživatel
návrat	návrat	k1gInSc1	návrat
a	a	k8xC	a
zde	zde	k6eAd1	zde
by	by	kYmCp3nS	by
za	za	k7c4	za
maximum	maximum	k1gNnSc4	maximum
počítač	počítač	k1gInSc4	počítač
označil	označit	k5eAaPmAgMnS	označit
chybně	chybně	k6eAd1	chybně
i	i	k9	i
Y.	Y.	kA	Y.
Predikát	predikát	k1gInSc1	predikát
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
nefungoval	fungovat	k5eNaImAgMnS	fungovat
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
řez	řez	k1gInSc4	řez
červený	červený	k2eAgInSc4d1	červený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řez	řez	k1gInSc1	řez
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
predikátem	predikát	k1gInSc7	predikát
selhání	selhání	k1gNnSc1	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
řez-selhání	řezelhání	k1gNnSc1	řez-selhání
(	(	kIx(	(
<g/>
cut-fail	cutail	k1gInSc1	cut-fail
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
fakta	faktum	k1gNnSc2	faktum
muz	muz	k?	muz
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
vytvořit	vytvořit	k5eAaPmF	vytvořit
predikát	predikát	k1gInSc4	predikát
zena	zen	k1gInSc2	zen
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zena	zena	k1gMnSc1	zena
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
muz	muz	k?	muz
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
fail	failit	k5eAaPmRp2nS	failit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zena	zena	k1gMnSc1	zena
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
X	X	kA	X
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
ženou	žena	k1gFnSc7	žena
–	–	k?	–
řez	řez	k1gInSc4	řez
zabrání	zabránit	k5eAaPmIp3nS	zabránit
hledání	hledání	k1gNnSc1	hledání
alternativ	alternativa	k1gFnPc2	alternativa
pro	pro	k7c4	pro
zena	zenus	k1gMnSc4	zenus
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
selhání	selhání	k1gNnSc1	selhání
určí	určit	k5eAaPmIp3nS	určit
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konvence	konvence	k1gFnPc4	konvence
a	a	k8xC	a
doporučení	doporučení	k1gNnPc4	doporučení
==	==	k?	==
</s>
</p>
<p>
<s>
Obecné	obecný	k2eAgNnSc1d1	obecné
doporučení	doporučení	k1gNnSc1	doporučení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
používat	používat	k5eAaImF	používat
mnemotechnické	mnemotechnický	k2eAgInPc4d1	mnemotechnický
identifikátory	identifikátor	k1gInPc4	identifikátor
</s>
</p>
<p>
<s>
nezapomínat	zapomínat	k5eNaImF	zapomínat
na	na	k7c4	na
dekompozici	dekompozice	k1gFnSc4	dekompozice
-	-	kIx~	-
složité	složitý	k2eAgFnPc1d1	složitá
konstrukce	konstrukce	k1gFnPc1	konstrukce
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
více	hodně	k6eAd2	hodně
jednodušších	jednoduchý	k2eAgFnPc6d2	jednodušší
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nelze	lze	k6eNd1	lze
vyhnout	vyhnout	k5eAaPmF	vyhnout
vedlejším	vedlejší	k2eAgInPc3d1	vedlejší
efektům	efekt	k1gInPc3	efekt
u	u	k7c2	u
predikátů	predikát	k1gInPc2	predikát
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
důkladně	důkladně	k6eAd1	důkladně
okomentovat	okomentovat	k5eAaPmF	okomentovat
</s>
</p>
<p>
<s>
dávat	dávat	k5eAaImF	dávat
přednost	přednost	k1gFnSc4	přednost
negaci	negace	k1gFnSc3	negace
před	před	k7c7	před
řezem	řez	k1gInSc7	řez
</s>
</p>
<p>
<s>
rozepsání	rozepsání	k1gNnSc1	rozepsání
pravidla	pravidlo	k1gNnSc2	pravidlo
na	na	k7c6	na
více	hodně	k6eAd2	hodně
řádků	řádek	k1gInPc2	řádek
je	být	k5eAaImIp3nS	být
čitelnější	čitelný	k2eAgFnSc1d2	čitelnější
nežli	nežli	k8xS	nežli
logická	logický	k2eAgFnSc1d1	logická
spojka	spojka	k1gFnSc1	spojka
(	(	kIx(	(
<g/>
;	;	kIx,	;
<g/>
)	)	kIx)	)
<g/>
Prolog	prolog	k1gInSc1	prolog
používá	používat	k5eAaImIp3nS	používat
obvykle	obvykle	k6eAd1	obvykle
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc1	typ
komentářů	komentář	k1gInPc2	komentář
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
*	*	kIx~	*
komentář	komentář	k1gInSc4	komentář
*	*	kIx~	*
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
%	%	kIx~	%
komentář	komentář	k1gInSc4	komentář
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
řádkuPři	řádkuPř	k1gFnSc3	řádkuPř
komentování	komentování	k1gNnSc1	komentování
predikátu	predikát	k1gInSc2	predikát
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
)	)	kIx)	)
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
které	který	k3yIgInPc1	který
argumenty	argument	k1gInPc1	argument
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
konvence	konvence	k1gFnSc1	konvence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
argument	argument	k1gInSc4	argument
umístěním	umístění	k1gNnSc7	umístění
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
+	+	kIx~	+
-	-	kIx~	-
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
před	před	k7c4	před
argument	argument	k1gInSc4	argument
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
-	-	kIx~	-
"	"	kIx"	"
<g/>
výstupní	výstupní	k2eAgInSc1d1	výstupní
argument	argument	k1gInSc1	argument
<g/>
"	"	kIx"	"
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
komentáře	komentář	k1gInSc2	komentář
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dotazech	dotaz	k1gInPc6	dotaz
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
místě	místo	k1gNnSc6	místo
stát	stát	k5eAaPmF	stát
nekonkretizovaná	konkretizovaný	k2eNgFnSc1d1	nekonkretizovaná
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
+	+	kIx~	+
"	"	kIx"	"
<g/>
vstupní	vstupní	k2eAgMnSc1d1	vstupní
<g/>
"	"	kIx"	"
parametr	parametr	k1gInSc1	parametr
–	–	k?	–
autor	autor	k1gMnSc1	autor
komentáře	komentář	k1gInSc2	komentář
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dotazech	dotaz	k1gInPc6	dotaz
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
místě	místo	k1gNnSc6	místo
stát	stát	k5eAaImF	stát
již	již	k6eAd1	již
konkretizovaný	konkretizovaný	k2eAgInSc4d1	konkretizovaný
term	term	k1gInSc4	term
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
argument	argument	k1gInSc1	argument
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
vstupní	vstupní	k2eAgMnSc1d1	vstupní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
výstupní	výstupní	k2eAgFnSc4d1	výstupní
tj.	tj.	kA	tj.
v	v	k7c6	v
dotazech	dotaz	k1gInPc6	dotaz
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
místě	místo	k1gNnSc6	místo
stát	stát	k5eAaImF	stát
"	"	kIx"	"
<g/>
cokoli	cokoli	k3yInSc4	cokoli
<g/>
"	"	kIx"	"
.	.	kIx.	.
<g/>
Příklad	příklad	k1gInSc1	příklad
vzorově	vzorově	k6eAd1	vzorově
okomentovaného	okomentovaný	k2eAgInSc2d1	okomentovaný
predikátu	predikát	k1gInSc2	predikát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
%	%	kIx~	%
predek	predka	k1gFnPc2	predka
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
Pred	Pred	k1gMnSc1	Pred
<g/>
,	,	kIx,	,
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
Pot	pot	k1gInSc1	pot
<g/>
)	)	kIx)	)
Pred	Pred	k1gMnSc1	Pred
je	být	k5eAaImIp3nS	být
předkem	předek	k1gMnSc7	předek
potomka	potomek	k1gMnSc2	potomek
Pot	pot	k1gInSc4	pot
</s>
</p>
<p>
<s>
predek	predek	k1gInSc1	predek
<g/>
(	(	kIx(	(
<g/>
Rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
<g/>
Pot	pot	k1gInSc1	pot
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
-	-	kIx~	-
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
Rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
<g/>
Pot	pot	k1gInSc1	pot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
%	%	kIx~	%
Rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
rodičem	rodič	k1gMnSc7	rodič
potomka	potomek	k1gMnSc2	potomek
Pot	pot	k1gInSc4	pot
</s>
</p>
<p>
<s>
predek	predek	k1gInSc1	predek
<g/>
(	(	kIx(	(
<g/>
Pred	Pred	k1gInSc1	Pred
<g/>
,	,	kIx,	,
<g/>
Pot	pot	k1gInSc1	pot
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
-	-	kIx~	-
</s>
</p>
<p>
<s>
rodic	rodic	k?	rodic
<g/>
(	(	kIx(	(
<g/>
Pred	Pred	k1gMnSc1	Pred
<g/>
,	,	kIx,	,
<g/>
MlPred	MlPred	k1gMnSc1	MlPred
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
%	%	kIx~	%
Pred	Pred	k1gMnSc1	Pred
je	být	k5eAaImIp3nS	být
rodičem	rodič	k1gMnSc7	rodič
mladšího	mladý	k2eAgNnSc2d2	mladší
</s>
</p>
<p>
<s>
predek	predek	k1gInSc1	predek
<g/>
(	(	kIx(	(
<g/>
MlPred	MlPred	k1gInSc1	MlPred
<g/>
,	,	kIx,	,
<g/>
Pot	pot	k1gInSc1	pot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
%	%	kIx~	%
předka	předek	k1gMnSc2	předek
Mlpred	Mlpred	k1gMnSc1	Mlpred
potomka	potomek	k1gMnSc2	potomek
Pot	pot	k1gInSc4	pot
</s>
</p>
<p>
<s>
==	==	k?	==
Implementace	implementace	k1gFnSc2	implementace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
ISO	ISO	kA	ISO
Prolog	prolog	k1gInSc1	prolog
===	===	k?	===
</s>
</p>
<p>
<s>
ISO	ISO	kA	ISO
Prolog	prolog	k1gInSc1	prolog
standard	standard	k1gInSc1	standard
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
13211	[number]	k4	13211
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
standardizovat	standardizovat	k5eAaBmF	standardizovat
existující	existující	k2eAgFnPc1d1	existující
praktiky	praktika	k1gFnPc1	praktika
mnoha	mnoho	k4c2	mnoho
implementací	implementace	k1gFnPc2	implementace
základních	základní	k2eAgInPc2d1	základní
prvků	prvek	k1gInPc2	prvek
Prologu	prolog	k1gInSc2	prolog
<g/>
.	.	kIx.	.
</s>
<s>
Ujasnil	ujasnit	k5eAaPmAgInS	ujasnit
aspekty	aspekt	k1gInPc4	aspekt
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
nejednoznačné	jednoznačný	k2eNgInPc1d1	nejednoznačný
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
přenositelným	přenositelný	k2eAgInPc3d1	přenositelný
programům	program	k1gInPc3	program
<g/>
.	.	kIx.	.
</s>
<s>
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
13211	[number]	k4	13211
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
standardu	standard	k1gInSc2	standard
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
moduly	modul	k1gInPc4	modul
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
je	být	k5eAaImIp3nS	být
udržován	udržovat	k5eAaImNgInS	udržovat
skupinou	skupina	k1gFnSc7	skupina
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
JTC	JTC	kA	JTC
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
SC	SC	kA	SC
<g/>
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
WG	WG	kA	WG
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
ANSI	ANSI	kA	ANSI
X3J17	X3J17	k1gMnSc4	X3J17
je	být	k5eAaImIp3nS	být
US	US	kA	US
Technical	Technical	k1gFnSc1	Technical
Advisory	Advisor	k1gMnPc7	Advisor
Group	Group	k1gInSc4	Group
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
efektivnosti	efektivnost	k1gFnSc3	efektivnost
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc1	kód
Prologu	prolog	k1gInSc2	prolog
obvykle	obvykle	k6eAd1	obvykle
kompilován	kompilovat	k5eAaImNgInS	kompilovat
do	do	k7c2	do
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ovlivněného	ovlivněný	k2eAgInSc2d1	ovlivněný
register-based	registerased	k1gInSc1	register-based
sadou	sada	k1gFnSc7	sada
instrukcí	instrukce	k1gFnPc2	instrukce
Warren	Warrna	k1gFnPc2	Warrna
Abstract	Abstract	k1gInSc1	Abstract
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
WAM	WAM	kA	WAM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
implementace	implementace	k1gFnPc1	implementace
používají	používat	k5eAaImIp3nP	používat
abstraktní	abstraktní	k2eAgFnSc4d1	abstraktní
interpretaci	interpretace	k1gFnSc4	interpretace
k	k	k7c3	k
odvození	odvození	k1gNnSc3	odvození
informace	informace	k1gFnSc2	informace
o	o	k7c4	o
typu	typa	k1gFnSc4	typa
a	a	k8xC	a
módu	móda	k1gFnSc4	móda
predikátů	predikát	k1gInPc2	predikát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kompilace	kompilace	k1gFnSc2	kompilace
nebo	nebo	k8xC	nebo
kompilují	kompilovat	k5eAaImIp3nP	kompilovat
do	do	k7c2	do
skutečného	skutečný	k2eAgInSc2d1	skutečný
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
pro	pro	k7c4	pro
vysokou	vysoký	k2eAgFnSc4d1	vysoká
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
efektivních	efektivní	k2eAgFnPc2d1	efektivní
metod	metoda	k1gFnPc2	metoda
implementace	implementace	k1gFnSc2	implementace
kódu	kód	k1gInSc2	kód
Prologu	prolog	k1gInSc2	prolog
je	být	k5eAaImIp3nS	být
oblastí	oblast	k1gFnSc7	oblast
aktivního	aktivní	k2eAgInSc2d1	aktivní
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
logického	logický	k2eAgNnSc2d1	logické
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
implementacích	implementace	k1gFnPc6	implementace
jsou	být	k5eAaImIp3nP	být
užity	užít	k5eAaPmNgFnP	užít
různé	různý	k2eAgFnPc1d1	různá
jiné	jiný	k2eAgFnPc1d1	jiná
metody	metoda	k1gFnPc1	metoda
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
některé	některý	k3yIgFnPc4	některý
patří	patřit	k5eAaImIp3nS	patřit
binarizace	binarizace	k1gFnSc1	binarizace
klauzulí	klauzule	k1gFnPc2	klauzule
a	a	k8xC	a
zásobníkově	zásobníkově	k6eAd1	zásobníkově
založené	založený	k2eAgInPc4d1	založený
virtuální	virtuální	k2eAgInPc4d1	virtuální
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koncová	koncový	k2eAgFnSc1d1	koncová
rekurze	rekurze	k1gFnSc1	rekurze
===	===	k?	===
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
obyčejně	obyčejně	k6eAd1	obyčejně
implementuje	implementovat	k5eAaImIp3nS	implementovat
známou	známý	k2eAgFnSc4d1	známá
optimalizační	optimalizační	k2eAgFnSc4d1	optimalizační
metodu	metoda	k1gFnSc4	metoda
zvanou	zvaný	k2eAgFnSc4d1	zvaná
optimalizace	optimalizace	k1gFnSc1	optimalizace
koncového	koncový	k2eAgNnSc2d1	koncové
volání	volání	k1gNnSc2	volání
(	(	kIx(	(
<g/>
tail	tail	k1gInSc1	tail
call	call	k1gMnSc1	call
optimization	optimization	k1gInSc1	optimization
(	(	kIx(	(
<g/>
TCO	TCO	kA	TCO
<g/>
))	))	k?	))
pro	pro	k7c4	pro
deterministické	deterministický	k2eAgNnSc4d1	deterministické
predikáty	predikát	k1gInPc1	predikát
vykazující	vykazující	k2eAgFnSc4d1	vykazující
koncovou	koncový	k2eAgFnSc4d1	koncová
rekurzi	rekurze	k1gFnSc4	rekurze
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
obecněji	obecně	k6eAd2	obecně
<g/>
,	,	kIx,	,
koncové	koncový	k2eAgNnSc1d1	koncové
volání	volání	k1gNnSc1	volání
<g/>
:	:	kIx,	:
Rámec	rámec	k1gInSc4	rámec
zásobníku	zásobník	k1gInSc2	zásobník
patřící	patřící	k2eAgFnSc3d1	patřící
klauzuli	klauzule	k1gFnSc3	klauzule
je	být	k5eAaImIp3nS	být
zahozen	zahozen	k2eAgInSc1d1	zahozen
před	před	k7c7	před
vykonáním	vykonání	k1gNnSc7	vykonání
volání	volání	k1gNnSc2	volání
v	v	k7c6	v
koncové	koncový	k2eAgFnSc6d1	koncová
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
deterministické	deterministický	k2eAgInPc1d1	deterministický
koncově-rekurzivní	koncověekurzivní	k2eAgInPc1d1	koncově-rekurzivní
predikáty	predikát	k1gInPc1	predikát
vykonány	vykonán	k2eAgInPc1d1	vykonán
s	s	k7c7	s
konstantním	konstantní	k2eAgInSc7d1	konstantní
zásobníkovým	zásobníkový	k2eAgInSc7d1	zásobníkový
prostorem	prostor	k1gInSc7	prostor
jako	jako	k9	jako
smyčky	smyčka	k1gFnPc1	smyčka
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Indexace	indexace	k1gFnSc1	indexace
termů	term	k1gInPc2	term
===	===	k?	===
</s>
</p>
<p>
<s>
Nalezení	nalezení	k1gNnSc1	nalezení
klauzulí	klauzule	k1gFnPc2	klauzule
unifikovatelných	unifikovatelný	k2eAgFnPc2d1	unifikovatelný
s	s	k7c7	s
termem	term	k1gInSc7	term
v	v	k7c6	v
dotazu	dotaz	k1gInSc6	dotaz
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
lineární	lineární	k2eAgInSc1d1	lineární
<g/>
.	.	kIx.	.
</s>
<s>
Indexace	indexace	k1gFnSc1	indexace
termů	term	k1gInPc2	term
používá	používat	k5eAaImIp3nS	používat
datovou	datový	k2eAgFnSc4d1	datová
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sublineární	sublineární	k2eAgInSc4d1	sublineární
čas	čas	k1gInSc4	čas
vyhledání	vyhledání	k1gNnSc2	vyhledání
<g/>
.	.	kIx.	.
</s>
<s>
Indexace	indexace	k1gFnSc1	indexace
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
pouze	pouze	k6eAd1	pouze
výkon	výkon	k1gInSc4	výkon
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
sémantiku	sémantika	k1gFnSc4	sémantika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tabling	Tabling	k1gInSc4	Tabling
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
Prologové	Prologový	k2eAgInPc1d1	Prologový
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
BProlog	BProlog	k1gMnSc1	BProlog
<g/>
,	,	kIx,	,
XSB	XSB	kA	XSB
a	a	k8xC	a
Yap	Yap	k1gFnSc2	Yap
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
implementují	implementovat	k5eAaImIp3nP	implementovat
memorizační	memorizační	k2eAgFnSc4d1	memorizační
metodu	metoda	k1gFnSc4	metoda
zvanou	zvaný	k2eAgFnSc7d1	zvaná
tabling	tabling	k1gInSc4	tabling
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
osvobozuje	osvobozovat	k5eAaImIp3nS	osvobozovat
uživatele	uživatel	k1gMnSc4	uživatel
od	od	k7c2	od
manuálního	manuální	k2eAgNnSc2d1	manuální
přechovávání	přechovávání	k1gNnSc2	přechovávání
mezivýsledků	mezivýsledek	k1gInPc2	mezivýsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Implementace	implementace	k1gFnSc2	implementace
v	v	k7c4	v
hardware	hardware	k1gInSc4	hardware
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
projektu	projekt	k1gInSc2	projekt
Páté	pátá	k1gFnSc2	pátá
Generace	generace	k1gFnSc2	generace
Počítačových	počítačový	k2eAgInPc2d1	počítačový
Systémů	systém	k1gInPc2	systém
probíhaly	probíhat	k5eAaImAgInP	probíhat
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
implementaci	implementace	k1gFnSc4	implementace
Prologu	prolog	k1gInSc2	prolog
do	do	k7c2	do
hardwaru	hardware	k1gInSc2	hardware
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosažení	dosažení	k1gNnSc2	dosažení
rychlejšího	rychlý	k2eAgInSc2d2	rychlejší
výkonu	výkon	k1gInSc2	výkon
s	s	k7c7	s
dedikovanými	dedikovaný	k2eAgFnPc7d1	dedikovaná
architekturami	architektura	k1gFnPc7	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
Prolog	prolog	k1gInSc4	prolog
mnoho	mnoho	k4c1	mnoho
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
zrychlení	zrychlení	k1gNnSc4	zrychlení
skrze	skrze	k?	skrze
paralelní	paralelní	k2eAgNnSc4d1	paralelní
provádění	provádění	k1gNnSc4	provádění
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInSc1d2	novější
přístup	přístup	k1gInSc1	přístup
spočíval	spočívat	k5eAaImAgInS	spočívat
ve	v	k7c6	v
zkompilování	zkompilování	k1gNnSc6	zkompilování
vyhrazených	vyhrazený	k2eAgInPc2d1	vyhrazený
programů	program	k1gInPc2	program
Prologu	prolog	k1gInSc2	prolog
na	na	k7c4	na
programovatelné	programovatelný	k2eAgNnSc4d1	programovatelné
hradlové	hradlový	k2eAgNnSc4d1	hradlové
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc4d1	rychlý
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hardwaru	hardware	k1gInSc2	hardware
pro	pro	k7c4	pro
všeobecné	všeobecný	k2eAgInPc4d1	všeobecný
účely	účel	k1gInPc4	účel
neustále	neustále	k6eAd1	neustále
předstihuje	předstihovat	k5eAaImIp3nS	předstihovat
více	hodně	k6eAd2	hodně
specializované	specializovaný	k2eAgFnSc2d1	specializovaná
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Prologu	prolog	k1gInSc2	prolog
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
různé	různý	k2eAgFnPc1d1	různá
implementace	implementace	k1gFnPc1	implementace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
možnosti	možnost	k1gFnPc4	možnost
logického	logický	k2eAgNnSc2d1	logické
programování	programování	k1gNnSc2	programování
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nP	patřit
typy	typ	k1gInPc1	typ
<g/>
,	,	kIx,	,
módy	mód	k1gInPc1	mód
<g/>
,	,	kIx,	,
CLP	CLP	kA	CLP
<g/>
,	,	kIx,	,
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc4d1	orientované
logické	logický	k2eAgNnSc4d1	logické
programování	programování	k1gNnSc4	programování
(	(	kIx(	(
<g/>
OOLP	OOLP	kA	OOLP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souběžnost	souběžnost	k1gFnSc1	souběžnost
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgFnSc1d1	lineární
logika	logika	k1gFnSc1	logika
(	(	kIx(	(
<g/>
LLP	LLP	kA	LLP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkcionální	funkcionální	k2eAgNnSc4d1	funkcionální
programování	programování	k1gNnSc4	programování
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
programování	programování	k1gNnSc2	programování
logiky	logika	k1gFnSc2	logika
vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
interoperabilita	interoperabilita	k1gFnSc1	interoperabilita
se	s	k7c7	s
znalostní	znalostní	k2eAgFnSc7d1	znalostní
bází	báze	k1gFnSc7	báze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Typy	typ	k1gInPc1	typ
===	===	k?	===
</s>
</p>
<p>
<s>
Prolog	prolog	k1gInSc1	prolog
je	být	k5eAaImIp3nS	být
netypový	typový	k2eNgInSc1d1	netypový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
přidání	přidání	k1gNnSc6	přidání
typů	typ	k1gInPc2	typ
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
pokusy	pokus	k1gInPc1	pokus
rozšířit	rozšířit	k5eAaPmF	rozšířit
Prolog	prolog	k1gInSc4	prolog
o	o	k7c4	o
typy	typ	k1gInPc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
typu	typ	k1gInSc6	typ
je	být	k5eAaImIp3nS	být
užitečná	užitečný	k2eAgFnSc1d1	užitečná
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
typovou	typový	k2eAgFnSc4d1	typová
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
při	při	k7c6	při
úvaze	úvaha	k1gFnSc6	úvaha
nad	nad	k7c7	nad
programem	program	k1gInSc7	program
Prologu	prolog	k1gInSc2	prolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Módy	mód	k1gInPc4	mód
===	===	k?	===
</s>
</p>
<p>
<s>
Syntaxe	syntaxe	k1gFnSc1	syntaxe
Prologu	prolog	k1gInSc2	prolog
nespecifikuje	specifikovat	k5eNaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
argumenty	argument	k1gInPc4	argument
predikátu	predikát	k1gInSc2	predikát
jsou	být	k5eAaImIp3nP	být
vstupy	vstup	k1gInPc7	vstup
a	a	k8xC	a
které	který	k3yRgInPc4	který
výstupy	výstup	k1gInPc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doporučeno	doporučen	k2eAgNnSc1d1	doporučeno
mít	mít	k5eAaImF	mít
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
mít	mít	k5eAaImF	mít
uvedenu	uveden	k2eAgFnSc4d1	uvedena
v	v	k7c6	v
komentářích	komentář	k1gInPc6	komentář
<g/>
.	.	kIx.	.
</s>
<s>
Módy	móda	k1gFnPc1	móda
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
cenné	cenný	k2eAgFnPc1d1	cenná
informace	informace	k1gFnPc1	informace
při	při	k7c6	při
uvažování	uvažování	k1gNnSc6	uvažování
o	o	k7c6	o
programech	program	k1gInPc6	program
prologu	prolog	k1gInSc2	prolog
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
provedení	provedení	k1gNnSc2	provedení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Omezující	omezující	k2eAgFnPc4d1	omezující
podmínky	podmínka	k1gFnPc4	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Logické	logický	k2eAgNnSc4d1	logické
programování	programování	k1gNnSc4	programování
s	s	k7c7	s
omezujícími	omezující	k2eAgFnPc7d1	omezující
podmínkami	podmínka	k1gFnPc7	podmínka
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
Prolog	prolog	k1gInSc1	prolog
o	o	k7c4	o
koncepty	koncept	k1gInPc4	koncept
z	z	k7c2	z
constraint	constrainta	k1gFnPc2	constrainta
satisfaction	satisfaction	k1gInSc1	satisfaction
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
program	program	k1gInSc1	program
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
omezení	omezení	k1gNnSc4	omezení
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
klauzulí	klauzule	k1gFnPc2	klauzule
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
:	:	kIx,	:
<g/>
-	-	kIx~	-
X	X	kA	X
<g/>
+	+	kIx~	+
<g/>
Y	Y	kA	Y
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
problémy	problém	k1gInPc4	problém
kombinatorické	kombinatorický	k2eAgFnSc2d1	kombinatorická
optimalizace	optimalizace	k1gFnSc2	optimalizace
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
je	být	k5eAaImIp3nS	být
užitečný	užitečný	k2eAgInSc1d1	užitečný
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
tvorba	tvorba	k1gFnSc1	tvorba
rozvrhů	rozvrh	k1gInPc2	rozvrh
a	a	k8xC	a
plánování	plánování	k1gNnSc4	plánování
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
systémů	systém	k1gInPc2	systém
Prologu	prolog	k1gInSc2	prolog
je	být	k5eAaImIp3nS	být
dodávána	dodávat	k5eAaImNgFnS	dodávat
s	s	k7c7	s
alespoň	alespoň	k9	alespoň
jedním	jeden	k4xCgInSc7	jeden
constraint	constrainta	k1gFnPc2	constrainta
solverem	solvero	k1gNnSc7	solvero
pro	pro	k7c4	pro
konečné	konečný	k2eAgFnPc4d1	konečná
domény	doména	k1gFnPc4	doména
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
se	s	k7c7	s
solvery	solver	k1gInPc7	solver
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
domény	doména	k1gFnPc4	doména
jako	jako	k8xS	jako
racionální	racionální	k2eAgNnPc1d1	racionální
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Programování	programování	k1gNnSc1	programování
vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
===	===	k?	===
</s>
</p>
<p>
<s>
HiLog	HiLog	k1gMnSc1	HiLog
a	a	k8xC	a
λ	λ	k?	λ
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
Prolog	prolog	k1gInSc4	prolog
o	o	k7c6	o
vlastnosti	vlastnost	k1gFnSc6	vlastnost
programování	programování	k1gNnSc2	programování
vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
ISO	ISO	kA	ISO
Prolog	prolog	k1gInSc1	prolog
nyní	nyní	k6eAd1	nyní
podporuje	podporovat	k5eAaImIp3nS	podporovat
vestavěné	vestavěný	k2eAgInPc4d1	vestavěný
predikáty	predikát	k1gInPc4	predikát
call	calla	k1gFnPc2	calla
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
call	call	k1gInSc1	call
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
...	...	k?	...
které	který	k3yRgNnSc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
programování	programování	k1gNnSc4	programování
vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
lambda	lambda	k1gNnSc2	lambda
abstrakce	abstrakce	k1gFnSc2	abstrakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Objektová	objektový	k2eAgFnSc1d1	objektová
orientace	orientace	k1gFnSc1	orientace
===	===	k?	===
</s>
</p>
<p>
<s>
Logtalk	Logtalk	k6eAd1	Logtalk
je	být	k5eAaImIp3nS	být
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
logický	logický	k2eAgInSc1d1	logický
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
užít	užít	k5eAaPmF	užít
většinu	většina	k1gFnSc4	většina
implementací	implementace	k1gFnPc2	implementace
Prologu	prolog	k1gInSc2	prolog
jako	jako	k8xS	jako
back-endový	backndový	k2eAgInSc1d1	back-endový
kompilátor	kompilátor	k1gInSc1	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
multi-paradigmový	multiaradigmový	k2eAgInSc1d1	multi-paradigmový
jazyk	jazyk	k1gInSc1	jazyk
podporuje	podporovat	k5eAaImIp3nS	podporovat
jak	jak	k6eAd1	jak
prototypy	prototyp	k1gInPc4	prototyp
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblog	Oblog	k1gInSc1	Oblog
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgInPc4d1	malý
<g/>
,	,	kIx,	,
přenosné	přenosný	k2eAgInPc4d1	přenosný
<g/>
,	,	kIx,	,
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc4d1	orientované
rozšíření	rozšíření	k1gNnSc4	rozšíření
Prologu	prolog	k1gInSc2	prolog
od	od	k7c2	od
Margaret	Margareta	k1gFnPc2	Margareta
McDougall	McDougalla	k1gFnPc2	McDougalla
z	z	k7c2	z
EdCAAD	EdCAAD	k1gFnSc2	EdCAAD
<g/>
,	,	kIx,	,
University	universita	k1gFnSc2	universita
of	of	k?	of
Edinburgh	Edinburgh	k1gMnSc1	Edinburgh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objlog	Objlog	k1gInSc4	Objlog
byl	být	k5eAaImAgInS	být
frame-based	frameased	k1gInSc1	frame-based
jazyk	jazyk	k1gInSc1	jazyk
kombinující	kombinující	k2eAgInPc1d1	kombinující
objekty	objekt	k1gInPc1	objekt
a	a	k8xC	a
Prolog	prolog	k1gInSc1	prolog
II	II	kA	II
z	z	k7c2	z
CNRS	CNRS	kA	CNRS
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Grafika	grafika	k1gFnSc1	grafika
===	===	k?	===
</s>
</p>
<p>
<s>
Prologové	Prologový	k2eAgInPc1d1	Prologový
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
grafickou	grafický	k2eAgFnSc4d1	grafická
knihovnu	knihovna	k1gFnSc4	knihovna
jsou	být	k5eAaImIp3nP	být
SWI-Prolog	SWI-Prolog	k1gMnSc1	SWI-Prolog
<g/>
,	,	kIx,	,
Visual-prolog	Visualrolog	k1gMnSc1	Visual-prolog
a	a	k8xC	a
B-Prolog	B-Prolog	k1gMnSc1	B-Prolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Souběžnost	souběžnost	k1gFnSc4	souběžnost
===	===	k?	===
</s>
</p>
<p>
<s>
Prolog-MPI	Prolog-MPI	k?	Prolog-MPI
je	být	k5eAaImIp3nS	být
open-source	openourka	k1gFnSc3	open-sourka
rozšíření	rozšíření	k1gNnSc2	rozšíření
SWI-Prologu	SWI-Prolog	k1gMnSc3	SWI-Prolog
pro	pro	k7c4	pro
distribuované	distribuovaný	k2eAgInPc4d1	distribuovaný
výpočty	výpočet	k1gInPc4	výpočet
přes	přes	k7c4	přes
Message	Messag	k1gFnPc4	Messag
Passing	Passing	k1gInSc4	Passing
Interface	interface	k1gInSc2	interface
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
souběžných	souběžný	k2eAgInPc2d1	souběžný
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
Prologu	prolog	k1gInSc2	prolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Programování	programování	k1gNnSc1	programování
webů	web	k1gInPc2	web
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
implementace	implementace	k1gFnPc1	implementace
Prologu	prolog	k1gInSc2	prolog
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
SWI-Prolog	SWI-Prolog	k1gMnSc1	SWI-Prolog
<g/>
,	,	kIx,	,
podporují	podporovat	k5eAaImIp3nP	podporovat
server-side	serverid	k1gInSc5	server-sid
programování	programování	k1gNnSc2	programování
webů	web	k1gInPc2	web
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
pro	pro	k7c4	pro
webové	webový	k2eAgInPc4d1	webový
protokoly	protokol	k1gInPc4	protokol
<g/>
,	,	kIx,	,
HTML	HTML	kA	HTML
a	a	k8xC	a
XML	XML	kA	XML
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
rozšíření	rozšíření	k1gNnSc4	rozšíření
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
formátů	formát	k1gInPc2	formát
sémantických	sémantický	k2eAgInPc2d1	sémantický
webů	web	k1gInPc2	web
jako	jako	k8xC	jako
RDF	RDF	kA	RDF
a	a	k8xC	a
OWL	OWL	kA	OWL
(	(	kIx(	(
<g/>
Web	web	k1gInSc1	web
Ontology	ontolog	k1gMnPc4	ontolog
Language	language	k1gFnPc2	language
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prolog	prolog	k1gInSc1	prolog
byl	být	k5eAaImAgInS	být
také	také	k9	také
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k8xS	jako
client-side	clientid	k1gInSc5	client-sid
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Adobe	Adobe	kA	Adobe
Flash	Flash	k1gInSc1	Flash
===	===	k?	===
</s>
</p>
<p>
<s>
Cedar	Cedar	k1gInSc1	Cedar
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc1d1	základní
interpreter	interpreter	k1gInSc1	interpreter
Prologu	prolog	k1gInSc2	prolog
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
<g/>
4	[number]	k4	4
a	a	k8xC	a
výše	vysoce	k6eAd2	vysoce
Cedar	Cedar	k1gInSc1	Cedar
podporuje	podporovat	k5eAaImIp3nS	podporovat
FCA	FCA	kA	FCA
(	(	kIx(	(
<g/>
Flash	Flash	k1gMnSc1	Flash
Cedar	Cedar	k1gMnSc1	Cedar
App	App	k1gMnSc1	App
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
novou	nový	k2eAgFnSc4d1	nová
platformu	platforma	k1gFnSc4	platforma
k	k	k7c3	k
programování	programování	k1gNnSc3	programování
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
pomocí	pomocí	k7c2	pomocí
ActionScriptu	ActionScript	k1gInSc2	ActionScript
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
F-logic	Fogic	k1gMnSc1	F-logic
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
Prolog	prolog	k1gInSc4	prolog
o	o	k7c4	o
rámce	rámec	k1gInPc4	rámec
<g/>
/	/	kIx~	/
<g/>
objekty	objekt	k1gInPc1	objekt
pro	pro	k7c4	pro
reprezentaci	reprezentace	k1gFnSc4	reprezentace
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OW	OW	kA	OW
Prolog	prolog	k1gInSc1	prolog
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
jako	jako	k8xS	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
rozhraní	rozhraní	k1gNnSc2	rozhraní
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhraní	rozhraní	k1gNnSc1	rozhraní
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
jazykům	jazyk	k1gInPc3	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
frameworky	frameworka	k1gFnPc1	frameworka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dokáží	dokázat	k5eAaPmIp3nP	dokázat
přemostit	přemostit	k5eAaPmF	přemostit
mezi	mezi	k7c7	mezi
Prologem	prolog	k1gInSc7	prolog
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Logic	Logic	k1gMnSc1	Logic
Server	server	k1gInSc1	server
API	API	kA	API
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jak	jak	k6eAd1	jak
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vložení	vložení	k1gNnSc4	vložení
Prologu	prolog	k1gInSc2	prolog
do	do	k7c2	do
C	C	kA	C
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
Javy	Javy	k1gInPc1	Javy
<g/>
,	,	kIx,	,
VB	VB	kA	VB
<g/>
,	,	kIx,	,
Delphi	Delphi	k1gNnSc2	Delphi
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
,	,	kIx,	,
a	a	k8xC	a
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
jazyku	jazyk	k1gInSc6	jazyk
<g/>
/	/	kIx~	/
<g/>
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
volat	volat	k5eAaImF	volat
.	.	kIx.	.
<g/>
dll	dll	k?	dll
nebo	nebo	k8xC	nebo
.	.	kIx.	.
<g/>
so	so	k?	so
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
implementováno	implementován	k2eAgNnSc1d1	implementováno
pro	pro	k7c4	pro
Amzi	Amze	k1gFnSc4	Amze
<g/>
!	!	kIx.	!
</s>
<s>
Prolog	prolog	k1gInSc1	prolog
Amzi	Amz	k1gFnSc2	Amz
<g/>
!	!	kIx.	!
</s>
<s>
Prolog	prolog	k1gInSc1	prolog
+	+	kIx~	+
Logic	Logic	k1gMnSc1	Logic
Server	server	k1gInSc4	server
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
API	API	kA	API
specifikace	specifikace	k1gFnPc1	specifikace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zpřístupněny	zpřístupněn	k2eAgFnPc1d1	zpřístupněna
pro	pro	k7c4	pro
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
implementaci	implementace	k1gFnSc4	implementace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JPL	JPL	kA	JPL
je	být	k5eAaImIp3nS	být
obousměrné	obousměrný	k2eAgNnSc1d1	obousměrné
Java	Javus	k1gMnSc2	Javus
Prolog	prolog	k1gInSc4	prolog
přemostění	přemostění	k1gNnSc2	přemostění
<g/>
,	,	kIx,	,
dodávané	dodávaný	k2eAgNnSc1d1	dodávané
s	s	k7c7	s
SWI-Prologem	SWI-Prolog	k1gMnSc7	SWI-Prolog
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc3d1	umožňující
Javě	Java	k1gFnSc3	Java
a	a	k8xC	a
Prologu	prolog	k1gInSc3	prolog
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
volání	volání	k1gNnSc1	volání
(	(	kIx(	(
<g/>
rekurzivně	rekurzivně	k6eAd1	rekurzivně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
souběžnost	souběžnost	k1gFnSc4	souběžnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
aktivním	aktivní	k2eAgInSc7d1	aktivní
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
InterProlog	InterProlog	k1gMnSc1	InterProlog
<g/>
,	,	kIx,	,
knihovna	knihovna	k1gFnSc1	knihovna
přemostění	přemostění	k1gNnSc2	přemostění
mezi	mezi	k7c7	mezi
Javou	Java	k1gFnSc7	Java
a	a	k8xC	a
Prologem	prolog	k1gInSc7	prolog
<g/>
,	,	kIx,	,
implementuje	implementovat	k5eAaImIp3nS	implementovat
obousměrné	obousměrný	k2eAgNnSc1d1	obousměrné
volání	volání	k1gNnSc1	volání
predikátů	predikát	k1gInPc2	predikát
<g/>
/	/	kIx~	/
<g/>
metod	metoda	k1gFnPc2	metoda
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Java	Java	k6eAd1	Java
objekty	objekt	k1gInPc1	objekt
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
namapovány	namapován	k2eAgFnPc1d1	namapován
na	na	k7c4	na
termy	termy	k1gFnPc4	termy
Prologu	prolog	k1gInSc2	prolog
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vývoj	vývoj	k1gInSc1	vývoj
GUI	GUI	kA	GUI
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
Javě	Java	k1gFnSc6	Java
při	při	k7c6	při
ponechání	ponechání	k1gNnSc6	ponechání
logického	logický	k2eAgNnSc2d1	logické
zpracování	zpracování	k1gNnSc2	zpracování
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
Prologu	prolog	k1gInSc2	prolog
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
XSB	XSB	kA	XSB
<g/>
,	,	kIx,	,
SWI-Prolog	SWI-Prolog	k1gMnSc1	SWI-Prolog
a	a	k8xC	a
YAP	YAP	kA	YAP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prova	Prova	k1gFnSc1	Prova
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
nativní	nativní	k2eAgFnSc4d1	nativní
integraci	integrace	k1gFnSc4	integrace
syntaxe	syntax	k1gFnSc2	syntax
s	s	k7c7	s
Javou	Java	k1gFnSc7	Java
<g/>
,	,	kIx,	,
agent	agent	k1gMnSc1	agent
messaging	messaging	k1gInSc1	messaging
a	a	k8xC	a
reakční	reakční	k2eAgNnPc1d1	reakční
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
Provy	Prova	k1gFnSc2	Prova
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
rule-based	ruleased	k1gMnSc1	rule-based
skriptovací	skriptovací	k2eAgMnSc1d1	skriptovací
(	(	kIx(	(
<g/>
RBS	RBS	kA	RBS
<g/>
)	)	kIx)	)
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
middleware	middlewar	k1gInSc5	middlewar
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
v	v	k7c6	v
kombinování	kombinování	k1gNnSc6	kombinování
imperativního	imperativní	k2eAgMnSc2d1	imperativní
a	a	k8xC	a
deklarativního	deklarativní	k2eAgNnSc2d1	deklarativní
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PROL	PROL	kA	PROL
je	být	k5eAaImIp3nS	být
embeddable	embeddable	k6eAd1	embeddable
Prolog	prolog	k1gInSc1	prolog
engine	enginout	k5eAaPmIp3nS	enginout
pro	pro	k7c4	pro
Javu	Java	k1gFnSc4	Java
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
malé	malý	k2eAgFnPc4d1	malá
IDE	IDE	kA	IDE
a	a	k8xC	a
pár	pár	k4xCyI	pár
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GNU	gnu	k1gNnSc1	gnu
Prolog	prolog	k1gInSc1	prolog
for	forum	k1gNnPc2	forum
Java	Jav	k1gInSc2	Jav
je	být	k5eAaImIp3nS	být
implementace	implementace	k1gFnSc1	implementace
ISO	ISO	kA	ISO
Prologu	prolog	k1gInSc2	prolog
jakožto	jakožto	k8xS	jakožto
knihovny	knihovna	k1gFnSc2	knihovna
Javy	Java	k1gFnSc2	Java
(	(	kIx(	(
<g/>
gnu	gnu	k1gMnSc1	gnu
<g/>
.	.	kIx.	.
<g/>
prolog	prolog	k1gInSc1	prolog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ciao	Ciao	k6eAd1	Ciao
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rozhraní	rozhraní	k1gNnSc4	rozhraní
k	k	k7c3	k
C	C	kA	C
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
++	++	k?	++
<g/>
,	,	kIx,	,
Javě	Javě	k1gMnSc4	Javě
<g/>
,	,	kIx,	,
a	a	k8xC	a
relačním	relační	k2eAgFnPc3d1	relační
databázím	databáze	k1gFnPc3	databáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
-Prolog	-Prolog	k1gMnSc1	-Prolog
je	být	k5eAaImIp3nS	být
interpreter	interpreter	k1gInSc4	interpreter
Prologu	prolog	k1gInSc2	prolog
napsaný	napsaný	k2eAgInSc4d1	napsaný
v	v	k7c4	v
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gNnSc4	on
snadno	snadno	k6eAd1	snadno
integrovat	integrovat	k5eAaBmF	integrovat
do	do	k7c2	do
C	C	kA	C
<g/>
#	#	kIx~	#
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristiky	charakteristika	k1gFnPc1	charakteristika
<g/>
:	:	kIx,	:
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
rychlý	rychlý	k2eAgInSc1d1	rychlý
interpreter	interpreter	k1gInSc1	interpreter
<g/>
,	,	kIx,	,
rozhraní	rozhraní	k1gNnSc1	rozhraní
přes	přes	k7c4	přes
příkazovou	příkazový	k2eAgFnSc4d1	příkazová
řádku	řádka	k1gFnSc4	řádka
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
rozhraní	rozhraní	k1gNnSc1	rozhraní
<g/>
,	,	kIx,	,
vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
DCG	DCG	kA	DCG
<g/>
,	,	kIx,	,
XML-predikáty	XMLredikát	k1gInPc1	XML-predikát
<g/>
,	,	kIx,	,
SQL-predikáty	SQLredikát	k1gInPc1	SQL-predikát
<g/>
,	,	kIx,	,
rozšířitelný	rozšířitelný	k2eAgInSc1d1	rozšířitelný
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
včetně	včetně	k7c2	včetně
generátoru	generátor	k1gInSc2	generátor
parserů	parser	k1gInPc2	parser
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
přidání	přidání	k1gNnSc4	přidání
rozšíření	rozšíření	k1gNnSc2	rozšíření
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
SWI-Prolog	SWI-Prolog	k1gMnSc1	SWI-Prolog
–	–	k?	–
implementace	implementace	k1gFnSc1	implementace
Prologu	prolog	k1gInSc2	prolog
pod	pod	k7c7	pod
GNU	gnu	k1gNnSc7	gnu
licencí	licence	k1gFnPc2	licence
</s>
</p>
<p>
<s>
Strawberry	Strawberra	k1gFnPc1	Strawberra
Prolog	prolog	k1gInSc1	prolog
–	–	k?	–
implementace	implementace	k1gFnSc1	implementace
Prologu	prolog	k1gInSc2	prolog
</s>
</p>
<p>
<s>
Tutoriál	tutoriál	k1gInSc1	tutoriál
k	k	k7c3	k
Prologu	prolog	k1gInSc3	prolog
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
Caly	Cala	k1gMnSc2	Cala
Poly	pola	k1gFnSc2	pola
Pomona	Pomon	k1gMnSc2	Pomon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bartákův	Bartákův	k2eAgInSc1d1	Bartákův
tutoriál	tutoriál	k1gInSc1	tutoriál
k	k	k7c3	k
Prologu	prolog	k1gInSc3	prolog
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
KTIML	KTIML	kA	KTIML
MFF	MFF	kA	MFF
UK	UK	kA	UK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
programování	programování	k1gNnSc2	programování
v	v	k7c6	v
Prologu	prolog	k1gInSc6	prolog
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
KSVI	KSVI	kA	KSVI
MFF	MFF	kA	MFF
UK	UK	kA	UK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Manuál	manuál	k1gInSc1	manuál
Prologu	prolog	k1gInSc2	prolog
s	s	k7c7	s
řešenými	řešený	k2eAgInPc7d1	řešený
příklady	příklad	k1gInPc7	příklad
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
FIM	FIM	kA	FIM
UHK	UHK	kA	UHK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
cvičení	cvičení	k1gNnSc4	cvičení
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
předmětu	předmět	k1gInSc2	předmět
"	"	kIx"	"
<g/>
Alternativní	alternativní	k2eAgFnPc1d1	alternativní
metody	metoda	k1gFnPc1	metoda
programování	programování	k1gNnSc2	programování
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
FM	FM	kA	FM
TUL	Tula	k1gFnPc2	Tula
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Artificial	Artificial	k1gMnSc1	Artificial
Intelligence	Intelligence	k1gFnSc2	Intelligence
through	through	k1gMnSc1	through
Prolog	prolog	k1gInSc1	prolog
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
C.	C.	kA	C.
Rowe	Rowe	k1gFnSc1	Rowe
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Learn	Learn	k1gMnSc1	Learn
Prolog	prolog	k1gInSc4	prolog
Now	Now	k1gFnSc2	Now
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
