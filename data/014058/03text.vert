<s>
Hafnium	hafnium	k1gNnSc1
</s>
<s>
Hafnium	hafnium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Hf	Hf	k?
</s>
<s>
72	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Hafnium	hafnium	k1gNnSc1
<g/>
,	,	kIx,
Hf	Hf	k1gFnSc1
<g/>
,	,	kIx,
72	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Hafnium	hafnium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
šedý	šedý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-58-6	7440-58-6	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
178,49	178,49	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
159	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
175	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
5	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
hexagonální	hexagonální	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
13,31	13,31	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
5,5	5,5	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
2	#num#	k4
232,85	232,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
506	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
4	#num#	k4
602,85	602,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
4	#num#	k4
876	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
331	#num#	k4
μ	μ	k?
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetické	paramagnetický	k2eAgNnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
5	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
neutrony	neutron	k1gInPc7
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zr	Zr	k?
<g/>
⋏	⋏	k?
</s>
<s>
Lutecium	lutecium	k1gNnSc1
≺	≺	k?
<g/>
Hf	Hf	k1gMnSc1
<g/>
≻	≻	k?
Tantal	Tantal	k1gMnSc1
</s>
<s>
⋎	⋎	k?
<g/>
Rf	Rf	k1gFnSc1
</s>
<s>
Hafnium	hafnium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Hf	Hf	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Hafnium	hafnium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
velmi	velmi	k6eAd1
podobný	podobný	k2eAgInSc1d1
zirkoniu	zirkonium	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
uplatnění	uplatnění	k1gNnSc1
nalézá	nalézat	k5eAaImIp3nS
jako	jako	k9
složka	složka	k1gFnSc1
některých	některý	k3yIgFnPc2
speciálních	speciální	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Kousky	kousek	k1gInPc1
hafnia	hafnium	k1gNnSc2
</s>
<s>
Hafnium	hafnium	k1gNnSc1
je	být	k5eAaImIp3nS
šedý	šedý	k2eAgInSc1d1
až	až	k9
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
středně	středně	k6eAd1
tvrdý	tvrdý	k2eAgMnSc1d1
<g/>
,	,	kIx,
poměrně	poměrně	k6eAd1
vzácný	vzácný	k2eAgInSc4d1
těžký	těžký	k2eAgInSc4d1
kov	kov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
teplotách	teplota	k1gFnPc6
pod	pod	k7c4
0,35	0,35	k4
K	k	k7c3
je	být	k5eAaImIp3nS
supravodičem	supravodič	k1gInSc7
1	#num#	k4
<g/>
.	.	kIx.
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
mimořádnou	mimořádný	k2eAgFnSc7d1
chemickou	chemický	k2eAgFnSc7d1
stálostí	stálost	k1gFnSc7
–	–	k?
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
netečný	tečný	k2eNgMnSc1d1,k2eAgMnSc1d1
k	k	k7c3
působení	působení	k1gNnSc3
vody	voda	k1gFnSc2
a	a	k8xC
odolává	odolávat	k5eAaImIp3nS
působení	působení	k1gNnSc1
většiny	většina	k1gFnSc2
běžných	běžný	k2eAgFnPc2d1
minerálních	minerální	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
i	i	k8xC
roztoků	roztok	k1gInPc2
alkalických	alkalický	k2eAgInPc2d1
hydroxidů	hydroxid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
rozpouštění	rozpouštění	k1gNnSc6
je	být	k5eAaImIp3nS
nejúčinnější	účinný	k2eAgFnSc1d3
kyselina	kyselina	k1gFnSc1
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
HF	HF	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
její	její	k3xOp3gFnPc4
směsi	směs	k1gFnPc4
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
minerálními	minerální	k2eAgFnPc7d1
kyselinami	kyselina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Chemicky	chemicky	k6eAd1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
silně	silně	k6eAd1
podobné	podobný	k2eAgNnSc1d1
zirkoniu	zirkonium	k1gNnSc3
<g/>
,	,	kIx,
doprovází	doprovázet	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
prakticky	prakticky	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgInPc6
minerálech	minerál	k1gInPc6
a	a	k8xC
horninách	hornina	k1gFnPc6
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
příprava	příprava	k1gFnSc1
velmi	velmi	k6eAd1
čistého	čistý	k2eAgNnSc2d1
hafnia	hafnium	k1gNnSc2
náročný	náročný	k2eAgInSc4d1
problém	problém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Hf	Hf	k1gFnSc2
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
i	i	k9
sloučeniny	sloučenina	k1gFnPc1
Hf	Hf	k1gFnSc1
<g/>
3	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
Hf	Hf	k1gFnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Hafnium	hafnium	k1gNnSc1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1923	#num#	k4
v	v	k7c6
dánském	dánský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Kodani	Kodaň	k1gFnSc6
<g/>
,	,	kIx,
podle	podle	k7c2
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
latinského	latinský	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
bylo	být	k5eAaImAgNnS
také	také	k9
pojmenováno	pojmenovat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objeviteli	objevitel	k1gMnSc3
byli	být	k5eAaImAgMnP
chemici	chemik	k1gMnPc1
Dirk	Dirk	k1gMnSc1
Coster	Coster	k1gMnSc1
a	a	k8xC
George	Georg	k1gMnSc2
de	de	k?
Hevesy	Hevesa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Minerál	minerál	k1gInSc1
zirkon	zirkon	k1gInSc1
</s>
<s>
Hafnium	hafnium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
řídkým	řídký	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
přibližně	přibližně	k6eAd1
4,5	4,5	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
(	(	kIx(
<g/>
4,5	4,5	k4
ppm	ppm	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
natolik	natolik	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
nelze	lze	k6eNd1
přesně	přesně	k6eAd1
určit	určit	k5eAaPmF
ani	ani	k9
nejcitlivějšími	citlivý	k2eAgFnPc7d3
analytickými	analytický	k2eAgFnPc7d1
technikami	technika	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udává	udávat	k5eAaImIp3nS
se	se	k3xPyFc4
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
je	být	k5eAaImIp3nS
nižší	nízký	k2eAgMnSc1d2
než	než	k8xS
0,000	0,000	k4
008	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
hafnia	hafnium	k1gNnSc2
na	na	k7c4
200	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hafnium	hafnium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minerálech	minerál	k1gInPc6
vždy	vždy	k6eAd1
doprovází	doprovázet	k5eAaImIp3nS
zirkonium	zirkonium	k1gNnSc4
v	v	k7c6
množství	množství	k1gNnSc6
1	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
%	%	kIx~
a	a	k8xC
minerály	minerál	k1gInPc1
obsahující	obsahující	k2eAgInPc1d1
samostatně	samostatně	k6eAd1
hafnium	hafnium	k1gNnSc4
nejsou	být	k5eNaImIp3nP
známy	znám	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
významnějších	významný	k2eAgInPc2d2
minerálů	minerál	k1gInPc2
zirkonia	zirkonium	k1gNnSc2
lze	lze	k6eAd1
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
baddeleyit	baddeleyit	k1gInSc4
<g/>
,	,	kIx,
zirkon	zirkon	k1gInSc4
<g/>
,	,	kIx,
zirkelit	zirkelit	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
uhligit	uhligit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
těžby	těžba	k1gFnSc2
minerálů	minerál	k1gInPc2
a	a	k8xC
hornin	hornina	k1gFnPc2
s	s	k7c7
výrazným	výrazný	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
zirkonia	zirkonium	k1gNnSc2
patří	patřit	k5eAaImIp3nS
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
hafnia	hafnium	k1gNnSc2
spočívá	spočívat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
separaci	separace	k1gFnSc6
od	od	k7c2
zirkonia	zirkonium	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
Krollově	Krollův	k2eAgInSc6d1
procesu	proces	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
základním	základní	k2eAgInSc7d1
postupem	postup	k1gInSc7
pro	pro	k7c4
rozklad	rozklad	k1gInSc4
a	a	k8xC
separaci	separace	k1gFnSc4
zirkoniových	zirkoniový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výsledným	výsledný	k2eAgInSc7d1
produktem	produkt	k1gInSc7
směs	směs	k1gFnSc1
Zr	Zr	k1gMnSc1
+	+	kIx~
Hf	Hf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1
vzájemná	vzájemný	k2eAgFnSc1d1
separace	separace	k1gFnSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
buď	buď	k8xC
frakční	frakční	k2eAgFnSc7d1
destilací	destilace	k1gFnSc7
chloridů	chlorid	k1gInPc2
nebo	nebo	k8xC
na	na	k7c6
ionexových	ionexový	k2eAgFnPc6d1
kolonách	kolona	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
omezené	omezený	k2eAgFnSc3d1
dostupnosti	dostupnost	k1gFnSc3
hrozí	hrozit	k5eAaImIp3nS
v	v	k7c6
nejbližších	blízký	k2eAgInPc6d3
letech	let	k1gInPc6
kritický	kritický	k2eAgInSc1d1
nedostatek	nedostatek	k1gInSc1
zdrojů	zdroj	k1gInPc2
prvku	prvek	k1gInSc2
pro	pro	k7c4
technologické	technologický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Krystalické	krystalický	k2eAgNnSc4d1
kovové	kovový	k2eAgNnSc4d1
hafnium	hafnium	k1gNnSc4
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
nízkému	nízký	k2eAgInSc3d1
výskytu	výskyt	k1gInSc3
a	a	k8xC
nákladné	nákladný	k2eAgFnSc3d1
výrobě	výroba	k1gFnSc3
nemá	mít	k5eNaImIp3nS
hafnium	hafnium	k1gNnSc1
příliš	příliš	k6eAd1
velké	velký	k2eAgNnSc4d1
praktické	praktický	k2eAgNnSc4d1
uplatnění	uplatnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
hlavním	hlavní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
je	být	k5eAaImIp3nS
proces	proces	k1gInSc1
čištění	čištění	k1gNnSc2
kovového	kovový	k2eAgNnSc2d1
zirkonia	zirkonium	k1gNnSc2
pro	pro	k7c4
účely	účel	k1gInPc4
jaderné	jaderný	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hafnium	hafnium	k1gNnSc1
dokáže	dokázat	k5eAaPmIp3nS
velmi	velmi	k6eAd1
účinně	účinně	k6eAd1
absorbovat	absorbovat	k5eAaBmF
neutrony	neutron	k1gInPc4
(	(	kIx(
<g/>
až	až	k9
600	#num#	k4
<g/>
×	×	k?
více	hodně	k6eAd2
než	než	k8xS
zirkonium	zirkonium	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
vynikající	vynikající	k2eAgFnPc4d1
mechanické	mechanický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
jako	jako	k9
materiál	materiál	k1gInSc1
pro	pro	k7c4
řídící	řídící	k2eAgFnPc4d1
tyče	tyč	k1gFnPc4
reaktorů	reaktor	k1gInPc2
jaderných	jaderný	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1
bod	bod	k1gInSc1
tání	tání	k1gNnSc2
a	a	k8xC
odolnost	odolnost	k1gFnSc4
hafnia	hafnium	k1gNnSc2
jej	on	k3xPp3gMnSc4
určují	určovat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgInSc1
z	z	k7c2
materiálů	materiál	k1gInPc2
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
klasických	klasický	k2eAgFnPc2d1
žárovkových	žárovkový	k2eAgFnPc2d1
vláken	vlákna	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
je	být	k5eAaImIp3nS
vlákno	vlákno	k1gNnSc1
rozžhaveno	rozžhaven	k2eAgNnSc1d1
průchodem	průchod	k1gInSc7
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
na	na	k7c4
takovou	takový	k3xDgFnSc4
teplotu	teplota	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zdrojem	zdroj	k1gInSc7
viditelného	viditelný	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
(	(	kIx(
<g/>
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
vlnových	vlnový	k2eAgFnPc2d1
délek	délka	k1gFnPc2
360	#num#	k4
<g/>
–	–	k?
<g/>
900	#num#	k4
nm	nm	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hafnia	hafnium	k1gNnSc2
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
elektrody	elektroda	k1gFnPc1
pro	pro	k7c4
plazmové	plazmový	k2eAgNnSc4d1
řezání	řezání	k1gNnSc4
kovů	kov	k1gInPc2
a	a	k8xC
sváření	sváření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
se	s	k7c7
zirkoniem	zirkonium	k1gNnSc7
<g/>
,	,	kIx,
niobem	niob	k1gInSc7
<g/>
,	,	kIx,
tantalem	tantal	k1gInSc7
a	a	k8xC
titanem	titan	k1gInSc7
je	být	k5eAaImIp3nS
složkou	složka	k1gFnSc7
speciálních	speciální	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
odolností	odolnost	k1gFnSc7
proti	proti	k7c3
korozi	koroze	k1gFnSc3
a	a	k8xC
vysokým	vysoký	k2eAgFnPc3d1
teplotám	teplota	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
výrobě	výroba	k1gFnSc6
polovodičů	polovodič	k1gInPc2
a	a	k8xC
integrovaných	integrovaný	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
nalézá	nalézat	k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc4
oxid	oxid	k1gInSc4
hafničitý	hafničitý	k2eAgInSc4d1
(	(	kIx(
<g/>
HfO	HfO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Energy	Energ	k1gInPc4
Department	department	k1gInSc1
Releases	Releases	k1gMnSc1
New	New	k1gMnSc1
Critical	Critical	k1gMnSc1
Materials	Materials	k1gInSc4
Strategy	Stratega	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2010	#num#	k4
<g/>
↑	↑	k?
Periodic	Periodic	k1gMnSc1
Table	tablo	k1gNnSc6
of	of	k?
Elements	Elements	k1gInSc1
<g/>
:	:	kIx,
Los	los	k1gInSc1
Alamos	Alamosa	k1gFnPc2
National	National	k1gFnSc2
Laboratory	Laborator	k1gInPc1
<g/>
.	.	kIx.
periodic	periodic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
lanl	lanl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
hafnium	hafnium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
hafnium	hafnium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4158734-0	4158734-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5762	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85058238	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85058238	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
