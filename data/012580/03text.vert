<p>
<s>
Palisáda	palisáda	k1gFnSc1	palisáda
je	být	k5eAaImIp3nS	být
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
plot	plot	k1gInSc4	plot
různé	různý	k2eAgFnSc2d1	různá
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jako	jako	k9	jako
obranný	obranný	k2eAgInSc1d1	obranný
prostředek	prostředek	k1gInSc1	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
palisáda	palisáda	k1gFnSc1	palisáda
tvořena	tvořit	k5eAaImNgFnS	tvořit
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
kůly	kůl	k1gInPc7	kůl
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
zaraženy	zarazit	k5eAaPmNgInP	zarazit
do	do	k7c2	do
země	zem	k1gFnSc2	zem
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Palisáda	palisáda	k1gFnSc1	palisáda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
příkopem	příkop	k1gInSc7	příkop
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
kůly	kůl	k1gInPc1	kůl
horizontálně	horizontálně	k6eAd1	horizontálně
propleteny	proplést	k5eAaPmNgInP	proplést
proutím	proutí	k1gNnSc7	proutí
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
taková	takový	k3xDgFnSc1	takový
palisáda	palisáda	k1gFnSc1	palisáda
polský	polský	k2eAgInSc4d1	polský
plot	plot	k1gInSc4	plot
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
průplet	průplet	k1gInSc1	průplet
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
zpevnění	zpevnění	k1gNnSc3	zpevnění
palisády	palisáda	k1gFnSc2	palisáda
a	a	k8xC	a
pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
trnitých	trnitý	k2eAgInPc2d1	trnitý
prutů	prut	k1gInPc2	prut
<g/>
,	,	kIx,	,
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
bránil	bránit	k5eAaImAgMnS	bránit
překonání	překonání	k1gNnPc4	překonání
celé	celý	k2eAgFnSc2d1	celá
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
bylo	být	k5eAaImAgNnS	být
dostupný	dostupný	k2eAgInSc4d1	dostupný
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
a	a	k8xC	a
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
.	.	kIx.	.
</s>
<s>
Palisády	palisáda	k1gFnPc1	palisáda
představují	představovat	k5eAaImIp3nP	představovat
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgInSc1d1	efektivní
obranný	obranný	k2eAgInSc1d1	obranný
prvek	prvek	k1gInSc1	prvek
při	při	k7c6	při
krátkodobém	krátkodobý	k2eAgInSc6d1	krátkodobý
konfliktu	konflikt	k1gInSc6	konflikt
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
před	před	k7c7	před
nepočetným	početný	k2eNgMnSc7d1	nepočetný
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
palisády	palisáda	k1gFnPc1	palisáda
ale	ale	k9	ale
nejsou	být	k5eNaImIp3nP	být
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
ohni	oheň	k1gInSc3	oheň
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
odolat	odolat	k5eAaPmF	odolat
obléhacím	obléhací	k2eAgInPc3d1	obléhací
strojům	stroj	k1gInPc3	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Palisády	palisáda	k1gFnPc1	palisáda
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
již	již	k6eAd1	již
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
užití	užití	k1gNnSc1	užití
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
i	i	k9	i
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byly	být	k5eAaImAgFnP	být
palisády	palisáda	k1gFnPc1	palisáda
používány	používán	k2eAgFnPc1d1	používána
buď	buď	k8xC	buď
pro	pro	k7c4	pro
opevňování	opevňování	k1gNnSc4	opevňování
malých	malý	k2eAgFnPc2d1	malá
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
doplněk	doplněk	k1gInSc4	doplněk
rozsáhlejších	rozsáhlý	k2eAgInPc2d2	rozsáhlejší
fortifikačních	fortifikační	k2eAgInPc2d1	fortifikační
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
valu	val	k1gInSc2	val
první	první	k4xOgFnSc2	první
linie	linie	k1gFnSc2	linie
opevnění	opevnění	k1gNnSc2	opevnění
hradů	hrad	k1gInPc2	hrad
i	i	k8xC	i
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
palisáda	palisáda	k1gFnSc1	palisáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
