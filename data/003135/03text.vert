<s>
Margaret	Margareta	k1gFnPc2	Margareta
Hilda	Hilda	k1gFnSc1	Hilda
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
<g/>
,	,	kIx,	,
baronka	baronka	k1gFnSc1	baronka
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
<g/>
,	,	kIx,	,
LG	LG	kA	LG
<g/>
,	,	kIx,	,
OM	OM	kA	OM
<g/>
,	,	kIx,	,
PC	PC	kA	PC
<g/>
,	,	kIx,	,
FRS	FRS	kA	FRS
<g/>
,	,	kIx,	,
nepřechýleně	přechýleně	k6eNd1	přechýleně
Margaret	Margareta	k1gFnPc2	Margareta
Thatcher	Thatchra	k1gFnPc2	Thatchra
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Roberts	Roberts	k1gInSc1	Roberts
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1925	[number]	k4	1925
Grantham	Grantham	k1gInSc1	Grantham
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
vůdkyně	vůdkyně	k1gFnSc2	vůdkyně
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
a	a	k8xC	a
setrvala	setrvat	k5eAaPmAgFnS	setrvat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
všichni	všechen	k3xTgMnPc1	všechen
její	její	k3xOp3gMnPc1	její
předchůdci	předchůdce	k1gMnPc1	předchůdce
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
jako	jako	k8xS	jako
poslankyně	poslankyně	k1gFnSc2	poslankyně
za	za	k7c4	za
britskou	britský	k2eAgFnSc4d1	britská
Konzervativní	konzervativní	k2eAgFnSc4d1	konzervativní
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
ji	on	k3xPp3gFnSc4	on
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Edward	Edward	k1gMnSc1	Edward
Heath	Heath	k1gMnSc1	Heath
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
ministryní	ministryně	k1gFnSc7	ministryně
pro	pro	k7c4	pro
školství	školství	k1gNnSc4	školství
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Heathově	Heathův	k2eAgFnSc6d1	Heathova
prohře	prohra	k1gFnSc6	prohra
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
únorových	únorový	k2eAgFnPc6d1	únorová
i	i	k8xC	i
říjnových	říjnový	k2eAgFnPc6d1	říjnová
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
vůdkyní	vůdkyně	k1gFnSc7	vůdkyně
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
ministerskou	ministerský	k2eAgFnSc7d1	ministerská
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členkou	členka	k1gFnSc7	členka
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
již	již	k6eAd1	již
jejích	její	k3xOp3gNnPc2	její
zasedání	zasedání	k1gNnPc2	zasedání
neúčastnila	účastnit	k5eNaImAgFnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
britských	britský	k2eAgFnPc2d1	britská
vlád	vláda	k1gFnPc2	vláda
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgFnPc2	tři
volebních	volební	k2eAgFnPc2d1	volební
období	období	k1gNnSc6	období
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
konzervativní	konzervativní	k2eAgFnPc4d1	konzervativní
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
rázných	rázný	k2eAgFnPc2d1	rázná
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
posílit	posílit	k5eAaPmF	posílit
tržní	tržní	k2eAgFnPc4d1	tržní
hospodářství	hospodářství	k1gNnPc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
thatcherismus	thatcherismus	k1gInSc4	thatcherismus
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
liberalismu	liberalismus	k1gInSc6	liberalismus
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xS	jako
snižování	snižování	k1gNnSc4	snižování
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
omezování	omezování	k1gNnSc1	omezování
vlivu	vliv	k1gInSc2	vliv
odborů	odbor	k1gInPc2	odbor
a	a	k8xC	a
regulaci	regulace	k1gFnSc4	regulace
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nekompromisnost	nekompromisnost	k1gFnSc4	nekompromisnost
a	a	k8xC	a
rozhodnost	rozhodnost	k1gFnSc4	rozhodnost
získala	získat	k5eAaPmAgFnS	získat
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
přezdívku	přezdívka	k1gFnSc4	přezdívka
Železná	železný	k2eAgFnSc1d1	železná
lady	lady	k1gFnSc1	lady
<g/>
.	.	kIx.	.
</s>
<s>
Prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
také	také	k9	také
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
a	a	k8xC	a
silnou	silný	k2eAgFnSc4d1	silná
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výrazem	výraz	k1gInSc7	výraz
byly	být	k5eAaImAgFnP	být
četné	četný	k2eAgFnPc1d1	četná
výjimky	výjimka	k1gFnPc1	výjimka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pro	pro	k7c4	pro
zemi	zem	k1gFnSc4	zem
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Falklandy	Falklanda	k1gFnPc4	Falklanda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Grantham	Grantham	k1gInSc1	Grantham
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Lincolnshire	Lincolnshir	k1gInSc5	Lincolnshir
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Alfred	Alfred	k1gMnSc1	Alfred
Roberts	Roberts	k1gInSc4	Roberts
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
hokynářství	hokynářství	k1gNnPc4	hokynářství
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
komunální	komunální	k2eAgFnSc6d1	komunální
politice	politika	k1gFnSc6	politika
jako	jako	k9	jako
příslušník	příslušník	k1gMnSc1	příslušník
liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
též	též	k6eAd1	též
laickým	laický	k2eAgMnSc7d1	laický
metodistickým	metodistický	k2eAgMnSc7d1	metodistický
kazatelem	kazatel	k1gMnSc7	kazatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
radě	rada	k1gFnSc6	rada
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Labouristická	labouristický	k2eAgFnSc1d1	labouristická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
sestru	sestra	k1gFnSc4	sestra
Muriel	Muriela	k1gFnPc2	Muriela
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
docházela	docházet	k5eAaImAgFnS	docházet
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
v	v	k7c4	v
Kestevenu	Kesteven	k2eAgFnSc4d1	Kesteven
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
studovala	studovat	k5eAaImAgFnS	studovat
Somerville	Somerville	k1gFnSc1	Somerville
College	Colleg	k1gFnSc2	Colleg
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
začala	začít	k5eAaPmAgFnS	začít
studovat	studovat	k5eAaImF	studovat
chemii	chemie	k1gFnSc4	chemie
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
poté	poté	k6eAd1	poté
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
chemická	chemický	k2eAgFnSc1d1	chemická
laborantka	laborantka	k1gFnSc1	laborantka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
i	i	k9	i
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
komunální	komunální	k2eAgFnSc6d1	komunální
politice	politika	k1gFnSc6	politika
v	v	k7c6	v
Dartfordu	Dartford	k1gInSc6	Dartford
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
mluvčí	mluvčí	k1gFnSc1	mluvčí
konzervativců	konzervativec	k1gMnPc2	konzervativec
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
bydlení	bydlení	k1gNnSc2	bydlení
a	a	k8xC	a
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
stínovou	stínový	k2eAgFnSc7d1	stínová
ministryní	ministryně	k1gFnSc7	ministryně
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
stínovou	stínový	k2eAgFnSc7d1	stínová
ministryní	ministryně	k1gFnSc7	ministryně
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
poslankyň	poslankyně	k1gFnPc2	poslankyně
například	například	k6eAd1	například
podpořila	podpořit	k5eAaPmAgFnS	podpořit
dekriminalizaci	dekriminalizace	k1gFnSc4	dekriminalizace
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
ministryní	ministryně	k1gFnSc7	ministryně
školství	školství	k1gNnSc4	školství
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Edwarda	Edward	k1gMnSc2	Edward
Heatha	Heath	k1gMnSc2	Heath
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgInPc4d1	veřejný
protesty	protest	k1gInPc4	protest
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
její	její	k3xOp3gNnSc1	její
zrušení	zrušení	k1gNnSc1	zrušení
podávání	podávání	k1gNnSc3	podávání
mléka	mléko	k1gNnSc2	mléko
zdarma	zdarma	k6eAd1	zdarma
pro	pro	k7c4	pro
školní	školní	k2eAgFnPc4d1	školní
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
byla	být	k5eAaImAgNnP	být
zaměřena	zaměřit	k5eAaPmNgNnP	zaměřit
na	na	k7c6	na
podporu	podpor	k1gInSc6	podpor
úplného	úplný	k2eAgNnSc2d1	úplné
sekundárního	sekundární	k2eAgNnSc2d1	sekundární
školství	školství	k1gNnSc2	školství
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
prostého	prostý	k2eAgNnSc2d1	prosté
základního	základní	k2eAgNnSc2d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
též	též	k9	též
uchránit	uchránit	k5eAaPmF	uchránit
nově	nově	k6eAd1	nově
zřízenou	zřízený	k2eAgFnSc7d1	zřízená
Open	Open	k1gNnSc4	Open
University	universita	k1gFnSc2	universita
před	před	k7c7	před
zrušením	zrušení	k1gNnSc7	zrušení
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
omezovala	omezovat	k5eAaImAgFnS	omezovat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
neproduktivním	produktivní	k2eNgInPc3d1	neproduktivní
vzdělávacím	vzdělávací	k2eAgInPc3d1	vzdělávací
programům	program	k1gInPc3	program
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
znepřátelila	znepřátelit	k5eAaPmAgFnS	znepřátelit
levicové	levicový	k2eAgMnPc4d1	levicový
akademické	akademický	k2eAgMnPc4d1	akademický
funkcionáře	funkcionář	k1gMnPc4	funkcionář
(	(	kIx(	(
<g/>
jichž	jenž	k3xRgFnPc2	jenž
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
Konzervativců	konzervativec	k1gMnPc2	konzervativec
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
stínovou	stínový	k2eAgFnSc7d1	stínová
ministryní	ministryně	k1gFnSc7	ministryně
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
Edwarda	Edward	k1gMnSc2	Edward
Heatha	Heath	k1gMnSc2	Heath
zvolena	zvolit	k5eAaPmNgFnS	zvolit
vůdkyní	vůdkyně	k1gFnSc7	vůdkyně
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
útočně	útočně	k6eAd1	útočně
proti	proti	k7c3	proti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
projevů	projev	k1gInPc2	projev
jí	jíst	k5eAaImIp3nS	jíst
vynesl	vynést	k5eAaPmAgInS	vynést
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
novinách	novina	k1gFnPc6	novina
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
přezdívku	přezdívka	k1gFnSc4	přezdívka
Železná	železný	k2eAgFnSc1d1	železná
lady	lady	k1gFnSc1	lady
<g/>
.	.	kIx.	.
</s>
<s>
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
přestala	přestat	k5eAaPmAgFnS	přestat
podporovat	podporovat	k5eAaImF	podporovat
dřívější	dřívější	k2eAgFnPc4d1	dřívější
iniciativy	iniciativa	k1gFnPc4	iniciativa
konzervativců	konzervativec	k1gMnPc2	konzervativec
k	k	k7c3	k
přenesení	přenesení	k1gNnSc3	přenesení
vládních	vládní	k2eAgFnPc2d1	vládní
kompetencí	kompetence	k1gFnPc2	kompetence
pro	pro	k7c4	pro
Skotsko	Skotsko	k1gNnSc4	Skotsko
<g/>
,	,	kIx,	,
kritiku	kritika	k1gFnSc4	kritika
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
i	i	k9	i
její	její	k3xOp3gInPc1	její
výroky	výrok	k1gInPc1	výrok
o	o	k7c6	o
lidech	lid	k1gInPc6	lid
jiné	jiný	k2eAgFnSc2d1	jiná
barvy	barva	k1gFnSc2	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
dokázala	dokázat	k5eAaPmAgFnS	dokázat
dobře	dobře	k6eAd1	dobře
využít	využít	k5eAaPmF	využít
vlny	vlna	k1gFnPc4	vlna
stávek	stávka	k1gFnPc2	stávka
a	a	k8xC	a
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
za	za	k7c2	za
labouristické	labouristický	k2eAgFnSc2d1	labouristická
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
vlády	vláda	k1gFnSc2	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
se	se	k3xPyFc4	se
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
stala	stát	k5eAaPmAgFnS	stát
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1979	[number]	k4	1979
a	a	k8xC	a
jako	jako	k9	jako
hlavní	hlavní	k2eAgInPc4d1	hlavní
úkoly	úkol	k1gInPc4	úkol
si	se	k3xPyFc3	se
vytyčila	vytyčit	k5eAaPmAgFnS	vytyčit
zastavení	zastavení	k1gNnSc4	zastavení
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
propadu	propad	k1gInSc2	propad
a	a	k8xC	a
zeslabení	zeslabení	k1gNnSc1	zeslabení
role	role	k1gFnSc2	role
státu	stát	k1gInSc2	stát
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
posílit	posílit	k5eAaPmF	posílit
pozici	pozice	k1gFnSc4	pozice
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgInPc7d1	americký
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedenými	vedený	k2eAgInPc7d1	vedený
prezidentem	prezident	k1gMnSc7	prezident
Ronaldem	Ronald	k1gMnSc7	Ronald
Reaganem	Reagan	k1gMnSc7	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
musela	muset	k5eAaImAgFnS	muset
čelit	čelit	k5eAaImF	čelit
hladovce	hladovka	k1gFnSc3	hladovka
přívrženců	přívrženec	k1gMnPc2	přívrženec
Irské	irský	k2eAgFnSc2d1	irská
republikánské	republikánský	k2eAgFnSc2d1	republikánská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
domáhali	domáhat	k5eAaImAgMnP	domáhat
obnovení	obnovení	k1gNnSc4	obnovení
statutu	statut	k1gInSc2	statut
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
kolegy	kolega	k1gMnPc4	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
tyto	tento	k3xDgInPc4	tento
požadavky	požadavek	k1gInPc4	požadavek
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
připustila	připustit	k5eAaPmAgFnS	připustit
v	v	k7c6	v
Anglo-irské	Anglorský	k2eAgFnSc6d1	Anglo-irská
dohodě	dohoda	k1gFnSc6	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
právo	právo	k1gNnSc4	právo
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
společný	společný	k2eAgInSc4d1	společný
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
přívrženkyni	přívrženkyně	k1gFnSc4	přívrženkyně
teoretického	teoretický	k2eAgInSc2d1	teoretický
směru	směr	k1gInSc2	směr
zvaného	zvaný	k2eAgInSc2d1	zvaný
monetarismus	monetarismus	k1gInSc1	monetarismus
<g/>
,	,	kIx,	,
amerického	americký	k2eAgMnSc4d1	americký
ekonoma	ekonom	k1gMnSc4	ekonom
Miltona	Milton	k1gMnSc4	Milton
Friedmana	Friedman	k1gMnSc4	Friedman
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
ale	ale	k8xC	ale
také	také	k9	také
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
rakouské	rakouský	k2eAgFnSc6d1	rakouská
škole	škola	k1gFnSc6	škola
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
čelným	čelný	k2eAgMnSc7d1	čelný
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Friedrich	Friedrich	k1gMnSc1	Friedrich
Hayek	Hayek	k1gMnSc1	Hayek
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
monetarismus	monetarismus	k1gInSc1	monetarismus
a	a	k8xC	a
rakouská	rakouský	k2eAgFnSc1d1	rakouská
škola	škola	k1gFnSc1	škola
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
peněžní	peněžní	k2eAgFnSc2d1	peněžní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňovala	upřednostňovat	k5eAaImAgFnS	upřednostňovat
nepřímé	přímý	k2eNgFnPc4d1	nepřímá
daně	daň	k1gFnPc4	daň
před	před	k7c7	před
daněmi	daň	k1gFnPc7	daň
přímými	přímý	k2eAgFnPc7d1	přímá
<g/>
,	,	kIx,	,
především	především	k9	především
před	před	k7c7	před
daní	daň	k1gFnSc7	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
převzala	převzít	k5eAaPmAgFnS	převzít
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
"	"	kIx"	"
<g/>
nemocným	nemocný	k2eAgMnSc7d1	nemocný
mužem	muž	k1gMnSc7	muž
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
procházela	procházet	k5eAaImAgFnS	procházet
obdobím	období	k1gNnSc7	období
hluboké	hluboký	k2eAgFnSc2d1	hluboká
stagnace	stagnace	k1gFnSc2	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Propadla	propadnout	k5eAaPmAgFnS	propadnout
se	se	k3xPyFc4	se
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
za	za	k7c4	za
SRN	SRN	kA	SRN
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
)	)	kIx)	)
jak	jak	k8xS	jak
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
ropné	ropný	k2eAgFnSc6d1	ropná
krizi	krize	k1gFnSc6	krize
a	a	k8xC	a
pádu	pád	k1gInSc6	pád
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
dokonce	dokonce	k9	dokonce
její	její	k3xOp3gInSc4	její
HDP	HDP	kA	HDP
klesal	klesat	k5eAaImAgMnS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
zaměstnávající	zaměstnávající	k2eAgFnPc1d1	zaměstnávající
přes	přes	k7c4	přes
30	[number]	k4	30
%	%	kIx~	%
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
krachovaly	krachovat	k5eAaBmAgFnP	krachovat
jeden	jeden	k4xCgInSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
moc	moc	k1gFnSc1	moc
odborů	odbor	k1gInPc2	odbor
bránila	bránit	k5eAaImAgFnS	bránit
jejich	jejich	k3xOp3gFnSc3	jejich
restrukturalizaci	restrukturalizace	k1gFnSc3	restrukturalizace
či	či	k8xC	či
jejich	jejich	k3xOp3gFnSc4	jejich
likvidaci	likvidace	k1gFnSc4	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
vlády	vláda	k1gFnSc2	vláda
přitom	přitom	k6eAd1	přitom
stále	stále	k6eAd1	stále
rostly	růst	k5eAaImAgFnP	růst
<g/>
,	,	kIx,	,
vzdor	vzdor	k1gInSc4	vzdor
klesajícímu	klesající	k2eAgInSc3d1	klesající
HDP	HDP	kA	HDP
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Británie	Británie	k1gFnSc1	Británie
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zadlužovala	zadlužovat	k5eAaImAgFnS	zadlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc1	inflace
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
18	[number]	k4	18
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
opatřeními	opatření	k1gNnPc7	opatření
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
snižování	snižování	k1gNnSc1	snižování
vládních	vládní	k2eAgInPc2d1	vládní
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
<g/>
,	,	kIx,	,
či	či	k8xC	či
zastavení	zastavení	k1gNnSc1	zastavení
podpory	podpora	k1gFnSc2	podpora
krachujícím	krachující	k2eAgInPc3d1	krachující
podnikům	podnik	k1gInPc3	podnik
<g/>
,	,	kIx,	,
privatizace	privatizace	k1gFnSc2	privatizace
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ještě	ještě	k9	ještě
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zachránit	zachránit	k5eAaPmF	zachránit
a	a	k8xC	a
stabilizace	stabilizace	k1gFnSc2	stabilizace
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitým	okamžitý	k2eAgInSc7d1	okamžitý
výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgNnPc2	tento
opatření	opatření	k1gNnPc2	opatření
bylo	být	k5eAaImAgNnS	být
prudké	prudký	k2eAgNnSc1d1	prudké
zvýšení	zvýšení	k1gNnSc1	zvýšení
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
z	z	k7c2	z
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
z	z	k7c2	z
necelých	celý	k2eNgNnPc2d1	necelé
5	[number]	k4	5
%	%	kIx~	%
na	na	k7c6	na
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
nebyla	být	k5eNaImAgFnS	být
Británie	Británie	k1gFnSc1	Británie
ani	ani	k8xC	ani
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
připravená	připravený	k2eAgFnSc1d1	připravená
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
mohutné	mohutný	k2eAgInPc4d1	mohutný
protesty	protest	k1gInPc4	protest
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
stávkou	stávka	k1gFnSc7	stávka
pořádanou	pořádaný	k2eAgFnSc4d1	pořádaná
Odborovým	odborový	k2eAgInSc7d1	odborový
svazem	svaz	k1gInSc7	svaz
horníků	horník	k1gMnPc2	horník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
uzavřením	uzavření	k1gNnSc7	uzavření
dvaceti	dvacet	k4xCc2	dvacet
ztrátových	ztrátový	k2eAgInPc2d1	ztrátový
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Stávka	stávka	k1gFnSc1	stávka
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgFnSc1d1	trvající
od	od	k7c2	od
března	březen	k1gInSc2	březen
1984	[number]	k4	1984
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1985	[number]	k4	1985
byla	být	k5eAaImAgFnS	být
soudem	soud	k1gInSc7	soud
prohlášena	prohlášen	k2eAgFnSc1d1	prohlášena
za	za	k7c4	za
nezákonnou	zákonný	k2eNgFnSc4d1	nezákonná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vůdce	vůdce	k1gMnSc1	vůdce
svazu	svaz	k1gInSc2	svaz
Arthur	Arthur	k1gMnSc1	Arthur
Scargill	Scargill	k1gMnSc1	Scargill
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
hlasování	hlasování	k1gNnSc4	hlasování
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
konání	konání	k1gNnSc6	konání
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
neúspěšné	úspěšný	k2eNgFnSc2d1	neúspěšná
stávky	stávka	k1gFnSc2	stávka
bylo	být	k5eAaImAgNnS	být
uzavření	uzavření	k1gNnSc1	uzavření
dvaceti	dvacet	k4xCc2	dvacet
pěti	pět	k4xCc2	pět
dolů	dol	k1gInPc2	dol
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
97	[number]	k4	97
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
stávce	stávka	k1gFnSc3	stávka
horníků	horník	k1gMnPc2	horník
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
jejího	její	k3xOp3gNnSc2	její
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
oslabení	oslabení	k1gNnSc4	oslabení
doposud	doposud	k6eAd1	doposud
velmi	velmi	k6eAd1	velmi
silného	silný	k2eAgInSc2d1	silný
vlivu	vliv	k1gInSc2	vliv
odborů	odbor	k1gInPc2	odbor
v	v	k7c6	v
hospodářském	hospodářský	k2eAgInSc6d1	hospodářský
životě	život	k1gInSc6	život
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
její	její	k3xOp3gFnSc2	její
politiky	politika	k1gFnSc2	politika
bylo	být	k5eAaImAgNnS	být
odborové	odborový	k2eAgNnSc1d1	odborové
hnutí	hnutí	k1gNnSc1	hnutí
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
ochromeno	ochromen	k2eAgNnSc4d1	ochromeno
a	a	k8xC	a
demoralizováno	demoralizován	k2eAgNnSc4d1	demoralizován
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
klesala	klesat	k5eAaImAgFnS	klesat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
činila	činit	k5eAaImAgFnS	činit
7	[number]	k4	7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Inflaci	inflace	k1gFnSc3	inflace
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
;	;	kIx,	;
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
období	období	k1gNnSc2	období
výrazně	výrazně	k6eAd1	výrazně
klesla	klesnout	k5eAaPmAgFnS	klesnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
opět	opět	k6eAd1	opět
mírně	mírně	k6eAd1	mírně
stoupat	stoupat	k5eAaImF	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
politiky	politika	k1gFnSc2	politika
Thatcherové	Thatcherový	k2eAgFnSc2d1	Thatcherová
a	a	k8xC	a
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
bylo	být	k5eAaImAgNnS	být
paradoxně	paradoxně	k6eAd1	paradoxně
vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
konzervativci	konzervativec	k1gMnPc1	konzervativec
získali	získat	k5eAaPmAgMnP	získat
většinu	většina	k1gFnSc4	většina
397	[number]	k4	397
křesel	křeslo	k1gNnPc2	křeslo
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
339	[number]	k4	339
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
státního	státní	k2eAgInSc2d1	státní
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
privatizaci	privatizace	k1gFnSc4	privatizace
státem	stát	k1gInSc7	stát
vlastněných	vlastněný	k2eAgInPc2d1	vlastněný
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kroky	krok	k1gInPc1	krok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
politiky	politika	k1gFnSc2	politika
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
řadu	řada	k1gFnSc4	řada
protestů	protest	k1gInPc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
její	její	k3xOp3gFnSc3	její
mateřská	mateřský	k2eAgFnSc1d1	mateřská
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
univerzita	univerzita	k1gFnSc1	univerzita
jí	on	k3xPp3gFnSc3	on
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
udělit	udělit	k5eAaPmF	udělit
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zpravidla	zpravidla	k6eAd1	zpravidla
dostávali	dostávat	k5eAaImAgMnP	dostávat
absolventi	absolvent	k1gMnPc1	absolvent
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
předsedy	předseda	k1gMnPc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prosadila	prosadit	k5eAaPmAgFnS	prosadit
také	také	k9	také
přesunutí	přesunutí	k1gNnSc4	přesunutí
části	část	k1gFnSc2	část
daní	daň	k1gFnPc2	daň
na	na	k7c4	na
lokální	lokální	k2eAgFnSc4d1	lokální
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
daně	daň	k1gFnPc1	daň
byly	být	k5eAaImAgFnP	být
však	však	k9	však
poté	poté	k6eAd1	poté
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
nepopularita	nepopularita	k1gFnSc1	nepopularita
těchto	tento	k3xDgFnPc2	tento
dávek	dávka	k1gFnPc2	dávka
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
jejího	její	k3xOp3gInSc2	její
politického	politický	k2eAgInSc2d1	politický
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
požadovala	požadovat	k5eAaImAgFnS	požadovat
zaručení	zaručení	k1gNnSc4	zaručení
zpětných	zpětný	k2eAgFnPc2d1	zpětná
plateb	platba	k1gFnPc2	platba
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
výši	výše	k1gFnSc6	výše
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
vlastní	vlastní	k2eAgFnPc1d1	vlastní
britské	britský	k2eAgFnPc1d1	britská
platby	platba	k1gFnPc1	platba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
je	být	k5eAaImIp3nS	být
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
přiznán	přiznán	k2eAgInSc4d1	přiznán
každoroční	každoroční	k2eAgInSc4d1	každoroční
rabat	rabat	k1gInSc4	rabat
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
66	[number]	k4	66
%	%	kIx~	%
rozdílu	rozdíl	k1gInSc2	rozdíl
mezi	mezi	k7c7	mezi
platbami	platba	k1gFnPc7	platba
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
britskými	britský	k2eAgInPc7d1	britský
příjmy	příjem	k1gInPc7	příjem
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
redukci	redukce	k1gFnSc4	redukce
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
úrovni	úroveň	k1gFnSc6	úroveň
vedeny	vést	k5eAaImNgFnP	vést
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
rozporu	rozpor	k1gInSc2	rozpor
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
Nigelem	Nigel	k1gMnSc7	Nigel
Lawsonem	Lawson	k1gMnSc7	Lawson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
připravoval	připravovat	k5eAaImAgMnS	připravovat
Británii	Británie	k1gFnSc4	Británie
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
měnového	měnový	k2eAgInSc2d1	měnový
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
EMS	EMS	kA	EMS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
k	k	k7c3	k
ustavení	ustavení	k1gNnSc3	ustavení
společné	společný	k2eAgFnSc2d1	společná
evropské	evropský	k2eAgFnSc2d1	Evropská
měny	měna	k1gFnSc2	měna
(	(	kIx(	(
<g/>
eura	euro	k1gNnSc2	euro
<g/>
)	)	kIx)	)
a	a	k8xC	a
eventuálnímu	eventuální	k2eAgNnSc3d1	eventuální
zrušení	zrušení	k1gNnSc3	zrušení
britské	britský	k2eAgFnSc2d1	britská
libry	libra	k1gFnSc2	libra
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
dodnes	dodnes	k6eAd1	dodnes
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
její	její	k3xOp3gInSc4	její
vláda	vláda	k1gFnSc1	vláda
odmítala	odmítat	k5eAaImAgFnS	odmítat
politiku	politika	k1gFnSc4	politika
embarga	embargo	k1gNnSc2	embargo
OSN	OSN	kA	OSN
vůči	vůči	k7c3	vůči
režimu	režim	k1gInSc3	režim
apartheidu	apartheid	k1gInSc2	apartheid
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
embargo	embargo	k1gNnSc1	embargo
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
dodržovala	dodržovat	k5eAaImAgFnS	dodržovat
<g/>
;	;	kIx,	;
<g/>
[	[	kIx(	[
<g/>
ujasnit	ujasnit	k5eAaPmF	ujasnit
<g/>
]	]	kIx)	]
Africký	africký	k2eAgInSc4d1	africký
národní	národní	k2eAgInSc4d1	národní
kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
za	za	k7c4	za
nějž	jenž	k3xRgMnSc4	jenž
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Nelson	Nelson	k1gMnSc1	Nelson
Mandela	Mandela	k1gFnSc1	Mandela
<g/>
,	,	kIx,	,
označila	označit	k5eAaPmAgFnS	označit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
za	za	k7c4	za
"	"	kIx"	"
<g/>
typickou	typický	k2eAgFnSc4d1	typická
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
organizaci	organizace	k1gFnSc4	organizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1982	[number]	k4	1982
provedla	provést	k5eAaPmAgFnS	provést
armáda	armáda	k1gFnSc1	armáda
Argentiny	Argentina	k1gFnSc2	Argentina
invazi	invaze	k1gFnSc4	invaze
na	na	k7c6	na
Falklandy	Falklanda	k1gFnSc2	Falklanda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
britské	britský	k2eAgNnSc4d1	Britské
zámořské	zámořský	k2eAgNnSc4d1	zámořské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
Argentina	Argentina	k1gFnSc1	Argentina
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
nárok	nárok	k1gInSc4	nárok
již	již	k6eAd1	již
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
obsazení	obsazení	k1gNnSc2	obsazení
Velkou	velká	k1gFnSc7	velká
Británií	Británie	k1gFnSc7	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
a	a	k8xC	a
těžkých	těžký	k2eAgFnPc6d1	těžká
ztrátách	ztráta	k1gFnPc6	ztráta
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
Argentiny	Argentina	k1gFnSc2	Argentina
i	i	k8xC	i
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
byly	být	k5eAaImAgFnP	být
boje	boj	k1gInSc2	boj
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
britským	britský	k2eAgNnSc7d1	Britské
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byly	být	k5eAaImAgInP	být
posíleny	posílen	k2eAgInPc1d1	posílen
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
diktaturou	diktatura	k1gFnSc7	diktatura
Augusta	August	k1gMnSc2	August
Pinocheta	Pinochet	k1gMnSc2	Pinochet
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
;	;	kIx,	;
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
obdivovala	obdivovat	k5eAaImAgFnS	obdivovat
reformy	reforma	k1gFnPc4	reforma
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
jeho	jeho	k3xOp3gInSc4	jeho
režim	režim	k1gInSc4	režim
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
vzedmutím	vzedmutí	k1gNnSc7	vzedmutí
militaristické	militaristický	k2eAgFnSc2d1	militaristická
vlny	vlna	k1gFnSc2	vlna
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
úloha	úloha	k1gFnSc1	úloha
ve	v	k7c6	v
falklandské	falklandský	k2eAgFnSc6d1	falklandská
válce	válka	k1gFnSc6	válka
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
dodnes	dodnes	k6eAd1	dodnes
kontroverze	kontroverze	k1gFnSc1	kontroverze
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
její	její	k3xOp3gFnPc4	její
volební	volební	k2eAgFnPc4d1	volební
preference	preference	k1gFnPc4	preference
během	během	k7c2	během
války	válka	k1gFnSc2	válka
vzrostly	vzrůst	k5eAaPmAgInP	vzrůst
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgInP	umožnit
jí	on	k3xPp3gFnSc3	on
překonat	překonat	k5eAaPmF	překonat
nepříznivé	příznivý	k2eNgInPc4d1	nepříznivý
vlivy	vliv	k1gInPc4	vliv
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
období	období	k1gNnSc6	období
jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
trápené	trápený	k2eAgNnSc1d1	trápené
vysokou	vysoký	k2eAgFnSc7d1	vysoká
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
a	a	k8xC	a
inflací	inflace	k1gFnSc7	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
mezi	mezi	k7c7	mezi
západními	západní	k2eAgFnPc7d1	západní
demokraciemi	demokracie	k1gFnPc7	demokracie
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
socialistickým	socialistický	k2eAgInSc7d1	socialistický
táborem	tábor	k1gInSc7	tábor
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
na	na	k7c6	na
ostře	ostro	k6eAd1	ostro
protisovětském	protisovětský	k2eAgInSc6d1	protisovětský
postoji	postoj	k1gInSc6	postoj
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
tuto	tento	k3xDgFnSc4	tento
svoji	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
jen	jen	k6eAd1	jen
zmírnila	zmírnit	k5eAaPmAgFnS	zmírnit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Michaila	Michail	k1gMnSc2	Michail
Gorbačova	Gorbačův	k2eAgMnSc2d1	Gorbačův
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
této	tento	k3xDgFnSc2	tento
velmoci	velmoc	k1gFnSc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
masového	masový	k2eAgNnSc2d1	masové
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
jaderné	jaderný	k2eAgNnSc4d1	jaderné
odzbrojení	odzbrojení	k1gNnSc4	odzbrojení
a	a	k8xC	a
převahy	převaha	k1gFnPc4	převaha
politiky	politika	k1gFnSc2	politika
détente	détente	k2eAgFnSc2d1	détente
v	v	k7c6	v
Západní	západní	k2eAgFnSc3d1	západní
Evropě	Evropa	k1gFnSc3	Evropa
svolila	svolit	k5eAaPmAgFnS	svolit
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
amerických	americký	k2eAgFnPc2d1	americká
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
atomovými	atomový	k2eAgFnPc7d1	atomová
hlavicemi	hlavice	k1gFnPc7	hlavice
v	v	k7c6	v
britských	britský	k2eAgFnPc6d1	britská
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
vedlo	vést	k5eAaImAgNnS	vést
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
znovuzprovoznil	znovuzprovoznit	k5eAaImAgMnS	znovuzprovoznit
flotilu	flotila	k1gFnSc4	flotila
ponorek	ponorka	k1gFnPc2	ponorka
667A	[number]	k4	667A
třídy	třída	k1gFnSc2	třída
Yankee	yankee	k1gMnPc4	yankee
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
tak	tak	k6eAd1	tak
svými	svůj	k3xOyFgMnPc7	svůj
24	[number]	k4	24
ponorkami	ponorka	k1gFnPc7	ponorka
s	s	k7c7	s
16	[number]	k4	16
jadernými	jaderný	k2eAgFnPc7d1	jaderná
sily	silo	k1gNnPc7	silo
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
pobřeží	pobřeží	k1gNnSc1	pobřeží
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
vedlo	vést	k5eAaImAgNnS	vést
až	až	k9	až
ke	k	k7c3	k
střetu	střet	k1gInSc2	střet
dvou	dva	k4xCgFnPc2	dva
ponorek	ponorka	k1gFnPc2	ponorka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
a	a	k8xC	a
potopení	potopení	k1gNnSc4	potopení
sovětské	sovětský	k2eAgFnSc2d1	sovětská
ponorky	ponorka	k1gFnSc2	ponorka
K-219	K-219	k1gFnSc2	K-219
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
ponorkou	ponorka	k1gFnSc7	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yIgFnPc4	který
v	v	k7c6	v
Sargasovém	sargasový	k2eAgNnSc6d1	Sargasové
moři	moře	k1gNnSc6	moře
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
sovětská	sovětský	k2eAgFnSc1d1	sovětská
ponorka	ponorka	k1gFnSc1	ponorka
s	s	k7c7	s
48	[number]	k4	48
jadernými	jaderný	k2eAgFnPc7d1	jaderná
hlavicemi	hlavice	k1gFnPc7	hlavice
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
jadernými	jaderný	k2eAgInPc7d1	jaderný
reaktory	reaktor	k1gInPc7	reaktor
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
Černobylské	černobylský	k2eAgFnSc3d1	Černobylská
jaderné	jaderný	k2eAgFnSc3d1	jaderná
katastrofě	katastrofa	k1gFnSc3	katastrofa
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
INF	INF	kA	INF
i	i	k8xC	i
přes	přes	k7c4	přes
její	její	k3xOp3gInSc4	její
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
bývalého	bývalý	k2eAgMnSc2d1	bývalý
německého	německý	k2eAgMnSc2d1	německý
kancléře	kancléř	k1gMnSc2	kancléř
Helmuta	Helmut	k1gMnSc2	Helmut
Kohla	Kohl	k1gMnSc2	Kohl
byla	být	k5eAaImAgFnS	být
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
odpůrkyní	odpůrkyně	k1gFnPc2	odpůrkyně
znovusjednocení	znovusjednocení	k1gNnSc2	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
aktivní	aktivní	k2eAgNnSc1d1	aktivní
angažmá	angažmá	k1gNnSc1	angažmá
proti	proti	k7c3	proti
rychlému	rychlý	k2eAgNnSc3d1	rychlé
sjednocení	sjednocení	k1gNnSc3	sjednocení
Německa	Německo	k1gNnSc2	Německo
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
i	i	k9	i
dokumenty	dokument	k1gInPc1	dokument
zpřístupněné	zpřístupněný	k2eAgInPc1d1	zpřístupněný
veřejnosti	veřejnost	k1gFnSc6	veřejnost
britským	britský	k2eAgNnSc7d1	Britské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
britské	britský	k2eAgFnSc2d1	britská
korunní	korunní	k2eAgFnSc2d1	korunní
kolonie	kolonie	k1gFnSc2	kolonie
Hongkongu	Hongkong	k1gInSc2	Hongkong
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
této	tento	k3xDgFnSc2	tento
kolonie	kolonie	k1gFnSc2	kolonie
k	k	k7c3	k
Čínské	čínský	k2eAgFnSc3d1	čínská
lidové	lidový	k2eAgFnSc3d1	lidová
republice	republika	k1gFnSc3	republika
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
výhradou	výhrada	k1gFnSc7	výhrada
<g/>
,	,	kIx,	,
že	že	k8xS	že
následujících	následující	k2eAgNnPc2d1	následující
50	[number]	k4	50
let	léto	k1gNnPc2	léto
bude	být	k5eAaImBp3nS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
politická	politický	k2eAgFnSc1d1	politická
autonomie	autonomie	k1gFnSc1	autonomie
Hongkongu	Hongkong	k1gInSc2	Hongkong
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
privilegii	privilegium	k1gNnPc7	privilegium
a	a	k8xC	a
odlišnostmi	odlišnost	k1gFnPc7	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgNnP	být
za	za	k7c2	za
britské	britský	k2eAgFnSc2d1	britská
účasti	účast	k1gFnSc2	účast
zahájena	zahájit	k5eAaPmNgFnS	zahájit
druhá	druhý	k4xOgFnSc1	druhý
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
Irákem	Irák	k1gInSc7	Irák
okupovaného	okupovaný	k2eAgInSc2d1	okupovaný
státu	stát	k1gInSc2	stát
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
nejméně	málo	k6eAd3	málo
populárním	populární	k2eAgMnPc3d1	populární
ministerským	ministerský	k2eAgMnPc3d1	ministerský
předsedům	předseda	k1gMnPc3	předseda
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
popularita	popularita	k1gFnSc1	popularita
nepřekročila	překročit	k5eNaPmAgFnS	překročit
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
války	válka	k1gFnSc2	válka
o	o	k7c4	o
Falklandy	Falklanda	k1gFnPc4	Falklanda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gFnSc1	její
popularita	popularita	k1gFnSc1	popularita
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
55	[number]	k4	55
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
jako	jako	k8xC	jako
taková	takový	k3xDgFnSc1	takový
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
oblíbenější	oblíbený	k2eAgMnSc1d2	oblíbenější
<g/>
,	,	kIx,	,
než	než	k8xS	než
její	její	k3xOp3gFnSc1	její
ministryně	ministryně	k1gFnSc1	ministryně
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
nepopularita	nepopularita	k1gFnSc1	nepopularita
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
především	především	k9	především
se	s	k7c7	s
škrty	škrt	k1gInPc7	škrt
ve	v	k7c6	v
veřejných	veřejný	k2eAgInPc6d1	veřejný
výdajích	výdaj	k1gInPc6	výdaj
a	a	k8xC	a
s	s	k7c7	s
ostrým	ostrý	k2eAgInSc7d1	ostrý
postupem	postup	k1gInSc7	postup
proti	proti	k7c3	proti
odborům	odbor	k1gInPc3	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
sice	sice	k8xC	sice
často	často	k6eAd1	často
ochromovaly	ochromovat	k5eAaImAgInP	ochromovat
zemi	zem	k1gFnSc3	zem
stávkami	stávka	k1gFnPc7	stávka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
postup	postup	k1gInSc4	postup
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vlády	vláda	k1gFnSc2	vláda
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
eskalaci	eskalace	k1gFnSc3	eskalace
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
symbolem	symbol	k1gInSc7	symbol
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
trvající	trvající	k2eAgFnSc1d1	trvající
stávka	stávka	k1gFnSc1	stávka
horníků	horník	k1gMnPc2	horník
proti	proti	k7c3	proti
propouštění	propouštění	k1gNnSc3	propouštění
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Horníci	Horník	k1gMnPc1	Horník
boj	boj	k1gInSc4	boj
nakonec	nakonec	k6eAd1	nakonec
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
,	,	kIx,	,
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ale	ale	k8xC	ale
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
rapidně	rapidně	k6eAd1	rapidně
klesala	klesat	k5eAaImAgFnS	klesat
popularita	popularita	k1gFnSc1	popularita
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
konzervativci	konzervativec	k1gMnPc1	konzervativec
prohráli	prohrát	k5eAaPmAgMnP	prohrát
evropské	evropský	k2eAgFnPc4d1	Evropská
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
narůstalo	narůstat	k5eAaImAgNnS	narůstat
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
pochybnosti	pochybnost	k1gFnPc4	pochybnost
ohledně	ohledně	k7c2	ohledně
jejího	její	k3xOp3gNnSc2	její
předsednictví	předsednictví	k1gNnSc2	předsednictví
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
klesla	klesnout	k5eAaPmAgFnS	klesnout
popularita	popularita	k1gFnSc1	popularita
konservativní	konservativní	k2eAgFnSc2d1	konservativní
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
popularita	popularita	k1gFnSc1	popularita
labouristů	labourista	k1gMnPc2	labourista
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
na	na	k7c4	na
53	[number]	k4	53
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
byla	být	k5eAaImAgFnS	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
především	především	k6eAd1	především
zavedením	zavedení	k1gNnSc7	zavedení
obecní	obecní	k2eAgFnSc2d1	obecní
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Daň	daň	k1gFnSc1	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
daň	daň	k1gFnSc4	daň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
měli	mít	k5eAaImAgMnP	mít
přispívat	přispívat	k5eAaImF	přispívat
všichni	všechen	k3xTgMnPc1	všechen
registrovaní	registrovaný	k2eAgMnPc1d1	registrovaný
voliči	volič	k1gMnPc1	volič
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
obci	obec	k1gFnSc6	obec
na	na	k7c4	na
komunální	komunální	k2eAgFnPc4d1	komunální
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
daně	daň	k1gFnSc2	daň
nebyla	být	k5eNaImAgFnS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
na	na	k7c6	na
výši	výše	k1gFnSc6	výše
nemovitého	movitý	k2eNgInSc2d1	nemovitý
majetku	majetek	k1gInSc2	majetek
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
platili	platit	k5eAaImAgMnP	platit
daň	daň	k1gFnSc4	daň
pouze	pouze	k6eAd1	pouze
majitelé	majitel	k1gMnPc1	majitel
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nájemníci	nájemník	k1gMnPc1	nájemník
daň	daň	k1gFnSc4	daň
neplatili	platit	k5eNaImAgMnP	platit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
plátců	plátce	k1gMnPc2	plátce
daně	daň	k1gFnSc2	daň
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
zvýšit	zvýšit	k5eAaPmF	zvýšit
ze	z	k7c2	z
14	[number]	k4	14
miliónů	milión	k4xCgInPc2	milión
na	na	k7c4	na
38	[number]	k4	38
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
protestů	protest	k1gInPc2	protest
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Trafalgaru	Trafalgar	k1gInSc6	Trafalgar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
londýnské	londýnský	k2eAgInPc4d1	londýnský
nepokoje	nepokoj	k1gInPc4	nepokoj
proti	proti	k7c3	proti
dani	daň	k1gFnSc3	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
měla	mít	k5eAaImAgFnS	mít
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
vládnutí	vládnutí	k1gNnSc2	vládnutí
v	v	k7c6	v
konzervativní	konzervativní	k2eAgFnSc6d1	konzervativní
straně	strana	k1gFnSc6	strana
své	svůj	k3xOyFgInPc4	svůj
odpůrce	odpůrce	k1gMnSc1	odpůrce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
wets	wets	k6eAd1	wets
<g/>
"	"	kIx"	"
-	-	kIx~	-
ubrečení	ubrečení	k1gNnSc1	ubrečení
<g/>
,	,	kIx,	,
mokří	mokřit	k5eAaImIp3nS	mokřit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc1	odpor
proti	proti	k7c3	proti
její	její	k3xOp3gFnSc3	její
politice	politika	k1gFnSc3	politika
i	i	k8xC	i
stylu	styl	k1gInSc3	styl
vedení	vedení	k1gNnSc2	vedení
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
její	její	k3xOp3gMnSc1	její
zástupce	zástupce	k1gMnSc1	zástupce
a	a	k8xC	a
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
sir	sir	k1gMnSc1	sir
Geoffrey	Geoffre	k2eAgFnPc4d1	Geoffre
Howe	How	k1gFnPc4	How
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
důležitého	důležitý	k2eAgMnSc4d1	důležitý
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
Howeova	Howeův	k2eAgNnSc2d1	Howeův
odstoupení	odstoupení	k1gNnSc2	odstoupení
byla	být	k5eAaImAgFnS	být
premiérčina	premiérčin	k2eAgFnSc1d1	premiérčin
protievropská	protievropský	k2eAgFnSc1d1	protievropská
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgMnPc2	tento
tří	tři	k4xCgMnPc2	tři
faktorů	faktor	k1gMnPc2	faktor
vedla	vést	k5eAaImAgFnS	vést
členy	člen	k1gInPc7	člen
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
jejích	její	k3xOp3gFnPc6	její
schopnostech	schopnost	k1gFnPc6	schopnost
vyhrát	vyhrát	k5eAaPmF	vyhrát
blížící	blížící	k2eAgFnSc1d1	blížící
se	se	k3xPyFc4	se
volby	volba	k1gFnSc2	volba
začali	začít	k5eAaPmAgMnP	začít
silně	silně	k6eAd1	silně
pochybovat	pochybovat	k5eAaImF	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Obávali	obávat	k5eAaImAgMnP	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
měnová	měnový	k2eAgNnPc4d1	měnové
politika	politikum	k1gNnPc4	politikum
a	a	k8xC	a
daňové	daňový	k2eAgInPc4d1	daňový
škrty	škrt	k1gInPc4	škrt
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
chystanou	chystaný	k2eAgFnSc7d1	chystaná
daní	daň	k1gFnSc7	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
uberou	ubrat	k5eAaPmIp3nP	ubrat
potřebné	potřebný	k2eAgInPc4d1	potřebný
voličské	voličský	k2eAgInPc4d1	voličský
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
aby	aby	kYmCp3nS	aby
riskovala	riskovat	k5eAaBmAgFnS	riskovat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
sesazení	sesazení	k1gNnSc4	sesazení
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
předsedy	předseda	k1gMnSc2	předseda
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
sama	sám	k3xTgMnSc4	sám
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
a	a	k8xC	a
přenechala	přenechat	k5eAaPmAgFnS	přenechat
post	post	k1gInSc4	post
Johnu	John	k1gMnSc3	John
Majorovi	major	k1gMnSc3	major
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
si	se	k3xPyFc3	se
sama	sám	k3xTgFnSc1	sám
přála	přát	k5eAaImAgFnS	přát
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
předsednictví	předsednictví	k1gNnSc2	předsednictví
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nadále	nadále	k6eAd1	nadále
politicky	politicky	k6eAd1	politicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Kriticky	kriticky	k6eAd1	kriticky
se	se	k3xPyFc4	se
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
například	například	k6eAd1	například
k	k	k7c3	k
Maastrichtské	maastrichtský	k2eAgFnSc3d1	Maastrichtská
smlouvě	smlouva	k1gFnSc3	smlouva
o	o	k7c4	o
prohloubení	prohloubení	k1gNnSc4	prohloubení
politické	politický	k2eAgFnSc2d1	politická
spolupráce	spolupráce	k1gFnSc2	spolupráce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
povýšena	povýšen	k2eAgFnSc1d1	povýšena
do	do	k7c2	do
doživotního	doživotní	k2eAgInSc2d1	doživotní
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
life	life	k6eAd1	life
peerage	peeragat	k5eAaPmIp3nS	peeragat
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
baronka	baronka	k1gFnSc1	baronka
(	(	kIx(	(
<g/>
Baroness	Baroness	k1gInSc1	Baroness
Thatcher	Thatchra	k1gFnPc2	Thatchra
of	of	k?	of
Kesteven	Kesteven	k2eAgInSc1d1	Kesteven
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
získala	získat	k5eAaPmAgFnS	získat
členství	členství	k1gNnSc4	členství
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
obdržela	obdržet	k5eAaPmAgFnS	obdržet
baronka	baronka	k1gFnSc1	baronka
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
britský	britský	k2eAgInSc4d1	britský
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
Podvazkový	podvazkový	k2eAgInSc1d1	podvazkový
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
Order	Order	k1gInSc1	Order
of	of	k?	of
the	the	k?	the
Garter	Garter	k1gInSc1	Garter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
udělovaný	udělovaný	k2eAgInSc1d1	udělovaný
anglickými	anglický	k2eAgMnPc7d1	anglický
panovníky	panovník	k1gMnPc7	panovník
již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
Thatcherové	Thatcherová	k1gFnSc2	Thatcherová
dostalo	dostat	k5eAaPmAgNnS	dostat
ocenění	ocenění	k1gNnSc1	ocenění
Ronald	Ronald	k1gInSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Freedom	Freedom	k1gInSc1	Freedom
Award	Awardo	k1gNnPc2	Awardo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1998	[number]	k4	1998
přicestoval	přicestovat	k5eAaPmAgMnS	přicestovat
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
bývalý	bývalý	k2eAgMnSc1d1	bývalý
chilský	chilský	k2eAgMnSc1d1	chilský
diktátor	diktátor	k1gMnSc1	diktátor
Augusto	Augusta	k1gMnSc5	Augusta
Pinochet	Pinochet	k1gMnSc1	Pinochet
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
Thatcherové	Thatcherová	k1gFnSc2	Thatcherová
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgMnS	být
zde	zde	k6eAd1	zde
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
následně	následně	k6eAd1	následně
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
souzen	soudit	k5eAaImNgInS	soudit
pro	pro	k7c4	pro
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
jej	on	k3xPp3gInSc4	on
výslovně	výslovně	k6eAd1	výslovně
podpořila	podpořit	k5eAaPmAgFnS	podpořit
<g/>
,	,	kIx,	,
navštívila	navštívit	k5eAaPmAgFnS	navštívit
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
poblíž	poblíž	k7c2	poblíž
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
ocenila	ocenit	k5eAaPmAgFnS	ocenit
jeho	jeho	k3xOp3gFnSc4	jeho
pomoc	pomoc	k1gFnSc4	pomoc
během	během	k7c2	během
války	válka	k1gFnSc2	válka
o	o	k7c4	o
Falklandy	Falklanda	k1gFnPc4	Falklanda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
zhoršujícím	zhoršující	k2eAgMnSc6d1	zhoršující
se	se	k3xPyFc4	se
jejím	její	k3xOp3gInSc6	její
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
rodiny	rodina	k1gFnSc2	rodina
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
zhoršování	zhoršování	k1gNnSc3	zhoršování
jejích	její	k3xOp3gFnPc2	její
duševních	duševní	k2eAgFnPc2d1	duševní
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
britský	britský	k2eAgInSc1d1	britský
životopisný	životopisný	k2eAgInSc1d1	životopisný
film	film	k1gInSc1	film
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
osobě	osoba	k1gFnSc6	osoba
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
Železná	železný	k2eAgFnSc1d1	železná
lady	lady	k1gFnPc7	lady
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
hlavní	hlavní	k2eAgFnSc6d1	hlavní
úloze	úloha	k1gFnSc6	úloha
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
herečka	herečka	k1gFnSc1	herečka
Meryl	Meryl	k1gInSc1	Meryl
Streepová	Streepová	k1gFnSc1	Streepová
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
po	po	k7c6	po
mozkové	mozkový	k2eAgFnSc6d1	mozková
mrtvici	mrtvice	k1gFnSc6	mrtvice
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
87	[number]	k4	87
let	léto	k1gNnPc2	léto
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Ritz	Ritza	k1gFnPc2	Ritza
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydlela	bydlet	k5eAaImAgFnS	bydlet
od	od	k7c2	od
Vánoc	Vánoce	k1gFnPc2	Vánoce
2012	[number]	k4	2012
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měla	mít	k5eAaImAgFnS	mít
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
vyjitím	vyjití	k1gNnSc7	vyjití
schodů	schod	k1gInPc2	schod
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
na	na	k7c4	na
Chester	Chester	k1gInSc4	Chester
Square	square	k1gInSc1	square
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
měla	mít	k5eAaImAgFnS	mít
silné	silný	k2eAgFnPc4d1	silná
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
vůli	vůle	k1gFnSc6	vůle
neměla	mít	k5eNaImAgFnS	mít
přímo	přímo	k6eAd1	přímo
státní	státní	k2eAgInSc4d1	státní
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
jako	jako	k8xS	jako
obřadní	obřadní	k2eAgFnSc1d1	obřadní
s	s	k7c7	s
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
poctami	pocta	k1gFnPc7	pocta
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
s	s	k7c7	s
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
byla	být	k5eAaImAgFnS	být
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c4	za
obchodníka	obchodník	k1gMnSc4	obchodník
Denise	Denisa	k1gFnSc6	Denisa
Thatchera	Thatcher	k1gMnSc2	Thatcher
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Thatcherovým	Thatcherův	k2eAgFnPc3d1	Thatcherova
narodila	narodit	k5eAaPmAgNnP	narodit
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Carol	Carola	k1gFnPc2	Carola
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Mark	Mark	k1gMnSc1	Mark
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Winstona	Winston	k1gMnSc2	Winston
Churchilla	Churchill	k1gMnSc2	Churchill
společně	společně	k6eAd1	společně
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
odhalila	odhalit	k5eAaPmAgFnS	odhalit
sochu	socha	k1gFnSc4	socha
britského	britský	k2eAgMnSc2d1	britský
státníka	státník	k1gMnSc2	státník
Winstona	Winston	k1gMnSc2	Winston
Churchila	Churchil	k1gMnSc2	Churchil
<g/>
.	.	kIx.	.
</s>
