<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
ocenění	ocenění	k1gNnSc4	ocenění
každoročně	každoročně	k6eAd1	každoročně
udělované	udělovaný	k2eAgFnPc1d1	udělovaná
za	za	k7c4	za
zásadní	zásadní	k2eAgInSc4d1	zásadní
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
technické	technický	k2eAgInPc4d1	technický
objevy	objev	k1gInPc4	objev
či	či	k8xC	či
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS	udělovat
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
oborech	obor	k1gInPc6	obor
<g/>
:	:	kIx,	:
fyzika	fyzik	k1gMnSc2	fyzik
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
fyziologie	fyziologie	k1gFnSc2	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
mír	mír	k1gInSc1	mír
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
také	také	k9	také
uděluje	udělovat	k5eAaImIp3nS	udělovat
Cena	cena	k1gFnSc1	cena
Švédské	švédský	k2eAgFnSc2d1	švédská
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
vědy	věda	k1gFnSc2	věda
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
udělována	udělovat	k5eAaImNgFnS	udělovat
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poslední	poslední	k2eAgFnSc2d1	poslední
vůle	vůle	k1gFnSc2	vůle
švédského	švédský	k2eAgMnSc2d1	švédský
vědce	vědec	k1gMnSc2	vědec
a	a	k8xC	a
průmyslníka	průmyslník	k1gMnSc2	průmyslník
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc2	vynálezce
dynamitu	dynamit	k1gInSc2	dynamit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
udělována	udělován	k2eAgFnSc1d1	udělována
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejprestižnější	prestižní	k2eAgNnSc4d3	nejprestižnější
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
každoročně	každoročně	k6eAd1	každoročně
udělovány	udělovat	k5eAaImNgFnP	udělovat
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
vynikající	vynikající	k2eAgInPc4d1	vynikající
činy	čin	k1gInPc4	čin
v	v	k7c6	v
pěti	pět	k4xCc6	pět
oblastech	oblast	k1gFnPc6	oblast
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
:	:	kIx,	:
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
–	–	k?	–
udělována	udělován	k2eAgFnSc1d1	udělována
švédskou	švédský	k2eAgFnSc7d1	švédská
Královskou	královský	k2eAgFnSc7d1	královská
akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
–	–	k?	–
udělována	udělován	k2eAgFnSc1d1	udělována
švédskou	švédský	k2eAgFnSc7d1	švédská
Královskou	královský	k2eAgFnSc7d1	královská
akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
fyziologii	fyziologie	k1gFnSc3	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc6	lékařství
–	–	k?	–
udělována	udělován	k2eAgFnSc1d1	udělována
institutem	institut	k1gInSc7	institut
Karolinska	Karolinsko	k1gNnSc2	Karolinsko
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
–	–	k?	–
udělována	udělován	k2eAgFnSc1d1	udělována
Švédskou	švédský	k2eAgFnSc7d1	švédská
akademií	akademie	k1gFnSc7	akademie
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
–	–	k?	–
udělována	udělovat	k5eAaImNgFnS	udělovat
komisí	komise	k1gFnSc7	komise
norského	norský	k2eAgInSc2d1	norský
parlamentu	parlament	k1gInSc2	parlament
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
Švédská	švédský	k2eAgFnSc1d1	švédská
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
založit	založit	k5eAaPmF	založit
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Cena	cena	k1gFnSc1	cena
Švédské	švédský	k2eAgFnSc2d1	švédská
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
za	za	k7c4	za
rozvoj	rozvoj	k1gInSc4	rozvoj
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
vědy	věda	k1gFnSc2	věda
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
uděluje	udělovat	k5eAaImIp3nS	udělovat
švédská	švédský	k2eAgFnSc1d1	švédská
Královská	královský	k2eAgFnSc1d1	královská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
není	být	k5eNaImIp3nS	být
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
v	v	k7c6	v
Nobelově	Nobelův	k2eAgFnSc6d1	Nobelova
závěti	závěť	k1gFnSc6	závěť
<g/>
,	,	kIx,	,
peněžní	peněžní	k2eAgFnSc1d1	peněžní
odměna	odměna	k1gFnSc1	odměna
se	se	k3xPyFc4	se
nevyplácí	vyplácet	k5eNaImIp3nS	vyplácet
z	z	k7c2	z
Nobelova	Nobelův	k2eAgInSc2d1	Nobelův
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
"	"	kIx"	"
<g/>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
cenami	cena	k1gFnPc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
však	však	k8xC	však
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgFnPc4	žádný
další	další	k2eAgFnPc4d1	další
ceny	cena	k1gFnPc4	cena
"	"	kIx"	"
<g/>
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
"	"	kIx"	"
již	již	k6eAd1	již
zaváděny	zaváděn	k2eAgInPc1d1	zaváděn
nebudou	být	k5eNaImBp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgFnP	být
Nobelovy	Nobelův	k2eAgFnPc4d1	Nobelova
ceny	cena	k1gFnPc4	cena
udělovány	udělován	k2eAgFnPc4d1	udělována
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
;	;	kIx,	;
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
cen	cena	k1gFnPc2	cena
předávána	předávat	k5eAaImNgFnS	předávat
švédským	švédský	k2eAgMnSc7d1	švédský
králem	král	k1gMnSc7	král
na	na	k7c6	na
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
večeru	večer	k1gInSc6	večer
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
vždy	vždy	k6eAd1	vždy
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
výročí	výročí	k1gNnSc2	výročí
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
;	;	kIx,	;
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
je	být	k5eAaImIp3nS	být
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
předávána	předávat	k5eAaImNgFnS	předávat
na	na	k7c6	na
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
držitelů	držitel	k1gMnPc2	držitel
cen	cena	k1gFnPc2	cena
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zveřejněna	zveřejnit	k5eAaPmNgNnP	zveřejnit
předem	předem	k6eAd1	předem
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
komise	komise	k1gFnPc1	komise
je	on	k3xPp3gMnPc4	on
zveřejňují	zveřejňovat	k5eAaImIp3nP	zveřejňovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
i	i	k8xC	i
mezi	mezi	k7c4	mezi
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvýše	nejvýše	k6eAd1	nejvýše
mezi	mezi	k7c4	mezi
tři	tři	k4xCgFnPc4	tři
osoby	osoba	k1gFnPc4	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
nelze	lze	k6eNd1	lze
udělit	udělit	k5eAaPmF	udělit
posmrtně	posmrtně	k6eAd1	posmrtně
(	(	kIx(	(
<g/>
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
držitel	držitel	k1gMnSc1	držitel
zemře	zemřít	k5eAaPmIp3nS	zemřít
mezi	mezi	k7c7	mezi
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
slavnostním	slavnostní	k2eAgNnSc7d1	slavnostní
předáváním	předávání	k1gNnSc7	předávání
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
cena	cena	k1gFnSc1	cena
udělena	udělen	k2eAgFnSc1d1	udělena
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
již	již	k6eAd1	již
nominovaný	nominovaný	k2eAgMnSc1d1	nominovaný
člověk	člověk	k1gMnSc1	člověk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
únoru	únor	k1gInSc6	únor
příslušného	příslušný	k2eAgInSc2d1	příslušný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Nad	nad	k7c4	nad
udělování	udělování	k1gNnSc4	udělování
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
stojí	stát	k5eAaImIp3nS	stát
správní	správní	k2eAgInSc1d1	správní
orgán	orgán	k1gInSc1	orgán
dohlížející	dohlížející	k2eAgInSc1d1	dohlížející
na	na	k7c4	na
stockholmské	stockholmský	k2eAgFnPc4d1	Stockholmská
nadace	nadace	k1gFnPc4	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nebyla	být	k5eNaImAgFnS	být
ctěna	ctěn	k2eAgFnSc1d1	ctěna
vůle	vůle	k1gFnSc1	vůle
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
zrušit	zrušit	k5eAaPmF	zrušit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c6	o
udělení	udělení	k1gNnSc6	udělení
cen	cena	k1gFnPc2	cena
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
medaile	medaile	k1gFnSc2	medaile
a	a	k8xC	a
diplomu	diplom	k1gInSc2	diplom
nárok	nárok	k1gInSc4	nárok
také	také	k9	také
na	na	k7c4	na
finanční	finanční	k2eAgFnSc4d1	finanční
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
činí	činit	k5eAaImIp3nS	činit
8	[number]	k4	8
miliónů	milión	k4xCgInPc2	milión
švédských	švédský	k2eAgFnPc2d1	švédská
korun	koruna	k1gFnPc2	koruna
(	(	kIx(	(
<g/>
cca	cca	kA	cca
23	[number]	k4	23
miliónů	milión	k4xCgInPc2	milión
českých	český	k2eAgFnPc2d1	Česká
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgInSc7d1	původní
cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
odměny	odměna	k1gFnSc2	odměna
bylo	být	k5eAaImAgNnS	být
umožnit	umožnit	k5eAaPmF	umožnit
pokračování	pokračování	k1gNnSc1	pokračování
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
či	či	k8xC	či
práci	práce	k1gFnSc3	práce
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
ohlížet	ohlížet	k5eAaImF	ohlížet
se	se	k3xPyFc4	se
na	na	k7c4	na
finanční	finanční	k2eAgFnSc4d1	finanční
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
důvod	důvod	k1gInSc1	důvod
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
aktuální	aktuální	k2eAgFnSc1d1	aktuální
<g/>
.	.	kIx.	.
</s>
<s>
Udělované	udělovaný	k2eAgFnPc1d1	udělovaná
medaile	medaile	k1gFnPc1	medaile
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
líci	líc	k1gFnSc6	líc
portrét	portrét	k1gInSc4	portrét
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
udávajícím	udávající	k2eAgInSc7d1	udávající
roky	rok	k1gInPc4	rok
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
:	:	kIx,	:
NAT-MDCCC	NAT-MDCCC	k1gFnSc1	NAT-MDCCC
XXXIII	XXXIII	kA	XXXIII
OB-MDCCC	OB-MDCCC	k1gFnSc2	OB-MDCCC
XCVI	XCVI	kA	XCVI
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rubu	rub	k1gInSc6	rub
je	být	k5eAaImIp3nS	být
motiv	motiv	k1gInSc1	motiv
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
příslušné	příslušný	k2eAgFnSc3d1	příslušná
ceně	cena	k1gFnSc3	cena
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
Na	na	k7c6	na
"	"	kIx"	"
<g/>
švédských	švédský	k2eAgFnPc6d1	švédská
<g/>
"	"	kIx"	"
cenách	cena	k1gFnPc6	cena
(	(	kIx(	(
<g/>
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
medicína	medicína	k1gFnSc1	medicína
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
Inventas	Inventasa	k1gFnPc2	Inventasa
vitam	vitam	k6eAd1	vitam
juvat	juvat	k5eAaImF	juvat
excoluisse	excoluisse	k1gFnPc4	excoluisse
per	pero	k1gNnPc2	pero
artes	artesa	k1gFnPc2	artesa
(	(	kIx(	(
<g/>
vynálezy	vynález	k1gInPc7	vynález
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
zkrášlován	zkrášlován	k2eAgInSc1d1	zkrášlován
<g />
.	.	kIx.	.
</s>
<s>
uměním	umění	k1gNnSc7	umění
–	–	k?	–
citát	citát	k1gInSc4	citát
z	z	k7c2	z
Vergiliovy	Vergiliův	k2eAgFnSc2d1	Vergiliova
Aeneidy	Aeneida	k1gFnSc2	Aeneida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
Pro	pro	k7c4	pro
pace	pac	k1gInSc5	pac
et	et	k?	et
fraternitate	fraternitat	k1gInSc5	fraternitat
gentium	gentium	k1gNnSc4	gentium
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
a	a	k8xC	a
bratrství	bratrství	k1gNnSc4	bratrství
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
ceny	cena	k1gFnSc2	cena
–	–	k?	–
Sveriges	Sveriges	k1gMnSc1	Sveriges
Riksbank	Riksbank	k1gMnSc1	Riksbank
till	till	k1gMnSc1	till
Alfred	Alfred	k1gMnSc1	Alfred
Nobels	Nobels	k1gInSc4	Nobels
Minne	Minn	k1gInSc5	Minn
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
medaili	medaile	k1gFnSc6	medaile
je	být	k5eAaImIp3nS	být
také	také	k9	také
vyryto	vyryt	k2eAgNnSc4d1	vyryto
jméno	jméno	k1gNnSc4	jméno
držitele	držitel	k1gMnSc2	držitel
<g/>
.	.	kIx.	.
</s>
<s>
Diplom	diplom	k1gInSc1	diplom
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
unikátní	unikátní	k2eAgNnSc1d1	unikátní
grafické	grafický	k2eAgNnSc1d1	grafické
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
diplomy	diplom	k1gInPc1	diplom
nemají	mít	k5eNaImIp3nP	mít
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
text	text	k1gInSc1	text
uvedený	uvedený	k2eAgInSc1d1	uvedený
na	na	k7c6	na
diplomu	diplom	k1gInSc6	diplom
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
švédské	švédský	k2eAgFnPc4d1	švédská
<g/>
"	"	kIx"	"
ceny	cena	k1gFnPc4	cena
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
diplomech	diplom	k1gInPc6	diplom
stručné	stručný	k2eAgNnSc1d1	stručné
zdůvodnění	zdůvodnění	k1gNnSc3	zdůvodnění
<g/>
,	,	kIx,	,
za	za	k7c4	za
co	co	k3yQnSc4	co
byla	být	k5eAaImAgFnS	být
cena	cena	k1gFnSc1	cena
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
osobnosti	osobnost	k1gFnPc1	osobnost
získaly	získat	k5eAaPmAgFnP	získat
dvě	dva	k4xCgFnPc4	dva
Nobelovy	Nobelův	k2eAgFnPc4d1	Nobelova
ceny	cena	k1gFnPc4	cena
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
–	–	k?	–
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
a	a	k8xC	a
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
Linus	Linus	k1gInSc1	Linus
Pauling	Pauling	k1gInSc4	Pauling
–	–	k?	–
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
a	a	k8xC	a
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
John	John	k1gMnSc1	John
Bardeen	Bardeen	k2eAgMnSc1d1	Bardeen
–	–	k?	–
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
Frederick	Fredericko	k1gNnPc2	Fredericko
Sanger	Sangra	k1gFnPc2	Sangra
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
Dvě	dva	k4xCgFnPc1	dva
organizace	organizace	k1gFnPc1	organizace
získaly	získat	k5eAaPmAgFnP	získat
vícekrát	vícekrát	k6eAd1	vícekrát
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
:	:	kIx,	:
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
–	–	k?	–
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
Úřad	úřad	k1gInSc1	úřad
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
–	–	k?	–
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Jaroslav	Jaroslava	k1gFnPc2	Jaroslava
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
–	–	k?	–
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
chemii	chemie	k1gFnSc4	chemie
(	(	kIx(	(
<g/>
polarografie	polarografie	k1gFnSc1	polarografie
<g/>
)	)	kIx)	)
1959	[number]	k4	1959
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
–	–	k?	–
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Bertha	Bertha	k1gFnSc1	Bertha
von	von	k1gInSc1	von
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
–	–	k?	–
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
Carl	Carlum	k1gNnPc2	Carlum
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Cori	Cori	k1gNnSc2	Cori
–	–	k?	–
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
Gerty	Gert	k1gMnPc4	Gert
Coriová	Coriový	k2eAgFnSc1d1	Coriová
–	–	k?	–
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gMnSc1	Grünberg
–	–	k?	–
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1901	[number]	k4	1901
až	až	k8xS	až
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
ohodnoceno	ohodnotit	k5eAaPmNgNnS	ohodnotit
celkem	celkem	k6eAd1	celkem
48	[number]	k4	48
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
dvakrát	dvakrát	k6eAd1	dvakrát
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
žena-laureátka	ženaaureátka	k1gFnSc1	žena-laureátka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
ženám	žena	k1gFnPc3	žena
uděleno	udělit	k5eAaPmNgNnS	udělit
49	[number]	k4	49
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
představují	představovat	k5eAaImIp3nP	představovat
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
a	a	k8xC	a
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
12	[number]	k4	12
cen	cena	k1gFnPc2	cena
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
,	,	kIx,	,
4	[number]	k4	4
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
a	a	k8xC	a
2	[number]	k4	2
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
náleží	náležet	k5eAaImIp3nS	náležet
ještě	ještě	k9	ještě
1	[number]	k4	1
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
svoji	svůj	k3xOyFgFnSc4	svůj
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
obdobné	obdobný	k2eAgFnPc4d1	obdobná
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
tolik	tolik	k6eAd1	tolik
známé	známý	k2eAgInPc1d1	známý
<g/>
:	:	kIx,	:
Např.	např.	kA	např.
za	za	k7c4	za
jakousi	jakýsi	k3yIgFnSc4	jakýsi
"	"	kIx"	"
<g/>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
matematiku	matematika	k1gFnSc4	matematika
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
Abelova	Abelův	k2eAgFnSc1d1	Abelova
cena	cena	k1gFnSc1	cena
či	či	k8xC	či
Fieldsova	Fieldsův	k2eAgFnSc1d1	Fieldsova
medaile	medaile	k1gFnSc1	medaile
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
informatiku	informatika	k1gFnSc4	informatika
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
Turingovu	Turingův	k2eAgFnSc4d1	Turingova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
"	"	kIx"	"
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
Templetonova	Templetonův	k2eAgFnSc1d1	Templetonova
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
udělována	udělován	k2eAgFnSc1d1	udělována
Ratzingerova	Ratzingerův	k2eAgFnSc1d1	Ratzingerova
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
rovněž	rovněž	k9	rovněž
jako	jako	k8xC	jako
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
teologii	teologie	k1gFnSc4	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
udělována	udělován	k2eAgFnSc1d1	udělována
Goldmanova	Goldmanův	k2eAgFnSc1d1	Goldmanova
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
rovněž	rovněž	k9	rovněž
jako	jako	k8xC	jako
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c2	za
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
parodické	parodický	k2eAgFnPc1d1	parodická
obdoby	obdoba	k1gFnPc1	obdoba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Ig	Ig	k1gFnSc1	Ig
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
"	"	kIx"	"
<g/>
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
nebo	nebo	k8xC	nebo
záhodno	záhodno	k6eAd1	záhodno
opakovat	opakovat	k5eAaImF	opakovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
udělováním	udělování	k1gNnSc7	udělování
cen	cena	k1gFnPc2	cena
panují	panovat	k5eAaImIp3nP	panovat
také	také	k9	také
kontroverze	kontroverze	k1gFnPc1	kontroverze
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
eurocentrismus	eurocentrismus	k1gInSc4	eurocentrismus
<g/>
,	,	kIx,	,
politické	politický	k2eAgNnSc4d1	politické
udělování	udělování	k1gNnSc4	udělování
cen	cena	k1gFnPc2	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ceny	cena	k1gFnPc1	cena
se	se	k3xPyFc4	se
neudělují	udělovat	k5eNaImIp3nP	udělovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
závětí	závěť	k1gFnSc7	závěť
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
.	.	kIx.	.
</s>
<s>
Nominace	nominace	k1gFnSc1	nominace
a	a	k8xC	a
hlasování	hlasování	k1gNnSc1	hlasování
výboru	výbor	k1gInSc2	výbor
je	být	k5eAaImIp3nS	být
tajné	tajný	k2eAgNnSc1d1	tajné
na	na	k7c4	na
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
například	například	k6eAd1	například
vynálezcům	vynálezce	k1gMnPc3	vynálezce
lobotomie	lobotomie	k1gFnSc2	lobotomie
<g/>
,	,	kIx,	,
DDT	DDT	kA	DDT
<g/>
,	,	kIx,	,
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
svržení	svržení	k1gNnSc6	svržení
atomových	atomový	k2eAgFnPc2d1	atomová
bomb	bomba	k1gFnPc2	bomba
udělena	udělit	k5eAaPmNgFnS	udělit
za	za	k7c4	za
štěpnou	štěpný	k2eAgFnSc4d1	štěpná
jadernou	jaderný	k2eAgFnSc4d1	jaderná
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Sommerfeld	Sommerfeld	k1gMnSc1	Sommerfeld
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
84	[number]	k4	84
<g/>
krát	krát	k6eAd1	krát
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
cenu	cena	k1gFnSc4	cena
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
komise	komise	k1gFnSc1	komise
na	na	k7c6	na
Institutu	institut	k1gInSc6	institut
Karolinska	Karolinsko	k1gNnSc2	Karolinsko
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Švédská	švédský	k2eAgFnSc1d1	švédská
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Norská	norský	k2eAgFnSc1d1	norská
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Počet	počet	k1gInSc1	počet
nositelů	nositel	k1gMnPc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k6eAd1	plus
Všechny	všechen	k3xTgFnPc4	všechen
věci	věc	k1gFnPc4	věc
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nejsou	být	k5eNaImIp3nP	být
krásné	krásný	k2eAgFnPc1d1	krásná
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
unikátních	unikátní	k2eAgFnPc2d1	unikátní
ukázek	ukázka	k1gFnPc2	ukázka
s	s	k7c7	s
hlasy	hlas	k1gInPc7	hlas
českých	český	k2eAgMnPc2d1	český
nositelů	nositel	k1gMnPc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
</s>
