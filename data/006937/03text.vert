<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Otruba	otruba	k1gFnSc1	otruba
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
Biskupství	biskupství	k1gNnSc1	biskupství
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Těšetice	Těšetika	k1gFnSc6	Těšetika
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Biskupství	biskupství	k1gNnSc2	biskupství
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
součástí	součást	k1gFnSc7	součást
Náměšti	Náměšť	k1gFnSc3	Náměšť
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rolnické	rolnický	k2eAgFnSc2d1	rolnická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
venkovský	venkovský	k2eAgMnSc1d1	venkovský
písmák	písmák	k1gMnSc1	písmák
a	a	k8xC	a
muzikant	muzikant	k1gMnSc1	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInPc4d1	hudební
základy	základ	k1gInPc4	základ
získal	získat	k5eAaPmAgMnS	získat
doma	doma	k6eAd1	doma
a	a	k8xC	a
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
se	s	k7c7	s
soukromým	soukromý	k2eAgNnSc7d1	soukromé
studiem	studio	k1gNnSc7	studio
u	u	k7c2	u
Josefa	Josef	k1gMnSc2	Josef
Nešvery	Nešvera	k1gFnSc2	Nešvera
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
dobrým	dobrý	k2eAgMnSc7d1	dobrý
houslistou	houslista	k1gMnSc7	houslista
a	a	k8xC	a
varhaníkem	varhaník	k1gMnSc7	varhaník
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
skladby	skladba	k1gFnSc2	skladba
napsal	napsat	k5eAaBmAgInS	napsat
již	již	k6eAd1	již
v	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
a	a	k8xC	a
na	na	k7c6	na
učitelském	učitelský	k2eAgInSc6d1	učitelský
ústavu	ústav	k1gInSc6	ústav
v	v	k7c6	v
Příboře	Příbor	k1gInSc6	Příbor
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
podučitelem	podučitel	k1gMnSc7	podučitel
na	na	k7c6	na
Svatém	svatý	k2eAgInSc6d1	svatý
Kopečku	kopeček	k1gInSc6	kopeček
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
učitelem	učitel	k1gMnSc7	učitel
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
i	i	k9	i
řídícím	řídící	k2eAgMnSc7d1	řídící
učitelem	učitel	k1gMnSc7	učitel
<g/>
)	)	kIx)	)
v	v	k7c6	v
Těšeticích	Těšetik	k1gMnPc6	Těšetik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrval	setrvat	k5eAaPmAgInS	setrvat
až	až	k9	až
do	do	k7c2	do
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
důchodu	důchod	k1gInSc6	důchod
se	se	k3xPyFc4	se
hudby	hudba	k1gFnSc2	hudba
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
vedl	vést	k5eAaImAgInS	vést
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
kroužek	kroužek	k1gInSc1	kroužek
učitelů	učitel	k1gMnPc2	učitel
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Těšeticích	Těšetik	k1gMnPc6	Těšetik
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
i	i	k8xC	i
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ochotníky	ochotník	k1gMnPc7	ochotník
prováděl	provádět	k5eAaImAgInS	provádět
hry	hra	k1gFnPc4	hra
se	s	k7c7	s
zpěvy	zpěv	k1gInPc7	zpěv
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
i	i	k9	i
Blodkovu	Blodkův	k2eAgFnSc4d1	Blodkova
operu	opera	k1gFnSc4	opera
V	v	k7c6	v
studni	studna	k1gFnSc6	studna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
skladatel	skladatel	k1gMnSc1	skladatel
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
převážně	převážně	k6eAd1	převážně
chrámové	chrámový	k2eAgFnSc3d1	chrámová
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
světské	světský	k2eAgFnPc1d1	světská
skladby	skladba	k1gFnPc1	skladba
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
melodiky	melodika	k1gFnSc2	melodika
hanáckých	hanácký	k2eAgFnPc2d1	Hanácká
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
sbory	sbor	k1gInPc1	sbor
byly	být	k5eAaImAgInP	být
komponovány	komponovat	k5eAaImNgInP	komponovat
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
hanácké	hanácký	k2eAgInPc4d1	hanácký
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Missa	Missa	k1gFnSc1	Missa
de	de	k?	de
Requiem	Requium	k1gNnSc7	Requium
pro	pro	k7c4	pro
4	[number]	k4	4
vocibus	vocibus	k1gInSc4	vocibus
cum	cum	k?	cum
Organo	Organa	k1gFnSc5	Organa
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
2	[number]	k4	2
Missa	Missa	k1gFnSc1	Missa
in	in	k?	in
honorem	honor	k1gInSc7	honor
Scti	Scti	k1gNnSc2	Scti
Stanislai	Stanislai	k1gNnSc1	Stanislai
Surrewit	Surrewit	k1gInSc1	Surrewit
pastor	pastor	k1gMnSc1	pastor
bonus	bonus	k1gInSc1	bonus
Haná	Haná	k1gFnSc1	Haná
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
s	s	k7c7	s
klavírem	klavír	k1gInSc7	klavír
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
B-dur	Bura	k1gFnPc2	B-dura
Elegie	elegie	k1gFnSc2	elegie
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
Nálady	nálada	k1gFnSc2	nálada
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
</s>
