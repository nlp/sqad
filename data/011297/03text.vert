<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Kočka	kočka	k1gFnSc1	kočka
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
organizátorem	organizátor	k1gInSc7	organizátor
Matějské	matějský	k2eAgFnSc2d1	Matějská
pouti	pouť	k1gFnSc2	pouť
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednával	vyjednávat	k5eAaImAgInS	vyjednávat
zájezdy	zájezd	k1gInPc4	zájezd
poutí	pouť	k1gFnPc2	pouť
<g/>
,	,	kIx,	,
cirkusů	cirkus	k1gInPc2	cirkus
<g/>
,	,	kIx,	,
lunaparků	lunapark	k1gInPc2	lunapark
i	i	k8xC	i
zpěváckých	zpěvácký	k2eAgFnPc2d1	zpěvácká
celebrit	celebrita	k1gFnPc2	celebrita
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
a	a	k8xC	a
Heleny	Helena	k1gFnSc2	Helena
Vondráčkové	Vondráčková	k1gFnSc2	Vondráčková
<g/>
)	)	kIx)	)
zejména	zejména	k9	zejména
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
asistentem	asistent	k1gMnSc7	asistent
ředitele	ředitel	k1gMnSc2	ředitel
společnosti	společnost	k1gFnSc2	společnost
Incheba	Incheba	k1gFnSc1	Incheba
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
pražského	pražský	k2eAgInSc2d1	pražský
magistrátu	magistrát	k1gInSc2	magistrát
pronajatý	pronajatý	k2eAgInSc1d1	pronajatý
areál	areál	k1gInSc1	areál
Výstaviště	výstaviště	k1gNnSc2	výstaviště
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
7	[number]	k4	7
–	–	k?	–
Bubenči	Bubeneč	k1gFnSc6	Bubeneč
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
působnosti	působnost	k1gFnSc2	působnost
patří	patřit	k5eAaImIp3nP	patřit
gastronomie	gastronomie	k1gFnPc1	gastronomie
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnPc1d1	sportovní
aktivity	aktivita	k1gFnPc1	aktivita
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
stánkový	stánkový	k2eAgInSc1d1	stánkový
prodej	prodej	k1gInSc1	prodej
a	a	k8xC	a
občerstvení	občerstvení	k1gNnSc1	občerstvení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
a	a	k8xC	a
místopředsedou	místopředseda	k1gMnSc7	místopředseda
její	její	k3xOp3gFnSc2	její
oblastní	oblastní	k2eAgFnSc2d1	oblastní
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
klubu	klub	k1gInSc2	klub
přátel	přítel	k1gMnPc2	přítel
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
policejní	policejní	k2eAgFnSc2d1	policejní
asociace	asociace	k1gFnSc2	asociace
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
četné	četný	k2eAgInPc4d1	četný
kontakty	kontakt	k1gInPc4	kontakt
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
i	i	k8xC	i
policejní	policejní	k2eAgFnSc6d1	policejní
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
loutkohercem	loutkoherec	k1gMnSc7	loutkoherec
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Kočka	kočka	k1gFnSc1	kočka
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
dřevařskou	dřevařský	k2eAgFnSc4d1	dřevařská
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
bagristou	bagrista	k1gMnSc7	bagrista
v	v	k7c6	v
jáchymovských	jáchymovský	k2eAgInPc6d1	jáchymovský
dolech	dol	k1gInPc6	dol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Olga	Olga	k1gFnSc1	Olga
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
provazochodecké	provazochodecký	k2eAgFnSc2d1	provazochodecký
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ekonomkou	ekonomka	k1gFnSc7	ekonomka
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Incheba	Incheba	k1gFnSc1	Incheba
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Václav	Václav	k1gMnSc1	Václav
byli	být	k5eAaImAgMnP	být
podnikatelsky	podnikatelsky	k6eAd1	podnikatelsky
činní	činný	k2eAgMnPc1d1	činný
<g/>
.	.	kIx.	.
</s>
<s>
Matějskou	matějský	k2eAgFnSc4d1	Matějská
pouť	pouť	k1gFnSc4	pouť
provozoval	provozovat	k5eAaImAgMnS	provozovat
syn	syn	k1gMnSc1	syn
Václav	Václav	k1gMnSc1	Václav
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
firmy	firma	k1gFnSc2	firma
KOČKA	kočka	k1gFnSc1	kočka
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
zapsané	zapsaný	k2eAgFnSc2d1	zapsaná
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgMnSc7d1	jediný
společníkem	společník	k1gMnSc7	společník
i	i	k8xC	i
jednatelem	jednatel	k1gMnSc7	jednatel
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
čtvrtinovými	čtvrtinový	k2eAgMnPc7d1	čtvrtinový
vlastníky	vlastník	k1gMnPc7	vlastník
společnosti	společnost	k1gFnSc2	společnost
Mirage	Mirage	k1gNnSc1	Mirage
<g/>
,	,	kIx,	,
organizačně	organizačně	k6eAd1	organizačně
zajišťující	zajišťující	k2eAgFnPc4d1	zajišťující
prodejní	prodejní	k2eAgFnPc4d1	prodejní
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
pronájem	pronájem	k1gInSc4	pronájem
stánků	stánek	k1gInPc2	stánek
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
polovinu	polovina	k1gFnSc4	polovina
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Andrej	Andrej	k1gMnSc1	Andrej
Korpljakov	Korpljakov	k1gInSc4	Korpljakov
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgNnSc6	jenž
časopis	časopis	k1gInSc1	časopis
Respekt	respekt	k1gInSc1	respekt
psal	psát	k5eAaImAgInS	psát
jako	jako	k9	jako
o	o	k7c6	o
známém	známý	k2eAgMnSc6d1	známý
členovi	člen	k1gMnSc6	člen
ruské	ruský	k2eAgFnSc2d1	ruská
mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
sledovala	sledovat	k5eAaImAgFnS	sledovat
česká	český	k2eAgFnSc1d1	Česká
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
a	a	k8xC	a
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
sdělení	sdělení	k1gNnSc2	sdělení
nejmenovaného	jmenovaný	k2eNgMnSc2d1	nejmenovaný
důstojníka	důstojník	k1gMnSc2	důstojník
BIS	BIS	kA	BIS
byli	být	k5eAaImAgMnP	být
dva	dva	k4xCgMnPc1	dva
důstojníci	důstojník	k1gMnPc1	důstojník
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
případu	případ	k1gInSc6	případ
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
B.	B.	kA	B.
a	a	k8xC	a
Marian	Marian	k1gMnSc1	Marian
V.	V.	kA	V.
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
z	z	k7c2	z
případu	případ	k1gInSc2	případ
staženi	stažen	k2eAgMnPc1d1	stažen
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
Miroslava	Miroslav	k1gMnSc2	Miroslav
Šloufa	Šlouf	k1gMnSc2	Šlouf
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc2d1	hlavní
poradce	poradce	k1gMnSc2	poradce
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
premiéra	premiér	k1gMnSc2	premiér
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
a	a	k8xC	a
dřívějšího	dřívější	k2eAgMnSc2d1	dřívější
předsedy	předseda	k1gMnSc2	předseda
Obvodního	obvodní	k2eAgInSc2d1	obvodní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
Prahy	Praha	k1gFnSc2	Praha
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Synové	syn	k1gMnPc1	syn
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
podle	podle	k7c2	podle
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
ČSSD	ČSSD	kA	ČSSD
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
agentury	agentura	k1gFnSc2	agentura
1	[number]	k4	1
<g/>
.	.	kIx.	.
česká	český	k2eAgFnSc1d1	Česká
produkční	produkční	k2eAgFnSc3d1	produkční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synovcem	synovec	k1gMnSc7	synovec
Václava	Václav	k1gMnSc2	Václav
Kočky	kočka	k1gFnSc2	kočka
z	z	k7c2	z
druhého	druhý	k4xOgInSc2	druhý
kolene	kolen	k1gInSc5	kolen
je	on	k3xPp3gNnPc4	on
Antonín	Antonín	k1gMnSc1	Antonín
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
honorární	honorární	k2eAgMnSc1d1	honorární
konzul	konzul	k1gMnSc1	konzul
státu	stát	k1gInSc2	stát
Belize	Belize	k1gFnSc2	Belize
a	a	k8xC	a
majitel	majitel	k1gMnSc1	majitel
sítě	síť	k1gFnSc2	síť
kasin	kasino	k1gNnPc2	kasino
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společnosti	společnost	k1gFnSc2	společnost
Happy	Happa	k1gFnSc2	Happa
Day	Day	k1gFnSc2	Day
ovládá	ovládat	k5eAaImIp3nS	ovládat
síť	síť	k1gFnSc1	síť
kasin	kasino	k1gNnPc2	kasino
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc3	Liberec
a	a	k8xC	a
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
podnikatele	podnikatel	k1gMnSc2	podnikatel
Mrázka	Mrázek	k1gMnSc2	Mrázek
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgInPc6d1	český
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Kočka	kočka	k1gFnSc1	kočka
(	(	kIx(	(
<g/>
někde	někde	k6eAd1	někde
senior	senior	k1gMnSc1	senior
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
junior	junior	k1gMnSc1	junior
<g/>
)	)	kIx)	)
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tajnou	tajný	k2eAgFnSc7d1	tajná
zprávou	zpráva	k1gFnSc7	zpráva
policejního	policejní	k2eAgInSc2d1	policejní
Útvaru	útvar	k1gInSc2	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
(	(	kIx(	(
<g/>
ÚOOZ	ÚOOZ	kA	ÚOOZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
projednával	projednávat	k5eAaImAgInS	projednávat
branný	branný	k2eAgInSc1d1	branný
výbor	výbor	k1gInSc1	výbor
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenovaný	jmenovaný	k2eNgInSc4d1	nejmenovaný
důvěryhodný	důvěryhodný	k2eAgInSc4d1	důvěryhodný
zdroj	zdroj	k1gInSc4	zdroj
údajně	údajně	k6eAd1	údajně
dal	dát	k5eAaPmAgMnS	dát
České	český	k2eAgFnSc3d1	Česká
tiskové	tiskový	k2eAgFnSc3d1	tisková
kanceláři	kancelář	k1gFnSc3	kancelář
do	do	k7c2	do
zprávy	zpráva	k1gFnSc2	zpráva
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
vyšetřovacích	vyšetřovací	k2eAgFnPc2d1	vyšetřovací
verzí	verze	k1gFnPc2	verze
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
údajně	údajně	k6eAd1	údajně
"	"	kIx"	"
<g/>
osoba	osoba	k1gFnSc1	osoba
Kočka	kočka	k1gFnSc1	kočka
<g/>
"	"	kIx"	"
zapletena	zapleten	k2eAgFnSc1d1	zapletena
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
do	do	k7c2	do
vraždy	vražda	k1gFnSc2	vražda
podnikatele	podnikatel	k1gMnSc2	podnikatel
Františka	František	k1gMnSc2	František
Mrázka	Mrázek	k1gMnSc2	Mrázek
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
stopu	stopa	k1gFnSc4	stopa
policii	policie	k1gFnSc4	policie
přivedly	přivést	k5eAaPmAgInP	přivést
materiály	materiál	k1gInPc1	materiál
nalezené	nalezený	k2eAgInPc1d1	nalezený
při	při	k7c6	při
domovní	domovní	k2eAgFnSc6d1	domovní
prohlídce	prohlídka	k1gFnSc6	prohlídka
u	u	k7c2	u
Tomáše	Tomáš	k1gMnSc2	Tomáš
Pitra	Pitr	k1gMnSc2	Pitr
<g/>
.	.	kIx.	.
</s>
<s>
ČTK	ČTK	kA	ČTK
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zprávě	zpráva	k1gFnSc6	zpráva
citované	citovaný	k2eAgFnSc2d1	citovaná
mnoha	mnoho	k4c3	mnoho
médii	médium	k1gNnPc7	médium
"	"	kIx"	"
<g/>
osobu	osoba	k1gFnSc4	osoba
Kočka	kočka	k1gFnSc1	kočka
<g/>
"	"	kIx"	"
ztotožnila	ztotožnit	k5eAaPmAgFnS	ztotožnit
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Kočkou	kočka	k1gFnSc7	kočka
seniorem	senior	k1gMnSc7	senior
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
ÚOOZ	ÚOOZ	kA	ÚOOZ
plk.	plk.	kA	plk.
Jan	Jan	k1gMnSc1	Jan
Kubice	kubika	k1gFnSc6	kubika
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
veřejně	veřejně	k6eAd1	veřejně
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
policejní	policejní	k2eAgNnSc1d1	policejní
prezidium	prezidium	k1gNnSc1	prezidium
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
prý	prý	k9	prý
omezují	omezovat	k5eAaImIp3nP	omezovat
činnost	činnost	k1gFnSc4	činnost
Útvaru	útvar	k1gInSc2	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
(	(	kIx(	(
<g/>
ÚOOZ	ÚOOZ	kA	ÚOOZ
<g/>
)	)	kIx)	)
a	a	k8xC	a
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
ředitele	ředitel	k1gMnSc2	ředitel
ÚOOZ	ÚOOZ	kA	ÚOOZ
Jana	Jan	k1gMnSc2	Jan
Kubiceho	Kubice	k1gMnSc2	Kubice
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
lháře	lhář	k1gMnSc4	lhář
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
aféru	aféra	k1gFnSc4	aféra
za	za	k7c4	za
předvolební	předvolební	k2eAgInSc4d1	předvolební
tah	tah	k1gInSc4	tah
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Kotrba	kotrba	k1gFnSc1	kotrba
v	v	k7c6	v
Britských	britský	k2eAgInPc6d1	britský
listech	list	k1gInPc6	list
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
možné	možný	k2eAgInPc4d1	možný
kontakty	kontakt	k1gInPc4	kontakt
mezi	mezi	k7c7	mezi
Markem	Marek	k1gMnSc7	Marek
Dalíkem	Dalík	k1gMnSc7	Dalík
<g/>
,	,	kIx,	,
lobbistou	lobbista	k1gMnSc7	lobbista
spojeným	spojený	k2eAgMnSc7d1	spojený
s	s	k7c7	s
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Kubicem	Kubic	k1gMnSc7	Kubic
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
ohledně	ohledně	k7c2	ohledně
úniku	únik	k1gInSc2	únik
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
ÚOOZ	ÚOOZ	kA	ÚOOZ
<g/>
.	.	kIx.	.
</s>
<s>
Aféra	aféra	k1gFnSc1	aféra
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
předvolební	předvolební	k2eAgInSc4d1	předvolební
boj	boj	k1gInSc4	boj
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
ERBÁKOVÁ	ERBÁKOVÁ	kA	ERBÁKOVÁ
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
angažovaní	angažovaný	k2eAgMnPc1d1	angažovaný
artisté	artista	k1gMnPc1	artista
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Matějská	matějský	k2eAgFnSc1d1	Matějská
pouť	pouť	k1gFnSc1	pouť
</s>
</p>
<p>
<s>
Tajná	tajný	k2eAgFnSc1d1	tajná
zpráva	zpráva	k1gFnSc1	zpráva
ÚOOZ	ÚOOZ	kA	ÚOOZ
<g/>
:	:	kIx,	:
Vrahem	vrah	k1gMnSc7	vrah
Mrázka	Mrázek	k1gMnSc2	Mrázek
byl	být	k5eAaImAgMnS	být
údajně	údajně	k6eAd1	údajně
podnikatel	podnikatel	k1gMnSc1	podnikatel
Kočka	kočka	k1gFnSc1	kočka
(	(	kIx(	(
<g/>
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
atlas	atlas	k1gInSc4	atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
policejní	policejní	k2eAgFnSc1d1	policejní
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
Mrázka	Mrázek	k1gMnSc2	Mrázek
zabil	zabílit	k5eAaPmRp2nS	zabílit
Kočka	kočka	k1gFnSc1	kočka
(	(	kIx(	(
<g/>
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kubice	kubika	k1gFnSc3	kubika
<g/>
:	:	kIx,	:
Organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
zločin	zločin	k1gInSc1	zločin
prorostl	prorůst	k5eAaPmAgInS	prorůst
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
internetový	internetový	k2eAgInSc1d1	internetový
odkaz	odkaz	k1gInSc1	odkaz
již	již	k6eAd1	již
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ČTK	ČTK	kA	ČTK
<g/>
:	:	kIx,	:
Ukázková	ukázkový	k2eAgFnSc1d1	ukázková
manipulace	manipulace	k1gFnSc1	manipulace
mediální	mediální	k2eAgFnSc1d1	mediální
kočkou	kočka	k1gFnSc7	kočka
(	(	kIx(	(
<g/>
Filip	Filip	k1gMnSc1	Filip
Rožánek	Rožánek	k1gMnSc1	Rožánek
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc4d1	britský
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
Paroubkův	Paroubkův	k2eAgMnSc1d1	Paroubkův
známý	známý	k1gMnSc1	známý
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Dolejší	Dolejší	k1gMnSc1	Dolejší
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kmenta	Kment	k1gMnSc2	Kment
<g/>
,	,	kIx,	,
IDnes	IDnes	k1gInSc1	IDnes
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
