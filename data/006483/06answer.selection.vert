<s>
Sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
busty	busta	k1gFnPc1	busta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Sókrata	Sókrat	k1gMnSc4	Sókrat
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
jako	jako	k8xC	jako
šeredného	šeredný	k2eAgMnSc4d1	šeredný
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
inspirovaly	inspirovat	k5eAaBmAgInP	inspirovat
Platónovým	Platónův	k2eAgInSc7d1	Platónův
a	a	k8xC	a
Xenofónovým	Xenofónový	k2eAgInSc7d1	Xenofónový
popisem	popis	k1gInSc7	popis
<g/>
,	,	kIx,	,
přirovnávajícím	přirovnávající	k2eAgInSc7d1	přirovnávající
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
žertu	žert	k1gInSc6	žert
k	k	k7c3	k
silénům	silén	k1gMnPc3	silén
a	a	k8xC	a
satyrům	satyr	k1gMnPc3	satyr
(	(	kIx(	(
<g/>
Platón	platón	k1gInSc1	platón
–	–	k?	–
Symposion	symposion	k1gNnSc1	symposion
<g/>
,	,	kIx,	,
Theaitétos	Theaitétos	k1gInSc1	Theaitétos
<g/>
;	;	kIx,	;
Xenofón	Xenofón	k1gInSc1	Xenofón
–	–	k?	–
Symposion	symposion	k1gNnSc1	symposion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
