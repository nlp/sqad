<s>
Ohře	Ohře	k1gFnSc1	Ohře
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Ohara	Ohara	k1gFnSc1	Ohara
<g/>
,	,	kIx,	,
Oharka	oharka	k1gFnSc1	oharka
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Eger	Eger	k1gInSc1	Eger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
levostranný	levostranný	k2eAgInSc4d1	levostranný
přítok	přítok	k1gInSc4	přítok
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
316	[number]	k4	316
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
246,55	[number]	k4	246,55
km	km	kA	km
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
5614	[number]	k4	5614
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4601	[number]	k4	4601
km2	km2	k4	km2
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
část	část	k1gFnSc1	část
řeky	řeka	k1gFnSc2	řeka
Ohře	Ohře	k1gFnSc2	Ohře
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
až	až	k9	až
během	během	k7c2	během
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
Ohře	Ohře	k1gFnSc1	Ohře
tekla	téct	k5eAaImAgFnS	téct
údolím	údolí	k1gNnSc7	údolí
dnešní	dnešní	k2eAgFnSc2d1	dnešní
řeky	řeka	k1gFnSc2	řeka
Bíliny	Bílina	k1gFnSc2	Bílina
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
Ohře	Ohře	k1gFnSc2	Ohře
i	i	k8xC	i
Ohara	Oharo	k1gNnSc2	Oharo
patrně	patrně	k6eAd1	patrně
vzešly	vzejít	k5eAaPmAgFnP	vzejít
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
keltského	keltský	k2eAgNnSc2d1	keltské
pojmenování	pojmenování	k1gNnSc2	pojmenování
Agara	Agara	k1gFnSc1	Agara
(	(	kIx(	(
<g/>
Ag	Ag	k1gMnSc1	Ag
znamená	znamenat	k5eAaImIp3nS	znamenat
losos	losos	k1gMnSc1	losos
<g/>
,	,	kIx,	,
Ara	ara	k1gMnSc1	ara
znamená	znamenat	k5eAaImIp3nS	znamenat
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
její	její	k3xOp3gInSc1	její
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
Eger	Egera	k1gFnPc2	Egera
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
význam	význam	k1gInSc1	význam
keltského	keltský	k2eAgInSc2d1	keltský
názvu	název	k1gInSc2	název
Agara	Agaro	k1gNnSc2	Agaro
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
měsíční	měsíční	k2eAgFnSc1d1	měsíční
řeka	řeka	k1gFnSc1	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
původnímu	původní	k2eAgNnSc3d1	původní
keltskému	keltský	k2eAgNnSc3d1	keltské
nebo	nebo	k8xC	nebo
předkeltskému	předkeltský	k2eAgNnSc3d1	předkeltský
jménu	jméno	k1gNnSc3	jméno
významy	význam	k1gInPc4	význam
jako	jako	k9	jako
hbitost	hbitost	k1gFnSc1	hbitost
a	a	k8xC	a
bystrost	bystrost	k1gFnSc1	bystrost
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Ohře	Ohře	k1gFnSc2	Ohře
nese	nést	k5eAaImIp3nS	nést
také	také	k9	také
planetka	planetka	k1gFnSc1	planetka
s	s	k7c7	s
katalogovým	katalogový	k2eAgNnSc7d1	Katalogové
číslem	číslo	k1gNnSc7	číslo
4801	[number]	k4	4801
objevená	objevený	k2eAgFnSc1d1	objevená
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
na	na	k7c6	na
Kleti	klet	k2eAgMnPc1d1	klet
Antonínem	Antonín	k1gMnSc7	Antonín
Mrkosem	Mrkos	k1gMnSc7	Mrkos
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
pramene	pramen	k1gInSc2	pramen
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Frankách	Franky	k1gInPc6	Franky
na	na	k7c6	na
území	území	k1gNnSc6	území
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
protéká	protékat	k5eAaImIp3nS	protékat
Ohře	Ohře	k1gFnSc2	Ohře
německou	německý	k2eAgFnSc7d1	německá
částí	část	k1gFnSc7	část
Smrčin	Smrčiny	k1gFnPc2	Smrčiny
a	a	k8xC	a
u	u	k7c2	u
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
osady	osada	k1gFnSc2	osada
Pomezná	pomezný	k2eAgFnSc1d1	Pomezná
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
teče	téct	k5eAaImIp3nS	téct
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
územím	území	k1gNnSc7	území
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
Smrčin	Smrčiny	k1gFnPc2	Smrčiny
protéká	protékat	k5eAaImIp3nS	protékat
postupně	postupně	k6eAd1	postupně
geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
celky	celek	k1gInPc7	celek
Chebskou	chebský	k2eAgFnSc4d1	Chebská
pánví	pánev	k1gFnSc7	pánev
<g/>
,	,	kIx,	,
Sokolovskou	sokolovský	k2eAgFnSc7d1	Sokolovská
pánví	pánev	k1gFnSc7	pánev
<g/>
,	,	kIx,	,
Slavkovským	slavkovský	k2eAgInSc7d1	slavkovský
lesem	les	k1gInSc7	les
a	a	k8xC	a
Doupovskými	Doupovský	k2eAgFnPc7d1	Doupovská
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
obcí	obec	k1gFnSc7	obec
Okounov	Okounovo	k1gNnPc2	Okounovo
přitéká	přitékat	k5eAaImIp3nS	přitékat
do	do	k7c2	do
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
okraje	okraj	k1gInSc2	okraj
Kadaně	Kadaň	k1gFnSc2	Kadaň
opuští	opuštit	k5eAaPmIp3nP	opuštit
Doupovské	Doupovský	k2eAgFnPc1d1	Doupovská
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
Mosteckou	mostecký	k2eAgFnSc7d1	Mostecká
pánví	pánev	k1gFnSc7	pánev
až	až	k6eAd1	až
k	k	k7c3	k
Postoloprtům	Postoloprt	k1gMnPc3	Postoloprt
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
městem	město	k1gNnSc7	město
již	již	k6eAd1	již
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
Dolnooharské	Dolnooharský	k2eAgFnSc6d1	Dolnooharský
tabuli	tabule	k1gFnSc6	tabule
až	až	k9	až
do	do	k7c2	do
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Ohře	Ohře	k1gFnSc1	Ohře
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Frankách	Franky	k1gInPc6	Franky
v	v	k7c6	v
Zemském	zemský	k2eAgInSc6d1	zemský
okresu	okres	k1gInSc6	okres
Wunsiedel	Wunsiedlo	k1gNnPc2	Wunsiedlo
im	im	k?	im
Fichtelgebirge	Fichtelgebirg	k1gFnSc2	Fichtelgebirg
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
Ohře	Ohře	k1gFnSc2	Ohře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
752	[number]	k4	752
m	m	kA	m
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
úpatí	úpatí	k1gNnSc6	úpatí
hory	hora	k1gFnSc2	hora
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
(	(	kIx(	(
<g/>
1051	[number]	k4	1051
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
Smrčin	Smrčiny	k1gFnPc2	Smrčiny
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
je	být	k5eAaImIp3nS	být
obložen	obložen	k2eAgInSc1d1	obložen
dvanácti	dvanáct	k4xCc2	dvanáct
hrubě	hrubě	k6eAd1	hrubě
tesanými	tesaný	k2eAgInPc7d1	tesaný
žulovými	žulový	k2eAgInPc7d1	žulový
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
jsou	být	k5eAaImIp3nP	být
vytesána	vytesán	k2eAgNnPc1d1	vytesáno
jména	jméno	k1gNnPc1	jméno
měst	město	k1gNnPc2	město
či	či	k8xC	či
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
úpravě	úprava	k1gFnSc6	úprava
pramene	pramen	k1gInSc2	pramen
podílela	podílet	k5eAaImAgFnS	podílet
a	a	k8xC	a
kterými	který	k3yQgFnPc7	který
Ohře	Ohře	k1gFnSc2	Ohře
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
Čelní	čelní	k2eAgInSc1d1	čelní
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgFnPc1d2	vyšší
a	a	k8xC	a
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
i	i	k8xC	i
s	s	k7c7	s
erbem	erb	k1gInSc7	erb
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
patři	patřit	k5eAaImRp2nS	patřit
městu	město	k1gNnSc3	město
Cheb	Cheb	k1gInSc1	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
kameny	kámen	k1gInPc1	kámen
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnPc4	jméno
Weißenstadt	Weißenstadta	k1gFnPc2	Weißenstadta
<g/>
,	,	kIx,	,
Marktleuthen	Marktleuthna	k1gFnPc2	Marktleuthna
<g/>
,	,	kIx,	,
Kynšperk	Kynšperk	k1gInSc1	Kynšperk
(	(	kIx(	(
<g/>
Königsberg	Königsberg	k1gInSc1	Königsberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sokolov	Sokolov	k1gInSc1	Sokolov
(	(	kIx(	(
<g/>
Falkenau	Falkenaus	k1gInSc2	Falkenaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Loket	loket	k1gInSc1	loket
(	(	kIx(	(
<g/>
Elbogen	Elbogen	k1gInSc1	Elbogen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
(	(	kIx(	(
<g/>
Karlsbad	Karlsbad	k1gInSc1	Karlsbad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rybáře	Rybář	k1gMnPc4	Rybář
(	(	kIx(	(
<g/>
Fischern	Fischern	k1gMnSc1	Fischern
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Klášterec	Klášterec	k1gInSc1	Klášterec
(	(	kIx(	(
<g/>
Klösterle	Klösterle	k1gInSc1	Klösterle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kadaň	Kadaň	k1gFnSc1	Kadaň
(	(	kIx(	(
<g/>
Kaaden	Kaadno	k1gNnPc2	Kaadno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žatec	Žatec	k1gInSc1	Žatec
(	(	kIx(	(
<g/>
Saaz	Saaz	k1gInSc1	Saaz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Postoloprty	Postoloprta	k1gFnSc2	Postoloprta
(	(	kIx(	(
<g/>
Postelberg	Postelberg	k1gInSc1	Postelberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úprava	úprava	k1gFnSc1	úprava
pramene	pramen	k1gInSc2	pramen
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
města	město	k1gNnSc2	město
Cheb	Cheb	k1gInSc1	Cheb
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
provedla	provést	k5eAaPmAgFnS	provést
kamenická	kamenický	k2eAgFnSc1d1	kamenická
firma	firma	k1gFnSc1	firma
Grasyma	Grasyma	k1gFnSc1	Grasyma
z	z	k7c2	z
Wunsiedelu	Wunsiedel	k1gInSc2	Wunsiedel
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
posvěcení	posvěcení	k1gNnSc1	posvěcení
pramene	pramen	k1gInSc2	pramen
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c4	na
první	první	k4xOgInSc4	první
Svatodušní	svatodušní	k2eAgInSc4d1	svatodušní
svátek	svátek	k1gInSc4	svátek
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
u	u	k7c2	u
obruby	obruba	k1gFnSc2	obruba
pramene	pramen	k1gInSc2	pramen
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
kamenných	kamenný	k2eAgInPc6d1	kamenný
kvádrech	kvádr	k1gInPc6	kvádr
pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
žulový	žulový	k2eAgInSc1d1	žulový
blok	blok	k1gInSc1	blok
s	s	k7c7	s
vytesaným	vytesaný	k2eAgInSc7d1	vytesaný
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
pozměněným	pozměněný	k2eAgInSc7d1	pozměněný
textem	text	k1gInSc7	text
z	z	k7c2	z
písně	píseň	k1gFnSc2	píseň
Podersamer	Podersamer	k1gInSc1	Podersamer
Heimatklänge	Heimatklänge	k1gFnSc4	Heimatklänge
<g/>
:	:	kIx,	:
Na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
žulového	žulový	k2eAgInSc2d1	žulový
bloku	blok	k1gInSc2	blok
je	být	k5eAaImIp3nS	být
vytesán	vytesat	k5eAaPmNgInS	vytesat
nápis	nápis	k1gInSc1	nápis
Egerquelle	Egerquelle	k1gFnSc2	Egerquelle
a	a	k8xC	a
rok	rok	k1gInSc4	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
pramen	pramen	k1gInSc1	pramen
Ohře	Ohře	k1gFnSc1	Ohře
však	však	k9	však
není	být	k5eNaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
původním	původní	k2eAgNnSc7d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
pramen	pramen	k1gInSc4	pramen
Ohře	Ohře	k1gFnSc2	Ohře
považován	považován	k2eAgInSc1d1	považován
jiný	jiný	k2eAgInSc1d1	jiný
pramen	pramen	k1gInSc1	pramen
<g/>
,	,	kIx,	,
od	od	k7c2	od
současného	současný	k2eAgInSc2d1	současný
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
km	km	kA	km
východněji	východně	k6eAd2	východně
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
mlýnem	mlýn	k1gInSc7	mlýn
Weißenhaider	Weißenhaider	k1gInSc1	Weißenhaider
Mühle	Mühle	k1gFnSc2	Mühle
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
pramen	pramen	k1gInSc1	pramen
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Alte	alt	k1gInSc5	alt
Egerquelle	Egerquelle	k1gNnPc7	Egerquelle
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc4d1	starý
pramen	pramen	k1gInSc4	pramen
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příslušný	příslušný	k2eAgInSc1d1	příslušný
krátký	krátký	k2eAgInSc1d1	krátký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
Ohře	Ohře	k1gFnSc2	Ohře
vlévá	vlévat	k5eAaImIp3nS	vlévat
nad	nad	k7c7	nad
osadou	osada	k1gFnSc7	osada
Voitsumra	Voitsumr	k1gInSc2	Voitsumr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
jako	jako	k9	jako
Zinnbach	Zinnbach	k1gInSc1	Zinnbach
(	(	kIx(	(
<g/>
Cínový	cínový	k2eAgInSc1d1	cínový
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
či	či	k8xC	či
Alte	alt	k1gInSc5	alt
Eger	Eger	k1gInSc1	Eger
(	(	kIx(	(
<g/>
Stará	starý	k2eAgFnSc1d1	stará
Ohře	Ohře	k1gFnSc1	Ohře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
pramene	pramen	k1gInSc2	pramen
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgInSc1d1	pamětní
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
vztyčený	vztyčený	k2eAgInSc1d1	vztyčený
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
sudetskými	sudetský	k2eAgMnPc7d1	sudetský
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
odsunutými	odsunutý	k2eAgInPc7d1	odsunutý
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
toku	tok	k1gInSc2	tok
2015	[number]	k4	2015
pramen	pramen	k1gInSc4	pramen
Ohře	Ohře	k1gFnSc2	Ohře
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
horkého	horký	k2eAgNnSc2d1	horké
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
teplého	teplý	k2eAgInSc2d1	teplý
podzimu	podzim	k1gInSc2	podzim
vyschnul	vyschnout	k5eAaPmAgInS	vyschnout
<g/>
.	.	kIx.	.
</s>
<s>
Informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
jak	jak	k6eAd1	jak
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
česká	český	k2eAgNnPc1d1	české
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
už	už	k6eAd1	už
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
neplatí	platit	k5eNaImIp3nS	platit
a	a	k8xC	a
smrkovým	smrkový	k2eAgInSc7d1	smrkový
lesem	les	k1gInSc7	les
již	již	k9	již
potůček	potůček	k1gInSc4	potůček
opět	opět	k6eAd1	opět
proudí	proudit	k5eAaPmIp3nP	proudit
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
prameni	pramen	k1gInSc3	pramen
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
parkoviště	parkoviště	k1gNnSc2	parkoviště
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
spojující	spojující	k2eAgInSc1d1	spojující
Bischofsgrün	Bischofsgrün	k1gInSc1	Bischofsgrün
s	s	k7c7	s
Weißenstadtem	Weißenstadt	k1gInSc7	Weißenstadt
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
parkoviště	parkoviště	k1gNnSc2	parkoviště
k	k	k7c3	k
prameni	pramen	k1gInSc3	pramen
činí	činit	k5eAaImIp3nS	činit
necelých	celý	k2eNgInPc2d1	necelý
200	[number]	k4	200
m.	m.	k?	m.
Na	na	k7c6	na
úbočích	úboč	k1gFnPc6	úboč
Schneebergu	Schneeberg	k1gInSc2	Schneeberg
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
vrchů	vrch	k1gInPc2	vrch
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
700	[number]	k4	700
až	až	k9	až
800	[number]	k4	800
m	m	kA	m
hranice	hranice	k1gFnSc2	hranice
čtyř	čtyři	k4xCgInPc2	čtyři
rozvodí	rozvodí	k1gNnPc2	rozvodí
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Ohře	Ohře	k1gFnSc2	Ohře
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
rozvodí	rozvodí	k1gNnSc4	rozvodí
řek	řeka	k1gFnPc2	řeka
Náby	Nába	k1gMnSc2	Nába
<g/>
,	,	kIx,	,
Sávy	Sáva	k1gFnSc2	Sáva
a	a	k8xC	a
Mohanu	Mohan	k1gInSc2	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Ohři	Ohře	k1gFnSc3	Ohře
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
řada	řada	k1gFnSc1	řada
bájí	báj	k1gFnPc2	báj
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
strážkyni	strážkyně	k1gFnSc4	strážkyně
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgFnSc3d1	krásná
víle	víla	k1gFnSc3	víla
Sibyle	Sibyla	k1gFnSc3	Sibyla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patřila	patřit	k5eAaImAgFnS	patřit
ke	k	k7c3	k
dvanácti	dvanáct	k4xCc2	dvanáct
družkám	družka	k1gFnPc3	družka
víly	víla	k1gFnSc2	víla
Egérie	Egérie	k1gFnSc2	Egérie
<g/>
,	,	kIx,	,
královny	královna	k1gFnSc2	královna
celé	celý	k2eAgFnSc2d1	celá
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dobrotu	dobrota	k1gFnSc4	dobrota
a	a	k8xC	a
léčitelské	léčitelský	k2eAgFnPc4d1	léčitelská
schopnosti	schopnost	k1gFnPc4	schopnost
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
Smrčinách	Smrčiny	k1gFnPc6	Smrčiny
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
krása	krása	k1gFnSc1	krása
oslnila	oslnit	k5eAaPmAgFnS	oslnit
mladého	mladý	k2eAgMnSc4d1	mladý
rytíře	rytíř	k1gMnSc4	rytíř
Arnolda	Arnold	k1gMnSc2	Arnold
z	z	k7c2	z
nedalekého	daleký	k2eNgInSc2d1	nedaleký
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
spatřil	spatřit	k5eAaPmAgMnS	spatřit
sedět	sedět	k5eAaImF	sedět
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
balvanu	balvan	k1gInSc6	balvan
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
setkal	setkat	k5eAaPmAgMnS	setkat
znova	znova	k6eAd1	znova
a	a	k8xC	a
hned	hned	k6eAd1	hned
jí	on	k3xPp3gFnSc3	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Víla	víla	k1gFnSc1	víla
však	však	k9	však
pravila	pravit	k5eAaImAgFnS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zaslíbena	zaslíben	k2eAgFnSc1d1	zaslíbena
panenskému	panenský	k2eAgInSc3d1	panenský
stavu	stav	k1gInSc3	stav
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
nesmí	smět	k5eNaImIp3nS	smět
opouštět	opouštět	k5eAaImF	opouštět
okolí	okolí	k1gNnSc4	okolí
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
úkol	úkol	k1gInSc1	úkol
jej	on	k3xPp3gMnSc4	on
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Kadaně	Kadaň	k1gFnSc2	Kadaň
do	do	k7c2	do
křišťálového	křišťálový	k2eAgInSc2d1	křišťálový
paláce	palác	k1gInSc2	palác
královny	královna	k1gFnSc2	královna
Egérie	Egérie	k1gFnSc2	Egérie
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Sibylu	Sibyla	k1gFnSc4	Sibyla
zbavila	zbavit	k5eAaPmAgFnS	zbavit
oné	onen	k3xDgFnSc2	onen
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sibylininu	Sibylinin	k2eAgFnSc4d1	Sibylinin
službu	služba	k1gFnSc4	služba
nemůže	moct	k5eNaImIp3nS	moct
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ta	ten	k3xDgFnSc1	ten
musí	muset	k5eAaImIp3nS	muset
trvat	trvat	k5eAaImF	trvat
navěky	navěky	k6eAd1	navěky
<g/>
.	.	kIx.	.
</s>
<s>
Požádala	požádat	k5eAaPmAgFnS	požádat
jej	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ochráncem	ochránce	k1gMnSc7	ochránce
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Odměnou	odměna	k1gFnSc7	odměna
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
se	s	k7c7	s
Sibylou	Sibyla	k1gFnSc7	Sibyla
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
řeku	řeka	k1gFnSc4	řeka
vzorně	vzorně	k6eAd1	vzorně
chránil	chránit	k5eAaImAgMnS	chránit
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c4	v
určený	určený	k2eAgInSc4d1	určený
den	den	k1gInSc4	den
u	u	k7c2	u
pramene	pramen	k1gInSc2	pramen
rozprávěl	rozprávět	k5eAaImAgMnS	rozprávět
se	s	k7c7	s
Sibylou	Sibyla	k1gFnSc7	Sibyla
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
válečné	válečný	k2eAgFnSc6d1	válečná
vřavě	vřava	k1gFnSc6	vřava
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
,	,	kIx,	,
Ohře	Ohře	k1gFnSc1	Ohře
se	se	k3xPyFc4	se
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
až	až	k9	až
po	po	k7c4	po
soutok	soutok	k1gInSc4	soutok
s	s	k7c7	s
Labem	Labe	k1gNnSc7	Labe
divoce	divoce	k6eAd1	divoce
rozvodnila	rozvodnit	k5eAaPmAgFnS	rozvodnit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
Sibyla	Sibyla	k1gFnSc1	Sibyla
ronila	ronit	k5eAaImAgFnS	ronit
slzy	slza	k1gFnPc4	slza
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
rytíře	rytíř	k1gMnSc4	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
Ohře	Ohře	k1gFnSc1	Ohře
protéká	protékat	k5eAaImIp3nS	protékat
je	být	k5eAaImIp3nS	být
Weißenstadt	Weißenstadt	k1gInSc1	Weißenstadt
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
městem	město	k1gNnSc7	město
napájí	napájet	k5eAaImIp3nS	napájet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
menšími	malý	k2eAgInPc7d2	menší
vodními	vodní	k2eAgInPc7d1	vodní
toky	tok	k1gInPc7	tok
velké	velká	k1gFnSc2	velká
jezero	jezero	k1gNnSc1	jezero
Weißenstädter	Weißenstädter	k1gInSc4	Weißenstädter
See	See	k1gFnSc2	See
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
využívané	využívaný	k2eAgFnPc1d1	využívaná
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
a	a	k8xC	a
vodním	vodní	k2eAgInPc3d1	vodní
sportům	sport	k1gInPc3	sport
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
jezera	jezero	k1gNnSc2	jezero
už	už	k6eAd1	už
má	mít	k5eAaImIp3nS	mít
Ohře	Ohře	k1gFnSc1	Ohře
charakter	charakter	k1gInSc1	charakter
malé	malý	k2eAgFnSc2d1	malá
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
obcí	obec	k1gFnSc7	obec
Röslau	Röslaus	k1gInSc2	Röslaus
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
města	město	k1gNnSc2	město
Marktleuthen	Marktleuthna	k1gFnPc2	Marktleuthna
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
až	až	k9	až
k	k	k7c3	k
Česko-bavorské	českoavorský	k2eAgFnSc3d1	česko-bavorská
státní	státní	k2eAgFnSc3d1	státní
hranici	hranice	k1gFnSc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skále	skála	k1gFnSc6	skála
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
vypíná	vypínat	k5eAaImIp3nS	vypínat
starobylý	starobylý	k2eAgInSc4d1	starobylý
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
městě	město	k1gNnSc6	město
Hohenberg	Hohenberg	k1gMnSc1	Hohenberg
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Eger	Eger	k1gInSc4	Eger
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
mostem	most	k1gInSc7	most
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
stojí	stát	k5eAaImIp3nP	stát
u	u	k7c2	u
Pfeiffermühle	Pfeiffermühle	k1gMnPc2	Pfeiffermühle
socha	socha	k1gFnSc1	socha
dudáka	dudák	k1gInSc2	dudák
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc2d1	připomínající
dávné	dávný	k2eAgFnSc2d1	dávná
hudební	hudební	k2eAgFnSc2d1	hudební
tradice	tradice	k1gFnSc2	tradice
chebského	chebský	k2eAgNnSc2d1	chebské
pomezí	pomezí	k1gNnSc2	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
jen	jen	k9	jen
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
zvedá	zvedat	k5eAaImIp3nS	zvedat
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Stráň	stráň	k1gFnSc1	stráň
u	u	k7c2	u
Dubiny	dubina	k1gFnSc2	dubina
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
zaniklé	zaniklý	k2eAgFnSc6d1	zaniklá
české	český	k2eAgFnSc6d1	Česká
osadě	osada	k1gFnSc6	osada
Dubina	dubina	k1gFnSc1	dubina
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Hammermühle	Hammermühle	k1gFnSc2	Hammermühle
teče	téct	k5eAaImIp3nS	téct
řeka	řeka	k1gFnSc1	řeka
podél	podél	k7c2	podél
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
pravého	pravý	k2eAgInSc2d1	pravý
břehu	břeh	k1gInSc2	břeh
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
v	v	k7c6	v
říční	říční	k2eAgFnSc6d1	říční
nivě	niva	k1gFnSc6	niva
minerální	minerální	k2eAgInSc1d1	minerální
Karolínin	Karolínin	k2eAgInSc1d1	Karolínin
pramen	pramen	k1gInSc1	pramen
(	(	kIx(	(
<g/>
Carolinenquelle	Carolinenquelle	k1gInSc1	Carolinenquelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
po	po	k7c6	po
bavorské	bavorský	k2eAgFnSc6d1	bavorská
královně	královna	k1gFnSc6	královna
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
..	..	k?	..
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
si	se	k3xPyFc3	se
k	k	k7c3	k
upravenému	upravený	k2eAgInSc3d1	upravený
prameni	pramen	k1gInSc3	pramen
chodí	chodit	k5eAaImIp3nS	chodit
nabírat	nabírat	k5eAaImF	nabírat
minerálku	minerálka	k1gFnSc4	minerálka
do	do	k7c2	do
láhví	láhev	k1gFnPc2	láhev
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
minerálky	minerálka	k1gFnSc2	minerálka
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
i	i	k9	i
čeština	čeština	k1gFnSc1	čeština
na	na	k7c6	na
informačním	informační	k2eAgInSc6d1	informační
panelu	panel	k1gInSc6	panel
u	u	k7c2	u
altánku	altánek	k1gInSc2	altánek
podchyceného	podchycený	k2eAgInSc2d1	podchycený
vývěru	vývěr	k1gInSc2	vývěr
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
podél	podél	k7c2	podél
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
a	a	k8xC	a
po	po	k7c6	po
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
m	m	kA	m
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
překročí	překročit	k5eAaPmIp3nS	překročit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
již	již	k6eAd1	již
teče	téct	k5eAaImIp3nS	téct
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Ohře	Ohře	k1gFnSc1	Ohře
u	u	k7c2	u
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
obce	obec	k1gFnSc2	obec
Pomezná	pomezný	k2eAgFnSc1d1	Pomezná
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
ruina	ruina	k1gFnSc1	ruina
věže	věž	k1gFnSc2	věž
tvrze	tvrz	k1gFnSc2	tvrz
a	a	k8xC	a
hradu	hrad	k1gInSc2	hrad
Pomezná	pomezný	k2eAgNnPc1d1	pomezný
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
do	do	k7c2	do
Ohře	Ohře	k1gFnSc2	Ohře
vlévá	vlévat	k5eAaImIp3nS	vlévat
Reslava	Reslava	k1gFnSc1	Reslava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
činí	činit	k5eAaImIp3nS	činit
pouhých	pouhý	k2eAgInPc2d1	pouhý
2,4	[number]	k4	2,4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
Rathsam	Rathsam	k1gInSc1	Rathsam
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc1d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Skalka	skalka	k1gFnSc1	skalka
a	a	k8xC	a
Chebem	Cheb	k1gInSc7	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
levým	levý	k2eAgInSc7d1	levý
břehem	břeh	k1gInSc7	břeh
se	se	k3xPyFc4	se
na	na	k7c4	na
návrší	návrší	k1gNnPc4	návrší
zvedá	zvedat	k5eAaImIp3nS	zvedat
starobylý	starobylý	k2eAgInSc1d1	starobylý
chebský	chebský	k2eAgInSc1d1	chebský
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Chebem	Cheb	k1gInSc7	Cheb
u	u	k7c2	u
Chocovic	Chocovice	k1gFnPc2	Chocovice
vtéká	vtékat	k5eAaImIp3nS	vtékat
Ohře	Ohře	k1gFnPc4	Ohře
do	do	k7c2	do
evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnSc2d1	významná
lokality	lokalita	k1gFnSc2	lokalita
Ramena	rameno	k1gNnSc2	rameno
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
meandrující	meandrující	k2eAgFnSc6d1	meandrující
části	část	k1gFnSc6	část
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
meandry	meandr	k1gInPc1	meandr
někdy	někdy	k6eAd1	někdy
otáčejí	otáčet	k5eAaImIp3nP	otáčet
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
180	[number]	k4	180
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
slepých	slepý	k2eAgNnPc2d1	slepé
ramen	rameno	k1gNnPc2	rameno
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vytvářejících	vytvářející	k2eAgFnPc2d1	vytvářející
někde	někde	k6eAd1	někde
soustavy	soustava	k1gFnSc2	soustava
propojených	propojený	k2eAgFnPc2d1	propojená
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
pravého	pravý	k2eAgInSc2d1	pravý
břehu	břeh	k1gInSc2	břeh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zámek	zámek	k1gInSc1	zámek
Mostov	Mostov	k1gInSc1	Mostov
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ke	k	k7c3	k
Kynšperku	Kynšperk	k1gInSc3	Kynšperk
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
Sokolovské	sokolovský	k2eAgFnSc6d1	Sokolovská
pánvi	pánev	k1gFnSc6	pánev
protéká	protékat	k5eAaImIp3nS	protékat
Dasnicemi	Dasnice	k1gFnPc7	Dasnice
a	a	k8xC	a
Šabinou	Šabina	k1gFnSc7	Šabina
<g/>
,	,	kIx,	,
před	před	k7c7	před
Černým	černý	k2eAgInSc7d1	černý
mlýnem	mlýn	k1gInSc7	mlýn
u	u	k7c2	u
elektrárny	elektrárna	k1gFnSc2	elektrárna
Tisová	tisový	k2eAgFnSc1d1	Tisová
opouští	opouštět	k5eAaImIp3nS	opouštět
evropsky	evropsky	k6eAd1	evropsky
významnou	významný	k2eAgFnSc4d1	významná
lokalitu	lokalita	k1gFnSc4	lokalita
a	a	k8xC	a
přitéká	přitékat	k5eAaImIp3nS	přitékat
k	k	k7c3	k
Sokolovu	Sokolov	k1gInSc3	Sokolov
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
z	z	k7c2	z
Ohře	Ohře	k1gFnSc2	Ohře
odebírá	odebírat	k5eAaImIp3nS	odebírat
voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
umělé	umělý	k2eAgNnSc4d1	umělé
jezero	jezero	k1gNnSc4	jezero
Medard	Medard	k1gMnSc1	Medard
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Sokolovem	Sokolov	k1gInSc7	Sokolov
v	v	k7c4	v
Královské	královský	k2eAgNnSc4d1	královské
Poříčí	Poříčí	k1gNnSc4	Poříčí
se	se	k3xPyFc4	se
na	na	k7c6	na
statku	statek	k1gInSc6	statek
Bernard	Bernard	k1gMnSc1	Bernard
nachází	nacházet	k5eAaImIp3nS	nacházet
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
expozice	expozice	k1gFnSc1	expozice
řeky	řeka	k1gFnSc2	řeka
–	–	k?	–
Centrum	centrum	k1gNnSc1	centrum
řeky	řeka	k1gFnSc2	řeka
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k9	až
2011	[number]	k4	2011
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zábavně	zábavně	k6eAd1	zábavně
naučná	naučný	k2eAgFnSc1d1	naučná
výstava	výstava	k1gFnSc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
interaktivních	interaktivní	k2eAgInPc2d1	interaktivní
modelů	model	k1gInPc2	model
představuje	představovat	k5eAaImIp3nS	představovat
zajímavosti	zajímavost	k1gFnSc2	zajímavost
celého	celý	k2eAgInSc2d1	celý
toku	tok	k1gInSc2	tok
Ohře	Ohře	k1gFnSc2	Ohře
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
až	až	k9	až
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
východního	východní	k2eAgNnSc2d1	východní
křídla	křídlo	k1gNnSc2	křídlo
statku	statek	k1gInSc2	statek
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mnoho	mnoho	k4c1	mnoho
statických	statický	k2eAgMnPc2d1	statický
i	i	k8xC	i
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc1d1	umístěný
umělecky	umělecky	k6eAd1	umělecky
volně	volně	k6eAd1	volně
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
model	model	k1gInSc1	model
pramene	pramen	k1gInSc2	pramen
Ohře	Ohře	k1gFnSc2	Ohře
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
a	a	k8xC	a
malíře	malíř	k1gMnSc2	malíř
Jiřího	Jiří	k1gMnSc2	Jiří
Černého	Černý	k1gMnSc2	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
podtéká	podtékat	k5eAaImIp3nS	podtékat
silniční	silniční	k2eAgInSc4d1	silniční
most	most	k1gInSc4	most
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
D6	D6	k1gFnSc6	D6
a	a	k8xC	a
v	v	k7c6	v
sevřeném	sevřený	k2eAgNnSc6d1	sevřené
údolí	údolí	k1gNnSc6	údolí
až	až	k9	až
do	do	k7c2	do
Starého	Starého	k2eAgNnSc2d1	Starého
Sedla	sedlo	k1gNnSc2	sedlo
protéká	protékat	k5eAaImIp3nS	protékat
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
Údolí	údolí	k1gNnSc2	údolí
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strmých	strmý	k2eAgInPc2d1	strmý
svahů	svah	k1gInPc2	svah
vystupujícími	vystupující	k2eAgInPc7d1	vystupující
skalními	skalní	k2eAgInPc7d1	skalní
bloky	blok	k1gInPc7	blok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
pseudokrasové	pseudokrasový	k2eAgFnPc1d1	pseudokrasová
jeskyně	jeskyně	k1gFnPc1	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Pseudokrasové	Pseudokrasový	k2eAgInPc1d1	Pseudokrasový
jevy	jev	k1gInPc1	jev
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
boční	boční	k2eAgFnSc7d1	boční
výmolovou	výmolový	k2eAgFnSc7d1	Výmolová
činností	činnost	k1gFnSc7	činnost
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lokti	loket	k1gInSc6	loket
opouští	opouštět	k5eAaImIp3nS	opouštět
řeka	řeka	k1gFnSc1	řeka
podkrušnohorské	podkrušnohorský	k2eAgFnSc2d1	Podkrušnohorská
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
obloukem	oblouk	k1gInSc7	oblouk
obtéká	obtékat	k5eAaImIp3nS	obtékat
město	město	k1gNnSc4	město
Loket	loket	k1gInSc1	loket
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
územím	území	k1gNnSc7	území
Slavkovského	slavkovský	k2eAgInSc2d1	slavkovský
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
severnímu	severní	k2eAgInSc3d1	severní
okraji	okraj	k1gInSc3	okraj
CHKO	CHKO	kA	CHKO
Slavkovský	slavkovský	k2eAgInSc4d1	slavkovský
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
strmém	strmý	k2eAgInSc6d1	strmý
skalním	skalní	k2eAgInSc6d1	skalní
ostrohu	ostroh	k1gInSc6	ostroh
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
loketský	loketský	k2eAgInSc1d1	loketský
hrad	hrad	k1gInSc1	hrad
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
až	až	k9	až
do	do	k7c2	do
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
teče	téct	k5eAaImIp3nS	téct
chráněným	chráněný	k2eAgNnSc7d1	chráněné
územím	území	k1gNnSc7	území
i	i	k9	i
evropsky	evropsky	k6eAd1	evropsky
významnou	významný	k2eAgFnSc7d1	významná
lokalitou	lokalita	k1gFnSc7	lokalita
Kaňon	kaňon	k1gInSc1	kaňon
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
dospěje	dochvít	k5eAaPmIp3nS	dochvít
k	k	k7c3	k
Doubí	doubí	k1gNnSc3	doubí
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
bájemi	báj	k1gFnPc7	báj
a	a	k8xC	a
pověstmi	pověst	k1gFnPc7	pověst
opředené	opředený	k2eAgNnSc4d1	opředené
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc4d1	národní
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
Svatošské	Svatošský	k2eAgFnSc2d1	Svatošská
skály	skála	k1gFnSc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gNnSc4	on
množství	množství	k1gNnSc4	množství
skalních	skalní	k2eAgInPc2d1	skalní
pilířů	pilíř	k1gInPc2	pilíř
<g/>
,	,	kIx,	,
hranolů	hranol	k1gInPc2	hranol
a	a	k8xC	a
jehlanů	jehlan	k1gInPc2	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgInPc1d1	skalní
útvary	útvar	k1gInPc1	útvar
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
postupným	postupný	k2eAgNnSc7d1	postupné
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
a	a	k8xC	a
odnosem	odnos	k1gInSc7	odnos
žulových	žulový	k2eAgInPc2d1	žulový
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
činností	činnost	k1gFnSc7	činnost
řeky	řeka	k1gFnSc2	řeka
i	i	k8xC	i
erozivním	erozivní	k2eAgNnSc7d1	erozivní
působením	působení	k1gNnSc7	působení
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
pověstí	pověst	k1gFnPc2	pověst
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
svatební	svatební	k2eAgInSc4d1	svatební
průvod	průvod	k1gInSc4	průvod
Jana	Jan	k1gMnSc2	Jan
Svatoše	Svatoš	k1gMnSc2	Svatoš
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nechala	nechat	k5eAaPmAgFnS	nechat
změnit	změnit	k5eAaPmF	změnit
v	v	k7c4	v
kámen	kámen	k1gInSc4	kámen
vodní	vodní	k2eAgFnSc1d1	vodní
víla	víla	k1gFnSc1	víla
z	z	k7c2	z
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Svatoš	Svatoš	k1gMnSc1	Svatoš
porušil	porušit	k5eAaPmAgMnS	porušit
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neožení	oženit	k5eNaPmIp3nS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
slib	slib	k1gInSc4	slib
jí	jíst	k5eAaImIp3nS	jíst
dal	dát	k5eAaPmAgMnS	dát
Jan	Jan	k1gMnSc1	Jan
Svatoš	Svatoš	k1gMnSc1	Svatoš
(	(	kIx(	(
<g/>
Hans	Hans	k1gMnSc1	Hans
Heiling	Heiling	k1gInSc1	Heiling
<g/>
)	)	kIx)	)
za	za	k7c4	za
protislužbu	protislužba	k1gFnSc4	protislužba
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
víla	víla	k1gFnSc1	víla
zasvětí	zasvětit	k5eAaPmIp3nS	zasvětit
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
pozemské	pozemský	k2eAgFnSc2d1	pozemská
dívky	dívka	k1gFnSc2	dívka
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Nedodržení	nedodržení	k1gNnSc1	nedodržení
jeho	on	k3xPp3gInSc2	on
slibu	slib	k1gInSc2	slib
odnesli	odnést	k5eAaPmAgMnP	odnést
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
ve	v	k7c4	v
svatební	svatební	k2eAgInSc4d1	svatební
den	den	k1gInSc4	den
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
svatebčané	svatebčan	k1gMnPc1	svatebčan
<g/>
,	,	kIx,	,
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
kletbou	kletba	k1gFnSc7	kletba
víly	víla	k1gFnSc2	víla
do	do	k7c2	do
jednoho	jeden	k4xCgMnSc2	jeden
zkameněli	zkamenět	k5eAaPmAgMnP	zkamenět
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc4	místo
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
J.	J.	kA	J.
W.	W.	kA	W.
Goetha	Goetha	k1gMnSc1	Goetha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
místo	místo	k6eAd1	místo
několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
Hansi	Hans	k1gMnSc6	Hans
Heilingovi	Heiling	k1gMnSc6	Heiling
byla	být	k5eAaImAgNnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
inspiračních	inspirační	k2eAgInPc2d1	inspirační
zdrojů	zdroj	k1gInPc2	zdroj
Goethova	Goethův	k2eAgNnSc2d1	Goethovo
díla	dílo	k1gNnSc2	dílo
o	o	k7c6	o
Faustovi	Fausta	k1gMnSc6	Fausta
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
pracoval	pracovat	k5eAaImAgInS	pracovat
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
levým	levý	k2eAgInSc7d1	levý
břehem	břeh	k1gInSc7	břeh
Ohře	Ohře	k1gFnSc2	Ohře
se	se	k3xPyFc4	se
na	na	k7c6	na
vyvýšeném	vyvýšený	k2eAgInSc6d1	vyvýšený
skalním	skalní	k2eAgInSc6d1	skalní
suku	suk	k1gInSc6	suk
nachází	nacházet	k5eAaImIp3nP	nacházet
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
starého	starý	k2eAgNnSc2d1	staré
hradiště	hradiště	k1gNnSc2	hradiště
Tašovice	Tašovice	k1gFnSc2	Tašovice
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
první	první	k4xOgNnSc1	první
osídlení	osídlení	k1gNnSc1	osídlení
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
mezolitu	mezolit	k1gInSc2	mezolit
(	(	kIx(	(
<g/>
10	[number]	k4	10
000	[number]	k4	000
–	–	k?	–
8000	[number]	k4	8000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Karlovými	Karlův	k2eAgInPc7d1	Karlův
Vary	Vary	k1gInPc7	Vary
přitéká	přitékat	k5eAaImIp3nS	přitékat
Ohře	Ohře	k1gFnSc1	Ohře
do	do	k7c2	do
Doupovských	Doupovských	k2eAgInSc2d1	Doupovských
hor.	hor.	k?	hor.
Nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Šemnice	Šemnice	k1gFnSc2	Šemnice
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
znělcová	znělcový	k2eAgFnSc1d1	znělcová
kupa	kupa	k1gFnSc1	kupa
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Šemnická	Šemnický	k2eAgFnSc1d1	Šemnická
skála	skála	k1gFnSc1	skála
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
pravěké	pravěký	k2eAgFnSc2d1	pravěká
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgInSc1d1	unikátní
geologický	geologický	k2eAgInSc1d1	geologický
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Skalky	skalka	k1gFnSc2	skalka
skřítků	skřítek	k1gMnPc2	skřítek
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
řeka	řeka	k1gFnSc1	řeka
Ohře	Ohře	k1gFnSc2	Ohře
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
zahlubování	zahlubování	k1gNnSc6	zahlubování
ve	v	k7c6	v
vulkanických	vulkanický	k2eAgFnPc6d1	vulkanická
vrstvách	vrstva	k1gFnPc6	vrstva
obnažila	obnažit	k5eAaPmAgFnS	obnažit
kanálkovité	kanálkovitý	k2eAgFnPc4d1	kanálkovitý
dutiny	dutina	k1gFnPc4	dutina
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
po	po	k7c6	po
vyhnilých	vyhnilý	k2eAgInPc6d1	vyhnilý
kmenech	kmen	k1gInPc6	kmen
třetihorních	třetihorní	k2eAgInPc2d1	třetihorní
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
zalitých	zalitý	k2eAgFnPc2d1	zalitá
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
lávou	láva	k1gFnSc7	láva
<g/>
.	.	kIx.	.
</s>
<s>
Peřejnatým	peřejnatý	k2eAgInSc7d1	peřejnatý
úsekem	úsek	k1gInSc7	úsek
přitéká	přitékat	k5eAaImIp3nS	přitékat
ke	k	k7c3	k
Kyselce	kyselka	k1gFnSc3	kyselka
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
půvabným	půvabný	k2eAgFnPc3d1	půvabná
lázním	lázeň	k1gFnPc3	lázeň
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
Heinrich	Heinrich	k1gMnSc1	Heinrich
Mattoni	Mattoň	k1gFnSc6	Mattoň
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
expedovaná	expedovaný	k2eAgFnSc1d1	expedovaná
minerální	minerální	k2eAgFnSc1d1	minerální
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Kyselkou	kyselka	k1gFnSc7	kyselka
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
Bučina	bučina	k1gFnSc1	bučina
(	(	kIx(	(
<g/>
582	[number]	k4	582
m	m	kA	m
<g/>
)	)	kIx)	)
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
ze	z	k7c2	z
ketré	ketrý	k2eAgFnSc2d1	ketrá
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
vidět	vidět	k5eAaImF	vidět
úsek	úsek	k1gInSc4	úsek
Ohře	Ohře	k1gFnSc2	Ohře
až	až	k9	až
k	k	k7c3	k
Radošovu	Radošův	k2eAgMnSc3d1	Radošův
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
staré	starý	k2eAgFnSc6d1	stará
královské	královský	k2eAgFnSc6d1	královská
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Lokte	loket	k1gInSc2	loket
protéká	protékat	k5eAaImIp3nS	protékat
Ohře	Ohře	k1gFnSc1	Ohře
v	v	k7c6	v
Radošově	Radošův	k2eAgFnSc6d1	Radošova
pod	pod	k7c7	pod
historickým	historický	k2eAgInSc7d1	historický
dřevěným	dřevěný	k2eAgInSc7d1	dřevěný
mostem	most	k1gInSc7	most
se	s	k7c7	s
sedlovou	sedlový	k2eAgFnSc7d1	sedlová
šindelovou	šindelový	k2eAgFnSc7d1	Šindelová
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
Velichovem	Velichov	k1gInSc7	Velichov
<g/>
,	,	kIx,	,
Vojkovicemi	Vojkovice	k1gFnPc7	Vojkovice
a	a	k8xC	a
Stráží	strážit	k5eAaImIp3nP	strážit
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
než	než	k8xS	než
dospěje	dochvít	k5eAaPmIp3nS	dochvít
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Boč	bočit	k5eAaImRp2nS	bočit
zvedá	zvedat	k5eAaImIp3nS	zvedat
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
levým	levý	k2eAgInSc7d1	levý
břehem	břeh	k1gInSc7	břeh
nápadná	nápadný	k2eAgFnSc1d1	nápadná
skála	skála	k1gFnSc1	skála
s	s	k7c7	s
čedičovými	čedičový	k2eAgInPc7d1	čedičový
sloupy	sloup	k1gInPc7	sloup
chráněné	chráněný	k2eAgFnSc3d1	chráněná
přírodní	přírodní	k2eAgFnSc3d1	přírodní
památce	památka	k1gFnSc3	památka
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
Čedičová	čedičový	k2eAgFnSc1d1	čedičová
žíla	žíla	k1gFnSc1	žíla
Boč	bočit	k5eAaImRp2nS	bočit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Bočí	bočit	k5eAaImIp3nS	bočit
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
okresů	okres	k1gInPc2	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
a	a	k8xC	a
Chomutov	Chomutov	k1gInSc1	Chomutov
a	a	k8xC	a
k.	k.	k?	k.
ú.	ú.	k?	ú.
Korunní	korunní	k2eAgMnSc1d1	korunní
a	a	k8xC	a
Okounov	Okounov	k1gInSc1	Okounov
<g/>
,	,	kIx,	,
vtéká	vtékat	k5eAaImIp3nS	vtékat
Ohře	Ohře	k1gFnSc1	Ohře
do	do	k7c2	do
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
vesnici	vesnice	k1gFnSc4	vesnice
Lužný	Lužný	k2eAgMnSc1d1	Lužný
<g/>
,	,	kIx,	,
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
u	u	k7c2	u
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
nad	nad	k7c7	nad
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
Hučivým	hučivý	k2eAgInSc7d1	hučivý
potokem	potok	k1gInSc7	potok
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Louka	louka	k1gFnSc1	louka
vstavačů	vstavač	k1gInPc2	vstavač
u	u	k7c2	u
Černýše	černýš	k1gInSc2	černýš
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
výskytem	výskyt	k1gInSc7	výskyt
prstnatce	prstnatec	k1gMnSc2	prstnatec
májového	májový	k2eAgMnSc2d1	májový
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
vesnici	vesnice	k1gFnSc4	vesnice
Kotvina	Kotvina	k1gFnSc1	Kotvina
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
levým	levý	k2eAgInSc7d1	levý
břehem	břeh	k1gInSc7	břeh
zvedá	zvedat	k5eAaImIp3nS	zvedat
výrazný	výrazný	k2eAgInSc1d1	výrazný
vrch	vrch	k1gInSc1	vrch
Šumburg	Šumburg	k1gInSc1	Šumburg
se	se	k3xPyFc4	se
ruinou	ruina	k1gFnSc7	ruina
hradu	hrad	k1gInSc2	hrad
Šumburg	Šumburg	k1gInSc1	Šumburg
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
přitéká	přitékat	k5eAaImIp3nS	přitékat
k	k	k7c3	k
městu	město	k1gNnSc3	město
Klášterec	Klášterec	k1gInSc1	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
vyvěrají	vyvěrat	k5eAaImIp3nP	vyvěrat
známé	známý	k2eAgInPc1d1	známý
minerální	minerální	k2eAgInPc1d1	minerální
prameny	pramen	k1gInPc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
historie	historie	k1gFnSc1	historie
započala	započnout	k5eAaPmAgFnS	započnout
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
místní	místní	k2eAgMnSc1d1	místní
občan	občan	k1gMnSc1	občan
narazil	narazit	k5eAaPmAgMnS	narazit
při	při	k7c6	při
kopání	kopání	k1gNnSc6	kopání
studně	studně	k1gFnSc2	studně
na	na	k7c4	na
lahodnou	lahodný	k2eAgFnSc4d1	lahodná
mineralizovanou	mineralizovaný	k2eAgFnSc4d1	mineralizovaná
vodu	voda	k1gFnSc4	voda
s	s	k7c7	s
příjemně	příjemně	k6eAd1	příjemně
kyselou	kyselý	k2eAgFnSc7d1	kyselá
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oficiálnímu	oficiální	k2eAgNnSc3d1	oficiální
zrušení	zrušení	k1gNnSc3	zrušení
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
však	však	k9	však
po	po	k7c6	po
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
peripetiích	peripetie	k1gFnPc6	peripetie
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Lázně	lázeň	k1gFnSc2	lázeň
Evženie	Evženie	k1gFnSc2	Evženie
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
kyselka	kyselka	k1gFnSc1	kyselka
z	z	k7c2	z
Klášterce	Klášterec	k1gInSc2	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
vozila	vozit	k5eAaImAgFnS	vozit
Rommelovo	Rommelův	k2eAgNnSc1d1	Rommelovo
armádě	armáda	k1gFnSc3	armáda
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Ohařské	Ohařský	k2eAgFnSc6d1	Ohařský
louce	louka	k1gFnSc6	louka
objeven	objevit	k5eAaPmNgInS	objevit
další	další	k2eAgInSc1d1	další
pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Klášterce	Klášterec	k1gInSc2	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
opouští	opouštět	k5eAaImIp3nS	opouštět
řeka	řeka	k1gFnSc1	řeka
Doupovské	Doupovský	k2eAgFnSc2d1	Doupovská
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Mosteckou	mostecký	k2eAgFnSc7d1	Mostecká
pánví	pánev	k1gFnSc7	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Klášterci	Klášterec	k1gInSc3	Klášterec
se	se	k3xPyFc4	se
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
rytíři	rytíř	k1gMnSc6	rytíř
Romualdovi	Romuald	k1gMnSc6	Romuald
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
vodní	vodní	k2eAgFnSc1d1	vodní
víla	víla	k1gFnSc1	víla
Ludga	Ludga	k1gFnSc1	Ludga
z	z	k7c2	z
družiny	družina	k1gFnSc2	družina
vodních	vodní	k2eAgFnPc2d1	vodní
panen	panna	k1gFnPc2	panna
nymfy	nymfa	k1gFnSc2	nymfa
Egérie	Egérie	k1gFnSc2	Egérie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
opustila	opustit	k5eAaPmAgFnS	opustit
křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
palác	palác	k1gInSc4	palác
ve	v	k7c6	v
vlnách	vlna	k1gFnPc6	vlna
Ohře	Ohře	k1gFnSc2	Ohře
pod	pod	k7c7	pod
Kadaní	Kadaň	k1gFnSc7	Kadaň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
rytíř	rytíř	k1gMnSc1	rytíř
dostal	dostat	k5eAaPmAgInS	dostat
hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
závoj	závoj	k1gInSc4	závoj
a	a	k8xC	a
lahvičku	lahvička	k1gFnSc4	lahvička
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
však	však	k9	však
přísahat	přísahat	k5eAaImF	přísahat
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Uběhlo	uběhnout	k5eAaPmAgNnS	uběhnout
mnoho	mnoho	k4c1	mnoho
šťastných	šťastný	k2eAgNnPc2d1	šťastné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
pořádal	pořádat	k5eAaImAgMnS	pořádat
velký	velký	k2eAgInSc4d1	velký
turnaj	turnaj	k1gInSc4	turnaj
<g/>
,	,	kIx,	,
odejel	odejet	k5eAaPmAgMnS	odejet
rytíř	rytíř	k1gMnSc1	rytíř
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
klání	klání	k1gNnSc4	klání
a	a	k8xC	a
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
bojů	boj	k1gInPc2	boj
vyšel	vyjít	k5eAaPmAgInS	vyjít
vítězně	vítězně	k6eAd1	vítězně
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
krásná	krásný	k2eAgFnSc1d1	krásná
hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
daleké	daleký	k2eAgFnSc2d1	daleká
země	zem	k1gFnSc2	zem
jej	on	k3xPp3gMnSc4	on
dekorovala	dekorovat	k5eAaBmAgFnS	dekorovat
řetězem	řetěz	k1gInSc7	řetěz
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
Romuald	Romuald	k1gMnSc1	Romuald
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
Ludgu	Ludga	k1gFnSc4	Ludga
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zásnubní	zásnubní	k2eAgFnSc6d1	zásnubní
hostině	hostina	k1gFnSc6	hostina
se	se	k3xPyFc4	se
rozvířily	rozvířit	k5eAaPmAgFnP	rozvířit
vody	voda	k1gFnPc1	voda
studní	studna	k1gFnPc2	studna
<g/>
,	,	kIx,	,
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
ženská	ženský	k2eAgFnSc1d1	ženská
postava	postava	k1gFnSc1	postava
a	a	k8xC	a
tiše	tiš	k1gFnSc2	tiš
prošla	projít	k5eAaPmAgFnS	projít
hodovním	hodovní	k2eAgInSc7d1	hodovní
sálem	sál	k1gInSc7	sál
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
zazněl	zaznět	k5eAaImAgInS	zaznět
výkřik	výkřik	k1gInSc1	výkřik
a	a	k8xC	a
Romuald	Romuald	k1gInSc1	Romuald
klesnul	klesnout	k5eAaPmAgInS	klesnout
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
srdci	srdce	k1gNnSc6	srdce
vězela	vězet	k5eAaImAgFnS	vězet
jehla	jehla	k1gFnSc1	jehla
s	s	k7c7	s
rudým	rudý	k2eAgInSc7d1	rudý
kamenem	kámen	k1gInSc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Zhrzená	zhrzený	k2eAgFnSc1d1	zhrzená
vodní	vodní	k2eAgFnSc1d1	vodní
panna	panna	k1gFnSc1	panna
se	se	k3xPyFc4	se
krutě	krutě	k6eAd1	krutě
pomstila	pomstit	k5eAaImAgFnS	pomstit
za	za	k7c4	za
Romualdovu	Romualdův	k2eAgFnSc4d1	Romualdova
zradu	zrada	k1gFnSc4	zrada
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
Klášterec	Klášterec	k1gInSc4	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zrcadlí	zrcadlit	k5eAaImIp3nS	zrcadlit
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Ohře	Ohře	k1gFnSc1	Ohře
do	do	k7c2	do
Kadaně	Kadaň	k1gFnSc2	Kadaň
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Kadaní	Kadaň	k1gFnSc7	Kadaň
plní	plnit	k5eAaImIp3nS	plnit
řeka	řeka	k1gFnSc1	řeka
Ohře	Ohře	k1gFnSc2	Ohře
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Kadaň	Kadaň	k1gFnSc4	Kadaň
<g/>
,	,	kIx,	,
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Kadaní	Kadaň	k1gFnSc7	Kadaň
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
ostrým	ostrý	k2eAgInSc7d1	ostrý
zákrutem	zákrut	k1gInSc7	zákrut
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
Želinský	Želinský	k2eAgInSc1d1	Želinský
meandr	meandr	k1gInSc1	meandr
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
meandru	meandr	k1gInSc2	meandr
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
starého	starý	k2eAgNnSc2d1	staré
slovanského	slovanský	k2eAgNnSc2d1	slovanské
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
Želiny	Želina	k1gFnSc2	Želina
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
Ohře	Ohře	k1gFnSc2	Ohře
plní	plnit	k5eAaImIp3nS	plnit
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Nechranice	Nechranice	k1gFnSc1	Nechranice
<g/>
,	,	kIx,	,
vybudovanou	vybudovaný	k2eAgFnSc4d1	vybudovaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
až	až	k9	až
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Nádrž	nádrž	k1gFnSc1	nádrž
s	s	k7c7	s
betonovou	betonový	k2eAgFnSc7d1	betonová
<g/>
,	,	kIx,	,
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hrází	hráz	k1gFnSc7	hráz
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
hydroenergetickým	hydroenergetický	k2eAgFnPc3d1	hydroenergetická
<g/>
,	,	kIx,	,
průmyslovým	průmyslový	k2eAgFnPc3d1	průmyslová
a	a	k8xC	a
rekreačním	rekreační	k2eAgInPc3d1	rekreační
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
mezi	mezi	k7c7	mezi
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
a	a	k8xC	a
areálem	areál	k1gInSc7	areál
elektrárny	elektrárna	k1gFnSc2	elektrárna
Tušimice	Tušimika	k1gFnSc3	Tušimika
II	II	kA	II
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Běšický	Běšický	k2eAgInSc4d1	Běšický
chochol	chochol	k1gInSc4	chochol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
toku	tok	k1gInSc2	tok
pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
se	se	k3xPyFc4	se
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Stroupeč	Stroupeč	k1gInSc1	Stroupeč
na	na	k7c6	na
terasovitých	terasovitý	k2eAgInPc6d1	terasovitý
stupních	stupeň	k1gInPc6	stupeň
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgNnSc1d1	další
chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Stroupeč	Stroupeč	k1gInSc1	Stroupeč
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
již	již	k6eAd1	již
řeka	řeka	k1gFnSc1	řeka
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
městu	město	k1gNnSc3	město
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc4d1	typická
siluetu	silueta	k1gFnSc4	silueta
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
poškodit	poškodit	k5eAaPmF	poškodit
ani	ani	k9	ani
výstavbou	výstavba	k1gFnSc7	výstavba
sídlišť	sídliště	k1gNnPc2	sídliště
pro	pro	k7c4	pro
horníky	horník	k1gMnPc4	horník
a	a	k8xC	a
vyhnance	vyhnanec	k1gMnSc4	vyhnanec
ze	z	k7c2	z
zaniklých	zaniklý	k2eAgFnPc2d1	zaniklá
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
teče	téct	k5eAaImIp3nS	téct
při	při	k7c6	při
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Postoloprt	Postoloprt	k1gInSc1	Postoloprt
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Loun	Louny	k1gInPc2	Louny
<g/>
.	.	kIx.	.
</s>
<s>
Opouští	opouštět	k5eAaImIp3nS	opouštět
Mosteckou	mostecký	k2eAgFnSc4d1	Mostecká
pánev	pánev	k1gFnSc4	pánev
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
již	již	k6eAd1	již
až	až	k9	až
do	do	k7c2	do
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Labem	Labe	k1gNnSc7	Labe
teče	teč	k1gInSc2	teč
územím	území	k1gNnSc7	území
v	v	k7c4	v
Dolnooharské	Dolnooharský	k2eAgFnPc4d1	Dolnooharský
tabule	tabule	k1gFnPc4	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Míjí	míjet	k5eAaImIp3nS	míjet
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
a	a	k8xC	a
současně	současně	k6eAd1	současně
významnou	významný	k2eAgFnSc4d1	významná
paleontologickou	paleontologický	k2eAgFnSc4d1	paleontologická
lokalitu	lokalita	k1gFnSc4	lokalita
Březno	Březno	k1gNnSc1	Březno
u	u	k7c2	u
Postoloprt	Postoloprta	k1gFnPc2	Postoloprta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lounech	Louny	k1gInPc6	Louny
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
jeden	jeden	k4xCgInSc4	jeden
kilometr	kilometr	k1gInSc4	kilometr
od	od	k7c2	od
pravého	pravý	k2eAgInSc2d1	pravý
břehu	břeh	k1gInSc2	břeh
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
naražen	naražen	k2eAgInSc1d1	naražen
geologickým	geologický	k2eAgInSc7d1	geologický
vrtem	vrt	k1gInSc7	vrt
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
1100	[number]	k4	1100
až	až	k9	až
1200	[number]	k4	1200
metrů	metr	k1gInPc2	metr
pramen	pramen	k1gInSc4	pramen
Luna	luna	k1gFnSc1	luna
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
místní	místní	k2eAgFnSc7d1	místní
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
Luna	luna	k1gFnSc1	luna
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vyveden	vyvést	k5eAaPmNgInS	vyvést
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
dostupný	dostupný	k2eAgInSc1d1	dostupný
<g/>
.	.	kIx.	.
</s>
<s>
Ohře	Ohře	k1gFnSc1	Ohře
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Libochovic	Libochovice	k1gFnPc2	Libochovice
<g/>
,	,	kIx,	,
u	u	k7c2	u
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
stojí	stát	k5eAaImIp3nS	stát
barokní	barokní	k2eAgInSc4d1	barokní
zámek	zámek	k1gInSc4	zámek
Libochovice	Libochovice	k1gFnPc4	Libochovice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
městem	město	k1gNnSc7	město
na	na	k7c4	na
ř.	ř.	k?	ř.
km	km	kA	km
22,3	[number]	k4	22,3
začíná	začínat	k5eAaImIp3nS	začínat
umělé	umělý	k2eAgNnSc4d1	umělé
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
11	[number]	k4	11
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
náhon	náhon	k1gInSc4	náhon
Malá	malý	k2eAgFnSc1d1	malá
Ohře	Ohře	k1gFnSc1	Ohře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
Budyní	Budyně	k1gFnSc7	Budyně
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
a	a	k8xC	a
za	za	k7c7	za
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
řekou	řeka	k1gFnSc7	řeka
spojí	spojit	k5eAaPmIp3nP	spojit
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Pístecký	Pístecký	k2eAgInSc4d1	Pístecký
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
13,7	[number]	k4	13,7
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
z	z	k7c2	z
Ohře	Ohře	k1gFnSc2	Ohře
u	u	k7c2	u
jezu	jez	k1gInSc2	jez
v	v	k7c6	v
Hostěnicích	Hostěnice	k1gFnPc6	Hostěnice
4,8	[number]	k4	4,8
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
Brozanský	Brozanský	k2eAgInSc1d1	Brozanský
náhon	náhon	k1gInSc1	náhon
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
mlýna	mlýn	k1gInSc2	mlýn
v	v	k7c6	v
Brozanech	Brozan	k1gInPc6	Brozan
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ohře	Ohře	k1gFnSc2	Ohře
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
v	v	k7c6	v
Doksanech	Doksan	k1gInPc6	Doksan
u	u	k7c2	u
cukrovaru	cukrovar	k1gInSc2	cukrovar
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
Bohušovic	Bohušovice	k1gFnPc2	Bohušovice
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
přitéká	přitékat	k5eAaImIp3nS	přitékat
řeka	řeka	k1gFnSc1	řeka
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
u	u	k7c2	u
pravého	pravý	k2eAgInSc2d1	pravý
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
nachází	nacházet	k5eAaImIp3nS	nacházet
Malá	malý	k2eAgFnSc1d1	malá
pevnost	pevnost	k1gFnSc1	pevnost
Terezín	Terezín	k1gInSc1	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
k	k	k7c3	k
Litoměřicím	Litoměřice	k1gInPc3	Litoměřice
a	a	k8xC	a
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
u	u	k7c2	u
Tyršova	Tyršův	k2eAgInSc2d1	Tyršův
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Ohře	Ohře	k1gFnSc2	Ohře
je	být	k5eAaImIp3nS	být
povodí	povodí	k1gNnSc1	povodí
2	[number]	k4	2
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
povodí	povodí	k1gNnSc2	povodí
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
do	do	k7c2	do
Ohře	Ohře	k1gFnSc2	Ohře
přitéká	přitékat	k5eAaImIp3nS	přitékat
voda	voda	k1gFnSc1	voda
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jejích	její	k3xOp3gInPc2	její
přítoků	přítok	k1gInPc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Klínovce	Klínovec	k1gInSc2	Klínovec
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
1244	[number]	k4	1244
m.	m.	k?	m.
Dílčí	dílčí	k2eAgNnSc1d1	dílčí
povodí	povodí	k1gNnSc1	povodí
tvoří	tvořit	k5eAaImIp3nS	tvořit
povodí	povodí	k1gNnSc2	povodí
řek	řeka	k1gFnPc2	řeka
Odravy	Odrava	k1gFnSc2	Odrava
<g/>
,	,	kIx,	,
Blšanky	Blšanka	k1gFnSc2	Blšanka
<g/>
,	,	kIx,	,
Teplé	Teplá	k1gFnSc2	Teplá
<g/>
,	,	kIx,	,
Liboce	Liboc	k1gFnSc2	Liboc
<g/>
,	,	kIx,	,
Svatavy	Svatava	k1gFnSc2	Svatava
<g/>
,	,	kIx,	,
Chomutovky	Chomutovka	k1gFnSc2	Chomutovka
<g/>
,	,	kIx,	,
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
,	,	kIx,	,
Rolavy	Rolava	k1gFnPc1	Rolava
a	a	k8xC	a
Plesné	plesný	k2eAgFnPc1d1	Plesná
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
průtok	průtok	k1gInSc1	průtok
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
37,94	[number]	k4	37,94
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Celý	celý	k2eAgInSc1d1	celý
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
nejnižší	nízký	k2eAgFnPc4d3	nejnižší
hodnoty	hodnota	k1gFnPc4	hodnota
průměrných	průměrný	k2eAgInPc2d1	průměrný
ročních	roční	k2eAgInPc2d1	roční
srážkových	srážkový	k2eAgInPc2d1	srážkový
úhrnů	úhrn	k1gInPc2	úhrn
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
500	[number]	k4	500
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgInPc1d1	průměrný
měsíční	měsíční	k2eAgInPc1d1	měsíční
průtoky	průtok	k1gInPc1	průtok
Ohře	Ohře	k1gFnSc2	Ohře
v	v	k7c6	v
Lounech	Louny	k1gInPc6	Louny
<g/>
:	:	kIx,	:
Vybrané	vybraný	k2eAgInPc1d1	vybraný
hlásné	hlásný	k2eAgInPc1d1	hlásný
profily	profil	k1gInPc1	profil
<g/>
:	:	kIx,	:
Její	její	k3xOp3gFnSc1	její
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
a	a	k8xC	a
k	k	k7c3	k
zisku	zisk	k1gInSc3	zisk
vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
patří	patřit	k5eAaImIp3nS	patřit
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
,	,	kIx,	,
Loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
(	(	kIx(	(
<g/>
lázně	lázeň	k1gFnPc1	lázeň
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
Teplé	Teplá	k1gFnSc2	Teplá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Klášterec	Klášterec	k1gInSc1	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Kadaň	Kadaň	k1gFnSc1	Kadaň
<g/>
,	,	kIx,	,
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
Postoloprty	Postoloprt	k1gInPc1	Postoloprt
<g/>
,	,	kIx,	,
Louny	Louny	k1gInPc1	Louny
<g/>
,	,	kIx,	,
Libochovice	Libochovice	k1gFnPc1	Libochovice
<g/>
,	,	kIx,	,
Budyně	Budyně	k1gFnPc1	Budyně
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Terezín	Terezín	k1gInSc1	Terezín
a	a	k8xC	a
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgFnPc1	tři
přehradní	přehradní	k2eAgFnPc1d1	přehradní
nádrže	nádrž	k1gFnPc1	nádrž
<g/>
:	:	kIx,	:
Skalka	skalka	k1gFnSc1	skalka
-	-	kIx~	-
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
plochou	plocha	k1gFnSc7	plocha
378	[number]	k4	378
ha	ha	kA	ha
Nechranice	Nechranice	k1gFnSc2	Nechranice
-	-	kIx~	-
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
plochou	plocha	k1gFnSc7	plocha
1338	[number]	k4	1338
ha	ha	kA	ha
Kadaň	Kadaň	k1gFnSc1	Kadaň
-	-	kIx~	-
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
plochou	plocha	k1gFnSc7	plocha
67,2	[number]	k4	67,2
ha	ha	kA	ha
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
vodohospodářské	vodohospodářský	k2eAgFnSc2d1	vodohospodářská
soustavy	soustava	k1gFnSc2	soustava
Kadaň	Kadaň	k1gFnSc1	Kadaň
–	–	k?	–
Klášterec	Klášterec	k1gInSc1	Klášterec
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
areálů	areál	k1gInPc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
čerpacích	čerpací	k2eAgFnPc2d1	čerpací
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Napouštěcí	napouštěcí	k2eAgInSc1d1	napouštěcí
objekt	objekt	k1gInSc1	objekt
pro	pro	k7c4	pro
jezero	jezero	k1gNnSc4	jezero
Medard	Medard	k1gMnSc1	Medard
před	před	k7c7	před
Sokolovem	Sokolov	k1gInSc7	Sokolov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odebírá	odebírat	k5eAaImIp3nS	odebírat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
vodohospodářské	vodohospodářský	k2eAgFnPc4d1	vodohospodářská
rekultivace	rekultivace	k1gFnPc4	rekultivace
<g/>
.	.	kIx.	.
</s>
<s>
Čerpací	čerpací	k2eAgFnSc1d1	čerpací
stanice	stanice	k1gFnSc1	stanice
před	před	k7c7	před
městem	město	k1gNnSc7	město
Loket	loket	k1gInSc1	loket
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
technickou	technický	k2eAgFnSc4d1	technická
vodu	voda	k1gFnSc4	voda
pro	pro	k7c4	pro
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
areál	areál	k1gInSc4	areál
Vřesová	vřesový	k2eAgFnSc1d1	Vřesová
(	(	kIx(	(
<g/>
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
uhelná	uhelný	k2eAgFnSc1d1	uhelná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podkrušnohorský	podkrušnohorský	k2eAgInSc1d1	podkrušnohorský
přivaděč	přivaděč	k1gInSc1	přivaděč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
vodou	voda	k1gFnSc7	voda
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
řeku	řeka	k1gFnSc4	řeka
Bílinu	Bílina	k1gFnSc4	Bílina
na	na	k7c6	na
Mostecku	Mostecko	k1gNnSc6	Mostecko
odebírá	odebírat	k5eAaImIp3nS	odebírat
vodu	voda	k1gFnSc4	voda
nad	nad	k7c7	nad
Kadaní	Kadaň	k1gFnSc7	Kadaň
pod	pod	k7c7	pod
Kláštercem	Klášterec	k1gInSc7	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
.	.	kIx.	.
elektrárna	elektrárna	k1gFnSc1	elektrárna
Počerady	Počerada	k1gFnSc2	Počerada
<g/>
,	,	kIx,	,
elektrárny	elektrárna	k1gFnSc2	elektrárna
Prunéřov	Prunéřovo	k1gNnPc2	Prunéřovo
<g/>
,	,	kIx,	,
elektrárna	elektrárna	k1gFnSc1	elektrárna
Tušimice	Tušimika	k1gFnSc3	Tušimika
II	II	kA	II
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
také	také	k9	také
elektrárna	elektrárna	k1gFnSc1	elektrárna
Tušimice	Tušimika	k1gFnSc3	Tušimika
I	I	kA	I
<g/>
,	,	kIx,	,
odebírají	odebírat	k5eAaImIp3nP	odebírat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k9	jako
technická	technický	k2eAgFnSc1d1	technická
voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
chlazení	chlazení	k1gNnSc4	chlazení
<g/>
.	.	kIx.	.
</s>
<s>
Čerpací	čerpací	k2eAgInSc1d1	čerpací
objekt	objekt	k1gInSc1	objekt
pod	pod	k7c7	pod
Nechranickou	nechranický	k2eAgFnSc7d1	Nechranická
přehradou	přehrada	k1gFnSc7	přehrada
odebírá	odebírat	k5eAaImIp3nS	odebírat
vodu	voda	k1gFnSc4	voda
pro	pro	k7c4	pro
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
vodovod	vodovod	k1gInSc4	vodovod
Nechranice	Nechranice	k1gFnSc2	Nechranice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
napouštělo	napouštět	k5eAaImAgNnS	napouštět
i	i	k9	i
Mostecké	mostecký	k2eAgNnSc1d1	Mostecké
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Ohře	Ohře	k1gFnSc2	Ohře
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
vodáky	vodák	k1gMnPc4	vodák
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
stálý	stálý	k2eAgInSc4d1	stálý
tok	tok	k1gInSc4	tok
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
vodácký	vodácký	k2eAgInSc4d1	vodácký
terén	terén	k1gInSc4	terén
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
klidnějších	klidný	k2eAgInPc2d2	klidnější
i	i	k8xC	i
náročnějších	náročný	k2eAgInPc2d2	náročnější
úseků	úsek	k1gInPc2	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
splavná	splavný	k2eAgFnSc1d1	splavná
téměř	téměř	k6eAd1	téměř
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sjízdnost	sjízdnost	k1gFnSc4	sjízdnost
horního	horní	k2eAgInSc2d1	horní
a	a	k8xC	a
středního	střední	k2eAgInSc2d1	střední
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
vodočet	vodočet	k1gInSc1	vodočet
v	v	k7c6	v
Drahovicích	Drahovice	k1gFnPc6	Drahovice
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
u	u	k7c2	u
lávky	lávka	k1gFnSc2	lávka
pro	pro	k7c4	pro
pěší	pěší	k2eAgInSc4d1	pěší
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgInSc4d1	bývalý
silniční	silniční	k2eAgInSc4d1	silniční
most	most	k1gInSc4	most
Patrice	patrice	k1gFnSc2	patrice
Lumumby	Lumumba	k1gFnSc2	Lumumba
<g/>
)	)	kIx)	)
na	na	k7c4	na
ř.	ř.	k?	ř.
km	km	kA	km
174,5	[number]	k4	174,5
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
minimální	minimální	k2eAgMnSc1d1	minimální
se	se	k3xPyFc4	se
uváděl	uvádět	k5eAaImAgMnS	uvádět
stav	stav	k1gInSc4	stav
vody	voda	k1gFnSc2	voda
60	[number]	k4	60
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zkušeností	zkušenost	k1gFnPc2	zkušenost
vodáků	vodák	k1gMnPc2	vodák
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
sjízdná	sjízdný	k2eAgFnSc1d1	sjízdná
i	i	k9	i
za	za	k7c2	za
nižšího	nízký	k2eAgInSc2d2	nižší
stavu	stav	k1gInSc2	stav
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
připomínek	připomínka	k1gFnPc2	připomínka
doporučený	doporučený	k2eAgInSc1d1	doporučený
minimální	minimální	k2eAgInSc1d1	minimální
stav	stav	k1gInSc1	stav
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
45	[number]	k4	45
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zdatní	zdatný	k2eAgMnPc1d1	zdatný
vodáci	vodák	k1gMnPc1	vodák
dokáží	dokázat	k5eAaPmIp3nP	dokázat
úsek	úsek	k1gInSc4	úsek
z	z	k7c2	z
Chebu	Cheb	k1gInSc2	Cheb
do	do	k7c2	do
Klášterce	Klášterec	k1gInSc2	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
117	[number]	k4	117
km	km	kA	km
<g/>
,	,	kIx,	,
sjet	sjet	k5eAaPmF	sjet
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
až	až	k9	až
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
sjíždění	sjíždění	k1gNnSc2	sjíždění
řeky	řeka	k1gFnSc2	řeka
není	být	k5eNaImIp3nS	být
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vodáků	vodák	k1gMnPc2	vodák
sjíždí	sjíždět	k5eAaImIp3nS	sjíždět
řeku	řeka	k1gFnSc4	řeka
kvůli	kvůli	k7c3	kvůli
zážitkům	zážitek	k1gInPc3	zážitek
z	z	k7c2	z
putování	putování	k1gNnSc2	putování
po	po	k7c6	po
atraktivním	atraktivní	k2eAgNnSc6d1	atraktivní
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Plavbu	plavba	k1gFnSc4	plavba
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
zahajují	zahajovat	k5eAaImIp3nP	zahajovat
vodáci	vodák	k1gMnPc1	vodák
většinou	většina	k1gFnSc7	většina
pod	pod	k7c7	pod
Chebem	Cheb	k1gInSc7	Cheb
v	v	k7c6	v
Tršnicích	Tršnice	k1gFnPc6	Tršnice
<g/>
,	,	kIx,	,
Kynšperku	Kynšperk	k1gInSc6	Kynšperk
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Sokolově	Sokolov	k1gInSc6	Sokolov
nebo	nebo	k8xC	nebo
Lokti	loket	k1gInSc6	loket
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
cílem	cíl	k1gInSc7	cíl
sjíždění	sjíždění	k1gNnSc2	sjíždění
řeky	řeka	k1gFnSc2	řeka
jsou	být	k5eAaImIp3nP	být
Klášterec	Klášterec	k1gInSc4	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
,	,	kIx,	,
Vojkovice	Vojkovice	k1gFnSc1	Vojkovice
nebo	nebo	k8xC	nebo
Kadaň	Kadaň	k1gFnSc1	Kadaň
<g/>
.	.	kIx.	.
</s>
<s>
Vodácky	vodácky	k6eAd1	vodácky
nejvyhledávanější	vyhledávaný	k2eAgInSc1d3	nejvyhledávanější
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc1	úsek
Loket	loket	k1gInSc1	loket
-	-	kIx~	-
Vojkovice	Vojkovice	k1gFnSc1	Vojkovice
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
vodácky	vodácky	k6eAd1	vodácky
a	a	k8xC	a
turisticky	turisticky	k6eAd1	turisticky
atraktivních	atraktivní	k2eAgNnPc2d1	atraktivní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
Svatošské	Svatošský	k2eAgFnPc1d1	Svatošská
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
peřej	peřej	k1gFnSc1	peřej
Hubertus	hubertus	k1gInSc1	hubertus
<g/>
,	,	kIx,	,
Kyselka	kyselka	k1gFnSc1	kyselka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
jezem	jez	k1gInSc7	jez
Radošov	Radošov	k1gInSc1	Radošov
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
vodáckých	vodácký	k2eAgNnPc2d1	vodácké
tábořišť	tábořiště	k1gNnPc2	tábořiště
či	či	k8xC	či
kempů	kemp	k1gInPc2	kemp
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
tábořiště	tábořiště	k1gNnSc4	tábořiště
nebo	nebo	k8xC	nebo
kempy	kemp	k1gInPc4	kemp
–	–	k?	–
Tršnice	Tršnice	k1gFnSc2	Tršnice
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
235,2	[number]	k4	235,2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chocovice	Chocovice	k1gFnSc1	Chocovice
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
234,3	[number]	k4	234,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kynšperk	Kynšperk	k1gInSc1	Kynšperk
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
218,5	[number]	k4	218,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šabina	Šabina	k1gFnSc1	Šabina
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
211,1	[number]	k4	211,1
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Sokolov	Sokolov	k1gInSc1	Sokolov
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
203,1	[number]	k4	203,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Královské	královský	k2eAgNnSc1d1	královské
Poříčí	Poříčí	k1gNnSc1	Poříčí
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
199,3	[number]	k4	199,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Loket	loket	k1gInSc1	loket
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
190,5	[number]	k4	190,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Doubí	doubí	k1gNnSc1	doubí
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
180,8	[number]	k4	180,8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hubertus	hubertus	k1gInSc1	hubertus
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
169,6	[number]	k4	169,6
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Dubina	dubina	k1gFnSc1	dubina
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
163,3	[number]	k4	163,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kyselka	kyselka	k1gFnSc1	kyselka
–	–	k?	–
Radošov	Radošov	k1gInSc1	Radošov
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
159,1	[number]	k4	159,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vojkovice	Vojkovice	k1gFnSc1	Vojkovice
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
151,3	[number]	k4	151,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jakubov	Jakubov	k1gInSc1	Jakubov
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
150,0	[number]	k4	150,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boč	bočit	k5eAaImRp2nS	bočit
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
142,5	[number]	k4	142,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miřetice	Miřetika	k1gFnSc6	Miřetika
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
131,6	[number]	k4	131,6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Klášterec	Klášterec	k1gInSc1	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
132,6	[number]	k4	132,6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vikletice	Vikletika	k1gFnSc6	Vikletika
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
106,0	[number]	k4	106,0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kolem	kolem	k7c2	kolem
vodáckého	vodácký	k2eAgInSc2d1	vodácký
úseku	úsek	k1gInSc2	úsek
řeky	řeka	k1gFnSc2	řeka
od	od	k7c2	od
Chebu	Cheb	k1gInSc2	Cheb
po	po	k7c4	po
Želinský	Želinský	k2eAgInSc4d1	Želinský
jez	jez	k1gInSc4	jez
u	u	k7c2	u
Kadaně	Kadaň	k1gFnSc2	Kadaň
Vodácká	vodácký	k2eAgFnSc1d1	vodácká
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
70	[number]	k4	70
informačních	informační	k2eAgFnPc2d1	informační
tabulí	tabule	k1gFnPc2	tabule
v	v	k7c6	v
českém	český	k2eAgMnSc6d1	český
<g/>
,	,	kIx,	,
německém	německý	k2eAgInSc6d1	německý
a	a	k8xC	a
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
tabuli	tabule	k1gFnSc6	tabule
je	být	k5eAaImIp3nS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
říční	říční	k2eAgInSc1d1	říční
kilometr	kilometr	k1gInSc1	kilometr
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Tabule	tabule	k1gFnPc1	tabule
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
informace	informace	k1gFnPc4	informace
pro	pro	k7c4	pro
vodáky	vodák	k1gMnPc4	vodák
<g/>
,	,	kIx,	,
možnostech	možnost	k1gFnPc6	možnost
ubytování	ubytování	k1gNnSc2	ubytování
i	i	k8xC	i
stručný	stručný	k2eAgInSc4d1	stručný
popis	popis	k1gInSc4	popis
přírodních	přírodní	k2eAgFnPc2d1	přírodní
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
poblíž	poblíž	k7c2	poblíž
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Tabule	tabule	k1gFnPc1	tabule
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
výřez	výřez	k1gInSc4	výřez
mapy	mapa	k1gFnSc2	mapa
příslušného	příslušný	k2eAgInSc2d1	příslušný
úseku	úsek	k1gInSc2	úsek
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Vodácká	vodácký	k2eAgFnSc1d1	vodácká
stezka	stezka	k1gFnSc1	stezka
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
,	,	kIx,	,
možná	možná	k9	možná
ani	ani	k8xC	ani
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Ohři	Ohře	k1gFnSc4	Ohře
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
mnoho	mnoho	k4c1	mnoho
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgFnP	chránit
jako	jako	k9	jako
nemovité	movitý	k2eNgFnPc1d1	nemovitá
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osadě	osada	k1gFnSc6	osada
Hendelhammer	Hendelhammra	k1gFnPc2	Hendelhammra
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnPc1d1	místní
části	část	k1gFnPc1	část
obce	obec	k1gFnSc2	obec
Thierstein	Thiersteina	k1gFnPc2	Thiersteina
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Frankách	Franky	k1gInPc6	Franky
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
historický	historický	k2eAgInSc1d1	historický
tříobloukový	tříobloukový	k2eAgInSc1d1	tříobloukový
kamenný	kamenný	k2eAgInSc1d1	kamenný
<g/>
,	,	kIx,	,
památkově	památkově	k6eAd1	památkově
chráněný	chráněný	k2eAgInSc4d1	chráněný
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kámen	kámen	k1gInSc1	kámen
s	s	k7c7	s
erbem	erb	k1gInSc7	erb
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
1763	[number]	k4	1763
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
je	být	k5eAaImIp3nS	být
Ohře	Ohře	k1gFnSc1	Ohře
u	u	k7c2	u
Písečné	písečný	k2eAgFnSc2d1	písečná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
bývalého	bývalý	k2eAgNnSc2d1	bývalé
městského	městský	k2eAgNnSc2d1	Městské
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
,	,	kIx,	,
přemostěna	přemostěn	k2eAgFnSc1d1	přemostěna
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
krytou	krytý	k2eAgFnSc7d1	krytá
lávkou	lávka	k1gFnSc7	lávka
s	s	k7c7	s
okenními	okenní	k2eAgInPc7d1	okenní
průzory	průzor	k1gInPc7	průzor
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
lávka	lávka	k1gFnSc1	lávka
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
koncem	koncem	k7c2	koncem
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
obnovena	obnoven	k2eAgFnSc1d1	obnovena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vybavena	vybavit	k5eAaPmNgFnS	vybavit
ocelovou	ocelový	k2eAgFnSc4d1	ocelová
konstrukci	konstrukce	k1gFnSc4	konstrukce
s	s	k7c7	s
osmi	osm	k4xCc7	osm
trojúhelníkovými	trojúhelníkový	k2eAgInPc7d1	trojúhelníkový
závěsy	závěs	k1gInPc7	závěs
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
hřebene	hřeben	k1gInSc2	hřeben
střechy	střecha	k1gFnSc2	střecha
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
Ohře	Ohře	k1gFnSc2	Ohře
je	být	k5eAaImIp3nS	být
5,5	[number]	k4	5,5
m.	m.	k?	m.
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
Chebu	Cheb	k1gInSc2	Cheb
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
z	z	k7c2	z
Chebu	Cheb	k1gInSc2	Cheb
do	do	k7c2	do
Františkových	Františkův	k2eAgFnPc2d1	Františkova
Lázní	lázeň	k1gFnPc2	lázeň
postaven	postavit	k5eAaPmNgInS	postavit
z	z	k7c2	z
opracovaných	opracovaný	k2eAgInPc2d1	opracovaný
žulových	žulový	k2eAgInPc2d1	žulový
kvádrů	kvádr	k1gInPc2	kvádr
348	[number]	k4	348
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
viadukt	viadukt	k1gInSc4	viadukt
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
Ohře	Ohře	k1gFnSc2	Ohře
činí	činit	k5eAaImIp3nS	činit
25	[number]	k4	25
m.	m.	k?	m.
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
americkém	americký	k2eAgInSc6d1	americký
náletu	nálet	k1gInSc6	nálet
většina	většina	k1gFnSc1	většina
oblouků	oblouk	k1gInPc2	oblouk
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
zcela	zcela	k6eAd1	zcela
zničena	zničit	k5eAaPmNgNnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
poškozené	poškozený	k2eAgFnPc1d1	poškozená
části	část	k1gFnPc1	část
dostavěny	dostavět	k5eAaPmNgFnP	dostavět
a	a	k8xC	a
opraveny	opravit	k5eAaPmNgFnP	opravit
betonem	beton	k1gInSc7	beton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kynšperku	Kynšperk	k1gInSc6	Kynšperk
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
přemostěna	přemostěn	k2eAgFnSc1d1	přemostěna
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
lávkou	lávka	k1gFnSc7	lávka
přes	přes	k7c4	přes
Ohři	Ohře	k1gFnSc4	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Nahradila	nahradit	k5eAaPmAgFnS	nahradit
původní	původní	k2eAgFnSc1d1	původní
130	[number]	k4	130
m	m	kA	m
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
železobetonovou	železobetonový	k2eAgFnSc4d1	železobetonová
lávku	lávka	k1gFnSc4	lávka
<g/>
,	,	kIx,	,
zničenou	zničený	k2eAgFnSc4d1	zničená
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
ustupující	ustupující	k2eAgFnSc7d1	ustupující
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Provizorní	provizorní	k2eAgFnSc4d1	provizorní
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
lávku	lávka	k1gFnSc4	lávka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
zničila	zničit	k5eAaPmAgFnS	zničit
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
krytá	krytý	k2eAgFnSc1d1	krytá
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
lávka	lávka	k1gFnSc1	lávka
byla	být	k5eAaImAgFnS	být
obnovena	obnovit	k5eAaPmNgFnS	obnovit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
až	až	k9	až
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
současná	současný	k2eAgFnSc1d1	současná
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
63	[number]	k4	63
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Sokolovem	Sokolov	k1gInSc7	Sokolov
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
až	až	k9	až
1975	[number]	k4	1975
vybudován	vybudovat	k5eAaPmNgMnS	vybudovat
na	na	k7c6	na
rychlostní	rychlostní	k2eAgFnSc6d1	rychlostní
silnici	silnice	k1gFnSc6	silnice
R6	R6	k1gFnSc2	R6
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
po	po	k7c4	po
rozšíření	rozšíření	k1gNnSc4	rozšíření
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
jízdní	jízdní	k2eAgInPc4d1	jízdní
pruhy	pruh	k1gInPc4	pruh
znova	znova	k6eAd1	znova
uvedený	uvedený	k2eAgInSc1d1	uvedený
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lokti	loket	k1gInSc6	loket
přemosťuje	přemosťovat	k5eAaImIp3nS	přemosťovat
Ohři	Ohře	k1gFnSc4	Ohře
železobetonový	železobetonový	k2eAgInSc4d1	železobetonový
most	most	k1gInSc4	most
vybudovaný	vybudovaný	k2eAgInSc4d1	vybudovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1934	[number]	k4	1934
až	až	k9	až
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
nahradil	nahradit	k5eAaPmAgInS	nahradit
historický	historický	k2eAgInSc1d1	historický
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
původního	původní	k2eAgInSc2d1	původní
mostu	most	k1gInSc2	most
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
Centrum	centrum	k1gNnSc1	centrum
řeky	řeka	k1gFnSc2	řeka
Ohře	Ohře	k1gFnSc2	Ohře
na	na	k7c6	na
statku	statek	k1gInSc6	statek
Bernard	Bernard	k1gMnSc1	Bernard
v	v	k7c6	v
Královském	královský	k2eAgNnSc6d1	královské
Poříčí	Poříčí	k1gNnSc6	Poříčí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
překonává	překonávat	k5eAaImIp3nS	překonávat
Ohři	Ohře	k1gFnSc4	Ohře
pětiobloukový	pětiobloukový	k2eAgInSc4d1	pětiobloukový
kamenný	kamenný	k2eAgInSc4d1	kamenný
Chebský	chebský	k2eAgInSc4d1	chebský
most	most	k1gInSc4	most
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
nechal	nechat	k5eAaPmAgMnS	nechat
Heinrich	Heinrich	k1gMnSc1	Heinrich
Mattoni	Mattoň	k1gFnSc3	Mattoň
postavit	postavit	k5eAaPmF	postavit
v	v	k7c6	v
Kyselce	kyselka	k1gFnSc6	kyselka
ocelový	ocelový	k2eAgInSc1d1	ocelový
příhradový	příhradový	k2eAgInSc1d1	příhradový
Kyselský	Kyselský	k2eAgInSc1d1	Kyselský
železniční	železniční	k2eAgInSc1d1	železniční
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
částečně	částečně	k6eAd1	částečně
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
silničně	silničně	k6eAd1	silničně
železniční	železniční	k2eAgInSc4d1	železniční
most	most	k1gInSc4	most
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
na	na	k7c4	na
silniční	silniční	k2eAgFnSc4d1	silniční
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc4d1	doplněný
chodníky	chodník	k1gInPc4	chodník
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
Pohnutou	pohnutý	k2eAgFnSc4d1	pohnutá
historii	historie	k1gFnSc4	historie
má	mít	k5eAaImIp3nS	mít
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
most	most	k1gInSc1	most
v	v	k7c6	v
Radošově	Radošův	k2eAgInSc6d1	Radošův
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
krytého	krytý	k2eAgInSc2d1	krytý
mostu	most	k1gInSc2	most
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1364	[number]	k4	1364
na	na	k7c6	na
původních	původní	k2eAgInPc6d1	původní
pilířích	pilíř	k1gInPc6	pilíř
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
stoleletí	stoleletí	k1gNnSc6	stoleletí
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
několikrát	několikrát	k6eAd1	několikrát
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
vždy	vždy	k6eAd1	vždy
obnoven	obnoven	k2eAgInSc1d1	obnoven
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
oprava	oprava	k1gFnSc1	oprava
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
požár	požár	k1gInSc1	požár
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
však	však	k9	však
opětovně	opětovně	k6eAd1	opětovně
zničil	zničit	k5eAaPmAgInS	zničit
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jen	jen	k9	jen
zbytky	zbytek	k1gInPc4	zbytek
kamenných	kamenný	k2eAgInPc2d1	kamenný
pilířů	pilíř	k1gInPc2	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
dokončena	dokončen	k2eAgFnSc1d1	dokončena
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
mostu	most	k1gInSc2	most
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
historické	historický	k2eAgFnSc2d1	historická
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
17.10	[number]	k4	17.10
<g/>
.2003	.2003	k4	.2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Klášterci	Klášterec	k1gInSc6	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
–	–	k?	–
<g/>
Cheb	Cheb	k1gInSc1	Cheb
překonává	překonávat	k5eAaImIp3nS	překonávat
údolí	údolí	k1gNnPc4	údolí
řeky	řeka	k1gFnSc2	řeka
Ohře	Ohře	k1gFnSc2	Ohře
ocelový	ocelový	k2eAgInSc4d1	ocelový
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
rekonstruován	rekonstruovat	k5eAaBmNgMnS	rekonstruovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
až	až	k9	až
2005	[number]	k4	2005
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stavby	stavba	k1gFnSc2	stavba
Elektrizace	elektrizace	k1gFnSc2	elektrizace
trati	trať	k1gFnSc2	trať
Kadaň	Kadaň	k1gFnSc1	Kadaň
–	–	k?	–
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Žatci	Žatec	k1gInSc6	Žatec
překračoval	překračovat	k5eAaImAgMnS	překračovat
Ohři	Ohře	k1gFnSc4	Ohře
historický	historický	k2eAgInSc4d1	historický
řetězový	řetězový	k2eAgInSc4d1	řetězový
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
uveden	uveden	k2eAgInSc1d1	uveden
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
64	[number]	k4	64
metrů	metr	k1gInPc2	metr
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gInSc4	on
ocelový	ocelový	k2eAgInSc4d1	ocelový
obloukový	obloukový	k2eAgInSc4d1	obloukový
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
D8	D8	k1gFnPc2	D8
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1995	[number]	k4	1995
zahájena	zahájen	k2eAgFnSc1d1	zahájena
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1999	[number]	k4	1999
dokončena	dokončen	k2eAgFnSc1d1	dokončena
stavba	stavba	k1gFnSc1	stavba
1	[number]	k4	1
183	[number]	k4	183
m	m	kA	m
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
mostu	most	k1gInSc2	most
u	u	k7c2	u
Doksan	Doksana	k1gFnPc2	Doksana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
estakáda	estakáda	k1gFnSc1	estakáda
přes	přes	k7c4	přes
celé	celý	k2eAgNnSc4d1	celé
inundační	inundační	k2eAgNnSc4d1	inundační
území	území	k1gNnSc4	území
řeky	řeka	k1gFnSc2	řeka
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
mostem	most	k1gInSc7	most
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
českým	český	k2eAgInSc7d1	český
dálničním	dálniční	k2eAgInSc7d1	dálniční
mostem	most	k1gInSc7	most
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
souvisí	souviset	k5eAaImIp3nS	souviset
dvě	dva	k4xCgFnPc4	dva
evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnPc4d1	významná
lokality	lokalita	k1gFnPc4	lokalita
zahrnuté	zahrnutý	k2eAgFnPc4d1	zahrnutá
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
–	–	k?	–
Ramena	rameno	k1gNnSc2	rameno
Ohře	Ohře	k1gFnSc2	Ohře
v	v	k7c6	v
chebsko-sokolovském	chebskookolovský	k2eAgInSc6d1	chebsko-sokolovský
bioregionu	bioregion	k1gInSc6	bioregion
a	a	k8xC	a
Kaňon	kaňon	k1gInSc1	kaňon
Ohře	Ohře	k1gFnSc2	Ohře
mezi	mezi	k7c7	mezi
Loktem	loket	k1gInSc7	loket
a	a	k8xC	a
Tašovicemi	Tašovice	k1gFnPc7	Tašovice
a	a	k8xC	a
Doubím	doubí	k1gNnSc7	doubí
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Údolí	údolí	k1gNnSc2	údolí
Ohře	Ohře	k1gFnSc2	Ohře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Staré	Stará	k1gFnSc2	Stará
Sedlo	sednout	k5eAaPmAgNnS	sednout
na	na	k7c6	na
Sokolovsku	Sokolovsko	k1gNnSc6	Sokolovsko
<g/>
.	.	kIx.	.
</s>
