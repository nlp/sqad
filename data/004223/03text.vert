<s>
Poupata	poupě	k1gNnPc1	poupě
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režijní	režijní	k2eAgInSc1d1	režijní
debut	debut	k1gInSc1	debut
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jiráského	Jiráský	k1gMnSc2	Jiráský
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Jardovi	Jarda	k1gMnSc6	Jarda
Hrdinovi	Hrdina	k1gMnSc6	Hrdina
<g/>
,	,	kIx,	,
nádražním	nádražní	k2eAgMnSc6d1	nádražní
hradlaři	hradlař	k1gMnSc6	hradlař
závislém	závislý	k2eAgMnSc6d1	závislý
na	na	k7c6	na
hracích	hrací	k2eAgInPc6d1	hrací
automatech	automat	k1gInPc6	automat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
2011	[number]	k4	2011
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
roku	rok	k1gInSc2	rok
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
tři	tři	k4xCgNnPc1	tři
ocenění	ocenění	k1gNnSc1	ocenění
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Javorského	Javorský	k1gMnSc4	Javorský
<g/>
.	.	kIx.	.
</s>
<s>
Nominován	nominovat	k5eAaBmNgInS	nominovat
byl	být	k5eAaImAgInS	být
také	také	k9	také
za	za	k7c4	za
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Malgorzatu	Malgorzata	k1gFnSc4	Malgorzata
Pikus	Pikus	k1gInSc1	Pikus
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
na	na	k7c6	na
Českých	český	k2eAgInPc6d1	český
lvech	lev	k1gInPc6	lev
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
k	k	k7c3	k
8	[number]	k4	8
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
2012	[number]	k4	2012
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
získal	získat	k5eAaPmAgInS	získat
před	před	k7c7	před
realizací	realizace	k1gFnSc7	realizace
filmu	film	k1gInSc2	film
Cenu	cena	k1gFnSc4	cena
nadace	nadace	k1gFnSc2	nadace
RWE	RWE	kA	RWE
a	a	k8xC	a
Studia	studio	k1gNnSc2	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
na	na	k7c6	na
Cenách	cena	k1gFnPc6	cena
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
2011	[number]	k4	2011
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Javorského	Javorský	k1gMnSc4	Javorský
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
získal	získat	k5eAaPmAgMnS	získat
nominace	nominace	k1gFnSc2	nominace
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
původní	původní	k2eAgFnSc1d1	původní
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Malgorzatu	Malgorzata	k1gFnSc4	Malgorzata
Pikus	Pikus	k1gMnSc1	Pikus
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Jiráský	Jiráský	k1gMnSc1	Jiráský
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
RWE	RWE	kA	RWE
pro	pro	k7c4	pro
objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cenách	cena	k1gFnPc6	cena
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
2011	[number]	k4	2011
získal	získat	k5eAaPmAgMnS	získat
ceny	cena	k1gFnPc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Javorského	Javorský	k1gMnSc4	Javorský
<g/>
.	.	kIx.	.
</s>
<s>
Nominován	nominován	k2eAgMnSc1d1	nominován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Malgorzatu	Malgorzata	k1gFnSc4	Malgorzata
Pikus	Pikus	k1gInSc1	Pikus
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
řešení	řešení	k1gNnSc1	řešení
<g/>
,	,	kIx,	,
nominován	nominovat	k5eAaBmNgMnS	nominovat
byl	být	k5eAaImAgMnS	být
také	také	k9	také
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
plakát	plakát	k1gInSc4	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
začínající	začínající	k2eAgMnPc4d1	začínající
tvůrce	tvůrce	k1gMnPc4	tvůrce
New	New	k1gMnSc1	New
Director	Director	k1gMnSc1	Director
Competition	Competition	k1gInSc4	Competition
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
Stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
cenu	cena	k1gFnSc4	cena
z	z	k7c2	z
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
jihokorejském	jihokorejský	k2eAgInSc6d1	jihokorejský
Pusanu	Pusan	k1gInSc6	Pusan
<g/>
.	.	kIx.	.
</s>
<s>
Kamil	Kamil	k1gMnSc1	Kamil
Fila	Fila	k1gMnSc1	Fila
<g/>
,	,	kIx,	,
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Mirka	Mirka	k1gFnSc1	Mirka
Spáčilová	Spáčilová	k1gFnSc1	Spáčilová
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Marcel	Marcel	k1gMnSc1	Marcel
Kabát	Kabát	k1gMnSc1	Kabát
<g/>
,	,	kIx,	,
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Hubert	Hubert	k1gMnSc1	Hubert
Poul	Poul	k1gMnSc1	Poul
<g/>
,	,	kIx,	,
Moviezone	Moviezon	k1gMnSc5	Moviezon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
Jan	Jan	k1gMnSc1	Jan
Jaroš	Jaroš	k1gMnSc1	Jaroš
<g/>
,	,	kIx,	,
Filmserver	Filmserver	k1gMnSc1	Filmserver
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Jan	Jan	k1gMnSc1	Jan
Gregor	Gregor	k1gMnSc1	Gregor
<g/>
,	,	kIx,	,
Respekt	respekt	k1gInSc1	respekt
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
František	František	k1gMnSc1	František
Fuka	Fuka	k1gMnSc1	Fuka
<g/>
,	,	kIx,	,
FFFilm	FFFilm	k1gMnSc1	FFFilm
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
Poupata	poupě	k1gNnPc1	poupě
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Poupata	poupě	k1gNnPc1	poupě
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
