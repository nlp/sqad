<s>
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
</s>
<s>
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Arabsko-izraelský	arabsko-izraelský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
</s>
<s>
ilustrační	ilustrační	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Počátek	počátek	k1gInSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
–	–	k?
současnost	současnost	k1gFnSc1
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
palestinská	palestinský	k2eAgNnPc4d1
území	území	k1gNnPc4
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Probíhající	probíhající	k2eAgFnPc1d1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Palestinci	Palestinec	k1gMnPc1
</s>
<s>
Izraelci	Izraelec	k1gMnPc1
</s>
<s>
Mírový	mírový	k2eAgInSc1d1
proces	proces	k1gInSc1
</s>
<s>
Camp	camp	k1gInSc1
Davidu	David	k1gMnSc3
·	·	k?
Madridská	madridský	k2eAgFnSc1d1
konferenceMírová	konferenceMírový	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
z	z	k7c2
Osla	Oslo	k1gNnSc2
/	/	kIx~
Osla	osel	k1gMnSc2
II	II	kA
·	·	k?
Hebronský	hebronský	k2eAgInSc4d1
protokolWye	protokolWye	k1gInSc4
River	River	k1gInSc1
/	/	kIx~
Memorandum	memorandum	k1gNnSc1
z	z	k7c2
Šarm	šarm	k1gInSc4
aš-ŠajchuSummit	aš-ŠajchuSummita	k1gFnPc2
v	v	k7c4
Camp	camp	k1gInSc4
Davidu	David	k1gMnSc3
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
·	·	k?
Summit	summit	k1gInSc1
v	v	k7c4
TaběSilniční	TaběSilniční	k2eAgFnSc4d1
mapa	mapa	k1gFnSc1
·	·	k?
Konference	konference	k1gFnSc1
v	v	k7c6
Annapolisu	Annapolis	k1gInSc6
</s>
<s>
Primární	primární	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
Obavy	obava	k1gFnPc1
</s>
<s>
Izraelské	izraelský	k2eAgInPc1d1
osadyIzraelská	osadyIzraelský	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
·	·	k?
židovský	židovský	k2eAgInSc1d1
státPalestinské	státPalestinský	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
násilíPalestinští	násilíPalestinský	k2eAgMnPc1d1
uprchlíci	uprchlík	k1gMnPc1
·	·	k?
Palestinského	palestinský	k2eAgInSc2d1
státu	stát	k1gInSc2
Status	status	k1gInSc1
Jeruzaléma	Jeruzalém	k1gInSc2
</s>
<s>
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
součást	součást	k1gFnSc1
arabsko-izraelského	arabsko-izraelský	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejproblematičtějších	problematický	k2eAgFnPc2d3
událostí	událost	k1gFnSc7
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
počátek	počátek	k1gInSc1
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
ke	k	k7c3
konci	konec	k1gInSc3
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
většímu	veliký	k2eAgInSc3d2
osidlování	osidlování	k1gNnPc1
Země	zem	k1gFnSc2
izraelské	izraelský	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
označovaná	označovaný	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gNnSc4
klíčové	klíčový	k2eAgFnPc1d1
sporné	sporný	k2eAgFnPc1d1
otázky	otázka	k1gFnPc1
patří	patřit	k5eAaImIp3nP
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
palestinští	palestinský	k2eAgMnPc1d1
uprchlíci	uprchlík	k1gMnPc1
<g/>
,	,	kIx,
izraelské	izraelský	k2eAgFnSc2d1
osady	osada	k1gFnSc2
<g/>
,	,	kIx,
bezpečnost	bezpečnost	k1gFnSc4
<g/>
,	,	kIx,
hranice	hranice	k1gFnPc4
a	a	k8xC
mezinárodní	mezinárodní	k2eAgInSc4d1
status	status	k1gInSc4
a	a	k8xC
rozdělení	rozdělení	k1gNnSc4
vodních	vodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Konec	konec	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
–	–	k?
rok	rok	k1gInSc4
1920	#num#	k4
<g/>
:	:	kIx,
Počátky	počátek	k1gInPc4
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Sféry	sféra	k1gFnPc4
vlivu	vliv	k1gInSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
červeně	červeně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Francie	Francie	k1gFnSc2
(	(	kIx(
<g/>
modře	modř	k1gFnSc2
<g/>
)	)	kIx)
ustavené	ustavený	k2eAgFnSc2d1
Sykesova	Sykesův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Picotova	Picotův	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Sionismus	sionismus	k1gInSc1
a	a	k8xC
Balfourova	Balfourův	k2eAgFnSc1d1
deklarace	deklarace	k1gFnSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
přílivu	příliv	k1gInSc3
obyvatel	obyvatel	k1gMnPc2
docházelo	docházet	k5eAaImAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
alijí	alijí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nově	nově	k6eAd1
příchozí	příchozí	k1gMnPc1
byli	být	k5eAaImAgMnP
k	k	k7c3
příchodu	příchod	k1gInSc3
různě	různě	k6eAd1
motivováni	motivovat	k5eAaBmNgMnP
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
důvody	důvod	k1gInPc4
náboženské	náboženský	k2eAgInPc4d1
<g/>
,	,	kIx,
či	či	k8xC
politické	politický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
olim	olima	k1gFnPc2
(	(	kIx(
<g/>
imigrantů	imigrant	k1gMnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
alijí	alije	k1gFnPc2
<g/>
)	)	kIx)
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
protižidovských	protižidovský	k2eAgInPc2d1
pogromů	pogrom	k1gInPc2
v	v	k7c6
carském	carský	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
a	a	k8xC
později	pozdě	k6eAd2
kvůli	kvůli	k7c3
vzestupu	vzestup	k1gInSc3
národního	národní	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
židovského	židovský	k2eAgNnSc2d1
přistěhovalectví	přistěhovalectví	k1gNnSc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
arabským	arabský	k2eAgInPc3d1
nepokojům	nepokoj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Plán	plán	k1gInSc1
OSN	OSN	kA
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
</s>
<s>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
Britský	britský	k2eAgInSc1d1
mandát	mandát	k1gInSc1
Palestina	Palestina	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Britský	britský	k2eAgInSc4d1
mandát	mandát	k1gInSc4
Palestina	Palestina	k1gFnSc1
a	a	k8xC
Deklarace	deklarace	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1948	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
plánu	plán	k1gInSc2
OSN	OSN	kA
vyhlásil	vyhlásit	k5eAaPmAgInS
Izrael	Izrael	k1gInSc1
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
vydal	vydat	k5eAaPmAgMnS
britský	britský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Arthur	Arthur	k1gMnSc1
Balfour	Balfour	k1gMnSc1
deklaraci	deklarace	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
sděluje	sdělovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
vláda	vláda	k1gFnSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
Veličenstva	veličenstvo	k1gNnSc2
pohlíží	pohlížet	k5eAaImIp3nS
příznivě	příznivě	k6eAd1
na	na	k7c6
zřízení	zřízení	k1gNnSc6
národní	národní	k2eAgFnSc2d1
domoviny	domovina	k1gFnSc2
židovského	židovský	k2eAgInSc2d1
lidu	lid	k1gInSc2
v	v	k7c6
Palestině	Palestina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozpadu	rozpad	k1gInSc3
Osmanské	osmanský	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
říše	říš	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc2
současného	současný	k2eAgInSc2d1
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
Palestinské	palestinský	k2eAgFnSc2d1
autonomie	autonomie	k1gFnSc2
a	a	k8xC
Jordánska	Jordánsko	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dána	dát	k5eAaPmNgFnS
do	do	k7c2
správy	správa	k1gFnSc2
Spojenému	spojený	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
protektorát	protektorát	k1gInSc4
Britský	britský	k2eAgInSc1d1
mandát	mandát	k1gInSc1
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
mezidobí	mezidobí	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
osamostatnění	osamostatnění	k1gNnSc3
Jordánska	Jordánsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
pro	pro	k7c4
zamýšlený	zamýšlený	k2eAgInSc4d1
židovský	židovský	k2eAgInSc4d1
stát	stát	k1gInSc4
připadlo	připadnout	k5eAaPmAgNnS
v	v	k7c4
úvahu	úvaha	k1gFnSc4
pouze	pouze	k6eAd1
území	území	k1gNnSc4
západně	západně	k6eAd1
od	od	k7c2
Jordánu	Jordán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
britské	britský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
bylo	být	k5eAaImAgNnS
Arabům	Arab	k1gMnPc3
i	i	k8xC
Židům	Žid	k1gMnPc3
umožněno	umožněn	k2eAgNnSc4d1
se	se	k3xPyFc4
částečně	částečně	k6eAd1
podílet	podílet	k5eAaImF
na	na	k7c6
správě	správa	k1gFnSc6
Mandátu	mandát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabové	Arab	k1gMnPc1
sice	sice	k8xC
působili	působit	k5eAaImAgMnP
na	na	k7c6
některých	některý	k3yIgNnPc6
úřednických	úřednický	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
odmítli	odmítnout	k5eAaPmAgMnP
se	se	k3xPyFc4
podílet	podílet	k5eAaImF
na	na	k7c6
vzniku	vznik	k1gInSc6
nového	nový	k2eAgInSc2d1
legislativního	legislativní	k2eAgInSc2d1
orgánu	orgán	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
odřízli	odříznout	k5eAaPmAgMnP
od	od	k7c2
vlivu	vliv	k1gInSc2
na	na	k7c4
dění	dění	k1gNnSc4
v	v	k7c6
Mandátu	mandát	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
správy	správa	k1gFnSc2
zavedli	zavést	k5eAaPmAgMnP
Britové	Brit	k1gMnPc1
kvóty	kvóta	k1gFnSc2
pro	pro	k7c4
židovské	židovský	k2eAgMnPc4d1
imigranty	imigrant	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
vyvrcholily	vyvrcholit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
přijetím	přijetí	k1gNnSc7
Bílé	bílý	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
neochotu	neochota	k1gFnSc4
Britů	Brit	k1gMnPc2
dostát	dostát	k5eAaPmF
svým	svůj	k3xOyFgInPc3
závazkům	závazek	k1gInPc3
se	se	k3xPyFc4
proti	proti	k7c3
mandátní	mandátní	k2eAgFnSc3d1
správě	správa	k1gFnSc3
postavila	postavit	k5eAaPmAgFnS
židovská	židovská	k1gFnSc1
podzemní	podzemní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Irgun	Irguna	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
proti	proti	k7c3
britským	britský	k2eAgInPc3d1
vojenským	vojenský	k2eAgInPc3d1
civilním	civilní	k2eAgInPc3d1
a	a	k8xC
vojenským	vojenský	k2eAgInPc3d1
cílům	cíl	k1gInPc3
páchala	páchat	k5eAaImAgFnS
atentáty	atentát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
z	z	k7c2
Irgunu	Irgun	k1gInSc2
vydělila	vydělit	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
Lechi	Lech	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
ještě	ještě	k6eAd1
militantnější	militantní	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
nakonec	nakonec	k6eAd1
nevěděli	vědět	k5eNaImAgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
nastalou	nastalý	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
řešit	řešit	k5eAaImF
a	a	k8xC
tak	tak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
předali	předat	k5eAaPmAgMnP
tento	tento	k3xDgInSc4
problém	problém	k1gInSc4
k	k	k7c3
řešení	řešení	k1gNnSc3
Organizaci	organizace	k1gFnSc4
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
sestavila	sestavit	k5eAaPmAgFnS
zvláštní	zvláštní	k2eAgFnSc4d1
komisi	komise	k1gFnSc4
pro	pro	k7c4
Palestinu	Palestina	k1gFnSc4
(	(	kIx(
<g/>
UNSCOP	UNSCOP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
posléze	posléze	k6eAd1
připravila	připravit	k5eAaPmAgFnS
plán	plán	k1gInSc4
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
schválen	schválen	k2eAgInSc1d1
Valným	valný	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
rozhodnutím	rozhodnutí	k1gNnSc7
připadlo	připadnout	k5eAaPmAgNnS
54	#num#	k4
%	%	kIx~
území	území	k1gNnSc6
mandátu	mandát	k1gInSc2
Židům	Žid	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
tvořili	tvořit	k5eAaImAgMnP
1	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Arabům	Arab	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
tvořili	tvořit	k5eAaImAgMnP
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
určeno	určit	k5eAaPmNgNnS
46	#num#	k4
%	%	kIx~
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židé	Žid	k1gMnPc1
plán	plán	k1gInSc4
přijali	přijmout	k5eAaPmAgMnP
<g/>
,	,	kIx,
přestože	přestože	k8xS
75	#num#	k4
%	%	kIx~
území	území	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
jim	on	k3xPp3gMnPc3
bylo	být	k5eAaImAgNnS
přiřknuto	přiřknout	k5eAaPmNgNnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
poušť	poušť	k1gFnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Liga	liga	k1gFnSc1
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
Vysoká	vysoký	k2eAgFnSc1d1
arabská	arabský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
plán	plán	k1gInSc4
odmítly	odmítnout	k5eAaPmAgFnP
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ihned	ihned	k6eAd1
zahájili	zahájit	k5eAaPmAgMnP
útoky	útok	k1gInPc4
na	na	k7c4
židovské	židovský	k2eAgFnPc4d1
osady	osada	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
z	z	k7c2
Palestiny	Palestina	k1gFnSc2
stahovali	stahovat	k5eAaImAgMnP
velmi	velmi	k6eAd1
neochotně	ochotně	k6eNd1
sice	sice	k8xC
Araby	Arab	k1gMnPc4
proti	proti	k7c3
Židům	Žid	k1gMnPc3
zvýhodňovali	zvýhodňovat	k5eAaImAgMnP
(	(	kIx(
<g/>
Arabskou	arabský	k2eAgFnSc4d1
legii	legie	k1gFnSc4
dokonce	dokonce	k9
vedl	vést	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
díky	díky	k7c3
ilegálnímu	ilegální	k2eAgNnSc3d1
vyzbrojování	vyzbrojování	k1gNnSc3
a	a	k8xC
malé	malý	k2eAgFnSc3d1
koordinaci	koordinace	k1gFnSc3
jednotlivých	jednotlivý	k2eAgFnPc2d1
arabských	arabský	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
frakcí	frakce	k1gFnPc2
se	se	k3xPyFc4
však	však	k9
Židé	Žid	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
těmto	tento	k3xDgInPc3
útokům	útok	k1gInPc3
nezřídka	nezřídka	k6eAd1
ubránit	ubránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židé	Žid	k1gMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
zejména	zejména	k9
o	o	k7c6
ubránění	ubránění	k1gNnSc6
celé	celý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
jim	on	k3xPp3gMnPc3
přiřkla	přiřknout	k5eAaPmAgFnS
rezoluce	rezoluce	k1gFnSc1
OSN	OSN	kA
<g/>
,	,	kIx,
v	v	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
jim	on	k3xPp3gMnPc3
pomohly	pomoct	k5eAaPmAgFnP
i	i	k9
československé	československý	k2eAgFnPc1d1
dodávky	dodávka	k1gFnPc1
zbraní	zbraň	k1gFnPc2
v	v	k7c6
dubnu	duben	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
židovských	židovský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
zejména	zejména	k9
v	v	k7c6
Galileji	Galilea	k1gFnSc6
vyděsily	vyděsit	k5eAaPmAgInP
arabské	arabský	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
<g/>
,	,	kIx,
již	jenž	k3xRgMnPc1
začali	začít	k5eAaPmAgMnP
masově	masově	k6eAd1
opouštět	opouštět	k5eAaImF
své	svůj	k3xOyFgInPc4
domovy	domov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
Židů	Žid	k1gMnPc2
dokonce	dokonce	k9
Araby	Arab	k1gMnPc4
zastrašovala	zastrašovat	k5eAaImAgFnS
zcela	zcela	k6eAd1
cíleně	cíleně	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
donutila	donutit	k5eAaPmAgFnS
k	k	k7c3
odchodu	odchod	k1gInSc3
z	z	k7c2
území	území	k1gNnSc2
budoucího	budoucí	k2eAgInSc2d1
židovského	židovský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
docházelo	docházet	k5eAaImAgNnS
ze	z	k7c2
strany	strana	k1gFnSc2
Židů	Žid	k1gMnPc2
k	k	k7c3
etnickému	etnický	k2eAgNnSc3d1
čištění	čištění	k1gNnSc3
území	území	k1gNnSc2
<g/>
,	,	kIx,
boření	boření	k1gNnSc2
domů	dům	k1gInPc2
a	a	k8xC
celých	celý	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
a	a	k8xC
k	k	k7c3
vyhánění	vyhánění	k1gNnSc3
místního	místní	k2eAgNnSc2d1
arabského	arabský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Odcházející	odcházející	k2eAgNnPc1d1
britská	britský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
se	se	k3xPyFc4
nijak	nijak	k6eAd1
nesnažila	snažit	k5eNaImAgFnS
tuto	tento	k3xDgFnSc4
„	„	k?
<g/>
nevyhlášenou	vyhlášený	k2eNgFnSc4d1
válku	válka	k1gFnSc4
<g/>
“	“	k?
nějak	nějak	k6eAd1
mírnit	mírnit	k5eAaImF
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
při	při	k7c6
ní	on	k3xPp3gFnSc6
docházelo	docházet	k5eAaImAgNnS
i	i	k9
k	k	k7c3
masakrům	masakr	k1gInPc3
židovského	židovský	k2eAgNnSc2d1
i	i	k8xC
arabského	arabský	k2eAgNnSc2d1
civilního	civilní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k8xC
den	den	k1gInSc4
před	před	k7c7
vypršením	vypršení	k1gNnSc7
britského	britský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
v	v	k7c6
Palestině	Palestina	k1gFnSc6
<g/>
)	)	kIx)
britští	britský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
opustili	opustit	k5eAaPmAgMnP
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
rozhodlo	rozhodnout	k5eAaPmAgNnS
se	se	k3xPyFc4
židovské	židovský	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
vyhlásit	vyhlásit	k5eAaPmF
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostní	slavnostní	k2eAgFnSc7d1
Prohlášení	prohlášení	k1gNnSc4
nezávislosti	nezávislost	k1gFnSc2
přečetl	přečíst	k5eAaPmAgMnS
v	v	k7c4
16	#num#	k4
hodin	hodina	k1gFnPc2
v	v	k7c6
Tel	tel	kA
Avivu	Aviv	k1gInSc6
David	David	k1gMnSc1
Ben	Ben	k1gInSc1
Gurion	Gurion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodního	mezinárodní	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
se	se	k3xPyFc4
Státu	stát	k1gInSc3
Izrael	Izrael	k1gInSc4
vzápětí	vzápětí	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
od	od	k7c2
USA	USA	kA
i	i	k8xC
SSSR	SSSR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
První	první	k4xOgFnSc1
arabsko-izraelská	arabsko-izraelský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
Palestinský	palestinský	k2eAgInSc1d1
exodus	exodus	k1gInSc1
<g/>
,	,	kIx,
Židovský	židovský	k2eAgInSc1d1
exodus	exodus	k1gInSc1
z	z	k7c2
arabských	arabský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
Suezská	suezský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
a	a	k8xC
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
linie	linie	k1gFnSc1
Borderes	Borderesa	k1gFnPc2
</s>
<s>
Druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
nato	nato	k6eAd1
armády	armáda	k1gFnSc2
pěti	pět	k4xCc2
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
Izrael	Izrael	k1gInSc4
napadly	napadnout	k5eAaPmAgFnP
<g/>
,	,	kIx,
a	a	k8xC
rozpoutaly	rozpoutat	k5eAaPmAgInP
tak	tak	k9
Válku	válka	k1gFnSc4
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
roce	rok	k1gInSc6
bojů	boj	k1gInPc2
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlášen	k2eAgNnSc1d1
příměří	příměří	k1gNnSc1
a	a	k8xC
stanoveny	stanoven	k2eAgFnPc1d1
dočasné	dočasný	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
<g/>
,	,	kIx,
známé	známý	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
Zelená	zelený	k2eAgFnSc1d1
linie	linie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Jordánsko	Jordánsko	k1gNnSc1
anektovalo	anektovat	k5eAaBmAgNnS
území	území	k1gNnSc4
Judeje	Judea	k1gFnSc2
a	a	k8xC
Samaří	Samaří	k1gNnSc2
<g/>
,	,	kIx,
známé	známý	k2eAgNnSc1d1
spíše	spíše	k9
jako	jako	k8xS,k8xC
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Východní	východní	k2eAgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypt	Egypt	k1gInSc1
převzal	převzít	k5eAaPmAgInS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Pásmem	pásmo	k1gNnSc7
Gazy	Gaza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
před	před	k7c7
válkou	válka	k1gFnSc7
a	a	k8xC
během	během	k7c2
ní	on	k3xPp3gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
masivnímu	masivní	k2eAgInSc3d1
arabskému	arabský	k2eAgInSc3d1
exodu	exodus	k1gInSc3
do	do	k7c2
sousedních	sousední	k2eAgFnPc2d1
arabských	arabský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
odhadů	odhad	k1gInPc2
OSN	OSN	kA
na	na	k7c4
711	#num#	k4
tisíc	tisíc	k4xCgInPc2
Arabů	Arab	k1gMnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
zhruba	zhruba	k6eAd1
80	#num#	k4
%	%	kIx~
dřívější	dřívější	k2eAgFnSc2d1
arabské	arabský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Arabové	Arab	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
zůstali	zůstat	k5eAaPmAgMnP
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
začleněni	začlenit	k5eAaPmNgMnP
do	do	k7c2
nového	nový	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
odešli	odejít	k5eAaPmAgMnP
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
označeni	označit	k5eAaPmNgMnP
jako	jako	k9
uprchlíci	uprchlík	k1gMnPc1
bez	bez	k7c2
práva	právo	k1gNnSc2
návratu	návrat	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabské	arabský	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
uprchlíci	uprchlík	k1gMnPc1
nacházeli	nacházet	k5eAaImAgMnP
<g/>
,	,	kIx,
však	však	k8xC
nepodnikly	podniknout	k5eNaPmAgFnP
žádná	žádný	k3yNgNnPc4
opatření	opatření	k1gNnPc4
vedoucí	vedoucí	k1gMnSc1
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
začlenění	začlenění	k1gNnSc3
a	a	k8xC
naopak	naopak	k6eAd1
je	on	k3xPp3gNnSc4
udržovali	udržovat	k5eAaImAgMnP
v	v	k7c6
uprchlických	uprchlický	k2eAgInPc6d1
táborech	tábor	k1gInPc6
<g/>
,	,	kIx,
využívali	využívat	k5eAaPmAgMnP,k5eAaImAgMnP
je	on	k3xPp3gNnSc4
jako	jako	k8xC,k8xS
politický	politický	k2eAgInSc4d1
trumf	trumf	k1gInSc4
a	a	k8xC
uměle	uměle	k6eAd1
v	v	k7c6
nich	on	k3xPp3gInPc6
vyvolávali	vyvolávat	k5eAaImAgMnP
nenávist	nenávist	k1gFnSc4
vůči	vůči	k7c3
Izraeli	Izrael	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
území	území	k1gNnPc2
okupovaných	okupovaný	k2eAgNnPc2d1
Egyptem	Egypt	k1gInSc7
a	a	k8xC
Jordánskem	Jordánsko	k1gNnSc7
do	do	k7c2
Izraele	Izrael	k1gInSc2
vyráželi	vyrážet	k5eAaImAgMnP
tzv.	tzv.	kA
fedajíni	fedajín	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
podnikali	podnikat	k5eAaImAgMnP
útoky	útok	k1gInPc4
na	na	k7c4
izraelské	izraelský	k2eAgInPc4d1
cíle	cíl	k1gInPc4
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
založení	založení	k1gNnSc3
teroristické	teroristický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Fatah	Fataha	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
k	k	k7c3
založení	založení	k1gNnSc3
Organizace	organizace	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
(	(	kIx(
<g/>
OOP	OOP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
deklarovala	deklarovat	k5eAaBmAgFnS
cíl	cíl	k1gInSc4
„	„	k?
<g/>
dosáhnout	dosáhnout	k5eAaPmF
zničení	zničení	k1gNnSc1
Izraele	Izrael	k1gInSc2
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
Národní	národní	k2eAgFnSc6d1
chartě	charta	k1gFnSc6
se	se	k3xPyFc4
až	až	k6eAd1
do	do	k7c2
počátku	počátek	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
uvádělo	uvádět	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
ozbrojené	ozbrojený	k2eAgFnPc4d1
povstání	povstání	k1gNnSc2
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
jak	jak	k8xC,k8xS
osvobodit	osvobodit	k5eAaPmF
vlast	vlast	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
také	také	k9
vzniká	vznikat	k5eAaImIp3nS
označení	označení	k1gNnSc1
„	„	k?
<g/>
Palestinci	Palestinec	k1gMnSc3
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgMnS
Jásir	Jásir	k1gMnSc1
Arafat	Arafat	k1gMnSc1
za	za	k7c7
účelem	účel	k1gInSc7
sjednocení	sjednocení	k1gNnSc2
palestinských	palestinský	k2eAgFnPc2d1
teroristických	teroristický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
palestinských	palestinský	k2eAgMnPc2d1
Arabů	Arab	k1gMnPc2
<g/>
,	,	kIx,
zvýšení	zvýšení	k1gNnSc3
nacionálního	nacionální	k2eAgNnSc2d1
cítění	cítění	k1gNnSc2
a	a	k8xC
nenávisti	nenávist	k1gFnSc2
vůči	vůči	k7c3
Izraeli	Izrael	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1967	#num#	k4
egyptské	egyptský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
překročilo	překročit	k5eAaPmAgNnS
Suez	Suez	k1gInSc4
a	a	k8xC
vyzvalo	vyzvat	k5eAaPmAgNnS
jednotky	jednotka	k1gFnPc4
OSN	OSN	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
ze	z	k7c2
Sinaje	Sinaj	k1gInSc2
stáhly	stáhnout	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jednotky	jednotka	k1gFnPc1
OSN	OSN	kA
výzvu	výzva	k1gFnSc4
uposlechly	uposlechnout	k5eAaPmAgFnP
<g/>
,	,	kIx,
vyhlásil	vyhlásit	k5eAaPmAgMnS
Izrael	Izrael	k1gMnSc1
totální	totální	k2eAgFnSc4d1
mobilizaci	mobilizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
měsíce	měsíc	k1gInSc2
vznikl	vzniknout	k5eAaPmAgInS
egyptsko-jordánsko-syrsko-irácký	egyptsko-jordánsko-syrsko-irácký	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
pakt	pakt	k1gInSc1
a	a	k8xC
arabské	arabský	k2eAgInPc1d1
státy	stát	k1gInPc1
začaly	začít	k5eAaPmAgInP
přesouvat	přesouvat	k5eAaImF
k	k	k7c3
izraelským	izraelský	k2eAgFnPc3d1
hranicím	hranice	k1gFnPc3
své	svůj	k3xOyFgFnSc2
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1967	#num#	k4
se	se	k3xPyFc4
proto	proto	k8xC
Izrael	Izrael	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
zaútočit	zaútočit	k5eAaPmF
<g/>
,	,	kIx,
zničil	zničit	k5eAaPmAgInS
celé	celý	k2eAgNnSc4d1
egyptské	egyptský	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
a	a	k8xC
během	během	k7c2
šesti	šest	k4xCc2
dnů	den	k1gInPc2
obsadil	obsadit	k5eAaPmAgInS
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
<g/>
,	,	kIx,
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
,	,	kIx,
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
a	a	k8xC
Golanské	Golanský	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Operace	operace	k1gFnSc2
Lítání	Lítání	k?
<g/>
,	,	kIx,
První	první	k4xOgFnSc1
libanonská	libanonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
a	a	k8xC
První	první	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
Šestidenní	šestidenní	k2eAgFnSc2d1
války	válka	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
odlivu	odliv	k1gInSc3
arabského	arabský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
a	a	k8xC
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
byly	být	k5eAaImAgInP
spravovány	spravovat	k5eAaImNgInP
jako	jako	k8xC,k8xS
okupovaná	okupovaný	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
východní	východní	k2eAgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
byl	být	k5eAaImAgInS
formálně	formálně	k6eAd1
připojen	připojit	k5eAaPmNgInS
k	k	k7c3
Izraeli	Izrael	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
Izraeli	Izrael	k1gInSc6
diskuse	diskuse	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
naložit	naložit	k5eAaPmF
se	s	k7c7
získanými	získaný	k2eAgNnPc7d1
územími	území	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silně	silně	k6eAd1
levicové	levicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
požadovaly	požadovat	k5eAaImAgFnP
jejich	jejich	k3xOp3gNnSc4
vrácení	vrácení	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
silně	silně	k6eAd1
pravicové	pravicový	k2eAgFnPc1d1
strany	strana	k1gFnPc1
o	o	k7c4
navrácení	navrácení	k1gNnSc4
odmítaly	odmítat	k5eAaImAgFnP
jednat	jednat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
došlo	dojít	k5eAaPmAgNnS
za	za	k7c2
levicové	levicový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
Ma	Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
arachu	aracha	k1gMnSc4
k	k	k7c3
výstavbě	výstavba	k1gFnSc6
prvních	první	k4xOgFnPc2
osad	osada	k1gFnPc2
na	na	k7c6
strategických	strategický	k2eAgNnPc6d1
územích	území	k1gNnPc6
podél	podél	k7c2
údolí	údolí	k1gNnSc2
Jordánu	Jordán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
válce	válka	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velkým	velký	k2eAgFnPc3d1
investicím	investice	k1gFnPc3
jak	jak	k6eAd1
v	v	k7c6
Pásmu	pásmo	k1gNnSc6
Gazy	Gaza	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
na	na	k7c6
Západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
a	a	k8xC
palestinští	palestinský	k2eAgMnPc1d1
Arabové	Arab	k1gMnPc1
z	z	k7c2
těchto	tento	k3xDgNnPc2
území	území	k1gNnPc2
získali	získat	k5eAaPmAgMnP
povolení	povolení	k1gNnSc4
pro	pro	k7c4
práci	práce	k1gFnSc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
takže	takže	k8xS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
plné	plný	k2eAgFnSc3d1
zaměstnanosti	zaměstnanost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Jordánsku	Jordánsko	k1gNnSc6
odehrálo	odehrát	k5eAaPmAgNnS
tzv.	tzv.	kA
Černé	Černé	k2eAgNnSc2d1
září	září	k1gNnSc2
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
palestinští	palestinský	k2eAgMnPc1d1
Arabové	Arab	k1gMnPc1
pokusili	pokusit	k5eAaPmAgMnP
svrhnout	svrhnout	k5eAaPmF
a	a	k8xC
zabít	zabít	k5eAaPmF
jordánského	jordánský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Husajna	Husajn	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgInSc1
následně	následně	k6eAd1
nařídil	nařídit	k5eAaPmAgInS
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
sídla	sídlo	k1gNnPc4
OOP	OOP	kA
a	a	k8xC
vyhnal	vyhnat	k5eAaPmAgInS
tuto	tento	k3xDgFnSc4
organizaci	organizace	k1gFnSc4
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
OOP	OOP	kA
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
Libanonu	Libanon	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
útočila	útočit	k5eAaImAgFnS
na	na	k7c4
Izrael	Izrael	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
mediálně	mediálně	k6eAd1
nejznámějšímu	známý	k2eAgInSc3d3
teroristickému	teroristický	k2eAgInSc3d1
útoku	útok	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
během	během	k7c2
letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
roku	rok	k1gInSc2
1972	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Došlo	dojít	k5eAaPmAgNnS
při	při	k7c6
něm	on	k3xPp3gMnSc6
k	k	k7c3
únosu	únos	k1gInSc2
a	a	k8xC
následnému	následný	k2eAgNnSc3d1
zavraždění	zavraždění	k1gNnSc3
11	#num#	k4
izraelských	izraelský	k2eAgMnPc2d1
olympioniků	olympionik	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Izrael	Izrael	k1gInSc1
odpověděl	odpovědět	k5eAaPmAgInS
Operací	operace	k1gFnSc7
Boží	boží	k2eAgInSc1d1
hněv	hněv	k1gInSc1
<g/>
,	,	kIx,
pokusem	pokus	k1gInSc7
izraelské	izraelský	k2eAgFnPc1d1
tajné	tajný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
Mosad	Mosad	k1gInSc4
zlikvidovat	zlikvidovat	k5eAaPmF
strůjce	strůjce	k1gMnSc1
mnichovského	mnichovský	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
sedmém	sedmý	k4xOgInSc6
arabském	arabský	k2eAgInSc6d1
summitu	summit	k1gInSc6
v	v	k7c6
Rabatu	rabat	k1gInSc6
určeno	určit	k5eAaPmNgNnS
OOP	OOP	kA
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
legitimní	legitimní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
palestinského	palestinský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
bylo	být	k5eAaImAgNnS
OOP	OOP	kA
přizváno	přizvat	k5eAaPmNgNnS
Valným	valný	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
OSN	OSN	kA
jako	jako	k8xC,k8xS
pozorovatel	pozorovatel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
teroristické	teroristický	k2eAgFnPc4d1
akce	akce	k1gFnPc4
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
mezinárodní	mezinárodní	k2eAgFnSc7d1
publicitou	publicita	k1gFnSc7
patřil	patřit	k5eAaImAgInS
únos	únos	k1gInSc1
letounu	letoun	k1gInSc2
Air	Air	k1gMnSc2
France	Franc	k1gMnSc2
s	s	k7c7
izraelskými	izraelský	k2eAgMnPc7d1
turisty	turist	k1gMnPc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
odlet	odlet	k1gInSc1
do	do	k7c2
Ugandy	Uganda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Izraelská	izraelský	k2eAgFnSc1d1
speciální	speciální	k2eAgFnSc1d1
komanda	komando	k1gNnSc2
Sajeret	Sajeret	k1gMnSc1
Matkal	Matkal	k1gMnSc1
však	však	k9
při	při	k7c6
operaci	operace	k1gFnSc6
Entebbe	Entebb	k1gInSc5
všechny	všechen	k3xTgMnPc4
cestující	cestující	k1gMnPc4
osvobodila	osvobodit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
útoků	útok	k1gInPc2
z	z	k7c2
jižního	jižní	k2eAgInSc2d1
Libanonu	Libanon	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
koncem	koncem	k7c2
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátkem	počátkem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
ke	k	k7c3
konfrontaci	konfrontace	k1gFnSc3
s	s	k7c7
Libanonem	Libanon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
operace	operace	k1gFnSc1
Lítání	Lítání	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
IOS	IOS	kA
zatlačily	zatlačit	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
OOP	OOP	kA
až	až	k8xS
za	za	k7c4
řeku	řeka	k1gFnSc4
Lítání	Lítání	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
však	však	k9
stáhly	stáhnout	k5eAaPmAgInP
a	a	k8xC
území	území	k1gNnSc6
kontrolovaly	kontrolovat	k5eAaImAgFnP
jednotky	jednotka	k1gFnPc1
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
konfrontací	konfrontace	k1gFnSc7
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
libanonská	libanonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
úplnému	úplný	k2eAgNnSc3d1
vyhnání	vyhnání	k1gNnSc3
OOP	OOP	kA
z	z	k7c2
Libanonu	Libanon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
válka	válka	k1gFnSc1
a	a	k8xC
následná	následný	k2eAgFnSc1d1
izraelská	izraelský	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
v	v	k7c6
Libanonu	Libanon	k1gInSc6
vedla	vést	k5eAaImAgFnS
k	k	k7c3
velkým	velký	k2eAgNnPc3d1
protestním	protestní	k2eAgNnPc3d1
hnutím	hnutí	k1gNnPc3
v	v	k7c6
Izraeli	Izrael	k1gInSc6
a	a	k8xC
ke	k	k7c3
vzniku	vznik	k1gInSc3
teroristických	teroristický	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
Hamás	Hamása	k1gFnPc2
a	a	k8xC
Hizballáh	Hizballáha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
tyto	tento	k3xDgFnPc1
organizace	organizace	k1gFnPc1
odmítají	odmítat	k5eAaImIp3nP
jakékoliv	jakýkoliv	k3yIgNnSc1
územní	územní	k2eAgInPc1d1
ústupky	ústupek	k1gInPc1
ve	v	k7c6
sporu	spor	k1gInSc6
s	s	k7c7
Izraelem	Izrael	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
palestinskému	palestinský	k2eAgNnSc3d1
lidovému	lidový	k2eAgNnSc3d1
povstání	povstání	k1gNnSc3
zvaného	zvaný	k2eAgInSc2d1
první	první	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
také	také	k9
někdy	někdy	k6eAd1
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
válka	válka	k1gFnSc1
kamenů	kámen	k1gInPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vyvrcholením	vyvrcholení	k1gNnSc7
trvalého	trvalý	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
mezi	mezi	k7c7
palestinskými	palestinský	k2eAgMnPc7d1
Araby	Arab	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Rozbuškou	rozbuška	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
dopravní	dopravní	k2eAgFnSc1d1
nehoda	nehoda	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
zahynulo	zahynout	k5eAaPmAgNnS
několik	několik	k4yIc1
Arabů	Arab	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Následné	následný	k2eAgNnSc1d1
násilí	násilí	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
nepokoje	nepokoj	k1gInPc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
z	z	k7c2
Pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gFnSc2
přesunuly	přesunout	k5eAaPmAgInP
i	i	k9
na	na	k7c4
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Oficiálně	oficiálně	k6eAd1
byla	být	k5eAaImAgFnS
intifáda	intifáda	k1gFnSc1
vedena	vést	k5eAaImNgFnS
OOP	OOP	kA
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
byl	být	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
Arafat	Arafat	k1gMnSc1
intifádou	intifáda	k1gFnSc7
překvapen	překvapit	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Většinou	většinou	k6eAd1
nedocházelo	docházet	k5eNaImAgNnS
k	k	k7c3
ozbrojeným	ozbrojený	k2eAgInPc3d1
bojům	boj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestinské	palestinský	k2eAgFnPc1d1
akce	akce	k1gFnPc1
zahrnovaly	zahrnovat	k5eAaImAgFnP
porušování	porušování	k1gNnSc4
veřejného	veřejný	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
<g/>
,	,	kIx,
demonstrace	demonstrace	k1gFnPc1
<g/>
,	,	kIx,
generální	generální	k2eAgFnPc1d1
stávky	stávka	k1gFnPc1
<g/>
,	,	kIx,
bojkoty	bojkot	k1gInPc1
<g/>
,	,	kIx,
házení	házení	k1gNnSc1
kamenů	kámen	k1gInPc2
<g/>
,	,	kIx,
zapalování	zapalování	k1gNnSc4
pneumatik	pneumatika	k1gFnPc2
<g/>
,	,	kIx,
graffiti	graffiti	k1gNnSc2
<g/>
,	,	kIx,
barikády	barikáda	k1gFnSc2
a	a	k8xC
pouze	pouze	k6eAd1
občas	občas	k6eAd1
byly	být	k5eAaImAgFnP
zaznamenány	zaznamenat	k5eAaPmNgFnP
skutečné	skutečný	k2eAgFnPc1d1
teroristické	teroristický	k2eAgFnPc1d1
akce	akce	k1gFnPc1
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgNnPc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
používání	používání	k1gNnSc3
Molotovových	Molotovův	k2eAgInPc2d1
koktejlů	koktejl	k1gInPc2
či	či	k8xC
granátů	granát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
:	:	kIx,
Dohody	dohoda	k1gFnPc1
z	z	k7c2
Osla	Oslo	k1gNnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Mírová	mírový	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
z	z	k7c2
Osla	Oslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jicchak	Jicchak	k1gMnSc1
Rabin	Rabin	k1gMnSc1
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
Clinton	Clinton	k1gMnSc1
a	a	k8xC
Jásir	Jásir	k1gMnSc1
Arafat	Arafat	k1gMnSc1
po	po	k7c6
podepsání	podepsání	k1gNnSc6
Mírových	Mírův	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
z	z	k7c2
Osla	Oslo	k1gNnSc2
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1993	#num#	k4
</s>
<s>
Během	během	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
množství	množství	k1gNnSc1
mírových	mírový	k2eAgFnPc2d1
konferencí	konference	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
po	po	k7c6
Válce	válka	k1gFnSc6
v	v	k7c6
Zálivu	záliv	k1gInSc6
roku	rok	k1gInSc2
1992	#num#	k4
a	a	k8xC
nazývala	nazývat	k5eAaImAgFnS
se	se	k3xPyFc4
Madridská	madridský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
průlomová	průlomový	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
u	u	k7c2
jednoho	jeden	k4xCgInSc2
stolu	stol	k1gInSc2
sešly	sejít	k5eAaPmAgFnP
znepřátelené	znepřátelený	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
představitelé	představitel	k1gMnPc1
zde	zde	k6eAd1
spolu	spolu	k6eAd1
přímo	přímo	k6eAd1
hovořili	hovořit	k5eAaImAgMnP
poprvé	poprvé	k6eAd1
od	od	k7c2
Války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
přímým	přímý	k2eAgFnPc3d1
schůzkám	schůzka	k1gFnPc3
mezi	mezi	k7c7
Izraelci	Izraelec	k1gMnPc7
a	a	k8xC
Araby	Arab	k1gMnPc7
v	v	k7c6
různých	různý	k2eAgNnPc6d1
městech	město	k1gNnPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Tzv.	tzv.	kA
Mírová	mírový	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
z	z	k7c2
Osla	Oslo	k1gNnSc2
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1993	#num#	k4
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ní	on	k3xPp3gFnSc2
vyplývalo	vyplývat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
po	po	k7c4
přechodnou	přechodný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
pěti	pět	k4xCc3
let	léto	k1gNnPc2
na	na	k7c6
území	území	k1gNnSc6
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
a	a	k8xC
pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gFnSc2
ustanovena	ustanoven	k2eAgFnSc1d1
Palestinská	palestinský	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Podepsání	podepsání	k1gNnSc1
dohod	dohoda	k1gFnPc2
z	z	k7c2
Osla	Oslo	k1gNnSc2
narazilo	narazit	k5eAaPmAgNnS
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
na	na	k7c4
odpor	odpor	k1gInSc4
radikálů	radikál	k1gMnPc2
<g/>
,	,	kIx,
což	což	k9
mírový	mírový	k2eAgInSc4d1
proces	proces	k1gInSc4
značně	značně	k6eAd1
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
palestinské	palestinský	k2eAgFnSc6d1
politické	politický	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
začala	začít	k5eAaPmAgFnS
sílit	sílit	k5eAaImF
opozice	opozice	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
Hamás	Hamás	k1gInSc1
a	a	k8xC
Palestinský	palestinský	k2eAgInSc1d1
islámský	islámský	k2eAgInSc1d1
džihád	džihád	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
proti	proti	k7c3
Organizaci	organizace	k1gFnSc3
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
a	a	k8xC
Smlouvám	smlouvat	k5eAaImIp1nS
z	z	k7c2
Osla	Oslo	k1gNnSc2
začala	začít	k5eAaPmAgFnS
vymezovat	vymezovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Izraeli	Izrael	k1gInSc6
se	se	k3xPyFc4
proti	proti	k7c3
smlouvě	smlouva	k1gFnSc3
vymezila	vymezit	k5eAaPmAgFnS
strana	strana	k1gFnSc1
Likud	Likudo	k1gNnPc2
a	a	k8xC
další	další	k2eAgNnPc4d1
pravicová	pravicový	k2eAgNnPc4d1
uskupení	uskupení	k1gNnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
sám	sám	k3xTgInSc1
Jicchak	Jicchak	k1gInSc1
Rabin	Rabina	k1gFnPc2
byl	být	k5eAaImAgInS
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1995	#num#	k4
zavražděn	zavraždit	k5eAaPmNgInS
pravicovým	pravicový	k2eAgInSc7d1
židovským	židovský	k2eAgInSc7d1
radikálem	radikál	k1gInSc7
Jigalem	Jigal	k1gMnSc7
Amirem	Amir	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mírový	mírový	k2eAgInSc1d1
proces	proces	k1gInSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
velmi	velmi	k6eAd1
zpomalil	zpomalit	k5eAaPmAgInS
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
po	po	k7c6
zvolení	zvolení	k1gNnSc6
Benjamina	Benjamin	k1gMnSc2
Netanjahu	Netanjah	k1gInSc2
z	z	k7c2
Likudu	Likud	k1gInSc2
premiérem	premiér	k1gMnSc7
se	se	k3xPyFc4
zastavil	zastavit	k5eAaPmAgMnS
téměř	téměř	k6eAd1
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Netanjahu	Netanjah	k1gInSc2
byl	být	k5eAaImAgMnS
odpůrcem	odpůrce	k1gMnSc7
územních	územní	k2eAgInPc2d1
kompromisů	kompromis	k1gInPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
i	i	k9
přes	přes	k7c4
to	ten	k3xDgNnSc4
byly	být	k5eAaImAgInP
za	za	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
podepsány	podepsán	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
důležité	důležitý	k2eAgFnPc1d1
dohody	dohoda	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
Hebronský	hebronský	k2eAgInSc1d1
protokol	protokol	k1gInSc1
a	a	k8xC
Memorandum	memorandum	k1gNnSc1
od	od	k7c2
Wye	Wye	k1gFnSc2
River	River	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
znamenala	znamenat	k5eAaImAgNnP
předání	předání	k1gNnSc3
posledního	poslední	k2eAgNnSc2d1
velkého	velký	k2eAgNnSc2d1
palestinského	palestinský	k2eAgNnSc2d1
města	město	k1gNnSc2
Arabům	Arab	k1gMnPc3
a	a	k8xC
druhá	druhý	k4xOgFnSc1
pak	pak	k6eAd1
předání	předání	k1gNnSc4
13	#num#	k4
%	%	kIx~
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Palestincům	Palestinec	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
vystřídal	vystřídat	k5eAaPmAgMnS
Netanjahua	Netanjahua	k1gMnSc1
labouristický	labouristický	k2eAgMnSc1d1
Ehud	Ehud	k1gMnSc1
Barak	Barak	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
konání	konání	k1gNnSc2
několika	několik	k4yIc2
mírových	mírový	k2eAgFnPc2d1
konferencí	konference	k1gFnPc2
(	(	kIx(
<g/>
nejznámější	známý	k2eAgFnSc1d3
z	z	k7c2
nich	on	k3xPp3gFnPc2
v	v	k7c4
Camp	camp	k1gInSc4
Davidu	David	k1gMnSc3
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
žádná	žádný	k3yNgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
nebyla	být	k5eNaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barak	Barak	k1gInSc1
v	v	k7c6
jednáních	jednání	k1gNnPc6
nabídl	nabídnout	k5eAaPmAgMnS
historické	historický	k2eAgInPc4d1
ústupky	ústupek	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
předání	předání	k1gNnSc2
východního	východní	k2eAgInSc2d1
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
Arafat	Arafat	k1gMnSc1
nabídku	nabídka	k1gFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
nevypořádávala	vypořádávat	k5eNaImAgFnS
s	s	k7c7
otázkou	otázka	k1gFnSc7
palestinských	palestinský	k2eAgMnPc2d1
uprchlíků	uprchlík	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
ztroskotání	ztroskotání	k1gNnSc6
jednání	jednání	k1gNnSc2
a	a	k8xC
poté	poté	k6eAd1
co	co	k9
Ariel	Ariel	k1gMnSc1
Šaron	Šaron	k1gMnSc1
z	z	k7c2
Likudu	Likud	k1gInSc2
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c4
Chrámovou	chrámový	k2eAgFnSc4d1
horu	hora	k1gFnSc4
vypukla	vypuknout	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
intifáda	intifáda	k1gFnSc1
al-Aksá	al-Aksat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
–	–	k?
Současnost	současnost	k1gFnSc1
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Druhá	druhý	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
<g/>
,	,	kIx,
Izraelský	izraelský	k2eAgInSc1d1
plán	plán	k1gInSc1
jednostranného	jednostranný	k2eAgNnSc2d1
stažení	stažení	k1gNnSc2
<g/>
,	,	kIx,
Izraelská	izraelský	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
a	a	k8xC
Operace	operace	k1gFnSc1
Lité	litý	k2eAgNnSc1d1
olovo	olovo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
kolem	kolem	k7c2
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
pod	pod	k7c7
správou	správa	k1gFnSc7
Izraele	Izrael	k1gInSc2
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
uzemí	uzemit	k5eAaPmIp3nS
pod	pod	k7c7
palestinskou	palestinský	k2eAgFnSc7d1
samosprávou	samospráva	k1gFnSc7
jsou	být	k5eAaImIp3nP
světlá	světlý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
tato	tento	k3xDgFnSc1
druhá	druhý	k4xOgFnSc1
byla	být	k5eAaImAgNnP
výsledkem	výsledek	k1gInSc7
dlouhodobé	dlouhodobý	k2eAgFnSc2d1
frustrace	frustrace	k1gFnSc2
palestinských	palestinský	k2eAgMnPc2d1
Arabů	Arab	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Vedou	vést	k5eAaImIp3nP
se	se	k3xPyFc4
spory	spor	k1gInPc7
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
organizované	organizovaný	k2eAgNnSc4d1
vypuknutí	vypuknutí	k1gNnSc4
nepokojů	nepokoj	k1gInPc2
<g/>
,	,	kIx,
či	či	k8xC
zda	zda	k8xS
bylo	být	k5eAaImAgNnS
samovolné	samovolný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
příčiny	příčina	k1gFnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
kromě	kromě	k7c2
neúspěchu	neúspěch	k1gInSc2
izraelsko-palestinských	izraelsko-palestinský	k2eAgNnPc2d1
vyjednávání	vyjednávání	k1gNnPc2
a	a	k8xC
cesty	cesta	k1gFnPc4
Ariela	Ariel	k1gMnSc2
Šarona	Šaron	k1gMnSc2
na	na	k7c4
Chrámovou	chrámový	k2eAgFnSc4d1
horu	hora	k1gFnSc4
řadí	řadit	k5eAaImIp3nS
také	také	k9
recese	recese	k1gFnSc1
palestinské	palestinský	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
vysoká	vysoký	k2eAgFnSc1d1
nezaměstnanost	nezaměstnanost	k1gFnSc1
<g/>
,	,	kIx,
výstavba	výstavba	k1gFnSc1
izraelských	izraelský	k2eAgFnPc2d1
osad	osada	k1gFnPc2
včetně	včetně	k7c2
výstavby	výstavba	k1gFnSc2
předměstí	předměstí	k1gNnSc2
ve	v	k7c6
východním	východní	k2eAgInSc6d1
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
během	během	k7c2
této	tento	k3xDgFnSc2
druhé	druhý	k4xOgFnSc2
intifády	intifáda	k1gFnSc2
hrála	hrát	k5eAaImAgFnS
média	médium	k1gNnPc4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
nejvíce	nejvíce	k6eAd1,k6eAd3
mediálně	mediálně	k6eAd1
známé	známý	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
této	tento	k3xDgFnSc2
intifády	intifáda	k1gFnSc2
patřilo	patřit	k5eAaImAgNnS
zabití	zabití	k1gNnSc1
dvanáctiletého	dvanáctiletý	k2eAgMnSc2d1
palestinského	palestinský	k2eAgMnSc2d1
chlapce	chlapec	k1gMnSc2
při	při	k7c6
přestřelce	přestřelka	k1gFnSc6
v	v	k7c6
Gaze	Gaze	k1gFnSc6
(	(	kIx(
<g/>
30.9	30.9	k4
<g/>
.2000	.2000	k4
<g/>
)	)	kIx)
údajně	údajně	k6eAd1
příslušníkem	příslušník	k1gMnSc7
izraelské	izraelský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zlynčování	zlynčování	k1gNnSc1
a	a	k8xC
umlácení	umlácení	k1gNnSc1
dvou	dva	k4xCgMnPc2
izraelských	izraelský	k2eAgMnPc2d1
záložníků	záložník	k1gMnPc2
v	v	k7c6
Ramalláhu	Ramalláh	k1gInSc6
(	(	kIx(
<g/>
12.10	12.10	k4
<g/>
.2000	.2000	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
konfliktu	konflikt	k1gInSc2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
nové	nový	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
boje	boj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
palestinské	palestinský	k2eAgInPc4d1
patřily	patřit	k5eAaImAgFnP
sebevražedné	sebevražedný	k2eAgInPc4d1
bombové	bombový	k2eAgInPc4d1
atentáty	atentát	k1gInPc4
na	na	k7c4
izraelské	izraelský	k2eAgMnPc4d1
občany	občan	k1gMnPc4
a	a	k8xC
po	po	k7c6
stažení	stažení	k1gNnSc6
Izraele	Izrael	k1gInSc2
z	z	k7c2
Gazy	Gaza	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
také	také	k6eAd1
raketové	raketový	k2eAgNnSc1d1
ostřelování	ostřelování	k1gNnSc1
jihoizraelských	jihoizraelský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
zahájil	zahájit	k5eAaPmAgInS
taktiku	taktika	k1gFnSc4
mimosoudní	mimosoudní	k2eAgFnSc2d1
likvidace	likvidace	k1gFnSc2
teroristů	terorista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
fakticky	fakticky	k6eAd1
znamenala	znamenat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
vybraní	vybraný	k2eAgMnPc1d1
palestinští	palestinský	k2eAgMnPc1d1
teroristé	terorista	k1gMnPc1
likvidováni	likvidován	k2eAgMnPc1d1
raketami	raketa	k1gFnPc7
vypálenými	vypálený	k2eAgFnPc7d1
z	z	k7c2
izraelských	izraelský	k2eAgInPc2d1
vrtulníků	vrtulník	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Názory	názor	k1gInPc1
na	na	k7c4
konec	konec	k1gInSc4
intifády	intifáda	k1gFnSc2
se	se	k3xPyFc4
různí	různý	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
za	za	k7c4
její	její	k3xOp3gInSc4
konec	konec	k1gInSc4
pokládají	pokládat	k5eAaImIp3nP
smrt	smrt	k1gFnSc1
Jásira	Jásir	k1gMnSc4
Arafata	Arafat	k1gMnSc4
v	v	k7c6
listopadu	listopad	k1gInSc6
2004	#num#	k4
<g/>
,	,	kIx,
jiní	jiný	k1gMnPc1
summit	summit	k1gInSc4
v	v	k7c4
Šarm	šarm	k1gInSc4
aš-Šajchu	aš-Šajch	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
představitelé	představitel	k1gMnPc1
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
Palestinců	Palestinec	k1gMnPc2
<g/>
,	,	kIx,
Egypta	Egypt	k1gInSc2
a	a	k8xC
Jordánska	Jordánsko	k1gNnSc2
vyhlásili	vyhlásit	k5eAaPmAgMnP
příměří	příměří	k1gNnSc4
<g/>
,	,	kIx,
jiní	jiný	k1gMnPc1
ji	on	k3xPp3gFnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
neukončenou	ukončený	k2eNgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
předčasné	předčasný	k2eAgFnPc1d1
premiérské	premiérský	k2eAgFnPc1d1
volby	volba	k1gFnPc1
v	v	k7c6
nichž	jenž	k3xRgInPc2
zvítězil	zvítězit	k5eAaPmAgMnS
Šaron	Šaron	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
vládu	vláda	k1gFnSc4
národní	národní	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
vytvořil	vytvořit	k5eAaPmAgMnS
pravicovou	pravicový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejnacionálnějších	nacionální	k2eAgFnPc2d3
v	v	k7c6
izraelských	izraelský	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
prosadil	prosadit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
jednostranné	jednostranný	k2eAgNnSc1d1
stažení	stažení	k1gNnSc1
z	z	k7c2
Pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozštěpení	rozštěpení	k1gNnSc3
Likudu	Likud	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
druhou	druhý	k4xOgFnSc4
intifádu	intifáda	k1gFnSc4
prosadil	prosadit	k5eAaPmAgMnS
výstavbu	výstavba	k1gFnSc4
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
bariéry	bariéra	k1gFnSc2
kolem	kolem	k7c2
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
V	v	k7c6
září	září	k1gNnSc6
2005	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
evakuaci	evakuace	k1gFnSc3
všech	všecek	k3xTgMnPc2
Izraelců	Izraelec	k1gMnPc2
a	a	k8xC
Izraelské	izraelský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
z	z	k7c2
Pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gFnSc2
a	a	k8xC
severního	severní	k2eAgInSc2d1
Samařska	Samařsk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Šaron	Šaron	k1gMnSc1
posléze	posléze	k6eAd1
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
Likudu	Likud	k1gInSc2
a	a	k8xC
založil	založit	k5eAaPmAgMnS
centristickou	centristický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Kadima	Kadima	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
územních	územní	k2eAgInPc2d1
ústupků	ústupek	k1gInPc2
rychle	rychle	k6eAd1
vyřešit	vyřešit	k5eAaPmF
izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
vyvolal	vyvolat	k5eAaPmAgInS
předčasné	předčasný	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
,	,	kIx,
však	však	k9
utrpěl	utrpět	k5eAaPmAgMnS
mozkovou	mozkový	k2eAgFnSc4d1
mrtvici	mrtvice	k1gFnSc4
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
upadl	upadnout	k5eAaPmAgMnS
do	do	k7c2
kómatu	kóma	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
neprobral	probrat	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Annapolis	Annapolis	k1gInSc1
mírová	mírový	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
vyřešit	vyřešit	k5eAaPmF
otázku	otázka	k1gFnSc4
územních	územní	k2eAgInPc2d1
sporů	spor	k1gInPc2
a	a	k8xC
palestinských	palestinský	k2eAgMnPc2d1
uprchlíků	uprchlík	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
praxi	praxe	k1gFnSc6
však	však	k9
nepřinesla	přinést	k5eNaPmAgFnS
žádné	žádný	k3yNgInPc4
hmatatelné	hmatatelný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
objevil	objevit	k5eAaPmAgInS
saúdský	saúdský	k2eAgInSc1d1
návrh	návrh	k1gInSc1
řešení	řešení	k1gNnSc2
konfliktu	konflikt	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
úplném	úplný	k2eAgInSc6d1
návratu	návrat	k1gInSc6
do	do	k7c2
hranic	hranice	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
(	(	kIx(
<g/>
tzv.	tzv.	kA
Zelená	zelený	k2eAgFnSc1d1
linie	linie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc4
by	by	kYmCp3nS
v	v	k7c6
praxi	praxe	k1gFnSc6
znamenalo	znamenat	k5eAaImAgNnS
vrácení	vrácení	k1gNnSc1
východního	východní	k2eAgInSc2d1
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Chrámové	chrámový	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
celý	celý	k2eAgInSc4d1
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
i	i	k8xC
strategicky	strategicky	k6eAd1
významné	významný	k2eAgFnPc4d1
Golanské	Golanský	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
eskalovalo	eskalovat	k5eAaImAgNnS
napětí	napětí	k1gNnSc1
v	v	k7c6
Pásmu	pásmo	k1gNnSc6
Gazy	Gaza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
Hamás	Hamása	k1gFnPc2
vypovědělo	vypovědět	k5eAaPmAgNnS
předchozí	předchozí	k2eAgNnSc4d1
dvouměsíční	dvouměsíční	k2eAgNnSc4d1
příměří	příměří	k1gNnSc4
a	a	k8xC
obnovilo	obnovit	k5eAaPmAgNnS
ostřelování	ostřelování	k1gNnSc4
izraelských	izraelský	k2eAgNnPc2d1
měst	město	k1gNnPc2
raketami	raketa	k1gFnPc7
Kásam	Kásam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
reagoval	reagovat	k5eAaBmAgInS
leteckými	letecký	k2eAgInPc7d1
útoky	útok	k1gInPc7
a	a	k8xC
pozemní	pozemní	k2eAgFnSc7d1
invazí	invaze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Kadimou	Kadimat	k5eAaPmIp3nP
vedená	vedený	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
předčasné	předčasný	k2eAgFnPc4d1
volby	volba	k1gFnPc4
do	do	k7c2
Knesetu	Kneset	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novou	nový	k2eAgFnSc4d1
izraelskou	izraelský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
sestavil	sestavit	k5eAaPmAgMnS
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
Likud	Likud	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
Netanjahu	Netanjaha	k1gFnSc4
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2021	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzájemným	vzájemný	k2eAgInPc3d1
raketovým	raketový	k2eAgInPc3d1
útokům	útok	k1gInPc3
mezi	mezi	k7c7
Pásmem	pásmo	k1gNnSc7
Gazy	Gaza	k1gFnSc2
a	a	k8xC
Izraelem	Izrael	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
NEWMAN	NEWMAN	kA
<g/>
,	,	kIx,
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akov	akov	k1gMnSc1
<g/>
;	;	kIx,
SIVAN	SIVAN	kA
<g/>
,	,	kIx,
Gavri	Gavri	k1gNnSc1
<g/>
'	'	kIx"
<g/>
el.	el.	k?
Judaismus	judaismus	k1gInSc1
od	od	k7c2
A	A	kA
do	do	k7c2
Z.	Z.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sefer	Sefer	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
900895	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
78	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ČEJKA	Čejka	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87029	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
36	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
–	–	k?
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
↑	↑	k?
GILBERT	Gilbert	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
Art	Art	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7257	#num#	k4
<g/>
-	-	kIx~
<g/>
740	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
54	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
50	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
152	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pardubice	Pardubice	k1gInPc4
<g/>
:	:	kIx,
Kora	Kora	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901092	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
83	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
↑	↑	k?
BLAUSTEIN	BLAUSTEIN	kA
<g/>
,	,	kIx,
Max	max	kA
<g/>
.	.	kIx.
Židovská	židovský	k2eAgFnSc1d1
historie	historie	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Britské	britský	k2eAgNnSc1d1
mandátní	mandátní	k2eAgNnSc1d1
území	území	k1gNnSc1
Palestina	Palestina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eretz	Eretza	k1gFnPc2
<g/>
,	,	kIx,
2006-01-18	2006-01-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sachar	Sachar	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
254	#num#	k4
<g/>
-	-	kIx~
<g/>
266	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ilan	Ilan	k1gNnSc4
Pappé	Pappý	k2eAgFnSc2d1
<g/>
:	:	kIx,
The	The	k1gFnPc2
Ethnic	Ethnice	k1gFnPc2
Cleansing	Cleansing	k1gInSc1
of	of	k?
Palestine	Palestin	k1gInSc5
(	(	kIx(
<g/>
London	London	k1gMnSc1
and	and	k?
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Oneworld	Oneworld	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85168	#num#	k4
<g/>
-	-	kIx~
<g/>
467	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
↑	↑	k?
Gilbert	Gilbert	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
168	#num#	k4
<g/>
-	-	kIx~
<g/>
192	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sachar	Sachar	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
267	#num#	k4
<g/>
-	-	kIx~
<g/>
271	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
103	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
83	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
</s>
<s>
General	Generat	k5eAaImAgInS,k5eAaPmAgInS
Progress	Progress	k1gInSc1
Report	report	k1gInSc1
and	and	k?
Supplementary	Supplementar	k1gInPc1
Report	report	k1gInSc1
of	of	k?
the	the	k?
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Conciliation	Conciliation	k1gInSc4
Commission	Commission	k1gInSc4
for	forum	k1gNnPc2
Palestine	Palestin	k1gInSc5
<g/>
,	,	kIx,
Covering	Covering	k1gInSc1
the	the	k?
Period	perioda	k1gFnPc2
from	from	k6eAd1
11	#num#	k4
December	Decembra	k1gFnPc2
1949	#num#	k4
to	ten	k3xDgNnSc1
23	#num#	k4
October	October	k1gInSc1
1950	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
United	United	k1gInSc4
Nations	Nations	k1gInSc1
Conciliation	Conciliation	k1gInSc1
Commission	Commission	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Židovská	židovský	k2eAgFnSc1d1
historie	historie	k1gFnSc1
<g/>
:	:	kIx,
Stát	stát	k1gInSc1
Izrael	Izrael	k1gInSc1
1948	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eretz	Eretz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2006-02-28	2006-02-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
132	#num#	k4
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
99	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
349	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902484	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
469	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
105	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SCHNEIDER	Schneider	k1gMnSc1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
:	:	kIx,
ohnisko	ohnisko	k1gNnSc1
dění	dění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
A-alef	A-alef	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85237	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sachar	Sachar	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
470	#num#	k4
<g/>
-	-	kIx~
<g/>
511	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
120	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
122	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KRUPP	KRUPP	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sionismus	sionismus	k1gInSc4
a	a	k8xC
Stát	stát	k1gInSc1
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
265	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
146	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Sionismus	sionismus	k1gInSc1
a	a	k8xC
Stát	stát	k1gInSc1
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
142	#num#	k4
<g/>
-	-	kIx~
<g/>
143.1	143.1	k4
2	#num#	k4
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
413	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
111.1	111.1	k4
2	#num#	k4
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
457.1	457.1	k4
2	#num#	k4
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
622	#num#	k4
<g/>
-	-	kIx~
<g/>
623	#num#	k4
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
155	#num#	k4
<g/>
↑	↑	k?
BLACK	BLACK	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
;	;	kIx,
MORRIS	MORRIS	kA
<g/>
,	,	kIx,
Benny	Benn	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mossad	Mossad	k1gInSc1
<g/>
,	,	kIx,
izraelské	izraelský	k2eAgFnSc2d1
tajné	tajný	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
392	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
381	#num#	k4
<g/>
-	-	kIx~
<g/>
382	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sionismus	sionismus	k1gInSc1
a	a	k8xC
Stát	stát	k1gInSc1
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
160	#num#	k4
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
173	#num#	k4
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
512.1	512.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
169-1701	169-1701	k4
2	#num#	k4
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
534	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
535	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
187	#num#	k4
<g/>
-	-	kIx~
<g/>
189.1	189.1	k4
2	#num#	k4
3	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
195	#num#	k4
<g/>
-	-	kIx~
<g/>
196	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
190.1	190.1	k4
2	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
205.1	205.1	k4
2	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
209	#num#	k4
<g/>
-	-	kIx~
<g/>
210.1	210.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
213	#num#	k4
<g/>
-	-	kIx~
<g/>
214.1	214.1	k4
2	#num#	k4
3	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
215	#num#	k4
<g/>
-	-	kIx~
<g/>
216	#num#	k4
<g/>
↑	↑	k?
srov.	srov.	kA
rekonstrukci	rekonstrukce	k1gFnSc4
případu	případ	k1gInSc2
v	v	k7c6
článku	článek	k1gInSc6
<g/>
:	:	kIx,
Exchange	Exchange	k1gNnSc1
of	of	k?
fire	fir	k1gInSc2
at	at	k?
the	the	k?
Netzarim	Netzarim	k1gInSc1
junction	junction	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Israel	Israel	k1gInSc1
Ministry	ministr	k1gMnPc4
of	of	k?
Foreign	Foreign	k1gInSc4
Affairs	Affairsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lynch	Lynch	k1gInSc1
mob	mob	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
brutal	brutal	k1gMnSc1
attack	attack	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
216	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
242.1	242.1	k4
2	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
243	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
239	#num#	k4
<g/>
-	-	kIx~
<g/>
240.1	240.1	k4
2	#num#	k4
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
308	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Obama	Obama	k?
podporuje	podporovat	k5eAaImIp3nS
saúdský	saúdský	k2eAgInSc1d1
plán	plán	k1gInSc1
rozdělit	rozdělit	k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eretz	Eretz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2008-11-17	2008-11-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Úvodní	úvodní	k2eAgInSc4d1
den	den	k1gInSc4
summitu	summit	k1gInSc2
v	v	k7c6
Annapolis	Annapolis	k1gFnSc6
<g/>
:	:	kIx,
Společné	společný	k2eAgNnSc1d1
prohlášení	prohlášení	k1gNnSc1
<g/>
,	,	kIx,
projevy	projev	k1gInPc1
Olmerta	Olmert	k1gMnSc2
a	a	k8xC
Abbáse	Abbás	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eretz	Eretz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2007-11-28	2007-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
oficiální	oficiální	k2eAgInSc1d1
<g/>
:	:	kIx,
Nové	Nové	k2eAgFnPc1d1
izraelské	izraelský	k2eAgFnPc1d1
volby	volba	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eretz	Eretz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2008-10-27	2008-10-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
Izraeli	Izrael	k1gInSc6
a	a	k8xC
Gaze	Gaze	k1gFnSc3
se	se	k3xPyFc4
roztočila	roztočit	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
spirála	spirála	k1gFnSc1
násilí	násilí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
stoupá	stoupat	k5eAaImIp3nS
<g/>
,	,	kIx,
k	k	k7c3
další	další	k2eAgFnSc3d1
eskalaci	eskalace	k1gFnSc3
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
‚	‚	k?
<g/>
jediná	jediný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
<g/>
‘	‘	k?
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
ČEJKA	Čejka	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
–	–	k?
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
321	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87029	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GILBERT	Gilbert	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
Art	Art	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
668	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7257	#num#	k4
<g/>
-	-	kIx~
<g/>
740	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CHAPMAN	CHAPMAN	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čí	čí	k3xOyRgNnSc1,k3xOyQgNnSc1
je	být	k5eAaImIp3nS
Země	země	k1gFnSc1
zaslíbená	zaslíbený	k2eAgFnSc1d1
–	–	k?
Pokračující	pokračující	k2eAgFnSc1d1
krize	krize	k1gFnSc1
mezi	mezi	k7c7
Izraelem	Izrael	k1gInSc7
a	a	k8xC
Palestinci	Palestinec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Volvox	Volvox	k1gInSc1
Globator	Globator	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
336	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7207	#num#	k4
<g/>
-	-	kIx~
<g/>
507	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KRUPP	KRUPP	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sionismus	sionismus	k1gInSc4
a	a	k8xC
Stát	stát	k1gInSc1
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
242	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
265	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SCHNEIDER	Schneider	k1gMnSc1
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalém	Jeruzalém	k1gInSc1
–	–	k?
ohnisko	ohnisko	k1gNnSc1
dění	dění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
A-Alef	A-Alef	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
91	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85237	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Palestina	Palestina	k1gFnSc1
</s>
<s>
Politika	politika	k1gFnSc1
osadnictví	osadnictví	k1gNnSc2
</s>
<s>
Sionismus	sionismus	k1gInSc1
</s>
<s>
Židovský	židovský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
intifáda	intifáda	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc2
–	–	k?
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
těžce	těžce	k6eAd1
dopadá	dopadat	k5eAaImIp3nS
na	na	k7c4
palestinské	palestinský	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
Archivováno	archivován	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Barrister	Barrister	k1gInSc1
–	–	k?
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
a	a	k8xC
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Global	globat	k5eAaImAgInS
Politics	Politics	k1gInSc1
–	–	k?
Současný	současný	k2eAgInSc1d1
izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
–	–	k?
pohled	pohled	k1gInSc1
zevnitř	zevnitř	k6eAd1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
–	–	k?
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
(	(	kIx(
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
