<s>
K	k	k7c3
přílivu	příliv	k1gInSc3
obyvatel	obyvatel	k1gMnPc2
docházelo	docházet	k5eAaImAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
alijí	alijí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nově	nově	k6eAd1
příchozí	příchozí	k1gMnPc1
byli	být	k5eAaImAgMnP
k	k	k7c3
příchodu	příchod	k1gInSc3
různě	různě	k6eAd1
motivováni	motivovat	k5eAaBmNgMnP
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
důvody	důvod	k1gInPc4
náboženské	náboženský	k2eAgInPc4d1
<g/>
,	,	kIx,
či	či	k8xC
politické	politický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
židovského	židovský	k2eAgNnSc2d1
přistěhovalectví	přistěhovalectví	k1gNnSc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
arabským	arabský	k2eAgInPc3d1
nepokojům	nepokoj	k1gInPc3
<g/>
.	.	kIx.
</s>