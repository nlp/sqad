<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
olim	olima	k1gFnPc2
(	(	kIx(
<g/>
imigrantů	imigrant	k1gMnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
alijí	alije	k1gFnPc2
<g/>
)	)	kIx)
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
protižidovských	protižidovský	k2eAgInPc2d1
pogromů	pogrom	k1gInPc2
v	v	k7c6
carském	carský	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
a	a	k8xC
později	pozdě	k6eAd2
kvůli	kvůli	k7c3
vzestupu	vzestup	k1gInSc3
národního	národní	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>