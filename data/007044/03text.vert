<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
větší	veliký	k2eAgInSc4d2	veliký
průtok	průtok	k1gInSc4	průtok
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
nebo	nebo	k8xC	nebo
rozlohu	rozloha	k1gFnSc4	rozloha
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
"	"	kIx"	"
<g/>
V	V	kA	V
<g/>
"	"	kIx"	"
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
usazenin	usazenina	k1gFnPc2	usazenina
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
eroze	eroze	k1gFnSc1	eroze
i	i	k8xC	i
sedimentace	sedimentace	k1gFnSc1	sedimentace
<g/>
,	,	kIx,	,
říční	říční	k2eAgNnPc4d1	říční
údolí	údolí	k1gNnPc4	údolí
je	být	k5eAaImIp3nS	být
plošší	plochý	k2eAgNnSc1d2	plošší
a	a	k8xC	a
s	s	k7c7	s
již	již	k6eAd1	již
významným	významný	k2eAgInSc7d1	významný
podílem	podíl	k1gInSc7	podíl
usazenin	usazenina	k1gFnPc2	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
toku	tok	k1gInSc2	tok
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
písmena	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
sedimentace	sedimentace	k1gFnSc2	sedimentace
–	–	k?	–
údolí	údolí	k1gNnSc6	údolí
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
ploché	plochý	k2eAgNnSc1d1	ploché
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
masivní	masivní	k2eAgFnSc3d1	masivní
sedimentaci	sedimentace	k1gFnSc3	sedimentace
vznikají	vznikat	k5eAaImIp3nP	vznikat
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
říční	říční	k2eAgFnSc2d1	říční
nivy	niva	k1gFnSc2	niva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
řek	řeka	k1gFnPc2	řeka
stáletekoucích	stáletekoucí	k2eAgFnPc2d1	stáletekoucí
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
i	i	k9	i
řeky	řeka	k1gFnPc4	řeka
občasně	občasně	k6eAd1	občasně
tekoucí	tekoucí	k2eAgFnPc4d1	tekoucí
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
bystřina	bystřina	k1gFnSc1	bystřina
<g/>
,	,	kIx,	,
potok	potok	k1gInSc1	potok
a	a	k8xC	a
říčka	říčka	k1gFnSc1	říčka
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
500	[number]	k4	500
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
alespoň	alespoň	k9	alespoň
100	[number]	k4	100
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
veletok	veletok	k1gInSc1	veletok
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	Řek	k1gMnPc4	Řek
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
toku	tok	k1gInSc2	tok
i	i	k8xC	i
podzemím	podzemí	k1gNnSc7	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
mizí	mizet	k5eAaImIp3nS	mizet
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
časté	častý	k2eAgNnSc1d1	časté
v	v	k7c6	v
krasových	krasový	k2eAgFnPc6d1	krasová
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
výskytem	výskyt	k1gInSc7	výskyt
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ponor	ponor	k1gInSc1	ponor
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
opětovného	opětovný	k2eAgNnSc2d1	opětovné
vynoření	vynoření	k1gNnSc2	vynoření
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
vyvěračku	vyvěračka	k1gFnSc4	vyvěračka
<g/>
,	,	kIx,	,
podzemní	podzemní	k2eAgFnPc1d1	podzemní
části	část	k1gFnPc1	část
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
punkva	punkva	k6eAd1	punkva
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
ve	v	k7c6	v
vodních	vodní	k2eAgInPc6d1	vodní
tocích	tok	k1gInPc6	tok
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tajícího	tající	k2eAgInSc2d1	tající
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vsákla	vsáknout	k5eAaPmAgFnS	vsáknout
do	do	k7c2	do
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
znovu	znovu	k6eAd1	znovu
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nazývaném	nazývaný	k2eAgNnSc6d1	nazývané
pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
řeky	řeka	k1gFnPc1	řeka
pramen	pramen	k1gInSc1	pramen
nemají	mít	k5eNaImIp3nP	mít
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
bažinách	bažina	k1gFnPc6	bažina
nebo	nebo	k8xC	nebo
tajícím	tající	k2eAgInSc6d1	tající
ledovci	ledovec	k1gInSc6	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
nebo	nebo	k8xC	nebo
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ústí	ústí	k1gNnSc1	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
před	před	k7c7	před
ústím	ústí	k1gNnSc7	ústí
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
ohraničené	ohraničený	k2eAgFnSc2d1	ohraničená
těmito	tento	k3xDgFnPc7	tento
větvemi	větev	k1gFnPc7	větev
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
vlévá	vlévat	k5eAaImIp3nS	vlévat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
shora	shora	k6eAd1	shora
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
přibližně	přibližně	k6eAd1	přibližně
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
tvar	tvar	k1gInSc1	tvar
jako	jako	k8xS	jako
řecké	řecký	k2eAgNnSc1d1	řecké
písmeno	písmeno	k1gNnSc1	písmeno
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větvení	větvení	k1gNnSc1	větvení
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
vytváření	vytváření	k1gNnSc1	vytváření
říčních	říční	k2eAgNnPc2d1	říční
ramen	rameno	k1gNnPc2	rameno
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
divočení	divočení	k1gNnSc1	divočení
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
toky	tok	k1gInPc4	tok
nastálo	nastálo	k6eAd1	nastálo
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
bifurkace	bifurkace	k1gFnSc1	bifurkace
(	(	kIx(	(
<g/>
geografie	geografie	k1gFnSc1	geografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
řeky	řeka	k1gFnPc1	řeka
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
<g/>
)	)	kIx)	)
setkávají	setkávat	k5eAaImIp3nP	setkávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
soutok	soutok	k1gInSc4	soutok
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
toku	tok	k1gInSc2	tok
za	za	k7c7	za
soutokem	soutok	k1gInSc7	soutok
dvou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
po	po	k7c4	po
delší	dlouhý	k2eAgFnPc4d2	delší
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgFnPc4d2	veliký
z	z	k7c2	z
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
zcela	zcela	k6eAd1	zcela
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnPc1d1	původní
toky	toka	k1gFnPc1	toka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
zdrojnice	zdrojnice	k1gFnPc1	zdrojnice
<g/>
;	;	kIx,	;
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vzniká	vznikat	k5eAaImIp3nS	vznikat
například	například	k6eAd1	například
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
tok	tok	k1gInSc1	tok
za	za	k7c7	za
soutokem	soutok	k1gInSc7	soutok
nést	nést	k5eAaImF	nést
jméno	jméno	k1gNnSc4	jméno
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
menší	malý	k2eAgFnPc1d2	menší
řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Labe	Labe	k1gNnSc2	Labe
po	po	k7c6	po
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
menší	malý	k2eAgFnSc2d2	menší
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prohrála	prohrát	k5eAaPmAgFnS	prohrát
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
souboj	souboj	k1gInSc4	souboj
<g/>
"	"	kIx"	"
o	o	k7c4	o
jméno	jméno	k1gNnSc4	jméno
další	další	k2eAgFnSc2d1	další
části	část	k1gFnSc2	část
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
rovněž	rovněž	k9	rovněž
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
ústí	ústí	k1gNnSc6	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
podle	podle	k7c2	podle
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
řeka	řeka	k1gFnSc1	řeka
I.	I.	kA	I.
kategorie	kategorie	k1gFnSc2	kategorie
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
takovéto	takovýto	k3xDgFnPc1	takovýto
řeky	řeka	k1gFnPc1	řeka
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
–	–	k?	–
Labe	Labe	k1gNnSc1	Labe
a	a	k8xC	a
Odra	Odra	k1gFnSc1	Odra
<g/>
)	)	kIx)	)
řeka	řeka	k1gFnSc1	řeka
II	II	kA	II
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc2	kategorie
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g />
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
kategorie	kategorie	k1gFnSc1	kategorie
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
např.	např.	kA	např.
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Ostravice	Ostravice	k1gFnSc1	Ostravice
<g/>
)	)	kIx)	)
řeka	řeka	k1gFnSc1	řeka
III	III	kA	III
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc2	kategorie
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
II	II	kA	II
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc2	kategorie
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
např.	např.	kA	např.
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
,	,	kIx,	,
Dyje	Dyje	k1gFnSc1	Dyje
<g/>
)	)	kIx)	)
řeka	řeka	k1gFnSc1	řeka
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc1	kategorie
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
III	III	kA	III
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc2	kategorie
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
např.	např.	kA	např.
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
,	,	kIx,	,
Úslava	Úslava	k1gFnSc1	Úslava
<g/>
)	)	kIx)	)
pátá	pátý	k4xOgFnSc1	pátý
kategorie	kategorie	k1gFnSc1	kategorie
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nezavádí	zavádět	k5eNaImIp3nS	zavádět
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
i	i	k9	i
takové	takový	k3xDgFnPc1	takový
řeky	řeka	k1gFnPc1	řeka
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
(	(	kIx(	(
<g/>
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
<g/>
)	)	kIx)	)
průměrný	průměrný	k2eAgInSc4d1	průměrný
nebo	nebo	k8xC	nebo
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
průtok	průtok	k1gInSc4	průtok
plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tabulce	tabulka	k1gFnSc6	tabulka
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
5	[number]	k4	5
největších	veliký	k2eAgFnPc2d3	veliký
řek	řeka	k1gFnPc2	řeka
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
průměrného	průměrný	k2eAgInSc2d1	průměrný
průtoku	průtok	k1gInSc2	průtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tabulce	tabulka	k1gFnSc6	tabulka
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
6	[number]	k4	6
největších	veliký	k2eAgFnPc2d3	veliký
řek	řeka	k1gFnPc2	řeka
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tabulce	tabulka	k1gFnSc6	tabulka
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
10	[number]	k4	10
největších	veliký	k2eAgFnPc2d3	veliký
řek	řeka	k1gFnPc2	řeka
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc1	nějaký
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
řeka	řeka	k1gFnSc1	řeka
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
ústí	ústit	k5eAaImIp3nS	ústit
tohoto	tento	k3xDgInSc2	tento
přítoku	přítok	k1gInSc2	přítok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
počítána	počítán	k2eAgFnSc1d1	počítána
délka	délka	k1gFnSc1	délka
přítoku	přítok	k1gInSc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
toku	tok	k1gInSc2	tok
Koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
Meandr	meandr	k1gInSc1	meandr
Odtok	odtok	k1gInSc1	odtok
Povodeň	povodeň	k1gFnSc1	povodeň
Povodí	povodit	k5eAaPmIp3nS	povodit
Průtok	průtok	k1gInSc1	průtok
Údolní	údolní	k2eAgFnSc1d1	údolní
niva	niva	k1gFnSc1	niva
Říční	říční	k2eAgFnSc1d1	říční
kilometr	kilometr	k1gInSc4	kilometr
Říční	říční	k2eAgNnPc4d1	říční
údolí	údolí	k1gNnPc4	údolí
Vádí	vádí	k1gNnSc2	vádí
Zdrojnice	zdrojnice	k1gFnSc2	zdrojnice
Zvodeň	Zvodeň	k1gFnSc1	Zvodeň
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc2	téma
Řeka	Řek	k1gMnSc2	Řek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Filmový	filmový	k2eAgInSc1d1	filmový
dokument	dokument	k1gInSc1	dokument
Řeka	Řek	k1gMnSc2	Řek
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
času	čas	k1gInSc2	čas
–	–	k?	–
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Ohři	Ohře	k1gFnSc4	Ohře
provází	provázet	k5eAaImIp3nS	provázet
Václav	Václav	k1gMnSc1	Václav
Cílek	Cílek	k1gMnSc1	Cílek
–	–	k?	–
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
ČT	ČT	kA	ČT
</s>
