<s desamb="1">
Hrobka	hrobka	k1gFnSc1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
88,66	88,66	k4
m	m	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
v	v	k7c6
ní	on	k3xPp3gFnSc6
tři	tři	k4xCgFnPc1
pozvolně	pozvolně	k6eAd1
klesající	klesající	k2eAgFnPc1d1
chodby	chodba	k1gFnPc1
(	(	kIx(
<g/>
označené	označený	k2eAgInPc1d1
B	B	kA
<g/>
,	,	kIx,
C	C	kA
a	a	k8xC
D	D	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozšířená	rozšířený	k2eAgFnSc1d1
komora	komora	k1gFnSc1
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pohřební	pohřební	k2eAgFnSc1d1
komora	komora	k1gFnSc1
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
a	a	k8xC
úzká	úzký	k2eAgFnSc1d1
chodba	chodba	k1gFnSc1
(	(	kIx(
<g/>
K	K	kA
<g/>
)	)	kIx)
obklopená	obklopený	k2eAgFnSc1d1
třemi	tři	k4xCgFnPc7
komorami	komora	k1gFnPc7
(	(	kIx(
<g/>
Ka	Ka	k1gFnSc7
<g/>
,	,	kIx,
Kb	kb	kA
a	a	k8xC
Kc	Kc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>