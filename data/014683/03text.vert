<s>
KV2	KV2	k4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
egyptské	egyptský	k2eAgFnSc6d1
hrobce	hrobka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tanku	tank	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
KV-	KV-	k1gFnSc2
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stěna	stěna	k1gFnSc1
hrobky	hrobka	k1gFnSc2
KV2	KV2	k1gFnSc2
</s>
<s>
KV2	KV2	k4
je	být	k5eAaImIp3nS
egyptská	egyptský	k2eAgFnSc1d1
hrobka	hrobka	k1gFnSc1
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
Údolí	údolí	k1gNnSc6
králů	král	k1gMnPc2
mezi	mezi	k7c7
hrobkami	hrobka	k1gFnPc7
KV1	KV1	k1gFnSc1
a	a	k8xC
KV	KV	kA
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
pochován	pochovat	k5eAaPmNgInS
faraon	faraon	k1gInSc4
Ramesse	Ramess	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
nástěnných	nástěnný	k2eAgFnPc2d1
maleb	malba	k1gFnPc2
a	a	k8xC
hieroglyfů	hieroglyf	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
plány	plán	k1gInPc1
hrobky	hrobka	k1gFnSc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
je	být	k5eAaImIp3nS
nakreslen	nakreslit	k5eAaPmNgMnS
v	v	k7c6
měřítku	měřítko	k1gNnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
na	na	k7c6
papyru	papyr	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
vystaven	vystavit	k5eAaPmNgMnS
v	v	k7c6
egyptském	egyptský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Turíně	Turín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgMnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
vápenci	vápenec	k1gInSc6
nedaleko	daleko	k6eNd1
od	od	k7c2
vstupu	vstup	k1gInSc2
do	do	k7c2
hrobky	hrobka	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
plán	plán	k1gInSc1
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
stavbou	stavba	k1gFnSc7
hrobky	hrobka	k1gFnSc2
se	se	k3xPyFc4
spěchalo	spěchat	k5eAaImAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
faraon	faraon	k1gMnSc1
Ramesse	Ramesse	k1gFnSc2
IV	IV	kA
<g/>
.	.	kIx.
usedl	usednout	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
až	až	k9
v	v	k7c6
pozdním	pozdní	k2eAgInSc6d1
věku	věk	k1gInSc6
a	a	k8xC
očekával	očekávat	k5eAaImAgInS
brzy	brzy	k6eAd1
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
vzhled	vzhled	k1gInSc4
hrobky	hrobka	k1gFnSc2
jednodušší	jednoduchý	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrobka	hrobka	k1gFnSc1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
88,66	88,66	k4
m	m	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
v	v	k7c6
ní	on	k3xPp3gFnSc6
tři	tři	k4xCgFnPc1
pozvolně	pozvolně	k6eAd1
klesající	klesající	k2eAgFnPc1d1
chodby	chodba	k1gFnPc1
(	(	kIx(
<g/>
označené	označený	k2eAgInPc1d1
B	B	kA
<g/>
,	,	kIx,
C	C	kA
a	a	k8xC
D	D	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozšířená	rozšířený	k2eAgFnSc1d1
komora	komora	k1gFnSc1
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pohřební	pohřební	k2eAgFnSc1d1
komora	komora	k1gFnSc1
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
a	a	k8xC
úzká	úzký	k2eAgFnSc1d1
chodba	chodba	k1gFnSc1
(	(	kIx(
<g/>
K	K	kA
<g/>
)	)	kIx)
obklopená	obklopený	k2eAgFnSc1d1
třemi	tři	k4xCgFnPc7
komorami	komora	k1gFnPc7
(	(	kIx(
<g/>
Ka	Ka	k1gFnSc7
<g/>
,	,	kIx,
Kb	kb	kA
a	a	k8xC
Kc	Kc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hrobka	hrobka	k1gFnSc1
je	být	k5eAaImIp3nS
většinou	většina	k1gFnSc7
neporušená	porušený	k2eNgFnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
zdobena	zdobit	k5eAaImNgFnS
výjevy	výjev	k1gInPc7
z	z	k7c2
knih	kniha	k1gFnPc2
Litanie	litanie	k1gFnSc2
na	na	k7c6
Rea	Rea	k1gFnSc1
<g/>
,	,	kIx,
Egyptská	egyptský	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
Amduat	Amduat	k1gInSc4
a	a	k8xC
dalších	další	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
největší	veliký	k2eAgFnSc1d3
v	v	k7c6
počtu	počet	k1gInSc6
nástěnných	nástěnný	k2eAgFnPc2d1
maleb	malba	k1gFnPc2
v	v	k7c4
Údolí	údolí	k1gNnSc4
králů	král	k1gMnPc2
(	(	kIx(
<g/>
po	po	k7c6
KV	KV	kA
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
KV2	KV2	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
hrobek	hrobka	k1gFnPc2
v	v	k7c6
Údolí	údolí	k1gNnSc6
králů	král	k1gMnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hrobky	hrobka	k1gFnPc1
v	v	k7c6
Údolí	údolí	k1gNnSc6
králů	král	k1gMnPc2
jižní	jižní	k2eAgFnSc2d1
údolí	údolí	k1gNnPc1
</s>
<s>
KV1	KV1	k4
•	•	k?
KV2	KV2	k1gMnSc1
•	•	k?
KV3	KV3	k1gMnSc1
•	•	k?
KV4	KV4	k1gMnSc1
•	•	k?
KV5	KV5	k1gMnSc1
•	•	k?
KV6	KV6	k1gMnSc1
•	•	k?
KV7	KV7	k1gMnSc1
•	•	k?
KV8	KV8	k1gMnSc1
•	•	k?
KV9	KV9	k1gMnSc1
•	•	k?
KV10	KV10	k1gMnSc1
•	•	k?
KV11	KV11	k1gMnSc1
•	•	k?
KV12	KV12	k1gMnSc1
•	•	k?
KV13	KV13	k1gMnSc1
•	•	k?
KV14	KV14	k1gMnSc1
•	•	k?
KV15	KV15	k1gMnSc1
•	•	k?
KV16	KV16	k1gMnSc1
•	•	k?
KV17	KV17	k1gMnSc1
•	•	k?
KV18	KV18	k1gMnSc1
•	•	k?
KV19	KV19	k1gMnSc1
•	•	k?
KV20	KV20	k1gMnSc1
•	•	k?
KV21	KV21	k1gMnSc1
•	•	k?
KV26	KV26	k1gMnSc1
•	•	k?
KV27	KV27	k1gMnSc1
•	•	k?
KV28	KV28	k1gMnSc1
•	•	k?
KV29	KV29	k1gMnSc1
•	•	k?
KV30	KV30	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
KV31	KV31	k1gMnSc1
•	•	k?
KV32	KV32	k1gMnSc1
•	•	k?
KV33	KV33	k1gMnSc1
•	•	k?
KV34	KV34	k1gMnSc1
•	•	k?
KV35	KV35	k1gMnSc1
•	•	k?
KV36	KV36	k1gMnSc1
•	•	k?
KV37	KV37	k1gMnSc1
•	•	k?
KV38	KV38	k1gMnSc1
•	•	k?
KV39	KV39	k1gMnSc1
•	•	k?
KV40	KV40	k1gMnSc1
•	•	k?
KV41	KV41	k1gMnSc1
•	•	k?
KV42	KV42	k1gMnSc1
•	•	k?
KV43	KV43	k1gMnSc1
•	•	k?
KV44	KV44	k1gMnSc1
•	•	k?
KV45	KV45	k1gMnSc1
•	•	k?
KV46	KV46	k1gMnSc1
•	•	k?
KV47	KV47	k1gMnSc1
•	•	k?
KV48	KV48	k1gMnSc1
•	•	k?
KV49	KV49	k1gMnSc1
•	•	k?
KV50	KV50	k1gMnSc1
•	•	k?
KV51	KV51	k1gMnSc1
•	•	k?
KV52	KV52	k1gMnSc1
•	•	k?
KV53	KV53	k1gMnSc1
•	•	k?
KV54	KV54	k1gMnSc1
•	•	k?
KV55	KV55	k1gMnSc1
•	•	k?
KV56	KV56	k1gMnSc1
•	•	k?
KV57	KV57	k1gMnSc1
•	•	k?
KV58	KV58	k1gMnSc1
•	•	k?
KV59	KV59	k1gMnSc1
•	•	k?
KV60	KV60	k1gMnSc1
•	•	k?
KV61	KV61	k1gMnSc1
•	•	k?
KV62	KV62	k1gMnSc1
•	•	k?
KV63	KV63	k1gMnSc1
•	•	k?
KV64	KV64	k1gMnSc1
•	•	k?
KV65	KV65	k1gMnSc1
severní	severní	k2eAgMnSc1d1
údolí	údolí	k1gNnSc2
</s>
<s>
WV22	WV22	k4
•	•	k?
WV23	WV23	k1gMnSc1
•	•	k?
WV24	WV24	k1gMnSc1
•	•	k?
WV25	WV25	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověký	starověký	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
</s>
