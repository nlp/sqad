<s>
Démaratos	Démaratos	k1gMnSc1
</s>
<s>
Démaratos	Démaratos	k1gMnSc1
</s>
<s>
král	král	k1gMnSc1
Sparty	Sparta	k1gFnSc2
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
515	#num#	k4
<g/>
–	–	k?
<g/>
491	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Aristón	Aristón	k1gMnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Leótychidas	Leótychidas	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Perkalus	Perkalus	k1gMnSc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Eurypontovci	Eurypontovec	k1gMnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Aristón	Aristón	k1gMnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Démaratos	Démaratos	k1gMnSc1
nebo	nebo	k8xC
Démarátos	Démarátos	k1gMnSc1
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Δ	Δ	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
spartský	spartský	k2eAgMnSc1d1
král	král	k1gMnSc1
z	z	k7c2
královské	královský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
Eurypontovců	Eurypontovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládl	vládnout	k5eAaImAgMnS
přibližně	přibližně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
515	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
do	do	k7c2
roku	rok	k1gInSc2
491	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
spolukrálem	spolukrál	k1gMnSc7
z	z	k7c2
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Agiovců	Agiovci	k1gMnPc2
byl	být	k5eAaImAgMnS
Kleomenes	Kleomenes	k1gMnSc1
I.	I.	k4
</s>
<s>
Démaratos	Démaratos	k1gMnSc1
byl	být	k5eAaImAgMnS
následníkem	následník	k1gMnSc7
(	(	kIx(
<g/>
možná	možná	k9
i	i	k9
synem	syn	k1gMnSc7
<g/>
)	)	kIx)
krále	král	k1gMnSc2
Aristona	Ariston	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
po	po	k7c6
dvou	dva	k4xCgNnPc6
manželstvích	manželství	k1gNnPc6
bezdětný	bezdětný	k2eAgMnSc1d1
a	a	k8xC
syn	syn	k1gMnSc1
Démaratos	Démaratosa	k1gFnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
narodil	narodit	k5eAaPmAgInS
až	až	k9
třetím	třetí	k4xOgInSc6
manželství	manželství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ariston	ariston	k1gInSc1
měl	mít	k5eAaImAgInS
však	však	k9
zpočátku	zpočátku	k6eAd1
pochybnosti	pochybnost	k1gFnPc4
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
dříve	dříve	k6eAd2
než	než	k8xS
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
ho	on	k3xPp3gMnSc4
uznal	uznat	k5eAaPmAgMnS
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panování	panování	k1gNnSc1
Demarata	Demarata	k1gFnSc1
bylo	být	k5eAaImAgNnS
z	z	k7c2
části	část	k1gFnSc2
možná	možná	k9
i	i	k9
proto	proto	k8xC
poznamenány	poznamenán	k2eAgFnPc1d1
neshodami	neshoda	k1gFnPc7
s	s	k7c7
jeho	jeho	k3xOp3gMnSc7
spolukrálem	spolukrál	k1gMnSc7
Kleomenem	Kleomen	k1gMnSc7
I.	I.	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
514	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
Athénách	Athéna	k1gFnPc6
zavraždili	zavraždit	k5eAaPmAgMnP
nenáviděného	nenáviděný	k2eAgMnSc4d1
tyrana	tyran	k1gMnSc4
Hipparcha	Hipparch	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
zaujal	zaujmout	k5eAaPmAgInS
Hippias	Hippias	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
však	však	k9
nevedl	vést	k5eNaImAgInS
o	o	k7c4
nic	nic	k6eAd1
lepší	dobrý	k2eAgFnSc4d2
politiku	politika	k1gFnSc4
a	a	k8xC
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
510	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ho	on	k3xPp3gNnSc4
Athéňané	Athéňan	k1gMnPc1
i	i	k8xC
za	za	k7c2
pomoci	pomoc	k1gFnSc2
Sparťanů	Sparťan	k1gMnPc2
vyhnali	vyhnat	k5eAaPmAgMnP
z	z	k7c2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hippias	Hippias	k1gInSc1
poté	poté	k6eAd1
odešel	odejít	k5eAaPmAgInS
do	do	k7c2
Persie	Persie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
později	pozdě	k6eAd2
králi	král	k1gMnSc3
Dareiovi	Dareius	k1gMnSc3
I.	I.	kA
důležitým	důležitý	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
poradcem	poradce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
Archonem	Archon	k1gInSc7
(	(	kIx(
<g/>
Archon	Archon	k1gMnSc1
eponymos	eponymos	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
v	v	k7c6
Aténách	Atény	k1gFnPc6
stal	stát	k5eAaPmAgMnS
Isagoras	Isagoras	k1gMnSc1
za	za	k7c2
podpory	podpora	k1gFnSc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
starého	starý	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
spartského	spartský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Kleomena	Kleomen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
Atén	Atény	k1gFnPc2
se	se	k3xPyFc4
ale	ale	k9
opět	opět	k6eAd1
vzbouřili	vzbouřit	k5eAaPmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
jejich	jejich	k3xOp3gFnSc4
podporu	podpora	k1gFnSc4
měl	mít	k5eAaImAgMnS
Kleisthenes	Kleisthenes	k1gMnSc1
(	(	kIx(
<g/>
reprezentant	reprezentant	k1gMnSc1
lidu	lid	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
Isagoras	Isagoras	k1gMnSc1
i	i	k8xC
Kleomenes	Kleomenes	k1gMnSc1
museli	muset	k5eAaImAgMnP
nedobrovolně	dobrovolně	k6eNd1
Atény	Atény	k1gFnPc4
opustit	opustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následná	následný	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
Sparty	Sparta	k1gFnSc2
proti	proti	k7c3
Aténám	Atény	k1gFnPc3
skončila	skončit	k5eAaPmAgFnS
fiaskem	fiasko	k1gNnSc7
pro	pro	k7c4
neshody	neshoda	k1gFnPc4
při	při	k7c6
jejím	její	k3xOp3gNnSc6
vedení	vedení	k1gNnSc6
mezi	mezi	k7c7
králi	král	k1gMnPc7
Sparty	Sparta	k1gFnSc2
Demaratem	Demarat	k1gMnSc7
a	a	k8xC
Kleomenem	Kleomen	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
těchto	tento	k3xDgFnPc6
událostech	událost	k1gFnPc6
se	se	k3xPyFc4
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
přijal	přijmout	k5eAaPmAgInS
zákon	zákon	k1gInSc1
zakazující	zakazující	k2eAgInSc1d1
vést	vést	k5eAaImF
vojenskou	vojenský	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
současně	současně	k6eAd1
oběma	dva	k4xCgMnPc7
králi	král	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Asi	asi	k9
o	o	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
maloasijští	maloasijský	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
vzbouřili	vzbouřit	k5eAaPmAgMnP
proti	proti	k7c3
perské	perský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
vypuklo	vypuknout	k5eAaPmAgNnS
v	v	k7c6
Milétu	Milét	k1gInSc6
a	a	k8xC
do	do	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
čela	čelo	k1gNnSc2
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgMnS
tyran	tyran	k1gMnSc1
Milétu	Milét	k1gInSc2
Aristagorás	Aristagorás	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
požádal	požádat	k5eAaPmAgInS
o	o	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
Spartu	Sparta	k1gFnSc4
i	i	k8xC
Atény	Atény	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Spartě	Sparta	k1gFnSc6
nepochodil	nepochodil	k?
<g/>
,	,	kIx,
ale	ale	k8xC
Athény	Athéna	k1gFnPc1
mu	on	k3xPp3gNnSc3
poskytly	poskytnout	k5eAaPmAgFnP
dvacet	dvacet	k4xCc4
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
skutečnost	skutečnost	k1gFnSc4
si	se	k3xPyFc3
perský	perský	k2eAgMnSc1d1
král	král	k1gMnSc1
Dareios	Dareiosa	k1gFnPc2
I.	I.	kA
zapsal	zapsat	k5eAaPmAgInS
do	do	k7c2
paměti	paměť	k1gFnSc2
a	a	k8xC
Athény	Athéna	k1gFnSc2
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
dostaly	dostat	k5eAaPmAgInP
na	na	k7c4
seznam	seznam	k1gInSc4
potenciálních	potenciální	k2eAgInPc2d1
cílů	cíl	k1gInPc2
vojenské	vojenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
Peršanů	Peršan	k1gMnPc2
v	v	k7c6
blízké	blízký	k2eAgFnSc6d1
budoucnosti	budoucnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
ještě	ještě	k9
během	během	k7c2
potlačování	potlačování	k1gNnSc2
povstání	povstání	k1gNnSc2
Řeků	Řek	k1gMnPc2
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
Peršané	Peršan	k1gMnPc1
postupně	postupně	k6eAd1
dobyli	dobýt	k5eAaPmAgMnP
území	území	k1gNnSc4
Bosporu	Bospor	k1gInSc2
a	a	k8xC
Thrákie	Thrákie	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
494	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
povstání	povstání	k1gNnSc2
maloasijských	maloasijský	k2eAgInPc2d1
Řeků	Řek	k1gMnPc2
v	v	k7c6
Milétu	Milét	k1gInSc6
definitivně	definitivně	k6eAd1
krvavě	krvavě	k6eAd1
potlačili	potlačit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
cílem	cíl	k1gInSc7
Dareia	Dareium	k1gNnSc2
I.	I.	kA
se	se	k3xPyFc4
už	už	k6eAd1
tentokrát	tentokrát	k6eAd1
stalo	stát	k5eAaPmAgNnS
Řecko	Řecko	k1gNnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
hlavně	hlavně	k9
Athény	Athéna	k1gFnPc1
<g/>
,	,	kIx,
proto	proto	k8xC
rozeslal	rozeslat	k5eAaPmAgInS
posly	posel	k1gMnPc4
do	do	k7c2
řeckých	řecký	k2eAgInPc2d1
států	stát	k1gInPc2
s	s	k7c7
požadavkem	požadavek	k1gInSc7
o	o	k7c4
"	"	kIx"
<g/>
vodu	voda	k1gFnSc4
a	a	k8xC
zem	zem	k1gFnSc4
<g/>
"	"	kIx"
tj.	tj.	kA
podřídit	podřídit	k5eAaPmF
se	se	k3xPyFc4
perské	perský	k2eAgFnSc3d1
moci	moc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslové	posel	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
i	i	k9
do	do	k7c2
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
již	již	k9
tehdy	tehdy	k6eAd1
svůj	svůj	k3xOyFgInSc4
postoj	postoj	k1gInSc4
k	k	k7c3
Peršanům	peršan	k1gInPc3
změnila	změnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokud	dokud	k8xS
Spartě	Sparta	k1gFnSc3
nehrozilo	hrozit	k5eNaImAgNnS
bezprostřední	bezprostřední	k2eAgNnSc1d1
nebezpečí	nebezpečí	k1gNnSc1
<g/>
,	,	kIx,
snažila	snažit	k5eAaImAgFnS
se	se	k3xPyFc4
z	z	k7c2
opatrnosti	opatrnost	k1gFnSc2
nezasahovat	zasahovat	k5eNaImF
do	do	k7c2
perských	perský	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
tentokrát	tentokrát	k6eAd1
rozhořčený	rozhořčený	k2eAgInSc1d1
dav	dav	k1gInSc1
dva	dva	k4xCgInPc4
posly	posel	k1gMnPc4
hodil	hodit	k5eAaImAgInS,k5eAaPmAgInS
do	do	k7c2
studny	studna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zákonů	zákon	k1gInPc2
Sparty	Sparta	k1gFnSc2
byla	být	k5eAaImAgFnS
však	však	k9
vražda	vražda	k1gFnSc1
posla	posel	k1gMnSc2
činem	čin	k1gInSc7
nepoctivým	poctivý	k2eNgInSc7d1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
přihlásili	přihlásit	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
Sparťané	Sparťan	k1gMnPc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
za	za	k7c4
to	ten	k3xDgNnSc4
zaplatili	zaplatit	k5eAaPmAgMnP
svými	svůj	k3xOyFgInPc7
životy	život	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Nedaleký	daleký	k2eNgInSc1d1
řecký	řecký	k2eAgInSc1d1
ostrovní	ostrovní	k2eAgInSc1d1
stát	stát	k1gInSc1
Ejina	Ejino	k1gNnSc2
perské	perský	k2eAgFnSc2d1
žádosti	žádost	k1gFnSc2
vyhověl	vyhovět	k5eAaPmAgInS
a	a	k8xC
spartský	spartský	k2eAgMnSc1d1
král	král	k1gMnSc1
Kleomenes	Kleomenes	k1gMnSc1
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
za	za	k7c4
to	ten	k3xDgNnSc4
rozhodl	rozhodnout	k5eAaPmAgMnS
potrestat	potrestat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolukrál	spolukrál	k1gMnSc1
Démaratos	Démaratos	k1gMnSc1
byl	být	k5eAaImAgMnS
ale	ale	k8xC
zásadně	zásadně	k6eAd1
proti	proti	k7c3
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
nepřátelství	nepřátelství	k1gNnSc1
mezi	mezi	k7c7
králi	král	k1gMnPc7
ještě	ještě	k9
více	hodně	k6eAd2
prohloubilo	prohloubit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kleomenes	Kleomenes	k1gInSc1
na	na	k7c4
to	ten	k3xDgNnSc4
obvinil	obvinit	k5eAaPmAgMnS
Demarata	Demarat	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
vládne	vládnout	k5eAaImIp3nS
neprávem	neprávo	k1gNnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
není	být	k5eNaImIp3nS
synem	syn	k1gMnSc7
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
tato	tento	k3xDgFnSc1
záležitost	záležitost	k1gFnSc1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
Delf	Delfy	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
obvinění	obvinění	k1gNnSc1
potvrzeno	potvrdit	k5eAaPmNgNnS
a	a	k8xC
Démaratos	Démaratos	k1gMnSc1
byl	být	k5eAaImAgMnS
zbaven	zbavit	k5eAaPmNgMnS
trůnu	trůn	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
obsadil	obsadit	k5eAaPmAgMnS
Leótychidas	Leótychidas	k1gMnSc1
syn	syn	k1gMnSc1
Menarea	Menarea	k1gMnSc1
z	z	k7c2
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Eurypontovcov	Eurypontovcov	k1gInSc1
(	(	kIx(
<g/>
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
v	v	k7c6
roce	rok	k1gInSc6
491	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leótychidas	Leótychidas	k1gMnSc1
nenáviděl	návidět	k5eNaImAgMnS,k5eAaImAgMnS
Demarata	Demarat	k1gMnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
mu	on	k3xPp3gMnSc3
v	v	k7c6
minulosti	minulost	k1gFnSc6
přebral	přebrat	k5eAaPmAgMnS
snoubenku	snoubenka	k1gFnSc4
Perkalu	Perkal	k1gMnSc3
dceru	dcera	k1gFnSc4
Chilóna	Chilón	k1gMnSc2
a	a	k8xC
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démaratovi	Démaratův	k2eAgMnPc5d1
proto	proto	k8xC
nezbývalo	zbývat	k5eNaImAgNnS
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
jen	jen	k6eAd1
utéci	utéct	k5eAaPmF
a	a	k8xC
své	svůj	k3xOyFgNnSc4
útočiště	útočiště	k1gNnSc4
hledat	hledat	k5eAaImF
u	u	k7c2
perského	perský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Dareia	Dareius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Historik	historik	k1gMnSc1
Herodotos	Herodotos	k1gMnSc1
nám	my	k3xPp1nPc3
zanechal	zanechat	k5eAaPmAgMnS
zajímavou	zajímavý	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
týkající	týkající	k2eAgFnSc4d1
se	se	k3xPyFc4
Demarata	Demare	k1gNnPc1
<g/>
,	,	kIx,
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
jediným	jediný	k2eAgInSc7d1
spartský	spartský	k2eAgInSc1d1
králem	král	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
olympijské	olympijský	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
soutěži	soutěž	k1gFnSc6
čtyřspřeží	čtyřspřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demarate	Demarat	k1gInSc5
odeslaná	odeslaný	k2eAgFnSc1d1
utajená	utajený	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
na	na	k7c6
dřevěné	dřevěný	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
zalitá	zalitý	k2eAgFnSc1d1
voskem	vosk	k1gInSc7
ze	z	k7c2
Sús	Sús	k1gFnSc2
do	do	k7c2
Sparty	Sparta	k1gFnSc2
je	být	k5eAaImIp3nS
zaznamenána	zaznamenat	k5eAaPmNgFnS
také	také	k9
Hérodotem	Hérodot	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démaratos	Démaratosa	k1gFnPc2
na	na	k7c6
ní	on	k3xPp3gFnSc6
oznámil	oznámit	k5eAaPmAgMnS
svým	svůj	k3xOyFgMnPc3
rodákům	rodák	k1gMnPc3
rozhodnutí	rozhodnutí	k1gNnSc4
perského	perský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Xerxa	Xerxes	k1gMnSc2
I.	I.	kA
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Řecko	Řecko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herodotos	Herodotos	k1gMnSc1
se	se	k3xPyFc4
ale	ale	k9
zamýšlí	zamýšlet	k5eAaImIp3nS
nad	nad	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jaké	jaký	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
úmysly	úmysl	k1gInPc1
vedly	vést	k5eAaImAgInP
Demarata	Demarat	k1gMnSc2
při	při	k7c6
jejím	její	k3xOp3gNnSc6
odesílání	odesílání	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgMnS
psancem	psanec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
480	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Xerxes	Xerxes	k1gMnSc1
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
Řecko	Řecko	k1gNnSc4
píše	psát	k5eAaImIp3nS
opět	opět	k6eAd1
Herodotos	Herodotos	k1gMnSc1
<g/>
,	,	kIx,
Démaratos	Démaratos	k1gMnSc1
doprovázel	doprovázet	k5eAaImAgMnS
perského	perský	k2eAgMnSc4d1
krále	král	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
otázku	otázka	k1gFnSc4
krále	král	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nP
o	o	k7c6
vojácích	voják	k1gMnPc6
se	se	k3xPyFc4
Sparty	Sparta	k1gFnSc2
odpověděl	odpovědět	k5eAaPmAgInS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Pokud	pokud	k8xS
nebudu	být	k5eNaImBp1nS
pravdou	pravda	k1gFnSc7
šetřit	šetřit	k5eAaImF
<g/>
,	,	kIx,
nebudou	být	k5eNaImBp3nP
se	se	k3xPyFc4
ti	ty	k3xPp2nSc3
moje	můj	k3xOp1gNnSc1
slova	slovo	k1gNnPc1
líbit	líbit	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
donutil	donutit	k5eAaPmAgMnS
si	se	k3xPyFc3
mě	já	k3xPp1nSc4
abych	aby	kYmCp1nS
ty	ten	k3xDgInPc4
nejpravdivější	pravdivý	k2eAgInPc4d3
slova	slovo	k1gNnSc2
pronesl	pronést	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
A	a	k9
v	v	k7c6
pokračujícím	pokračující	k2eAgInSc6d1
rozhovoru	rozhovor	k1gInSc6
popisujícím	popisující	k2eAgInSc6d1
morální	morální	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
Sparťanů	Sparťan	k1gMnPc2
řekl	říct	k5eAaPmAgInS
Xerxovi	Xerxes	k1gMnSc3
tato	tento	k3xDgNnPc1
slova	slovo	k1gNnPc1
<g/>
:	:	kIx,
<g/>
"	"	kIx"
<g/>
Sparťané	Sparťan	k1gMnPc1
když	když	k8xS
bojují	bojovat	k5eAaImIp3nP
za	za	k7c4
sebe	sebe	k3xPyFc4
po	po	k7c6
jednom	jeden	k4xCgInSc6
nejsou	být	k5eNaImIp3nP
o	o	k7c4
nic	nic	k3yNnSc4
lepšími	dobrý	k2eAgNnPc7d2
bojovníky	bojovník	k1gMnPc4
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
společně	společně	k6eAd1
jsou	být	k5eAaImIp3nP
nejlepší	dobrý	k2eAgMnPc1d3
ze	z	k7c2
všech	všecek	k3xTgMnPc2
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
jsou	být	k5eAaImIp3nP
svobodní	svobodný	k2eAgMnPc1d1
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
svobodní	svobodný	k2eAgMnPc1d1
ve	v	k7c6
všem	všecek	k3xTgNnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gMnSc7
pánem	pán	k1gMnSc7
je	být	k5eAaImIp3nS
zákon	zákon	k1gInSc4
a	a	k8xC
toho	ten	k3xDgMnSc4
se	se	k3xPyFc4
obávají	obávat	k5eAaImIp3nP
více	hodně	k6eAd2
než	než	k8xS
tebe	ty	k3xPp2nSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konají	konat	k5eAaImIp3nP
co	co	k9
jim	on	k3xPp3gMnPc3
káže	kázat	k5eAaImIp3nS
a	a	k8xC
když	když	k8xS
jim	on	k3xPp3gMnPc3
káže	kázat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
neutíkali	utíkat	k5eNaImAgMnP
před	před	k7c7
jakýmkoli	jakýkoli	k3yIgNnSc7
množstvím	množství	k1gNnSc7
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
vytrvají	vytrvat	k5eAaPmIp3nP
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
místě	místo	k1gNnSc6
ať	ať	k9
zvítězí	zvítězit	k5eAaPmIp3nP
nebo	nebo	k8xC
zahynou	zahynout	k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Démaratos	Démaratosa	k1gFnPc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Herodotos	Herodotos	k1gMnSc1
<g/>
,	,	kIx,
Historie	historie	k1gFnSc1
<g/>
,	,	kIx,
V	V	kA
<g/>
,	,	kIx,
70	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
;	;	kIx,
V	V	kA
<g/>
,	,	kIx,
94	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
;	;	kIx,
VI	VI	kA
<g/>
,	,	kIx,
48	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
<g/>
;	;	kIx,
VI	VI	kA
<g/>
,	,	kIx,
51	#num#	k4
<g/>
;	;	kIx,
VI	VI	kA
<g/>
,	,	kIx,
60	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
;	;	kIx,
VI	VI	kA
<g/>
,	,	kIx,
70	#num#	k4
<g/>
;	;	kIx,
VII	VII	kA
<g/>
,	,	kIx,
239	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pausanias	Pausanias	k1gInSc1
<g/>
,	,	kIx,
Periégésis	Periégésis	k1gInSc1
TES	tes	k1gInSc1
Hellados	Helladosa	k1gFnPc2
<g/>
,	,	kIx,
3,4	3,4	k4
<g/>
,2	,2	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtech	Vojta	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grécky	Gréck	k1gInPc4
zázrak	zázrak	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Perfekt	perfektum	k1gNnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8080461031	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Polyainos	Polyainos	k1gInSc1
<g/>
,	,	kIx,
Strategie	strategie	k1gFnSc1
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
2,20	2,20	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
králů	král	k1gMnPc2
Sparty	Sparta	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Antika	antika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
