<s>
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
je	být	k5eAaImIp3nS	být
dostihový	dostihový	k2eAgInSc1d1	dostihový
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
steeplechase	steeplechase	k1gFnSc1	steeplechase
cross-country	crossountrum	k1gNnPc7	cross-countrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konající	konající	k2eAgNnSc1d1	konající
se	se	k3xPyFc4	se
každou	každý	k3xTgFnSc4	každý
druhou	druhý	k4xOgFnSc4	druhý
říjnovou	říjnový	k2eAgFnSc4d1	říjnová
neděli	neděle	k1gFnSc4	neděle
na	na	k7c6	na
dostihovém	dostihový	k2eAgNnSc6d1	dostihové
závodišti	závodiště	k1gNnSc6	závodiště
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
dostih	dostih	k1gInSc4	dostih
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
závod	závod	k1gInSc1	závod
Velké	velká	k1gFnSc2	velká
pardubické	pardubický	k2eAgFnSc2d1	pardubická
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
tradici	tradice	k1gFnSc4	tradice
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
již	již	k6eAd1	již
za	za	k7c7	za
sebou	se	k3xPyFc7	se
měla	mít	k5eAaImAgFnS	mít
padesát	padesát	k4xCc4	padesát
startů	start	k1gInPc2	start
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
byly	být	k5eAaImAgInP	být
hony	hon	k1gInPc1	hon
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
šlechta	šlechta	k1gFnSc1	šlechta
oblečená	oblečený	k2eAgFnSc1d1	oblečená
do	do	k7c2	do
barevných	barevný	k2eAgInPc2d1	barevný
kabátců	kabátec	k1gInPc2	kabátec
se	s	k7c7	s
smečkou	smečka	k1gFnSc7	smečka
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
na	na	k7c4	na
koních	koní	k2eAgFnPc2d1	koní
pronásledovala	pronásledovat	k5eAaImAgFnS	pronásledovat
zvěř	zvěř	k1gFnSc1	zvěř
a	a	k8xC	a
překonávala	překonávat	k5eAaImAgFnS	překonávat
přitom	přitom	k6eAd1	přitom
různé	různý	k2eAgFnPc4d1	různá
přírodní	přírodní	k2eAgFnPc4d1	přírodní
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
zábavy	zábava	k1gFnSc2	zábava
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgInS	stát
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
i	i	k8xC	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
na	na	k7c6	na
chlumeckém	chlumecký	k2eAgNnSc6d1	chlumecké
panství	panství	k1gNnSc6	panství
Oktaviána	Oktavián	k1gMnSc2	Oktavián
Kinského	Kinský	k1gMnSc2	Kinský
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
aktivitě	aktivita	k1gFnSc3	aktivita
knížete	kníže	k1gMnSc2	kníže
Františka	František	k1gMnSc2	František
Lichtenštejnského	lichtenštejnský	k2eAgMnSc2d1	lichtenštejnský
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
hony	hon	k1gInPc1	hon
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgFnSc3d1	zvaná
parforsní	parforsní	k2eAgFnSc3d1	parforsní
<g/>
)	)	kIx)	)
přesunuly	přesunout	k5eAaPmAgInP	přesunout
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
zdejší	zdejší	k2eAgFnSc2d1	zdejší
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
připomínala	připomínat	k5eAaImAgFnS	připomínat
anglický	anglický	k2eAgInSc4d1	anglický
park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
příčinou	příčina	k1gFnSc7	příčina
velkého	velký	k2eAgInSc2d1	velký
rozmachu	rozmach	k1gInSc2	rozmach
honů	hon	k1gInPc2	hon
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
Velká	velká	k1gFnSc1	velká
liverpoolská	liverpoolský	k2eAgFnSc1d1	liverpoolská
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
i	i	k9	i
ostatní	ostatní	k2eAgFnPc4d1	ostatní
evropské	evropský	k2eAgFnPc4d1	Evropská
země	zem	k1gFnPc4	zem
a	a	k8xC	a
např.	např.	kA	např.
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
působila	působit	k5eAaImAgFnS	působit
řada	řada	k1gFnSc1	řada
trenérů	trenér	k1gMnPc2	trenér
a	a	k8xC	a
žokejů	žokej	k1gMnPc2	žokej
z	z	k7c2	z
kolébky	kolébka	k1gFnSc2	kolébka
turfu	turf	k1gInSc2	turf
<g/>
,	,	kIx,	,
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
závodní	závodní	k2eAgFnSc1d1	závodní
dráha	dráha	k1gFnSc1	dráha
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
Dostihový	dostihový	k2eAgInSc1d1	dostihový
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
dostihů	dostih	k1gInPc2	dostih
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
požádal	požádat	k5eAaPmAgMnS	požádat
obec	obec	k1gFnSc1	obec
Pardubice	Pardubice	k1gInPc1	Pardubice
o	o	k7c6	o
postoupení	postoupení	k1gNnSc6	postoupení
pastvin	pastvina	k1gFnPc2	pastvina
i	i	k9	i
s	s	k7c7	s
lesíkem	lesík	k1gInSc7	lesík
Cvrčkovem	Cvrčkov	k1gInSc7	Cvrčkov
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
závodní	závodní	k2eAgFnSc2d1	závodní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
snaze	snaha	k1gFnSc3	snaha
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
a	a	k8xC	a
nezvyklé	zvyklý	k2eNgNnSc4d1	nezvyklé
uspořádání	uspořádání	k1gNnSc4	uspořádání
překážek	překážka	k1gFnPc2	překážka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
a	a	k8xC	a
obtížná	obtížný	k2eAgFnSc1d1	obtížná
závodní	závodní	k2eAgFnSc1d1	závodní
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
steeplechase	steeplechase	k1gFnSc1	steeplechase
se	se	k3xPyFc4	se
běžela	běžet	k5eAaImAgFnS	běžet
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1874	[number]	k4	1874
o	o	k7c4	o
8000	[number]	k4	8000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
startu	start	k1gInSc6	start
stálo	stát	k5eAaImAgNnS	stát
14	[number]	k4	14
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
odnesli	odnést	k5eAaPmAgMnP	odnést
plnokrevný	plnokrevný	k2eAgMnSc1d1	plnokrevný
hřebec	hřebec	k1gMnSc1	hřebec
Fantome	fantom	k1gInSc5	fantom
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
žokejem	žokej	k1gMnSc7	žokej
Georgem	Georg	k1gMnSc7	Georg
Sayersem	Sayers	k1gMnSc7	Sayers
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Dostih	dostih	k1gInSc4	dostih
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
pouhých	pouhý	k2eAgMnPc2d1	pouhý
7	[number]	k4	7
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
dostihu	dostih	k1gInSc2	dostih
a	a	k8xC	a
výsledek	výsledek	k1gInSc4	výsledek
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
ohlas	ohlas	k1gInSc4	ohlas
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Velká	velká	k1gFnSc1	velká
běžela	běžet	k5eAaImAgFnS	běžet
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
válečných	válečný	k2eAgNnPc2d1	válečné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
politických	politický	k2eAgFnPc2d1	politická
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
sousedící	sousedící	k2eAgNnSc4d1	sousedící
vojenské	vojenský	k2eAgNnSc4d1	vojenské
letiště	letiště	k1gNnSc4	letiště
zabrali	zabrat	k5eAaPmAgMnP	zabrat
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1876	[number]	k4	1876
a	a	k8xC	a
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
má	mít	k5eAaImIp3nS	mít
závodiště	závodiště	k1gNnSc1	závodiště
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
poválečných	poválečný	k2eAgNnPc2d1	poválečné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
běhalo	běhat	k5eAaImAgNnS	běhat
až	až	k9	až
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Popkovice	Popkovice	k1gFnSc2	Popkovice
a	a	k8xC	a
běhalo	běhat	k5eAaImAgNnS	běhat
se	se	k3xPyFc4	se
též	též	k9	též
za	za	k7c7	za
tribunami	tribuna	k1gFnPc7	tribuna
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
parkoviště	parkoviště	k1gNnSc4	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc3	ročník
závodu	závod	k1gInSc2	závod
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vydána	vydán	k2eAgFnSc1d1	vydána
stříbrná	stříbrná	k1gFnSc1	stříbrná
stokorunová	stokorunový	k2eAgFnSc1d1	stokorunová
pamětní	pamětní	k2eAgFnPc4d1	pamětní
mince	mince	k1gFnPc4	mince
(	(	kIx(	(
<g/>
autorka	autorka	k1gFnSc1	autorka
Jarmila	Jarmila	k1gFnSc1	Jarmila
Truhlíková-Spěváková	Truhlíková-Spěváková	k1gFnSc1	Truhlíková-Spěváková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
koná	konat	k5eAaImIp3nS	konat
vždy	vždy	k6eAd1	vždy
druhou	druhý	k4xOgFnSc4	druhý
říjnovou	říjnový	k2eAgFnSc4d1	říjnová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
tohoto	tento	k3xDgInSc2	tento
dostihu	dostih	k1gInSc2	dostih
měří	měřit	k5eAaImIp3nS	měřit
6900	[number]	k4	6900
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
31	[number]	k4	31
překážek	překážka	k1gFnPc2	překážka
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc1	kůň
ji	on	k3xPp3gFnSc4	on
běží	běžet	k5eAaImIp3nP	běžet
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
startu	start	k1gInSc6	start
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
asi	asi	k9	asi
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
koní	kůň	k1gMnPc2	kůň
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
žokeji	žokej	k1gMnPc7	žokej
a	a	k8xC	a
žokejkami	žokejka	k1gFnPc7	žokejka
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgMnS	vžít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
celého	celý	k2eAgInSc2d1	celý
dostihového	dostihový	k2eAgInSc2d1	dostihový
víkendu	víkend	k1gInSc2	víkend
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
8	[number]	k4	8
dostihů	dostih	k1gInPc2	dostih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
dostihem	dostih	k1gInSc7	dostih
je	být	k5eAaImIp3nS	být
dostih	dostih	k1gInSc1	dostih
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
současném	současný	k2eAgMnSc6d1	současný
sponzorovi	sponzor	k1gMnSc6	sponzor
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
pojišťovnou	pojišťovna	k1gFnSc7	pojišťovna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
pardubické	pardubický	k2eAgInPc1d1	pardubický
mohou	moct	k5eAaImIp3nP	moct
startovat	startovat	k5eAaBmF	startovat
pouze	pouze	k6eAd1	pouze
šestiletí	šestiletý	k2eAgMnPc1d1	šestiletý
a	a	k8xC	a
starší	starý	k2eAgMnPc1d2	starší
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
kvalifikačních	kvalifikační	k2eAgInPc2d1	kvalifikační
dostihů	dostih	k1gInPc2	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Dostih	dostih	k1gInSc1	dostih
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
vítěze	vítěz	k1gMnSc4	vítěz
-	-	kIx~	-
rekordmana	rekordman	k1gMnSc4	rekordman
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Velkou	velký	k2eAgFnSc4d1	velká
pardubickou	pardubický	k2eAgFnSc4d1	pardubická
kůň	kůň	k1gMnSc1	kůň
Železník	železník	k1gInSc1	železník
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
a	a	k8xC	a
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Váňou	Váňa	k1gMnSc7	Váňa
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
běžel	běžet	k5eAaImAgInS	běžet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Traťový	traťový	k2eAgInSc1d1	traťový
rekord	rekord	k1gInSc1	rekord
"	"	kIx"	"
<g/>
Velké	velká	k1gFnSc2	velká
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
překonán	překonat	k5eAaPmNgInS	překonat
valachem	valach	k1gMnSc7	valach
Nikasem	Nikas	k1gMnSc7	Nikas
s	s	k7c7	s
žokejem	žokej	k1gMnSc7	žokej
Markem	Marek	k1gMnSc7	Marek
Stromským	Stromský	k2eAgMnSc7d1	Stromský
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
čas	čas	k1gInSc1	čas
byl	být	k5eAaImAgInS	být
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
55,29	[number]	k4	55,29
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
teprve	teprve	k6eAd1	teprve
druhá	druhý	k4xOgFnSc1	druhý
Velká	velká	k1gFnSc1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jela	jet	k5eAaImAgFnS	jet
pod	pod	k7c4	pod
9	[number]	k4	9
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vítěz	vítěz	k1gMnSc1	vítěz
Nikas	Nikas	k1gMnSc1	Nikas
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
rámcových	rámcový	k2eAgInPc2d1	rámcový
závodů	závod	k1gInPc2	závod
Čáriray	Čáriraa	k1gFnSc2	Čáriraa
měli	mít	k5eAaImAgMnP	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
dopingový	dopingový	k2eAgInSc4d1	dopingový
nález	nález	k1gInSc4	nález
<g/>
.	.	kIx.	.
</s>
<s>
Trenérem	trenér	k1gMnSc7	trenér
obou	dva	k4xCgMnPc2	dva
koní	kůň	k1gMnPc2	kůň
byl	být	k5eAaImAgMnS	být
Stanislav	Stanislav	k1gMnSc1	Stanislav
Popelka	Popelka	k1gMnSc1	Popelka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
diskvalifikaci	diskvalifikace	k1gFnSc6	diskvalifikace
je	být	k5eAaImIp3nS	být
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
časem	čas	k1gInSc7	čas
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
56,01	[number]	k4	56,01
valacha	valach	k1gMnSc2	valach
Ribelina	Ribelin	k2eAgMnSc2d1	Ribelin
s	s	k7c7	s
žokejem	žokej	k1gMnSc7	žokej
Pavlem	Pavel	k1gMnSc7	Pavel
Kašným	Kašný	k1gMnSc7	Kašný
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
druhým	druhý	k4xOgInSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
držel	držet	k5eAaImAgInS	držet
rekord	rekord	k1gInSc1	rekord
závod	závod	k1gInSc1	závod
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
vítězka	vítězka	k1gFnSc1	vítězka
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
za	za	k7c4	za
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
58,99	[number]	k4	58,99
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c4	na
6900	[number]	k4	6900
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
trati	trať	k1gFnSc2	trať
činila	činit	k5eAaImAgFnS	činit
12,78	[number]	k4	12,78
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
-	-	kIx~	-
46,00	[number]	k4	46,00
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
O	o	k7c4	o
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
rekord	rekord	k1gInSc4	rekord
se	se	k3xPyFc4	se
postarala	postarat	k5eAaPmAgFnS	postarat
bělka	bělka	k1gFnSc1	bělka
Sixteen	Sixteno	k1gNnPc2	Sixteno
s	s	k7c7	s
žokejem	žokej	k1gMnSc7	žokej
Bartošem	Bartoš	k1gMnSc7	Bartoš
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
sice	sice	k8xC	sice
doběhla	doběhnout	k5eAaPmAgFnS	doběhnout
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Amant	amant	k1gMnSc1	amant
Gris	Gris	k1gInSc4	Gris
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Marka	Marek	k1gMnSc2	Marek
Stromského	Stromský	k2eAgInSc2d1	Stromský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
dostal	dostat	k5eAaPmAgInS	dostat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
porušení	porušení	k1gNnSc3	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
špatnému	špatný	k2eAgNnSc3d1	špatné
objetí	objetí	k1gNnSc3	objetí
točného	točný	k2eAgInSc2d1	točný
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
bělky	bělka	k1gFnSc2	bělka
Sixteen	Sixtena	k1gFnPc2	Sixtena
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
rekordem	rekord	k1gInSc7	rekord
pro	pro	k7c4	pro
klisny	klisna	k1gFnPc4	klisna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
dostih	dostih	k1gInSc4	dostih
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
žádný	žádný	k3yNgMnSc1	žádný
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
dostih	dostih	k1gInSc1	dostih
ukončen	ukončit	k5eAaPmNgInS	ukončit
bez	bez	k7c2	bez
vítěze	vítěz	k1gMnSc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
pro	pro	k7c4	pro
překročení	překročení	k1gNnSc4	překročení
daného	daný	k2eAgInSc2d1	daný
časového	časový	k2eAgInSc2d1	časový
limitu	limit	k1gInSc2	limit
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladším	mladý	k2eAgMnSc7d3	nejmladší
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Josef	Josef	k1gMnSc1	Josef
Vavroušek	Vavroušek	k1gMnSc1	Vavroušek
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
Letce	letec	k1gMnSc2	letec
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
vítězem	vítěz	k1gMnSc7	vítěz
je	být	k5eAaImIp3nS	být
amatér	amatér	k1gMnSc1	amatér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hejmovský	Hejmovský	k1gMnSc1	Hejmovský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
59	[number]	k4	59
let	léto	k1gNnPc2	léto
na	na	k7c4	na
koni	kůň	k1gMnPc7	kůň
Salvátor	Salvátor	k1gMnSc1	Salvátor
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
osminásobným	osminásobný	k2eAgMnSc7d1	osminásobný
vítězem	vítěz	k1gMnSc7	vítěz
je	být	k5eAaImIp3nS	být
žokej	žokej	k1gMnSc1	žokej
Josef	Josef	k1gMnSc1	Josef
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
Velké	velká	k1gFnSc6	velká
pardubické	pardubický	k2eAgNnSc1d1	pardubické
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Lata	lata	k1gFnSc1	lata
Brandisová	Brandisový	k2eAgFnSc1d1	Brandisová
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
a	a	k8xC	a
jela	jet	k5eAaImAgFnS	jet
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
klisny	klisna	k1gFnSc2	klisna
Normy	Norma	k1gFnSc2	Norma
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
žokej	žokej	k1gMnSc1	žokej
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
čtyři	čtyři	k4xCgInPc4	čtyři
ročníky	ročník	k1gInPc4	ročník
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Peter	Peter	k1gMnSc1	Peter
Gehm	Gehm	k1gMnSc1	Gehm
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
obsadili	obsadit	k5eAaPmAgMnP	obsadit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
množství	množství	k1gNnSc3	množství
koní	kůň	k1gMnPc2	kůň
zahynuvších	zahynuvší	k2eAgMnPc2d1	zahynuvší
při	při	k7c6	při
závodu	závod	k1gInSc6	závod
ochránci	ochránce	k1gMnPc1	ochránce
zvířat	zvíře	k1gNnPc2	zvíře
startovní	startovní	k2eAgInSc1d1	startovní
prostor	prostor	k1gInSc1	prostor
a	a	k8xC	a
pokusili	pokusit	k5eAaPmAgMnP	pokusit
se	se	k3xPyFc4	se
zabránit	zabránit	k5eAaPmF	zabránit
zahájení	zahájení	k1gNnSc3	zahájení
dostihu	dostih	k1gInSc2	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
policejní	policejní	k2eAgInSc1d1	policejní
zákrok	zákrok	k1gInSc1	zákrok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
rozehnání	rozehnání	k1gNnSc3	rozehnání
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
chaos	chaos	k1gInSc1	chaos
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Taxisově	Taxisův	k2eAgInSc6d1	Taxisův
příkopu	příkop	k1gInSc6	příkop
spadlo	spadnout	k5eAaPmAgNnS	spadnout
osm	osm	k4xCc1	osm
koní	kůň	k1gMnPc2	kůň
včetně	včetně	k7c2	včetně
obhájce	obhájce	k1gMnSc2	obhájce
prvenství	prvenství	k1gNnSc2	prvenství
Železníka	železník	k1gMnSc2	železník
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
aféry	aféra	k1gFnSc2	aféra
byla	být	k5eAaImAgFnS	být
úprava	úprava	k1gFnSc1	úprava
trati	trať	k1gFnSc2	trať
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
zavlažování	zavlažování	k1gNnSc1	zavlažování
změkčující	změkčující	k2eAgNnSc1d1	změkčující
povrch	povrch	k7c2wR	povrch
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
snížena	snížit	k5eAaPmNgFnS	snížit
hloubka	hloubka	k1gFnSc1	hloubka
Velkého	velký	k2eAgInSc2d1	velký
Taxisu	taxis	k1gInSc2	taxis
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Aktualizováno	aktualizován	k2eAgNnSc1d1	Aktualizováno
po	po	k7c6	po
ročníku	ročník	k1gInSc6	ročník
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
započítán	započítán	k2eAgInSc1d1	započítán
dostih	dostih	k1gInSc1	dostih
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cílem	cíl	k1gInSc7	cíl
neprošel	projít	k5eNaPmAgMnS	projít
žádný	žádný	k1gMnSc1	žádný
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
dva	dva	k4xCgInPc1	dva
závody	závod	k1gInPc1	závod
Velké	velký	k2eAgFnSc2d1	velká
pardubické	pardubický	k2eAgFnSc2d1	pardubická
armády	armáda	k1gFnSc2	armáda
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1912	[number]	k4	1912
a	a	k8xC	a
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
