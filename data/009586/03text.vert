<p>
<s>
Surinam	Surinam	k1gInSc1	Surinam
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
Republiek	Republiek	k6eAd1	Republiek
Suriname	Surinam	k1gInSc5	Surinam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Republika	republika	k1gFnSc1	republika
Surinam	Surinam	k1gInSc1	Surinam
je	být	k5eAaImIp3nS	být
přímořský	přímořský	k2eAgInSc1d1	přímořský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
a	a	k8xC	a
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
země	zem	k1gFnSc2	zem
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Paramaribu	Paramarib	k1gInSc2	Paramarib
<g/>
.	.	kIx.	.
</s>
<s>
Převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
pralesy	prales	k1gInPc4	prales
a	a	k8xC	a
savany	savana	k1gFnPc4	savana
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejmenší	malý	k2eAgInSc4d3	nejmenší
suverénní	suverénní	k2eAgInSc4d1	suverénní
stát	stát	k1gInSc4	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Surinam	Surinam	k6eAd1	Surinam
vede	vést	k5eAaImIp3nS	vést
hraniční	hraniční	k2eAgInPc4d1	hraniční
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
Guyanou	Guyana	k1gFnSc7	Guyana
i	i	k8xC	i
Guyanou	Guyana	k1gFnSc7	Guyana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
požaduje	požadovat	k5eAaImIp3nS	požadovat
vyřešení	vyřešení	k1gNnSc4	vyřešení
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
námořní	námořní	k2eAgFnSc4d1	námořní
hranici	hranice	k1gFnSc4	hranice
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Surinam	Surinam	k1gInSc1	Surinam
je	být	k5eAaImIp3nS	být
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
podle	podle	k7c2	podle
kmene	kmen	k1gInSc2	kmen
Surinamů	Surinam	k1gInPc2	Surinam
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
prvními	první	k4xOgInPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
ze	z	k7c2	z
Surinamu	Surinam	k1gInSc2	Surinam
ještě	ještě	k9	ještě
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1499	[number]	k4	1499
přistála	přistát	k5eAaPmAgFnS	přistát
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
španělská	španělský	k2eAgFnSc1d1	španělská
výprava	výprava	k1gFnSc1	výprava
Alonsa	Alons	k1gMnSc2	Alons
de	de	k?	de
Hojedy	Hojeda	k1gMnSc2	Hojeda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1667	[number]	k4	1667
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Britů	Brit	k1gMnPc2	Brit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1667	[number]	k4	1667
učinili	učinit	k5eAaImAgMnP	učinit
Britové	Brit	k1gMnPc1	Brit
s	s	k7c7	s
Nizozemci	Nizozemec	k1gMnPc7	Nizozemec
výměnný	výměnný	k2eAgInSc4d1	výměnný
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
Surinam	Surinam	k1gInSc4	Surinam
za	za	k7c4	za
území	území	k1gNnSc4	území
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
osada	osada	k1gFnSc1	osada
Nieuw	Nieuw	k1gFnSc2	Nieuw
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	on	k3xPp3gFnPc4	on
tato	tento	k3xDgFnSc1	tento
původně	původně	k6eAd1	původně
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
osada	osada	k1gFnSc1	osada
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
-	-	kIx~	-
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Surinam	Surinam	k1gInSc1	Surinam
byl	být	k5eAaImAgInS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
k	k	k7c3	k
okolním	okolní	k2eAgInPc3d1	okolní
nizozemským	nizozemský	k2eAgInPc3d1	nizozemský
koloniím	kolonie	k1gFnPc3	kolonie
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
státu	stát	k1gInSc2	stát
Guayana	Guayana	k1gFnSc1	Guayana
pod	pod	k7c7	pod
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1682	[number]	k4	1682
začala	začít	k5eAaPmAgFnS	začít
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
Západoindická	západoindický	k2eAgFnSc1d1	Západoindická
společnost	společnost	k1gFnSc1	společnost
přivážet	přivážet	k5eAaImF	přivážet
do	do	k7c2	do
Surinamu	Surinam	k1gInSc2	Surinam
otroky	otrok	k1gMnPc4	otrok
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Otroci	otrok	k1gMnPc1	otrok
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
<g/>
,	,	kIx,	,
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
čaj	čaj	k1gInSc4	čaj
a	a	k8xC	a
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
otroci	otrok	k1gMnPc1	otrok
utekli	utéct	k5eAaPmAgMnP	utéct
do	do	k7c2	do
buše	buš	k1gFnSc2	buš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
komunity	komunita	k1gFnSc2	komunita
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
potomci	potomek	k1gMnPc1	potomek
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
známí	známý	k1gMnPc1	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
černoši	černoch	k1gMnPc1	černoch
z	z	k7c2	z
buše	buš	k1gFnSc2	buš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
začali	začít	k5eAaPmAgMnP	začít
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
vozit	vozit	k5eAaImF	vozit
námezdní	námezdní	k2eAgFnPc4d1	námezdní
síly	síla	k1gFnPc4	síla
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
pestrost	pestrost	k1gFnSc1	pestrost
obyvatel	obyvatel	k1gMnPc2	obyvatel
ještě	ještě	k6eAd1	ještě
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
získal	získat	k5eAaPmAgInS	získat
Surinam	Surinam	k1gInSc1	Surinam
statut	statut	k1gInSc1	statut
zámořské	zámořský	k2eAgFnSc2d1	zámořská
autonomie	autonomie	k1gFnSc2	autonomie
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Republika	republika	k1gFnSc1	republika
Surinam	Surinam	k1gInSc1	Surinam
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
začaly	začít	k5eAaPmAgInP	začít
etnické	etnický	k2eAgInPc1d1	etnický
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
svržena	svržen	k2eAgFnSc1d1	svržena
civilní	civilní	k2eAgFnSc1d1	civilní
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
chopila	chopit	k5eAaPmAgFnS	chopit
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
humanitární	humanitární	k2eAgFnSc1d1	humanitární
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc1	hospodářství
zkolabovalo	zkolabovat	k5eAaPmAgNnS	zkolabovat
a	a	k8xC	a
"	"	kIx"	"
<g/>
černoši	černoch	k1gMnPc1	černoch
z	z	k7c2	z
buše	buš	k1gFnSc2	buš
<g/>
"	"	kIx"	"
začali	začít	k5eAaPmAgMnP	začít
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
boj	boj	k1gInSc4	boj
za	za	k7c4	za
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
přešla	přejít	k5eAaPmAgFnS	přejít
moc	moc	k1gFnSc1	moc
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
mír	mír	k1gInSc4	mír
s	s	k7c7	s
"	"	kIx"	"
<g/>
černochy	černoch	k1gMnPc4	černoch
z	z	k7c2	z
buše	buš	k1gInSc2	buš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
začíná	začínat	k5eAaImIp3nS	začínat
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
a	a	k8xC	a
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
pomoci	pomoc	k1gFnSc6	pomoc
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Surinamu	Surinam	k1gInSc2	Surinam
Guyanská	Guyanský	k2eAgFnSc1d1	Guyanská
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
vrcholky	vrcholek	k1gInPc1	vrcholek
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
1	[number]	k4	1
000	[number]	k4	000
m	m	kA	m
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
vodopádů	vodopád	k1gInPc2	vodopád
a	a	k8xC	a
peřejí	peřej	k1gFnPc2	peřej
mají	mít	k5eAaImIp3nP	mít
dostatek	dostatek	k1gInSc4	dostatek
vody	voda	k1gFnSc2	voda
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
Courantyne	Courantyn	k1gInSc5	Courantyn
<g/>
,	,	kIx,	,
Marowijne	Marowijn	k1gInSc5	Marowijn
a	a	k8xC	a
Suriname	Surinam	k1gInSc5	Surinam
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
velká	velký	k2eAgFnSc1d1	velká
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
Brokopondo	Brokopondo	k6eAd1	Brokopondo
<g/>
.	.	kIx.	.
</s>
<s>
Tropické	tropický	k2eAgNnSc1d1	tropické
rovníkové	rovníkový	k2eAgNnSc1d1	rovníkové
klima	klima	k1gNnSc1	klima
má	mít	k5eAaImIp3nS	mít
průměrné	průměrný	k2eAgFnPc4d1	průměrná
roční	roční	k2eAgFnPc4d1	roční
teploty	teplota	k1gFnPc4	teplota
mezi	mezi	k7c4	mezi
25,5	[number]	k4	25,5
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
28	[number]	k4	28
°	°	k?	°
<g/>
C.	C.	kA	C.
Paramaribo	Paramariba	k1gFnSc5	Paramariba
má	mít	k5eAaImIp3nS	mít
roční	roční	k2eAgInSc4d1	roční
průměr	průměr	k1gInSc4	průměr
26,5	[number]	k4	26,5
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgInPc1d1	průměrný
roční	roční	k2eAgInPc1d1	roční
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
2	[number]	k4	2
000	[number]	k4	000
mm	mm	kA	mm
do	do	k7c2	do
3	[number]	k4	3
500	[number]	k4	500
mm	mm	kA	mm
<g/>
,	,	kIx,	,
Paramaribo	Paramariba	k1gFnSc5	Paramariba
2	[number]	k4	2
300	[number]	k4	300
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Surinamu	Surinam	k1gInSc2	Surinam
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
-	-	kIx~	-
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Centrální	centrální	k2eAgFnSc1d1	centrální
Surinam	Surinam	k1gInSc1	Surinam
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Surinam	Surinam	k6eAd1	Surinam
sedělí	sedělit	k5eAaPmIp3nS	sedělit
na	na	k7c4	na
10	[number]	k4	10
distriktů	distrikt	k1gInPc2	distrikt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Surinam	Surinam	k6eAd1	Surinam
je	být	k5eAaImIp3nS	být
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
a	a	k8xC	a
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
hospodářství	hospodářství	k1gNnSc1	hospodářství
se	se	k3xPyFc4	se
však	však	k9	však
kromě	kromě	k7c2	kromě
pěstování	pěstování	k1gNnSc2	pěstování
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
věnuje	věnovat	k5eAaPmIp3nS	věnovat
především	především	k6eAd1	především
těžbě	těžba	k1gFnSc3	těžba
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
bauxit	bauxit	k1gInSc4	bauxit
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
ložiska	ložisko	k1gNnPc1	ložisko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
a	a	k8xC	a
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
hliník	hliník	k1gInSc1	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
zásob	zásoba	k1gFnPc2	zásoba
bauxitové	bauxitový	k2eAgFnSc2d1	bauxitový
rudy	ruda	k1gFnSc2	ruda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
peněz	peníze	k1gInPc2	peníze
z	z	k7c2	z
těžby	těžba	k1gFnSc2	těžba
bauxitu	bauxit	k1gInSc2	bauxit
země	zem	k1gFnSc2	zem
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
svojí	svojit	k5eAaImIp3nP	svojit
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
(	(	kIx(	(
<g/>
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgFnPc7d1	další
dvěma	dva	k4xCgFnPc7	dva
významnými	významný	k2eAgFnPc7d1	významná
surovinami	surovina	k1gFnPc7	surovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
těží	těžet	k5eAaImIp3nS	těžet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
těžba	těžba	k1gFnSc1	těžba
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
rozvíjena	rozvíjet	k5eAaImNgFnS	rozvíjet
především	především	k9	především
až	až	k9	až
v	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	oboj	k1gFnSc7	oboj
významně	významně	k6eAd1	významně
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
surinamské	surinamský	k2eAgFnSc2d1	surinamská
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
spaluje	spalovat	k5eAaImIp3nS	spalovat
se	se	k3xPyFc4	se
v	v	k7c6	v
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
75	[number]	k4	75
%	%	kIx~	%
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
produkuje	produkovat	k5eAaImIp3nS	produkovat
hlavní	hlavní	k2eAgFnSc1d1	hlavní
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Surinam	Surinam	k1gInSc4	Surinam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
(	(	kIx(	(
<g/>
95	[number]	k4	95
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Surinamská	Surinamský	k2eAgFnSc1d1	Surinamská
vláda	vláda	k1gFnSc1	vláda
lesy	les	k1gInPc1	les
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
těží	těžet	k5eAaImIp3nS	těžet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
sklizeň	sklizeň	k1gFnSc1	sklizeň
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
banánů	banán	k1gInPc2	banán
<g/>
,	,	kIx,	,
citrusů	citrus	k1gInPc2	citrus
<g/>
,	,	kIx,	,
kokosových	kokosový	k2eAgInPc2d1	kokosový
ořechů	ořech	k1gInPc2	ořech
<g/>
,	,	kIx,	,
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
pro	pro	k7c4	pro
obživu	obživa	k1gFnSc4	obživa
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Obdělává	obdělávat	k5eAaImIp3nS	obdělávat
se	se	k3xPyFc4	se
půda	půda	k1gFnSc1	půda
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
do	do	k7c2	do
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
mořského	mořský	k2eAgNnSc2d1	mořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
řídká	řídký	k2eAgFnSc1d1	řídká
silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Surinam	Surinam	k6eAd1	Surinam
je	být	k5eAaImIp3nS	být
zemí	zem	k1gFnPc2	zem
etnicky	etnicky	k6eAd1	etnicky
i	i	k8xC	i
nábožensky	nábožensky	k6eAd1	nábožensky
velmi	velmi	k6eAd1	velmi
různorodou	různorodý	k2eAgFnSc7d1	různorodá
<g/>
.	.	kIx.	.
38	[number]	k4	38
<g/>
%	%	kIx~	%
veškerého	veškerý	k3xTgNnSc2	veškerý
surinamského	surinamský	k2eAgNnSc2d1	surinamský
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
Indové	Ind	k1gMnPc1	Ind
a	a	k8xC	a
Pákistánci	Pákistánec	k1gMnPc1	Pákistánec
<g/>
,	,	kIx,	,
31,5	[number]	k4	31,5
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
surinamští	surinamský	k2eAgMnPc1d1	surinamský
Kreolové	kreol	k1gMnPc1	kreol
<g/>
,	,	kIx,	,
14	[number]	k4	14
%	%	kIx~	%
"	"	kIx"	"
<g/>
černoši	černoch	k1gMnPc1	černoch
z	z	k7c2	z
buše	buš	k1gFnSc2	buš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
3	[number]	k4	3
%	%	kIx~	%
indiáni	indián	k1gMnPc1	indián
(	(	kIx(	(
<g/>
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2,7	[number]	k4	2,7
%	%	kIx~	%
Číňané	Číňan	k1gMnPc1	Číňan
a	a	k8xC	a
1,3	[number]	k4	1,3
%	%	kIx~	%
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
<g/>
.	.	kIx.	.
</s>
<s>
Pestrost	pestrost	k1gFnSc1	pestrost
náboženství	náboženství	k1gNnPc2	náboženství
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pestrosti	pestrost	k1gFnPc4	pestrost
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
státu	stát	k1gInSc2	stát
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Paramaribu	Paramarib	k1gInSc2	Paramarib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Surinam	Surinam	k1gInSc4	Surinam
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
"	"	kIx"	"
<g/>
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
román	román	k1gInSc1	román
<g/>
"	"	kIx"	"
Annejet	Annejet	k1gInSc1	Annejet
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Zijlové	Zijlový	k2eAgFnPc4d1	Zijlový
Sonny	Sonen	k2eAgFnPc4d1	Sonen
Boy	boa	k1gFnPc4	boa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
archivních	archivní	k2eAgFnPc2d1	archivní
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
doložených	doložený	k2eAgNnPc2d1	doložené
svědectví	svědectví	k1gNnPc2	svědectví
rekonstruuje	rekonstruovat	k5eAaBmIp3nS	rekonstruovat
neobyčejný	obyčejný	k2eNgInSc1d1	neobyčejný
příběh	příběh	k1gInSc1	příběh
Waldemara	Waldemar	k1gMnSc2	Waldemar
ze	z	k7c2	z
Surinamu	Surinam	k1gInSc2	Surinam
a	a	k8xC	a
Riky	Rika	k1gFnSc2	Rika
z	z	k7c2	z
Haagu	Haag	k1gInSc2	Haag
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ukrývali	ukrývat	k5eAaImAgMnP	ukrývat
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
Židy	Žid	k1gMnPc4	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Sonny	Sonen	k2eAgFnPc1d1	Sonen
Boy	boa	k1gFnPc1	boa
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
také	také	k9	také
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
románu	román	k1gInSc2	román
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
s	s	k7c7	s
premiérou	premiéra	k1gFnSc7	premiéra
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Surinam	Surinam	k1gInSc1	Surinam
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Surinam	Surinam	k1gInSc1	Surinam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Suriname	Surinam	k1gInSc5	Surinam
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Western	western	k1gInSc1	western
Hemisphere	Hemispher	k1gInSc5	Hemispher
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Suriname	Surinam	k1gInSc5	Surinam
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-06-13	[number]	k4	2011-06-13
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Suriname	Surinam	k1gInSc5	Surinam
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Brasílii	Brasílie	k1gFnSc6	Brasílie
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Surinam	Surinam	k1gInSc1	Surinam
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-01-31	[number]	k4	2011-01-31
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CHIN	China	k1gFnPc2	China
<g/>
,	,	kIx,	,
Henk	Henk	k1gMnSc1	Henk
E.	E.	kA	E.
<g/>
;	;	kIx,	;
MENKE	MENKE	kA	MENKE
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
K.	K.	kA	K.
Turkey	Turkey	k1gInPc1	Turkey
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
