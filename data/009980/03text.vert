<p>
<s>
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	coli	k6eAd1	coli
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Bacterium	Bacterium	k1gNnSc1	Bacterium
coli	col	k1gFnSc2	col
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
gramnegativní	gramnegativní	k2eAgMnSc1d1	gramnegativní
fakultativně	fakultativně	k6eAd1	fakultativně
anaerobní	anaerobní	k2eAgInPc4d1	anaerobní
spory	spor	k1gInPc4	spor
netvořící	tvořící	k2eNgFnSc1d1	netvořící
tyčinkovitá	tyčinkovitý	k2eAgFnSc1d1	tyčinkovitá
bakterie	bakterie	k1gFnSc1	bakterie
pohybující	pohybující	k2eAgFnSc1d1	pohybující
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
bičíků	bičík	k1gInPc2	bičík
<g/>
.	.	kIx.	.
</s>
<s>
Spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
čeleď	čeleď	k1gFnSc4	čeleď
Enterobacteriaceae	Enterobacteriacea	k1gFnSc2	Enterobacteriacea
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
také	také	k9	také
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mnoho	mnoho	k4c4	mnoho
patogenních	patogenní	k2eAgInPc2d1	patogenní
rodů	rod	k1gInPc2	rod
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
střevní	střevní	k2eAgFnSc3d1	střevní
mikrofloře	mikroflora	k1gFnSc3	mikroflora
teplokrevných	teplokrevný	k2eAgMnPc2d1	teplokrevný
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
indikátorem	indikátor	k1gInSc7	indikátor
fekálního	fekální	k2eAgNnSc2d1	fekální
znečištění	znečištění	k1gNnSc2	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
Člověku	člověk	k1gMnSc3	člověk
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
přirozené	přirozený	k2eAgFnSc2d1	přirozená
mikroflory	mikroflora	k1gFnSc2	mikroflora
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
produkuje	produkovat	k5eAaImIp3nS	produkovat
řadu	řada	k1gFnSc4	řada
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
brání	bránit	k5eAaImIp3nP	bránit
rozšíření	rozšíření	k1gNnSc4	rozšíření
patogenních	patogenní	k2eAgFnPc2d1	patogenní
bakterií	bakterie	k1gFnPc2	bakterie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
koliciny	kolicin	k2eAgMnPc4d1	kolicin
<g/>
)	)	kIx)	)
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
některých	některý	k3yIgInPc2	některý
vitamínů	vitamín	k1gInPc2	vitamín
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
německo-rakouským	německoakouský	k2eAgMnSc7d1	německo-rakouský
pediatrem	pediatr	k1gMnSc7	pediatr
a	a	k8xC	a
bakteriologem	bakteriolog	k1gMnSc7	bakteriolog
Theodorem	Theodor	k1gMnSc7	Theodor
Escherichem	Escherich	k1gMnSc7	Escherich
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejlépe	dobře	k6eAd3	dobře
prostudovaným	prostudovaný	k2eAgInPc3d1	prostudovaný
mikroorganismům	mikroorganismus	k1gInPc3	mikroorganismus
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
modelovým	modelový	k2eAgInSc7d1	modelový
organismem	organismus	k1gInSc7	organismus
pro	pro	k7c4	pro
genové	genový	k2eAgFnPc4d1	genová
a	a	k8xC	a
klinické	klinický	k2eAgFnPc4d1	klinická
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Joshua	Joshua	k1gMnSc1	Joshua
Lederberg	Lederberg	k1gMnSc1	Lederberg
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
r.	r.	kA	r.
1947	[number]	k4	1947
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
na	na	k7c4	na
bakterii	bakterie	k1gFnSc4	bakterie
E.	E.	kA	E.
coli	cole	k1gFnSc4	cole
výměnu	výměna	k1gFnSc4	výměna
genetického	genetický	k2eAgInSc2d1	genetický
materiálu	materiál	k1gInSc2	materiál
tzv.	tzv.	kA	tzv.
konjugaci	konjugace	k1gFnSc4	konjugace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
E.	E.	kA	E.
coli	col	k1gInSc3	col
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Morfologická	morfologický	k2eAgFnSc1d1	morfologická
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
je	být	k5eAaImIp3nS	být
nesporotvorná	sporotvorný	k2eNgFnSc1d1	sporotvorný
tyčinka	tyčinka	k1gFnSc1	tyčinka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pomocí	pomocí	k7c2	pomocí
bičíku	bičík	k1gInSc2	bičík
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
μ	μ	k?	μ
a	a	k8xC	a
šířky	šířka	k1gFnPc1	šířka
0,6	[number]	k4	0,6
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
slizovité	slizovitý	k2eAgInPc4d1	slizovitý
obaly	obal	k1gInPc4	obal
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
nese	nést	k5eAaImIp3nS	nést
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
fimbrií	fimbrie	k1gFnPc2	fimbrie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
typ	typ	k1gInSc1	typ
fimbrií	fimbrie	k1gFnPc2	fimbrie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kyselého	kyselý	k2eAgInSc2d1	kyselý
hydrofobního	hydrofobní	k2eAgInSc2d1	hydrofobní
proteinu	protein	k1gInSc2	protein
tzv.	tzv.	kA	tzv.
fimbrinu	fimbrin	k1gInSc2	fimbrin
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
bakterii	bakterie	k1gFnSc4	bakterie
přichytit	přichytit	k5eAaPmF	přichytit
se	se	k3xPyFc4	se
na	na	k7c4	na
epitel	epitel	k1gInSc4	epitel
hostitele	hostitel	k1gMnSc2	hostitel
a	a	k8xC	a
následně	následně	k6eAd1	následně
jej	on	k3xPp3gMnSc4	on
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
<g/>
.	.	kIx.	.
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1	kolonizace
je	být	k5eAaImIp3nS	být
usnadněna	usnadnit	k5eAaPmNgFnS	usnadnit
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
fimbrií	fimbrie	k1gFnPc2	fimbrie
prvního	první	k4xOgInSc2	první
typu	typ	k1gInSc2	typ
na	na	k7c4	na
buňku	buňka	k1gFnSc4	buňka
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
ks	ks	kA	ks
<g/>
/	/	kIx~	/
<g/>
buňka	buňka	k1gFnSc1	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
fimbrie	fimbrie	k1gFnPc1	fimbrie
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
antigenní	antigenní	k2eAgFnPc1d1	antigenní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tzv.	tzv.	kA	tzv.
F	F	kA	F
antigeny	antigen	k1gInPc7	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
fimbrií	fimbrie	k1gFnPc2	fimbrie
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
sex	sex	k1gInSc4	sex
pili	pít	k5eAaImAgMnP	pít
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
konjugaci	konjugace	k1gFnSc6	konjugace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
pomocí	pomocí	k7c2	pomocí
bičíků	bičík	k1gInPc2	bičík
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
flagelinu	flagelin	k2eAgFnSc4d1	flagelin
(	(	kIx(	(
<g/>
na	na	k7c4	na
lysin	lysin	k1gInSc4	lysin
bohatý	bohatý	k2eAgInSc4d1	bohatý
protein	protein	k1gInSc4	protein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bičíky	bičík	k1gInPc1	bičík
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
fimbrie	fimbrie	k1gFnSc1	fimbrie
vysoce	vysoce	k6eAd1	vysoce
antigenní	antigenní	k2eAgFnSc1d1	antigenní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
tzv.	tzv.	kA	tzv.
H	H	kA	H
antigenům	antigen	k1gInPc3	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
bakterie	bakterie	k1gFnSc2	bakterie
se	se	k3xPyFc4	se
při	při	k7c6	při
stresových	stresový	k2eAgFnPc6d1	stresová
podmínkách	podmínka	k1gFnPc6	podmínka
dále	daleko	k6eAd2	daleko
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
polysacharidové	polysacharidový	k2eAgInPc4d1	polysacharidový
kapsule	kapsule	k?	kapsule
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tzv.	tzv.	kA	tzv.
K	K	kA	K
a	a	k8xC	a
M	M	kA	M
antigeny	antigen	k1gInPc1	antigen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
lipopolysacharidem	lipopolysacharid	k1gInSc7	lipopolysacharid
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
lipidové	lipidový	k2eAgFnSc2d1	lipidová
dvojvrstvy	dvojvrstvy	k?	dvojvrstvy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ukotveno	ukotven	k2eAgNnSc1d1	ukotveno
množství	množství	k1gNnSc1	množství
membránových	membránový	k2eAgInPc2d1	membránový
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
proteiny	protein	k1gInPc7	protein
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
póry	pór	k1gInPc4	pór
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
poriny	porin	k2eAgFnPc4d1	porin
Omp	Omp	k1gFnPc4	Omp
C	C	kA	C
<g/>
,	,	kIx,	,
Omp	Omp	k1gFnPc1	Omp
F	F	kA	F
a	a	k8xC	a
Pho	Pho	k1gFnSc1	Pho
E.	E.	kA	E.
Poriny	Porina	k1gFnPc1	Porina
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
vstupní	vstupní	k2eAgInPc1d1	vstupní
a	a	k8xC	a
výstupní	výstupní	k2eAgInPc1d1	výstupní
kanály	kanál	k1gInPc1	kanál
pro	pro	k7c4	pro
buněčné	buněčný	k2eAgInPc4d1	buněčný
metabolity	metabolit	k1gInPc4	metabolit
a	a	k8xC	a
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
vitaminů	vitamin	k1gInPc2	vitamin
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostor	prostor	k1gInSc1	prostor
mezi	mezi	k7c7	mezi
vnější	vnější	k2eAgFnSc7d1	vnější
membránou	membrána	k1gFnSc7	membrána
a	a	k8xC	a
buněčnou	buněčný	k2eAgFnSc7d1	buněčná
stěnou	stěna	k1gFnSc7	stěna
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
periplazmatický	periplazmatický	k2eAgInSc1d1	periplazmatický
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
např.	např.	kA	např.
proteiny	protein	k1gInPc4	protein
vázající	vázající	k2eAgFnSc2d1	vázající
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
či	či	k8xC	či
cukry	cukr	k1gInPc4	cukr
<g/>
,	,	kIx,	,
enzymy	enzym	k1gInPc4	enzym
degradující	degradující	k2eAgNnPc1d1	degradující
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
(	(	kIx(	(
<g/>
beta-laktamasy	betaaktamas	k1gInPc1	beta-laktamas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buněčná	buněčný	k2eAgFnSc1d1	buněčná
stěna	stěna	k1gFnSc1	stěna
E.	E.	kA	E.
coli	col	k1gFnSc2	col
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
zástupce	zástupce	k1gMnSc2	zástupce
gramnegativních	gramnegativní	k2eAgFnPc2d1	gramnegativní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tenké	tenký	k2eAgFnSc2d1	tenká
vrstvy	vrstva	k1gFnSc2	vrstva
peptidoglykanu	peptidoglykan	k1gInSc2	peptidoglykan
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
rigidní	rigidní	k2eAgInSc4d1	rigidní
tvar	tvar	k1gInSc4	tvar
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
peptidoglykanu	peptidoglykan	k1gInSc2	peptidoglykan
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
z	z	k7c2	z
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lipopolysacharidů	lipopolysacharid	k1gInPc2	lipopolysacharid
a	a	k8xC	a
fosfolipidů	fosfolipid	k1gInPc2	fosfolipid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
lokalizováno	lokalizován	k2eAgNnSc1d1	lokalizováno
mnoho	mnoho	k4c1	mnoho
biochemických	biochemický	k2eAgInPc2d1	biochemický
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dýchací	dýchací	k2eAgInSc1d1	dýchací
řetězec	řetězec	k1gInSc1	řetězec
a	a	k8xC	a
syntéza	syntéza	k1gFnSc1	syntéza
ATP.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Cytoplazma	cytoplazma	k1gFnSc1	cytoplazma
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
buňky	buňka	k1gFnSc2	buňka
je	být	k5eAaImIp3nS	být
viskózní	viskózní	k2eAgInSc1d1	viskózní
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
anorganické	anorganický	k2eAgFnSc2d1	anorganická
a	a	k8xC	a
organické	organický	k2eAgFnSc2d1	organická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
ribozomů	ribozom	k1gInPc2	ribozom
(	(	kIx(	(
<g/>
cca	cca	kA	cca
40	[number]	k4	40
<g/>
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
celé	celý	k2eAgFnSc2d1	celá
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgMnPc3	jenž
je	být	k5eAaImIp3nS	být
proteosyntéza	proteosyntéza	k1gFnSc1	proteosyntéza
a	a	k8xC	a
dělení	dělení	k1gNnSc1	dělení
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněk	buňka	k1gFnPc2	buňka
velice	velice	k6eAd1	velice
rychlé	rychlý	k2eAgNnSc4d1	rychlé
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
optimálních	optimální	k2eAgFnPc6d1	optimální
podmínkách	podmínka	k1gFnPc6	podmínka
(	(	kIx(	(
<g/>
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
dostatek	dostatek	k1gInSc1	dostatek
živin	živina	k1gFnPc2	živina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
generace	generace	k1gFnSc2	generace
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Bakteriální	bakteriální	k2eAgInPc1d1	bakteriální
ribozomy	ribozom	k1gInPc1	ribozom
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
eukaryotní	eukaryotní	k2eAgInPc1d1	eukaryotní
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
sedimentační	sedimentační	k2eAgFnSc4d1	sedimentační
konstantu	konstanta	k1gFnSc4	konstanta
70	[number]	k4	70
<g/>
S.	S.	kA	S.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nalézá	nalézat	k5eAaImIp3nS	nalézat
molekula	molekula	k1gFnSc1	molekula
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
veškerá	veškerý	k3xTgFnSc1	veškerý
dědičná	dědičný	k2eAgFnSc1d1	dědičná
informace	informace	k1gFnSc1	informace
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
DNA	DNA	kA	DNA
u	u	k7c2	u
E.	E.	kA	E.
coli	col	k1gFnSc2	col
K-12	K-12	k1gFnSc2	K-12
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
4700	[number]	k4	4700
kbp	kbp	k?	kbp
a	a	k8xC	a
kóduje	kódovat	k5eAaBmIp3nS	kódovat
cca	cca	kA	cca
4400	[number]	k4	4400
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Cytoplazma	cytoplazma	k1gNnSc1	cytoplazma
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
eukaryot	eukaryota	k1gFnPc2	eukaryota
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
membránové	membránový	k2eAgFnPc4d1	membránová
organely	organela	k1gFnPc4	organela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
je	být	k5eAaImIp3nS	být
fakultativní	fakultativní	k2eAgMnSc1d1	fakultativní
anaerob	anaerob	k1gMnSc1	anaerob
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
využívá	využívat	k5eAaImIp3nS	využívat
respirační	respirační	k2eAgInSc1d1	respirační
i	i	k8xC	i
kvasný	kvasný	k2eAgInSc1d1	kvasný
metabolismus	metabolismus	k1gInSc1	metabolismus
(	(	kIx(	(
<g/>
fermentace	fermentace	k1gFnSc1	fermentace
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
přísun	přísun	k1gInSc4	přísun
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
jako	jako	k8xC	jako
chemoheterotrof	chemoheterotrof	k1gInSc4	chemoheterotrof
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
využívat	využívat	k5eAaPmF	využívat
množství	množství	k1gNnSc4	množství
cukrů	cukr	k1gInPc2	cukr
i	i	k8xC	i
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
nejrychleji	rychle	k6eAd3	rychle
však	však	k9	však
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
glukose	glukosa	k1gFnSc6	glukosa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
anaerobních	anaerobní	k2eAgFnPc2d1	anaerobní
podmínek	podmínka	k1gFnPc2	podmínka
E.	E.	kA	E.
coli	coli	k6eAd1	coli
utilizuje	utilizovat	k5eAaBmIp3nS	utilizovat
glukosu	glukosa	k1gFnSc4	glukosa
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
laktátu	laktát	k1gInSc2	laktát
<g/>
,	,	kIx,	,
sukcinátu	sukcinát	k1gInSc2	sukcinát
<g/>
,	,	kIx,	,
acetátu	acetát	k1gInSc2	acetát
i	i	k8xC	i
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
aerobních	aerobní	k2eAgFnPc2d1	aerobní
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
glukosa	glukosa	k1gFnSc1	glukosa
využita	využít	k5eAaPmNgFnS	využít
efektivněji	efektivně	k6eAd2	efektivně
a	a	k8xC	a
konečnými	konečný	k2eAgInPc7d1	konečný
produkty	produkt	k1gInPc7	produkt
je	být	k5eAaImIp3nS	být
především	především	k9	především
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
produkuje	produkovat	k5eAaImIp3nS	produkovat
indol	indol	k1gInSc1	indol
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neroste	růst	k5eNaImIp3nS	růst
na	na	k7c6	na
citrátu	citrát	k1gInSc6	citrát
a	a	k8xC	a
neprodukuje	produkovat	k5eNaImIp3nS	produkovat
sirovodík	sirovodík	k1gInSc4	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kataláza-pozitivní	katalázaozitivní	k2eAgMnSc1d1	kataláza-pozitivní
<g/>
,	,	kIx,	,
oxidáza-negativní	oxidázaegativní	k2eAgInSc1d1	oxidáza-negativní
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
vlastností	vlastnost	k1gFnPc2	vlastnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
identifikaci	identifikace	k1gFnSc6	identifikace
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
Enterotestu	Enterotest	k1gInSc2	Enterotest
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
růst	růst	k5eAaImF	růst
za	za	k7c4	za
teploty	teplota	k1gFnPc4	teplota
8	[number]	k4	8
°	°	k?	°
<g/>
C-	C-	k1gFnSc1	C-
<g/>
48	[number]	k4	48
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
optimální	optimální	k2eAgFnSc1d1	optimální
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
37	[number]	k4	37
°	°	k?	°
<g/>
C.	C.	kA	C.
Rozsah	rozsah	k1gInSc1	rozsah
pH	ph	kA	ph
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
je	být	k5eAaImIp3nS	být
pH	ph	kA	ph
<g/>
6	[number]	k4	6
<g/>
-pH	H	k?	-pH
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Antigenní	antigenní	k2eAgFnSc1d1	antigenní
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
můžeme	moct	k5eAaImIp1nP	moct
taxonomicky	taxonomicky	k6eAd1	taxonomicky
dělit	dělit	k5eAaImF	dělit
dle	dle	k7c2	dle
antigenních	antigenní	k2eAgFnPc2d1	antigenní
struktur	struktura	k1gFnPc2	struktura
na	na	k7c4	na
sérotypy	sérotyp	k1gInPc4	sérotyp
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
struktury	struktura	k1gFnPc4	struktura
patří	patřit	k5eAaImIp3nS	patřit
somatické	somatický	k2eAgNnSc1d1	somatické
O	o	k7c4	o
antigeny	antigen	k1gInPc4	antigen
(	(	kIx(	(
<g/>
lipopolysacharid	lipopolysacharid	k1gInSc4	lipopolysacharid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
170	[number]	k4	170
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
a	a	k8xC	a
kapsulární	kapsulární	k2eAgInPc1d1	kapsulární
K	K	kA	K
antigeny	antigen	k1gInPc1	antigen
(	(	kIx(	(
<g/>
80	[number]	k4	80
typů	typ	k1gInPc2	typ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
strukturami	struktura	k1gFnPc7	struktura
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
H	H	kA	H
antigeny	antigen	k1gInPc4	antigen
(	(	kIx(	(
<g/>
flagelární	flagelární	k2eAgInPc4d1	flagelární
proteiny	protein	k1gInPc4	protein
<g/>
)	)	kIx)	)
a	a	k8xC	a
F	F	kA	F
antigeny	antigen	k1gInPc1	antigen
(	(	kIx(	(
<g/>
bílkoviny	bílkovina	k1gFnPc1	bílkovina
fimbrií	fimbrie	k1gFnSc7	fimbrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Patogenita	patogenita	k1gFnSc1	patogenita
E.	E.	kA	E.
coli	col	k1gFnSc2	col
==	==	k?	==
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k1gNnSc1	coli
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgFnPc2d1	vyskytující
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
klinických	klinický	k2eAgInPc6d1	klinický
vzorcích	vzorec	k1gInPc6	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
přítomnost	přítomnost	k1gFnSc1	přítomnost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
střevní	střevní	k2eAgFnSc2d1	střevní
mikroflory	mikroflora	k1gFnSc2	mikroflora
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
patologicky	patologicky	k6eAd1	patologicky
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
v	v	k7c6	v
krevních	krevní	k2eAgInPc6d1	krevní
vzorcích	vzorec	k1gInPc6	vzorec
<g/>
,	,	kIx,	,
a	a	k8xC	a
být	být	k5eAaImF	být
tak	tak	k9	tak
původcem	původce	k1gMnSc7	původce
bakterémie	bakterémie	k1gFnSc2	bakterémie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nosokomiální	nosokomiální	k2eAgFnPc4d1	nosokomiální
infekce	infekce	k1gFnPc4	infekce
tj.	tj.	kA	tj.
infekce	infekce	k1gFnSc2	infekce
získané	získaný	k2eAgFnSc2d1	získaná
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
infekce	infekce	k1gFnSc1	infekce
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nS	léčit
podáváním	podávání	k1gNnSc7	podávání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
však	však	k9	však
nalézáme	nalézat	k5eAaImIp1nP	nalézat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
kmenů	kmen	k1gInPc2	kmen
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
podávaným	podávaný	k2eAgNnPc3d1	podávané
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patogenní	patogenní	k2eAgInPc1d1	patogenní
kmeny	kmen	k1gInPc1	kmen
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
extraintestiální	extraintestiální	k2eAgNnPc4d1	extraintestiální
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
napadeny	napaden	k2eAgFnPc4d1	napadena
především	především	k6eAd1	především
močové	močový	k2eAgFnPc4d1	močová
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
ran	rána	k1gFnPc2	rána
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
hnisání	hnisání	k1gNnSc2	hnisání
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnSc1	bakterie
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
intestinálního	intestinální	k2eAgInSc2d1	intestinální
traktu	trakt	k1gInSc2	trakt
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
infekce	infekce	k1gFnPc1	infekce
provázené	provázený	k2eAgFnPc1d1	provázená
průjmy	průjem	k1gInPc4	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Extraintestiální	Extraintestiální	k2eAgFnPc1d1	Extraintestiální
formy	forma	k1gFnPc1	forma
onemocnění	onemocnění	k1gNnPc2	onemocnění
jsou	být	k5eAaImIp3nP	být
vyvolávány	vyvolávat	k5eAaImNgInP	vyvolávat
kmeny	kmen	k1gInPc1	kmen
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
polysacharidový	polysacharidový	k2eAgInSc4d1	polysacharidový
kapsulární	kapsulární	k2eAgInSc4d1	kapsulární
K	K	kA	K
antigen	antigen	k1gInSc4	antigen
<g/>
,	,	kIx,	,
příp	příp	kA	příp
P	P	kA	P
fimbrie	fimbrie	k1gFnPc1	fimbrie
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
adherují	adherovat	k5eAaImIp3nP	adherovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnSc1	bakterie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
jako	jako	k8xS	jako
o	o	k7c6	o
enteropatogenním	enteropatogenní	k2eAgInSc6d1	enteropatogenní
kmenu	kmen	k1gInSc6	kmen
E.	E.	kA	E.
coli	coli	k6eAd1	coli
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Enteropatogenní	Enteropatogenní	k2eAgFnSc7d1	Enteropatogenní
(	(	kIx(	(
<g/>
EPEC	EPEC	kA	EPEC
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
EPEC	EPEC	kA	EPEC
E.	E.	kA	E.
coli	coli	k6eAd1	coli
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
vodnaté	vodnatý	k2eAgInPc4d1	vodnatý
průjmy	průjem	k1gInPc4	průjem
především	především	k9	především
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
stupni	stupeň	k1gInSc3	stupeň
dehydratace	dehydratace	k1gFnSc2	dehydratace
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
problémem	problém	k1gInSc7	problém
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Frekvenci	frekvence	k1gFnSc4	frekvence
infekce	infekce	k1gFnSc2	infekce
způsobené	způsobený	k2eAgFnSc2d1	způsobená
EPEC	EPEC	kA	EPEC
E.	E.	kA	E.
coli	coli	k6eAd1	coli
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
starších	starý	k2eAgMnPc2d2	starší
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
po	po	k7c6	po
příčině	příčina	k1gFnSc6	příčina
nezávažného	závažný	k2eNgInSc2d1	nezávažný
průjmu	průjem	k1gInSc2	průjem
nepátrá	pátrat	k5eNaImIp3nS	pátrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Enterotoxigenní	Enterotoxigenní	k2eAgFnSc7d1	Enterotoxigenní
(	(	kIx(	(
<g/>
ETEC	ETEC	kA	ETEC
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
ETEC	ETEC	kA	ETEC
E.	E.	kA	E.
coli	coli	k6eAd1	coli
též	též	k9	též
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
průjmové	průjmový	k2eAgInPc4d1	průjmový
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tak	tak	k9	tak
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kmen	kmen	k1gInSc1	kmen
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
s	s	k7c7	s
cestovateli	cestovatel	k1gMnPc7	cestovatel
<g/>
.	.	kIx.	.
</s>
<s>
ETEC	ETEC	kA	ETEC
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tvorbou	tvorba	k1gFnSc7	tvorba
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
enterotoxinů-termolabilního	enterotoxinůermolabilní	k2eAgNnSc2d1	enterotoxinů-termolabilní
(	(	kIx(	(
<g/>
TL	TL	kA	TL
<g/>
)	)	kIx)	)
a	a	k8xC	a
termostabilního	termostabilní	k2eAgMnSc2d1	termostabilní
(	(	kIx(	(
<g/>
TS	ts	k0	ts
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
těchto	tento	k3xDgInPc2	tento
toxinů	toxin	k1gInPc2	toxin
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
na	na	k7c6	na
bakteriálních	bakteriální	k2eAgInPc6d1	bakteriální
plasmidech	plasmid	k1gInPc6	plasmid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TL	TL	kA	TL
enterotoxin	enterotoxin	k1gInSc1	enterotoxin
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
polypeptidové	polypeptidový	k2eAgFnSc2d1	polypeptidový
podjednotky	podjednotka	k1gFnSc2	podjednotka
A	a	k9	a
o	o	k7c6	o
molekulární	molekulární	k2eAgFnSc6d1	molekulární
hmotnosti	hmotnost	k1gFnSc6	hmotnost
25	[number]	k4	25
kDa	kDa	k?	kDa
a	a	k8xC	a
pěti	pět	k4xCc2	pět
podjednotek	podjednotka	k1gFnPc2	podjednotka
B	B	kA	B
o	o	k7c6	o
molekulární	molekulární	k2eAgFnSc6d1	molekulární
hmotnosti	hmotnost	k1gFnSc6	hmotnost
11,5	[number]	k4	11,5
kDa	kDa	k?	kDa
<g/>
.	.	kIx.	.
</s>
<s>
Podjednotka	podjednotka	k1gFnSc1	podjednotka
B	B	kA	B
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
epitelové	epitelový	k2eAgFnPc4d1	epitelová
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
translokaci	translokace	k1gFnSc4	translokace
podjednotky	podjednotka	k1gFnSc2	podjednotka
A	a	k9	a
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
katalyzuje	katalyzovat	k5eAaBmIp3nS	katalyzovat
aktivaci	aktivace	k1gFnSc3	aktivace
cAMP	camp	k1gInSc4	camp
<g/>
.	.	kIx.	.
</s>
<s>
Enzymatickou	enzymatický	k2eAgFnSc7d1	enzymatická
kaskádou	kaskáda	k1gFnSc7	kaskáda
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
sekreci	sekrece	k1gFnSc3	sekrece
sodíku	sodík	k1gInSc2	sodík
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
úniků	únik	k1gInPc2	únik
chloridů	chlorid	k1gInPc2	chlorid
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následek	k1gInSc7	následek
jsou	být	k5eAaImIp3nP	být
vodnaté	vodnatý	k2eAgInPc1d1	vodnatý
průjmy	průjem	k1gInPc1	průjem
<g/>
.	.	kIx.	.
</s>
<s>
ST	St	kA	St
enterotoxin	enterotoxin	k1gMnSc1	enterotoxin
je	být	k5eAaImIp3nS	být
slabě	slabě	k6eAd1	slabě
imunogenní	imunogenní	k2eAgNnSc1d1	imunogenní
<g/>
.	.	kIx.	.
</s>
<s>
Aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
guanylátcyklázu	guanylátcykláza	k1gFnSc4	guanylátcykláza
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
koncentrace	koncentrace	k1gFnSc1	koncentrace
cGMP	cGMP	k?	cGMP
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
mechanismus	mechanismus	k1gInSc1	mechanismus
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
určitou	určitý	k2eAgFnSc4d1	určitá
úlohu	úloha	k1gFnSc4	úloha
hraje	hrát	k5eAaImIp3nS	hrát
vápník	vápník	k1gInSc1	vápník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Enteroinvazivní	Enteroinvazivní	k2eAgFnSc7d1	Enteroinvazivní
(	(	kIx(	(
<g/>
EIEC	EIEC	kA	EIEC
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
EIEC	EIEC	kA	EIEC
E.	E.	kA	E.
coli	col	k1gMnPc1	col
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
připomíná	připomínat	k5eAaImIp3nS	připomínat
průběh	průběh	k1gInSc4	průběh
bacilární	bacilární	k2eAgFnSc2d1	bacilární
dysenterie	dysenterie	k1gFnSc2	dysenterie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
epidemiologii	epidemiologie	k1gFnSc6	epidemiologie
EIEC	EIEC	kA	EIEC
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
její	její	k3xOp3gNnSc1	její
lidský	lidský	k2eAgInSc1d1	lidský
ani	ani	k8xC	ani
zvířecí	zvířecí	k2eAgInSc1d1	zvířecí
rezervoár	rezervoár	k1gInSc1	rezervoár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
hygienou	hygiena	k1gFnSc7	hygiena
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
EIEC	EIEC	kA	EIEC
až	až	k6eAd1	až
5	[number]	k4	5
<g/>
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
průjmů	průjem	k1gInPc2	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
sérologickou	sérologický	k2eAgFnSc7d1	sérologická
skupinou	skupina	k1gFnSc7	skupina
je	být	k5eAaImIp3nS	být
O	o	k7c4	o
<g/>
124	[number]	k4	124
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Enterohemoragické	Enterohemoragický	k2eAgInPc1d1	Enterohemoragický
(	(	kIx(	(
<g/>
EHEC	EHEC	kA	EHEC
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
EHEC	EHEC	kA	EHEC
E.	E.	kA	E.
coli	coli	k1gNnPc4	coli
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
EPEC	EPEC	kA	EPEC
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
adheze	adheze	k1gFnPc4	adheze
na	na	k7c4	na
stěny	stěna	k1gFnPc4	stěna
endotelu	endotel	k1gInSc2	endotel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
EPEC	EPEC	kA	EPEC
E.	E.	kA	E.
coli	coli	k6eAd1	coli
se	se	k3xPyFc4	se
však	však	k9	však
EHEC	EHEC	kA	EHEC
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
endotelie	endotelie	k1gFnPc4	endotelie
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
produkuje	produkovat	k5eAaImIp3nS	produkovat
zde	zde	k6eAd1	zde
toxin	toxin	k1gInSc1	toxin
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
shigella	shigella	k1gFnSc1	shigella
toxin	toxin	k1gInSc1	toxin
<g/>
,	,	kIx,	,
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
zvaný	zvaný	k2eAgInSc1d1	zvaný
verotoxin	verotoxin	k1gInSc1	verotoxin
<g/>
.	.	kIx.	.
</s>
<s>
Verotoxin	Verotoxin	k1gMnSc1	Verotoxin
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
poškození	poškození	k1gNnSc4	poškození
sliznice	sliznice	k1gFnSc2	sliznice
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
krvavým	krvavý	k2eAgMnPc3d1	krvavý
průjmům	průjem	k1gInPc3	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tento	tento	k3xDgInSc4	tento
kmen	kmen	k1gInSc4	kmen
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
kolitida	kolitida	k1gFnSc1	kolitida
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
pacientů	pacient	k1gMnPc2	pacient
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc1	onemocnění
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
hemolyticko-uremického	hemolytickoremický	k2eAgInSc2d1	hemolyticko-uremický
syndromu	syndrom	k1gInSc2	syndrom
(	(	kIx(	(
<g/>
HUS	Hus	k1gMnSc1	Hus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
infikované	infikovaný	k2eAgNnSc1d1	infikované
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Identifikace	identifikace	k1gFnPc1	identifikace
EHEC	EHEC	kA	EHEC
E.	E.	kA	E.
coli	coli	k6eAd1	coli
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
unikátní	unikátní	k2eAgFnSc4d1	unikátní
neschopnost	neschopnost	k1gFnSc4	neschopnost
utilizovat	utilizovat	k5eAaBmF	utilizovat
sorbitol	sorbitol	k1gInSc4	sorbitol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
kmene	kmen	k1gInSc2	kmen
EHEC	EHEC	kA	EHEC
je	být	k5eAaImIp3nS	být
sérotyp	sérotyp	k1gInSc1	sérotyp
E.	E.	kA	E.
coli	coli	k6eAd1	coli
O	o	k7c4	o
<g/>
157	[number]	k4	157
<g/>
:	:	kIx,	:
<g/>
H	H	kA	H
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc1d1	významný
sérotypy	sérotyp	k1gInPc1	sérotyp
E.	E.	kA	E.
coli	coli	k6eAd1	coli
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
E.	E.	kA	E.
coli	coli	k6eAd1	coli
nissle	nissle	k6eAd1	nissle
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgMnS	být
izolován	izolovat	k5eAaBmNgMnS	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
profesorem	profesor	k1gMnSc7	profesor
Alfrédem	Alfréd	k1gMnSc7	Alfréd
Nissle	Nissle	k1gMnSc7	Nissle
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
přítomnost	přítomnost	k1gFnSc1	přítomnost
tohoto	tento	k3xDgInSc2	tento
sérotypu	sérotyp	k1gInSc2	sérotyp
ve	v	k7c6	v
střevní	střevní	k2eAgFnSc6d1	střevní
mikroflóře	mikroflóra	k1gFnSc6	mikroflóra
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
ulcerózní	ulcerózní	k2eAgFnSc2d1	ulcerózní
kolitidy	kolitis	k1gFnSc2	kolitis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
znovu	znovu	k6eAd1	znovu
experimentálně	experimentálně	k6eAd1	experimentálně
potvrzován	potvrzován	k2eAgInSc1d1	potvrzován
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostatní	ostatní	k2eAgInPc1d1	ostatní
sérotypy	sérotyp	k1gInPc1	sérotyp
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
nemají	mít	k5eNaImIp3nP	mít
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
ve	v	k7c4	v
střeve	střeev	k1gFnPc4	střeev
může	moct	k5eAaImIp3nS	moct
snižovat	snižovat	k5eAaImF	snižovat
účinnost	účinnost	k1gFnSc4	účinnost
E.	E.	kA	E.
coli	coli	k6eAd1	coli
nissle	nissle	k6eAd1	nissle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
mikrobiologové	mikrobiolog	k1gMnPc1	mikrobiolog
snaží	snažit	k5eAaImIp3nP	snažit
vyvinout	vyvinout	k5eAaPmF	vyvinout
léčbu	léčba	k1gFnSc4	léčba
ulcerózní	ulcerózní	k2eAgFnSc2d1	ulcerózní
kolitidy	kolitis	k1gFnSc2	kolitis
pomocí	pomocí	k7c2	pomocí
osazení	osazení	k1gNnSc2	osazení
střeva	střevo	k1gNnSc2	střevo
pacienta	pacient	k1gMnSc2	pacient
E.	E.	kA	E.
coli	col	k1gFnSc2	col
nissle	nissle	k6eAd1	nissle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
E.	E.	kA	E.
coli	coli	k6eAd1	coli
O	o	k7c4	o
<g/>
157	[number]	k4	157
<g/>
:	:	kIx,	:
H7	H7	k1gFnSc2	H7
===	===	k?	===
</s>
</p>
<p>
<s>
Sérotyp	Sérotyp	k1gMnSc1	Sérotyp
E.	E.	kA	E.
coli	coli	k1gNnSc2	coli
O	o	k7c4	o
<g/>
157	[number]	k4	157
<g/>
:	:	kIx,	:
H7	H7	k1gFnSc1	H7
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
zástupce	zástupce	k1gMnSc2	zástupce
E.	E.	kA	E.
coli	col	k1gFnSc2	col
EHEC	EHEC	kA	EHEC
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
HUS	Hus	k1gMnSc1	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Reservoirem	Reservoir	k1gInSc7	Reservoir
tohoto	tento	k3xDgNnSc2	tento
patogena	patogeno	k1gNnSc2	patogeno
je	být	k5eAaImIp3nS	být
především	především	k9	především
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
přežvýkavci	přežvýkavec	k1gMnPc1	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
potravou	potrava	k1gFnSc7	potrava
(	(	kIx(	(
<g/>
tepelně	tepelně	k6eAd1	tepelně
neupravené	upravený	k2eNgNnSc1d1	neupravené
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
mléko	mléko	k1gNnSc1	mléko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
při	při	k7c6	při
fekálním	fekální	k2eAgNnSc6d1	fekální
znečištění	znečištění	k1gNnSc6	znečištění
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
křížovou	křížový	k2eAgFnSc7d1	křížová
kontaminací	kontaminace	k1gFnSc7	kontaminace
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
jednotky	jednotka	k1gFnPc1	jednotka
bakterií	bakterium	k1gNnPc2	bakterium
na	na	k7c4	na
gram	gram	k1gInSc4	gram
potravy	potrava	k1gFnSc2	potrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
serotyp	serotyp	k1gInSc1	serotyp
izolován	izolován	k2eAgInSc1d1	izolován
r.	r.	kA	r.
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
E.	E.	kA	E.
coli	coli	k1gNnSc1	coli
K-12	K-12	k1gMnSc1	K-12
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
sérotyp	sérotyp	k1gInSc1	sérotyp
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
izolován	izolován	k2eAgInSc4d1	izolován
r.	r.	kA	r.
1922	[number]	k4	1922
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Stanfordu	Stanford	k1gInSc6	Stanford
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
stolice	stolice	k1gFnSc2	stolice
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
coli	coli	k1gNnSc1	coli
K-12	K-12	k1gMnSc1	K-12
se	se	k3xPyFc4	se
nejprobádanějším	probádaný	k2eAgMnSc7d3	probádaný
zástupcem	zástupce	k1gMnSc7	zástupce
E.	E.	kA	E.
coli	coli	k6eAd1	coli
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
přítomnost	přítomnost	k1gFnSc4	přítomnost
lysogenního	lysogenní	k2eAgMnSc2d1	lysogenní
bakteriofága	bakteriofág	k1gMnSc2	bakteriofág
lambda	lambda	k1gNnSc2	lambda
v	v	k7c6	v
bakterii	bakterie	k1gFnSc6	bakterie
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
široká	široký	k2eAgFnSc1d1	široká
vybavenost	vybavenost	k1gFnSc1	vybavenost
E.	E.	kA	E.
coli	col	k1gMnSc3	col
K-12	K-12	k1gMnSc3	K-12
množstvím	množství	k1gNnSc7	množství
plasmidů	plasmid	k1gMnPc2	plasmid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dává	dávat	k5eAaImIp3nS	dávat
široké	široký	k2eAgFnPc4d1	široká
možnosti	možnost	k1gFnPc4	možnost
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
genovém	genový	k2eAgNnSc6d1	genové
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sérotyp	Sérotyp	k1gInSc1	Sérotyp
E.	E.	kA	E.
coli	col	k1gFnSc2	col
K-12	K-12	k1gFnSc2	K-12
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
např.	např.	kA	např.
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
metabolismu	metabolismus	k1gInSc2	metabolismus
dusíku	dusík	k1gInSc2	dusík
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
biosyntéze	biosyntéza	k1gFnSc6	biosyntéza
L-tryptofanu	Lryptofan	k1gInSc2	L-tryptofan
z	z	k7c2	z
indolu	indol	k1gInSc2	indol
a	a	k8xC	a
L-serinu	Lerin	k1gInSc2	L-serin
a	a	k8xC	a
také	také	k9	také
při	při	k7c6	při
studiích	studie	k1gFnPc6	studie
konjugace	konjugace	k1gFnSc2	konjugace
bakteriích	bakterie	k1gFnPc6	bakterie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
infekcí	infekce	k1gFnPc2	infekce
způsobených	způsobený	k2eAgFnPc2d1	způsobená
E.	E.	kA	E.
coli	coli	k6eAd1	coli
==	==	k?	==
</s>
</p>
<p>
<s>
Bakteriální	bakteriální	k2eAgFnPc1d1	bakteriální
infekce	infekce	k1gFnPc1	infekce
jsou	být	k5eAaImIp3nP	být
léčeny	léčen	k2eAgFnPc1d1	léčena
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
(	(	kIx(	(
<g/>
beta-laktamová	betaaktamový	k2eAgNnPc1d1	beta-laktamový
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
fluorochinolony	fluorochinolon	k1gInPc1	fluorochinolon
a	a	k8xC	a
aminoglykosidy	aminoglykosid	k1gInPc1	aminoglykosid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
E.	E.	kA	E.
coli	coli	k6eAd1	coli
má	mít	k5eAaImIp3nS	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
generační	generační	k2eAgFnSc4d1	generační
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
stejně	stejně	k9	stejně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
bakterií	bakterie	k1gFnPc2	bakterie
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
vyvinout	vyvinout	k5eAaPmF	vyvinout
rezistence	rezistence	k1gFnSc2	rezistence
k	k	k7c3	k
používanému	používaný	k2eAgNnSc3d1	používané
antibiotiku	antibiotikum	k1gNnSc3	antibiotikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rezistence	rezistence	k1gFnPc1	rezistence
k	k	k7c3	k
beta-laktamovým	betaaktamový	k2eAgNnPc3d1	beta-laktamový
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
===	===	k?	===
</s>
</p>
<p>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
účinku	účinek	k1gInSc2	účinek
beta-laktamových	betaaktamový	k2eAgNnPc2d1	beta-laktamový
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
(	(	kIx(	(
<g/>
penicilin	penicilin	k1gInSc1	penicilin
<g/>
,	,	kIx,	,
ampicilin	ampicilin	k1gInSc1	ampicilin
<g/>
,	,	kIx,	,
cefalosporiny	cefalosporin	k1gInPc1	cefalosporin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
beta-laktamový	betaaktamový	k2eAgInSc1d1	beta-laktamový
kruh	kruh	k1gInSc1	kruh
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
naruší	narušit	k5eAaPmIp3nS	narušit
syntézu	syntéza	k1gFnSc4	syntéza
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
není	být	k5eNaImIp3nS	být
chráněná	chráněný	k2eAgFnSc1d1	chráněná
<g/>
,	,	kIx,	,
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
může	moct	k5eAaImIp3nS	moct
produkovat	produkovat	k5eAaImF	produkovat
enzym	enzym	k1gInSc4	enzym
tzv.	tzv.	kA	tzv.
beta-laktamázu	betaaktamáza	k1gFnSc4	beta-laktamáza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
beta-laktamový	betaaktamový	k2eAgInSc4d1	beta-laktamový
kruh	kruh	k1gInSc4	kruh
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
imunní	imunní	k2eAgFnSc1d1	imunní
k	k	k7c3	k
působení	působení	k1gNnSc3	působení
tohoto	tento	k3xDgNnSc2	tento
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Geny	gen	k1gInPc1	gen
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
rezistenci	rezistence	k1gFnSc4	rezistence
bývají	bývat	k5eAaImIp3nP	bývat
uloženy	uložit	k5eAaPmNgFnP	uložit
na	na	k7c4	na
plasmidu	plasmida	k1gFnSc4	plasmida
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
velice	velice	k6eAd1	velice
snadno	snadno	k6eAd1	snadno
přístupná	přístupný	k2eAgFnSc1d1	přístupná
k	k	k7c3	k
předávání	předávání	k1gNnSc3	předávání
jiným	jiný	k2eAgFnPc3d1	jiná
bakteriím	bakterie	k1gFnPc3	bakterie
(	(	kIx(	(
<g/>
i	i	k9	i
mezidruhově	mezidruhově	k6eAd1	mezidruhově
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
informace	informace	k1gFnSc2	informace
uložené	uložený	k2eAgFnSc2d1	uložená
na	na	k7c4	na
plasmidu	plasmida	k1gFnSc4	plasmida
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemusí	muset	k5eNaImIp3nS	muset
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
buňky	buňka	k1gFnSc2	buňka
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
předání	předání	k1gNnSc3	předání
oběma	dva	k4xCgFnPc3	dva
dceřiným	dceřin	k2eAgFnPc3d1	dceřina
buňkám	buňka	k1gFnPc3	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Rezistentní	rezistentní	k2eAgFnSc1d1	rezistentní
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
by	by	kYmCp3nS	by
rezistenci	rezistence	k1gFnSc4	rezistence
nenesla	nést	k5eNaImAgFnS	nést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
E.	E.	kA	E.
coli	col	k1gFnSc2	col
cca	cca	kA	cca
ze	z	k7c2	z
60	[number]	k4	60
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
k	k	k7c3	k
podávání	podávání	k1gNnSc3	podávání
aminopenicilinů	aminopenicilin	k1gInPc2	aminopenicilin
a	a	k8xC	a
z	z	k7c2	z
15	[number]	k4	15
<g/>
%	%	kIx~	%
k	k	k7c3	k
cefalosporinům	cefalosporin	k1gInPc3	cefalosporin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rezistence	rezistence	k1gFnSc1	rezistence
k	k	k7c3	k
fluorochinolonům	fluorochinolon	k1gInPc3	fluorochinolon
===	===	k?	===
</s>
</p>
<p>
<s>
Fluorochinolony	Fluorochinolon	k1gInPc1	Fluorochinolon
jsou	být	k5eAaImIp3nP	být
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
inhibují	inhibovat	k5eAaBmIp3nP	inhibovat
replikaci	replikace	k1gFnSc4	replikace
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
bakterie	bakterie	k1gFnPc4	bakterie
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
rezistenci	rezistence	k1gFnSc4	rezistence
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
různé	různý	k2eAgInPc4d1	různý
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgMnSc7d3	nejčastější
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
efluxu	eflux	k1gInSc2	eflux
–	–	k?	–
tj.	tj.	kA	tj.
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
se	se	k3xPyFc4	se
antibiotikum	antibiotikum	k1gNnSc1	antibiotikum
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
membránových	membránový	k2eAgFnPc2d1	membránová
pump	pumpa	k1gFnPc2	pumpa
vyčerpáno	vyčerpán	k2eAgNnSc1d1	vyčerpáno
mimo	mimo	k7c4	mimo
buňku	buňka	k1gFnSc4	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgInSc7	druhý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnSc1	bakterie
brání	bránit	k5eAaImIp3nS	bránit
působení	působení	k1gNnSc4	působení
fluorochinolonů	fluorochinolon	k1gInPc2	fluorochinolon
je	být	k5eAaImIp3nS	být
pozměnění	pozměnění	k1gNnSc1	pozměnění
(	(	kIx(	(
<g/>
methylace	methylace	k1gFnSc1	methylace
<g/>
,	,	kIx,	,
adenylace	adenylace	k1gFnSc1	adenylace
<g/>
)	)	kIx)	)
jejich	jejich	k3xOp3gFnPc2	jejich
cílových	cílový	k2eAgFnPc2d1	cílová
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
replikaci	replikace	k1gFnSc4	replikace
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
enzymy	enzym	k1gInPc1	enzym
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
DNA	dna	k1gFnSc1	dna
gyráza	gyráza	k1gFnSc1	gyráza
a	a	k8xC	a
topoisomeráza	topoisomeráza	k1gFnSc1	topoisomeráza
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
rezistence	rezistence	k1gFnSc1	rezistence
kmenů	kmen	k1gInPc2	kmen
E.	E.	kA	E.
coli	coli	k1gNnSc1	coli
k	k	k7c3	k
fluorochinolonům	fluorochinolon	k1gInPc3	fluorochinolon
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
25	[number]	k4	25
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
evropský	evropský	k2eAgInSc4d1	evropský
průměr	průměr	k1gInSc4	průměr
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
===	===	k?	===
Rezistence	rezistence	k1gFnPc1	rezistence
k	k	k7c3	k
aminoglykosidům	aminoglykosid	k1gMnPc3	aminoglykosid
===	===	k?	===
</s>
</p>
<p>
<s>
Aminoglykosidy	Aminoglykosida	k1gFnPc1	Aminoglykosida
jsou	být	k5eAaImIp3nP	být
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
blokují	blokovat	k5eAaImIp3nP	blokovat
proces	proces	k1gInSc4	proces
translace	translace	k1gFnSc2	translace
tj.	tj.	kA	tj.
přepis	přepis	k1gInSc1	přepis
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
řeči	řeč	k1gFnSc2	řeč
nukleotidů	nukleotid	k1gInPc2	nukleotid
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
translace	translace	k1gFnSc1	translace
probíhá	probíhat	k5eAaImIp3nS	probíhat
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
ribozomy	ribozom	k1gInPc1	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Rezistence	rezistence	k1gFnSc1	rezistence
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
modifikaci	modifikace	k1gFnSc6	modifikace
ribozomů	ribozom	k1gInPc2	ribozom
(	(	kIx(	(
<g/>
methylace	methylace	k1gFnSc1	methylace
<g/>
,	,	kIx,	,
adenylace	adenylace	k1gFnSc1	adenylace
<g/>
)	)	kIx)	)
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
pro	pro	k7c4	pro
aminoglykosidová	aminoglykosidový	k2eAgNnPc4d1	aminoglykosidový
antibiotika	antibiotikum	k1gNnPc4	antibiotikum
biologicky	biologicky	k6eAd1	biologicky
inertní	inertní	k2eAgFnPc1d1	inertní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
výskyt	výskyt	k1gInSc1	výskyt
E.	E.	kA	E.
coli	coli	k6eAd1	coli
rezistentní	rezistentní	k2eAgFnSc1d1	rezistentní
k	k	k7c3	k
aminoglykosidům	aminoglykosid	k1gInPc3	aminoglykosid
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
stále	stále	k6eAd1	stále
průměrné	průměrný	k2eAgNnSc4d1	průměrné
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
E.	E.	kA	E.
coli	coli	k1gNnSc1	coli
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
přirozené	přirozený	k2eAgFnSc2d1	přirozená
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
teplokrevných	teplokrevný	k2eAgMnPc2d1	teplokrevný
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
a	a	k8xC	a
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
touto	tento	k3xDgFnSc7	tento
bakterií	bakterie	k1gFnSc7	bakterie
kolonizován	kolonizovat	k5eAaBmNgMnS	kolonizovat
už	už	k6eAd1	už
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
(	(	kIx(	(
<g/>
kontaminace	kontaminace	k1gFnSc1	kontaminace
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
přenos	přenos	k1gInSc4	přenos
z	z	k7c2	z
již	již	k6eAd1	již
kolonizovaného	kolonizovaný	k2eAgMnSc2d1	kolonizovaný
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejlépe	dobře	k6eAd3	dobře
prostudovaným	prostudovaný	k2eAgNnPc3d1	prostudované
známým	známý	k2eAgNnPc3d1	známé
bakteriím	bakterium	k1gNnPc3	bakterium
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
jejího	její	k3xOp3gNnSc2	její
využití	využití	k1gNnSc2	využití
v	v	k7c6	v
biotechnologiích	biotechnologie	k1gFnPc6	biotechnologie
a	a	k8xC	a
genovém	genový	k2eAgNnSc6d1	genové
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
klady	klad	k1gInPc4	klad
jakožto	jakožto	k8xS	jakožto
zástupce	zástupce	k1gMnSc1	zástupce
prokaryot	prokaryot	k1gMnSc1	prokaryot
patří	patřit	k5eAaImIp3nS	patřit
rychlý	rychlý	k2eAgInSc4d1	rychlý
růst	růst	k1gInSc4	růst
a	a	k8xC	a
levná	levný	k2eAgNnPc4d1	levné
kultivační	kultivační	k2eAgNnPc4d1	kultivační
media	medium	k1gNnPc4	medium
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
použití	použití	k1gNnSc2	použití
E.	E.	kA	E.
coli	coli	k6eAd1	coli
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
její	její	k3xOp3gFnSc4	její
neschopnost	neschopnost	k1gFnSc4	neschopnost
provádět	provádět	k5eAaImF	provádět
posttranslační	posttranslační	k2eAgFnPc4d1	posttranslační
modifikace	modifikace	k1gFnPc4	modifikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
glykosylace	glykosylace	k1gFnSc1	glykosylace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovýchto	takovýto	k3xDgInPc6	takovýto
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
využívána	využíván	k2eAgFnSc1d1	využívána
kvasinka	kvasinka	k1gFnSc1	kvasinka
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
cerevisiae	cerevisiaat	k5eAaPmIp3nS	cerevisiaat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Genové	genový	k2eAgNnSc1d1	genové
inženýrství	inženýrství	k1gNnSc1	inženýrství
===	===	k?	===
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
je	být	k5eAaImIp3nS	být
nejběžněji	běžně	k6eAd3	běžně
používanou	používaný	k2eAgFnSc7d1	používaná
bakterií	bakterie	k1gFnSc7	bakterie
v	v	k7c6	v
metodách	metoda	k1gFnPc6	metoda
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kultivace	kultivace	k1gFnSc1	kultivace
je	být	k5eAaImIp3nS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
bohatých	bohatý	k2eAgMnPc2d1	bohatý
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
zdroje	zdroj	k1gInSc2	zdroj
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
esenciálních	esenciální	k2eAgFnPc2d1	esenciální
látek	látka	k1gFnPc2	látka
tato	tento	k3xDgNnPc1	tento
média	médium	k1gNnPc1	médium
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
a	a	k8xC	a
vitaminy	vitamin	k1gInPc1	vitamin
<g/>
)	)	kIx)	)
i	i	k8xC	i
minimálních	minimální	k2eAgFnPc6d1	minimální
(	(	kIx(	(
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
glukosu	glukosa	k1gFnSc4	glukosa
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
apod	apod	kA	apod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
klonování	klonování	k1gNnSc4	klonování
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
speciálně	speciálně	k6eAd1	speciálně
upravené	upravený	k2eAgInPc1d1	upravený
kmeny	kmen	k1gInPc1	kmen
E.	E.	kA	E.
coli	col	k1gFnPc1	col
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zbavené	zbavený	k2eAgFnPc1d1	zbavená
schopnosti	schopnost	k1gFnPc1	schopnost
syntézy	syntéza	k1gFnSc2	syntéza
restrikčních	restrikční	k2eAgFnPc2d1	restrikční
endonukleáz	endonukleáza	k1gFnPc2	endonukleáza
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
molekulární	molekulární	k2eAgFnSc2d1	molekulární
genetiky	genetika	k1gFnSc2	genetika
byla	být	k5eAaImAgFnS	být
připravena	připraven	k2eAgFnSc1d1	připravena
řada	řada	k1gFnSc1	řada
mutantů	mutant	k1gMnPc2	mutant
E.	E.	kA	E.
coli	coli	k6eAd1	coli
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
DH	DH	kA	DH
<g/>
5	[number]	k4	5
<g/>
-kmen	men	k1gInSc1	-kmen
zbavený	zbavený	k2eAgInSc1d1	zbavený
schopnosti	schopnost	k1gFnSc3	schopnost
rekombinace	rekombinace	k1gFnSc2	rekombinace
tj.	tj.	kA	tj.
opravy	oprava	k1gFnSc2	oprava
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
pro	pro	k7c4	pro
amplifikaci	amplifikace	k1gFnSc4	amplifikace
plasmidové	plasmid	k1gMnPc1	plasmid
DNA	dno	k1gNnSc2	dno
</s>
</p>
<p>
<s>
BL	BL	kA	BL
<g/>
21	[number]	k4	21
<g/>
-kmen	men	k1gInSc1	-kmen
schopný	schopný	k2eAgInSc1d1	schopný
vysoké	vysoká	k1gFnSc3	vysoká
exprese	exprese	k1gFnSc2	exprese
svých	svůj	k3xOyFgInPc2	svůj
genů	gen	k1gInPc2	gen
díky	díky	k7c3	díky
promotoru	promotor	k1gInSc3	promotor
bakteriofága	bakteriofág	k1gMnSc2	bakteriofág
T	T	kA	T
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
integrován	integrovat	k5eAaBmNgInS	integrovat
do	do	k7c2	do
chromosomu	chromosom	k1gInSc2	chromosom
bakterie	bakterie	k1gFnSc2	bakterie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lidský	lidský	k2eAgInSc1d1	lidský
insulin	insulin	k1gInSc1	insulin
===	===	k?	===
</s>
</p>
<p>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
I.	I.	kA	I.
<g/>
stupně	stupeň	k1gInSc2	stupeň
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
slinivka	slinivka	k1gFnSc1	slinivka
břišní	břišní	k2eAgFnSc1d1	břišní
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
produkovat	produkovat	k5eAaImF	produkovat
insulin	insulin	k1gInSc4	insulin
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
substituční	substituční	k2eAgFnSc1d1	substituční
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pacientovi	pacient	k1gMnSc3	pacient
exogenně	exogenně	k6eAd1	exogenně
podává	podávat	k5eAaImIp3nS	podávat
insulin	insulin	k1gInSc1	insulin
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
získáván	získávat	k5eAaImNgInS	získávat
z	z	k7c2	z
jatečních	jateční	k2eAgNnPc2d1	jateční
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
však	však	k9	však
k	k	k7c3	k
alergickým	alergický	k2eAgFnPc3d1	alergická
reakcím	reakce	k1gFnPc3	reakce
na	na	k7c4	na
takto	takto	k6eAd1	takto
získaný	získaný	k2eAgInSc4d1	získaný
insulin	insulin	k1gInSc4	insulin
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
různá	různý	k2eAgFnSc1d1	různá
lokace	lokace	k1gFnSc1	lokace
glykosylace	glykosylace	k1gFnSc2	glykosylace
řetězců	řetězec	k1gInPc2	řetězec
insulinu	insulin	k1gInSc2	insulin
u	u	k7c2	u
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uveden	uveden	k2eAgInSc1d1	uveden
insulin	insulin	k1gInSc1	insulin
získaný	získaný	k2eAgInSc1d1	získaný
rekombinantně	rekombinantně	k6eAd1	rekombinantně
pomocí	pomocí	k7c2	pomocí
geneticky	geneticky	k6eAd1	geneticky
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
bakterie	bakterie	k1gFnSc2	bakterie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
firmy	firma	k1gFnSc2	firma
Eli	Eli	k1gMnSc2	Eli
Lilly	Lilla	k1gMnSc2	Lilla
<g/>
.	.	kIx.	.
</s>
<s>
Insulin	insulin	k1gInSc1	insulin
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
získává	získávat	k5eAaImIp3nS	získávat
rekombinantními	rekombinantní	k2eAgFnPc7d1	rekombinantní
metodami	metoda	k1gFnPc7	metoda
i	i	k8xC	i
pomocí	pomoc	k1gFnSc7	pomoc
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisiae	k1gInSc1	cerevisiae
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
komerčním	komerční	k2eAgInSc7d1	komerční
názvem	název	k1gInSc7	název
Novokin	Novokina	k1gFnPc2	Novokina
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
NovoNordisk	NovoNordisk	k1gInSc1	NovoNordisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výroba	výroba	k1gFnSc1	výroba
indiga	indigo	k1gNnSc2	indigo
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
schopnost	schopnost	k1gFnSc1	schopnost
bakterie	bakterie	k1gFnSc2	bakterie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
textilní	textilní	k2eAgNnSc4d1	textilní
barvivo	barvivo	k1gNnSc4	barvivo
indigo	indigo	k1gNnSc1	indigo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
však	však	k9	však
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
účely	účel	k1gInPc4	účel
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
náklady	náklad	k1gInPc4	náklad
vysoké	vysoký	k2eAgInPc4d1	vysoký
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
výchozí	výchozí	k2eAgInSc1d1	výchozí
substrát	substrát	k1gInSc1	substrát
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
indol	indol	k1gInSc1	indol
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
L-tryptofan	Lryptofan	k1gMnSc1	L-tryptofan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
pomocí	pomocí	k7c2	pomocí
rekombinace	rekombinace	k1gFnSc2	rekombinace
získána	získán	k2eAgFnSc1d1	získána
E.	E.	kA	E.
coli	cole	k1gFnSc4	cole
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
jako	jako	k8xS	jako
výchozí	výchozí	k2eAgInSc1d1	výchozí
substrát	substrát	k1gInSc1	substrát
používat	používat	k5eAaImF	používat
glukosu	glukosa	k1gFnSc4	glukosa
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
kladem	klad	k1gInSc7	klad
této	tento	k3xDgFnSc2	tento
biotechnologické	biotechnologický	k2eAgFnSc2d1	biotechnologická
výroby	výroba	k1gFnSc2	výroba
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
E.	E.	kA	E.
coli	col	k1gFnSc2	col
oproti	oproti	k7c3	oproti
syntetické	syntetický	k2eAgFnSc3d1	syntetická
výrobě	výroba	k1gFnSc3	výroba
je	být	k5eAaImIp3nS	být
ekologická	ekologický	k2eAgFnSc1d1	ekologická
nezávadnost	nezávadnost	k1gFnSc1	nezávadnost
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
při	při	k7c6	při
syntetické	syntetický	k2eAgFnSc6d1	syntetická
výrobě	výroba	k1gFnSc6	výroba
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
toxické	toxický	k2eAgInPc4d1	toxický
meziprodukty	meziprodukt	k1gInPc4	meziprodukt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
produkty	produkt	k1gInPc1	produkt
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
produkty	produkt	k1gInPc4	produkt
rekombinantní	rekombinantní	k2eAgInSc4d1	rekombinantní
E.	E.	kA	E.
coli	col	k1gFnSc2	col
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
příprava	příprava	k1gFnSc1	příprava
lidského	lidský	k2eAgInSc2d1	lidský
růstového	růstový	k2eAgInSc2d1	růstový
hormonu	hormon	k1gInSc2	hormon
<g/>
,	,	kIx,	,
interferonu	interferon	k1gInSc2	interferon
alfa	alfa	k1gNnSc2	alfa
<g/>
,	,	kIx,	,
interferonu	interferon	k1gInSc2	interferon
gama	gama	k1gNnSc2	gama
<g/>
,	,	kIx,	,
interleukinu-	interleukinu-	k?	interleukinu-
<g/>
2	[number]	k4	2
či	či	k8xC	či
produkce	produkce	k1gFnSc1	produkce
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
L-valin	Lalin	k1gInSc1	L-valin
<g/>
,	,	kIx,	,
L-threonin	Lhreonin	k1gInSc1	L-threonin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
firmy	firma	k1gFnSc2	firma
Eli	Eli	k1gMnSc2	Eli
Lilly	Lilla	k1gMnSc2	Lilla
se	se	k3xPyFc4	se
na	na	k7c4	na
trh	trh	k1gInSc4	trh
s	s	k7c7	s
léčivy	léčivo	k1gNnPc7	léčivo
dostaly	dostat	k5eAaPmAgInP	dostat
rekombinantní	rekombinantní	k2eAgInPc1d1	rekombinantní
hormony	hormon	k1gInPc1	hormon
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
parathormon	parathormon	k1gNnSc1	parathormon
a	a	k8xC	a
kalcitonin	kalcitonin	k1gInSc1	kalcitonin
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
osteoporosou	osteoporosa	k1gFnSc7	osteoporosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
USA	USA	kA	USA
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
lymské	lymská	k1gFnSc3	lymská
borelioze	borelioze	k6eAd1	borelioze
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lymerix	Lymerix	k1gInSc1	Lymerix
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rekombinantní	rekombinantní	k2eAgInSc4d1	rekombinantní
protein	protein	k1gInSc4	protein
připravený	připravený	k2eAgInSc4d1	připravený
z	z	k7c2	z
E.	E.	kA	E.
coli	col	k1gFnSc2	col
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
účinná	účinný	k2eAgFnSc1d1	účinná
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
bakterii	bakterie	k1gFnSc4	bakterie
Borrelia	Borrelium	k1gNnSc2	Borrelium
burgdorferi	burgdorfer	k1gFnSc2	burgdorfer
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
majoritním	majoritní	k2eAgMnSc7d1	majoritní
původcem	původce	k1gMnSc7	původce
lymské	lymský	k2eAgInPc4d1	lymský
boreloisy	borelois	k1gInPc7	borelois
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
Borrelia	Borrelia	k1gFnSc1	Borrelia
garinii	garinie	k1gFnSc4	garinie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Sang	Sang	k1gInSc4	Sang
Yup	Yup	k1gFnSc2	Yup
<g/>
.	.	kIx.	.
</s>
<s>
Systems	Systems	k6eAd1	Systems
Biology	biolog	k1gMnPc7	biolog
and	and	k?	and
Biotechnology	biotechnolog	k1gMnPc4	biotechnolog
of	of	k?	of
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	coli	k1gNnPc2	coli
<g/>
,	,	kIx,	,
Springer	Springra	k1gFnPc2	Springra
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4020	[number]	k4	4020
<g/>
-	-	kIx~	-
<g/>
9393	[number]	k4	9393
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Enterobacter	Enterobacter	k1gMnSc1	Enterobacter
</s>
</p>
<p>
<s>
Enterobacteriaceae	Enterobacteriaceae	k6eAd1	Enterobacteriaceae
</s>
</p>
<p>
<s>
Geneticky	geneticky	k6eAd1	geneticky
modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
organismus	organismus	k1gInSc4	organismus
</s>
</p>
<p>
<s>
Koliformní	Koliformní	k2eAgFnSc1d1	Koliformní
bakterie	bakterie	k1gFnSc1	bakterie
</s>
</p>
<p>
<s>
Konjugace	konjugace	k1gFnSc1	konjugace
bakterií	bakterie	k1gFnPc2	bakterie
</s>
</p>
<p>
<s>
Salmonella	Salmonella	k6eAd1	Salmonella
</s>
</p>
<p>
<s>
Shigella	Shigella	k6eAd1	Shigella
</s>
</p>
<p>
<s>
Joshua	Joshua	k1gMnSc1	Joshua
Lederberg	Lederberg	k1gMnSc1	Lederberg
</s>
</p>
<p>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
Escherich	Escherich	k1gMnSc1	Escherich
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
E.	E.	kA	E.
coli	coli	k6eAd1	coli
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
terapeutické	terapeutický	k2eAgFnSc6d1	terapeutická
léčbě	léčba	k1gFnSc6	léčba
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
strategie	strategie	k1gFnSc1	strategie
E.	E.	kA	E.
coli	col	k1gFnSc2	col
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
E.	E.	kA	E.
coli	col	k1gFnSc2	col
EHEC	EHEC	kA	EHEC
O	o	k7c6	o
<g/>
104	[number]	k4	104
<g/>
:	:	kIx,	:
H4	H4	k1gFnSc1	H4
</s>
</p>
<p>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
EHEC-Státní	EHEC-Státní	k2eAgInSc1d1	EHEC-Státní
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
ústav	ústav	k1gInSc1	ústav
</s>
</p>
<p>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
O	o	k7c4	o
<g/>
104	[number]	k4	104
<g/>
:	:	kIx,	:
H4	H4	k1gFnSc2	H4
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
O	o	k7c4	o
<g/>
157	[number]	k4	157
<g/>
:	:	kIx,	:
H7	H7	k1gFnSc2	H7
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
