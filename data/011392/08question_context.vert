<s>
Tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
(	(	kIx(	(
<g/>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tučňákem	tučňák	k1gMnSc7	tučňák
skutečně	skutečně	k6eAd1	skutečně
nejmenším	malý	k2eAgMnSc6d3	nejmenší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nočním	noční	k2eAgInSc6d1	noční
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
nebo	nebo	k8xC	nebo
v	v	k7c6	v
podzemním	podzemní	k2eAgNnSc6d1	podzemní
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
vychází	vycházet	k5eAaImIp3nS	vycházet
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vrací	vracet	k5eAaImIp3nS	vracet
až	až	k9	až
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
obývá	obývat	k5eAaImIp3nS	obývat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
oblasti	oblast	k1gFnSc2	oblast
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
většinou	většinou	k6eAd1	většinou
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
mírného	mírný	k2eAgInSc2d1	mírný
podnebného	podnebný	k2eAgInSc2d1	podnebný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Shoalwater	Shoalwatra	k1gFnPc2	Shoalwatra
Islands	Islandsa	k1gFnPc2	Islandsa
Marine	Marin	k1gInSc5	Marin
Park	park	k1gInSc1	park
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Perthu	Perth	k1gInSc2	Perth
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Bassově	Bassův	k2eAgInSc6d1	Bassův
průlivu	průliv	k1gInSc6	průliv
mezi	mezi	k7c7	mezi
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
Tasmánií	Tasmánie	k1gFnSc7	Tasmánie
až	až	k9	až
po	po	k7c4	po
South	South	k1gInSc4	South
Solitary	Solitara	k1gFnSc2	Solitara
Island	Island	k1gInSc1	Island
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
pobřežích	pobřeží	k1gNnPc6	pobřeží
Severního	severní	k2eAgInSc2d1	severní
a	a	k8xC	a
Jižního	jižní	k2eAgInSc2d1	jižní
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
Stewartově	Stewartův	k2eAgInSc6d1	Stewartův
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
Chathamských	Chathamský	k2eAgInPc6d1	Chathamský
a	a	k8xC	a
Snaresových	Snaresový	k2eAgInPc6d1	Snaresový
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dalších	další	k2eAgInPc2d1	další
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
zatoulá	zatoulat	k5eAaPmIp3nS	zatoulat
i	i	k9	i
na	na	k7c4	na
jihoamerické	jihoamerický	k2eAgNnSc4d1	jihoamerické
pobřeží	pobřeží	k1gNnSc4	pobřeží
do	do	k7c2	do
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
písčitých	písčitý	k2eAgFnPc6d1	písčitá
nebo	nebo	k8xC	nebo
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
plážích	pláž	k1gFnPc6	pláž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
snadný	snadný	k2eAgInSc1d1	snadný
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
měří	měřit	k5eAaImIp3nP	měřit
asi	asi	k9	asi
35	[number]	k4	35
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
1,1	[number]	k4	1,1
až	až	k9	až
1,6	[number]	k4	1,6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
něco	něco	k6eAd1	něco
větší	veliký	k2eAgFnSc4d2	veliký
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
mohutnější	mohutný	k2eAgInSc4d2	mohutnější
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
nepatrně	patrně	k6eNd1	patrně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
tmavý	tmavý	k2eAgInSc1d1	tmavý
"	"	kIx"	"
<g/>
šat	šat	k1gInSc1	šat
<g/>
"	"	kIx"	"
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
zádech	záda	k1gNnPc6	záda
a	a	k8xC	a
křídlech	křídlo	k1gNnPc6	křídlo
má	mít	k5eAaImIp3nS	mít
modravý	modravý	k2eAgInSc4d1	modravý
nádech	nádech	k1gInSc4	nádech
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
i	i	k9	i
modrý	modrý	k2eAgMnSc1d1	modrý
tučňák	tučňák	k1gMnSc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Obličej	obličej	k1gInSc1	obličej
a	a	k8xC	a
krk	krk	k1gInSc1	krk
jsou	být	k5eAaImIp3nP	být
světle	světle	k6eAd1	světle
šedé	šedý	k2eAgFnPc1d1	šedá
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
i	i	k8xC	i
ploutvovitých	ploutvovitý	k2eAgNnPc2d1	ploutvovitý
křídel	křídlo	k1gNnPc2	křídlo
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc4	oko
má	mít	k5eAaImIp3nS	mít
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedé	šedý	k2eAgNnSc1d1	šedé
<g/>
.	.	kIx.	.
</s>

