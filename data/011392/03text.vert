<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
(	(	kIx(	(
<g/>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tučňákem	tučňák	k1gMnSc7	tučňák
skutečně	skutečně	k6eAd1	skutečně
nejmenším	malý	k2eAgMnSc6d3	nejmenší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nočním	noční	k2eAgInSc6d1	noční
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
nebo	nebo	k8xC	nebo
v	v	k7c6	v
podzemním	podzemní	k2eAgNnSc6d1	podzemní
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
vychází	vycházet	k5eAaImIp3nS	vycházet
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vrací	vracet	k5eAaImIp3nS	vracet
až	až	k9	až
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
obývá	obývat	k5eAaImIp3nS	obývat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
oblasti	oblast	k1gFnSc2	oblast
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
většinou	většinou	k6eAd1	většinou
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
mírného	mírný	k2eAgInSc2d1	mírný
podnebného	podnebný	k2eAgInSc2d1	podnebný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Shoalwater	Shoalwatra	k1gFnPc2	Shoalwatra
Islands	Islandsa	k1gFnPc2	Islandsa
Marine	Marin	k1gInSc5	Marin
Park	park	k1gInSc1	park
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Perthu	Perth	k1gInSc2	Perth
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Bassově	Bassův	k2eAgInSc6d1	Bassův
průlivu	průliv	k1gInSc6	průliv
mezi	mezi	k7c7	mezi
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
Tasmánií	Tasmánie	k1gFnSc7	Tasmánie
až	až	k9	až
po	po	k7c4	po
South	South	k1gInSc4	South
Solitary	Solitara	k1gFnSc2	Solitara
Island	Island	k1gInSc1	Island
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
pobřežích	pobřeží	k1gNnPc6	pobřeží
Severního	severní	k2eAgInSc2d1	severní
a	a	k8xC	a
Jižního	jižní	k2eAgInSc2d1	jižní
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
Stewartově	Stewartův	k2eAgInSc6d1	Stewartův
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
Chathamských	Chathamský	k2eAgInPc6d1	Chathamský
a	a	k8xC	a
Snaresových	Snaresový	k2eAgInPc6d1	Snaresový
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dalších	další	k2eAgInPc2d1	další
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
zatoulá	zatoulat	k5eAaPmIp3nS	zatoulat
i	i	k9	i
na	na	k7c4	na
jihoamerické	jihoamerický	k2eAgNnSc4d1	jihoamerické
pobřeží	pobřeží	k1gNnSc4	pobřeží
do	do	k7c2	do
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
písčitých	písčitý	k2eAgFnPc6d1	písčitá
nebo	nebo	k8xC	nebo
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
plážích	pláž	k1gFnPc6	pláž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
snadný	snadný	k2eAgInSc1d1	snadný
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
měří	měřit	k5eAaImIp3nP	měřit
asi	asi	k9	asi
35	[number]	k4	35
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
1,1	[number]	k4	1,1
až	až	k9	až
1,6	[number]	k4	1,6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
něco	něco	k6eAd1	něco
větší	veliký	k2eAgFnSc4d2	veliký
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
mohutnější	mohutný	k2eAgInSc4d2	mohutnější
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
nepatrně	patrně	k6eNd1	patrně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
tmavý	tmavý	k2eAgInSc1d1	tmavý
"	"	kIx"	"
<g/>
šat	šat	k1gInSc1	šat
<g/>
"	"	kIx"	"
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
zádech	záda	k1gNnPc6	záda
a	a	k8xC	a
křídlech	křídlo	k1gNnPc6	křídlo
má	mít	k5eAaImIp3nS	mít
modravý	modravý	k2eAgInSc4d1	modravý
nádech	nádech	k1gInSc4	nádech
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
i	i	k9	i
modrý	modrý	k2eAgMnSc1d1	modrý
tučňák	tučňák	k1gMnSc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Obličej	obličej	k1gInSc1	obličej
a	a	k8xC	a
krk	krk	k1gInSc1	krk
jsou	být	k5eAaImIp3nP	být
světle	světle	k6eAd1	světle
šedé	šedý	k2eAgFnPc1d1	šedá
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
i	i	k8xC	i
ploutvovitých	ploutvovitý	k2eAgNnPc2d1	ploutvovitý
křídel	křídlo	k1gNnPc2	křídlo
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc4	oko
má	mít	k5eAaImIp3nS	mít
stříbřitě	stříbřitě	k6eAd1	stříbřitě
šedé	šedý	k2eAgNnSc1d1	šedé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
rybami	ryba	k1gFnPc7	ryba
(	(	kIx(	(
<g/>
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
10	[number]	k4	10
až	až	k9	až
35	[number]	k4	35
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malými	malý	k2eAgInPc7d1	malý
hlavonožci	hlavonožec	k1gMnSc3	hlavonožec
a	a	k8xC	a
občas	občas	k6eAd1	občas
korýši	korýš	k1gMnPc1	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Potápí	potápět	k5eAaImIp3nS	potápět
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
30	[number]	k4	30
m	m	kA	m
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
asi	asi	k9	asi
25	[number]	k4	25
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
plave	plavat	k5eAaImIp3nS	plavat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
6	[number]	k4	6
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc4	hod
<g/>
,	,	kIx,	,
potravu	potrava	k1gFnSc4	potrava
polyká	polykat	k5eAaImIp3nS	polykat
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Neloví	lovit	k5eNaImIp3nS	lovit
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	se	k3xPyFc4	se
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
do	do	k7c2	do
25	[number]	k4	25
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
6,5	[number]	k4	6,5
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
druhy	druh	k1gInPc7	druh
má	mít	k5eAaImIp3nS	mít
pomalý	pomalý	k2eAgInSc1d1	pomalý
metabolismus	metabolismus	k1gInSc1	metabolismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
svým	svůj	k3xOyFgInSc7	svůj
rodinným	rodinný	k2eAgInSc7d1	rodinný
životem	život	k1gInSc7	život
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
<g/>
,	,	kIx,	,
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
období	období	k1gNnSc4	období
hloubení	hloubení	k1gNnSc2	hloubení
nor	nora	k1gFnPc2	nora
jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
spatření	spatření	k1gNnSc3	spatření
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
nebo	nebo	k8xC	nebo
odcházejí	odcházet	k5eAaImIp3nP	odcházet
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
nebo	nebo	k8xC	nebo
spí	spát	k5eAaImIp3nS	spát
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
<g/>
.	.	kIx.	.
</s>
<s>
Ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
takto	takto	k6eAd1	takto
svá	svůj	k3xOyFgNnPc4	svůj
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc4	mládě
i	i	k9	i
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
před	před	k7c7	před
většinou	většina	k1gFnSc7	většina
predátorů	predátor	k1gMnPc2	predátor
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
při	při	k7c6	při
hnízdění	hnízdění	k1gNnSc6	hnízdění
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
zvýšeným	zvýšený	k2eAgFnPc3d1	zvýšená
teplotám	teplota	k1gFnPc3	teplota
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
léta	léto	k1gNnSc2	léto
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
kolonii	kolonie	k1gFnSc4	kolonie
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
až	až	k8xS	až
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
února	únor	k1gInSc2	únor
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nočním	noční	k2eAgInSc6d1	noční
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
se	se	k3xPyFc4	se
původní	původní	k2eAgMnPc1d1	původní
partneři	partner	k1gMnPc1	partner
většinou	většinou	k6eAd1	většinou
poznávají	poznávat	k5eAaImIp3nP	poznávat
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
opět	opět	k6eAd1	opět
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
páry	pár	k1gInPc1	pár
obvykle	obvykle	k6eAd1	obvykle
zabírají	zabírat	k5eAaImIp3nP	zabírat
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hnízdili	hnízdit	k5eAaImAgMnP	hnízdit
již	již	k9	již
v	v	k7c6	v
předešlých	předešlý	k2eAgNnPc6d1	předešlé
létech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
soubojům	souboj	k1gInPc3	souboj
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
syčí	syčet	k5eAaImIp3nS	syčet
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
si	se	k3xPyFc3	se
zobákem	zobák	k1gInSc7	zobák
i	i	k8xC	i
klovají	klovat	k5eAaImIp3nP	klovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Opravují	opravovat	k5eAaImIp3nP	opravovat
nebo	nebo	k8xC	nebo
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
nové	nový	k2eAgFnPc4d1	nová
nory	nora	k1gFnPc4	nora
v	v	k7c6	v
písku	písek	k1gInSc6	písek
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
hustou	hustý	k2eAgFnSc7d1	hustá
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
používají	používat	k5eAaImIp3nP	používat
nory	nora	k1gFnPc1	nora
po	po	k7c6	po
buřňácích	buřňák	k1gMnPc6	buřňák
úzkozobých	úzkozobý	k2eAgNnPc6d1	úzkozobý
(	(	kIx(	(
<g/>
Puffinus	Puffinus	k1gMnSc1	Puffinus
tenuirostris	tenuirostris	k1gFnSc2	tenuirostris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
si	se	k3xPyFc3	se
založí	založit	k5eAaPmIp3nP	založit
hnízdo	hnízdo	k1gNnSc4	hnízdo
i	i	k8xC	i
jen	jen	k9	jen
ve	v	k7c6	v
skalní	skalní	k2eAgFnSc6d1	skalní
rozsedlině	rozsedlina	k1gFnSc6	rozsedlina
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
obtížný	obtížný	k2eAgInSc4d1	obtížný
přístup	přístup	k1gInSc4	přístup
přes	přes	k7c4	přes
skaliska	skalisko	k1gNnPc4	skalisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
veškerý	veškerý	k3xTgInSc4	veškerý
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
vybudují	vybudovat	k5eAaPmIp3nP	vybudovat
prostrannou	prostranný	k2eAgFnSc4d1	prostranná
komůrku	komůrka	k1gFnSc4	komůrka
s	s	k7c7	s
hnízdem	hnízdo	k1gNnSc7	hnízdo
vystlaným	vystlaný	k2eAgNnSc7d1	vystlané
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
větvičkami	větvička	k1gFnPc7	větvička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nanosí	nanosit	k5eAaBmIp3nP	nanosit
oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
páry	pár	k1gInPc1	pár
nezačínají	začínat	k5eNaImIp3nP	začínat
hnízdit	hnízdit	k5eAaImF	hnízdit
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
dlouho	dlouho	k6eAd1	dlouho
jim	on	k3xPp3gMnPc3	on
trvá	trvat	k5eAaImIp3nS	trvat
oprava	oprava	k1gFnSc1	oprava
nebo	nebo	k8xC	nebo
vystavění	vystavění	k1gNnSc1	vystavění
nového	nový	k2eAgNnSc2d1	nové
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
hned	hned	k6eAd1	hned
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
některá	některý	k3yIgNnPc1	některý
hnízda	hnízdo	k1gNnPc1	hnízdo
zničena	zničit	k5eAaPmNgNnP	zničit
nebo	nebo	k8xC	nebo
vyplaven	vyplavit	k5eAaPmNgInS	vyplavit
za	za	k7c4	za
bouře	bouř	k1gFnPc4	bouř
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
hnízdní	hnízdní	k2eAgNnSc4d1	hnízdní
období	období	k1gNnSc4	období
nuceni	nucen	k2eAgMnPc1d1	nucen
i	i	k9	i
ke	k	k7c3	k
třem	tři	k4xCgFnPc3	tři
snůškám	snůška	k1gFnPc3	snůška
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dohotovení	dohotovení	k1gNnSc6	dohotovení
hnízda	hnízdo	k1gNnSc2	hnízdo
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
k	k	k7c3	k
toku	tok	k1gInSc3	tok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
mimo	mimo	k7c4	mimo
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k1gInSc1	pár
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
čistí	čistit	k5eAaImIp3nS	čistit
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
oštipují	oštipovat	k5eAaImIp3nP	oštipovat
si	se	k3xPyFc3	se
jemně	jemně	k6eAd1	jemně
konce	konec	k1gInPc4	konec
zobáků	zobák	k1gInPc2	zobák
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
troubí	troubit	k5eAaImIp3nS	troubit
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
vztyčenou	vztyčený	k2eAgFnSc7d1	vztyčená
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mává	mávat	k5eAaImIp3nS	mávat
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
připojí	připojit	k5eAaPmIp3nS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
námluvy	námluva	k1gFnPc1	námluva
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
samec	samec	k1gMnSc1	samec
vydávaje	vydávat	k5eAaImSgInS	vydávat
tikavé	tikavý	k2eAgInPc4d1	tikavý
zvuky	zvuk	k1gInPc4	zvuk
přitlačuje	přitlačovat	k5eAaImIp3nS	přitlačovat
samici	samice	k1gFnSc4	samice
ke	k	k7c3	k
stěně	stěna	k1gFnSc3	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k8xS	až
mu	on	k3xPp3gMnSc3	on
samice	samice	k1gFnSc1	samice
začne	začít	k5eAaPmIp3nS	začít
odpovídat	odpovídat	k5eAaImF	odpovídat
<g/>
,	,	kIx,	,
uchopí	uchopit	k5eAaPmIp3nP	uchopit
ji	on	k3xPp3gFnSc4	on
zobákem	zobák	k1gInSc7	zobák
za	za	k7c4	za
peří	peří	k1gNnSc4	peří
v	v	k7c6	v
týle	týl	k1gInSc6	týl
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
položí	položit	k5eAaPmIp3nS	položit
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
páření	páření	k1gNnSc4	páření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spáření	spáření	k1gNnSc6	spáření
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
upraví	upravit	k5eAaPmIp3nS	upravit
peří	peří	k1gNnSc1	peří
a	a	k8xC	a
společně	společně	k6eAd1	společně
opět	opět	k6eAd1	opět
troubí	troubit	k5eAaImIp3nS	troubit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
páří	pářit	k5eAaImIp3nS	pářit
i	i	k9	i
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snese	snést	k5eAaPmIp3nS	snést
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
při	při	k7c6	při
sezení	sezení	k1gNnSc6	sezení
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
,	,	kIx,	,
ke	k	k7c3	k
střídaní	střídaný	k2eAgMnPc1d1	střídaný
dochází	docházet	k5eAaImIp3nP	docházet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
33	[number]	k4	33
až	až	k9	až
37	[number]	k4	37
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
rozptyl	rozptyl	k1gInSc4	rozptyl
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezasedají	zasedat	k5eNaImIp3nP	zasedat
na	na	k7c4	na
vejce	vejce	k1gNnPc4	vejce
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
z	z	k7c2	z
vejce	vejce	k1gNnSc2	vejce
líhne	líhnout	k5eAaImIp3nS	líhnout
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
24	[number]	k4	24
i	i	k8xC	i
více	hodně	k6eAd2	hodně
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vylíhlá	vylíhlý	k2eAgNnPc1d1	vylíhlé
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
slepá	slepý	k2eAgNnPc1d1	slepé
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
hustým	hustý	k2eAgNnSc7d1	husté
šedým	šedý	k2eAgNnSc7d1	šedé
prachovým	prachový	k2eAgNnSc7d1	prachové
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
peří	peří	k1gNnSc1	peří
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
za	za	k7c4	za
tmavší	tmavý	k2eAgInSc4d2	tmavší
a	a	k8xC	a
po	po	k7c6	po
4	[number]	k4	4
týdnech	týden	k1gInPc6	týden
začíná	začínat	k5eAaImIp3nS	začínat
narůstat	narůstat	k5eAaImF	narůstat
definitivní	definitivní	k2eAgNnSc1d1	definitivní
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zcela	zcela	k6eAd1	zcela
doroste	dorůst	k5eAaPmIp3nS	dorůst
v	v	k7c6	v
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
začínají	začínat	k5eAaImIp3nP	začínat
vidět	vidět	k5eAaImF	vidět
asi	asi	k9	asi
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
plazí	plazit	k5eAaImIp3nS	plazit
po	po	k7c6	po
břiše	břicho	k1gNnSc6	břicho
a	a	k8xC	a
za	za	k7c7	za
4	[number]	k4	4
týdny	týden	k1gInPc7	týden
dovedou	dovést	k5eAaPmIp3nP	dovést
chodit	chodit	k5eAaImF	chodit
v	v	k7c6	v
předklonu	předklon	k1gInSc6	předklon
<g/>
.	.	kIx.	.
</s>
<s>
Vzpřímené	vzpřímený	k2eAgFnPc1d1	vzpřímená
chůze	chůze	k1gFnPc1	chůze
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgFnSc1d1	schopna
až	až	k6eAd1	až
jim	on	k3xPp3gMnPc3	on
narostou	narůst	k5eAaPmIp3nP	narůst
ocasní	ocasní	k2eAgNnPc4d1	ocasní
péra	péro	k1gNnPc4	péro
<g/>
,	,	kIx,	,
asi	asi	k9	asi
po	po	k7c6	po
8	[number]	k4	8
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvé	prvý	k4xOgInPc1	prvý
tři	tři	k4xCgInPc1	tři
týdny	týden	k1gInPc1	týden
je	on	k3xPp3gFnPc4	on
rodiče	rodič	k1gMnPc1	rodič
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
a	a	k8xC	a
stráží	strážit	k5eAaImIp3nP	strážit
<g/>
,	,	kIx,	,
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
i	i	k8xC	i
v	v	k7c6	v
přinášení	přinášení	k1gNnSc6	přinášení
potravy	potrava	k1gFnSc2	potrava
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
čekají	čekat	k5eAaImIp3nP	čekat
mláďata	mládě	k1gNnPc1	mládě
osamocena	osamocen	k2eAgNnPc1d1	osamoceno
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
na	na	k7c4	na
příchod	příchod	k1gInSc4	příchod
obou	dva	k4xCgMnPc2	dva
rodičů	rodič	k1gMnPc2	rodič
s	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
sami	sám	k3xTgMnPc1	sám
opouštějí	opouštět	k5eAaImIp3nP	opouštět
noru	nora	k1gFnSc4	nora
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
asi	asi	k9	asi
60	[number]	k4	60
dnech	den	k1gInPc6	den
odplouvají	odplouvat	k5eAaImIp3nP	odplouvat
trvale	trvale	k6eAd1	trvale
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
90	[number]	k4	90
%	%	kIx~	%
konečné	konečný	k2eAgFnSc2d1	konečná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgFnPc1d1	sexuální
zralosti	zralost	k1gFnPc1	zralost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
létech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
po	po	k7c6	po
měsíční	měsíční	k2eAgFnSc6d1	měsíční
vykrmovací	vykrmovací	k2eAgFnSc6d1	vykrmovací
kůře	kůra	k1gFnSc6	kůra
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
opět	opět	k6eAd1	opět
vracejí	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
nor	nora	k1gFnPc2	nora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
skrytu	skryt	k1gInSc6	skryt
přepeřují	přepeřovat	k5eAaImIp3nP	přepeřovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
14	[number]	k4	14
až	až	k9	až
18	[number]	k4	18
dnů	den	k1gInPc2	den
jim	on	k3xPp3gMnPc3	on
naroste	narůst	k5eAaPmIp3nS	narůst
nové	nový	k2eAgNnSc4d1	nové
peří	peří	k1gNnSc4	peří
a	a	k8xC	a
odcházejí	odcházet	k5eAaImIp3nP	odcházet
z	z	k7c2	z
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
cca	cca	kA	cca
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k1gMnPc1	mladý
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
prvé	prvý	k4xOgNnSc4	prvý
zahnízdění	zahnízdění	k1gNnSc4	zahnízdění
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
vybírají	vybírat	k5eAaImIp3nP	vybírat
jiná	jiný	k2eAgNnPc4d1	jiné
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
počty	počet	k1gInPc1	počet
tučňáka	tučňák	k1gMnSc2	tučňák
nejmenšího	malý	k2eAgMnSc2d3	nejmenší
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
jinde	jinde	k6eAd1	jinde
zase	zase	k9	zase
nové	nový	k2eAgFnSc2d1	nová
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
červeného	červený	k2eAgInSc2d1	červený
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
regionu	region	k1gInSc6	region
asi	asi	k9	asi
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
jako	jako	k9	jako
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc4d1	dotčený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gMnSc1	druh
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
se	se	k3xPyFc4	se
nověji	nově	k6eAd2	nově
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
6	[number]	k4	6
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnPc2d1	minor
minor	minor	k2eAgFnPc2d1	minor
Forster	Forstra	k1gFnPc2	Forstra
<g/>
,	,	kIx,	,
1781	[number]	k4	1781
</s>
</p>
<p>
<s>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc2d1	minor
chathamensis	chathamensis	k1gFnSc2	chathamensis
Kinsky	Kinska	k1gFnSc2	Kinska
et	et	k?	et
Falla	Falla	k1gMnSc1	Falla
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgNnSc2d1	minor
iredalei	iredalei	k1gNnSc2	iredalei
Mathews	Mathewsa	k1gFnPc2	Mathewsa
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
</s>
</p>
<p>
<s>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgNnSc2d1	minor
novaehollandiae	novaehollandiae	k1gNnSc2	novaehollandiae
Stephens	Stephensa	k1gFnPc2	Stephensa
<g/>
,	,	kIx,	,
1826	[number]	k4	1826
</s>
</p>
<p>
<s>
Eudyptula	Eudyptula	k1gFnSc1	Eudyptula
minor	minor	k2eAgFnSc2d1	minor
variabilis	variabilis	k1gFnSc2	variabilis
Kinsky	Kinska	k1gFnSc2	Kinska
et	et	k?	et
Falla	Falla	k1gMnSc1	Falla
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
tučňák	tučňák	k1gMnSc1	tučňák
bělopásý	bělopásý	k2eAgMnSc1d1	bělopásý
(	(	kIx(	(
<g/>
Eudyptula	Eudyptul	k1gMnSc4	Eudyptul
minor	minor	k2eAgMnSc4d1	minor
albosignata	albosignat	k1gMnSc4	albosignat
<g/>
)	)	kIx)	)
Finsch	Finsch	k1gMnSc1	Finsch
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
<g/>
Poslední	poslední	k2eAgNnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
tučňák	tučňák	k1gMnSc1	tučňák
bělopásý	bělopásý	k2eAgMnSc1d1	bělopásý
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Tučňáci	tučňák	k1gMnPc1	tučňák
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
:	:	kIx,	:
Tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mořský	mořský	k2eAgInSc1d1	mořský
život	život	k1gInSc1	život
<g/>
:	:	kIx,	:
Tučňák	tučňák	k1gMnSc1	tučňák
nejmenší	malý	k2eAgMnSc1d3	nejmenší
</s>
</p>
