<s>
Autogenocida	Autogenocida	k1gFnSc1	Autogenocida
je	být	k5eAaImIp3nS	být
exterminace	exterminace	k1gFnSc1	exterminace
či	či	k8xC	či
genocida	genocida	k1gFnSc1	genocida
občanů	občan	k1gMnPc2	občan
země	země	k1gFnSc1	země
spáchaná	spáchaný	k2eAgFnSc1d1	spáchaná
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vládou	vláda	k1gFnSc7	vláda
či	či	k8xC	či
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
autogenocida	autogenocid	k1gMnSc2	autogenocid
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
let	léto	k1gNnPc2	léto
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
režimem	režim	k1gInSc7	režim
Rudých	rudý	k2eAgMnPc2d1	rudý
Khmérů	Khmér	k1gMnPc2	Khmér
v	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
k	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
od	od	k7c2	od
genocid	genocida	k1gFnPc2	genocida
spáchané	spáchaný	k2eAgNnSc1d1	spáchané
cizí	cizí	k2eAgFnSc7d1	cizí
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vraždění	vraždění	k1gNnSc1	vraždění
lidí	člověk	k1gMnPc2	člověk
židovského	židovský	k2eAgMnSc2d1	židovský
a	a	k8xC	a
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Autogenocide	Autogenocid	k1gInSc5	Autogenocid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc3d1	anglická
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
exterminace	exterminace	k1gFnSc1	exterminace
democida	democida	k1gFnSc1	democida
</s>
