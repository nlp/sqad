<s>
Tellurid	tellurid	k1gInSc1
bismutitý	bismutitý	k2eAgInSc1d1
</s>
<s>
Tellurid	tellurid	k1gInSc1
bismutitý	bismutitý	k2eAgInSc1d1
</s>
<s>
Krystalová	krystalový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
telluridu	tellurid	k1gInSc2
bismutitého	bismutitý	k2eAgInSc2d1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Tellurid	tellurid	k1gInSc1
bismutitý	bismutitý	k2eAgInSc1d1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Bismuth	Bismuth	k1gMnSc1
telluride	tellurid	k1gInSc5
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Bismuttellurid	Bismuttellurid	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Bi	Bi	k?
<g/>
2	#num#	k4
<g/>
Te	Te	k1gFnSc1
<g/>
3	#num#	k4
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
šedý	šedý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
1304-82-1	1304-82-1	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
6379155	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
[	[	kIx(
<g/>
Te-	Te-	k1gFnPc2
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
Te-	Te-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
Te-	Te-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
Bi	Bi	k1gMnSc1
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
Bi	Bi	k1gMnSc1
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
Bi	Bi	k1gFnPc2
<g/>
.3	.3	k4
<g/>
Te	Te	k1gFnPc2
<g/>
/	/	kIx~
<g/>
q	q	k?
<g/>
2	#num#	k4
<g/>
*	*	kIx~
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
;	;	kIx,
<g/>
3	#num#	k4
<g/>
*	*	kIx~
<g/>
-	-	kIx~
<g/>
2	#num#	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
800,76	800,76	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
580	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
7,74	7,74	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tellurid	tellurid	k1gInSc1
bismutitý	bismutitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
anorganická	anorganický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
s	s	k7c7
vzorcem	vzorec	k1gInSc7
Bi	Bi	k1gFnSc1
<g/>
2	#num#	k4
<g/>
Te	Te	k1gFnSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
šedý	šedý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
s	s	k7c7
polovodivými	polovodivý	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc4
slitiny	slitina	k1gFnPc4
s	s	k7c7
antimonem	antimon	k1gInSc7
a	a	k8xC
selenem	selen	k1gInSc7
vykazují	vykazovat	k5eAaImIp3nP
termoelektrické	termoelektrický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bi	Bi	k1gFnSc1
<g/>
2	#num#	k4
<g/>
Te	Te	k1gFnSc1
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
topologický	topologický	k2eAgInSc4d1
izolant	izolant	k1gInSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
jsou	být	k5eAaImIp3nP
závislé	závislý	k2eAgInPc1d1
na	na	k7c6
tloušťce	tloušťka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
minerál	minerál	k1gInSc1
tellurobismutit	tellurobismutit	k5eAaPmF
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
se	se	k3xPyFc4
v	v	k7c6
Česku	Česko	k1gNnSc6
nachází	nacházet	k5eAaImIp3nS
např.	např.	kA
v	v	k7c6
Bělčicích	Bělčice	k1gFnPc6
<g/>
,	,	kIx,
Jílové	jílový	k2eAgFnPc1d1
a	a	k8xC
Kasejovicích	Kasejovice	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Známe	znát	k5eAaImIp1nP
i	i	k9
další	další	k2eAgInPc4d1
přírodní	přírodní	k2eAgInPc4d1
minerály	minerál	k1gInPc4
telluridu	tellurid	k1gInSc2
bismutitého	bismutitý	k2eAgInSc2d1
s	s	k7c7
nestechiometrickým	stechiometrický	k2eNgNnSc7d1
složením	složení	k1gNnSc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
minerály	minerál	k1gInPc4
se	s	k7c7
složením	složení	k1gNnSc7
Bi-Te-S-	Bi-Te-S-	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Se	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Tellurid	tellurid	k1gInSc1
bismutitý	bismutitý	k2eAgMnSc1d1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
zahříváním	zahřívání	k1gNnSc7
směsi	směs	k1gFnSc2
bismutu	bismut	k1gInSc2
a	a	k8xC
telluru	tellur	k1gInSc2
v	v	k7c6
křemenné	křemenný	k2eAgFnSc6d1
ampuli	ampule	k1gFnSc6
na	na	k7c4
teplotu	teplota	k1gFnSc4
800	#num#	k4
°	°	k?
<g/>
C.	C.	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Bismuth	Bismutha	k1gFnPc2
telluride	tellurid	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Tellurobismutit	Tellurobismutit	k1gFnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Tellurobismuthite	Tellurobismuthit	k1gInSc5
<g/>
↑	↑	k?
Thermoelectrics	Thermoelectrics	k1gInSc1
handbook	handbook	k1gInSc1
:	:	kIx,
macro	macro	k6eAd1
to	ten	k3xDgNnSc4
nano	nano	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boca	Boca	k1gMnSc1
Raton	Raton	k1gMnSc1
<g/>
:	:	kIx,
CRC	CRC	kA
<g/>
/	/	kIx~
<g/>
Taylor	Taylor	k1gMnSc1
&	&	k?
Francis	Francis	k1gInSc1
1	#num#	k4
volume	volum	k1gInSc5
(	(	kIx(
<g/>
various	various	k1gInSc1
pagings	pagings	k1gInSc1
<g/>
)	)	kIx)
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
849322642	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780849322648	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
60312078	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
