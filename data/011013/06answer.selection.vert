<s>
Hliník	hliník	k1gInSc1	hliník
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Aluminium	aluminium	k1gNnSc1	aluminium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
lehký	lehký	k2eAgInSc1d1	lehký
kov	kov	k1gInSc1	kov
bělavě	bělavě	k6eAd1	bělavě
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc1d1	dobrý
vodič	vodič	k1gInSc1	vodič
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
a	a	k8xC	a
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
slitin	slitina	k1gFnPc2	slitina
v	v	k7c6	v
leteckém	letecký	k2eAgInSc6d1	letecký
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
