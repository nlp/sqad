<p>
<s>
Texel	Texel	k1gInSc4	Texel
(	(	kIx(	(
<g/>
texture	textur	k1gMnSc5	textur
element	element	k1gInSc4	element
nebo	nebo	k8xC	nebo
texture	textur	k1gMnSc5	textur
pixel	pixel	k1gInSc1	pixel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
textury	textura	k1gFnSc2	textura
(	(	kIx(	(
<g/>
tapety	tapeta	k1gFnPc4	tapeta
<g/>
)	)	kIx)	)
používané	používaný	k2eAgFnPc4d1	používaná
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
grafice	grafika	k1gFnSc6	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
polem	pole	k1gNnSc7	pole
pixelů	pixel	k1gInPc2	pixel
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
textura	textura	k1gFnSc1	textura
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
polem	pole	k1gNnSc7	pole
texelů	texel	k1gMnPc2	texel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
texturování	texturování	k1gNnSc6	texturování
3D	[number]	k4	3D
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
proces	proces	k1gInSc1	proces
mapování	mapování	k1gNnSc2	mapování
textur	textura	k1gFnPc2	textura
<g/>
)	)	kIx)	)
renderer	renderer	k1gInSc1	renderer
mapuje	mapovat	k5eAaImIp3nS	mapovat
texely	texel	k1gInPc4	texel
do	do	k7c2	do
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
pixelů	pixel	k1gInPc2	pixel
výsledného	výsledný	k2eAgInSc2d1	výsledný
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
moderních	moderní	k2eAgInPc6d1	moderní
počítačích	počítač	k1gInPc6	počítač
tuto	tento	k3xDgFnSc4	tento
operaci	operace	k1gFnSc4	operace
provádí	provádět	k5eAaImIp3nS	provádět
grafická	grafický	k2eAgFnSc1d1	grafická
karta	karta	k1gFnSc1	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
texel	texela	k1gFnPc2	texela
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
než	než	k8xS	než
celočíselné	celočíselný	k2eAgFnSc6d1	celočíselná
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
filtrování	filtrování	k1gNnSc1	filtrování
textur	textura	k1gFnPc2	textura
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
texel	texel	k1gInSc4	texel
mimo	mimo	k7c4	mimo
texturu	textura	k1gFnSc4	textura
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
možností	možnost	k1gFnPc2	možnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
cyklické	cyklický	k2eAgNnSc1d1	cyklické
opakování	opakování	k1gNnSc1	opakování
(	(	kIx(	(
<g/>
dlaždice	dlaždice	k1gFnPc1	dlaždice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zrcadlové	zrcadlový	k2eAgNnSc1d1	zrcadlové
opakování	opakování	k1gNnSc1	opakování
(	(	kIx(	(
<g/>
překlápění	překlápění	k1gNnSc1	překlápění
dlaždic	dlaždice	k1gFnPc2	dlaždice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
texel	texel	k1gInSc4	texel
(	(	kIx(	(
<g/>
okraj	okraj	k1gInSc4	okraj
textury	textura	k1gFnSc2	textura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
explicitní	explicitní	k2eAgInSc4d1	explicitní
okraj	okraj	k1gInSc4	okraj
(	(	kIx(	(
<g/>
definovaný	definovaný	k2eAgInSc4d1	definovaný
řádek	řádek	k1gInSc4	řádek
nebo	nebo	k8xC	nebo
sloupec	sloupec	k1gInSc4	sloupec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rastrová	rastrový	k2eAgFnSc1d1	rastrová
grafika	grafika	k1gFnSc1	grafika
</s>
</p>
<p>
<s>
Shader	Shader	k1gMnSc1	Shader
</s>
</p>
<p>
<s>
Voxel	Voxel	k1gMnSc1	Voxel
</s>
</p>
