<s>
Spasitelský	spasitelský	k2eAgInSc1d1
komplex	komplex	k1gInSc1
</s>
<s>
Spasitelský	spasitelský	k2eAgInSc1d1
komplex	komplex	k1gInSc1
(	(	kIx(
<g/>
také	také	k9
nazývaný	nazývaný	k2eAgInSc1d1
mesiášský	mesiášský	k2eAgInSc1d1
komplex	komplex	k1gInSc1
<g/>
,	,	kIx,
Božský	božský	k2eAgInSc1d1
komplex	komplex	k1gInSc1
nebo	nebo	k8xC
spasitelský	spasitelský	k2eAgInSc1d1
syndrom	syndrom	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stav	stav	k1gInSc4
mysli	mysl	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
se	se	k3xPyFc4
osoba	osoba	k1gFnSc1
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jejím	její	k3xOp3gInSc7
osudem	osud	k1gInSc7
či	či	k8xC
určením	určení	k1gNnSc7
je	být	k5eAaImIp3nS
zachraňovat	zachraňovat	k5eAaImF
a	a	k8xC
zachránit	zachránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varianta	varianta	k1gFnSc1
názvu	název	k1gInSc2
odkazuje	odkazovat	k5eAaImIp3nS
k	k	k7c3
osobě	osoba	k1gFnSc3
mesiáše	mesiáš	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
židovský	židovský	k2eAgInSc1d1
a	a	k8xC
křesťanský	křesťanský	k2eAgInSc1d1
termín	termín	k1gInSc1
pro	pro	k7c4
spasitele	spasitel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
zvláštní	zvláštní	k2eAgFnSc4d1
psychiatrickou	psychiatrický	k2eAgFnSc4d1
diagnózu	diagnóza	k1gFnSc4
či	či	k8xC
duševní	duševní	k2eAgFnSc4d1
poruchu	porucha	k1gFnSc4
<g/>
,	,	kIx,
Diagnostický	diagnostický	k2eAgInSc4d1
a	a	k8xC
statistický	statistický	k2eAgInSc4d1
manuál	manuál	k1gInSc4
mentálních	mentální	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ani	ani	k8xC
Mezinárodní	mezinárodní	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
nemocí	nemoc	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
takovou	takový	k3xDgFnSc4
samostatnou	samostatný	k2eAgFnSc4d1
diagnózu	diagnóza	k1gFnSc4
neobsahuje	obsahovat	k5eNaImIp3nS
<g/>
,	,	kIx,
spasitelský	spasitelský	k2eAgInSc1d1
syndrom	syndrom	k1gInSc1
se	se	k3xPyFc4
však	však	k9
může	moct	k5eAaImIp3nS
objevit	objevit	k5eAaPmF
jako	jako	k9
součást	součást	k1gFnSc4
psychotických	psychotický	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
apod.	apod.	kA
</s>
<s>
Výskyt	výskyt	k1gInSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
V	v	k7c6
páté	pátý	k4xOgFnSc6
sezoně	sezona	k1gFnSc6
seriálu	seriál	k1gInSc2
Dr	dr	kA
<g/>
.	.	kIx.
House	house	k1gNnSc1
jedna	jeden	k4xCgFnSc1
z	z	k7c2
postav	postava	k1gFnPc2
trpí	trpět	k5eAaImIp3nP
spasitelským	spasitelský	k2eAgInSc7d1
komplexem	komplex	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
si	se	k3xPyFc3
vybírá	vybírat	k5eAaImIp3nS
za	za	k7c4
partnery	partner	k1gMnPc4
pouze	pouze	k6eAd1
lidi	člověk	k1gMnPc4
nemocné	nemocný	k2eAgMnPc4d1,k2eNgMnPc4d1
či	či	k8xC
postižené	postižený	k1gMnPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jí	on	k3xPp3gFnSc3
umožňuje	umožňovat	k5eAaImIp3nS
realizovat	realizovat	k5eAaBmF
se	se	k3xPyFc4
jako	jako	k9
zachránce	zachránce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
manze	manz	k1gInSc6
Zápisník	zápisník	k1gInSc1
smrti	smrt	k1gFnSc2
nalezne	nalézt	k5eAaBmIp3nS,k5eAaPmIp3nS
hrdina	hrdina	k1gMnSc1
smrtící	smrtící	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
a	a	k8xC
ve	v	k7c6
víře	víra	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
stal	stát	k5eAaPmAgMnS
bohem	bůh	k1gMnSc7
nového	nový	k2eAgInSc2d1
světa	svět	k1gInSc2
začne	začít	k5eAaPmIp3nS
zabíjet	zabíjet	k5eAaImF
zločince	zločinec	k1gMnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tím	ten	k3xDgNnSc7
zlepší	zlepšit	k5eAaPmIp3nS
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Psycholog	psycholog	k1gMnSc1
Joseph	Josepha	k1gFnPc2
MacCurdy	MacCurda	k1gFnSc2
z	z	k7c2
univerzity	univerzita	k1gFnSc2
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
ve	v	k7c6
studii	studie	k1gFnSc6
vytvořené	vytvořený	k2eAgFnSc6d1
na	na	k7c4
zakázku	zakázka	k1gFnSc4
propagandistického	propagandistický	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
BBC	BBC	kA
došel	dojít	k5eAaPmAgInS
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
nacistický	nacistický	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
trpěl	trpět	k5eAaImAgMnS
mesiášským	mesiášský	k2eAgInSc7d1
komplexem	komplex	k1gInSc7
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Diagnostic	Diagnostice	k1gFnPc2
and	and	k?
Statistical	Statistical	k1gMnSc1
Manual	Manual	k1gMnSc1
of	of	k?
Mental	Mental	k1gMnSc1
Disorders	Disorders	k1gInSc1
(	(	kIx(
<g/>
DSM	DSM	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arlington	Arlington	k1gInSc1
<g/>
:	:	kIx,
DSM-IV-TR	DSM-IV-TR	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MKN-	MKN-	k1gFnPc2
<g/>
10	#num#	k4
<g/>
↑	↑	k?
idnes	idnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Hitler	Hitler	k1gMnSc1
trpí	trpět	k5eAaImIp3nS
mesiášským	mesiášský	k2eAgInSc7d1
komplexem	komplex	k1gInSc7
<g/>
,	,	kIx,
vybádal	vybádat	k5eAaPmAgMnS
za	za	k7c2
války	válka	k1gFnSc2
britský	britský	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
</s>
