<p>
<s>
Andské	andský	k2eAgInPc1d1	andský
státy	stát	k1gInPc1	stát
či	či	k8xC	či
andské	andský	k2eAgFnPc1d1	andská
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Andské	andský	k2eAgInPc1d1	andský
státy	stát	k1gInPc1	stát
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
skupiny	skupina	k1gFnSc2	skupina
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
prochází	procházet	k5eAaImIp3nP	procházet
Andy	Anda	k1gFnPc1	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
andské	andský	k2eAgInPc4d1	andský
státy	stát	k1gInPc4	stát
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
</s>
</p>
<p>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
</s>
</p>
<p>
<s>
Bolívie	Bolívie	k1gFnSc1	Bolívie
</s>
</p>
<p>
<s>
Peru	Peru	k1gNnSc1	Peru
</s>
</p>
<p>
<s>
ChileZ	ChileZ	k?	ChileZ
čistě	čistě	k6eAd1	čistě
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
pohledu	pohled	k1gInSc2	pohled
mezi	mezi	k7c4	mezi
andské	andský	k2eAgInPc4d1	andský
státy	stát	k1gInPc4	stát
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Venezuela	Venezuela	k1gFnSc1	Venezuela
a	a	k8xC	a
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panují	panovat	k5eAaImIp3nP	panovat
tady	tady	k6eAd1	tady
drsné	drsný	k2eAgFnPc1d1	drsná
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nerostné	nerostný	k2eAgFnPc1d1	nerostná
suroviny	surovina	k1gFnPc1	surovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
dobytek	dobytek	k1gInSc1	dobytek
<g/>
,	,	kIx,	,
lamy	lama	k1gFnPc1	lama
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc1	koza
<g/>
..	..	k?	..
</s>
</p>
