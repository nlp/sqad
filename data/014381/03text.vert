<s>
Žaltman	Žaltman	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
nejvyšším	vysoký	k2eAgInSc6d3
vrcholu	vrchol	k1gInSc6
Jestřebích	Jestřebí	k1gNnPc6
hor.	hor.	k?
O	o	k7c6
chráněném	chráněný	k2eAgNnSc6d1
území	území	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Žaltman	Žaltman	k1gMnSc1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Žaltman	Žaltman	k1gMnSc1
Rozhledna	rozhledna	k1gFnSc1
Žaltman	Žaltman	k1gMnSc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
741	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
210	#num#	k4
m	m	kA
↓	↓	k?
JZ	JZ	kA
Chełmsko	Chełmsko	k1gNnSc4
Śląskie	Śląskie	k1gFnSc2
Izolace	izolace	k1gFnSc2
</s>
<s>
5,5	5,5	k4
km	km	kA
→	→	k?
Čáp	Čáp	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Seznamy	seznam	k1gInPc1
</s>
<s>
Nejprominentnější	prominentní	k2eAgFnPc4d3
hory	hora	k1gFnPc4
CZHory	CZHora	k1gFnSc2
Broumovské	broumovský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
Poznámka	poznámka	k1gFnSc1
</s>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Jestřebích	Jestřebí	k1gNnPc6
hor	hora	k1gFnPc2
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Broumovská	broumovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Žacléřská	žacléřský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Jestřebí	Jestřebí	k1gNnSc1
hory	hora	k1gFnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Žaltman	Žaltman	k1gMnSc1
</s>
<s>
Povodí	povodí	k1gNnSc1
</s>
<s>
Metuje	Metuje	k1gFnSc1
→	→	k?
Labe	Labe	k1gNnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Žaltman	Žaltman	k1gMnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
též	též	k9
Hexenstein	Hexenstein	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Jestřebích	Jestřebí	k1gNnPc6
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
okrsku	okrsek	k1gInSc2
Broumovské	broumovský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
2	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Malých	Malých	k2eAgFnPc2d1
Svatoňovic	Svatoňovice	k1gFnPc2
a	a	k8xC
9	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Trutnova	Trutnov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Přes	přes	k7c4
vrchol	vrchol	k1gInSc4
vede	vést	k5eAaImIp3nS
zeleně	zeleně	k6eAd1
značená	značený	k2eAgFnSc1d1
odbočka	odbočka	k1gFnSc1
z	z	k7c2
cesty	cesta	k1gFnSc2
po	po	k7c6
hřebeni	hřeben	k1gInSc6
Jestřebích	Jestřebí	k1gNnPc6
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
necelých	celý	k2eNgInPc2d1
200	#num#	k4
metrů	metr	k1gInPc2
od	od	k7c2
vrcholu	vrchol	k1gInSc2
vede	vést	k5eAaImIp3nS
i	i	k9
cyklotrasa	cyklotrasa	k1gFnSc1
č.	č.	k?
4091	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Okolí	okolí	k1gNnSc1
přístupových	přístupový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
je	být	k5eAaImIp3nS
bohaté	bohatý	k2eAgNnSc1d1
na	na	k7c4
zaniklá	zaniklý	k2eAgNnPc4d1
důlní	důlní	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
a	a	k8xC
lehká	lehký	k2eAgNnPc4d1
opevnění	opevnění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
původně	původně	k6eAd1
stála	stát	k5eAaImAgFnS
kovová	kovový	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
Žaltman	Žaltman	k1gMnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1967	#num#	k4
místním	místní	k2eAgInSc7d1
národním	národní	k2eAgInSc7d1
výborem	výbor	k1gInSc7
v	v	k7c6
Malých	Malých	k2eAgFnPc6d1
Svatoňovicích	Svatoňovice	k1gFnPc6
za	za	k7c2
pomoci	pomoc	k1gFnSc2
členů	člen	k1gInPc2
TJ	tj	kA
Baník	Baník	k1gInSc1
a	a	k8xC
KČT	KČT	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Rozhledna	rozhledna	k1gFnSc1
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhledna	rozhledna	k1gFnSc1
byla	být	k5eAaImAgFnS
demontována	demontovat	k5eAaBmNgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
září	září	k1gNnSc2
roku	rok	k1gInSc2
2020	#num#	k4
stojí	stát	k5eAaImIp3nS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
nová	nový	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostní	slavnostní	k2eAgInSc4d1
otevření	otevření	k1gNnSc1
rozhledny	rozhledna	k1gFnPc4
proběhlo	proběhnout	k5eAaPmAgNnS
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
vyhlídkovou	vyhlídkový	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
je	být	k5eAaImIp3nS
přístup	přístup	k1gInSc1
přes	přes	k7c4
točité	točitý	k2eAgNnSc4d1
schodiště	schodiště	k1gNnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
z	z	k7c2
ní	on	k3xPp3gFnSc2
panoramatický	panoramatický	k2eAgInSc1d1
výhled	výhled	k1gInSc4
na	na	k7c4
Krkonoše	Krkonoše	k1gFnPc4
(	(	kIx(
<g/>
Sněžka	Sněžka	k1gFnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rýchory	Rýchor	k1gInPc1
<g/>
,	,	kIx,
Vraní	vraní	k2eAgFnPc1d1
hory	hora	k1gFnPc1
(	(	kIx(
<g/>
Královecký	Královecký	k2eAgInSc1d1
Špičák	špičák	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Adršpašsko-teplické	adršpašsko-teplický	k2eAgFnSc2d1
skály	skála	k1gFnSc2
(	(	kIx(
<g/>
Čáp	Čáp	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Góry	Góry	k1gInPc1
Kamienne	Kamienn	k1gInSc5
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Waligóra	Waligóro	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Javoří	javoří	k1gNnSc1
hory	hora	k1gFnPc1
<g/>
,	,	kIx,
Soví	soví	k2eAgFnPc1d1
hory	hora	k1gFnPc1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
Sova	sova	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ostaš	Ostaš	k1gInSc1
<g/>
,	,	kIx,
Broumovské	broumovský	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
<g/>
,	,	kIx,
Stolové	stolový	k2eAgFnPc1d1
hory	hora	k1gFnPc1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
Hejšovina	Hejšovina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Králický	králický	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
<g/>
,	,	kIx,
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
,	,	kIx,
Rtyňsko	Rtyňsko	k1gNnSc1
<g/>
,	,	kIx,
Úpicko	Úpicko	k1gNnSc1
<g/>
,	,	kIx,
Kunětickou	kunětický	k2eAgFnSc4d1
horu	hora	k1gFnSc4
<g/>
,	,	kIx,
Zvičinu	Zvičina	k1gFnSc4
<g/>
,	,	kIx,
Kumburk	Kumburk	k1gInSc4
<g/>
,	,	kIx,
Tábor	Tábor	k1gInSc4
a	a	k8xC
blízké	blízký	k2eAgNnSc4d1
okolí	okolí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ultrakopce	Ultrakopce	k1gFnPc1
na	na	k7c4
Ultratisicovky	Ultratisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Malé	Malé	k2eAgFnPc1d1
Svatoňovice	Svatoňovice	k1gFnPc1
<g/>
:	:	kIx,
Spolky	spolek	k1gInPc1
a	a	k8xC
sdružení	sdružení	k1gNnSc1
-	-	kIx~
Klub	klub	k1gInSc1
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Žaltman	Žaltman	k1gMnSc1
na	na	k7c4
Rozhledny	rozhledna	k1gFnPc4
<g/>
.	.	kIx.
<g/>
webzdarma	webzdarma	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
</s>
