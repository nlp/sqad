<p>
<s>
Román	Román	k1gMnSc1	Román
Doktor	doktor	k1gMnSc1	doktor
Živago	Živago	k1gMnSc1	Živago
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Д	Д	k?	Д
Ж	Ж	k?	Ж
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
prózou	próza	k1gFnSc7	próza
ruského	ruský	k2eAgMnSc2d1	ruský
autora	autor	k1gMnSc2	autor
Borise	Boris	k1gMnSc2	Boris
Leonidoviče	Leonidovič	k1gMnSc2	Leonidovič
Pasternaka	Pasternak	k1gMnSc2	Pasternak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
politickým	politický	k2eAgInPc3d1	politický
poměrům	poměr	k1gInPc3	poměr
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
vydán	vydat	k5eAaPmNgInS	vydat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
"	"	kIx"	"
<g/>
Novyj	Novyj	k1gFnSc1	Novyj
mir	mir	k1gInSc1	mir
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
č.	č.	k?	č.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
také	také	k9	také
první	první	k4xOgNnSc4	první
sovětské	sovětský	k2eAgNnSc4d1	sovětské
knižní	knižní	k2eAgNnSc4d1	knižní
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
u	u	k7c2	u
nakladetelství	nakladetelství	k1gNnSc2	nakladetelství
Feltrinelli	Feltrinelle	k1gFnSc4	Feltrinelle
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
Jurije	Jurije	k1gMnSc2	Jurije
Andrejeviče	Andrejevič	k1gMnSc2	Andrejevič
Živaga	Živag	k1gMnSc2	Živag
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
současně	současně	k6eAd1	současně
píše	psát	k5eAaImIp3nS	psát
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
složitém	složitý	k2eAgInSc6d1	složitý
životním	životní	k2eAgInSc6d1	životní
příběhu	příběh	k1gInSc6	příběh
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
:	:	kIx,	:
Ruské	ruský	k2eAgFnSc2d1	ruská
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
během	během	k7c2	během
Ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
zobrazen	zobrazen	k2eAgInSc4d1	zobrazen
život	život	k1gInSc4	život
ruské	ruský	k2eAgFnSc2d1	ruská
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Živago	Živago	k1gMnSc1	Živago
</s>
</p>
<p>
<s>
Toňa	Toňa	k1gFnSc1	Toňa
-	-	kIx~	-
Antonina	Antonin	k2eAgFnSc1d1	Antonina
Alexandrovna	Alexandrovna	k1gFnSc1	Alexandrovna
Gromeková	Gromeková	k1gFnSc1	Gromeková
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
</s>
</p>
<p>
<s>
Lara	Lara	k1gFnSc1	Lara
-	-	kIx~	-
Larisa	Larisa	k1gFnSc1	Larisa
Fjodorovna	Fjodorovna	k1gFnSc1	Fjodorovna
Guichardová	Guichardová	k1gFnSc1	Guichardová
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
milenka	milenka	k1gFnSc1	milenka
</s>
</p>
<p>
<s>
Paša	paša	k1gMnSc1	paša
-	-	kIx~	-
Pavel	Pavel	k1gMnSc1	Pavel
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Antipov	Antipov	k1gInSc1	Antipov
(	(	kIx(	(
<g/>
Strelnikov	Strelnikov	k1gInSc1	Strelnikov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lařin	Lařin	k1gMnSc1	Lařin
manžel	manžel	k1gMnSc1	manžel
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Ipolitovič	Ipolitovič	k1gMnSc1	Ipolitovič
Komarovskij	Komarovskij	k1gMnSc1	Komarovskij
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
Živagova	Živagův	k2eAgMnSc2d1	Živagův
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Lařin	Lařin	k2eAgMnSc1d1	Lařin
bývalý	bývalý	k2eAgMnSc1d1	bývalý
milenec	milenec	k1gMnSc1	milenec
</s>
</p>
<p>
<s>
Jevgraf	Jevgraf	k1gMnSc1	Jevgraf
Živago	Živago	k1gMnSc1	Živago
<g/>
,	,	kIx,	,
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
Jurije	Jurije	k1gMnSc1	Jurije
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Román	Román	k1gMnSc1	Román
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
sleduje	sledovat	k5eAaImIp3nS	sledovat
osudy	osud	k1gInPc4	osud
Jurije	Jurije	k1gMnSc4	Jurije
Živaga	Živag	k1gMnSc4	Živag
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
zemřela	zemřít	k5eAaPmAgFnS	zemřít
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
neznámo	neznámo	k6eAd1	neznámo
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
nevede	vést	k5eNaImIp3nS	vést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
kdysi	kdysi	k6eAd1	kdysi
jméno	jméno	k1gNnSc4	jméno
Živago	Živago	k6eAd1	Živago
mělo	mít	k5eAaImAgNnS	mít
lesk	lesk	k1gInSc4	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
smutném	smutný	k2eAgInSc6d1	smutný
konci	konec	k1gInSc6	konec
má	mít	k5eAaImIp3nS	mít
podíl	podíl	k1gInSc1	podíl
jeho	jeho	k3xOp3gNnSc1	jeho
osobní	osobní	k2eAgMnSc1d1	osobní
právník	právník	k1gMnSc1	právník
Komarovskij	Komarovskij	k1gMnSc1	Komarovskij
<g/>
.	.	kIx.	.
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
strýcovou	strýcův	k2eAgFnSc7d1	strýcova
dcerou	dcera	k1gFnSc7	dcera
Toňou	Toňý	k2eAgFnSc7d1	Toňý
<g/>
.	.	kIx.	.
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
maminka	maminka	k1gFnSc1	maminka
Toni	Toni	k1gFnSc2	Toni
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
řekne	říct	k5eAaPmIp3nS	říct
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souběžně	souběžně	k6eAd1	souběžně
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
osud	osud	k1gInSc1	osud
Lary	Lara	k1gFnSc2	Lara
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
vlastní	vlastní	k2eAgFnSc4d1	vlastní
krejčovskou	krejčovský	k2eAgFnSc4d1	krejčovská
dílnu	dílna	k1gFnSc4	dílna
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
ji	on	k3xPp3gFnSc4	on
Komarovskij	Komarovskij	k1gFnSc4	Komarovskij
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
naváže	navázat	k5eAaPmIp3nS	navázat
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Larou	Lara	k1gFnSc7	Lara
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
však	však	k9	však
pokračující	pokračující	k2eAgInSc4d1	pokračující
vztah	vztah	k1gInSc4	vztah
ničí	ničit	k5eAaImIp3nS	ničit
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vymanit	vymanit	k5eAaPmF	vymanit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
pokusí	pokusit	k5eAaPmIp3nP	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
záchraně	záchrana	k1gFnSc3	záchrana
<g/>
,	,	kIx,	,
setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
poprvé	poprvé	k6eAd1	poprvé
mladý	mladý	k2eAgMnSc1d1	mladý
Jurij	Jurij	k1gMnSc1	Jurij
s	s	k7c7	s
Larou	Lara	k1gMnSc7	Lara
<g/>
.	.	kIx.	.
</s>
<s>
Lara	Lara	k6eAd1	Lara
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
osamostatní	osamostatnit	k5eAaPmIp3nS	osamostatnit
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
vychovatelka	vychovatelka	k1gFnSc1	vychovatelka
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
své	svůj	k3xOyFgFnSc2	svůj
spolužačky	spolužačka	k1gFnSc2	spolužačka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
spokojená	spokojený	k2eAgFnSc1d1	spokojená
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
chráněnka	chráněnka	k1gFnSc1	chráněnka
odroste	odrůst	k5eAaPmIp3nS	odrůst
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
vyhledat	vyhledat	k5eAaPmF	vyhledat
Komarovského	Komarovský	k2eAgMnSc4d1	Komarovský
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
nezištně	zištně	k6eNd1	zištně
pomohl	pomoct	k5eAaPmAgMnS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Zastihne	zastihnout	k5eAaPmIp3nS	zastihnout
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
vánočním	vánoční	k2eAgInSc6d1	vánoční
večírku	večírek	k1gInSc6	večírek
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Jurijem	Jurij	k1gMnSc7	Jurij
<g/>
.	.	kIx.	.
</s>
<s>
Lara	Lara	k6eAd1	Lara
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ranou	rána	k1gFnSc7	rána
není	být	k5eNaImIp3nS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
Komarovskij	Komarovskij	k1gMnSc1	Komarovskij
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
kolega	kolega	k1gMnSc1	kolega
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Komarovskému	Komarovský	k2eAgMnSc3d1	Komarovský
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
skandál	skandál	k1gInSc1	skandál
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
kontaktům	kontakt	k1gInPc3	kontakt
ututlat	ututlat	k5eAaPmF	ututlat
a	a	k8xC	a
Lara	Lara	k1gMnSc1	Lara
může	moct	k5eAaImIp3nS	moct
dostudovat	dostudovat	k5eAaPmF	dostudovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
školy	škola	k1gFnSc2	škola
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
Pašu	paša	k1gMnSc4	paša
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
do	do	k7c2	do
Jurjatinu	Jurjatina	k1gFnSc4	Jurjatina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Lara	Lara	k1gMnSc1	Lara
i	i	k8xC	i
Paša	paša	k1gMnSc1	paša
pracují	pracovat	k5eAaImIp3nP	pracovat
jako	jako	k9	jako
učitelé	učitel	k1gMnPc1	učitel
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
a	a	k8xC	a
narodí	narodit	k5eAaPmIp3nP	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dcera	dcera	k1gFnSc1	dcera
Kátěnka	Kátěnka	k1gFnSc1	Kátěnka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začala	začít	k5eAaPmAgFnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jurijovi	Jurij	k1gMnSc3	Jurij
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
syn	syn	k1gMnSc1	syn
Saša	Saša	k1gMnSc1	Saša
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
lékařů	lékař	k1gMnPc2	lékař
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
musí	muset	k5eAaImIp3nS	muset
narukovat	narukovat	k5eAaPmF	narukovat
<g/>
.	.	kIx.	.
</s>
<s>
Paša	paša	k1gMnSc1	paša
také	také	k9	také
narukuje	narukovat	k5eAaPmIp3nS	narukovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
Lara	Lar	k1gInSc2	Lar
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
hledat	hledat	k5eAaImF	hledat
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	s	k7c7	s
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
sestrou	sestra	k1gFnSc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
Živagem	Živag	k1gMnSc7	Živag
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Laru	Lara	k1gFnSc4	Lara
už	už	k6eAd1	už
dvakrát	dvakrát	k6eAd1	dvakrát
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
Toně	Tona	k1gFnSc6	Tona
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
získá	získat	k5eAaPmIp3nS	získat
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
navázal	navázat	k5eAaPmAgInS	navázat
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Paša	paša	k1gMnSc1	paša
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
se	se	k3xPyFc4	se
Živago	Živago	k6eAd1	Živago
vrací	vracet	k5eAaImIp3nS	vracet
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
nyní	nyní	k6eAd1	nyní
užívá	užívat	k5eAaImIp3nS	užívat
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
pokoje	pokoj	k1gInPc4	pokoj
svého	svůj	k3xOyFgInSc2	svůj
velkého	velký	k2eAgInSc2d1	velký
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
vládne	vládnout	k5eAaImIp3nS	vládnout
bída	bída	k1gFnSc1	bída
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
tomu	ten	k3xDgMnSc3	ten
rodina	rodina	k1gFnSc1	rodina
unikla	uniknout	k5eAaPmAgFnS	uniknout
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
žít	žít	k5eAaImF	žít
nenápadně	nápadně	k6eNd1	nápadně
<g/>
,	,	kIx,	,
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
odjet	odjet	k5eAaPmF	odjet
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
někdejší	někdejší	k2eAgFnSc4d1	někdejší
usedlost	usedlost	k1gFnSc4	usedlost
Varykino	Varykin	k2eAgNnSc1d1	Varykin
u	u	k7c2	u
města	město	k1gNnSc2	město
Jurjatin	Jurjatina	k1gFnPc2	Jurjatina
na	na	k7c6	na
Uralu	Ural	k1gInSc6	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vlakem	vlak	k1gInSc7	vlak
v	v	k7c6	v
nákladních	nákladní	k2eAgInPc6d1	nákladní
vagónech	vagón	k1gInPc6	vagón
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
zastávce	zastávka	k1gFnSc6	zastávka
Jurij	Jurij	k1gFnSc2	Jurij
vystoupí	vystoupit	k5eAaPmIp3nP	vystoupit
a	a	k8xC	a
zadrží	zadržet	k5eAaPmIp3nP	zadržet
ho	on	k3xPp3gNnSc4	on
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odveden	odveden	k2eAgInSc1d1	odveden
ke	k	k7c3	k
Strelnikovovi	Strelnikova	k1gMnSc3	Strelnikova
<g/>
,	,	kIx,	,
rudému	rudý	k2eAgMnSc3d1	rudý
vojenskému	vojenský	k2eAgMnSc3d1	vojenský
komisaři	komisař	k1gMnSc3	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Strelnikov	Strelnikov	k1gInSc1	Strelnikov
není	být	k5eNaImIp3nS	být
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
Lařin	Lařin	k2eAgMnSc1d1	Lařin
domněle	domněle	k6eAd1	domněle
padlý	padlý	k1gMnSc1	padlý
muž	muž	k1gMnSc1	muž
Paša	paša	k1gMnSc1	paša
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgMnS	být
Jurij	Jurij	k1gMnSc1	Jurij
pro	pro	k7c4	pro
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
neschvaloval	schvalovat	k5eNaImAgMnS	schvalovat
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
přístavku	přístavek	k1gInSc6	přístavek
domu	dům	k1gInSc2	dům
ve	v	k7c6	v
Varykinu	Varykin	k1gInSc6	Varykin
<g/>
,	,	kIx,	,
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
,	,	kIx,	,
Živago	Živago	k1gMnSc1	Živago
jako	jako	k8xS	jako
lékař	lékař	k1gMnSc1	lékař
radí	radit	k5eAaImIp3nS	radit
lidem	lid	k1gInSc7	lid
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Toňa	Toňa	k1gFnSc1	Toňa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Pomůže	pomoct	k5eAaPmIp3nS	pomoct
mu	on	k3xPp3gNnSc3	on
zde	zde	k6eAd1	zde
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
Jevgraf	Jevgraf	k1gMnSc1	Jevgraf
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
už	už	k6eAd1	už
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Živago	Živago	k6eAd1	Živago
začne	začít	k5eAaPmIp3nS	začít
zajíždět	zajíždět	k5eAaImF	zajíždět
do	do	k7c2	do
Jurjatinu	Jurjatina	k1gFnSc4	Jurjatina
do	do	k7c2	do
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Larou	Lara	k1gFnSc7	Lara
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
scházet	scházet	k5eAaImF	scházet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
poměr	poměr	k1gInSc1	poměr
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přepaden	přepadnout	k5eAaPmNgMnS	přepadnout
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Živago	Živago	k6eAd1	Živago
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
musí	muset	k5eAaImIp3nS	muset
žít	žít	k5eAaImF	žít
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
popisován	popisovat	k5eAaImNgInS	popisovat
jejich	jejich	k3xOp3gInSc1	jejich
kočovný	kočovný	k2eAgInSc1d1	kočovný
život	život	k1gInSc1	život
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
boje	boj	k1gInPc1	boj
<g/>
,	,	kIx,	,
krutost	krutost	k1gFnSc1	krutost
obou	dva	k4xCgFnPc2	dva
bojujících	bojující	k2eAgFnPc2d1	bojující
stran	strana	k1gFnPc2	strana
i	i	k8xC	i
strach	strach	k1gInSc1	strach
partyzánů	partyzán	k1gMnPc2	partyzán
o	o	k7c4	o
jejich	jejich	k3xOp3gFnPc4	jejich
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živago	Živago	k6eAd1	Živago
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
uprchnout	uprchnout	k5eAaPmF	uprchnout
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
do	do	k7c2	do
Jurjatinu	Jurjatina	k1gFnSc4	Jurjatina
<g/>
.	.	kIx.	.
</s>
<s>
Dozví	dozvědět	k5eAaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Toňa	Toňa	k1gFnSc1	Toňa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mezitím	mezitím	k6eAd1	mezitím
porodila	porodit	k5eAaPmAgFnS	porodit
holčičku	holčička	k1gFnSc4	holčička
Mášu	Máša	k1gFnSc4	Máša
<g/>
,	,	kIx,	,
s	s	k7c7	s
tchánem	tchán	k1gMnSc7	tchán
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Živago	Živago	k6eAd1	Živago
žije	žít	k5eAaImIp3nS	žít
u	u	k7c2	u
Lary	Lara	k1gFnSc2	Lara
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
je	být	k5eAaImIp3nS	být
nemocný	mocný	k2eNgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Laře	Laře	k6eAd1	Laře
i	i	k8xC	i
jemu	on	k3xPp3gNnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
:	:	kIx,	:
Laře	Laře	k1gFnSc1	Laře
jako	jako	k8xC	jako
manželce	manželka	k1gFnSc3	manželka
Strelnikova	Strelnikův	k2eAgFnSc1d1	Strelnikův
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jurijovi	Jurij	k1gMnSc3	Jurij
jako	jako	k8xC	jako
synovi	syn	k1gMnSc3	syn
"	"	kIx"	"
<g/>
sibiřského	sibiřský	k2eAgMnSc4d1	sibiřský
milionáře	milionář	k1gMnSc4	milionář
<g/>
"	"	kIx"	"
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
nesouhlasícího	souhlasící	k2eNgMnSc2d1	nesouhlasící
s	s	k7c7	s
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Toni	Toni	k1gFnSc2	Toni
mu	on	k3xPp3gMnSc3	on
dojde	dojít	k5eAaPmIp3nS	dojít
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
vypovězena	vypovědět	k5eAaPmNgFnS	vypovědět
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c7	za
Živagem	Živag	k1gMnSc7	Živag
a	a	k8xC	a
Larou	Lara	k1gMnSc7	Lara
přijde	přijít	k5eAaPmIp3nS	přijít
Komarovskij	Komarovskij	k1gMnSc1	Komarovskij
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
jim	on	k3xPp3gMnPc3	on
pomoc	pomoc	k1gFnSc1	pomoc
–	–	k?	–
mohl	moct	k5eAaImAgMnS	moct
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
odvézt	odvézt	k5eAaPmF	odvézt
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
tak	tak	k6eAd1	tak
jejich	jejich	k3xOp3gFnSc4	jejich
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnou	odmítnout	k5eAaPmIp3nP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
strávit	strávit	k5eAaPmF	strávit
zbývající	zbývající	k2eAgInSc4d1	zbývající
čas	čas	k1gInSc4	čas
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
ve	v	k7c6	v
Varykinu	Varykin	k1gInSc6	Varykin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebudou	být	k5eNaImBp3nP	být
tolik	tolik	k6eAd1	tolik
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tam	tam	k6eAd1	tam
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
přijede	přijet	k5eAaPmIp3nS	přijet
Komarovskij	Komarovskij	k1gFnSc4	Komarovskij
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svou	svůj	k3xOyFgFnSc4	svůj
nabídku	nabídka	k1gFnSc4	nabídka
opakoval	opakovat	k5eAaImAgMnS	opakovat
<g/>
,	,	kIx,	,
Živago	Živago	k6eAd1	Živago
ji	on	k3xPp3gFnSc4	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Komarovskij	Komarovskít	k5eAaPmRp2nS	Komarovskít
Jurijovi	Jurijův	k2eAgMnPc1d1	Jurijův
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Strelnikov	Strelnikov	k1gInSc1	Strelnikov
je	být	k5eAaImIp3nS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
a	a	k8xC	a
Laře	Laře	k1gInSc1	Laře
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tedy	tedy	k9	tedy
aspoň	aspoň	k9	aspoň
předstíral	předstírat	k5eAaImAgMnS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabídku	nabídka	k1gFnSc4	nabídka
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Lara	Lara	k1gFnSc1	Lara
s	s	k7c7	s
Kátěnkou	Kátěnka	k1gFnSc7	Kátěnka
a	a	k8xC	a
Komarovským	Komarovský	k2eAgMnSc7d1	Komarovský
odjede	odjet	k5eAaPmIp3nS	odjet
<g/>
,	,	kIx,	,
Živago	Živago	k6eAd1	Živago
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
se	s	k7c7	s
Strelnikovem	Strelnikov	k1gInSc7	Strelnikov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nebyl	být	k5eNaImAgInS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
ve	v	k7c6	v
Varykinu	Varykin	k1gInSc6	Varykin
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
píše	psát	k5eAaImIp3nS	psát
drobné	drobný	k2eAgInPc4d1	drobný
spisky	spisek	k1gInPc4	spisek
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
za	za	k7c4	za
Toňou	Toňá	k1gFnSc4	Toňá
<g/>
.	.	kIx.	.
</s>
<s>
Zanechá	zanechat	k5eAaPmIp3nS	zanechat
lékařské	lékařský	k2eAgFnSc2d1	lékařská
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
přestane	přestat	k5eAaPmIp3nS	přestat
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
dbát	dbát	k5eAaImF	dbát
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
Marinou	Marina	k1gFnSc7	Marina
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc1	dva
holčičky	holčička	k1gFnPc1	holčička
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
potká	potkat	k5eAaPmIp3nS	potkat
Jevgrafa	Jevgraf	k1gMnSc4	Jevgraf
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
opatří	opatřit	k5eAaPmIp3nS	opatřit
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
poprvé	poprvé	k6eAd1	poprvé
jede	jet	k5eAaImIp3nS	jet
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
v	v	k7c6	v
tramvaji	tramvaj	k1gFnSc6	tramvaj
srdeční	srdeční	k2eAgInSc4d1	srdeční
záchvat	záchvat	k1gInSc4	záchvat
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
pohřeb	pohřeb	k1gInSc4	pohřeb
přijde	přijít	k5eAaPmIp3nS	přijít
i	i	k9	i
Lara	Lara	k1gFnSc1	Lara
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přijela	přijet	k5eAaPmAgFnS	přijet
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
Jevgrafem	Jevgraf	k1gInSc7	Jevgraf
<g/>
.	.	kIx.	.
</s>
<s>
Pomůže	pomoct	k5eAaPmIp3nS	pomoct
mu	on	k3xPp3gMnSc3	on
pořádat	pořádat	k5eAaImF	pořádat
Jurijovy	Jurijův	k2eAgFnPc4d1	Jurijův
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
poprosí	poprosit	k5eAaPmIp3nP	poprosit
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
své	svůj	k3xOyFgFnSc2	svůj
a	a	k8xC	a
Jurijovy	Jurijův	k2eAgFnSc2d1	Jurijův
dcery	dcera	k1gFnSc2	dcera
Táni	Táňa	k1gFnSc2	Táňa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kdysi	kdysi	k6eAd1	kdysi
–	–	k?	–
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
Komarovským	Komarovský	k2eAgInSc7d1	Komarovský
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Ruska	Rusko	k1gNnSc2	Rusko
-	-	kIx~	-
musela	muset	k5eAaImAgFnS	muset
svěřit	svěřit	k5eAaPmF	svěřit
do	do	k7c2	do
cizích	cizí	k2eAgFnPc2d1	cizí
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
však	však	k8xC	však
Lara	Lara	k1gMnSc1	Lara
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byla	být	k5eAaImAgFnS	být
zatčena	zatčen	k2eAgFnSc1d1	zatčena
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dospělá	dospělý	k2eAgFnSc1d1	dospělá
Táňa	Táňa	k1gFnSc1	Táňa
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Jevgrafem	Jevgraf	k1gInSc7	Jevgraf
Živagem	Živag	k1gMnSc7	Živag
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
strýcem	strýc	k1gMnSc7	strýc
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
chce	chtít	k5eAaImIp3nS	chtít
ujmout	ujmout	k5eAaPmF	ujmout
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
náhodě	náhoda	k1gFnSc3	náhoda
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
natočen	natočit	k5eAaBmNgInS	natočit
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Davida	David	k1gMnSc2	David
Leana	Lean	k1gMnSc2	Lean
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
doktora	doktor	k1gMnSc2	doktor
Živaga	Živag	k1gMnSc2	Živag
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgMnS	představit
Omar	Omar	k1gMnSc1	Omar
Sharif	Sharif	k1gMnSc1	Sharif
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
hráli	hrát	k5eAaImAgMnP	hrát
Julie	Julie	k1gFnPc4	Julie
Christie	Christie	k1gFnSc2	Christie
jako	jako	k8xS	jako
Lara	Larum	k1gNnSc2	Larum
Geraldine	Geraldin	k1gInSc5	Geraldin
Chaplin	Chaplin	k1gInSc1	Chaplin
jako	jako	k8xC	jako
Toňa	Toňa	k1gFnSc1	Toňa
<g/>
,	,	kIx,	,
Alec	Alec	k1gFnSc1	Alec
Guiness	Guiness	k1gMnSc1	Guiness
jako	jako	k8xS	jako
Jevgraf	Jevgraf	k1gMnSc1	Jevgraf
Živago	Živago	k1gMnSc1	Živago
<g/>
,	,	kIx,	,
Rita	Rita	k1gFnSc1	Rita
Tushingham	Tushingham	k1gInSc1	Tushingham
jako	jako	k8xS	jako
Táňa	Táňa	k1gFnSc1	Táňa
<g/>
,	,	kIx,	,
Rod	rod	k1gInSc1	rod
Steiger	Steigra	k1gFnPc2	Steigra
jako	jako	k8xC	jako
Komarovskij	Komarovskij	k1gFnPc2	Komarovskij
<g/>
;	;	kIx,	;
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Klaus	Klaus	k1gMnSc1	Klaus
Kinski	Kinsk	k1gFnSc2	Kinsk
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
Oscarů	Oscar	k1gMnPc2	Oscar
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
filmových	filmový	k2eAgFnPc2d1	filmová
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
knižní	knižní	k2eAgFnSc1d1	knižní
předloha	předloha	k1gFnSc1	předloha
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
až	až	k6eAd1	až
do	do	k7c2	do
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
dvacatého	dvacatý	k2eAgNnSc2d1	dvacatý
století	století	k1gNnSc2	století
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
zfilmování	zfilmování	k1gNnSc1	zfilmování
této	tento	k3xDgFnSc2	tento
předlohy	předloha	k1gFnSc2	předloha
pak	pak	k6eAd1	pak
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
od	od	k7c2	od
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
britsko-americký	britskomerický	k2eAgInSc4d1	britsko-americký
snímek	snímek	k1gInSc4	snímek
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
režiséra	režisér	k1gMnSc2	režisér
Giacoma	Giacom	k1gMnSc2	Giacom
Campiottiho	Campiotti	k1gMnSc2	Campiotti
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
Hans	Hans	k1gMnSc1	Hans
Matheson	Matheson	k1gMnSc1	Matheson
(	(	kIx(	(
<g/>
Živago	Živago	k1gMnSc1	Živago
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Keira	Keira	k1gMnSc1	Keira
Knightley	Knightlea	k1gFnSc2	Knightlea
(	(	kIx(	(
<g/>
Lara	Lara	k1gMnSc1	Lara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alexandra	Alexandr	k1gMnSc2	Alexandr
Maria	Mario	k1gMnSc2	Mario
Lara	Larus	k1gMnSc2	Larus
(	(	kIx(	(
<g/>
Toňa	Toňa	k1gMnSc1	Toňa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
Neill	Neill	k1gMnSc1	Neill
(	(	kIx(	(
<g/>
Komarovskij	Komarovskij	k1gMnSc1	Komarovskij
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
natočen	natočen	k2eAgInSc4d1	natočen
11	[number]	k4	11
<g/>
dílný	dílný	k2eAgInSc4d1	dílný
ruský	ruský	k2eAgInSc4d1	ruský
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
Oleg	Oleg	k1gMnSc1	Oleg
Menšikov	Menšikov	k1gInSc1	Menšikov
(	(	kIx(	(
<g/>
Živago	Živago	k6eAd1	Živago
<g/>
)	)	kIx)	)
a	a	k8xC	a
Chulpan	Chulpan	k1gInSc4	Chulpan
Chamatova	Chamatův	k2eAgNnSc2d1	Chamatův
(	(	kIx(	(
<g/>
Lara	Larum	k1gNnSc2	Larum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
