<s>
Obec	obec	k1gFnSc1	obec
Babice	babice	k1gFnSc2	babice
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Dolnomoravského	dolnomoravský	k2eAgInSc2d1	dolnomoravský
úvalu	úval	k1gInSc2	úval
<g/>
,	,	kIx,	,
v	v	k7c6	v
údolní	údolní	k2eAgFnSc6d1	údolní
nivě	niva	k1gFnSc6	niva
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
