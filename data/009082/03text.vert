<p>
<s>
Obec	obec	k1gFnSc1	obec
Babice	babice	k1gFnSc2	babice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Babice	babice	k1gFnSc2	babice
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Dolnomoravského	dolnomoravský	k2eAgInSc2d1	dolnomoravský
úvalu	úval	k1gInSc2	úval
<g/>
,	,	kIx,	,
v	v	k7c6	v
údolní	údolní	k2eAgFnSc6d1	údolní
nivě	niva	k1gFnSc6	niva
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc4d1	západní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
začínající	začínající	k2eAgNnSc1d1	začínající
pásmo	pásmo	k1gNnSc1	pásmo
Chřibů	Chřiby	k1gInPc2	Chřiby
<g/>
.	.	kIx.	.
</s>
<s>
Babice	babice	k1gFnSc1	babice
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
660	[number]	k4	660
ha	ha	kA	ha
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
181	[number]	k4	181
<g/>
–	–	k?	–
<g/>
185	[number]	k4	185
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
od	od	k7c2	od
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1220	[number]	k4	1220
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Robert	Robert	k1gMnSc1	Robert
daroval	darovat	k5eAaPmAgMnS	darovat
velehradskému	velehradský	k2eAgInSc3d1	velehradský
cisterciáckému	cisterciácký	k2eAgInSc3d1	cisterciácký
klášteru	klášter	k1gInSc3	klášter
obilné	obilný	k2eAgFnSc2d1	obilná
a	a	k8xC	a
vinné	vinný	k2eAgFnSc2d1	vinná
desátky	desátka	k1gFnSc2	desátka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velehradskému	velehradský	k2eAgInSc3d1	velehradský
klášteru	klášter	k1gInSc3	klášter
patřila	patřit	k5eAaImAgFnS	patřit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
parcelaci	parcelace	k1gFnSc6	parcelace
vrchnostenských	vrchnostenský	k2eAgInPc2d1	vrchnostenský
dvorů	dvůr	k1gInPc2	dvůr
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
i	i	k8xC	i
babický	babický	k2eAgInSc1d1	babický
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
zřízena	zřízen	k2eAgFnSc1d1	zřízena
osada	osada	k1gFnSc1	osada
Cerony	Cerona	k1gFnSc2	Cerona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
číslování	číslování	k1gNnSc4	číslování
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
stopy	stopa	k1gFnPc4	stopa
osídlení	osídlení	k1gNnPc2	osídlení
však	však	k9	však
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
pochází	pocházet	k5eAaImIp3nS	pocházet
pečeť	pečeť	k1gFnSc4	pečeť
obce	obec	k1gFnSc2	obec
–	–	k?	–
v	v	k7c6	v
pečetním	pečetní	k2eAgNnSc6d1	pečetní
poli	pole	k1gNnSc6	pole
lemovaném	lemovaný	k2eAgNnSc6d1	lemované
vavřínovým	vavřínový	k2eAgInSc7d1	vavřínový
věncem	věnec	k1gInSc7	věnec
je	být	k5eAaImIp3nS	být
vlevo	vlevo	k6eAd1	vlevo
vinařský	vinařský	k2eAgInSc4d1	vinařský
nůž	nůž	k1gInSc4	nůž
a	a	k8xC	a
vpravo	vpravo	k6eAd1	vpravo
hrozen	hrozen	k1gInSc1	hrozen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Babice	babice	k1gFnSc2	babice
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
díky	díky	k7c3	díky
výrobně	výrobna	k1gFnSc3	výrobna
paštik	paštika	k1gFnPc2	paštika
a	a	k8xC	a
hotových	hotový	k2eAgNnPc2d1	hotové
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
realizována	realizovat	k5eAaBmNgFnS	realizovat
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
společností	společnost	k1gFnPc2	společnost
Hamé	Hamé	k1gNnSc2	Hamé
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Fruta	Frut	k1gMnSc2	Frut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecní	obecní	k2eAgFnSc1d1	obecní
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
starostou	starosta	k1gMnSc7	starosta
Miloslav	Miloslav	k1gMnSc1	Miloslav
Maňásek	Maňásek	k1gMnSc1	Maňásek
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
funkci	funkce	k1gFnSc4	funkce
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
Martina	Martina	k1gFnSc1	Martina
Horňáková	Horňáková	k1gFnSc1	Horňáková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Obcí	obec	k1gFnSc7	obec
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
už	už	k6eAd1	už
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
jejího	její	k3xOp3gInSc2	její
katastru	katastr	k1gInSc2	katastr
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
fakticky	fakticky	k6eAd1	fakticky
již	již	k6eAd1	již
na	na	k7c6	na
území	území	k1gNnSc6	území
Huštěnovic	Huštěnovice	k1gFnPc2	Huštěnovice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Huštěnovice	Huštěnovice	k1gFnSc2	Huštěnovice
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Přerov	Přerov	k1gInSc1	Přerov
<g/>
–	–	k?	–
<g/>
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
sídlí	sídlet	k5eAaImIp3nS	sídlet
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Babice	babice	k1gFnSc2	babice
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
má	mít	k5eAaImIp3nS	mít
klub	klub	k1gInSc4	klub
106	[number]	k4	106
aktivních	aktivní	k2eAgInPc2d1	aktivní
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
má	mít	k5eAaImIp3nS	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
několika	několik	k4yIc6	několik
věkových	věkový	k2eAgFnPc6d1	věková
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
:	:	kIx,	:
přípravka	přípravka	k1gFnSc1	přípravka
hrající	hrající	k2eAgFnSc4d1	hrající
okresní	okresní	k2eAgFnSc4d1	okresní
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
žáci	žák	k1gMnPc1	žák
hrající	hrající	k2eAgFnSc4d1	hrající
okresní	okresní	k2eAgFnSc4d1	okresní
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
dorost	dorost	k1gInSc1	dorost
hrající	hrající	k2eAgInSc1d1	hrající
okresní	okresní	k2eAgInSc1d1	okresní
přebor	přebor	k1gInSc1	přebor
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
hrající	hrající	k2eAgFnSc4d1	hrající
okresní	okresní	k2eAgFnSc4d1	okresní
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c6	na
Uherskohradišťsku	Uherskohradišťsko	k1gNnSc6	Uherskohradišťsko
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sezónou	sezóna	k1gFnSc7	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
družstvo	družstvo	k1gNnSc4	družstvo
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
odhlásilo	odhlásit	k5eAaPmAgNnS	odhlásit
z	z	k7c2	z
krajského	krajský	k2eAgInSc2d1	krajský
přeboru	přebor	k1gInSc2	přebor
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gInSc1	Sokol
Babice	babice	k1gFnSc1	babice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
hlavně	hlavně	k9	hlavně
zásluhou	zásluha	k1gFnSc7	zásluha
učitelů	učitel	k1gMnPc2	učitel
p.	p.	k?	p.
Válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
p.	p.	k?	p.
Grundmana	Grundman	k1gMnSc2	Grundman
<g/>
,	,	kIx,	,
p.	p.	k?	p.
Maňáska	Maňásek	k1gMnSc2	Maňásek
<g/>
,	,	kIx,	,
p.	p.	k?	p.
Varmuži	varmuže	k1gFnSc4	varmuže
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
TJ	tj	kA	tj
Sokol	Sokol	k1gInSc4	Sokol
Babice	babice	k1gFnSc2	babice
spravují	spravovat	k5eAaImIp3nP	spravovat
David	David	k1gMnSc1	David
Šurmánek	Šurmánek	k1gMnSc1	Šurmánek
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
Juráňová	Juráňová	k1gFnSc1	Juráňová
a	a	k8xC	a
Marek	Marek	k1gMnSc1	Marek
Ulman	Ulman	k1gMnSc1	Ulman
<g/>
.	.	kIx.	.
</s>
<s>
TJ	tj	kA	tj
Sokol	Sokol	k1gMnSc1	Sokol
nabízí	nabízet	k5eAaImIp3nS	nabízet
pohybové	pohybový	k2eAgNnSc4d1	pohybové
cvičení	cvičení	k1gNnSc4	cvičení
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
předškolního	předškolní	k2eAgInSc2d1	předškolní
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
pořádá	pořádat	k5eAaImIp3nS	pořádat
výlety	výlet	k1gInPc4	výlet
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
posilovnu	posilovna	k1gFnSc4	posilovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Marcela	Marcela	k1gFnSc1	Marcela
Babáčková	Babáčková	k1gFnSc1	Babáčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistryně	mistryně	k1gFnPc4	mistryně
ČR	ČR	kA	ČR
a	a	k8xC	a
ČSFR	ČSFR	kA	ČSFR
v	v	k7c6	v
kulturistice	kulturistika	k1gFnSc6	kulturistika
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Býček	býček	k1gMnSc1	býček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kickboxu	kickbox	k1gInSc6	kickbox
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Horníček	Horníček	k1gMnSc1	Horníček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Marcel	Marcel	k1gMnSc1	Marcel
Litoš	Litoš	k1gMnSc1	Litoš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Sečen	sečen	k2eAgMnSc1d1	sečen
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
NEKUDA	Nekuda	k1gMnSc1	Nekuda
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
JANÁK	Janák	k1gMnSc1	Janák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
MICHNA	Michna	k1gMnSc1	Michna
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Uherskohradišťsko	Uherskohradišťsko	k1gNnSc1	Uherskohradišťsko
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Muzejní	muzejní	k2eAgFnSc1d1	muzejní
a	a	k8xC	a
vlastivědná	vlastivědný	k2eAgFnSc1d1	vlastivědná
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Slovácké	slovácký	k2eAgInPc1d1	slovácký
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
855	[number]	k4	855
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85048	[number]	k4	85048
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Babice	babice	k1gFnSc1	babice
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Babice	babice	k1gFnSc2	babice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Babice	babice	k1gFnSc2	babice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
