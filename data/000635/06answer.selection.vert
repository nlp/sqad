<s>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1530	[number]	k4	1530
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Ivan	Ivan	k1gMnSc1	Ivan
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
(	(	kIx(	(
И	И	k?	И
Г	Г	k?	Г
<g/>
;	;	kIx,	;
přídomek	přídomek	k1gInSc1	přídomek
"	"	kIx"	"
<g/>
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
<g/>
"	"	kIx"	"
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Г	Г	k?	Г
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
znamená	znamenat	k5eAaImIp3nS	znamenat
impozantní	impozantní	k2eAgInSc1d1	impozantní
nebo	nebo	k8xC	nebo
úžasný	úžasný	k2eAgInSc1d1	úžasný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
také	také	k9	také
Michael	Michael	k1gMnSc1	Michael
Fedorov	Fedorov	k1gInSc1	Fedorov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
moskevský	moskevský	k2eAgInSc4d1	moskevský
velkokníže	velkokníže	k1gNnSc1wR	velkokníže
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
první	první	k4xOgInSc1	první
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
<g/>
.	.	kIx.	.
</s>
