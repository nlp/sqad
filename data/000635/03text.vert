<s>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1530	[number]	k4	1530
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Ivan	Ivan	k1gMnSc1	Ivan
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
(	(	kIx(	(
И	И	k?	И
Г	Г	k?	Г
<g/>
;	;	kIx,	;
přídomek	přídomek	k1gInSc1	přídomek
"	"	kIx"	"
<g/>
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
<g/>
"	"	kIx"	"
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Г	Г	k?	Г
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
znamená	znamenat	k5eAaImIp3nS	znamenat
impozantní	impozantní	k2eAgInSc1d1	impozantní
nebo	nebo	k8xC	nebo
úžasný	úžasný	k2eAgInSc1d1	úžasný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
také	také	k9	také
Michael	Michael	k1gMnSc1	Michael
Fedorov	Fedorov	k1gInSc1	Fedorov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
moskevský	moskevský	k2eAgInSc4d1	moskevský
velkokníže	velkokníže	k1gNnSc1wR	velkokníže
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
první	první	k4xOgInSc1	první
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejschopnějších	schopný	k2eAgMnPc2d3	nejschopnější
a	a	k8xC	a
nejvzdělanějších	vzdělaný	k2eAgMnPc2d3	nejvzdělanější
ruských	ruský	k2eAgMnPc2d1	ruský
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jeho	jeho	k3xOp3gFnSc1	jeho
paranoidní	paranoidní	k2eAgFnSc1d1	paranoidní
povaha	povaha	k1gFnSc1	povaha
a	a	k8xC	a
krutost	krutost	k1gFnSc1	krutost
vůči	vůči	k7c3	vůči
skutečným	skutečný	k2eAgInPc3d1	skutečný
i	i	k8xC	i
domnělým	domnělý	k2eAgMnPc3d1	domnělý
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
obklopil	obklopit	k5eAaPmAgMnS	obklopit
loajálními	loajální	k2eAgMnPc7d1	loajální
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
a	a	k8xC	a
významně	významně	k6eAd1	významně
zreformoval	zreformovat	k5eAaPmAgMnS	zreformovat
mnoho	mnoho	k4c4	mnoho
oblastí	oblast	k1gFnPc2	oblast
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
carem	car	k1gMnSc7	car
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pokusu	pokus	k1gInSc3	pokus
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
ruského	ruský	k2eAgInSc2d1	ruský
absolutismu	absolutismus	k1gInSc2	absolutismus
-	-	kIx~	-
samoděržaví	samoděržaví	k1gNnSc2	samoděržaví
<g/>
.	.	kIx.	.
</s>
<s>
Velikým	veliký	k2eAgMnSc7d1	veliký
knížetem	kníže	k1gMnSc7	kníže
moskevským	moskevský	k2eAgMnSc7d1	moskevský
se	se	k3xPyFc4	se
Ivan	Ivan	k1gMnSc1	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Vasiljevič	Vasiljevič	k1gInSc1	Vasiljevič
stal	stát	k5eAaPmAgInS	stát
formálně	formálně	k6eAd1	formálně
již	již	k6eAd1	již
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
svého	své	k1gNnSc2	své
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Vasilije	Vasilije	k1gMnSc2	Vasilije
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
stanovil	stanovit	k5eAaPmAgMnS	stanovit
regentskou	regentský	k2eAgFnSc4d1	regentská
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
vládla	vládnout	k5eAaImAgFnS	vládnout
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
správy	správa	k1gFnSc2	správa
země	zem	k1gFnSc2	zem
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
Ivanova	Ivanův	k2eAgFnSc1d1	Ivanova
matka	matka	k1gFnSc1	matka
Jelena	Jelena	k1gFnSc1	Jelena
Glinská	Glinská	k1gFnSc1	Glinská
a	a	k8xC	a
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
svého	svůj	k3xOyFgMnSc2	svůj
favorita	favorit	k1gMnSc2	favorit
knížete	kníže	k1gMnSc2	kníže
Ivana	Ivan	k1gMnSc2	Ivan
Fjodoroviče	Fjodorovič	k1gMnSc2	Fjodorovič
Ovčiny	ovčina	k1gFnSc2	ovčina
Tělěpněva-Obolenského	Tělěpněva-Obolenský	k2eAgMnSc4d1	Tělěpněva-Obolenský
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
sblížila	sblížit	k5eAaPmAgFnS	sblížit
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
ovdovění	ovdovění	k1gNnSc6	ovdovění
<g/>
.	.	kIx.	.
</s>
<s>
Svérázná	svérázný	k2eAgFnSc1d1	svérázná
povaha	povaha	k1gFnSc1	povaha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
malý	malý	k2eAgMnSc1d1	malý
Ivan	Ivan	k1gMnSc1	Ivan
dostal	dostat	k5eAaPmAgMnS	dostat
nepochybně	pochybně	k6eNd1	pochybně
do	do	k7c2	do
vínku	vínek	k1gInSc2	vínek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
utvářena	utvářen	k2eAgFnSc1d1	utvářena
či	či	k8xC	či
spíše	spíše	k9	spíše
deformována	deformovat	k5eAaImNgFnS	deformovat
nelítostnými	lítostný	k2eNgInPc7d1	nelítostný
poměry	poměr	k1gInPc7	poměr
moskevského	moskevský	k2eAgInSc2d1	moskevský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
musel	muset	k5eAaImAgInS	muset
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
žít	žít	k5eAaImF	žít
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
panovat	panovat	k5eAaImF	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
Ivanovo	Ivanův	k2eAgNnSc1d1	Ivanovo
dětství	dětství	k1gNnSc1	dětství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
svědkem	svědek	k1gMnSc7	svědek
mnoha	mnoho	k4c2	mnoho
ukrutností	ukrutnost	k1gFnPc2	ukrutnost
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc2d2	pozdější
zrady	zrada	k1gFnSc2	zrada
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
osob	osoba	k1gFnPc2	osoba
těžce	těžce	k6eAd1	těžce
dopadly	dopadnout	k5eAaPmAgInP	dopadnout
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
psychiku	psychika	k1gFnSc4	psychika
a	a	k8xC	a
projevily	projevit	k5eAaPmAgInP	projevit
se	s	k7c7	s
krajní	krajní	k2eAgFnSc7d1	krajní
podezřívavostí	podezřívavost	k1gFnSc7	podezřívavost
<g/>
,	,	kIx,	,
přehnaným	přehnaný	k2eAgInSc7d1	přehnaný
strachem	strach	k1gInSc7	strach
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
osobu	osoba	k1gFnSc4	osoba
a	a	k8xC	a
neúměrnou	úměrný	k2eNgFnSc4d1	neúměrná
krutostí	krutost	k1gFnSc7	krutost
vůči	vůči	k7c3	vůči
otevřeným	otevřený	k2eAgMnPc3d1	otevřený
nepřátelům	nepřítel	k1gMnPc3	nepřítel
(	(	kIx(	(
<g/>
bojaři	bojar	k1gMnPc1	bojar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
pouze	pouze	k6eAd1	pouze
padl	padnout	k5eAaImAgInS	padnout
stín	stín	k1gInSc1	stín
podezření	podezření	k1gNnSc2	podezření
ze	z	k7c2	z
zrady	zrada	k1gFnSc2	zrada
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
i	i	k9	i
proti	proti	k7c3	proti
carovu	carův	k2eAgNnSc3d1	carovo
nejbližšímu	blízký	k2eAgNnSc3d3	nejbližší
okolí	okolí	k1gNnSc3	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
regentky	regentka	k1gFnSc2	regentka
Jeleny	Jelena	k1gFnSc2	Jelena
Glinské	Glinské	k2eAgInSc2d1	Glinské
roku	rok	k1gInSc2	rok
1538	[number]	k4	1538
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
státě	stát	k1gInSc6	stát
výrazně	výrazně	k6eAd1	výrazně
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
už	už	k6eAd1	už
nestál	stát	k5eNaImAgMnS	stát
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
bral	brát	k5eAaImAgMnS	brát
ohledy	ohled	k1gInPc4	ohled
na	na	k7c4	na
státní	státní	k2eAgInPc4d1	státní
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Decentralizační	decentralizační	k2eAgFnPc1d1	decentralizační
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
regentka	regentka	k1gFnSc1	regentka
snažila	snažit	k5eAaImAgFnS	snažit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
zachování	zachování	k1gNnPc4	zachování
pevné	pevný	k2eAgFnSc2d1	pevná
ústřední	ústřední	k2eAgFnSc2d1	ústřední
moci	moc	k1gFnSc2	moc
držet	držet	k5eAaImF	držet
na	na	k7c6	na
uzdě	uzda	k1gFnSc6	uzda
<g/>
,	,	kIx,	,
dostaly	dostat	k5eAaPmAgFnP	dostat
nyní	nyní	k6eAd1	nyní
volnou	volný	k2eAgFnSc4d1	volná
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Bojarsko-knížecí	bojarskonížecí	k2eAgFnSc1d1	bojarsko-knížecí
aristokracie	aristokracie	k1gFnSc1	aristokracie
se	se	k3xPyFc4	se
usilovně	usilovně	k6eAd1	usilovně
snažila	snažit	k5eAaImAgFnS	snažit
nejen	nejen	k6eAd1	nejen
uchvátit	uchvátit	k5eAaPmF	uchvátit
ústřední	ústřední	k2eAgFnSc4d1	ústřední
moc	moc	k1gFnSc4	moc
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
změnit	změnit	k5eAaPmF	změnit
její	její	k3xOp3gFnSc4	její
podstatu	podstata	k1gFnSc4	podstata
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
nabývala	nabývat	k5eAaImAgFnS	nabývat
autokratických	autokratický	k2eAgInPc2d1	autokratický
rysů	rys	k1gInPc2	rys
<g/>
,	,	kIx,	,
a	a	k8xC	a
posunout	posunout	k5eAaPmF	posunout
ji	on	k3xPp3gFnSc4	on
nazpět	nazpět	k6eAd1	nazpět
k	k	k7c3	k
dualistickému	dualistický	k2eAgInSc3d1	dualistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázala	dokázat	k5eNaPmAgFnS	dokázat
skutečně	skutečně	k6eAd1	skutečně
vládnout	vládnout	k5eAaImF	vládnout
a	a	k8xC	a
nastolit	nastolit	k5eAaPmF	nastolit
pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
moc	moc	k1gFnSc4	moc
se	se	k3xPyFc4	se
svářily	svářit	k5eAaImAgInP	svářit
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
aristokratické	aristokratický	k2eAgInPc1d1	aristokratický
kliky	klik	k1gInPc1	klik
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
knížata	kníže	k1gMnPc1wR	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Šujských	Šujský	k1gMnPc2	Šujský
a	a	k8xC	a
Bělských	Bělský	k1gMnPc2	Bělský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
nabylo	nabýt	k5eAaPmAgNnS	nabýt
svrchu	svrchu	k6eAd1	svrchu
uskupení	uskupení	k1gNnSc1	uskupení
vedené	vedený	k2eAgNnSc1d1	vedené
Šujskými	Šujský	k1gMnPc7	Šujský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
doplatil	doplatit	k5eAaPmAgMnS	doplatit
kníže	kníže	k1gMnSc1	kníže
Tělepněv	Tělepněv	k1gMnSc1	Tělepněv
Ovčina	ovčina	k1gFnSc1	ovčina
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
si	se	k3xPyFc3	se
urychleně	urychleně	k6eAd1	urychleně
vyřídili	vyřídit	k5eAaPmAgMnP	vyřídit
své	svůj	k3xOyFgInPc4	svůj
účty	účet	k1gInPc4	účet
<g/>
,	,	kIx,	,
i	i	k8xC	i
metropolita	metropolita	k1gMnSc1	metropolita
Daniil	Daniil	k1gMnSc1	Daniil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podporoval	podporovat	k5eAaImAgInS	podporovat
Jelenu	Jelena	k1gFnSc4	Jelena
Glinskou	Glinský	k2eAgFnSc4d1	Glinský
i	i	k8xC	i
jejího	její	k3xOp3gMnSc2	její
favorita	favorit	k1gMnSc2	favorit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
také	také	k9	také
Glinští	Glinský	k1gMnPc1	Glinský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pouhých	pouhý	k2eAgNnPc2d1	pouhé
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1543	[number]	k4	1543
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
mladý	mladý	k2eAgMnSc1d1	mladý
velkokníže	velkokníže	k1gMnSc1	velkokníže
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
věrnými	věrný	k2eAgFnPc7d1	věrná
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
zavraždit	zavraždit	k5eAaPmF	zavraždit
Andreje	Andrej	k1gMnSc4	Andrej
Šujského	Šujský	k1gMnSc4	Šujský
a	a	k8xC	a
jeho	on	k3xPp3gMnSc4	on
příznivce	příznivec	k1gMnSc4	příznivec
odsoudit	odsoudit	k5eAaPmF	odsoudit
k	k	k7c3	k
vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Velkokníže	velkokníže	k1gMnSc1	velkokníže
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
cítil	cítit	k5eAaImAgMnS	cítit
už	už	k6eAd1	už
dostatečně	dostatečně	k6eAd1	dostatečně
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
navždy	navždy	k6eAd1	navždy
zbavit	zbavit	k5eAaPmF	zbavit
dohledu	dohled	k1gInSc3	dohled
bojarů	bojar	k1gMnPc2	bojar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mu	on	k3xPp3gMnSc3	on
mohlo	moct	k5eAaImAgNnS	moct
dopomoci	dopomoct	k5eAaPmF	dopomoct
jen	jen	k9	jen
dosažení	dosažení	k1gNnSc4	dosažení
plnoletosti	plnoletost	k1gFnSc2	plnoletost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zase	zase	k9	zase
mohla	moct	k5eAaImAgFnS	moct
uspíšit	uspíšit	k5eAaPmF	uspíšit
jen	jen	k9	jen
ženitba	ženitba	k1gFnSc1	ženitba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
sedmnáct	sedmnáct	k4xCc4	sedmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ivan	Ivan	k1gMnSc1	Ivan
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c2	za
cara	car	k1gMnSc2	car
a	a	k8xC	a
samovládce	samovládce	k1gMnSc2	samovládce
vší	veš	k1gFnPc2	veš
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ne	ne	k9	ne
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1547	[number]	k4	1547
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
car	car	k1gMnSc1	car
Ivan	Ivan	k1gMnSc1	Ivan
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Anastazií	Anastazie	k1gFnSc7	Anastazie
Zacharjinou	Zacharjina	k1gFnSc7	Zacharjina
Romanovnou-Koškinovou	Romanovnou-Koškinová	k1gFnSc7	Romanovnou-Koškinová
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
vlády	vláda	k1gFnSc2	vláda
Ivana	Ivan	k1gMnSc2	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
jeho	jeho	k3xOp3gMnPc1	jeho
první	první	k4xOgMnPc1	první
ženou	hnát	k5eAaImIp3nP	hnát
Anastazií	Anastazie	k1gFnSc7	Anastazie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
špatné	špatný	k2eAgFnPc4d1	špatná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mladého	mladý	k1gMnSc2	mladý
cara	car	k1gMnSc2	car
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
mnoho	mnoho	k4c4	mnoho
oblastí	oblast	k1gFnPc2	oblast
života	život	k1gInSc2	život
ruského	ruský	k2eAgInSc2d1	ruský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Reformní	reformní	k2eAgInPc1d1	reformní
kroky	krok	k1gInPc1	krok
Ivana	Ivan	k1gMnSc2	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
natolik	natolik	k6eAd1	natolik
radikální	radikální	k2eAgFnSc1d1	radikální
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
moskevském	moskevský	k2eAgInSc6d1	moskevský
státě	stát	k1gInSc6	stát
způsobily	způsobit	k5eAaPmAgFnP	způsobit
opravdu	opravdu	k6eAd1	opravdu
podstatné	podstatný	k2eAgFnPc1d1	podstatná
změny	změna	k1gFnPc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
tyto	tento	k3xDgInPc4	tento
skromné	skromný	k2eAgInPc4d1	skromný
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
u	u	k7c2	u
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
u	u	k7c2	u
vyšší	vysoký	k2eAgFnSc2d2	vyšší
šlechty	šlechta	k1gFnSc2	šlechta
(	(	kIx(	(
<g/>
bojarů	bojar	k1gMnPc2	bojar
<g/>
)	)	kIx)	)
velké	velký	k2eAgNnSc4d1	velké
znepokojení	znepokojení	k1gNnSc4	znepokojení
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
daně	daň	k1gFnPc4	daň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
zpočátku	zpočátku	k6eAd1	zpočátku
k	k	k7c3	k
hromadným	hromadný	k2eAgInPc3d1	hromadný
útěkům	útěk	k1gInPc3	útěk
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
vylidňování	vylidňování	k1gNnSc1	vylidňování
rozsáhlých	rozsáhlý	k2eAgNnPc2d1	rozsáhlé
území	území	k1gNnPc2	území
a	a	k8xC	a
k	k	k7c3	k
rebeliím	rebelie	k1gFnPc3	rebelie
<g/>
.	.	kIx.	.
</s>
<s>
Prudce	prudko	k6eAd1	prudko
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
zločinnost	zločinnost	k1gFnSc1	zločinnost
<g/>
,	,	kIx,	,
což	což	k9	což
roku	rok	k1gInSc2	rok
1555	[number]	k4	1555
donutilo	donutit	k5eAaPmAgNnS	donutit
cara	car	k1gMnSc2	car
k	k	k7c3	k
zpřísnění	zpřísnění	k1gNnSc3	zpřísnění
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
reorganizaci	reorganizace	k1gFnSc4	reorganizace
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1555	[number]	k4	1555
-	-	kIx~	-
1556	[number]	k4	1556
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
konečné	konečný	k2eAgNnSc1d1	konečné
rozdělení	rozdělení	k1gNnSc1	rozdělení
státu	stát	k1gInSc2	stát
na	na	k7c4	na
guby	guba	k1gFnPc4	guba
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc4	okres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přineslo	přinést	k5eAaPmAgNnS	přinést
i	i	k9	i
reorganizaci	reorganizace	k1gFnSc4	reorganizace
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nové	nový	k2eAgInPc1d1	nový
centrální	centrální	k2eAgInPc1d1	centrální
orgány	orgán	k1gInPc1	orgán
-	-	kIx~	-
prikazy	prikaz	k1gInPc1	prikaz
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
její	její	k3xOp3gInSc4	její
početní	početní	k2eAgInSc4d1	početní
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
reformy	reforma	k1gFnPc1	reforma
přinesly	přinést	k5eAaPmAgFnP	přinést
výsledky	výsledek	k1gInPc4	výsledek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dobytí	dobytí	k1gNnSc2	dobytí
dvou	dva	k4xCgInPc2	dva
tatarských	tatarský	k2eAgInPc2d1	tatarský
chanátů	chanát	k1gInPc2	chanát
za	za	k7c7	za
jihovýchodními	jihovýchodní	k2eAgFnPc7d1	jihovýchodní
hranicemi	hranice	k1gFnPc7	hranice
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
kazaňského	kazaňský	k2eAgInSc2d1	kazaňský
(	(	kIx(	(
<g/>
1552	[number]	k4	1552
<g/>
)	)	kIx)	)
a	a	k8xC	a
astrachaňského	astrachaňský	k2eAgInSc2d1	astrachaňský
(	(	kIx(	(
<g/>
1555	[number]	k4	1555
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
těchto	tento	k3xDgNnPc2	tento
vítězství	vítězství	k1gNnPc2	vítězství
nechal	nechat	k5eAaPmAgMnS	nechat
založit	založit	k5eAaPmF	založit
Ivan	Ivan	k1gMnSc1	Ivan
chrám	chrám	k1gInSc1	chrám
Vasila	Vasil	k1gMnSc2	Vasil
Blaženého	blažený	k2eAgMnSc2d1	blažený
<g/>
,	,	kIx,	,
nejpozoruhodnější	pozoruhodný	k2eAgNnSc1d3	nejpozoruhodnější
architektonické	architektonický	k2eAgNnSc1d1	architektonické
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
také	také	k6eAd1	také
znamenaly	znamenat	k5eAaImAgFnP	znamenat
dočasné	dočasný	k2eAgNnSc4d1	dočasné
smíření	smíření	k1gNnSc4	smíření
dvou	dva	k4xCgFnPc2	dva
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
cíle	cíl	k1gInPc4	cíl
a	a	k8xC	a
názory	názor	k1gInPc4	názor
<g/>
:	:	kIx,	:
bojarsko-knížecí	bojarskonížecí	k2eAgFnSc1d1	bojarsko-knížecí
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
nepřála	přát	k5eNaImAgFnS	přát
upevňování	upevňování	k1gNnSc4	upevňování
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
drobné	drobný	k2eAgFnPc1d1	drobná
poměstné	poměstný	k2eAgFnPc1d1	poměstný
šlechty	šlechta	k1gFnPc1	šlechta
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgInPc1d1	podporující
naopak	naopak	k6eAd1	naopak
panovníka	panovník	k1gMnSc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
však	však	k9	však
netrvala	trvat	k5eNaImAgFnS	trvat
věčně	věčně	k6eAd1	věčně
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
politiku	politika	k1gFnSc4	politika
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozešly	rozejít	k5eAaPmAgFnP	rozejít
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
získat	získat	k5eAaPmF	získat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
Baltu	Balt	k1gInSc3	Balt
<g/>
,	,	kIx,	,
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
tak	tak	k9	tak
Rusku	Ruska	k1gFnSc4	Ruska
intenzívnější	intenzívní	k2eAgInPc4d2	intenzívnější
obchodní	obchodní	k2eAgInPc4d1	obchodní
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Knížata	kníže	k1gMnPc1wR	kníže
a	a	k8xC	a
bojaři	bojar	k1gMnPc1	bojar
chtěli	chtít	k5eAaImAgMnP	chtít
dále	daleko	k6eAd2	daleko
válčit	válčit	k5eAaImF	válčit
s	s	k7c7	s
Tatary	Tatar	k1gMnPc7	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1553	[number]	k4	1553
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
Angličané	Angličan	k1gMnPc1	Angličan
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
nalézt	nalézt	k5eAaBmF	nalézt
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výprava	výprava	k1gFnSc1	výprava
skončila	skončit	k5eAaPmAgFnS	skončit
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
severní	severní	k2eAgFnSc2d1	severní
Rusi	Rus	k1gFnSc2	Rus
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
založen	založit	k5eAaPmNgInS	založit
Archangelsk	Archangelsk	k1gInSc1	Archangelsk
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
plán	plán	k1gInSc1	plán
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgInS	být
uskutečněn	uskutečněn	k2eAgMnSc1d1	uskutečněn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
základy	základ	k1gInPc1	základ
obchodního	obchodní	k2eAgNnSc2d1	obchodní
a	a	k8xC	a
diplomatického	diplomatický	k2eAgNnSc2d1	diplomatické
spojení	spojení	k1gNnSc2	spojení
Ruska	Rusko	k1gNnSc2	Rusko
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
anglickým	anglický	k2eAgMnSc7d1	anglický
kupcům	kupec	k1gMnPc3	kupec
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
značné	značný	k2eAgFnSc2d1	značná
výsady	výsada	k1gFnSc2	výsada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
slibně	slibně	k6eAd1	slibně
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
obchod	obchod	k1gInSc4	obchod
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Narvy	Narva	k1gFnSc2	Narva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
ruských	ruský	k2eAgInPc6d1	ruský
trzích	trh	k1gInPc6	trh
objevili	objevit	k5eAaPmAgMnP	objevit
také	také	k9	také
Holanďané	Holanďan	k1gMnPc1	Holanďan
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
velkými	velký	k2eAgMnPc7d1	velký
konkurenty	konkurent	k1gMnPc7	konkurent
Angličanů	Angličan	k1gMnPc2	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Ivanovy	Ivanův	k2eAgFnSc2d1	Ivanova
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
poprvé	poprvé	k6eAd1	poprvé
dostal	dostat	k5eAaPmAgInS	dostat
knihtisk	knihtisk	k1gInSc1	knihtisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Ivanova	Ivanův	k2eAgFnSc1d1	Ivanova
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
psychický	psychický	k2eAgInSc1d1	psychický
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1564	[number]	k4	1564
v	v	k7c6	v
záchvatu	záchvat	k1gInSc6	záchvat
paranoii	paranoia	k1gFnSc4	paranoia
před	před	k7c7	před
bojarským	bojarský	k2eAgNnSc7d1	bojarský
spiknutím	spiknutí	k1gNnSc7	spiknutí
nechal	nechat	k5eAaPmAgMnS	nechat
sbalit	sbalit	k5eAaPmF	sbalit
kremelský	kremelský	k2eAgInSc4d1	kremelský
poklad	poklad	k1gInSc4	poklad
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Alexandrovské	Alexandrovský	k2eAgFnSc2d1	Alexandrovská
slobody	sloboda	k1gFnSc2	sloboda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
zlomený	zlomený	k2eAgInSc1d1	zlomený
a	a	k8xC	a
nervovým	nervový	k2eAgInSc7d1	nervový
kolapsem	kolaps	k1gInSc7	kolaps
těžce	těžce	k6eAd1	těžce
poznamenaný	poznamenaný	k2eAgMnSc1d1	poznamenaný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
urozenou	urozený	k2eAgFnSc7d1	urozená
bojarsko-knížecí	bojarskonížecí	k2eAgFnSc7d1	bojarsko-knížecí
aristokracií	aristokracie	k1gFnSc7	aristokracie
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
roku	rok	k1gInSc2	rok
1565	[number]	k4	1565
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ivan	Ivan	k1gMnSc1	Ivan
zavedl	zavést	k5eAaPmAgMnS	zavést
opričninu	opričnina	k1gFnSc4	opričnina
<g/>
.	.	kIx.	.
</s>
<s>
Opričnina	opričnina	k1gFnSc1	opričnina
byla	být	k5eAaImAgFnS	být
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
správním	správní	k2eAgInSc7d1	správní
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
podstatou	podstata	k1gFnSc7	podstata
bylo	být	k5eAaImAgNnS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
zemštinu	zemština	k1gFnSc4	zemština
tradičně	tradičně	k6eAd1	tradičně
spravovanou	spravovaný	k2eAgFnSc7d1	spravovaná
zemskými	zemský	k2eAgInPc7d1	zemský
orgány	orgán	k1gInPc7	orgán
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
bojarskou	bojarský	k2eAgFnSc7d1	bojarská
dumou	duma	k1gFnSc7	duma
a	a	k8xC	a
opričninu	opričnina	k1gFnSc4	opričnina
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
přímém	přímý	k2eAgNnSc6d1	přímé
carově	carův	k2eAgNnSc6d1	carovo
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
opričniny	opričnina	k1gFnSc2	opričnina
polovina	polovina	k1gFnSc1	polovina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
až	až	k9	až
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
moskevského	moskevský	k2eAgNnSc2d1	moskevské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
především	především	k9	především
centrální	centrální	k2eAgFnSc6d1	centrální
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
kvalitnější	kvalitní	k2eAgFnSc7d2	kvalitnější
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opričnině	opričnina	k1gFnSc6	opričnina
zřídil	zřídit	k5eAaPmAgMnS	zřídit
car	car	k1gMnSc1	car
stejné	stejný	k2eAgInPc4d1	stejný
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
zemštině	zemština	k1gFnSc6	zemština
<g/>
,	,	kIx,	,
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
příkazy	příkaz	k1gInPc1	příkaz
<g/>
,	,	kIx,	,
dumu	duma	k1gFnSc4	duma
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zamýšlené	zamýšlený	k2eAgInPc1d1	zamýšlený
nové	nový	k2eAgInPc1d1	nový
typy	typ	k1gInPc1	typ
orgánů	orgán	k1gInPc2	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
nevznikly	vzniknout	k5eNaPmAgInP	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Opričnina	opričnina	k1gFnSc1	opričnina
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
proti	proti	k7c3	proti
mocenským	mocenský	k2eAgFnPc3d1	mocenská
a	a	k8xC	a
separatistickým	separatistický	k2eAgFnPc3d1	separatistická
snahám	snaha	k1gFnPc3	snaha
bojarsko-knížecí	bojarskonížecí	k2eAgFnSc2d1	bojarsko-knížecí
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
prosazení	prosazení	k1gNnSc1	prosazení
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
moci	moc	k1gFnSc2	moc
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
samoděržaví	samoděržaví	k1gNnSc1	samoděržaví
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
mocenským	mocenský	k2eAgInSc7d1	mocenský
nástrojem	nástroj	k1gInSc7	nástroj
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
opričné	opričný	k2eAgNnSc1d1	opričný
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
tisíc	tisíc	k4xCgInSc4	tisíc
mužů	muž	k1gMnPc2	muž
tvořících	tvořící	k2eAgMnPc2d1	tvořící
carovu	carův	k2eAgFnSc4d1	carova
osobní	osobní	k2eAgFnSc4d1	osobní
gardu	garda	k1gFnSc4	garda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
na	na	k7c4	na
šest	šest	k4xCc4	šest
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Rekrutovali	rekrutovat	k5eAaImAgMnP	rekrutovat
se	se	k3xPyFc4	se
především	především	k9	především
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
poměstné	poměstný	k2eAgFnSc2d1	poměstný
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
zde	zde	k6eAd1	zde
také	také	k9	také
příslušníci	příslušník	k1gMnPc1	příslušník
starobylého	starobylý	k2eAgNnSc2d1	starobylé
moskevského	moskevský	k2eAgNnSc2d1	moskevské
bojarstva	bojarstvo	k1gNnSc2	bojarstvo
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
oddaností	oddanost	k1gFnSc7	oddanost
carovi	carův	k2eAgMnPc1d1	carův
<g/>
,	,	kIx,	,
cizinci	cizinec	k1gMnPc1	cizinec
a	a	k8xC	a
dobrodruzi	dobrodruh	k1gMnPc1	dobrodruh
<g/>
.	.	kIx.	.
</s>
<s>
Opričníkům	opričník	k1gMnPc3	opričník
byla	být	k5eAaImAgNnP	být
přidělována	přidělován	k2eAgNnPc1d1	přidělováno
poměstí	poměstí	k1gNnPc1	poměstí
na	na	k7c4	na
území	území	k1gNnSc4	území
opričniny	opričnina	k1gFnSc2	opričnina
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byli	být	k5eAaImAgMnP	být
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
ze	z	k7c2	z
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vybírala	vybírat	k5eAaImAgFnS	vybírat
ze	z	k7c2	z
zemštiny	zemština	k1gFnSc2	zemština
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
odznakem	odznak	k1gInSc7	odznak
byla	být	k5eAaImAgFnS	být
psí	psí	k2eAgFnSc1d1	psí
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
koště	koště	k1gNnSc1	koště
<g/>
.	.	kIx.	.
</s>
<s>
Opričníci	opričník	k1gMnPc1	opričník
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
funkci	funkce	k1gFnSc4	funkce
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Pátrali	pátrat	k5eAaImAgMnP	pátrat
po	po	k7c6	po
nepřátelích	nepřítel	k1gMnPc6	nepřítel
<g/>
,	,	kIx,	,
vyslýchali	vyslýchat	k5eAaImAgMnP	vyslýchat
je	on	k3xPp3gFnPc4	on
a	a	k8xC	a
mučili	mučit	k5eAaImAgMnP	mučit
<g/>
.	.	kIx.	.
</s>
<s>
Stáli	stát	k5eAaImAgMnP	stát
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
nikdo	nikdo	k3yNnSc1	nikdo
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nesměl	smět	k5eNaImAgMnS	smět
stěžovat	stěžovat	k5eAaImF	stěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
byly	být	k5eAaImAgFnP	být
konfiskace	konfiskace	k1gFnPc1	konfiskace
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
masové	masový	k2eAgFnSc2d1	masová
popravy	poprava	k1gFnSc2	poprava
Ivanových	Ivanových	k2eAgMnPc2d1	Ivanových
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
skutečných	skutečný	k2eAgMnPc2d1	skutečný
i	i	k8xC	i
domnělých	domnělý	k2eAgMnPc2d1	domnělý
<g/>
,	,	kIx,	,
konaly	konat	k5eAaImAgInP	konat
se	se	k3xPyFc4	se
trestné	trestný	k2eAgFnSc2d1	trestná
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
městům	město	k1gNnPc3	město
či	či	k8xC	či
celým	celý	k2eAgInPc3d1	celý
újezdům	újezd	k1gInPc3	újezd
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
výprav	výprava	k1gFnPc2	výprava
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc4d1	odhaleno
údajné	údajný	k2eAgNnSc4d1	údajné
spiknutí	spiknutí	k1gNnSc4	spiknutí
v	v	k7c6	v
Novgorodu	Novgorod	k1gInSc6	Novgorod
<g/>
.	.	kIx.	.
</s>
<s>
Účtování	účtování	k1gNnSc1	účtování
s	s	k7c7	s
údajnými	údajný	k2eAgMnPc7d1	údajný
zrádci	zrádce	k1gMnPc7	zrádce
trvalo	trvat	k5eAaImAgNnS	trvat
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
města	město	k1gNnSc2	město
byly	být	k5eAaImAgInP	být
tisíce	tisíc	k4xCgInPc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
mučeny	mučen	k2eAgInPc1d1	mučen
a	a	k8xC	a
topeny	topen	k2eAgInPc4d1	topen
v	v	k7c6	v
mrznoucím	mrznoucí	k2eAgInSc6d1	mrznoucí
Volchovu	Volchův	k2eAgFnSc4d1	Volchův
i	i	k9	i
s	s	k7c7	s
celými	celý	k2eAgFnPc7d1	celá
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
život	život	k1gInSc4	život
tehdy	tehdy	k6eAd1	tehdy
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
4000	[number]	k4	4000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Represálie	represálie	k1gFnPc1	represálie
byly	být	k5eAaImAgFnP	být
namířeny	namířen	k2eAgFnPc1d1	namířena
nejen	nejen	k6eAd1	nejen
proti	proti	k7c3	proti
aristokracii	aristokracie	k1gFnSc3	aristokracie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
proti	proti	k7c3	proti
jejím	její	k3xOp3gMnPc3	její
poddaným	poddaný	k1gMnPc3	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Opričnina	opričnina	k1gFnSc1	opričnina
měla	mít	k5eAaImAgFnS	mít
jen	jen	k9	jen
dočasný	dočasný	k2eAgInSc4d1	dočasný
efekt	efekt	k1gInSc4	efekt
a	a	k8xC	a
očekávaný	očekávaný	k2eAgInSc1d1	očekávaný
výsledek	výsledek	k1gInSc1	výsledek
Ivanu	Ivana	k1gFnSc4	Ivana
Hroznému	hrozný	k2eAgMnSc3d1	hrozný
nepřinesla	přinést	k5eNaPmAgNnP	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
síly	síla	k1gFnPc1	síla
odporu	odpor	k1gInSc2	odpor
bojarstva	bojarstvo	k1gNnSc2	bojarstvo
byly	být	k5eAaImAgFnP	být
sice	sice	k8xC	sice
zlomeny	zlomen	k2eAgFnPc1d1	zlomena
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
rodin	rodina	k1gFnPc2	rodina
vyhlazeno	vyhladit	k5eAaPmNgNnS	vyhladit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nastolit	nastolit	k5eAaPmF	nastolit
samoděržaví	samoděržaví	k1gNnSc4	samoděržaví
se	se	k3xPyFc4	se
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
spěla	spět	k5eAaImAgFnS	spět
Moskva	Moskva	k1gFnSc1	Moskva
k	k	k7c3	k
neúspěchům	neúspěch	k1gInPc3	neúspěch
v	v	k7c6	v
livonské	livonský	k2eAgFnSc6d1	livonská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
-	-	kIx~	-
<g/>
1582	[number]	k4	1582
<g/>
/	/	kIx~	/
<g/>
1583	[number]	k4	1583
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
Ivan	Ivan	k1gMnSc1	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
válčil	válčit	k5eAaImAgMnS	válčit
s	s	k7c7	s
Livonskem	Livonsko	k1gNnSc7	Livonsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
napadl	napadnout	k5eAaPmAgMnS	napadnout
roku	rok	k1gInSc2	rok
1558	[number]	k4	1558
<g/>
,	,	kIx,	,
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
Narvy	Narva	k1gFnSc2	Narva
pouze	pouze	k6eAd1	pouze
dočasně	dočasně	k6eAd1	dočasně
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1558	[number]	k4	1558
<g/>
-	-	kIx~	-
<g/>
1581	[number]	k4	1581
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpávající	vyčerpávající	k2eAgInSc1d1	vyčerpávající
vojenský	vojenský	k2eAgInSc1d1	vojenský
konflikt	konflikt	k1gInSc1	konflikt
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
opričninou	opričnina	k1gFnSc7	opričnina
podlomily	podlomit	k5eAaPmAgFnP	podlomit
síly	síla	k1gFnPc4	síla
moskevského	moskevský	k2eAgInSc2d1	moskevský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
přivedly	přivést	k5eAaPmAgFnP	přivést
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
hospodářskému	hospodářský	k2eAgInSc3d1	hospodářský
úpadku	úpadek	k1gInSc3	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnPc1d1	centrální
oblasti	oblast	k1gFnPc1	oblast
kolem	kolem	k7c2	kolem
Moskvy	Moskva	k1gFnSc2	Moskva
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vylidňovat	vylidňovat	k5eAaImF	vylidňovat
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
prchali	prchat	k5eAaImAgMnP	prchat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
na	na	k7c4	na
Litvu	Litva	k1gFnSc4	Litva
<g/>
,	,	kIx,	,
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
i	i	k9	i
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
opričné	opričný	k2eAgNnSc1d1	opričný
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
zvyklé	zvyklý	k2eAgFnPc1d1	zvyklá
terorizovat	terorizovat	k5eAaImF	terorizovat
domácí	domácí	k2eAgNnSc4d1	domácí
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
pro	pro	k7c4	pro
regulérně	regulérně	k6eAd1	regulérně
vedenou	vedený	k2eAgFnSc4d1	vedená
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Neosvědčilo	osvědčit	k5eNaPmAgNnS	osvědčit
se	se	k3xPyFc4	se
na	na	k7c6	na
livonském	livonský	k2eAgNnSc6d1	livonské
bojišti	bojiště	k1gNnSc6	bojiště
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Tatary	Tatar	k1gMnPc7	Tatar
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
roku	rok	k1gInSc2	rok
1571	[number]	k4	1571
napadli	napadnout	k5eAaPmAgMnP	napadnout
a	a	k8xC	a
vypálili	vypálit	k5eAaPmAgMnP	vypálit
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mezi	mezi	k7c7	mezi
novými	nový	k2eAgMnPc7d1	nový
vlastníky	vlastník	k1gMnPc7	vlastník
půdy	půda	k1gFnSc2	půda
brzy	brzy	k6eAd1	brzy
nová	nový	k2eAgFnSc1d1	nová
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
byla	být	k5eAaImAgFnS	být
opričnina	opričnina	k1gFnSc1	opričnina
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
nesmělo	smět	k5eNaImAgNnS	smět
ani	ani	k9	ani
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
opričných	opričný	k2eAgMnPc2d1	opričný
vojsk	vojsko	k1gNnPc2	vojsko
kníže	kníže	k1gMnSc1	kníže
Čerkasskij	Čerkasskij	k1gMnSc1	Čerkasskij
byl	být	k5eAaImAgMnS	být
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc2d1	další
přední	přední	k2eAgFnSc2d1	přední
osobnosti	osobnost	k1gFnSc2	osobnost
opričniny	opričnina	k1gFnSc2	opričnina
stihly	stihnout	k5eAaPmAgInP	stihnout
těžké	těžký	k2eAgInPc1d1	těžký
tresty	trest	k1gInPc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Opričnina	opričnina	k1gFnSc1	opričnina
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
fázi	fáze	k1gFnSc6	fáze
obrátila	obrátit	k5eAaPmAgFnS	obrátit
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
tvůrcům	tvůrce	k1gMnPc3	tvůrce
<g/>
,	,	kIx,	,
málokdo	málokdo	k3yInSc1	málokdo
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
opričníků	opričník	k1gMnPc2	opričník
ji	on	k3xPp3gFnSc4	on
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
půdy	půda	k1gFnSc2	půda
zabrané	zabraný	k2eAgFnSc2d1	zabraná
pro	pro	k7c4	pro
opričninu	opričnina	k1gFnSc4	opričnina
byla	být	k5eAaImAgFnS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
původním	původní	k2eAgMnPc3d1	původní
vlastníkům	vlastník	k1gMnPc3	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
třikrát	třikrát	k6eAd1	třikrát
vrátil	vrátit	k5eAaPmAgInS	vrátit
k	k	k7c3	k
opričným	opričný	k2eAgInPc3d1	opričný
pořádkům	pořádek	k1gInPc3	pořádek
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
modifikované	modifikovaný	k2eAgFnSc6d1	modifikovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
obnovit	obnovit	k5eAaPmF	obnovit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
carův	carův	k2eAgInSc4d1	carův
úděl	úděl	k1gInSc4	úděl
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vlády	vláda	k1gFnSc2	vláda
pokřtěného	pokřtěný	k2eAgMnSc2d1	pokřtěný
tatarského	tatarský	k2eAgMnSc2d1	tatarský
prince	princ	k1gMnSc2	princ
Simeona	Simeon	k1gMnSc2	Simeon
Bekbulatoviče	Bekbulatovič	k1gMnSc2	Bekbulatovič
a	a	k8xC	a
jako	jako	k9	jako
carův	carův	k2eAgInSc1d1	carův
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
vlnami	vlna	k1gFnPc7	vlna
teroru	teror	k1gInSc2	teror
a	a	k8xC	a
represálií	represálie	k1gFnPc2	represálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1581	[number]	k4	1581
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejtěžší	těžký	k2eAgFnSc1d3	nejtěžší
rodinná	rodinný	k2eAgFnSc1d1	rodinná
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
Ivan	Ivan	k1gMnSc1	Ivan
surově	surově	k6eAd1	surově
zbil	zbít	k5eAaPmAgMnS	zbít
svoji	svůj	k3xOyFgFnSc4	svůj
snachu	snacha	k1gFnSc4	snacha
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
stupni	stupeň	k1gInSc6	stupeň
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
prý	prý	k9	prý
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
v	v	k7c6	v
carském	carský	k2eAgInSc6d1	carský
paláci	palác	k1gInSc6	palác
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
vlastních	vlastní	k2eAgFnPc6d1	vlastní
komnatách	komnata	k1gFnPc6	komnata
<g/>
)	)	kIx)	)
nedostatečně	dostatečně	k6eNd1	dostatečně
oděná	oděný	k2eAgFnSc1d1	oděná
<g/>
;	;	kIx,	;
žena	žena	k1gFnSc1	žena
následně	následně	k6eAd1	následně
potratila	potratit	k5eAaPmAgFnS	potratit
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Ivana	Ivan	k1gMnSc4	Ivan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
manželky	manželka	k1gFnSc2	manželka
zastal	zastat	k5eAaPmAgMnS	zastat
<g/>
,	,	kIx,	,
car	car	k1gMnSc1	car
smrtelně	smrtelně	k6eAd1	smrtelně
zranil	zranit	k5eAaPmAgMnS	zranit
železnou	železný	k2eAgFnSc7d1	železná
holí	hole	k1gFnSc7	hole
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
a	a	k8xC	a
pronásledován	pronásledován	k2eAgInSc1d1	pronásledován
výčitkami	výčitka	k1gFnPc7	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
,	,	kIx,	,
chodil	chodit	k5eAaImAgMnS	chodit
po	po	k7c6	po
Kremlu	Kreml	k1gInSc6	Kreml
a	a	k8xC	a
vyl	výt	k5eAaImAgMnS	výt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
v	v	k7c6	v
moskevském	moskevský	k2eAgInSc6d1	moskevský
státě	stát	k1gInSc6	stát
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
hluboká	hluboký	k2eAgFnSc1d1	hluboká
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1584	[number]	k4	1584
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
)	)	kIx)	)
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Fjodor	Fjodor	k1gMnSc1	Fjodor
I.	I.	kA	I.
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
postrádal	postrádat	k5eAaImAgInS	postrádat
vladařské	vladařský	k2eAgFnPc4d1	vladařská
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nebyl	být	k5eNaImAgInS	být
duševně	duševně	k6eAd1	duševně
méněcenný	méněcenný	k2eAgInSc1d1	méněcenný
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Faktické	faktický	k2eAgFnSc2d1	faktická
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
počátkem	počátek	k1gInSc7	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Boris	Boris	k1gMnSc1	Boris
Godunov	Godunov	k1gInSc1	Godunov
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Irina	Irien	k2eAgFnSc1d1	Irina
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
careviče	carevič	k1gMnSc4	carevič
Fjodora	Fjodor	k1gMnSc4	Fjodor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
žil	žít	k5eAaImAgMnS	žít
další	další	k2eAgMnSc1d1	další
Ivanův	Ivanův	k2eAgMnSc1d1	Ivanův
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
carevič	carevič	k1gMnSc1	carevič
Dimitrij	Dimitrij	k1gMnSc1	Dimitrij
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1582	[number]	k4	1582
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ivana	Ivan	k1gMnSc2	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgMnS	stát
carem	car	k1gMnSc7	car
jeho	jeho	k3xOp3gMnSc7	jeho
syn	syn	k1gMnSc1	syn
Fjodor	Fjodor	k1gMnSc1	Fjodor
I.	I.	kA	I.
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Dimitrij	Dimitrij	k1gMnSc1	Dimitrij
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Ugliče	Uglič	k1gInSc2	Uglič
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1591	[number]	k4	1591
zahynul	zahynout	k5eAaPmAgMnS	zahynout
za	za	k7c2	za
dosud	dosud	k6eAd1	dosud
nevyjasněných	vyjasněný	k2eNgFnPc2d1	nevyjasněná
okolností	okolnost	k1gFnPc2	okolnost
ranou	rána	k1gFnSc7	rána
nožem	nůž	k1gInSc7	nůž
do	do	k7c2	do
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vraždu	vražda	k1gFnSc4	vražda
objednal	objednat	k5eAaPmAgMnS	objednat
Boris	Boris	k1gMnSc1	Boris
Godunov	Godunov	k1gInSc4	Godunov
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nastal	nastat	k5eAaPmAgInS	nastat
dočasně	dočasně	k6eAd1	dočasně
klid	klid	k1gInSc1	klid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
Fjodorově	Fjodorův	k2eAgFnSc6d1	Fjodorova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1598	[number]	k4	1598
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Godunov	Godunov	k1gInSc1	Godunov
stal	stát	k5eAaPmAgInS	stát
carem	car	k1gMnSc7	car
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nashromážděné	nashromážděný	k2eAgInPc1d1	nashromážděný
problémy	problém	k1gInPc1	problém
projevily	projevit	k5eAaPmAgInP	projevit
novou	nový	k2eAgFnSc4d1	nová
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
řada	řada	k1gFnSc1	řada
vzpour	vzpoura	k1gFnPc2	vzpoura
a	a	k8xC	a
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
navíc	navíc	k6eAd1	navíc
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
intervence	intervence	k1gFnSc1	intervence
(	(	kIx(	(
<g/>
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
historiografii	historiografie	k1gFnSc6	historiografie
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
smuta	smuta	k1gFnSc1	smuta
-	-	kIx~	-
období	období	k1gNnSc1	období
zmatků	zmatek	k1gInPc2	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
povahu	povaha	k1gFnSc4	povaha
a	a	k8xC	a
psychické	psychický	k2eAgInPc4d1	psychický
problémy	problém	k1gInPc4	problém
byl	být	k5eAaImAgMnS	být
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nadaným	nadaný	k2eAgMnSc7d1	nadaný
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dobře	dobře	k6eAd1	dobře
chápal	chápat	k5eAaImAgMnS	chápat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Rusko	Rusko	k1gNnSc1	Rusko
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ku	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
prospěchu	prospěch	k1gInSc3	prospěch
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc3	omezení
moci	moc	k1gFnSc2	moc
vysoké	vysoký	k2eAgFnSc2d1	vysoká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
monarchiích	monarchie	k1gFnPc6	monarchie
<g/>
,	,	kIx,	,
styky	styk	k1gInPc4	styk
s	s	k7c7	s
hospodářsky	hospodářsky	k6eAd1	hospodářsky
vyspělejšími	vyspělý	k2eAgFnPc7d2	vyspělejší
evropskými	evropský	k2eAgFnPc7d1	Evropská
zeměmi	zem	k1gFnPc7	zem
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Svoje	své	k1gNnSc1	své
záměry	záměra	k1gFnSc2	záměra
však	však	k9	však
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
velmi	velmi	k6eAd1	velmi
neadekvátními	adekvátní	k2eNgFnPc7d1	neadekvátní
krutými	krutý	k2eAgFnPc7d1	krutá
metodami	metoda	k1gFnPc7	metoda
a	a	k8xC	a
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
výsledku	výsledek	k1gInSc6	výsledek
citelně	citelně	k6eAd1	citelně
poškodil	poškodit	k5eAaPmAgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
ruský	ruský	k2eAgInSc1d1	ruský
stát	stát	k1gInSc1	stát
propadl	propadnout	k5eAaPmAgInS	propadnout
do	do	k7c2	do
hluboké	hluboký	k2eAgFnSc2d1	hluboká
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
Ivan	Ivan	k1gMnSc1	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
cítil	cítit	k5eAaImAgInS	cítit
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
i	i	k8xC	i
domnělou	domnělý	k2eAgFnSc7d1	domnělá
opozicí	opozice	k1gFnSc7	opozice
a	a	k8xC	a
na	na	k7c6	na
klidu	klid	k1gInSc6	klid
mu	on	k3xPp3gNnSc3	on
nepřidávala	přidávat	k5eNaImAgFnS	přidávat
ani	ani	k9	ani
situace	situace	k1gFnSc1	situace
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
s	s	k7c7	s
bojarsko-knížecí	bojarskonížecí	k2eAgFnSc7d1	bojarsko-knížecí
aristokracií	aristokracie	k1gFnSc7	aristokracie
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
krutě	krutě	k6eAd1	krutě
a	a	k8xC	a
nekompromisně	kompromisně	k6eNd1	kompromisně
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
jednoznačných	jednoznačný	k2eAgInPc2d1	jednoznačný
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusko	Rusko	k1gNnSc1	Rusko
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
zcela	zcela	k6eAd1	zcela
izolované	izolovaný	k2eAgInPc1d1	izolovaný
a	a	k8xC	a
bez	bez	k7c2	bez
vnějších	vnější	k2eAgMnPc2d1	vnější
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
Ivana	Ivan	k1gMnSc2	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
neustále	neustále	k6eAd1	neustále
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
polsko-litevskou	polskoitevský	k2eAgFnSc7d1	polsko-litevská
unií	unie	k1gFnSc7	unie
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
zbylými	zbylý	k2eAgInPc7d1	zbylý
krymskými	krymský	k2eAgInPc7d1	krymský
Tatary	tatar	k1gInPc7	tatar
<g/>
,	,	kIx,	,
vazaly	vazal	k1gMnPc4	vazal
mocné	mocný	k2eAgFnSc2d1	mocná
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
prohrál	prohrát	k5eAaPmAgInS	prohrát
boj	boj	k1gInSc1	boj
o	o	k7c4	o
Livonsko	Livonsko	k1gNnSc4	Livonsko
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sám	sám	k3xTgMnSc1	sám
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
a	a	k8xC	a
pak	pak	k6eAd1	pak
o	o	k7c4	o
ně	on	k3xPp3gNnSc4	on
vedl	vést	k5eAaImAgInS	vést
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
polsko-litevskou	polskoitevský	k2eAgFnSc7d1	polsko-litevská
unií	unie	k1gFnSc7	unie
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
dobyl	dobýt	k5eAaPmAgInS	dobýt
kazaňský	kazaňský	k2eAgInSc4d1	kazaňský
a	a	k8xC	a
astrachaňský	astrachaňský	k2eAgInSc4d1	astrachaňský
chanát	chanát	k1gInSc4	chanát
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vojáci	voják	k1gMnPc1	voják
také	také	k9	také
překročili	překročit	k5eAaPmAgMnP	překročit
Ural	Ural	k1gInSc4	Ural
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
obsazováním	obsazování	k1gNnSc7	obsazování
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1579	[number]	k4	1579
-	-	kIx~	-
1582	[number]	k4	1582
se	se	k3xPyFc4	se
Ivan	Ivan	k1gMnSc1	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
snažil	snažit	k5eAaImAgInS	snažit
navázat	navázat	k5eAaPmF	navázat
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Rudolfem	Rudolf	k1gMnSc7	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Řehořem	Řehoř	k1gMnSc7	Řehoř
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
protiturecké	protiturecký	k2eAgFnSc2d1	protiturecká
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgNnPc1	tento
jednání	jednání	k1gNnPc1	jednání
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgNnP	skončit
nezdarem	nezdar	k1gInSc7	nezdar
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
nadále	nadále	k6eAd1	nadále
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
izolaci	izolace	k1gFnSc6	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
cara	car	k1gMnSc2	car
Ivana	Ivan	k1gMnSc2	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
opričniny	opričnina	k1gFnSc2	opričnina
a	a	k8xC	a
neustálých	neustálý	k2eAgFnPc2d1	neustálá
válek	válka	k1gFnPc2	válka
značně	značně	k6eAd1	značně
snížila	snížit	k5eAaPmAgFnS	snížit
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
především	především	k9	především
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
navíc	navíc	k6eAd1	navíc
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1581	[number]	k4	1581
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
letech	let	k1gInPc6	let
stěhovat	stěhovat	k5eAaImF	stěhovat
a	a	k8xC	a
opouštět	opouštět	k5eAaImF	opouštět
svoji	svůj	k3xOyFgFnSc4	svůj
vrchnost	vrchnost	k1gFnSc4	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nadále	nadále	k6eAd1	nadále
negramotné	gramotný	k2eNgNnSc1d1	negramotné
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
a	a	k8xC	a
neexistovala	existovat	k5eNaImAgFnS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
zůstal	zůstat	k5eAaPmAgInS	zůstat
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
a	a	k8xC	a
bez	bez	k7c2	bez
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
zodpovídal	zodpovídat	k5eAaPmAgMnS	zodpovídat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
činů	čin	k1gInPc2	čin
jen	jen	k9	jen
jediné	jediné	k1gNnSc4	jediné
instanci	instance	k1gFnSc4	instance
<g/>
,	,	kIx,	,
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
byl	být	k5eAaImAgMnS	být
Poslední	poslední	k2eAgInSc4d1	poslední
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
pouze	pouze	k6eAd1	pouze
před	před	k7c7	před
Bohem	bůh	k1gMnSc7	bůh
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ivan	Ivan	k1gMnSc1	Ivan
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
mohl	moct	k5eAaImAgInS	moct
pokusit	pokusit	k5eAaPmF	pokusit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
proměnit	proměnit	k5eAaPmF	proměnit
tak	tak	k6eAd1	tak
Moskevskou	moskevský	k2eAgFnSc4d1	Moskevská
Rus	Rus	k1gFnSc4	Rus
v	v	k7c4	v
samoděržavnou	samoděržavný	k2eAgFnSc4d1	samoděržavný
despocii	despocie	k1gFnSc4	despocie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidových	lidový	k2eAgFnPc6d1	lidová
vrstvách	vrstva	k1gFnPc6	vrstva
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
všemu	všecek	k3xTgMnSc3	všecek
udržoval	udržovat	k5eAaImAgInS	udržovat
mýtus	mýtus	k1gInSc1	mýtus
dobrého	dobrý	k2eAgMnSc2d1	dobrý
cara	car	k1gMnSc2	car
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ruské	ruský	k2eAgNnSc4d1	ruské
prostředí	prostředí	k1gNnSc4	prostředí
zvláště	zvláště	k6eAd1	zvláště
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
<g/>
.	.	kIx.	.
</s>
<s>
Ivanův	Ivanův	k2eAgInSc1d1	Ivanův
nástup	nástup	k1gInSc1	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
byl	být	k5eAaImAgInS	být
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
nadějemi	naděje	k1gFnPc7	naděje
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
svévole	svévole	k1gFnSc2	svévole
bojarsko-knížecí	bojarskonížecí	k2eAgFnSc2d1	bojarsko-knížecí
aristokracie	aristokracie	k1gFnSc2	aristokracie
a	a	k8xC	a
pronásledování	pronásledování	k1gNnSc2	pronásledování
nenáviděných	nenáviděný	k2eAgMnPc2d1	nenáviděný
bojarů	bojar	k1gMnPc2	bojar
bylo	být	k5eAaImAgNnS	být
vítáno	vítat	k5eAaImNgNnS	vítat
<g/>
.	.	kIx.	.
</s>
<s>
Represálie	represálie	k1gFnPc1	represálie
ovšem	ovšem	k9	ovšem
později	pozdě	k6eAd2	pozdě
postihly	postihnout	k5eAaPmAgFnP	postihnout
také	také	k9	také
jejich	jejich	k3xOp3gFnPc1	jejich
poddané	poddaná	k1gFnPc1	poddaná
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
však	však	k9	však
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
nebyl	být	k5eNaImAgInS	být
"	"	kIx"	"
<g/>
hrozný	hrozný	k2eAgInSc1d1	hrozný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
ruského	ruský	k2eAgInSc2d1	ruský
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
groznyj	groznyj	k1gInSc1	groznyj
<g/>
"	"	kIx"	"
sice	sice	k8xC	sice
znamená	znamenat	k5eAaImIp3nS	znamenat
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
,	,	kIx,	,
přísný	přísný	k2eAgInSc1d1	přísný
nebo	nebo	k8xC	nebo
strašný	strašný	k2eAgInSc1d1	strašný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
majestátní	majestátní	k2eAgInSc4d1	majestátní
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
hrozivý	hrozivý	k2eAgInSc4d1	hrozivý
či	či	k8xC	či
vzbuzující	vzbuzující	k2eAgFnSc4d1	vzbuzující
úctu	úcta	k1gFnSc4	úcta
,	,	kIx,	,
což	což	k3yQnSc4	což
český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
hrozný	hrozný	k2eAgInSc1d1	hrozný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
německý	německý	k2eAgMnSc1d1	německý
"	"	kIx"	"
<g/>
der	drát	k5eAaImRp2nS	drát
Schreckliche	Schreckliche	k1gInSc1	Schreckliche
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
anglický	anglický	k2eAgInSc1d1	anglický
"	"	kIx"	"
<g/>
the	the	k?	the
Terrible	Terrible	k1gFnSc1	Terrible
<g/>
"	"	kIx"	"
nevystihují	vystihovat	k5eNaImIp3nP	vystihovat
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
a	a	k8xC	a
výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
chápán	chápat	k5eAaImNgInS	chápat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
tyran	tyran	k1gMnSc1	tyran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Rusku	Rusko	k1gNnSc6	Rusko
existují	existovat	k5eAaImIp3nP	existovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
napadají	napadat	k5eAaPmIp3nP	napadat
použití	použití	k1gNnSc4	použití
přídomku	přídomek	k1gInSc2	přídomek
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
pro	pro	k7c4	pro
Ivana	Ivan	k1gMnSc4	Ivan
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
svatořečení	svatořečení	k1gNnSc4	svatořečení
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
oficiální	oficiální	k2eAgNnSc4d1	oficiální
stanovisko	stanovisko	k1gNnSc4	stanovisko
ruské	ruský	k2eAgFnSc2d1	ruská
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
zastává	zastávat	k5eAaImIp3nS	zastávat
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
negativní	negativní	k2eAgInSc1d1	negativní
postoj	postoj	k1gInSc1	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Payne	Payn	k1gInSc5	Payn
<g/>
,	,	kIx,	,
Nikita	Nikita	k1gMnSc1	Nikita
Romanoff	Romanoff	k1gMnSc1	Romanoff
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
<g/>
,	,	kIx,	,
Beta-Dobrovský	Beta-Dobrovský	k2eAgMnSc1d1	Beta-Dobrovský
-	-	kIx~	-
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Plzeň	Plzeň	k1gFnSc1	Plzeň
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7306-337-5	[number]	k4	978-80-7306-337-5
a	a	k8xC	a
978-80-7291-183-7	[number]	k4	978-80-7291-183-7
Władysław	Władysław	k1gMnSc1	Władysław
Andrzej	Andrzej	k1gMnSc1	Andrzej
Serczyk	Serczyk	k1gMnSc1	Serczyk
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
-	-	kIx~	-
car	car	k1gMnSc1	car
vší	všecek	k3xTgFnSc2	všecek
Rusi	Rus	k1gFnSc2	Rus
a	a	k8xC	a
stvořitel	stvořitel	k1gMnSc1	stvořitel
samoděržaví	samoděržaví	k1gNnSc1	samoděržaví
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7106-577-3	[number]	k4	80-7106-577-3
Dana	Dana	k1gFnSc1	Dana
Picková	Picková	k1gFnSc1	Picková
<g/>
:	:	kIx,	:
Anglo-ruské	Anglouský	k2eAgInPc1d1	Anglo-ruský
vztahy	vztah	k1gInPc1	vztah
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7066-209-3	[number]	k4	80-7066-209-3
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Ruslan	Ruslan	k1gInSc1	Ruslan
Georgijevič	Georgijevič	k1gMnSc1	Georgijevič
Skrynnikov	Skrynnikov	k1gInSc1	Skrynnikov
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Groznyj	Groznyj	k1gMnSc1	Groznyj
<g/>
,	,	kIx,	,
AST	AST	kA	AST
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
5-17-004358-9	[number]	k4	5-17-004358-9
Seznam	seznam	k1gInSc4	seznam
hlav	hlava	k1gFnPc2	hlava
ruského	ruský	k2eAgInSc2d1	ruský
státu	stát	k1gInSc2	stát
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
Jurodiví	jurodivý	k2eAgMnPc1d1	jurodivý
Galerie	galerie	k1gFnSc1	galerie
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ivan	Ivana	k1gFnPc2	Ivana
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
