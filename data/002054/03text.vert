<s>
Dallas	Dallas	k1gInSc4	Dallas
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Texas	Texas	k1gInSc1	Texas
(	(	kIx(	(
<g/>
po	po	k7c6	po
Houstonu	Houston	k1gInSc6	Houston
a	a	k8xC	a
San	San	k1gMnSc6	San
Antoniu	Antonio	k1gMnSc6	Antonio
<g/>
)	)	kIx)	)
a	a	k8xC	a
deváté	devátý	k4xOgNnSc4	devátý
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
i	i	k9	i
územně	územně	k6eAd1	územně
<g/>
,	,	kIx,	,
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c4	na
997	[number]	k4	997
km2	km2	k4	km2
(	(	kIx(	(
<g/>
385	[number]	k4	385
mi2	mi2	k4	mi2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dallas	Dallas	k1gInSc1	Dallas
County	Counta	k1gFnSc2	Counta
je	být	k5eAaImIp3nS	být
devátý	devátý	k4xOgMnSc1	devátý
největší	veliký	k2eAgInSc1d3	veliký
okres	okres	k1gInSc1	okres
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dallas	Dallas	k1gInSc1	Dallas
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
11	[number]	k4	11
měst	město	k1gNnPc2	město
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zařazena	zařazen	k2eAgNnPc1d1	zařazeno
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
Globální	globální	k2eAgMnPc1d1	globální
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
"	"	kIx"	"
<g/>
Gamma	Gamma	k1gNnSc1	Gamma
World	Worlda	k1gFnPc2	Worlda
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
Georgi	Georgi	k1gNnSc6	Georgi
Mifflinu	Mifflin	k1gInSc2	Mifflin
Dallasovi	Dallasův	k2eAgMnPc1d1	Dallasův
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
viceprezident	viceprezident	k1gMnSc1	viceprezident
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
-	-	kIx~	-
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajů	údaj	k1gInPc2	údaj
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
1,2	[number]	k4	1,2
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dallas	Dallas	k1gInSc1	Dallas
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
kulturním	kulturní	k2eAgNnSc7d1	kulturní
<g/>
,	,	kIx,	,
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
a	a	k8xC	a
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Dallas-Fort	Dallas-Fort	k1gInSc1	Dallas-Fort
Worth-Arlington	Worth-Arlington	k1gInSc1	Worth-Arlington
(	(	kIx(	(
<g/>
Dallas	Dallas	k1gInSc1	Dallas
<g/>
/	/	kIx~	/
<g/>
Fort	Fort	k?	Fort
Worth	Worth	k1gInSc1	Worth
Metroplex	Metroplex	k1gInSc1	Metroplex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
5,7	[number]	k4	5,7
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
pátou	pátá	k1gFnSc4	pátá
největší	veliký	k2eAgFnSc7d3	veliký
metropolitní	metropolitní	k2eAgFnSc7d1	metropolitní
oblastí	oblast	k1gFnSc7	oblast
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
zabírá	zabírat	k5eAaImIp3nS	zabírat
území	území	k1gNnSc4	území
12	[number]	k4	12
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Dallas	Dallas	k1gInSc1	Dallas
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1856	[number]	k4	1856
získal	získat	k5eAaPmAgInS	získat
statut	statut	k1gInSc1	statut
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Důležitost	důležitost	k1gFnSc1	důležitost
Dallasu	Dallas	k1gInSc2	Dallas
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
historického	historický	k2eAgInSc2d1	historický
významu	význam	k1gInSc2	význam
jakožto	jakožto	k8xS	jakožto
centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zpracování	zpracování	k1gNnSc2	zpracování
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Výhodná	výhodný	k2eAgFnSc1d1	výhodná
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mnoha	mnoho	k4c2	mnoho
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Johna	John	k1gMnSc2	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Dallas	Dallas	k1gInSc1	Dallas
1	[number]	k4	1
197	[number]	k4	197
816	[number]	k4	816
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc4d1	průměrné
stáří	stáří	k1gNnSc4	stáří
obyvatel	obyvatel	k1gMnPc2	obyvatel
Dallasu	Dallas	k1gInSc2	Dallas
je	být	k5eAaImIp3nS	být
31,8	[number]	k4	31,8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
téhož	týž	k3xTgNnSc2	týž
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
populaci	populace	k1gFnSc3	populace
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
50,7	[number]	k4	50,7
%	%	kIx~	%
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
28,8	[number]	k4	28,8
%	%	kIx~	%
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
25	[number]	k4	25
%	%	kIx~	%
černoši	černoch	k1gMnPc1	černoch
nebo	nebo	k8xC	nebo
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
<g/>
,	,	kIx,	,
0,7	[number]	k4	0,7
%	%	kIx~	%
domorodí	domorodý	k2eAgMnPc1d1	domorodý
Američané	Američan	k1gMnPc1	Američan
a	a	k8xC	a
domorodí	domorodý	k2eAgMnPc1d1	domorodý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
,	,	kIx,	,
2,9	[number]	k4	2,9
%	%	kIx~	%
Asijci	Asijec	k1gMnPc1	Asijec
a	a	k8xC	a
2,6	[number]	k4	2,6
%	%	kIx~	%
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
.	.	kIx.	.
42,4	[number]	k4	42,4
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Hispánci	Hispánek	k1gMnPc1	Hispánek
či	či	k8xC	či
Latinoameričané	Latinoameričan	k1gMnPc1	Latinoameričan
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
American	American	k1gInSc1	American
Community	Communit	k2eAgInPc1d1	Communit
Survey	Survey	k1gInPc1	Survey
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2006-2010	[number]	k4	2006-2010
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hispánskou	hispánský	k2eAgFnSc4d1	hispánská
populaci	populace	k1gFnSc4	populace
Dallasu	Dallas	k1gInSc2	Dallas
tvoří	tvořit	k5eAaImIp3nS	tvořit
36,8	[number]	k4	36,8
%	%	kIx~	%
Mexičanů	Mexičan	k1gMnPc2	Mexičan
<g/>
,	,	kIx,	,
0,3	[number]	k4	0,3
%	%	kIx~	%
Portoričanů	Portoričan	k1gMnPc2	Portoričan
<g/>
,	,	kIx,	,
0,2	[number]	k4	0,2
%	%	kIx~	%
Kubánců	Kubánec	k1gMnPc2	Kubánec
a	a	k8xC	a
4,3	[number]	k4	4,3
%	%	kIx~	%
jiných	jiný	k2eAgMnPc2d1	jiný
Hispánců	Hispánec	k1gMnPc2	Hispánec
a	a	k8xC	a
Latinoameračanů	Latinoameračan	k1gMnPc2	Latinoameračan
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
nachází	nacházet	k5eAaImIp3nS	nacházet
celkem	celkem	k6eAd1	celkem
458	[number]	k4	458
057	[number]	k4	057
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
29,1	[number]	k4	29,1
%	%	kIx~	%
bydlí	bydlet	k5eAaImIp3nS	bydlet
i	i	k9	i
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
36,1	[number]	k4	36,1
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
obývají	obývat	k5eAaImIp3nP	obývat
manželské	manželský	k2eAgInPc1d1	manželský
páry	pár	k1gInPc1	pár
žijící	žijící	k2eAgInPc1d1	žijící
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
v	v	k7c6	v
16,0	[number]	k4	16,0
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
domácnosti	domácnost	k1gFnSc2	domácnost
pouze	pouze	k6eAd1	pouze
žena	žena	k1gFnSc1	žena
bez	bez	k7c2	bez
manžela	manžel	k1gMnSc2	manžel
a	a	k8xC	a
42	[number]	k4	42
%	%	kIx~	%
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
klasifikováno	klasifikovat	k5eAaImNgNnS	klasifikovat
jako	jako	k8xS	jako
nerodinné	rodinný	k2eNgFnPc1d1	nerodinná
domácnosti	domácnost	k1gFnPc1	domácnost
<g/>
.	.	kIx.	.
33,7	[number]	k4	33,7
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
domácností	domácnost	k1gFnPc2	domácnost
obývá	obývat	k5eAaImIp3nS	obývat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
mladších	mladý	k2eAgNnPc2d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
17,6	[number]	k4	17,6
%	%	kIx~	%
obývá	obývat	k5eAaImIp3nS	obývat
minimálně	minimálně	k6eAd1	minimálně
jeden	jeden	k4xCgMnSc1	jeden
člověk	člověk	k1gMnSc1	člověk
starší	starší	k1gMnSc1	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
domácnost	domácnost	k1gFnSc1	domácnost
má	mít	k5eAaImIp3nS	mít
2,57	[number]	k4	2,57
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rodina	rodina	k1gFnSc1	rodina
3,42	[number]	k4	3,42
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zkoumalo	zkoumat	k5eAaImAgNnS	zkoumat
i	i	k9	i
věkové	věkový	k2eAgNnSc1d1	věkové
rozložení	rozložení	k1gNnSc1	rozložení
populace	populace	k1gFnSc2	populace
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
26,5	[number]	k4	26,5
%	%	kIx~	%
mladších	mladý	k2eAgNnPc2d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
8,8	[number]	k4	8,8
%	%	kIx~	%
starších	starý	k2eAgNnPc2d2	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
stáří	stáří	k1gNnSc1	stáří
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
31,8	[number]	k4	31,8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
50	[number]	k4	50
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
50	[number]	k4	50
%	%	kIx~	%
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
American	American	k1gInSc1	American
Community	Communita	k1gFnSc2	Communita
Survey	Survea	k1gFnSc2	Survea
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2005-2007	[number]	k4	2005-2007
činí	činit	k5eAaImIp3nS	činit
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
dallaské	dallaský	k2eAgFnSc2d1	dallaská
domácnosti	domácnost	k1gFnSc2	domácnost
40	[number]	k4	40
147	[number]	k4	147
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
810	[number]	k4	810
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
rodiny	rodina	k1gFnSc2	rodina
je	být	k5eAaImIp3nS	být
42	[number]	k4	42
670	[number]	k4	670
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
860	[number]	k4	860
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
zaměstnaný	zaměstnaný	k1gMnSc1	zaměstnaný
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
úvazek	úvazek	k1gInSc4	úvazek
má	mít	k5eAaImIp3nS	mít
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
32	[number]	k4	32
265	[number]	k4	265
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
cca	cca	kA	cca
650	[number]	k4	650
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
a	a	k8xC	a
žena	žena	k1gFnSc1	žena
32	[number]	k4	32
402	[number]	k4	402
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
asi	asi	k9	asi
652	[number]	k4	652
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
per	pero	k1gNnPc2	pero
capita	capita	k1gMnSc1	capita
činí	činit	k5eAaImIp3nS	činit
25	[number]	k4	25
904	[number]	k4	904
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
520	[number]	k4	520
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
18,7	[number]	k4	18,7
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
21,7	[number]	k4	21,7
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
Dallasu	Dallas	k1gInSc2	Dallas
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
33,6	[number]	k4	33,6
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
osoby	osoba	k1gFnPc1	osoba
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
13,4	[number]	k4	13,4
%	%	kIx~	%
osoby	osoba	k1gFnPc1	osoba
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
stál	stát	k5eAaImAgInS	stát
průměrně	průměrně	k6eAd1	průměrně
129	[number]	k4	129
600	[number]	k4	600
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
600	[number]	k4	600
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Dallasu	Dallas	k1gInSc2	Dallas
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
převážně	převážně	k6eAd1	převážně
bílá	bílý	k2eAgNnPc1d1	bílé
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
tvořili	tvořit	k5eAaImAgMnP	tvořit
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
82,8	[number]	k4	82,8
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
velikosti	velikost	k1gFnSc2	velikost
i	i	k8xC	i
významu	význam	k1gInSc2	význam
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
stala	stát	k5eAaPmAgFnS	stát
pestřejší	pestrý	k2eAgFnSc1d2	pestřejší
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
představují	představovat	k5eAaImIp3nP	představovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
nedávné	dávný	k2eNgInPc1d1	nedávný
údaje	údaj	k1gInPc1	údaj
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
26,5	[number]	k4	26,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Dallasu	Dallas	k1gInSc2	Dallas
a	a	k8xC	a
17	[number]	k4	17
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
celé	celý	k2eAgFnSc2d1	celá
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Dallas	Dallas	k1gInSc1	Dallas
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
mexických	mexický	k2eAgMnPc2d1	mexický
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
legálních	legální	k2eAgInPc2d1	legální
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
nelegálních	legální	k2eNgFnPc2d1	nelegální
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgFnPc1d1	jihozápadní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnPc1d1	jihovýchodní
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
čtvrti	čtvrt	k1gFnPc1	čtvrt
Oak	Oak	k1gMnPc1	Oak
Cliff	Cliff	k1gMnSc1	Cliff
a	a	k8xC	a
Pleasant	Pleasant	k1gMnSc1	Pleasant
Grove	Groev	k1gFnSc2	Groev
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
obývány	obývat	k5eAaImNgInP	obývat
černými	černý	k2eAgFnPc7d1	černá
a	a	k8xC	a
hispánskými	hispánský	k2eAgMnPc7d1	hispánský
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
města	město	k1gNnSc2	město
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
černoši	černoch	k1gMnPc1	černoch
a	a	k8xC	a
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
a	a	k8xC	a
východních	východní	k2eAgFnPc6d1	východní
zase	zase	k9	zase
Hispánci	Hispánec	k1gMnPc7	Hispánec
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
sever	sever	k1gInSc4	sever
Dallasu	Dallas	k1gInSc2	Dallas
obývají	obývat	k5eAaImIp3nP	obývat
většinou	většina	k1gFnSc7	většina
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
tam	tam	k6eAd1	tam
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
mnoho	mnoho	k4c4	mnoho
enkláv	enkláva	k1gFnPc2	enkláva
převážně	převážně	k6eAd1	převážně
černých	černý	k2eAgMnPc2d1	černý
a	a	k8xC	a
hispánských	hispánský	k2eAgMnPc2d1	hispánský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
Dallas	Dallas	k1gInSc1	Dallas
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
předměstí	předměstí	k1gNnSc6	předměstí
domovem	domov	k1gInSc7	domov
i	i	k8xC	i
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
asijských	asijský	k2eAgMnPc2d1	asijský
obyvatel	obyvatel	k1gMnPc2	obyvatel
-	-	kIx~	-
Korejců	Korejec	k1gMnPc2	Korejec
<g/>
,	,	kIx,	,
Tchajwanců	Tchajwanec	k1gMnPc2	Tchajwanec
<g/>
,	,	kIx,	,
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
,	,	kIx,	,
Filipínců	Filipínec	k1gMnPc2	Filipínec
<g/>
,	,	kIx,	,
Vietnamců	Vietnamec	k1gMnPc2	Vietnamec
<g/>
,	,	kIx,	,
Thajců	Thajce	k1gMnPc2	Thajce
<g/>
,	,	kIx,	,
Indů	Ind	k1gMnPc2	Ind
<g/>
,	,	kIx,	,
Bangladéšanů	Bangladéšan	k1gMnPc2	Bangladéšan
<g/>
,	,	kIx,	,
Pákistánců	Pákistánec	k1gMnPc2	Pákistánec
<g/>
,	,	kIx,	,
Srílančanů	Srílančan	k1gMnPc2	Srílančan
<g/>
,	,	kIx,	,
Nepálců	Nepálec	k1gMnPc2	Nepálec
a	a	k8xC	a
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
velké	velký	k2eAgNnSc4d1	velké
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
Arlington	Arlington	k1gInSc1	Arlington
<g/>
,	,	kIx,	,
Garland	Garland	k1gInSc1	Garland
<g/>
,	,	kIx,	,
Richardson	Richardson	k1gInSc1	Richardson
<g/>
,	,	kIx,	,
Plano	Plano	k?	Plano
<g/>
,	,	kIx,	,
Carrollton	Carrollton	k1gInSc1	Carrollton
<g/>
,	,	kIx,	,
Irving	Irving	k1gInSc1	Irving
<g/>
,	,	kIx,	,
Frisco	Frisco	k1gMnSc1	Frisco
<g/>
,	,	kIx,	,
Flower	Flower	k1gMnSc1	Flower
Mound	Mound	k1gMnSc1	Mound
a	a	k8xC	a
Allen	Allen	k1gMnSc1	Allen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Afrického	africký	k2eAgInSc2d1	africký
rohu	roh	k1gInSc2	roh
-	-	kIx~	-
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
,	,	kIx,	,
Eritrey	Eritrea	k1gFnSc2	Eritrea
a	a	k8xC	a
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
vysokému	vysoký	k2eAgInSc3d1	vysoký
počtu	počet	k1gInSc3	počet
přistěhovaleckých	přistěhovalecký	k2eAgFnPc2d1	přistěhovalecká
skupin	skupina	k1gFnPc2	skupina
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
vícejazyčné	vícejazyčný	k2eAgInPc1d1	vícejazyčný
nápisy	nápis	k1gInPc1	nápis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
Dallas	Dallas	k1gInSc1	Dallas
-	-	kIx~	-
Fort	Fort	k?	Fort
Worth	Worth	k1gInSc1	Worth
žije	žít	k5eAaImIp3nS	žít
odhadem	odhad	k1gInSc7	odhad
70	[number]	k4	70
000	[number]	k4	000
rusky	rusky	k6eAd1	rusky
mluvících	mluvící	k2eAgFnPc2d1	mluvící
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
zahrnuti	zahrnout	k5eAaPmNgMnP	zahrnout
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
ruští	ruský	k2eAgMnPc1d1	ruský
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Bělorusové	Bělorus	k1gMnPc1	Bělorus
<g/>
,	,	kIx,	,
Moldavané	Moldavan	k1gMnPc1	Moldavan
<g/>
,	,	kIx,	,
Uzbeci	Uzbek	k1gMnPc1	Uzbek
<g/>
,	,	kIx,	,
Kyrgyzové	Kyrgyzové	k2eAgMnPc1d1	Kyrgyzové
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Rusky	Ruska	k1gFnPc1	Ruska
mluvící	mluvící	k2eAgFnSc2d1	mluvící
populace	populace	k1gFnSc2	populace
Dallasu	Dallas	k1gInSc2	Dallas
nadále	nadále	k6eAd1	nadále
roste	růst	k5eAaImIp3nS	růst
díky	díky	k7c3	díky
modelu	model	k1gInSc3	model
"	"	kIx"	"
<g/>
manžel	manžel	k1gMnSc1	manžel
Američan	Američan	k1gMnSc1	Američan
-	-	kIx~	-
manželka	manželka	k1gFnSc1	manželka
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
komunita	komunita	k1gFnSc1	komunita
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
i	i	k9	i
vlastní	vlastní	k2eAgFnPc1d1	vlastní
noviny	novina	k1gFnPc1	novina
The	The	k1gFnSc2	The
Dallas	Dallas	k1gMnSc1	Dallas
Telegraph	Telegraph	k1gMnSc1	Telegraph
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Dallasu	Dallas	k1gInSc2	Dallas
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Texasu	Texas	k1gInSc2	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
se	se	k3xPyFc4	se
do	do	k7c2	do
Dallasu	Dallas	k1gInSc2	Dallas
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
Středozápadu	středozápad	k1gInSc2	středozápad
<g/>
,	,	kIx,	,
Severovýchodu	severovýchod	k1gInSc2	severovýchod
a	a	k8xC	a
z	z	k7c2	z
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dallas	Dallas	k1gInSc1	Dallas
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
DART	DART	kA	DART
-	-	kIx~	-
Dallas	Dallas	k1gInSc1	Dallas
Area	area	k1gFnSc1	area
Rapid	rapid	k1gInSc1	rapid
Transit	transit	k1gInSc1	transit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
rychlodrážních	rychlodrážní	k2eAgFnPc2d1	rychlodrážní
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
postavena	postavit	k5eAaPmNgFnS	postavit
první	první	k4xOgFnSc1	první
rychlodrážní	rychlodrážní	k2eAgFnSc1d1	rychlodrážní
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
linka	linka	k1gFnSc1	linka
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
-	-	kIx~	-
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
a	a	k8xC	a
Oranžová	oranžový	k2eAgFnSc1d1	oranžová
linka	linka	k1gFnSc1	linka
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Dijon	Dijon	k1gInSc1	Dijon
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
Kalkata	Kalkata	k1gFnSc1	Kalkata
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
Monterrey	Monterrea	k1gFnSc2	Monterrea
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Valencia	Valencia	k1gFnSc1	Valencia
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Riga	Riga	k1gFnSc1	Riga
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
Saratov	Saratov	k1gInSc1	Saratov
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Sendai	Senda	k1gFnSc2	Senda
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Tchaj-pej	Tchajej	k1gFnSc2	Tchaj-pej
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
Tchien-ťin	Tchien-ťin	k1gInSc1	Tchien-ťin
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
</s>
