<p>
<s>
Buřinka	buřinka	k1gFnSc1	buřinka
(	(	kIx(	(
<g/>
též	též	k9	též
tvrďák	tvrďák	k?	tvrďák
či	či	k8xC	či
bouřka	bouřka	k1gFnSc1	bouřka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
plstěný	plstěný	k2eAgInSc1d1	plstěný
pánský	pánský	k2eAgInSc1d1	pánský
klobouk	klobouk	k1gInSc1	klobouk
s	s	k7c7	s
okrouhlým	okrouhlý	k2eAgInSc7d1	okrouhlý
vrškem	vršek	k1gInSc7	vršek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
klasická	klasický	k2eAgFnSc1d1	klasická
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
legendárním	legendární	k2eAgNnSc6d1	legendární
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
kloboučnictví	kloboučnictví	k1gNnSc6	kloboučnictví
Lock	Locka	k1gFnPc2	Locka
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
objednal	objednat	k5eAaPmAgMnS	objednat
jistý	jistý	k2eAgInSc4d1	jistý
William	William	k1gInSc4	William
Coke	Cok	k1gFnSc2	Cok
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
pro	pro	k7c4	pro
hajné	hajná	k1gFnPc4	hajná
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
panství	panství	k1gNnSc6	panství
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
měla	mít	k5eAaImAgFnS	mít
chránit	chránit	k5eAaImF	chránit
hlavy	hlava	k1gFnPc4	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
a	a	k8xC	a
oblý	oblý	k2eAgInSc1d1	oblý
klobouk	klobouk	k1gInSc1	klobouk
opravdu	opravdu	k6eAd1	opravdu
výborně	výborně	k6eAd1	výborně
odolával	odolávat	k5eAaImAgMnS	odolávat
překážkám	překážka	k1gFnPc3	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Williamu	William	k1gInSc6	William
Cokeovi	Cokeův	k2eAgMnPc1d1	Cokeův
v	v	k7c4	v
kloboučnictví	kloboučnictví	k1gNnPc4	kloboučnictví
ukázali	ukázat	k5eAaPmAgMnP	ukázat
prototyp	prototyp	k1gInSc4	prototyp
buřinky	buřinka	k1gFnSc2	buřinka
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
jej	on	k3xPp3gMnSc4	on
před	před	k7c4	před
dílnu	dílna	k1gFnSc4	dílna
<g/>
,	,	kIx,	,
položil	položit	k5eAaPmAgMnS	položit
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
chodník	chodník	k1gInSc4	chodník
a	a	k8xC	a
šlápl	šlápnout	k5eAaPmAgMnS	šlápnout
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
jeho	jeho	k3xOp3gFnSc4	jeho
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
tento	tento	k3xDgInSc4	tento
test	test	k1gInSc4	test
vydržel	vydržet	k5eAaPmAgMnS	vydržet
a	a	k8xC	a
zákazník	zákazník	k1gMnSc1	zákazník
si	se	k3xPyFc3	se
jich	on	k3xPp3gMnPc2	on
několik	několik	k4yIc4	několik
objednal	objednat	k5eAaPmAgMnS	objednat
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
buřince	buřinka	k1gFnSc3	buřinka
anglicky	anglicky	k6eAd1	anglicky
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
coke	coke	k6eAd1	coke
<g/>
"	"	kIx"	"
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
její	její	k3xOp3gInSc4	její
vznik	vznik	k1gInSc4	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
firma	firma	k1gFnSc1	firma
Bowler	Bowler	k1gInSc4	Bowler
&	&	k?	&
Son	son	k1gInSc1	son
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
dnešní	dnešní	k2eAgInSc4d1	dnešní
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
bowler	bowler	k1gInSc4	bowler
hat	hat	k0	hat
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
novináře	novinář	k1gMnSc2	novinář
Lucia	Lucius	k1gMnSc2	Lucius
Beebeho	Beebe	k1gMnSc2	Beebe
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
buřinka	buřinka	k1gFnSc1	buřinka
zvaná	zvaný	k2eAgFnSc1d1	zvaná
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
derby	derby	k1gNnSc2	derby
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
dobyl	dobýt	k5eAaPmAgMnS	dobýt
americký	americký	k2eAgInSc4d1	americký
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
kovbojský	kovbojský	k2eAgInSc1d1	kovbojský
klobouk	klobouk	k1gInSc1	klobouk
či	či	k8xC	či
sombrero	sombrero	k1gNnSc1	sombrero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
totiž	totiž	k9	totiž
jak	jak	k6eAd1	jak
kovbojové	kovboj	k1gMnPc1	kovboj
<g/>
,	,	kIx,	,
tak	tak	k9	tak
dělníci	dělník	k1gMnPc1	dělník
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gInPc3	on
nepadala	padat	k5eNaImAgFnS	padat
snadno	snadno	k6eAd1	snadno
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
za	za	k7c2	za
silného	silný	k2eAgInSc2d1	silný
větru	vítr	k1gInSc2	vítr
nebo	nebo	k8xC	nebo
když	když	k8xS	když
vystrčili	vystrčit	k5eAaPmAgMnP	vystrčit
hlavu	hlava	k1gFnSc4	hlava
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
jedoucího	jedoucí	k2eAgInSc2d1	jedoucí
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Nosili	nosit	k5eAaImAgMnP	nosit
ji	on	k3xPp3gFnSc4	on
jak	jak	k6eAd1	jak
strážci	strážce	k1gMnPc1	strážce
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
psanci	psanec	k1gMnPc1	psanec
jako	jako	k9	jako
Bat	Bat	k1gMnSc1	Bat
Masterson	Masterson	k1gMnSc1	Masterson
<g/>
,	,	kIx,	,
Butch	Butch	k1gMnSc1	Butch
Cassidy	Cassida	k1gFnSc2	Cassida
<g/>
,	,	kIx,	,
Black	Black	k1gInSc4	Black
Bart	Barta	k1gFnPc2	Barta
či	či	k8xC	či
Billy	Bill	k1gMnPc4	Bill
the	the	k?	the
Kid	Kid	k1gFnSc1	Kid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
nosily	nosit	k5eAaImAgInP	nosit
buřinku	buřinka	k1gFnSc4	buřinka
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
španělsky	španělsky	k6eAd1	španělsky
bombín	bombína	k1gFnPc2	bombína
<g/>
,	,	kIx,	,
kečujské	kečujský	k2eAgFnPc1d1	kečujský
a	a	k8xC	a
aymarské	aymarský	k2eAgFnPc1d1	aymarský
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesli	přinést	k5eAaPmAgMnP	přinést
britští	britský	k2eAgMnPc1d1	britský
železniční	železniční	k2eAgMnPc1d1	železniční
dělníci	dělník	k1gMnPc1	dělník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
symbolem	symbol	k1gInSc7	symbol
londýnských	londýnský	k2eAgMnPc2d1	londýnský
burzovních	burzovní	k2eAgMnPc2d1	burzovní
makléřů	makléř	k1gMnPc2	makléř
a	a	k8xC	a
bankéřů	bankéř	k1gMnPc2	bankéř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ji	on	k3xPp3gFnSc4	on
nosí	nosit	k5eAaImIp3nP	nosit
již	již	k6eAd1	již
jen	jen	k9	jen
důstojníci	důstojník	k1gMnPc1	důstojník
královské	královský	k2eAgFnSc2d1	královská
gardy	garda	k1gFnSc2	garda
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
civilního	civilní	k2eAgInSc2d1	civilní
obleku	oblek	k1gInSc2	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavách	hlava	k1gFnPc6	hlava
burzovních	burzovní	k2eAgMnPc2d1	burzovní
makléřů	makléř	k1gMnPc2	makléř
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
buřinku	buřinka	k1gFnSc4	buřinka
spatřit	spatřit	k5eAaPmF	spatřit
již	již	k6eAd1	již
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
nositelé	nositel	k1gMnPc1	nositel
==	==	k?	==
</s>
</p>
<p>
<s>
Stan	stan	k1gInSc1	stan
Laurel	Laurela	k1gFnPc2	Laurela
a	a	k8xC	a
Oliver	Olivra	k1gFnPc2	Olivra
Hardy	Harda	k1gFnSc2	Harda
<g/>
,	,	kIx,	,
komická	komický	k2eAgFnSc1d1	komická
dvojice	dvojice	k1gFnSc1	dvojice
z	z	k7c2	z
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
komik	komik	k1gMnSc1	komik
</s>
</p>
<p>
<s>
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gInSc1	Churchill
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
partner	partner	k1gMnSc1	partner
Jiřího	Jiří	k1gMnSc2	Jiří
Suchého	Suchého	k2eAgInSc2d1	Suchého
</s>
</p>
<p>
<s>
Henri	Henri	k6eAd1	Henri
de	de	k?	de
Toulouse-Lautrec	Toulouse-Lautrec	k1gMnSc1	Toulouse-Lautrec
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Jack	Jack	k1gMnSc1	Jack
White	Whit	k1gInSc5	Whit
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
White	Whit	k1gMnSc5	Whit
Stripes	Stripes	k1gInSc1	Stripes
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Tenant	Tenant	k1gMnSc1	Tenant
a	a	k8xC	a
Chris	Chris	k1gFnSc1	Chris
Lowe	Lowe	k1gFnSc1	Lowe
z	z	k7c2	z
anglické	anglický	k2eAgFnSc2d1	anglická
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Pet	Pet	k1gFnPc1	Pet
Shop	shop	k1gInSc4	shop
Boys	boy	k1gMnPc2	boy
nosili	nosit	k5eAaImAgMnP	nosit
buřinky	buřinka	k1gFnPc4	buřinka
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
svého	svůj	k3xOyFgNnSc2	svůj
světového	světový	k2eAgNnSc2d1	světové
turné	turné	k1gNnSc2	turné
Performance	performance	k1gFnSc2	performance
</s>
</p>
<p>
<s>
===	===	k?	===
Buřinka	buřinka	k1gFnSc1	buřinka
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
filmu	film	k1gInSc6	film
===	===	k?	===
</s>
</p>
<p>
<s>
Hercule	Hercula	k1gFnSc3	Hercula
Poirot	Poirot	k1gMnSc1	Poirot
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgMnSc1d1	fiktivní
belgický	belgický	k2eAgMnSc1d1	belgický
detektív	detektív	k1gMnSc1	detektív
z	z	k7c2	z
románů	román	k1gInPc2	román
anglické	anglický	k2eAgFnSc2d1	anglická
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Agathy	Agatha	k1gFnSc2	Agatha
Christie	Christie	k1gFnSc2	Christie
</s>
</p>
<p>
<s>
postava	postava	k1gFnSc1	postava
Alexe	Alex	k1gMnSc5	Alex
(	(	kIx(	(
<g/>
Malcolm	Malcolm	k1gMnSc1	Malcolm
McDowell	McDowell	k1gMnSc1	McDowell
<g/>
)	)	kIx)	)
z	z	k7c2	z
filmu	film	k1gInSc2	film
Stanleyho	Stanley	k1gMnSc2	Stanley
Kubricka	Kubricko	k1gNnSc2	Kubricko
Mechanický	mechanický	k2eAgInSc1d1	mechanický
pomeranč	pomeranč	k1gInSc1	pomeranč
</s>
</p>
<p>
<s>
postava	postava	k1gFnSc1	postava
Oddjoba	Oddjoba	k1gFnSc1	Oddjoba
z	z	k7c2	z
bondovky	bondovky	k?	bondovky
Goldfinger	Goldfinger	k1gInSc1	Goldfinger
používá	používat	k5eAaImIp3nS	používat
buřinku	buřinka	k1gFnSc4	buřinka
s	s	k7c7	s
nabroušeným	nabroušený	k2eAgNnSc7d1	nabroušené
ostřím	ostří	k1gNnSc7	ostří
jako	jako	k8xC	jako
zbraň	zbraň	k1gFnSc1	zbraň
</s>
</p>
<p>
<s>
postavy	postava	k1gFnPc1	postava
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Samuela	Samuel	k1gMnSc2	Samuel
Becketta	Beckett	k1gMnSc2	Beckett
Čekání	čekání	k1gNnSc2	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
popisu	popis	k1gInSc6	popis
kostýmů	kostým	k1gInPc2	kostým
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
hlavách	hlava	k1gFnPc6	hlava
buřinky	buřinka	k1gFnSc2	buřinka
</s>
</p>
<p>
<s>
v	v	k7c6	v
Kunderově	Kunderův	k2eAgInSc6d1	Kunderův
románu	román	k1gInSc6	román
Nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
lehkost	lehkost	k1gFnSc1	lehkost
bytí	bytí	k1gNnSc2	bytí
hraje	hrát	k5eAaImIp3nS	hrát
buřinka	buřinka	k1gFnSc1	buřinka
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
</s>
</p>
<p>
<s>
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
buřinku	buřinka	k1gFnSc4	buřinka
používá	používat	k5eAaImIp3nS	používat
Pan	Pan	k1gMnSc1	Pan
Tau	tau	k1gNnSc2	tau
<g/>
,	,	kIx,	,
kouzelník	kouzelník	k1gMnSc1	kouzelník
ze	z	k7c2	z
série	série	k1gFnSc2	série
stejnojmenných	stejnojmenný	k2eAgInPc2d1	stejnojmenný
československých	československý	k2eAgInPc2d1	československý
filmů	film	k1gInPc2	film
a	a	k8xC	a
seriálů	seriál	k1gInPc2	seriál
</s>
</p>
<p>
<s>
detektivové	detektiv	k1gMnPc1	detektiv
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Hříšní	hříšný	k2eAgMnPc1d1	hříšný
lidé	člověk	k1gMnPc1	člověk
města	město	k1gNnSc2	město
Pražského	pražský	k2eAgInSc2d1	pražský
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
-	-	kIx~	-
Noční	noční	k2eAgInSc1d1	noční
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
Wries	Wries	k1gInSc1	Wries
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Buřinka	buřinka	k1gFnSc1	buřinka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
buřinka	buřinka	k1gFnSc1	buřinka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
