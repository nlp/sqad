<p>
<s>
Ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
menší	malý	k2eAgMnSc1d2	menší
(	(	kIx(	(
<g/>
Lanius	Lanius	k1gInSc1	Lanius
minor	minor	k2eAgInSc1d1	minor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
pěvce	pěvec	k1gMnSc2	pěvec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ťuhýkovitých	ťuhýkovitý	k2eAgInPc2d1	ťuhýkovitý
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
mezi	mezi	k7c7	mezi
ťuhýkem	ťuhýk	k1gMnSc7	ťuhýk
obecným	obecný	k2eAgMnSc7d1	obecný
a	a	k8xC	a
ťuhýkem	ťuhýk	k1gMnSc7	ťuhýk
šedým	šedý	k2eAgMnSc7d1	šedý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
šedý	šedý	k2eAgMnSc1d1	šedý
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
oční	oční	k2eAgFnSc1d1	oční
maska	maska	k1gFnSc1	maska
však	však	k9	však
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
ptáků	pták	k1gMnPc2	pták
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
a	a	k8xC	a
přední	přední	k2eAgFnSc4d1	přední
část	část	k1gFnSc4	část
temene	temeno	k1gNnSc2	temeno
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc4	břicho
a	a	k8xC	a
hruď	hruď	k1gFnSc4	hruď
jsou	být	k5eAaImIp3nP	být
lososově	lososově	k6eAd1	lososově
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
poněkud	poněkud	k6eAd1	poněkud
kratší	krátký	k2eAgInSc4d2	kratší
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
šedé	šedý	k2eAgNnSc4d1	šedé
čelo	čelo	k1gNnSc4	čelo
a	a	k8xC	a
světle	světle	k6eAd1	světle
vlnkované	vlnkovaný	k2eAgNnSc4d1	vlnkované
temeno	temeno	k1gNnSc4	temeno
a	a	k8xC	a
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zcela	zcela	k6eAd1	zcela
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
registrována	registrovat	k5eAaBmNgFnS	registrovat
pouhá	pouhý	k2eAgNnPc4d1	pouhé
dvě	dva	k4xCgNnPc4	dva
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
menší	malý	k2eAgMnSc1d2	menší
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
menší	malý	k2eAgMnSc1d2	menší
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Lanius	Lanius	k1gInSc1	Lanius
minor	minor	k2eAgInSc1d1	minor
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
