<s>
NNTP	NNTP	kA	NNTP
(	(	kIx(	(
<g/>
Network	network	k1gInSc1	network
News	News	k1gInSc1	News
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
přenosový	přenosový	k2eAgInSc4d1	přenosový
protokol	protokol	k1gInSc4	protokol
pro	pro	k7c4	pro
síťové	síťový	k2eAgFnPc4d1	síťová
diskuzní	diskuzní	k2eAgFnPc4d1	diskuzní
skupiny	skupina	k1gFnPc4	skupina
(	(	kIx(	(
<g/>
též	též	k9	též
Usenet	Usenet	k1gInSc1	Usenet
NetNews	NetNewsa	k1gFnPc2	NetNewsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
zejména	zejména	k9	zejména
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvky	příspěvek	k1gInPc1	příspěvek
diskuzních	diskuzní	k2eAgFnPc2d1	diskuzní
skupin	skupina	k1gFnPc2	skupina
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgFnPc1d1	uložena
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
serverech	server	k1gInPc6	server
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
příspěvky	příspěvek	k1gInPc1	příspěvek
synchronizují	synchronizovat	k5eAaBmIp3nP	synchronizovat
<g/>
.	.	kIx.	.
</s>
<s>
Servery	server	k1gInPc1	server
tvoří	tvořit	k5eAaImIp3nP	tvořit
distribuovanou	distribuovaný	k2eAgFnSc4d1	distribuovaná
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
NNTP	NNTP	kA	NNTP
protokol	protokol	k1gInSc1	protokol
ke	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
distribuce	distribuce	k1gFnSc1	distribuce
příspěvků	příspěvek	k1gInPc2	příspěvek
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
pomocí	pomocí	k7c2	pomocí
UUCP	UUCP	kA	UUCP
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
používalo	používat	k5eAaImAgNnS	používat
pro	pro	k7c4	pro
přenosy	přenos	k1gInPc4	přenos
telefonní	telefonní	k2eAgFnSc2d1	telefonní
linky	linka	k1gFnSc2	linka
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vytáčené	vytáčený	k2eAgNnSc1d1	vytáčené
připojení	připojení	k1gNnSc1	připojení
pomocí	pomocí	k7c2	pomocí
modemů	modem	k1gInPc2	modem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
byly	být	k5eAaImAgFnP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
soubory	soubor	k1gInPc1	soubor
se	s	k7c7	s
zprávami	zpráva	k1gFnPc7	zpráva
typicky	typicky	k6eAd1	typicky
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
zprávy	zpráva	k1gFnPc4	zpráva
četli	číst	k5eAaImAgMnP	číst
z	z	k7c2	z
lokálních	lokální	k2eAgInPc2d1	lokální
disků	disk	k1gInPc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvky	příspěvek	k1gInPc1	příspěvek
se	se	k3xPyFc4	se
ukládaly	ukládat	k5eAaImAgInP	ukládat
na	na	k7c4	na
disk	disk	k1gInSc4	disk
a	a	k8xC	a
opět	opět	k6eAd1	opět
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
distribuovaly	distribuovat	k5eAaBmAgFnP	distribuovat
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
Usenet	Usenet	k1gInSc4	Usenet
servery	server	k1gInPc1	server
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
interaktivní	interaktivní	k2eAgNnSc4d1	interaktivní
spojení	spojení	k1gNnSc4	spojení
pomocí	pomocí	k7c2	pomocí
protokolu	protokol	k1gInSc2	protokol
TCP	TCP	kA	TCP
byla	být	k5eAaImAgFnS	být
metoda	metoda	k1gFnSc1	metoda
výměny	výměna	k1gFnSc2	výměna
souborů	soubor	k1gInPc2	soubor
(	(	kIx(	(
<g/>
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
UUCP	UUCP	kA	UUCP
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
protokolem	protokol	k1gInSc7	protokol
NNTP	NNTP	kA	NNTP
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
do	do	k7c2	do
diskusních	diskusní	k2eAgFnPc2d1	diskusní
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
se	se	k3xPyFc4	se
z	z	k7c2	z
libovolného	libovolný	k2eAgNnSc2d1	libovolné
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
síti	síť	k1gFnSc6	síť
spojuje	spojovat	k5eAaImIp3nS	spojovat
pomocí	pomocí	k7c2	pomocí
NNTP	NNTP	kA	NNTP
protokolu	protokol	k1gInSc2	protokol
se	s	k7c7	s
serverem	server	k1gInSc7	server
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
příspěvky	příspěvek	k1gInPc4	příspěvek
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
diskovém	diskový	k2eAgInSc6d1	diskový
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
NNTP	NNTP	kA	NNTP
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
interaktivní	interaktivní	k2eAgInSc4d1	interaktivní
přenos	přenos	k1gInSc4	přenos
článků	článek	k1gInPc2	článek
s	s	k7c7	s
takřka	takřka	k6eAd1	takřka
nulovým	nulový	k2eAgNnSc7d1	nulové
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
omezena	omezen	k2eAgFnSc1d1	omezena
potřeba	potřeba	k1gFnSc1	potřeba
replikace	replikace	k1gFnSc2	replikace
článků	článek	k1gInPc2	článek
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
se	s	k7c7	s
serverem	server	k1gInSc7	server
<g/>
,	,	kIx,	,
ukládání	ukládání	k1gNnSc6	ukládání
<g/>
,	,	kIx,	,
čtení	čtení	k1gNnSc6	čtení
a	a	k8xC	a
předávání	předávání	k1gNnSc6	předávání
článků	článek	k1gInPc2	článek
slouží	sloužit	k5eAaImIp3nP	sloužit
klientům	klient	k1gMnPc3	klient
různé	různý	k2eAgInPc4d1	různý
příkazy	příkaz	k1gInPc4	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
NNTP	NNTP	kA	NNTP
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
také	také	k9	také
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
pasivní	pasivní	k2eAgInSc1d1	pasivní
způsob	způsob	k1gInSc1	způsob
přenosu	přenos	k1gInSc2	přenos
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
také	také	k9	také
hovorově	hovorově	k6eAd1	hovorově
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
pushing	pushing	k1gInSc4	pushing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tlačení	tlačení	k1gNnSc1	tlačení
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
pulling	pulling	k1gInSc1	pulling
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tažení	tažení	k1gNnSc1	tažení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tlačení	tlačení	k1gNnSc6	tlačení
klient	klient	k1gMnSc1	klient
zadá	zadat	k5eAaPmIp3nS	zadat
příkaz	příkaz	k1gInSc4	příkaz
IHAVE	IHAVE	kA	IHAVE
<varmsig>
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
již	již	k6eAd1	již
článek	článek	k1gInSc1	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
či	či	k8xC	či
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Tlačení	tlačený	k2eAgMnPc1d1	tlačený
však	však	k9	však
díky	díky	k7c3	díky
neustálému	neustálý	k2eAgNnSc3d1	neustálé
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
již	již	k9	již
článek	článek	k1gInSc1	článek
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
značně	značně	k6eAd1	značně
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
systém	systém	k1gInSc1	systém
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
tažení	tažení	k1gNnSc4	tažení
zadané	zadaný	k2eAgFnSc2d1	zadaná
příkazem	příkaz	k1gInSc7	příkaz
NEWNEWS	NEWNEWS	kA	NEWNEWS
zašle	zaslat	k5eAaPmIp3nS	zaslat
uživateli	uživatel	k1gMnSc3	uživatel
seznam	seznam	k1gInSc4	seznam
všech	všecek	k3xTgInPc2	všecek
dostupných	dostupný	k2eAgInPc2d1	dostupný
článků	článek	k1gInPc2	článek
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
následně	následně	k6eAd1	následně
vybere	vybrat	k5eAaPmIp3nS	vybrat
ty	ten	k3xDgInPc4	ten
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ještě	ještě	k6eAd1	ještě
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
tažení	tažení	k1gNnSc2	tažení
je	být	k5eAaImIp3nS	být
však	však	k9	však
autorizace	autorizace	k1gFnSc1	autorizace
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
server	server	k1gInSc1	server
musí	muset	k5eAaImIp3nS	muset
ujistit	ujistit	k5eAaPmF	ujistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
neposílá	posílat	k5eNaImIp3nS	posílat
neautorizovaným	autorizovaný	k2eNgMnPc3d1	neautorizovaný
uživatelům	uživatel	k1gMnPc3	uživatel
tajné	tajný	k2eAgFnPc1d1	tajná
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gMnPc3	on
nepřístupné	přístupný	k2eNgFnPc1d1	nepřístupná
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečenou	zabezpečený	k2eAgFnSc7d1	zabezpečená
variantou	varianta	k1gFnSc7	varianta
NNTP	NNTP	kA	NNTP
je	být	k5eAaImIp3nS	být
protokol	protokol	k1gInSc1	protokol
NNTPS	NNTPS	kA	NNTPS
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
komunikaci	komunikace	k1gFnSc4	komunikace
šifruje	šifrovat	k5eAaBmIp3nS	šifrovat
pomocí	pomocí	k7c2	pomocí
SSL	SSL	kA	SSL
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
standardně	standardně	k6eAd1	standardně
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
na	na	k7c6	na
TCP	TCP	kA	TCP
portu	port	k1gInSc2	port
119	[number]	k4	119
<g/>
,	,	kIx,	,
při	při	k7c6	při
zabezpečeném	zabezpečený	k2eAgNnSc6d1	zabezpečené
spojení	spojení	k1gNnSc6	spojení
pak	pak	k6eAd1	pak
na	na	k7c6	na
portu	port	k1gInSc6	port
563	[number]	k4	563
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
v	v	k7c6	v
RFC	RFC	kA	RFC
977	[number]	k4	977
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
k	k	k7c3	k
serveru	server	k1gInSc3	server
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komunikaci	komunikace	k1gFnSc4	komunikace
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
server	server	k1gInSc4	server
uvítací	uvítací	k2eAgInSc4d1	uvítací
zprávou	zpráva	k1gFnSc7	zpráva
začínající	začínající	k2eAgFnSc7d1	začínající
kódem	kód	k1gInSc7	kód
200	[number]	k4	200
nebo	nebo	k8xC	nebo
201	[number]	k4	201
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
textové	textový	k2eAgFnSc6d1	textová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
řádek	řádek	k1gInSc1	řádek
je	být	k5eAaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
znaky	znak	k1gInPc7	znak
CR-LF	CR-LF	k1gMnPc2	CR-LF
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
řádku	řádek	k1gInSc2	řádek
zasílanému	zasílaný	k2eAgInSc3d1	zasílaný
serveru	server	k1gInSc3	server
je	být	k5eAaImIp3nS	být
510	[number]	k4	510
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
512	[number]	k4	512
i	i	k8xC	i
s	s	k7c7	s
CR-LF	CR-LF	k1gFnSc7	CR-LF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hlavičky	hlavička	k1gFnSc2	hlavička
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
u	u	k7c2	u
příkazu	příkaz	k1gInSc2	příkaz
<ARTICLE>
je	být	k5eAaImIp3nS	být
server	server	k1gInSc1	server
oddělí	oddělit	k5eAaPmIp3nS	oddělit
jedním	jeden	k4xCgInSc7	jeden
prázdným	prázdný	k2eAgInSc7d1	prázdný
řádkem	řádek	k1gInSc7	řádek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
každým	každý	k3xTgInSc7	každý
příspěvkem	příspěvek	k1gInSc7	příspěvek
zašle	zaslat	k5eAaPmIp3nS	zaslat
server	server	k1gInSc4	server
jednu	jeden	k4xCgFnSc4	jeden
tečku	tečka	k1gFnSc4	tečka
na	na	k7c6	na
samostatném	samostatný	k2eAgInSc6d1	samostatný
řádku	řádek	k1gInSc6	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
příspěvku	příspěvek	k1gInSc2	příspěvek
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řádek	řádek	k1gInSc1	řádek
začínající	začínající	k2eAgInSc1d1	začínající
tečkou	tečka	k1gFnSc7	tečka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
tečka	tečka	k1gFnSc1	tečka
zdvojena	zdvojen	k2eAgFnSc1d1	zdvojena
<g/>
.	.	kIx.	.
<	<	kIx(	<
<g/>
otevření	otevření	k1gNnSc3	otevření
spojení	spojení	k1gNnSc3	spojení
<g/>
>	>	kIx)	>
S	s	k7c7	s
<g/>
:	:	kIx,	:
200	[number]	k4	200
news	newsa	k1gFnPc2	newsa
<g/>
.	.	kIx.	.
<g/>
fit	fit	k2eAgFnPc2d1	fit
<g/>
.	.	kIx.	.
<g/>
vutbr	vutbra	k1gFnPc2	vutbra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
InterNetNews	InterNetNews	k1gInSc1	InterNetNews
NNRP	NNRP	kA	NNRP
server	server	k1gInSc1	server
INN	INN	kA	INN
2.2	[number]	k4	2.2
<g/>
.2	.2	k4	.2
13	[number]	k4	13
<g/>
-Dec-	-Dec-	k?	-Dec-
<g/>
1999	[number]	k4	1999
ready	ready	k0	ready
(	(	kIx(	(
<g/>
posting	posting	k1gInSc1	posting
ok	oka	k1gFnPc2	oka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
:	:	kIx,	:
GROUP	GROUP	kA	GROUP
linux	linux	k1gInSc1	linux
<g/>
.	.	kIx.	.
<g/>
kernel	kernel	k1gInSc1	kernel
S	s	k7c7	s
<g/>
:	:	kIx,	:
211	[number]	k4	211
4923	[number]	k4	4923
395068	[number]	k4	395068
399990	[number]	k4	399990
linux	linux	k1gInSc1	linux
<g/>
.	.	kIx.	.
<g/>
kernel	kernel	k1gInSc1	kernel
C	C	kA	C
<g/>
:	:	kIx,	:
ARTICLE	ARTICLE	kA	ARTICLE
S	s	k7c7	s
<g/>
:	:	kIx,	:
220	[number]	k4	220
395068	[number]	k4	395068
<	<	kIx(	<
<g/>
74yTD-8pz-9@gated-at.bofh.it	74yTD-8pz-9@gatedt.bofh.it	k1gInSc1	74yTD-8pz-9@gated-at.bofh.it
<g/>
>	>	kIx)	>
article	article	k1gInSc1	article
<	<	kIx(	<
<g/>
server	server	k1gInSc1	server
posílá	posílat	k5eAaImIp3nS	posílat
tělo	tělo	k1gNnSc4	tělo
prvního	první	k4xOgMnSc2	první
příspěvku	příspěvek	k1gInSc2	příspěvek
<g/>
>	>	kIx)	>
S	s	k7c7	s
<g/>
:	:	kIx,	:
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
:	:	kIx,	:
NEXT	NEXT	kA	NEXT
S	s	k7c7	s
<g/>
:	:	kIx,	:
223	[number]	k4	223
395069	[number]	k4	395069
<	<	kIx(	<
<g/>
74yTE-8pz-13@gated-at.bofh.it	74yTE-8pz-13@gatedt.bofh.it	k1gInSc1	74yTE-8pz-13@gated-at.bofh.it
<g/>
>	>	kIx)	>
Article	Article	k1gInSc1	Article
retrieved	retrieved	k1gInSc1	retrieved
<g/>
;	;	kIx,	;
request	request	k5eAaPmF	request
text	text	k1gInSc4	text
separately	separatela	k1gFnSc2	separatela
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
:	:	kIx,	:
ARTICLE	ARTICLE	kA	ARTICLE
S	s	k7c7	s
<g/>
:	:	kIx,	:
220	[number]	k4	220
395069	[number]	k4	395069
<	<	kIx(	<
<g/>
74yTE-8pz-13@gated-at.bofh.it	74yTE-8pz-13@gatedt.bofh.it	k1gInSc1	74yTE-8pz-13@gated-at.bofh.it
<g/>
>	>	kIx)	>
article	article	k1gInSc1	article
<	<	kIx(	<
<g/>
server	server	k1gInSc1	server
posílá	posílat	k5eAaImIp3nS	posílat
tělo	tělo	k1gNnSc4	tělo
druhého	druhý	k4xOgMnSc2	druhý
příspěvku	příspěvek	k1gInSc2	příspěvek
<g/>
>	>	kIx)	>
S	s	k7c7	s
<g/>
:	:	kIx,	:
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
:	:	kIx,	:
QUIT	QUIT	kA	QUIT
S	s	k7c7	s
<g/>
:	:	kIx,	:
205	[number]	k4	205
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
problém	problém	k1gInSc1	problém
protokolu	protokol	k1gInSc2	protokol
NNTP	NNTP	kA	NNTP
je	být	k5eAaImIp3nS	být
umožnění	umožnění	k1gNnSc1	umožnění
informovanému	informovaný	k2eAgMnSc3d1	informovaný
klientovi	klient	k1gMnSc3	klient
vložit	vložit	k5eAaPmF	vložit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
články	článek	k1gInPc1	článek
s	s	k7c7	s
falešnou	falešný	k2eAgFnSc7d1	falešná
specifikací	specifikace	k1gFnSc7	specifikace
odesílatele	odesílatel	k1gMnSc2	odesílatel
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
falšování	falšování	k1gNnSc1	falšování
news	newsa	k1gFnPc2	newsa
(	(	kIx(	(
<g/>
news	wsit	k5eNaPmRp2nS	wsit
faking	faking	k1gInSc1	faking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
protokolu	protokol	k1gInSc2	protokol
SMTP	SMTP	kA	SMTP
(	(	kIx(	(
<g/>
Simple	Simple	k1gFnSc1	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
rozšíření	rozšíření	k1gNnSc1	rozšíření
protokolu	protokol	k1gInSc2	protokol
NNTP	NNTP	kA	NNTP
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
příkazů	příkaz	k1gInPc2	příkaz
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
autentizaci	autentizace	k1gFnSc4	autentizace
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
možnost	možnost	k1gFnSc1	možnost
vytváření	vytváření	k1gNnSc2	vytváření
falešných	falešný	k2eAgFnPc2d1	falešná
zpráv	zpráva	k1gFnPc2	zpráva
částečně	částečně	k6eAd1	částečně
omezí	omezit	k5eAaPmIp3nS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
balíkem	balík	k1gInSc7	balík
protokolu	protokol	k1gInSc2	protokol
NNTP	NNTP	kA	NNTP
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
démon	démon	k1gMnSc1	démon
NNTP	NNTP	kA	NNTP
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
původními	původní	k2eAgMnPc7d1	původní
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
Phil	Phil	k1gMnSc1	Phil
Lapsley	Lapslea	k1gFnSc2	Lapslea
a	a	k8xC	a
Stan	stan	k1gInSc1	stan
Barber	Barbra	k1gFnPc2	Barbra
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
původně	původně	k6eAd1	původně
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gMnSc1	jeho
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
detaily	detail	k1gInPc4	detail
RFC	RFC	kA	RFC
977	[number]	k4	977
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
balíky	balík	k1gMnPc7	balík
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
máme	mít	k5eAaImIp1nP	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
mezi	mezi	k7c4	mezi
balíky	balík	k1gInPc4	balík
binárních	binární	k2eAgInPc2d1	binární
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různé	různý	k2eAgInPc1d1	různý
programy	program	k1gInPc1	program
pro	pro	k7c4	pro
instalaci	instalace	k1gFnSc4	instalace
můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
také	také	k9	také
balík	balík	k1gInSc1	balík
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
InterNet	Internet	k1gInSc1	Internet
News	News	k1gInSc1	News
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
INN	INN	kA	INN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
INN	INN	kA	INN
je	být	k5eAaImIp3nS	být
Rich	Rich	k1gMnSc1	Rich
Salz	Salz	k1gMnSc1	Salz
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
balík	balík	k1gInSc1	balík
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jak	jak	k6eAd1	jak
přenos	přenos	k1gInSc1	přenos
NNTP	NNTP	kA	NNTP
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
starší	starý	k2eAgInSc4d2	starší
UUCP-news	UUCPews	k1gInSc4	UUCP-news
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
balík	balík	k1gInSc1	balík
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
navržen	navržen	k2eAgInSc4d1	navržen
pro	pro	k7c4	pro
větší	veliký	k2eAgInPc4d2	veliký
systémové	systémový	k2eAgInPc4d1	systémový
servery	server	k1gInPc4	server
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
NNTP	NNTP	kA	NNTP
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nntpd	nntpd	k6eAd1	nntpd
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
zkompilovat	zkompilovat	k5eAaPmF	zkompilovat
dvěma	dva	k4xCgInPc7	dva
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
očekávaném	očekávaný	k2eAgNnSc6d1	očekávané
zatížení	zatížení	k1gNnSc6	zatížení
systému	systém	k1gInSc3	systém
news	newsa	k1gFnPc2	newsa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
žádných	žádný	k3yNgFnPc2	žádný
neexistujících	existující	k2eNgFnPc2d1	neexistující
již	již	k6eAd1	již
zkompilovaných	zkompilovaný	k2eAgFnPc2d1	zkompilovaná
verzí	verze	k1gFnPc2	verze
se	se	k3xPyFc4	se
veškerá	veškerý	k3xTgFnSc1	veškerý
konfigurace	konfigurace	k1gFnSc1	konfigurace
provádí	provádět	k5eAaImIp3nS	provádět
skrze	skrze	k?	skrze
makro	makro	k1gNnSc4	makro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
common	common	k1gMnSc1	common
<g/>
/	/	kIx~	/
<g/>
conf	conf	k1gMnSc1	conf
<g/>
.	.	kIx.	.
<g/>
h	h	k?	h
Systém	systém	k1gInSc1	systém
nntpd	nntpd	k6eAd1	nntpd
je	být	k5eAaImIp3nS	být
nakonfigurován	nakonfigurován	k2eAgInSc1d1	nakonfigurován
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
spouští	spouštět	k5eAaImIp3nS	spouštět
při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
systému	systém	k1gInSc2	systém
z	z	k7c2	z
rc	rc	k?	rc
<g/>
.	.	kIx.	.
<g/>
inet	inet	k1gInSc1	inet
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
nntpd	nntpd	k6eAd1	nntpd
je	být	k5eAaImIp3nS	být
nakonfigurován	nakonfigurován	k2eAgMnSc1d1	nakonfigurován
jako	jako	k8xS	jako
démon	démon	k1gMnSc1	démon
řízený	řízený	k2eAgInSc4d1	řízený
inetd	inetd	k1gInSc4	inetd
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
NNTP	NNTP	kA	NNTP
<g/>
:	:	kIx,	:
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
zdrojům	zdroj	k1gInPc3	zdroj
NNTP	NNTP	kA	NNTP
je	být	k5eAaImIp3nS	být
řízen	řídit	k5eAaImNgInS	řídit
souborem	soubor	k1gInSc7	soubor
nntp_access	nntp_access	k6eAd1	nntp_access
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
adresáři	adresář	k1gInSc6	adresář
/	/	kIx~	/
<g/>
usr	usr	k?	usr
<g/>
/	/	kIx~	/
<g/>
lib	líbit	k5eAaImRp2nS	líbit
<g/>
/	/	kIx~	/
<g/>
news	news	k1gInSc1	news
<g/>
.	.	kIx.	.
</s>
<s>
Řádky	řádek	k1gInPc1	řádek
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
souboru	soubor	k1gInSc6	soubor
specifikují	specifikovat	k5eAaBmIp3nP	specifikovat
přístupová	přístupový	k2eAgNnPc1d1	přístupové
práva	právo	k1gNnPc1	právo
přidělená	přidělený	k2eAgNnPc1d1	přidělené
vzdáleným	vzdálený	k2eAgMnSc7d1	vzdálený
hostitelům	hostitel	k1gMnPc3	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
řádek	řádek	k1gInSc1	řádek
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgInSc1d1	následující
formát	formát	k1gInSc1	formát
<g/>
:	:	kIx,	:
site	sitat	k5eAaPmIp3nS	sitat
read	read	k1gMnSc1	read
<g/>
|	|	kIx~	|
<g/>
xfer	xfer	k1gMnSc1	xfer
<g/>
|	|	kIx~	|
<g/>
both	both	k1gMnSc1	both
<g/>
|	|	kIx~	|
<g/>
no	no	k9	no
post	post	k1gInSc4	post
<g/>
|	|	kIx~	|
<g/>
no	no	k9	no
[	[	kIx(	[
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
exceptgroup	exceptgroup	k1gInSc1	exceptgroup
<g/>
]	]	kIx)	]
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
klient	klient	k1gMnSc1	klient
připojí	připojit	k5eAaPmIp3nS	připojit
na	na	k7c4	na
NNTP-port	NNTPort	k1gInSc4	NNTP-port
<g/>
,	,	kIx,	,
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
server	server	k1gInSc1	server
nntpd	nntpda	k1gFnPc2	nntpda
pomocí	pomocí	k7c2	pomocí
zpětného	zpětný	k2eAgInSc2d1	zpětný
překladu	překlad	k1gInSc2	překlad
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
získat	získat	k5eAaPmF	získat
doménové	doménový	k2eAgNnSc4d1	doménové
jméno	jméno	k1gNnSc4	jméno
klienta	klient	k1gMnSc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Doménové	doménový	k2eAgNnSc4d1	doménové
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
jsou	být	k5eAaImIp3nP	být
porovnávány	porovnáván	k2eAgInPc1d1	porovnáván
s	s	k7c7	s
polem	pole	k1gNnSc7	pole
site	sit	k1gFnSc2	sit
každého	každý	k3xTgInSc2	každý
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Shoda	shoda	k1gFnSc1	shoda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
částečná	částečný	k2eAgNnPc4d1	částečné
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
úplné	úplný	k2eAgFnSc2d1	úplná
shody	shoda	k1gFnSc2	shoda
ji	on	k3xPp3gFnSc4	on
použije	použít	k5eAaPmIp3nS	použít
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
případě	případ	k1gInSc6	případ
částečné	částečný	k2eAgFnSc2d1	částečná
shody	shoda	k1gFnSc2	shoda
ji	on	k3xPp3gFnSc4	on
použije	použít	k5eAaPmIp3nS	použít
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nenajde	najít	k5eNaPmIp3nS	najít
žádná	žádný	k3yNgFnSc1	žádný
další	další	k2eAgFnSc1d1	další
částečná	částečný	k2eAgFnSc1d1	částečná
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
s	s	k7c7	s
obecnější	obecní	k2eAgFnSc7d2	obecní
specifikací	specifikace	k1gFnSc7	specifikace
adres	adresa	k1gFnPc2	adresa
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
případné	případný	k2eAgFnPc1d1	případná
shody	shoda	k1gFnPc1	shoda
jsou	být	k5eAaImIp3nP	být
pozdějšími	pozdní	k2eAgInPc7d2	pozdější
přesnějšími	přesný	k2eAgInPc7d2	přesnější
záznamy	záznam	k1gInPc7	záznam
potlačeny	potlačen	k2eAgInPc4d1	potlačen
<g/>
.	.	kIx.	.
</s>
<s>
Příručka	příručka	k1gFnSc1	příručka
správce	správce	k1gMnSc2	správce
sítě	síť	k1gFnSc2	síť
Linux	Linux	kA	Linux
<g/>
;	;	kIx,	;
Pavel	Pavel	k1gMnSc1	Pavel
Janík	Janík	k1gMnSc1	Janík
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
;	;	kIx,	;
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Press	Pressa	k1gFnPc2	Pressa
Linux	Linux	kA	Linux
-	-	kIx~	-
praktické	praktický	k2eAgInPc4d1	praktický
návody	návod	k1gInPc4	návod
<g/>
;	;	kIx,	;
Jiří	Jiří	k1gMnSc1	Jiří
Veselský	Veselský	k1gMnSc1	Veselský
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
;	;	kIx,	;
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
</s>
