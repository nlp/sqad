<s>
Korjačtina	Korjačtina	k1gFnSc1	Korjačtina
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc4	jazyk
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
čukotsko-kamčatské	čukotskoamčatský	k2eAgFnSc2d1	čukotsko-kamčatský
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
Korjaci	Korjace	k1gFnSc4	Korjace
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
národ	národ	k1gInSc4	národ
sídlící	sídlící	k2eAgInSc4d1	sídlící
na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
Kamčatském	kamčatský	k2eAgInSc6d1	kamčatský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
uvedlo	uvést	k5eAaPmAgNnS	uvést
znalost	znalost	k1gFnSc4	znalost
korjackého	korjacký	k2eAgInSc2d1	korjacký
jazyka	jazyk	k1gInSc2	jazyk
3	[number]	k4	3
019	[number]	k4	019
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
34,5	[number]	k4	34,5
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
Korjaků	Korjak	k1gMnPc2	Korjak
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Procento	procento	k1gNnSc1	procento
Korjaků	Korjak	k1gMnPc2	Korjak
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jazyk	jazyk	k1gInSc4	jazyk
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
ovládají	ovládat	k5eAaImIp3nP	ovládat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
patrně	patrně	k6eAd1	patrně
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
jen	jen	k9	jen
5,4	[number]	k4	5,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
morfologické	morfologický	k2eAgFnSc2d1	morfologická
typologie	typologie	k1gFnSc2	typologie
se	se	k3xPyFc4	se
korjačtina	korjačtina	k1gFnSc1	korjačtina
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
aglutinačním	aglutinační	k2eAgMnPc3d1	aglutinační
jazykům	jazyk	k1gMnPc3	jazyk
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
sufixů	sufix	k1gInPc2	sufix
<g/>
,	,	kIx,	,
prefixů	prefix	k1gInPc2	prefix
i	i	k8xC	i
cirkumfixů	cirkumfix	k1gInPc2	cirkumfix
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
afixů	afix	k1gInPc2	afix
složených	složený	k2eAgInPc2d1	složený
z	z	k7c2	z
prefixové	prefixový	k2eAgFnSc2d1	prefixová
a	a	k8xC	a
sufixové	sufixový	k2eAgFnSc2d1	sufixový
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
příbuzné	příbuzný	k2eAgFnSc3d1	příbuzná
čukotštině	čukotština	k1gFnSc3	čukotština
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
navíc	navíc	k6eAd1	navíc
opozice	opozice	k1gFnSc1	opozice
dvojného	dvojný	k2eAgNnSc2d1	dvojné
a	a	k8xC	a
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
čukotský	čukotský	k2eAgInSc1d1	čukotský
příznak	příznak	k1gInSc1	příznak
plurálu	plurál	k1gInSc2	plurál
-t	-t	k?	-t
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
korjackému	korjacký	k2eAgInSc3d1	korjacký
duálu	duál	k1gInSc3	duál
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
korjacký	korjacký	k2eAgInSc1d1	korjacký
plurál	plurál	k1gInSc1	plurál
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
čukotštině	čukotština	k1gFnSc6	čukotština
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
<g/>
)	)	kIx)	)
a	a	k8xC	a
specifické	specifický	k2eAgFnPc4d1	specifická
pádové	pádový	k2eAgFnPc4d1	pádová
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
větná	větný	k2eAgFnSc1d1	větná
konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
ergativní	ergativní	k2eAgFnSc1d1	ergativní
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
ergativních	ergativní	k2eAgInPc6d1	ergativní
jazycích	jazyk	k1gInPc6	jazyk
existují	existovat	k5eAaImIp3nP	existovat
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
časování	časování	k1gNnSc2	časování
<g/>
:	:	kIx,	:
podmětné	podmětný	k2eAgInPc4d1	podmětný
a	a	k8xC	a
podmětně-předmětné	podmětněředmětný	k2eAgInPc4d1	podmětně-předmětný
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
tvary	tvar	k1gInPc1	tvar
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
shodu	shoda	k1gFnSc4	shoda
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
podmětu	podmět	k1gInSc2	podmět
i	i	k8xC	i
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
mají	mít	k5eAaImIp3nP	mít
čas	čas	k1gInSc4	čas
přítomno-minulý	přítomnoinulý	k2eAgInSc4d1	přítomno-minulý
<g/>
,	,	kIx,	,
předminulý	předminulý	k2eAgInSc4d1	předminulý
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
II	II	kA	II
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc1d1	budoucí
I	i	k9	i
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc1d1	budoucí
II	II	kA	II
<g/>
;	;	kIx,	;
způsob	způsob	k1gInSc4	způsob
oznamovací	oznamovací	k2eAgInSc4d1	oznamovací
<g/>
,	,	kIx,	,
rozkazovací	rozkazovací	k2eAgInSc4d1	rozkazovací
a	a	k8xC	a
subjunktiv	subjunktiv	k1gInSc4	subjunktiv
<g/>
.	.	kIx.	.
</s>
<s>
Větný	větný	k2eAgInSc1d1	větný
slovosled	slovosled	k1gInSc1	slovosled
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
volný	volný	k2eAgInSc1d1	volný
<g/>
.	.	kIx.	.
</s>
<s>
Korjačtina	Korjačtina	k1gFnSc1	Korjačtina
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
čavčuvenský	čavčuvenský	k2eAgMnSc1d1	čavčuvenský
<g/>
,	,	kIx,	,
apukinský	apukinský	k2eAgMnSc1d1	apukinský
<g/>
,	,	kIx,	,
itkanský	itkanský	k2eAgMnSc1d1	itkanský
<g/>
,	,	kIx,	,
kamenský	kamenský	k2eAgMnSc1d1	kamenský
<g/>
,	,	kIx,	,
parenský	parenský	k2eAgMnSc1d1	parenský
<g/>
,	,	kIx,	,
karaginský	karaginský	k2eAgMnSc1d1	karaginský
nebo	nebo	k8xC	nebo
palanský	palanský	k2eAgMnSc1d1	palanský
<g/>
;	;	kIx,	;
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dialekt	dialekt	k1gInSc4	dialekt
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
také	také	k9	také
aljutorština	aljutorština	k1gFnSc1	aljutorština
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zpravidla	zpravidla	k6eAd1	zpravidla
chápaná	chápaný	k2eAgFnSc1d1	chápaná
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
přiřazovány	přiřazován	k2eAgFnPc1d1	přiřazován
i	i	k8xC	i
některé	některý	k3yIgInPc1	některý
dialekty	dialekt	k1gInPc1	dialekt
původně	původně	k6eAd1	původně
chápané	chápaný	k2eAgInPc1d1	chápaný
jako	jako	k8xS	jako
korjacké	korjacký	k2eAgInPc1d1	korjacký
(	(	kIx(	(
<g/>
palanský	palanský	k2eAgInSc1d1	palanský
<g/>
,	,	kIx,	,
karaginský	karaginský	k2eAgInSc1d1	karaginský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čukotsko-korjacké	čukotskoorjacký	k2eAgFnSc2d1	čukotsko-korjacký
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
tzv.	tzv.	kA	tzv.
j-dialekty	jialekt	k1gInPc1	j-dialekt
(	(	kIx(	(
<g/>
čavčuvenský	čavčuvenský	k2eAgMnSc1d1	čavčuvenský
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
t-dialekty	tialekt	k1gInPc1	t-dialekt
(	(	kIx(	(
<g/>
aljutorské	aljutorský	k2eAgFnSc3d1	aljutorský
<g/>
)	)	kIx)	)
a	a	k8xC	a
r-dialekty	rialekt	k1gInPc4	r-dialekt
(	(	kIx(	(
<g/>
čukotské	čukotský	k2eAgInPc4d1	čukotský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
podle	podle	k7c2	podle
korespondujících	korespondující	k2eAgFnPc2d1	korespondující
hlásek	hláska	k1gFnPc2	hláska
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
dialektech	dialekt	k1gInPc6	dialekt
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
např.	např.	kA	např.
korjacké	korjacké	k2eAgMnSc4d1	korjacké
jin	jin	k?	jin
<g/>
'	'	kIx"	'
<g/>
e-k	e	k?	e-k
<g/>
,	,	kIx,	,
aljutorské	aljutorský	k2eAgFnSc6d1	aljutorský
tin	tin	k?	tin
<g/>
'	'	kIx"	'
<g/>
a-k	a	k?	a-k
a	a	k8xC	a
čukotské	čukotský	k2eAgInPc1d1	čukotský
rin	rin	k?	rin
<g/>
'	'	kIx"	'
<g/>
e-k	e	k?	e-k
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
létat	létat	k5eAaImF	létat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
korjacká	korjacký	k2eAgFnSc1d1	korjacký
abeceda	abeceda	k1gFnSc1	abeceda
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
latinky	latinka	k1gFnSc2	latinka
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
pro	pro	k7c4	pro
čavčuvenský	čavčuvenský	k2eAgInSc4d1	čavčuvenský
dialekt	dialekt	k1gInSc4	dialekt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
abeceda	abeceda	k1gFnSc1	abeceda
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
azbuky	azbuka	k1gFnSc2	azbuka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dosud	dosud	k6eAd1	dosud
<g/>
:	:	kIx,	:
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
korjačtina	korjačtina	k1gFnSc1	korjačtina
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
stupni	stupeň	k1gInSc6	stupeň
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
na	na	k7c6	na
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
městě	město	k1gNnSc6	město
Palaně	Palaň	k1gFnSc2	Palaň
<g/>
,	,	kIx,	,
na	na	k7c6	na
Institutu	institut	k1gInSc6	institut
národů	národ	k1gInPc2	národ
Severu	sever	k1gInSc2	sever
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
univerzity	univerzita	k1gFnSc2	univerzita
A.	A.	kA	A.
I.	I.	kA	I.
Gercena	Gercen	k2eAgFnSc1d1	Gercena
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korjackém	korjacký	k2eAgInSc6d1	korjacký
jazyce	jazyk	k1gInSc6	jazyk
existuje	existovat	k5eAaImIp3nS	existovat
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
rádiové	rádiový	k2eAgNnSc4d1	rádiové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
(	(	kIx(	(
<g/>
z	z	k7c2	z
Palany	Palana	k1gFnSc2	Palana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
v	v	k7c6	v
korjačtině	korjačtina	k1gFnSc6	korjačtina
občas	občas	k6eAd1	občas
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
v	v	k7c6	v
regionálním	regionální	k2eAgInSc6d1	regionální
deníku	deník	k1gInSc6	deník
Narodovlastije	Narodovlastije	k1gFnSc2	Narodovlastije
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
médiích	médium	k1gNnPc6	médium
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
čavčuvenský	čavčuvenský	k2eAgInSc1d1	čavčuvenský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
všem	všecek	k3xTgMnPc3	všecek
mluvčím	mluvčí	k1gMnPc3	mluvčí
korjačtiny	korjačtina	k1gFnSc2	korjačtina
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
<g/>
.	.	kIx.	.
</s>
