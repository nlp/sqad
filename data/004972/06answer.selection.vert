<s>
Korjačtina	Korjačtina	k1gFnSc1	Korjačtina
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc4	jazyk
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
čukotsko-kamčatské	čukotskoamčatský	k2eAgFnSc2d1	čukotsko-kamčatský
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
Korjaci	Korjace	k1gFnSc4	Korjace
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
národ	národ	k1gInSc4	národ
sídlící	sídlící	k2eAgInSc4d1	sídlící
na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
Kamčatském	kamčatský	k2eAgInSc6d1	kamčatský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
