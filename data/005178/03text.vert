<s>
Laurasie	Laurasie	k1gFnPc4	Laurasie
byl	být	k5eAaImAgInS	být
severnější	severní	k2eAgInSc1d2	severnější
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
druhohorách	druhohory	k1gFnPc6	druhohory
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
superkontinent	superkontinent	k1gInSc1	superkontinent
Pangea	Pangea	k1gFnSc1	Pangea
<g/>
.	.	kIx.	.
</s>
<s>
Zformovala	zformovat	k5eAaPmAgFnS	zformovat
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Pangey	Pangea	k1gFnSc2	Pangea
před	před	k7c7	před
300	[number]	k4	300
až	až	k8xS	až
250	[number]	k4	250
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
Pangei	Pangea	k1gFnSc3	Pangea
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
připojila	připojit	k5eAaPmAgFnS	připojit
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
200	[number]	k4	200
až	až	k8xS	až
150	[number]	k4	150
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Laurasie	Laurasie	k1gFnSc1	Laurasie
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
jižnější	jižní	k2eAgFnSc2d2	jižnější
Gondwany	Gondwana	k1gFnSc2	Gondwana
a	a	k8xC	a
Pangea	Pangea	k1gFnSc1	Pangea
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Laurasií	Laurasie	k1gFnSc7	Laurasie
a	a	k8xC	a
Gondwanou	Gondwana	k1gFnSc7	Gondwana
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
odděloval	oddělovat	k5eAaImAgInS	oddělovat
oceán	oceán	k1gInSc1	oceán
Tethys	Tethys	k1gInSc1	Tethys
<g/>
.	.	kIx.	.
</s>
<s>
Laurasii	Laurasie	k1gFnSc3	Laurasie
daly	dát	k5eAaPmAgInP	dát
jméno	jméno	k1gNnSc4	jméno
dvě	dva	k4xCgNnPc1	dva
velká	velký	k2eAgNnPc1d1	velké
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
ležela	ležet	k5eAaImAgFnS	ležet
<g/>
:	:	kIx,	:
Laurentie	Laurentie	k1gFnSc1	Laurentie
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
včetně	včetně	k7c2	včetně
Floridy	Florida	k1gFnSc2	Florida
<g/>
)	)	kIx)	)
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
samozřejmě	samozřejmě	k6eAd1	samozřejmě
leželo	ležet	k5eAaImAgNnS	ležet
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
včetně	včetně	k7c2	včetně
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
a	a	k8xC	a
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
však	však	k9	však
chyběla	chybět	k5eAaImAgFnS	chybět
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oblasti	oblast	k1gFnPc1	oblast
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
asi	asi	k9	asi
před	před	k7c7	před
100	[number]	k4	100
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
se	se	k3xPyFc4	se
Laurasie	Laurasie	k1gFnSc1	Laurasie
téměř	téměř	k6eAd1	téměř
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
Eurasii	Eurasie	k1gFnSc4	Eurasie
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Laurasii	Laurasie	k1gFnSc6	Laurasie
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
chybně	chybně	k6eAd1	chybně
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
Pangey	Pangea	k1gFnSc2	Pangea
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
však	však	k9	však
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
Laurasii	Laurasie	k1gFnSc4	Laurasie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Laurussii	Laurussie	k1gFnSc4	Laurussie
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Eurameriku	Euramerika	k1gFnSc4	Euramerika
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc6	ten
do	do	k7c2	do
Laurasie	Laurasie	k1gFnSc2	Laurasie
chyběly	chybět	k5eAaImAgFnP	chybět
podstatné	podstatný	k2eAgFnPc1d1	podstatná
části	část	k1gFnPc1	část
budoucí	budoucí	k2eAgFnSc2d1	budoucí
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Sibiř	Sibiř	k1gFnSc1	Sibiř
a	a	k8xC	a
Kazachstánie	Kazachstánie	k1gFnSc1	Kazachstánie
<g/>
.	.	kIx.	.
</s>
