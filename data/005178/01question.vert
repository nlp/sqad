<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
severnější	severní	k2eAgInSc1d2	severnější
prakontinent	prakontinent	k1gInSc1	prakontinent
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
superkontinentu	superkontinent	k1gInSc2	superkontinent
Pangea	Pangea	k1gFnSc1	Pangea
<g/>
?	?	kIx.	?
</s>
