<s>
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Boccaccio	Boccaccio	k1gMnSc1	Boccaccio
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1313	[number]	k4	1313
v	v	k7c6	v
Toskánsku	Toskánsko	k1gNnSc6	Toskánsko
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
Certaldu	Certald	k1gInSc6	Certald
<g/>
)	)	kIx)	)
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1375	[number]	k4	1375
v	v	k7c6	v
Certaldu	Certald	k1gInSc6	Certald
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
renesanční	renesanční	k2eAgMnSc1d1	renesanční
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
novelista	novelista	k1gMnSc1	novelista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
italské	italský	k2eAgFnSc2d1	italská
umělecké	umělecký	k2eAgFnSc2d1	umělecká
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
za	za	k7c7	za
obchodem	obchod	k1gInSc7	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
uměleckému	umělecký	k2eAgNnSc3d1	umělecké
nadání	nadání	k1gNnSc3	nadání
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
neapolského	neapolský	k2eAgMnSc2d1	neapolský
krále	král	k1gMnSc2	král
Roberta	Robert	k1gMnSc2	Robert
I.	I.	kA	I.
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
sídlo	sídlo	k1gNnSc1	sídlo
bylo	být	k5eAaImAgNnS	být
centrem	centr	k1gInSc7	centr
raného	raný	k2eAgInSc2d1	raný
humanismu	humanismus	k1gInSc2	humanismus
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
stala	stát	k5eAaPmAgFnS	stát
ideálem	ideál	k1gInSc7	ideál
královnina	královnin	k2eAgFnSc1d1	Královnina
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Aquinská	Aquinský	k2eAgFnSc1d1	Aquinský
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1342	[number]	k4	1342
opěvoval	opěvovat	k5eAaImAgMnS	opěvovat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Elegia	Elegius	k1gMnSc2	Elegius
di	di	k?	di
Madonna	Madonn	k1gMnSc2	Madonn
Fiammetta	Fiammett	k1gMnSc2	Fiammett
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
diplomatické	diplomatický	k2eAgFnPc1d1	diplomatická
mise	mise	k1gFnPc1	mise
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1344	[number]	k4	1344
napsal	napsat	k5eAaPmAgInS	napsat
básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
Fiesolské	Fiesolský	k2eAgFnSc2d1	Fiesolský
nymfy	nymfa	k1gFnSc2	nymfa
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Ninfale	Ninfala	k1gFnSc3	Ninfala
Fiesolano	Fiesolana	k1gFnSc5	Fiesolana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přebásněno	přebásnit	k5eAaPmNgNnS	přebásnit
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
J.	J.	kA	J.
Hiršalem	Hiršal	k1gMnSc7	Hiršal
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
napsané	napsaný	k2eAgNnSc1d1	napsané
v	v	k7c6	v
oktávách	oktáva	k1gFnPc6	oktáva
a	a	k8xC	a
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgInSc1	první
pastýřský	pastýřský	k2eAgInSc1d1	pastýřský
epos	epos	k1gInSc1	epos
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
cyklus	cyklus	k1gInSc1	cyklus
Dekameron	Dekameron	k1gInSc1	Dekameron
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
Boccacciovo	Boccacciův	k2eAgNnSc4d1	Boccacciův
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1348	[number]	k4	1348
až	až	k9	až
1353	[number]	k4	1353
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
sta	sto	k4xCgNnPc4	sto
novel	novela	k1gFnPc2	novela
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
erotickým	erotický	k2eAgNnSc7d1	erotické
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stejném	stejné	k1gNnSc6	stejné
poměru	poměr	k1gInSc2	poměr
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
deseti	deset	k4xCc2	deset
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
deka	deka	k1gNnSc2	deka
=	=	kIx~	=
deset	deset	k4xCc4	deset
<g/>
,	,	kIx,	,
hemerá	hemerat	k5eAaPmIp3nS	hemerat
=	=	kIx~	=
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
také	také	k9	také
název	název	k1gInSc1	název
cyklu	cyklus	k1gInSc2	cyklus
<g/>
)	)	kIx)	)
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
je	být	k5eAaImIp3nS	být
deset	deset	k4xCc1	deset
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
7	[number]	k4	7
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
3	[number]	k4	3
muži	muž	k1gMnPc7	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
utekli	utéct	k5eAaPmAgMnP	utéct
z	z	k7c2	z
města	město	k1gNnSc2	město
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zachránili	zachránit	k5eAaPmAgMnP	zachránit
před	před	k7c7	před
morem	mor	k1gInSc7	mor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
dílo	dílo	k1gNnSc4	dílo
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
a	a	k8xC	a
na	na	k7c6	na
Indexu	index	k1gInSc6	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
byl	být	k5eAaImAgInS	být
Boccaccio	Boccaccio	k6eAd1	Boccaccio
uváděn	uvádět	k5eAaImNgInS	uvádět
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
vyškrtnut	vyškrtnout	k5eAaPmNgMnS	vyškrtnout
z	z	k7c2	z
Indexu	index	k1gInSc2	index
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgMnSc1d1	slavný
básník	básník	k1gMnSc1	básník
pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1375	[number]	k4	1375
(	(	kIx(	(
<g/>
62	[number]	k4	62
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
'	'	kIx"	'
<g/>
Chiesa	Chiesa	k1gFnSc1	Chiesa
dei	dei	k?	dei
Santi	Sanť	k1gFnSc2	Sanť
Jacopo	Jacopa	k1gFnSc5	Jacopa
e	e	k0	e
Filippo	Filippa	k1gFnSc5	Filippa
<g/>
'	'	kIx"	'
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Certaldu	Certald	k1gInSc6	Certald
<g/>
.	.	kIx.	.
</s>
<s>
Neapolská	neapolský	k2eAgFnSc1d1	neapolská
fáze	fáze	k1gFnSc1	fáze
La	la	k1gNnSc2	la
caccia	caccia	k1gFnSc1	caccia
di	di	k?	di
Diana	Diana	k1gFnSc1	Diana
(	(	kIx(	(
<g/>
1334	[number]	k4	1334
<g/>
)	)	kIx)	)
-	-	kIx~	-
krátký	krátký	k2eAgInSc4d1	krátký
epos	epos	k1gInSc4	epos
o	o	k7c6	o
18	[number]	k4	18
zpěvech	zpěv	k1gInPc6	zpěv
Il	Il	k1gFnSc2	Il
Filostrato	Filostrat	k2eAgNnSc1d1	Filostrato
(	(	kIx(	(
<g/>
1335	[number]	k4	1335
<g/>
)	)	kIx)	)
-	-	kIx~	-
epos	epos	k1gInSc1	epos
ve	v	k7c6	v
stancích	stance	k1gFnPc6	stance
Il	Il	k1gFnSc2	Il
Filocolo	Filocola	k1gFnSc5	Filocola
(	(	kIx(	(
<g/>
1336	[number]	k4	1336
<g/>
-	-	kIx~	-
<g/>
1339	[number]	k4	1339
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc4	román
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
Teseida	Teseida	k1gFnSc1	Teseida
(	(	kIx(	(
<g/>
1340	[number]	k4	1340
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1341	[number]	k4	1341
<g/>
)	)	kIx)	)
-	-	kIx~	-
epos	epos	k1gInSc1	epos
ve	v	k7c6	v
stancích	stance	k1gFnPc6	stance
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
Rime	Rime	k1gFnSc1	Rime
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Boccaccio	Boccaccio	k6eAd1	Boccaccio
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
básně	báseň	k1gFnSc2	báseň
sám	sám	k3xTgInSc4	sám
nezahrnul	zahrnout	k5eNaPmAgMnS	zahrnout
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
díla	dílo	k1gNnSc2	dílo
Léta	léto	k1gNnSc2	léto
1340-1350	[number]	k4	1340-1350
Ninfale	Ninfala	k1gFnSc6	Ninfala
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Ameto	Ameto	k1gNnSc4	Ameto
(	(	kIx(	(
<g/>
1341	[number]	k4	1341
<g/>
-	-	kIx~	-
<g/>
1342	[number]	k4	1342
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
pastýřský	pastýřský	k2eAgInSc1d1	pastýřský
epos	epos	k1gInSc1	epos
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
a	a	k8xC	a
próze	próza	k1gFnSc6	próza
Milostná	milostný	k2eAgFnSc1d1	milostná
vidina	vidina	k1gFnSc1	vidina
-	-	kIx~	-
epos	epos	k1gInSc1	epos
v	v	k7c6	v
tercínách	tercína	k1gFnPc6	tercína
<g/>
,	,	kIx,	,
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
Alighierovu	Alighierův	k2eAgFnSc4d1	Alighierův
Božskou	božský	k2eAgFnSc4d1	božská
komedii	komedie	k1gFnSc4	komedie
Fiesolské	Fiesolský	k2eAgFnSc2d1	Fiesolský
nymfy	nymfa	k1gFnSc2	nymfa
(	(	kIx(	(
<g/>
Ninfale	Ninfal	k1gMnSc5	Ninfal
Fiesolano	Fiesolana	k1gFnSc5	Fiesolana
<g/>
,	,	kIx,	,
1344	[number]	k4	1344
<g/>
-	-	kIx~	-
<g/>
1346	[number]	k4	1346
<g/>
)	)	kIx)	)
Elegie	elegie	k1gFnSc1	elegie
o	o	k7c4	o
paní	paní	k1gFnSc4	paní
Fiamettě	Fiametť	k1gFnSc2	Fiametť
(	(	kIx(	(
<g/>
Elegia	Elegius	k1gMnSc2	Elegius
di	di	k?	di
madonna	madonn	k1gMnSc2	madonn
Fiammetta	Fiammett	k1gMnSc2	Fiammett
<g/>
,	,	kIx,	,
1342	[number]	k4	1342
<g/>
)	)	kIx)	)
-	-	kIx~	-
román	román	k1gInSc4	román
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
dílo	dílo	k1gNnSc1	dílo
Dekameron	Dekameron	k1gInSc1	Dekameron
(	(	kIx(	(
<g/>
Il	Il	k1gMnSc1	Il
Decameron	Decameron	k1gMnSc1	Decameron
<g/>
,	,	kIx,	,
1348	[number]	k4	1348
–	–	k?	–
1353	[number]	k4	1353
<g/>
)	)	kIx)	)
-	-	kIx~	-
cyklus	cyklus	k1gInSc1	cyklus
100	[number]	k4	100
novel	novela	k1gFnPc2	novela
Pozdní	pozdní	k2eAgNnSc4d1	pozdní
dílo	dílo	k1gNnSc4	dílo
Život	život	k1gInSc4	život
Dantův	Dantův	k2eAgInSc4d1	Dantův
(	(	kIx(	(
<g/>
1358	[number]	k4	1358
–	–	k?	–
1363	[number]	k4	1363
<g/>
)	)	kIx)	)
O	o	k7c6	o
příbězích	příběh	k1gInPc6	příběh
slavných	slavný	k2eAgMnPc2d1	slavný
mužů	muž	k1gMnPc2	muž
–	–	k?	–
latinsky	latinsky	k6eAd1	latinsky
(	(	kIx(	(
<g/>
1355	[number]	k4	1355
–	–	k?	–
1374	[number]	k4	1374
<g/>
)	)	kIx)	)
</s>
