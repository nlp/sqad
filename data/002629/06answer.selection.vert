<s>
Ligueil	Ligueil	k1gInSc1	Ligueil
je	být	k5eAaImIp3nS	být
sýr	sýr	k1gInSc4	sýr
typu	typ	k1gInSc2	typ
camembert	camembert	k1gInSc1	camembert
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Poitiers	Poitiers	k1gInSc1	Poitiers
z	z	k7c2	z
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
