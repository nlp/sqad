<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
One	One	k1gMnSc1	One
world	world	k1gMnSc1	world
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
festival	festival	k1gInSc4	festival
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
věnovaných	věnovaný	k2eAgInPc2d1	věnovaný
problematice	problematika	k1gFnSc3	problematika
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
aktivit	aktivita	k1gFnPc2	aktivita
obecně	obecně	k6eAd1	obecně
prospěšné	prospěšný	k2eAgFnSc2d1	prospěšná
společnosti	společnost	k1gFnSc2	společnost
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
festivalům	festival	k1gInPc3	festival
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
získal	získat	k5eAaPmAgMnS	získat
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
(	(	kIx(	(
<g/>
Honourable	Honourable	k1gFnSc1	Honourable
Mention	Mention	k1gInSc1	Mention
for	forum	k1gNnPc2	forum
Peace	Peace	k1gFnSc2	Peace
Education	Education	k1gInSc1	Education
<g/>
)	)	kIx)	)
UNESCO	Unesco	k1gNnSc1	Unesco
za	za	k7c4	za
výchovu	výchova	k1gFnSc4	výchova
k	k	k7c3	k
lidským	lidský	k2eAgNnPc3d1	lidské
právům	právo	k1gNnPc3	právo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
festivalu	festival	k1gInSc2	festival
je	být	k5eAaImIp3nS	být
udělována	udělován	k2eAgFnSc1d1	udělována
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
lidskoprávní	lidskoprávní	k2eAgFnSc1d1	lidskoprávní
cena	cena	k1gFnSc1	cena
Homo	Homo	k6eAd1	Homo
Homini	Homin	k2eAgMnPc1d1	Homin
určená	určený	k2eAgNnPc1d1	určené
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
o	o	k7c6	o
prosazování	prosazování	k1gNnSc6	prosazování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
nenásilného	násilný	k2eNgNnSc2d1	nenásilné
řešení	řešení	k1gNnSc2	řešení
politických	politický	k2eAgInPc2d1	politický
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
veřejná	veřejný	k2eAgFnSc1d1	veřejná
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgFnSc7d1	každoroční
přehlídkou	přehlídka	k1gFnSc7	přehlídka
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
(	(	kIx(	(
<g/>
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
(	(	kIx(	(
<g/>
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
)	)	kIx)	)
školách	škola	k1gFnPc6	škola
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc1d1	vybraný
filmy	film	k1gInPc1	film
z	z	k7c2	z
minulých	minulý	k2eAgInPc2d1	minulý
ročníku	ročník	k1gInSc2	ročník
jsou	být	k5eAaImIp3nP	být
zdarma	zdarma	k6eAd1	zdarma
dostupné	dostupný	k2eAgInPc1d1	dostupný
k	k	k7c3	k
nekomerčnímu	komerční	k2eNgNnSc3d1	nekomerční
promítaní	promítaný	k2eAgMnPc1d1	promítaný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
"	"	kIx"	"
<g/>
PROMÍTEJ	promítat	k5eAaImRp2nS	promítat
I	i	k9	i
TY	ty	k3xPp2nSc1	ty
<g/>
"	"	kIx"	"
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
==	==	k?	==
Držitelé	držitel	k1gMnPc1	držitel
ceny	cena	k1gFnSc2	cena
Homo	Homo	k6eAd1	Homo
Homini	Homin	k2eAgMnPc1d1	Homin
==	==	k?	==
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Pham	Pham	k1gMnSc1	Pham
Doan	Doan	k1gMnSc1	Doan
Trang	Trang	k1gMnSc1	Trang
(	(	kIx(	(
<g/>
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Výbor	výbor	k1gInSc1	výbor
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
mučení	mučení	k1gNnSc3	mučení
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
jedenáct	jedenáct	k4xCc1	jedenáct
disidentů	disident	k1gMnPc2	disident
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vězňové	vězeň	k1gMnPc1	vězeň
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
Černého	černé	k1gNnSc2	černé
jara	jaro	k1gNnSc2	jaro
(	(	kIx(	(
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Su	Su	k?	Su
<g/>
'	'	kIx"	'
<g/>
ád	ád	k?	ád
Nawfal	Nawfal	k1gFnSc1	Nawfal
(	(	kIx(	(
<g/>
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Sapijat	Sapijat	k1gInSc1	Sapijat
Magomedovová	Magomedovová	k1gFnSc1	Magomedovová
(	(	kIx(	(
<g/>
Dagestán	Dagestán	k1gInSc1	Dagestán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Intigam	Intigam	k1gInSc1	Intigam
Alijev	Alijev	k1gFnSc2	Alijev
(	(	kIx(	(
<g/>
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Doctors	Doctors	k1gInSc1	Doctors
Coordinate	Coordinat	k1gInSc5	Coordinat
of	of	k?	of
Damascus	Damascus	k1gInSc1	Damascus
(	(	kIx(	(
<g/>
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Ayimjan	Ayimjan	k1gInSc1	Ayimjan
Askarov	Askarov	k1gInSc1	Askarov
(	(	kIx(	(
<g/>
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Majid	Majid	k1gInSc4	Majid
Tavakoli	Tavakole	k1gFnSc4	Tavakole
<g/>
,	,	kIx,	,
Abdollah	Abdollah	k1gMnSc1	Abdollah
Momeni	Momen	k2eAgMnPc1d1	Momen
a	a	k8xC	a
íránské	íránský	k2eAgFnPc1d1	íránská
studentské	studentská	k1gFnSc2	studentská
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c4	v
rozpětí	rozpětí	k1gNnSc4	rozpětí
dvou	dva	k4xCgFnPc2	dva
generací	generace	k1gFnPc2	generace
(	(	kIx(	(
<g/>
Írán	Írán	k1gInSc1	Írán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Liou	Lious	k1gInSc2	Lious
Siao-po	Siaoa	k1gFnSc5	Siao-pa
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Su	Su	k?	Su
Su	Su	k?	Su
Nway	Nwaa	k1gFnSc2	Nwaa
<g/>
,	,	kIx,	,
Phyu	Phy	k2eAgFnSc4d1	Phy
Phyu	Phya	k1gFnSc4	Phya
Thin	Thin	k1gMnSc1	Thin
<g/>
,	,	kIx,	,
Nilar	Nilar	k1gMnSc1	Nilar
Thein	thein	k1gInSc1	thein
(	(	kIx(	(
<g/>
Barma	Barma	k1gFnSc1	Barma
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Světlana	Světlana	k1gFnSc1	Světlana
Gannuškinová	Gannuškinová	k1gFnSc1	Gannuškinová
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Ales	Ales	k1gInSc1	Ales
Bjaljacki	Bjaljack	k1gFnSc2	Bjaljack
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
Viasna	Viasn	k1gInSc2	Viasn
(	(	kIx(	(
<g/>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Gheorghe	Gheorgh	k1gInSc2	Gheorgh
Briceag	Briceag	k1gInSc1	Briceag
(	(	kIx(	(
<g/>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Nataša	Nataša	k1gFnSc1	Nataša
Kandić	Kandić	k1gFnSc1	Kandić
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
humanitární	humanitární	k2eAgNnSc4d1	humanitární
právo	právo	k1gNnSc4	právo
(	(	kIx(	(
<g/>
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Thich	Thich	k1gMnSc1	Thich
Huyen	Huyna	k1gFnPc2	Huyna
Quang	Quang	k1gMnSc1	Quang
<g/>
,	,	kIx,	,
Thich	Thich	k1gMnSc1	Thich
Quang	Quang	k1gInSc1	Quang
Do	do	k7c2	do
a	a	k8xC	a
Nguyen	Nguyen	k2eAgInSc1d1	Nguyen
Van	van	k1gInSc1	van
Ly	Ly	k1gFnSc1	Ly
(	(	kIx(	(
<g/>
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Zackie	Zackie	k1gFnSc1	Zackie
Achmat	Achmat	k1gInSc1	Achmat
(	(	kIx(	(
<g/>
JAR	jaro	k1gNnPc2	jaro
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Min	min	kA	min
Ko	Ko	k1gMnSc1	Ko
Nain	Nain	k1gMnSc1	Nain
(	(	kIx(	(
<g/>
Barma	Barma	k1gFnSc1	Barma
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Oswaldo	Oswaldo	k1gNnSc1	Oswaldo
Payá	Payá	k1gFnSc1	Payá
Sardiňas	Sardiňas	k1gMnSc1	Sardiňas
(	(	kIx(	(
<g/>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Ibrahim	Ibrahim	k1gInSc1	Ibrahim
Rugova	Rugov	k1gInSc2	Rugov
(	(	kIx(	(
<g/>
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Szetó	Szetó	k1gFnSc1	Szetó
Wah	Wah	k1gFnSc1	Wah
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Sergej	Sergej	k1gMnSc1	Sergej
Kovaljov	Kovaljov	k1gInSc1	Kovaljov
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Témata	téma	k1gNnPc1	téma
a	a	k8xC	a
termíny	termín	k1gInPc1	termín
konání	konání	k1gNnSc2	konání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ročníků	ročník	k1gInPc2	ročník
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Prameny	pramen	k1gInPc1	pramen
===	===	k?	===
</s>
</p>
<p>
<s>
cena	cena	k1gFnSc1	cena
homo	homo	k6eAd1	homo
homini	homin	k2eAgMnPc1d1	homin
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Držitelé	držitel	k1gMnPc1	držitel
ceny	cena	k1gFnSc2	cena
Homo	Homo	k6eAd1	Homo
Homini	Homin	k2eAgMnPc1d1	Homin
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc1d1	vybraný
filmy	film	k1gInPc1	film
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
</s>
</p>
