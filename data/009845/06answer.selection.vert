<s>
Jiří	Jiří	k1gMnSc1	Jiří
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1542	[number]	k4	1542
<g/>
–	–	k?	–
<g/>
1543	[number]	k4	1543
účastnil	účastnit	k5eAaImAgInS	účastnit
válek	válek	k1gInSc1	válek
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1564	[number]	k4	1564
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
hejtmana	hejtman	k1gMnSc2	hejtman
Slánského	Slánský	k1gMnSc2	Slánský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
