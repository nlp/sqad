<s>
Veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Sciurus	Sciurus	k1gInSc1	Sciurus
vulgaris	vulgaris	k1gInSc1	vulgaris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
hlodavec	hlodavec	k1gMnSc1	hlodavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
veverkovitých	veverkovitý	k2eAgFnPc2d1	veverkovitý
(	(	kIx(	(
<g/>
Sciuridae	Sciuridae	k1gFnPc2	Sciuridae
<g/>
)	)	kIx)	)
obývající	obývající	k2eAgNnSc1d1	obývající
široké	široký	k2eAgNnSc1d1	široké
území	území	k1gNnSc1	území
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
až	až	k9	až
po	po	k7c6	po
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ji	on	k3xPp3gFnSc4	on
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
lesích	lese	k1gFnPc6	lese
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
alejích	alej	k1gFnPc6	alej
<g/>
,	,	kIx,	,
větších	veliký	k2eAgFnPc6d2	veliký
zahradách	zahrada	k1gFnPc6	zahrada
nebo	nebo	k8xC	nebo
hřbitovech	hřbitov	k1gInPc6	hřbitov
se	se	k3xPyFc4	se
stromovým	stromový	k2eAgInSc7d1	stromový
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zrzavou	zrzavý	k2eAgFnSc4d1	zrzavá
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc4d1	černá
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
hnědou	hnědý	k2eAgFnSc4d1	hnědá
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
obvykle	obvykle	k6eAd1	obvykle
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
19	[number]	k4	19
až	až	k9	až
23	[number]	k4	23
cm	cm	kA	cm
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
mezi	mezi	k7c7	mezi
250	[number]	k4	250
až	až	k9	až
340	[number]	k4	340
g	g	kA	g
někdy	někdy	k6eAd1	někdy
i	i	k9	i
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Huňatý	huňatý	k2eAgInSc1d1	huňatý
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
udržovat	udržovat	k5eAaImF	udržovat
rovnováhu	rovnováha	k1gFnSc4	rovnováha
při	při	k7c6	při
lezení	lezení	k1gNnSc6	lezení
a	a	k8xC	a
skocích	skok	k1gInPc6	skok
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
který	který	k3yQgInSc4	který
veverka	veverka	k1gFnSc1	veverka
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pokrývku	pokrývka	k1gFnSc4	pokrývka
<g/>
"	"	kIx"	"
těla	tělo	k1gNnSc2	tělo
při	při	k7c6	při
spánku	spánek	k1gInSc6	spánek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
14,5	[number]	k4	14,5
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
pro	pro	k7c4	pro
veverku	veverka	k1gFnSc4	veverka
obecnou	obecná	k1gFnSc4	obecná
jsou	být	k5eAaImIp3nP	být
střapce	střapec	k1gInPc1	střapec
chlupů	chlup	k1gInPc2	chlup
na	na	k7c6	na
ušních	ušní	k2eAgInPc6d1	ušní
boltcích	boltec	k1gInPc6	boltec
směřující	směřující	k2eAgInPc1d1	směřující
do	do	k7c2	do
špičky	špička	k1gFnSc2	špička
a	a	k8xC	a
viditelné	viditelný	k2eAgFnSc2d1	viditelná
především	především	k9	především
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
stromových	stromový	k2eAgFnPc2d1	stromová
veverek	veverka	k1gFnPc2	veverka
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
naše	náš	k3xOp1gFnSc1	náš
veverka	veverka	k1gFnSc1	veverka
ostré	ostrý	k2eAgFnSc2d1	ostrá
a	a	k8xC	a
zakřivené	zakřivený	k2eAgInPc1d1	zakřivený
drápy	dráp	k1gInPc1	dráp
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jí	on	k3xPp3gFnSc7	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
lezení	lezení	k1gNnSc6	lezení
po	po	k7c6	po
větvích	větev	k1gFnPc6	větev
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
zaměňována	zaměňovat	k5eAaImNgFnS	zaměňovat
s	s	k7c7	s
blízce	blízce	k6eAd1	blízce
podobným	podobný	k2eAgNnSc7d1	podobné
čikarím	čikarí	k2eAgNnSc7d1	čikarí
červeným	červené	k1gNnSc7	červené
(	(	kIx(	(
<g/>
Tamiasciurus	Tamiasciurus	k1gMnSc1	Tamiasciurus
hudsonicus	hudsonicus	k1gMnSc1	hudsonicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývajícím	obývající	k2eAgInSc6d1	obývající
Severní	severní	k2eAgFnSc1d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
veverkou	veverka	k1gFnSc7	veverka
popelavou	popelavý	k2eAgFnSc7d1	popelavá
(	(	kIx(	(
<g/>
Sciurus	Sciurus	k1gInSc1	Sciurus
carolinensis	carolinensis	k1gFnSc2	carolinensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obývá	obývat	k5eAaImIp3nS	obývat
zvláště	zvláště	k6eAd1	zvláště
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
veverky	veverka	k1gFnPc1	veverka
obecné	obecná	k1gFnSc2	obecná
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
lokality	lokalita	k1gFnSc2	lokalita
rozšíření	rozšíření	k1gNnSc2	rozšíření
a	a	k8xC	a
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
srst	srst	k1gFnSc4	srst
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
a	a	k8xC	a
hrdle	hrdla	k1gFnSc6	hrdla
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
krémově	krémově	k6eAd1	krémově
až	až	k6eAd1	až
bíle	bíle	k6eAd1	bíle
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pohlaví	pohlaví	k1gNnSc2	pohlaví
není	být	k5eNaImIp3nS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
sexuální	sexuální	k2eAgInSc1d1	sexuální
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
není	být	k5eNaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
ani	ani	k8xC	ani
šedá	šedat	k5eAaImIp3nS	šedat
nebo	nebo	k8xC	nebo
čistě	čistě	k6eAd1	čistě
bílá	bílý	k2eAgFnSc1d1	bílá
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Veverce	veverka	k1gFnSc3	veverka
obecné	obecná	k1gFnSc2	obecná
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
srst	srst	k1gFnSc1	srst
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
letní	letní	k2eAgFnSc2d1	letní
na	na	k7c4	na
zimní	zimní	k2eAgFnSc4d1	zimní
a	a	k8xC	a
ze	z	k7c2	z
zimní	zimní	k2eAgFnSc2d1	zimní
znovu	znovu	k6eAd1	znovu
na	na	k7c6	na
letní	letní	k2eAgFnSc6d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1	zimní
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgFnSc1d2	hustší
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
tmavší	tmavý	k2eAgFnSc1d2	tmavší
než	než	k8xS	než
letní	letní	k2eAgFnSc1d1	letní
a	a	k8xC	a
veverkám	veverka	k1gFnPc3	veverka
narůstá	narůstat	k5eAaImIp3nS	narůstat
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
srpnem	srpen	k1gInSc7	srpen
a	a	k8xC	a
listopadem	listopad	k1gInSc7	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
na	na	k7c4	na
období	období	k1gNnSc4	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
samotářským	samotářský	k2eAgInSc7d1	samotářský
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
jiným	jiný	k2eAgFnPc3d1	jiná
veverkám	veverka	k1gFnPc3	veverka
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
na	na	k7c6	na
tlustších	tlustý	k2eAgFnPc6d2	tlustší
větvích	větev	k1gFnPc6	větev
<g/>
,	,	kIx,	,
obývá	obývat	k5eAaImIp3nS	obývat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
cm	cm	kA	cm
velké	velký	k2eAgNnSc1d1	velké
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kukaně	kukaň	k1gFnSc2	kukaň
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgFnSc6d1	tvořená
mechem	mech	k1gInSc7	mech
<g/>
,	,	kIx,	,
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vypnout	vypnout	k5eAaPmF	vypnout
tělo	tělo	k1gNnSc4	tělo
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
delších	dlouhý	k2eAgInPc6d2	delší
skocích	skok	k1gInPc6	skok
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
teritoriální	teritoriální	k2eAgNnSc4d1	teritoriální
a	a	k8xC	a
domácí	domácí	k2eAgNnSc4d1	domácí
území	území	k1gNnSc4	území
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
jedinců	jedinec	k1gMnPc2	jedinec
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Veverka	veverka	k1gFnSc1	veverka
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
části	část	k1gFnSc6	část
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
ukryta	ukryt	k2eAgFnSc1d1	ukryta
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
teplu	teplo	k1gNnSc3	teplo
a	a	k8xC	a
větší	veliký	k2eAgFnSc2d2	veliký
viditelnosti	viditelnost	k1gFnSc2	viditelnost
vůči	vůči	k7c3	vůči
predátorům	predátor	k1gMnPc3	predátor
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
kuna	kuna	k1gFnSc1	kuna
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
lasice	lasice	k1gFnSc1	lasice
kolčava	kolčava	k1gFnSc1	kolčava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
loví	lovit	k5eAaImIp3nS	lovit
především	především	k9	především
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
větší	veliký	k2eAgMnPc1d2	veliký
dravci	dravec	k1gMnPc1	dravec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
káňata	káně	k1gNnPc4	káně
lesní	lesní	k2eAgFnSc2d1	lesní
nebo	nebo	k8xC	nebo
sovy	sova	k1gFnSc2	sova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nehibernuje	hibernovat	k5eNaBmIp3nS	hibernovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tráví	trávit	k5eAaImIp3nP	trávit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
hnízdě	hnízdo	k1gNnSc6	hnízdo
větší	veliký	k2eAgFnSc4d2	veliký
dobu	doba	k1gFnSc4	doba
než	než	k8xS	než
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
19	[number]	k4	19
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	kA	h
<g/>
.	.	kIx.	.
</s>
<s>
60	[number]	k4	60
až	až	k6eAd1	až
80	[number]	k4	80
%	%	kIx~	%
dne	den	k1gInSc2	den
tráví	trávit	k5eAaImIp3nS	trávit
hledáním	hledání	k1gNnSc7	hledání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k9	především
semena	semeno	k1gNnPc4	semeno
šišek	šiška	k1gFnPc2	šiška
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
suší	sušit	k5eAaImIp3nP	sušit
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
ptačí	ptačit	k5eAaImIp3nP	ptačit
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
oříšky	oříšek	k1gInPc1	oříšek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
si	se	k3xPyFc3	se
pochutná	pochutnat	k5eAaPmIp3nS	pochutnat
i	i	k9	i
na	na	k7c6	na
čerstvé	čerstvý	k2eAgFnSc6d1	čerstvá
míze	míza	k1gFnSc6	míza
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
nalezené	nalezený	k2eAgFnSc2d1	nalezená
potravy	potrava	k1gFnSc2	potrava
si	se	k3xPyFc3	se
uschovává	uschovávat	k5eAaImIp3nS	uschovávat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
"	"	kIx"	"
<g/>
spižíren	spižírna	k1gFnPc2	spižírna
<g/>
"	"	kIx"	"
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
výbornou	výborný	k2eAgFnSc4d1	výborná
zásobárnu	zásobárna	k1gFnSc4	zásobárna
potravy	potrava	k1gFnSc2	potrava
v	v	k7c6	v
nejtěžších	těžký	k2eAgNnPc6d3	nejtěžší
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
potravy	potrava	k1gFnSc2	potrava
přitom	přitom	k6eAd1	přitom
sedí	sedit	k5eAaImIp3nS	sedit
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
veverkovců	veverkovec	k1gMnPc2	veverkovec
"	"	kIx"	"
<g/>
na	na	k7c6	na
bobku	bobek	k1gInSc6	bobek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
potravu	potrava	k1gFnSc4	potrava
drží	držet	k5eAaImIp3nP	držet
v	v	k7c6	v
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
jedinců	jedinec	k1gMnPc2	jedinec
probíhá	probíhat	k5eAaImIp3nS	probíhat
páření	páření	k1gNnSc1	páření
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
počátku	počátek	k1gInSc6	počátek
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
během	během	k7c2	během
února	únor	k1gInSc2	únor
a	a	k8xC	a
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
však	však	k9	však
obvyklejší	obvyklý	k2eAgFnSc1d2	obvyklejší
doba	doba	k1gFnSc1	doba
páření	páření	k1gNnSc2	páření
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
během	během	k7c2	během
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
svou	svůj	k3xOyFgFnSc4	svůj
partnerku	partnerka	k1gFnSc4	partnerka
nachází	nacházet	k5eAaImIp3nS	nacházet
díky	díky	k7c3	díky
pronikavému	pronikavý	k2eAgInSc3d1	pronikavý
pachu	pach	k1gInSc3	pach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
samice	samice	k1gFnSc1	samice
vypuzuje	vypuzovat	k5eAaImIp3nS	vypuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
dvou	dva	k4xCgMnPc2	dva
partnerů	partner	k1gMnPc2	partner
začne	začít	k5eAaPmIp3nS	začít
samec	samec	k1gMnSc1	samec
svou	svůj	k3xOyFgFnSc4	svůj
partnerku	partnerka	k1gFnSc4	partnerka
honit	honit	k5eAaImF	honit
po	po	k7c6	po
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
tak	tak	k6eAd1	tak
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
do	do	k7c2	do
hodiny	hodina	k1gFnSc2	hodina
před	před	k7c7	před
spářením	spáření	k1gNnSc7	spáření
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rozmnožováním	rozmnožování	k1gNnSc7	rozmnožování
se	se	k3xPyFc4	se
také	také	k9	také
samice	samice	k1gFnPc1	samice
vykrmují	vykrmovat	k5eAaImIp3nP	vykrmovat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
přibrat	přibrat	k5eAaPmF	přibrat
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
samice	samice	k1gFnPc1	samice
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hmotností	hmotnost	k1gFnSc7	hmotnost
rodí	rodit	k5eAaImIp3nS	rodit
více	hodně	k6eAd2	hodně
mláďat	mládě	k1gNnPc2	mládě
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
nižší	nízký	k2eAgFnSc7d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
dva	dva	k4xCgInPc4	dva
vrhy	vrh	k1gInPc4	vrh
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeden	jeden	k4xCgInSc4	jeden
vrh	vrh	k1gInSc4	vrh
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obvykle	obvykle	k6eAd1	obvykle
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
po	po	k7c6	po
38	[number]	k4	38
až	až	k9	až
39	[number]	k4	39
denní	denní	k2eAgFnSc2d1	denní
březosti	březost	k1gFnSc2	březost
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
holá	holý	k2eAgFnSc1d1	holá
a	a	k8xC	a
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
váží	vážit	k5eAaImIp3nS	vážit
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
g.	g.	k?	g.
Plně	plně	k6eAd1	plně
osrstěná	osrstěný	k2eAgNnPc1d1	osrstěné
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
21	[number]	k4	21
dnech	den	k1gInPc6	den
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
otevřou	otevřít	k5eAaPmIp3nP	otevřít
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
týdnech	týden	k1gInPc6	týden
a	a	k8xC	a
chrup	chrup	k1gInSc4	chrup
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
plně	plně	k6eAd1	plně
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
až	až	k9	až
po	po	k7c6	po
42	[number]	k4	42
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začínají	začínat	k5eAaImIp3nP	začínat
požírat	požírat	k5eAaImF	požírat
pevnou	pevný	k2eAgFnSc4d1	pevná
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	on	k3xPp3gFnPc4	on
však	však	k9	však
kojí	kojit	k5eAaImIp3nP	kojit
až	až	k9	až
do	do	k7c2	do
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
<g/>
,	,	kIx,	,
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
%	%	kIx~	%
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
umírá	umírat	k5eAaImIp3nS	umírat
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prvního	první	k4xOgNnSc2	první
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
dožívá	dožívat	k5eAaImIp3nS	dožívat
průměrně	průměrně	k6eAd1	průměrně
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
tato	tento	k3xDgFnSc1	tento
hranice	hranice	k1gFnSc1	hranice
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
až	až	k9	až
na	na	k7c4	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Veverka	veverka	k1gFnSc1	veverka
obecná	obecná	k1gFnSc1	obecná
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
chráněným	chráněný	k2eAgInSc7d1	chráněný
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
totiž	totiž	k8xC	totiž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
příkrému	příkrý	k2eAgInSc3d1	příkrý
poklesu	pokles	k1gInSc3	pokles
početnosti	početnost	k1gFnSc2	početnost
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
ulovených	ulovená	k1gFnPc2	ulovená
ročně	ročně	k6eAd1	ročně
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
ulovených	ulovený	k2eAgFnPc2d1	ulovená
veverek	veverka	k1gFnPc2	veverka
o	o	k7c4	o
50	[number]	k4	50
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
snížil	snížit	k5eAaPmAgInS	snížit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
veverky	veverka	k1gFnSc2	veverka
již	již	k6eAd1	již
lovit	lovit	k5eAaImF	lovit
nesmí	smět	k5eNaImIp3nS	smět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
pohledu	pohled	k1gInSc2	pohled
však	však	k9	však
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
druh	druh	k1gInSc4	druh
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
a	a	k8xC	a
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
IUCN	IUCN	kA	IUCN
jej	on	k3xPp3gMnSc4	on
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
málo	málo	k6eAd1	málo
dotčených	dotčený	k2eAgInPc2d1	dotčený
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
veverky	veverka	k1gFnPc1	veverka
loví	lovit	k5eAaImIp3nP	lovit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kožešinu	kožešina	k1gFnSc4	kožešina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Kožešina	kožešina	k1gFnSc1	kožešina
z	z	k7c2	z
šedých	šedý	k2eAgFnPc2d1	šedá
veverek	veverka	k1gFnPc2	veverka
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
středověku	středověk	k1gInSc6	středověk
jako	jako	k8xS	jako
popelčina	popelčin	k2eAgFnSc1d1	popelčina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
veverčí	veverčí	k2eAgNnSc1d1	veverčí
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
jedlo	jíst	k5eAaImAgNnS	jíst
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
i	i	k8xC	i
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
se	se	k3xPyFc4	se
veverčí	veverčí	k2eAgFnPc1d1	veverčí
kožešiny	kožešina	k1gFnPc1	kožešina
nazývají	nazývat	k5eAaImIp3nP	nazývat
běla	bělo	k1gNnPc1	bělo
nebo	nebo	k8xC	nebo
bělka	bělka	k1gFnSc1	bělka
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
platidlo	platidlo	k1gNnSc1	platidlo
nebo	nebo	k8xC	nebo
součást	součást	k1gFnSc1	součást
naturální	naturální	k2eAgFnSc2d1	naturální
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
od	od	k7c2	od
domorodců	domorodec	k1gMnPc2	domorodec
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vybírali	vybírat	k5eAaImAgMnP	vybírat
ruští	ruský	k2eAgMnPc1d1	ruský
kozáci	kozák	k1gMnPc1	kozák
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
původ	původ	k1gInSc4	původ
rčení	rčení	k1gNnSc2	rčení
stojí	stát	k5eAaImIp3nS	stát
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
starou	starý	k2eAgFnSc4d1	stará
belu	bela	k1gFnSc4	bela
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
starou	stará	k1gFnSc4	stará
<g/>
,	,	kIx,	,
opelichanou	opelichaný	k2eAgFnSc4d1	opelichaná
veverčí	veverčí	k2eAgFnSc4d1	veverčí
kožku	kožka	k1gFnSc4	kožka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
nejvíce	hodně	k6eAd3	hodně
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
spíše	spíše	k9	spíše
ztráta	ztráta	k1gFnSc1	ztráta
lesů	les	k1gInPc2	les
–	–	k?	–
jejího	její	k3xOp3gInSc2	její
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biomu	biom	k1gInSc2	biom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
potýkají	potýkat	k5eAaImIp3nP	potýkat
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vytlačováním	vytlačování	k1gNnPc3	vytlačování
veverky	veverka	k1gFnSc2	veverka
obecné	obecná	k1gFnSc2	obecná
úspěšnějším	úspěšný	k2eAgInSc7d2	úspěšnější
druhem	druh	k1gInSc7	druh
–	–	k?	–
veverkou	veverka	k1gFnSc7	veverka
popelavou	popelavý	k2eAgFnSc7d1	popelavá
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
40	[number]	k4	40
poddruhů	poddruh	k1gInPc2	poddruh
veverky	veverka	k1gFnSc2	veverka
obecné	obecná	k1gFnSc2	obecná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
číslem	číslo	k1gNnSc7	číslo
nesouhlasilo	souhlasit	k5eNaImAgNnS	souhlasit
několik	několik	k4yIc1	několik
zoologů	zoolog	k1gMnPc2	zoolog
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
nové	nový	k2eAgNnSc1d1	nové
studium	studium	k1gNnSc1	studium
schvalující	schvalující	k2eAgNnSc1d1	schvalující
pouze	pouze	k6eAd1	pouze
16	[number]	k4	16
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vypsány	vypsat	k5eAaPmNgFnP	vypsat
i	i	k9	i
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
níže	níže	k1gFnSc2	níže
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
v.	v.	k?	v.
altaicus	altaicus	k1gInSc1	altaicus
Serebrennikov	Serebrennikov	k1gInSc1	Serebrennikov
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
S.	S.	kA	S.
v.	v.	k?	v.
anadyrensis	anadyrensis	k1gInSc1	anadyrensis
Ognev	Ognva	k1gFnPc2	Ognva
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
S.	S.	kA	S.
v.	v.	k?	v.
argenteus	argenteus	k1gInSc1	argenteus
Kerr	Kerra	k1gFnPc2	Kerra
<g/>
,	,	kIx,	,
1792	[number]	k4	1792
S.	S.	kA	S.
v.	v.	k?	v.
balcanicus	balcanicus	k1gMnSc1	balcanicus
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
S.	S.	kA	S.
v.	v.	k?	v.
bashkiricus	bashkiricus	k1gInSc1	bashkiricus
Ognev	Ognva	k1gFnPc2	Ognva
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
S.	S.	kA	S.
v.	v.	k?	v.
fuscoater	fuscoater	k1gInSc1	fuscoater
Altum	Altum	k1gInSc1	Altum
<g/>
,	,	kIx,	,
1876	[number]	k4	1876
S.	S.	kA	S.
v.	v.	k?	v.
fusconigricans	fusconigricans	k1gInSc1	fusconigricans
Dvigubsky	Dvigubsky	k1gFnPc2	Dvigubsky
<g/>
,	,	kIx,	,
1804	[number]	k4	1804
S.	S.	kA	S.
<g />
.	.	kIx.	.
</s>
<s>
v.	v.	k?	v.
infuscatus	infuscatus	k1gInSc1	infuscatus
Cabrera	Cabrera	k1gFnSc1	Cabrera
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
S.	S.	kA	S.
v.	v.	k?	v.
italicus	italicus	k1gInSc1	italicus
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
1838	[number]	k4	1838
S.	S.	kA	S.
v.	v.	k?	v.
jacutensis	jacutensis	k1gInSc1	jacutensis
Ognev	Ognva	k1gFnPc2	Ognva
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
S.	S.	kA	S.
v.	v.	k?	v.
jenissejensis	jenissejensis	k1gInSc1	jenissejensis
Ognev	Ognva	k1gFnPc2	Ognva
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
S.	S.	kA	S.
v.	v.	k?	v.
leucourus	leucourus	k1gMnSc1	leucourus
Kerr	Kerr	k1gMnSc1	Kerr
<g/>
,	,	kIx,	,
1792	[number]	k4	1792
S.	S.	kA	S.
v.	v.	k?	v.
mantchuricus	mantchuricus	k1gMnSc1	mantchuricus
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
S.	S.	kA	S.
v.	v.	k?	v.
meridionalis	meridionalis	k1gInSc1	meridionalis
Lucifero	Lucifero	k1gNnSc4	Lucifero
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
S.	S.	kA	S.
v.	v.	k?	v.
rupestris	rupestris	k1gFnSc1	rupestris
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
S.	S.	kA	S.
v.	v.	k?	v.
vulgaris	vulgaris	k1gInSc1	vulgaris
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
</s>
