<s>
Prvním	první	k4xOgInSc7	první
doloženým	doložený	k2eAgInSc7d1	doložený
státním	státní	k2eAgInSc7d1	státní
útvarem	útvar	k1gInSc7	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
nadkmenový	nadkmenový	k2eAgInSc1d1	nadkmenový
svaz	svaz	k1gInSc1	svaz
Sámovy	Sámův	k2eAgFnSc2d1	Sámova
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
