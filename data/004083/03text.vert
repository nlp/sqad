<s>
William	William	k1gInSc1	William
Otis	Otis	k1gInSc1	Otis
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1813	[number]	k4	1813
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
vynálezce	vynálezce	k1gMnSc1	vynálezce
parního	parní	k2eAgNnSc2d1	parní
rypadla	rypadlo	k1gNnSc2	rypadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
obdržel	obdržet	k5eAaPmAgMnS	obdržet
patent	patent	k1gInSc4	patent
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
konstrukci	konstrukce	k1gFnSc6	konstrukce
parního	parní	k2eAgNnSc2d1	parní
rypadla	rypadlo	k1gNnSc2	rypadlo
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
James	James	k1gInSc1	James
Watt	watt	k1gInSc4	watt
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
ale	ale	k9	ale
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostatečně	dostatečně	k6eAd1	dostatečně
silný	silný	k2eAgInSc4d1	silný
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jeho	jeho	k3xOp3gNnSc1	jeho
rypadlo	rypadlo	k1gNnSc1	rypadlo
zklamalo	zklamat	k5eAaPmAgNnS	zklamat
<g/>
.	.	kIx.	.
</s>
<s>
Ottis	Ottis	k1gInSc4	Ottis
využil	využít	k5eAaPmAgMnS	využít
mezitím	mezitím	k6eAd1	mezitím
proběhlého	proběhlý	k2eAgInSc2d1	proběhlý
vývoje	vývoj	k1gInSc2	vývoj
parních	parní	k2eAgInPc2d1	parní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
15	[number]	k4	15
tun	tuna	k1gFnPc2	tuna
těžké	těžký	k2eAgNnSc4d1	těžké
rypadlo	rypadlo	k1gNnSc4	rypadlo
s	s	k7c7	s
12	[number]	k4	12
kW	kW	kA	kW
parním	parní	k2eAgInSc7d1	parní
strojem	stroj	k1gInSc7	stroj
na	na	k7c6	na
železničním	železniční	k2eAgInSc6d1	železniční
podvozku	podvozek	k1gInSc6	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
konstrukce	konstrukce	k1gFnSc1	konstrukce
byla	být	k5eAaImAgFnS	být
natolik	natolik	k6eAd1	natolik
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgNnSc4	první
rypadlo	rypadlo	k1gNnSc4	rypadlo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgNnSc7d1	výrazné
zlepšením	zlepšení	k1gNnSc7	zlepšení
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
nahrazení	nahrazení	k1gNnSc1	nahrazení
řetězů	řetěz	k1gInPc2	řetěz
ocelovými	ocelový	k2eAgNnPc7d1	ocelové
lany	lano	k1gNnPc7	lano
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
nejen	nejen	k6eAd1	nejen
Ottisova	Ottisův	k2eAgNnPc4d1	Ottisův
rypadla	rypadlo	k1gNnPc4	rypadlo
</s>
