<s>
Nil	Nil	k1gInSc1	Nil
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
[	[	kIx(	[
<g/>
an-níl	aníl	k1gMnSc1	an-níl
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Nile	Nil	k1gInSc5	Nil
<g/>
,	,	kIx,	,
staroegyptsky	staroegyptsky	k6eAd1	staroegyptsky
iteru	iter	k1gInSc3	iter
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
východní	východní	k2eAgFnSc1d1	východní
Afrikou	Afrika	k1gFnSc7	Afrika
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
6	[number]	k4	6
671	[number]	k4	671
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
řeky	řeka	k1gFnSc2	řeka
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
915	[number]	k4	915
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nil	Nil	k1gInSc1	Nil
a	a	k8xC	a
především	především	k9	především
jeho	jeho	k3xOp3gInPc1	jeho
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
záplavové	záplavový	k2eAgInPc1d1	záplavový
cykly	cyklus	k1gInPc1	cyklus
přinášející	přinášející	k2eAgInPc1d1	přinášející
úrodné	úrodný	k2eAgInPc1d1	úrodný
nánosy	nános	k1gInPc1	nános
daly	dát	k5eAaPmAgInP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
staroegyptské	staroegyptský	k2eAgFnSc3d1	staroegyptská
civilizaci	civilizace	k1gFnSc3	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
byli	být	k5eAaImAgMnP	být
existenčně	existenčně	k6eAd1	existenčně
na	na	k7c6	na
Nilu	Nil	k1gInSc2	Nil
závislí	závislý	k2eAgMnPc1d1	závislý
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
pro	pro	k7c4	pro
Nil	Nil	k1gInSc4	Nil
žádný	žádný	k3yNgInSc1	žádný
specifický	specifický	k2eAgInSc1d1	specifický
název	název	k1gInSc1	název
a	a	k8xC	a
říkali	říkat	k5eAaImAgMnP	říkat
mu	on	k3xPp3gMnSc3	on
iteru	iter	k1gMnSc3	iter
což	což	k3yRnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
řeka	řeka	k1gFnSc1	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
shoda	shoda	k1gFnSc1	shoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
absolutní	absolutní	k2eAgNnSc1d1	absolutní
světové	světový	k2eAgNnSc1d1	světové
prvenství	prvenství	k1gNnSc1	prvenství
patří	patřit	k5eAaImIp3nS	patřit
Nilu	Nil	k1gInSc2	Nil
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jihoamerické	jihoamerický	k2eAgFnSc6d1	jihoamerická
Amazonce	Amazonka	k1gFnSc6	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
řeky	řeka	k1gFnSc2	řeka
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
přesouvají	přesouvat	k5eAaImIp3nP	přesouvat
své	svůj	k3xOyFgNnSc4	svůj
koryto	koryto	k1gNnSc4	koryto
a	a	k8xC	a
tedy	tedy	k9	tedy
mění	měnit	k5eAaImIp3nS	měnit
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc4d1	různý
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
měla	mít	k5eAaImAgFnS	mít
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nebyl	být	k5eNaImAgInS	být
pramen	pramen	k1gInSc1	pramen
Nilu	Nil	k1gInSc2	Nil
znám	znát	k5eAaImIp1nS	znát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ležel	ležet	k5eAaImAgMnS	ležet
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
neznámé	známý	k2eNgFnSc6d1	neznámá
"	"	kIx"	"
<g/>
Černé	Černé	k2eAgFnSc6d1	Černé
Africe	Afrika	k1gFnSc6	Afrika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
přišlo	přijít	k5eAaPmAgNnS	přijít
až	až	k9	až
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
průzkumných	průzkumný	k2eAgInPc2d1	průzkumný
objevů	objev	k1gInPc2	objev
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zdroj	zdroj	k1gInSc4	zdroj
Nilu	Nil	k1gInSc2	Nil
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
jezero	jezero	k1gNnSc1	jezero
Ukerewe	Ukerew	k1gFnSc2	Ukerew
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
samo	sám	k3xTgNnSc1	sám
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
přítoků	přítok	k1gInPc2	přítok
nezanedbatelné	zanedbatelný	k2eNgFnSc2d1	nezanedbatelná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Nejzazším	zadní	k2eAgInSc7d3	nejzazší
zdrojem	zdroj	k1gInSc7	zdroj
Nilu	Nil	k1gInSc2	Nil
je	být	k5eAaImIp3nS	být
burundská	burundský	k2eAgFnSc1d1	Burundská
řeka	řeka	k1gFnSc1	řeka
Ruvyironza	Ruvyironza	k1gFnSc1	Ruvyironza
<g/>
,	,	kIx,	,
větev	větev	k1gFnSc1	větev
řeky	řeka	k1gFnSc2	řeka
Kagera	Kagera	k1gFnSc1	Kagera
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
po	po	k7c6	po
690	[number]	k4	690
km	km	kA	km
dospěje	dochvít	k5eAaPmIp3nS	dochvít
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Ukerewe	Ukerew	k1gFnSc2	Ukerew
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
Východoafrické	východoafrický	k2eAgFnSc6d1	východoafrická
vysočině	vysočina	k1gFnSc6	vysočina
východně	východně	k6eAd1	východně
od	od	k7c2	od
jezer	jezero	k1gNnPc2	jezero
Kivu	Kivus	k1gInSc2	Kivus
a	a	k8xC	a
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
délka	délka	k1gFnSc1	délka
Nilu	Nil	k1gInSc2	Nil
6	[number]	k4	6
695	[number]	k4	695
km	km	kA	km
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
Kageru	Kagera	k1gFnSc4	Kagera
a	a	k8xC	a
Ruvyironzu	Ruvyironza	k1gFnSc4	Ruvyironza
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jezerem	jezero	k1gNnSc7	jezero
Ukerewe	Ukerew	k1gFnSc2	Ukerew
(	(	kIx(	(
<g/>
známým	známý	k1gMnSc7	známý
též	též	k9	též
jako	jako	k9	jako
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
nese	nést	k5eAaImIp3nS	nést
Nil	Nil	k1gInSc4	Nil
jméno	jméno	k1gNnSc4	jméno
Viktoriin	Viktoriin	k2eAgInSc1d1	Viktoriin
Nil	Nil	k1gInSc1	Nil
než	než	k8xS	než
dospěje	dochvít	k5eAaPmIp3nS	dochvít
do	do	k7c2	do
Albertova	Albertův	k2eAgNnSc2d1	Albertovo
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Albertův	Albertův	k2eAgInSc1d1	Albertův
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
pak	pak	k6eAd1	pak
Bahhr	Bahhr	k1gInSc1	Bahhr
al-Jabal	al-Jabal	k1gInSc1	al-Jabal
(	(	kIx(	(
<g/>
Horský	horský	k2eAgInSc1d1	horský
Nil	Nil	k1gInSc1	Nil
resp.	resp.	kA	resp.
Horský	horský	k2eAgInSc1d1	horský
veletok	veletok	k1gInSc1	veletok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Bahhr	Bahhra	k1gFnPc2	Bahhra
al	ala	k1gFnPc2	ala
Ghazal	Ghazal	k1gFnSc2	Ghazal
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
Bahhr	Bahhr	k1gInSc1	Bahhr
al-Abyad	al-Abyad	k1gInSc1	al-Abyad
(	(	kIx(	(
<g/>
Bílý	bílý	k2eAgInSc1d1	bílý
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
dávají	dávat	k5eAaImIp3nP	dávat
jílovité	jílovitý	k2eAgFnPc1d1	jílovitá
splaveniny	splavenina	k1gFnPc1	splavenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zprava	zprava	k6eAd1	zprava
přijímá	přijímat	k5eAaImIp3nS	přijímat
řeky	řeka	k1gFnPc1	řeka
Aswa	Asw	k1gInSc2	Asw
a	a	k8xC	a
Sobat	Sobat	k1gInSc4	Sobat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Chartúmu	Chartúm	k1gInSc2	Chartúm
se	se	k3xPyFc4	se
stéká	stékat	k5eAaImIp3nS	stékat
s	s	k7c7	s
Modrým	modrý	k2eAgInSc7d1	modrý
Nilem	Nil	k1gInSc7	Nil
(	(	kIx(	(
<g/>
Bahr	Bahr	k1gInSc1	Bahr
al	ala	k1gFnPc2	ala
Azraq	Azraq	k1gFnPc2	Azraq
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytékajícím	vytékající	k2eAgMnSc7d1	vytékající
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Tana	tanout	k5eAaImSgMnS	tanout
v	v	k7c4	v
Etiopii	Etiopie	k1gFnSc4	Etiopie
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
již	již	k9	již
řeka	řeka	k1gFnSc1	řeka
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Nil	Nil	k1gInSc1	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Veletok	veletok	k1gInSc1	veletok
poté	poté	k6eAd1	poté
ještě	ještě	k6eAd1	ještě
přijímá	přijímat	k5eAaImIp3nS	přijímat
zprava	zprava	k6eAd1	zprava
Atbaru	Atbara	k1gFnSc4	Atbara
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
až	až	k9	až
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc2	jeho
vody	voda	k1gFnSc2	voda
spoutává	spoutávat	k5eAaImIp3nS	spoutávat
Asuánská	asuánský	k2eAgFnSc1d1	Asuánská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Chartúmem	Chartúm	k1gInSc7	Chartúm
a	a	k8xC	a
přehradou	přehrada	k1gFnSc7	přehrada
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1850	[number]	k4	1850
km	km	kA	km
činí	činit	k5eAaImIp3nS	činit
spád	spád	k1gInSc4	spád
290	[number]	k4	290
m.	m.	k?	m.
Pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Atbary	Atbara	k1gFnSc2	Atbara
protéká	protékat	k5eAaImIp3nS	protékat
Nubijskou	Nubijský	k2eAgFnSc7d1	Nubijská
pouští	poušť	k1gFnSc7	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Protíná	protínat	k5eAaImIp3nS	protínat
řetěz	řetěz	k1gInSc1	řetěz
nízkých	nízký	k2eAgFnPc2d1	nízká
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
velkou	velký	k2eAgFnSc4d1	velká
smyčku	smyčka	k1gFnSc4	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
oddělených	oddělený	k2eAgFnPc6d1	oddělená
úsecích	úsek	k1gInPc6	úsek
doliny	dolina	k1gFnPc1	dolina
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
krystalické	krystalický	k2eAgFnSc2d1	krystalická
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
řeka	řeka	k1gFnSc1	řeka
překonává	překonávat	k5eAaImIp3nS	překonávat
šest	šest	k4xCc4	šest
známých	známý	k2eAgInPc2d1	známý
nilských	nilský	k2eAgInPc2d1	nilský
kataraktů	katarakt	k1gInPc2	katarakt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Asuánu	Asuán	k1gInSc2	Asuán
do	do	k7c2	do
Káhiry	Káhira	k1gFnSc2	Káhira
protéká	protékat	k5eAaImIp3nS	protékat
bez	bez	k7c2	bez
přítoků	přítok	k1gInPc2	přítok
horním	horní	k2eAgInSc7d1	horní
a	a	k8xC	a
dolním	dolní	k2eAgInSc7d1	dolní
Egyptem	Egypt	k1gInSc7	Egypt
jehož	jehož	k3xOyRp3gFnSc6	jehož
starověké	starověký	k2eAgFnSc6d1	starověká
civilizaci	civilizace	k1gFnSc6	civilizace
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
900	[number]	k4	900
km	km	kA	km
má	mít	k5eAaImIp3nS	mít
řeka	řek	k1gMnSc4	řek
malý	malý	k2eAgInSc1d1	malý
spád	spád	k1gInSc1	spád
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
doliny	dolina	k1gFnSc2	dolina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Mohutnou	mohutný	k2eAgFnSc7d1	mohutná
deltou	delta	k1gFnSc7	delta
končí	končit	k5eAaImIp3nS	končit
řeka	řeka	k1gFnSc1	řeka
svou	svůj	k3xOyFgFnSc4	svůj
pouť	pouť	k1gFnSc4	pouť
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
Nilu	Nil	k1gInSc2	Nil
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
22	[number]	k4	22
000	[number]	k4	000
až	až	k9	až
24	[number]	k4	24
000	[number]	k4	000
km2	km2	k4	km2
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
20	[number]	k4	20
km	km	kA	km
od	od	k7c2	od
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deltě	delta	k1gFnSc6	delta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
podél	podél	k7c2	podél
moře	moře	k1gNnSc2	moře
od	od	k7c2	od
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
k	k	k7c3	k
Port	porta	k1gFnPc2	porta
Saidu	Said	k1gInSc3	Said
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
260	[number]	k4	260
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hlavních	hlavní	k2eAgNnPc2d1	hlavní
ramen	rameno	k1gNnPc2	rameno
je	být	k5eAaImIp3nS	být
devět	devět	k4xCc1	devět
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
dvě	dva	k4xCgNnPc1	dva
(	(	kIx(	(
<g/>
Damietta	Damietta	k1gFnSc1	Damietta
a	a	k8xC	a
Rosetta	Rosetta	k1gFnSc1	Rosetta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
splavná	splavný	k2eAgFnSc1d1	splavná
pro	pro	k7c4	pro
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
každé	každý	k3xTgNnSc4	každý
délku	délka	k1gFnSc4	délka
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
kanál	kanál	k1gInSc4	kanál
Ibrahimia	Ibrahimium	k1gNnSc2	Ibrahimium
a	a	k8xC	a
rameno	rameno	k1gNnSc4	rameno
Jusuf	Jusuf	k1gMnSc1	Jusuf
část	část	k1gFnSc1	část
vody	voda	k1gFnSc2	voda
odtéká	odtékat	k5eAaImIp3nS	odtékat
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Birket	Birket	k1gMnSc1	Birket
Karún	Karún	k1gMnSc1	Karún
a	a	k8xC	a
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
oázy	oáza	k1gFnSc2	oáza
Fajúm	Fajúma	k1gFnPc2	Fajúma
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
kanál	kanál	k1gInSc4	kanál
Ismailia	Ismailium	k1gNnSc2	Ismailium
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
vody	voda	k1gFnSc2	voda
odváděna	odváděn	k2eAgFnSc1d1	odváděna
k	k	k7c3	k
zavlažování	zavlažování	k1gNnSc3	zavlažování
oblasti	oblast	k1gFnSc2	oblast
Suezského	suezský	k2eAgInSc2d1	suezský
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
kanál	kanál	k1gInSc4	kanál
Mahmudia	Mahmudium	k1gNnSc2	Mahmudium
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
vody	voda	k1gFnSc2	voda
odváděna	odváděn	k2eAgFnSc1d1	odváděna
k	k	k7c3	k
zavlažování	zavlažování	k1gNnSc3	zavlažování
města	město	k1gNnSc2	město
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
delty	delta	k1gFnSc2	delta
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
lagunová	lagunový	k2eAgNnPc1d1	lagunové
jezera	jezero	k1gNnPc1	jezero
Menzala	Menzala	k1gFnSc2	Menzala
<g/>
,	,	kIx,	,
Burullus	Burullus	k1gMnSc1	Burullus
a	a	k8xC	a
Marjut	Marjut	k1gMnSc1	Marjut
<g/>
.	.	kIx.	.
</s>
<s>
Nil	Nil	k1gInSc1	Nil
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc4d1	složitý
vodní	vodní	k2eAgInSc4d1	vodní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
části	část	k1gFnSc6	část
povodí	povodí	k1gNnSc2	povodí
nastávají	nastávat	k5eAaImIp3nP	nastávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
dvě	dva	k4xCgFnPc1	dva
maxima	maximum	k1gNnSc2	maximum
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
až	až	k8xS	až
květen	květen	k1gInSc1	květen
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
(	(	kIx(	(
<g/>
září	zářit	k5eAaImIp3nS	zářit
až	až	k9	až
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
zvýšení	zvýšení	k1gNnSc2	zvýšení
průtoku	průtok	k1gInSc2	průtok
pod	pod	k7c7	pod
soutěskou	soutěska	k1gFnSc7	soutěska
Nimule	Nimule	k1gFnSc2	Nimule
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Modrého	modrý	k2eAgInSc2d1	modrý
Nilu	Nil	k1gInSc2	Nil
prší	pršet	k5eAaImIp3nS	pršet
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
až	až	k8xS	až
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
se	se	k3xPyFc4	se
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
díky	díky	k7c3	díky
monzunovým	monzunový	k2eAgInPc3d1	monzunový
dešťům	dešť	k1gInPc3	dešť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
vody	voda	k1gFnSc2	voda
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
vypařováním	vypařování	k1gNnSc7	vypařování
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
tak	tak	k9	tak
nejvíce	hodně	k6eAd3	hodně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
Modrý	modrý	k2eAgInSc1d1	modrý
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
přináší	přinášet	k5eAaImIp3nS	přinášet
60	[number]	k4	60
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
vzestup	vzestup	k1gInSc4	vzestup
hladiny	hladina	k1gFnPc1	hladina
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
severním	severní	k2eAgInSc6d1	severní
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
Egyptě	Egypt	k1gInSc6	Egypt
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
povodním	povodeň	k1gFnPc3	povodeň
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
Asuánu	Asuán	k1gInSc2	Asuán
je	být	k5eAaImIp3nS	být
2600	[number]	k4	2600
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
15	[number]	k4	15
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
minimální	minimální	k2eAgInSc4d1	minimální
500	[number]	k4	500
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Při	při	k7c6	při
obvyklých	obvyklý	k2eAgFnPc6d1	obvyklá
povodních	povodeň	k1gFnPc6	povodeň
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
od	od	k7c2	od
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
m.	m.	k?	m.
Řeka	řeka	k1gFnSc1	řeka
unáší	unášet	k5eAaImIp3nS	unášet
u	u	k7c2	u
Asuánu	Asuán	k1gInSc2	Asuán
ročně	ročně	k6eAd1	ročně
62	[number]	k4	62
Mt	Mt	k1gMnPc2	Mt
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
usazují	usazovat	k5eAaImIp3nP	usazovat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
zavlažovacích	zavlažovací	k2eAgInPc6d1	zavlažovací
kanálech	kanál	k1gInPc6	kanál
a	a	k8xC	a
přehradách	přehrada	k1gFnPc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
náplavový	náplavový	k2eAgInSc1d1	náplavový
úrodný	úrodný	k2eAgInSc1d1	úrodný
materiál	materiál	k1gInSc1	materiál
byl	být	k5eAaImAgInS	být
klíčový	klíčový	k2eAgInSc1d1	klíčový
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
zemědělství	zemědělství	k1gNnSc2	zemědělství
staroegyptské	staroegyptský	k2eAgFnSc2d1	staroegyptská
a	a	k8xC	a
nubijské	nubijský	k2eAgFnSc2d1	nubijská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
však	však	k9	však
na	na	k7c6	na
území	území	k1gNnSc6	území
Egypta	Egypt	k1gInSc2	Egypt
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
existence	existence	k1gFnSc2	existence
přehrady	přehrada	k1gFnSc2	přehrada
je	být	k5eAaImIp3nS	být
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
cyklus	cyklus	k1gInSc1	cyklus
záplav	záplava	k1gFnPc2	záplava
a	a	k8xC	a
přísun	přísun	k1gInSc1	přísun
náplavu	náplav	k1gInSc2	náplav
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Regulací	regulace	k1gFnSc7	regulace
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
však	však	k9	však
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
nadměrným	nadměrný	k2eAgFnPc3d1	nadměrná
ničivým	ničivý	k2eAgFnPc3d1	ničivá
záplavám	záplava	k1gFnPc3	záplava
i	i	k8xC	i
suchu	sucho	k1gNnSc3	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Nilu	Nil	k1gInSc2	Nil
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
podnebných	podnebný	k2eAgInPc6d1	podnebný
pásech	pás	k1gInPc6	pás
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nS	lišit
teplotami	teplota	k1gFnPc7	teplota
i	i	k8xC	i
objemem	objem	k1gInSc7	objem
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
afrických	africký	k2eAgNnPc2d1	africké
jezer	jezero	k1gNnPc2	jezero
je	být	k5eAaImIp3nS	být
rovníkové	rovníkový	k2eAgNnSc1d1	rovníkové
podnebí	podnebí	k1gNnSc1	podnebí
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
dostatkem	dostatek	k1gInSc7	dostatek
srážek	srážka	k1gFnPc2	srážka
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vlhkostí	vlhkost	k1gFnSc7	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
teplotami	teplota	k1gFnPc7	teplota
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
období	období	k1gNnSc1	období
srážek	srážka	k1gFnPc2	srážka
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
duben	duben	k1gInSc4	duben
a	a	k8xC	a
říjen	říjen	k1gInSc4	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
sušší	suchý	k2eAgFnSc1d2	sušší
-	-	kIx~	-
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
února	únor	k1gInSc2	únor
panuje	panovat	k5eAaImIp3nS	panovat
chladnější	chladný	k2eAgNnSc1d2	chladnější
suché	suchý	k2eAgNnSc1d1	suché
období	období	k1gNnSc1	období
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
velmi	velmi	k6eAd1	velmi
teplé	teplý	k2eAgNnSc1d1	teplé
počasí	počasí	k1gNnSc1	počasí
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
dosahujícími	dosahující	k2eAgFnPc7d1	dosahující
nezřídka	nezřídka	k6eAd1	nezřídka
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
deště	dešť	k1gInPc1	dešť
(	(	kIx(	(
<g/>
200	[number]	k4	200
až	až	k9	až
500	[number]	k4	500
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
Modrého	modrý	k2eAgInSc2d1	modrý
a	a	k8xC	a
Bílého	bílý	k2eAgInSc2d1	bílý
Nilu	Nil	k1gInSc2	Nil
začíná	začínat	k5eAaImIp3nS	začínat
pás	pás	k1gInSc1	pás
pouští	poušť	k1gFnPc2	poušť
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc1d1	vysoká
denní	denní	k2eAgFnPc1d1	denní
a	a	k8xC	a
nízké	nízký	k2eAgFnPc1d1	nízká
noční	noční	k2eAgFnPc1d1	noční
teploty	teplota	k1gFnPc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
zde	zde	k6eAd1	zde
neprší	pršet	k5eNaImIp3nS	pršet
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
roční	roční	k2eAgInSc1d1	roční
objem	objem	k1gInSc1	objem
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
50	[number]	k4	50
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jarních	jarní	k2eAgInPc6d1	jarní
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
písečné	písečný	k2eAgFnPc4d1	písečná
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
prvotních	prvotní	k2eAgFnPc2d1	prvotní
civilizací	civilizace	k1gFnPc2	civilizace
využívána	využíván	k2eAgFnSc1d1	využívána
k	k	k7c3	k
zavlažování	zavlažování	k1gNnSc3	zavlažování
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
úrodnosti	úrodnost	k1gFnSc2	úrodnost
polí	pole	k1gFnPc2	pole
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgNnSc1d1	přírodní
hnojení	hnojení	k1gNnSc1	hnojení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc2	zásobování
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jako	jako	k8xS	jako
významná	významný	k2eAgFnSc1d1	významná
dopravní	dopravní	k2eAgFnSc1d1	dopravní
tepna	tepna	k1gFnSc1	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Obzvlášť	obzvlášť	k6eAd1	obzvlášť
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
širokém	široký	k2eAgNnSc6d1	široké
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
žije	žít	k5eAaImIp3nS	žít
97	[number]	k4	97
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
Asuánské	asuánský	k2eAgFnSc2d1	Asuánská
přehrady	přehrada	k1gFnSc2	přehrada
bylo	být	k5eAaImAgNnS	být
zabráněné	zabráněný	k2eAgNnSc1d1	zabráněné
katastrofálním	katastrofální	k2eAgFnPc3d1	katastrofální
povodním	povodeň	k1gFnPc3	povodeň
a	a	k8xC	a
suchům	sucho	k1gNnPc3	sucho
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
rozloha	rozloha	k1gFnSc1	rozloha
zavlažovaných	zavlažovaný	k2eAgNnPc2d1	zavlažované
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
měla	mít	k5eAaImAgFnS	mít
výstavba	výstavba	k1gFnSc1	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
eliminaci	eliminace	k1gFnSc3	eliminace
každoročních	každoroční	k2eAgFnPc2d1	každoroční
záplav	záplava	k1gFnPc2	záplava
byly	být	k5eAaImAgFnP	být
umožněny	umožněn	k2eAgFnPc1d1	umožněna
dvě	dva	k4xCgFnPc4	dva
sklizně	sklizeň	k1gFnPc4	sklizeň
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
bavlnářské	bavlnářský	k2eAgFnSc6d1	Bavlnářská
oblasti	oblast	k1gFnSc6	oblast
Gezira	Gezir	k1gInSc2	Gezir
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
a	a	k8xC	a
jejích	její	k3xOp3gInPc6	její
přítocích	přítok	k1gInPc6	přítok
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
přehrady	přehrada	k1gFnPc1	přehrada
<g/>
:	:	kIx,	:
Stará	starý	k2eAgFnSc1d1	stará
Asuánská	asuánský	k2eAgFnSc1d1	Asuánská
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
5,5	[number]	k4	5,5
km3	km3	k4	km3
(	(	kIx(	(
<g/>
Nil	Nil	k1gInSc1	Nil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Násirovo	Násirův	k2eAgNnSc1d1	Násirovo
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Asuánská	asuánský	k2eAgFnSc1d1	Asuánská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
)	)	kIx)	)
–	–	k?	–
164	[number]	k4	164
km3	km3	k4	km3
(	(	kIx(	(
<g/>
Nil	Nil	k1gInSc1	Nil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Merowe	Merowe	k1gInSc1	Merowe
–	–	k?	–
12,5	[number]	k4	12,5
km3	km3	k4	km3
(	(	kIx(	(
<g/>
Nil	Nil	k1gInSc1	Nil
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Gebel-Aulija	Gebel-Aulija	k1gFnSc1	Gebel-Aulija
–	–	k?	–
2,5	[number]	k4	2,5
km3	km3	k4	km3
(	(	kIx(	(
<g/>
Bílý	bílý	k2eAgInSc1d1	bílý
Nil	Nil	k1gInSc1	Nil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Ethiopian	Ethiopian	k1gMnSc1	Ethiopian
Renaissance	Renaissanec	k1gInSc2	Renaissanec
Dam	dáma	k1gFnPc2	dáma
(	(	kIx(	(
<g/>
Millennium	millennium	k1gNnSc1	millennium
Dam	dáma	k1gFnPc2	dáma
<g/>
)	)	kIx)	)
–	–	k?	–
63	[number]	k4	63
km3	km3	k4	km3
(	(	kIx(	(
<g/>
Modrý	modrý	k2eAgInSc1d1	modrý
Nil	Nil	k1gInSc1	Nil
<g/>
)	)	kIx)	)
a	a	k8xC	a
uzávěry	uzávěra	k1gFnSc2	uzávěra
Iena	Ien	k1gInSc2	Ien
<g/>
,	,	kIx,	,
Nag-Hammadi	Nag-Hammad	k1gMnPc1	Nag-Hammad
<g/>
,	,	kIx,	,
Asjut	Asjut	k1gMnSc1	Asjut
<g/>
,	,	kIx,	,
Muhammed-Ali	Muhammed-Al	k1gMnPc1	Muhammed-Al
<g/>
,	,	kIx,	,
Zifta	Zifta	k1gMnSc1	Zifta
<g/>
,	,	kIx,	,
Edfina	Edfina	k1gMnSc1	Edfina
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Senarská	Senarský	k2eAgFnSc1d1	Senarský
na	na	k7c6	na
Modrém	modrý	k2eAgInSc6d1	modrý
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
žije	žít	k5eAaImIp3nS	žít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
jsou	být	k5eAaImIp3nP	být
zpracovávány	zpracováván	k2eAgInPc1d1	zpracováván
nilský	nilský	k2eAgMnSc1d1	nilský
okoun	okoun	k1gMnSc1	okoun
<g/>
,	,	kIx,	,
bichiři	bichiř	k1gMnSc6	bichiř
<g/>
,	,	kIx,	,
tygří	tygří	k2eAgFnSc1d1	tygří
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
sumci	sumec	k1gMnPc1	sumec
<g/>
,	,	kIx,	,
zubatý	zubatý	k2eAgMnSc1d1	zubatý
kapr	kapr	k1gMnSc1	kapr
<g/>
,	,	kIx,	,
balt	balt	k1gMnSc1	balt
(	(	kIx(	(
<g/>
africký	africký	k2eAgMnSc1d1	africký
kapr	kapr	k1gMnSc1	kapr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
také	také	k9	také
žije	žít	k5eAaImIp3nS	žít
krokodýl	krokodýl	k1gMnSc1	krokodýl
nilský	nilský	k2eAgMnSc1d1	nilský
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
ze	z	k7c2	z
Súdánu	Súdán	k1gInSc2	Súdán
a	a	k8xC	a
Horního	horní	k2eAgInSc2d1	horní
Egypta	Egypt	k1gInSc2	Egypt
dopravovaly	dopravovat	k5eAaImAgInP	dopravovat
díky	dík	k1gInPc1	dík
němu	on	k3xPp3gMnSc3	on
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
do	do	k7c2	do
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
,	,	kIx,	,
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
skončilo	skončit	k5eAaPmAgNnS	skončit
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Asuánu	Asuán	k1gInSc6	Asuán
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
Egypt	Egypt	k1gInSc1	Egypt
<g/>
)	)	kIx)	)
vybudována	vybudován	k2eAgFnSc1d1	vybudována
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
<g/>
"	"	kIx"	"
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
završeno	završen	k2eAgNnSc1d1	završeno
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
<g/>
"	"	kIx"	"
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Násirovo	Násirův	k2eAgNnSc1d1	Násirovo
jezero	jezero	k1gNnSc1	jezero
táhnoucí	táhnoucí	k2eAgMnSc1d1	táhnoucí
se	se	k3xPyFc4	se
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
zaujímající	zaujímající	k2eAgFnSc4d1	zaujímající
plochu	plocha	k1gFnSc4	plocha
4	[number]	k4	4
640	[number]	k4	640
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Nil	Nil	k1gInSc1	Nil
splavný	splavný	k2eAgInSc1d1	splavný
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
Asuánu	Asuán	k1gInSc2	Asuán
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
dřívější	dřívější	k2eAgInSc4d1	dřívější
dopravní	dopravní	k2eAgInSc4d1	dopravní
ruch	ruch	k1gInSc4	ruch
připomínají	připomínat	k5eAaImIp3nP	připomínat
pouze	pouze	k6eAd1	pouze
reznoucí	reznoucí	k2eAgFnPc1d1	reznoucí
osvětlovací	osvětlovací	k2eAgFnPc1d1	osvětlovací
věže	věž	k1gFnPc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
Egyptu	Egypt	k1gInSc3	Egypt
zajištěn	zajištěn	k2eAgInSc1d1	zajištěn
alespoň	alespoň	k9	alespoň
nějaký	nějaký	k3yIgInSc4	nějaký
příjem	příjem	k1gInSc4	příjem
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
budován	budován	k2eAgInSc1d1	budován
průplav	průplav	k1gInSc1	průplav
Tuschka	Tuschek	k1gInSc2	Tuschek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
propojí	propojit	k5eAaPmIp3nS	propojit
Násirovo	Násirův	k2eAgNnSc4d1	Násirovo
jezero	jezero	k1gNnSc4	jezero
s	s	k7c7	s
Rudým	rudý	k2eAgNnSc7d1	Rudé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
Nil	Nil	k1gInSc1	Nil
Modrý	modrý	k2eAgInSc1d1	modrý
Nil	Nil	k1gInSc1	Nil
delta	delta	k1gNnSc2	delta
Nilu	Nil	k1gInSc2	Nil
povodí	povodí	k1gNnSc2	povodí
Nilu	Nil	k1gInSc2	Nil
Nilské	nilský	k2eAgFnSc2d1	nilská
katarakty	katarakta	k1gFnSc2	katarakta
Asuánská	asuánský	k2eAgFnSc1d1	Asuánská
přehrada	přehrada	k1gFnSc1	přehrada
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
Seznam	seznam	k1gInSc1	seznam
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
řek	řeka	k1gFnPc2	řeka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
tajuplných	tajuplný	k2eAgInPc2d1	tajuplný
světů	svět	k1gInPc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Reader	Reader	k1gInSc1	Reader
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Digest	Digest	k1gMnSc1	Digest
Výběr	výběr	k1gInSc1	výběr
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902069	[number]	k4	902069
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Řeka	řeka	k1gFnSc1	řeka
Nil	Nil	k1gInSc1	Nil
<g/>
:	:	kIx,	:
Zdroj	zdroj	k1gInSc1	zdroj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
s.	s.	k?	s.
192	[number]	k4	192
<g/>
-	-	kIx~	-
<g/>
197	[number]	k4	197
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Popov	Popov	k1gInSc1	Popov
I.	I.	kA	I.
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Řeka	řeka	k1gFnSc1	řeka
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
П	П	k?	П
И	И	k?	И
В	В	k?	В
<g/>
,	,	kIx,	,
Р	Р	k?	Р
Н	Н	k?	Н
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Dmitrevskij	Dmitrevskij	k1gMnSc1	Dmitrevskij
J.	J.	kA	J.
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Vnitrozemské	vnitrozemský	k2eAgNnSc1d1	vnitrozemské
vodstvo	vodstvo	k1gNnSc1	vodstvo
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
využití	využití	k1gNnSc2	využití
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Д	Д	k?	Д
Ю	Ю	k?	Ю
Д	Д	k?	Д
<g/>
,	,	kIx,	,
В	В	k?	В
в	в	k?	в
А	А	k?	А
и	и	k?	и
и	и	k?	и
и	и	k?	и
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
О	О	k?	О
А	А	k?	А
Р	Р	k?	Р
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Н	Н	k?	Н
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Nil	Nil	k1gInSc1	Nil
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nil	Nil	k1gInSc4	Nil
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Nil	Nil	k1gInSc4	Nil
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Nil	Nil	k1gInSc1	Nil
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Mapa	mapa	k1gFnSc1	mapa
Nilu	Nil	k1gInSc2	Nil
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Přehrady	přehrada	k1gFnPc1	přehrada
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Nilu	Nil	k1gInSc2	Nil
-	-	kIx~	-
přehled	přehled	k1gInSc1	přehled
a	a	k8xC	a
popis	popis	k1gInSc1	popis
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Povodí	povodí	k1gNnSc1	povodí
Nilu	Nil	k1gInSc2	Nil
</s>
