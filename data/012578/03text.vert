<p>
<s>
Vodouch	vodouch	k1gMnSc1	vodouch
stříbřitý	stříbřitý	k2eAgMnSc1d1	stříbřitý
(	(	kIx(	(
<g/>
Argyroneta	Argyronet	k2eAgFnSc1d1	Argyroneta
aquatica	aquatica	k1gFnSc1	aquatica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
známý	známý	k2eAgMnSc1d1	známý
vodní	vodní	k2eAgMnSc1d1	vodní
pavouk	pavouk	k1gMnSc1	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
neumí	umět	k5eNaImIp3nS	umět
přijímat	přijímat	k5eAaImF	přijímat
rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
kyslík	kyslík	k1gInSc4	kyslík
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
staví	stavit	k5eAaPmIp3nP	stavit
si	se	k3xPyFc3	se
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
síťové	síťový	k2eAgFnPc4d1	síťová
klece	klec	k1gFnPc4	klec
ve	v	k7c6	v
spleti	spleť	k1gFnSc6	spleť
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
ve	v	k7c6	v
vzduchové	vzduchový	k2eAgFnSc6d1	vzduchová
bublině	bublina	k1gFnSc6	bublina
schraňuje	schraňovat	k5eAaImIp3nS	schraňovat
kyslík	kyslík	k1gInSc4	kyslík
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
druhů	druh	k1gInPc2	druh
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
samečci	sameček	k1gMnPc1	sameček
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
mm	mm	kA	mm
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
samičky	samička	k1gFnPc4	samička
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
má	mít	k5eAaImIp3nS	mít
tmavohnědou	tmavohnědý	k2eAgFnSc4d1	tmavohnědá
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
a	a	k8xC	a
šedý	šedý	k2eAgInSc4d1	šedý
zadeček	zadeček	k1gInSc4	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
hlavohrudi	hlavohruď	k1gFnSc2	hlavohruď
a	a	k8xC	a
u	u	k7c2	u
samice	samice	k1gFnSc2	samice
celý	celý	k2eAgInSc1d1	celý
zadeček	zadeček	k1gInSc1	zadeček
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
stříbřitě	stříbřitě	k6eAd1	stříbřitě
lesklou	lesklý	k2eAgFnSc7d1	lesklá
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
druhu	druh	k1gInSc2	druh
"	"	kIx"	"
<g/>
stříbřitý	stříbřitý	k2eAgMnSc1d1	stříbřitý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samce	samec	k1gInSc2	samec
tato	tento	k3xDgFnSc1	tento
vrstvička	vrstvička	k1gFnSc1	vrstvička
na	na	k7c6	na
hřbetní	hřbetní	k2eAgFnSc6d1	hřbetní
straně	strana	k1gFnSc6	strana
zadečku	zadeček	k1gInSc2	zadeček
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Vodouch	vodouch	k1gMnSc1	vodouch
stříbřitý	stříbřitý	k2eAgMnSc1d1	stříbřitý
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejjedovatějším	jedovatý	k2eAgMnPc3d3	nejjedovatější
pavoukům	pavouk	k1gMnPc3	pavouk
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
nejjedovatějším	jedovatý	k2eAgMnSc7d3	nejjedovatější
pavoukem	pavouk	k1gMnSc7	pavouk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
Zápřednice	Zápřednice	k1gFnSc1	Zápřednice
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Vodouch	vodouch	k1gMnSc1	vodouch
stříbřitý	stříbřitý	k2eAgMnSc1d1	stříbřitý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
stojatých	stojatý	k2eAgFnPc6d1	stojatá
vodách	voda	k1gFnPc6	voda
zarostlých	zarostlý	k2eAgFnPc2d1	zarostlá
vegetací	vegetace	k1gFnPc2	vegetace
<g/>
,	,	kIx,	,
rozmanitě	rozmanitě	k6eAd1	rozmanitě
velkých	velká	k1gFnPc2	velká
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
hojný	hojný	k2eAgMnSc1d1	hojný
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tůních	tůně	k1gFnPc6	tůně
bažin	bažina	k1gFnPc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
také	také	k9	také
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
příkopech	příkop	k1gInPc6	příkop
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
i	i	k9	i
v	v	k7c6	v
rybnících	rybník	k1gInPc6	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
pavouk	pavouk	k1gMnSc1	pavouk
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
prostorný	prostorný	k2eAgInSc4d1	prostorný
vzduchový	vzduchový	k2eAgInSc4d1	vzduchový
zvon	zvon	k1gInSc4	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
utká	utkat	k5eAaPmIp3nS	utkat
mezi	mezi	k7c7	mezi
vodními	vodní	k2eAgFnPc7d1	vodní
rostlinami	rostlina	k1gFnPc7	rostlina
horizontální	horizontální	k2eAgInSc4d1	horizontální
pavučinovou	pavučinový	k2eAgFnSc4d1	pavučinová
plachetku	plachetka	k1gFnSc4	plachetka
a	a	k8xC	a
upřede	upříst	k5eAaPmIp3nS	upříst
jedno	jeden	k4xCgNnSc1	jeden
vodící	vodící	k2eAgNnSc1d1	vodící
vlákno	vlákno	k1gNnSc1	vlákno
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
vysune	vysunout	k5eAaPmIp3nS	vysunout
konec	konec	k1gInSc4	konec
zadečku	zadeček	k1gInSc2	zadeček
a	a	k8xC	a
překřížené	překřížený	k2eAgFnSc2d1	překřížená
zadní	zadní	k2eAgFnSc2d1	zadní
nohy	noha	k1gFnSc2	noha
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
zachytí	zachytit	k5eAaPmIp3nS	zachytit
pomocí	pomocí	k7c2	pomocí
chloupků	chloupek	k1gInPc2	chloupek
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
velkou	velký	k2eAgFnSc4d1	velká
vzduchovou	vzduchový	k2eAgFnSc4d1	vzduchová
bublinu	bublina	k1gFnSc4	bublina
a	a	k8xC	a
po	po	k7c6	po
vodícím	vodící	k2eAgNnSc6d1	vodící
vlákně	vlákno	k1gNnSc6	vlákno
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
ponoří	ponořit	k5eAaPmIp3nP	ponořit
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Bublinu	bublina	k1gFnSc4	bublina
si	se	k3xPyFc3	se
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
přidržuje	přidržovat	k5eAaImIp3nS	přidržovat
pomocí	pomocí	k7c2	pomocí
zadních	zadní	k2eAgFnPc2d1	zadní
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
plachetce	plachetka	k1gFnSc3	plachetka
<g/>
,	,	kIx,	,
vleze	vlézt	k5eAaPmIp3nS	vlézt
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
bublinu	bublina	k1gFnSc4	bublina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hnízdo	hnízdo	k1gNnSc4	hnízdo
kopulovitě	kopulovitě	k6eAd1	kopulovitě
vyklene	vyklenout	k5eAaPmIp3nS	vyklenout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
transport	transport	k1gInSc1	transport
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
získá	získat	k5eAaPmIp3nS	získat
vodouchův	vodouchův	k2eAgInSc4d1	vodouchův
úkryt	úkryt	k1gInSc4	úkryt
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pavouk	pavouk	k1gMnSc1	pavouk
ještě	ještě	k6eAd1	ještě
dodatečně	dodatečně	k6eAd1	dodatečně
vyztuží	vyztužit	k5eAaPmIp3nP	vyztužit
dalšími	další	k2eAgFnPc7d1	další
pavučinami	pavučina	k1gFnPc7	pavučina
pevnost	pevnost	k1gFnSc4	pevnost
svého	svůj	k3xOyFgInSc2	svůj
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
útvar	útvar	k1gInSc1	útvar
vydrží	vydržet	k5eAaPmIp3nS	vydržet
obyčejně	obyčejně	k6eAd1	obyčejně
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
až	až	k8xS	až
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
pavouk	pavouk	k1gMnSc1	pavouk
ale	ale	k8xC	ale
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
musí	muset	k5eAaImIp3nS	muset
doplnit	doplnit	k5eAaPmF	doplnit
jeho	jeho	k3xOp3gInSc4	jeho
vzduchový	vzduchový	k2eAgInSc4d1	vzduchový
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzduchovém	vzduchový	k2eAgInSc6d1	vzduchový
zvonu	zvon	k1gInSc6	zvon
probíhají	probíhat	k5eAaImIp3nP	probíhat
veškeré	veškerý	k3xTgInPc4	veškerý
životní	životní	k2eAgInPc4d1	životní
pochody	pochod	k1gInPc4	pochod
pavouka	pavouk	k1gMnSc2	pavouk
-	-	kIx~	-
příjem	příjem	k1gInSc1	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
svlékání	svlékání	k1gNnSc6	svlékání
<g/>
,	,	kIx,	,
páření	páření	k1gNnSc6	páření
a	a	k8xC	a
kladení	kladení	k1gNnSc6	kladení
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Vodouch	vodouch	k1gMnSc1	vodouch
stříbřitý	stříbřitý	k2eAgMnSc1d1	stříbřitý
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
pavoukovců	pavoukovec	k1gMnPc2	pavoukovec
má	mít	k5eAaImIp3nS	mít
tělo	tělo	k1gNnSc1	tělo
rozdělené	rozdělená	k1gFnSc2	rozdělená
na	na	k7c4	na
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
má	mít	k5eAaImIp3nS	mít
chloupky	chloupek	k1gInPc4	chloupek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
vzduchové	vzduchový	k2eAgFnPc4d1	vzduchová
bubliny	bublina	k1gFnPc4	bublina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Pavoukovci	Pavoukovec	k1gMnPc1	Pavoukovec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
152	[number]	k4	152
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
1114	[number]	k4	1114
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vodouch	vodouch	k1gMnSc1	vodouch
stříbřitý	stříbřitý	k2eAgMnSc1d1	stříbřitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Argyroneta	Argyronet	k1gMnSc2	Argyronet
aquatica	aquaticus	k1gMnSc2	aquaticus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Water	Water	k1gMnSc1	Water
spider	spider	k1gMnSc1	spider
(	(	kIx(	(
<g/>
Argyroneta	Argyronet	k2eAgFnSc1d1	Argyroneta
aquatica	aquatica	k1gFnSc1	aquatica
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
