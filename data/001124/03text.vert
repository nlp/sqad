<s>
Texas	Texas	k1gInSc1	Texas
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
západních	západní	k2eAgInPc2d1	západní
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
státě	stát	k1gInSc6	stát
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Louisianou	Louisiana	k1gFnSc7	Louisiana
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Arkansasem	Arkansas	k1gInSc7	Arkansas
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Oklahomou	Oklahomý	k2eAgFnSc7d1	Oklahomý
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Novým	nový	k2eAgNnSc7d1	nové
Mexikem	Mexiko	k1gNnSc7	Mexiko
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
mexickými	mexický	k2eAgInPc7d1	mexický
státy	stát	k1gInPc7	stát
Chihuahua	Chihuahu	k1gInSc2	Chihuahu
<g/>
,	,	kIx,	,
Coahuila	Coahuila	k1gFnSc1	Coahuila
<g/>
,	,	kIx,	,
Nuevo	Nuevo	k1gNnSc1	Nuevo
León	Leóna	k1gFnPc2	Leóna
a	a	k8xC	a
Tamaulipas	Tamaulipasa	k1gFnPc2	Tamaulipasa
<g/>
.	.	kIx.	.
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgNnSc1d1	jihovýchodní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Mexický	mexický	k2eAgInSc4d1	mexický
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
696	[number]	k4	696
241	[number]	k4	241
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Texas	Texas	k1gInSc1	Texas
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
27,5	[number]	k4	27,5
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
druhým	druhý	k4xOgInSc7	druhý
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
41	[number]	k4	41
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Austin	Austin	k1gInSc1	Austin
s	s	k7c7	s
890	[number]	k4	890
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Houston	Houston	k1gInSc4	Houston
s	s	k7c7	s
2,2	[number]	k4	2,2
milióny	milión	k4xCgInPc7	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
San	San	k1gMnSc1	San
Antonio	Antonio	k1gMnSc1	Antonio
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
miliónů	milión	k4xCgInPc2	milión
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
miliónů	milión	k4xCgInPc2	milión
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fort	Fort	k?	Fort
Worth	Worth	k1gInSc1	Worth
(	(	kIx(	(
<g/>
790	[number]	k4	790
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Paso	Paso	k6eAd1	Paso
(	(	kIx(	(
<g/>
680	[number]	k4	680
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Texasu	Texas	k1gInSc2	Texas
patří	patřit	k5eAaImIp3nS	patřit
591	[number]	k4	591
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Guadalupe	Guadalup	k1gInSc5	Guadalup
Peak	Peaka	k1gFnPc2	Peaka
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
2667	[number]	k4	2667
m	m	kA	m
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Guadalupe	Guadalup	k1gInSc5	Guadalup
Mountains	Mountains	k1gInSc1	Mountains
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Red	Red	k1gFnSc2	Red
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc4	část
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Oklahomou	Oklahomý	k2eAgFnSc7d1	Oklahomý
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rio	Rio	k1gMnSc5	Rio
Grande	grand	k1gMnSc5	grand
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
která	který	k3yQgFnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
doložení	doložený	k2eAgMnPc1d1	doložený
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Texasu	Texas	k1gInSc2	Texas
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1519	[number]	k4	1519
a	a	k8xC	a
1528	[number]	k4	1528
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
zůstal	zůstat	k5eAaPmAgInS	zůstat
mimo	mimo	k7c4	mimo
zájem	zájem	k1gInSc4	zájem
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
až	až	k9	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
španělsko-indiánské	španělskondiánský	k2eAgNnSc1d1	španělsko-indiánský
osídlení	osídlení	k1gNnSc1	osídlení
v	v	k7c6	v
El	Ela	k1gFnPc2	Ela
Pasu	pas	k1gInSc2	pas
a	a	k8xC	a
také	také	k9	také
francouzská	francouzský	k2eAgFnSc1d1	francouzská
kolonie	kolonie	k1gFnSc1	kolonie
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
vydržela	vydržet	k5eAaPmAgFnS	vydržet
jen	jen	k9	jen
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
si	se	k3xPyFc3	se
nárokovali	nárokovat	k5eAaImAgMnP	nárokovat
Španělé	Španěl	k1gMnPc1	Španěl
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
kolonizaci	kolonizace	k1gFnSc4	kolonizace
sousední	sousední	k2eAgFnSc2d1	sousední
Louisiany	Louisiana	k1gFnSc2	Louisiana
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
Texasu	Texas	k1gInSc2	Texas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
zůstal	zůstat	k5eAaPmAgInS	zůstat
nadále	nadále	k6eAd1	nadále
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
zalidněn	zalidněn	k2eAgInSc1d1	zalidněn
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
oblast	oblast	k1gFnSc1	oblast
získala	získat	k5eAaPmAgFnS	získat
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
tejas	tejas	k1gInSc1	tejas
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
přátelé	přítel	k1gMnPc1	přítel
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
z	z	k7c2	z
indiánského	indiánský	k2eAgInSc2d1	indiánský
jazyka	jazyk	k1gInSc2	jazyk
caddo	caddo	k1gNnSc1	caddo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
se	se	k3xPyFc4	se
Texas	Texas	k1gInSc1	Texas
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
nastala	nastat	k5eAaPmAgFnS	nastat
větší	veliký	k2eAgFnSc1d2	veliký
kolonizace	kolonizace	k1gFnSc1	kolonizace
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Texaská	texaský	k2eAgFnSc1d1	texaská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
anektovaly	anektovat	k5eAaBmAgInP	anektovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1845	[number]	k4	1845
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
28	[number]	k4	28
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
svoji	svůj	k3xOyFgFnSc4	svůj
suverenitu	suverenita	k1gFnSc4	suverenita
formálně	formálně	k6eAd1	formálně
předal	předat	k5eAaPmAgInS	předat
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Texas	Texas	k1gInSc1	Texas
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1865	[number]	k4	1865
součástí	součást	k1gFnSc7	součást
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
k	k	k7c3	k
Unii	unie	k1gFnSc3	unie
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
připojen	připojen	k2eAgInSc1d1	připojen
roku	rok	k1gInSc3	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
americký	americký	k2eAgInSc1d1	americký
stát	stát	k1gInSc1	stát
po	po	k7c6	po
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
tak	tak	k6eAd1	tak
velký	velký	k2eAgMnSc1d1	velký
jako	jako	k8xC	jako
Německo	Německo	k1gNnSc1	Německo
nebo	nebo	k8xC	nebo
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
středojižní	středojižní	k2eAgFnSc6d1	středojižní
části	část	k1gFnSc6	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
hranic	hranice	k1gFnPc2	hranice
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgFnPc1d1	definována
řekami	řeka	k1gFnPc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Rio	Rio	k?	Rio
Grande	grand	k1gMnSc5	grand
tvoří	tvořit	k5eAaImIp3nP	tvořit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
mexickými	mexický	k2eAgInPc7d1	mexický
státy	stát	k1gInPc7	stát
Chihuahua	Chihuahu	k1gInSc2	Chihuahu
<g/>
,	,	kIx,	,
Coahuila	Coahuila	k1gFnSc1	Coahuila
<g/>
,	,	kIx,	,
Nuevo	Nuevo	k1gNnSc1	Nuevo
León	Leóna	k1gFnPc2	Leóna
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tamaulipas	Tamaulipas	k1gInSc1	Tamaulipas
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Red	Red	k?	Red
River	River	k1gInSc1	River
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Oklahomou	Oklahomý	k2eAgFnSc7d1	Oklahomý
a	a	k8xC	a
Arkansasem	Arkansas	k1gInSc7	Arkansas
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Sabine	Sabinout	k5eAaPmIp3nS	Sabinout
River	River	k1gInSc1	River
je	být	k5eAaImIp3nS	být
hraniční	hraniční	k2eAgFnSc7d1	hraniční
řekou	řeka	k1gFnSc7	řeka
s	s	k7c7	s
Louisianou	Louisiana	k1gFnSc7	Louisiana
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
Texas	Texas	kA	Texas
Panhandle	Panhandle	k1gFnPc1	Panhandle
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgInPc1d1	definován
zeměpisnými	zeměpisný	k2eAgFnPc7d1	zeměpisná
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Oklahomou	Oklahomý	k2eAgFnSc7d1	Oklahomý
na	na	k7c4	na
100	[number]	k4	100
<g/>
°	°	k?	°
W	W	kA	W
západní	západní	k2eAgFnSc2d1	západní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
na	na	k7c4	na
Oklahoma	Oklahoma	k1gNnSc4	Oklahoma
na	na	k7c4	na
36	[number]	k4	36
<g/>
°	°	k?	°
<g/>
30	[number]	k4	30
<g/>
'	'	kIx"	'
N	N	kA	N
severní	severní	k2eAgFnPc4d1	severní
šířky	šířka	k1gFnPc4	šířka
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Novým	nový	k2eAgNnSc7d1	nové
Mexikem	Mexiko	k1gNnSc7	Mexiko
na	na	k7c4	na
103	[number]	k4	103
<g/>
°	°	k?	°
W	W	kA	W
západní	západní	k2eAgFnSc2d1	západní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
má	mít	k5eAaImIp3nS	mít
3700	[number]	k4	3700
potůčků	potůček	k1gInPc2	potůček
a	a	k8xC	a
15	[number]	k4	15
větších	veliký	k2eAgFnPc2d2	veliký
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Rio	Rio	k1gMnSc5	Rio
Grande	grand	k1gMnSc5	grand
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
většími	veliký	k2eAgFnPc7d2	veliký
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Pecos	Pecos	k1gInSc4	Pecos
<g/>
,	,	kIx,	,
Brazos	Brazos	k1gInSc4	Brazos
<g/>
,	,	kIx,	,
Colorado	Colorado	k1gNnSc4	Colorado
a	a	k8xC	a
Red	Red	k1gFnSc4	Red
River	Rivra	k1gFnPc2	Rivra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
lemuje	lemovat	k5eAaImIp3nS	lemovat
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Oklahomou	Oklahomý	k2eAgFnSc7d1	Oklahomý
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
několik	několik	k4yIc4	několik
přirozených	přirozený	k2eAgNnPc2d1	přirozené
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
Texasané	Texasan	k1gMnPc1	Texasan
ale	ale	k8xC	ale
postavili	postavit	k5eAaPmAgMnP	postavit
kolem	kolem	k7c2	kolem
100	[number]	k4	100
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Texasu	Texas	k1gInSc2	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Texasu	Texas	k1gInSc2	Texas
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
období	období	k1gNnSc2	období
součástí	součást	k1gFnPc2	součást
španělské	španělský	k2eAgFnSc2d1	španělská
kolonie	kolonie	k1gFnSc2	kolonie
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1821-1836	[number]	k4	1821-1836
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Texasu	Texas	k1gInSc2	Texas
součástí	součást	k1gFnSc7	součást
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
území	území	k1gNnSc1	území
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc4	stát
Coahuila	Coahuil	k1gMnSc2	Coahuil
y	y	k?	y
Tejas	Tejas	k1gInSc1	Tejas
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Coahuila	Coahuila	k1gFnSc1	Coahuila
a	a	k8xC	a
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chihuahua	Chihuahua	k1gFnSc1	Chihuahua
<g/>
,	,	kIx,	,
Nuevo	Nuevo	k1gNnSc1	Nuevo
León	Leóna	k1gFnPc2	Leóna
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Nový	nový	k2eAgInSc1d1	nový
León	León	k1gInSc1	León
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tamaulipas	Tamaulipas	k1gInSc4	Tamaulipas
<g/>
,	,	kIx,	,
a	a	k8xC	a
teritorium	teritorium	k1gNnSc1	teritorium
Santa	Sant	k1gInSc2	Sant
Fe	Fe	k1gFnSc3	Fe
de	de	k?	de
Nuevo	Nuevo	k1gNnSc4	Nuevo
México	México	k6eAd1	México
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
Santa	Sant	k1gMnSc4	Sant
Annovu	Annův	k2eAgFnSc4d1	Annův
mexickou	mexický	k2eAgFnSc4d1	mexická
ústavu	ústava	k1gFnSc4	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
Mexiko	Mexiko	k1gNnSc1	Mexiko
stalo	stát	k5eAaPmAgNnS	stát
unitárním	unitární	k2eAgInSc7d1	unitární
státem	stát	k1gInSc7	stát
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Texas	Texas	k1gInSc1	Texas
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1836	[number]	k4	1836
svoji	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
jako	jako	k9	jako
Texaská	texaský	k2eAgFnSc1d1	texaská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
tato	tento	k3xDgFnSc1	tento
republika	republika	k1gFnSc1	republika
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
jen	jen	k9	jen
asi	asi	k9	asi
třetinu	třetina	k1gFnSc4	třetina
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Texasu	Texas	k1gInSc2	Texas
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
si	se	k3xPyFc3	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
vedle	vedle	k7c2	vedle
zbytku	zbytek	k1gInSc2	zbytek
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Texasu	Texas	k1gInSc2	Texas
nárokovat	nárokovat	k5eAaImF	nárokovat
veškerá	veškerý	k3xTgNnPc4	veškerý
mexická	mexický	k2eAgNnPc4d1	mexické
území	území	k1gNnPc4	území
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Rio	Rio	k1gMnSc5	Rio
Grande	grand	k1gMnSc5	grand
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc4d1	tvořící
okrajová	okrajový	k2eAgNnPc4d1	okrajové
území	území	k1gNnPc4	území
mexických	mexický	k2eAgInPc2d1	mexický
států	stát	k1gInPc2	stát
Chihuahua	Chihuahuum	k1gNnSc2	Chihuahuum
<g/>
,	,	kIx,	,
Coahuila	Coahuila	k1gFnSc1	Coahuila
<g/>
,	,	kIx,	,
Nuevo	Nuevo	k1gNnSc1	Nuevo
León	Leóna	k1gFnPc2	Leóna
a	a	k8xC	a
Tamaulipas	Tamaulipasa	k1gFnPc2	Tamaulipasa
<g/>
,	,	kIx,	,
a	a	k8xC	a
teritorií	teritorium	k1gNnPc2	teritorium
Alta	Alta	k1gMnSc1	Alta
California	Californium	k1gNnSc2	Californium
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Horní	horní	k2eAgFnSc1d1	horní
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Santa	Santa	k1gMnSc1	Santa
Fe	Fe	k1gFnPc2	Fe
de	de	k?	de
Nuevo	Nuevo	k1gNnSc4	Nuevo
México	México	k6eAd1	México
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
se	se	k3xPyFc4	se
Texas	Texas	k1gInSc1	Texas
snažil	snažit	k5eAaImAgInS	snažit
stát	stát	k1gInSc4	stát
součástí	součást	k1gFnPc2	součást
USA	USA	kA	USA
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
přidružení	přidružení	k1gNnSc1	přidružení
nejprve	nejprve	k6eAd1	nejprve
odmítalo	odmítat	k5eAaImAgNnS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
prezidenta	prezident	k1gMnSc2	prezident
J.	J.	kA	J.
Polka	Polka	k1gFnSc1	Polka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
Texas	Texas	k1gInSc1	Texas
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
USA	USA	kA	USA
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1845	[number]	k4	1845
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
28	[number]	k4	28
<g/>
.	.	kIx.	.
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc3d1	dnešní
velikosti	velikost	k1gFnSc3	velikost
nabyl	nabýt	k5eAaPmAgInS	nabýt
Texas	Texas	k1gInSc1	Texas
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
Kompromisu	kompromis	k1gInSc2	kompromis
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
přišel	přijít	k5eAaPmAgInS	přijít
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
umoření	umoření	k1gNnSc4	umoření
dluhu	dluh	k1gInSc2	dluh
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
o	o	k7c4	o
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgNnPc4d1	vytvořené
teritoria	teritorium	k1gNnPc4	teritorium
Nové	Nové	k2eAgNnPc4d1	Nové
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
,	,	kIx,	,
Utah	Utah	k1gInSc4	Utah
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
teritoria	teritorium	k1gNnSc2	teritorium
Kansas	Kansas	k1gInSc1	Kansas
a	a	k8xC	a
Nebraska	Nebraska	k1gFnSc1	Nebraska
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
pěti	pět	k4xCc2	pět
regionů	region	k1gInPc2	region
a	a	k8xC	a
topografických	topografický	k2eAgInPc2d1	topografický
celků	celek	k1gInPc2	celek
<g/>
:	:	kIx,	:
United	United	k1gInSc1	United
States	Statesa	k1gFnPc2	Statesa
Census	census	k1gInSc1	census
Bureau	Bureaus	k1gInSc2	Bureaus
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Texasu	Texas	k1gInSc2	Texas
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2011	[number]	k4	2011
na	na	k7c4	na
25	[number]	k4	25
674	[number]	k4	674
681	[number]	k4	681
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
2,1	[number]	k4	2,1
%	%	kIx~	%
přírůstek	přírůstek	k1gInSc1	přírůstek
od	od	k7c2	od
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatel	obyvatel	k1gMnPc2	obyvatel
Texasu	Texas	k1gInSc2	Texas
činí	činit	k5eAaImIp3nS	činit
34,8	[number]	k4	34,8
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
nad	nad	k7c7	nad
průměrem	průměr	k1gInSc7	průměr
hustoty	hustota	k1gFnSc2	hustota
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
31	[number]	k4	31
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
rozlohu	rozloha	k1gFnSc4	rozloha
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
116	[number]	k4	116
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
70,4	[number]	k4	70,4
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
11,8	[number]	k4	11,8
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,7	[number]	k4	0,7
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,8	[number]	k4	3,8
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
10,5	[number]	k4	10,5
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,7	[number]	k4	2,7
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
37,6	[number]	k4	37,6
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Texasu	Texas	k1gInSc2	Texas
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
kvůli	kvůli	k7c3	kvůli
přílivu	příliv	k1gInSc2	příliv
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Černoši	černoch	k1gMnPc1	černoch
(	(	kIx(	(
<g/>
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
<g/>
)	)	kIx)	)
obývají	obývat	k5eAaImIp3nP	obývat
hlavně	hlavně	k9	hlavně
východ	východ	k1gInSc4	východ
Texasu	Texas	k1gInSc2	Texas
<g/>
,	,	kIx,	,
Hispánci	Hispánek	k1gMnPc1	Hispánek
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
hispánské	hispánský	k2eAgFnSc2d1	hispánská
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
Mexičané	Mexičan	k1gMnPc1	Mexičan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
Hispánců	Hispánec	k1gMnPc2	Hispánec
z	z	k7c2	z
šesti	šest	k4xCc2	šest
a	a	k8xC	a
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
(	(	kIx(	(
<g/>
32	[number]	k4	32
%	%	kIx~	%
<g/>
)	)	kIx)	)
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
35,5	[number]	k4	35,5
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
je	být	k5eAaImIp3nS	být
i	i	k9	i
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
usídlených	usídlený	k2eAgFnPc2d1	usídlená
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
úrodné	úrodný	k2eAgFnSc6d1	úrodná
černozemi	černozem	k1gFnSc6	černozem
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Waco	Waco	k6eAd1	Waco
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
oblasti	oblast	k1gFnPc4	oblast
Rožnovska	Rožnovsko	k1gNnSc2	Rožnovsko
a	a	k8xC	a
Frýdlantska	Frýdlantsko	k1gNnSc2	Frýdlantsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Početně	početně	k6eAd1	početně
dominují	dominovat	k5eAaImIp3nP	dominovat
protestanté	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
roztříštěni	roztříštit	k5eAaPmNgMnP	roztříštit
do	do	k7c2	do
několika	několik	k4yIc2	několik
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejvíce	hodně	k6eAd3	hodně
členů	člen	k1gMnPc2	člen
má	mít	k5eAaImIp3nS	mít
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
asi	asi	k9	asi
28	[number]	k4	28
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
skupinu	skupina	k1gFnSc4	skupina
představují	představovat	k5eAaImIp3nP	představovat
baptisté	baptista	k1gMnPc1	baptista
(	(	kIx(	(
<g/>
21	[number]	k4	21
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
východní	východní	k2eAgInSc1d1	východní
Texas	Texas	k1gInSc1	Texas
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Aglomerace	aglomerace	k1gFnSc1	aglomerace
Dallas-Fort	Dallas-Fort	k1gInSc1	Dallas-Fort
Worth	Worth	k1gInSc1	Worth
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
tří	tři	k4xCgInPc2	tři
významných	významný	k2eAgInPc2d1	významný
evangelických	evangelický	k2eAgInPc2d1	evangelický
seminářů	seminář	k1gInPc2	seminář
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Lakewood	Lakewood	k1gInSc1	Lakewood
Church	Church	k1gInSc1	Church
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
v	v	k7c6	v
celých	celá	k1gFnPc6	celá
USA	USA	kA	USA
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
43	[number]	k4	43
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Lubbock	Lubbock	k6eAd1	Lubbock
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
místních	místní	k2eAgInPc2d1	místní
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
kostelů	kostel	k1gInPc2	kostel
na	na	k7c4	na
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
San	San	k1gMnSc7	San
Antoniem	Antonio	k1gMnSc7	Antonio
a	a	k8xC	a
Dallasem	Dallas	k1gMnSc7	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tři	tři	k4xCgNnPc1	tři
města	město	k1gNnPc1	město
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
Texasanů	Texasan	k1gMnPc2	Texasan
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
metropolitních	metropolitní	k2eAgFnPc6d1	metropolitní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Aglommerace	Aglommerace	k1gFnSc1	Aglommerace
Dallas-Fort	Dallas-Fort	k1gInSc1	Dallas-Fort
Worth	Worth	k1gInSc1	Worth
Metropolitan	metropolitan	k1gInSc1	metropolitan
Area	area	k1gFnSc1	area
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Houston	Houston	k1gInSc1	Houston
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
a	a	k8xC	a
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
největší	veliký	k2eAgFnPc4d3	veliký
v	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
aglomerace	aglomerace	k1gFnSc1	aglomerace
Dallas-Fort	Dallas-Fort	k1gInSc1	Dallas-Fort
Worth	Worth	k1gInSc1	Worth
metropolitan	metropolitan	k1gInSc1	metropolitan
area	area	k1gFnSc1	area
je	být	k5eAaImIp3nS	být
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
město	město	k1gNnSc1	město
Houston	Houston	k1gInSc1	Houston
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
Texasu	Texas	k1gInSc2	Texas
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejrychleji	rychle	k6eAd3	rychle
rostoucích	rostoucí	k2eAgNnPc2d1	rostoucí
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
měřena	měřen	k2eAgFnSc1d1	měřena
objemem	objem	k1gInSc7	objem
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patnáctá	patnáctý	k4xOgFnSc1	patnáctý
největší	veliký	k2eAgFnSc1d3	veliký
ekonomika	ekonomika	k1gFnSc1	ekonomika
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
měl	mít	k5eAaImAgInS	mít
Texas	Texas	k1gInSc1	Texas
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
8,4	[number]	k4	8,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
nízkými	nízký	k2eAgFnPc7d1	nízká
daněmi	daň	k1gFnPc7	daň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
společnosti	společnost	k1gFnSc2	společnost
Tax	taxa	k1gFnPc2	taxa
Foundation	Foundation	k1gInSc4	Foundation
je	být	k5eAaImIp3nS	být
břemeno	břemeno	k1gNnSc1	břemeno
daňového	daňový	k2eAgNnSc2d1	daňové
zatížení	zatížení	k1gNnSc2	zatížení
sedmé	sedmý	k4xOgFnSc2	sedmý
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
pořád	pořád	k6eAd1	pořád
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
ekonomiky	ekonomika	k1gFnSc2	ekonomika
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc4	pěstování
obilnin	obilnina	k1gFnPc2	obilnina
a	a	k8xC	a
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
je	být	k5eAaImIp3nS	být
též	též	k9	též
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
vápna	vápno	k1gNnSc2	vápno
<g/>
,	,	kIx,	,
drceného	drcený	k2eAgNnSc2d1	drcené
kameniva	kamenivo	k1gNnSc2	kamenivo
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
ale	ale	k8xC	ale
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejvíc	nejvíc	k6eAd1	nejvíc
větrné	větrný	k2eAgFnSc2d1	větrná
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
největší	veliký	k2eAgFnSc1d3	veliký
větrná	větrný	k2eAgFnSc1d1	větrná
elektrárna	elektrárna	k1gFnSc1	elektrárna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
Roscoe	Roscoe	k1gFnSc1	Roscoe
Wind	Wind	k1gMnSc1	Wind
Farm	Farm	k1gMnSc1	Farm
v	v	k7c6	v
Roscoe	Roscoe	k1gFnSc6	Roscoe
<g/>
.	.	kIx.	.
</s>
<s>
Aglomerace	aglomerace	k1gFnSc1	aglomerace
Dallas-Fort	Dallas-Fort	k1gInSc1	Dallas-Fort
Worth	Worth	k1gMnSc1	Worth
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
hustotu	hustota	k1gFnSc4	hustota
obchodních	obchodní	k2eAgNnPc2d1	obchodní
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
systémem	systém	k1gInSc7	systém
vysokého	vysoký	k2eAgNnSc2d1	vysoké
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
podnikatelských	podnikatelský	k2eAgFnPc2d1	podnikatelská
nadací	nadace	k1gFnPc2	nadace
jako	jako	k9	jako
Texas	Texas	kA	Texas
Enterprise	Enterprise	k1gFnSc1	Enterprise
Fund	fund	k1gInSc1	fund
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Texas	Texas	k1gInSc1	Texas
Emerging	Emerging	k1gInSc1	Emerging
Technology	technolog	k1gMnPc4	technolog
Fund	fund	k1gInSc1	fund
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
státě	stát	k1gInSc6	stát
vysoce	vysoce	k6eAd1	vysoce
diverzifikované	diverzifikovaný	k2eAgNnSc4d1	diverzifikované
odvětví	odvětví	k1gNnSc4	odvětví
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
severně	severně	k6eAd1	severně
od	od	k7c2	od
Austinu	Austin	k1gInSc2	Austin
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgFnPc4d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Sillicon	Sillicon	k1gNnSc1	Sillicon
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
"	"	kIx"	"
a	a	k8xC	a
území	území	k1gNnSc4	území
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dallasu	Dallas	k1gInSc2	Dallas
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Sillicon	Sillicon	k1gNnSc1	Sillicon
Prairie	Prairie	k1gFnSc2	Prairie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
centrály	centrála	k1gFnPc1	centrála
mnohé	mnohý	k2eAgFnPc4d1	mnohá
známé	známý	k2eAgFnPc4d1	známá
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Dell	Dell	kA	Dell
<g/>
,	,	kIx,	,
Texas	Texas	kA	Texas
Instruments	Instruments	kA	Instruments
<g/>
,	,	kIx,	,
Perrot	Perrot	k1gInSc4	Perrot
Systems	Systemsa	k1gFnPc2	Systemsa
a	a	k8xC	a
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T.	T.	kA	T.
Texasané	Texasan	k1gMnPc1	Texasan
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
potíže	potíž	k1gFnSc2	potíž
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
části	část	k1gFnSc2	část
státu	stát	k1gInSc2	stát
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
velké	velký	k2eAgFnSc2d1	velká
rozlohy	rozloha	k1gFnSc2	rozloha
a	a	k8xC	a
drsného	drsný	k2eAgInSc2d1	drsný
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
127	[number]	k4	127
999	[number]	k4	999
km	km	kA	km
veřejných	veřejný	k2eAgFnPc2d1	veřejná
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
17	[number]	k4	17
úseků	úsek	k1gInPc2	úsek
dálnic	dálnice	k1gFnPc2	dálnice
je	být	k5eAaImIp3nS	být
zpoplatněných	zpoplatněný	k2eAgFnPc2d1	zpoplatněná
<g/>
.	.	kIx.	.
</s>
<s>
Celostátní	celostátní	k2eAgFnSc1d1	celostátní
dálnice	dálnice	k1gFnSc1	dálnice
I10	I10	k1gFnSc2	I10
a	a	k8xC	a
I	i	k9	i
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
procházejí	procházet	k5eAaImIp3nP	procházet
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
rychlostní	rychlostní	k2eAgInSc4d1	rychlostní
limit	limit	k1gInSc4	limit
80	[number]	k4	80
mil	míle	k1gFnPc2	míle
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
/	/	kIx~	/
<g/>
130	[number]	k4	130
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíc	nejvíc	k6eAd1	nejvíc
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
má	mít	k5eAaImIp3nS	mít
víc	hodně	k6eAd2	hodně
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
jiný	jiný	k2eAgInSc1d1	jiný
stát	stát	k1gInSc1	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
letiště	letiště	k1gNnSc1	letiště
co	co	k9	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
i	i	k8xC	i
počtu	počet	k1gInSc2	počet
přepravených	přepravený	k2eAgMnPc2d1	přepravený
pasažérů	pasažér	k1gMnPc2	pasažér
je	být	k5eAaImIp3nS	být
Fort	Fort	k?	Fort
Worth	Worth	k1gMnSc1	Worth
Dallas	Dallas	k1gMnSc1	Dallas
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc4	Airport
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
je	být	k5eAaImIp3nS	být
houstonské	houstonský	k2eAgNnSc4d1	houstonské
letiště	letiště	k1gNnSc4	letiště
George	Georg	k1gMnSc2	Georg
Bush	Bush	k1gMnSc1	Bush
Intercontinental	Intercontinental	k1gMnSc1	Intercontinental
Airport	Airport	k1gInSc4	Airport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
je	být	k5eAaImIp3nS	být
1150	[number]	k4	1150
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
houstonský	houstonský	k2eAgInSc1d1	houstonský
Port	port	k1gInSc1	port
of	of	k?	of
Houston	Houston	k1gInSc1	Houston
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
objem	objem	k1gInSc4	objem
přepravy	přeprava	k1gFnSc2	přeprava
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
zboží	zboží	k1gNnSc2	zboží
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
měl	mít	k5eAaImAgInS	mít
Texas	Texas	k1gInSc1	Texas
22	[number]	k4	22
540	[number]	k4	540
km	km	kA	km
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
je	být	k5eAaImIp3nS	být
1150	[number]	k4	1150
mořských	mořský	k2eAgInPc2d1	mořský
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
lemují	lemovat	k5eAaImIp3nP	lemovat
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
texaské	texaský	k2eAgNnSc4d1	texaské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Přístavy	přístav	k1gInPc1	přístav
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
přeloží	přeložit	k5eAaPmIp3nS	přeložit
317	[number]	k4	317
milionů	milion	k4xCgInPc2	milion
metrických	metrický	k2eAgInPc2d1	metrický
ton	ton	k?	ton
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Přístavy	přístav	k1gInPc1	přístav
Texasu	Texas	k1gInSc2	Texas
jsou	být	k5eAaImIp3nP	být
propojeny	propojit	k5eAaPmNgInP	propojit
s	s	k7c7	s
přístavy	přístav	k1gInPc7	přístav
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
kanály	kanál	k1gInPc4	kanál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Gulf	Gulf	k1gInSc1	Gulf
Intracoastal	Intracoastal	k1gMnSc2	Intracoastal
Waterway	Waterwaa	k1gMnSc2	Waterwaa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Intracoastal	Intracoastal	k1gFnSc2	Intracoastal
Waterway	Waterwaa	k1gFnSc2	Waterwaa
<g/>
.	.	kIx.	.
</s>
<s>
Houstonský	Houstonský	k2eAgInSc1d1	Houstonský
přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejvytíženější	vytížený	k2eAgInSc1d3	nejvytíženější
přístav	přístav	k1gInSc1	přístav
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
překládky	překládka	k1gFnSc2	překládka
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
objemu	objem	k1gInSc6	objem
překládky	překládka	k1gFnSc2	překládka
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
desátý	desátý	k4xOgInSc1	desátý
největší	veliký	k2eAgInSc1d3	veliký
přístav	přístav	k1gInSc1	přístav
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
kultura	kultura	k1gFnSc1	kultura
Texasu	Texas	k1gInSc2	Texas
propojením	propojení	k1gNnSc7	propojení
jižanských	jižanský	k2eAgInPc2d1	jižanský
<g/>
,	,	kIx,	,
západních	západní	k2eAgInPc2d1	západní
a	a	k8xC	a
jihozápadních	jihozápadní	k2eAgInPc2d1	jihozápadní
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Fort	Fort	k?	Fort
Worth	Worth	k1gInSc1	Worth
má	mít	k5eAaImIp3nS	mít
širokou	široký	k2eAgFnSc4d1	široká
nabídku	nabídka	k1gFnSc4	nabídka
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejstaršího	starý	k2eAgNnSc2d3	nejstarší
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnPc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Modern	Modern	k1gNnSc1	Modern
Art	Art	k1gFnPc2	Art
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Fort	Fort	k?	Fort
Worth	Worth	k1gInSc1	Worth
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
Deep	Deep	k1gInSc1	Deep
Ellum	Ellum	k1gInSc1	Ellum
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
blues	blues	k1gNnSc2	blues
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Austin	Austin	k1gMnSc1	Austin
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
živé	živý	k2eAgFnSc2d1	živá
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
a	a	k8xC	a
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
představitelů	představitel	k1gMnPc2	představitel
města	město	k1gNnSc2	město
hostí	hostit	k5eAaImIp3nP	hostit
více	hodně	k6eAd2	hodně
živých	živý	k2eAgNnPc2d1	živé
hudebních	hudební	k2eAgNnPc2d1	hudební
vystoupení	vystoupení	k1gNnPc2	vystoupení
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
než	než	k8xS	než
hudební	hudební	k2eAgNnPc1d1	hudební
centra	centrum	k1gNnPc1	centrum
jako	jako	k8xC	jako
Nashville	Nashville	k1gFnPc1	Nashville
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Memphis	Memphis	k1gFnSc1	Memphis
<g/>
.	.	kIx.	.
</s>
<s>
Otcem	otec	k1gMnSc7	otec
texaského	texaský	k2eAgNnSc2d1	texaské
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgMnSc1	druhý
prezident	prezident	k1gMnSc1	prezident
Texaské	texaský	k2eAgFnSc2d1	texaská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Mirabeau	Mirabeaus	k1gInSc2	Mirabeaus
B.	B.	kA	B.
Lamar	Lamar	k1gMnSc1	Lamar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gNnSc2	on
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
každá	každý	k3xTgNnPc4	každý
county	counta	k1gFnPc4	counta
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
musela	muset	k5eAaImAgFnS	muset
přidělit	přidělit	k5eAaPmF	přidělit
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
budování	budování	k1gNnSc2	budování
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
šest	šest	k4xCc1	šest
státních	státní	k2eAgInPc2d1	státní
univerzitních	univerzitní	k2eAgInPc2d1	univerzitní
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc1	čtyři
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
jsou	být	k5eAaImIp3nP	být
University	universita	k1gFnPc4	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
a	a	k8xC	a
A	A	kA	A
<g/>
&	&	k?	&
<g/>
M	M	kA	M
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
universitní	universitní	k2eAgInPc1d1	universitní
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
University	universita	k1gFnPc4	universita
of	of	k?	of
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
North	North	k1gInSc1	North
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
a	a	k8xC	a
Texas	Texas	k1gInSc1	Texas
Tech	Tech	k?	Tech
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
soukromých	soukromý	k2eAgFnPc2d1	soukromá
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
Rice	Ric	k1gFnSc2	Ric
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
univerzit	univerzita	k1gFnPc2	univerzita
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
podle	podle	k7c2	podle
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
News	News	k1gInSc1	News
and	and	k?	and
World	World	k1gInSc1	World
Report	report	k1gInSc1	report
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
sedmnáctém	sedmnáctý	k4xOgNnSc6	sedmnáctý
místě	místo	k1gNnSc6	místo
řebříčku	řebříček	k1gInSc2	řebříček
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
univerzit	univerzita	k1gFnPc2	univerzita
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
na	na	k7c6	na
špici	špice	k1gFnSc6	špice
zájmu	zájem	k1gInSc2	zájem
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
provozuje	provozovat	k5eAaImIp3nS	provozovat
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
sportovních	sportovní	k2eAgFnPc2d1	sportovní
profesionálních	profesionální	k2eAgFnPc2d1	profesionální
soutěží	soutěž	k1gFnPc2	soutěž
(	(	kIx(	(
<g/>
Big	Big	k1gMnSc1	Big
Four	Four	k1gMnSc1	Four
<g/>
,	,	kIx,	,
NFL	NFL	kA	NFL
<g/>
,	,	kIx,	,
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
NBA	NBA	kA	NBA
a	a	k8xC	a
MLB	MLB	kA	MLB
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
Texas	Texas	k1gInSc1	Texas
zastoupení	zastoupení	k1gNnSc2	zastoupení
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
National	National	k1gFnSc6	National
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gFnPc2	League
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
kluby	klub	k1gInPc1	klub
Dallas	Dallas	k1gInSc1	Dallas
Cowboys	Cowboys	k1gInSc1	Cowboys
a	a	k8xC	a
Houston	Houston	k1gInSc1	Houston
Texans	Texansa	k1gFnPc2	Texansa
<g/>
,	,	kIx,	,
v	v	k7c6	v
NHL	NHL	kA	NHL
klub	klub	k1gInSc1	klub
Dallas	Dallas	k1gInSc1	Dallas
Stars	Stars	k1gInSc4	Stars
<g/>
,	,	kIx,	,
v	v	k7c6	v
Major	major	k1gMnSc1	major
League	Leagu	k1gInSc2	Leagu
Baseball	baseball	k1gInSc1	baseball
Texas	Texas	k1gInSc1	Texas
Rangers	Rangers	k1gInSc1	Rangers
a	a	k8xC	a
Houston	Houston	k1gInSc1	Houston
Astros	Astrosa	k1gFnPc2	Astrosa
a	a	k8xC	a
v	v	k7c6	v
NBA	NBA	kA	NBA
kluby	klub	k1gInPc1	klub
Houston	Houston	k1gInSc1	Houston
Rockets	Rockets	k1gInSc1	Rockets
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Antonio	Antonio	k1gMnSc1	Antonio
Spurs	Spurs	k1gInSc1	Spurs
a	a	k8xC	a
Dallas	Dallas	k1gInSc1	Dallas
Mavericks	Mavericksa	k1gFnPc2	Mavericksa
<g/>
.	.	kIx.	.
</s>
<s>
Texaské	texaský	k2eAgFnPc1d1	texaská
střední	střední	k2eAgFnPc1d1	střední
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
univerzity	univerzita	k1gFnPc1	univerzita
mají	mít	k5eAaImIp3nP	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
atletických	atletický	k2eAgInPc6d1	atletický
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Texasané	Texasan	k1gMnPc1	Texasan
ovšem	ovšem	k9	ovšem
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
také	také	k9	také
rodeo	rodeo	k1gNnSc4	rodeo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
rodeo	rodeo	k1gNnSc1	rodeo
na	na	k7c6	na
světe	svět	k1gInSc5	svět
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Pecosu	Pecos	k1gInSc6	Pecos
<g/>
,	,	kIx,	,
horském	horský	k2eAgNnSc6d1	horské
městečku	městečko	k1gNnSc6	městečko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Texasu	Texas	k1gInSc2	Texas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
se	se	k3xPyFc4	se
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
koná	konat	k5eAaImIp3nS	konat
Houston	Houston	k1gInSc1	Houston
Livestock	Livestocka	k1gFnPc2	Livestocka
Show	show	k1gFnPc2	show
and	and	k?	and
Rodeo	rodeo	k1gNnSc4	rodeo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc4d3	veliký
soutěž	soutěž	k1gFnSc4	soutěž
rodea	rodeo	k1gNnSc2	rodeo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
hostoval	hostovat	k5eAaImAgInS	hostovat
Austin	Austin	k1gInSc1	Austin
soutěž	soutěž	k1gFnSc1	soutěž
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
soutěž	soutěž	k1gFnSc1	soutěž
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Watkins	Watkinsa	k1gFnPc2	Watkinsa
Glen	Glen	k1gMnSc1	Glen
International	International	k1gMnSc1	International
<g/>
.	.	kIx.	.
</s>
