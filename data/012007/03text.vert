<p>
<s>
Poklad	poklad	k1gInSc1	poklad
Černého	Černý	k1gMnSc2	Černý
delfína	delfín	k1gMnSc2	delfín
je	být	k5eAaImIp3nS	být
dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
dvou	dva	k4xCgMnPc6	dva
kamarádech	kamarád	k1gMnPc6	kamarád
Standovi	Standa	k1gMnSc3	Standa
a	a	k8xC	a
Lojzkovi	Lojzek	k1gMnSc3	Lojzek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
radlického	radlický	k2eAgInSc2d1	radlický
turistického	turistický	k2eAgInSc2d1	turistický
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
skautského	skautský	k2eAgInSc2d1	skautský
<g/>
)	)	kIx)	)
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
Foglarových	Foglarův	k2eAgInPc2d1	Foglarův
románů	román	k1gInPc2	román
je	být	k5eAaImIp3nS	být
děj	děj	k1gInSc1	děj
konkrétně	konkrétně	k6eAd1	konkrétně
lokalizován	lokalizovat	k5eAaBmNgInS	lokalizovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Standa	Standa	k1gMnSc1	Standa
Melichar	Melichar	k1gMnSc1	Melichar
a	a	k8xC	a
Lojzek	Lojzek	k1gMnSc1	Lojzek
Nosek	Nosek	k1gMnSc1	Nosek
jsou	být	k5eAaImIp3nP	být
kamarádi	kamarád	k1gMnPc1	kamarád
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
toulají	toulat	k5eAaImIp3nP	toulat
po	po	k7c6	po
městě	město	k1gNnSc6	město
a	a	k8xC	a
touží	toužit	k5eAaImIp3nP	toužit
zažívat	zažívat	k5eAaImF	zažívat
nějaká	nějaký	k3yIgNnPc4	nějaký
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
se	se	k3xPyFc4	se
přimotají	přimotat	k5eAaPmIp3nP	přimotat
ke	k	k7c3	k
skautské	skautský	k2eAgFnSc3d1	skautská
hře	hra	k1gFnSc3	hra
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
pomůžou	pomůžou	k?	pomůžou
chlapci	chlapec	k1gMnSc3	chlapec
Jindrovi	Jindra	k1gMnSc3	Jindra
ohroženému	ohrožený	k2eAgMnSc3d1	ohrožený
přesilou	přesila	k1gFnSc7	přesila
násilnické	násilnický	k2eAgFnSc2d1	násilnická
party	parta	k1gFnSc2	parta
"	"	kIx"	"
<g/>
Stepních	stepní	k2eAgMnPc2d1	stepní
vlků	vlk	k1gMnPc2	vlk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
Modré	modrý	k2eAgFnSc2d1	modrá
družiny	družina	k1gFnSc2	družina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zrovna	zrovna	k6eAd1	zrovna
volná	volný	k2eAgNnPc4d1	volné
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Jindrou	Jindra	k1gFnSc7	Jindra
sdílí	sdílet	k5eAaImIp3nS	sdílet
jeho	jeho	k3xOp3gNnSc1	jeho
výbušné	výbušný	k2eAgNnSc1d1	výbušné
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
že	že	k8xS	že
totiž	totiž	k9	totiž
při	při	k7c6	při
zmíněném	zmíněný	k2eAgNnSc6d1	zmíněné
přepadení	přepadení	k1gNnSc6	přepadení
ztratil	ztratit	k5eAaPmAgInS	ztratit
družinovou	družinový	k2eAgFnSc4d1	družinová
kroniku	kronika	k1gFnSc4	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Standa	Standa	k1gMnSc1	Standa
s	s	k7c7	s
Lojzkem	Lojzek	k1gMnSc7	Lojzek
Jindru	Jindra	k1gFnSc4	Jindra
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
kryjí	krýt	k5eAaImIp3nP	krýt
výmluvami	výmluva	k1gFnPc7	výmluva
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
kroniku	kronika	k1gFnSc4	kronika
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
půjčenou	půjčený	k2eAgFnSc4d1	půjčená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
družiny	družina	k1gFnSc2	družina
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
nastaveném	nastavený	k2eAgInSc6d1	nastavený
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
knihu	kniha	k1gFnSc4	kniha
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
Výmluvný	výmluvný	k2eAgMnSc1d1	výmluvný
Lojzek	Lojzek	k1gMnSc1	Lojzek
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vytáčkách	vytáčka	k1gFnPc6	vytáčka
obratnější	obratní	k2eAgFnSc2d2	obratní
než	než	k8xS	než
poctivec	poctivec	k1gMnSc1	poctivec
Standa	Standa	k1gMnSc1	Standa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
házet	házet	k5eAaImF	házet
vinu	vina	k1gFnSc4	vina
za	za	k7c4	za
stále	stále	k6eAd1	stále
prodlužovanou	prodlužovaný	k2eAgFnSc4d1	prodlužovaná
zápůjčku	zápůjčka	k1gFnSc4	zápůjčka
spíše	spíše	k9	spíše
na	na	k7c4	na
Standu	Standa	k1gMnSc4	Standa
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
proto	proto	k8xC	proto
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
nevraživost	nevraživost	k1gFnSc1	nevraživost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvrcholí	vyvrcholit	k5eAaPmIp3nS	vyvrcholit
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
dobytí	dobytí	k1gNnSc4	dobytí
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
spolu	spolu	k6eAd1	spolu
přestanou	přestat	k5eAaPmIp3nP	přestat
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
zjistí	zjistit	k5eAaPmIp3nS	zjistit
Jindra	Jindra	k1gFnSc1	Jindra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zničený	zničený	k2eAgMnSc1d1	zničený
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
přiznat	přiznat	k5eAaPmF	přiznat
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ale	ale	k9	ale
vztah	vztah	k1gInSc1	vztah
Standy	Standa	k1gMnSc2	Standa
s	s	k7c7	s
Lojzkem	Lojzek	k1gMnSc7	Lojzek
neurovná	urovnat	k5eNaPmIp3nS	urovnat
<g/>
,	,	kIx,	,
a	a	k8xC	a
Standa	Standa	k1gMnSc1	Standa
proto	proto	k8xC	proto
uvítá	uvítat	k5eAaPmIp3nS	uvítat
možnost	možnost	k1gFnSc4	možnost
přesunout	přesunout	k5eAaPmF	přesunout
se	se	k3xPyFc4	se
do	do	k7c2	do
Žluté	žlutý	k2eAgFnSc2d1	žlutá
družiny	družina	k1gFnSc2	družina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
Lojzkem	Lojzek	k1gMnSc7	Lojzek
nebude	být	k5eNaImBp3nS	být
muset	muset	k5eAaImF	muset
tolik	tolik	k6eAd1	tolik
vídat	vídat	k5eAaImF	vídat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jemu	on	k3xPp3gMnSc3	on
samotnému	samotný	k2eAgMnSc3d1	samotný
podaří	podařit	k5eAaPmIp3nS	podařit
za	za	k7c2	za
dramatických	dramatický	k2eAgFnPc2d1	dramatická
okolností	okolnost	k1gFnPc2	okolnost
kroniku	kronika	k1gFnSc4	kronika
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
prvního	první	k4xOgMnSc4	první
pak	pak	k6eAd1	pak
potká	potkat	k5eAaPmIp3nS	potkat
oddílového	oddílový	k2eAgMnSc4d1	oddílový
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
knihu	kniha	k1gFnSc4	kniha
vezme	vzít	k5eAaPmIp3nS	vzít
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
nález	nález	k1gInSc4	nález
zatím	zatím	k6eAd1	zatím
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
pak	pak	k6eAd1	pak
jede	jet	k5eAaImIp3nS	jet
na	na	k7c4	na
letní	letní	k2eAgInSc4d1	letní
tábor	tábor	k1gInSc4	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
divoké	divoký	k2eAgFnSc6d1	divoká
krajině	krajina	k1gFnSc6	krajina
se	s	k7c7	s
skalami	skála	k1gFnPc7	skála
a	a	k8xC	a
vodopády	vodopád	k1gInPc7	vodopád
koná	konat	k5eAaImIp3nS	konat
velká	velký	k2eAgFnSc1d1	velká
táborová	táborový	k2eAgFnSc1d1	táborová
hra	hra	k1gFnSc1	hra
o	o	k7c4	o
"	"	kIx"	"
<g/>
poklad	poklad	k1gInSc4	poklad
Černého	Černý	k1gMnSc2	Černý
delfína	delfín	k1gMnSc2	delfín
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
Standa	Standa	k1gFnSc1	Standa
s	s	k7c7	s
Lojzkem	Lojzek	k1gMnSc7	Lojzek
náhodou	náhodou	k6eAd1	náhodou
octnou	octnout	k5eAaPmIp3nP	octnout
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
tváří	tvář	k1gFnPc2	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
si	se	k3xPyFc3	se
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
nedorozumění	nedorozumění	k1gNnSc4	nedorozumění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Lojzek	Lojzek	k1gMnSc1	Lojzek
přizná	přiznat	k5eAaPmIp3nS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Standu	Standa	k1gFnSc4	Standa
podezříval	podezřívat	k5eAaImAgMnS	podezřívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kroniku	kronika	k1gFnSc4	kronika
někde	někde	k6eAd1	někde
zašantročil	zašantročit	k5eAaPmAgMnS	zašantročit
<g/>
,	,	kIx,	,
a	a	k8xC	a
Standa	Standa	k1gMnSc1	Standa
mu	on	k3xPp3gMnSc3	on
vyjeví	vyjevit	k5eAaPmIp3nS	vyjevit
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
hoši	hoch	k1gMnPc1	hoch
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
rozhovorem	rozhovor	k1gInSc7	rozhovor
zase	zase	k9	zase
udobří	udobřit	k5eAaPmIp3nS	udobřit
<g/>
.	.	kIx.	.
</s>
<s>
Standova	Standův	k2eAgFnSc1d1	Standova
skupina	skupina	k1gFnSc1	skupina
i	i	k9	i
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
důvtipu	důvtip	k1gInSc3	důvtip
v	v	k7c6	v
napínavé	napínavý	k2eAgFnSc6d1	napínavá
hře	hra	k1gFnSc6	hra
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
největší	veliký	k2eAgFnSc1d3	veliký
radost	radost	k1gFnSc1	radost
má	mít	k5eAaImIp3nS	mít
nakonec	nakonec	k6eAd1	nakonec
poslední	poslední	k2eAgFnSc1d1	poslední
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Modré	modrý	k2eAgFnSc2d1	modrá
družiny	družina	k1gFnSc2	družina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jim	on	k3xPp3gMnPc3	on
jako	jako	k9	jako
cenu	cena	k1gFnSc4	cena
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
připravil	připravit	k5eAaPmAgMnS	připravit
právě	právě	k9	právě
jejich	jejich	k3xOp3gFnSc4	jejich
už	už	k6eAd1	už
oplakanou	oplakaný	k2eAgFnSc4d1	oplakaná
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
kroniku	kronika	k1gFnSc4	kronika
<g/>
.	.	kIx.	.
</s>
</p>
