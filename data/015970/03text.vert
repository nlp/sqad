<s>
Seznam	seznam	k1gInSc1
znaků	znak	k1gInPc2
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Velký	velký	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
USA	USA	kA
</s>
<s>
Znaky	znak	k1gInPc1
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgMnPc2d1
ukazují	ukazovat	k5eAaImIp3nP
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
místních	místní	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
<g/>
,	,	kIx,
prvky	prvek	k1gInPc1
z	z	k7c2
historie	historie	k1gFnSc2
těchto	tento	k3xDgInPc2
států	stát	k1gInPc2
a	a	k8xC
také	také	k9
různé	různý	k2eAgInPc4d1
principy	princip	k1gInPc4
návrhu	návrh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
vlajky	vlajka	k1gFnSc2
nebo	nebo	k8xC
pečeti	pečeť	k1gFnSc6
dalším	další	k2eAgInSc7d1
oficiálním	oficiální	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Coat	Coat	k2eAgInSc1d1
of	of	k?
arms	arms	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pečetí	pečeť	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Seal	Seal	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
užívají	užívat	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
Seznam	seznam	k1gInSc1
pečetí	pečeť	k1gFnPc2
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přijalo	přijmout	k5eAaPmAgNnS
oficiálně	oficiálně	k6eAd1
pouze	pouze	k6eAd1
18	#num#	k4
států	stát	k1gInPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Státy	stát	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInPc4d1
znaky	znak	k1gInPc4
států	stát	k1gInPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Stát	stát	k1gInSc1
/	/	kIx~
článek	článek	k1gInSc1
</s>
<s>
Přijetí	přijetí	k1gNnSc1
</s>
<s>
Motto	motto	k1gNnSc1
nebo	nebo	k8xC
text	text	k1gInSc1
na	na	k7c6
znaku	znak	k1gInSc6
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
AlabamaZnak	AlabamaZnak	k6eAd1
Alabamy	Alabam	k1gInPc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
státní	státní	k2eAgFnSc7d1
legislativou	legislativa	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
AUDEMUS	AUDEMUS	kA
JURA	Jura	k1gMnSc1
NOSTRA	NOSTRA	kA
DEFENDERE	DEFENDERE	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Dovolíme	dovolit	k5eAaPmIp1nP
si	se	k3xPyFc3
hájit	hájit	k5eAaImF
svá	svůj	k3xOyFgNnPc4
práva	právo	k1gNnPc4
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
ConnecticutZnak	ConnecticutZnak	k1gInSc1
Connecticutu	Connecticut	k1gInSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1931	#num#	k4
(	(	kIx(
<g/>
státní	státní	k2eAgFnSc7d1
legislativou	legislativa	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
QUI	QUI	kA
TRANSTULIT	TRANSTULIT	kA
SUSTINET	SUSTINET	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
transplantoval	transplantovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
stále	stále	k6eAd1
udržuje	udržovat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
DelawareZnak	DelawareZnak	k1gInSc1
Delawaru	Delawar	k1gInSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1777	#num#	k4
</s>
<s>
LIBERTY	LIBERTY	kA
AND	Anda	k1gFnPc2
INDEPENDENCE	INDEPENDENCE	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
nezávislost	nezávislost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
MaineZnak	MaineZnak	k6eAd1
Maine	Main	k1gMnSc5
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1820	#num#	k4
</s>
<s>
Dirigo	Dirigo	k6eAd1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Řídím	řídit	k5eAaImIp1nS
nebo	nebo	k8xC
vedu	vést	k5eAaImIp1nS
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
MassachusettsZnak	MassachusettsZnak	k6eAd1
Massachusetts	Massachusetts	k1gNnSc1
</s>
<s>
1775	#num#	k4
(	(	kIx(
<g/>
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
potvrzen	potvrzen	k2eAgInSc1d1
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1780	#num#	k4
</s>
<s>
ENSE	ENSE	kA
PETIT	petit	k1gInSc4
PLACIDAM	PLACIDAM	kA
SUB	sub	k7c4
LIBERTATE	LIBERTATE	kA
QUIETEM	QUIETEM	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Mečem	meč	k1gInSc7
hledáme	hledat	k5eAaImIp1nP
mír	mír	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
mír	mír	k1gInSc4
pouze	pouze	k6eAd1
na	na	k7c6
svobodě	svoboda	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
MichiganZnak	MichiganZnak	k1gInSc1
Michiganu	Michigan	k1gInSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1835	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
ústavním	ústavní	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
upraven	upraven	k2eAgInSc1d1
1911	#num#	k4
</s>
<s>
E	E	kA
PLURIBUS	PLURIBUS	kA
UNUM	UNUM	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Z	z	k7c2
mnoha	mnoho	k4c2
jeden	jeden	k4xCgMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
TUEBOR	TUEBOR	kA
(	(	kIx(
<g/>
Budu	být	k5eAaImBp1nS
se	se	k3xPyFc4
bránit	bránit	k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
SI	si	k1gNnSc1
QUÆ	QUÆ	k1gFnPc2
PENINSULAM	PENINSULAM	kA
AMŒ	AMŒ	k1gFnSc2
CIRCUMSPICE	CIRCUMSPICE	kA
(	(	kIx(
<g/>
Pokud	pokud	k8xS
hledáte	hledat	k5eAaImIp2nP
příjemný	příjemný	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
,	,	kIx,
rozhlédněte	rozhlédnout	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
MississippiZnak	MississippiZnak	k1gInSc1
Mississippi	Mississippi	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
původně	původně	k6eAd1
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1894	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
legislativním	legislativní	k2eAgInSc7d1
výborem	výbor	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
VIRTUTE	VIRTUTE	kA
ET	ET	kA
ARMIS	ARMIS	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Síla	síla	k1gFnSc1
a	a	k8xC
zbraně	zbraň	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
MissouriZnak	MissouriZnak	k1gInSc1
Missouri	Missouri	k1gFnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1822	#num#	k4
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
vlajkou	vlajka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
upraven	upraven	k2eAgInSc1d1
1907	#num#	k4
</s>
<s>
SALUS	SALUS	kA
POPULI	POPULI	kA
SUPREMA	SUPREMA	kA
LEX	LEX	kA
ESTO	ESTO	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Bezpečnost	bezpečnost	k1gFnSc1
lidí	člověk	k1gMnPc2
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
zákonem	zákon	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Polooficiální	polooficiální	k2eAgFnSc1d1
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
státní	státní	k2eAgFnSc6d1
vlajce	vlajka	k1gFnSc6
<g/>
,	,	kIx,
pečeti	pečeť	k1gFnSc6
a	a	k8xC
na	na	k7c6
některých	některý	k3yIgFnPc6
vládních	vládní	k2eAgFnPc6d1
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zákoně	zákon	k1gInSc6
o	o	k7c4
přijetí	přijetí	k1gNnSc4
vlajky	vlajka	k1gFnSc2
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
symbol	symbol	k1gInSc1
označován	označovat	k5eAaImNgInS
jako	jako	k8xS,k8xC
znak	znak	k1gInSc1
</s>
<s>
New	New	k?
JerseyZnak	JerseyZnak	k1gInSc1
New	New	k1gMnSc2
Jersey	Jersea	k1gMnSc2
</s>
<s>
1777	#num#	k4
<g/>
,	,	kIx,
upraven	upraven	k2eAgInSc1d1
1928	#num#	k4
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
pečetí	pečeť	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
LIBERTY	LIBERTY	kA
AND	Anda	k1gFnPc2
PROSPERITY	prosperita	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
prosperita	prosperita	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nenalezen	nalezen	k2eNgInSc1d1
přímý	přímý	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
je	být	k5eAaImIp3nS
užíván	užívat	k5eAaImNgInS
odděleně	odděleně	k6eAd1
od	od	k7c2
státní	státní	k2eAgFnSc2d1
pečeti	pečeť	k1gFnSc2
oficiálním	oficiální	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
</s>
<s>
New	New	k?
YorkZnak	YorkZnak	k1gMnSc1
státu	stát	k1gInSc2
New	New	k1gFnSc2
York	York	k1gInSc1
</s>
<s>
1778	#num#	k4
<g/>
,	,	kIx,
upraven	upraven	k2eAgMnSc1d1
1901	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
pečetí	pečeť	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
EXCELSIOR	EXCELSIOR	kA
–	–	k?
E	E	kA
PLURIBUS	PLURIBUS	kA
UNUM	UNUM	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
Stále	stále	k6eAd1
vzhůru	vzhůru	k6eAd1
–	–	k?
jeden	jeden	k4xCgMnSc1
z	z	k7c2
mnoha	mnoho	k4c2
<g/>
)	)	kIx)
</s>
<s>
Nenalezen	nalezen	k2eNgInSc1d1
přímý	přímý	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
je	být	k5eAaImIp3nS
užíván	užívat	k5eAaImNgInS
odděleně	odděleně	k6eAd1
od	od	k7c2
státní	státní	k2eAgFnSc2d1
pečeti	pečeť	k1gFnSc2
oficiálním	oficiální	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
</s>
<s>
OhioZnak	OhioZnak	k1gInSc1
Ohia	Ohio	k1gNnSc2
</s>
<s>
1953	#num#	k4
<g/>
,	,	kIx,
současný	současný	k2eAgInSc1d1
design	design	k1gInSc1
1996	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
PensylvánieZnak	PensylvánieZnak	k6eAd1
Pensylvánie	Pensylvánie	k1gFnSc1
</s>
<s>
1778	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
VIRTUE	VIRTUE	kA
LIBERTY	LIBERTY	kA
AND	Anda	k1gFnPc2
INDEPENDENCE	INDEPENDENCE	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Ctnost	ctnost	k1gFnSc1
<g/>
,	,	kIx,
svoboda	svoboda	k1gFnSc1
a	a	k8xC
nezávislost	nezávislost	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nenalezen	nalezen	k2eNgInSc1d1
přímý	přímý	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
je	být	k5eAaImIp3nS
užíván	užívat	k5eAaImNgInS
odděleně	odděleně	k6eAd1
od	od	k7c2
státní	státní	k2eAgFnSc2d1
pečeti	pečeť	k1gFnSc2
oficiálním	oficiální	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
</s>
<s>
Rhode	Rhodos	k1gInSc5
IslandZnak	IslandZnak	k1gInSc1
Rhode	Rhodos	k1gInSc5
Islandu	Island	k1gInSc6
</s>
<s>
1881	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1882	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
valným	valný	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
HOPE	HOPE	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Naděje	naděje	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
Severní	severní	k2eAgInSc1d1
DakotaZnak	DakotaZnak	k1gInSc1
Severní	severní	k2eAgFnSc2d1
Dakoty	Dakota	k1gFnSc2
</s>
<s>
1957	#num#	k4
</s>
<s>
STRENGTH	STRENGTH	kA
FROM	FROM	kA
THE	THE	kA
SOIL	SOIL	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Síla	síla	k1gFnSc1
z	z	k7c2
půdy	půda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
TexasZnak	TexasZnak	k1gInSc1
Texasu	Texas	k1gInSc2
</s>
<s>
1845	#num#	k4
<g/>
,	,	kIx,
červen	červen	k1gInSc1
1992	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
stanoveno	stanovit	k5eAaPmNgNnS
ústavou	ústava	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
VermontZnak	VermontZnak	k1gInSc1
Vermontu	Vermont	k1gInSc2
</s>
<s>
1862	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
VERMONT	VERMONT	kA
<g/>
,	,	kIx,
FREEDOM	FREEDOM	kA
AND	Anda	k1gFnPc2
UNITY	unita	k1gMnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
WisconsinZnak	WisconsinZnak	k6eAd1
Wisconsinu	Wisconsin	k2eAgFnSc4d1
</s>
<s>
</s>
<s>
FORWARD	FORWARD	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Vpřed	vpřed	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
E	E	kA
PLURIBUS	PLURIBUS	kA
UNUM	UNUM	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Z	z	k7c2
mnoha	mnoho	k4c2
jeden	jeden	k4xCgInSc4
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
Západní	západní	k2eAgInSc1d1
VirginieZnak	VirginieZnak	k1gInSc1
Západní	západní	k2eAgInSc1d1
Virginie	Virginie	k1gFnSc5
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1863	#num#	k4
(	(	kIx(
<g/>
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
MONTANI	MONTANI	kA
SEMPER	Sempra	k1gFnPc2
LIBERI	LIBERI	kA
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Horolezci	horolezec	k1gMnPc1
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
volní	volní	k2eAgFnSc4d1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
</s>
<s>
Neoficiální	oficiální	k2eNgFnSc1d1,k2eAgFnSc1d1
</s>
<s>
Následující	následující	k2eAgInPc1d1
znaky	znak	k1gInPc1
nebyly	být	k5eNaImAgInP
oficiálně	oficiálně	k6eAd1
přijaty	přijmout	k5eAaPmNgInP
ani	ani	k8xC
se	se	k3xPyFc4
neužívají	užívat	k5eNaImIp3nP
odděleně	odděleně	k6eAd1
od	od	k7c2
státní	státní	k2eAgFnSc2d1
pečeti	pečeť	k1gFnSc2
oficiálním	oficiální	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Stát	stát	k1gInSc1
/	/	kIx~
článek	článek	k1gInSc1
</s>
<s>
Přijetí	přijetí	k1gNnSc1
</s>
<s>
Motto	motto	k1gNnSc1
nebo	nebo	k8xC
text	text	k1gInSc1
na	na	k7c6
znaku	znak	k1gInSc6
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
ArizonaZnak	ArizonaZnak	k1gInSc1
Arizony	Arizona	k1gFnSc2
</s>
<s>
1911	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
pečeť	pečeť	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
DITAT	DITAT	kA
DEUS	DEUS	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
Bůh	bůh	k1gMnSc1
obohacuje	obohacovat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Použit	použit	k2eAgMnSc1d1
na	na	k7c6
arizonské	arizonský	k2eAgFnSc6d1
pečeti	pečeť	k1gFnSc6
</s>
<s>
ColoradoZnak	ColoradoZnak	k1gInSc1
Colorada	Colorado	k1gNnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1877	#num#	k4
(	(	kIx(
<g/>
pečeť	pečeť	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
NIL	Nil	k1gInSc1
SINE	sinus	k1gInSc5
NUMINE	NUMINE	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
Nic	nic	k6eAd1
bez	bez	k7c2
prozřetelnosti	prozřetelnost	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Použit	použit	k2eAgMnSc1d1
na	na	k7c6
coloradské	coloradský	k2eAgFnSc6d1
pečeti	pečeť	k1gFnSc6
</s>
<s>
MarylandZnak	MarylandZnak	k1gInSc1
Marylandu	Maryland	k1gInSc2
</s>
<s>
1874	#num#	k4
<g/>
,	,	kIx,
upraven	upraven	k2eAgInSc1d1
1959	#num#	k4
a	a	k8xC
1969	#num#	k4
(	(	kIx(
<g/>
pečeť	pečeť	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
FATTI	FATTI	kA
MASCHII	MASCHII	kA
<g/>
,	,	kIx,
PAROLE	parole	k1gFnPc1
FEMINE	FEMINE	kA
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Silné	silný	k2eAgInPc1d1
skutky	skutek	k1gInPc1
<g/>
,	,	kIx,
jemná	jemný	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Použit	použit	k2eAgMnSc1d1
na	na	k7c6
marylandské	marylandský	k2eAgFnSc6d1
pečeti	pečeť	k1gFnSc6
</s>
<s>
UtahZnak	UtahZnak	k1gInSc1
Utahu	Utah	k1gInSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1896	#num#	k4
<g/>
,	,	kIx,
upraven	upraven	k2eAgInSc1d1
únor	únor	k1gInSc1
2011	#num#	k4
(	(	kIx(
<g/>
pečeť	pečeť	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
INDUSTRY	INDUSTRY	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
Průmysl	průmysl	k1gInSc1
<g/>
,	,	kIx,
píle	píle	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Použit	použit	k2eAgMnSc1d1
na	na	k7c6
utažské	utažský	k2eAgFnSc6d1
pečeti	pečeť	k1gFnSc6
a	a	k8xC
vlajce	vlajka	k1gFnSc6
</s>
<s>
Nezačleněné	začleněný	k2eNgNnSc1d1
území	území	k1gNnSc1
</s>
<s>
Znaky	znak	k1gInPc1
(	(	kIx(
<g/>
pečeti	pečeť	k1gFnPc1
<g/>
)	)	kIx)
nezačleněných	začleněný	k2eNgNnPc2d1
území	území	k1gNnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc4
(	(	kIx(
<g/>
pečeť	pečeť	k1gFnSc4
<g/>
)	)	kIx)
nezačleněného	začleněný	k2eNgNnSc2d1
území	území	k1gNnSc2
Americké	americký	k2eAgFnSc2d1
Panenské	panenský	k2eAgFnSc2d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Znak	znak	k1gInSc4
(	(	kIx(
<g/>
pečeť	pečeť	k1gFnSc4
<g/>
)	)	kIx)
nezačleněného	začleněný	k2eNgNnSc2d1
území	území	k1gNnSc2
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
</s>
<s>
Znak	znak	k1gInSc1
nezačleněného	začleněný	k2eNgNnSc2d1
území	území	k1gNnSc2
Guam	Guam	k1gMnSc1
</s>
<s>
Znak	znak	k1gInSc1
nezačleněného	začleněný	k2eNgNnSc2d1
území	území	k1gNnSc2
Portoriko	Portoriko	k1gNnSc4
(	(	kIx(
<g/>
znak	znak	k1gInSc1
je	být	k5eAaImIp3nS
rozdílný	rozdílný	k2eAgInSc1d1
od	od	k7c2
pečeti	pečeť	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Znak	znak	k1gInSc4
(	(	kIx(
<g/>
pečeť	pečeť	k1gFnSc4
<g/>
)	)	kIx)
nezačleněného	začleněný	k2eNgNnSc2d1
území	území	k1gNnSc2
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
</s>
<s>
Historické	historický	k2eAgInPc1d1
</s>
<s>
Před	před	k7c7
přistoupením	přistoupení	k1gNnSc7
do	do	k7c2
Unie	unie	k1gFnSc2
měly	mít	k5eAaImAgInP
některé	některý	k3yIgInPc1
nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
svůj	svůj	k3xOyFgInSc4
znak	znak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Havajského	havajský	k2eAgNnSc2d1
království	království	k1gNnSc2
(	(	kIx(
<g/>
~	~	kIx~
<g/>
1850	#num#	k4
<g/>
–	–	k?
<g/>
1893	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Texaské	texaský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
–	–	k?
<g/>
1845	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Official	Official	k1gInSc1
Symbols	Symbols	k1gInSc1
and	and	k?
Emblems	Emblems	k1gInSc1
of	of	k?
Alabama	Alabama	k1gFnSc1
<g/>
↑	↑	k?
https://archive.org/stream/cihm_02950#page/n133/mode/1up/search/seal	https://archive.org/stream/cihm_02950#page/n133/mode/1up/search/seal	k1gInSc1
<g/>
↑	↑	k?
State	status	k1gInSc5
of	of	k?
Mississippi	Mississippi	k1gNnPc2
Symbols	Symbols	k1gInSc1
<g/>
↑	↑	k?
Mississippi	Mississippi	k1gFnSc2
Symbols	Symbolsa	k1gFnPc2
Sheet	Sheet	k1gMnSc1
na	na	k7c6
webu	web	k1gInSc6
sos	sos	k1gInSc1
<g/>
.	.	kIx.
<g/>
ms.	ms.	k?
<g/>
gov	gov	k?
<g/>
↑	↑	k?
Ohio	Ohio	k1gNnSc4
–	–	k?
Coat	Coat	k1gInSc1
of	of	k?
arms	arms	k1gInSc1
of	of	k?
state	status	k1gInSc5
<g/>
↑	↑	k?
Pennsylvania	Pennsylvanium	k1gNnPc1
State	status	k1gInSc5
Coat	Coat	k2eAgInSc1d1
of	of	k?
Arms	Arms	k1gInSc1
<g/>
↑	↑	k?
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
–	–	k?
State	status	k1gInSc5
Symbols	Symbols	k1gInSc1
<g/>
↑	↑	k?
The	The	k1gFnSc1
Texas	Texas	k1gInSc1
State	status	k1gInSc5
Arms	Armsa	k1gFnPc2
<g/>
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Great	Great	k2eAgMnSc1d1
Seal	Seal	k1gMnSc1
of	of	k?
the	the	k?
State	status	k1gInSc5
of	of	k?
Arizona	Arizona	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Státní	státní	k2eAgFnSc1d1
pečeť	pečeť	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Seznam	seznam	k1gInSc1
vlajek	vlajka	k1gFnPc2
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Seznam	seznam	k1gInSc1
pečetí	pečeť	k1gFnSc7
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Seznam	seznam	k1gInSc1
znaků	znak	k1gInPc2
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Seznamy	seznam	k1gInPc1
států	stát	k1gInPc2
USA	USA	kA
podle	podle	k7c2
</s>
<s>
vstupu	vstup	k1gInSc3
do	do	k7c2
Unie	unie	k1gFnSc2
•	•	k?
zkratek	zkratka	k1gFnPc2
•	•	k?
vlajek	vlajka	k1gFnPc2
•	•	k?
hlavních	hlavní	k2eAgNnPc2d1
měst	město	k1gNnPc2
•	•	k?
pečetí	pečetit	k5eAaImIp3nP
•	•	k?
zákonů	zákon	k1gInPc2
týkajících	týkající	k2eAgInPc2d1
se	se	k3xPyFc4
alkoholu	alkohol	k1gInSc2
•	•	k?
rozlohy	rozloha	k1gFnSc2
•	•	k?
Kapitolů	Kapitol	k1gInPc2
•	•	k?
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
•	•	k?
přezdívek	přezdívka	k1gFnPc2
•	•	k?
HDP	HDP	kA
(	(	kIx(
<g/>
per	pero	k1gNnPc2
capita	capitum	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Indexu	index	k1gInSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
•	•	k?
Insignií	insignie	k1gFnPc2
•	•	k?
nejlidnatějších	lidnatý	k2eAgNnPc2d3
měst	město	k1gNnPc2
•	•	k?
střední	střední	k2eAgFnSc2d1
délky	délka	k1gFnSc2
života	život	k1gInSc2
•	•	k?
minimální	minimální	k2eAgFnPc4d1
mzdy	mzda	k1gFnPc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
etymologie	etymologie	k1gFnSc1
názvu	název	k1gInSc2
•	•	k?
států	stát	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
nebyly	být	k5eNaImAgInP
teritorii	teritorium	k1gNnPc7
•	•	k?
obezity	obezita	k1gFnSc2
•	•	k?
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
měst	město	k1gNnPc2
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
•	•	k?
nejlidnatějších	lidnatý	k2eAgNnPc2d3
měst	město	k1gNnPc2
ve	v	k7c6
státech	stát	k1gInPc6
<g/>
)	)	kIx)
•	•	k?
hustoty	hustota	k1gFnSc2
zalidnění	zalidnění	k1gNnSc2
(	(	kIx(
<g/>
města	město	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
chudoby	chudoba	k1gFnSc2
•	•	k?
zákonů	zákon	k1gInPc2
homosexuálních	homosexuální	k2eAgInPc2d1
svazků	svazek	k1gInPc2
•	•	k?
zákazu	zákaz	k1gInSc3
kouření	kouření	k1gNnSc2
•	•	k?
sbírek	sbírka	k1gFnPc2
zákonů	zákon	k1gInPc2
•	•	k?
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
(	(	kIx(
<g/>
ve	v	k7c6
státech	stát	k1gInPc6
Unie	unie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
daní	daň	k1gFnPc2
(	(	kIx(
<g/>
státních	státní	k2eAgFnPc2d1
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
federálních	federální	k2eAgFnPc2d1
daní	daň	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
časových	časový	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
•	•	k?
nezaměstnanosti	nezaměstnanost	k1gFnSc2
•	•	k?
mot	moto	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
