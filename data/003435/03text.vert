<s>
Peří	peří	k1gNnSc1	peří
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
pennae	pennae	k1gInSc1	pennae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tělní	tělní	k2eAgInSc1d1	tělní
pokryv	pokryv	k1gInSc1	pokryv
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
jako	jako	k8xC	jako
takové	takový	k3xDgNnSc1	takový
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
peří	peří	k1gNnSc1	peří
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
i	i	k9	i
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kožní	kožní	k2eAgInSc1d1	kožní
derivát	derivát	k1gInSc1	derivát
tvořený	tvořený	k2eAgInSc1d1	tvořený
zrohovatělou	zrohovatělý	k2eAgFnSc7d1	zrohovatělá
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
naprosto	naprosto	k6eAd1	naprosto
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
účinným	účinný	k2eAgInSc7d1	účinný
tepelným	tepelný	k2eAgInSc7d1	tepelný
izolantem	izolant	k1gInSc7	izolant
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tak	tak	k9	tak
ptákům	pták	k1gMnPc3	pták
udržovat	udržovat	k5eAaImF	udržovat
jejich	jejich	k3xOp3gFnSc4	jejich
vysokou	vysoký	k2eAgFnSc4d1	vysoká
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nP	tvořit
konturu	kontura	k1gFnSc4	kontura
ptačího	ptačí	k2eAgNnSc2d1	ptačí
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
plochu	plocha	k1gFnSc4	plocha
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
ocasu	ocas	k1gInSc2	ocas
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
lehké	lehký	k2eAgNnSc1d1	lehké
-	-	kIx~	-
peří	peří	k1gNnSc1	peří
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ptákům	pták	k1gMnPc3	pták
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
peří	peří	k1gNnSc1	peří
a	a	k8xC	a
opeření	opeření	k1gNnSc1	opeření
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
také	také	k9	také
významným	významný	k2eAgMnSc7d1	významný
druhově	druhově	k6eAd1	druhově
specifickým	specifický	k2eAgInSc7d1	specifický
znakem	znak	k1gInSc7	znak
a	a	k8xC	a
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
patří	patřit	k5eAaImIp3nP	patřit
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
peří	peřit	k5eAaImIp3nS	peřit
ke	k	k7c3	k
znakům	znak	k1gInPc3	znak
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
dimorfismu	dimorfismus	k1gInSc2	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Opeření	opeření	k1gNnSc1	opeření
dospělých	dospělý	k2eAgMnPc2d1	dospělý
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
dvouvrstevné	dvouvrstevný	k2eAgNnSc1d1	dvouvrstevné
-	-	kIx~	-
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
krycího	krycí	k2eAgNnSc2d1	krycí
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
hluboká	hluboký	k2eAgFnSc1d1	hluboká
péřová	péřový	k2eAgFnSc1d1	péřová
vrstva	vrstva	k1gFnSc1	vrstva
tvořená	tvořený	k2eAgFnSc1d1	tvořená
prachovitým	prachovitý	k2eAgMnSc7d1	prachovitý
<g/>
,	,	kIx,	,
prachovým	prachový	k2eAgMnSc7d1	prachový
<g/>
,	,	kIx,	,
štětečkovitým	štětečkovitý	k2eAgNnSc7d1	štětečkovitý
nebo	nebo	k8xC	nebo
vlasovým	vlasový	k2eAgNnSc7d1	vlasové
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
má	mít	k5eAaImIp3nS	mít
hlavně	hlavně	k6eAd1	hlavně
termoregulační	termoregulační	k2eAgFnSc4d1	termoregulační
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
peří	peří	k1gNnSc2	peří
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
stavbou	stavba	k1gFnSc7	stavba
i	i	k8xC	i
uspořádáním	uspořádání	k1gNnSc7	uspořádání
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
základními	základní	k2eAgInPc7d1	základní
typy	typ	k1gInPc7	typ
ptačího	ptačí	k2eAgNnSc2d1	ptačí
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
ptáků	pták	k1gMnPc2	pták
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
štětinovité	štětinovitý	k2eAgNnSc4d1	štětinovitý
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
u	u	k7c2	u
tučnáků	tučnák	k1gInPc2	tučnák
je	být	k5eAaImIp3nS	být
krycí	krycí	k2eAgNnSc1d1	krycí
peří	peří	k1gNnSc1	peří
změněno	změnit	k5eAaPmNgNnS	změnit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tuhých	tuhý	k2eAgFnPc2d1	tuhá
šupin	šupina	k1gFnPc2	šupina
<g/>
,	,	kIx,	,
u	u	k7c2	u
běžců	běžec	k1gMnPc2	běžec
zase	zase	k9	zase
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
praporu	prapor	k1gInSc2	prapor
obrysových	obrysový	k2eAgNnPc2d1	obrysové
per	pero	k1gNnPc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Krycí	krycí	k2eAgNnSc1d1	krycí
peří	peří	k1gNnSc1	peří
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
pennae	pennaat	k5eAaPmIp3nS	pennaat
contourae	contourae	k1gFnSc4	contourae
generales	generalesa	k1gFnPc2	generalesa
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
vnější	vnější	k2eAgFnSc4d1	vnější
vrstvu	vrstva	k1gFnSc4	vrstva
ptačího	ptačí	k2eAgNnSc2d1	ptačí
opeření	opeření	k1gNnSc2	opeření
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
jeho	on	k3xPp3gInSc4	on
obrys	obrys	k1gInSc4	obrys
<g/>
,	,	kIx,	,
na	na	k7c6	na
určitých	určitý	k2eAgFnPc6d1	určitá
částech	část	k1gFnPc6	část
těla	tělo	k1gNnSc2	tělo
také	také	k6eAd1	také
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
specializaci	specializace	k1gFnSc3	specializace
krycích	krycí	k2eAgNnPc2d1	krycí
per	pero	k1gNnPc2	pero
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
létáním	létání	k1gNnSc7	létání
<g/>
.	.	kIx.	.
</s>
<s>
Krycí	krycí	k2eAgNnSc1d1	krycí
peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
obrysovými	obrysový	k2eAgInPc7d1	obrysový
pery	pero	k1gNnPc7	pero
s	s	k7c7	s
pevnými	pevný	k2eAgInPc7d1	pevný
prapory	prapor	k1gInPc7	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
postnatálního	postnatální	k2eAgInSc2d1	postnatální
vývoje	vývoj	k1gInSc2	vývoj
ptáků	pták	k1gMnPc2	pták
narůstá	narůstat	k5eAaImIp3nS	narůstat
nejprve	nejprve	k6eAd1	nejprve
prachový	prachový	k2eAgInSc1d1	prachový
šat	šat	k1gInSc1	šat
mláďat	mládě	k1gNnPc2	mládě
–	–	k?	–
pápěří	pápěří	k1gNnSc4	pápěří
(	(	kIx(	(
<g/>
neoptile	optile	k6eNd1	optile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narůstá	narůstat	k5eAaImIp3nS	narůstat
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
na	na	k7c6	na
určitých	určitý	k2eAgNnPc6d1	určité
místech	místo	k1gNnPc6	místo
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgInPc2d1	zvaný
pernice	pernice	k?	pernice
(	(	kIx(	(
<g/>
pterylae	pterylae	k1gFnSc1	pterylae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
neopeřené	opeřený	k2eNgFnPc4d1	neopeřená
nažiny	nažina	k1gFnPc4	nažina
(	(	kIx(	(
<g/>
apteriae	apteriae	k6eAd1	apteriae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
peří	peří	k1gNnSc2	peří
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
metabolismus	metabolismus	k1gInSc1	metabolismus
ptáků	pták	k1gMnPc2	pták
až	až	k9	až
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jak	jak	k6eAd1	jak
zvýšenými	zvýšený	k2eAgInPc7d1	zvýšený
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
výživu	výživa	k1gFnSc4	výživa
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
vnímavostí	vnímavost	k1gFnSc7	vnímavost
ke	k	k7c3	k
stresorům	stresor	k1gMnPc3	stresor
<g/>
.	.	kIx.	.
</s>
<s>
Opeření	opeření	k1gNnSc1	opeření
dospělých	dospělí	k1gMnPc2	dospělí
(	(	kIx(	(
<g/>
teleoptile	teleoptile	k6eAd1	teleoptile
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
obrysovými	obrysový	k2eAgInPc7d1	obrysový
a	a	k8xC	a
prachovými	prachový	k2eAgNnPc7d1	prachové
pery	pero	k1gNnPc7	pero
<g/>
.	.	kIx.	.
</s>
<s>
Obrysová	obrysový	k2eAgNnPc1d1	obrysové
pera	pero	k1gNnPc1	pero
(	(	kIx(	(
<g/>
pennae	pennae	k1gInSc1	pennae
contourae	contoura	k1gInSc2	contoura
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
všichni	všechen	k3xTgMnPc1	všechen
naši	náš	k3xOp1gMnPc1	náš
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
pera	pero	k1gNnPc4	pero
prachová	prachový	k2eAgNnPc4d1	prachové
jen	jen	k9	jen
někteří	některý	k3yIgMnPc1	některý
<g/>
.	.	kIx.	.
</s>
<s>
Osu	osa	k1gFnSc4	osa
obrysového	obrysový	k2eAgNnSc2d1	obrysové
pera	pero	k1gNnSc2	pero
tvoří	tvořit	k5eAaImIp3nS	tvořit
centrální	centrální	k2eAgInSc1d1	centrální
stvol	stvol	k1gInSc1	stvol
(	(	kIx(	(
<g/>
scapus	scapus	k1gInSc1	scapus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnPc6	jehož
stranách	strana	k1gFnPc6	strana
je	být	k5eAaImIp3nS	být
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
vexillum	vexillum	k1gInSc1	vexillum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
část	část	k1gFnSc1	část
stvolu	stvol	k1gInSc2	stvol
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
prapor	prapor	k1gInSc1	prapor
je	být	k5eAaImIp3nS	být
osten	osten	k1gInSc1	osten
(	(	kIx(	(
<g/>
rhachis	rhachis	k1gInSc1	rhachis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osten	osten	k1gInSc1	osten
má	mít	k5eAaImIp3nS	mít
vrstvu	vrstva	k1gFnSc4	vrstva
korovou	korový	k2eAgFnSc4d1	korová
a	a	k8xC	a
dřeňovou	dřeňový	k2eAgFnSc4d1	dřeňová
<g/>
.	.	kIx.	.
</s>
<s>
Korová	korový	k2eAgFnSc1d1	korová
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
zrohovatělými	zrohovatělý	k2eAgFnPc7d1	zrohovatělá
<g/>
,	,	kIx,	,
plochými	plochý	k2eAgFnPc7d1	plochá
<g/>
,	,	kIx,	,
bezjadernými	bezjaderný	k2eAgFnPc7d1	Bezjaderná
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
pružná	pružný	k2eAgFnSc1d1	pružná
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
vyztužená	vyztužený	k2eAgFnSc1d1	vyztužená
podélnými	podélný	k2eAgFnPc7d1	podélná
ostrými	ostrý	k2eAgFnPc7d1	ostrá
lištami	lišta	k1gFnPc7	lišta
<g/>
.	.	kIx.	.
</s>
<s>
Dřeň	dřeň	k1gFnSc1	dřeň
ostnu	osten	k1gInSc2	osten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
zrohovatělých	zrohovatělý	k2eAgFnPc2d1	zrohovatělá
epitelových	epitelový	k2eAgFnPc2d1	epitelová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
dutinky	dutinka	k1gFnSc2	dutinka
naplněné	naplněný	k2eAgFnSc2d1	naplněná
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
<g/>
,	,	kIx,	,
dutá	dutý	k2eAgFnSc1d1	dutá
část	část	k1gFnSc1	část
stvolu	stvol	k1gInSc2	stvol
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zakotvuje	zakotvovat	k5eAaImIp3nS	zakotvovat
pero	pero	k1gNnSc4	pero
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
a	a	k8xC	a
nenese	nést	k5eNaImIp3nS	nést
prapor	prapor	k1gInSc4	prapor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
brk	brk	k1gInSc1	brk
(	(	kIx(	(
<g/>
calamus	calamus	k1gInSc1	calamus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
válcovitý	válcovitý	k2eAgInSc4d1	válcovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostnu	osten	k1gInSc2	osten
<g/>
,	,	kIx,	,
plochými	plochý	k2eAgFnPc7d1	plochá
bezjadernými	bezjaderný	k2eAgFnPc7d1	Bezjaderná
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
růstu	růst	k1gInSc2	růst
je	být	k5eAaImIp3nS	být
brk	brk	k1gInSc1	brk
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
pulpou	pulpa	k1gFnSc7	pulpa
<g/>
,	,	kIx,	,
po	po	k7c6	po
dozrání	dozrání	k1gNnSc6	dozrání
brku	brk	k1gInSc2	brk
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
brková	brkový	k2eAgFnSc1d1	brkový
duše	duše	k1gFnSc1	duše
<g/>
.	.	kIx.	.
</s>
<s>
Brk	brk	k1gInSc1	brk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
zanořen	zanořit	k5eAaPmNgMnS	zanořit
v	v	k7c6	v
perovém	perový	k2eAgInSc6d1	perový
váčku	váček	k1gInSc6	váček
(	(	kIx(	(
<g/>
folliculus	folliculus	k1gInSc1	folliculus
pennae	penna	k1gInSc2	penna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
epitelové	epitelový	k2eAgFnSc2d1	epitelová
pochvy	pochva	k1gFnSc2	pochva
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
peru	pero	k1gNnSc3	pero
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
vazivové	vazivový	k2eAgFnSc2d1	vazivová
pochvy	pochva	k1gFnSc2	pochva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouzdro	pouzdro	k1gNnSc4	pouzdro
folikulu	folikul	k1gInSc2	folikul
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
vazivové	vazivový	k2eAgFnSc2d1	vazivová
pochvy	pochva	k1gFnSc2	pochva
obrysových	obrysový	k2eAgNnPc2d1	obrysové
per	pero	k1gNnPc2	pero
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgInPc1d1	hladký
svaly	sval	k1gInPc1	sval
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
ovládání	ovládání	k1gNnSc3	ovládání
pera	pero	k1gNnSc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Prapor	prapor	k1gInSc4	prapor
tvoří	tvořit	k5eAaImIp3nP	tvořit
větve	větev	k1gFnPc1	větev
(	(	kIx(	(
<g/>
rami	ram	k1gFnPc1	ram
<g/>
)	)	kIx)	)
vyrůstající	vyrůstající	k2eAgNnSc1d1	vyrůstající
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
ostnu	osten	k1gInSc2	osten
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
větve	větev	k1gFnSc2	větev
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
opět	opět	k6eAd1	opět
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
rovině	rovina	k1gFnSc6	rovina
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
paprsky	paprsek	k1gInPc7	paprsek
(	(	kIx(	(
<g/>
radii	radio	k1gNnPc7	radio
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
paprsky	paprsek	k1gInPc1	paprsek
horní	horní	k2eAgFnSc2d1	horní
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
směřující	směřující	k2eAgFnSc1d1	směřující
ke	k	k7c3	k
špičce	špička	k1gFnSc3	špička
pera	pero	k1gNnSc2	pero
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
háčky	háček	k1gInPc1	háček
(	(	kIx(	(
<g/>
hamuli	hamout	k5eAaPmAgMnP	hamout
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
mikrostrukturami	mikrostruktura	k1gFnPc7	mikrostruktura
(	(	kIx(	(
<g/>
brvy	brva	k1gFnPc1	brva
<g/>
,	,	kIx,	,
cilie	cilie	k1gFnPc1	cilie
<g/>
)	)	kIx)	)
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
se	se	k3xPyFc4	se
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
za	za	k7c4	za
jednodušeji	jednoduše	k6eAd2	jednoduše
stavěné	stavěný	k2eAgInPc4d1	stavěný
paprsky	paprsek	k1gInPc4	paprsek
spodní	spodní	k2eAgFnSc2d1	spodní
řady	řada	k1gFnSc2	řada
–	–	k?	–
tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
souvislá	souvislý	k2eAgFnSc1d1	souvislá
plocha	plocha	k1gFnSc1	plocha
praporu	prapor	k1gInSc2	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Prapor	prapor	k1gInSc4	prapor
pera	pero	k1gNnSc2	pero
pštrosa	pštros	k1gMnSc4	pštros
<g/>
,	,	kIx,	,
emu	emu	k1gMnSc1	emu
a	a	k8xC	a
nandu	nandu	k1gMnSc1	nandu
nemá	mít	k5eNaImIp3nS	mít
paprsky	paprsek	k1gInPc4	paprsek
s	s	k7c7	s
háčky	háček	k1gMnPc7	háček
<g/>
.	.	kIx.	.
</s>
<s>
Krycí	krycí	k2eAgNnPc1d1	krycí
pera	pero	k1gNnPc1	pero
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
zakládají	zakládat	k5eAaImIp3nP	zakládat
dvouvětevně	dvouvětevně	k6eAd1	dvouvětevně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
paosten	paosten	k2eAgInSc1d1	paosten
(	(	kIx(	(
<g/>
hyporhachis	hyporhachis	k1gInSc1	hyporhachis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přisedá	přisedat	k5eAaImIp3nS	přisedat
na	na	k7c6	na
ventrální	ventrální	k2eAgFnSc6d1	ventrální
straně	strana	k1gFnSc6	strana
pera	pero	k1gNnSc2	pero
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
vždy	vždy	k6eAd1	vždy
měkké	měkký	k2eAgFnPc4d1	měkká
větve	větev	k1gFnPc4	větev
a	a	k8xC	a
paprsky	paprsek	k1gInPc4	paprsek
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
různá	různý	k2eAgNnPc4d1	různé
okrasná	okrasný	k2eAgNnPc4d1	okrasné
pera	pero	k1gNnPc4	pero
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
přeměněnými	přeměněný	k2eAgInPc7d1	přeměněný
pery	pero	k1gNnPc7	pero
krycími	krycí	k2eAgInPc7d1	krycí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rýdovacími	rýdovací	k2eAgInPc7d1	rýdovací
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
letu	let	k1gInSc2	let
se	se	k3xPyFc4	se
některá	některý	k3yIgNnPc1	některý
obrysová	obrysový	k2eAgNnPc1d1	obrysové
pera	pero	k1gNnPc1	pero
funkčně	funkčně	k6eAd1	funkčně
specializovala	specializovat	k5eAaBmAgFnS	specializovat
<g/>
:	:	kIx,	:
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
specializace	specializace	k1gFnSc1	specializace
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
na	na	k7c6	na
ocasu	ocas	k1gInSc6	ocas
ptáka	pták	k1gMnSc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Obrysová	obrysový	k2eAgNnPc1d1	obrysové
pera	pero	k1gNnPc1	pero
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
na	na	k7c6	na
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
krycí	krycí	k2eAgInSc1d1	krycí
(	(	kIx(	(
<g/>
tectrices	tectrices	k1gInSc1	tectrices
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
kryjí	krýt	k5eAaImIp3nP	krýt
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tělní	tělní	k2eAgInSc4d1	tělní
obrys	obrys	k1gInSc4	obrys
ptáka	pták	k1gMnSc2	pták
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
letky	letka	k1gFnSc2	letka
(	(	kIx(	(
<g/>
remiges	remiges	k1gInSc1	remiges
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
pera	pero	k1gNnPc1	pero
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
rýdovací	rýdovací	k2eAgInSc1d1	rýdovací
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pera	pero	k1gNnSc2	pero
(	(	kIx(	(
<g/>
rectrices	rectrices	k1gInSc1	rectrices
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
narůstající	narůstající	k2eAgFnSc7d1	narůstající
vějířovitě	vějířovitě	k6eAd1	vějířovitě
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Letky	letka	k1gFnPc1	letka
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
přirůstání	přirůstání	k1gNnSc2	přirůstání
na	na	k7c4	na
letky	letek	k1gInPc4	letek
ruční	ruční	k2eAgInPc4d1	ruční
(	(	kIx(	(
<g/>
primární	primární	k2eAgInPc4d1	primární
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loketní	loketní	k2eAgMnSc1d1	loketní
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgMnSc1d1	sekundární
<g/>
)	)	kIx)	)
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
ramenní	ramenní	k2eAgFnSc6d1	ramenní
(	(	kIx(	(
<g/>
terciární	terciární	k2eAgFnSc6d1	terciární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
typů	typ	k1gInPc2	typ
letek	letka	k1gFnPc2	letka
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
taxonomickým	taxonomický	k2eAgInSc7d1	taxonomický
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Krycí	krycí	k2eAgFnSc1d1	krycí
pera	pero	k1gNnSc2	pero
překrývající	překrývající	k2eAgFnSc6d1	překrývající
bázi	báze	k1gFnSc6	báze
letek	letka	k1gFnPc2	letka
a	a	k8xC	a
rýdovacích	rýdovací	k2eAgNnPc2d1	rýdovací
per	pero	k1gNnPc2	pero
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
krovky	krovka	k1gFnPc1	krovka
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
per	pero	k1gNnPc2	pero
upínající	upínající	k2eAgNnPc4d1	upínající
se	se	k3xPyFc4	se
k	k	k7c3	k
jedinému	jediný	k2eAgInSc3d1	jediný
článku	článek	k1gInSc3	článek
1	[number]	k4	1
<g/>
.	.	kIx.	.
prstu	prst	k1gInSc2	prst
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
křidélko	křidélko	k1gNnSc1	křidélko
(	(	kIx(	(
<g/>
alula	alula	k1gFnSc1	alula
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
za	za	k7c2	za
letu	let	k1gInSc2	let
při	při	k7c6	při
manévrování	manévrování	k1gNnSc6	manévrování
a	a	k8xC	a
přistávání	přistávání	k1gNnSc6	přistávání
<g/>
.	.	kIx.	.
</s>
<s>
Tuhá	tuhý	k2eAgNnPc1d1	tuhé
krycí	krycí	k2eAgNnPc1d1	krycí
pera	pero	k1gNnPc1	pero
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
kostrče	kostrč	k1gFnSc2	kostrč
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
pera	pero	k1gNnPc1	pero
rýdovací	rýdovací	k2eAgNnPc1d1	rýdovací
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
kormidla	kormidlo	k1gNnSc2	kormidlo
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
ocas	ocas	k1gInSc4	ocas
ptáka	pták	k1gMnSc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Shora	shora	k6eAd1	shora
překrývají	překrývat	k5eAaImIp3nP	překrývat
rýdovací	rýdovací	k2eAgNnPc4d1	rýdovací
pera	pero	k1gNnPc4	pero
vrchní	vrchní	k2eAgFnSc2d1	vrchní
ocasní	ocasní	k2eAgFnSc2d1	ocasní
krovky	krovka	k1gFnSc2	krovka
<g/>
,	,	kIx,	,
zespoda	zespoda	k7c2	zespoda
je	on	k3xPp3gMnPc4	on
podepírají	podepírat	k5eAaImIp3nP	podepírat
spodní	spodní	k2eAgFnPc1d1	spodní
ocasní	ocasní	k2eAgFnPc1d1	ocasní
krovky	krovka	k1gFnPc1	krovka
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
ptačích	ptačí	k2eAgNnPc2d1	ptačí
per	pero	k1gNnPc2	pero
jsou	být	k5eAaImIp3nP	být
pera	pero	k1gNnPc1	pero
prachová	prachový	k2eAgNnPc1d1	prachové
(	(	kIx(	(
<g/>
plumae	plumae	k1gNnPc1	plumae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
ptáků	pták	k1gMnPc2	pták
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
obrysovými	obrysový	k2eAgInPc7d1	obrysový
a	a	k8xC	a
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
tepelné	tepelný	k2eAgFnSc6d1	tepelná
izolaci	izolace	k1gFnSc6	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
krátký	krátký	k2eAgInSc4d1	krátký
stvol	stvol	k1gInSc4	stvol
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
větve	větev	k1gFnPc1	větev
netvoří	tvořit	k5eNaImIp3nP	tvořit
prapor	prapor	k1gInSc4	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Paprsky	paprsek	k1gInPc1	paprsek
jsou	být	k5eAaImIp3nP	být
zkrácené	zkrácený	k2eAgInPc1d1	zkrácený
a	a	k8xC	a
nenesou	nést	k5eNaImIp3nP	nést
háčky	háček	k1gInPc1	háček
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prachového	prachový	k2eAgNnSc2d1	prachové
peří	peří	k1gNnSc2	peří
mláďat	mládě	k1gNnPc2	mládě
vycházejí	vycházet	k5eAaImIp3nP	vycházet
větve	větev	k1gFnPc1	větev
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
báze	báze	k1gFnSc2	báze
pera	pero	k1gNnSc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
prachových	prachový	k2eAgNnPc2d1	prachové
per	pero	k1gNnPc2	pero
<g/>
.	.	kIx.	.
</s>
<s>
Prachovitá	prachovitý	k2eAgNnPc1d1	prachovitý
pera	pero	k1gNnPc1	pero
(	(	kIx(	(
<g/>
semiplumae	semipluma	k1gInPc1	semipluma
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
zřetelný	zřetelný	k2eAgInSc4d1	zřetelný
stvol	stvol	k1gInSc4	stvol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prapor	prapor	k1gInSc1	prapor
není	být	k5eNaImIp3nS	být
soudržný	soudržný	k2eAgInSc1d1	soudržný
<g/>
,	,	kIx,	,
větve	větev	k1gFnPc1	větev
se	se	k3xPyFc4	se
nespojují	spojovat	k5eNaImIp3nP	spojovat
a	a	k8xC	a
paprsky	paprsek	k1gInPc1	paprsek
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
při	při	k7c6	při
bázi	báze	k1gFnSc6	báze
zobáku	zobák	k1gInSc2	zobák
<g/>
;	;	kIx,	;
u	u	k7c2	u
hrabavých	hrabavý	k2eAgMnPc2d1	hrabavý
ptáků	pták	k1gMnPc2	pták
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c6	na
nažinách	nažina	k1gFnPc6	nažina
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
zvaný	zvaný	k2eAgInSc1d1	zvaný
drobivý	drobivý	k2eAgInSc1d1	drobivý
prach	prach	k1gInSc1	prach
tvoří	tvořit	k5eAaImIp3nP	tvořit
pera	pero	k1gNnPc1	pero
pudrovitá	pudrovitý	k2eAgNnPc1d1	pudrovitý
(	(	kIx(	(
<g/>
pennae	pennae	k1gNnPc1	pennae
pulveriformes	pulveriformesa	k1gFnPc2	pulveriformesa
<g/>
)	)	kIx)	)
s	s	k7c7	s
brkem	brko	k1gNnSc7	brko
a	a	k8xC	a
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
postranními	postranní	k2eAgFnPc7d1	postranní
vlákny	vlákna	k1gFnSc2	vlákna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
distálním	distální	k2eAgInSc6d1	distální
konci	konec	k1gInSc6	konec
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
drobné	drobný	k2eAgFnPc1d1	drobná
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
prach	prach	k1gInSc1	prach
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
udržující	udržující	k2eAgNnSc1d1	udržující
peří	peří	k1gNnSc1	peří
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
zamezující	zamezující	k2eAgFnSc1d1	zamezující
jeho	jeho	k3xOp3gNnSc4	jeho
vlhnutí	vlhnutí	k1gNnSc4	vlhnutí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
papoušci	papoušek	k1gMnPc1	papoušek
<g/>
,	,	kIx,	,
volavky	volavka	k1gFnPc1	volavka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Štětinovitá	štětinovitý	k2eAgNnPc1d1	štětinovitý
pera	pero	k1gNnPc1	pero
(	(	kIx(	(
<g/>
vibrissae	vibrissa	k1gInPc1	vibrissa
<g/>
,	,	kIx,	,
setae	seta	k1gInPc1	seta
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
hmatovou	hmatový	k2eAgFnSc4d1	hmatová
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
zobáku	zobák	k1gInSc2	zobák
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
lelků	lelek	k1gInPc2	lelek
<g/>
,	,	kIx,	,
rorýsů	rorýs	k1gMnPc2	rorýs
<g/>
,	,	kIx,	,
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlasová	vlasový	k2eAgNnPc1d1	vlasové
pera	pero	k1gNnPc1	pero
(	(	kIx(	(
<g/>
filoplumae	filoplumae	k1gInSc1	filoplumae
<g/>
)	)	kIx)	)
s	s	k7c7	s
tenkým	tenký	k2eAgInSc7d1	tenký
ostnem	osten	k1gInSc7	osten
a	a	k8xC	a
koncovým	koncový	k2eAgInSc7d1	koncový
praporem	prapor	k1gInSc7	prapor
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mezi	mezi	k7c7	mezi
krycím	krycí	k2eAgNnSc7d1	krycí
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Štětečkovitá	Štětečkovitý	k2eAgNnPc1d1	Štětečkovitý
pera	pero	k1gNnPc1	pero
(	(	kIx(	(
<g/>
pennae	pennae	k1gInSc1	pennae
penicilliformes	penicilliformes	k1gInSc1	penicilliformes
<g/>
)	)	kIx)	)
stavbou	stavba	k1gFnSc7	stavba
připomíná	připomínat	k5eAaImIp3nS	připomínat
prachové	prachový	k2eAgNnSc1d1	prachové
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
krátkého	krátký	k2eAgInSc2d1	krátký
brku	brk	k1gInSc2	brk
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
štěteček	štěteček	k1gInSc1	štěteček
tvořený	tvořený	k2eAgInSc1d1	tvořený
větvemi	větev	k1gFnPc7	větev
s	s	k7c7	s
nezaklesnutými	zaklesnutý	k2eNgInPc7d1	zaklesnutý
háčky	háček	k1gInPc7	háček
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
kloaky	kloaka	k1gFnSc2	kloaka
a	a	k8xC	a
kolem	kolem	k7c2	kolem
vyústění	vyústění	k1gNnSc2	vyústění
kostrční	kostrční	k2eAgFnSc2d1	kostrční
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
přepeřování	přepeřování	k1gNnSc2	přepeřování
<g/>
.	.	kIx.	.
</s>
<s>
Přepeřování	přepeřování	k1gNnSc1	přepeřování
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
staré	starý	k2eAgNnSc1d1	staré
peří	peří	k1gNnSc1	peří
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
narůstají	narůstat	k5eAaImIp3nP	narůstat
nová	nový	k2eAgNnPc4d1	nové
pera	pero	k1gNnPc4	pero
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
peří	peří	k1gNnSc2	peří
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
hormonálně	hormonálně	k6eAd1	hormonálně
a	a	k8xC	a
periodicky	periodicky	k6eAd1	periodicky
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
výměně	výměna	k1gFnSc3	výměna
podléhají	podléhat	k5eAaImIp3nP	podléhat
všechna	všechen	k3xTgNnPc4	všechen
pera	pero	k1gNnPc4	pero
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c4	o
přepeřováním	přepeřování	k1gNnPc3	přepeřování
úplném	úplný	k2eAgNnSc6d1	úplné
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
peří	peřit	k5eAaImIp3nS	peřit
jen	jen	k9	jen
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přepeřování	přepeřování	k1gNnSc4	přepeřování
částečné	částečný	k2eAgNnSc4d1	částečné
<g/>
.	.	kIx.	.
</s>
<s>
Přepeřování	přepeřování	k1gNnSc1	přepeřování
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
ptáka	pták	k1gMnSc4	pták
oslabení	oslabení	k1gNnSc2	oslabení
a	a	k8xC	a
stres	stres	k1gInSc1	stres
<g/>
,	,	kIx,	,
při	při	k7c6	při
ztrátě	ztráta	k1gFnSc6	ztráta
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
letek	letka	k1gFnPc2	letka
také	také	k6eAd1	také
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
ztrátu	ztráta	k1gFnSc4	ztráta
schopnosti	schopnost	k1gFnSc2	schopnost
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
peří	peří	k1gNnSc2	peří
je	být	k5eAaImIp3nS	být
dáno	dán	k2eAgNnSc1d1	dáno
jednak	jednak	k8xC	jednak
přítomností	přítomnost	k1gFnSc7	přítomnost
pigmentů	pigment	k1gInPc2	pigment
v	v	k7c6	v
epidermálních	epidermální	k2eAgFnPc6d1	epidermální
buňkách	buňka	k1gFnPc6	buňka
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
fyzikálně	fyzikálně	k6eAd1	fyzikálně
optickými	optický	k2eAgInPc7d1	optický
jevy	jev	k1gInPc7	jev
(	(	kIx(	(
<g/>
lomem	lom	k1gInSc7	lom
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
na	na	k7c6	na
mikrostrukturách	mikrostruktura	k1gFnPc6	mikrostruktura
paprsků	paprsek	k1gInPc2	paprsek
a	a	k8xC	a
větví	větev	k1gFnPc2	větev
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
strukturální	strukturální	k2eAgNnSc1d1	strukturální
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
)	)	kIx)	)
a	a	k8xC	a
kombinací	kombinace	k1gFnPc2	kombinace
obou	dva	k4xCgInPc2	dva
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
pigmenty	pigment	k1gInPc7	pigment
jsou	být	k5eAaImIp3nP	být
melaniny	melanin	k1gInPc1	melanin
(	(	kIx(	(
<g/>
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
tmavožluté	tmavožlutý	k2eAgFnPc1d1	tmavožlutá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lipochromy	lipochrom	k1gInPc1	lipochrom
či	či	k8xC	či
karotenoidy	karotenoid	k1gInPc1	karotenoid
(	(	kIx(	(
<g/>
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácnější	vzácný	k2eAgInPc1d2	vzácnější
jsou	být	k5eAaImIp3nP	být
porfyriny	porfyrin	k1gInPc1	porfyrin
(	(	kIx(	(
<g/>
zelené	zelená	k1gFnPc1	zelená
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strukturální	strukturální	k2eAgFnPc1d1	strukturální
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
bílá	bílý	k2eAgNnPc4d1	bílé
(	(	kIx(	(
<g/>
úplný	úplný	k2eAgInSc1d1	úplný
odraz	odraz	k1gInSc1	odraz
<g/>
)	)	kIx)	)
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
tóny	tón	k1gInPc1	tón
modré	modrý	k2eAgInPc1d1	modrý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
leskem	lesk	k1gInSc7	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
odstínů	odstín	k1gInPc2	odstín
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
souhrou	souhra	k1gFnSc7	souhra
melaninů	melanin	k1gInPc2	melanin
a	a	k8xC	a
strukturálních	strukturální	k2eAgFnPc2d1	strukturální
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
peří	peří	k1gNnSc2	peří
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
hormony	hormon	k1gInPc7	hormon
<g/>
,	,	kIx,	,
dietou	dieta	k1gFnSc7	dieta
<g/>
,	,	kIx,	,
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
fyzikálním	fyzikální	k2eAgNnSc7d1	fyzikální
poškozením	poškození	k1gNnSc7	poškození
<g/>
,	,	kIx,	,
nemocemi	nemoc	k1gFnPc7	nemoc
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
