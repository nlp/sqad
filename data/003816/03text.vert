<s>
Claude	Claude	k6eAd1	Claude
Bernard	Bernard	k1gMnSc1	Bernard
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1813	[number]	k4	1813
Saint-Julien	Saint-Julina	k1gFnPc2	Saint-Julina
u	u	k7c2	u
Villefranche-sur-Saône	Villefrancheur-Saôn	k1gInSc5	Villefranche-sur-Saôn
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1878	[number]	k4	1878
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
experimentální	experimentální	k2eAgFnSc2d1	experimentální
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
ale	ale	k8xC	ale
školu	škola	k1gFnSc4	škola
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
příručím	příručí	k1gNnSc7	příručí
v	v	k7c6	v
drogerii	drogerie	k1gFnSc6	drogerie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
psal	psát	k5eAaImAgMnS	psát
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
komedie	komedie	k1gFnSc2	komedie
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
poradit	poradit	k5eAaPmF	poradit
se	se	k3xPyFc4	se
se	s	k7c7	s
známým	známý	k2eAgMnSc7d1	známý
divadelním	divadelní	k2eAgMnSc7d1	divadelní
kritikem	kritik	k1gMnSc7	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
poradil	poradit	k5eAaPmAgInS	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
mediciny	medicin	k2eAgFnPc1d1	medicina
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
skutečně	skutečně	k6eAd1	skutečně
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
medicinu	medicin	k2eAgFnSc4d1	medicina
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
se	s	k7c7	s
slavným	slavný	k2eAgMnSc7d1	slavný
fyziologem	fyziolog	k1gMnSc7	fyziolog
F.	F.	kA	F.
Magendie	Magendie	k1gFnPc1	Magendie
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
jako	jako	k8xS	jako
docent	docent	k1gMnSc1	docent
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
jako	jako	k8xC	jako
profesor	profesor	k1gMnSc1	profesor
<g/>
;	;	kIx,	;
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc6d1	založená
katedře	katedra	k1gFnSc6	katedra
fyziologie	fyziologie	k1gFnSc2	fyziologie
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
medicině	medicin	k2eAgFnSc6d1	medicina
chybí	chybit	k5eAaPmIp3nS	chybit
experimentální	experimentální	k2eAgInSc1d1	experimentální
základ	základ	k1gInSc1	základ
a	a	k8xC	a
prováděl	provádět	k5eAaImAgMnS	provádět
i	i	k8xC	i
propagoval	propagovat	k5eAaImAgMnS	propagovat
pokusy	pokus	k1gInPc7	pokus
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Fanny	Fann	k1gMnPc7	Fann
Martin	Martina	k1gFnPc2	Martina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
mluvčím	mluvčí	k1gMnSc7	mluvčí
hnutí	hnutí	k1gNnSc4	hnutí
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
mu	on	k3xPp3gNnSc3	on
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
dal	dát	k5eAaPmAgMnS	dát
zařídit	zařídit	k5eAaPmF	zařídit
laboratoř	laboratoř	k1gFnSc4	laboratoř
při	při	k7c6	při
Musée	Musée	k1gNnSc6	Musée
national	nationat	k5eAaPmAgMnS	nationat
d	d	k?	d
<g/>
́	́	k?	́
<g/>
historie	historie	k1gFnSc2	historie
naturelle	naturelle	k1gFnSc2	naturelle
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
1868	[number]	k4	1868
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
vypraven	vypraven	k2eAgInSc4d1	vypraven
státní	státní	k2eAgInSc4d1	státní
pohřeb	pohřeb	k1gInSc4	pohřeb
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k9	jako
průkopník	průkopník	k1gMnSc1	průkopník
přísně	přísně	k6eAd1	přísně
vědecké	vědecký	k2eAgFnSc2d1	vědecká
metody	metoda	k1gFnSc2	metoda
a	a	k8xC	a
mediciny	medicin	k2eAgInPc1d1	medicin
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
experimentech	experiment	k1gInPc6	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
hlavními	hlavní	k2eAgInPc7d1	hlavní
objevy	objev	k1gInPc7	objev
byl	být	k5eAaImAgInS	být
výklad	výklad	k1gInSc1	výklad
funkce	funkce	k1gFnSc2	funkce
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
(	(	kIx(	(
<g/>
pancreasu	pancreas	k1gMnSc6	pancreas
<g/>
)	)	kIx)	)
a	a	k8xC	a
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
cukrovky	cukrovka	k1gFnSc2	cukrovka
(	(	kIx(	(
<g/>
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
objev	objev	k1gInSc1	objev
vasomotorického	vasomotorický	k2eAgInSc2d1	vasomotorický
systému	systém	k1gInSc2	systém
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
objev	objev	k1gInSc1	objev
významu	význam	k1gInSc2	význam
nitrotělního	nitrotělní	k2eAgNnSc2d1	nitrotělní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
homeostáze	homeostáze	k1gFnSc2	homeostáze
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
také	také	k9	také
účinky	účinek	k1gInPc1	účinek
některých	některý	k3yIgInPc2	některý
jedů	jed	k1gInPc2	jed
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
a	a	k8xC	a
možnostmi	možnost	k1gFnPc7	možnost
anestezie	anestezie	k1gFnSc2	anestezie
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
C.	C.	kA	C.
Bernarda	Bernard	k1gMnSc4	Bernard
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
experimentální	experimentální	k2eAgFnSc2d1	experimentální
mediciny	medicin	k2eAgFnSc2d1	medicina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
následující	následující	k2eAgInPc4d1	následující
citáty	citát	k1gInPc4	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
narazíme	narazit	k5eAaPmIp1nP	narazit
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odporuje	odporovat	k5eAaImIp3nS	odporovat
přijaté	přijatý	k2eAgFnSc3d1	přijatá
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
respektovat	respektovat	k5eAaImF	respektovat
fakt	fakt	k1gInSc4	fakt
a	a	k8xC	a
opustit	opustit	k5eAaPmF	opustit
teorii	teorie	k1gFnSc4	teorie
–	–	k?	–
i	i	k8xC	i
kdyby	kdyby	k9	kdyby
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
opírala	opírat	k5eAaImAgFnS	opírat
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
slavnější	slavný	k2eAgNnPc1d2	slavnější
jména	jméno	k1gNnPc1	jméno
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
ji	on	k3xPp3gFnSc4	on
přijímali	přijímat	k5eAaImAgMnP	přijímat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Věda	věda	k1gFnSc1	věda
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
;	;	kIx,	;
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
deterministická	deterministický	k2eAgFnSc1d1	deterministická
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
nemohla	moct	k5eNaImAgNnP	moct
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnPc1	teorie
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
ověřené	ověřený	k2eAgFnPc1d1	ověřená
větším	veliký	k2eAgInSc7d2	veliký
či	či	k8xC	či
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
svědčí	svědčit	k5eAaImIp3nS	svědčit
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
fakt	faktum	k1gNnPc2	faktum
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
ty	ten	k3xDgFnPc1	ten
nejsou	být	k5eNaImIp3nP	být
definitivní	definitivní	k2eAgFnPc1d1	definitivní
a	a	k8xC	a
nesmíme	smět	k5eNaImIp1nP	smět
jim	on	k3xPp3gMnPc3	on
absolutně	absolutně	k6eAd1	absolutně
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Že	že	k8xS	že
nějaká	nějaký	k3yIgFnSc1	nějaký
podmínka	podmínka	k1gFnSc1	podmínka
vždy	vždy	k6eAd1	vždy
předchází	předcházet	k5eAaImIp3nS	předcházet
či	či	k8xC	či
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
určitý	určitý	k2eAgInSc1d1	určitý
jev	jev	k1gInSc1	jev
ještě	ještě	k6eAd1	ještě
neoprávńuje	oprávńovat	k5eNaBmIp3nS	oprávńovat
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Muselo	muset	k5eAaImAgNnS	muset
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
podmínku	podmínka	k1gFnSc4	podmínka
odstraníme	odstranit	k5eAaPmIp1nP	odstranit
<g/>
,	,	kIx,	,
k	k	k7c3	k
jevu	jev	k1gInSc3	jev
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Cílem	cíl	k1gInSc7	cíl
všech	všecek	k3xTgInPc2	všecek
životních	životní	k2eAgInPc2d1	životní
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
,	,	kIx,	,
jakkoli	jakkoli	k6eAd1	jakkoli
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
udržovat	udržovat	k5eAaImF	udržovat
stálé	stálý	k2eAgFnPc4d1	stálá
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
prostředí	prostředí	k1gNnSc6	prostředí
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jako	jako	k8xC	jako
badatel	badatel	k1gMnSc1	badatel
o	o	k7c6	o
homeostázi	homeostáze	k1gFnSc6	homeostáze
je	být	k5eAaImIp3nS	být
Claude	Claud	k1gInSc5	Claud
Bernard	Bernard	k1gMnSc1	Bernard
pokládán	pokládat	k5eAaImNgMnS	pokládat
i	i	k9	i
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
předchůdců	předchůdce	k1gMnPc2	předchůdce
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Věda	věda	k1gFnSc1	věda
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
naši	náš	k3xOp1gFnSc4	náš
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
krotí	krotit	k5eAaImIp3nS	krotit
naši	náš	k3xOp1gFnSc4	náš
pýchu	pýcha	k1gFnSc4	pýcha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Kdybych	kdyby	kYmCp1nS	kdyby
měl	mít	k5eAaImAgMnS	mít
stručně	stručně	k6eAd1	stručně
definovat	definovat	k5eAaBmF	definovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
bych	by	kYmCp1nS	by
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
tvoření	tvoření	k1gNnSc4	tvoření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
