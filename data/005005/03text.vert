<s>
Baskičtina	baskičtina	k1gFnSc1	baskičtina
(	(	kIx(	(
<g/>
baskicky	baskicky	k6eAd1	baskicky
<g/>
:	:	kIx,	:
Euskara	Euskara	k1gFnSc1	Euskara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
jazyků	jazyk	k1gInPc2	jazyk
z	z	k7c2	z
okolních	okolní	k2eAgNnPc2d1	okolní
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
katalánština	katalánština	k1gFnSc1	katalánština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
typologického	typologický	k2eAgNnSc2d1	typologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jazyk	jazyk	k1gInSc1	jazyk
aglutinační	aglutinační	k2eAgInSc1d1	aglutinační
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
oblastí	oblast	k1gFnSc7	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
je	být	k5eAaImIp3nS	být
Baskicko	Baskicko	k1gNnSc1	Baskicko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
kraj	kraj	k1gInSc1	kraj
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
širší	široký	k2eAgNnSc4d2	širší
okolí	okolí	k1gNnSc4	okolí
zasahující	zasahující	k2eAgNnSc4d1	zasahující
přes	přes	k7c4	přes
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
také	také	k9	také
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
značnou	značný	k2eAgFnSc4d1	značná
kulturní	kulturní	k2eAgFnSc4d1	kulturní
i	i	k8xC	i
politickou	politický	k2eAgFnSc4d1	politická
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Národ	národ	k1gInSc1	národ
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
mluvící	mluvící	k2eAgInSc1d1	mluvící
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Baskové	Bask	k1gMnPc1	Bask
<g/>
.	.	kIx.	.
</s>
<s>
Standardizovaná	standardizovaný	k2eAgFnSc1d1	standardizovaná
forma	forma	k1gFnSc1	forma
baskičtiny	baskičtina	k1gFnSc2	baskičtina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Batua	Batua	k1gFnSc1	Batua
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
této	tento	k3xDgFnSc3	tento
standardní	standardní	k2eAgFnSc1d1	standardní
verze	verze	k1gFnSc1	verze
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
6	[number]	k4	6
hlavních	hlavní	k2eAgNnPc2d1	hlavní
nářečí	nářečí	k1gNnPc2	nářečí
baskičtiny	baskičtina	k1gFnSc2	baskičtina
–	–	k?	–
biskajské	biskajský	k2eAgNnSc4d1	biskajský
<g/>
,	,	kIx,	,
gipuzkoanské	gipuzkoanský	k2eAgNnSc4d1	gipuzkoanský
a	a	k8xC	a
hornonavarrské	hornonavarrský	k2eAgNnSc4d1	hornonavarrský
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
dolnonavarrské	dolnonavarrský	k2eAgNnSc4d1	dolnonavarrský
<g/>
,	,	kIx,	,
lapurdianské	lapurdianský	k2eAgNnSc4d1	lapurdianský
a	a	k8xC	a
zuberoanské	zuberoanský	k2eAgNnSc4d1	zuberoanský
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc4d1	přesná
hranice	hranice	k1gFnPc4	hranice
rozšíření	rozšíření	k1gNnSc2	rozšíření
dialektů	dialekt	k1gInPc2	dialekt
nekorespondují	korespondovat	k5eNaImIp3nP	korespondovat
s	s	k7c7	s
hranicemi	hranice	k1gFnPc7	hranice
politickými	politický	k2eAgFnPc7d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baskičtině	baskičtina	k1gFnSc6	baskičtina
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
oficiálně	oficiálně	k6eAd1	oficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
Euskara	Euskara	k1gFnSc1	Euskara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
možné	možný	k2eAgFnPc1d1	možná
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
etymologickém	etymologický	k2eAgInSc6d1	etymologický
původu	původ	k1gInSc6	původ
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uznávány	uznávat	k5eAaImNgFnP	uznávat
seriózními	seriózní	k2eAgFnPc7d1	seriózní
vědci	vědec	k1gMnPc1	vědec
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
probírány	probírat	k5eAaImNgInP	probírat
na	na	k7c6	na
fórech	fór	k1gInPc6	fór
o	o	k7c6	o
baskičtině	baskičtina	k1gFnSc6	baskičtina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
je	být	k5eAaImIp3nS	být
baskičtina	baskičtina	k1gFnSc1	baskičtina
běžně	běžně	k6eAd1	běžně
nazývána	nazývat	k5eAaImNgFnS	nazývat
basque	basque	k6eAd1	basque
nebo	nebo	k8xC	nebo
dnes	dnes	k6eAd1	dnes
moderněji	moderně	k6eAd2	moderně
euskara	euskara	k1gFnSc1	euskara
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnSc7	možnost
–	–	k?	–
el	ela	k1gFnPc2	ela
vasco	vasco	k6eAd1	vasco
<g/>
,	,	kIx,	,
la	la	k1gNnPc4	la
lengua	lengua	k6eAd1	lengua
vasca	vascum	k1gNnSc2	vascum
nebo	nebo	k8xC	nebo
el	ela	k1gFnPc2	ela
euskera	eusker	k1gMnSc4	eusker
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
termíny	termín	k1gInPc1	termín
–	–	k?	–
vasco	vasco	k6eAd1	vasco
a	a	k8xC	a
basque	basque	k1gFnSc1	basque
jsou	být	k5eAaImIp3nP	být
svým	svůj	k3xOyFgInSc7	svůj
původem	původ	k1gInSc7	původ
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
vascones	vascones	k1gMnSc1	vascones
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sám	sám	k3xTgMnSc1	sám
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
až	až	k9	až
na	na	k7c4	na
řecký	řecký	k2eAgInSc4d1	řecký
původ	původ	k1gInSc4	původ
výrazu	výraz	k1gInSc2	výraz
(	(	kIx(	(
<g/>
ο	ο	k?	ο
(	(	kIx(	(
<g/>
ouaskō	ouaskō	k?	ouaskō
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
Strabón	Strabón	k1gMnSc1	Strabón
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Vascuence	Vascuence	k1gFnSc2	Vascuence
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
vasconĭ	vasconĭ	k?	vasconĭ
<g/>
;	;	kIx,	;
během	během	k7c2	během
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
staletí	staletí	k1gNnPc2	staletí
útlaku	útlak	k1gInSc3	útlak
Basků	Bask	k1gMnPc2	Bask
získal	získat	k5eAaPmAgInS	získat
poněkud	poněkud	k6eAd1	poněkud
hanlivý	hanlivý	k2eAgInSc4d1	hanlivý
výraz	výraz	k1gInSc4	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
užíván	užívat	k5eAaImNgMnS	užívat
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
přinejmenším	přinejmenším	k6eAd1	přinejmenším
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
baskičtina	baskičtina	k1gFnSc1	baskičtina
uznávána	uznávat	k5eAaImNgFnS	uznávat
jako	jako	k8xS	jako
jazyk	jazyk	k1gInSc1	jazyk
menšiny	menšina	k1gFnSc2	menšina
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
prvního	první	k4xOgMnSc2	první
úředního	úřední	k2eAgMnSc2d1	úřední
jazyka	jazyk	k1gMnSc2	jazyk
autonomní	autonomní	k2eAgFnSc2d1	autonomní
oblasti	oblast	k1gFnSc2	oblast
Baskicko	Baskicko	k1gNnSc1	Baskicko
a	a	k8xC	a
částí	část	k1gFnPc2	část
kraje	kraj	k1gInSc2	kraj
Navarra	Navarra	k1gFnSc1	Navarra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
však	však	k9	však
-	-	kIx~	-
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
generála	generál	k1gMnSc2	generál
Franca	Franca	k?	Franca
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
tvrdě	tvrdě	k6eAd1	tvrdě
potírána	potírán	k2eAgFnSc1d1	potírána
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
pak	pak	k6eAd1	pak
není	být	k5eNaImIp3nS	být
jako	jako	k9	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
uznávána	uznávat	k5eAaImNgFnS	uznávat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
baskičtina	baskičtina	k1gFnSc1	baskičtina
zcela	zcela	k6eAd1	zcela
obklopena	obklopit	k5eAaPmNgFnS	obklopit
indoevropskými	indoevropský	k2eAgInPc7d1	indoevropský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k9	jako
Izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
předindoevropského	předindoevropský	k2eAgNnSc2d1	předindoevropský
osídlení	osídlení	k1gNnSc2	osídlení
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
ucelené	ucelený	k2eAgInPc1d1	ucelený
záznamy	záznam	k1gInPc1	záznam
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
per	pero	k1gNnPc2	pero
antických	antický	k2eAgMnPc2d1	antický
autorů	autor	k1gMnPc2	autor
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
baskičtiny	baskičtina	k1gFnSc2	baskičtina
již	již	k9	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
římským	římský	k2eAgMnPc3d1	římský
autorům	autor	k1gMnPc3	autor
dobře	dobře	k6eAd1	dobře
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
jména	jméno	k1gNnSc2	jméno
Nescato	Nescat	k2eAgNnSc1d1	Nescat
a	a	k8xC	a
Gison	Gison	k1gNnSc1	Gison
(	(	kIx(	(
<g/>
neskato	skato	k6eNd1	skato
a	a	k8xC	a
gizon	gizon	k1gMnSc1	gizon
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
i	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
baskičtině	baskičtina	k1gFnSc6	baskičtina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
Iberském	iberský	k2eAgInSc6d1	iberský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
před	před	k7c7	před
římským	římský	k2eAgInSc7d1	římský
záborem	zábor	k1gInSc7	zábor
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Baskičtina	baskičtina	k1gFnSc1	baskičtina
měla	mít	k5eAaImAgFnS	mít
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
jazyků	jazyk	k1gInPc2	jazyk
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
iberština	iberština	k1gFnSc1	iberština
a	a	k8xC	a
tartesština	tartesština	k1gFnSc1	tartesština
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
silnou	silný	k2eAgFnSc4d1	silná
romanizaci	romanizace	k1gFnSc4	romanizace
země	zem	k1gFnSc2	zem
si	se	k3xPyFc3	se
díky	díky	k7c3	díky
římskému	římský	k2eAgNnSc3d1	římské
zanedbávání	zanedbávání	k1gNnSc3	zanedbávání
a	a	k8xC	a
přehlížení	přehlížení	k1gNnSc4	přehlížení
tohoto	tento	k3xDgInSc2	tento
koutu	kout	k1gInSc2	kout
Iberského	iberský	k2eAgInSc2d1	iberský
poloostrova	poloostrov	k1gInSc2	poloostrov
udržela	udržet	k5eAaPmAgFnS	udržet
svůj	svůj	k3xOyFgInSc4	svůj
specifický	specifický	k2eAgInSc4d1	specifický
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
ani	ani	k8xC	ani
ona	onen	k3xDgFnSc1	onen
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
značnému	značný	k2eAgNnSc3d1	značné
přejímání	přejímání	k1gNnSc6	přejímání
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
oznámil	oznámit	k5eAaPmAgMnS	oznámit
vedoucí	vedoucí	k1gMnSc1	vedoucí
archeologického	archeologický	k2eAgInSc2d1	archeologický
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
Iruñ	Iruñ	k1gFnSc6	Iruñ
Elišek	Eliška	k1gFnPc2	Eliška
Gil	Gil	k1gFnSc2	Gil
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc3	jeho
týmu	tým	k1gInSc3	tým
podařilo	podařit	k5eAaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
sadu	sada	k1gFnSc4	sada
destiček	destička	k1gFnPc2	destička
s	s	k7c7	s
270	[number]	k4	270
baskickými	baskický	k2eAgInPc7d1	baskický
nápisy	nápis	k1gInPc7	nápis
a	a	k8xC	a
malbami	malba	k1gFnPc7	malba
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
slovům	slovo	k1gNnPc3	slovo
moderní	moderní	k2eAgFnSc2d1	moderní
baskičtiny	baskičtina	k1gFnPc1	baskičtina
<g/>
.	.	kIx.	.
</s>
<s>
Destičky	destička	k1gFnPc1	destička
byly	být	k5eAaImAgFnP	být
označeny	označit	k5eAaPmNgFnP	označit
za	za	k7c4	za
první	první	k4xOgInPc4	první
psané	psaný	k2eAgInPc4d1	psaný
baskické	baskický	k2eAgInPc4d1	baskický
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
objevu	objev	k1gInSc6	objev
se	se	k3xPyFc4	se
však	však	k9	však
dostal	dostat	k5eAaPmAgInS	dostat
celý	celý	k2eAgInSc1d1	celý
tým	tým	k1gInSc1	tým
pod	pod	k7c4	pod
palbu	palba	k1gFnSc4	palba
odborné	odborný	k2eAgFnSc2d1	odborná
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
objevy	objev	k1gInPc1	objev
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
označeny	označen	k2eAgInPc1d1	označen
jako	jako	k8xS	jako
padělky	padělek	k1gInPc1	padělek
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
spojitost	spojitost	k1gFnSc1	spojitost
baskičtiny	baskičtina	k1gFnSc2	baskičtina
s	s	k7c7	s
jejími	její	k3xOp3gMnPc7	její
indoevropskými	indoevropský	k2eAgMnPc7d1	indoevropský
sousedy	soused	k1gMnPc7	soused
vedla	vést	k5eAaImAgFnS	vést
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
podrobnému	podrobný	k2eAgNnSc3d1	podrobné
zkoumání	zkoumání	k1gNnSc3	zkoumání
baskického	baskický	k2eAgInSc2d1	baskický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
příbuzných	příbuzný	k1gMnPc2	příbuzný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
mnoho	mnoho	k4c4	mnoho
fantastických	fantastický	k2eAgInPc2d1	fantastický
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
nevědeckých	vědecký	k2eNgInPc2d1	nevědecký
názorů	názor	k1gInPc2	názor
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
uznávaných	uznávaný	k2eAgFnPc2d1	uznávaná
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
baskičtinu	baskičtina	k1gFnSc4	baskičtina
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
jí	on	k3xPp3gFnSc3	on
velmi	velmi	k6eAd1	velmi
vzdálené	vzdálený	k2eAgFnSc3d1	vzdálená
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
baskičtiny	baskičtina	k1gFnSc2	baskičtina
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
pokládány	pokládán	k2eAgInPc1d1	pokládán
za	za	k7c4	za
neověřené	ověřený	k2eNgInPc4d1	neověřený
a	a	k8xC	a
nepodložené	podložený	k2eNgInPc4d1	nepodložený
<g/>
.	.	kIx.	.
</s>
<s>
Iberština	iberština	k1gFnSc1	iberština
<g/>
:	:	kIx,	:
starověký	starověký	k2eAgInSc1d1	starověký
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
užíván	užíván	k2eAgInSc4d1	užíván
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
dnes	dnes	k6eAd1	dnes
vymřelé	vymřelý	k2eAgFnSc6d1	vymřelá
akvitánštině	akvitánština	k1gFnSc6	akvitánština
a	a	k8xC	a
také	také	k9	také
moderní	moderní	k2eAgFnSc4d1	moderní
baskičtině	baskičtina	k1gFnSc3	baskičtina
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
sama	sám	k3xTgFnSc1	sám
iberština	iberština	k1gFnSc1	iberština
však	však	k9	však
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
lingvisticky	lingvisticky	k6eAd1	lingvisticky
zařazena	zařadit	k5eAaPmNgFnS	zařadit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
baskičtina	baskičtina	k1gFnSc1	baskičtina
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
pouze	pouze	k6eAd1	pouze
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
zcela	zcela	k6eAd1	zcela
příbuzná	příbuzná	k1gFnSc1	příbuzná
<g/>
.	.	kIx.	.
</s>
<s>
Eduardo	Eduardo	k1gNnSc1	Eduardo
Orduñ	Orduñ	k1gFnSc2	Orduñ
Sunar	sunar	k1gInSc1	sunar
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
příbuznost	příbuznost	k1gFnSc4	příbuznost
iberských	iberský	k2eAgFnPc2d1	iberská
a	a	k8xC	a
baskických	baskický	k2eAgFnPc2d1	baskická
číslovek	číslovka	k1gFnPc2	číslovka
a	a	k8xC	a
také	také	k9	také
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
příbuzností	příbuznost	k1gFnSc7	příbuznost
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
také	také	k9	také
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
von	von	k1gInSc4	von
Humboldt	Humboldt	k2eAgInSc4d1	Humboldt
<g/>
,	,	kIx,	,
či	či	k8xC	či
Hugo	Hugo	k1gMnSc1	Hugo
Schuchardt	Schuchardt	k1gMnSc1	Schuchardt
<g/>
.	.	kIx.	.
</s>
<s>
Ligurská	ligurský	k2eAgFnSc1d1	Ligurská
teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
jejími	její	k3xOp3gMnPc7	její
autory	autor	k1gMnPc7	autor
a	a	k8xC	a
zastánci	zastánce	k1gMnPc1	zastánce
byli	být	k5eAaImAgMnP	být
Henri	Henre	k1gFnSc4	Henre
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arbois	Arbois	k1gInSc1	Arbois
de	de	k?	de
Joubainville	Joubainville	k1gInSc1	Joubainville
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Pokorny	Pokorny	k?	Pokorny
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Kretschmer	Kretschmer	k1gInSc1	Kretschmer
a	a	k8xC	a
několik	několik	k4yIc1	několik
jiných	jiný	k2eAgMnPc2d1	jiný
lingvistů	lingvista	k1gMnPc2	lingvista
<g/>
.	.	kIx.	.
</s>
<s>
Gruzínština	gruzínština	k1gFnSc1	gruzínština
<g/>
:	:	kIx,	:
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
možnou	možný	k2eAgFnSc4d1	možná
příbuznost	příbuznost	k1gFnSc4	příbuznost
baskičtiny	baskičtina	k1gFnSc2	baskičtina
s	s	k7c7	s
jihokavkazskými	jihokavkazský	k2eAgInPc7d1	jihokavkazský
jazyky	jazyk	k1gInPc7	jazyk
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
pokládána	pokládán	k2eAgFnSc1d1	pokládána
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
věrohodnou	věrohodný	k2eAgFnSc4d1	věrohodná
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
předpokladu	předpoklad	k1gInSc6	předpoklad
existence	existence	k1gFnSc2	existence
Iberského	iberský	k2eAgNnSc2d1	Iberské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
rozkládat	rozkládat	k5eAaImF	rozkládat
dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
podle	podle	k7c2	podle
J.	J.	kA	J.
<g/>
P.	P.	kA	P.
Malorryho	Malorry	k1gMnSc2	Malorry
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
teorii	teorie	k1gFnSc6	teorie
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
"	"	kIx"	"
<g/>
Hledání	hledání	k1gNnSc1	hledání
Indoevropanů	Indoevropan	k1gMnPc2	Indoevropan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
podložena	podložit	k5eAaPmNgFnS	podložit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
baskické	baskický	k2eAgNnSc1d1	baskické
označení	označení	k1gNnSc1	označení
místa	místo	k1gNnSc2	místo
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
gruzínštině	gruzínština	k1gFnSc6	gruzínština
–	–	k?	–
adze	adze	k1gFnPc2	adze
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
6	[number]	k4	6
hlavních	hlavní	k2eAgInPc2d1	hlavní
dialektů	dialekt	k1gInPc2	dialekt
baskičtiny	baskičtina	k1gFnSc2	baskičtina
<g/>
:	:	kIx,	:
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
biskajský	biskajský	k2eAgInSc4d1	biskajský
gipuzkoanský	gipuzkoanský	k2eAgInSc4d1	gipuzkoanský
hornonavarrský	hornonavarrský	k2eAgInSc4d1	hornonavarrský
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
dolnonavarrský	dolnonavarrský	k2eAgInSc1d1	dolnonavarrský
lapurdianský	lapurdianský	k2eAgInSc1d1	lapurdianský
zuberoanský	zuberoanský	k2eAgInSc1d1	zuberoanský
Rozšíření	rozšíření	k1gNnSc1	rozšíření
těchto	tento	k3xDgInPc2	tento
dialektů	dialekt	k1gInPc2	dialekt
nekopíruje	kopírovat	k5eNaImIp3nS	kopírovat
hranice	hranice	k1gFnSc1	hranice
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
baskickou	baskický	k2eAgFnSc7d1	baskická
dialektologií	dialektologie	k1gFnSc7	dialektologie
zabýval	zabývat	k5eAaImAgInS	zabývat
také	také	k9	také
např.	např.	kA	např.
Louis	Louis	k1gMnSc1	Louis
Lucien	Lucien	k2eAgMnSc1d1	Lucien
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnSc2	slovo
přijímá	přijímat	k5eAaImIp3nS	přijímat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
španělštiny	španělština	k1gFnSc2	španělština
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
gaskonštiny	gaskonština	k1gFnSc2	gaskonština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fonetická	fonetický	k2eAgFnSc1d1	fonetická
revoluce	revoluce	k1gFnSc1	revoluce
změnila	změnit	k5eAaPmAgFnS	změnit
tato	tento	k3xDgNnPc4	tento
slova	slovo	k1gNnPc4	slovo
někdy	někdy	k6eAd1	někdy
až	až	k9	až
k	k	k7c3	k
nepoznání	nepoznání	k1gNnSc3	nepoznání
<g/>
.	.	kIx.	.
</s>
<s>
Bai	Bai	k?	Bai
–	–	k?	–
Ano	ano	k9	ano
Ez	Ez	k1gMnSc1	Ez
–	–	k?	–
Ne	ne	k9	ne
Kaixo	Kaixo	k6eAd1	Kaixo
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Ahoj	ahoj	k0	ahoj
<g/>
!	!	kIx.	!
</s>
<s>
Agur	Agur	k1gMnSc1	Agur
<g/>
!	!	kIx.	!
</s>
<s>
/	/	kIx~	/
Aio	Aio	k1gFnPc6	Aio
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Na	na	k7c4	na
shledanou	shledaná	k1gFnSc4	shledaná
!	!	kIx.	!
</s>
<s>
Ikusi	Ikuse	k1gFnSc4	Ikuse
arte	art	k1gFnSc2	art
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Na	na	k7c4	na
viděnou	viděná	k1gFnSc4	viděná
<g/>
!	!	kIx.	!
</s>
<s>
Eskerrik	Eskerrik	k1gMnSc1	Eskerrik
asko	asko	k1gMnSc1	asko
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Děkuji	děkovat	k5eAaImIp1nS	děkovat
<g/>
!	!	kIx.	!
</s>
<s>
Egun	Egun	k1gMnSc1	Egun
on	on	k3xPp3gMnSc1	on
–	–	k?	–
Dobré	dobrý	k2eAgNnSc4d1	dobré
ráno	ráno	k1gNnSc4	ráno
(	(	kIx(	(
<g/>
literárně	literárně	k6eAd1	literárně
<g/>
:	:	kIx,	:
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
Egun	Egun	k1gMnSc1	Egun
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
bai	bai	k?	bai
–	–	k?	–
Odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
Egun	Egun	k1gNnSc4	Egun
on	on	k3xPp3gMnSc1	on
Arratsalde	Arratsald	k1gInSc5	Arratsald
on	on	k3xPp3gMnSc1	on
–	–	k?	–
Dobrý	dobrý	k2eAgInSc1d1	dobrý
večer	večer	k1gInSc1	večer
Gabon	Gabon	k1gInSc1	Gabon
–	–	k?	–
Dobrou	dobrý	k2eAgFnSc4d1	dobrá
noc	noc	k1gFnSc4	noc
Mesedez	Mesedeza	k1gFnPc2	Mesedeza
–	–	k?	–
Prosím	prosit	k5eAaImIp1nS	prosit
Barkatu	Barkata	k1gFnSc4	Barkata
–	–	k?	–
Promiňte	prominout	k5eAaPmRp2nP	prominout
(	(	kIx(	(
<g/>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c4	na
něco	něco	k3yInSc4	něco
ptám	ptat	k5eAaImIp1nS	ptat
<g/>
)	)	kIx)	)
Barkatu	Barkat	k2eAgFnSc4d1	Barkat
–	–	k?	–
Omlouvám	omlouvat	k5eAaImIp1nS	omlouvat
se	se	k3xPyFc4	se
Aizu	Aizus	k1gInSc2	Aizus
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Poslouchej	poslouchat	k5eAaImRp2nS	poslouchat
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
zdvořilé	zdvořilý	k2eAgNnSc1d1	zdvořilé
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
užíváno	užívat	k5eAaImNgNnS	užívat
mezi	mezi	k7c7	mezi
známými	známý	k2eAgInPc7d1	známý
<g/>
)	)	kIx)	)
Kafe	Kafe	k?	Kafe
hutsa	hutsa	k1gFnSc1	hutsa
nahi	nah	k1gFnSc2	nah
nuke	nuke	k1gFnSc1	nuke
–	–	k?	–
Mohu	moct	k5eAaImIp1nS	moct
dostat	dostat	k5eAaPmF	dostat
kafe	kafe	k?	kafe
<g/>
?	?	kIx.	?
</s>
<s>
Kafe	Kafe	k?	Kafe
ebakia	ebakia	k1gFnSc1	ebakia
nahi	nah	k1gFnSc2	nah
nuke	nuke	k1gFnSc1	nuke
–	–	k?	–
Mohu	moct	k5eAaImIp1nS	moct
dostat	dostat	k5eAaPmF	dostat
macchiato	macchiato	k6eAd1	macchiato
<g/>
?	?	kIx.	?
</s>
<s>
Kafesnea	Kafesnea	k1gFnSc1	Kafesnea
nahi	nah	k1gFnSc2	nah
nuke	nuke	k1gFnSc1	nuke
–	–	k?	–
Mohu	moct	k5eAaImIp1nS	moct
dostat	dostat	k5eAaPmF	dostat
café	café	k1gNnSc4	café
latte	latte	k5eAaPmIp2nP	latte
<g/>
?	?	kIx.	?
</s>
<s>
Garagardoa	Garagardoa	k1gFnSc1	Garagardoa
nahi	nah	k1gFnSc2	nah
nuke	nuke	k1gFnSc1	nuke
–	–	k?	–
Mohu	moct	k5eAaImIp1nS	moct
dostat	dostat	k5eAaPmF	dostat
pivo	pivo	k1gNnSc4	pivo
<g/>
?	?	kIx.	?
</s>
<s>
Komunak	Komunak	k1gInSc1	Komunak
–	–	k?	–
Toalety	toaleta	k1gFnSc2	toaleta
Non	Non	k1gMnSc1	Non
dago	dago	k1gMnSc1	dago
komuna	komuna	k1gFnSc1	komuna
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
toalety	toaleta	k1gFnPc1	toaleta
<g/>
?	?	kIx.	?
</s>
<s>
Non	Non	k?	Non
dago	dago	k6eAd1	dago
tren-geltokia	treneltokium	k1gNnSc2	tren-geltokium
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vlakové	vlakový	k2eAgNnSc4d1	vlakové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
?	?	kIx.	?
</s>
<s>
Non	Non	k?	Non
dago	dago	k6eAd1	dago
autobus-geltokia	autobuseltokium	k1gNnSc2	autobus-geltokium
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
?	?	kIx.	?
</s>
<s>
Ba	ba	k9	ba
al	ala	k1gFnPc2	ala
da	da	k?	da
hotelik	hotelik	k1gMnSc1	hotelik
hemen	hemen	k2eAgInSc4d1	hemen
inguruan	inguruan	k1gInSc4	inguruan
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hotel	hotel	k1gInSc1	hotel
<g/>
?	?	kIx.	?
</s>
<s>
Zorionak	Zorionak	k6eAd1	Zorionak
–	–	k?	–
Šťastné	Šťastné	k2eAgFnPc4d1	Šťastné
prázdniny	prázdniny	k1gFnPc4	prázdniny
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
užíváno	užívat	k5eAaImNgNnS	užívat
na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
<g/>
)	)	kIx)	)
Zer	Zer	k1gMnSc1	Zer
moduz	moduz	k1gMnSc1	moduz
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
máš	mít	k5eAaImIp2nS	mít
<g/>
?	?	kIx.	?
</s>
<s>
Baskický	baskický	k2eAgInSc1d1	baskický
číselný	číselný	k2eAgInSc1d1	číselný
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
číslu	číslo	k1gNnSc3	číslo
20	[number]	k4	20
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
dvacítkovou	dvacítkový	k2eAgFnSc4d1	dvacítková
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
poměrně	poměrně	k6eAd1	poměrně
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
ji	on	k3xPp3gFnSc4	on
např.	např.	kA	např.
v	v	k7c6	v
čečenštině	čečenština	k1gFnSc6	čečenština
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
podporovat	podporovat	k5eAaImF	podporovat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
příbuzenství	příbuzenství	k1gNnSc6	příbuzenství
mezi	mezi	k7c7	mezi
kavkazskými	kavkazský	k2eAgInPc7d1	kavkazský
jazyky	jazyk	k1gInPc7	jazyk
a	a	k8xC	a
baskičtinou	baskičtina	k1gFnSc7	baskičtina
<g/>
.	.	kIx.	.
</s>
<s>
Dvacítkovou	dvacítkový	k2eAgFnSc4d1	dvacítková
soustavu	soustava	k1gFnSc4	soustava
ovšem	ovšem	k9	ovšem
využívá	využívat	k5eAaPmIp3nS	využívat
částečně	částečně	k6eAd1	částečně
i	i	k9	i
francouzština	francouzština	k1gFnSc1	francouzština
(	(	kIx(	(
<g/>
quatre-vingt	quatreingt	k1gInSc1	quatre-vingt
dix	dix	k?	dix
=	=	kIx~	=
4	[number]	k4	4
krát	krát	k6eAd1	krát
20	[number]	k4	20
a	a	k8xC	a
10	[number]	k4	10
=	=	kIx~	=
90	[number]	k4	90
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
i	i	k9	i
gaelština	gaelština	k1gFnSc1	gaelština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k9	ale
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
nahrazování	nahrazování	k1gNnSc3	nahrazování
desítkovou	desítkový	k2eAgFnSc4d1	desítková
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
používali	používat	k5eAaImAgMnP	používat
dvacítkovou	dvacítkový	k2eAgFnSc4d1	dvacítková
soustavu	soustava	k1gFnSc4	soustava
Mayové	Mayová	k1gFnSc2	Mayová
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Mayská	mayský	k2eAgFnSc1d1	mayská
dvacítková	dvacítkový	k2eAgFnSc1d1	dvacítková
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
0	[number]	k4	0
–	–	k?	–
huts	huts	k1gInSc1	huts
1	[number]	k4	1
–	–	k?	–
bat	bat	k?	bat
2	[number]	k4	2
–	–	k?	–
bi	bi	k?	bi
3	[number]	k4	3
–	–	k?	–
hiru	hiru	k5eAaPmIp1nS	hiru
4	[number]	k4	4
–	–	k?	–
lau	lau	k?	lau
5	[number]	k4	5
–	–	k?	–
bost	bost	k1gInSc1	bost
6	[number]	k4	6
–	–	k?	–
sei	sei	k?	sei
7	[number]	k4	7
–	–	k?	–
zazpi	zazpi	k1gNnSc1	zazpi
8	[number]	k4	8
–	–	k?	–
zortzi	zortze	k1gFnSc6	zortze
9	[number]	k4	9
–	–	k?	–
bederatzi	bederatze	k1gFnSc6	bederatze
10	[number]	k4	10
–	–	k?	–
hamar	hamar	k1gMnSc1	hamar
<g />
.	.	kIx.	.
</s>
<s>
11	[number]	k4	11
–	–	k?	–
hamaika	hamaika	k1gFnSc1	hamaika
12	[number]	k4	12
–	–	k?	–
hamabi	hamabi	k1gNnSc1	hamabi
13	[number]	k4	13
–	–	k?	–
hamahiru	hamahir	k1gInSc2	hamahir
14	[number]	k4	14
–	–	k?	–
hamalau	hamalau	k5eAaPmIp1nS	hamalau
15	[number]	k4	15
–	–	k?	–
hamabost	hamabost	k1gFnSc1	hamabost
16	[number]	k4	16
–	–	k?	–
hamasei	hamasei	k1gNnSc1	hamasei
17	[number]	k4	17
–	–	k?	–
hamazazpi	hamazazpi	k1gNnSc1	hamazazpi
18	[number]	k4	18
–	–	k?	–
hemezortzi	hemezortze	k1gFnSc6	hemezortze
19	[number]	k4	19
–	–	k?	–
hemeretzi	hemeretze	k1gFnSc6	hemeretze
20	[number]	k4	20
–	–	k?	–
hogei	hogei	k1gNnSc1	hogei
21	[number]	k4	21
–	–	k?	–
hogeita	hogeita	k1gFnSc1	hogeita
bat	bat	k?	bat
22	[number]	k4	22
–	–	k?	–
hogeita	hogeita	k1gFnSc1	hogeita
bi	bi	k?	bi
23	[number]	k4	23
–	–	k?	–
hogeita	hogeit	k1gMnSc4	hogeit
hiru	hira	k1gMnSc4	hira
30	[number]	k4	30
–	–	k?	–
hogeita	hogeita	k1gMnSc1	hogeita
hamar	hamar	k1gMnSc1	hamar
(	(	kIx(	(
<g/>
hogei-ta-hamar	hogeiaamar	k1gMnSc1	hogei-ta-hamar
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s>
dvacet-a-deset	dvaceteset	k1gInSc1	dvacet-a-deset
=	=	kIx~	=
20	[number]	k4	20
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
31	[number]	k4	31
–	–	k?	–
hogeita	hogeit	k2eAgFnSc1d1	hogeit
hamaika	hamaika	k1gFnSc1	hamaika
(	(	kIx(	(
<g/>
hogei-ta-hamaika	hogeiaamaika	k1gFnSc1	hogei-ta-hamaika
=	=	kIx~	=
dvacet-a-jedenáct	dvacetedenáct	k1gInSc1	dvacet-a-jedenáct
=	=	kIx~	=
20	[number]	k4	20
<g/>
+	+	kIx~	+
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
40	[number]	k4	40
–	–	k?	–
berrogei	berroge	k1gFnSc2	berroge
(	(	kIx(	(
<g/>
ber-hogei	berogei	k6eAd1	ber-hogei
=	=	kIx~	=
dvakrát	dvakrát	k6eAd1	dvakrát
dvacet	dvacet	k4xCc1	dvacet
=	=	kIx~	=
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
50	[number]	k4	50
–	–	k?	–
berrogeita	berrogeita	k1gMnSc1	berrogeita
hamar	hamar	k1gMnSc1	hamar
(	(	kIx(	(
<g/>
ber-hogei-ta-hamar	berogeiaamar	k1gMnSc1	ber-hogei-ta-hamar
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
dvakrát-dvacet-a-deset	dvakrátvaceteset	k1gMnSc1	dvakrát-dvacet-a-deset
=	=	kIx~	=
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
60	[number]	k4	60
–	–	k?	–
hirurogei	hiruroge	k1gFnSc2	hiruroge
(	(	kIx(	(
<g/>
hirur-hogei	hirurogei	k6eAd1	hirur-hogei
=	=	kIx~	=
třikrát-dvacet	třikrátvacet	k5eAaPmF	třikrát-dvacet
=	=	kIx~	=
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
70	[number]	k4	70
–	–	k?	–
hirurogeita	hirurogeita	k1gMnSc1	hirurogeita
hamar	hamar	k1gMnSc1	hamar
(	(	kIx(	(
<g/>
hirur-hogei-ta-hamar	hirurogeiaamar	k1gMnSc1	hirur-hogei-ta-hamar
=	=	kIx~	=
třikrát-dvacet-a-deset	třikrátvaceteset	k1gMnSc1	třikrát-dvacet-a-deset
=	=	kIx~	=
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
80	[number]	k4	80
–	–	k?	–
laurogei	lauroge	k1gFnSc2	lauroge
(	(	kIx(	(
<g/>
laur-hogei	laurogei	k6eAd1	laur-hogei
=	=	kIx~	=
čtyřikrát-dvacet	čtyřikrátvacet	k5eAaPmF	čtyřikrát-dvacet
=	=	kIx~	=
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
90	[number]	k4	90
–	–	k?	–
laurogeita	laurogeita	k1gMnSc1	laurogeita
hamar	hamar	k1gMnSc1	hamar
(	(	kIx(	(
<g/>
laur-hogei-ta-hamar	laurogeiaamar	k1gMnSc1	laur-hogei-ta-hamar
=	=	kIx~	=
čtyřikrát-dvacet-a-deset	čtyřikrátvaceteset	k1gMnSc1	čtyřikrát-dvacet-a-deset
=	=	kIx~	=
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
100	[number]	k4	100
–	–	k?	–
ehun	ehun	k1gNnSc1	ehun
200	[number]	k4	200
–	–	k?	–
berrehun	berrehun	k1gNnSc1	berrehun
300	[number]	k4	300
–	–	k?	–
hirurehun	hirurehun	k1gNnSc1	hirurehun
1000	[number]	k4	1000
–	–	k?	–
mila	mila	k1gMnSc1	mila
2000	[number]	k4	2000
–	–	k?	–
bi	bi	k?	bi
mila	mila	k6eAd1	mila
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
–	–	k?	–
milioi	milioi	k1gNnPc2	milioi
bat	bat	k?	bat
půl	půl	k1xP	půl
–	–	k?	–
erdi	erdi	k6eAd1	erdi
méně	málo	k6eAd2	málo
–	–	k?	–
gutxiago	gutxiago	k6eAd1	gutxiago
více	hodně	k6eAd2	hodně
–	–	k?	–
gehiago	gehiago	k6eAd1	gehiago
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
různých	různý	k2eAgInPc2d1	různý
jazykových	jazykový	k2eAgInPc2d1	jazykový
mixů	mix	k1gInPc2	mix
(	(	kIx(	(
<g/>
pidžinů	pidžin	k1gInPc2	pidžin
<g/>
)	)	kIx)	)
baskičtiny	baskičtina	k1gFnSc2	baskičtina
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
mix	mix	k1gInSc1	mix
baskičtiny	baskičtina	k1gFnSc2	baskičtina
s	s	k7c7	s
islandštinou	islandština	k1gFnSc7	islandština
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
baskičtí	baskický	k2eAgMnPc1d1	baskický
námořníci	námořník	k1gMnPc1	námořník
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
měli	mít	k5eAaImAgMnP	mít
výrazné	výrazný	k2eAgInPc4d1	výrazný
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Islandem	Island	k1gInSc7	Island
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
baskicko-islandský	baskickoslandský	k2eAgInSc1d1	baskicko-islandský
pidžin	pidžin	k1gInSc1	pidžin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
pidžin	pidžin	k1gInSc1	pidžin
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
díky	díky	k7c3	díky
kontaktu	kontakt	k1gInSc3	kontakt
baskických	baskický	k2eAgMnPc2d1	baskický
velrybářů	velrybář	k1gMnPc2	velrybář
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
indiánským	indiánský	k2eAgNnSc7d1	indiánské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
baskičtina	baskičtina	k1gFnSc1	baskičtina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
baskičtina	baskičtina	k1gFnSc1	baskičtina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
