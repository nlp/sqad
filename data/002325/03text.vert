<s>
Angína	angína	k1gFnSc1	angína
čili	čili	k8xC	čili
tonzilitida	tonzilitida	k1gFnSc1	tonzilitida
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc4	zánět
krčních	krční	k2eAgFnPc2d1	krční
mandlí	mandle	k1gFnPc2	mandle
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
onemocnění	onemocnění	k1gNnSc4	onemocnění
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
<g/>
,	,	kIx,	,
virového	virový	k2eAgMnSc2d1	virový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
výjimečně	výjimečně	k6eAd1	výjimečně
mykotického	mykotický	k2eAgInSc2d1	mykotický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
formou	forma	k1gFnSc7	forma
kapénkové	kapénkový	k2eAgFnSc2d1	kapénková
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Angína	angína	k1gFnSc1	angína
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc4d1	běžné
onemocnění	onemocnění	k1gNnSc4	onemocnění
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
léčitelná	léčitelný	k2eAgFnSc1d1	léčitelná
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Angína	angína	k1gFnSc1	angína
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
svíravá	svíravý	k2eAgFnSc1d1	svíravá
bolest	bolest	k1gFnSc1	bolest
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
angere	angrat	k5eAaPmIp3nS	angrat
=	=	kIx~	=
svírat	svírat	k5eAaImF	svírat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
-	-	kIx~	-
nejčastěji	často	k6eAd3	často
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pyogenes	pyogenes	k1gMnSc1	pyogenes
-	-	kIx~	-
v	v	k7c6	v
případě	případ	k1gInSc6	případ
produkce	produkce	k1gFnSc2	produkce
Virová	virový	k2eAgFnSc1d1	virová
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
vzácněji	vzácně	k6eAd2	vzácně
adenoviry	adenovira	k1gFnSc2	adenovira
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gInSc1	simplex
virus	virus	k1gInSc4	virus
Mezi	mezi	k7c4	mezi
příznaky	příznak	k1gInPc4	příznak
patří	patřit	k5eAaImIp3nP	patřit
bolesti	bolest	k1gFnPc1	bolest
v	v	k7c6	v
krku	krk	k1gInSc6	krk
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgNnSc1d1	související
obtížné	obtížný	k2eAgNnSc1d1	obtížné
polykání	polykání	k1gNnSc1	polykání
<g/>
,	,	kIx,	,
chrapot	chrapot	k1gInSc1	chrapot
<g/>
,	,	kIx,	,
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
otok	otok	k1gInSc1	otok
mandlí	mandle	k1gFnPc2	mandle
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc4	bolest
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
<g/>
,	,	kIx,	,
zimnice	zimnice	k1gFnSc1	zimnice
a	a	k8xC	a
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
dávení	dávení	k1gNnSc1	dávení
a	a	k8xC	a
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
ucpání	ucpání	k1gNnSc1	ucpání
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
nosní	nosní	k2eAgInSc1d1	nosní
výtok	výtok	k1gInSc1	výtok
a	a	k8xC	a
zvětšené	zvětšený	k2eAgFnPc1d1	zvětšená
mízní	mízní	k2eAgFnPc1d1	mízní
uzliny	uzlina	k1gFnPc1	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
angína	angína	k1gFnSc1	angína
zkomplikuje	zkomplikovat	k5eAaPmIp3nS	zkomplikovat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vzácně	vzácně	k6eAd1	vzácně
způsobit	způsobit	k5eAaPmF	způsobit
i	i	k9	i
absces	absces	k1gInSc4	absces
a	a	k8xC	a
dušení	dušení	k1gNnSc4	dušení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
až	až	k9	až
bakteriální	bakteriální	k2eAgNnSc4d1	bakteriální
postižení	postižení	k1gNnSc4	postižení
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
také	také	k9	také
nejprve	nejprve	k6eAd1	nejprve
zvýšením	zvýšení	k1gNnSc7	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
zvětšené	zvětšený	k2eAgFnSc2d1	zvětšená
uzliny	uzlina	k1gFnSc2	uzlina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc4	bolest
šíje	šíj	k1gFnSc2	šíj
a	a	k8xC	a
následně	následně	k6eAd1	následně
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
nepříjemného	příjemný	k2eNgInSc2d1	nepříjemný
pocitu	pocit	k1gInSc2	pocit
mrazení	mrazení	k1gNnSc2	mrazení
při	při	k7c6	při
otáčení	otáčení	k1gNnSc6	otáčení
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
příznaků	příznak	k1gInPc2	příznak
přichází	přicházet	k5eAaImIp3nS	přicházet
po	po	k7c6	po
pár	pár	k4xCyI	pár
dnech	den	k1gInPc6	den
kromě	kromě	k7c2	kromě
zvětšených	zvětšený	k2eAgFnPc2d1	zvětšená
uzlin	uzlina	k1gFnPc2	uzlina
bolest	bolest	k1gFnSc1	bolest
v	v	k7c6	v
krku	krk	k1gInSc6	krk
při	při	k7c6	při
polykání	polykání	k1gNnSc6	polykání
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
nemožnost	nemožnost	k1gFnSc1	nemožnost
polknout	polknout	k5eAaPmF	polknout
tuhou	tuhý	k2eAgFnSc4d1	tuhá
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
hnis	hnis	k1gInSc1	hnis
na	na	k7c6	na
mandlích	mandle	k1gFnPc6	mandle
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
a	a	k8xC	a
analgetika	analgetikum	k1gNnPc1	analgetikum
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
abscesu	absces	k1gInSc2	absces
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
drenáž	drenáž	k1gFnSc1	drenáž
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
lékař	lékař	k1gMnSc1	lékař
předepíše	předepsat	k5eAaPmIp3nS	předepsat
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
jíst	jíst	k5eAaImF	jíst
jogurt	jogurt	k1gInSc4	jogurt
a	a	k8xC	a
užívat	užívat	k5eAaImF	užívat
přípravky	přípravek	k1gInPc4	přípravek
pro	pro	k7c4	pro
obnovení	obnovení	k1gNnSc4	obnovení
střevní	střevní	k2eAgFnSc2d1	střevní
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
(	(	kIx(	(
<g/>
lactobacilus	lactobacilus	k1gMnSc1	lactobacilus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
současně	současně	k6eAd1	současně
s	s	k7c7	s
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
dobrání	dobrání	k1gNnSc6	dobrání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
se	se	k3xPyFc4	se
kouřit	kouřit	k5eAaImF	kouřit
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
nemocní	nemocný	k1gMnPc1	nemocný
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
i	i	k9	i
pasivnímu	pasivní	k2eAgNnSc3d1	pasivní
kouření	kouření	k1gNnSc3	kouření
<g/>
.	.	kIx.	.
</s>
<s>
Tabákový	tabákový	k2eAgInSc1d1	tabákový
kouř	kouř	k1gInSc1	kouř
dráždí	dráždit	k5eAaImIp3nS	dráždit
krční	krční	k2eAgFnSc4d1	krční
sliznici	sliznice	k1gFnSc4	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
angína	angína	k1gFnSc1	angína
často	často	k6eAd1	často
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
případů	případ	k1gInPc2	případ
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
chirurgické	chirurgický	k2eAgNnSc1d1	chirurgické
odstranění	odstranění	k1gNnSc4	odstranění
krčních	krční	k2eAgFnPc2d1	krční
mandlí	mandle	k1gFnPc2	mandle
<g/>
.	.	kIx.	.
</s>
