<p>
<s>
Evanescence	Evanescence	k1gFnSc1	Evanescence
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
hrající	hrající	k2eAgNnSc1d1	hrající
spojení	spojení	k1gNnSc4	spojení
gothic	gothice	k1gFnPc2	gothice
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
alternative	alternativ	k1gInSc5	alternativ
rocku	rock	k1gInSc3	rock
a	a	k8xC	a
alternative	alternativ	k1gInSc5	alternativ
metalu	metal	k1gInSc3	metal
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
skupiny	skupina	k1gFnSc2	skupina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
pomíjivost	pomíjivost	k1gFnSc1	pomíjivost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nestálost	nestálost	k1gFnSc1	nestálost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zmizet	zmizet	k5eAaPmF	zmizet
jako	jako	k9	jako
pára	pára	k1gFnSc1	pára
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
skupiny	skupina	k1gFnSc2	skupina
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
kapele	kapela	k1gFnSc6	kapela
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrají	hrát	k5eAaImIp3nP	hrát
především	především	k9	především
epicky	epicky	k6eAd1	epicky
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
<g/>
,	,	kIx,	,
temnou	temný	k2eAgFnSc4d1	temná
rockovou	rockový	k2eAgFnSc4d1	rocková
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
textové	textový	k2eAgFnSc6d1	textová
stránce	stránka	k1gFnSc6	stránka
Evanescence	Evanescence	k1gFnSc1	Evanescence
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
témata	téma	k1gNnPc4	téma
od	od	k7c2	od
lásky	láska	k1gFnSc2	láska
přes	přes	k7c4	přes
zoufalství	zoufalství	k1gNnSc4	zoufalství
až	až	k9	až
po	po	k7c4	po
temnou	temný	k2eAgFnSc4d1	temná
beznaděj	beznaděj	k1gFnSc4	beznaděj
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
milionům	milion	k4xCgInPc3	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
desek	deska	k1gFnPc2	deska
také	také	k6eAd1	také
získali	získat	k5eAaPmAgMnP	získat
dvě	dva	k4xCgFnPc4	dva
sošky	soška	k1gFnPc4	soška
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Rocková	rockový	k2eAgFnSc1d1	rocková
formace	formace	k1gFnSc1	formace
Evanescence	Evanescence	k1gFnSc2	Evanescence
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
v	v	k7c6	v
arkansaském	arkansaský	k2eAgNnSc6d1	arkansaský
městě	město	k1gNnSc6	město
Little	Little	k1gFnSc1	Little
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
ji	on	k3xPp3gFnSc4	on
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Amy	Amy	k1gFnSc1	Amy
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Ben	Ben	k1gInSc4	Ben
Moody	Mooda	k1gFnSc2	Mooda
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
seznámili	seznámit	k5eAaPmAgMnP	seznámit
v	v	k7c6	v
teenagerských	teenagerský	k2eAgNnPc6d1	teenagerské
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
dětském	dětský	k2eAgInSc6d1	dětský
táboře	tábor	k1gInSc6	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Amy	Amy	k1gFnSc1	Amy
občas	občas	k6eAd1	občas
zpívala	zpívat	k5eAaImAgFnS	zpívat
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
ji	on	k3xPp3gFnSc4	on
uslyšel	uslyšet	k5eAaPmAgInS	uslyšet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zpívá	zpívat	k5eAaImIp3nS	zpívat
píseň	píseň	k1gFnSc4	píseň
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Do	do	k7c2	do
Anything	Anything	k1gInSc1	Anything
For	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
o	o	k7c6	o
společném	společný	k2eAgNnSc6d1	společné
založení	založení	k1gNnSc6	založení
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Začátky	začátek	k1gInPc1	začátek
a	a	k8xC	a
první	první	k4xOgFnSc2	první
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
Evanescence	Evanescence	k1gFnSc1	Evanescence
o	o	k7c4	o
sobě	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
dali	dát	k5eAaPmAgMnP	dát
vědět	vědět	k5eAaImF	vědět
v	v	k7c4	v
Little	Little	k1gFnPc4	Little
Rocku	rock	k1gInSc2	rock
na	na	k7c6	na
konci	konec	k1gInSc6	konec
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
tohoto	tento	k3xDgNnSc2	tento
ražení	ražení	k1gNnSc2	ražení
to	ten	k3xDgNnSc1	ten
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
místním	místní	k2eAgFnPc3d1	místní
hudebním	hudební	k2eAgFnPc3d1	hudební
tradicím	tradice	k1gFnPc3	tradice
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
Středozápadu	středozápad	k1gInSc3	středozápad
neměla	mít	k5eNaImAgFnS	mít
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
zde	zde	k6eAd1	zde
většinou	většinou	k6eAd1	většinou
poslouchají	poslouchat	k5eAaImIp3nP	poslouchat
death	death	k1gInSc4	death
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnPc1d2	starší
generace	generace	k1gFnPc1	generace
zase	zase	k9	zase
podstatně	podstatně	k6eAd1	podstatně
měkčí	měkký	k2eAgFnSc4d2	měkčí
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Lee	Lea	k1gFnSc3	Lea
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vlastně	vlastně	k9	vlastně
si	se	k3xPyFc3	se
nevzpomenu	vzpomenout	k5eNaPmIp1nS	vzpomenout
ani	ani	k8xC	ani
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
zdejší	zdejší	k2eAgFnSc4d1	zdejší
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
začali	začít	k5eAaPmAgMnP	začít
<g/>
,	,	kIx,	,
ovlivněni	ovlivnit	k5eAaPmNgMnP	ovlivnit
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
vydávat	vydávat	k5eAaPmF	vydávat
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgFnSc2	první
EP	EP	kA	EP
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
nedisponovali	disponovat	k5eNaBmAgMnP	disponovat
reputací	reputace	k1gFnSc7	reputace
získanou	získaný	k2eAgFnSc4d1	získaná
živými	živý	k2eAgNnPc7d1	živé
vystoupeními	vystoupení	k1gNnPc7	vystoupení
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
si	se	k3xPyFc3	se
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc4d1	dobré
renomé	renomé	k1gNnSc4	renomé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Paradoxně	paradoxně	k6eAd1	paradoxně
jsme	být	k5eAaImIp1nP	být
s	s	k7c7	s
Amy	Amy	k1gFnSc7	Amy
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
skrytí	skrytí	k1gNnSc4	skrytí
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
Moody	Moodo	k1gNnPc7	Moodo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
napsali	napsat	k5eAaPmAgMnP	napsat
druhou	druhý	k4xOgFnSc4	druhý
skladbu	skladba	k1gFnSc4	skladba
–	–	k?	–
sedmiminutový	sedmiminutový	k2eAgInSc1d1	sedmiminutový
gothic-rockový	gothicockový	k2eAgInSc1d1	gothic-rockový
hymnus	hymnus	k1gInSc1	hymnus
Understanding	Understanding	k1gInSc4	Understanding
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
ho	on	k3xPp3gMnSc4	on
z	z	k7c2	z
nám	my	k3xPp1nPc3	my
neznámého	známý	k2eNgInSc2d1	neznámý
důvodu	důvod	k1gInSc2	důvod
hodně	hodně	k6eAd1	hodně
často	často	k6eAd1	často
hrát	hrát	k5eAaImF	hrát
jedna	jeden	k4xCgFnSc1	jeden
místní	místní	k2eAgFnSc1d1	místní
rádiová	rádiový	k2eAgFnSc1d1	rádiová
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
jsme	být	k5eAaImIp1nP	být
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
nikdo	nikdo	k3yNnSc1	nikdo
nás	my	k3xPp1nPc4	my
vlastně	vlastně	k9	vlastně
neznal	neznat	k5eAaImAgMnS	neznat
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
nás	my	k3xPp1nPc4	my
mohl	moct	k5eAaImAgMnS	moct
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
nemohli	moct	k5eNaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
odehrát	odehrát	k5eAaPmF	odehrát
koncert	koncert	k1gInSc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
jen	jen	k9	jen
já	já	k3xPp1nSc1	já
a	a	k8xC	a
Amy	Amy	k1gFnSc1	Amy
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
na	na	k7c4	na
muzikanty	muzikant	k1gMnPc4	muzikant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
mohli	moct	k5eAaImAgMnP	moct
hrát	hrát	k5eAaImF	hrát
naživo	naživo	k1gNnSc4	naživo
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
prostě	prostě	k9	prostě
tehdy	tehdy	k6eAd1	tehdy
neměli	mít	k5eNaImAgMnP	mít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Evanescence	Evanescence	k1gFnPc4	Evanescence
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
album	album	k1gNnSc4	album
Origin	Origina	k1gFnPc2	Origina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
hitu	hit	k1gInSc2	hit
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Immortal	Immortal	k1gMnPc1	Immortal
<g/>
"	"	kIx"	"
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
podpory	podpora	k1gFnSc2	podpora
pro	pro	k7c4	pro
oficiální	oficiální	k2eAgNnSc4d1	oficiální
vydání	vydání	k1gNnSc4	vydání
druhé	druhý	k4xOgFnSc2	druhý
desky	deska	k1gFnSc2	deska
Fallen	Fallna	k1gFnPc2	Fallna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fallen	Fallen	k2eAgMnSc1d1	Fallen
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
Fallen	Fallna	k1gFnPc2	Fallna
===	===	k?	===
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
Fallen	Fallna	k1gFnPc2	Fallna
bylo	být	k5eAaImAgNnS	být
nahráváno	nahrávat	k5eAaImNgNnS	nahrávat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2001	[number]	k4	2001
a	a	k8xC	a
2003	[number]	k4	2003
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
u	u	k7c2	u
labelu	label	k1gInSc2	label
Wind-up	Windp	k1gInSc1	Wind-up
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Vznikalo	vznikat	k5eAaImAgNnS	vznikat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
kapele	kapela	k1gFnSc3	kapela
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
rovnováhu	rovnováha	k1gFnSc4	rovnováha
mezi	mezi	k7c7	mezi
křehkou	křehký	k2eAgFnSc7d1	křehká
krásou	krása	k1gFnSc7	krása
a	a	k8xC	a
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
rockovou	rockový	k2eAgFnSc7d1	rocková
tvrdostí	tvrdost	k1gFnSc7	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
deskou	deska	k1gFnSc7	deska
se	se	k3xPyFc4	se
Evanescence	Evanescenec	k1gMnSc2	Evanescenec
proslavili	proslavit	k5eAaPmAgMnP	proslavit
i	i	k9	i
za	za	k7c7	za
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
písní	píseň	k1gFnPc2	píseň
Bring	Bring	k1gMnSc1	Bring
Me	Me	k1gMnSc1	Me
To	ten	k3xDgNnSc4	ten
Life	Life	k1gNnSc4	Life
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozornili	upozornit	k5eAaPmAgMnP	upozornit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Daredevil	Daredevil	k1gFnSc2	Daredevil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odchod	odchod	k1gInSc1	odchod
Bena	Bena	k?	Bena
Moodyho	Moody	k1gMnSc2	Moody
===	===	k?	===
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
Ben	Ben	k1gInSc4	Ben
Moody	Mooda	k1gFnSc2	Mooda
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
hádce	hádka	k1gFnSc6	hádka
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
a	a	k8xC	a
názorech	názor	k1gInPc6	názor
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
bylo	být	k5eAaImAgNnS	být
zmateno	zmást	k5eAaPmNgNnS	zmást
touto	tento	k3xDgFnSc7	tento
změnou	změna	k1gFnSc7	změna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Fallen	Fallna	k1gFnPc2	Fallna
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
Moodyho	Moody	k1gMnSc2	Moody
podílela	podílet	k5eAaImAgFnS	podílet
také	také	k9	také
spousta	spousta	k1gFnSc1	spousta
jejich	jejich	k3xOp3gMnPc2	jejich
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
příznivci	příznivec	k1gMnPc1	příznivec
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evanescence	Evanescence	k1gFnSc1	Evanescence
kvůli	kvůli	k7c3	kvůli
ztrátě	ztráta	k1gFnSc3	ztráta
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
zakladatelů	zakladatel	k1gMnPc2	zakladatel
zaniknou	zaniknout	k5eAaPmIp3nP	zaniknout
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nestalo	stát	k5eNaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Moodyho	Moody	k1gMnSc4	Moody
post	post	k1gInSc4	post
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
byl	být	k5eAaImAgMnS	být
dosazen	dosazen	k2eAgMnSc1d1	dosazen
kytarista	kytarista	k1gMnSc1	kytarista
Terry	Terra	k1gFnSc2	Terra
Balsamo	Balsama	k1gFnSc5	Balsama
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Evanescence	Evanescence	k1gFnSc2	Evanescence
tvořili	tvořit	k5eAaImAgMnP	tvořit
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
klavíristka	klavíristka	k1gFnSc1	klavíristka
Amy	Amy	k1gFnSc2	Amy
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Terry	Terra	k1gFnSc2	Terra
Balsamo	Balsama	k1gFnSc5	Balsama
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
kytarista	kytarista	k1gMnSc1	kytarista
John	John	k1gMnSc1	John
LeCompt	LeCompt	k1gMnSc1	LeCompt
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
Will	Will	k1gMnSc1	Will
Boyd	Boyd	k1gMnSc1	Boyd
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Rocky	rock	k1gInPc4	rock
Gray	Gra	k2eAgFnPc1d1	Gra
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
kapely	kapela	k1gFnSc2	kapela
fanoušci	fanoušek	k1gMnPc1	fanoušek
označili	označit	k5eAaPmAgMnP	označit
jako	jako	k9	jako
lepší	dobrý	k2eAgMnPc1d2	lepší
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Anywhere	Anywher	k1gMnSc5	Anywher
But	But	k1gMnSc5	But
Home	Homus	k1gMnSc5	Homus
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vydání	vydání	k1gNnSc4	vydání
koncertního	koncertní	k2eAgInSc2d1	koncertní
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
přibylo	přibýt	k5eAaPmAgNnS	přibýt
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
Evanescence	Evanescence	k1gFnSc2	Evanescence
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Anywhere	Anywher	k1gMnSc5	Anywher
But	But	k1gMnSc5	But
Home	Homus	k1gMnSc5	Homus
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
audio	audio	k2eAgFnSc4d1	audio
a	a	k8xC	a
videozáznam	videozáznam	k1gInSc4	videozáznam
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rocková	rockový	k2eAgFnSc1d1	rocková
formace	formace	k1gFnSc1	formace
zahrála	zahrát	k5eAaPmAgFnS	zahrát
nejen	nejen	k6eAd1	nejen
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
Fallen	Fallna	k1gFnPc2	Fallna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
představila	představit	k5eAaPmAgFnS	představit
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
oficiálně	oficiálně	k6eAd1	oficiálně
nevydané	vydaný	k2eNgFnPc1d1	nevydaná
písně	píseň	k1gFnPc1	píseň
Missing	Missing	k1gInSc1	Missing
<g/>
,	,	kIx,	,
Breathe	Breathe	k1gInSc1	Breathe
No	no	k9	no
More	mor	k1gInSc5	mor
a	a	k8xC	a
Farther	Farthra	k1gFnPc2	Farthra
Away	Awaa	k1gFnSc2	Awaa
a	a	k8xC	a
také	také	k9	také
cover	cover	k1gInSc1	cover
písně	píseň	k1gFnSc2	píseň
Thoughtless	Thoughtlessa	k1gFnPc2	Thoughtlessa
od	od	k7c2	od
KoRn	KoRna	k1gFnPc2	KoRna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změna	změna	k1gFnSc1	změna
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
baskytaristy	baskytarista	k1gMnSc2	baskytarista
===	===	k?	===
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
baskytarista	baskytarista	k1gMnSc1	baskytarista
Will	Will	k1gMnSc1	Will
Boyd	Boyd	k1gMnSc1	Boyd
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
<g/>
?	?	kIx.	?
</s>
<s>
Údajně	údajně	k6eAd1	údajně
již	již	k6eAd1	již
nechtěl	chtít	k5eNaImAgMnS	chtít
absolvovat	absolvovat	k5eAaPmF	absolvovat
další	další	k2eAgNnSc4d1	další
náročné	náročný	k2eAgNnSc4d1	náročné
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
světě	svět	k1gInSc6	svět
–	–	k?	–
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgMnSc3	ten
nebyl	být	k5eNaImAgInS	být
vůbec	vůbec	k9	vůbec
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
rád	rád	k6eAd1	rád
věnoval	věnovat	k5eAaPmAgMnS	věnovat
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
změnu	změna	k1gFnSc4	změna
oznámila	oznámit	k5eAaPmAgFnS	oznámit
fanouškům	fanoušek	k1gMnPc3	fanoušek
sama	sám	k3xTgFnSc1	sám
Amy	Amy	k1gFnSc1	Amy
Lee	Lea	k1gFnSc3	Lea
na	na	k7c6	na
neoficiální	neoficiální	k2eAgFnSc6d1	neoficiální
webové	webový	k2eAgFnSc6d1	webová
diskusní	diskusní	k2eAgFnSc6d1	diskusní
stránce	stránka	k1gFnSc6	stránka
EvBoard	EvBoard	k1gInSc1	EvBoard
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
interview	interview	k1gNnSc6	interview
s	s	k7c7	s
MTV	MTV	kA	MTV
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Amy	Amy	k1gFnSc1	Amy
Lee	Lea	k1gFnSc3	Lea
představila	představit	k5eAaPmAgFnS	představit
nového	nový	k2eAgMnSc4d1	nový
člena	člen	k1gMnSc4	člen
jménem	jméno	k1gNnSc7	jméno
Tim	Tim	k?	Tim
McCord	McCord	k1gMnSc1	McCord
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
opustil	opustit	k5eAaPmAgMnS	opustit
svou	svůj	k3xOyFgFnSc4	svůj
předchozí	předchozí	k2eAgFnSc4d1	předchozí
kapelu	kapela	k1gFnSc4	kapela
Revolution	Revolution	k1gInSc1	Revolution
Smile	smil	k1gInSc5	smil
a	a	k8xC	a
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
kytaru	kytara	k1gFnSc4	kytara
za	za	k7c4	za
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
The	The	k1gMnSc1	The
Open	Open	k1gMnSc1	Open
Door	Door	k1gMnSc1	Door
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
a	a	k8xC	a
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Amy	Amy	k1gFnSc3	Amy
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
John	John	k1gMnSc1	John
LeCompt	LeCompt	k1gMnSc1	LeCompt
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
města	město	k1gNnSc2	město
Londýn	Londýn	k1gInSc4	Londýn
<g/>
,	,	kIx,	,
Barcelonu	Barcelona	k1gFnSc4	Barcelona
a	a	k8xC	a
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
turné	turné	k1gNnSc6	turné
vydali	vydat	k5eAaPmAgMnP	vydat
další	další	k2eAgNnSc4d1	další
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Open	Open	k1gMnSc1	Open
Door	Door	k1gMnSc1	Door
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vydáno	vydat	k5eAaPmNgNnS	vydat
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
13	[number]	k4	13
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oficiálními	oficiální	k2eAgInPc7d1	oficiální
singly	singl	k1gInPc7	singl
se	se	k3xPyFc4	se
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
staly	stát	k5eAaPmAgFnP	stát
Call	Call	k1gInSc4	Call
Me	Me	k1gFnSc2	Me
When	Whena	k1gFnPc2	Whena
You	You	k1gFnSc7	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Sober	Sober	k1gInSc1	Sober
<g/>
,	,	kIx,	,
Lithium	lithium	k1gNnSc1	lithium
<g/>
,	,	kIx,	,
Sweet	Sweet	k1gInSc1	Sweet
Sacrifice	Sacrifice	k1gFnSc2	Sacrifice
a	a	k8xC	a
Good	Good	k1gMnSc1	Good
Enough	Enough	k1gMnSc1	Enough
<g/>
.	.	kIx.	.
</s>
<s>
Jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
písně	píseň	k1gFnPc1	píseň
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
videoklipy	videoklip	k1gInPc4	videoklip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
turné	turné	k1gNnSc1	turné
Evanescence	Evanescence	k1gFnSc2	Evanescence
začalo	začít	k5eAaPmAgNnS	začít
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
přes	přes	k7c4	přes
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
opět	opět	k6eAd1	opět
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
The	The	k1gFnPc3	The
Open	Opena	k1gFnPc2	Opena
Door	Doora	k1gFnPc2	Doora
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Family	Famila	k1gFnSc2	Famila
Values	Values	k1gInSc1	Values
Tour	Tour	k1gInSc1	Tour
2007	[number]	k4	2007
s	s	k7c7	s
KoRn	KoRn	k1gInSc1	KoRn
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
také	také	k9	také
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
neznámého	známý	k2eNgInSc2d1	neznámý
důvodu	důvod	k1gInSc2	důvod
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amy	Amy	k?	Amy
Lee	Lea	k1gFnSc6	Lea
napsala	napsat	k5eAaBmAgFnS	napsat
také	také	k9	také
píseň	píseň	k1gFnSc1	píseň
Together	Togethra	k1gFnPc2	Togethra
Again	Againa	k1gFnPc2	Againa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
použita	použít	k5eAaPmNgFnS	použít
pro	pro	k7c4	pro
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Letopisy	letopis	k1gInPc7	letopis
Narnie	Narnie	k1gFnSc2	Narnie
-	-	kIx~	-
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
Čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
Skříň	skříň	k1gFnSc1	skříň
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
píseň	píseň	k1gFnSc4	píseň
nakonec	nakonec	k6eAd1	nakonec
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
s	s	k7c7	s
vyjádřením	vyjádření	k1gNnSc7	vyjádření
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
temná	temnat	k5eAaImIp3nS	temnat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešli	odejít	k5eAaPmAgMnP	odejít
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
–	–	k?	–
bubeník	bubeník	k1gMnSc1	bubeník
Rocky	rock	k1gInPc7	rock
Gray	Graa	k1gFnSc2	Graa
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
John	John	k1gMnSc1	John
LeCompt	LeCompt	k1gMnSc1	LeCompt
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
věnovali	věnovat	k5eAaPmAgMnP	věnovat
i	i	k8xC	i
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
tvrdšími	tvrdý	k2eAgFnPc7d2	tvrdší
kapelami	kapela	k1gFnPc7	kapela
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
ohledně	ohledně	k7c2	ohledně
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Amy	Amy	k?	Amy
Lee	Lea	k1gFnSc6	Lea
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
svatbou	svatba	k1gFnSc7	svatba
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2007	[number]	k4	2007
najímá	najímat	k5eAaImIp3nS	najímat
na	na	k7c4	na
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
výpomoc	výpomoc	k1gFnSc4	výpomoc
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgMnPc4d1	nový
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
:	:	kIx,	:
Troye	Troye	k1gFnSc1	Troye
McLawhorna	McLawhorna	k1gFnSc1	McLawhorna
a	a	k8xC	a
Willa	Willa	k1gFnSc1	Willa
Hunta	Hunt	k1gMnSc2	Hunt
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Dark	Dark	k1gMnSc1	Dark
New	New	k1gMnSc1	New
Day	Day	k1gMnSc1	Day
–	–	k?	–
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
vdává	vdávat	k5eAaImIp3nS	vdávat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
terapeuta	terapeut	k1gMnSc4	terapeut
jménem	jméno	k1gNnSc7	jméno
Josh	Josh	k1gMnSc1	Josh
Hartzler	Hartzler	k1gMnSc1	Hartzler
<g/>
.	.	kIx.	.
</s>
<s>
Dočasní	dočasný	k2eAgMnPc1d1	dočasný
členové	člen	k1gMnPc1	člen
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
sestavu	sestava	k1gFnSc4	sestava
Evanescence	Evanescence	k1gFnSc2	Evanescence
během	během	k7c2	během
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
The	The	k1gFnSc2	The
Open	Open	k1gInSc1	Open
Door	Door	k1gMnSc1	Door
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
vracejí	vracet	k5eAaImIp3nP	vracet
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
původní	původní	k2eAgFnSc3d1	původní
kapele	kapela	k1gFnSc3	kapela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Together	Togethra	k1gFnPc2	Togethra
Again	Again	k1gInSc1	Again
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
The	The	k1gMnPc2	The
Open	Open	k1gInSc4	Open
Door	Doora	k1gFnPc2	Doora
vyšvihla	vyšvihnout	k5eAaPmAgFnS	vyšvihnout
Evanescence	Evanescence	k1gFnSc1	Evanescence
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
ve	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Together	Togethra	k1gFnPc2	Togethra
Again	Againa	k1gFnPc2	Againa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
nebyla	být	k5eNaImAgFnS	být
na	na	k7c4	na
album	album	k1gNnSc4	album
zařazena	zařazen	k2eAgFnSc1d1	zařazena
<g/>
,	,	kIx,	,
vydávají	vydávat	k5eAaPmIp3nP	vydávat
Evanescence	Evanescence	k1gFnPc4	Evanescence
až	až	k9	až
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
skupina	skupina	k1gFnSc1	skupina
oficiálně	oficiálně	k6eAd1	oficiálně
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
partnerství	partnerství	k1gNnSc2	partnerství
s	s	k7c7	s
nadací	nadace	k1gFnSc7	nadace
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Foundation	Foundation	k1gInSc4	Foundation
a	a	k8xC	a
hodlají	hodlat	k5eAaImIp3nP	hodlat
finančně	finančně	k6eAd1	finančně
podpořit	podpořit	k5eAaPmF	podpořit
jejich	jejich	k3xOp3gInSc4	jejich
program	program	k1gInSc4	program
pro	pro	k7c4	pro
výpomoc	výpomoc	k1gFnSc4	výpomoc
oblastem	oblast	k1gFnPc3	oblast
na	na	k7c6	na
Haiti	Haiti	k1gNnSc6	Haiti
<g/>
,	,	kIx,	,
postiženým	postižený	k2eAgNnSc7d1	postižené
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nevydaný	vydaný	k2eNgInSc1d1	nevydaný
song	song	k1gInSc1	song
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
zaslal	zaslat	k5eAaPmAgMnS	zaslat
dobrovolný	dobrovolný	k2eAgInSc4d1	dobrovolný
příspěvek	příspěvek	k1gInSc4	příspěvek
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
tím	ten	k3xDgNnSc7	ten
United	United	k1gMnSc1	United
Nations	Nations	k1gInSc1	Nations
<g/>
'	'	kIx"	'
Central	Central	k1gFnSc1	Central
Emergency	Emergenca	k1gFnSc2	Emergenca
Response	response	k1gFnSc1	response
Fund	fund	k1gInSc1	fund
(	(	kIx(	(
<g/>
CERF	CERF	kA	CERF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Evanescence	Evanescence	k1gFnSc2	Evanescence
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgNnSc1	třetí
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
===	===	k?	===
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
Evanescence	Evanescence	k1gFnPc1	Evanescence
<g/>
.	.	kIx.	.
</s>
<s>
Objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
–	–	k?	–
standardní	standardní	k2eAgInSc1d1	standardní
CD	CD	kA	CD
se	s	k7c7	s
dvanácti	dvanáct	k4xCc7	dvanáct
skladbami	skladba	k1gFnPc7	skladba
a	a	k8xC	a
"	"	kIx"	"
<g/>
Deluxe	Deluxe	k1gFnSc1	Deluxe
edition	edition	k1gInSc1	edition
<g/>
"	"	kIx"	"
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
bonusovými	bonusový	k2eAgFnPc7d1	bonusová
skladbami	skladba	k1gFnPc7	skladba
a	a	k8xC	a
přiloženým	přiložený	k2eAgMnSc7d1	přiložený
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
videoklipy	videoklip	k1gInPc1	videoklip
a	a	k8xC	a
záběry	záběr	k1gInPc1	záběr
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
třetího	třetí	k4xOgNnSc2	třetí
alba	album	k1gNnSc2	album
začalo	začít	k5eAaPmAgNnS	začít
již	již	k6eAd1	již
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kapela	kapela	k1gFnSc1	kapela
oficiálně	oficiálně	k6eAd1	oficiálně
oznámila	oznámit	k5eAaPmAgFnS	oznámit
přesun	přesun	k1gInSc4	přesun
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
nové	nový	k2eAgFnSc2d1	nová
desky	deska	k1gFnSc2	deska
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
nejprve	nejprve	k6eAd1	nejprve
Steve	Steve	k1gMnSc5	Steve
Lillywhite	Lillywhit	k1gMnSc5	Lillywhit
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
oznámena	oznámen	k2eAgFnSc1d1	oznámena
změna	změna	k1gFnSc1	změna
a	a	k8xC	a
desku	deska	k1gFnSc4	deska
produkoval	produkovat	k5eAaImAgMnS	produkovat
Nick	Nick	k1gMnSc1	Nick
Raskulinecz	Raskulinecz	k1gMnSc1	Raskulinecz
(	(	kIx(	(
<g/>
Foo	Foo	k1gFnSc1	Foo
Fighters	Fighters	k1gInSc1	Fighters
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
in	in	k?	in
Chains	Chains	k1gInSc1	Chains
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
později	pozdě	k6eAd2	pozdě
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Steve	Steve	k1gMnSc1	Steve
prostě	prostě	k9	prostě
nebyl	být	k5eNaImAgMnS	být
správná	správný	k2eAgFnSc1d1	správná
volba	volba	k1gFnSc1	volba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
značkou	značka	k1gFnSc7	značka
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
Blackbird	Blackbirdo	k1gNnPc2	Blackbirdo
Studios	Studios	k?	Studios
v	v	k7c6	v
Nashville	Nashvill	k1gInSc6	Nashvill
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc5	Tennessee
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
hudebním	hudební	k2eAgInSc7d1	hudební
magazínem	magazín	k1gInSc7	magazín
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
později	pozdě	k6eAd2	pozdě
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaImF	jmenovat
jednoduše	jednoduše	k6eAd1	jednoduše
"	"	kIx"	"
<g/>
Evanescence	Evanescence	k1gFnSc1	Evanescence
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
o	o	k7c6	o
kapele	kapela	k1gFnSc6	kapela
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
do	do	k7c2	do
Evanescence	Evanescence	k1gFnSc2	Evanescence
zamilovat	zamilovat	k5eAaPmF	zamilovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
tématy	téma	k1gNnPc7	téma
jsou	být	k5eAaImIp3nP	být
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
What	What	k1gMnSc1	What
You	You	k1gMnSc1	You
Want	Want	k1gMnSc1	Want
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgMnS	vyjít
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
vyšel	vyjít	k5eAaPmAgInS	vyjít
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
jménem	jméno	k1gNnSc7	jméno
My	my	k3xPp1nPc1	my
Heart	Heart	k1gInSc4	Heart
is	is	k?	is
Broken	Broken	k1gInSc1	Broken
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Fallen	Fallen	k1gInSc1	Fallen
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Open	Open	k1gMnSc1	Open
Door	Door	k1gMnSc1	Door
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evanescence	Evanescence	k1gFnSc1	Evanescence
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Synthesis	Synthesis	k1gFnSc1	Synthesis
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
Evanescence	Evanescence	k1gFnSc1	Evanescence
EP	EP	kA	EP
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sound	Sound	k1gMnSc1	Sound
Asleep	Asleep	k1gMnSc1	Asleep
<g/>
/	/	kIx~	/
<g/>
Whisper	Whisper	k1gMnSc1	Whisper
EP	EP	kA	EP
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mystery	Myster	k1gInPc1	Myster
EP	EP	kA	EP
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Demo	demo	k2eAgFnPc1d1	demo
nahrávky	nahrávka	k1gFnPc1	nahrávka
===	===	k?	===
</s>
</p>
<p>
<s>
Origin	Origin	k1gInSc1	Origin
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Evanescence	Evanescence	k1gFnSc2	Evanescence
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evanescence	Evanescence	k1gFnSc2	Evanescence
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
USA	USA	kA	USA
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
VB	VB	kA	VB
</s>
</p>
