<s>
Mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
amerických	americký	k2eAgFnPc2d1	americká
kulturních	kulturní	k2eAgFnPc2d1	kulturní
hnutí	hnutí	k1gNnPc4	hnutí
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Harlem	Harl	k1gInSc7	Harl
Renaissance	Renaissance	k1gFnSc2	Renaissance
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
formovala	formovat	k5eAaImAgFnS	formovat
černošskou	černošský	k2eAgFnSc4d1	černošská
literaturu	literatura	k1gFnSc4	literatura
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
