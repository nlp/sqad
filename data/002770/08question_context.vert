<s>
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
(	(	kIx(	(
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
6959	[number]	k4	6959
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
nalézající	nalézající	k2eAgFnSc1d1	nalézající
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
mimo	mimo	k7c4	mimo
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
