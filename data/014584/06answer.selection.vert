<s>
Podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
(	(	kIx(
<g/>
též	též	k6eAd1
substantivum	substantivum	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ohebný	ohebný	k2eAgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
označuje	označovat	k5eAaImIp3nS
názvy	název	k1gInPc4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
dějů	děj	k1gInPc2
a	a	k8xC
vztahů	vztah	k1gInPc2
<g/>
.	.	kIx.
</s>