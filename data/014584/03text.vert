<s>
Podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
(	(	kIx(
<g/>
též	též	k6eAd1
substantivum	substantivum	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ohebný	ohebný	k2eAgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
označuje	označovat	k5eAaImIp3nS
názvy	název	k1gInPc4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
dějů	děj	k1gInPc2
a	a	k8xC
vztahů	vztah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
(	(	kIx(
<g/>
i	i	k8xC
když	když	k8xS
ne	ne	k9
jediným	jediný	k2eAgInSc7d1
<g/>
)	)	kIx)
slovním	slovní	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ve	v	k7c6
větě	věta	k1gFnSc6
vystupuje	vystupovat	k5eAaImIp3nS
jako	jako	k9
argument	argument	k1gInSc1
slovesa	sloveso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
podle	podle	k7c2
obecnosti	obecnost	k1gFnSc2
</s>
<s>
Podle	podle	k7c2
významu	význam	k1gInSc2
lze	lze	k6eAd1
rozlišovat	rozlišovat	k5eAaImF
podstatná	podstatný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
konkrétní	konkrétní	k2eAgNnPc4d1
(	(	kIx(
<g/>
konkréta	konkrétum	k1gNnPc4
<g/>
,	,	kIx,
sg.	sg.	k?
konkrétum	konkrétum	k1gNnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
–	–	k?
označují	označovat	k5eAaImIp3nP
názvy	název	k1gInPc1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
zvířat	zvíře	k1gNnPc2
a	a	k8xC
věcí	věc	k1gFnPc2
<g/>
;	;	kIx,
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
dělit	dělit	k5eAaImF
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
obecná	obecný	k2eAgNnPc4d1
(	(	kIx(
<g/>
apelativa	apelativum	k1gNnPc4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
vlastní	vlastní	k2eAgNnPc4d1
(	(	kIx(
<g/>
propria	proprium	k1gNnPc4
<g/>
)	)	kIx)
</s>
<s>
abstraktní	abstraktní	k2eAgMnSc1d1
(	(	kIx(
<g/>
abstrakta	abstraktum	k1gNnSc2
<g/>
,	,	kIx,
sg.	sg.	k?
abstraktum	abstraktum	k1gNnSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
–	–	k?
vyjadřují	vyjadřovat	k5eAaImIp3nP
názvy	název	k1gInPc1
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
dějů	děj	k1gInPc2
<g/>
,	,	kIx,
činností	činnost	k1gFnPc2
a	a	k8xC
stavů	stav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
slovesná	slovesný	k2eAgNnPc1d1
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
slovesná	slovesný	k2eAgNnPc1d1
jsou	být	k5eAaImIp3nP
odvozena	odvodit	k5eAaPmNgFnS
ze	z	k7c2
slovesného	slovesný	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
u	u	k7c2
sloves	sloveso	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
nemají	mít	k5eNaImIp3nP
příčestí	příčestí	k1gNnSc4
trpné	trpný	k2eAgNnSc4d1
<g/>
,	,	kIx,
za	za	k7c2
pomoci	pomoc	k1gFnSc2
přípony	přípona	k1gFnSc2
-nutí	-nuť	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
procitnout	procitnout	k5eAaPmF
–	–	k?
procitnutí	procitnutí	k1gNnSc4
<g/>
,	,	kIx,
stárnout	stárnout	k5eAaImF
–	–	k?
stárnutí	stárnutí	k1gNnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
u	u	k7c2
sloves	sloveso	k1gNnPc2
s	s	k7c7
příčestím	příčestí	k1gNnSc7
trpným	trpný	k2eAgNnSc7d1
končícím	končící	k2eAgMnSc7d1
na	na	k7c6
-en	-en	k?
za	za	k7c2
pomoci	pomoc	k1gFnSc2
přípony	přípona	k1gFnSc2
-ení	-ení	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
nalezen	nalezen	k2eAgInSc1d1
–	–	k?
nalezení	nalezení	k1gNnPc2
<g/>
,	,	kIx,
ukraden	ukraden	k2eAgMnSc1d1
–	–	k?
ukradení	ukradení	k1gNnSc6
<g/>
,	,	kIx,
zasažen	zasažen	k2eAgInSc1d1
–	–	k?
zasažení	zasažení	k1gNnPc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
s	s	k7c7
omezeným	omezený	k2eAgNnSc7d1
číselným	číselný	k2eAgNnSc7d1
paradigmatem	paradigma	k1gNnSc7
</s>
<s>
Substantiva	substantivum	k1gNnPc1
obvykle	obvykle	k6eAd1
vyjadřují	vyjadřovat	k5eAaImIp3nP
jednotné	jednotný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
i	i	k8xC
množné	množný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
však	však	k9
skupiny	skupina	k1gFnPc1
substantiv	substantivum	k1gNnPc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
je	být	k5eAaImIp3nS
tvoření	tvoření	k1gNnSc2
obou	dva	k4xCgNnPc2
čísel	číslo	k1gNnPc2
omezené	omezený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
je	být	k5eAaImIp3nS
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
singularia	singulare	k1gNnPc4
a	a	k8xC
pluralia	plurale	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Singularia	singulare	k1gNnPc1
</s>
<s>
Singularia	singulare	k1gNnPc1
tvoří	tvořit	k5eAaImIp3nP
výhradně	výhradně	k6eAd1
nebo	nebo	k8xC
převážně	převážně	k6eAd1
jednotné	jednotný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označují	označovat	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
jevy	jev	k1gInPc1
<g/>
,	,	kIx,
vnímané	vnímaný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
nestrukturované	strukturovaný	k2eNgInPc1d1
<g/>
,	,	kIx,
neohraničené	ohraničený	k2eNgInPc1d1
<g/>
,	,	kIx,
jedinečné	jedinečný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množné	množný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
,	,	kIx,
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
označení	označení	k1gNnSc3
druhové	druhový	k2eAgFnSc2d1
různosti	různost	k1gFnSc2
<g/>
,	,	kIx,
intenzity	intenzita	k1gFnSc2
nebo	nebo	k8xC
opakování	opakování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jich	on	k3xPp3gMnPc2
několik	několik	k4yIc1
druhů	druh	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
jména	jméno	k1gNnPc1
látková	látkový	k2eAgNnPc1d1
–	–	k?
označují	označovat	k5eAaImIp3nP
jednolitý	jednolitý	k2eAgInSc4d1
materiální	materiální	k2eAgInSc4d1
jev	jev	k1gInSc4
<g/>
,	,	kIx,
vnitřně	vnitřně	k6eAd1
nečleněné	členěný	k2eNgNnSc1d1
kontinuum	kontinuum	k1gNnSc1
–	–	k?
potraviny	potravina	k1gFnPc4
<g/>
,	,	kIx,
chemické	chemický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
soudržné	soudržný	k2eAgInPc4d1
materiály	materiál	k1gInPc4
apod.	apod.	kA
<g/>
:	:	kIx,
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
olej	olej	k1gInSc1
<g/>
,	,	kIx,
písek	písek	k1gInSc1
<g/>
,	,	kIx,
mouka	mouka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plurál	plurál	k1gInSc1
označuje	označovat	k5eAaImIp3nS
jasně	jasně	k6eAd1
vymezený	vymezený	k2eAgInSc1d1
celek	celek	k1gInSc1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
vody	voda	k1gFnPc1
–	–	k?
tj.	tj.	kA
lahve	lahev	k1gFnSc2
vody	voda	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
intenzitu	intenzita	k1gFnSc4
(	(	kIx(
<g/>
rozbouřené	rozbouřený	k2eAgFnPc1d1
vody	voda	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
jména	jméno	k1gNnPc4
hromadná	hromadný	k2eAgFnSc1d1
–	–	k?
označují	označovat	k5eAaImIp3nP
fyzicky	fyzicky	k6eAd1
členitelný	členitelný	k2eAgInSc4d1
jev	jev	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
vnímán	vnímat	k5eAaImNgInS
jako	jako	k9
jev	jev	k1gInSc1
jednolitý	jednolitý	k2eAgInSc1d1
<g/>
:	:	kIx,
nádobí	nádobí	k1gNnSc1
<g/>
,	,	kIx,
listí	listí	k1gNnSc1
<g/>
,	,	kIx,
žactvo	žactvo	k1gNnSc1
<g/>
,	,	kIx,
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc1
<g/>
,	,	kIx,
borůvčí	borůvčí	k1gNnSc1
<g/>
,	,	kIx,
stádo	stádo	k1gNnSc1
<g/>
,	,	kIx,
roj	roj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plurál	plurál	k1gInSc1
se	se	k3xPyFc4
u	u	k7c2
nich	on	k3xPp3gInPc2
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
chceme	chtít	k5eAaImIp1nP
kvantifikovat	kvantifikovat	k5eAaBmF
několik	několik	k4yIc4
jasně	jasně	k6eAd1
vymezených	vymezený	k2eAgInPc2d1
celků	celek	k1gInPc2
(	(	kIx(
<g/>
znepřátelené	znepřátelený	k2eAgFnPc1d1
armády	armáda	k1gFnPc1
<g/>
,	,	kIx,
vlčí	vlčí	k2eAgFnPc1d1
smečky	smečka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
abstrakta	abstraktum	k1gNnSc2
–	–	k?
singularita	singularita	k1gFnSc1
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
jejich	jejich	k3xOp3gFnSc2
povahy	povaha	k1gFnSc2
–	–	k?
označují	označovat	k5eAaImIp3nP
nefyzické	fyzický	k2eNgInPc1d1
jevy	jev	k1gInPc1
<g/>
:	:	kIx,
nebezpečí	nebezpečí	k1gNnSc1
<g/>
,	,	kIx,
lakomost	lakomost	k1gFnSc1
<g/>
,	,	kIx,
demokracie	demokracie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
konkretizaci	konkretizace	k1gFnSc3
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
snadno	snadno	k6eAd1
plurál	plurál	k1gInSc4
tvořit	tvořit	k5eAaImF
<g/>
:	:	kIx,
pronášená	pronášený	k2eAgNnPc1d1
moudra	moudro	k1gNnPc1
<g/>
,	,	kIx,
zapomenuté	zapomenutý	k2eAgFnPc1d1
lásky	láska	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
unika	unikum	k1gNnSc2
–	–	k?
slova	slovo	k1gNnSc2
označující	označující	k2eAgInPc1d1
jednotlivé	jednotlivý	k2eAgInPc1d1
jevy	jev	k1gInPc1
<g/>
:	:	kIx,
vesmír	vesmír	k1gInSc1
<g/>
,	,	kIx,
svět	svět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
nimi	on	k3xPp3gMnPc7
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
vlastní	vlastní	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
(	(	kIx(
<g/>
propria	proprium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
označují	označovat	k5eAaImIp3nP
jednotlivou	jednotlivý	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
(	(	kIx(
<g/>
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
jev	jev	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jména	jméno	k1gNnPc1
míst	místo	k1gNnPc2
mají	mít	k5eAaImIp3nP
často	často	k6eAd1
gramatický	gramatický	k2eAgInSc4d1
tvar	tvar	k1gInSc4
množného	množný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
(	(	kIx(
<g/>
Kravaře	kravař	k1gMnSc2
<g/>
,	,	kIx,
Postoloprty	Postoloprta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plurál	plurál	k1gInSc1
u	u	k7c2
proprií	proprium	k1gNnPc2
a	a	k8xC
unik	unikum	k1gNnPc2
vzniká	vznikat	k5eAaImIp3nS
výjimečně	výjimečně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
nikoli	nikoli	k9
nesystémově	systémově	k6eNd1
<g/>
:	:	kIx,
v	v	k7c6
naší	náš	k3xOp1gFnSc6
třídě	třída	k1gFnSc6
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgFnPc4
Lukášové	Lukášová	k1gFnPc4
–	–	k?
znám	znát	k5eAaImIp1nS
několik	několik	k4yIc4
Lhot	Lhota	k1gFnPc2
–	–	k?
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
Prah	Praha	k1gFnPc2
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
nahoře	nahoře	k6eAd1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
dole	dole	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plurál	plurál	k1gInSc4
také	také	k9
někdy	někdy	k6eAd1
může	moct	k5eAaImIp3nS
signalizovat	signalizovat	k5eAaImF
významový	významový	k2eAgInSc1d1
posun	posun	k1gInSc1
(	(	kIx(
<g/>
malí	malý	k2eAgMnPc1d1
Einsteinové	Einstein	k1gMnPc1
<g/>
;	;	kIx,
pátrejte	pátrat	k5eAaImRp2nP
<g/>
,	,	kIx,
Sherlockové	Sherlockové	k2eAgInSc4d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
označení	označení	k1gNnSc1
značky	značka	k1gFnSc2
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
Toyoty	toyota	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmnožení	zmnožení	k1gNnSc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
používá	používat	k5eAaImIp3nS
také	také	k9
pro	pro	k7c4
deindividualizaci	deindividualizace	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
má	mít	k5eAaImIp3nS
označovanou	označovaný	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
znevážit	znevážit	k5eAaPmF
<g/>
:	:	kIx,
A	a	k9
další	další	k2eAgFnPc1d1
<g/>
,	,	kIx,
né	né	k?
600	#num#	k4
<g/>
,	,	kIx,
milión	milión	k4xCgInSc1
<g/>
,	,	kIx,
dva	dva	k4xCgInPc1
milióny	milión	k4xCgInPc1
berou	brát	k5eAaImIp3nP
Jandové	Jandové	k2eAgMnSc1d1
a	a	k8xC
jiní	jiný	k1gMnPc1
<g/>
,	,	kIx,
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pluralia	plurale	k1gNnPc1
</s>
<s>
Pluralia	plurale	k1gNnPc1
neboli	neboli	k8xC
jména	jméno	k1gNnPc1
pomnožná	pomnožný	k2eAgNnPc1d1
mají	mít	k5eAaImIp3nP
tvar	tvar	k1gInSc4
množného	množný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
označují	označovat	k5eAaImIp3nP
jedinou	jediný	k2eAgFnSc4d1
věc	věc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
především	především	k6eAd1
názvy	název	k1gInPc4
předmětů	předmět	k1gInPc2
skládajících	skládající	k2eAgInPc2d1
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgFnPc2
stejných	stejný	k2eAgFnPc2d1
částí	část	k1gFnPc2
(	(	kIx(
<g/>
nůžky	nůžky	k1gFnPc4
<g/>
,	,	kIx,
kleště	kleště	k1gFnPc4
<g/>
,	,	kIx,
brýle	brýle	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
běžné	běžný	k2eAgInPc1d1
předměty	předmět	k1gInPc1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgMnPc2
je	být	k5eAaImIp3nS
původní	původní	k2eAgFnPc4d1
motivace	motivace	k1gFnPc4
složení	složení	k1gNnSc2
z	z	k7c2
více	hodně	k6eAd2
částí	část	k1gFnPc2
již	již	k6eAd1
skryta	skrýt	k5eAaPmNgFnS
(	(	kIx(
<g/>
dveře	dveře	k1gFnPc1
<g/>
,	,	kIx,
kamna	kamna	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
části	část	k1gFnPc4
těla	tělo	k1gNnSc2
(	(	kIx(
<g/>
záda	záda	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nemoci	nemoc	k1gFnPc1
(	(	kIx(
<g/>
spalničky	spalničky	k1gFnPc1
<g/>
,	,	kIx,
neštovice	neštovice	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
svátky	svátek	k1gInPc1
(	(	kIx(
<g/>
Vánoce	Vánoce	k1gFnPc1
<g/>
,	,	kIx,
Velikonoce	Velikonoce	k1gFnPc1
<g/>
)	)	kIx)
aj.	aj.	kA
Některá	některý	k3yIgNnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
mohou	moct	k5eAaImIp3nP
jednotné	jednotný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
tvořit	tvořit	k5eAaImF
ve	v	k7c6
specifických	specifický	k2eAgInPc6d1
případech	případ	k1gInPc6
(	(	kIx(
<g/>
roztržená	roztržený	k2eAgFnSc1d1
kalhota	kalhota	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mluvnické	mluvnický	k2eAgFnPc1d1
kategorie	kategorie	k1gFnPc1
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
</s>
<s>
U	u	k7c2
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
určujeme	určovat	k5eAaImIp1nP
následující	následující	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
<g/>
:	:	kIx,
pád	pád	k1gInSc4
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
rod	rod	k1gInSc4
a	a	k8xC
vzor	vzor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pád	Pád	k1gInSc1
(	(	kIx(
<g/>
casus	casus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Mluvnický	mluvnický	k2eAgInSc4d1
pád	pád	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Mluvnický	mluvnický	k2eAgInSc1d1
pád	pád	k1gInSc1
je	být	k5eAaImIp3nS
mluvnická	mluvnický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
flektivní	flektivní	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
obvykle	obvykle	k6eAd1
vyjadřují	vyjadřovat	k5eAaImIp3nP
vztah	vztah	k1gInSc4
mluvnických	mluvnický	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
(	(	kIx(
<g/>
podstatná	podstatný	k2eAgNnPc1d1
<g/>
,	,	kIx,
přídavná	přídavný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
<g/>
,	,	kIx,
zájmena	zájmeno	k1gNnPc1
<g/>
,	,	kIx,
číslovky	číslovka	k1gFnPc1
<g/>
)	)	kIx)
k	k	k7c3
přísudku	přísudek	k1gInSc3
či	či	k8xC
jiným	jiný	k2eAgMnPc3d1
větným	větný	k2eAgMnPc3d1
členům	člen	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pády	Pád	k1gInPc7
zpravidla	zpravidla	k6eAd1
mění	měnit	k5eAaImIp3nS
tato	tento	k3xDgNnPc4
jména	jméno	k1gNnPc4
podle	podle	k7c2
koncovek	koncovka	k1gFnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
uvnitř	uvnitř	k7c2
slova	slovo	k1gNnSc2
(	(	kIx(
<g/>
vnitřní	vnitřní	k2eAgFnSc2d1
flexe	flexe	k1gFnSc2
<g/>
,	,	kIx,
ohýbání	ohýbání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
existuje	existovat	k5eAaImIp3nS
různé	různý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
pádů	pád	k1gInPc2
s	s	k7c7
rozličnými	rozličný	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
pádů	pád	k1gInPc2
v	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
</s>
<s>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
bulharština	bulharština	k1gFnSc1
<g/>
,	,	kIx,
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
švédština	švédština	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
němčina	němčina	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
latina	latina	k1gFnSc1
<g/>
,	,	kIx,
slovenština	slovenština	k1gFnSc1
<g/>
,	,	kIx,
ruština	ruština	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
čeština	čeština	k1gFnSc1
<g/>
,	,	kIx,
polština	polština	k1gFnSc1
<g/>
,	,	kIx,
ukrajinština	ukrajinština	k1gFnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
i	i	k9
slovenština	slovenština	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
:	:	kIx,
estonština	estonština	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
:	:	kIx,
finština	finština	k1gFnSc1
</s>
<s>
Pády	Pád	k1gInPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Česká	český	k2eAgNnPc1d1
podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
nominativ	nominativ	k1gInSc1
s	s	k7c7
pádovými	pádový	k2eAgFnPc7d1
otázkami	otázka	k1gFnPc7
(	(	kIx(
<g/>
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
<g/>
)	)	kIx)
</s>
<s>
genitiv	genitiv	k1gInSc1
(	(	kIx(
<g/>
koho	kdo	k3yQnSc4,k3yInSc4,k3yRnSc4
<g/>
,	,	kIx,
čeho	co	k3yRnSc2,k3yInSc2,k3yQnSc2
<g/>
)	)	kIx)
</s>
<s>
dativ	dativ	k1gInSc1
(	(	kIx(
<g/>
komu	kdo	k3yInSc3,k3yQnSc3,k3yRnSc3
<g/>
,	,	kIx,
čemu	co	k3yInSc3,k3yRnSc3,k3yQnSc3
<g/>
)	)	kIx)
</s>
<s>
akuzativ	akuzativ	k1gInSc1
(	(	kIx(
<g/>
koho	kdo	k3yRnSc4,k3yQnSc4,k3yInSc4
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
<g/>
)	)	kIx)
</s>
<s>
vokativ	vokativ	k1gInSc4
(	(	kIx(
<g/>
oslovujeme	oslovovat	k5eAaImIp1nP
<g/>
,	,	kIx,
voláme	volat	k5eAaImIp1nP
<g/>
)	)	kIx)
</s>
<s>
lokál	lokál	k1gInSc1
(	(	kIx(
<g/>
kom	kdo	k3yQnSc6,k3yRnSc6,k3yInSc6
<g/>
,	,	kIx,
čem	co	k3yRnSc6,k3yQnSc6,k3yInSc6
<g/>
)	)	kIx)
</s>
<s>
instrumentál	instrumentál	k1gInSc4
(	(	kIx(
<g/>
kým	kdo	k3yQnSc7,k3yRnSc7,k3yInSc7
<g/>
,	,	kIx,
čím	čí	k3xOyQgNnSc7,k3xOyRgNnSc7
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
numerus	numerus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
mluvnice	mluvnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
mluvnická	mluvnický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
udává	udávat	k5eAaImIp3nS
počet	počet	k1gInSc4
účastníků	účastník	k1gMnPc2
děje	děj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
češtiny	čeština	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kategorie	kategorie	k1gFnSc1
čísla	číslo	k1gNnSc2
založena	založit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
na	na	k7c6
protikladu	protiklad	k1gInSc6
jednosti	jednost	k1gFnSc2
–	–	k?
mnohosti	mnohost	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
je	být	k5eAaImIp3nS
struktura	struktura	k1gFnSc1
složitější	složitý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
jednotného	jednotný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
(	(	kIx(
<g/>
singulár	singulár	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
množného	množný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
(	(	kIx(
<g/>
plurál	plurál	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
vyskytuje	vyskytovat	k5eAaImIp3nS
dvojné	dvojný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
duál	duál	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
evropských	evropský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
např.	např.	kA
ve	v	k7c6
slovinštině	slovinština	k1gFnSc6
nebo	nebo	k8xC
lužické	lužický	k2eAgFnSc3d1
srbštině	srbština	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozůstatky	pozůstatek	k1gInPc7
duálu	duál	k1gInSc2
najdeme	najít	k5eAaPmIp1nP
také	také	k9
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatná	podstatný	k2eAgFnSc1d1
jména	jméno	k1gNnPc1
látková	látkový	k2eAgNnPc1d1
<g/>
,	,	kIx,
hromadná	hromadný	k2eAgNnPc1d1
a	a	k8xC
pomnožná	pomnožný	k2eAgNnPc1d1
mají	mít	k5eAaImIp3nP
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc4
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Rod	rod	k1gInSc1
(	(	kIx(
<g/>
genus	genus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Jmenný	jmenný	k2eAgInSc4d1
rod	rod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jmenný	jmenný	k2eAgInSc1d1
rod	rod	k1gInSc1
(	(	kIx(
<g/>
lat.	lat.	k?
genus	genus	k1gInSc1
nominis	nominis	k1gFnSc2
či	či	k8xC
jen	jen	k9
genus	genus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mluvnická	mluvnický	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
skloňování	skloňování	k1gNnSc3
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
často	často	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
pohlavím	pohlaví	k1gNnSc7
a	a	k8xC
s	s	k7c7
životností	životnost	k1gFnSc7
či	či	k8xC
neživotností	neživotnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
mluvnický	mluvnický	k2eAgInSc1d1
však	však	k9
nemusí	muset	k5eNaImIp3nS
vždy	vždy	k6eAd1
odpovídat	odpovídat	k5eAaImF
rodu	rod	k1gInSc3
přirozenému	přirozený	k2eAgInSc3d1
–	–	k?
např.	např.	kA
v	v	k7c6
češtině	čeština	k1gFnSc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
neživé	živý	k2eNgFnPc4d1
věci	věc	k1gFnPc4
nejen	nejen	k6eAd1
rodu	rod	k1gInSc2
středního	střední	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
mužského	mužský	k2eAgNnSc2d1
nebo	nebo	k8xC
ženského	ženský	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
indoevropských	indoevropský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
existují	existovat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
rody	rod	k1gInPc1
<g/>
:	:	kIx,
rod	rod	k1gInSc1
mužský	mužský	k2eAgInSc1d1
(	(	kIx(
<g/>
maskulinum	maskulinum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rod	rod	k1gInSc1
ženský	ženský	k2eAgInSc1d1
(	(	kIx(
<g/>
femininum	femininum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
rod	rod	k1gInSc1
střední	střední	k2eAgInSc1d1
(	(	kIx(
<g/>
neutrum	neutrum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
jazyky	jazyk	k1gInPc4
kategorii	kategorie	k1gFnSc3
rodů	rod	k1gInPc2
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
neznají	neznat	k5eAaImIp3nP,k5eNaImIp3nP
–	–	k?
např.	např.	kA
angličtina	angličtina	k1gFnSc1
rozlišuje	rozlišovat	k5eAaImIp3nS
rody	rod	k1gInPc4
pouze	pouze	k6eAd1
u	u	k7c2
zájmen	zájmeno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
mají	mít	k5eAaImIp3nP
rozsah	rozsah	k1gInSc4
rodů	rod	k1gInPc2
omezený	omezený	k2eAgMnSc1d1
<g/>
,	,	kIx,
např.	např.	kA
švédština	švédština	k1gFnSc1
rozlišuje	rozlišovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc4
rody	rod	k1gInPc4
<g/>
:	:	kIx,
společný	společný	k2eAgInSc4d1
a	a	k8xC
střední	střední	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
některé	některý	k3yIgInPc1
jazyky	jazyk	k1gInPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
rodů	rod	k1gInPc2
i	i	k9
více	hodně	k6eAd2
–	–	k?
např.	např.	kA
v	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
mužském	mužský	k2eAgInSc6d1
rodě	rod	k1gInSc6
rozlišuje	rozlišovat	k5eAaImIp3nS
ještě	ještě	k9
rod	rod	k1gInSc1
životný	životný	k2eAgInSc1d1
(	(	kIx(
<g/>
animatum	animatum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
neživotný	životný	k2eNgMnSc1d1
(	(	kIx(
<g/>
inanimatum	inanimatum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzor	vzor	k1gInSc1
(	(	kIx(
<g/>
paradigma	paradigma	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Vzor	vzor	k1gInSc1
(	(	kIx(
<g/>
mluvnice	mluvnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzor	vzor	k1gInSc1
je	být	k5eAaImIp3nS
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
určené	určený	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
ukázka	ukázka	k1gFnSc1
pro	pro	k7c4
skloňování	skloňování	k1gNnSc4
(	(	kIx(
<g/>
deklinaci	deklinace	k1gFnSc4
<g/>
)	)	kIx)
dalších	další	k2eAgNnPc2d1
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
stejného	stejný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
jazyk	jazyk	k1gInSc1
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc4d1
inventář	inventář	k1gInSc4
skloňovacích	skloňovací	k2eAgInPc2d1
vzorů	vzor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
následující	následující	k2eAgInPc1d1
hlavní	hlavní	k2eAgInPc1d1
vzory	vzor	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
čtyři	čtyři	k4xCgInPc4
vzory	vzor	k1gInPc4
pro	pro	k7c4
rod	rod	k1gInSc4
mužský	mužský	k2eAgMnSc1d1
životný	životný	k2eAgMnSc1d1
–	–	k?
pán	pán	k1gMnSc1
<g/>
,	,	kIx,
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
<g/>
,	,	kIx,
soudce	soudce	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
dva	dva	k4xCgInPc4
vzory	vzor	k1gInPc4
pro	pro	k7c4
rod	rod	k1gInSc4
mužský	mužský	k2eAgInSc4d1
neživotný	životný	k2eNgInSc4d1
–	–	k?
hrad	hrad	k1gInSc4
<g/>
,	,	kIx,
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
čtyři	čtyři	k4xCgInPc4
vzory	vzor	k1gInPc4
pro	pro	k7c4
rod	rod	k1gInSc4
ženský	ženský	k2eAgInSc4d1
–	–	k?
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
růže	růže	k1gFnSc1
<g/>
,	,	kIx,
píseň	píseň	k1gFnSc1
<g/>
,	,	kIx,
kost	kost	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
čtyři	čtyři	k4xCgInPc4
vzory	vzor	k1gInPc4
pro	pro	k7c4
rod	rod	k1gInSc4
střední	střední	k2eAgFnSc2d1
–	–	k?
město	město	k1gNnSc1
<g/>
,	,	kIx,
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
kuře	kuře	k1gNnSc1
<g/>
,	,	kIx,
stavení	stavení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
ke	k	k7c3
každému	každý	k3xTgInSc3
vzoru	vzor	k1gInSc3
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
podvzorů	podvzor	k1gInPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
existují	existovat	k5eAaImIp3nP
vzory	vzor	k1gInPc1
pro	pro	k7c4
zpodstatnělá	zpodstatnělý	k2eAgNnPc4d1
přídavná	přídavný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
(	(	kIx(
<g/>
hajný	hajný	k1gMnSc1
<g/>
,	,	kIx,
průvodčí	průvodčí	k1gMnSc1
<g/>
,	,	kIx,
zlatý	zlatý	k1gInSc1
<g/>
,	,	kIx,
bytná	bytná	k1gFnSc1
<g/>
,	,	kIx,
vstupné	vstupné	k1gNnSc1
<g/>
,	,	kIx,
hovězí	hovězí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některá	některý	k3yIgNnPc1
jména	jméno	k1gNnPc1
se	se	k3xPyFc4
skloňují	skloňovat	k5eAaImIp3nP
nepravidelně	pravidelně	k6eNd1
(	(	kIx(
<g/>
idea	idea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
několik	několik	k4yIc4
podstatných	podstatný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
je	být	k5eAaImIp3nS
nesklonných	sklonný	k2eNgNnPc2d1
(	(	kIx(
<g/>
kupé	kupé	k1gNnPc2
<g/>
,	,	kIx,
atašé	atašé	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PETRÁČKOVÁ	Petráčková	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademický	akademický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
412	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
607	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PETRÁČKOVÁ	Petráčková	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademický	akademický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
607	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Podle	podle	k7c2
ŠTÍCHA	Štícha	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademická	akademický	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
spisovné	spisovný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
291	#num#	k4
<g/>
–	–	k?
<g/>
293	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vyjmenovaná	vyjmenovaný	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
</s>
<s>
Skloňování	skloňování	k1gNnSc1
</s>
<s>
Podmět	podmět	k1gInSc1
</s>
<s>
České	český	k2eAgNnSc1d1
skloňování	skloňování	k1gNnSc1
</s>
<s>
Česká	český	k2eAgNnPc1d1
podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
latinských	latinský	k2eAgInPc2d1
gramatických	gramatický	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Substantiva	substantivum	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4058333-8	4058333-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6852	#num#	k4
</s>
