<s>
Chlamydia	Chlamydium	k1gNnPc1	Chlamydium
trachomatis	trachomatis	k1gFnSc2	trachomatis
jsou	být	k5eAaImIp3nP	být
gramnegativní	gramnegativní	k2eAgFnPc1d1	gramnegativní
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
parazitují	parazitovat	k5eAaImIp3nP	parazitovat
uvnitř	uvnitř	k7c2	uvnitř
vnímavých	vnímavý	k2eAgFnPc2d1	vnímavá
buněk	buňka	k1gFnPc2	buňka
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
původců	původce	k1gMnPc2	původce
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
má	mít	k5eAaImIp3nS	mít
dvoufázový	dvoufázový	k2eAgInSc4d1	dvoufázový
životní	životní	k2eAgInSc4d1	životní
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
mimo	mimo	k6eAd1	mimo
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
je	být	k5eAaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
prasknutím	prasknutí	k1gNnSc7	prasknutí
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
výsevem	výsev	k1gInSc7	výsev
infekčních	infekční	k2eAgNnPc2d1	infekční
tělísek	tělísko	k1gNnPc2	tělísko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
infikují	infikovat	k5eAaBmIp3nP	infikovat
další	další	k2eAgFnPc1d1	další
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
trvá	trvat	k5eAaImIp3nS	trvat
obvykle	obvykle	k6eAd1	obvykle
48	[number]	k4	48
až	až	k9	až
72	[number]	k4	72
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydia	Chlamydium	k1gNnSc2	Chlamydium
trachomatis	trachomatis	k1gFnSc2	trachomatis
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
i	i	k9	i
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
způsobit	způsobit	k5eAaPmF	způsobit
vážné	vážný	k2eAgFnPc4d1	vážná
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
příznaky	příznak	k1gInPc4	příznak
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
:	:	kIx,	:
80	[number]	k4	80
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
příznaky	příznak	k1gInPc4	příznak
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
vaginální	vaginální	k2eAgInSc4d1	vaginální
výtok	výtok	k1gInSc4	výtok
bolest	bolest	k1gFnSc1	bolest
či	či	k8xC	či
pálení	pálení	k1gNnSc1	pálení
při	při	k7c6	při
močení	močení	k1gNnSc6	močení
bolest	bolest	k1gFnSc4	bolest
při	při	k7c6	při
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
styku	styk	k1gInSc6	styk
bolest	bolest	k1gFnSc1	bolest
v	v	k7c6	v
podbřišku	podbřišek	k1gInSc6	podbřišek
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
krvácení	krvácení	k1gNnSc2	krvácení
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
:	:	kIx,	:
50	[number]	k4	50
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
příznaky	příznak	k1gInPc4	příznak
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
výtok	výtok	k1gInSc4	výtok
z	z	k7c2	z
penisu	penis	k1gInSc2	penis
řezání	řezání	k1gNnSc2	řezání
nebo	nebo	k8xC	nebo
pálení	pálení	k1gNnSc2	pálení
při	při	k7c6	při
močení	močení	k1gNnSc6	močení
časté	častý	k2eAgNnSc1d1	časté
močení	močení	k1gNnSc1	močení
nebo	nebo	k8xC	nebo
problém	problém	k1gInSc1	problém
udržet	udržet	k5eAaPmF	udržet
moč	moč	k1gFnSc4	moč
napětí	napětí	k1gNnSc2	napětí
nebo	nebo	k8xC	nebo
bolest	bolest	k1gFnSc4	bolest
ve	v	k7c6	v
varlatech	varle	k1gNnPc6	varle
</s>
