<s>
Chlamydia	Chlamydium	k1gNnPc1	Chlamydium
trachomatis	trachomatis	k1gFnSc2	trachomatis
jsou	být	k5eAaImIp3nP	být
gramnegativní	gramnegativní	k2eAgFnPc1d1	gramnegativní
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
parazitují	parazitovat	k5eAaImIp3nP	parazitovat
uvnitř	uvnitř	k7c2	uvnitř
vnímavých	vnímavý	k2eAgFnPc2d1	vnímavá
buněk	buňka	k1gFnPc2	buňka
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
.	.	kIx.	.
</s>
