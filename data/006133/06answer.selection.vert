<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
písmenem	písmeno	k1gNnSc7	písmeno
c	c	k0	c
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
celeritas	celeritas	k1gMnSc1	celeritas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
constans	constansa	k1gFnPc2	constansa
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgFnSc1d1	znamenající
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
