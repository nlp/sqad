<s>
Hlas	hlas	k1gInSc1	hlas
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
vytvářený	vytvářený	k2eAgInSc1d1	vytvářený
hlasivkami	hlasivka	k1gFnPc7	hlasivka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
bývá	bývat	k5eAaImIp3nS	bývat
slovo	slovo	k1gNnSc1	slovo
hlas	hlas	k1gInSc1	hlas
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
melodické	melodický	k2eAgFnSc2d1	melodická
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
varhan	varhany	k1gFnPc2	varhany
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
hlas	hlas	k1gInSc1	hlas
označena	označen	k2eAgFnSc1d1	označena
skupina	skupina	k1gFnSc1	skupina
píšťal	píšťala	k1gFnPc2	píšťala
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
barvou	barva	k1gFnSc7	barva
(	(	kIx(	(
<g/>
rejstřík	rejstřík	k1gInSc1	rejstřík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
hlasu	hlas	k1gInSc2	hlas
je	být	k5eAaImIp3nS	být
výdechový	výdechový	k2eAgInSc1d1	výdechový
proud	proud	k1gInSc1	proud
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Hlasivky	hlasivka	k1gFnPc1	hlasivka
se	se	k3xPyFc4	se
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
hlasu	hlas	k1gInSc2	hlas
podílí	podílet	k5eAaImIp3nS	podílet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
kmitají	kmitat	k5eAaImIp3nP	kmitat
a	a	k8xC	a
propůjčují	propůjčovat	k5eAaImIp3nP	propůjčovat
hlasu	hlas	k1gInSc3	hlas
tónové	tónový	k2eAgFnSc2d1	tónová
složky	složka	k1gFnSc2	složka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
,	,	kIx,	,
sonory	sonora	k1gFnSc2	sonora
<g/>
,	,	kIx,	,
znělé	znělý	k2eAgInPc1d1	znělý
konsonanty	konsonant	k1gInPc1	konsonant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
že	že	k8xS	že
nekmitají	kmitat	k5eNaImIp3nP	kmitat
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tvorbu	tvorba	k1gFnSc4	tvorba
šumu	šum	k1gInSc2	šum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
neznělé	znělý	k2eNgInPc1d1	neznělý
obstruenty	obstruent	k1gInPc1	obstruent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlasivky	hlasivka	k1gFnPc1	hlasivka
můžeme	moct	k5eAaImIp1nP	moct
pomocí	pomocí	k7c2	pomocí
svalů	sval	k1gInPc2	sval
přibližovat	přibližovat	k5eAaImF	přibližovat
či	či	k8xC	či
oddalovat	oddalovat	k5eAaImF	oddalovat
<g/>
,	,	kIx,	,
kmitání	kmitání	k1gNnSc4	kmitání
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
svalová	svalový	k2eAgFnSc1d1	svalová
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
výsledek	výsledek	k1gInSc1	výsledek
tlakových	tlakový	k2eAgInPc2d1	tlakový
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
vzniku	vznik	k1gInSc2	vznik
hlasu	hlas	k1gInSc2	hlas
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
fonace	fonace	k1gFnSc1	fonace
<g/>
.	.	kIx.	.
</s>
<s>
Hlasivky	hlasivka	k1gFnPc1	hlasivka
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
fonačním	fonační	k2eAgNnSc6d1	fonační
postavení	postavení	k1gNnSc6	postavení
přiblíženy	přiblížit	k5eAaPmNgInP	přiblížit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
narůstající	narůstající	k2eAgInSc1d1	narůstající
subglotální	subglotální	k2eAgInSc1d1	subglotální
tlak	tlak	k1gInSc1	tlak
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
elasticitě	elasticita	k1gFnSc3	elasticita
hlasivek	hlasivka	k1gFnPc2	hlasivka
a	a	k8xC	a
oddálí	oddálit	k5eAaPmIp3nS	oddálit
je	on	k3xPp3gMnPc4	on
<g/>
,	,	kIx,	,
následné	následný	k2eAgNnSc4d1	následné
snížení	snížení	k1gNnSc4	snížení
subglotálního	subglotální	k2eAgInSc2d1	subglotální
tlaku	tlak	k1gInSc2	tlak
způsobí	způsobit	k5eAaPmIp3nS	způsobit
opětovné	opětovný	k2eAgNnSc1d1	opětovné
přiblížení	přiblížení	k1gNnSc1	přiblížení
hlasivek	hlasivka	k1gFnPc2	hlasivka
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečnost	jedinečnost	k1gFnSc4	jedinečnost
každého	každý	k3xTgInSc2	každý
lidského	lidský	k2eAgInSc2d1	lidský
hlasu	hlas	k1gInSc2	hlas
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
základní	základní	k2eAgFnSc7d1	základní
frekvencí	frekvence	k1gFnSc7	frekvence
a	a	k8xC	a
modifikací	modifikace	k1gFnSc7	modifikace
této	tento	k3xDgFnSc2	tento
frekvence	frekvence	k1gFnSc2	frekvence
v	v	k7c6	v
nadhrtanových	nadhrtanový	k2eAgFnPc6d1	nadhrtanový
dutinách	dutina	k1gFnPc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
hlasivkový	hlasivkový	k2eAgInSc1d1	hlasivkový
tón	tón	k1gInSc1	tón
je	být	k5eAaImIp3nS	být
složený	složený	k2eAgInSc1d1	složený
tón	tón	k1gInSc1	tón
s	s	k7c7	s
odpovídající	odpovídající	k2eAgFnSc7d1	odpovídající
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
řadou	řada	k1gFnSc7	řada
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
tónu	tón	k1gInSc2	tón
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
všechny	všechen	k3xTgFnPc1	všechen
tónové	tónový	k2eAgFnPc1d1	tónová
složky	složka	k1gFnPc1	složka
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Nadhrtanové	nadhrtanový	k2eAgFnPc1d1	nadhrtanový
dutiny	dutina	k1gFnPc1	dutina
poté	poté	k6eAd1	poté
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xS	jako
rezonátor	rezonátor	k1gInSc4	rezonátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
propouští	propouštět	k5eAaImIp3nS	propouštět
jen	jen	k9	jen
některé	některý	k3yIgInPc4	některý
svrchní	svrchní	k2eAgInPc4d1	svrchní
harmonické	harmonický	k2eAgInPc4d1	harmonický
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc4d1	označovaná
jako	jako	k8xC	jako
rezonanční	rezonanční	k2eAgFnPc4d1	rezonanční
frekvence	frekvence	k1gFnPc4	frekvence
nebo	nebo	k8xC	nebo
formanty	formans	k1gInPc4	formans
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
u	u	k7c2	u
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
sonory	sonora	k1gFnPc1	sonora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlasové	hlasový	k2eAgNnSc1d1	hlasové
ústrojí	ústrojí	k1gNnSc1	ústrojí
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
šumu	šum	k1gInSc2	šum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
hlasivky	hlasivka	k1gFnSc2	hlasivka
nekmitají	kmitat	k5eNaImIp3nP	kmitat
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
pouze	pouze	k6eAd1	pouze
štěrbinu	štěrbina	k1gFnSc4	štěrbina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
proniká	pronikat	k5eAaImIp3nS	pronikat
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
neznělé	znělý	k2eNgFnSc2d1	neznělá
obstruenty	obstruenta	k1gFnSc2	obstruenta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvukovou	zvukový	k2eAgFnSc4d1	zvuková
stránku	stránka	k1gFnSc4	stránka
jazyka	jazyk	k1gInSc2	jazyk
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
fonetika	fonetika	k1gFnSc1	fonetika
<g/>
.	.	kIx.	.
</s>
<s>
Frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
rozsah	rozsah	k1gInSc1	rozsah
neškoleného	školený	k2eNgInSc2d1	neškolený
hlasu	hlas	k1gInSc2	hlas
dospělého	dospělý	k2eAgMnSc2d1	dospělý
jedince	jedinec	k1gMnSc2	jedinec
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
asi	asi	k9	asi
1,5	[number]	k4	1,5
oktávy	oktáva	k1gFnPc4	oktáva
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
hlas	hlas	k1gInSc1	hlas
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
:	:	kIx,	:
hlas	hlas	k1gInSc1	hlas
novorozence	novorozenec	k1gMnSc2	novorozenec
má	mít	k5eAaImIp3nS	mít
frekvenci	frekvence	k1gFnSc4	frekvence
přibližně	přibližně	k6eAd1	přibližně
440	[number]	k4	440
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
komorní	komorní	k2eAgFnSc1d1	komorní
a	a	k8xC	a
<g/>
,	,	kIx,	,
základní	základní	k2eAgMnSc1d1	základní
tón	tón	k1gInSc4	tón
pro	pro	k7c4	pro
ladění	ladění	k1gNnSc4	ladění
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tutéž	týž	k3xTgFnSc4	týž
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
dospívání	dospívání	k1gNnSc2	dospívání
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
mutaci	mutace	k1gFnSc3	mutace
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
hlasu	hlas	k1gInSc2	hlas
klesá	klesat	k5eAaImIp3nS	klesat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
-	-	kIx~	-
rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
výšky	výška	k1gFnSc2	výška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Složky	složka	k1gFnSc2	složka
frekvenčního	frekvenční	k2eAgNnSc2d1	frekvenční
spektra	spektrum	k1gNnSc2	spektrum
hlasu	hlas	k1gInSc2	hlas
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
kolem	kolem	k7c2	kolem
10	[number]	k4	10
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
srozumitelné	srozumitelný	k2eAgFnSc2d1	srozumitelná
řeči	řeč	k1gFnSc2	řeč
postačuje	postačovat	k5eAaImIp3nS	postačovat
pásmo	pásmo	k1gNnSc1	pásmo
výrazně	výrazně	k6eAd1	výrazně
užší	úzký	k2eAgNnSc1d2	užší
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
složky	složka	k1gFnPc1	složka
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc1d1	zajišťující
srozumitelnost	srozumitelnost	k1gFnSc1	srozumitelnost
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
především	především	k9	především
na	na	k7c6	na
barvě	barva	k1gFnSc6	barva
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
telekomunikační	telekomunikační	k2eAgFnSc1d1	telekomunikační
unie	unie	k1gFnSc1	unie
stanovila	stanovit	k5eAaPmAgFnS	stanovit
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
telefonních	telefonní	k2eAgInPc2d1	telefonní
hovorů	hovor	k1gInPc2	hovor
frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
rozsahu	rozsah	k1gInSc2	rozsah
300	[number]	k4	300
Hz	Hz	kA	Hz
-	-	kIx~	-
3,4	[number]	k4	3,4
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesrozumitelná	srozumitelný	k2eNgNnPc1d1	nesrozumitelné
slova	slovo	k1gNnPc1	slovo
si	se	k3xPyFc3	se
telefonisté	telefonista	k1gMnPc1	telefonista
domyslí	domyslet	k5eAaPmIp3nP	domyslet
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
vyhláskují	vyhláskovat	k5eAaImIp3nP	vyhláskovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hladina	hladina	k1gFnSc1	hladina
akustického	akustický	k2eAgInSc2d1	akustický
tlaku	tlak	k1gInSc2	tlak
vytvářeného	vytvářený	k2eAgInSc2d1	vytvářený
hlasem	hlasem	k6eAd1	hlasem
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
běžném	běžný	k2eAgInSc6d1	běžný
hovoru	hovor	k1gInSc6	hovor
asi	asi	k9	asi
+40	+40	k4	+40
až	až	k9	až
+60	+60	k4	+60
dB	db	kA	db
<g/>
,	,	kIx,	,
při	při	k7c6	při
křiku	křik	k1gInSc6	křik
cca	cca	kA	cca
+80	+80	k4	+80
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Pianissimo	pianissimo	k6eAd1	pianissimo
při	při	k7c6	při
operním	operní	k2eAgInSc6d1	operní
zpěvu	zpěv	k1gInSc6	zpěv
je	být	k5eAaImIp3nS	být
+50	+50	k4	+50
dB	db	kA	db
<g/>
,	,	kIx,	,
fortissimo	fortissimo	k6eAd1	fortissimo
+85	+85	k4	+85
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Rekordy	rekord	k1gInPc1	rekord
při	při	k7c6	při
světové	světový	k2eAgFnSc6d1	světová
soutěži	soutěž	k1gFnSc6	soutěž
v	v	k7c6	v
řevu	řev	k1gInSc6	řev
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
přes	přes	k7c4	přes
+100	+100	k4	+100
dB	db	kA	db
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Druhy	druh	k1gInPc4	druh
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
nejčastěji	často	k6eAd3	často
používán	používat	k5eAaImNgInS	používat
jako	jako	k9	jako
zpěv	zpěv	k1gInSc1	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Hlasy	hlas	k1gInPc1	hlas
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
výškových	výškový	k2eAgFnPc6d1	výšková
polohách	poloha	k1gFnPc6	poloha
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc1d1	různý
názvy	název	k1gInPc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k9	co
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
jiné	jiný	k2eAgNnSc4d1	jiné
přiřazení	přiřazení	k1gNnSc4	přiřazení
názvů	název	k1gInPc2	název
hlasů	hlas	k1gInPc2	hlas
k	k	k7c3	k
frekvenčním	frekvenční	k2eAgInPc3d1	frekvenční
rozsahům	rozsah	k1gInPc3	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
uvedené	uvedený	k2eAgFnPc4d1	uvedená
hodnoty	hodnota	k1gFnPc4	hodnota
viz	vidět	k5eAaImRp2nS	vidět
Horová	Horová	k1gFnSc1	Horová
<g/>
,	,	kIx,	,
doplněno	doplnit	k5eAaPmNgNnS	doplnit
Juríkem	Jurík	k1gInSc7	Jurík
<g/>
,	,	kIx,	,
laskavý	laskavý	k2eAgMnSc1d1	laskavý
čtenář	čtenář	k1gMnSc1	čtenář
si	se	k3xPyFc3	se
srovná	srovnat	k5eAaPmIp3nS	srovnat
s	s	k7c7	s
údaji	údaj	k1gInPc7	údaj
v	v	k7c6	v
připojených	připojený	k2eAgInPc6d1	připojený
odkazech	odkaz	k1gInPc6	odkaz
na	na	k7c4	na
anglická	anglický	k2eAgNnPc4d1	anglické
hesla	heslo	k1gNnPc4	heslo
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
a	a	k8xC	a
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
uvedené	uvedený	k2eAgFnSc6d1	uvedená
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klasického	klasický	k2eAgInSc2d1	klasický
zpěvu	zpěv	k1gInSc2	zpěv
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
hlasových	hlasový	k2eAgFnPc2d1	hlasová
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
buď	buď	k8xC	buď
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
některých	některý	k3yIgInPc2	některý
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
západní	západní	k2eAgFnSc2d1	západní
umělé	umělý	k2eAgFnSc2d1	umělá
a	a	k8xC	a
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
<g/>
:	:	kIx,	:
Alikvotní	alikvotní	k2eAgInSc4d1	alikvotní
zpěv	zpěv	k1gInSc4	zpěv
-	-	kIx~	-
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yRgFnSc2	který
lze	lze	k6eAd1	lze
zesílit	zesílit	k5eAaPmF	zesílit
alikvotní	alikvotní	k2eAgInPc4d1	alikvotní
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
přirozeně	přirozeně	k6eAd1	přirozeně
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c6	v
hlase	hlas	k1gInSc6	hlas
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
iluzi	iluze	k1gFnSc4	iluze
dvojhlasého	dvojhlasý	k2eAgInSc2d1	dvojhlasý
zpěvu	zpěv	k1gInSc2	zpěv
Sprechgesang	Sprechgesanga	k1gFnPc2	Sprechgesanga
-	-	kIx~	-
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
napůl	napůl	k6eAd1	napůl
cesty	cesta	k1gFnPc1	cesta
mezi	mezi	k7c7	mezi
řečí	řeč	k1gFnSc7	řeč
a	a	k8xC	a
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Využil	využít	k5eAaPmAgMnS	využít
ji	on	k3xPp3gFnSc4	on
například	například	k6eAd1	například
Arnold	Arnold	k1gMnSc1	Arnold
Schoenberg	Schoenberg	k1gMnSc1	Schoenberg
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
skladbě	skladba	k1gFnSc6	skladba
Pierrot	Pierrot	k1gMnSc1	Pierrot
Lunaire	Lunair	k1gInSc5	Lunair
Rap	rapa	k1gFnPc2	rapa
-	-	kIx~	-
rytmická	rytmický	k2eAgFnSc1d1	rytmická
recitace	recitace	k1gFnSc1	recitace
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
,	,	kIx,	,
frázování	frázování	k1gNnSc1	frázování
a	a	k8xC	a
rytmus	rytmus	k1gInSc1	rytmus
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
na	na	k7c6	na
vytváření	vytváření	k1gNnSc6	vytváření
tónů	tón	k1gInPc2	tón
Beatbox	Beatbox	k1gInSc1	Beatbox
-	-	kIx~	-
nápodoba	nápodoba	k1gFnSc1	nápodoba
zvuků	zvuk	k1gInPc2	zvuk
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
perkusí	perkuse	k1gFnPc2	perkuse
pomocí	pomocí	k7c2	pomocí
hlasu	hlas	k1gInSc2	hlas
Scat	scat	k1gInSc1	scat
-	-	kIx~	-
improvizovaný	improvizovaný	k2eAgInSc1d1	improvizovaný
jazzový	jazzový	k2eAgInSc1d1	jazzový
zpěv	zpěv	k1gInSc1	zpěv
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nesmyslným	smyslný	k2eNgInSc7d1	nesmyslný
textem	text	k1gInSc7	text
Growling	Growling	k1gInSc1	Growling
-	-	kIx~	-
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
growl	grownout	k5eAaPmAgInS	grownout
=	=	kIx~	=
mručet	mručet	k5eAaImF	mručet
<g/>
,	,	kIx,	,
vrčet	vrčet	k5eAaImF	vrčet
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
velmi	velmi	k6eAd1	velmi
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
mručení	mručení	k1gNnSc2	mručení
<g/>
,	,	kIx,	,
řevu	řev	k1gInSc2	řev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
death	deatha	k1gFnPc2	deatha
metalové	metalový	k2eAgFnSc2d1	metalová
hudby	hudba	k1gFnSc2	hudba
Jódlování	jódlování	k1gNnSc2	jódlování
-	-	kIx~	-
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
Alpských	alpský	k2eAgFnPc6d1	alpská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
velkých	velká	k1gFnPc6	velká
a	a	k8xC	a
rychlých	rychlý	k2eAgInPc6d1	rychlý
intervalových	intervalový	k2eAgInPc6d1	intervalový
skocích	skok	k1gInPc6	skok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
změnou	změna	k1gFnSc7	změna
hlasového	hlasový	k2eAgInSc2d1	hlasový
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgInSc1d2	nižší
tón	tón	k1gInSc1	tón
je	být	k5eAaImIp3nS	být
zpíván	zpívat	k5eAaImNgInS	zpívat
prsním	prsní	k2eAgInSc7d1	prsní
rejstříkem	rejstřík	k1gInSc7	rejstřík
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
pak	pak	k6eAd1	pak
falzetem	falzet	k1gInSc7	falzet
Brumendo	brumendo	k6eAd1	brumendo
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
bez	bez	k7c2	bez
artikulace	artikulace	k1gFnSc2	artikulace
<g/>
,	,	kIx,	,
se	s	k7c7	s
zavřenými	zavřený	k2eAgNnPc7d1	zavřené
ústy	ústa	k1gNnPc7	ústa
(	(	kIx(	(
<g/>
zvuk	zvuk	k1gInSc1	zvuk
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
hm	hm	k?	hm
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Screaming	Screaming	k1gInSc1	Screaming
-	-	kIx~	-
hrdelní	hrdelní	k2eAgInSc1d1	hrdelní
řev	řev	k1gInSc1	řev
<g/>
,	,	kIx,	,
<g/>
používaný	používaný	k2eAgInSc1d1	používaný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
metalu	metal	k1gInSc6	metal
a	a	k8xC	a
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
stylech	styl	k1gInPc6	styl
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
buď	buď	k8xC	buď
celé	celý	k2eAgFnPc4d1	celá
písně	píseň	k1gFnPc4	píseň
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
úseky	úsek	k1gInPc1	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Scream	Scream	k1gInSc1	Scream
-	-	kIx~	-
jednorázové	jednorázový	k2eAgNnSc1d1	jednorázové
zařvání	zařvání	k1gNnSc1	zařvání
používané	používaný	k2eAgNnSc1d1	používané
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
rocku	rock	k1gInSc6	rock
a	a	k8xC	a
metalu	metal	k1gInSc6	metal
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vysoké	vysoký	k2eAgInPc4d1	vysoký
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgInPc4d1	nízký
nebo	nebo	k8xC	nebo
střední	střední	k2eAgInPc4d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
krátké	krátký	k2eAgNnSc1d1	krátké
nebo	nebo	k8xC	nebo
také	také	k9	také
velice	velice	k6eAd1	velice
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
měnit	měnit	k5eAaImF	měnit
tonina	tonina	k1gFnSc1	tonina
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
screamingu	screaming	k1gInSc2	screaming
se	se	k3xPyFc4	se
nezpívají	zpívat	k5eNaImIp3nP	zpívat
žádná	žádný	k3yNgNnPc4	žádný
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
patologická	patologický	k2eAgFnSc1d1	patologická
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
změna	změna	k1gFnSc1	změna
hlasu	hlas	k1gInSc2	hlas
(	(	kIx(	(
<g/>
chrapot	chrapot	k1gInSc1	chrapot
<g/>
,	,	kIx,	,
přeskakování	přeskakování	k1gNnSc1	přeskakování
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
výšky	výška	k1gFnSc2	výška
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
-	-	kIx~	-
dysfonie	dysfonie	k1gFnSc1	dysfonie
šepot	šepot	k1gInSc1	šepot
-	-	kIx~	-
hlasivky	hlasivka	k1gFnPc1	hlasivka
se	se	k3xPyFc4	se
nezavírají	zavírat	k5eNaImIp3nP	zavírat
-	-	kIx~	-
afonie	afonie	k1gFnSc1	afonie
ztráta	ztráta	k1gFnSc1	ztráta
vyvinuté	vyvinutý	k2eAgFnSc2d1	vyvinutá
řeči	řeč	k1gFnSc2	řeč
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
mozku	mozek	k1gInSc2	mozek
-	-	kIx~	-
afázie	afázie	k1gFnSc1	afázie
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ztráta	ztráta	k1gFnSc1	ztráta
po	po	k7c6	po
fyzickém	fyzický	k2eAgInSc6d1	fyzický
nebo	nebo	k8xC	nebo
psychickém	psychický	k2eAgNnSc6d1	psychické
traumatu	trauma	k1gNnSc6	trauma
-	-	kIx~	-
mutismus	mutismus	k1gInSc1	mutismus
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
vysoký	vysoký	k2eAgInSc4d1	vysoký
hlas	hlas	k1gInSc4	hlas
při	pře	k1gFnSc3	pře
i	i	k8xC	i
po	po	k7c6	po
pubertě	puberta	k1gFnSc6	puberta
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
-	-	kIx~	-
puberphonie	puberphonie	k1gFnSc1	puberphonie
Poruchami	porucha	k1gFnPc7	porucha
hlasu	hlas	k1gInSc2	hlas
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
foniatrie	foniatrie	k1gFnSc1	foniatrie
<g/>
,	,	kIx,	,
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
disciplína	disciplína	k1gFnSc1	disciplína
otorinolaryngologie	otorinolaryngologie	k1gFnSc1	otorinolaryngologie
neboli	neboli	k8xC	neboli
ORL	ORL	kA	ORL
-	-	kIx~	-
"	"	kIx"	"
<g/>
ušní	ušní	k2eAgFnSc1d1	ušní
<g/>
,	,	kIx,	,
nosní	nosní	k2eAgFnSc1d1	nosní
<g/>
,	,	kIx,	,
krční	krční	k2eAgFnSc1d1	krční
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nápravou	náprava	k1gFnSc7	náprava
poruch	porucha	k1gFnPc2	porucha
řeči	řeč	k1gFnSc2	řeč
pak	pak	k6eAd1	pak
logopedie	logopedie	k1gFnSc2	logopedie
<g/>
.	.	kIx.	.
</s>
