<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
udělena	udělit	k5eAaPmNgFnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
i	i	k9	i
ocenění	ocenění	k1gNnSc3	ocenění
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
