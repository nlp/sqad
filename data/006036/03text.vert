<s>
Kolbachovo	Kolbachův	k2eAgNnSc1d1	Kolbachovo
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
ukazatelů	ukazatel	k1gInPc2	ukazatel
jakosti	jakost	k1gFnSc2	jakost
sladovnického	sladovnický	k2eAgInSc2d1	sladovnický
ječmene	ječmen	k1gInSc2	ječmen
a	a	k8xC	a
sladu	slad	k1gInSc2	slad
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
poměr	poměr	k1gInSc1	poměr
rozpustného	rozpustný	k2eAgInSc2d1	rozpustný
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
sladině	sladina	k1gFnSc6	sladina
k	k	k7c3	k
celkovému	celkový	k2eAgInSc3d1	celkový
obsahu	obsah	k1gInSc3	obsah
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
sladu	slad	k1gInSc6	slad
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
za	za	k7c4	za
optimální	optimální	k2eAgNnSc4d1	optimální
Kolbachovo	Kolbachův	k2eAgNnSc4d1	Kolbachovo
číslo	číslo	k1gNnSc4	číslo
považuje	považovat	k5eAaImIp3nS	považovat
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
