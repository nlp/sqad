<s>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
je	být	k5eAaImIp3nS	být
opera	opera	k1gFnSc1	opera
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
libreto	libreto	k1gNnSc4	libreto
skladatel	skladatel	k1gMnSc1	skladatel
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
úpravou	úprava	k1gFnSc7	úprava
(	(	kIx(	(
<g/>
zkrácením	zkrácení	k1gNnSc7	zkrácení
<g/>
)	)	kIx)	)
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
dramatu	drama	k1gNnSc2	drama
Gabriely	Gabriela	k1gFnSc2	Gabriela
Preissové	Preissová	k1gFnSc2	Preissová
<g/>
.	.	kIx.	.
</s>
