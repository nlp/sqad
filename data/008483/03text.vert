<p>
<s>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
je	být	k5eAaImIp3nS	být
opera	opera	k1gFnSc1	opera
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
libreto	libreto	k1gNnSc4	libreto
skladatel	skladatel	k1gMnSc1	skladatel
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
úpravou	úprava	k1gFnSc7	úprava
(	(	kIx(	(
<g/>
zkrácením	zkrácení	k1gNnSc7	zkrácení
<g/>
)	)	kIx)	)
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
dramatu	drama	k1gNnSc2	drama
Gabriely	Gabriela	k1gFnSc2	Gabriela
Preissové	Preissová	k1gFnSc2	Preissová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
je	být	k5eAaImIp3nS	být
opera	opera	k1gFnSc1	opera
zpravidla	zpravidla	k6eAd1	zpravidla
uváděna	uvádět	k5eAaImNgFnS	uvádět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoby	osoba	k1gFnSc2	osoba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
vzniku	vznik	k1gInSc2	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Gabriely	Gabriela	k1gFnSc2	Gabriela
Preissové	Preissová	k1gFnSc2	Preissová
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
autorčiny	autorčin	k2eAgFnSc2d1	autorčina
hry	hra	k1gFnSc2	hra
Gazdina	gazdina	k1gFnSc1	gazdina
roba	roba	k1gFnSc1	roba
neměla	mít	k5eNaImAgFnS	mít
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
ještě	ještě	k9	ještě
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
seznámil	seznámit	k5eAaPmAgMnS	seznámit
i	i	k9	i
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pozdějších	pozdní	k2eAgInPc6d2	pozdější
úspěších	úspěch	k1gInPc6	úspěch
opery	opera	k1gFnSc2	opera
Preissová	Preissový	k2eAgFnSc1d1	Preissová
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929-1930	[number]	k4	1929-1930
drama	drama	k1gFnSc1	drama
přepsala	přepsat	k5eAaPmAgFnS	přepsat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
požádal	požádat	k5eAaPmAgMnS	požádat
autorku	autorka	k1gFnSc4	autorka
o	o	k7c4	o
svolení	svolení	k1gNnSc4	svolení
dílo	dílo	k1gNnSc1	dílo
zhudebnit	zhudebnit	k5eAaPmF	zhudebnit
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
autorka	autorka	k1gFnSc1	autorka
zpočátku	zpočátku	k6eAd1	zpočátku
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
<g/>
,	,	kIx,	,
Janáček	Janáček	k1gMnSc1	Janáček
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
hru	hra	k1gFnSc4	hra
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
libreta	libreto	k1gNnSc2	libreto
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachoval	zachovat	k5eAaPmAgInS	zachovat
prozaickou	prozaický	k2eAgFnSc4d1	prozaická
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
průkopníků	průkopník	k1gMnPc2	průkopník
opery	opera	k1gFnSc2	opera
zpívané	zpívaný	k2eAgFnSc2d1	zpívaná
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
veršů	verš	k1gInPc2	verš
si	se	k3xPyFc3	se
v	v	k7c6	v
libretu	libreto	k1gNnSc6	libreto
vypomáhá	vypomáhat	k5eAaImIp3nS	vypomáhat
častým	častý	k2eAgNnSc7d1	časté
opakováním	opakování	k1gNnSc7	opakování
motivů	motiv	k1gInPc2	motiv
a	a	k8xC	a
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
opeře	opera	k1gFnSc6	opera
pracoval	pracovat	k5eAaImAgMnS	pracovat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
dokončil	dokončit	k5eAaPmAgInS	dokončit
až	až	k6eAd1	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
operu	opera	k1gFnSc4	opera
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Šéfdirigentem	šéfdirigent	k1gMnSc7	šéfdirigent
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Kovařovic	Kovařovice	k1gFnPc2	Kovařovice
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
Janáčkem	Janáček	k1gMnSc7	Janáček
osobní	osobní	k2eAgFnSc2d1	osobní
rozepře	rozepře	k1gFnSc2	rozepře
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
opera	opera	k1gFnSc1	opera
nastudována	nastudován	k2eAgFnSc1d1	nastudována
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1904	[number]	k4	1904
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c4	na
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
sklidila	sklidit	k5eAaPmAgFnS	sklidit
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
byla	být	k5eAaImAgNnP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
Janáčkových	Janáčkových	k2eAgFnPc6d1	Janáčkových
i	i	k8xC	i
Kovařovicových	Kovařovicový	k2eAgFnPc6d1	Kovařovicový
úpravách	úprava	k1gFnPc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Sklidila	sklidit	k5eAaPmAgFnS	sklidit
opět	opět	k6eAd1	opět
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
nastudována	nastudovat	k5eAaBmNgFnS	nastudovat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
metropolích	metropol	k1gFnPc6	metropol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
Metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
opeře	opera	k1gFnSc6	opera
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Artur	Artur	k1gMnSc1	Artur
Bodanzky	Bodanzka	k1gFnSc2	Bodanzka
<g/>
,	,	kIx,	,
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
zpívala	zpívat	k5eAaImAgFnS	zpívat
Maria	Maria	k1gFnSc1	Maria
Jeritza	Jeritza	k1gFnSc1	Jeritza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
toto	tento	k3xDgNnSc4	tento
uvedení	uvedení	k1gNnSc4	uvedení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
opera	opera	k1gFnSc1	opera
uváděla	uvádět	k5eAaImAgFnS	uvádět
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
překladu	překlad	k1gInSc6	překlad
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
předtavení	předtavení	k1gNnSc6	předtavení
řídil	řídit	k5eAaImAgMnS	řídit
John	John	k1gMnSc1	John
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
byla	být	k5eAaImAgFnS	být
opera	opera	k1gFnSc1	opera
poprvé	poprvé	k6eAd1	poprvé
uvedena	uveden	k2eAgFnSc1d1	uvedena
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
zpívala	zpívat	k5eAaImAgFnS	zpívat
Gabriela	Gabriela	k1gFnSc1	Gabriela
Beňačková	Beňačková	k1gFnSc1	Beňačková
a	a	k8xC	a
představení	představení	k1gNnSc4	představení
řídil	řídit	k5eAaImAgMnS	řídit
James	James	k1gMnSc1	James
Conlon	Conlon	k1gInSc4	Conlon
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
české	český	k2eAgNnSc1d1	české
uvedení	uvedení	k1gNnSc1	uvedení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Jiřího	Jiří	k1gMnSc2	Jiří
Bělohlávka	Bělohlávek	k1gMnSc2	Bělohlávek
<g/>
.	.	kIx.	.
<g/>
Dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
repertoáru	repertoár	k1gInSc2	repertoár
mnoha	mnoho	k4c2	mnoho
světových	světový	k2eAgFnPc2d1	světová
oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
uváděna	uvádět	k5eAaImNgFnS	uvádět
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
s	s	k7c7	s
originálním	originální	k2eAgInSc7d1	originální
textem	text	k1gInSc7	text
libreta	libreto	k1gNnSc2	libreto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
podhorském	podhorský	k2eAgInSc6d1	podhorský
mlýně	mlýn	k1gInSc6	mlýn
nedaleko	nedaleko	k7c2	nedaleko
vesnice	vesnice	k1gFnSc2	vesnice
hospodaří	hospodařit	k5eAaImIp3nP	hospodařit
svým	svůj	k3xOyFgMnPc3	svůj
dvěma	dva	k4xCgMnPc3	dva
vnukům	vnuk	k1gMnPc3	vnuk
<g/>
,	,	kIx,	,
nevlastním	vlastní	k2eNgMnPc3d1	nevlastní
bratrům	bratr	k1gMnPc3	bratr
-	-	kIx~	-
mlynáři	mlynář	k1gMnSc3	mlynář
Števovi	Števa	k1gMnSc3	Števa
a	a	k8xC	a
mládkovi	mládek	k1gMnSc3	mládek
Lacovi	Laca	k1gMnSc3	Laca
-	-	kIx~	-
stařenka	stařenka	k1gFnSc1	stařenka
Buryjovka	Buryjovka	k1gFnSc1	Buryjovka
<g/>
.	.	kIx.	.
</s>
<s>
Vypomáhá	vypomáhat	k5eAaImIp3nS	vypomáhat
jí	jíst	k5eAaImIp3nS	jíst
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
<g/>
,	,	kIx,	,
příbuzná	příbuzná	k1gFnSc1	příbuzná
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
pastorkyně	pastorkyně	k1gFnSc1	pastorkyně
(	(	kIx(	(
<g/>
schovanka	schovanka	k1gFnSc1	schovanka
<g/>
)	)	kIx)	)
vdovy	vdova	k1gFnPc1	vdova
Kostelničky	kostelnička	k1gFnSc2	kostelnička
Buryjovky	Buryjovka	k1gFnSc2	Buryjovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
statečným	statečný	k2eAgInSc7d1	statečný
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
životu	život	k1gInSc3	život
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
respekt	respekt	k1gInSc4	respekt
celé	celý	k2eAgFnSc2d1	celá
vsi	ves	k1gFnSc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
lehkovážného	lehkovážný	k2eAgMnSc2d1	lehkovážný
vesnického	vesnický	k2eAgMnSc2d1	vesnický
krasavce	krasavec	k1gMnSc2	krasavec
Števy	Števa	k1gFnSc2	Števa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
neklidem	neklid	k1gInSc7	neklid
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
jeho	jeho	k3xOp3gNnSc1	jeho
návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
předvolán	předvolat	k5eAaPmNgInS	předvolat
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
zatím	zatím	k6eAd1	zatím
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
očekává	očekávat	k5eAaImIp3nS	očekávat
Števovo	Števův	k2eAgNnSc4d1	Števův
dítě	dítě	k1gNnSc4	dítě
-	-	kIx~	-
nástup	nástup	k1gInSc1	nástup
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
by	by	kYmCp3nS	by
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gInSc1	jejich
sňatek	sňatek	k1gInSc1	sňatek
překazil	překazit	k5eAaPmAgInS	překazit
<g/>
.	.	kIx.	.
</s>
<s>
Jenůfu	Jenůfa	k1gFnSc4	Jenůfa
upřímně	upřímně	k6eAd1	upřímně
miluje	milovat	k5eAaImIp3nS	milovat
mládek	mládek	k1gMnSc1	mládek
Laca	Laca	k1gMnSc1	Laca
<g/>
,	,	kIx,	,
bratrovi	bratrův	k2eAgMnPc1d1	bratrův
dívku	dívka	k1gFnSc4	dívka
nepřeje	přát	k5eNaImIp3nS	přát
a	a	k8xC	a
rád	rád	k6eAd1	rád
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Jenůfku	Jenůfka	k1gFnSc4	Jenůfka
stále	stále	k6eAd1	stále
žárlivě	žárlivě	k6eAd1	žárlivě
střeží	střežit	k5eAaImIp3nS	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Števa	Števa	k6eAd1	Števa
odveden	odveden	k2eAgMnSc1d1	odveden
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
v	v	k7c6	v
povznešené	povznešený	k2eAgFnSc6d1	povznešená
náladě	nálada	k1gFnSc6	nálada
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
rekrutů	rekrut	k1gMnPc2	rekrut
do	do	k7c2	do
mlýna	mlýn	k1gInSc2	mlýn
a	a	k8xC	a
furiantsky	furiantsky	k6eAd1	furiantsky
se	se	k3xPyFc4	se
před	před	k7c7	před
Jenůfou	Jenůfa	k1gFnSc7	Jenůfa
vychloubá	vychloubat	k5eAaImIp3nS	vychloubat
úspěchy	úspěch	k1gInPc4	úspěch
u	u	k7c2	u
místních	místní	k2eAgFnPc2d1	místní
děvčat	děvče	k1gNnPc2	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
rozjařenou	rozjařený	k2eAgFnSc4d1	rozjařená
mládež	mládež	k1gFnSc4	mládež
náhle	náhle	k6eAd1	náhle
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
Kostelnička	kostelnička	k1gFnSc1	kostelnička
<g/>
.	.	kIx.	.
</s>
<s>
Přísná	přísný	k2eAgFnSc1d1	přísná
pěstounka	pěstounka	k1gFnSc1	pěstounka
Jenůfy	Jenůfa	k1gFnSc2	Jenůfa
rozhodně	rozhodně	k6eAd1	rozhodně
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Števa	Števa	k1gFnSc1	Števa
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
pastorkyni	pastorkyně	k1gFnSc4	pastorkyně
ucházel	ucházet	k5eAaImAgMnS	ucházet
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
po	po	k7c6	po
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zanechá	zanechat	k5eAaPmIp3nS	zanechat
pití	pití	k1gNnSc1	pití
<g/>
.	.	kIx.	.
</s>
<s>
Zděšená	zděšený	k2eAgFnSc1d1	zděšená
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
matka	matka	k1gFnSc1	matka
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
výchově	výchova	k1gFnSc6	výchova
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Števovi	Števa	k1gMnSc3	Števa
domlouvá	domlouvat	k5eAaImIp3nS	domlouvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
udobřil	udobřit	k5eAaPmAgMnS	udobřit
<g/>
.	.	kIx.	.
</s>
<s>
Laca	Laca	k6eAd1	Laca
nesmiřitelně	smiřitelně	k6eNd1	smiřitelně
žárlí	žárlit	k5eAaImIp3nS	žárlit
na	na	k7c4	na
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
láká	lákat	k5eAaImIp3nS	lákat
jen	jen	k9	jen
Jenůfina	Jenůfin	k2eAgFnSc1d1	Jenůfin
krása	krása	k1gFnSc1	krása
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
při	při	k7c6	při
potyčce	potyčka	k1gFnSc6	potyčka
poraní	poranit	k5eAaPmIp3nP	poranit
dívčinu	dívčin	k2eAgFnSc4d1	dívčina
tvář	tvář	k1gFnSc4	tvář
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Kostelnička	kostelnička	k1gFnSc1	kostelnička
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domku	domek	k1gInSc6	domek
Jenůfu	Jenůfa	k1gFnSc4	Jenůfa
i	i	k8xC	i
s	s	k7c7	s
narozeným	narozený	k2eAgMnSc7d1	narozený
Števovým	Števův	k2eAgMnSc7d1	Števův
synkem	synek	k1gMnSc7	synek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svatby	svatba	k1gFnSc2	svatba
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
,	,	kIx,	,
Števovi	Števův	k2eAgMnPc1d1	Števův
se	se	k3xPyFc4	se
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
s	s	k7c7	s
poznamenanou	poznamenaný	k2eAgFnSc7d1	poznamenaná
tváří	tvář	k1gFnSc7	tvář
znelíbila	znelíbit	k5eAaPmAgFnS	znelíbit
<g/>
.	.	kIx.	.
</s>
<s>
Kostelnička	kostelnička	k1gFnSc1	kostelnička
poklesek	poklesek	k1gInSc1	poklesek
své	svůj	k3xOyFgFnSc2	svůj
schovanky	schovanka	k1gFnSc2	schovanka
před	před	k7c7	před
lidmi	člověk	k1gMnPc7	člověk
tají	tají	k6eAd1	tají
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránila	zachránit	k5eAaPmAgFnS	zachránit
dívčinu	dívčin	k2eAgFnSc4d1	dívčina
pověst	pověst	k1gFnSc4	pověst
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlásila	rozhlásit	k5eAaPmAgFnS	rozhlásit
proto	proto	k8xC	proto
po	po	k7c6	po
vsi	ves	k1gFnSc6	ves
<g/>
,	,	kIx,	,
že	že	k8xS	že
poslala	poslat	k5eAaPmAgFnS	poslat
Jenůfu	Jenůfa	k1gFnSc4	Jenůfa
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
sloužit	sloužit	k5eAaImF	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Kostelnička	kostelnička	k1gFnSc1	kostelnička
pozve	pozvat	k5eAaPmIp3nS	pozvat
Števu	Števa	k1gFnSc4	Števa
k	k	k7c3	k
rozmluvě	rozmluva	k1gFnSc3	rozmluva
a	a	k8xC	a
pokoří	pokořit	k5eAaPmIp3nS	pokořit
se	se	k3xPyFc4	se
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
přece	přece	k9	přece
jen	jen	k9	jen
přiměla	přimět	k5eAaPmAgFnS	přimět
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
s	s	k7c7	s
pastorkyní	pastorkyně	k1gFnSc7	pastorkyně
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
zoufalé	zoufalý	k2eAgFnPc1d1	zoufalá
prosby	prosba	k1gFnPc1	prosba
jsou	být	k5eAaImIp3nP	být
marné	marný	k2eAgFnPc1d1	marná
<g/>
,	,	kIx,	,
Števova	Števův	k2eAgFnSc1d1	Števova
náklonnost	náklonnost	k1gFnSc1	náklonnost
k	k	k7c3	k
Jenůfce	Jenůfka	k1gFnSc3	Jenůfka
zcela	zcela	k6eAd1	zcela
zmizela	zmizet	k5eAaPmAgFnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
brzy	brzy	k6eAd1	brzy
bude	být	k5eAaImBp3nS	být
brát	brát	k5eAaImF	brát
Karolku	Karolka	k1gFnSc4	Karolka
<g/>
,	,	kIx,	,
rychtářovu	rychtářův	k2eAgFnSc4d1	Rychtářova
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Laca	Laca	k1gFnSc1	Laca
pevně	pevně	k6eAd1	pevně
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
úmyslu	úmysl	k1gInSc6	úmysl
ucházet	ucházet	k5eAaImF	ucházet
se	se	k3xPyFc4	se
o	o	k7c4	o
Jenůfu	Jenůfa	k1gFnSc4	Jenůfa
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Kostelnička	kostelnička	k1gFnSc1	kostelnička
prozradí	prozradit	k5eAaPmIp3nS	prozradit
dívčino	dívčin	k2eAgNnSc4d1	dívčino
neštěstí	neštěstí	k1gNnSc4	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Lacu	Lacu	k6eAd1	Lacu
přece	přece	k9	přece
jen	jen	k9	jen
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
zarazí	zarazit	k5eAaPmIp3nS	zarazit
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Števově	Števův	k2eAgNnSc6d1	Števův
dítěti	dítě	k1gNnSc6	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Kostelnička	kostelnička	k1gFnSc1	kostelnička
se	se	k3xPyFc4	se
v	v	k7c6	v
rozrušení	rozrušení	k1gNnSc6	rozrušení
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
k	k	k7c3	k
hroznému	hrozný	k2eAgInSc3d1	hrozný
činu	čin	k1gInSc3	čin
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
únavou	únava	k1gFnSc7	únava
usne	usnout	k5eAaPmIp3nS	usnout
<g/>
,	,	kIx,	,
Kostelnička	kostelnička	k1gFnSc1	kostelnička
vyběhne	vyběhnout	k5eAaPmIp3nS	vyběhnout
s	s	k7c7	s
nemluvnětem	nemluvně	k1gNnSc7	nemluvně
do	do	k7c2	do
mrazivé	mrazivý	k2eAgFnSc2d1	mrazivá
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
utopí	utopit	k5eAaPmIp3nP	utopit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
mlýnském	mlýnský	k2eAgInSc6d1	mlýnský
náhonu	náhon	k1gInSc6	náhon
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pastorkyni	pastorkyně	k1gFnSc3	pastorkyně
i	i	k8xC	i
Lacovi	Laca	k1gMnSc3	Laca
namluví	namluvit	k5eAaBmIp3nS	namluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
děcko	děcko	k1gNnSc1	děcko
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
spala	spát	k5eAaImAgFnS	spát
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
přistoupí	přistoupit	k5eAaPmIp3nP	přistoupit
na	na	k7c4	na
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
Lacou	Laca	k1gFnSc7	Laca
<g/>
,	,	kIx,	,
Kostelnička	kostelnička	k1gFnSc1	kostelnička
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachránila	zachránit	k5eAaPmAgFnS	zachránit
budoucnost	budoucnost	k1gFnSc4	budoucnost
milované	milovaný	k2eAgFnSc3d1	milovaná
pastorkyně	pastorkyňa	k1gFnSc3	pastorkyňa
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
čisté	čistý	k2eAgNnSc4d1	čisté
svědomí	svědomí	k1gNnSc4	svědomí
<g/>
,	,	kIx,	,
však	však	k9	však
zatížila	zatížit	k5eAaPmAgFnS	zatížit
těžkou	těžký	k2eAgFnSc7d1	těžká
vinou	vina	k1gFnSc7	vina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Blíží	blížit	k5eAaImIp3nP	blížit
se	se	k3xPyFc4	se
svatební	svatební	k2eAgInPc1d1	svatební
obřady	obřad	k1gInPc1	obřad
Jenůfy	Jenůfa	k1gFnSc2	Jenůfa
a	a	k8xC	a
Laci	Lac	k1gFnSc2	Lac
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
Števa	Števa	k1gFnSc1	Števa
a	a	k8xC	a
Karolka	Karolka	k1gFnSc1	Karolka
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
brzy	brzy	k6eAd1	brzy
brát	brát	k5eAaImF	brát
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Kostelnička	kostelnička	k1gFnSc1	kostelnička
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
proměněná	proměněný	k2eAgFnSc1d1	proměněná
<g/>
:	:	kIx,	:
z	z	k7c2	z
energické	energický	k2eAgFnSc2d1	energická
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
plné	plný	k2eAgFnSc2d1	plná
rozhodnosti	rozhodnost	k1gFnSc2	rozhodnost
<g/>
,	,	kIx,	,
zbývá	zbývat	k5eAaImIp3nS	zbývat
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Sužují	sužovat	k5eAaImIp3nP	sužovat
ji	on	k3xPp3gFnSc4	on
zlé	zlý	k2eAgFnPc1d1	zlá
výčitky	výčitka	k1gFnPc1	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
Svatebčané	svatebčan	k1gMnPc1	svatebčan
se	se	k3xPyFc4	se
scházejí	scházet	k5eAaImIp3nP	scházet
<g/>
,	,	kIx,	,
děvčata	děvče	k1gNnPc4	děvče
vážné	vážný	k2eAgFnSc2d1	vážná
Jenůfce	Jenůfka	k1gFnSc3	Jenůfka
zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
,	,	kIx,	,
prohlíží	prohlížet	k5eAaImIp3nS	prohlížet
se	se	k3xPyFc4	se
výbava	výbava	k1gFnSc1	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Stařenka	stařenka	k1gFnSc1	stařenka
Buryjovka	Buryjovka	k1gFnSc1	Buryjovka
uděluje	udělovat	k5eAaImIp3nS	udělovat
dvojici	dvojice	k1gFnSc4	dvojice
požehnání	požehnání	k1gNnSc2	požehnání
a	a	k8xC	a
též	též	k9	též
dojatá	dojatý	k2eAgFnSc1d1	dojatá
Kostelnička	kostelnička	k1gFnSc1	kostelnička
chce	chtít	k5eAaImIp3nS	chtít
učinit	učinit	k5eAaPmF	učinit
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Nebude	být	k5eNaImBp3nS	být
jí	on	k3xPp3gFnSc7	on
dopřáno	dopřát	k5eAaPmNgNnS	dopřát
<g/>
!	!	kIx.	!
</s>
<s>
Venku	venku	k6eAd1	venku
se	se	k3xPyFc4	se
strhne	strhnout	k5eAaPmIp3nS	strhnout
křik	křik	k1gInSc1	křik
-	-	kIx~	-
pasáček	pasáček	k1gMnSc1	pasáček
přibíhá	přibíhat	k5eAaImIp3nS	přibíhat
s	s	k7c7	s
úděsnou	úděsný	k2eAgFnSc7d1	úděsná
zprávou	zpráva	k1gFnSc7	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
pivovaru	pivovar	k1gInSc2	pivovar
objevili	objevit	k5eAaPmAgMnP	objevit
pod	pod	k7c7	pod
ledem	led	k1gInSc7	led
ve	v	k7c6	v
strouze	strouha	k1gFnSc6	strouha
zmrzlé	zmrzlý	k2eAgNnSc4d1	zmrzlé
děcko	děcko	k1gNnSc4	děcko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
vřavě	vřava	k1gFnSc6	vřava
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jejího	její	k3xOp3gMnSc4	její
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
synka	synek	k1gMnSc4	synek
a	a	k8xC	a
označí	označit	k5eAaPmIp3nS	označit
Števu	Števa	k1gFnSc4	Števa
jako	jako	k8xS	jako
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
rozvášněný	rozvášněný	k2eAgInSc1d1	rozvášněný
dav	dav	k1gInSc1	dav
stačí	stačit	k5eAaBmIp3nS	stačit
na	na	k7c4	na
nešťastnou	šťastný	k2eNgFnSc4d1	nešťastná
dívku	dívka	k1gFnSc4	dívka
vrhnout	vrhnout	k5eAaImF	vrhnout
<g/>
,	,	kIx,	,
Kostelnička	kostelnička	k1gFnSc1	kostelnička
se	se	k3xPyFc4	se
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
rychtářem	rychtář	k1gMnSc7	rychtář
odvést	odvést	k5eAaPmF	odvést
a	a	k8xC	a
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
chce	chtít	k5eAaImIp3nS	chtít
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
doznat	doznat	k5eAaPmF	doznat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zbavila	zbavit	k5eAaPmAgFnS	zbavit
deptající	deptající	k2eAgFnPc4d1	deptající
tíhy	tíha	k1gFnPc4	tíha
na	na	k7c6	na
duši	duše	k1gFnSc6	duše
<g/>
.	.	kIx.	.
</s>
<s>
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
bezmeznou	bezmezný	k2eAgFnSc4d1	bezmezná
oběť	oběť	k1gFnSc4	oběť
své	svůj	k3xOyFgFnSc2	svůj
pěstounky	pěstounka	k1gFnSc2	pěstounka
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
jí	on	k3xPp3gFnSc7	on
odpouští	odpouštět	k5eAaImIp3nP	odpouštět
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svatby	svatba	k1gFnSc2	svatba
Karolky	Karolka	k1gFnSc2	Karolka
a	a	k8xC	a
Števy	Števa	k1gFnSc2	Števa
také	také	k9	také
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
zdrceně	zdrceně	k6eAd1	zdrceně
odcházejí	odcházet	k5eAaImIp3nP	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Jenůfka	Jenůfka	k1gFnSc1	Jenůfka
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
Lacu	Laca	k1gFnSc4	Laca
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
odejde	odejít	k5eAaPmIp3nS	odejít
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Laca	Laca	k6eAd1	Laca
jí	on	k3xPp3gFnSc2	on
naopak	naopak	k6eAd1	naopak
slibuje	slibovat	k5eAaImIp3nS	slibovat
nést	nést	k5eAaImF	nést
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
boku	bok	k1gInSc6	bok
všechna	všechen	k3xTgNnPc1	všechen
příkoří	příkoří	k1gNnPc1	příkoří
-	-	kIx~	-
však	však	k9	však
jednou	jednou	k6eAd1	jednou
nastane	nastat	k5eAaPmIp3nS	nastat
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zasvitne	zasvitnout	k5eAaPmIp3nS	zasvitnout
slunce	slunce	k1gNnSc1	slunce
nad	nad	k7c7	nad
společnou	společný	k2eAgFnSc7d1	společná
cestou	cesta	k1gFnSc7	cesta
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Měšťáka	měšťák	k1gMnSc2	měšťák
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
pastorkyně	pastorkyně	k1gFnSc1	pastorkyně
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Miroslava	Miroslav	k1gMnSc2	Miroslav
Cikána	cikán	k1gMnSc2	cikán
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
-	-	kIx~	-
televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
opery	opera	k1gFnSc2	opera
režisérky	režisérka	k1gFnSc2	režisérka
Evy	Eva	k1gFnSc2	Eva
Marie	Maria	k1gFnSc2	Maria
Bergerové	Bergerová	k1gFnSc2	Bergerová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
moravskoslezské	moravskoslezský	k2eAgFnPc1d1	Moravskoslezská
(	(	kIx(	(
<g/>
Daniel	Daniel	k1gMnSc1	Daniel
Jäger	Jäger	k1gMnSc1	Jäger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
moravskoslezské	moravskoslezský	k2eAgInPc1d1	moravskoslezský
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOSTOMSKÁ	HOSTOMSKÁ	kA	HOSTOMSKÁ
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
–	–	k?	–
Průvodce	průvodka	k1gFnSc6	průvodka
operní	operní	k2eAgFnSc7d1	operní
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NS	NS	kA	NS
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
1466	[number]	k4	1466
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
637	[number]	k4	637
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
747	[number]	k4	747
<g/>
–	–	k?	–
<g/>
749	[number]	k4	749
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
