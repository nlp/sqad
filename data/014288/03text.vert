<s>
International	Internationat	k5eAaPmAgMnS,k5eAaImAgMnS
Plant	planta	k1gFnPc2
Names	Namesa	k1gFnPc2
Index	index	k1gInSc1
</s>
<s>
International	Internationat	k5eAaImAgMnS,k5eAaPmAgMnS
Plant	planta	k1gFnPc2
Names	Namesa	k1gFnPc2
Index	index	k1gInSc1
(	(	kIx(
<g/>
IPNI	IPNI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
databáze	databáze	k1gFnSc1
vědeckých	vědecký	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
semenných	semenný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
kapradin	kapradina	k1gFnPc2
a	a	k8xC
jim	on	k3xPp3gFnPc3
příbuzných	příbuzný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInPc4d1
i	i	k8xC
základní	základní	k2eAgInPc4d1
bibliografické	bibliografický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
s	s	k7c7
těmito	tento	k3xDgInPc7
názvy	název	k1gInPc7
souvisí	souviset	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Databáze	databáze	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnPc4d3
podrobnosti	podrobnost	k1gFnPc4
na	na	k7c6
úrovni	úroveň	k1gFnSc6
druhů	druh	k1gInPc2
a	a	k8xC
rodů	rod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
eliminovat	eliminovat	k5eAaBmF
nutnost	nutnost	k1gFnSc4
neustále	neustále	k6eAd1
vyhledávat	vyhledávat	k5eAaImF
primární	primární	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
při	při	k7c6
shánění	shánění	k1gNnSc6
bibliografických	bibliografický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
rostlinných	rostlinný	k2eAgInPc6d1
názvech	název	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
International	International	k1gFnSc2
Plant	planta	k1gFnPc2
Names	Names	k1gMnSc1
Index	index	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
</s>
<s>
International	Internationat	k5eAaPmAgMnS,k5eAaImAgMnS
Plant	planta	k1gFnPc2
Name	Nam	k1gFnSc2
Index	index	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
</s>
<s>
LUGHADHA	LUGHADHA	kA
<g/>
,	,	kIx,
E.	E.	kA
N.	N.	kA
Towards	Towards	k1gInSc1
a	a	k8xC
working	working	k1gInSc1
list	list	k1gInSc1
of	of	k?
all	all	k?
known	known	k1gInSc1
plant	planta	k1gFnPc2
species	species	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philos	Philos	k1gInSc1
Trans	trans	k1gInSc4
R	R	kA
Soc	soc	kA
Lond	Lond	k1gMnSc1
B	B	kA
Biol	Biol	k1gMnSc1
Sci	Sci	k1gMnSc1
<g/>
..	..	k?
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
359	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1444	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
681	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
962	#num#	k4
<g/>
-	-	kIx~
<g/>
8436	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
CROFT	CROFT	kA
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
CROSS	CROSS	kA
<g/>
,	,	kIx,
N.	N.	kA
<g/>
;	;	kIx,
HINCHCLIFFE	HINCHCLIFFE	kA
<g/>
,	,	kIx,
S.	S.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plant	planta	k1gFnPc2
Names	Namesa	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
21	#num#	k4
<g/>
st	st	kA
Century	Centura	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
International	International	k1gFnSc2
Plant	planta	k1gFnPc2
Names	Namesa	k1gFnPc2
Index	index	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
Distributed	Distributed	k1gMnSc1
Data	datum	k1gNnSc2
Source	Source	k1gMnSc1
of	of	k?
General	General	k1gFnSc2
Accessibility	Accessibilita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taxon	taxon	k1gInSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
317	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
400262	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1224436	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
botaniků	botanik	k1gMnPc2
a	a	k8xC
mykologů	mykolog	k1gMnPc2
dle	dle	k7c2
zkratek	zkratka	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Biologie	biologie	k1gFnPc4
|	|	kIx~
Houby	houby	k6eAd1
|	|	kIx~
Informační	informační	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
knihovnictví	knihovnictví	k1gNnSc1
|	|	kIx~
Rostliny	rostlina	k1gFnPc1
</s>
