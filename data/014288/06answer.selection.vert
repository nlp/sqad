<s>
International	Internationat	k5eAaImAgMnS,k5eAaPmAgMnS
Plant	planta	k1gFnPc2
Names	Namesa	k1gFnPc2
Index	index	k1gInSc1
(	(	kIx(
<g/>
IPNI	IPNI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
databáze	databáze	k1gFnSc1
vědeckých	vědecký	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
semenných	semenný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
kapradin	kapradina	k1gFnPc2
a	a	k8xC
jim	on	k3xPp3gFnPc3
příbuzných	příbuzný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgInPc4d1
i	i	k8xC
základní	základní	k2eAgInPc4d1
bibliografické	bibliografický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
s	s	k7c7
těmito	tento	k3xDgInPc7
názvy	název	k1gInPc7
souvisí	souviset	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>