<p>
<s>
Eolické	eolický	k2eAgNnSc1d1	eolický
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
větrné	větrný	k2eAgFnSc2d1	větrná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
např.	např.	kA	např.
zahrazení	zahrazení	k1gNnSc4	zahrazení
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
nebo	nebo	k8xC	nebo
prameniště	prameniště	k1gNnSc1	prameniště
pohyblivými	pohyblivý	k2eAgFnPc7d1	pohyblivá
písečnými	písečný	k2eAgFnPc7d1	písečná
dunami	duna	k1gFnPc7	duna
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
přesypová	přesypový	k2eAgNnPc1d1	přesypový
jezera	jezero	k1gNnPc1	jezero
-	-	kIx~	-
hrazená	hrazený	k2eAgFnSc1d1	hrazená
písečnými	písečný	k2eAgInPc7d1	písečný
přesypy	přesyp	k1gInPc1	přesyp
</s>
</p>
<p>
<s>
deflační	deflační	k2eAgNnPc1d1	deflační
jezera	jezero	k1gNnPc1	jezero
-	-	kIx~	-
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
deflačních	deflační	k2eAgFnPc6d1	deflační
sníženinách	sníženina	k1gFnPc6	sníženina
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Balaton	Balaton	k1gInSc1	Balaton
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bezedné	bezedný	k2eAgNnSc4d1	bezedné
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čadské	čadský	k2eAgNnSc1d1	Čadské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moczydło	Moczydło	k1gMnSc1	Moczydło
<g/>
,	,	kIx,	,
Orzełek	Orzełek	k1gMnSc1	Orzełek
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
