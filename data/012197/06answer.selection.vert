<s>
Eolické	eolický	k2eAgNnSc1d1	eolický
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
větrné	větrný	k2eAgFnSc2d1	větrná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
