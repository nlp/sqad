<s>
Vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
je	být	k5eAaImIp3nS	být
letadlo	letadlo	k1gNnSc4	letadlo
lehčí	lehký	k2eAgNnSc4d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc4	vzduch
(	(	kIx(	(
<g/>
aerostat	aerostat	k1gInSc1	aerostat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
řiditelný	řiditelný	k2eAgInSc4d1	řiditelný
balón	balón	k1gInSc4	balón
<g/>
.	.	kIx.	.
</s>
<s>
Vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
mívá	mívat	k5eAaImIp3nS	mívat
obvykle	obvykle	k6eAd1	obvykle
doutníkový	doutníkový	k2eAgInSc4d1	doutníkový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
bývá	bývat	k5eAaImIp3nS	bývat
vybavena	vybavit	k5eAaPmNgFnS	vybavit
jedním	jeden	k4xCgInSc7	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
teoretický	teoretický	k2eAgInSc1d1	teoretický
návrh	návrh	k1gInSc1	návrh
řiditelné	řiditelný	k2eAgFnSc2d1	řiditelná
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
první	první	k4xOgInSc1	první
balón	balón	k1gInSc1	balón
bratří	bratr	k1gMnPc2	bratr
Montgolfierů	Montgolfier	k1gInPc2	Montgolfier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jej	on	k3xPp3gNnSc4	on
Jean-Baptiste-Marie	Jean-Baptiste-Marie	k1gFnPc1	Jean-Baptiste-Marie
Meusier	Meusira	k1gFnPc2	Meusira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
stavba	stavba	k1gFnSc1	stavba
nerealizovatelná	realizovatelný	k2eNgFnSc1d1	nerealizovatelná
pro	pro	k7c4	pro
neexistenci	neexistence	k1gFnSc4	neexistence
vhodného	vhodný	k2eAgInSc2d1	vhodný
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
skutečně	skutečně	k6eAd1	skutečně
fungující	fungující	k2eAgFnSc4d1	fungující
vzducholoď	vzducholoď	k1gFnSc4	vzducholoď
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
Henri	Henri	k1gNnPc2	Henri
Giffard	Giffarda	k1gFnPc2	Giffarda
<g/>
.	.	kIx.	.
</s>
<s>
Vybavil	vybavit	k5eAaPmAgMnS	vybavit
ji	on	k3xPp3gFnSc4	on
lehkým	lehký	k2eAgInSc7d1	lehký
parním	parní	k2eAgInSc7d1	parní
strojem	stroj	k1gInSc7	stroj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
řiditelná	řiditelný	k2eAgFnSc1d1	řiditelná
jen	jen	k9	jen
za	za	k7c2	za
úplného	úplný	k2eAgNnSc2d1	úplné
bezvětří	bezvětří	k1gNnSc2	bezvětří
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
krokem	krok	k1gInSc7	krok
vpřed	vpřed	k6eAd1	vpřed
byla	být	k5eAaImAgFnS	být
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
La	la	k1gNnSc4	la
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
poháněna	pohánět	k5eAaImNgFnS	pohánět
elektromotorem	elektromotor	k1gInSc7	elektromotor
<g/>
,	,	kIx,	,
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vrátit	vrátit	k5eAaPmF	vrátit
i	i	k8xC	i
proti	proti	k7c3	proti
slabému	slabý	k2eAgInSc3d1	slabý
větru	vítr	k1gInSc3	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
první	první	k4xOgFnSc1	první
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
<g/>
,	,	kIx,	,
poháněná	poháněný	k2eAgNnPc4d1	poháněné
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Zášleh	zášleh	k1gInSc1	zášleh
plamene	plamen	k1gInSc2	plamen
z	z	k7c2	z
motoru	motor	k1gInSc2	motor
ale	ale	k8xC	ale
způsobil	způsobit	k5eAaPmAgMnS	způsobit
její	její	k3xOp3gInSc4	její
požár	požár	k1gInSc4	požár
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
vynálezce	vynálezce	k1gMnSc2	vynálezce
i	i	k8xC	i
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
vznesla	vznést	k5eAaPmAgFnS	vznést
i	i	k9	i
unikátní	unikátní	k2eAgFnSc1d1	unikátní
celokovová	celokovový	k2eAgFnSc1d1	celokovová
(	(	kIx(	(
<g/>
hliníková	hliníkový	k2eAgFnSc1d1	hliníková
<g/>
)	)	kIx)	)
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
Davida	David	k1gMnSc2	David
Schwarze	Schwarz	k1gMnSc2	Schwarz
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
startu	start	k1gInSc6	start
havarovala	havarovat	k5eAaPmAgFnS	havarovat
vinou	vinou	k7c2	vinou
nezkušeného	zkušený	k2eNgMnSc2d1	nezkušený
pilota	pilot	k1gMnSc2	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
historie	historie	k1gFnSc1	historie
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
spojena	spojit	k5eAaPmNgFnS	spojit
především	především	k9	především
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
von	von	k1gInSc4	von
Zeppelinem	Zeppelin	k1gInSc7	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
se	se	k3xPyFc4	se
úspěchy	úspěch	k1gInPc1	úspěch
i	i	k8xC	i
nezdary	nezdar	k1gInPc1	nezdar
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Využil	využít	k5eAaPmAgMnS	využít
především	především	k6eAd1	především
Schwarzova	Schwarzův	k2eAgInSc2d1	Schwarzův
nápadu	nápad	k1gInSc2	nápad
využití	využití	k1gNnSc2	využití
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
kostry	kostra	k1gFnSc2	kostra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
let	let	k1gInSc4	let
jeho	jeho	k3xOp3gMnPc2	jeho
127	[number]	k4	127
metrů	metr	k1gMnPc2	metr
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
ztužené	ztužený	k2eAgFnSc2d1	ztužená
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
LZ	LZ	kA	LZ
1	[number]	k4	1
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1900	[number]	k4	1900
nad	nad	k7c7	nad
Bodamským	bodamský	k2eAgNnSc7d1	Bodamské
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
byly	být	k5eAaImAgFnP	být
vzducholodi	vzducholoď	k1gFnPc1	vzducholoď
dále	daleko	k6eAd2	daleko
zdokonalovány	zdokonalován	k2eAgFnPc1d1	zdokonalována
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
pak	pak	k6eAd1	pak
začaly	začít	k5eAaPmAgFnP	začít
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
civilní	civilní	k2eAgFnSc3d1	civilní
letecké	letecký	k2eAgFnSc3d1	letecká
přepravě	přeprava	k1gFnSc3	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
široce	široko	k6eAd1	široko
používaly	používat	k5eAaImAgFnP	používat
Francie	Francie	k1gFnPc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
dálkový	dálkový	k2eAgInSc4d1	dálkový
průzkum	průzkum	k1gInSc4	průzkum
<g/>
,	,	kIx,	,
smutně	smutně	k6eAd1	smutně
prosluly	proslout	k5eAaPmAgInP	proslout
také	také	k9	také
jako	jako	k8xC	jako
první	první	k4xOgInPc4	první
strategické	strategický	k2eAgInPc4d1	strategický
bombardéry	bombardér	k1gInPc4	bombardér
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc4d1	německá
vzducholodě	vzducholoď	k1gFnPc4	vzducholoď
typu	typ	k1gInSc2	typ
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
podnikaly	podnikat	k5eAaImAgInP	podnikat
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
lety	let	k1gInPc1	let
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bombardovaly	bombardovat	k5eAaImAgFnP	bombardovat
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
civilní	civilní	k2eAgInPc4d1	civilní
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
Německo	Německo	k1gNnSc1	Německo
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
typu	typ	k1gInSc2	typ
Schütte-Lanz	Schütte-Lanza	k1gFnPc2	Schütte-Lanza
či	či	k8xC	či
Parseval	Parseval	k1gFnPc2	Parseval
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
stroje	stroj	k1gInPc4	stroj
od	od	k7c2	od
firem	firma	k1gFnPc2	firma
Astra	astra	k1gFnSc1	astra
<g/>
,	,	kIx,	,
Chalais-Meudon	Chalais-Meudon	k1gInSc1	Chalais-Meudon
<g/>
,	,	kIx,	,
Zodiac	Zodiac	k1gInSc1	Zodiac
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
zdokonalování	zdokonalování	k1gNnSc3	zdokonalování
letadel	letadlo	k1gNnPc2	letadlo
těžších	těžký	k2eAgFnPc2d2	těžší
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytláčela	vytláčet	k5eAaImAgFnS	vytláčet
vzducholodi	vzducholoď	k1gFnPc4	vzducholoď
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
však	však	k9	však
zůstávaly	zůstávat	k5eAaImAgFnP	zůstávat
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
armád	armáda	k1gFnPc2	armáda
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
válce	válec	k1gInPc4	válec
nuceno	nucen	k2eAgNnSc1d1	nuceno
postoupit	postoupit	k5eAaPmF	postoupit
své	svůj	k3xOyFgFnPc4	svůj
vzducholodě	vzducholoď	k1gFnPc4	vzducholoď
vítězným	vítězný	k2eAgFnPc3d1	vítězná
mocnostem	mocnost	k1gFnPc3	mocnost
jako	jako	k8xS	jako
reparace	reparace	k1gFnPc1	reparace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vývoj	vývoj	k1gInSc1	vývoj
vzducholodí	vzducholoď	k1gFnSc7	vzducholoď
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
britská	britský	k2eAgFnSc1d1	britská
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
R	R	kA	R
34	[number]	k4	34
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
úspěšnému	úspěšný	k2eAgInSc3d1	úspěšný
přeletu	přelet	k1gInSc3	přelet
Atlantiku	Atlantik	k1gInSc2	Atlantik
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Evropa-Amerika	Evropa-Amerika	k1gFnSc1	Evropa-Amerika
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
přeletěla	přeletět	k5eAaPmAgFnS	přeletět
posádka	posádka	k1gFnSc1	posádka
upraveného	upravený	k2eAgInSc2d1	upravený
bombardéru	bombardér	k1gInSc2	bombardér
Vickers	Vickersa	k1gFnPc2	Vickersa
Vimy	Vima	k1gFnSc2	Vima
o	o	k7c4	o
pouhé	pouhý	k2eAgInPc4d1	pouhý
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
nadšení	nadšení	k1gNnSc1	nadšení
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
dalších	další	k2eAgFnPc2d1	další
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ale	ale	k9	ale
poněkud	poněkud	k6eAd1	poněkud
ochablo	ochabnout	k5eAaPmAgNnS	ochabnout
po	po	k7c6	po
sérii	série	k1gFnSc6	série
katastrof	katastrofa	k1gFnPc2	katastrofa
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
katastrofy	katastrofa	k1gFnPc1	katastrofa
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
části	část	k1gFnSc2	část
způsobeny	způsoben	k2eAgFnPc4d1	způsobena
vysoce	vysoce	k6eAd1	vysoce
vznětlivým	vznětlivý	k2eAgInSc7d1	vznětlivý
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
jejich	jejich	k3xOp3gFnSc4	jejich
náplň	náplň	k1gFnSc4	náplň
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
je	být	k5eAaImIp3nS	být
ohrozit	ohrozit	k5eAaPmF	ohrozit
zkrat	zkrat	k1gInSc1	zkrat
v	v	k7c6	v
palubní	palubní	k2eAgFnSc6d1	palubní
elektroinstalaci	elektroinstalace	k1gFnSc6	elektroinstalace
<g/>
,	,	kIx,	,
úder	úder	k1gInSc4	úder
blesku	blesk	k1gInSc2	blesk
<g/>
,	,	kIx,	,
nekázeň	nekázeň	k1gFnSc1	nekázeň
posádky	posádka	k1gFnSc2	posádka
či	či	k8xC	či
zášleh	zášleh	k1gInSc1	zášleh
z	z	k7c2	z
výfuku	výfuk	k1gInSc2	výfuk
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
měly	mít	k5eAaImAgFnP	mít
proto	proto	k8xC	proto
vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
motory	motor	k1gInPc4	motor
v	v	k7c6	v
gondolách	gondola	k1gFnPc6	gondola
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgNnSc4d1	hlavní
těleso	těleso	k1gNnSc4	těleso
a	a	k8xC	a
na	na	k7c6	na
palubách	paluba	k1gFnPc6	paluba
se	se	k3xPyFc4	se
nesmělo	smět	k5eNaImAgNnS	smět
kouřit	kouřit	k5eAaImF	kouřit
<g/>
.	.	kIx.	.
</s>
<s>
Vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaPmNgFnP	využívat
i	i	k9	i
pro	pro	k7c4	pro
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Připomeňme	připomenout	k5eAaPmRp1nP	připomenout
alespoň	alespoň	k9	alespoň
výpravy	výprava	k1gFnPc4	výprava
Umberta	Umbert	k1gMnSc2	Umbert
Nobileho	Nobile	k1gMnSc2	Nobile
ve	v	k7c6	v
vzducholodích	vzducholoď	k1gFnPc6	vzducholoď
Norge	Norg	k1gInSc2	Norg
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1926	[number]	k4	1926
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
)	)	kIx)	)
a	a	k8xC	a
Italia	Italia	k1gFnSc1	Italia
(	(	kIx(	(
<g/>
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1928	[number]	k4	1928
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
;	;	kIx,	;
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
byl	být	k5eAaImAgMnS	být
i	i	k9	i
český	český	k2eAgMnSc1d1	český
fyzik	fyzik	k1gMnSc1	fyzik
František	František	k1gMnSc1	František
Běhounek	běhounek	k1gMnSc1	běhounek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
obřích	obří	k2eAgFnPc2d1	obří
dopravních	dopravní	k2eAgFnPc2d1	dopravní
a	a	k8xC	a
vojenských	vojenský	k2eAgFnPc2d1	vojenská
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
Graf	graf	k1gInSc4	graf
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
transatlantickou	transatlantický	k2eAgFnSc4d1	transatlantická
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
podnikla	podniknout	k5eAaPmAgFnS	podniknout
mnoho	mnoho	k4c4	mnoho
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
a	a	k8xC	a
propagačních	propagační	k2eAgInPc2d1	propagační
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
konec	konec	k1gInSc4	konec
éry	éra	k1gFnSc2	éra
velkých	velký	k2eAgFnPc2d1	velká
dopravních	dopravní	k2eAgFnPc2d1	dopravní
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
katastrofa	katastrofa	k1gFnSc1	katastrofa
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
vzducholodi	vzducholoď	k1gFnPc1	vzducholoď
používány	používat	k5eAaImNgFnP	používat
k	k	k7c3	k
protiponorkovému	protiponorkový	k2eAgNnSc3d1	protiponorkové
hlídkování	hlídkování	k1gNnSc3	hlídkování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
postupně	postupně	k6eAd1	postupně
upadal	upadat	k5eAaPmAgInS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
měla	mít	k5eAaImAgFnS	mít
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc1d1	poslední
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Havárie	havárie	k1gFnSc1	havárie
moderní	moderní	k2eAgFnSc2d1	moderní
radiolokační	radiolokační	k2eAgFnSc2d1	radiolokační
vzducholodě	vzducholoď	k1gFnSc2	vzducholoď
typu	typ	k1gInSc2	typ
ZPG-3W	ZPG-3W	k1gFnSc2	ZPG-3W
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
definitivní	definitivní	k2eAgFnSc7d1	definitivní
tečkou	tečka	k1gFnSc7	tečka
i	i	k8xC	i
za	za	k7c7	za
jejich	jejich	k3xOp3gNnSc7	jejich
armádním	armádní	k2eAgNnSc7d1	armádní
použitím	použití	k1gNnSc7	použití
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1961	[number]	k4	1961
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
námořní	námořní	k2eAgFnSc1d1	námořní
vzduchoplavba	vzduchoplavba	k1gFnSc1	vzduchoplavba
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
doprava	doprava	k1gFnSc1	doprava
vzducholoděmi	vzducholoď	k1gFnPc7	vzducholoď
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
a	a	k8xC	a
vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vzduchem	vzduch	k1gInSc7	vzduch
plující	plující	k2eAgFnSc1d1	plující
reklama	reklama	k1gFnSc1	reklama
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
se	se	k3xPyFc4	se
ale	ale	k9	ale
možnost	možnost	k1gFnSc1	možnost
používat	používat	k5eAaImF	používat
je	on	k3xPp3gFnPc4	on
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
dopravní	dopravní	k2eAgFnPc1d1	dopravní
společnosti	společnost	k1gFnPc1	společnost
stále	stále	k6eAd1	stále
plánují	plánovat	k5eAaImIp3nP	plánovat
využití	využití	k1gNnSc4	využití
moderních	moderní	k2eAgFnPc2d1	moderní
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
jak	jak	k8xS	jak
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
turistické	turistický	k2eAgInPc4d1	turistický
či	či	k8xC	či
rekreační	rekreační	k2eAgInPc4d1	rekreační
vyhlídkové	vyhlídkový	k2eAgInPc4d1	vyhlídkový
lety	let	k1gInPc4	let
<g/>
)	)	kIx)	)
tak	tak	k9	tak
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
plánech	plán	k1gInPc6	plán
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
technicky	technicky	k6eAd1	technicky
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
výhodné	výhodný	k2eAgNnSc1d1	výhodné
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
těžkých	těžký	k2eAgInPc2d1	těžký
a	a	k8xC	a
rozměrných	rozměrný	k2eAgInPc2d1	rozměrný
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
přeprava	přeprava	k1gFnSc1	přeprava
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
či	či	k8xC	či
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
bývá	bývat	k5eAaImIp3nS	bývat
technicky	technicky	k6eAd1	technicky
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
a	a	k8xC	a
drahá	drahý	k2eAgFnSc1d1	drahá
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
plněny	plnit	k5eAaImNgInP	plnit
netečným	tečný	k2eNgMnPc3d1	tečný
plynem	plyn	k1gInSc7	plyn
heliem	helium	k1gNnSc7	helium
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
provozního	provozní	k2eAgNnSc2d1	provozní
hlediska	hledisko	k1gNnSc2	hledisko
stroje	stroj	k1gInSc2	stroj
velmi	velmi	k6eAd1	velmi
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Ambiciózní	ambiciózní	k2eAgInSc1d1	ambiciózní
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
velkých	velký	k2eAgFnPc2d1	velká
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
skončil	skončit	k5eAaPmAgInS	skončit
finančním	finanční	k2eAgInSc7d1	finanční
nezdarem	nezdar	k1gInSc7	nezdar
<g/>
.	.	kIx.	.
</s>
<s>
Postavený	postavený	k2eAgInSc1d1	postavený
hangár	hangár	k1gInSc1	hangár
byl	být	k5eAaImAgInS	být
přebudován	přebudovat	k5eAaPmNgInS	přebudovat
na	na	k7c4	na
akvapark	akvapark	k1gInSc4	akvapark
Tropical	Tropical	k1gFnSc2	Tropical
Islands	Islandsa	k1gFnPc2	Islandsa
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
řídí	řídit	k5eAaImIp3nP	řídit
lidé	člověk	k1gMnPc1	člověk
pomocí	pomocí	k7c2	pomocí
moderní	moderní	k2eAgFnSc2d1	moderní
elektroniky	elektronika	k1gFnSc2	elektronika
a	a	k8xC	a
avioniky	avionika	k1gFnSc2	avionika
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
satelitní	satelitní	k2eAgFnSc2d1	satelitní
navigace	navigace	k1gFnSc2	navigace
a	a	k8xC	a
moderní	moderní	k2eAgFnSc2d1	moderní
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
plány	plán	k1gInPc1	plán
na	na	k7c4	na
přepravu	přeprava	k1gFnSc4	přeprava
některých	některý	k3yIgFnPc2	některý
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomocí	pomocí	k7c2	pomocí
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
speciálních	speciální	k2eAgFnPc2d1	speciální
nákladních	nákladní	k2eAgFnPc2d1	nákladní
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
zapřažených	zapřažený	k2eAgInPc2d1	zapřažený
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
do	do	k7c2	do
jakýchsi	jakýsi	k3yIgInPc2	jakýsi
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
vzducholodních	vzducholodní	k2eAgInPc2d1	vzducholodní
vlaků	vlak	k1gInPc2	vlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
širším	široký	k2eAgNnSc6d2	širší
použití	použití	k1gNnSc6	použití
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
pro	pro	k7c4	pro
pozorovací	pozorovací	k2eAgInPc4d1	pozorovací
účely	účel	k1gInPc4	účel
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
tamním	tamní	k2eAgInSc7d1	tamní
úřadem	úřad	k1gInSc7	úřad
Homeland	Homeland	k1gInSc1	Homeland
Security	Securita	k1gFnSc2	Securita
Office	Office	kA	Office
<g/>
.	.	kIx.	.
</s>
<s>
Nasazeny	nasazen	k2eAgFnPc1d1	nasazena
bývají	bývat	k5eAaImIp3nP	bývat
při	při	k7c6	při
výjimečných	výjimečný	k2eAgFnPc6d1	výjimečná
akcích	akce	k1gFnPc6	akce
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
shromáždit	shromáždit	k5eAaPmF	shromáždit
davy	dav	k1gInPc4	dav
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
koncert	koncert	k1gInSc1	koncert
Madonny	Madonna	k1gFnSc2	Madonna
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
nebo	nebo	k8xC	nebo
sjezd	sjezd	k1gInSc1	sjezd
republikánů	republikán	k1gMnPc2	republikán
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
prezidenta	prezident	k1gMnSc2	prezident
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
paralelně	paralelně	k6eAd1	paralelně
probíhající	probíhající	k2eAgInPc1d1	probíhající
protiválečné	protiválečný	k2eAgInPc1d1	protiválečný
protesty	protest	k1gInPc1	protest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovací	pozorovací	k2eAgFnSc1d1	pozorovací
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
testována	testován	k2eAgFnSc1d1	testována
např.	např.	kA	např.
nad	nad	k7c7	nad
Washingtonem	Washington	k1gInSc7	Washington
nebo	nebo	k8xC	nebo
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přímo	přímo	k6eAd1	přímo
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
pro	pro	k7c4	pro
pozorovací	pozorovací	k2eAgInPc4d1	pozorovací
účely	účel	k1gInPc4	účel
bylo	být	k5eAaImAgNnS	být
též	též	k9	též
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
pro	pro	k7c4	pro
americké	americký	k2eAgFnPc4d1	americká
a	a	k8xC	a
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
realizováno	realizovat	k5eAaBmNgNnS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
použití	použití	k1gNnSc4	použití
např.	např.	kA	např.
proti	proti	k7c3	proti
vrtulníkům	vrtulník	k1gInPc3	vrtulník
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
úspora	úspora	k1gFnSc1	úspora
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Neztužená	ztužený	k2eNgFnSc1d1	ztužený
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Parseval	Parseval	k1gFnSc1	Parseval
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
též	též	k9	též
"	"	kIx"	"
<g/>
blimp	blimp	k1gMnSc1	blimp
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
bez	bez	k7c2	bez
pevné	pevný	k2eAgFnSc2d1	pevná
kostry	kostra	k1gFnSc2	kostra
Poloztužená	Poloztužený	k2eAgFnSc1d1	Poloztužený
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
-	-	kIx~	-
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
kýlem	kýl	k1gInSc7	kýl
Ztužená	ztužený	k2eAgFnSc1d1	ztužená
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
kostrou	kostra	k1gFnSc7	kostra
celého	celý	k2eAgNnSc2d1	celé
tělesa	těleso	k1gNnSc2	těleso
Hybridní	hybridní	k2eAgFnSc1d1	hybridní
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
Václav	Václav	k1gMnSc1	Václav
Němeček	Němeček	k1gMnSc1	Němeček
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
-	-	kIx~	-
letadla	letadlo	k1gNnSc2	letadlo
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Karel	Karel	k1gMnSc1	Karel
Vrchovecký	Vrchovecký	k2eAgMnSc1d1	Vrchovecký
<g/>
,	,	kIx,	,
Lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Airships	Airshipsa	k1gFnPc2	Airshipsa
<g/>
:	:	kIx,	:
A	a	k8xC	a
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
History	Histor	k1gInPc4	Histor
Site	Sit	k1gInSc2	Sit
</s>
