<s>
Missourský	missourský	k2eAgInSc1d1
kompromis	kompromis	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
USA	USA	kA
v	v	k7c6
roce	rok	k1gInSc6
1820	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missourský	missourský	k2eAgInSc1d1
kompromis	kompromis	k1gInSc1
zakázal	zakázat	k5eAaPmAgInS
otrokářství	otrokářství	k1gNnPc4
v	v	k7c6
neorganizovaném	organizovaný	k2eNgNnSc6d1
teritoriu	teritorium	k1gNnSc6
Velkých	velký	k2eAgFnPc2d1
plání	pláň	k1gFnPc2
(	(	kIx(
<g/>
tmavozelená	tmavozelený	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
povolil	povolit	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
v	v	k7c4
Missouri	Missouri	k1gNnSc4
(	(	kIx(
<g/>
žlutá	žlutý	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
arkansaském	arkansaský	k2eAgNnSc6d1
teritoriu	teritorium	k1gNnSc6
(	(	kIx(
<g/>
modrá	modrý	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Missourský	missourský	k2eAgInSc4d1
kompromis	kompromis	k1gInSc4
byla	být	k5eAaImAgFnS
dohoda	dohoda	k1gFnSc1
přijata	přijat	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1820	#num#	k4
Kongresem	kongres	k1gInSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
zachovat	zachovat	k5eAaPmF
politickou	politický	k2eAgFnSc4d1
rovnováhu	rovnováha	k1gFnSc4
mezi	mezi	k7c7
jižními	jižní	k2eAgInPc7d1
otrokářskými	otrokářský	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
severními	severní	k2eAgInPc7d1
protiotrokářskými	protiotrokářský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc4
</s>
<s>
Koupě	koupě	k1gFnSc1
Louisiany	Louisiana	k1gFnSc2
(	(	kIx(
<g/>
1803	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
rozvinutým	rozvinutý	k2eAgInSc7d1
otrokářským	otrokářský	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Žádost	žádost	k1gFnSc1
Missouri	Missouri	k1gFnSc2
o	o	k7c4
přijetí	přijetí	k1gNnSc4
do	do	k7c2
svazku	svazek	k1gInSc2
USA	USA	kA
jako	jako	k8xS,k8xC
dalšího	další	k2eAgInSc2d1
otrokářského	otrokářský	k2eAgInSc2d1
státu	stát	k1gInSc2
podnítilo	podnítit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1818	#num#	k4
ostrý	ostrý	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
v	v	k7c6
politickém	politický	k2eAgInSc6d1
životě	život	k1gInSc6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Jednání	jednání	k1gNnSc1
</s>
<s>
Po	po	k7c6
dlouhých	dlouhý	k2eAgNnPc6d1
jednáních	jednání	k1gNnPc6
dospěl	dochvít	k5eAaPmAgInS
Kongres	kongres	k1gInSc1
ke	k	k7c3
kompromisu	kompromis	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
přijaty	přijat	k2eAgInPc1d1
dva	dva	k4xCgInPc1
státy	stát	k1gInPc1
-	-	kIx~
jeden	jeden	k4xCgInSc1
otrokářský	otrokářský	k2eAgInSc1d1
(	(	kIx(
<g/>
Missouri	Missouri	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
jeden	jeden	k4xCgInSc4
neotrokářský	otrokářský	k2eNgInSc4d1
(	(	kIx(
<g/>
Maine	Main	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
Louisiany	Louisiana	k1gFnSc2
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c4
jižní	jižní	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
otrokářství	otrokářství	k1gNnSc1
povoleno	povolen	k2eAgNnSc1d1
a	a	k8xC
na	na	k7c4
severní	severní	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
otrokářství	otrokářství	k1gNnSc1
zakázáno	zakázat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Následky	následek	k1gInPc1
</s>
<s>
Kompromis	kompromis	k1gInSc1
narazil	narazit	k5eAaPmAgInS
na	na	k7c4
odpor	odpor	k1gInSc4
plantážníků	plantážník	k1gMnPc2
v	v	k7c6
otrokářských	otrokářský	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
zákonem	zákon	k1gInSc7
o	o	k7c6
Kansasu	Kansas	k1gInSc6
a	a	k8xC
Nebrasce	Nebraska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Missourský	missourský	k2eAgInSc4d1
kompromis	kompromis	k1gInSc4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
