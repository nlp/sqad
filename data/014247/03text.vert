<s>
Bristol	Bristol	k1gInSc1
Siddeley	Siddelea	k1gFnSc2
Orpheus	Orpheus	k1gMnSc1
</s>
<s>
Orpheus	Orpheus	k1gInSc1
Typ	typ	k1gInSc1
</s>
<s>
Proudový	proudový	k2eAgInSc1d1
motor	motor	k1gInSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Bristol	Bristol	k1gInSc1
Siddeley	Siddelea	k1gFnSc2
První	první	k4xOgFnSc2
rozběh	rozběh	k1gInSc4
</s>
<s>
1957	#num#	k4
Hlavní	hlavní	k2eAgInSc4d1
použití	použití	k1gNnSc1
</s>
<s>
Fiat	fiat	k1gInSc1
G	G	kA
<g/>
.91	.91	k4
Folland	Folland	k1gInSc1
Gnat	Gnat	k1gInSc4
Fuji	Fuji	kA
T-1HAL	T-1HAL	k1gMnSc2
HF-24	HF-24	k1gMnSc2
Marut	Marut	k2eAgInSc1d1
Další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Rolls-Royce	Rolls-Royce	k1gMnSc1
Pegasus	Pegasus	k1gMnSc1
</s>
<s>
Bristol	Bristol	k1gInSc1
Siddeley	Siddelea	k1gInSc1
Orpheus	Orpheus	k1gInSc1
byl	být	k5eAaImAgMnS
jednohřídelový	jednohřídelový	k2eAgInSc1d1
proudový	proudový	k2eAgInSc1d1
motor	motor	k1gInSc1
vyvinutý	vyvinutý	k2eAgInSc1d1
společností	společnost	k1gFnSc7
Bristol	Bristol	k1gInSc1
Siddeley	Siddelea	k1gInSc1
pro	pro	k7c4
různé	různý	k2eAgInPc4d1
lehké	lehký	k2eAgInPc4d1
stíhací	stíhací	k2eAgInPc4d1
a	a	k8xC
cvičné	cvičný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
jako	jako	k8xS,k8xC
Folland	Folland	k1gInSc1
Gnat	Gnata	k1gInSc1
nebo	nebo	k8xC
Fiat	fiat	k1gInSc1
G	G	kA
<g/>
.91	.91	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
Orpheus	Orpheus	k1gInSc1
posloužil	posloužit	k5eAaPmAgInS
jako	jako	k9
jádro	jádro	k1gNnSc4
pro	pro	k7c4
první	první	k4xOgInSc4
dvouproudový	dvouproudový	k2eAgInSc4d1
motor	motor	k1gInSc4
s	s	k7c7
vektorováním	vektorování	k1gNnSc7
tahu	tah	k1gInSc2
-	-	kIx~
Rolls-Royce	Rolls-Royce	k1gMnSc1
Pegasus	Pegasus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
našel	najít	k5eAaPmAgMnS
uplatnění	uplatnění	k1gNnSc3
v	v	k7c6
sérii	série	k1gFnSc6
letounů	letoun	k1gInPc2
Harrier	Harrier	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Breguet	Breguet	k1gMnSc1
Taon	Taon	k1gMnSc1
</s>
<s>
Fiat	fiat	k1gInSc1
G	G	kA
<g/>
.91	.91	k4
</s>
<s>
Folland	Folland	k1gInSc1
Gnat	Gnata	k1gFnPc2
</s>
<s>
Fuji	Fuji	kA
T-1	T-1	k1gFnSc1
</s>
<s>
HAL	hala	k1gFnPc2
HF-24	HF-24	k1gMnSc1
Marut	Marut	k1gMnSc1
</s>
<s>
Helwan	Helwan	k1gMnSc1
HA-300	HA-300	k1gMnSc1
</s>
<s>
Hunting	Hunting	k1gInSc1
H	H	kA
<g/>
.126	.126	k4
</s>
<s>
Short	Short	k1gInSc1
SB	sb	kA
<g/>
.5	.5	k4
</s>
<s>
Specifikace	specifikace	k1gFnSc1
(	(	kIx(
<g/>
Orpheus	Orpheus	k1gInSc1
BOr	bor	k1gInSc1
<g/>
.3	.3	k4
/	/	kIx~
Mk	Mk	k1gFnSc1
<g/>
.803	.803	k4
<g/>
)	)	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
jednohřídelový	jednohřídelový	k2eAgInSc1d1
proudový	proudový	k2eAgInSc1d1
motor	motor	k1gInSc1
</s>
<s>
Průměr	průměr	k1gInSc1
<g/>
:	:	kIx,
823	#num#	k4
mm	mm	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
916	#num#	k4
mm	mm	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
suchého	suchý	k2eAgInSc2d1
motoru	motor	k1gInSc2
<g/>
:	:	kIx,
379	#num#	k4
kg	kg	kA
</s>
<s>
Součásti	součást	k1gFnPc1
</s>
<s>
Kompresor	kompresor	k1gInSc1
<g/>
:	:	kIx,
7	#num#	k4
<g/>
stupňový	stupňový	k2eAgInSc1d1
axiální	axiální	k2eAgInSc1d1
kompresor	kompresor	k1gInSc1
</s>
<s>
Spalovací	spalovací	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
:	:	kIx,
hybridní	hybridní	k2eAgNnSc1d1
se	s	k7c7
sedmi	sedm	k4xCc7
plamenci	plamenec	k1gInPc7
</s>
<s>
Turbína	turbína	k1gFnSc1
<g/>
:	:	kIx,
jednostupňová	jednostupňový	k2eAgFnSc1d1
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Maximální	maximální	k2eAgInSc1d1
tah	tah	k1gInSc1
<g/>
:	:	kIx,
22	#num#	k4
kN	kN	k?
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
poměr	poměr	k1gInSc1
stlačení	stlačení	k1gNnSc2
<g/>
:	:	kIx,
4,4	4,4	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Teplota	teplota	k1gFnSc1
plynů	plyn	k1gInPc2
před	před	k7c7
turbínou	turbína	k1gFnSc7
<g/>
:	:	kIx,
640	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
paliva	palivo	k1gNnSc2
<g/>
:	:	kIx,
110,1	110,1	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
kN	kN	k?
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Poměr	poměr	k1gInSc1
tah	tah	k1gInSc1
<g/>
/	/	kIx~
<g/>
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
0,0587	0,0587	k4
kN	kN	k?
<g/>
/	/	kIx~
<g/>
kg	kg	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bristol	Bristol	k1gInSc4
Siddeley	Siddelea	k1gFnSc2
Orpheus	Orpheus	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
