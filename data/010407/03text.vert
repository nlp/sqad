<p>
<s>
Návaznost	návaznost	k1gFnSc1	návaznost
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
biskupství	biskupství	k1gNnSc2	biskupství
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
biskupských	biskupský	k2eAgInPc2d1	biskupský
stolců	stolec	k1gInPc2	stolec
<g/>
,	,	kIx,	,
zřízených	zřízený	k2eAgInPc2d1	zřízený
po	po	k7c6	po
roce	rok	k1gInSc6	rok
898	[number]	k4	898
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
obnovení	obnovení	k1gNnSc2	obnovení
moravsko-panonské	moravskoanonský	k2eAgFnSc2d1	moravsko-panonský
církevní	církevní	k2eAgFnSc2d1	církevní
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
olomouckých	olomoucký	k2eAgInPc6d1	olomoucký
biskupech	biskup	k1gInPc6	biskup
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1063	[number]	k4	1063
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
útržkovité	útržkovitý	k2eAgFnPc1d1	útržkovitá
zmínky	zmínka	k1gFnPc1	zmínka
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
<g/>
,	,	kIx,	,
datace	datace	k1gFnSc1	datace
je	být	k5eAaImIp3nS	být
orientační	orientační	k2eAgFnSc1d1	orientační
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
seznamů	seznam	k1gInPc2	seznam
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
biskupů	biskup	k1gMnPc2	biskup
je	být	k5eAaImIp3nS	být
Granum	Granum	k1gNnSc1	Granum
catalogi	catalogi	k1gNnSc2	catalogi
praesulum	praesulum	k1gInSc4	praesulum
Moraviae	Moravia	k1gInSc2	Moravia
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biskupové	biskup	k1gMnPc1	biskup
před	před	k7c7	před
obnovením	obnovení	k1gNnSc7	obnovení
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
biskupství	biskupství	k1gNnSc2	biskupství
==	==	k?	==
</s>
</p>
<p>
<s>
869	[number]	k4	869
<g/>
–	–	k?	–
<g/>
885	[number]	k4	885
svatý	svatý	k1gMnSc1	svatý
Metoděj	Metoděj	k1gMnSc1	Metoděj
–	–	k?	–
moravský	moravský	k2eAgMnSc1d1	moravský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
</s>
</p>
<p>
<s>
biskup	biskup	k1gMnSc1	biskup
Wiching	Wiching	k1gInSc1	Wiching
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roku	rok	k1gInSc2	rok
890	[number]	k4	890
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
odešel	odejít	k5eAaPmAgInS	odejít
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
nepravděpodobný	pravděpodobný	k2eNgMnSc1d1	nepravděpodobný
Svatý	svatý	k2eAgMnSc1d1	svatý
Gorazd	Gorazd	k1gMnSc1	Gorazd
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
biskupové	biskup	k1gMnPc1	biskup
vysvěceni	vysvěcen	k2eAgMnPc1d1	vysvěcen
899	[number]	k4	899
</s>
</p>
<p>
<s>
898	[number]	k4	898
<g/>
/	/	kIx~	/
<g/>
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
?	?	kIx.	?
</s>
<s>
biskup	biskup	k1gMnSc1	biskup
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
</s>
</p>
<p>
<s>
914	[number]	k4	914
<g/>
–	–	k?	–
<g/>
932	[number]	k4	932
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
uprázdněno	uprázdněn	k2eAgNnSc1d1	uprázdněno
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
942	[number]	k4	942
<g/>
–	–	k?	–
<g/>
947	[number]	k4	947
Silvestr	Silvestr	k1gInSc1	Silvestr
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
†	†	k?	†
961	[number]	k4	961
</s>
</p>
<p>
<s>
947	[number]	k4	947
<g/>
–	–	k?	–
<g/>
976	[number]	k4	976
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
biskupstvím	biskupství	k1gNnSc7	biskupství
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
976	[number]	k4	976
<g/>
–	–	k?	–
<g/>
981	[number]	k4	981
Vratislav	Vratislava	k1gFnPc2	Vratislava
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
981	[number]	k4	981
<g/>
–	–	k?	–
<g/>
991	[number]	k4	991
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
biskupstvím	biskupství	k1gNnSc7	biskupství
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
991	[number]	k4	991
<g/>
–	–	k?	–
<g/>
1063	[number]	k4	1063
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
biskupstvím	biskupství	k1gNnSc7	biskupství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Olomoučtí	olomoucký	k2eAgMnPc1d1	olomoucký
biskupové	biskup	k1gMnPc1	biskup
==	==	k?	==
</s>
</p>
<p>
<s>
1063	[number]	k4	1063
<g/>
–	–	k?	–
<g/>
1085	[number]	k4	1085
Jan	Jan	k1gMnSc1	Jan
I	i	k9	i
<g/>
.1086	.1086	k4	.1086
<g/>
–	–	k?	–
<g/>
1088	[number]	k4	1088
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
biskupstvím	biskupství	k1gNnSc7	biskupství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
1088	[number]	k4	1088
<g/>
–	–	k?	–
<g/>
1091	[number]	k4	1091
Vezel	Vezel	k1gInSc1	Vezel
<g/>
,	,	kIx,	,
kaplan	kaplan	k1gMnSc1	kaplan
krále	král	k1gMnSc2	král
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1091	[number]	k4	1091
<g/>
–	–	k?	–
<g/>
1096	[number]	k4	1096
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
1096	[number]	k4	1096
<g/>
–	–	k?	–
<g/>
1099	[number]	k4	1099
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
1097	[number]	k4	1097
<g/>
/	/	kIx~	/
<g/>
1099	[number]	k4	1099
<g/>
–	–	k?	–
<g/>
1104	[number]	k4	1104
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
</s>
</p>
<p>
<s>
1104	[number]	k4	1104
<g/>
–	–	k?	–
<g/>
1126	[number]	k4	1126
Jan	Jana	k1gFnPc2	Jana
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1126	[number]	k4	1126
<g/>
–	–	k?	–
<g/>
1151	[number]	k4	1151
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
</s>
</p>
<p>
<s>
1151	[number]	k4	1151
<g/>
–	–	k?	–
<g/>
1157	[number]	k4	1157
Jan	Jana	k1gFnPc2	Jana
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1157	[number]	k4	1157
<g/>
–	–	k?	–
<g/>
1172	[number]	k4	1172
Jan	Jana	k1gFnPc2	Jana
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
z	z	k7c2	z
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
</s>
</p>
<p>
<s>
1172	[number]	k4	1172
<g/>
–	–	k?	–
<g/>
1182	[number]	k4	1182
Dětleb	Dětlba	k1gFnPc2	Dětlba
</s>
</p>
<p>
<s>
1182	[number]	k4	1182
<g/>
–	–	k?	–
<g/>
1184	[number]	k4	1184
Pelhřim	Pelhřima	k1gFnPc2	Pelhřima
</s>
</p>
<p>
<s>
1184	[number]	k4	1184
<g/>
–	–	k?	–
<g/>
1194	[number]	k4	1194
Kaim	Kaim	k1gInSc1	Kaim
</s>
</p>
<p>
<s>
1194	[number]	k4	1194
<g/>
–	–	k?	–
<g/>
1199	[number]	k4	1199
Engelbert	Engelbert	k1gInSc1	Engelbert
</s>
</p>
<p>
<s>
1199	[number]	k4	1199
<g/>
–	–	k?	–
<g/>
1201	[number]	k4	1201
Jan	Jana	k1gFnPc2	Jana
V.	V.	kA	V.
Bavor	Bavor	k1gMnSc1	Bavor
</s>
</p>
<p>
<s>
1201	[number]	k4	1201
<g/>
–	–	k?	–
<g/>
1240	[number]	k4	1240
Robert	Robert	k1gMnSc1	Robert
Angličan	Angličan	k1gMnSc1	Angličan
(	(	kIx(	(
<g/>
donucen	donutit	k5eAaPmNgMnS	donutit
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1241	[number]	k4	1241
<g/>
–	–	k?	–
<g/>
1245	[number]	k4	1245
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
zvolený	zvolený	k2eAgMnSc1d1	zvolený
biskup	biskup	k1gMnSc1	biskup
</s>
</p>
<p>
<s>
1241	[number]	k4	1241
<g/>
–	–	k?	–
<g/>
1245	[number]	k4	1245
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Friedberka	Friedberka	k1gFnSc1	Friedberka
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
biskup	biskup	k1gMnSc1	biskup
</s>
</p>
<p>
<s>
1245	[number]	k4	1245
<g/>
–	–	k?	–
<g/>
1281	[number]	k4	1281
Bruno	Bruno	k1gMnSc1	Bruno
ze	z	k7c2	z
Schauenburku	Schauenburk	k1gInSc2	Schauenburk
</s>
</p>
<p>
<s>
1281	[number]	k4	1281
<g/>
–	–	k?	–
<g/>
1302	[number]	k4	1302
Dětřich	Dětřich	k1gMnSc1	Dětřich
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
</s>
</p>
<p>
<s>
1302	[number]	k4	1302
<g/>
–	–	k?	–
<g/>
1311	[number]	k4	1311
Jan	Jana	k1gFnPc2	Jana
VI	VI	kA	VI
<g/>
.	.	kIx.	.
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
</s>
</p>
<p>
<s>
1311	[number]	k4	1311
<g/>
–	–	k?	–
<g/>
1316	[number]	k4	1316
Petr	Petr	k1gMnSc1	Petr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Andělův	Andělův	k2eAgInSc1d1	Andělův
(	(	kIx(	(
<g/>
Angeli	angel	k1gMnSc5	angel
<g/>
)	)	kIx)	)
de	de	k?	de
Ponte	Pont	k1gInSc5	Pont
Corvo	Corvo	k1gNnSc1	Corvo
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Bradavice	bradavice	k1gFnPc4	bradavice
</s>
</p>
<p>
<s>
1316	[number]	k4	1316
<g/>
–	–	k?	–
<g/>
1326	[number]	k4	1326
Konrád	Konrád	k1gMnSc1	Konrád
I.	I.	kA	I.
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Bavor	Bavor	k1gMnSc1	Bavor
</s>
</p>
<p>
<s>
1326	[number]	k4	1326
<g/>
–	–	k?	–
<g/>
1333	[number]	k4	1333
Jindřich	Jindřich	k1gMnSc1	Jindřich
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
</s>
</p>
<p>
<s>
1334	[number]	k4	1334
<g/>
–	–	k?	–
<g/>
1351	[number]	k4	1351
Jan	Jana	k1gFnPc2	Jana
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Volek	Volek	k1gMnSc1	Volek
(	(	kIx(	(
<g/>
nelegitimní	legitimní	k2eNgMnSc1d1	nelegitimní
Přemyslovec-nemanželský	Přemyslovecemanželský	k2eAgMnSc1d1	Přemyslovec-nemanželský
syn	syn	k1gMnSc1	syn
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1351	[number]	k4	1351
<g/>
–	–	k?	–
<g/>
1364	[number]	k4	1364
Jan	Jan	k1gMnSc1	Jan
Očko	očko	k1gNnSc4	očko
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
1364	[number]	k4	1364
<g/>
–	–	k?	–
<g/>
1378,30	[number]	k4	1378,30
<g/>
.11	.11	k4	.11
<g/>
.	.	kIx.	.
<g/>
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
,	,	kIx,	,
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1378	[number]	k4	1378
kardinál-první	kardinálrvnit	k5eAaPmIp3nS	kardinál-prvnit
Čech	Čech	k1gMnSc1	Čech
</s>
</p>
<p>
<s>
1364	[number]	k4	1364
<g/>
–	–	k?	–
<g/>
1380	[number]	k4	1380
Jan	Jana	k1gFnPc2	Jana
IX	IX	kA	IX
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Středy	středa	k1gFnSc2	středa
(	(	kIx(	(
<g/>
1353	[number]	k4	1353
<g/>
–	–	k?	–
<g/>
1364	[number]	k4	1364
biskup	biskup	k1gInSc1	biskup
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1381	[number]	k4	1381
<g/>
–	–	k?	–
<g/>
1387	[number]	k4	1387
Petr	Petr	k1gMnSc1	Petr
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Jelito	jelito	k1gNnSc1	jelito
(	(	kIx(	(
<g/>
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1371	[number]	k4	1371
biskup	biskup	k1gInSc1	biskup
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1387	[number]	k4	1387
Jan	Jan	k1gMnSc1	Jan
X.	X.	kA	X.
(	(	kIx(	(
<g/>
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1380	[number]	k4	1380
<g/>
–	–	k?	–
<g/>
1387	[number]	k4	1387
biskup	biskup	k1gInSc1	biskup
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
,	,	kIx,	,
1387	[number]	k4	1387
<g/>
–	–	k?	–
<g/>
1394	[number]	k4	1394
patriarcha	patriarcha	k1gMnSc1	patriarcha
akvilejský	akvilejský	k2eAgMnSc1d1	akvilejský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1388	[number]	k4	1388
<g/>
–	–	k?	–
<g/>
1397	[number]	k4	1397
Mikuláš	mikuláš	k1gInSc1	mikuláš
z	z	k7c2	z
Riesenburka	Riesenburek	k1gMnSc2	Riesenburek
</s>
</p>
<p>
<s>
1398	[number]	k4	1398
<g/>
–	–	k?	–
<g/>
1403	[number]	k4	1403
Jan	Jana	k1gFnPc2	Jana
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Mráz	mráz	k1gInSc1	mráz
</s>
</p>
<p>
<s>
1403	[number]	k4	1403
<g/>
–	–	k?	–
<g/>
1408	[number]	k4	1408
Lacek	Lacko	k1gNnPc2	Lacko
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
</s>
</p>
<p>
<s>
1409	[number]	k4	1409
<g/>
–	–	k?	–
<g/>
1412	[number]	k4	1412
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Vechty	Vechta	k1gFnSc2	Vechta
(	(	kIx(	(
<g/>
1412	[number]	k4	1412
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
1421	[number]	k4	1421
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
,	,	kIx,	,
<g/>
od	od	k7c2	od
1421	[number]	k4	1421
husitský-suspendován	husitskýuspendován	k2eAgMnSc1d1	husitský-suspendován
a	a	k8xC	a
1425	[number]	k4	1425
definitivně	definitivně	k6eAd1	definitivně
zbaven	zbaven	k2eAgMnSc1d1	zbaven
úřadu	úřad	k1gInSc2	úřad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1412	[number]	k4	1412
<g/>
–	–	k?	–
<g/>
1416	[number]	k4	1416
Václav	Václav	k1gMnSc1	Václav
Králík	Králík	k1gMnSc1	Králík
z	z	k7c2	z
Buřenic	Buřenice	k1gFnPc2	Buřenice
</s>
</p>
<p>
<s>
1416	[number]	k4	1416
<g/>
–	–	k?	–
<g/>
1430	[number]	k4	1430
kardinál	kardinál	k1gMnSc1	kardinál
Jan	Jan	k1gMnSc1	Jan
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Železný	železný	k2eAgMnSc1d1	železný
<g/>
1416	[number]	k4	1416
<g/>
–	–	k?	–
<g/>
1418	[number]	k4	1418
(	(	kIx(	(
<g/>
Aleš	Aleš	k1gMnSc1	Aleš
z	z	k7c2	z
Březí	březí	k1gNnSc2	březí
<g/>
)	)	kIx)	)
–	–	k?	–
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
ho	on	k3xPp3gNnSc4	on
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
biskupem	biskup	k1gInSc7	biskup
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nestal	stát	k5eNaPmAgInS	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
l	l	kA	l
<g/>
.1418	.1418	k4	.1418
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
1442	[number]	k4	1442
(	(	kIx(	(
<g/>
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
biskup	biskup	k1gMnSc1	biskup
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
1430	[number]	k4	1430
<g/>
–	–	k?	–
<g/>
1434	[number]	k4	1434
Kuneš	Kuneš	k1gMnSc1	Kuneš
ze	z	k7c2	z
Zvole	Zvole	k1gFnSc2	Zvole
(	(	kIx(	(
<g/>
Konrád	Konrád	k1gMnSc1	Konrád
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1434	[number]	k4	1434
<g/>
–	–	k?	–
<g/>
1450	[number]	k4	1450
Pavel	Pavel	k1gMnSc1	Pavel
z	z	k7c2	z
Miličína	Miličín	k1gInSc2	Miličín
</s>
</p>
<p>
<s>
1450	[number]	k4	1450
<g/>
–	–	k?	–
<g/>
1454	[number]	k4	1454
Jan	Jana	k1gFnPc2	Jana
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Johann	Johann	k1gInSc1	Johann
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Haz	Haz	k?	Haz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1454	[number]	k4	1454
<g/>
–	–	k?	–
<g/>
1457	[number]	k4	1457
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
ze	z	k7c2	z
Zvole	Zvole	k1gFnSc2	Zvole
</s>
</p>
<p>
<s>
1457	[number]	k4	1457
<g/>
–	–	k?	–
<g/>
1482	[number]	k4	1482
Tas	tasit	k5eAaPmRp2nS	tasit
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Protasius	Protasius	k1gInSc1	Protasius
<g/>
)	)	kIx)	)
<g/>
1483	[number]	k4	1483
<g/>
/	/	kIx~	/
<g/>
1484	[number]	k4	1484
<g/>
–	–	k?	–
<g/>
1490	[number]	k4	1490
Jan	Jan	k1gMnSc1	Jan
Filipec	Filipec	k1gMnSc1	Filipec
z	z	k7c2	z
Prostějova	Prostějov	k1gInSc2	Prostějov
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
biskupství	biskupství	k1gNnSc2	biskupství
nikdy	nikdy	k6eAd1	nikdy
nepotvrzený	potvrzený	k2eNgInSc1d1	nepotvrzený
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
varadínský	varadínský	k2eAgMnSc1d1	varadínský
biskup	biskup	k1gMnSc1	biskup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1487	[number]	k4	1487
<g/>
–	–	k?	–
<g/>
1489	[number]	k4	1489
Jan	Jana	k1gFnPc2	Jana
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Vitéz	Vitéza	k1gFnPc2	Vitéza
(	(	kIx(	(
<g/>
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
funkci	funkce	k1gFnSc4	funkce
nevykonával	vykonávat	k5eNaImAgInS	vykonávat
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
sremský	sremský	k2eAgInSc1d1	sremský
biskup	biskup	k1gInSc1	biskup
<g/>
)	)	kIx)	)
a	a	k8xC	a
administrátor	administrátor	k1gMnSc1	administrátor
diecéze	diecéze	k1gFnSc2	diecéze
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
</s>
</p>
<p>
<s>
1489	[number]	k4	1489
<g/>
–	–	k?	–
<g/>
1493	[number]	k4	1493
kardinál	kardinál	k1gMnSc1	kardinál
Ardicino	Ardicin	k2eAgNnSc4d1	Ardicin
della	della	k6eAd1	della
Porta	porta	k1gFnSc1	porta
mladší	mladý	k2eAgFnSc1d2	mladší
(	(	kIx(	(
<g/>
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc1	úřad
prakticky	prakticky	k6eAd1	prakticky
nevykonával	vykonávat	k5eNaImAgInS	vykonávat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1493	[number]	k4	1493
<g/>
–	–	k?	–
<g/>
1497	[number]	k4	1497
kardinál	kardinál	k1gMnSc1	kardinál
Jan	Jan	k1gMnSc1	Jan
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Borgia	Borgia	k1gFnSc1	Borgia
(	(	kIx(	(
<g/>
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc1	úřad
prakticky	prakticky	k6eAd1	prakticky
nevykonával	vykonávat	k5eNaImAgInS	vykonávat
<g/>
)	)	kIx)	)
<g/>
1497	[number]	k4	1497
<g/>
–	–	k?	–
<g/>
1540	[number]	k4	1540
Stanislav	Stanislava	k1gFnPc2	Stanislava
I.	I.	kA	I.
Thurzo	Thurza	k1gFnSc5	Thurza
</s>
</p>
<p>
<s>
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1541	[number]	k4	1541
Bernard	Bernard	k1gMnSc1	Bernard
Zoubek	Zoubek	k1gMnSc1	Zoubek
ze	z	k7c2	z
Zdětína	Zdětín	k1gInSc2	Zdětín
</s>
</p>
<p>
<s>
1541	[number]	k4	1541
<g/>
–	–	k?	–
<g/>
1553	[number]	k4	1553
Jan	Jana	k1gFnPc2	Jana
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Skála	Skála	k1gMnSc1	Skála
z	z	k7c2	z
Doubravky	Doubravka	k1gFnSc2	Doubravka
a	a	k8xC	a
Hradiště	Hradiště	k1gNnSc2	Hradiště
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Dubravius	Dubravius	k1gMnSc1	Dubravius
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Dubraw	Dubraw	k?	Dubraw
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1553	[number]	k4	1553
<g/>
–	–	k?	–
<g/>
1565	[number]	k4	1565
Marek	marka	k1gFnPc2	marka
Khuen	Khuna	k1gFnPc2	Khuna
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
</s>
</p>
<p>
<s>
1565	[number]	k4	1565
<g/>
–	–	k?	–
<g/>
1572	[number]	k4	1572
Vilém	Vilém	k1gMnSc1	Vilém
Prusinovský	Prusinovský	k2eAgMnSc1d1	Prusinovský
z	z	k7c2	z
Víckova	Víckův	k2eAgInSc2d1	Víckův
</s>
</p>
<p>
<s>
1572	[number]	k4	1572
<g/>
–	–	k?	–
<g/>
1574	[number]	k4	1574
Jan	Jana	k1gFnPc2	Jana
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
</s>
<s>
Grodecký	Grodecký	k2eAgInSc1d1	Grodecký
</s>
</p>
<p>
<s>
1574	[number]	k4	1574
<g/>
–	–	k?	–
<g/>
1575	[number]	k4	1575
Tomáš	Tomáš	k1gMnSc1	Tomáš
Albín	Albín	k1gMnSc1	Albín
z	z	k7c2	z
Helfenburka	Helfenburek	k1gMnSc2	Helfenburek
</s>
</p>
<p>
<s>
1576	[number]	k4	1576
<g/>
–	–	k?	–
<g/>
1578	[number]	k4	1578
Jan	Jana	k1gFnPc2	Jana
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
</s>
<s>
Mezoun	Mezoun	k1gInSc1	Mezoun
z	z	k7c2	z
Telče	Telč	k1gFnSc2	Telč
</s>
</p>
<p>
<s>
1579	[number]	k4	1579
<g/>
–	–	k?	–
<g/>
1599	[number]	k4	1599
Stanislav	Stanislava	k1gFnPc2	Stanislava
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pavlovský	pavlovský	k2eAgInSc1d1	pavlovský
</s>
</p>
<p>
<s>
1599	[number]	k4	1599
<g/>
–	–	k?	–
<g/>
1636	[number]	k4	1636
kardinál	kardinál	k1gMnSc1	kardinál
František	František	k1gMnSc1	František
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
Dietrichsteina	Dietrichsteino	k1gNnSc2	Dietrichsteino
</s>
</p>
<p>
<s>
1636	[number]	k4	1636
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
Jan	Jana	k1gFnPc2	Jana
XIX	XIX	kA	XIX
<g/>
.	.	kIx.	.
</s>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Plattenštejna	Plattenštejn	k1gInSc2	Plattenštejn
</s>
</p>
<p>
<s>
1638	[number]	k4	1638
<g/>
–	–	k?	–
<g/>
1662	[number]	k4	1662
Leopold	Leopolda	k1gFnPc2	Leopolda
I.	I.	kA	I.
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
biskupské	biskupský	k2eAgNnSc4d1	Biskupské
svěcení	svěcení	k1gNnSc4	svěcení
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
biskupství	biskupství	k1gNnSc2	biskupství
spravovali	spravovat	k5eAaImAgMnP	spravovat
následující	následující	k2eAgMnPc1d1	následující
administrátoři	administrátor	k1gMnPc1	administrátor
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
kanovníků	kanovník	k1gMnPc2	kanovník
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1638	[number]	k4	1638
Ondřej	Ondřej	k1gMnSc1	Ondřej
Orlík	orlík	k1gMnSc1	orlík
z	z	k7c2	z
Laziska	Lazisko	k1gNnSc2	Lazisko
(	(	kIx(	(
<g/>
kapitulní	kapitulní	k2eAgMnSc1d1	kapitulní
děkan	děkan	k1gMnSc1	děkan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1638	[number]	k4	1638
<g/>
–	–	k?	–
<g/>
1640	[number]	k4	1640
Kašpar	Kašpar	k1gMnSc1	Kašpar
Karas	Karas	k1gMnSc1	Karas
z	z	k7c2	z
Rhomsteinu	Rhomstein	k1gInSc2	Rhomstein
</s>
</p>
<p>
<s>
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1642	[number]	k4	1642
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
Stredele	Stredel	k1gInSc2	Stredel
z	z	k7c2	z
Montani	Montaň	k1gFnSc6	Montaň
a	a	k8xC	a
Bergenu	Bergen	k1gInSc6	Bergen
(	(	kIx(	(
<g/>
pomocný	pomocný	k2eAgMnSc1d1	pomocný
biskup	biskup	k1gMnSc1	biskup
pasovský	pasovský	k1gMnSc1	pasovský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1643	[number]	k4	1643
<g/>
–	–	k?	–
<g/>
1646	[number]	k4	1646
Kašpar	Kašpar	k1gMnSc1	Kašpar
Karas	Karas	k1gMnSc1	Karas
z	z	k7c2	z
Rhomsteinu	Rhomstein	k1gInSc2	Rhomstein
</s>
</p>
<p>
<s>
1642	[number]	k4	1642
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
Roderich	Roderich	k1gInSc1	Roderich
Santhiller	Santhiller	k1gInSc4	Santhiller
(	(	kIx(	(
<g/>
Saint-Hilaire	Saint-Hilair	k1gMnSc5	Saint-Hilair
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1650	[number]	k4	1650
<g/>
–	–	k?	–
<g/>
1653	[number]	k4	1653
Jakub	Jakub	k1gMnSc1	Jakub
Merkurián	Merkurián	k1gMnSc1	Merkurián
</s>
</p>
<p>
<s>
1654	[number]	k4	1654
<g/>
–	–	k?	–
<g/>
1664	[number]	k4	1664
František	František	k1gMnSc1	František
Eliáš	Eliáš	k1gMnSc1	Eliáš
Kastelle	Kastelle	k1gInSc4	Kastelle
</s>
</p>
<p>
<s>
1663	[number]	k4	1663
<g/>
–	–	k?	–
<g/>
1664	[number]	k4	1664
Karel	Karla	k1gFnPc2	Karla
I.	I.	kA	I.
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
</s>
</p>
<p>
<s>
1664	[number]	k4	1664
<g/>
–	–	k?	–
<g/>
1695	[number]	k4	1695
Karel	Karla	k1gFnPc2	Karla
II	II	kA	II
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Lichtenštejna-Kastelkornu	Lichtenštejna-Kastelkorn	k1gInSc2	Lichtenštejna-Kastelkorn
</s>
</p>
<p>
<s>
1695	[number]	k4	1695
<g/>
–	–	k?	–
<g/>
1711	[number]	k4	1711
Karel	Karla	k1gFnPc2	Karla
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
</s>
</p>
<p>
<s>
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1738	[number]	k4	1738
kardinál	kardinál	k1gMnSc1	kardinál
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Hannibal	Hannibal	k1gInSc4	Hannibal
von	von	k1gInSc1	von
Schrattenbach	Schrattenbach	k1gInSc1	Schrattenbach
(	(	kIx(	(
<g/>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Hanibal	Hanibal	k1gMnSc1	Hanibal
hrabě	hrabě	k1gMnSc1	hrabě
Schrattenbach	Schrattenbach	k1gMnSc1	Schrattenbach
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1738	[number]	k4	1738
<g/>
–	–	k?	–
<g/>
1745	[number]	k4	1745
Jakub	Jakub	k1gMnSc1	Jakub
Arnošt	Arnošt	k1gMnSc1	Arnošt
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Lichtenštejna-Kastelkornu	Lichtenštejna-Kastelkorn	k1gInSc2	Lichtenštejna-Kastelkorn
</s>
</p>
<p>
<s>
1745	[number]	k4	1745
<g/>
–	–	k?	–
<g/>
1758	[number]	k4	1758
kardinál	kardinál	k1gMnSc1	kardinál
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Julius	Julius	k1gMnSc1	Julius
hrabě	hrabě	k1gMnSc1	hrabě
Troyer	Troyer	k1gMnSc1	Troyer
z	z	k7c2	z
Troyersteinu	Troyerstein	k1gInSc2	Troyerstein
</s>
</p>
<p>
<s>
1758	[number]	k4	1758
<g/>
–	–	k?	–
<g/>
1760	[number]	k4	1760
Leopold	Leopolda	k1gFnPc2	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Egkhu	Egkh	k1gInSc2	Egkh
a	a	k8xC	a
Hungersbachu	Hungersbach	k1gInSc2	Hungersbach
</s>
</p>
<p>
<s>
1761	[number]	k4	1761
<g/>
–	–	k?	–
<g/>
1776	[number]	k4	1776
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Hamiltonu	Hamilton	k1gInSc2	Hamilton
</s>
</p>
<p>
<s>
==	==	k?	==
Olomoučtí	olomoucký	k2eAgMnPc1d1	olomoucký
arcibiskupové	arcibiskup	k1gMnPc1	arcibiskup
==	==	k?	==
</s>
</p>
<p>
<s>
1777	[number]	k4	1777
<g/>
–	–	k?	–
<g/>
1811	[number]	k4	1811
kardinál	kardinál	k1gMnSc1	kardinál
Antonín	Antonín	k1gMnSc1	Antonín
Theodor	Theodor	k1gMnSc1	Theodor
Colloredo-Waldsee	Colloredo-Waldsee	k1gFnSc1	Colloredo-Waldsee
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
</s>
</p>
<p>
<s>
1811	[number]	k4	1811
<g/>
–	–	k?	–
<g/>
1819	[number]	k4	1819
kardinál	kardinál	k1gMnSc1	kardinál
Maria	Mario	k1gMnSc2	Mario
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Trauttmansdorff	Trauttmansdorff	k1gMnSc1	Trauttmansdorff
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
</s>
</p>
<p>
<s>
1819	[number]	k4	1819
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
kardinál	kardinál	k1gMnSc1	kardinál
Rudolf	Rudolf	k1gMnSc1	Rudolf
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
</s>
</p>
<p>
<s>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Maria	Maria	k1gFnSc1	Maria
Chotek	Chotek	k1gMnSc1	Chotek
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
(	(	kIx(	(
<g/>
od	od	k7c2	od
1817	[number]	k4	1817
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
pomocný	pomocný	k2eAgInSc1d1	pomocný
biskup	biskup	k1gInSc1	biskup
<g/>
,	,	kIx,	,
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
biskup	biskup	k1gMnSc1	biskup
tarnówský	tarnówský	k1gMnSc1	tarnówský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1836	[number]	k4	1836
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
kardinál	kardinál	k1gMnSc1	kardinál
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Josef	Josef	k1gMnSc1	Josef
Sommerau-Beckh	Sommerau-Beckh	k1gMnSc1	Sommerau-Beckh
</s>
</p>
<p>
<s>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
kardinál	kardinál	k1gMnSc1	kardinál
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Fürstenberka	Fürstenberka	k1gFnSc1	Fürstenberka
<g/>
,	,	kIx,	,
lankrabě	lankrabě	k1gMnSc1	lankrabě
</s>
</p>
<p>
<s>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
Theodor	Theodora	k1gFnPc2	Theodora
Kohn	Kohna	k1gFnPc2	Kohna
(	(	kIx(	(
<g/>
přinucen	přinucen	k2eAgInSc1d1	přinucen
k	k	k7c3	k
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
kardinál	kardinál	k1gMnSc1	kardinál
František	František	k1gMnSc1	František
Saleský	Saleský	k2eAgMnSc1d1	Saleský
Bauer	Bauer	k1gMnSc1	Bauer
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
jmenován	jmenovat	k5eAaImNgInS	jmenovat
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1904	[number]	k4	1904
intronizován	intronizován	k2eAgInSc1d1	intronizován
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1911	[number]	k4	1911
kreován	kreovat	k5eAaBmNgInS	kreovat
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1915	[number]	k4	1915
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
kardinál	kardinál	k1gMnSc1	kardinál
Lev	Lev	k1gMnSc1	Lev
Skrbenský	Skrbenský	k2eAgMnSc1d1	Skrbenský
z	z	k7c2	z
Hříště	hříště	k1gNnSc2	hříště
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1916	[number]	k4	1916
zvolen	zvolit	k5eAaPmNgMnS	zvolit
kapitulou	kapitula	k1gFnSc7	kapitula
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1916	[number]	k4	1916
přeložen	přeložit	k5eAaPmNgInS	přeložit
z	z	k7c2	z
pražského	pražský	k2eAgNnSc2d1	Pražské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
intronizován	intronizován	k2eAgInSc1d1	intronizován
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1920	[number]	k4	1920
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
Antonín	Antonín	k1gMnSc1	Antonín
Cyril	Cyril	k1gMnSc1	Cyril
Stojan	stojan	k1gInSc1	stojan
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1921	[number]	k4	1921
jmenován	jmenovat	k5eAaImNgInS	jmenovat
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1921	[number]	k4	1921
intronizován	intronizován	k2eAgInSc1d1	intronizován
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1923	[number]	k4	1923
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
Leopold	Leopold	k1gMnSc1	Leopold
Prečan	Prečan	k1gMnSc1	Prečan
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1923	[number]	k4	1923
jmenován	jmenovat	k5eAaImNgInS	jmenovat
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1923	[number]	k4	1923
intronizován	intronizován	k2eAgInSc1d1	intronizován
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1947	[number]	k4	1947
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
Josef	Josef	k1gMnSc1	Josef
Karel	Karel	k1gMnSc1	Karel
Matocha	Matoch	k1gMnSc2	Matoch
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
jmenován	jmenovat	k5eAaImNgInS	jmenovat
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
intronizován	intronizován	k2eAgInSc1d1	intronizován
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1961	[number]	k4	1961
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
v	v	k7c6	v
internaci	internace	k1gFnSc6	internace
a	a	k8xC	a
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
vykonávat	vykonávat	k5eAaImF	vykonávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
biskup	biskup	k1gMnSc1	biskup
Josef	Josef	k1gMnSc1	Josef
Vrana	Vrana	k1gFnSc1	Vrana
(	(	kIx(	(
<g/>
apoštolský	apoštolský	k2eAgMnSc1d1	apoštolský
administrátor	administrátor	k1gMnSc1	administrátor
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
Mons	Monsa	k1gFnPc2	Monsa
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Vaňák	Vaňák	k1gMnSc1	Vaňák
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
intronizován	intronizován	k2eAgInSc1d1	intronizován
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1991	[number]	k4	1991
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
od	od	k7c2	od
1992	[number]	k4	1992
Mons	Monsa	k1gFnPc2	Monsa
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Graubner	Graubner	k1gMnSc1	Graubner
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1992	[number]	k4	1992
jmenován	jmenovat	k5eAaImNgInS	jmenovat
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
intronizován	intronizován	k2eAgInSc1d1	intronizován
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Pomocní	pomocní	k2eAgMnPc1d1	pomocní
biskupové	biskup	k1gMnPc1	biskup
olomoučtí	olomoucký	k2eAgMnPc1d1	olomoucký
==	==	k?	==
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
1304	[number]	k4	1304
<g/>
–	–	k?	–
<g/>
1324	[number]	k4	1324
františkán	františkán	k1gMnSc1	františkán
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
</s>
</p>
<p>
<s>
Dětřich	Dětřich	k1gMnSc1	Dětřich
z	z	k7c2	z
Portic	Portice	k1gFnPc2	Portice
(	(	kIx(	(
<g/>
Theodoricus	Theodoricus	k1gMnSc1	Theodoricus
Kagelwit	Kagelwit	k1gMnSc1	Kagelwit
<g/>
)	)	kIx)	)
1347	[number]	k4	1347
cisterciák	cisterciák	k1gMnSc1	cisterciák
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
1442	[number]	k4	1442
<g/>
–	–	k?	–
<g/>
1482	[number]	k4	1482
augustinián-eremita	augustiniánremita	k1gFnSc1	augustinián-eremita
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Wismann	Wismann	k1gMnSc1	Wismann
1482	[number]	k4	1482
<g/>
–	–	k?	–
<g/>
1501	[number]	k4	1501
augustinián-eremita	augustiniánremita	k1gFnSc1	augustinián-eremita
</s>
</p>
<p>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
Altheimer	Altheimer	k1gMnSc1	Altheimer
z	z	k7c2	z
Vaserburgu	Vaserburg	k1gInSc2	Vaserburg
1498	[number]	k4	1498
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Göschl	Göschl	k1gMnSc1	Göschl
1509	[number]	k4	1509
<g/>
–	–	k?	–
<g/>
1529	[number]	k4	1529
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Přemyslovic	Přemyslovice	k1gFnPc2	Přemyslovice
OPraem	OPra	k1gInSc7	OPra
1547	[number]	k4	1547
<g/>
–	–	k?	–
<g/>
1549	[number]	k4	1549
opat	opat	k1gMnSc1	opat
na	na	k7c6	na
Hradisku	hradisko	k1gNnSc6	hradisko
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
1551	[number]	k4	1551
<g/>
–	–	k?	–
<g/>
1561	[number]	k4	1561
cisterciák	cisterciák	k1gMnSc1	cisterciák
<g/>
,	,	kIx,	,
opat	opat	k1gMnSc1	opat
žďárský	žďárský	k2eAgMnSc1d1	žďárský
</s>
</p>
<p>
<s>
Melchior	Melchior	k1gMnSc1	Melchior
Pyrnesius	Pyrnesius	k1gMnSc1	Pyrnesius
z	z	k7c2	z
Pyrnu	Pyrn	k1gInSc2	Pyrn
1603	[number]	k4	1603
<g/>
–	–	k?	–
<g/>
1607	[number]	k4	1607
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
Civalli	Civalle	k1gFnSc4	Civalle
1608	[number]	k4	1608
<g/>
–	–	k?	–
<g/>
1617	[number]	k4	1617
</s>
</p>
<p>
<s>
Hynek	Hynek	k1gMnSc1	Hynek
Jindřich	Jindřich	k1gMnSc1	Jindřich
Novohradský	novohradský	k2eAgMnSc1d1	novohradský
z	z	k7c2	z
Kolovrat	kolovrat	k1gInSc1	kolovrat
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1628	[number]	k4	1628
</s>
</p>
<p>
<s>
Caesar	Caesar	k1gMnSc1	Caesar
Nardus	nardus	k1gInSc4	nardus
z	z	k7c2	z
Montopole	Montopole	k1gFnSc2	Montopole
<g/>
,	,	kIx,	,
1629	[number]	k4	1629
<g/>
–	–	k?	–
<g/>
1632	[number]	k4	1632
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
Fridrich	Fridrich	k1gMnSc1	Fridrich
Breuner	Breuner	k1gMnSc1	Breuner
<g/>
,	,	kIx,	,
pozd	pozd	k1gMnSc1	pozd
<g/>
.	.	kIx.	.
biskup	biskup	k1gMnSc1	biskup
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
1630	[number]	k4	1630
<g/>
–	–	k?	–
<g/>
1639	[number]	k4	1639
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
Stredele	Stredel	k1gInSc2	Stredel
z	z	k7c2	z
Montani	Montaň	k1gFnSc6	Montaň
a	a	k8xC	a
Bergenu	Bergen	k1gInSc6	Bergen
(	(	kIx(	(
<g/>
pomocný	pomocný	k2eAgMnSc1d1	pomocný
biskup	biskup	k1gMnSc1	biskup
pasovský	pasovský	k1gMnSc1	pasovský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1637	[number]	k4	1637
<g/>
–	–	k?	–
<g/>
1642	[number]	k4	1642
</s>
</p>
<p>
<s>
Kašpar	Kašpar	k1gMnSc1	Kašpar
Karas	Karas	k1gMnSc1	Karas
z	z	k7c2	z
Rhomsteinu	Rhomstein	k1gInSc2	Rhomstein
<g/>
,	,	kIx,	,
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1646	[number]	k4	1646
</s>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Miutini	Miutin	k2eAgMnPc1d1	Miutin
ze	z	k7c2	z
Spillimbergu	Spillimberg	k1gInSc2	Spillimberg
1639	[number]	k4	1639
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Gobbar	Gobbar	k1gMnSc1	Gobbar
1652	[number]	k4	1652
<g/>
–	–	k?	–
<g/>
1665	[number]	k4	1665
<g/>
,	,	kIx,	,
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
<g/>
.1665	.1665	k4	.1665
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Bredimus	Bredimus	k1gMnSc1	Bredimus
1665	[number]	k4	1665
<g/>
–	–	k?	–
<g/>
1668	[number]	k4	1668
nepotvrzen	potvrzen	k2eNgInSc4d1	nepotvrzen
Římem	Řím	k1gInSc7	Řím
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Dirre	Dirr	k1gInSc5	Dirr
1668	[number]	k4	1668
<g/>
–	–	k?	–
<g/>
1669	[number]	k4	1669
†	†	k?	†
21.11	[number]	k4	21.11
<g/>
.	.	kIx.	.
1669	[number]	k4	1669
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Josef	Josef	k1gMnSc1	Josef
Breuner	Breuner	k1gMnSc1	Breuner
1670	[number]	k4	1670
<g/>
–	–	k?	–
<g/>
1695	[number]	k4	1695
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
(	(	kIx(	(
<g/>
1695	[number]	k4	1695
<g/>
–	–	k?	–
<g/>
1710	[number]	k4	1710
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
von	von	k1gInSc4	von
Losenstein	Losenstein	k1gMnSc1	Losenstein
(	(	kIx(	(
<g/>
koadjutor	koadjutor	k1gMnSc1	koadjutor
<g/>
)	)	kIx)	)
1690	[number]	k4	1690
<g/>
–	–	k?	–
<g/>
1692	[number]	k4	1692
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Schröffel	Schröffel	k1gMnSc1	Schröffel
ze	z	k7c2	z
Schröffenheimu	Schröffenheim	k1gInSc2	Schröffenheim
1696	[number]	k4	1696
<g/>
–	–	k?	–
<g/>
1702	[number]	k4	1702
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Julián	Julián	k1gMnSc1	Julián
von	von	k1gInSc4	von
Braida	Braida	k1gFnSc1	Braida
1703	[number]	k4	1703
<g/>
–	–	k?	–
<g/>
1727	[number]	k4	1727
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Honorius	Honorius	k1gMnSc1	Honorius
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Egkhu	Egkh	k1gInSc2	Egkh
a	a	k8xC	a
Hungersbachu	Hungersbach	k1gInSc2	Hungersbach
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1748	[number]	k4	1748
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Karel	Karel	k1gMnSc1	Karel
Leopold	Leopold	k1gMnSc1	Leopold
von	von	k1gInSc4	von
Scherffenberg	Scherffenberg	k1gInSc1	Scherffenberg
1749	[number]	k4	1749
<g/>
–	–	k?	–
<g/>
1771	[number]	k4	1771
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Xaver	Xaver	k1gMnSc1	Xaver
Frey	Frea	k1gFnSc2	Frea
von	von	k1gInSc1	von
Freyenfels	Freyenfels	k1gInSc1	Freyenfels
1771	[number]	k4	1771
<g/>
–	–	k?	–
<g/>
1776	[number]	k4	1776
</s>
</p>
<p>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
František	František	k1gMnSc1	František
Chorinský	Chorinský	k2eAgMnSc1d1	Chorinský
z	z	k7c2	z
Ledské	Ledská	k1gFnSc2	Ledská
(	(	kIx(	(
<g/>
1775	[number]	k4	1775
<g/>
–	–	k?	–
<g/>
1777	[number]	k4	1777
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
pomocný	pomocný	k2eAgMnSc1d1	pomocný
biskup	biskup	k1gMnSc1	biskup
královéhradecký	královéhradecký	k2eAgMnSc1d1	královéhradecký
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
první	první	k4xOgInSc4	první
biskup	biskup	k1gInSc4	biskup
brněnský	brněnský	k2eAgInSc4d1	brněnský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Godefried	Godefried	k1gMnSc1	Godefried
von	von	k1gInSc4	von
Rosenthal	Rosenthal	k1gMnSc1	Rosenthal
1779	[number]	k4	1779
<g/>
–	–	k?	–
<g/>
1800	[number]	k4	1800
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Josef	Josef	k1gMnSc1	Josef
Krakovský	krakovský	k2eAgMnSc1d1	krakovský
z	z	k7c2	z
Kolovrat	kolovrat	k1gInSc1	kolovrat
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
–	–	k?	–
<g/>
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1812	[number]	k4	1812
biskup	biskup	k1gInSc4	biskup
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Maria	Mario	k1gMnSc2	Mario
Chotek	Chotek	k1gMnSc1	Chotek
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
biskup	biskup	k1gMnSc1	biskup
tarnovský	tarnovský	k2eAgMnSc1d1	tarnovský
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
von	von	k1gInSc4	von
Thyssebaert	Thyssebaert	k1gInSc1	Thyssebaert
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Gindl	Gindl	k1gMnSc1	Gindl
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
sídelní	sídelní	k2eAgMnSc1d1	sídelní
biskup	biskup	k1gMnSc1	biskup
brněnský	brněnský	k2eAgMnSc1d1	brněnský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Josef	Josef	k1gMnSc1	Josef
Schrenk	Schrenk	k1gMnSc1	Schrenk
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1838	[number]	k4	1838
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Arnošt	Arnošt	k1gMnSc1	Arnošt
Schaffgotsche	Schaffgotsche	k1gFnSc1	Schaffgotsche
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1841	[number]	k4	1841
biskup	biskup	k1gInSc1	biskup
brněnský	brněnský	k2eAgInSc1d1	brněnský
</s>
</p>
<p>
<s>
Ignaz	Ignaz	k1gInSc1	Ignaz
Feigerle	Feigerle	k1gFnSc2	Feigerle
(	(	kIx(	(
<g/>
do	do	k7c2	do
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
de	de	k?	de
Belrupt-Tyssac	Belrupt-Tyssac	k1gInSc1	Belrupt-Tyssac
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Weinlich	Weinlich	k1gMnSc1	Weinlich
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Wisnar	Wisnar	k1gMnSc1	Wisnar
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
bisk	bisk	k1gInSc1	bisk
<g/>
.	.	kIx.	.
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Blažek	Blažek	k1gMnSc1	Blažek
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Schinzel	Schinzel	k1gMnSc1	Schinzel
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Stavěl	stavět	k5eAaImAgMnS	stavět
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Zela	zet	k5eAaImAgFnS	zet
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
†	†	k?	†
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
vězněn	vězněn	k2eAgInSc4d1	vězněn
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
internaci	internace	k1gFnSc6	internace
bez	bez	k7c2	bez
státního	státní	k2eAgInSc2d1	státní
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Martin	Martin	k1gMnSc1	Martin
Nathan	Nathan	k1gMnSc1	Nathan
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Tomášek	Tomášek	k1gMnSc1	Tomášek
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Graubner	Graubner	k1gMnSc1	Graubner
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Hrdlička	Hrdlička	k1gMnSc1	Hrdlička
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Nuzík	Nuzík	k1gMnSc1	Nuzík
(	(	kIx(	(
<g/>
od	od	k7c2	od
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Basler	Basler	k1gMnSc1	Basler
(	(	kIx(	(
<g/>
od	od	k7c2	od
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ZUBER	ZUBER	kA	ZUBER
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Osudy	osud	k1gInPc1	osud
moravské	moravský	k2eAgFnSc2d1	Moravská
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
církevní	církevní	k2eAgNnSc1d1	církevní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
292	[number]	k4	292
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ZUBER	ZUBER	kA	ZUBER
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Osudy	osud	k1gInPc1	osud
moravské	moravský	k2eAgFnSc2d1	Moravská
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
605	[number]	k4	605
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7266	[number]	k4	7266
<g/>
-	-	kIx~	-
<g/>
156	[number]	k4	156
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Gottwald	Gottwald	k1gMnSc1	Gottwald
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Sulovský	Sulovský	k1gMnSc1	Sulovský
(	(	kIx(	(
<g/>
reds	reds	k1gInSc1	reds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
pořadů	pořad	k1gInPc2	pořad
ČRo	ČRo	k1gFnSc2	ČRo
Olomouc	Olomouc	k1gFnSc1	Olomouc
r.	r.	kA	r.
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
MCM	MCM	kA	MCM
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Seznam	seznam	k1gInSc1	seznam
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
arcibiskupů	arcibiskup	k1gMnPc2	arcibiskup
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
biskupové	biskup	k1gMnPc1	biskup
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
současní	současný	k2eAgMnPc1d1	současný
biskupové	biskup	k1gMnPc1	biskup
</s>
</p>
<p>
<s>
data	datum	k1gNnPc1	datum
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
biskupů	biskup	k1gMnPc2	biskup
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
