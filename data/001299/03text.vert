<s>
Arsen	arsen	k1gInSc1	arsen
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
As	as	k1gNnSc2	as
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Arsenicum	Arsenicum	k1gInSc1	Arsenicum
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
al-zarnī	alarnī	k?	al-zarnī
-	-	kIx~	-
zlatavá	zlatavý	k2eAgFnSc1d1	zlatavá
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
název	název	k1gInSc1	název
Arzén	arzén	k1gInSc1	arzén
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
toxický	toxický	k2eAgInSc1d1	toxický
polokovový	polokovový	k2eAgInSc1d1	polokovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
současné	současný	k2eAgNnSc1d1	současné
uplatnění	uplatnění	k1gNnSc1	uplatnění
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
metalurgie	metalurgie	k1gFnSc2	metalurgie
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
a	a	k8xC	a
v	v	k7c6	v
polovodičovém	polovodičový	k2eAgInSc6d1	polovodičový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Polokovový	polokovový	k2eAgInSc1d1	polokovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
sloučeninách	sloučenina	k1gFnPc6	sloučenina
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
<g/>
:	:	kIx,	:
As-	As-	k1gFnSc1	As-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
As	as	k1gInSc1	as
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
a	a	k8xC	a
As	as	k1gInSc1	as
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
arsen	arsen	k1gInSc1	arsen
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
barevných	barevný	k2eAgFnPc6d1	barevná
allotropních	allotropní	k2eAgFnPc6d1	allotropní
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
:	:	kIx,	:
žlutý	žlutý	k2eAgInSc4d1	žlutý
<g/>
,	,	kIx,	,
šedý	šedý	k2eAgInSc4d1	šedý
<g/>
,	,	kIx,	,
hnědý	hnědý	k2eAgInSc4d1	hnědý
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
arsen	arsen	k1gInSc4	arsen
<g/>
.	.	kIx.	.
</s>
<s>
Toxické	toxický	k2eAgFnPc1d1	toxická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
sloučenin	sloučenina	k1gFnPc2	sloučenina
arsenu	arsen	k1gInSc2	arsen
byly	být	k5eAaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
prvku	prvek	k1gInSc2	prvek
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
středověký	středověký	k2eAgMnSc1d1	středověký
učenec	učenec	k1gMnSc1	učenec
sv.	sv.	kA	sv.
Albert	Albert	k1gMnSc1	Albert
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
poprvé	poprvé	k6eAd1	poprvé
izoloval	izolovat	k5eAaBmAgMnS	izolovat
elementární	elementární	k2eAgInSc4d1	elementární
arsen	arsen	k1gInSc4	arsen
<g/>
.	.	kIx.	.
</s>
<s>
Arsen	arsen	k1gInSc1	arsen
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
značně	značně	k6eAd1	značně
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
-	-	kIx~	-
5	[number]	k4	5
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
mimořádně	mimořádně	k6eAd1	mimořádně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,003	[number]	k4	0,003
mg	mg	kA	mg
As	as	k1gNnSc2	as
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
arsenu	arsen	k1gInSc2	arsen
přibližně	přibližně	k6eAd1	přibližně
miliarda	miliarda	k4xCgFnSc1	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
rudou	ruda	k1gFnSc7	ruda
arsenu	arsen	k1gInSc2	arsen
je	být	k5eAaImIp3nS	být
směsný	směsný	k2eAgInSc1d1	směsný
sulfid	sulfid	k1gInSc1	sulfid
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
arsenu	arsen	k1gInSc2	arsen
<g/>
,	,	kIx,	,
arsenopyrit	arsenopyrit	k1gInSc1	arsenopyrit
(	(	kIx(	(
<g/>
FeAsS	FeAsS	k1gFnSc1	FeAsS
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
löllingit	löllingit	k2eAgInSc1d1	löllingit
(	(	kIx(	(
<g/>
FeAs	FeAs	k1gInSc1	FeAs
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
sulfidy	sulfid	k1gInPc4	sulfid
arsenu	arsen	k1gInSc2	arsen
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
realgar	realgar	k1gInSc4	realgar
<g/>
,	,	kIx,	,
AsS	AsS	k1gFnSc4	AsS
a	a	k8xC	a
auripigment	auripigment	k1gInSc4	auripigment
As	as	k1gNnSc2	as
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
antimonu	antimon	k1gInSc2	antimon
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
jako	jako	k8xS	jako
stopová	stopový	k2eAgFnSc1d1	stopová
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ložiscích	ložisko	k1gNnPc6	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
elementárního	elementární	k2eAgInSc2d1	elementární
arsenu	arsen	k1gInSc2	arsen
z	z	k7c2	z
arsenopyritu	arsenopyrit	k1gInSc2	arsenopyrit
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
pražení	pražení	k1gNnSc6	pražení
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
zachycování	zachycování	k1gNnSc6	zachycování
těkavého	těkavý	k2eAgInSc2d1	těkavý
oxidu	oxid	k1gInSc2	oxid
arsenitého	arsenitý	k2eAgInSc2d1	arsenitý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
surovinu	surovina	k1gFnSc4	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
arsenu	arsen	k1gInSc2	arsen
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
popel	popel	k1gInSc1	popel
uhlí	uhlí	k1gNnSc2	uhlí
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
výskytem	výskyt	k1gInSc7	výskyt
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
čistý	čistý	k2eAgInSc1d1	čistý
arsen	arsen	k1gInSc1	arsen
pro	pro	k7c4	pro
polovodičové	polovodičový	k2eAgNnSc4d1	polovodičové
použití	použití	k1gNnSc4	použití
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
především	především	k6eAd1	především
metodou	metoda	k1gFnSc7	metoda
zonálního	zonální	k2eAgNnSc2d1	zonální
tavení	tavení	k1gNnSc2	tavení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
křemík	křemík	k1gInSc1	křemík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maximum	maximum	k1gNnSc1	maximum
současného	současný	k2eAgInSc2d1	současný
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
využití	využití	k1gNnSc4	využití
arsenu	arsen	k1gInSc2	arsen
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
arsenid	arsenid	k1gInSc1	arsenid
gallia	gallia	k1gFnSc1	gallia
<g/>
,	,	kIx,	,
GaAs	GaAs	k1gInSc1	GaAs
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vynikající	vynikající	k2eAgFnPc4d1	vynikající
polovodičové	polovodičový	k2eAgFnPc4d1	polovodičová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
výrobní	výrobní	k2eAgFnSc4d1	výrobní
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
speciálních	speciální	k2eAgFnPc2d1	speciální
elektrotechnických	elektrotechnický	k2eAgFnPc2d1	elektrotechnická
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
mimořádně	mimořádně	k6eAd1	mimořádně
nízká	nízký	k2eAgFnSc1d1	nízká
úroveň	úroveň	k1gFnSc1	úroveň
šumu	šum	k1gInSc2	šum
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Dotování	dotování	k1gNnSc1	dotování
krystalů	krystal	k1gInPc2	krystal
superčistého	superčistý	k2eAgInSc2d1	superčistý
křemíku	křemík	k1gInSc2	křemík
přesným	přesný	k2eAgNnSc7d1	přesné
množstvím	množství	k1gNnSc7	množství
atomů	atom	k1gInPc2	atom
arsenu	arsen	k1gInSc2	arsen
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
polovodič	polovodič	k1gInSc1	polovodič
typu	typ	k1gInSc2	typ
N	N	kA	N
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
součástí	součást	k1gFnPc2	součást
všech	všecek	k3xTgInPc2	všecek
tranzistorů	tranzistor	k1gInPc2	tranzistor
a	a	k8xC	a
tak	tak	k6eAd1	tak
i	i	k8xC	i
všech	všecek	k3xTgInPc2	všecek
současných	současný	k2eAgInPc2d1	současný
počítačových	počítačový	k2eAgInPc2d1	počítačový
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
okrajově	okrajově	k6eAd1	okrajově
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
olovem	olovo	k1gNnSc7	olovo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
arsenu	arsen	k1gInSc2	arsen
kolem	kolem	k7c2	kolem
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnSc1	sloužící
jako	jako	k8xC	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
broků	brok	k1gInPc2	brok
a	a	k8xC	a
střeliva	střelivo	k1gNnSc2	střelivo
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
nejznámější	známý	k2eAgInSc1d3	nejznámější
oxid	oxid	k1gInSc1	oxid
arsenitý	arsenitý	k2eAgInSc1d1	arsenitý
As	as	k9	as
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
arsenik	arsenik	k1gInSc1	arsenik
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
toxická	toxický	k2eAgFnSc1d1	toxická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
odpradávna	odpradávna	k6eAd1	odpradávna
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
jed	jed	k1gInSc1	jed
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
nástrah	nástraha	k1gFnPc2	nástraha
na	na	k7c4	na
hlodavce	hlodavec	k1gMnSc4	hlodavec
nebo	nebo	k8xC	nebo
lovu	lov	k1gInSc3	lov
kožešinové	kožešinový	k2eAgFnSc2d1	kožešinová
zvěře	zvěř	k1gFnSc2	zvěř
v	v	k7c6	v
arktických	arktický	k2eAgFnPc6d1	arktická
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
také	také	k9	také
častým	častý	k2eAgInSc7d1	častý
nástrojem	nástroj	k1gInSc7	nástroj
travičů	travič	k1gMnPc2	travič
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Heleně	Helena	k1gFnSc6	Helena
postupně	postupně	k6eAd1	postupně
tráven	trávit	k5eAaImNgInS	trávit
právě	právě	k9	právě
tímto	tento	k3xDgInSc7	tento
jedem	jed	k1gInSc7	jed
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
arsenik	arsenik	k1gInSc4	arsenik
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
toleranci	tolerance	k1gFnSc4	tolerance
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
hemopoetický	hemopoetický	k2eAgInSc1d1	hemopoetický
prostředek	prostředek	k1gInSc1	prostředek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
otravy	otrava	k1gFnSc2	otrava
považuje	považovat	k5eAaImIp3nS	považovat
spíš	spíš	k9	spíš
antimon	antimon	k1gInSc1	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
oxid	oxid	k1gInSc1	oxid
arsenitý	arsenitý	k2eAgInSc1d1	arsenitý
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
velmi	velmi	k6eAd1	velmi
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
jako	jako	k9	jako
primární	primární	k2eAgInSc1d1	primární
standard	standard	k1gInSc1	standard
pro	pro	k7c4	pro
oxidimetrické	oxidimetrický	k2eAgFnPc4d1	oxidimetrický
titrace	titrace	k1gFnPc4	titrace
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
titru	titr	k1gInSc2	titr
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
jako	jako	k8xC	jako
manganistan	manganistan	k1gInSc1	manganistan
draselný	draselný	k2eAgInSc1d1	draselný
nebo	nebo	k8xC	nebo
jod	jod	k1gInSc1	jod
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
arsenitý	arsenitý	k2eAgInSc4d1	arsenitý
As	as	k1gInSc4	as
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
dobře	dobře	k6eAd1	dobře
kryjící	kryjící	k2eAgInSc4d1	kryjící
barevný	barevný	k2eAgInSc4d1	barevný
pigment	pigment	k1gInSc4	pigment
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
královská	královský	k2eAgFnSc1d1	královská
žluť	žluť	k1gFnSc1	žluť
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
arsen	arsen	k1gInSc1	arsen
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xS	jako
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
kovový	kovový	k2eAgInSc1d1	kovový
arsen	arsen	k1gInSc1	arsen
je	být	k5eAaImIp3nS	být
netoxický	toxický	k2eNgInSc1d1	netoxický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
je	být	k5eAaImIp3nS	být
však	však	k9	však
metabolizován	metabolizován	k2eAgInSc1d1	metabolizován
na	na	k7c4	na
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
arsenitý	arsenitý	k2eAgInSc4d1	arsenitý
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgFnPc1d1	akutní
otravy	otrava	k1gFnPc1	otrava
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
,	,	kIx,	,
průjmy	průjem	k1gInPc7	průjem
<g/>
,	,	kIx,	,
svalovými	svalový	k2eAgFnPc7d1	svalová
křečemi	křeč	k1gFnPc7	křeč
<g/>
,	,	kIx,	,
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
a	a	k8xC	a
zástavou	zástava	k1gFnSc7	zástava
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
As	as	k1gInSc1	as
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
AsCl	AsCl	k1gInSc1	AsCl
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
AsF	AsF	k1gFnSc1	AsF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
toxičtější	toxický	k2eAgFnPc1d2	toxičtější
než	než	k8xS	než
sloučeniny	sloučenina	k1gFnPc1	sloučenina
pětivazného	pětivazný	k2eAgInSc2d1	pětivazný
arsenu	arsen	k1gInSc2	arsen
<g/>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
látky	látka	k1gFnPc4	látka
mutagenní	mutagenní	k2eAgFnPc4d1	mutagenní
<g/>
,	,	kIx,	,
teratogenní	teratogenní	k2eAgFnPc4d1	teratogenní
a	a	k8xC	a
karcinogenní	karcinogenní	k2eAgFnPc4d1	karcinogenní
<g/>
.	.	kIx.	.
</s>
<s>
As	as	k1gNnSc1	as
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
As	as	k1gInSc1	as
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
netoxické	toxický	k2eNgFnPc1d1	netoxická
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
okolním	okolní	k2eAgNnSc6d1	okolní
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
nízkou	nízký	k2eAgFnSc7d1	nízká
hladinou	hladina	k1gFnSc7	hladina
expozice	expozice	k1gFnSc2	expozice
arsenem	arsen	k1gInSc7	arsen
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
organizmus	organizmus	k1gInSc1	organizmus
nijak	nijak	k6eAd1	nijak
nepoškozuje	poškozovat	k5eNaImIp3nS	poškozovat
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
naopak	naopak	k6eAd1	naopak
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc4d1	nízká
dávky	dávka	k1gFnPc4	dávka
arsenu	arsen	k1gInSc2	arsen
v	v	k7c6	v
přijímané	přijímaný	k2eAgFnSc6d1	přijímaná
potravě	potrava	k1gFnSc6	potrava
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgNnSc4d1	důležité
a	a	k8xC	a
prospěšné	prospěšný	k2eAgNnSc4d1	prospěšné
<g/>
.	.	kIx.	.
</s>
<s>
Bezesporu	bezesporu	k9	bezesporu
je	být	k5eAaImIp3nS	být
však	však	k9	však
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trvalé	trvalý	k2eAgNnSc4d1	trvalé
vystavení	vystavení	k1gNnSc4	vystavení
organizmu	organizmus	k1gInSc2	organizmus
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
dávkám	dávka	k1gFnPc3	dávka
sloučenin	sloučenina	k1gFnPc2	sloučenina
arsenu	arsen	k1gInSc2	arsen
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc1	projev
trvalé	trvalý	k2eAgFnSc2d1	trvalá
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
expozice	expozice	k1gFnSc2	expozice
arsenem	arsen	k1gInSc7	arsen
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
jsou	být	k5eAaImIp3nP	být
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
:	:	kIx,	:
dermatologické	dermatologický	k2eAgNnSc1d1	dermatologické
poškození	poškození	k1gNnSc1	poškození
-	-	kIx~	-
změny	změna	k1gFnPc1	změna
na	na	k7c6	na
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
různých	různý	k2eAgInPc2d1	různý
ekzémů	ekzém	k1gInPc2	ekzém
a	a	k8xC	a
alergické	alergický	k2eAgFnSc2d1	alergická
dermatitidy	dermatitida	k1gFnSc2	dermatitida
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
výskyt	výskyt	k1gInSc1	výskyt
kardiovaskulárních	kardiovaskulární	k2eAgFnPc2d1	kardiovaskulární
chorob	choroba	k1gFnPc2	choroba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
výskyt	výskyt	k1gInSc1	výskyt
potratů	potrat	k1gInPc2	potrat
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
trvale	trvale	k6eAd1	trvale
vystavených	vystavená	k1gFnPc2	vystavená
vysokým	vysoký	k2eAgFnPc3d1	vysoká
dávkám	dávka	k1gFnPc3	dávka
arsenu	arsen	k1gInSc2	arsen
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
karcinogenita	karcinogenita	k1gFnSc1	karcinogenita
-	-	kIx~	-
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
výskyt	výskyt	k1gInSc1	výskyt
případů	případ	k1gInPc2	případ
rakoviny	rakovina	k1gFnSc2	rakovina
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
pokožky	pokožka	k1gFnSc2	pokožka
mutagenita	mutagenita	k1gFnSc1	mutagenita
-	-	kIx~	-
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
výskyt	výskyt	k1gInSc1	výskyt
novorozenců	novorozenec	k1gMnPc2	novorozenec
s	s	k7c7	s
vrozenými	vrozený	k2eAgFnPc7d1	vrozená
vadami	vada	k1gFnPc7	vada
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
různých	různý	k2eAgInPc2d1	různý
způsobů	způsob	k1gInPc2	způsob
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
expozice	expozice	k1gFnSc2	expozice
arsenem	arsen	k1gInSc7	arsen
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
metalurgických	metalurgický	k2eAgInPc2d1	metalurgický
závodů	závod	k1gInPc2	závod
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
barevných	barevný	k2eAgInPc2d1	barevný
kovů	kov	k1gInPc2	kov
bývá	bývat	k5eAaImIp3nS	bývat
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
koncentrace	koncentrace	k1gFnSc1	koncentrace
arsenu	arsen	k1gInSc2	arsen
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
při	při	k7c6	při
masivním	masivní	k2eAgNnSc6d1	masivní
spalování	spalování	k1gNnSc6	spalování
uhlí	uhlí	k1gNnSc2	uhlí
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
arsenu	arsen	k1gInSc2	arsen
např.	např.	kA	např.
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
nebo	nebo	k8xC	nebo
výtopnách	výtopna	k1gFnPc6	výtopna
<g/>
.	.	kIx.	.
</s>
<s>
Vdechování	vdechování	k1gNnSc1	vdechování
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
částeček	částečka	k1gFnPc2	částečka
(	(	kIx(	(
<g/>
aerosolů	aerosol	k1gInPc2	aerosol
<g/>
)	)	kIx)	)
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
arsenu	arsen	k1gInSc2	arsen
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
riziku	riziko	k1gNnSc3	riziko
vzniku	vznik	k1gInSc2	vznik
plicní	plicní	k2eAgFnSc2d1	plicní
rakoviny	rakovina	k1gFnSc2	rakovina
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
množství	množství	k1gNnSc4	množství
potratů	potrat	k1gInPc2	potrat
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
hutí	huť	k1gFnPc2	huť
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
arsenu	arsen	k1gInSc2	arsen
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
vede	vést	k5eAaImIp3nS	vést
nejčastěji	často	k6eAd3	často
k	k	k7c3	k
dermatologickým	dermatologický	k2eAgInPc3d1	dermatologický
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
desítky	desítka	k1gFnPc1	desítka
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
nuceny	nucen	k2eAgFnPc1d1	nucena
pít	pít	k5eAaImF	pít
vodu	voda	k1gFnSc4	voda
ze	z	k7c2	z
studní	studna	k1gFnPc2	studna
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
minerální	minerální	k2eAgFnPc1d1	minerální
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
sloučeniny	sloučenina	k1gFnPc1	sloučenina
arsenu	arsen	k1gInSc2	arsen
z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
podloží	podloží	k1gNnSc2	podloží
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
arsenu	arsen	k1gInSc2	arsen
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k6eAd1	až
stovek	stovka	k1gFnPc2	stovka
miligramů	miligram	k1gInPc2	miligram
v	v	k7c6	v
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
příjmu	příjem	k1gInSc2	příjem
arsenu	arsen	k1gInSc2	arsen
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mořské	mořský	k2eAgFnPc1d1	mořská
ryby	ryba	k1gFnPc1	ryba
z	z	k7c2	z
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
koncentraci	koncentrace	k1gFnSc3	koncentrace
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
lidská	lidský	k2eAgFnSc1d1	lidská
aktivita	aktivita	k1gFnSc1	aktivita
(	(	kIx(	(
<g/>
vypouštění	vypouštění	k1gNnSc1	vypouštění
závadných	závadný	k2eAgFnPc2d1	závadná
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
i	i	k9	i
podmořská	podmořský	k2eAgFnSc1d1	podmořská
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Protilátka	protilátka	k1gFnSc1	protilátka
<g/>
:	:	kIx,	:
dimerkaprol	dimerkaprol	k1gInSc1	dimerkaprol
Asi	asi	k9	asi
450	[number]	k4	450
vesničanů	vesničan	k1gMnPc2	vesničan
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
arzenem	arzen	k1gInSc7	arzen
přiotrávilo	přiotrávit	k5eAaPmAgNnS	přiotrávit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
látka	látka	k1gFnSc1	látka
unikla	uniknout	k5eAaPmAgFnS	uniknout
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
2008	[number]	k4	2008
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
z	z	k7c2	z
úložiště	úložiště	k1gNnSc2	úložiště
odpadu	odpad	k1gInSc2	odpad
státní	státní	k2eAgFnSc2d1	státní
továrny	továrna	k1gFnSc2	továrna
společnosti	společnost	k1gFnSc2	společnost
Liuzhou	Liuzha	k1gMnSc7	Liuzha
China	China	k1gFnSc1	China
Tin	Tina	k1gFnPc2	Tina
Group	Group	k1gMnSc1	Group
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgMnSc7	třetí
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
cínu	cín	k1gInSc2	cín
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
byl	být	k5eAaImAgMnS	být
příčinou	příčina	k1gFnSc7	příčina
neštěstí	neštěstí	k1gNnSc2	neštěstí
silný	silný	k2eAgInSc4d1	silný
dešť	dešť	k1gInSc4	dešť
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgInSc6	který
přetekl	přetéct	k5eAaPmAgInS	přetéct
obsah	obsah	k1gInSc1	obsah
špatně	špatně	k6eAd1	špatně
zabezpečeného	zabezpečený	k2eAgNnSc2d1	zabezpečené
úložiště	úložiště	k1gNnSc2	úložiště
do	do	k7c2	do
místní	místní	k2eAgFnSc2d1	místní
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
pili	pít	k5eAaImAgMnP	pít
vodu	voda	k1gFnSc4	voda
kontaminovanou	kontaminovaný	k2eAgFnSc4d1	kontaminovaná
arzenem	arzen	k1gInSc7	arzen
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
postižených	postižený	k1gMnPc2	postižený
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
hospitalizaci	hospitalizace	k1gFnSc6	hospitalizace
propuštěna	propustit	k5eAaPmNgFnS	propustit
do	do	k7c2	do
domácí	domácí	k2eAgFnSc2d1	domácí
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
hospitalizováno	hospitalizovat	k5eAaBmNgNnS	hospitalizovat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Galerie	galerie	k1gFnSc2	galerie
arsen	arsen	k1gInSc4	arsen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
arsen	arsen	k1gInSc4	arsen
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
<g />
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
arsen	arsen	k1gInSc1	arsen
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pertold	Pertold	k1gMnSc1	Pertold
<g/>
:	:	kIx,	:
Arzen	arzen	k1gInSc1	arzen
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
-	-	kIx~	-
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
i	i	k8xC	i
jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
arzenu	arzen	k1gInSc2	arzen
a	a	k8xC	a
zpù	zpù	k?	zpù
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
jej	on	k3xPp3gMnSc4	on
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
<g/>
,	,	kIx,	,
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
77	[number]	k4	77
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
arsen	arsen	k1gInSc1	arsen
<g />
.	.	kIx.	.
</s>
<s>
Arnika	arnika	k1gFnSc1	arnika
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Toxické	toxický	k2eAgFnPc1d1	toxická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
arsenu	arsen	k1gInSc2	arsen
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Látka	látka	k1gFnSc1	látka
<g/>
:	:	kIx,	:
Arzen	arzen	k1gInSc1	arzen
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
As	as	k9	as
<g/>
)	)	kIx)	)
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
registr	registr	k1gInSc1	registr
znečišťování	znečišťování	k1gNnSc2	znečišťování
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
WHO	WHO	kA	WHO
<g/>
:	:	kIx,	:
Arsenic	Arsenice	k1gFnPc2	Arsenice
in	in	k?	in
drinking	drinking	k1gInSc1	drinking
water	water	k1gInSc1	water
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
International	International	k1gFnSc1	International
Agency	Agenca	k1gFnSc2	Agenca
for	forum	k1gNnPc2	forum
Research	Research	k1gInSc4	Research
on	on	k3xPp3gMnSc1	on
Cancer	Cancer	k1gMnSc1	Cancer
<g/>
:	:	kIx,	:
ARSENIC	ARSENIC	kA	ARSENIC
AND	Anda	k1gFnPc2	Anda
ARSENIC	ARSENIC	kA	ARSENIC
COMPOUNDS	COMPOUNDS	kA	COMPOUNDS
-	-	kIx~	-
Summaries	Summaries	k1gMnSc1	Summaries
&	&	k?	&
Evaluations	Evaluations	k1gInSc1	Evaluations
</s>
