<s>
Not	nota	k1gFnPc2
safe	safe	k1gInSc4
for	forum	k1gNnPc2
work	work	k6eAd1
</s>
<s>
Not	nota	k1gFnPc2
safe	safe	k1gInSc4
for	forum	k1gNnPc2
work	work	k6eAd1
(	(	kIx(
<g/>
NSFW	NSFW	kA
<g/>
,	,	kIx,
česky	česky	k6eAd1
není	být	k5eNaImIp3nS
bezpečné	bezpečný	k2eAgNnSc1d1
pro	pro	k7c4
práci	práce	k1gFnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
internetová	internetový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
k	k	k7c3
označování	označování	k1gNnSc3
URL	URL	kA
adres	adresa	k1gFnPc2
a	a	k8xC
hypertextových	hypertextový	k2eAgInPc2d1
odkazů	odkaz	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
odkazují	odkazovat	k5eAaImIp3nP
na	na	k7c4
videa	video	k1gNnPc4
nebo	nebo	k8xC
webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
nahotu	nahota	k1gFnSc4
<g/>
,	,	kIx,
pornografii	pornografie	k1gFnSc4
<g/>
,	,	kIx,
vulgarity	vulgarita	k1gFnPc4
<g/>
,	,	kIx,
násilí	násilí	k1gNnSc4
nebo	nebo	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
jiný	jiný	k2eAgInSc4d1
potenciálně	potenciálně	k6eAd1
rušivý	rušivý	k2eAgInSc4d1
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
jehož	jehož	k3xOyRp3gNnSc6
sledování	sledování	k1gNnSc6
si	se	k3xPyFc3
divák	divák	k1gMnSc1
nemusí	muset	k5eNaImIp3nS
přát	přát	k5eAaImF
být	být	k5eAaImF
viděn	vidět	k5eAaImNgMnS
ve	v	k7c6
veřejném	veřejný	k2eAgNnSc6d1
nebo	nebo	k8xC
formálním	formální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
pracovišti	pracoviště	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
škole	škola	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
rodinném	rodinný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
NSFW	NSFW	kA
má	mít	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgInSc4d1
význam	význam	k1gInSc4
pro	pro	k7c4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
o	o	k7c4
osobní	osobní	k2eAgNnSc4d1
používání	používání	k1gNnSc4
internetu	internet	k1gInSc2
na	na	k7c6
pracovištích	pracoviště	k1gNnPc6
nebo	nebo	k8xC
ve	v	k7c6
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
platí	platit	k5eAaImIp3nP
zásady	zásada	k1gFnPc4
zakazující	zakazující	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
sexuálním	sexuální	k2eAgNnPc3d1
a	a	k8xC
násilným	násilný	k2eAgNnPc3d1
tématům	téma	k1gNnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podobný	podobný	k2eAgInSc1d1
výraz	výraz	k1gInSc1
<g/>
,	,	kIx,
not	nota	k1gFnPc2
safe	safe	k1gInSc1
for	forum	k1gNnPc2
life	lif	k1gFnSc2
(	(	kIx(
<g/>
NSFL	NSFL	kA
<g/>
,	,	kIx,
česky	česky	k6eAd1
není	být	k5eNaImIp3nS
bezpečné	bezpečný	k2eAgNnSc1d1
pro	pro	k7c4
život	život	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
pro	pro	k7c4
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
rušivý	rušivý	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
způsobit	způsobit	k5eAaPmF
psychické	psychický	k2eAgNnSc4d1
trauma	trauma	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odkazy	odkaz	k1gInPc7
označené	označený	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
NSFL	NSFL	kA
mohou	moct	k5eAaImIp3nP
obsahovat	obsahovat	k5eAaImF
například	například	k6eAd1
zobrazení	zobrazení	k1gNnSc1
smrtelného	smrtelný	k2eAgNnSc2d1
násilí	násilí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Not	nota	k1gFnPc2
safe	safe	k1gInSc1
for	forum	k1gNnPc2
work	work	k6eAd1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
CHEN	CHEN	kA
<g/>
,	,	kIx,
Alice	Alice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MyEnglishTeacher	MyEnglishTeachra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
eu	eu	k?
Blog	Blog	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-29	2017-12-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
How	How	k1gFnSc2
Not	nota	k1gFnPc2
To	to	k9
Get	Get	k1gMnSc1
Caught	Caughta	k1gFnPc2
Looking	Looking	k1gInSc4
at	at	k?
NSFW	NSFW	kA
Content	Content	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Job	Job	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Complex	Complex	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PATRINOS	PATRINOS	kA
<g/>
,	,	kIx,
Thalia	thalium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Not	nota	k1gFnPc2
Safe	safe	k1gInSc4
For	forum	k1gNnPc2
Life	Lif	k1gMnSc2
(	(	kIx(
<g/>
NSFL	NSFL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medium	medium	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-27	2017-07-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
