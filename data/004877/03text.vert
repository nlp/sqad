<s>
Horečka	horečka	k1gFnSc1	horečka
Lassa	Lass	k1gMnSc2	Lass
je	být	k5eAaImIp3nS	být
závažné	závažný	k2eAgNnSc1d1	závažné
nakažlivé	nakažlivý	k2eAgNnSc1d1	nakažlivé
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
hemoragické	hemoragický	k2eAgFnSc2d1	hemoragická
(	(	kIx(	(
<g/>
krvácivé	krvácivý	k2eAgFnSc2d1	krvácivá
<g/>
)	)	kIx)	)
horečky	horečka	k1gFnSc2	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
se	se	k3xPyFc4	se
po	po	k7c6	po
místě	místo	k1gNnSc6	místo
objevu	objev	k1gInSc2	objev
–	–	k?	–
první	první	k4xOgMnSc1	první
popsaný	popsaný	k2eAgInSc1d1	popsaný
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
v	v	k7c6	v
městě	město	k1gNnSc6	město
Lassa	Lassa	k1gFnSc1	Lassa
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Čadského	čadský	k2eAgNnSc2d1	Čadské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
nemoc	nemoc	k1gFnSc4	nemoc
zemřely	zemřít	k5eAaPmAgFnP	zemřít
dvě	dva	k4xCgFnPc1	dva
misionářky	misionářka	k1gFnPc1	misionářka
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
Lassa	Lass	k1gMnSc2	Lass
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
primitivní	primitivní	k2eAgInPc4d1	primitivní
arenaviry	arenavir	k1gInPc4	arenavir
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Nigérie	Nigérie	k1gFnSc2	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Lassa	Lassa	k1gFnSc1	Lassa
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
podstatné	podstatný	k2eAgInPc4d1	podstatný
důvody	důvod	k1gInPc4	důvod
úmrtí	úmrtí	k1gNnSc2	úmrtí
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
%	%	kIx~	%
případu	případ	k1gInSc2	případ
má	mít	k5eAaImIp3nS	mít
Lassa	Lass	k1gMnSc4	Lass
mírný	mírný	k2eAgInSc1d1	mírný
až	až	k8xS	až
nepozorovatelný	pozorovatelný	k2eNgInSc1d1	nepozorovatelný
průběh	průběh	k1gInSc1	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
zbylých	zbylý	k2eAgNnPc2d1	zbylé
20	[number]	k4	20
%	%	kIx~	%
případů	případ	k1gInPc2	případ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vážnou	vážný	k2eAgFnSc4d1	vážná
chorobu	choroba	k1gFnSc4	choroba
postihující	postihující	k2eAgFnSc4d1	postihující
skoro	skoro	k6eAd1	skoro
všechny	všechen	k3xTgInPc4	všechen
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
