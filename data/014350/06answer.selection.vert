<s desamb="1">
Počátky	počátek	k1gInPc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
masového	masový	k2eAgNnSc2d1
používání	používání	k1gNnSc2
k	k	k7c3
tisku	tisk	k1gInSc3
knih	kniha	k1gFnPc2
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
na	na	k7c4
přelom	přelom	k1gInSc4
let	rok	k1gNnPc2
1447	#num#	k4
<g/>
/	/	kIx~
<g/>
1448	#num#	k4
a	a	k8xC
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgInP
s	s	k7c7
osobou	osoba	k1gFnSc7
Johanna	Johann	k1gMnSc2
Gutenberga	Gutenberg	k1gMnSc2
<g/>
.	.	kIx.
</s>