<s>
Knihtisk	knihtisk	k1gInSc1
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
(	(	kIx(
<g/>
kolem	kolem	k7c2
1500	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tisk	tisk	k1gInSc1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Knihtisk	knihtisk	k1gInSc1
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
mechanického	mechanický	k2eAgNnSc2d1
rozmnožování	rozmnožování	k1gNnSc2
textu	text	k1gInSc2
nebo	nebo	k8xC
obrazu	obraz	k1gInSc2
vytvářející	vytvářející	k2eAgFnSc2d1
stejné	stejný	k2eAgFnSc2d1
kopie	kopie	k1gFnSc2
tiskem	tisk	k1gInSc7
z	z	k7c2
výšky	výška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
masového	masový	k2eAgNnSc2d1
používání	používání	k1gNnSc2
k	k	k7c3
tisku	tisk	k1gInSc3
knih	kniha	k1gFnPc2
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
na	na	k7c4
přelom	přelom	k1gInSc4
let	rok	k1gNnPc2
1447	#num#	k4
<g/>
/	/	kIx~
<g/>
1448	#num#	k4
a	a	k8xC
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgInP
s	s	k7c7
osobou	osoba	k1gFnSc7
Johanna	Johann	k1gMnSc2
Gutenberga	Gutenberg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynález	vynález	k1gInSc1
spočíval	spočívat	k5eAaImAgInS
ve	v	k7c6
zdokonalení	zdokonalení	k1gNnSc6
procesu	proces	k1gInSc2
sazby	sazba	k1gFnSc2
skládané	skládaný	k2eAgFnSc2d1
ze	z	k7c2
sériově	sériově	k6eAd1
odlévaných	odlévaný	k2eAgFnPc2d1
tiskařských	tiskařský	k2eAgFnPc2d1
liter	litera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
polovině	polovina	k1gFnSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
knihtisk	knihtisk	k1gInSc1
rychle	rychle	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
po	po	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
následně	následně	k6eAd1
do	do	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInPc4d3
evropské	evropský	k2eAgInPc4d1
tisky	tisk	k1gInPc4
do	do	k7c2
roku	rok	k1gInSc2
1500	#num#	k4
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
inkunábule	inkunábule	k1gFnPc1
<g/>
,	,	kIx,
tisky	tisk	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
1501	#num#	k4
<g/>
–	–	k?
<g/>
1520	#num#	k4
postinkunábule	postinkunábule	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
let	léto	k1gNnPc2
1501	#num#	k4
<g/>
–	–	k?
<g/>
1550	#num#	k4
pak	pak	k6eAd1
souhrnně	souhrnně	k6eAd1
paleotypy	paleotyp	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
vzniku	vznik	k1gInSc2
</s>
<s>
Johannes	Johannes	k1gMnSc1
Gutenberg	Gutenberg	k1gMnSc1
měl	mít	k5eAaImAgMnS
mnoho	mnoho	k6eAd1
přímých	přímý	k2eAgMnPc2d1
i	i	k8xC
nepřímých	přímý	k2eNgMnPc2d1
předchůdců	předchůdce	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
nejstaršího	starý	k2eAgMnSc2d3
předchůdce	předchůdce	k1gMnSc2
knihtisku	knihtisk	k1gInSc2
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
nejrůznější	různý	k2eAgNnPc4d3
pečetidla	pečetidlo	k1gNnPc4
a	a	k8xC
razítka	razítko	k1gNnPc4
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
první	první	k4xOgInPc1
nástroje	nástroj	k1gInPc1
pro	pro	k7c4
opakované	opakovaný	k2eAgInPc4d1
„	„	k?
<g/>
psaní	psaní	k1gNnSc1
<g/>
“	“	k?
stejné	stejný	k2eAgFnSc2d1
značky	značka	k1gFnSc2
<g/>
,	,	kIx,
slova	slovo	k1gNnSc2
či	či	k8xC
věty	věta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
území	území	k1gNnSc2
Mezopotámie	Mezopotámie	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
působily	působit	k5eAaImAgFnP
kultury	kultura	k1gFnPc1
sumerská	sumerský	k2eAgFnSc1d1
<g/>
,	,	kIx,
asyrská	asyrský	k2eAgFnSc1d1
a	a	k8xC
babylonská	babylonský	k2eAgFnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgFnP
známky	známka	k1gFnPc1
tiskařské	tiskařský	k2eAgFnPc1d1
<g/>
,	,	kIx,
či	či	k8xC
spíše	spíše	k9
razičské	razičský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
malých	malý	k2eAgInPc2d1
kamenných	kamenný	k2eAgInPc2d1
válečků	váleček	k1gInPc2
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
se	se	k3xPyFc4
pečetily	pečetit	k5eAaImAgFnP
ještě	ještě	k9
nevypálené	vypálený	k2eNgFnPc1d1
hliněné	hliněný	k2eAgFnPc1d1
destičky	destička	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
doby	doba	k1gFnSc2
asi	asi	k9
3000	#num#	k4
let	léto	k1gNnPc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
cihly	cihla	k1gFnPc1
s	s	k7c7
tištěnými	tištěný	k2eAgInPc7d1
nápisy	nápis	k1gInPc7
ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
známý	známý	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Disk	disk	k1gInSc1
z	z	k7c2
Faistu	Faist	k1gInSc2
z	z	k7c2
období	období	k1gNnSc2
okolo	okolo	k7c2
roku	rok	k1gInSc2
1600	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
hliněný	hliněný	k2eAgInSc1d1
kotouč	kotouč	k1gInSc1
<g/>
,	,	kIx,
nalezený	nalezený	k2eAgInSc1d1
roku	rok	k1gInSc2
1908	#num#	k4
na	na	k7c6
Krétě	Kréta	k1gFnSc6
v	v	k7c6
paláci	palác	k1gInSc6
ve	v	k7c6
Faistu	Faist	k1gInSc6
<g/>
,	,	kIx,
nesoucí	nesoucí	k2eAgFnSc7d1
na	na	k7c4
sobě	se	k3xPyFc3
nápis	nápis	k1gInSc4
v	v	k7c6
hieroglyfickém	hieroglyfický	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
provedený	provedený	k2eAgInSc1d1
jednotlivými	jednotlivý	k2eAgNnPc7d1
razidly	razidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význam	význam	k1gInSc1
textu	text	k1gInSc2
či	či	k8xC
textů	text	k1gInPc2
na	na	k7c6
disku	disk	k1gInSc6
nebyl	být	k5eNaImAgInS
dosud	dosud	k6eAd1
rozluštěn	rozluštit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Tisk	tisk	k1gInSc1
z	z	k7c2
výšky	výška	k1gFnSc2
se	se	k3xPyFc4
patrně	patrně	k6eAd1
poprvé	poprvé	k6eAd1
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
první	první	k4xOgInPc1
deskové	deskový	k2eAgInPc1d1
tisky	tisk	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
v	v	k7c6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
údajně	údajně	k6eAd1
užívaly	užívat	k5eAaImAgFnP
i	i	k9
pohyblivé	pohyblivý	k2eAgFnPc1d1
litery	litera	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jejich	jejich	k3xOp3gMnPc4
vynálezce	vynálezce	k1gMnSc4
bývá	bývat	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
čínský	čínský	k2eAgMnSc1d1
kovář	kovář	k1gMnSc1
Pi-Sheng	Pi-Sheng	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
používal	používat	k5eAaImAgInS
ručně	ručně	k6eAd1
vyráběných	vyráběný	k2eAgFnPc2d1
liter	litera	k1gFnPc2
z	z	k7c2
pálené	pálený	k2eAgFnSc2d1
hlíny	hlína	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
vysázel	vysázet	k5eAaPmAgMnS
na	na	k7c4
kovovou	kovový	k2eAgFnSc4d1
desku	deska	k1gFnSc4
a	a	k8xC
slepil	slepit	k5eAaBmAgMnS
je	být	k5eAaImIp3nS
voskovým	voskový	k2eAgInSc7d1
tmelem	tmel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyhotovení	vyhotovení	k1gNnSc6
otisku	otisk	k1gInSc2
se	se	k3xPyFc4
deska	deska	k1gFnSc1
nahřála	nahřát	k5eAaPmAgFnS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
tmel	tmel	k1gInSc1
povolil	povolit	k5eAaPmAgInS
a	a	k8xC
písmena	písmeno	k1gNnPc1
povolila	povolit	k5eAaPmAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
sestavit	sestavit	k5eAaPmF
jiný	jiný	k2eAgInSc4d1
text	text	k1gInSc4
a	a	k8xC
tisknout	tisknout	k5eAaImF
znovu	znovu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
nedochovaly	dochovat	k5eNaPmAgInP
se	se	k3xPyFc4
ani	ani	k8xC
hliněné	hliněný	k2eAgFnPc1d1
litery	litera	k1gFnPc1
<g/>
,	,	kIx,
ani	ani	k8xC
žádný	žádný	k3yNgInSc4
takto	takto	k6eAd1
provedený	provedený	k2eAgInSc4d1
tisk	tisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Koreji	Korea	k1gFnSc6
byly	být	k5eAaImAgFnP
vynalezeny	vynalezen	k2eAgFnPc1d1
trvanlivější	trvanlivý	k2eAgFnPc1d2
litery	litera	k1gFnPc1
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
se	se	k3xPyFc4
písmo	písmo	k1gNnSc1
vyřezávalo	vyřezávat	k5eAaImAgNnS
negativně	negativně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1234	#num#	k4
soubor	soubor	k1gInSc4
zákonů	zákon	k1gInPc2
Sandem	Sand	k1gInSc7
Remum	Remum	k?
<g/>
,	,	kIx,
jejž	jenž	k3xRgMnSc4
považujeme	považovat	k5eAaImIp1nP
za	za	k7c4
nejstarší	starý	k2eAgFnSc4d3
knihu	kniha	k1gFnSc4
tištěnou	tištěný	k2eAgFnSc4d1
pohyblivými	pohyblivý	k2eAgFnPc7d1
literami	litera	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
dochovaný	dochovaný	k2eAgInSc1d1
tisk	tisk	k1gInSc1
(	(	kIx(
<g/>
Kniha	kniha	k1gFnSc1
Čikči	Čikč	k1gFnSc6
<g/>
)	)	kIx)
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
období	období	k1gNnSc6
Dynastie	dynastie	k1gFnSc2
Korjo	Korjo	k6eAd1
v	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
1377	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kovové	kovový	k2eAgFnSc2d1
litery	litera	k1gFnSc2
z	z	k7c2
mosazi	mosaz	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
ryly	rýt	k5eAaImAgInP
jednotlivě	jednotlivě	k6eAd1
<g/>
,	,	kIx,
nepoužíval	používat	k5eNaImAgInS
se	se	k3xPyFc4
tiskařský	tiskařský	k2eAgInSc1d1
lis	lis	k1gInSc1
a	a	k8xC
technika	technika	k1gFnSc1
se	se	k3xPyFc4
nerozšířila	rozšířit	k5eNaPmAgFnS
<g/>
,	,	kIx,
možná	možná	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
nebyla	být	k5eNaImAgFnS
dostatečně	dostatečně	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
a	a	k8xC
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
nebyla	být	k5eNaImAgFnS
po	po	k7c6
ní	on	k3xPp3gFnSc6
poptávka	poptávka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisklo	tisknout	k5eAaImAgNnS
se	se	k3xPyFc4
dále	daleko	k6eAd2
z	z	k7c2
dřevěných	dřevěný	k2eAgFnPc2d1
desek	deska	k1gFnPc2
a	a	k8xC
moderní	moderní	k2eAgInSc1d1
knihtisk	knihtisk	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
i	i	k9
do	do	k7c2
východní	východní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgInS
až	až	k6eAd1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Evropy	Evropa	k1gFnSc2
razítkový	razítkový	k2eAgInSc1d1
tisk	tisk	k1gInSc1
moc	moc	k6eAd1
nepronikl	proniknout	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klášteře	klášter	k1gInSc6
Prüfening	Prüfening	k1gInSc4
u	u	k7c2
Řezna	Řezno	k1gNnSc2
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
asi	asi	k9
nejstarší	starý	k2eAgFnSc4d3
ukázku	ukázka	k1gFnSc4
této	tento	k3xDgFnSc2
techniky	technika	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
nápis	nápis	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1119	#num#	k4
pořízený	pořízený	k2eAgInSc4d1
pro	pro	k7c4
zasvěcení	zasvěcení	k1gNnSc4
kostela	kostel	k1gInSc2
svatému	svatý	k1gMnSc3
Jiří	Jiří	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápis	nápis	k1gInSc1
byl	být	k5eAaImAgInS
vytlačen	vytlačit	k5eAaPmNgInS
do	do	k7c2
hliněné	hliněný	k2eAgFnSc2d1
desky	deska	k1gFnSc2
jednotlivými	jednotlivý	k2eAgInPc7d1
písmenovými	písmenový	k2eAgInPc7d1
razidly	razidlo	k1gNnPc7
nejspíše	nejspíše	k9
z	z	k7c2
tvrdého	tvrdý	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
dřevěná	dřevěný	k2eAgNnPc4d1
razítka	razítko	k1gNnPc4
jsou	být	k5eAaImIp3nP
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
z	z	k7c2
anglických	anglický	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
v	v	k7c6
Birminghamu	Birmingham	k1gInSc6
<g/>
,	,	kIx,
Oxfordu	Oxford	k1gInSc6
a	a	k8xC
Surrey	Surrea	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jimi	on	k3xPp3gFnPc7
byly	být	k5eAaImAgFnP
pořízeny	pořídit	k5eAaPmNgInP
sakrální	sakrální	k2eAgInPc1d1
nápisy	nápis	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Razítka	razítko	k1gNnPc1
se	se	k3xPyFc4
vzácně	vzácně	k6eAd1
užívala	užívat	k5eAaImAgFnS
také	také	k9
k	k	k7c3
tisku	tisk	k1gInSc3
iniciál	iniciála	k1gFnPc2
v	v	k7c6
některých	některý	k3yIgInPc6
francouzských	francouzský	k2eAgInPc6d1
rukopisech	rukopis	k1gInPc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgMnSc7d1
předchůdcem	předchůdce	k1gMnSc7
a	a	k8xC
inspirací	inspirace	k1gFnSc7
ke	k	k7c3
knihtisku	knihtisk	k1gInSc2
byly	být	k5eAaImAgInP
otisky	otisk	k1gInPc1
dřevořezů	dřevořez	k1gInPc2
užívané	užívaný	k2eAgInPc1d1
původně	původně	k6eAd1
ke	k	k7c3
zdobení	zdobení	k1gNnSc3
látek	látka	k1gFnPc2
(	(	kIx(
<g/>
zejména	zejména	k9
tapet	tapet	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deskové	deskový	k2eAgInPc1d1
či	či	k8xC
blokové	blokový	k2eAgInPc1d1
tisky	tisk	k1gInPc1
se	se	k3xPyFc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
rozšířily	rozšířit	k5eAaPmAgFnP
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pro	pro	k7c4
tisk	tisk	k1gInSc4
obrázků	obrázek	k1gInPc2
a	a	k8xC
jednotlivých	jednotlivý	k2eAgInPc2d1
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějšími	známý	k2eAgFnPc7d3
zástupci	zástupce	k1gMnPc1
této	tento	k3xDgFnSc2
produkce	produkce	k1gFnSc2
jsou	být	k5eAaImIp3nP
hrací	hrací	k2eAgFnPc1d1
karty	karta	k1gFnPc1
a	a	k8xC
tzv.	tzv.	kA
svaté	svatý	k2eAgInPc4d1
obrázky	obrázek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisková	tiskový	k2eAgFnSc1d1
forma	forma	k1gFnSc1
pro	pro	k7c4
celý	celý	k2eAgInSc4d1
list	list	k1gInSc4
nebo	nebo	k8xC
stránku	stránka	k1gFnSc4
byla	být	k5eAaImAgFnS
vyryta	vyryt	k2eAgFnSc1d1
do	do	k7c2
zpravidla	zpravidla	k6eAd1
dřevěné	dřevěný	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
byla	být	k5eAaImAgFnS
však	však	k9
kvůli	kvůli	k7c3
jednorázovému	jednorázový	k2eAgNnSc3d1
využití	využití	k1gNnSc3
drahých	drahý	k2eAgInPc2d1
štočků	štoček	k1gInPc2
velmi	velmi	k6eAd1
pracná	pracný	k2eAgFnSc1d1
a	a	k8xC
nákladná	nákladný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
papír	papír	k1gInSc1
znám	znám	k2eAgInSc1d1
již	již	k6eAd1
od	od	k7c2
přelomu	přelom	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
mechanické	mechanický	k2eAgNnSc1d1
rozmnožování	rozmnožování	k1gNnSc1
obrazu	obraz	k1gInSc2
a	a	k8xC
textu	text	k1gInSc2
používáno	používán	k2eAgNnSc1d1
podstatně	podstatně	k6eAd1
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základy	základ	k1gInPc1
deskotisku	deskotisk	k1gInSc2
zde	zde	k6eAd1
byly	být	k5eAaImAgInP
položeny	položit	k5eAaPmNgInP
již	již	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
technologie	technologie	k1gFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
Koreje	Korea	k1gFnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
exemplářů	exemplář	k1gInPc2
buddhistického	buddhistický	k2eAgInSc2d1
kánonu	kánon	k1gInSc2
Dharani-charms	Dharani-charmsa	k1gFnPc2
<g/>
,	,	kIx,
vytištěného	vytištěný	k2eAgInSc2d1
v	v	k7c6
letech	let	k1gInPc6
764	#num#	k4
<g/>
–	–	k?
<g/>
770	#num#	k4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
na	na	k7c4
papírové	papírový	k2eAgInPc4d1
svitky	svitek	k1gInPc4
<g/>
,	,	kIx,
prý	prý	k9
v	v	k7c6
počtu	počet	k1gInSc6
asi	asi	k9
milionu	milion	k4xCgInSc2
kusů	kus	k1gInPc2
a	a	k8xC
rozesílán	rozesílat	k5eAaImNgInS
všem	všecek	k3xTgInPc3
klášterům	klášter	k1gInPc3
a	a	k8xC
svatyním	svatyně	k1gFnPc3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
svitky	svitek	k1gInPc1
jsou	být	k5eAaImIp3nP
uznávány	uznávat	k5eAaImNgInP
za	za	k7c4
nejstarší	starý	k2eAgFnPc4d3
tištěné	tištěný	k2eAgFnPc4d1
knihy	kniha	k1gFnPc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3
zachovaným	zachovaný	k2eAgInSc7d1
evropským	evropský	k2eAgInSc7d1
dřevořezovým	dřevořezový	k2eAgInSc7d1
štočkem	štoček	k1gInSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Le	Le	k1gFnSc4
Bois	Boisa	k1gFnPc2
Protat	Protat	k1gFnSc2
z	z	k7c2
období	období	k1gNnPc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1370	#num#	k4
a	a	k8xC
1380	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
sloužil	sloužit	k5eAaImAgInS
k	k	k7c3
dekoraci	dekorace	k1gFnSc3
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
60	#num#	k4
cm	cm	kA
vysoký	vysoký	k2eAgMnSc1d1
a	a	k8xC
23	#num#	k4
cm	cm	kA
široký	široký	k2eAgMnSc1d1
a	a	k8xC
zobrazuje	zobrazovat	k5eAaImIp3nS
tři	tři	k4xCgMnPc4
bojovníky	bojovník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
tisku	tisk	k1gInSc2
na	na	k7c4
látku	látka	k1gFnSc4
je	být	k5eAaImIp3nS
zhotovena	zhotoven	k2eAgFnSc1d1
i	i	k8xC
tapeta	tapeta	k1gFnSc1
ze	z	k7c2
Sitten	Sittno	k1gNnPc2
z	z	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tisku	tisk	k1gInSc2
na	na	k7c4
látku	látka	k1gFnSc4
k	k	k7c3
tisku	tisk	k1gInSc3
na	na	k7c4
papír	papír	k1gInSc4
již	již	k6eAd1
nebylo	být	k5eNaImAgNnS
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
vznikaly	vznikat	k5eAaImAgInP
zejména	zejména	k9
již	již	k6eAd1
zmíněné	zmíněný	k2eAgFnSc2d1
hrací	hrací	k2eAgFnSc2d1
karty	karta	k1gFnSc2
a	a	k8xC
devoční	devoční	k2eAgFnSc1d1
grafika	grafika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
datovaných	datovaný	k2eAgInPc2d1
dřevořezů	dřevořez	k1gInPc2
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
známe	znát	k5eAaImIp1nP
bruselskou	bruselský	k2eAgFnSc4d1
Madonu	Madona	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1418	#num#	k4
a	a	k8xC
postavu	postava	k1gFnSc4
sv.	sv.	kA
Kryštofa	Kryštof	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1423	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
je	být	k5eAaImIp3nS
již	již	k6eAd1
jen	jen	k9
krůček	krůček	k1gInSc1
(	(	kIx(
<g/>
spíše	spíše	k9
hypotetický	hypotetický	k2eAgMnSc1d1
<g/>
,	,	kIx,
vytvořený	vytvořený	k2eAgMnSc1d1
pořádáním	pořádání	k1gNnSc7
historie	historie	k1gFnSc2
<g/>
)	)	kIx)
k	k	k7c3
deskotisku	deskotisk	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
tisky	tisk	k1gInPc1
byly	být	k5eAaImAgFnP
vzácně	vzácně	k6eAd1
spojovány	spojovat	k5eAaImNgFnP
do	do	k7c2
ucelené	ucelený	k2eAgFnSc2d1
formy	forma	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
nazýváme	nazývat	k5eAaImIp1nP
bloková	blokový	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blokovou	blokový	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
můžeme	moct	k5eAaImIp1nP
vnímat	vnímat	k5eAaImF
jako	jako	k9
přechodové	přechodový	k2eAgNnSc4d1
stadium	stadium	k1gNnSc4
mezi	mezi	k7c7
iluminovaným	iluminovaný	k2eAgInSc7d1
rukopisem	rukopis	k1gInSc7
a	a	k8xC
prvotiskem	prvotisk	k1gInSc7
tištěným	tištěný	k2eAgInSc7d1
již	již	k6eAd1
novou	nový	k2eAgFnSc7d1
tiskovou	tiskový	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
pohyblivých	pohyblivý	k2eAgFnPc2d1
liter	litera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
nám	my	k3xPp1nPc3
známy	znám	k2eAgFnPc1d1
i	i	k8xC
starší	starý	k2eAgInPc1d2
typy	typ	k1gInPc1
blokových	blokový	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
byl	být	k5eAaImAgInS
text	text	k1gInSc4
na	na	k7c4
vyhotovený	vyhotovený	k2eAgInSc4d1
obrazový	obrazový	k2eAgInSc4d1
tisk	tisk	k1gInSc4
dopisován	dopisovat	k5eAaImNgInS
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
Gutenberga	Gutenberg	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgInS
vyučen	vyučit	k5eAaPmNgMnS
ve	v	k7c6
zlatnictví	zlatnictví	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
jak	jak	k6eAd1
rytí	rytí	k1gNnSc4
do	do	k7c2
kovu	kov	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k9
také	také	k9
odlévání	odlévání	k1gNnSc1
<g/>
,	,	kIx,
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
velmi	velmi	k6eAd1
inspirativní	inspirativní	k2eAgInPc4d1
nápisy	nápis	k1gInPc4
ražené	ražený	k2eAgInPc4d1
na	na	k7c6
vazbách	vazba	k1gFnPc6
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
označovaly	označovat	k5eAaImAgFnP
jejich	jejich	k3xOp3gInSc4
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
udávaly	udávat	k5eAaImAgInP
letopočet	letopočet	k1gInSc4
či	či	k8xC
jiné	jiný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápisy	nápis	k1gInPc7
do	do	k7c2
vazeb	vazba	k1gFnPc2
knih	kniha	k1gFnPc2
razil	razit	k5eAaImAgInS
patrně	patrně	k6eAd1
jako	jako	k8xS,k8xC
první	první	k4xOgInSc1
(	(	kIx(
<g/>
nemáme	mít	k5eNaImIp1nP
žádné	žádný	k3yNgInPc1
starší	starý	k2eAgInPc1d2
doklady	doklad	k1gInPc1
této	tento	k3xDgFnSc2
činnosti	činnost	k1gFnSc2
<g/>
)	)	kIx)
Gutenbergův	Gutenbergův	k2eAgMnSc1d1
současník	současník	k1gMnSc1
norimberský	norimberský	k2eAgMnSc1d1
dominikán	dominikán	k1gMnSc1
Conrad	Conrada	k1gFnPc2
Forster	Forster	k1gInSc4
z	z	k7c2
Ansbachu	Ansbach	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
již	již	k6eAd1
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johannes	Johannes	k1gMnSc1
Gutenberg	Gutenberg	k1gMnSc1
nebyl	být	k5eNaImAgMnS
jediným	jediné	k1gNnSc7
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
myšlenkou	myšlenka	k1gFnSc7
tisku	tisk	k1gInSc2
ze	z	k7c2
sazby	sazba	k1gFnSc2
skládané	skládaný	k2eAgFnSc2d1
z	z	k7c2
oddělených	oddělený	k2eAgNnPc2d1
písmen	písmeno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
ale	ale	k9
pro	pro	k7c4
jejich	jejich	k3xOp3gFnSc4
výrobu	výroba	k1gFnSc4
používalo	používat	k5eAaImAgNnS
dřevo	dřevo	k1gNnSc1
<g/>
,	,	kIx,
odolnost	odolnost	k1gFnSc1
materiálu	materiál	k1gInSc2
nebyla	být	k5eNaImAgFnS
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
rytí	rytí	k1gNnSc1
jednotlivých	jednotlivý	k2eAgNnPc2d1
písmen	písmeno	k1gNnPc2
z	z	k7c2
kovu	kov	k1gInSc2
bylo	být	k5eAaImAgNnS
pracné	pracný	k2eAgNnSc1d1
a	a	k8xC
nákladné	nákladný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
knihtisku	knihtisk	k1gInSc2
vynalezeného	vynalezený	k2eAgInSc2d1
Gutenbergem	Gutenberg	k1gInSc7
</s>
<s>
Tiskové	tiskový	k2eAgNnSc1d1
písmeno	písmeno	k1gNnSc1
–	–	k?
ligatura	ligatura	k1gFnSc1
písmen	písmeno	k1gNnPc2
ſ	ſ	k?
(	(	kIx(
<g/>
dlouhé	dlouhý	k2eAgFnPc1d1
s	s	k7c7
<g/>
)	)	kIx)
a	a	k8xC
i.	i.	k?
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3
z	z	k7c2
Gutenbergových	Gutenbergův	k2eAgFnPc2d1
inovací	inovace	k1gFnPc2
byl	být	k5eAaImAgInS
vynález	vynález	k1gInSc1
písmolijectví	písmolijectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňoval	umožňovat	k5eAaImAgMnS
sériovou	sériový	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
tvarově	tvarově	k6eAd1
shodných	shodný	k2eAgFnPc2d1
tiskařských	tiskařský	k2eAgFnPc2d1
liter	litera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písmorytec	Písmorytec	k1gMnSc1
nejprve	nejprve	k6eAd1
vyryl	vyrýt	k5eAaPmAgMnS
zrcadlově	zrcadlově	k6eAd1
obrácený	obrácený	k2eAgInSc4d1
obraz	obraz	k1gInSc4
písmene	písmeno	k1gNnSc2
–	–	k?
patrici	patrice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
použita	použít	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
razidlo	razidlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otiskem	otisk	k1gInSc7
do	do	k7c2
měkčího	měkký	k2eAgInSc2d2
kovu	kov	k1gInSc2
vznikla	vzniknout	k5eAaPmAgFnS
forma	forma	k1gFnSc1
–	–	k?
matrice	matrice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matrice	matrice	k1gFnSc1
se	se	k3xPyFc4
vložila	vložit	k5eAaPmAgFnS
do	do	k7c2
licího	licí	k2eAgInSc2d1
strojku	strojek	k1gInSc2
a	a	k8xC
zalila	zalít	k5eAaPmAgFnS
roztavenou	roztavený	k2eAgFnSc7d1
liteřinou	liteřina	k1gFnSc7
<g/>
,	,	kIx,
slitinou	slitina	k1gFnSc7
cínu	cín	k1gInSc2
<g/>
,	,	kIx,
olova	olovo	k1gNnSc2
a	a	k8xC
antimonu	antimon	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
také	také	k9
vynález	vynález	k1gInSc1
Gutenbergův	Gutenbergův	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlité	odlitý	k2eAgFnPc4d1
litery	litera	k1gFnPc4
byly	být	k5eAaImAgInP
otočeným	otočený	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
písmena	písmeno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základna	základna	k1gFnSc1
(	(	kIx(
<g/>
tělo	tělo	k1gNnSc1
<g/>
)	)	kIx)
litery	litera	k1gFnSc2
sloužila	sloužit	k5eAaImAgFnS
nejen	nejen	k6eAd1
jako	jako	k8xC,k8xS
podložka	podložka	k1gFnSc1
pro	pro	k7c4
písmeno	písmeno	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
svými	svůj	k3xOyFgInPc7
rozměry	rozměr	k1gInPc7
určovala	určovat	k5eAaImAgFnS
i	i	k9
rozestupy	rozestup	k1gInPc4
mezi	mezi	k7c7
písmeny	písmeno	k1gNnPc7
v	v	k7c6
řádku	řádek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejná	stejný	k2eAgFnSc1d1
písmová	písmový	k2eAgFnSc1d1
výška	výška	k1gFnSc1
umožňovala	umožňovat	k5eAaImAgFnS
řazení	řazení	k1gNnSc4
různých	různý	k2eAgNnPc2d1
písmen	písmeno	k1gNnPc2
do	do	k7c2
jednotné	jednotný	k2eAgFnSc2d1
sazby	sazba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
tisková	tiskový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
sestavována	sestavovat	k5eAaImNgFnS
do	do	k7c2
dřevěné	dřevěný	k2eAgFnSc2d1
sazebnice	sazebnice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
určovala	určovat	k5eAaImAgFnS
přesné	přesný	k2eAgNnSc4d1
řádkování	řádkování	k1gNnSc4
i	i	k8xC
místa	místo	k1gNnPc4
pro	pro	k7c4
mezery	mezera	k1gFnPc4
a	a	k8xC
ilustrace	ilustrace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stránky	stránka	k1gFnPc1
se	se	k3xPyFc4
sestavily	sestavit	k5eAaPmAgFnP
do	do	k7c2
archu	arch	k1gInSc2
a	a	k8xC
na	na	k7c4
sazbu	sazba	k1gFnSc4
se	se	k3xPyFc4
nanesla	nanést	k5eAaPmAgFnS,k5eAaBmAgFnS
koženými	kožený	k2eAgFnPc7d1
tampony	tampon	k1gInPc4
tiskařská	tiskařský	k2eAgFnSc1d1
čerň	čerň	k1gFnSc1
<g/>
,	,	kIx,
Gutenbergem	Gutenberg	k1gInSc7
nově	nově	k6eAd1
vyráběná	vyráběný	k2eAgFnSc1d1
ze	z	k7c2
sazí	saze	k1gFnPc2
a	a	k8xC
lněné	lněný	k2eAgFnSc2d1
fermeže	fermež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
upraveného	upravený	k2eAgInSc2d1
lisu	lis	k1gInSc2
na	na	k7c4
víno	víno	k1gNnSc4
se	se	k3xPyFc4
pak	pak	k6eAd1
dal	dát	k5eAaPmAgMnS
vytisknout	vytisknout	k5eAaPmF
celý	celý	k2eAgInSc4d1
arch	arch	k1gInSc4
najednou	najednou	k6eAd1
místo	místo	k7c2
dřívějšího	dřívější	k2eAgNnSc2d1
ručního	ruční	k2eAgNnSc2d1
přitlačování	přitlačování	k1gNnSc2
hladítky	hladítko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gutenbergem	Gutenberg	k1gInSc7
vynalezený	vynalezený	k2eAgInSc1d1
postup	postup	k1gInSc1
se	se	k3xPyFc4
používal	používat	k5eAaImAgInS
bez	bez	k7c2
výrazných	výrazný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
knihtisku	knihtisk	k1gInSc2
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
význam	význam	k1gInSc1
knihtisku	knihtisk	k1gInSc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
rozšíření	rozšíření	k1gNnSc6
písemné	písemný	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
mezi	mezi	k7c4
širší	široký	k2eAgFnPc4d2
vrstvy	vrstva	k1gFnPc4
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihy	kniha	k1gFnPc1
vydané	vydaný	k2eAgFnPc1d1
v	v	k7c6
mnoha	mnoho	k4c6
tištěných	tištěný	k2eAgInPc6d1
exemplářích	exemplář	k1gInPc6
byly	být	k5eAaImAgFnP
daleko	daleko	k6eAd1
dostupnější	dostupný	k2eAgFnPc1d2
a	a	k8xC
lépe	dobře	k6eAd2
odolaly	odolat	k5eAaPmAgInP
hrozbě	hrozba	k1gFnSc3
úplného	úplný	k2eAgInSc2d1
zániku	zánik	k1gInSc2
než	než	k8xS
unikátní	unikátní	k2eAgInPc4d1
rukopisy	rukopis	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vytváření	vytváření	k1gNnSc1
identických	identický	k2eAgFnPc2d1
kopií	kopie	k1gFnPc2
mělo	mít	k5eAaImAgNnS
i	i	k9
velký	velký	k2eAgInSc4d1
normotvorný	normotvorný	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
knihtisku	knihtisk	k1gInSc3
například	například	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
sjednocení	sjednocení	k1gNnSc3
liturgických	liturgický	k2eAgInPc2d1
textů	text	k1gInPc2
<g/>
,	,	kIx,
zásadní	zásadní	k2eAgFnSc4d1
roli	role	k1gFnSc4
hrál	hrát	k5eAaImAgInS
knihtisk	knihtisk	k1gInSc1
i	i	k8xC
při	pře	k1gFnSc3
utváření	utváření	k1gNnSc2
závazných	závazný	k2eAgNnPc2d1
jazykových	jazykový	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
překlady	překlad	k1gInPc1
Bible	bible	k1gFnSc2
Martina	Martin	k1gMnSc2
Luthera	Luther	k1gMnSc2
nebo	nebo	k8xC
Bible	bible	k1gFnPc1
kralické	kralický	k2eAgFnPc1d1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
základem	základ	k1gInSc7
spisovného	spisovný	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
a	a	k8xC
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
knihtisk	knihtisk	k1gInSc1
stal	stát	k5eAaPmAgInS
zásadním	zásadní	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
rozmach	rozmach	k1gInSc4
reformačního	reformační	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihtisk	knihtisk	k1gInSc1
umožnil	umožnit	k5eAaPmAgInS
masovou	masový	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jedné	jeden	k4xCgFnSc2
knihy	kniha	k1gFnSc2
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
najednou	najednou	k6eAd1
vytištěny	vytištěn	k2eAgInPc4d1
statisíce	statisíce	k1gInPc4
exemplářů	exemplář	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
jen	jen	k9
dílna	dílna	k1gFnSc1
Hanse	Hans	k1gMnSc2
Luffta	Lufft	k1gMnSc2
<g/>
,	,	kIx,
tiskaře	tiskař	k1gMnSc2
ve	v	k7c6
Wittenbergu	Wittenberg	k1gInSc6
<g/>
,	,	kIx,
vytiskla	vytisknout	k5eAaPmAgFnS
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1534	#num#	k4
a	a	k8xC
1574	#num#	k4
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
000	#num#	k4
výtisků	výtisk	k1gInPc2
Lutherovy	Lutherův	k2eAgFnSc2d1
bible	bible	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nakladatelsko-autorské	nakladatelsko-autorský	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
vytvářené	vytvářený	k2eAgFnPc1d1
kolem	kolo	k1gNnSc7
tiskařských	tiskařský	k2eAgFnPc2d1
dílen	dílna	k1gFnPc2
významně	významně	k6eAd1
působily	působit	k5eAaImAgFnP
při	pře	k1gFnSc4
utváření	utváření	k1gNnSc2
individuálně	individuálně	k6eAd1
chápaného	chápaný	k2eAgNnSc2d1
moderního	moderní	k2eAgNnSc2d1
autorství	autorství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
tiskáren	tiskárna	k1gFnPc2
způsobilo	způsobit	k5eAaPmAgNnS
také	také	k9
pokles	pokles	k1gInSc1
ceny	cena	k1gFnSc2
knih	kniha	k1gFnPc2
a	a	k8xC
vznik	vznik	k1gInSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
skutečného	skutečný	k2eAgInSc2d1
knižního	knižní	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
knihtisku	knihtisk	k1gInSc2
</s>
<s>
Určení	určení	k1gNnSc1
přesného	přesný	k2eAgNnSc2d1
data	datum	k1gNnSc2
vzniku	vznik	k1gInSc2
u	u	k7c2
nejstarších	starý	k2eAgInPc2d3
prvotisků	prvotisk	k1gInPc2
bývá	bývat	k5eAaImIp3nS
velice	velice	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelské	nakladatelský	k2eAgInPc1d1
údaje	údaj	k1gInPc1
u	u	k7c2
knih	kniha	k1gFnPc2
totiž	totiž	k9
zpočátku	zpočátku	k6eAd1
nebyly	být	k5eNaImAgFnP
pravidlem	pravidlem	k6eAd1
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
se	se	k3xPyFc4
zachoval	zachovat	k5eAaPmAgInS
pouhý	pouhý	k2eAgInSc1d1
zlomek	zlomek	k1gInSc1
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
na	na	k7c6
datování	datování	k1gNnSc6
prvních	první	k4xOgMnPc2
tisků	tisk	k1gInPc2
často	často	k6eAd1
usuzuje	usuzovat	k5eAaImIp3nS
srovnávacím	srovnávací	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
tiskového	tiskový	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
<g/>
,	,	kIx,
filigránů	filigrán	k1gInPc2
<g/>
,	,	kIx,
obsahovou	obsahový	k2eAgFnSc7d1
a	a	k8xC
jazykovou	jazykový	k2eAgFnSc7d1
analýzou	analýza	k1gFnSc7
textu	text	k1gInSc2
nebo	nebo	k8xC
na	na	k7c6
základě	základ	k1gInSc6
archivních	archivní	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zpočátku	zpočátku	k6eAd1
byl	být	k5eAaImAgInS
vynález	vynález	k1gInSc1
výrobním	výrobní	k2eAgNnSc7d1
tajemstvím	tajemství	k1gNnSc7
dílny	dílna	k1gFnSc2
vynálezce	vynálezce	k1gMnSc2
knihtisku	knihtisk	k1gInSc2
Johannese	Johannese	k1gFnSc1
Gutenberga	Gutenberga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
Gutenberg	Gutenberg	k1gInSc1
prohrál	prohrát	k5eAaPmAgInS
soudní	soudní	k2eAgInSc1d1
spor	spor	k1gInSc1
s	s	k7c7
Johannem	Johann	k1gMnSc7
Fustem	Fust	k1gMnSc7
a	a	k8xC
byla	být	k5eAaImAgFnS
mu	on	k3xPp3gMnSc3
v	v	k7c4
exekuci	exekuce	k1gFnSc4
zabavena	zabaven	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
<g/>
,	,	kIx,
rozešla	rozejít	k5eAaPmAgFnS
se	se	k3xPyFc4
část	část	k1gFnSc1
jeho	jeho	k3xOp3gMnPc2
učňů	učeň	k1gMnPc2
do	do	k7c2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1458	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
se	se	k3xPyFc4
usadili	usadit	k5eAaPmAgMnP
v	v	k7c6
Bamberku	Bamberk	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1460	#num#	k4
ve	v	k7c6
Štrasburku	Štrasburk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Německa	Německo	k1gNnSc2
se	se	k3xPyFc4
potom	potom	k6eAd1
knihtisk	knihtisk	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
(	(	kIx(
<g/>
1461	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
(	(	kIx(
<g/>
1465	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
1470	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Švýcarska	Švýcarsko	k1gNnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
1472	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
1473	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Maďarska	Maďarsko	k1gNnSc2
(	(	kIx(
<g/>
1473	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Polska	Polsko	k1gNnSc2
(	(	kIx(
<g/>
1473	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dánska	Dánsko	k1gNnSc2
(	(	kIx(
<g/>
1482	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
Moravu	Morava	k1gFnSc4
(	(	kIx(
<g/>
1486	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
nejstarší	starý	k2eAgFnSc4d3
knihu	kniha	k1gFnSc4
vytištěnou	vytištěný	k2eAgFnSc4d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
se	se	k3xPyFc4
dlouho	dlouho	k6eAd1
pokládala	pokládat	k5eAaImAgFnS
"	"	kIx"
<g/>
Kronika	kronika	k1gFnSc1
trojánská	trojánský	k2eAgFnSc1d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
vytištěná	vytištěný	k2eAgFnSc1d1
neznámým	známý	k2eNgMnSc7d1
tiskařem	tiskař	k1gMnSc7
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
a	a	k8xC
datovaná	datovaný	k2eAgFnSc1d1
rokem	rok	k1gInSc7
1468	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
však	však	k9
objevil	objevit	k5eAaPmAgMnS
rukopis	rukopis	k1gInSc4
s	s	k7c7
datem	datum	k1gNnSc7
1468	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
přesnou	přesný	k2eAgFnSc7d1
předlohou	předloha	k1gFnSc7
pro	pro	k7c4
tisk	tisk	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
tedy	tedy	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
datum	datum	k1gNnSc1
bylo	být	k5eAaImAgNnS
z	z	k7c2
něho	on	k3xPp3gInSc2
mechanicky	mechanicky	k6eAd1
převzato	převzít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
není	být	k5eNaImIp3nS
rok	rok	k1gInSc1
1468	#num#	k4
vzhledem	vzhledem	k7c3
k	k	k7c3
šíření	šíření	k1gNnSc3
knihtisku	knihtisk	k1gInSc2
po	po	k7c6
Evropě	Evropa	k1gFnSc6
příliš	příliš	k6eAd1
pravděpodobný	pravděpodobný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihtisk	knihtisk	k1gInSc1
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
do	do	k7c2
Čech	Čechy	k1gFnPc2
rozšířil	rozšířit	k5eAaPmAgInS
v	v	k7c6
polovině	polovina	k1gFnSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
přímo	přímo	k6eAd1
z	z	k7c2
Bamberku	Bamberk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
rokem	rok	k1gInSc7
1500	#num#	k4
se	se	k3xPyFc4
dále	daleko	k6eAd2
tisklo	tisknout	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
Chorvatska	Chorvatsko	k1gNnSc2
<g/>
,	,	kIx,
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgMnSc1d1
tiskař	tiskař	k1gMnSc1
v	v	k7c6
portugalských	portugalský	k2eAgFnPc6d1
službách	služba	k1gFnPc6
tiskl	tisknout	k5eAaImAgInS
roku	rok	k1gInSc2
1494	#num#	k4
na	na	k7c6
ostrovech	ostrov	k1gInPc6
Sao	Sao	k1gFnSc2
Tomé	Tomý	k2eAgFnSc2d1
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1500	#num#	k4
existovalo	existovat	k5eAaImAgNnS
už	už	k6eAd1
1100	#num#	k4
dílen	dílna	k1gFnPc2
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
250	#num#	k4
městech	město	k1gNnPc6
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
(	(	kIx(
<g/>
80	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
50	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Francii	Francie	k1gFnSc4
(	(	kIx(
<g/>
40	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Španělsku	Španělsko	k1gNnSc6
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šíření	šíření	k1gNnSc1
knihtisku	knihtisk	k1gInSc2
záviselo	záviset	k5eAaImAgNnS
na	na	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
vyspělosti	vyspělost	k1gFnSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
,	,	kIx,
dostupnosti	dostupnost	k1gFnSc2
surovin	surovina	k1gFnPc2
a	a	k8xC
existenci	existence	k1gFnSc4
movitých	movitý	k2eAgMnPc2d1
objednavatelů	objednavatel	k1gMnPc2
(	(	kIx(
<g/>
biskupské	biskupský	k2eAgInPc1d1
a	a	k8xC
panovnické	panovnický	k2eAgInPc1d1
dvory	dvůr	k1gInPc1
<g/>
,	,	kIx,
univerzity	univerzita	k1gFnPc1
<g/>
,	,	kIx,
kapituly	kapitula	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
knihtisk	knihtisk	k1gInSc1
zaveden	zavést	k5eAaPmNgInS
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
<g/>
,	,	kIx,
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
Bosně	Bosna	k1gFnSc6
<g/>
,	,	kIx,
Litvě	Litva	k1gFnSc6
<g/>
,	,	kIx,
Islandu	Island	k1gInSc6
<g/>
,	,	kIx,
Srbsku	Srbsko	k1gNnSc6
<g/>
,	,	kIx,
Rusku	Rusko	k1gNnSc6
<g/>
,	,	kIx,
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
Portugalska	Portugalsko	k1gNnSc2
knihtisk	knihtisk	k1gInSc4
rozšířili	rozšířit	k5eAaPmAgMnP
misionáři	misionář	k1gMnPc1
do	do	k7c2
Jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc1
1539	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
Goa	Goa	k1gFnSc1
1550	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
(	(	kIx(
<g/>
Nagasaki	Nagasaki	k1gNnSc1
1590	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židé	Žid	k1gMnPc1
začali	začít	k5eAaPmAgMnP
tisknout	tisknout	k5eAaImF
hebrejskými	hebrejský	k2eAgInPc7d1
typy	typ	k1gInPc7
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
Řecku	Řecko	k1gNnSc6
a	a	k8xC
Turecku	Turecko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
Alp	Alpy	k1gFnPc2
potom	potom	k6eAd1
poprvé	poprvé	k6eAd1
1525	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisk	tisk	k1gInSc1
slovanskými	slovanský	k2eAgFnPc7d1
literami	litera	k1gFnPc7
(	(	kIx(
<g/>
hlaholicí	hlaholice	k1gFnSc7
a	a	k8xC
cyrilicí	cyrilice	k1gFnSc7
<g/>
)	)	kIx)
vznikl	vzniknout	k5eAaPmAgInS
koncem	koncem	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1517	#num#	k4
tiskl	tisknout	k5eAaImAgMnS
František	František	k1gMnSc1
Skorina	Skorin	k2eAgFnSc1d1
cyrilskými	cyrilský	k2eAgFnPc7d1
literami	litera	k1gFnPc7
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
knihtisk	knihtisk	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
například	například	k6eAd1
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Finska	Finsko	k1gNnSc2
nebo	nebo	k8xC
Norska	Norsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
islámských	islámský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byl	být	k5eAaImAgInS
knihtisk	knihtisk	k1gInSc1
přísně	přísně	k6eAd1
zakázán	zakázat	k5eAaPmNgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1729	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
židovských	židovský	k2eAgFnPc2d1
a	a	k8xC
křesťanských	křesťanský	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharsko	Bulharsko	k1gNnSc1
či	či	k8xC
Austrálie	Austrálie	k1gFnPc1
datují	datovat	k5eAaImIp3nP
první	první	k4xOgFnPc1
tištěné	tištěný	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
až	až	k9
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
tiskařských	tiskařský	k2eAgFnPc2d1
technik	technika	k1gFnPc2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Se	s	k7c7
šířením	šíření	k1gNnSc7
gramotnosti	gramotnost	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nová	nový	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
po	po	k7c6
levnějších	levný	k2eAgFnPc6d2
knihách	kniha	k1gFnPc6
a	a	k8xC
tiskovinách	tiskovina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitým	důležitý	k2eAgInSc7d1
pokrokem	pokrok	k1gInSc7
bylo	být	k5eAaImAgNnS
zavedení	zavedení	k1gNnSc1
strojní	strojní	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
papíru	papír	k1gInSc2
v	v	k7c6
"	"	kIx"
<g/>
nekonečném	konečný	k2eNgInSc6d1
<g/>
"	"	kIx"
pásu	pás	k1gInSc6
<g/>
,	,	kIx,
od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
ze	z	k7c2
dřeva	dřevo	k1gNnSc2
<g/>
,	,	kIx,
vynálezy	vynález	k1gInPc1
lepších	dobrý	k2eAgInPc2d2
tiskařských	tiskařský	k2eAgInPc2d1
lisů	lis	k1gInPc2
(	(	kIx(
<g/>
rychlolisy	rychlolis	k1gInPc1
<g/>
,	,	kIx,
rotační	rotační	k2eAgInSc1d1
tisk	tisk	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
postupně	postupně	k6eAd1
i	i	k9
nových	nový	k2eAgFnPc2d1
technik	technika	k1gFnPc2
tisku	tisk	k1gInSc2
z	z	k7c2
hloubky	hloubka	k1gFnSc2
(	(	kIx(
<g/>
hlubotisk	hlubotisk	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
plochy	plocha	k1gFnSc2
(	(	kIx(
<g/>
litografie	litografie	k1gFnSc1
<g/>
,	,	kIx,
ofset	ofset	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začal	začít	k5eAaPmAgMnS
masový	masový	k2eAgInSc4d1
tisk	tisk	k1gInSc4
novin	novina	k1gFnPc2
na	na	k7c6
rotačních	rotační	k2eAgInPc6d1
strojích	stroj	k1gInPc6
a	a	k8xC
od	od	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
začal	začít	k5eAaPmAgInS
ofset	ofset	k1gInSc1
vytlačovat	vytlačovat	k5eAaImF
tisk	tisk	k1gInSc4
z	z	k7c2
výšky	výška	k1gFnSc2
i	i	k9
v	v	k7c6
tisku	tisk	k1gInSc6
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
časopisů	časopis	k1gInPc2
a	a	k8xC
novin	novina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
masově	masově	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
fotosazba	fotosazba	k1gFnSc1
a	a	k8xC
barevný	barevný	k2eAgInSc1d1
ofsetový	ofsetový	k2eAgInSc1d1
tisk	tisk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
nejběžnější	běžný	k2eAgFnSc7d3
tiskovou	tiskový	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Grafy	graf	k1gInPc1
</s>
<s>
Produkce	produkce	k1gFnSc1
tištěných	tištěný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
do	do	k7c2
roku	rok	k1gInSc2
1800	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tiskárny	tiskárna	k1gFnPc1
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozložení	rozložení	k1gNnSc1
prvotisků	prvotisk	k1gInPc2
podle	podle	k7c2
oblastí	oblast	k1gFnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozložení	rozložení	k1gNnSc1
prvotisků	prvotisk	k1gInPc2
podle	podle	k7c2
jazyka	jazyk	k1gInSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Dřevěný	dřevěný	k2eAgInSc1d1
lis	lis	k1gInSc1
z	z	k7c2
jezuitské	jezuitský	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
v	v	k7c6
pražském	pražský	k2eAgNnSc6d1
Klementinu	Klementinum	k1gNnSc6
z	z	k7c2
přelomu	přelom	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
Jediný	jediný	k2eAgInSc1d1
zachovaný	zachovaný	k2eAgInSc1d1
dřevěný	dřevěný	k2eAgInSc1d1
lis	lis	k1gInSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Kopie	kopie	k1gFnSc1
stránky	stránka	k1gFnSc2
z	z	k7c2
Gutenbergovy	Gutenbergův	k2eAgFnSc2d1
42	#num#	k4
<g/>
řádkové	řádkový	k2eAgFnPc4d1
Bible	bible	k1gFnPc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Národní	národní	k2eAgNnSc1d1
technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgMnSc1
prakticky	prakticky	k6eAd1
použitelný	použitelný	k2eAgInSc1d1
celokovový	celokovový	k2eAgInSc1d1
knihtiskařský	knihtiskařský	k2eAgInSc1d1
lis	lis	k1gInSc1
<g/>
,	,	kIx,
vynalezený	vynalezený	k2eAgInSc1d1
lordem	lord	k1gMnSc7
Robertem	Robert	k1gMnSc7
Walkerem	Walker	k1gMnSc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
roku	rok	k1gInSc2
1800	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Národní	národní	k2eAgNnSc1d1
technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Rychlolis	rychlolis	k1gInSc1
Johannisberg	Johannisberg	k1gInSc1
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
poháněn	pohánět	k5eAaImNgInS
řemenem	řemen	k1gInSc7
od	od	k7c2
transmisní	transmisní	k2eAgFnSc2d1
hřídele	hřídel	k1gFnSc2
a	a	k8xC
tiskl	tisknout	k5eAaImAgInS
kývavým	kývavý	k2eAgInSc7d1
pohybem	pohyb	k1gInSc7
oběma	dva	k4xCgInPc7
směry	směr	k1gInPc7
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Národní	národní	k2eAgNnSc1d1
technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Memory	Memor	k1gInPc1
of	of	k?
the	the	k?
World	World	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
unesco	unesco	k6eAd1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
accessed	accessed	k1gInSc1
November	November	k1gInSc1
2009	#num#	k4
<g/>
↑	↑	k?
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Knihtiskařství	knihtiskařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
14	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
444	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
John	John	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Gutenberg	Gutenberg	k1gMnSc1
Revolution	Revolution	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Story	story	k1gFnSc2
of	of	k?
a	a	k8xC
Genius	genius	k1gMnSc1
and	and	k?
an	an	k?
Invention	Invention	k1gInSc1
that	that	k1gMnSc1
Changed	Changed	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Review	Review	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7472	#num#	k4
<g/>
-	-	kIx~
<g/>
4504	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cit	cit	k1gInSc1
<g/>
.	.	kIx.
podle	podle	k7c2
anglické	anglický	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Buringh	Buringh	k1gMnSc1
<g/>
,	,	kIx,
Eltjo	Eltjo	k1gMnSc1
<g/>
;	;	kIx,
van	van	k1gInSc1
Zanden	Zandna	k1gFnPc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Luiten	Luiten	k2eAgMnSc1d1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Charting	Charting	k1gInSc1
the	the	k?
“	“	k?
<g/>
Rise	Rise	k1gInSc1
of	of	k?
the	the	k?
West	West	k1gInSc1
<g/>
”	”	k?
<g/>
:	:	kIx,
Manuscripts	Manuscripts	k1gInSc1
and	and	k?
Printed	Printed	k1gInSc1
Books	Books	k1gInSc1
in	in	k?
Europe	Europ	k1gMnSc5
<g/>
,	,	kIx,
A	a	k9
Long-Term	Long-Term	k1gInSc1
Perspective	Perspectiv	k1gInSc5
from	fro	k1gNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
the	the	k?
Sixth	Sixth	k1gMnSc1
through	through	k1gMnSc1
Eighteenth	Eighteenth	k1gMnSc1
Centuries	Centuries	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
The	The	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Economic	Economic	k1gMnSc1
History	Histor	k1gInPc4
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
69	#num#	k4
<g/>
,	,	kIx,
No	no	k9
<g/>
.	.	kIx.
2	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
409	#num#	k4
<g/>
–	–	k?
<g/>
445	#num#	k4
(	(	kIx(
<g/>
417	#num#	k4
<g/>
,	,	kIx,
table	tablo	k1gNnSc6
2	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Incunabula	Incunabul	k1gMnSc2
Short	Short	k1gInSc4
Title	titla	k1gFnSc3
Catalogue	Catalogue	k1gInSc1
<g/>
,	,	kIx,
navštíveno	navštíven	k2eAgNnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Knihtiskařství	knihtiskařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
14	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
444	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kneidl	Kneidnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Pravoslav	Pravoslav	k1gMnSc1
<g/>
:	:	kIx,
Z	z	k7c2
historie	historie	k1gFnSc2
evropské	evropský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1989	#num#	k4
</s>
<s>
VOIT	VOIT	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgInSc1d2
knihtisk	knihtisk	k1gInSc1
a	a	k8xC
příbuzné	příbuzný	k2eAgInPc1d1
obory	obor	k1gInPc1
mezi	mezi	k7c7
polovinou	polovina	k1gFnSc7
15	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátkem	počátkem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
1350	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Bibliotheca	Bibliothec	k2eAgFnSc1d1
Strahoviensis	Strahoviensis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Series	Series	k1gMnSc1
monographica	monographica	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
312	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VOIT	VOIT	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
knihtisk	knihtisk	k1gInSc1
mezi	mezi	k7c7
pozdní	pozdní	k2eAgFnSc7d1
gotikou	gotika	k1gFnSc7
a	a	k8xC
renesancí	renesance	k1gFnSc7
II	II	kA
:	:	kIx,
tiskaři	tiskař	k1gMnSc3
pro	pro	k7c4
víru	víra	k1gFnSc4
i	i	k9
tiskaři	tiskař	k1gMnPc1
pro	pro	k7c4
obrození	obrození	k1gNnSc4
národa	národ	k1gInSc2
1498	#num#	k4
<g/>
-	-	kIx~
<g/>
1547	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
:	:	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-200-2752-8	978-80-200-2752-8	k4
</s>
<s>
WINTER	Winter	k1gMnSc1
<g/>
,	,	kIx,
Zikmund	Zikmund	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
a	a	k8xC
obchod	obchod	k1gInSc1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
pro	pro	k7c4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Grafika	grafika	k1gFnSc1
a	a	k8xC
knihtisk	knihtisk	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
538	#num#	k4
<g/>
–	–	k?
<g/>
603	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Tiskařský	tiskařský	k2eAgInSc1d1
lis	lis	k1gInSc1
</s>
<s>
Hlubotisk	hlubotisk	k1gInSc1
</s>
<s>
Inkunábule	inkunábule	k1gFnSc1
</s>
<s>
Kniha	kniha	k1gFnSc1
</s>
<s>
Ofset	ofset	k1gInSc1
</s>
<s>
Polotón	polotón	k1gInSc1
</s>
<s>
Světlotisk	světlotisk	k1gInSc1
</s>
<s>
Štoček	štoček	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
knihtisk	knihtisk	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
knihtisk	knihtisk	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Knihtisk	knihtisk	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
typomil	typomil	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Global	globat	k5eAaImAgInS
spread	spread	k1gInSc1
of	of	k?
the	the	k?
printing	printing	k1gInSc1
press	press	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
