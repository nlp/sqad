<s>
Míčové	míčový	k2eAgFnPc1d1	Míčová
hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
míč	míč	k1gInSc1	míč
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
různé	různý	k2eAgInPc4d1	různý
kolektivní	kolektivní	k2eAgInPc4d1	kolektivní
a	a	k8xC	a
plážové	plážový	k2eAgInPc4d1	plážový
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
her	hra	k1gFnPc2	hra
nebo	nebo	k8xC	nebo
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
hraje	hrát	k5eAaImIp3nS	hrát
míč	míč	k1gInSc1	míč
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podobný	podobný	k2eAgInSc4d1	podobný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Míče	míč	k1gInPc1	míč
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc4d1	různá
velikosti	velikost	k1gFnPc4	velikost
a	a	k8xC	a
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
hrách	hra	k1gFnPc6	hra
mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
způsob	způsob	k1gInSc1	způsob
manipulace	manipulace	k1gFnSc2	manipulace
s	s	k7c7	s
míčem	míč	k1gInSc7	míč
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
<g/>
.	.	kIx.	.
florbal	florbal	k1gInSc1	florbal
basketbal	basketbal	k1gInSc1	basketbal
házená	házená	k1gFnSc1	házená
softball	softball	k1gInSc4	softball
stolní	stolní	k2eAgInSc1d1	stolní
tenis	tenis	k1gInSc1	tenis
tenis	tenis	k1gInSc1	tenis
volejbal	volejbal	k1gInSc4	volejbal
kopaná	kopaná	k1gFnSc1	kopaná
nohejbal	nohejbal	k1gInSc1	nohejbal
ringo	ringo	k6eAd1	ringo
frisbee	frisbee	k6eAd1	frisbee
crosball	crosbalnout	k5eAaPmAgInS	crosbalnout
americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
tchoukball	tchoukbalnout	k5eAaPmAgInS	tchoukbalnout
australský	australský	k2eAgInSc1d1	australský
fotbal	fotbal	k1gInSc1	fotbal
baseball	baseball	k1gInSc4	baseball
futsal	futsat	k5eAaPmAgInS	futsat
kriket	kriket	k1gInSc1	kriket
netball	netball	k1gInSc1	netball
pozemní	pozemní	k2eAgInSc1d1	pozemní
hokej	hokej	k1gInSc1	hokej
ragby	ragby	k1gNnSc1	ragby
squash	squash	k1gInSc1	squash
stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
tenis	tenis	k1gInSc4	tenis
vodní	vodní	k2eAgNnSc1d1	vodní
pólo	pólo	k1gNnSc1	pólo
badminton	badminton	k1gInSc4	badminton
bandy	banda	k1gFnSc2	banda
bowls	bowls	k6eAd1	bowls
hokejbal	hokejbal	k1gInSc4	hokejbal
kolová	kolová	k1gFnSc1	kolová
lakros	lakros	k1gInSc1	lakros
pelota	pelota	k1gFnSc1	pelota
plážový	plážový	k2eAgInSc1d1	plážový
tenis	tenis	k1gInSc4	tenis
pozemní	pozemní	k2eAgInSc4d1	pozemní
hokej	hokej	k1gInSc4	hokej
pólo	pólo	k1gNnSc4	pólo
ricochet	ricochet	k5eAaPmF	ricochet
softball	softball	k1gInSc4	softball
vybíjená	vybíjená	k1gFnSc1	vybíjená
kin-ball	kinall	k1gInSc1	kin-ball
</s>
