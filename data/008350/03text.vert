<p>
<s>
Dezertifikace	Dezertifikace	k1gFnSc1	Dezertifikace
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
degradace	degradace	k1gFnSc2	degradace
území	území	k1gNnSc2	území
na	na	k7c4	na
pouště	poušť	k1gFnPc4	poušť
a	a	k8xC	a
polopouště	polopoušť	k1gFnPc4	polopoušť
<g/>
.	.	kIx.	.
</s>
<s>
Způsobena	způsoben	k2eAgFnSc1d1	způsobena
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různými	různý	k2eAgInPc7d1	různý
globálními	globální	k2eAgInPc7d1	globální
klimatickými	klimatický	k2eAgInPc7d1	klimatický
jevy	jev	k1gInPc7	jev
<g/>
,	,	kIx,	,
přirozenými	přirozený	k2eAgInPc7d1	přirozený
i	i	k9	i
člověkem	člověk	k1gMnSc7	člověk
vyvolanými	vyvolaný	k2eAgFnPc7d1	vyvolaná
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
přímou	přímý	k2eAgFnSc7d1	přímá
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
či	či	k8xC	či
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
těsně	těsně	k6eAd1	těsně
sousedících	sousedící	k2eAgMnPc2d1	sousedící
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
změnou	změna	k1gFnSc7	změna
lokálního	lokální	k2eAgNnSc2d1	lokální
klimatu	klima	k1gNnSc2	klima
</s>
</p>
<p>
<s>
změna	změna	k1gFnSc1	změna
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
</s>
</p>
<p>
<s>
změna	změna	k1gFnSc1	změna
srážkového	srážkový	k2eAgInSc2d1	srážkový
či	či	k8xC	či
odtokového	odtokový	k2eAgInSc2d1	odtokový
režimu	režim	k1gInSc2	režim
oblasti	oblast	k1gFnSc2	oblast
</s>
</p>
<p>
<s>
zásahem	zásah	k1gInSc7	zásah
do	do	k7c2	do
přírodních	přírodní	k2eAgInPc2d1	přírodní
procesů	proces	k1gInPc2	proces
probíhajících	probíhající	k2eAgInPc2d1	probíhající
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
</s>
</p>
<p>
<s>
velké	velký	k2eAgNnSc1d1	velké
snížení	snížení	k1gNnSc1	snížení
vsaku	vsak	k1gInSc2	vsak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zemědělství	zemědělství	k1gNnSc2	zemědělství
</s>
</p>
<p>
<s>
rovnoběžné	rovnoběžný	k2eAgNnSc1d1	rovnoběžné
orání	orání	k1gNnSc1	orání
polí	pole	k1gFnPc2	pole
monokultur	monokultura	k1gFnPc2	monokultura
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
proteče	protéct	k5eAaPmIp3nS	protéct
a	a	k8xC	a
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
ji	on	k3xPp3gFnSc4	on
ani	ani	k9	ani
nižší	nízký	k2eAgFnSc1d2	nižší
vegetace	vegetace	k1gFnSc1	vegetace
</s>
</p>
<p>
<s>
zmizí	zmizet	k5eAaPmIp3nS	zmizet
vegetace	vegetace	k1gFnSc1	vegetace
pro	pro	k7c4	pro
zamořením	zamoření	k1gNnSc7	zamoření
kontaminanty	kontaminant	k1gInPc7	kontaminant
nebo	nebo	k8xC	nebo
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
</s>
</p>
<p>
<s>
těžba	těžba	k1gFnSc1	těžba
např.	např.	kA	např.
dřeva	dřevo	k1gNnSc2	dřevo
apod.	apod.	kA	apod.
naruší	narušit	k5eAaPmIp3nS	narušit
místní	místní	k2eAgNnSc1d1	místní
ekosystémSpolečným	ekosystémSpolečný	k2eAgInSc7d1	ekosystémSpolečný
prvkem	prvek	k1gInSc7	prvek
je	být	k5eAaImIp3nS	být
přetěžování	přetěžování	k1gNnSc1	přetěžování
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
spodní	spodní	k2eAgFnPc1d1	spodní
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
půda	půda	k1gFnSc1	půda
a	a	k8xC	a
zeleň	zeleň	k1gFnSc1	zeleň
dokáží	dokázat	k5eAaPmIp3nP	dokázat
obnovovat	obnovovat	k5eAaImF	obnovovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
dezertifikace	dezertifikace	k1gFnSc2	dezertifikace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
Sahary	Sahara	k1gFnSc2	Sahara
nebo	nebo	k8xC	nebo
vysychání	vysychání	k1gNnSc2	vysychání
Aralského	aralský	k2eAgNnSc2d1	Aralské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řešení	řešení	k1gNnSc1	řešení
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
desertifikací	desertifikace	k1gFnSc7	desertifikace
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
i	i	k9	i
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc6	on
zahájila	zahájit	k5eAaPmAgFnS	zahájit
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
projekt	projekt	k1gInSc4	projekt
–	–	k?	–
tzv.	tzv.	kA	tzv.
Velkou	velký	k2eAgFnSc4d1	velká
zelenou	zelený	k2eAgFnSc4d1	zelená
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
<g/>
Problému	problém	k1gInSc2	problém
desertifikace	desertifikace	k1gFnSc2	desertifikace
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
věnuje	věnovat	k5eAaPmIp3nS	věnovat
např.	např.	kA	např.
zimbabwský	zimbabwský	k2eAgMnSc1d1	zimbabwský
vědec	vědec	k1gMnSc1	vědec
Allan	Allan	k1gMnSc1	Allan
Savory	Savora	k1gFnPc4	Savora
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc4	výsledek
jehož	jehož	k3xOyRp3gFnSc2	jehož
práce	práce	k1gFnSc2	práce
boří	bořit	k5eAaImIp3nS	bořit
mýtus	mýtus	k1gInSc1	mýtus
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
dezertifikaci	dezertifikace	k1gFnSc4	dezertifikace
mohou	moct	k5eAaImIp3nP	moct
četná	četný	k2eAgNnPc1d1	četné
stáda	stádo	k1gNnPc1	stádo
dobytka	dobytek	k1gMnSc2	dobytek
–	–	k?	–
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
trus	trus	k1gInSc4	trus
a	a	k8xC	a
jimi	on	k3xPp3gMnPc7	on
udusaná	udusaný	k2eAgFnSc1d1	udusaná
nižší	nízký	k2eAgFnSc1d2	nižší
vegetace	vegetace	k1gFnSc1	vegetace
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
biologicky	biologicky	k6eAd1	biologicky
rozloženou	rozložený	k2eAgFnSc4d1	rozložená
vrstvu	vrstva	k1gFnSc4	vrstva
příznivou	příznivý	k2eAgFnSc4d1	příznivá
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
zeleň	zeleň	k1gFnSc4	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Burkině	Burkina	k1gFnSc6	Burkina
Faso	Faso	k6eAd1	Faso
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
levné	levný	k2eAgFnPc1d1	levná
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
metody	metoda	k1gFnPc1	metoda
jako	jako	k8xS	jako
zaï	zaï	k?	zaï
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgFnPc4d1	drobná
kompostovací	kompostovací	k2eAgFnPc4d1	kompostovací
jámy	jáma	k1gFnPc4	jáma
umožňující	umožňující	k2eAgFnSc4d1	umožňující
výsadbu	výsadba	k1gFnSc4	výsadba
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sahel	Sahel	k1gMnSc1	Sahel
</s>
</p>
<p>
<s>
Sucho	sucho	k6eAd1	sucho
</s>
</p>
<p>
<s>
Eroze	eroze	k1gFnSc1	eroze
</s>
</p>
<p>
<s>
Poušť	poušť	k1gFnSc1	poušť
</s>
</p>
<p>
<s>
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
voda	voda	k1gFnSc1	voda
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
OSN	OSN	kA	OSN
o	o	k7c6	o
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
desertifikaci	desertifikace	k1gFnSc3	desertifikace
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dezertifikace	Dezertifikace	k1gFnSc2	Dezertifikace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Environmental	Environmental	k1gMnSc1	Environmental
TEDtalk	TEDtalk	k1gMnSc1	TEDtalk
-	-	kIx~	-
Allan	Allan	k1gMnSc1	Allan
Savory	Savora	k1gFnSc2	Savora
<g/>
:	:	kIx,	:
How	How	k1gMnSc1	How
to	ten	k3xDgNnSc4	ten
reverse	revers	k1gInSc5	revers
climate	climat	k1gInSc5	climat
change	change	k1gFnPc4	change
by	by	kYmCp3nS	by
greening	greening	k1gInSc1	greening
the	the	k?	the
world	world	k1gInSc1	world
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
desserts	desserts	k1gInSc1	desserts
</s>
</p>
