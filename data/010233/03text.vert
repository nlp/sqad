<p>
<s>
Kalcitriol	Kalcitriol	k1gInSc1	Kalcitriol
(	(	kIx(	(
<g/>
též	též	k9	též
1,25	[number]	k4	1,25
<g/>
-dihydroxycholekalciferol	ihydroxycholekalciferola	k1gFnPc2	-dihydroxycholekalciferola
či	či	k8xC	či
1,25	[number]	k4	1,25
<g/>
-dihydroxyvitamín	ihydroxyvitamín	k1gInSc1	-dihydroxyvitamín
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
steroidní	steroidní	k2eAgInSc1d1	steroidní
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
vysoce	vysoce	k6eAd1	vysoce
aktivní	aktivní	k2eAgFnSc4d1	aktivní
formu	forma	k1gFnSc4	forma
vitamínu	vitamín	k1gInSc2	vitamín
D.	D.	kA	D.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
vitamínu	vitamín	k1gInSc2	vitamín
D3	D3	k1gMnSc2	D3
nejprve	nejprve	k6eAd1	nejprve
25	[number]	k4	25
<g/>
-hydroxylací	ydroxylace	k1gFnPc2	-hydroxylace
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
1	[number]	k4	1
<g/>
-hydroxylací	ydroxylace	k1gFnSc7	-hydroxylace
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
jmenovaný	jmenovaný	k2eAgInSc1d1	jmenovaný
krok	krok	k1gInSc1	krok
je	být	k5eAaImIp3nS	být
stimulován	stimulovat	k5eAaImNgInS	stimulovat
parathormonem	parathormon	k1gInSc7	parathormon
<g/>
,	,	kIx,	,
důležitým	důležitý	k2eAgInSc7d1	důležitý
regulátorem	regulátor	k1gInSc7	regulátor
vápníkového	vápníkový	k2eAgInSc2d1	vápníkový
metabolismu	metabolismus	k1gInSc2	metabolismus
produkovaným	produkovaný	k2eAgInSc7d1	produkovaný
v	v	k7c6	v
příštítných	příštítný	k2eAgMnPc6d1	příštítný
tělíscích	tělísek	k1gMnPc6	tělísek
<g/>
.	.	kIx.	.
<g/>
Kalcitriol	Kalcitriol	k1gInSc1	Kalcitriol
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zvyšování	zvyšování	k1gNnSc4	zvyšování
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
koncentrace	koncentrace	k1gFnSc2	koncentrace
vápníku	vápník	k1gInSc2	vápník
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
střevo	střevo	k1gNnSc4	střevo
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc4	ledvina
a	a	k8xC	a
kostní	kostní	k2eAgFnSc4d1	kostní
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střevu	střevo	k1gNnSc6	střevo
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tvorbu	tvorba	k1gFnSc4	tvorba
tzv.	tzv.	kA	tzv.
vápník	vápník	k1gInSc4	vápník
vázajícího	vázající	k2eAgInSc2d1	vázající
proteinu	protein	k1gInSc2	protein
(	(	kIx(	(
<g/>
calcium	calcium	k1gNnSc1	calcium
binding	binding	k1gInSc1	binding
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
calbindin	calbindin	k2eAgInSc1d1	calbindin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
vápník	vápník	k1gInSc4	vápník
ze	z	k7c2	z
střeva	střevo	k1gNnSc2	střevo
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
střevního	střevní	k2eAgInSc2d1	střevní
epitelu	epitel	k1gInSc2	epitel
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
následně	následně	k6eAd1	následně
vápník	vápník	k1gInSc1	vápník
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
zároveň	zároveň	k6eAd1	zároveň
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
absorpce	absorpce	k1gFnSc2	absorpce
fosforečnanů	fosforečnan	k1gInPc2	fosforečnan
ze	z	k7c2	z
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
kalcitriol	kalcitriol	k1gInSc1	kalcitriol
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnSc3d1	omezená
míře	míra	k1gFnSc3	míra
<g/>
)	)	kIx)	)
schopný	schopný	k2eAgInSc1d1	schopný
stimulovat	stimulovat	k5eAaImF	stimulovat
i	i	k9	i
reabsorbci	reabsorbce	k1gMnPc1	reabsorbce
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
jeho	jeho	k3xOp3gNnPc2	jeho
vylučování	vylučování	k1gNnPc2	vylučování
močí	moč	k1gFnSc7	moč
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
kostní	kostní	k2eAgFnSc4d1	kostní
tkáň	tkáň	k1gFnSc4	tkáň
-	-	kIx~	-
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgFnPc6d1	vysoká
koncentracích	koncentrace	k1gFnPc6	koncentrace
razantně	razantně	k6eAd1	razantně
odbourává	odbourávat	k5eAaImIp3nS	odbourávat
kosti	kost	k1gFnPc4	kost
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
stimulací	stimulace	k1gFnSc7	stimulace
transportu	transport	k1gInSc2	transport
vápníku	vápník	k1gInSc2	vápník
přes	přes	k7c4	přes
buněčné	buněčný	k2eAgFnPc4d1	buněčná
membrány	membrána	k1gFnPc4	membrána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
