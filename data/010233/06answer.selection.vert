<s>
Kalcitriol	Kalcitriol	k1gInSc1	Kalcitriol
(	(	kIx(	(
<g/>
též	též	k9	též
1,25	[number]	k4	1,25
<g/>
-dihydroxycholekalciferol	ihydroxycholekalciferola	k1gFnPc2	-dihydroxycholekalciferola
či	či	k8xC	či
1,25	[number]	k4	1,25
<g/>
-dihydroxyvitamín	ihydroxyvitamín	k1gInSc1	-dihydroxyvitamín
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
steroidní	steroidní	k2eAgInSc1d1	steroidní
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
vysoce	vysoce	k6eAd1	vysoce
aktivní	aktivní	k2eAgFnSc4d1	aktivní
formu	forma	k1gFnSc4	forma
vitamínu	vitamín	k1gInSc2	vitamín
D.	D.	kA	D.
</s>
