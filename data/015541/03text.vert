<s>
Solange	Solange	k6eAd1
Du	Du	k?
hier	hier	k1gMnSc1
bist	bist	k1gMnSc1
</s>
<s>
Solange	Solange	k1gFnSc1
Du	Du	k?
hier	hier	k1gInSc1
bist	bist	k2eAgInSc1d1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Solange	Solange	k1gFnSc1
Du	Du	k?
hier	hier	k1gInSc1
bist	bista	k1gFnPc2
Země	zem	k1gFnSc2
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
němčina	němčina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
77	#num#	k4
min	mina	k1gFnPc2
Žánr	žánr	k1gInSc4
</s>
<s>
drama	drama	k1gNnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Christina	Christin	k2eAgFnSc1d1
Freye	Freye	k1gFnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Stefan	Stefan	k1gMnSc1
Westerwelle	Westerwelle	k1gNnSc2
Obsazení	obsazení	k1gNnSc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
GempartLeander	GempartLeandero	k1gNnPc2
Lichti	Lichti	k1gNnSc7
Hudba	hudba	k1gFnSc1
</s>
<s>
Talking	Talking	k1gInSc1
HornsMartin	HornsMartin	k2eAgInSc4d1
Lesniak	Lesniak	k1gInSc4
Kamera	kamera	k1gFnSc1
</s>
<s>
Bernadette	Bernadette	k5eAaPmIp2nP
Paassen	Paassen	k2eAgInSc4d1
Střih	střih	k1gInSc4
</s>
<s>
Huynh	Huynh	k1gMnSc1
Trang	Trang	k1gMnSc1
Lam	lama	k1gFnPc2
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2006	#num#	k4
Solange	Solang	k1gFnSc2
Du	Du	k?
hier	hier	k1gMnSc1
bist	bist	k1gMnSc1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Solange	Solange	k1gFnSc1
Du	Du	k?
hier	hier	k1gInSc1
bist	bist	k1gInSc1
je	být	k5eAaImIp3nS
německý	německý	k2eAgInSc1d1
hraný	hraný	k2eAgInSc1d1
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
režíroval	režírovat	k5eAaImAgMnS
Stefan	Stefan	k1gMnSc1
Westerwelle	Westerwelle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
zachycuje	zachycovat	k5eAaImIp3nS
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
starším	starý	k2eAgMnSc7d2
mužem	muž	k1gMnSc7
a	a	k8xC
mladým	mladý	k2eAgMnSc7d1
prostitutem	prostitut	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
na	na	k7c6
filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
Febiofest	Febiofest	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Georg	Georg	k1gInSc1
je	být	k5eAaImIp3nS
už	už	k6eAd1
v	v	k7c6
důchodu	důchod	k1gInSc6
a	a	k8xC
žije	žít	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čas	čas	k1gInSc1
od	od	k7c2
času	čas	k1gInSc2
ho	on	k3xPp3gMnSc4
navštěvuje	navštěvovat	k5eAaImIp3nS
mladý	mladý	k2eAgMnSc1d1
prostitut	prostitut	k1gMnSc1
Sebastian	Sebastian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
Sebastian	Sebastian	k1gMnSc1
zůstane	zůstat	k5eAaPmIp3nS
u	u	k7c2
Georga	Georg	k1gMnSc2
přes	přes	k7c4
noc	noc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhého	druhý	k4xOgInSc2
dne	den	k1gInSc2
má	mít	k5eAaImIp3nS
Sebastian	Sebastian	k1gMnSc1
narozeniny	narozeniny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sebastian	Sebastian	k1gMnSc1
se	se	k3xPyFc4
Georgovi	Georg	k1gMnSc6
svěří	svěřit	k5eAaPmIp3nS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
zážitky	zážitek	k1gInPc7
z	z	k7c2
dětství	dětství	k1gNnSc2
a	a	k8xC
také	také	k9
Georg	Georg	k1gMnSc1
mu	on	k3xPp3gMnSc3
vypráví	vyprávět	k5eAaImIp3nS
o	o	k7c6
svém	své	k1gNnSc6
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
vztah	vztah	k1gInSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
jiné	jiný	k2eAgFnSc2d1
roviny	rovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sebastian	Sebastian	k1gMnSc1
nicméně	nicméně	k8xC
poté	poté	k6eAd1
odjíždí	odjíždět	k5eAaImIp3nS
definitivně	definitivně	k6eAd1
z	z	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Gempart	Gemparta	k1gFnPc2
</s>
<s>
Georg	Georg	k1gMnSc1
</s>
<s>
Leander	Leander	k1gInSc1
Lichti	Lichť	k1gFnSc2
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Záznam	záznam	k1gInSc1
v	v	k7c6
katalogu	katalog	k1gInSc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc4d1
zdroj	zdroj	k1gInSc4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
filmu	film	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
