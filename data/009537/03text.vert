<p>
<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Ottawa	Ottawa	k1gFnSc1	Ottawa
River	Rivra	k1gFnPc2	Rivra
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Riviè	Riviè	k1gMnSc4	Riviè
des	des	k1gNnSc2	des
Outaouais	Outaouais	k1gFnSc7	Outaouais
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Québec	Québec	k1gInSc4	Québec
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nP	tvořit
její	její	k3xOp3gFnSc4	její
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
provincií	provincie	k1gFnSc7	provincie
Ontario	Ontario	k1gNnSc4	Ontario
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
1120	[number]	k4	1120
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
115	[number]	k4	115
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Odtéká	odtékat	k5eAaImIp3nS	odtékat
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Lac	Lac	k1gFnSc2	Lac
des	des	k1gNnSc2	des
Outaouais	Outaouais	k1gFnSc2	Outaouais
v	v	k7c6	v
Laurentidách	Laurentida	k1gFnPc6	Laurentida
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
Québecu	Québecus	k1gInSc6	Québecus
<g/>
,	,	kIx,	,
asi	asi	k9	asi
250	[number]	k4	250
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
Ottawy	Ottawa	k1gFnSc2	Ottawa
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
západu	západ	k1gInSc3	západ
přes	přes	k7c4	přes
Réserve	Réserev	k1gFnPc4	Réserev
faunique	faunique	k1gInSc4	faunique
La	la	k1gNnSc1	la
Vérendrye	Vérendry	k1gInSc2	Vérendry
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgNnPc2d1	další
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Grand	grand	k1gMnSc1	grand
Lac	Lac	k1gMnSc2	Lac
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
Simard	Simard	k1gInSc1	Simard
<g/>
,	,	kIx,	,
Timiskaming	Timiskaming	k1gInSc1	Timiskaming
<g/>
)	)	kIx)	)
a	a	k8xC	a
místy	místy	k6eAd1	místy
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
peřeje	peřej	k1gFnPc4	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jezera	jezero	k1gNnSc2	jezero
Timiskaming	Timiskaming	k1gInSc1	Timiskaming
(	(	kIx(	(
<g/>
Lac	Lac	k1gFnSc1	Lac
Témiscamingue	Témiscamingu	k1gFnSc2	Témiscamingu
<g/>
)	)	kIx)	)
téměř	téměř	k6eAd1	téměř
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
Québecu	Québec	k2eAgFnSc4d1	Québec
s	s	k7c7	s
Ontariem	Ontario	k1gNnSc7	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
teče	téct	k5eAaImIp3nS	téct
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
u	u	k7c2	u
Mattawy	Mattawa	k1gFnSc2	Mattawa
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1	poslední
asi	asi	k9	asi
300	[number]	k4	300
km	km	kA	km
protéká	protékat	k5eAaImIp3nS	protékat
hustě	hustě	k6eAd1	hustě
osídlenou	osídlený	k2eAgFnSc7d1	osídlená
krajinou	krajina	k1gFnSc7	krajina
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
březích	břeh	k1gInPc6	břeh
tu	tu	k6eAd1	tu
leží	ležet	k5eAaImIp3nS	ležet
mj.	mj.	kA	mj.
města	město	k1gNnPc1	město
Pembroke	Pembroke	k1gFnSc1	Pembroke
<g/>
,	,	kIx,	,
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
,	,	kIx,	,
Hull	Hulla	k1gFnPc2	Hulla
a	a	k8xC	a
Gatineau	Gatineaus	k1gInSc2	Gatineaus
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
ústím	ústí	k1gNnSc7	ústí
vtéká	vtékat	k5eAaImIp3nS	vtékat
opět	opět	k6eAd1	opět
do	do	k7c2	do
québeckého	québecký	k2eAgNnSc2d1	québecký
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
Soutok	soutok	k1gInSc1	soutok
obou	dva	k4xCgFnPc2	dva
řek	řeka	k1gFnPc2	řeka
tvoří	tvořit	k5eAaImIp3nS	tvořit
soustava	soustava	k1gFnSc1	soustava
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc4	město
Montréal	Montréal	k1gMnSc1	Montréal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
2000	[number]	k4	2000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
k	k	k7c3	k
vodopádu	vodopád	k1gInSc3	vodopád
Chaudiè	Chaudiè	k1gFnSc2	Chaudiè
<g/>
.	.	kIx.	.
</s>
<s>
Plavební	plavební	k2eAgInSc1d1	plavební
kanál	kanál	k1gInSc1	kanál
Rideau	Rideaus	k1gInSc2	Rideaus
spojuje	spojovat	k5eAaImIp3nS	spojovat
řeku	řeka	k1gFnSc4	řeka
s	s	k7c7	s
Ontarijským	ontarijský	k2eAgNnSc7d1	Ontarijské
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc1	několik
přehradních	přehradní	k2eAgFnPc2d1	přehradní
nádrží	nádrž	k1gFnPc2	nádrž
(	(	kIx(	(
<g/>
Cabonga	Cabonga	k1gFnSc1	Cabonga
<g/>
,	,	kIx,	,
Dozois	Dozois	k1gFnSc1	Dozois
<g/>
,	,	kIx,	,
Decelles	Decelles	k1gInSc1	Decelles
<g/>
)	)	kIx)	)
s	s	k7c7	s
vodními	vodní	k2eAgFnPc7d1	vodní
elektrárnami	elektrárna	k1gFnPc7	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nS	ležet
města	město	k1gNnPc1	město
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
,	,	kIx,	,
Gatineau	Gatineaa	k1gFnSc4	Gatineaa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
О	О	k?	О
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ottawa	Ottawa	k1gFnSc1	Ottawa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ottawa	Ottawa	k1gFnSc1	Ottawa
River	Rivero	k1gNnPc2	Rivero
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
