<s>
Johann	Johann	k1gMnSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jazyce	jazyk	k1gInSc6	jazyk
uváděn	uvádět	k5eAaImNgMnS	uvádět
též	též	k9	též
jako	jako	k9	jako
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1667	[number]	k4	1667
Basilej	Basilej	k1gFnSc1	Basilej
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1748	[number]	k4	1748
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
bratrem	bratr	k1gMnSc7	bratr
Jacoba	Jacoba	k1gFnSc1	Jacoba
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
Daniela	Daniel	k1gMnSc2	Daniel
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
matematiky	matematika	k1gFnSc2	matematika
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
evropských	evropský	k2eAgMnPc2d1	evropský
vědců	vědec	k1gMnPc2	vědec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
dalšího	další	k2eAgMnSc2d1	další
slavného	slavný	k2eAgMnSc2d1	slavný
matematika	matematik	k1gMnSc2	matematik
Leonharda	Leonhard	k1gMnSc2	Leonhard
Eulera	Euler	k1gMnSc2	Euler
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
světu	svět	k1gInSc3	svět
řadu	řad	k1gInSc2	řad
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jeho	jeho	k3xOp3gMnPc1	jeho
prarodiče	prarodič	k1gMnPc1	prarodič
dříve	dříve	k6eAd2	dříve
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
Antverp	Antverpy	k1gFnPc2	Antverpy
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
vyznání	vyznání	k1gNnSc3	vyznání
(	(	kIx(	(
<g/>
byli	být	k5eAaImAgMnP	být
protestanty	protestant	k1gMnPc7	protestant
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Belgie	Belgie	k1gFnSc1	Belgie
byla	být	k5eAaImAgFnS	být
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
otce	otec	k1gMnSc2	otec
stát	stát	k5eAaImF	stát
obchodníkem	obchodník	k1gMnSc7	obchodník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
takové	takový	k3xDgNnSc4	takový
povolání	povolání	k1gNnSc4	povolání
ale	ale	k8xC	ale
syn	syn	k1gMnSc1	syn
neprojevoval	projevovat	k5eNaImAgMnS	projevovat
velké	velký	k2eAgNnSc4d1	velké
nadšení	nadšení	k1gNnSc4	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1683	[number]	k4	1683
mu	on	k3xPp3gMnSc3	on
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
zapsat	zapsat	k5eAaPmF	zapsat
se	se	k3xPyFc4	se
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
medicíny	medicína	k1gFnSc2	medicína
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Jacob	Jacoba	k1gFnPc2	Jacoba
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
jako	jako	k8xC	jako
profesor	profesor	k1gMnSc1	profesor
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Johann	Johann	k1gInSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
kurzy	kurz	k1gInPc4	kurz
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
jeho	on	k3xPp3gNnSc2	on
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
věnoval	věnovat	k5eAaImAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
matematiky	matematika	k1gFnSc2	matematika
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
si	se	k3xPyFc3	se
brzy	brzy	k6eAd1	brzy
osvojil	osvojit	k5eAaPmAgMnS	osvojit
tehdy	tehdy	k6eAd1	tehdy
žhavou	žhavý	k2eAgFnSc4d1	žhavá
matematickou	matematický	k2eAgFnSc4d1	matematická
novinku	novinka	k1gFnSc4	novinka
-	-	kIx~	-
Leibnizův	Leibnizův	k2eAgInSc4d1	Leibnizův
kalkulus	kalkulus	k1gInSc4	kalkulus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
už	už	k6eAd1	už
měl	mít	k5eAaImAgInS	mít
údajně	údajně	k6eAd1	údajně
plně	plně	k6eAd1	plně
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
Jacobovi	Jacobův	k2eAgMnPc1d1	Jacobův
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
matematických	matematický	k2eAgFnPc2d1	matematická
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
publikaci	publikace	k1gFnSc4	publikace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
procesu	proces	k1gInSc2	proces
fermentace	fermentace	k1gFnSc2	fermentace
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc4	její
náplň	náplň	k1gFnSc4	náplň
tedy	tedy	k9	tedy
ještě	ještě	k6eAd1	ještě
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
jeho	jeho	k3xOp3gNnSc3	jeho
původnímu	původní	k2eAgNnSc3d1	původní
zaměření	zaměření	k1gNnSc3	zaměření
na	na	k7c4	na
medicínu	medicína	k1gFnSc4	medicína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
přednášel	přednášet	k5eAaImAgMnS	přednášet
diferenciální	diferenciální	k2eAgInSc4d1	diferenciální
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1691	[number]	k4	1691
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navázal	navázat	k5eAaPmAgInS	navázat
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
markýzem	markýz	k1gMnSc7	markýz
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Hospitalem	Hospital	k1gMnSc7	Hospital
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
brzy	brzy	k6eAd1	brzy
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
Bernoulliovy	Bernoulliův	k2eAgFnPc4d1	Bernoulliova
kvality	kvalita	k1gFnPc4	kvalita
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
Bernoulli	Bernoulli	kA	Bernoulli
za	za	k7c4	za
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
roční	roční	k2eAgInSc4d1	roční
honorář	honorář	k1gInSc4	honorář
učil	učít	k5eAaPmAgInS	učít
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Hospitala	Hospital	k1gMnSc2	Hospital
novým	nový	k2eAgFnPc3d1	nová
Leibnizovým	Leibnizův	k2eAgFnPc3d1	Leibnizova
metodám	metoda	k1gFnPc3	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
vyučování	vyučování	k1gNnSc4	vyučování
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
wentil	wentit	k5eAaImAgMnS	wentit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
korespondenci	korespondence	k1gFnSc6	korespondence
s	s	k7c7	s
markýzem	markýz	k1gMnSc7	markýz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1696	[number]	k4	1696
vydal	vydat	k5eAaPmAgInS	vydat
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Hospital	Hospital	k1gMnSc1	Hospital
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
učebnici	učebnice	k1gFnSc4	učebnice
infinitezimálního	infinitezimální	k2eAgInSc2d1	infinitezimální
počtu	počet	k1gInSc2	počet
(	(	kIx(	(
<g/>
Analyse	analysa	k1gFnSc3	analysa
des	des	k1gNnSc2	des
infiniment	infiniment	k1gInSc4	infiniment
petits	petits	k6eAd1	petits
pour	pour	k1gMnSc1	pour
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
intelligence	intelligenec	k1gInSc2	intelligenec
des	des	k1gNnSc2	des
lignes	lignes	k1gMnSc1	lignes
courbes	courbes	k1gMnSc1	courbes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
Bernoulliových	Bernoulliový	k2eAgFnPc6d1	Bernoulliový
myšlenkách	myšlenka	k1gFnPc6	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Hospitalovi	Hospital	k1gMnSc3	Hospital
zprostředkovával	zprostředkovávat	k5eAaImAgMnS	zprostředkovávat
<g/>
.	.	kIx.	.
</s>
<s>
Bernoulli	Bernoulli	kA	Bernoulli
byl	být	k5eAaImAgInS	být
dotčen	dotknout	k5eAaPmNgMnS	dotknout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xS	jako
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Hospitalova	Hospitalův	k2eAgFnSc1d1	Hospitalův
práce	práce	k1gFnPc1	práce
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
zde	zde	k6eAd1	zde
víceméně	víceméně	k9	víceméně
vůbec	vůbec	k9	vůbec
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakého	jaký	k3yRgInSc2	jaký
zdroje	zdroj	k1gInSc2	zdroj
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
čerpá	čerpat	k5eAaImIp3nS	čerpat
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vydávání	vydávání	k1gNnSc3	vydávání
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c7	pod
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Hospitalovým	Hospitalův	k2eAgNnSc7d1	Hospitalovo
jménem	jméno	k1gNnSc7	jméno
začal	začít	k5eAaPmAgMnS	začít
ale	ale	k8xC	ale
ostře	ostro	k6eAd1	ostro
protestovat	protestovat	k5eAaBmF	protestovat
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
markýzově	markýzův	k2eAgFnSc6d1	markýzova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Příčinou	příčina	k1gFnSc7	příčina
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyplácení	vyplácení	k1gNnSc1	vyplácení
štědrého	štědrý	k2eAgInSc2d1	štědrý
honoráře	honorář	k1gInSc2	honorář
Bernoullimu	Bernoullim	k1gInSc2	Bernoullim
bylo	být	k5eAaImAgNnS	být
možná	možná	k9	možná
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
pomlčí	pomlčet	k5eAaPmIp3nS	pomlčet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nenašlo	najít	k5eNaPmAgNnS	najít
se	se	k3xPyFc4	se
ale	ale	k9	ale
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
skutečným	skutečný	k2eAgMnSc7d1	skutečný
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Věrohodně	věrohodně	k6eAd1	věrohodně
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgInP	najít
písemné	písemný	k2eAgInPc1d1	písemný
záznamy	záznam	k1gInPc1	záznam
dokazující	dokazující	k2eAgFnSc4d1	dokazující
Bernoulliovo	Bernoulliův	k2eAgNnSc4d1	Bernoulliův
autorství	autorství	k1gNnSc4	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Dorotheou	Dorothea	k1gFnSc7	Dorothea
Falknerovou	Falknerová	k1gFnSc7	Falknerová
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
z	z	k7c2	z
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
společensky	společensky	k6eAd1	společensky
situované	situovaný	k2eAgFnPc4d1	situovaná
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1694	[number]	k4	1694
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc4	doktorát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
své	svůj	k3xOyFgFnSc2	svůj
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
matematického	matematický	k2eAgInSc2d1	matematický
popisu	popis	k1gInSc2	popis
činnosti	činnost	k1gFnSc2	činnost
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
už	už	k6eAd1	už
udržoval	udržovat	k5eAaImAgInS	udržovat
korespondenci	korespondence	k1gFnSc4	korespondence
se	s	k7c7	s
samotným	samotný	k2eAgNnSc7d1	samotné
G.	G.	kA	G.
W.	W.	kA	W.
Leibnizem	Leibniz	k1gInSc7	Leibniz
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
temnou	temný	k2eAgFnSc7d1	temná
stránkou	stránka	k1gFnSc7	stránka
osobnosti	osobnost	k1gFnSc2	osobnost
Johanna	Johann	k1gMnSc2	Johann
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
jeho	jeho	k3xOp3gInPc1	jeho
rodinné	rodinný	k2eAgInPc1d1	rodinný
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Jacobem	Jacob	k1gMnSc7	Jacob
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
rapidně	rapidně	k6eAd1	rapidně
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jacob	Jacoba	k1gFnPc2	Jacoba
odmítal	odmítat	k5eAaImAgMnS	odmítat
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Rozpory	rozpor	k1gInPc1	rozpor
přerostly	přerůst	k5eAaPmAgInP	přerůst
až	až	k9	až
v	v	k7c4	v
otevřené	otevřený	k2eAgNnSc4d1	otevřené
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
navzájem	navzájem	k6eAd1	navzájem
napadali	napadat	k5eAaBmAgMnP	napadat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
často	často	k6eAd1	často
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k8xC	i
vzájemně	vzájemně	k6eAd1	vzájemně
kradli	krást	k5eAaImAgMnP	krást
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1695	[number]	k4	1695
už	už	k6eAd1	už
mělo	mít	k5eAaImAgNnS	mít
jméno	jméno	k1gNnSc1	jméno
Johanna	Johann	k1gMnSc2	Johann
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
věhlas	věhlas	k1gInSc1	věhlas
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
nabídnuta	nabídnut	k2eAgNnPc4d1	nabídnuto
profesorská	profesorský	k2eAgNnPc4d1	profesorské
křesla	křeslo	k1gNnPc4	křeslo
v	v	k7c6	v
Halle	Halla	k1gFnSc6	Halla
a	a	k8xC	a
v	v	k7c6	v
holandském	holandský	k2eAgInSc6d1	holandský
Groningenu	Groningen	k1gInSc6	Groningen
<g/>
.	.	kIx.	.
</s>
<s>
Bernoulli	Bernoulli	kA	Bernoulli
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
matematiky	matematika	k1gFnSc2	matematika
v	v	k7c6	v
Groningenu	Groningen	k1gInSc6	Groningen
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1695	[number]	k4	1695
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
nedávno	nedávno	k6eAd1	nedávno
narozeným	narozený	k2eAgMnSc7d1	narozený
synem	syn	k1gMnSc7	syn
(	(	kIx(	(
<g/>
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
)	)	kIx)	)
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
formuloval	formulovat	k5eAaImAgInS	formulovat
problém	problém	k1gInSc4	problém
brachistochrony	brachistochron	k1gInPc4	brachistochron
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
matematiky	matematik	k1gMnPc4	matematik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
vyřešili	vyřešit	k5eAaPmAgMnP	vyřešit
v	v	k7c6	v
co	co	k9	co
nejkratší	krátký	k2eAgFnSc6d3	nejkratší
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešený	k2eAgMnPc1d1	řešený
nalezli	nalézt	k5eAaBmAgMnP	nalézt
Johann	Johann	k1gInSc4	Johann
Bernoulli	Bernoulli	kA	Bernoulli
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leibniz	Leibniz	k1gMnSc1	Leibniz
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Hospital	Hospital	k1gMnSc1	Hospital
a	a	k8xC	a
Jacob	Jacoba	k1gFnPc2	Jacoba
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
.	.	kIx.	.
</s>
<s>
Johannovo	Johannův	k2eAgNnSc1d1	Johannův
řešení	řešení	k1gNnSc1	řešení
původně	původně	k6eAd1	původně
nebylo	být	k5eNaImAgNnS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
přivlastnit	přivlastnit	k5eAaPmF	přivlastnit
řešení	řešení	k1gNnSc4	řešení
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
jejich	jejich	k3xOp3gInPc1	jejich
vztahy	vztah	k1gInPc1	vztah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
nalezl	nalézt	k5eAaBmAgMnS	nalézt
správné	správný	k2eAgNnSc4d1	správné
řešení	řešení	k1gNnSc4	řešení
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Groningenu	Groningen	k1gInSc6	Groningen
se	se	k3xPyFc4	se
Johann	Johann	k1gInSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
často	často	k6eAd1	často
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
šarvátek	šarvátka	k1gFnPc2	šarvátka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ohledně	ohledně	k7c2	ohledně
náboženských	náboženský	k2eAgFnPc2d1	náboženská
a	a	k8xC	a
filozofických	filozofický	k2eAgFnPc2d1	filozofická
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
byl	být	k5eAaImAgMnS	být
obviňován	obviňovat	k5eAaImNgMnS	obviňovat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
popírá	popírat	k5eAaImIp3nS	popírat
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc4	zmrtvýchvstání
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
kvůli	kvůli	k7c3	kvůli
podporování	podporování	k1gNnSc3	podporování
karteziánské	karteziánský	k2eAgFnSc2d1	karteziánská
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
údajnému	údajný	k2eAgNnSc3d1	údajné
napadání	napadání	k1gNnSc3	napadání
kalvinistické	kalvinistický	k2eAgFnSc2d1	kalvinistická
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
přitom	přitom	k6eAd1	přitom
Bernoulli	Bernoulli	kA	Bernoulli
vyznával	vyznávat	k5eAaImAgInS	vyznávat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
právě	právě	k9	právě
kalvinismus	kalvinismus	k1gInSc4	kalvinismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
se	se	k3xPyFc4	se
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
umírajícího	umírající	k2eAgMnSc2d1	umírající
tchána	tchán	k1gMnSc2	tchán
vracel	vracet	k5eAaImAgMnS	vracet
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
Groningenu	Groningen	k1gInSc2	Groningen
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jacob	Jacoba	k1gFnPc2	Jacoba
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
podnikl	podniknout	k5eAaPmAgInS	podniknout
Johann	Johann	k1gInSc4	Johann
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
obsadit	obsadit	k5eAaPmF	obsadit
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
křeslo	křeslo	k1gNnSc4	křeslo
profesora	profesor	k1gMnSc2	profesor
matematiky	matematika	k1gFnSc2	matematika
na	na	k7c6	na
basilejské	basilejský	k2eAgFnSc6d1	Basilejská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
pak	pak	k6eAd1	pak
dostával	dostávat	k5eAaImAgInS	dostávat
nabídky	nabídka	k1gFnPc4	nabídka
i	i	k8xC	i
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
univerzit	univerzita	k1gFnPc2	univerzita
včetně	včetně	k7c2	včetně
nabídky	nabídka	k1gFnSc2	nabídka
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Groningenu	Groningen	k1gInSc2	Groningen
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
ale	ale	k9	ale
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1713	[number]	k4	1713
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
Newtonem	Newton	k1gMnSc7	Newton
a	a	k8xC	a
Leibnizem	Leibniz	k1gMnSc7	Leibniz
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
dříve	dříve	k6eAd2	dříve
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	se	k3xPyFc3	se
objevili	objevit	k5eAaPmAgMnP	objevit
kalkulus	kalkulus	k1gInSc4	kalkulus
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
prosadit	prosadit	k5eAaPmF	prosadit
každý	každý	k3xTgMnSc1	každý
svou	svůj	k3xOyFgFnSc4	svůj
verzi	verze	k1gFnSc4	verze
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
matematické	matematický	k2eAgFnSc3d1	matematická
disciplíně	disciplína	k1gFnSc3	disciplína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Johann	Johann	k1gInSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
jednoznačně	jednoznačně	k6eAd1	jednoznačně
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Leibnize	Leibnize	k1gFnSc2	Leibnize
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
podpoře	podpora	k1gFnSc3	podpora
získal	získat	k5eAaPmAgMnS	získat
Leibnizův	Leibnizův	k2eAgMnSc1d1	Leibnizův
kalkulus	kalkulus	k1gMnSc1	kalkulus
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
převahu	převaha	k1gFnSc4	převaha
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
podpora	podpora	k1gFnSc1	podpora
Leibnizových	Leibnizův	k2eAgFnPc2d1	Leibnizova
metod	metoda	k1gFnPc2	metoda
byla	být	k5eAaImAgFnS	být
opodstatněná	opodstatněný	k2eAgFnSc1d1	opodstatněná
a	a	k8xC	a
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
přínosná	přínosný	k2eAgFnSc1d1	přínosná
<g/>
,	,	kIx,	,
Bernoulli	Bernoulli	kA	Bernoulli
se	se	k3xPyFc4	se
ale	ale	k9	ale
později	pozdě	k6eAd2	pozdě
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
Newtonovi	Newton	k1gMnSc3	Newton
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
otázkách	otázka	k1gFnPc6	otázka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ovšem	ovšem	k9	ovšem
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
teorie	teorie	k1gFnSc2	teorie
byly	být	k5eAaImAgInP	být
správné	správný	k2eAgInPc1d1	správný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
de	de	k?	de
facto	facto	k1gNnSc1	facto
zdržel	zdržet	k5eAaPmAgInS	zdržet
přijetí	přijetí	k1gNnSc4	přijetí
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
kdysi	kdysi	k6eAd1	kdysi
tvrdě	tvrdě	k6eAd1	tvrdě
soupeřil	soupeřit	k5eAaImAgMnS	soupeřit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
totéž	týž	k3xTgNnSc1	týž
nastalo	nastat	k5eAaPmAgNnS	nastat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
jeho	jeho	k3xOp3gMnSc3	jeho
vlastnímu	vlastní	k2eAgMnSc3d1	vlastní
synovi	syn	k1gMnSc3	syn
Danielovi	Daniel	k1gMnSc3	Daniel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možná	možná	k9	možná
v	v	k7c6	v
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
excelentní	excelentní	k2eAgMnSc1d1	excelentní
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
zejména	zejména	k9	zejména
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
žárlil	žárlit	k5eAaImAgMnS	žárlit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Daniel	Daniel	k1gMnSc1	Daniel
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgMnS	ucházet
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Johann	Johann	k1gInSc1	Johann
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
vyhodil	vyhodit	k5eAaPmAgMnS	vyhodit
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
ukrást	ukrást	k5eAaPmF	ukrást
některé	některý	k3yIgInPc4	některý
synovy	synův	k2eAgInPc4d1	synův
objevy	objev	k1gInPc4	objev
a	a	k8xC	a
vydávat	vydávat	k5eAaPmF	vydávat
je	on	k3xPp3gMnPc4	on
za	za	k7c4	za
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
týká	týkat	k5eAaImIp3nS	týkat
Danielovy	Danielův	k2eAgFnPc4d1	Danielova
publikace	publikace	k1gFnPc4	publikace
Hydrodynamica	Hydrodynamicum	k1gNnSc2	Hydrodynamicum
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Bernoulli	Bernoulli	kA	Bernoulli
ji	on	k3xPp3gFnSc4	on
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1734	[number]	k4	1734
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
Hydraulica	Hydraulicus	k1gMnSc2	Hydraulicus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
ale	ale	k8xC	ale
datoval	datovat	k5eAaImAgMnS	datovat
1732	[number]	k4	1732
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
prvenství	prvenství	k1gNnSc4	prvenství
před	před	k7c7	před
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyla	být	k5eNaImAgFnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
publikována	publikovat	k5eAaBmNgFnS	publikovat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
Danielova	Danielův	k2eAgFnSc1d1	Danielova
Hydrodynamica	Hydrodynamica	k1gFnSc1	Hydrodynamica
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
stránky	stránka	k1gFnPc4	stránka
své	svůj	k3xOyFgFnSc2	svůj
povahy	povaha	k1gFnSc2	povaha
byl	být	k5eAaImAgInS	být
Johann	Johann	k1gInSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
bezpochyby	bezpochyby	k6eAd1	bezpochyby
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
matematiků	matematik	k1gMnPc2	matematik
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
učinili	učinit	k5eAaImAgMnP	učinit
rozhodující	rozhodující	k2eAgInPc4d1	rozhodující
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
rozvíjející	rozvíjející	k2eAgFnSc6d1	rozvíjející
se	se	k3xPyFc4	se
matematické	matematický	k2eAgFnSc3d1	matematická
analýze	analýza	k1gFnSc3	analýza
(	(	kIx(	(
<g/>
kalkulu	kalkul	k1gInSc3	kalkul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bernoulli	Bernoulli	kA	Bernoulli
nalezl	naleznout	k5eAaPmAgInS	naleznout
mnoho	mnoho	k4c4	mnoho
aplikací	aplikace	k1gFnPc2	aplikace
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
disciplíny	disciplína	k1gFnSc2	disciplína
a	a	k8xC	a
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
přijetí	přijetí	k1gNnSc3	přijetí
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
důležitých	důležitý	k2eAgInPc2d1	důležitý
výsledků	výsledek	k1gInPc2	výsledek
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
funkce	funkce	k1gFnSc2	funkce
popsané	popsaný	k2eAgFnSc2d1	popsaná
rovnicí	rovnice	k1gFnSc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
i	i	k9	i
zdokonalování	zdokonalování	k1gNnSc4	zdokonalování
metod	metoda	k1gFnPc2	metoda
námořní	námořní	k2eAgFnSc2d1	námořní
navigace	navigace	k1gFnSc2	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
získal	získat	k5eAaPmAgMnS	získat
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
akademií	akademie	k1gFnPc2	akademie
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
mladého	mladý	k2eAgMnSc4d1	mladý
Leonharda	Leonhard	k1gMnSc4	Leonhard
Eulera	Euler	k1gMnSc4	Euler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gMnSc7	jeho
žákem	žák	k1gMnSc7	žák
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
dalším	další	k2eAgMnSc7d1	další
brilantním	brilantní	k2eAgMnSc7d1	brilantní
matematikem	matematik	k1gMnSc7	matematik
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1748	[number]	k4	1748
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
"	"	kIx"	"
<g/>
Archimédes	Archimédes	k1gMnSc1	Archimédes
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
i	i	k9	i
vytesáno	vytesat	k5eAaPmNgNnS	vytesat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
náhrobním	náhrobní	k2eAgInSc6d1	náhrobní
kameni	kámen	k1gInSc6	kámen
<g/>
.	.	kIx.	.
</s>
