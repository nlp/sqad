<s>
Nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
reportérem	reportér	k1gMnSc7	reportér
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
William	William	k1gInSc4	William
Howard	Howarda	k1gFnPc2	Howarda
Russell	Russella	k1gFnPc2	Russella
z	z	k7c2	z
The	The	k1gFnSc2	The
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
novinář	novinář	k1gMnSc1	novinář
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
válečný	válečný	k2eAgMnSc1d1	válečný
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
