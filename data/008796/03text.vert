<p>
<s>
Stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
označovaný	označovaný	k2eAgInSc1d1	označovaný
ss	ss	k?	ss
nebo	nebo	k8xC	nebo
DC	DC	kA	DC
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
direct	direct	k2eAgInSc1d1	direct
current	current	k1gInSc1	current
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
obvodem	obvod	k1gInSc7	obvod
stále	stále	k6eAd1	stále
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
proudu	proud	k1gInSc2	proud
střídavého	střídavý	k2eAgInSc2d1	střídavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInSc2	zdroj
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Nejrozšířenější	rozšířený	k2eAgInPc1d3	nejrozšířenější
zdroje	zdroj	k1gInPc1	zdroj
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
galvanický	galvanický	k2eAgInSc1d1	galvanický
článek	článek	k1gInSc1	článek
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
o	o	k7c4	o
primární	primární	k2eAgInPc4d1	primární
články	článek	k1gInPc4	článek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tužková	tužkový	k2eAgFnSc1d1	tužková
baterie	baterie	k1gFnSc1	baterie
<g/>
)	)	kIx)	)
určené	určený	k2eAgFnPc1d1	určená
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
spotřebičů	spotřebič	k1gInPc2	spotřebič
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
příkonem	příkon	k1gInSc7	příkon
nebo	nebo	k8xC	nebo
články	článek	k1gInPc1	článek
sekundární	sekundární	k2eAgInPc1d1	sekundární
–	–	k?	–
akumulátory	akumulátor	k1gInPc1	akumulátor
–	–	k?	–
určené	určený	k2eAgFnSc2d1	určená
pro	pro	k7c4	pro
energeticky	energeticky	k6eAd1	energeticky
náročnější	náročný	k2eAgInPc4d2	náročnější
přenosné	přenosný	k2eAgInPc4d1	přenosný
spotřebiče	spotřebič	k1gInPc4	spotřebič
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
fotovoltaický	fotovoltaický	k2eAgInSc1d1	fotovoltaický
článek	článek	k1gInSc1	článek
–	–	k?	–
používaný	používaný	k2eAgInSc1d1	používaný
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
malých	malý	k2eAgInPc2d1	malý
kapesních	kapesní	k2eAgInPc2d1	kapesní
kalkulátorů	kalkulátor	k1gInPc2	kalkulátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
mohutných	mohutný	k2eAgFnPc2d1	mohutná
fotovoltaických	fotovoltaický	k2eAgFnPc2d1	fotovoltaická
elektráren	elektrárna	k1gFnPc2	elektrárna
</s>
</p>
<p>
<s>
termočlánek	termočlánek	k1gInSc1	termočlánek
–	–	k?	–
napájí	napájet	k5eAaImIp3nS	napájet
meziplanetární	meziplanetární	k2eAgFnPc4d1	meziplanetární
sondy	sonda	k1gFnPc4	sonda
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
radioizotopového	radioizotopový	k2eAgInSc2d1	radioizotopový
termoelektrického	termoelektrický	k2eAgInSc2d1	termoelektrický
generátoru	generátor	k1gInSc2	generátor
</s>
</p>
<p>
<s>
dynamo	dynamo	k1gNnSc1	dynamo
–	–	k?	–
dříve	dříve	k6eAd2	dříve
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
zdroj	zdroj	k1gInSc4	zdroj
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
zcela	zcela	k6eAd1	zcela
vytlačen	vytlačit	k5eAaPmNgInS	vytlačit
alternátorem	alternátor	k1gInSc7	alternátor
s	s	k7c7	s
usměrňovačem	usměrňovač	k1gInSc7	usměrňovač
</s>
</p>
<p>
<s>
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
získat	získat	k5eAaPmF	získat
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
ze	z	k7c2	z
střídavého	střídavý	k2eAgNnSc2d1	střídavé
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
síťového	síťový	k2eAgInSc2d1	síťový
proudu	proud	k1gInSc2	proud
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
byl	být	k5eAaImAgInS	být
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgInSc7	první
využívaným	využívaný	k2eAgInSc7d1	využívaný
druhem	druh	k1gInSc7	druh
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
rozšíření	rozšíření	k1gNnSc6	rozšíření
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgInS	zasloužit
svými	svůj	k3xOyFgInPc7	svůj
vynálezy	vynález	k1gInPc7	vynález
především	především	k6eAd1	především
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
obvodech	obvod	k1gInPc6	obvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
jeho	jeho	k3xOp3gFnSc7	jeho
vlastností	vlastnost	k1gFnSc7	vlastnost
–	–	k?	–
například	například	k6eAd1	například
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
součástky	součástka	k1gFnPc1	součástka
citlivé	citlivý	k2eAgFnPc1d1	citlivá
na	na	k7c4	na
směr	směr	k1gInSc4	směr
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
elektrolytický	elektrolytický	k2eAgInSc1d1	elektrolytický
kondenzátor	kondenzátor	k1gInSc1	kondenzátor
nebo	nebo	k8xC	nebo
tranzistor	tranzistor	k1gInSc1	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
elektrolýzu	elektrolýza	k1gFnSc4	elektrolýza
nebo	nebo	k8xC	nebo
galvanické	galvanický	k2eAgNnSc4d1	galvanické
pokovování	pokovování	k1gNnSc4	pokovování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
ekonomicky	ekonomicky	k6eAd1	ekonomicky
výhodnější	výhodný	k2eAgInSc1d2	výhodnější
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
schématech	schéma	k1gNnPc6	schéma
elektrických	elektrický	k2eAgInPc2d1	elektrický
obvodů	obvod	k1gInPc2	obvod
se	se	k3xPyFc4	se
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
označuje	označovat	k5eAaImIp3nS	označovat
=	=	kIx~	=
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
DC	DC	kA	DC
(	(	kIx(	(
<g/>
direct	direct	k1gMnSc1	direct
current	current	k1gMnSc1	current
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Směr	směr	k1gInSc1	směr
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Směr	směr	k1gInSc1	směr
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
domluven	domluvit	k5eAaPmNgMnS	domluvit
od	od	k7c2	od
kladného	kladný	k2eAgMnSc2d1	kladný
k	k	k7c3	k
zápornému	záporný	k2eAgInSc3d1	záporný
pólu	pól	k1gInSc3	pól
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
konvenční	konvenční	k2eAgInSc1d1	konvenční
směr	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
skutečný	skutečný	k2eAgInSc4d1	skutečný
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
částic	částice	k1gFnPc2	částice
nesoucích	nesoucí	k2eAgInPc2d1	nesoucí
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
složených	složený	k2eAgInPc6d1	složený
elektrických	elektrický	k2eAgInPc6d1	elektrický
obvodech	obvod	k1gInPc6	obvod
se	se	k3xPyFc4	se
směr	směr	k1gInSc1	směr
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
větvích	větev	k1gFnPc6	větev
určí	určit	k5eAaPmIp3nS	určit
pomocí	pomocí	k7c2	pomocí
Kirchhoffových	Kirchhoffův	k2eAgInPc2d1	Kirchhoffův
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
výkonu	výkon	k1gInSc2	výkon
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Elektřina	elektřina	k1gFnSc1	elektřina
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
</s>
</p>
<p>
<s>
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
indukce	indukce	k1gFnSc1	indukce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
proud	proud	k1gInSc4	proud
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
