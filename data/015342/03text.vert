<s>
Transport	transporta	k1gFnPc2
for	forum	k1gNnPc2
London	London	k1gMnSc1
</s>
<s>
Transport	transporta	k1gFnPc2
for	forum	k1gNnPc2
London	London	k1gMnSc1
Logo	logo	k1gNnSc4
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
autorita	autorita	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2000	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
London	London	k1gMnSc1
Regional	Regional	k1gFnSc2
Transport	transporta	k1gFnPc2
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Windsor	Windsor	k1gInSc4
House	house	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Charakteristika	charakteristikon	k1gNnSc2
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Greater	Greater	k1gMnSc1
London	London	k1gMnSc1
Authority	Authorita	k1gFnSc2
Dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Docklands	Docklands	k1gInSc1
Light	Light	k1gInSc1
Railway	Railwa	k2eAgInPc1d1
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc4
</s>
<s>
tfl	tfl	k?
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
uk	uk	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Správním	správní	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
zodpovědným	zodpovědný	k2eAgInSc7d1
za	za	k7c4
dopravu	doprava	k1gFnSc4
ve	v	k7c6
Velkém	velký	k2eAgInSc6d1
Londýně	Londýn	k1gInSc6
je	být	k5eAaImIp3nS
Transport	transport	k1gInSc1
for	forum	k1gNnPc2
London	London	k1gMnSc1
(	(	kIx(
<g/>
TfL	TfL	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
uskutečňovat	uskutečňovat	k5eAaImF
dopravní	dopravní	k2eAgFnSc4d1
strategii	strategie	k1gFnSc4
a	a	k8xC
řídit	řídit	k5eAaImF
dopravní	dopravní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
TfL	TfL	k1gMnSc1
nahradil	nahradit	k5eAaPmAgMnS
London	London	k1gMnSc1
Transport	transporta	k1gFnPc2
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
převzal	převzít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
většinu	většina	k1gFnSc4
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýnské	londýnský	k2eAgNnSc1d1
metro	metro	k1gNnSc1
bylo	být	k5eAaImAgNnS
převedeno	převést	k5eAaPmNgNnS
pod	pod	k7c4
jeho	jeho	k3xOp3gFnPc4
pravomoci	pravomoc	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
DLR	DLR	kA
Canary	Canara	k1gFnPc1
Wharf	Wharf	k1gMnSc1
</s>
<s>
TfL	TfL	k?
je	být	k5eAaImIp3nS
kontrolován	kontrolovat	k5eAaImNgInS
správní	správní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
členové	člen	k1gMnPc1
jsou	být	k5eAaImIp3nP
jmenováni	jmenovat	k5eAaImNgMnP,k5eAaBmNgMnP
starostou	starosta	k1gMnSc7
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
jejím	její	k3xOp3gMnSc7
předsedou	předseda	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
TfL	TfL	k1gMnSc1
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgMnS
na	na	k7c4
několik	několik	k4yIc4
funkčních	funkční	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
se	s	k7c7
zodpovědností	zodpovědnost	k1gFnSc7
za	za	k7c4
určitou	určitý	k2eAgFnSc4d1
část	část	k1gFnSc4
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Docklands	Docklands	k6eAd1
Light	Light	k1gInSc1
Railway	Railwaa	k1gFnSc2
(	(	kIx(
<g/>
DLR	DLR	kA
<g/>
)	)	kIx)
–	–	k?
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
bezobslužnou	bezobslužný	k2eAgFnSc4d1
lehkou	lehký	k2eAgFnSc4d1
železnici	železnice	k1gFnSc4
ve	v	k7c6
východním	východní	k2eAgInSc6d1
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
DLR	DLR	kA
provozována	provozovat	k5eAaImNgFnS
soukromou	soukromý	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
London	London	k1gMnSc1
Buses	Buses	k1gMnSc1
–	–	k?
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
řízení	řízení	k1gNnSc4
dopravy	doprava	k1gFnSc2
červenými	červený	k2eAgInPc7d1
linkovými	linkový	k2eAgInPc7d1
autobusy	autobus	k1gInPc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
Velkého	velký	k2eAgInSc2d1
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
sjednává	sjednávat	k5eAaImIp3nS
kontrakty	kontrakt	k1gInPc4
na	na	k7c4
provozování	provozování	k1gNnSc4
linek	linka	k1gFnPc2
soukromými	soukromý	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Doubledecker	Doubledecker	k1gMnSc1
</s>
<s>
London	London	k1gMnSc1
Dial-a-Ride	Dial-a-Rid	k1gInSc5
–	–	k?
poskytování	poskytování	k1gNnSc3
dopravy	doprava	k1gFnSc2
pro	pro	k7c4
paraplegiky	paraplegik	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
London	London	k1gMnSc1
Rail	Rail	k1gMnSc1
–	–	k?
zodpovědnost	zodpovědnost	k1gFnSc4
za	za	k7c4
koordinaci	koordinace	k1gFnSc4
s	s	k7c7
dopravci	dopravce	k1gMnPc7
provozujícími	provozující	k2eAgMnPc7d1
železniční	železniční	k2eAgFnSc7d1
dopravu	doprava	k1gFnSc4
-	-	kIx~
National	National	k1gFnSc1
Rail	Rail	k1gInSc1
(	(	kIx(
<g/>
označení	označení	k1gNnSc1
pro	pro	k7c4
systém	systém	k1gInSc4
služeb	služba	k1gFnPc2
poskytovaných	poskytovaný	k2eAgFnPc2d1
sdružením	sdružení	k1gNnSc7
soukromých	soukromý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
provozují	provozovat	k5eAaImIp3nP
železniční	železniční	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
v	v	k7c6
rozsahu	rozsah	k1gInSc6
původní	původní	k2eAgMnSc1d1
British	British	k1gMnSc1
Rail	Rail	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
oblasti	oblast	k1gFnSc6
Velkého	velký	k2eAgInSc2d1
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
London	London	k1gMnSc1
River	River	k1gMnSc1
Services	Services	k1gMnSc1
–	–	k?
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
vydávání	vydávání	k1gNnSc4
licencí	licence	k1gFnPc2
a	a	k8xC
koordinaci	koordinace	k1gFnSc4
dopravy	doprava	k1gFnSc2
po	po	k7c6
řece	řeka	k1gFnSc6
Temži	Temže	k1gFnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
London	London	k1gMnSc1
Street	Streeta	k1gFnPc2
Management	management	k1gInSc1
–	–	k?
zodpovídá	zodpovídat	k5eAaPmIp3nS,k5eAaImIp3nS
za	za	k7c4
správu	správa	k1gFnSc4
strategické	strategický	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
vozovek	vozovka	k1gFnPc2
a	a	k8xC
za	za	k7c4
London	London	k1gMnSc1
Congestion	Congestion	k1gInSc4
Charge	Charg	k1gInSc2
(	(	kIx(
<g/>
Poplatek	poplatek	k1gInSc1
za	za	k7c4
vjezd	vjezd	k1gInSc4
do	do	k7c2
vymezeného	vymezený	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
centra	centrum	k1gNnSc2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
vést	vést	k5eAaImF
ke	k	k7c3
snížení	snížení	k1gNnSc3
dopravy	doprava	k1gFnSc2
v	v	k7c6
centru	centrum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnosy	výnos	k1gInPc1
poplatku	poplatek	k1gInSc2
jsou	být	k5eAaImIp3nP
věnovány	věnovat	k5eAaImNgFnP,k5eAaPmNgFnP
na	na	k7c4
rozvoj	rozvoj	k1gInSc4
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
London	London	k1gMnSc1
Trams	Tramsa	k1gFnPc2
–	–	k?
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
řízení	řízení	k1gNnSc4
londýnské	londýnský	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
tramvajové	tramvajový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
systém	systém	k1gInSc1
Tramlink	Tramlink	k1gInSc1
</s>
<s>
Pohyblivé	pohyblivý	k2eAgNnSc1d1
schodiště	schodiště	k1gNnSc1
ve	v	k7c6
stanici	stanice	k1gFnSc6
metra	metro	k1gNnSc2
Waterloo	Waterloo	k1gNnSc1
station	station	k1gInSc1
</s>
<s>
London	London	k1gMnSc1
Underground	underground	k1gInSc1
–	–	k?
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
podzemní	podzemní	k2eAgFnSc4d1
kolejovou	kolejový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
označovanou	označovaný	k2eAgFnSc4d1
Tube	tubus	k1gInSc5
<g/>
,	,	kIx,
a	a	k8xC
řízení	řízení	k1gNnSc4
údržby	údržba	k1gFnSc2
prováděné	prováděný	k2eAgInPc1d1
soukromými	soukromý	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metro	metro	k1gNnSc1
je	být	k5eAaImIp3nS
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
12	#num#	k4
linek	linka	k1gFnPc2
a	a	k8xC
každá	každý	k3xTgFnSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
název	název	k1gInSc4
a	a	k8xC
identifikační	identifikační	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Metropolitan	metropolitan	k1gInSc1
–	–	k?
purpurová	purpurový	k2eAgFnSc5d1
</s>
<s>
Piccadilly	Piccadilla	k1gFnPc1
–	–	k?
tmavě	tmavě	k6eAd1
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
District	District	k1gInSc1
–	–	k?
zelená	zelenat	k5eAaImIp3nS
</s>
<s>
Victoria	Victorium	k1gNnSc2
–	–	k?
světle	světle	k6eAd1
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
East	East	k2eAgMnSc1d1
London	London	k1gMnSc1
–	–	k?
oranžová	oranžový	k2eAgFnSc1d1
</s>
<s>
Central	Centrat	k5eAaPmAgMnS,k5eAaImAgMnS
–	–	k?
červená	červenat	k5eAaImIp3nS
</s>
<s>
Circle	Circle	k1gFnSc1
–	–	k?
žlutá	žlutat	k5eAaImIp3nS
</s>
<s>
Hammersmith	Hammersmith	k1gInSc1
&	&	k?
City	City	k1gFnSc2
–	–	k?
růžová	růžový	k2eAgFnSc1d1
</s>
<s>
Waterloo	Waterloo	k1gNnSc1
&	&	k?
City	City	k1gFnSc2
–	–	k?
tyrkysová	tyrkysový	k2eAgFnSc1d1
</s>
<s>
Jubilee	Jubilee	k1gFnSc1
–	–	k?
šedá	šedat	k5eAaImIp3nS
</s>
<s>
Northern	Northern	k1gInSc1
–	–	k?
černá	černat	k5eAaImIp3nS
</s>
<s>
Bakerloo	Bakerloo	k6eAd1
–	–	k?
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
Public	publicum	k1gNnPc2
Carriage	Carriag	k1gMnSc2
Office	Office	kA
–	–	k?
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
vydávání	vydávání	k1gNnSc4
licencí	licence	k1gFnPc2
pro	pro	k7c4
provozování	provozování	k1gNnSc4
pověstných	pověstný	k2eAgNnPc2d1
černých	černé	k1gNnPc2
taxi	taxi	k1gNnSc2
a	a	k8xC
půjčování	půjčování	k1gNnSc2
jiných	jiný	k2eAgNnPc2d1
soukromých	soukromý	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Victoria	Victorium	k1gNnPc4
Coach	Coach	k1gInSc1
Station	station	k1gInSc1
–	–	k?
vlastní	vlastnit	k5eAaImIp3nS
a	a	k8xC
provozuje	provozovat	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc4d1
londýnské	londýnský	k2eAgNnSc4d1
stanoviště	stanoviště	k1gNnSc4
dálkových	dálkový	k2eAgInPc2d1
autobusů	autobus	k1gInPc2
a	a	k8xC
autokarů	autokar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Každá	každý	k3xTgFnSc1
výše	výše	k1gFnSc1
zmiňovaná	zmiňovaný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
firemní	firemní	k2eAgFnSc4d1
značku	značka	k1gFnSc4
<g/>
,	,	kIx,
tvořenou	tvořený	k2eAgFnSc7d1
specificky	specificky	k6eAd1
barevnou	barevný	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
standardního	standardní	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
doplněnou	doplněná	k1gFnSc7
o	o	k7c4
příslušný	příslušný	k2eAgInSc4d1
nápis	nápis	k1gInSc4
v	v	k7c6
příčném	příčný	k2eAgInSc6d1
pruhu	pruh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kruh	kruh	k1gInSc1
modré	modrý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
bez	bez	k7c2
nápisu	nápis	k1gInSc2
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
TfL	TfL	k1gFnSc2
jako	jako	k8xC,k8xS
celku	celek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jízdné	jízdný	k2eAgNnSc1d1
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
články	článek	k1gInPc1
o	o	k7c6
systému	systém	k1gInSc6
jízdenek	jízdenka	k1gFnPc2
v	v	k7c6
londýnské	londýnský	k2eAgFnSc6d1
městské	městský	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
–	–	k?
Travelcard	Travelcarda	k1gFnPc2
a	a	k8xC
Oyster	Oystra	k1gFnPc2
card	card	k6eAd1
</s>
<s>
Většina	většina	k1gFnSc1
typů	typ	k1gInPc2
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
kontroluje	kontrolovat	k5eAaImIp3nS
TfL	TfL	k1gFnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
sazebník	sazebník	k1gInSc4
jízdného	jízdné	k1gNnSc2
a	a	k8xC
své	svůj	k3xOyFgFnSc2
vlastní	vlastní	k2eAgFnSc2d1
jízdenky	jízdenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
jsou	být	k5eAaImIp3nP
dvojice	dvojice	k1gFnPc4
autobusy	autobus	k1gInPc4
a	a	k8xC
tramvaje	tramvaj	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
metrem	metro	k1gNnSc7
a	a	k8xC
DLR	DLR	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
sloučené	sloučený	k2eAgNnSc4d1
jízdné	jízdné	k1gNnSc4
a	a	k8xC
společné	společný	k2eAgInPc4d1
jízdní	jízdní	k2eAgInPc4d1
doklady	doklad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Nadstavbou	nadstavba	k1gFnSc7
tohoto	tento	k3xDgInSc2
odvětvového	odvětvový	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
účtování	účtování	k1gNnSc1
jízdného	jízdné	k1gNnSc2
je	být	k5eAaImIp3nS
Travelcard	Travelcard	k1gInSc1
–	–	k?
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nabízí	nabízet	k5eAaImIp3nS
zónové	zónový	k2eAgFnPc4d1
jízdenky	jízdenka	k1gFnPc4
s	s	k7c7
platností	platnost	k1gFnSc7
od	od	k7c2
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
do	do	k7c2
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
a	a	k8xC
variantu	varianta	k1gFnSc4
jízdenky	jízdenka	k1gFnSc2
platné	platný	k2eAgFnSc2d1
mimo	mimo	k7c4
dopravní	dopravní	k2eAgFnSc4d1
špičku	špička	k1gFnSc4
(	(	kIx(
<g/>
off-peak	off-peak	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
jízdenky	jízdenka	k1gFnPc1
jsou	být	k5eAaImIp3nP
platné	platný	k2eAgInPc1d1
v	v	k7c6
DLR	DLR	kA
<g/>
,	,	kIx,
autobusech	autobus	k1gInPc6
<g/>
,	,	kIx,
vlacích	vlak	k1gInPc6
<g/>
,	,	kIx,
tramvajích	tramvaj	k1gFnPc6
<g/>
,	,	kIx,
metru	metro	k1gNnSc6
a	a	k8xC
na	na	k7c6
některých	některý	k3yIgFnPc6
trasách	trasa	k1gFnPc6
říční	říční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
londýnské	londýnský	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
začal	začít	k5eAaPmAgInS
zavádět	zavádět	k5eAaImF
nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
bezkontaktních	bezkontaktní	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
–	–	k?
Oyster	Oyster	k1gInSc1
card	carda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
použít	použít	k5eAaPmF
jako	jako	k9
elektronické	elektronický	k2eAgFnPc4d1
peněženky	peněženka	k1gFnPc4
na	na	k7c4
platbu	platba	k1gFnSc4
jednotlivého	jednotlivý	k2eAgNnSc2d1
jízdného	jízdné	k1gNnSc2
nebo	nebo	k8xC
v	v	k7c6
nich	on	k3xPp3gFnPc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
nahrány	nahrát	k5eAaPmNgFnP,k5eAaBmNgFnP
různé	různý	k2eAgFnPc1d1
zónové	zónový	k2eAgFnPc1d1
jízdenky	jízdenka	k1gFnPc1
systému	systém	k1gInSc2
Travelcard	Travelcard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaje	údaj	k1gInSc2
z	z	k7c2
této	tento	k3xDgFnSc2
karty	karta	k1gFnSc2
jsou	být	k5eAaImIp3nP
snímány	snímat	k5eAaImNgInP
přiložením	přiložení	k1gNnSc7
karty	karta	k1gFnSc2
na	na	k7c4
čtečku	čtečka	k1gFnSc4
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
u	u	k7c2
vstupu	vstup	k1gInSc2
do	do	k7c2
každé	každý	k3xTgFnSc2
stanice	stanice	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
jinak	jinak	k6eAd1
potřeba	potřeba	k6eAd1
vložit	vložit	k5eAaPmF
papírovou	papírový	k2eAgFnSc4d1
jízdenku	jízdenka	k1gFnSc4
aby	aby	kYmCp3nS
se	se	k3xPyFc4
otevřel	otevřít	k5eAaPmAgInS
průchod	průchod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
TfL	TfL	k?
vyvinula	vyvinout	k5eAaPmAgFnS
vlastní	vlastní	k2eAgFnSc4d1
plánovač	plánovač	k1gMnSc1
jízd	jízda	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
naplánovat	naplánovat	k5eAaBmF
cestu	cesta	k1gFnSc4
různými	různý	k2eAgInPc7d1
dopravními	dopravní	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
jeho	jeho	k3xOp3gNnSc6
blízkém	blízký	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
na	na	k7c4
jeho	jeho	k3xOp3gInPc4
WWW	WWW	kA
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
kioscích	kiosek	k1gInPc6
nebo	nebo	k8xC
prostřednictvím	prostřednictvím	k7c2
některých	některý	k3yIgFnPc2
telefonních	telefonní	k2eAgFnPc2d1
budek	budka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Muzeum	muzeum	k1gNnSc1
</s>
<s>
TfL	TfL	k?
vlastní	vlastnit	k5eAaImIp3nS
a	a	k8xC
provozuje	provozovat	k5eAaImIp3nS
Londýnské	londýnský	k2eAgNnSc1d1
dopravní	dopravní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
(	(	kIx(
<g/>
London	London	k1gMnSc1
Transport	transport	k1gInSc4
Museum	museum	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c4
Covent	Covent	k1gInSc4
Garden	Gardna	k1gFnPc2
–	–	k?
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
zachovává	zachovávat	k5eAaImIp3nS
a	a	k8xC
vysvětluje	vysvětlovat	k5eAaImIp3nS
dopravní	dopravní	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
zavřeno	zavřít	k5eAaPmNgNnS
pro	pro	k7c4
rekonstrukci	rekonstrukce	k1gFnSc4
a	a	k8xC
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
znovu	znovu	k6eAd1
otevřeno	otevřít	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
má	mít	k5eAaImIp3nS
rovněž	rovněž	k9
své	svůj	k3xOyFgInPc4
depozitáře	depozitář	k1gInPc4
v	v	k7c6
Actonu	Acton	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
materiály	materiál	k1gInPc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
nejsou	být	k5eNaImIp3nP
běžně	běžně	k6eAd1
vystavovány	vystavován	k2eAgInPc1d1
<g/>
;	;	kIx,
bývají	bývat	k5eAaImIp3nP
otevřeny	otevřít	k5eAaPmNgFnP
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
do	do	k7c2
roka	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Transport	transporta	k1gFnPc2
for	forum	k1gNnPc2
London	London	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.tfl.gov.uk/	http://www.tfl.gov.uk/	k?
-	-	kIx~
WWW	WWW	kA
stránky	stránka	k1gFnSc2
Transport	transporta	k1gFnPc2
for	forum	k1gNnPc2
London	London	k1gMnSc1
</s>
<s>
http://www.tfl.gov.uk/journeyplanner	http://www.tfl.gov.uk/journeyplanner	k1gMnSc1
-	-	kIx~
Plánovač	plánovač	k1gMnSc1
jízd	jízda	k1gFnPc2
TfL	TfL	k1gMnSc1
</s>
<s>
https://web.archive.org/web/20071212022106/http://www.cclondon.com/	https://web.archive.org/web/20071212022106/http://www.cclondon.com/	k4
-	-	kIx~
WWW	WWW	kA
stránky	stránka	k1gFnPc1
London	London	k1gMnSc1
Congestion	Congestion	k1gInSc4
Charge	Charg	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Londýnská	londýnský	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
Transport	transporta	k1gFnPc2
for	forum	k1gNnPc2
London	London	k1gMnSc1
</s>
<s>
Metro	metro	k1gNnSc1
•	•	k?
DLR	DLR	kA
•	•	k?
Městské	městský	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
•	•	k?
Tramvaje	tramvaj	k1gFnSc2
•	•	k?
Železnice	železnice	k1gFnSc1
•	•	k?
Victoria	Victorium	k1gNnSc2
Coach	Coacha	k1gFnPc2
Station	station	k1gInSc4
•	•	k?
Říční	říční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
•	•	k?
Taxi	taxe	k1gFnSc4
•	•	k?
Doprava	doprava	k1gFnSc1
paraplegiků	paraplegik	k1gMnPc2
•	•	k?
Správa	správa	k1gFnSc1
silnic	silnice	k1gFnPc2
•	•	k?
London	London	k1gMnSc1
Congestion	Congestion	k1gInSc4
Charge	Charge	k1gNnSc2
Jízdenky	jízdenka	k1gFnSc2
</s>
<s>
Travelcard	Travelcard	k1gInSc1
•	•	k?
Oyster	Oyster	k1gInSc1
card	card	k1gInSc1
Ostatní	ostatní	k2eAgInPc1d1
typy	typ	k1gInPc1
dopravy	doprava	k1gFnSc2
</s>
<s>
Heathrow	Heathrow	k?
Express	express	k1gInSc1
•	•	k?
Gatwick	Gatwick	k1gInSc1
Express	express	k1gInSc1
•	•	k?
Stansted	Stansted	k1gInSc1
Express	express	k1gInSc1
•	•	k?
Eurostar	Eurostar	k1gInSc1
•	•	k?
National	National	k1gFnSc2
Rail	Raila	k1gFnPc2
Letiště	letiště	k1gNnSc2
</s>
<s>
Heathrow	Heathrow	k?
•	•	k?
Gatwick	Gatwick	k1gMnSc1
•	•	k?
Stansted	Stansted	k1gMnSc1
•	•	k?
City	City	k1gFnSc2
•	•	k?
Luton	Luton	k1gMnSc1
•	•	k?
Biggin	Biggin	k1gMnSc1
Hill	Hill	k1gMnSc1
•	•	k?
Southend	Southend	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
5559066-4	5559066-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2230	#num#	k4
1005	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2003123082	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
153411703	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2003123082	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Doprava	doprava	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
