<p>
<s>
Terbium	terbium	k1gNnSc1	terbium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Tb	Tb	k1gFnSc2	Tb
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Terbium	terbium	k1gNnSc1	terbium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
vnitřně	vnitřně	k6eAd1	vnitřně
přechodný	přechodný	k2eAgInSc1d1	přechodný
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
devátý	devátý	k4xOgInSc1	devátý
člen	člen	k1gInSc1	člen
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nP	nacházet
využití	využití	k1gNnSc4	využití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
pro	pro	k7c4	pro
elektroniku	elektronika	k1gFnSc4	elektronika
a	a	k8xC	a
barevných	barevný	k2eAgInPc2d1	barevný
luminoforů	luminofor	k1gInPc2	luminofor
pro	pro	k7c4	pro
televizní	televizní	k2eAgFnPc4d1	televizní
obrazovky	obrazovka	k1gFnPc4	obrazovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Terbium	terbium	k1gNnSc1	terbium
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgInSc1d1	měkký
přechodný	přechodný	k2eAgInSc1d1	přechodný
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
terbium	terbium	k1gNnSc4	terbium
méně	málo	k6eAd2	málo
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
než	než	k8xS	než
předchozí	předchozí	k2eAgInPc4d1	předchozí
prvky	prvek	k1gInPc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
oxidu	oxid	k1gInSc2	oxid
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
terbium	terbium	k1gNnSc1	terbium
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Tb	Tb	k1gFnSc2	Tb
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
soli	sůl	k1gFnPc1	sůl
Tb	Tb	k1gFnPc1	Tb
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
jsou	být	k5eAaImIp3nP	být
nestálé	stálý	k2eNgInPc1d1	nestálý
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k9	jen
za	za	k7c2	za
extrémních	extrémní	k2eAgFnPc2d1	extrémní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
Tb	Tb	k1gFnPc2	Tb
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podobné	podobný	k2eAgFnPc4d1	podobná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
ostatních	ostatní	k2eAgInPc2d1	ostatní
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgInPc1d1	stabilní
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nereagují	reagovat	k5eNaBmIp3nP	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
především	především	k9	především
fluoridy	fluorid	k1gInPc1	fluorid
a	a	k8xC	a
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
nerozpustnost	nerozpustnost	k1gFnSc1	nerozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Terbité	Terbitý	k2eAgFnPc1d1	Terbitý
soli	sůl	k1gFnPc1	sůl
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Terbium	terbium	k1gNnSc4	terbium
objevil	objevit	k5eAaPmAgMnS	objevit
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
Carl	Carl	k1gMnSc1	Carl
Gustaf	Gustaf	k1gMnSc1	Gustaf
Mosander	Mosander	k1gMnSc1	Mosander
jako	jako	k8xC	jako
nečistotu	nečistota	k1gFnSc4	nečistota
ve	v	k7c6	v
zkoumaném	zkoumaný	k2eAgInSc6d1	zkoumaný
oxidu	oxid	k1gInSc6	oxid
yttritém	yttritý	k2eAgInSc6d1	yttritý
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
dostalo	dostat	k5eAaPmAgNnS	dostat
terbium	terbium	k1gNnSc4	terbium
podle	podle	k7c2	podle
švédské	švédský	k2eAgFnSc2d1	švédská
vesnice	vesnice	k1gFnSc2	vesnice
Ytterby	Ytterba	k1gFnSc2	Ytterba
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
i	i	k9	i
název	název	k1gInSc1	název
pro	pro	k7c4	pro
ytterbium	ytterbium	k1gNnSc4	ytterbium
<g/>
,	,	kIx,	,
yttrium	yttrium	k1gNnSc4	yttrium
a	a	k8xC	a
erbium	erbium	k1gNnSc4	erbium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Terbium	terbium	k1gNnSc1	terbium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
0,9	[number]	k4	0,9
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
obsahu	obsah	k1gInSc6	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
údaje	údaj	k1gInPc1	údaj
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
terbia	terbium	k1gNnSc2	terbium
na	na	k7c4	na
600	[number]	k4	600
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
terbium	terbium	k1gNnSc1	terbium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
ani	ani	k9	ani
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
lanthanoidy	lanthanoida	k1gFnPc1	lanthanoida
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
minerály	minerál	k1gInPc4	minerál
směsné	směsný	k2eAgInPc4d1	směsný
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
monazity	monazit	k1gInPc1	monazit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Th	Th	k1gFnSc1	Th
<g/>
,	,	kIx,	,
Nd	Nd	k1gFnSc1	Nd
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bastnäsity	bastnäsit	k1gInPc1	bastnäsit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
F	F	kA	F
–	–	k?	–
směsné	směsný	k2eAgInPc4d1	směsný
flourouhličitany	flourouhličitan	k1gInPc4	flourouhličitan
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
např.	např.	kA	např.
minerál	minerál	k1gInSc1	minerál
euxenit	euxenit	k5eAaImF	euxenit
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
<g/>
Th	Th	k1gFnSc1	Th
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
Ti	ty	k3xPp2nSc3	ty
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
ve	v	k7c4	v
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
suroviny	surovina	k1gFnPc1	surovina
–	–	k?	–
apatity	apatit	k1gInPc1	apatit
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
ohlášen	ohlášet	k5eAaImNgInS	ohlášet
nález	nález	k1gInSc1	nález
ložiska	ložisko	k1gNnSc2	ložisko
bohatého	bohatý	k2eAgNnSc2d1	bohaté
na	na	k7c6	na
yttrium	yttrium	k1gNnSc4	yttrium
<g/>
,	,	kIx,	,
dysprosium	dysprosium	k1gNnSc4	dysprosium
<g/>
,	,	kIx,	,
europium	europium	k1gNnSc4	europium
a	a	k8xC	a
terbium	terbium	k1gNnSc4	terbium
poblíž	poblíž	k7c2	poblíž
japonského	japonský	k2eAgInSc2d1	japonský
ostrůvku	ostrůvek	k1gInSc2	ostrůvek
Minamitori	Minamitor	k1gFnSc2	Minamitor
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1	[number]	k4	1
850	[number]	k4	850
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezené	omezený	k2eAgFnSc3d1	omezená
dostupnosti	dostupnost	k1gFnSc3	dostupnost
hrozí	hrozit	k5eAaImIp3nS	hrozit
v	v	k7c6	v
nejbližších	blízký	k2eAgInPc6d3	Nejbližší
letech	let	k1gInPc6	let
kritický	kritický	k2eAgInSc1d1	kritický
nedostatek	nedostatek	k1gInSc1	nedostatek
zdrojů	zdroj	k1gInPc2	zdroj
prvku	prvek	k1gInSc2	prvek
pro	pro	k7c4	pro
technologické	technologický	k2eAgNnSc4d1	Technologické
využití	využití	k1gNnSc4	využití
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc1d1	uvedený
nález	nález	k1gInSc1	nález
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgInPc2d1	vzácný
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
rudy	ruda	k1gFnPc1	ruda
nejprve	nejprve	k6eAd1	nejprve
louží	loužit	k5eAaImIp3nP	loužit
směsí	směs	k1gFnSc7	směs
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
ze	z	k7c2	z
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
roztoku	roztok	k1gInSc2	roztok
solí	solit	k5eAaImIp3nS	solit
se	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Separace	separace	k1gFnSc1	separace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgInPc2d1	různý
postupů	postup	k1gInPc2	postup
–	–	k?	–
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
extrakcí	extrakce	k1gFnSc7	extrakce
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
ionexových	ionexový	k2eAgFnPc2d1	ionexová
kolon	kolona	k1gFnPc2	kolona
nebo	nebo	k8xC	nebo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
srážením	srážení	k1gNnSc7	srážení
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
komplexních	komplexní	k2eAgFnPc2d1	komplexní
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
terbitého	terbitý	k2eAgMnSc4d1	terbitý
Tb	Tb	k1gMnSc4	Tb
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
elementárním	elementární	k2eAgInSc7d1	elementární
vápníkem	vápník	k1gInSc7	vápník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tb	Tb	k?	Tb
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
Ca	ca	kA	ca
→	→	k?	→
2	[number]	k4	2
Tb	Tb	k1gFnPc2	Tb
+	+	kIx~	+
3	[number]	k4	3
CaO	CaO	k1gFnPc2	CaO
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
europium	europium	k1gNnSc4	europium
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
terbium	terbium	k1gNnSc1	terbium
jako	jako	k8xC	jako
luminofor	luminofor	k1gInSc1	luminofor
v	v	k7c6	v
obrazovkách	obrazovka	k1gFnPc6	obrazovka
barevných	barevný	k2eAgInPc2d1	barevný
televizorů	televizor	k1gInPc2	televizor
<g/>
.	.	kIx.	.
</s>
<s>
Materiály	materiál	k1gInPc4	materiál
aktivované	aktivovaný	k2eAgInPc4d1	aktivovaný
terbiem	terbium	k1gNnSc7	terbium
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
emisi	emise	k1gFnSc4	emise
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
až	až	k8xS	až
žlutozelené	žlutozelený	k2eAgFnSc6d1	žlutozelená
oblasti	oblast	k1gFnSc6	oblast
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rentgenologii	rentgenologie	k1gFnSc6	rentgenologie
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
speciální	speciální	k2eAgFnPc4d1	speciální
fólie	fólie	k1gFnPc4	fólie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
emisi	emise	k1gFnSc4	emise
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
modře	modro	k6eAd1	modro
emitující	emitující	k2eAgFnSc1d1	emitující
fólie	fólie	k1gFnSc1	fólie
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
–	–	k?	–
oxid-bromid	oxidromid	k1gInSc1	oxid-bromid
lanthanitý	lanthanitý	k2eAgInSc1d1	lanthanitý
(	(	kIx(	(
<g/>
LaOBr	LaOBr	k1gInSc1	LaOBr
<g/>
)	)	kIx)	)
aktivované	aktivovaný	k2eAgNnSc1d1	aktivované
terbiem	terbium	k1gNnSc7	terbium
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
s	s	k7c7	s
modrocitlivými	modrocitlivý	k2eAgInPc7d1	modrocitlivý
filmy	film	k1gInPc7	film
nebo	nebo	k8xC	nebo
zeleně	zeleně	k6eAd1	zeleně
emitující	emitující	k2eAgFnPc4d1	emitující
fólie	fólie	k1gFnPc4	fólie
-	-	kIx~	-
dioxid-sulfid	dioxidulfid	k1gInSc4	dioxid-sulfid
gadolinitý	gadolinitý	k2eAgInSc4d1	gadolinitý
(	(	kIx(	(
<g/>
Gd	Gd	k1gFnSc1	Gd
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
aktivovaný	aktivovaný	k2eAgInSc1d1	aktivovaný
terbiem	terbium	k1gNnSc7	terbium
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
se	s	k7c7	s
zelenocitlivými	zelenocitlivý	k2eAgInPc7d1	zelenocitlivý
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
terbium	terbium	k1gNnSc1	terbium
společně	společně	k6eAd1	společně
s	s	k7c7	s
gadoliniem	gadolinium	k1gNnSc7	gadolinium
základní	základní	k2eAgFnSc2d1	základní
součástí	součást	k1gFnSc7	součást
magnetooptických	magnetooptický	k2eAgNnPc2d1	magnetooptické
záznamových	záznamový	k2eAgNnPc2d1	záznamové
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
uchovávání	uchovávání	k1gNnSc3	uchovávání
dat	datum	k1gNnPc2	datum
po	po	k7c6	po
aktivaci	aktivace	k1gFnSc6	aktivace
záznamové	záznamový	k2eAgFnSc2d1	záznamová
vrstvy	vrstva	k1gFnSc2	vrstva
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
teplotou	teplota	k1gFnSc7	teplota
vyvolanou	vyvolaný	k2eAgFnSc4d1	vyvolaná
laserovým	laserový	k2eAgInSc7d1	laserový
paprskem	paprsek	k1gInSc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Nosným	nosný	k2eAgInSc7d1	nosný
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
aktivní	aktivní	k2eAgInPc4d1	aktivní
prvky	prvek	k1gInPc4	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc1	slitina
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
načtení	načtení	k1gNnSc6	načtení
dat	datum	k1gNnPc2	datum
za	za	k7c2	za
přesně	přesně	k6eAd1	přesně
definované	definovaný	k2eAgFnSc2d1	definovaná
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
záznamového	záznamový	k2eAgNnSc2d1	záznamové
média	médium	k1gNnSc2	médium
(	(	kIx(	(
<g/>
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
záznam	záznam	k1gInSc1	záznam
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
prakticky	prakticky	k6eAd1	prakticky
neomezeně	omezeně	k6eNd1	omezeně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
terbium	terbium	k1gNnSc4	terbium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
terbium	terbium	k1gNnSc4	terbium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
