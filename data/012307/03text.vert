<p>
<s>
Mikroplatba	Mikroplatba	k1gFnSc1	Mikroplatba
či	či	k8xC	či
mikrotransakce	mikrotransakce	k1gFnSc1	mikrotransakce
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
finanční	finanční	k2eAgFnPc4d1	finanční
transakce	transakce	k1gFnPc4	transakce
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
obnosy	obnos	k1gInPc7	obnos
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
platby	platba	k1gFnPc4	platba
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
a	a	k8xC	a
hranice	hranice	k1gFnSc2	hranice
"	"	kIx"	"
<g/>
malosti	malost	k1gFnSc2	malost
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
PayPal	PayPal	k1gInSc1	PayPal
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
mikroplatby	mikroplatba	k1gFnPc4	mikroplatba
platby	platba	k1gFnSc2	platba
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
12	[number]	k4	12
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Visa	viso	k1gNnPc4	viso
rozumím	rozumět	k5eAaImIp1nS	rozumět
mikroplatbami	mikroplatba	k1gFnPc7	mikroplatba
platby	platba	k1gFnSc2	platba
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
20	[number]	k4	20
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
mikroplatbách	mikroplatba	k1gFnPc6	mikroplatba
coby	coby	k?	coby
skutečně	skutečně	k6eAd1	skutečně
malinkých	malinký	k2eAgFnPc6d1	malinká
platbách	platba	k1gFnPc6	platba
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
centů	cent	k1gInPc2	cent
respektive	respektive	k9	respektive
haléřů	haléř	k1gInPc2	haléř
tak	tak	k6eAd1	tak
dnes	dnes	k6eAd1	dnes
splňují	splňovat	k5eAaImIp3nP	splňovat
spíše	spíše	k9	spíše
méně	málo	k6eAd2	málo
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
platební	platební	k2eAgInPc1d1	platební
systémy	systém	k1gInPc1	systém
typu	typ	k1gInSc2	typ
Bitcoin	Bitcoin	k1gInSc1	Bitcoin
<g/>
,	,	kIx,	,
systémy	systém	k1gInPc1	systém
jako	jako	k8xS	jako
Flattr	Flattr	k1gInSc1	Flattr
provázené	provázený	k2eAgFnPc1d1	provázená
se	s	k7c7	s
sociálními	sociální	k2eAgFnPc7d1	sociální
sítěmi	síť	k1gFnPc7	síť
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obchodování	obchodování	k1gNnSc1	obchodování
ve	v	k7c6	v
virtuálních	virtuální	k2eAgFnPc6d1	virtuální
měnách	měna	k1gFnPc6	měna
internetových	internetový	k2eAgFnPc2d1	internetová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
mikroplateb	mikroplatba	k1gFnPc2	mikroplatba
zažila	zažít	k5eAaPmAgFnS	zažít
velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
létech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
elektronického	elektronický	k2eAgNnSc2d1	elektronické
obchodování	obchodování	k1gNnSc2	obchodování
a	a	k8xC	a
prodejného	prodejný	k2eAgInSc2d1	prodejný
mediálního	mediální	k2eAgInSc2d1	mediální
obsahu	obsah	k1gInSc2	obsah
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původních	původní	k2eAgFnPc2d1	původní
představ	představa	k1gFnPc2	představa
měly	mít	k5eAaImAgInP	mít
mikroplatby	mikroplatb	k1gInPc1	mikroplatb
usnadnit	usnadnit	k5eAaPmF	usnadnit
prodej	prodej	k1gInSc4	prodej
mediálního	mediální	k2eAgInSc2d1	mediální
obsahu	obsah	k1gInSc2	obsah
a	a	k8xC	a
pro	pro	k7c4	pro
poskytovatele	poskytovatel	k1gMnPc4	poskytovatel
obsahu	obsah	k1gInSc2	obsah
tak	tak	k9	tak
být	být	k5eAaImF	být
alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
platbám	platba	k1gFnPc3	platba
za	za	k7c2	za
kliknutí	kliknutí	k1gNnSc2	kliknutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
založily	založit	k5eAaPmAgFnP	založit
i	i	k8xC	i
velké	velký	k2eAgFnPc1d1	velká
zavedené	zavedený	k2eAgFnPc1d1	zavedená
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
IBM	IBM	kA	IBM
nebo	nebo	k8xC	nebo
Compaq	Compaq	kA	Compaq
<g/>
,	,	kIx,	,
svá	svůj	k3xOyFgNnPc4	svůj
oddělení	oddělení	k1gNnPc4	oddělení
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
mikroplateb	mikroplatba	k1gFnPc2	mikroplatba
<g/>
.	.	kIx.	.
</s>
<s>
Mikroplacením	Mikroplacení	k1gNnSc7	Mikroplacení
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
i	i	k9	i
akademická	akademický	k2eAgFnSc1d1	akademická
sféra	sféra	k1gFnSc1	sféra
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
možných	možný	k2eAgInPc2d1	možný
standardů	standard	k1gInPc2	standard
probíhal	probíhat	k5eAaImAgInS	probíhat
například	například	k6eAd1	například
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Carnegie	Carnegie	k1gFnSc2	Carnegie
Mellon	Mellon	k1gInSc1	Mellon
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
konsorcia	konsorcium	k1gNnSc2	konsorcium
W	W	kA	W
<g/>
3	[number]	k4	3
<g/>
C.	C.	kA	C.
To	to	k9	to
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
přímou	přímý	k2eAgFnSc4d1	přímá
podporu	podpora	k1gFnSc4	podpora
mikroplateb	mikroplatba	k1gFnPc2	mikroplatba
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
HTML	HTML	kA	HTML
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jejich	jejich	k3xOp3gFnSc4	jejich
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
stavových	stavový	k2eAgInPc6d1	stavový
kódech	kód	k1gInPc6	kód
HTTP	HTTP	kA	HTTP
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ovšem	ovšem	k9	ovšem
konsorcium	konsorcium	k1gNnSc4	konsorcium
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
aktivit	aktivita	k1gFnPc2	aktivita
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Micropayment	Micropayment	k1gInSc1	Micropayment
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
