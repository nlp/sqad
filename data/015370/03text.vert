<s>
Kočkovití	kočkovití	k1gMnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
zvířeti	zvíře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
kočka	kočka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kočkovití	kočkovitý	k2eAgMnPc1d1
Reprezentanti	reprezentant	k1gMnPc1
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
šelmy	šelma	k1gFnPc1
(	(	kIx(
<g/>
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kočkovití	kočkovití	k1gMnPc1
(	(	kIx(
<g/>
Felidae	Felidae	k1gInSc1
<g/>
)	)	kIx)
<g/>
J.	J.	kA
Fischer	Fischer	k1gMnSc1
de	de	k?
Waldheim	Waldheim	k1gMnSc1
<g/>
,	,	kIx,
1817	#num#	k4
Rody	rod	k1gInPc1
</s>
<s>
podčeleď	podčeleď	k1gFnSc1
<g/>
:	:	kIx,
Pantherinae	Pantherinae	k1gFnSc1
</s>
<s>
Neofelis	Neofelis	k1gFnSc1
</s>
<s>
Panthera	Panthera	k1gFnSc1
</s>
<s>
podčeleď	podčeleď	k1gFnSc1
<g/>
:	:	kIx,
Felinae	Felinae	k1gFnSc1
</s>
<s>
Pardofelis	Pardofelis	k1gFnSc1
</s>
<s>
Catopuma	Catopuma	k1gFnSc1
</s>
<s>
Caracal	Caracat	k5eAaPmAgMnS
</s>
<s>
Leptailurus	Leptailurus	k1gMnSc1
</s>
<s>
Leopardus	Leopardus	k1gMnSc1
</s>
<s>
Lynx	Lynx	k1gInSc1
</s>
<s>
Acinonyx	Acinonyx	k1gInSc1
</s>
<s>
Puma	puma	k1gFnSc1
</s>
<s>
Otocolobus	Otocolobus	k1gMnSc1
</s>
<s>
Prionailurus	Prionailurus	k1gMnSc1
</s>
<s>
Felis	Felis	k1gFnSc1
</s>
<s>
podčeleď	podčeleď	k1gFnSc1
<g/>
:	:	kIx,
†	†	k?
Machairodontinae	Machairodontinae	k1gInSc1
</s>
<s>
podčeleď	podčeleď	k1gFnSc1
<g/>
:	:	kIx,
†	†	k?
Proailurinae	Proailurinae	k1gInSc1
</s>
<s>
Sesterská	sesterský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
asijští	asijský	k2eAgMnPc1d1
linsangové	linsang	k1gMnPc1
(	(	kIx(
<g/>
Prionodontidae	Prionodontidae	k1gInSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kočkovití	kočkovití	k1gMnPc1
(	(	kIx(
<g/>
Felidae	Felidae	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
nazývaní	nazývaný	k2eAgMnPc1d1
kočky	kočka	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
čeledí	čeleď	k1gFnSc7
řádu	řád	k1gInSc2
šelem	šelma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělí	dělit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
dvě	dva	k4xCgFnPc4
recentní	recentní	k2eAgFnPc4d1
podčeledi	podčeleď	k1gFnPc4
–	–	k?
velké	velká	k1gFnPc4
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
Felinae	Felina	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
dvě	dva	k4xCgFnPc1
fosilní	fosilní	k2eAgFnPc1d1
podčeledi	podčeleď	k1gFnPc1
–	–	k?
Machairodontinae	Machairodontina	k1gFnSc2
a	a	k8xC
Proailurinae	Proailurina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
První	první	k4xOgFnPc1
kočky	kočka	k1gFnPc1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
během	během	k7c2
oligocénu	oligocén	k1gInSc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
před	před	k7c7
25	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
recentní	recentní	k2eAgFnPc1d1
podčeledi	podčeleď	k1gFnPc1
se	se	k3xPyFc4
od	od	k7c2
sebe	sebe	k3xPyFc4
oddělily	oddělit	k5eAaPmAgFnP
asi	asi	k9
před	před	k7c7
11,5	11,5	k4
miliony	milion	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kočkovité	kočkovitý	k2eAgFnPc4d1
šelmy	šelma	k1gFnPc4
jsou	být	k5eAaImIp3nP
skvěle	skvěle	k6eAd1
adaptovaní	adaptovaný	k2eAgMnPc1d1
lovci	lovec	k1gMnPc1
<g/>
,	,	kIx,
uzpůsobení	uzpůsobený	k2eAgMnPc1d1
k	k	k7c3
lovu	lov	k1gInSc3
ze	z	k7c2
zálohy	záloha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
žije	žít	k5eAaImIp3nS
na	na	k7c4
Zemi	zem	k1gFnSc4
41	#num#	k4
druhů	druh	k1gInPc2
těchto	tento	k3xDgFnPc2
šelem	šelma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgFnPc4d3
kočky	kočka	k1gFnPc4
patří	patřit	k5eAaImIp3nS
bezpochyby	bezpochyby	k6eAd1
kočka	kočka	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc4
soužití	soužití	k1gNnSc4
s	s	k7c7
člověkem	člověk	k1gMnSc7
je	být	k5eAaImIp3nS
známé	známý	k2eAgNnSc1d1
již	již	k6eAd1
4	#num#	k4
000	#num#	k4
až	až	k9
7	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divocí	divoký	k2eAgMnPc1d1
příbuzní	příbuzný	k1gMnPc1
kočky	kočka	k1gFnSc2
domácí	domácí	k1gMnPc4
stále	stále	k6eAd1
žijí	žít	k5eAaImIp3nP
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
západní	západní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
poničené	poničený	k2eAgNnSc1d1
životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
značně	značně	k6eAd1
omezilo	omezit	k5eAaPmAgNnS
plochy	plocha	k1gFnPc4
jejich	jejich	k3xOp3gInSc2
výskytu	výskyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
velmi	velmi	k6eAd1
známé	známý	k2eAgInPc1d1
druhy	druh	k1gInPc1
kočkovitých	kočkovití	k1gMnPc2
zahrnují	zahrnovat	k5eAaImIp3nP
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
lev	lev	k1gMnSc1
<g/>
,	,	kIx,
tygr	tygr	k1gMnSc1
<g/>
,	,	kIx,
levhart	levhart	k1gMnSc1
<g/>
,	,	kIx,
jaguár	jaguár	k1gMnSc1
a	a	k8xC
množství	množství	k1gNnSc1
malých	malý	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
jako	jako	k8xC,k8xS
například	například	k6eAd1
ocelot	ocelot	k1gMnSc1
<g/>
,	,	kIx,
rys	rys	k1gMnSc1
ostrovid	ostrovid	k1gMnSc1
nebo	nebo	k8xC
puma	puma	k1gFnSc1
americká	americký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nejbližší	blízký	k2eAgMnPc4d3
příbuzné	příbuzný	k1gMnPc4
koček	kočka	k1gFnPc2
se	se	k3xPyFc4
považují	považovat	k5eAaImIp3nP
asijští	asijský	k2eAgMnPc1d1
linsangové	linsang	k1gMnPc1
rodu	rod	k1gInSc2
Prionodon	Prionodona	k1gFnPc2
dříve	dříve	k6eAd2
řazení	řazení	k1gNnSc2
k	k	k7c3
cibetkám	cibetka	k1gFnPc3
(	(	kIx(
<g/>
Viverrinae	Viverrinae	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
celého	celý	k2eAgInSc2d1
řádu	řád	k1gInSc2
šelem	šelma	k1gFnPc2
se	se	k3xPyFc4
právě	právě	k9
kočkovití	kočkovití	k1gMnPc1
nejvíce	hodně	k6eAd3,k6eAd1
drží	držet	k5eAaImIp3nS
masožravého	masožravý	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
života	život	k1gInSc2
–	–	k?
jsou	být	k5eAaImIp3nP
na	na	k7c6
příjmu	příjem	k1gInSc6
masa	maso	k1gNnSc2
zcela	zcela	k6eAd1
závislí	závislý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
proto	proto	k6eAd1
říká	říkat	k5eAaImIp3nS
hypermasožravci	hypermasožravec	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
tělesná	tělesný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
přizpůsobená	přizpůsobený	k2eAgFnSc1d1
lovu	lov	k1gInSc2
jiných	jiný	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
pružné	pružný	k2eAgFnPc1d1
a	a	k8xC
svalnaté	svalnatý	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
<g/>
,	,	kIx,
čelisti	čelist	k1gFnPc1
a	a	k8xC
zuby	zub	k1gInPc1
určené	určený	k2eAgInPc1d1
k	k	k7c3
silnému	silný	k2eAgInSc3d1
skusu	skus	k1gInSc3
<g/>
,	,	kIx,
flexibilní	flexibilní	k2eAgFnSc1d1
přední	přední	k2eAgFnPc4d1
končetiny	končetina	k1gFnPc4
zakončené	zakončený	k2eAgFnPc4d1
ostrými	ostrý	k2eAgInPc7d1
zatažitelnými	zatažitelný	k2eAgInPc7d1
drápy	dráp	k1gInPc7
(	(	kIx(
<g/>
všichni	všechen	k3xTgMnPc1
kromě	kromě	k7c2
geparda	gepard	k1gMnSc2
<g/>
)	)	kIx)
sloužícími	sloužící	k2eAgInPc7d1
k	k	k7c3
přidržení	přidržení	k1gNnSc3
kořisti	kořist	k1gFnSc2
<g/>
,	,	kIx,
zadní	zadní	k2eAgFnSc2d1
končetiny	končetina	k1gFnSc2
umožňující	umožňující	k2eAgNnSc1d1
prudké	prudký	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
a	a	k8xC
skoky	skok	k1gInPc1
a	a	k8xC
srst	srst	k1gFnSc1
tvořenou	tvořený	k2eAgFnSc7d1
kamuflážními	kamuflážní	k2eAgInPc7d1
barevnými	barevný	k2eAgInPc7d1
odstíny	odstín	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
smyslů	smysl	k1gInPc2
je	být	k5eAaImIp3nS
co	co	k9
nejlépe	dobře	k6eAd3
vyvinut	vyvinout	k5eAaPmNgInS
především	především	k9
zrak	zrak	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
k	k	k7c3
zachycení	zachycení	k1gNnSc3
pohybu	pohyb	k1gInSc3
a	a	k8xC
k	k	k7c3
dobrému	dobrý	k2eAgNnSc3d1
vidění	vidění	k1gNnSc3
za	za	k7c4
šera	šero	k1gNnPc4
a	a	k8xC
tmy	tma	k1gFnPc4
a	a	k8xC
následně	následně	k6eAd1
také	také	k9
sluch	sluch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
chování	chování	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
typický	typický	k2eAgInSc1d1
samotářský	samotářský	k2eAgInSc1d1
způsob	způsob	k1gInSc1
života	život	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
lvů	lev	k1gMnPc2
a	a	k8xC
částečně	částečně	k6eAd1
gepardů	gepard	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Kočkovité	kočkovitý	k2eAgFnPc1d1
šelmy	šelma	k1gFnPc1
přenášejí	přenášet	k5eAaImIp3nP
nemoc	nemoc	k1gFnSc4
toxoplazmózu	toxoplazmóza	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
původcem	původce	k1gMnSc7
je	být	k5eAaImIp3nS
výtrusovec	výtrusovec	k1gMnSc1
Toxoplasma	Toxoplasmum	k1gNnSc2
gondii	gondie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
velkými	velký	k2eAgFnPc7d1
a	a	k8xC
malými	malý	k2eAgFnPc7d1
kočkami	kočka	k1gFnPc7
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
velkými	velký	k2eAgFnPc7d1
a	a	k8xC
malými	malý	k2eAgFnPc7d1
kočkami	kočka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Obecně	obecně	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
podčeleď	podčeleď	k1gFnSc1
Felinae	Felina	k1gFnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
menší	malý	k2eAgFnPc1d2
než	než	k8xS
zástupci	zástupce	k1gMnPc7
podčeledi	podčeleď	k1gFnSc2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
naopak	naopak	k6eAd1
bývají	bývat	k5eAaImIp3nP
těžší	těžký	k2eAgNnSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neplatí	platit	k5eNaImIp3nS
to	ten	k3xDgNnSc1
ale	ale	k8xC
u	u	k7c2
všech	všecek	k3xTgInPc2
druhů	druh	k1gInPc2
<g/>
:	:	kIx,
Puma	puma	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
<g/>
,	,	kIx,
ač	ač	k8xS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
podle	podle	k7c2
její	její	k3xOp3gFnSc2
velikosti	velikost	k1gFnSc2
intuitivně	intuitivně	k6eAd1
nezdá	zdát	k5eNaImIp3nS,k5eNaPmIp3nS
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
malé	malý	k2eAgFnPc4d1
kočky	kočka	k1gFnPc4
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
irbis	irbis	k1gFnSc1
<g/>
,	,	kIx,
levhart	levhart	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
nebo	nebo	k8xC
jaguár	jaguár	k1gMnSc1
americký	americký	k2eAgMnSc1d1
<g/>
,	,	kIx,
než	než	k8xS
zástupci	zástupce	k1gMnPc1
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
rozlišovacím	rozlišovací	k2eAgInSc7d1
znakem	znak	k1gInSc7
jsou	být	k5eAaImIp3nP
zornice	zornice	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Všechny	všechen	k3xTgFnPc1
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
mají	mít	k5eAaImIp3nP
kulatou	kulatý	k2eAgFnSc4d1
zornici	zornice	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
oproti	oproti	k7c3
úzkým	úzké	k1gInPc3
a	a	k8xC
svislým	svislý	k2eAgFnPc3d1
zornicím	zornice	k1gFnPc3
malých	malý	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
některé	některý	k3yIgFnPc1
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
mají	mít	k5eAaImIp3nP
také	také	k9
kulatou	kulatý	k2eAgFnSc4d1
zornici	zornice	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
rys	rys	k1gInSc1
<g/>
,	,	kIx,
jaguarundi	jaguarund	k1gMnPc1
nebo	nebo	k8xC
manul	manul	k1gMnSc1
mají	mít	k5eAaImIp3nP
zornice	zornice	k1gFnPc4
kulaté	kulatý	k2eAgFnPc4d1
<g/>
,	,	kIx,
a	a	k8xC
přesto	přesto	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
malé	malý	k2eAgFnPc4d1
kočky	kočka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInSc1d3
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
velkými	velký	k2eAgFnPc7d1
a	a	k8xC
malými	malý	k2eAgFnPc7d1
kočkami	kočka	k1gFnPc7
je	být	k5eAaImIp3nS
ve	v	k7c6
stavbě	stavba	k1gFnSc6
jazylky	jazylka	k1gFnSc2
<g/>
:	:	kIx,
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
mají	mít	k5eAaImIp3nP
jazylku	jazylka	k1gFnSc4
zkostnatělou	zkostnatělý	k2eAgFnSc4d1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgInPc2,k3yRgInPc2,k3yIgInPc2
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
chrupavčitá	chrupavčitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Chrupavčitá	chrupavčitý	k2eAgFnSc1d1
jazylka	jazylka	k1gFnSc1
a	a	k8xC
pohyblivý	pohyblivý	k2eAgInSc1d1
hrtan	hrtan	k1gInSc1
dovoluje	dovolovat	k5eAaImIp3nS
velkým	velký	k2eAgFnPc3d1
kočkám	kočka	k1gFnPc3
hlasitě	hlasitě	k6eAd1
řvát	řvát	k5eAaImF
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
nedovedou	dovést	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
mohou	moct	k5eAaImIp3nP
příst	příst	k5eAaImF
jenom	jenom	k9
při	při	k7c6
výdechu	výdech	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kostěná	kostěný	k2eAgFnSc1d1
jazylka	jazylka	k1gFnSc1
omezuje	omezovat	k5eAaImIp3nS
pohyblivost	pohyblivost	k1gFnSc4
hrtanu	hrtan	k1gInSc2
<g/>
,	,	kIx,
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
proto	proto	k8xC
nedokážou	dokázat	k5eNaPmIp3nP
řvát	řvát	k5eAaImF
<g/>
,	,	kIx,
avšak	avšak	k8xC
zas	zas	k6eAd1
mohou	moct	k5eAaImIp3nP
příst	příst	k5eAaImF
při	při	k7c6
nádechu	nádech	k1gInSc6
i	i	k8xC
při	při	k7c6
výdechu	výdech	k1gInSc6
<g/>
,	,	kIx,
nepřerušovaně	přerušovaně	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgNnSc7d1
spolehlivým	spolehlivý	k2eAgNnSc7d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
méně	málo	k6eAd2
nápadným	nápadný	k2eAgInSc7d1
rozdílem	rozdíl	k1gInSc7
je	být	k5eAaImIp3nS
rhinarium	rhinarium	k1gNnSc1
–	–	k?
neosrstěná	osrstěný	k2eNgFnSc1d1
plocha	plocha	k1gFnSc1
kolem	kolem	k7c2
nozder	nozdra	k1gFnPc2
<g/>
:	:	kIx,
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
mají	mít	k5eAaImIp3nP
rhinarium	rhinarium	k1gNnSc1
menší	malý	k2eAgFnSc2d2
než	než	k8xS
malé	malý	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc1
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
také	také	k9
mají	mít	k5eAaImIp3nP
ušní	ušní	k2eAgInPc4d1
boltce	boltec	k1gInPc4
bez	bez	k7c2
koncových	koncový	k2eAgFnPc2d1
štětiček	štětička	k1gFnPc2
z	z	k7c2
delších	dlouhý	k2eAgInPc2d2
chlupů	chlup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
mají	mít	k5eAaImIp3nP
čenich	čenich	k1gInSc4
vždy	vždy	k6eAd1
lysý	lysý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
i	i	k9
etologické	etologický	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
krmení	krmení	k1gNnSc6
<g/>
:	:	kIx,
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
konzumují	konzumovat	k5eAaBmIp3nP
potravu	potrava	k1gFnSc4
přikrčené	přikrčený	k2eAgFnPc1d1
<g/>
,	,	kIx,
s	s	k7c7
končetinami	končetina	k1gFnPc7
pod	pod	k7c7
tělem	tělo	k1gNnSc7
<g/>
,	,	kIx,
</s>
<s>
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
žerou	žrát	k5eAaImIp3nP
ve	v	k7c4
stoje	stoj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
lze	lze	k6eAd1
pozorovat	pozorovat	k5eAaImF
v	v	k7c6
míře	míra	k1gFnSc6
schopnosti	schopnost	k1gFnSc2
zatahovat	zatahovat	k5eAaImF
drápy	dráp	k1gInPc4
a	a	k8xC
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
tělesnou	tělesný	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
také	také	k9
ve	v	k7c6
schopnosti	schopnost	k1gFnSc6
šplhat	šplhat	k5eAaImF
na	na	k7c4
stromy	strom	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
</s>
<s>
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
</s>
<s>
velikost	velikost	k1gFnSc1
</s>
<s>
menší	malý	k2eAgInPc1d2
druhy	druh	k1gInPc1
</s>
<s>
větší	veliký	k2eAgInPc1d2
druhy	druh	k1gInPc1
</s>
<s>
zornice	zornice	k1gFnSc1
</s>
<s>
většinou	většinou	k6eAd1
svislá	svislý	k2eAgFnSc1d1
</s>
<s>
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
gepardů	gepard	k1gMnPc2
<g/>
,	,	kIx,
manulů	manul	k1gMnPc2
a	a	k8xC
pum	puma	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
vždy	vždy	k6eAd1
kulatá	kulatý	k2eAgFnSc1d1
</s>
<s>
jazylka	jazylka	k1gFnSc1
</s>
<s>
kostěná	kostěný	k2eAgFnSc1d1
(	(	kIx(
<g/>
neumožňuje	umožňovat	k5eNaImIp3nS
řev	řev	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
částečně	částečně	k6eAd1
chrupavčitá	chrupavčitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
umožňuje	umožňovat	k5eAaImIp3nS
řev	řev	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
rhinarium	rhinarium	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
nozdry	nozdra	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
větší	veliký	k2eAgFnSc1d2
neosrstěná	osrstěný	k2eNgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
menší	malý	k2eAgFnSc1d2
neosrstěná	osrstěný	k2eNgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1
</s>
<s>
Novější	nový	k2eAgInPc1d2
výzkumy	výzkum	k1gInPc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
v	v	k7c6
populární	populární	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
pak	pak	k6eAd1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
zredukovaly	zredukovat	k5eAaPmAgInP
počet	počet	k1gInSc4
rodů	rod	k1gInPc2
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
a	a	k8xC
vyvrátily	vyvrátit	k5eAaPmAgFnP
zvláštní	zvláštní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
gepardů	gepard	k1gMnPc2
(	(	kIx(
<g/>
nejblíže	blízce	k6eAd3
příbuzní	příbuzný	k1gMnPc1
jim	on	k3xPp3gMnPc3
jsou	být	k5eAaImIp3nP
jaguarundi	jaguarund	k1gMnPc1
a	a	k8xC
puma	puma	k1gFnSc1
americká	americký	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
podčeleď	podčeleď	k1gFnSc4
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
jako	jako	k9
monofyletická	monofyletický	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
bázi	báze	k1gFnSc6
stojící	stojící	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
zásadní	zásadní	k2eAgFnSc1d1
revize	revize	k1gFnSc1
klasifikace	klasifikace	k1gFnSc1
kočkovitých	kočkovití	k1gMnPc2
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
a	a	k8xC
představil	představit	k5eAaPmAgInS
ji	on	k3xPp3gFnSc4
tým	tým	k1gInSc1
vědců	vědec	k1gMnPc2
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
Mezinárodního	mezinárodní	k2eAgInSc2d1
svazu	svaz	k1gInSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
klasifikace	klasifikace	k1gFnSc1
rozeznává	rozeznávat	k5eAaImIp3nS
41	#num#	k4
recentních	recentní	k2eAgInPc2d1
druhů	druh	k1gInPc2
dělených	dělený	k2eAgInPc2d1
do	do	k7c2
14	#num#	k4
rodů	rod	k1gInPc2
(	(	kIx(
<g/>
konstatuje	konstatovat	k5eAaBmIp3nS
nejasný	jasný	k2eNgInSc1d1
stav	stav	k1gInSc1
ohledně	ohledně	k7c2
rodu	rod	k1gInSc2
Herpailurus	Herpailurus	k1gInSc1
-	-	kIx~
jaguarundi	jaguarund	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
provizorně	provizorně	k6eAd1
uznává	uznávat	k5eAaImIp3nS
<g/>
)	)	kIx)
a	a	k8xC
2	#num#	k4
podčeledí	podčeleď	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Neofelis	Neofelis	k1gFnSc1
</s>
<s>
levhart	levhart	k1gMnSc1
obláčkový	obláčkový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Neofelis	Neofelis	k1gInSc1
nebulosa	nebulosa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
levhart	levhart	k1gMnSc1
Diardův	Diardův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Neofelis	Neofelis	k1gFnSc1
diardi	diardit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
</s>
<s>
irbis	irbis	k1gFnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
tygr	tygr	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
tigris	tigris	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
levhart	levhart	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
pardus	pardus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
jaguár	jaguár	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
onca	onca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
lev	lev	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
leo	leo	k?
<g/>
)	)	kIx)
</s>
<s>
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
Felinae	Felinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Pardofelis	Pardofelis	k1gFnSc1
</s>
<s>
kočka	kočka	k1gFnSc1
mramorovaná	mramorovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Pardofelis	Pardofelis	k1gFnSc1
marmorata	marmorata	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Catopuma	Catopuma	k1gFnSc1
</s>
<s>
kočka	kočka	k1gFnSc1
bornejská	bornejský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Catopuma	Catopuma	k1gFnSc1
badia	badia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
Temminckova	Temminckův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Catopuma	Catopuma	k1gFnSc1
temminckii	temminckium	k1gNnPc7
<g/>
)	)	kIx)
</s>
<s>
Caracal	Caracat	k5eAaPmAgMnS
</s>
<s>
kočka	kočka	k1gFnSc1
zlatá	zlatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Caracal	Caracal	k1gFnSc1
aurata	aurata	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
karakal	karakal	k1gMnSc1
(	(	kIx(
<g/>
Caracal	Caracal	k1gMnSc1
caracal	caracat	k5eAaPmAgMnS
<g/>
)	)	kIx)
</s>
<s>
Leptailurus	Leptailurus	k1gMnSc1
</s>
<s>
serval	serval	k1gMnSc1
(	(	kIx(
<g/>
Leptailurus	Leptailurus	k1gMnSc1
serval	serval	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Leopardus	Leopardus	k1gMnSc1
</s>
<s>
ocelot	ocelot	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Leopardus	Leopardus	k1gInSc1
pardalis	pardalis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
margay	margaa	k1gFnPc1
(	(	kIx(
<g/>
Leopardus	Leopardus	k1gInSc1
wiedii	wiedie	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
pampová	pampový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leopardus	Leopardus	k1gMnSc1
colocolo	colocola	k1gFnSc5
či	či	k8xC
L.	L.	kA
colocola	colocola	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
horská	horský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leopardus	Leopardus	k1gMnSc1
jacobita	jacobita	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ocelot	ocelot	k1gMnSc1
stromový	stromový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Leopardus	Leopardus	k1gMnSc1
tigrinus	tigrinus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
tmavá	tmavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leopardus	Leopardus	k1gInSc1
guigna	guign	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
slaništní	slaništní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leopardus	Leopardus	k1gMnSc1
geoffroyi	geoffroy	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Leopardus	Leopardus	k1gMnSc1
guttulus	guttulus	k1gMnSc1
</s>
<s>
Lynx	Lynx	k1gInSc1
</s>
<s>
rys	rys	k1gInSc1
červený	červený	k2eAgInSc1d1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
rufus	rufus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
rys	rys	k1gInSc1
kanadský	kanadský	k2eAgInSc1d1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
canadensis	canadensis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
rys	rys	k1gInSc1
ostrovid	ostrovid	k1gInSc1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
lynx	lynx	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
rys	rys	k1gInSc1
pardálový	pardálový	k2eAgInSc1d1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
pardinus	pardinus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Acinonyx	Acinonyx	k1gInSc1
</s>
<s>
gepard	gepard	k1gMnSc1
(	(	kIx(
<g/>
Acinonyx	Acinonyx	k1gInSc1
jubatus	jubatus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Puma	puma	k1gFnSc1
</s>
<s>
puma	puma	k1gFnSc1
americká	americký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Puma	puma	k1gFnSc1
concolor	concolor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Herpailurus	Herpailurus	k1gMnSc1
</s>
<s>
jaguarundi	jaguarund	k1gMnPc1
(	(	kIx(
<g/>
Herpailurus	Herpailurus	k1gMnSc1
yagouaroundi	yagouaround	k1gMnPc1
či	či	k8xC
Puma	puma	k2eAgMnPc1d1
yagouaroundi	yagouaround	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Otocolobus	Otocolobus	k1gMnSc1
</s>
<s>
manul	manul	k1gMnSc1
(	(	kIx(
<g/>
Otocolobus	Otocolobus	k1gMnSc1
manul	manul	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Prionailurus	Prionailurus	k1gMnSc1
</s>
<s>
kočka	kočka	k1gFnSc1
cejlonská	cejlonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Prionailurus	Prionailurus	k1gMnSc1
rubiginosus	rubiginosus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
plochočelá	plochočelý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Prionailurus	Prionailurus	k1gInSc1
planiceps	planiceps	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
rybářská	rybářský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Prionailurus	Prionailurus	k1gMnSc1
viverrinus	viverrinus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
bengálská	bengálský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Prionailurus	Prionailurus	k1gMnSc1
bengalensis	bengalensis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Prionailurus	Prionailurus	k1gInSc1
javanensis	javanensis	k1gFnSc2
</s>
<s>
Felis	Felis	k1gFnSc1
</s>
<s>
kočka	kočka	k1gFnSc1
bažinná	bažinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Felis	Felis	k1gFnSc1
chaus	chaus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
černonohá	černonohý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Felis	Felis	k1gFnSc1
nigripes	nigripes	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
pouštní	pouštní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Felis	Felis	k1gFnSc1
margarita	margarita	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
divoká	divoký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Felis	Felis	k1gFnSc1
silvestris	silvestris	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
plavá	plavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Felis	Felis	k1gFnSc1
lybica	lybica	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
(	(	kIx(
<g/>
Felis	Felis	k1gFnSc1
catus	catus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
šedá	šedá	k1gFnSc1
(	(	kIx(
<g/>
Felis	Felis	k1gFnSc1
bieti	bieť	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
fosilních	fosilní	k2eAgFnPc2d1
podčeledí	podčeleď	k1gFnPc2
a	a	k8xC
rodů	rod	k1gInPc2
</s>
<s>
†	†	k?
Machairodontinae	Machairodontinae	k1gInSc1
(	(	kIx(
<g/>
šavlozubí	šavlozubý	k2eAgMnPc1d1
tygři	tygr	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
rod	rod	k1gInSc1
Adelphailurus	Adelphailurus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Amphimachairodus	Amphimachairodus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Dinofelis	Dinofelis	k1gFnSc2
</s>
<s>
rod	rod	k1gInSc1
Hemimachairodus	Hemimachairodus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Homotherium	Homotherium	k1gNnSc1
</s>
<s>
rod	rod	k1gInSc1
Lokotunjailurus	Lokotunjailurus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Machairodus	Machairodus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Megantereon	Megantereona	k1gFnPc2
</s>
<s>
rod	rod	k1gInSc1
Metailurus	Metailurus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Miomachairodus	Miomachairodus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Nimravides	Nimravidesa	k1gFnPc2
</s>
<s>
rod	rod	k1gInSc1
Paramachairodus	Paramachairodus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Pontosmilus	Pontosmilus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Promegantereon	Promegantereona	k1gFnPc2
</s>
<s>
rod	rod	k1gInSc1
Rhizosmilodon	Rhizosmilodona	k1gFnPc2
</s>
<s>
rod	rod	k1gInSc1
Smilodon	Smilodona	k1gFnPc2
</s>
<s>
rod	rod	k1gInSc1
Stenailurus	Stenailurus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Xenosmilus	Xenosmilus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Yoshi	Yosh	k1gFnSc2
</s>
<s>
†	†	k?
Proailurinae	Proailurinae	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Proailurus	Proailurus	k1gInSc1
</s>
<s>
rod	rod	k1gInSc1
Stenogale	Stenogala	k1gFnSc6
</s>
<s>
Kladogram	Kladogram	k1gInSc1
</s>
<s>
Podle	podle	k7c2
fylogenetických	fylogenetický	k2eAgFnPc2d1
analýz	analýza	k1gFnPc2
lze	lze	k6eAd1
příbuzenské	příbuzenský	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
extantními	extantní	k2eAgInPc7d1
rody	rod	k1gInPc7
znázornit	znázornit	k5eAaPmF
následujícím	následující	k2eAgInSc7d1
fylogenetickým	fylogenetický	k2eAgInSc7d1
stromem	strom	k1gInSc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
{	{	kIx(
<g/>
border-spacing	border-spacing	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
margin	margina	k1gFnPc2
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc2
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
line-height	line-height	k1gInSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
border-collapse	border-collapse	k1gFnSc1
<g/>
:	:	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
separate	separat	k1gInSc5
<g/>
;	;	kIx,
<g/>
width	width	k1gInSc1
<g/>
:	:	kIx,
<g/>
auto	auto	k1gNnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
{	{	kIx(
<g/>
width	width	k1gInSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
clade-label	clade-label	k1gInSc1
<g/>
{	{	kIx(
<g/>
width	width	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.7	0.7	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0.15	0.15	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
vertical-align	vertical-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
bottom	bottom	k1gInSc1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
border-bottom	border-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-fixed-width	clade-fixed-width	k1gMnSc1
<g/>
{	{	kIx(
<g/>
overflow	overflow	k?
<g/>
:	:	kIx,
<g/>
hidden	hiddna	k1gFnPc2
<g/>
;	;	kIx,
<g/>
text-overflow	text-overflow	k?
<g/>
:	:	kIx,
<g/>
ellipsis	ellipsis	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-fixed-width	clade-fixed-width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
hover	hover	k1gMnSc1
<g/>
{	{	kIx(
<g/>
overflow	overflow	k?
<g/>
:	:	kIx,
<g/>
visible	visible	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-label	clade-label	k1gMnSc1
<g/>
.	.	kIx.
<g/>
first	first	k1gMnSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gFnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-label	clade-label	k1gMnSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
{	{	kIx(
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0.15	0.15	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
border-left	border-left	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
:	:	kIx,
<g/>
hover	hover	k1gMnSc1
<g/>
{	{	kIx(
<g/>
overflow	overflow	k?
<g/>
:	:	kIx,
<g/>
visible	visible	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
last	last	k1gMnSc1
<g/>
{	{	kIx(
<g/>
border-left	border-left	k1gMnSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gFnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-bar	clade-bar	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
middle	middle	k6eAd1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
left	left	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0.5	0.5	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
position	position	k1gInSc1
<g/>
:	:	kIx,
<g/>
relative	relativ	k1gInSc5
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-bar	clade-bar	k1gMnSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
{	{	kIx(
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gInSc1
<g/>
;	;	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
position	position	k1gInSc1
<g/>
:	:	kIx,
<g/>
relative	relativ	k1gInSc5
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-leaf	clade-leaf	k1gMnSc1
<g/>
{	{	kIx(
<g/>
border	border	k1gMnSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
left	left	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-leafR	clade-leafR	k?
<g/>
{	{	kIx(
<g/>
border	border	k1gMnSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
clade-leaf	clade-leaf	k1gInSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
{	{	kIx(
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
:	:	kIx,
<g/>
hover	hovrat	k5eAaPmRp2nS
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
linkA	linka	k1gFnSc1
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
yellow	yellow	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
:	:	kIx,
<g/>
hover	hovrat	k5eAaPmRp2nS
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
linkB	linkB	k?
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gMnSc1
<g/>
:	:	kIx,
<g/>
green	green	k1gInSc1
<g/>
}	}	kIx)
</s>
<s>
Kočkovití	kočkovití	k1gMnPc1
</s>
<s>
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
</s>
<s>
Neofelis	Neofelis	k1gFnSc1
</s>
<s>
Panthera	Panthera	k1gFnSc1
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
</s>
<s>
Pardofelis	Pardofelis	k1gFnSc1
</s>
<s>
Catopuma	Catopuma	k1gFnSc1
</s>
<s>
Caracal	Caracat	k5eAaPmAgMnS
</s>
<s>
Leptailurus	Leptailurus	k1gMnSc1
</s>
<s>
Leopardus	Leopardus	k1gMnSc1
</s>
<s>
Lynx	Lynx	k1gInSc1
</s>
<s>
Herpailurus	Herpailurus	k1gMnSc1
</s>
<s>
Puma	puma	k1gFnSc1
</s>
<s>
Acinonyx	Acinonyx	k1gInSc1
</s>
<s>
Otocolobus	Otocolobus	k1gMnSc1
</s>
<s>
Prionailurus	Prionailurus	k1gMnSc1
</s>
<s>
Felis	Felis	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
David	David	k1gMnSc1
Polly	Polla	k1gFnSc2
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecometrics	Ecometrics	k1gInSc1
and	and	k?
Neogene	Neogen	k1gInSc5
faunal	faunat	k5eAaImAgMnS,k5eAaPmAgMnS
turnover	turnover	k1gInSc4
<g/>
:	:	kIx,
the	the	k?
roles	roles	k1gInSc1
of	of	k?
cats	cats	k1gInSc1
and	and	k?
hindlimb	hindlimb	k1gInSc1
morphology	morpholog	k1gMnPc4
in	in	k?
the	the	k?
assembly	assembnout	k5eAaPmAgFnP
of	of	k?
carnivoran	carnivoran	k1gInSc1
communities	communities	k1gInSc1
in	in	k?
the	the	k?
New	New	k1gMnSc1
World	World	k1gMnSc1
<g/>
.	.	kIx.
in	in	k?
Bonis	Bonis	k1gFnPc4
L.	L.	kA
de	de	k?
&	&	k?
Werdelin	Werdelina	k1gFnPc2
L.	L.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Memorial	Memorial	k1gInSc1
to	ten	k3xDgNnSc1
Stéphane	Stéphan	k1gMnSc5
Peigné	Peigné	k2eAgNnPc6d1
<g/>
:	:	kIx,
Carnivores	Carnivores	k1gMnSc1
(	(	kIx(
<g/>
Hyaenodonta	Hyaenodonta	k1gMnSc1
and	and	k?
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
of	of	k?
the	the	k?
Cenozoic	Cenozoic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geodiversitas	Geodiversitasa	k1gFnPc2
<g/>
,	,	kIx,
42	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
257	#num#	k4
<g/>
-	-	kIx~
<g/>
304	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.5252/geodiversitas2020v42a17	https://doi.org/10.5252/geodiversitas2020v42a17	k4
<g/>
↑	↑	k?
MATTERN	MATTERN	kA
<g/>
,	,	kIx,
M.	M.	kA
Y.	Y.	kA
a	a	k8xC
MCLENNAN	MCLENNAN	kA
<g/>
,	,	kIx,
D.	D.	kA
A.	A.	kA
Phylogeny	Phylogen	k1gInPc1
and	and	k?
Speciation	Speciation	k1gInSc1
of	of	k?
Felids	Felids	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cladistics	Cladistics	k1gInSc1
16	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
232	#num#	k4
<g/>
–	–	k?
<g/>
253	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
W.	W.	kA
E.	E.	kA
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Late	lat	k1gInSc5
Miocene	Miocen	k1gInSc5
Radiation	Radiation	k1gInSc4
of	of	k?
Modern	Modern	k1gInSc1
Felidae	Felidae	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Genetic	Genetice	k1gFnPc2
Assessment	Assessment	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
311	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
73	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
O	O	kA
<g/>
'	'	kIx"
<g/>
BRIEN	BRIEN	kA
<g/>
,	,	kIx,
S.	S.	kA
J.	J.	kA
a	a	k8xC
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
W.	W.	kA
E.	E.	kA
The	The	k1gFnSc1
Evolution	Evolution	k1gInSc1
of	of	k?
Cats	Cats	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientific	Scientific	k1gMnSc1
American	American	k1gMnSc1
297	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZRZAVÝ	zrzavý	k2eAgInSc1d1
<g/>
,	,	kIx,
J.	J.	kA
a	a	k8xC
ROBOVSKÝ	ROBOVSKÝ	kA
<g/>
,	,	kIx,
J.	J.	kA
Kočkovité	kočkovitý	k2eAgFnSc2d1
šelmy	šelma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesmír	vesmír	k1gInSc1
86	#num#	k4
(	(	kIx(
<g/>
137	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
566	#num#	k4
<g/>
–	–	k?
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KITCHENER	KITCHENER	kA
<g/>
,	,	kIx,
A.	A.	kA
C.	C.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
revised	revised	k1gInSc1
taxonomy	taxonom	k1gInPc1
of	of	k?
the	the	k?
Felidae	Felidae	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cat	Cat	k1gFnPc2
News	Newsa	k1gFnPc2
Special	Special	k1gInSc1
Issue	Issu	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Winter	Winter	k1gMnSc1
2017	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SAKAMOTO	SAKAMOTO	kA
<g/>
,	,	kIx,
Manabu	Manab	k1gInSc2
<g/>
;	;	kIx,
RUTA	rout	k5eAaImNgFnS
<g/>
,	,	kIx,
Marcello	Marcella	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Convergence	Convergence	k1gFnSc1
and	and	k?
Divergence	divergence	k1gFnSc2
in	in	k?
the	the	k?
Evolution	Evolution	k1gInSc1
of	of	k?
Cat	Cat	k1gFnSc2
Skulls	Skullsa	k1gFnPc2
<g/>
:	:	kIx,
Temporal	Temporal	k1gFnSc1
and	and	k?
Spatial	Spatial	k1gInSc1
Patterns	Patterns	k1gInSc1
of	of	k?
Morphological	Morphological	k1gFnSc1
Diversity	Diversit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
,	,	kIx,
e	e	k0
<g/>
39752	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLoS	plos	k1gMnSc1
ONE	ONE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
7	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.137	10.137	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
journal	journat	k5eAaPmAgInS,k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
pone	pone	k1gInSc1
<g/>
.0039752	.0039752	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kočkovití	kočkovitý	k2eAgMnPc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Felidae	Felidae	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kočkovití	kočkovití	k1gMnPc1
Malé	Malé	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Felinae	Felinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
divoká	divoký	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
šedá	šedý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
bažinná	bažinný	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
pouštní	pouštní	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
černonohá	černonohý	k2eAgFnSc1d1
•	•	k?
manul	manul	k1gMnSc1
•	•	k?
kočka	kočka	k1gFnSc1
bornejská	bornejský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
Temminckova	Temminckův	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
mramorovaná	mramorovaný	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
bengálská	bengálský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
plochočelá	plochočelý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
cejlonská	cejlonský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
rybářská	rybářský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
pampová	pampový	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
slaništní	slaništní	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
tmavá	tmavý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
horská	horský	k2eAgFnSc1d1
•	•	k?
ocelot	ocelot	k1gMnSc1
velký	velký	k2eAgMnSc1d1
•	•	k?
ocelot	ocelot	k1gMnSc1
stromový	stromový	k2eAgMnSc1d1
•	•	k?
margay	margay	k1gInPc4
•	•	k?
rys	rys	k1gInSc1
kanadský	kanadský	k2eAgInSc1d1
•	•	k?
rys	rys	k1gInSc1
ostrovid	ostrovid	k1gMnSc1
•	•	k?
rys	rys	k1gMnSc1
iberský	iberský	k2eAgMnSc1d1
•	•	k?
rys	rys	k1gMnSc1
červený	červený	k2eAgMnSc1d1
•	•	k?
karakal	karakal	k1gMnSc1
•	•	k?
serval	serval	k1gMnSc1
•	•	k?
kočka	kočka	k1gFnSc1
zlatá	zlatý	k2eAgFnSc1d1
•	•	k?
jaguarundi	jaguarundit	k5eAaPmRp2nS
•	•	k?
puma	puma	k1gFnSc1
•	•	k?
gepard	gepard	k1gMnSc1
Velké	velká	k1gFnSc2
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
levhart	levhart	k1gMnSc1
obláčkový	obláčkový	k2eAgMnSc1d1
•	•	k?
levhart	levhart	k1gMnSc1
Diardův	Diardův	k2eAgMnSc1d1
•	•	k?
irbis	irbis	k1gInSc1
•	•	k?
lev	lev	k1gMnSc1
•	•	k?
tygr	tygr	k1gMnSc1
•	•	k?
levhart	levhart	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
•	•	k?
jaguár	jaguár	k1gMnSc1
†	†	k?
<g/>
Machairodontinae	Machairodontinae	k1gInSc1
</s>
<s>
Adelphailurus	Adelphailurus	k1gMnSc1
•	•	k?
Amphimachairodus	Amphimachairodus	k1gMnSc1
•	•	k?
Dinofelis	Dinofelis	k1gFnSc2
•	•	k?
Hemimachairodus	Hemimachairodus	k1gMnSc1
•	•	k?
Homotherium	Homotherium	k1gNnSc4
•	•	k?
Lokotunjailurus	Lokotunjailurus	k1gMnSc1
•	•	k?
Machairodus	Machairodus	k1gMnSc1
•	•	k?
Megantereon	Megantereon	k1gMnSc1
•	•	k?
Metailurus	Metailurus	k1gMnSc1
•	•	k?
Miomachairodus	Miomachairodus	k1gMnSc1
•	•	k?
Nimravides	Nimravides	k1gMnSc1
•	•	k?
Paramachairodus	Paramachairodus	k1gMnSc1
•	•	k?
Pontosmilus	Pontosmilus	k1gMnSc1
•	•	k?
Rhizosmilodon	Rhizosmilodon	k1gMnSc1
•	•	k?
Smilodon	Smilodon	k1gMnSc1
•	•	k?
Stenailurus	Stenailurus	k1gMnSc1
•	•	k?
Xenosmilus	Xenosmilus	k1gMnSc1
hodsonae	hodsona	k1gFnSc2
†	†	k?
<g/>
Proailurinae	Proailurina	k1gFnSc2
</s>
<s>
Proailurus	Proailurus	k1gInSc1
•	•	k?
Stenogale	Stenogala	k1gFnSc3
</s>
<s>
Savci	savec	k1gMnPc1
Podtřída	podtřída	k1gFnSc1
vejcorodí	vejcorodý	k2eAgMnPc1d1
</s>
<s>
řád	řád	k1gInSc1
ptakořitní	ptakořitní	k2eAgFnSc1d1
Podtřída	podtřída	k1gFnSc1
živorodí	živorodý	k2eAgMnPc5d1
</s>
<s>
Nadřád	nadřád	k1gInSc4
vačnatci	vačnatec	k1gMnPc1
</s>
<s>
řády	řád	k1gInPc1
<g/>
:	:	kIx,
vačice	vačice	k1gFnSc1
•	•	k?
vačíci	vačík	k1gMnPc1
•	•	k?
kolokolové	kolokolový	k2eAgNnSc1d1
•	•	k?
kunovci	kunovec	k1gInSc6
•	•	k?
bandikuti	bandikuť	k1gFnSc2
•	•	k?
vakokrti	vakokrt	k1gMnPc1
•	•	k?
dvojitozubci	dvojitozubec	k1gInSc6
Nadřád	nadřád	k1gInSc1
placentálové	placentálový	k2eAgInPc1d1
</s>
<s>
Afrotheria	Afrotherium	k1gNnPc1
(	(	kIx(
<g/>
afrosoricida	afrosoricida	k1gFnSc1
•	•	k?
bércouni	bércoun	k1gMnPc1
•	•	k?
hrabáči	hrabáč	k1gInSc6
•	•	k?
damani	daman	k1gMnPc1
•	•	k?
chobotnatci	chobotnatec	k1gMnSc3
•	•	k?
sirény	siréna	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
chudozubí	chudozubí	k1gMnPc1
•	•	k?
Laurasiatheria	Laurasiatherium	k1gNnSc2
(	(	kIx(
<g/>
hmyzožravci	hmyzožravec	k1gMnPc1
•	•	k?
letouni	letoun	k1gMnPc1
•	•	k?
luskouni	luskoun	k1gMnPc1
•	•	k?
šelmy	šelma	k1gFnSc2
•	•	k?
lichokopytníci	lichokopytník	k1gMnPc1
•	•	k?
sudokopytníci	sudokopytník	k1gMnPc1
+	+	kIx~
kytovci	kytovec	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
Euarchontoglires	Euarchontoglires	k1gInSc1
(	(	kIx(
<g/>
letuchy	letucha	k1gFnPc1
•	•	k?
tany	tany	k?
•	•	k?
primáti	primát	k1gMnPc1
•	•	k?
hlodavci	hlodavec	k1gMnSc3
•	•	k?
zajícovci	zajícovec	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kočky	kočka	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4163488-3	4163488-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85047696	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85047696	#num#	k4
</s>
