<s>
Roman	Roman	k1gMnSc1	Roman
Hogen	Hogen	k2eAgMnSc1d1	Hogen
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
Satalicích	Satalice	k1gFnPc6	Satalice
<g/>
,	,	kIx,	,
v	v	k7c6	v
mládežnických	mládežnický	k2eAgFnPc6d1	mládežnická
kategoriích	kategorie	k1gFnPc6	kategorie
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
Spartě	Sparta	k1gFnSc6	Sparta
a	a	k8xC	a
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
jako	jako	k9	jako
hráč	hráč	k1gMnSc1	hráč
Blšan	Blšan	k1gMnSc1	Blšan
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
hostoval	hostovat	k5eAaImAgInS	hostovat
ve	v	k7c6	v
Viktorii	Viktoria	k1gFnSc6	Viktoria
Žižkov	Žižkov	k1gInSc4	Žižkov
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
hrál	hrát	k5eAaImAgMnS	hrát
ve	v	k7c6	v
Slávii	Slávia	k1gFnSc6	Slávia
<g/>
,	,	kIx,	,
hostoval	hostovat	k5eAaImAgInS	hostovat
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Nürnberg	Nürnberg	k1gInSc1	Nürnberg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgInS	vracet
do	do	k7c2	do
Blšan	Blšana	k1gFnPc2	Blšana
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgMnSc1d1	klasický
koncový	koncový	k2eAgMnSc1d1	koncový
útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
výborný	výborný	k2eAgMnSc1d1	výborný
hlavičkář	hlavičkář	k1gMnSc1	hlavičkář
<g/>
,	,	kIx,	,
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
přestal	přestat	k5eAaPmAgMnS	přestat
dávat	dávat	k5eAaImF	dávat
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Slávií	Slávia	k1gFnSc7	Slávia
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
ligový	ligový	k2eAgInSc4d1	ligový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lize	liga	k1gFnSc6	liga
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
ke	k	k7c3	k
118	[number]	k4	118
utkáním	utkání	k1gNnPc3	utkání
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
31	[number]	k4	31
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
i	i	k8xC	i
zatracený	zatracený	k2eAgInSc1d1	zatracený
Hogen	Hogen	k1gInSc1	Hogen
teď	teď	k6eAd1	teď
chybí	chybět	k5eAaImIp3nS	chybět
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c6	na
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
www.sportovci.cz	www.sportovci.cz	k1gInSc1	www.sportovci.cz
Transfermarkt	Transfermarkt	k1gInSc1	Transfermarkt
</s>
