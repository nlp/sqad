<s>
Olymp	Olymp	k1gInSc1	Olymp
je	být	k5eAaImIp3nS	být
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc1	sídlo
řeckých	řecký	k2eAgMnPc2d1	řecký
bohů	bůh	k1gMnPc2	bůh
<g/>
:	:	kIx,	:
Dia	Dia	k1gFnPc1	Dia
<g/>
,	,	kIx,	,
Afrodíté	Afrodítý	k2eAgFnPc1d1	Afrodítý
<g/>
,	,	kIx,	,
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
Athény	Athéna	k1gFnPc1	Athéna
<g/>
,	,	kIx,	,
Herma	Hermes	k1gMnSc2	Hermes
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
