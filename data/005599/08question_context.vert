<s>
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
též	též	k9	též
vitriol	vitriol	k1gInSc1	vitriol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
dvojsytná	dvojsytný	k2eAgFnSc1d1	dvojsytný
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
průmyslově	průmyslově	k6eAd1	průmyslově
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
sumární	sumární	k2eAgInSc1d1	sumární
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgInSc2	jeden
atomu	atom	k1gInSc2	atom
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
čtyř	čtyři	k4xCgInPc2	čtyři
atomů	atom	k1gInPc2	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
