<s>
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
též	též	k9	též
vitriol	vitriol	k1gInSc1	vitriol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
dvojsytná	dvojsytný	k2eAgFnSc1d1	dvojsytný
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
průmyslově	průmyslově	k6eAd1	průmyslově
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
sumární	sumární	k2eAgInSc1d1	sumární
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgInSc2	jeden
atomu	atom	k1gInSc2	atom
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
čtyř	čtyři	k4xCgInPc2	čtyři
atomů	atom	k1gInPc2	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
probíhá	probíhat	k5eAaImIp3nS	probíhat
třístupňově	třístupňově	k6eAd1	třístupňově
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
získává	získávat	k5eAaImIp3nS	získávat
buď	buď	k8xC	buď
přímým	přímý	k2eAgNnSc7d1	přímé
spalováním	spalování	k1gNnSc7	spalování
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
S	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
SO_	SO_	k1gMnSc6	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
pražením	pražení	k1gNnSc7	pražení
pyritu	pyrit	k1gInSc2	pyrit
či	či	k8xC	či
markazitu	markazit	k1gInSc2	markazit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
F	F	kA	F
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
8	[number]	k4	8
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
O	o	k7c4	o
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
FeS_	FeS_	k1gMnSc1	FeS_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
11	[number]	k4	11
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
8	[number]	k4	8
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Fe_	Fe_	k1gMnSc1	Fe_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
nebo	nebo	k8xC	nebo
pražením	pražení	k1gNnSc7	pražení
sulfidu	sulfid	k1gInSc2	sulfid
železnatého	železnatý	k2eAgInSc2d1	železnatý
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
sulfidů	sulfid	k1gInPc2	sulfid
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
F	F	kA	F
e	e	k0	e
S	s	k7c7	s
+	+	kIx~	+
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
FeS	fes	k1gNnSc1	fes
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Fe_	Fe_	k1gMnSc1	Fe_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Druhým	druhý	k4xOgInSc7	druhý
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
oxidace	oxidace	k1gFnSc1	oxidace
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
sírový	sírový	k2eAgInSc4d1	sírový
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
se	se	k3xPyFc4	se
jako	jako	k9	jako
katalyzátoru	katalyzátor	k1gInSc2	katalyzátor
používá	používat	k5eAaImIp3nS	používat
oxidu	oxid	k1gInSc2	oxid
vanadičného	vanadičný	k2eAgMnSc4d1	vanadičný
V2O5	V2O5	k1gMnSc4	V2O5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
O	o	k7c4	o
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
xrightarrow	xrightarrow	k?	xrightarrow
{	{	kIx(	{
<g/>
V_	V_	k1gMnSc1	V_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kontaktní	kontaktní	k2eAgInSc1d1	kontaktní
způsob	způsob	k1gInSc1	způsob
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
N	N	kA	N
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
N	N	kA	N
O	O	kA	O
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
NO_	NO_	k1gMnSc1	NO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
NO	no	k9	no
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
komorový	komorový	k2eAgInSc1d1	komorový
způsob	způsob	k1gInSc1	způsob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
sírového	sírový	k2eAgMnSc4d1	sírový
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
(	(	kIx(	(
l	l	kA	l
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
O	O	kA	O
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
V	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s hack="1">
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
(	(	kIx(	(
<g/>
96	[number]	k4	96
<g/>
-	-	kIx~	-
<g/>
98	[number]	k4	98
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jako	jako	k8xS	jako
mezistupeň	mezistupeň	k1gInSc1	mezistupeň
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
disírová	disírová	k1gFnSc1	disírová
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
jen	jen	k9	jen
hemihydrát	hemihydrát	k1gInSc4	hemihydrát
oxidu	oxid	k1gInSc2	oxid
sírového	sírový	k2eAgInSc2d1	sírový
2	[number]	k4	2
SO	So	kA	So
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
(	(	kIx(	(
l	l	kA	l
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gFnSc1	H_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	O	kA	O
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Dalším	další	k2eAgNnSc7d1	další
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
sírového	sírový	k2eAgInSc2d1	sírový
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
disírová	disírový	k2eAgFnSc1d1	disírový
a	a	k8xC	a
následně	následně	k6eAd1	následně
tzv.	tzv.	kA	tzv.
oleum	oleum	k1gNnSc1	oleum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
ředěním	ředění	k1gNnSc7	ředění
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
koncentrace	koncentrace	k1gFnPc4	koncentrace
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc1	O_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
stavu	stav	k1gInSc6	stav
hustá	hustý	k2eAgFnSc1d1	hustá
olejnatá	olejnatý	k2eAgFnSc1d1	olejnatá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
neomezeně	omezeně	k6eNd1	omezeně
mísitelná	mísitelný	k2eAgFnSc1d1	mísitelná
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ředění	ředění	k1gNnSc1	ředění
této	tento	k3xDgFnSc2	tento
kyseliny	kyselina	k1gFnSc2	kyselina
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
exotermní	exotermní	k2eAgInSc1d1	exotermní
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
(	(	kIx(	(
<g/>
96	[number]	k4	96
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgInPc4d1	silný
dehydratační	dehydratační	k2eAgInPc4d1	dehydratační
a	a	k8xC	a
oxidační	oxidační	k2eAgInPc4d1	oxidační
účinky	účinek	k1gInPc4	účinek
(	(	kIx(	(
<g/>
zvlášť	zvlášť	k6eAd1	zvlášť
za	za	k7c4	za
horka	horko	k1gNnPc4	horko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
vodní	vodní	k2eAgInPc4d1	vodní
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
žíravinou	žíravina	k1gFnSc7	žíravina
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
dehydrataci	dehydratace	k1gFnSc4	dehydratace
(	(	kIx(	(
<g/>
zuhelnatění	zuhelnatění	k1gNnSc2	zuhelnatění
<g/>
)	)	kIx)	)
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Zředěná	zředěný	k2eAgFnSc1d1	zředěná
kyselina	kyselina	k1gFnSc1	kyselina
oxidační	oxidační	k2eAgFnSc2d1	oxidační
schopnosti	schopnost	k1gFnSc2	schopnost
nemá	mít	k5eNaImIp3nS	mít
a	a	k8xC	a
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
neušlechtilými	ušlechtilý	k2eNgInPc7d1	neušlechtilý
kovy	kov	k1gInPc7	kov
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
síranů	síran	k1gInPc2	síran
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
téměř	téměř	k6eAd1	téměř
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
kovy	kov	k1gInPc7	kov
kromě	kromě	k7c2	kromě
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
v	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
stavu	stav	k1gInSc6	stav
jej	on	k3xPp3gMnSc4	on
pasivuje	pasivovat	k5eAaBmIp3nS	pasivovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
platiny	platina	k1gFnSc2	platina
a	a	k8xC	a
wolframu	wolfram	k1gInSc2	wolfram
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
%	%	kIx~	%
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
oxidu	oxid	k1gInSc2	oxid
sírového	sírový	k2eAgInSc2d1	sírový
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgNnSc4d1	sírové
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
oleum	oleum	k1gNnSc1	oleum
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
tvoří	tvořit	k5eAaImIp3nS	tvořit
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
solí	sůl	k1gFnPc2	sůl
–	–	k?	–
sírany	síran	k1gInPc1	síran
a	a	k8xC	a
hydrogensírany	hydrogensíran	k1gInPc1	hydrogensíran
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
její	její	k3xOp3gFnPc1	její
soli	sůl	k1gFnPc1	sůl
tvoří	tvořit	k5eAaImIp3nP	tvořit
hydráty	hydrát	k1gInPc4	hydrát
<g/>
.	.	kIx.	.
</s>
<s>
Neušlechtilé	ušlechtilý	k2eNgInPc1d1	neušlechtilý
kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
za	za	k7c4	za
vývoje	vývoj	k1gInPc4	vývoj
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
příslušných	příslušný	k2eAgInPc2d1	příslušný
síranů	síran	k1gInPc2	síran
<g/>
,	,	kIx,	,
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
Z	z	k7c2	z
n	n	k0	n
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
Z	z	k7c2	z
n	n	k0	n
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc1	SO_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
Zn	zn	kA	zn
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
ZnSO_	ZnSO_	k1gMnSc1	ZnSO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
reaguje	reagovat	k5eAaBmIp3nS	reagovat
se	s	k7c7	s
zinkem	zinek	k1gInSc7	zinek
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
síranu	síran	k1gInSc2	síran
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
A	a	k8xC	a
l	l	kA	l
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
A	A	kA	A
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
Al	ala	k1gFnPc2	ala
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
Al_	Al_	k1gMnSc1	Al_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
hliníkem	hliník	k1gInSc7	hliník
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
síranu	síran	k1gInSc2	síran
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
většina	většina	k1gFnSc1	většina
oxidů	oxid	k1gInPc2	oxid
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
solí	sůl	k1gFnPc2	sůl
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
u	u	k7c2	u
O	O	kA	O
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
(	(	kIx(	(
l	l	kA	l
)	)	kIx)	)
+	+	kIx~	+
C	C	kA	C
u	u	k7c2	u
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
CuO	CuO	k1gMnSc1	CuO
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
H_	H_	k1gFnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	O	kA	O
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
CuSO_	CuSO_	k1gMnSc1	CuSO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Oxid	oxid	k1gInSc1	oxid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
síranu	síran	k1gInSc2	síran
měďnatého	měďnatý	k2eAgInSc2d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc7	jeho
vodným	vodný	k2eAgInSc7d1	vodný
roztokem	roztok	k1gInSc7	roztok
(	(	kIx(	(
<g/>
čpavkem	čpavek	k1gInSc7	čpavek
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
síran	síran	k1gInSc1	síran
amonný	amonný	k2eAgInSc1d1	amonný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
(	(	kIx(	(
N	N	kA	N
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NH_	NH_	k1gMnSc1	NH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
(	(	kIx(	(
<g/>
NH_	NH_	k1gMnSc1	NH_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
resp.	resp.	kA	resp.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
H	H	kA	H
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
(	(	kIx(	(
N	N	kA	N
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
(	(	kIx(	(
l	l	kA	l
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NH_	NH_	k1gMnSc1	NH_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
OH	OH	kA	OH
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc1	SO_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
(	(	kIx(	(
<g/>
NH_	NH_	k1gMnSc1	NH_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	O	kA	O
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Průmyslově	průmyslově	k6eAd1	průmyslově
významnou	významný	k2eAgFnSc7d1	významná
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
s	s	k7c7	s
fosforečnanem	fosforečnan	k1gInSc7	fosforečnan
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
síranu	síran	k1gInSc2	síran
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
monohydrogenfosforečnanu	monohydrogenfosforečnan	k1gInSc2	monohydrogenfosforečnan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
,	,	kIx,	,
dihydrogenfosforečnanu	dihydrogenfosforečnan	k1gInSc2	dihydrogenfosforečnan
vápenatého	vápenatý	k2eAgNnSc2d1	vápenaté
a	a	k8xC	a
volné	volný	k2eAgFnPc1d1	volná
kyseliny	kyselina	k1gFnPc1	kyselina
fosforečné	fosforečný	k2eAgFnPc1d1	fosforečná
známá	známá	k1gFnSc1	známá
jako	jako	k8xC	jako
fosforečné	fosforečný	k2eAgNnSc1d1	fosforečné
hnojivo	hnojivo	k1gNnSc1	hnojivo
superfosfát	superfosfát	k1gInSc1	superfosfát
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
P	P	kA	P
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
H	H	kA	H
P	P	kA	P
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
C	C	kA	C
a	a	k8xC	a
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
Ca_	Ca_	k1gMnSc1	Ca_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
PO_	PO_	k1gMnSc1	PO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CaHPO_	CaHPO_	k1gMnSc1	CaHPO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
CaSO_	CaSO_	k1gMnSc1	CaSO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
P	P	kA	P
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
(	(	kIx(	(
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
Ca_	Ca_	k1gMnSc1	Ca_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
PO_	PO_	k1gMnSc1	PO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
PO_	PO_	k1gFnSc1	PO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CaSO_	CaSO_	k1gMnSc1	CaSO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
P	P	kA	P
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
q	q	k?	q
)	)	kIx)	)
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
Ca_	Ca_	k1gMnSc1	Ca_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
PO_	PO_	k1gMnSc1	PO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
SO_	SO_	k1gFnSc2	SO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
PO_	PO_	k1gMnSc6	PO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
aq	aq	k?	aq
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CaSO_	CaSO_	k1gMnSc1	CaSO_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Touto	tento	k3xDgFnSc7	tento
reakcí	reakce	k1gFnSc7	reakce
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
téměř	téměř	k6eAd1	téměř
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
fosforečnan	fosforečnan	k1gInSc1	fosforečnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
směs	směs	k1gFnSc4	směs
rozpustnějších	rozpustný	k2eAgInPc2d2	rozpustnější
kyselých	kyselý	k2eAgInPc2d1	kyselý
fosforečnanů	fosforečnan	k1gInPc2	fosforečnan
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
kyseliny	kyselina	k1gFnPc1	kyselina
fosforečné	fosforečný	k2eAgFnPc1d1	fosforečná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
využití	využití	k1gNnSc1	využití
fosforu	fosfor	k1gInSc2	fosfor
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
hnojiv	hnojivo	k1gNnPc2	hnojivo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
chemikálií	chemikálie	k1gFnPc2	chemikálie
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
plastů	plast	k1gInPc2	plast
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
léčiv	léčivo	k1gNnPc2	léčivo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barviv	barvivo	k1gNnPc2	barvivo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
výbušnin	výbušnina	k1gFnPc2	výbušnina
v	v	k7c6	v
papírenském	papírenský	k2eAgInSc6d1	papírenský
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
průmyslu	průmysl	k1gInSc6	průmysl
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
syntetických	syntetický	k2eAgNnPc2d1	syntetické
vláken	vlákno	k1gNnPc2	vlákno
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
rud	ruda	k1gFnPc2	ruda
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
ropy	ropa	k1gFnSc2	ropa
jako	jako	k8xC	jako
elektrolyt	elektrolyt	k1gInSc1	elektrolyt
do	do	k7c2	do
olověných	olověný	k2eAgInPc2d1	olověný
akumulátorů	akumulátor	k1gInPc2	akumulátor
při	při	k7c6	při
sušení	sušení	k1gNnSc6	sušení
a	a	k8xC	a
odvodňování	odvodňování	k1gNnSc6	odvodňování
látek	látka	k1gFnPc2	látka
při	při	k7c6	při
úpravě	úprava	k1gFnSc6	úprava
pH	ph	kA	ph
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
Kolových	kolový	k2eAgInPc2d1	kolový
nápojů	nápoj	k1gInPc2	nápoj
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Sulfuric	Sulfuric	k1gMnSc1	Sulfuric
acid	acid	k1gMnSc1	acid
-	-	kIx~	-
UNEP	UNEP	kA	UNEP
publication	publication	k1gInSc1	publication
(	(	kIx(	(
<g/>
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
toxikologie	toxikologie	k1gFnSc1	toxikologie
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
