<s>
Caerphilly	Caerphilla	k1gFnPc1
(	(	kIx(
<g/>
hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Caerphilly	Caerphill	k1gInPc1
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc4
na	na	k7c4
hrad	hrad	k1gInSc4
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1268	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Caerphilly	Caerphilla	k1gFnPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc3
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
94,9	94,9	k4
m	m	kA
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
33,79	33,79	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
13,2	13,2	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
13539	#num#	k4
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jihovýchodní	jihovýchodní	k2eAgFnSc1d1
„	„	k?
<g/>
šikmá	šikmý	k2eAgFnSc1d1
<g/>
“	“	k?
věž	věž	k1gFnSc1
</s>
<s>
Hrad	hrad	k1gInSc1
Caerphilly	Caerphilly	k1gFnSc1
(	(	kIx(
<g/>
velšsky	velšsky	k6eAd1
Castell	Castell	k1gMnSc1
Caerffili	Caerffili	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3
hrad	hrad	k1gInSc1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
10	#num#	k4
kilometrů	kilometr	k1gInPc2
severně	severně	k6eAd1
od	od	k7c2
Cardiffu	Cardiff	k1gInSc2
ve	v	k7c6
Walesu	Wales	k1gInSc6
a	a	k8xC
rozprostírá	rozprostírat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ploše	plocha	k1gFnSc6
větší	veliký	k2eAgFnSc4d2
než	než	k8xS
30	#num#	k4
akrů	akr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Caerphilly	Caerphilla	k1gFnSc2
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejrozsáhlejších	rozsáhlý	k2eAgFnPc2d3
středověkých	středověký	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
stavbu	stavba	k1gFnSc4
započal	započnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1268	#num#	k4
anglo-normanský	anglo-normanský	k2eAgMnSc1d1
lord	lord	k1gMnSc1
Gilbert	Gilbert	k1gMnSc1
z	z	k7c2
Clare	Clar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrad	hrad	k1gInSc1
byl	být	k5eAaImAgInS
upraven	upravit	k5eAaPmNgInS
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soustředné	soustředný	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
kamenného	kamenný	k2eAgNnSc2d1
a	a	k8xC
vodního	vodní	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
jsou	být	k5eAaImIp3nP
impozantní	impozantní	k2eAgFnSc4d1
ještě	ještě	k9
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
proslulý	proslulý	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc7
jihovýchodní	jihovýchodní	k2eAgFnSc7d1
„	„	k?
<g/>
šikmou	šikma	k1gFnSc7
věží	věž	k1gFnSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
následek	následek	k1gInSc1
bojů	boj	k1gInPc2
během	během	k7c2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působivá	působivý	k2eAgFnSc1d1
aula	aula	k1gFnSc1
(	(	kIx(
<g/>
great	great	k2eAgInSc1d1
hall	hall	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
k	k	k7c3
různým	různý	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstupné	vstupné	k1gNnSc1
pro	pro	k7c4
dospělé	dospělí	k1gMnPc4
činí	činit	k5eAaImIp3nS
6	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Caerphilly	Caerphilla	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
316594010	#num#	k4
</s>
