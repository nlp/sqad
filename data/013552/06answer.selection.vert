<s>
Hrad	hrad	k1gInSc1
Caerphilly	Caerphilly	k1gFnSc1
(	(	kIx(
<g/>
velšsky	velšsky	k6eAd1
Castell	Castell	k1gMnSc1
Caerffili	Caerffili	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3
hrad	hrad	k1gInSc1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
10	#num#	k4
kilometrů	kilometr	k1gInPc2
severně	severně	k6eAd1
od	od	k7c2
Cardiffu	Cardiff	k1gInSc2
ve	v	k7c6
Walesu	Wales	k1gInSc6
a	a	k8xC
rozprostírá	rozprostírat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ploše	plocha	k1gFnSc6
větší	veliký	k2eAgFnSc4d2
než	než	k8xS
30	#num#	k4
akrů	akr	k1gInPc2
<g/>
.	.	kIx.
</s>