<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
IDS	IDS	kA	IDS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
dopravní	dopravní	k2eAgFnSc2d1	dopravní
obsluhy	obsluha	k1gFnSc2	obsluha
určitého	určitý	k2eAgNnSc2d1	určité
uceleného	ucelený	k2eAgNnSc2d1	ucelené
území	území	k1gNnSc2	území
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
dopravou	doprava	k1gFnSc7	doprava
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
např.	např.	kA	např.
městskou	městský	k2eAgFnSc7d1	městská
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc7d1	regionální
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc7d1	železniční
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
linky	linka	k1gFnPc4	linka
více	hodně	k6eAd2	hodně
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
jsou	být	k5eAaImIp3nP	být
cestující	cestující	k1gMnPc1	cestující
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
přepravováni	přepravovat	k5eAaImNgMnP	přepravovat
podle	podle	k7c2	podle
jednotných	jednotný	k2eAgFnPc2d1	jednotná
přepravních	přepravní	k2eAgFnPc2d1	přepravní
a	a	k8xC	a
tarifních	tarifní	k2eAgFnPc2d1	tarifní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
