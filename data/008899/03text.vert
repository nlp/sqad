<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
IDS	IDS	kA	IDS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
dopravní	dopravní	k2eAgFnSc2d1	dopravní
obsluhy	obsluha	k1gFnSc2	obsluha
určitého	určitý	k2eAgNnSc2d1	určité
uceleného	ucelený	k2eAgNnSc2d1	ucelené
území	území	k1gNnSc2	území
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
dopravou	doprava	k1gFnSc7	doprava
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
např.	např.	kA	např.
městskou	městský	k2eAgFnSc7d1	městská
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc7d1	regionální
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc7d1	železniční
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
linky	linka	k1gFnPc4	linka
více	hodně	k6eAd2	hodně
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
jsou	být	k5eAaImIp3nP	být
cestující	cestující	k1gMnPc1	cestující
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
přepravováni	přepravovat	k5eAaImNgMnP	přepravovat
podle	podle	k7c2	podle
jednotných	jednotný	k2eAgFnPc2d1	jednotná
přepravních	přepravní	k2eAgFnPc2d1	přepravní
a	a	k8xC	a
tarifních	tarifní	k2eAgFnPc2d1	tarifní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doprava	doprava	k1gFnSc1	doprava
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
IDS	IDS	kA	IDS
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
různými	různý	k2eAgInPc7d1	různý
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
:	:	kIx,	:
železnicí	železnice	k1gFnSc7	železnice
<g/>
,	,	kIx,	,
metrem	metr	k1gInSc7	metr
<g/>
,	,	kIx,	,
tramvajemi	tramvaj	k1gFnPc7	tramvaj
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc7	trolejbus
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
lanovkami	lanovka	k1gFnPc7	lanovka
nebo	nebo	k8xC	nebo
plavidly	plavidlo	k1gNnPc7	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Integrace	integrace	k1gFnSc1	integrace
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
návaznosti	návaznost	k1gFnSc2	návaznost
na	na	k7c4	na
cyklistickou	cyklistický	k2eAgFnSc4d1	cyklistická
nebo	nebo	k8xC	nebo
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
dopravou	doprava	k1gFnSc7	doprava
formou	forma	k1gFnSc7	forma
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
nebo	nebo	k8xC	nebo
K	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
R.	R.	kA	R.
Na	na	k7c6	na
dopravě	doprava	k1gFnSc6	doprava
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
IDS	IDS	kA	IDS
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
účastnit	účastnit	k5eAaImF	účastnit
různí	různý	k2eAgMnPc1d1	různý
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jízdní	jízdní	k2eAgInPc4d1	jízdní
řády	řád	k1gInPc4	řád
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
linek	linka	k1gFnPc2	linka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
IDS	IDS	kA	IDS
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
optimalizovány	optimalizovat	k5eAaBmNgFnP	optimalizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dopravce	dopravce	k1gMnSc1	dopravce
dotyčnou	dotyčný	k2eAgFnSc4d1	dotyčná
linku	linka	k1gFnSc4	linka
provozuje	provozovat	k5eAaImIp3nS	provozovat
<g/>
.	.	kIx.	.
</s>
<s>
Cestující	cestující	k1gMnPc1	cestující
v	v	k7c6	v
integrované	integrovaný	k2eAgFnSc6d1	integrovaná
dopravě	doprava	k1gFnSc6	doprava
používají	používat	k5eAaImIp3nP	používat
jednotné	jednotný	k2eAgFnPc1d1	jednotná
jízdenky	jízdenka	k1gFnPc1	jízdenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
systému	systém	k1gInSc6	systém
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
dopravce	dopravce	k1gMnSc4	dopravce
a	a	k8xC	a
použitý	použitý	k2eAgInSc4d1	použitý
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
integrace	integrace	k1gFnSc2	integrace
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
IDS	IDS	kA	IDS
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charakter	charakter	k1gInSc1	charakter
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
i	i	k9	i
stávající	stávající	k2eAgInPc4d1	stávající
systémy	systém	k1gInPc4	systém
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
nebo	nebo	k8xC	nebo
systém	systém	k1gInSc1	systém
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
celostátních	celostátní	k2eAgFnPc6d1	celostátní
a	a	k8xC	a
regionálních	regionální	k2eAgFnPc6d1	regionální
železničních	železniční	k2eAgFnPc6d1	železniční
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
označují	označovat	k5eAaImIp3nP	označovat
až	až	k9	až
dopravní	dopravní	k2eAgInPc4d1	dopravní
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
rozšířením	rozšíření	k1gNnSc7	rozšíření
stávajícího	stávající	k2eAgInSc2d1	stávající
systému	systém	k1gInSc2	systém
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
integrací	integrace	k1gFnPc2	integrace
více	hodně	k6eAd2	hodně
tradičních	tradiční	k2eAgInPc2d1	tradiční
dopravních	dopravní	k2eAgInPc2d1	dopravní
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgFnSc1d1	tradiční
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
příměstské	příměstský	k2eAgFnPc1d1	příměstská
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zavedením	zavedení	k1gNnSc7	zavedení
zónového	zónový	k2eAgInSc2d1	zónový
tarifu	tarif	k1gInSc2	tarif
v	v	k7c6	v
uceleném	ucelený	k2eAgInSc6d1	ucelený
širším	široký	k2eAgInSc6d2	širší
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc2d1	Česká
IDS	IDS	kA	IDS
zpravidla	zpravidla	k6eAd1	zpravidla
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
zónový	zónový	k2eAgInSc1d1	zónový
nebo	nebo	k8xC	nebo
pásmový	pásmový	k2eAgInSc1d1	pásmový
tarif	tarif	k1gInSc1	tarif
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
území	území	k1gNnPc1	území
s	s	k7c7	s
integrovaným	integrovaný	k2eAgInSc7d1	integrovaný
dopravním	dopravní	k2eAgInSc7d1	dopravní
systémem	systém	k1gInSc7	systém
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
zóny	zóna	k1gFnPc4	zóna
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
soustředných	soustředný	k2eAgInPc2d1	soustředný
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
mezikruží	mezikruží	k1gNnSc2	mezikruží
čili	čili	k8xC	čili
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
pásma	pásmo	k1gNnSc2	pásmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pásmové	pásmový	k2eAgNnSc1d1	pásmové
rozdělení	rozdělení	k1gNnSc1	rozdělení
území	území	k1gNnSc2	území
IDS	IDS	kA	IDS
je	být	k5eAaImIp3nS	být
výhodnější	výhodný	k2eAgFnSc1d2	výhodnější
v	v	k7c6	v
případě	případ	k1gInSc6	případ
menšího	malý	k2eAgNnSc2d2	menší
území	území	k1gNnSc2	území
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
městskou	městský	k2eAgFnSc7d1	městská
aglomerací	aglomerace	k1gFnSc7	aglomerace
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgNnSc2	který
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
převládají	převládat	k5eAaImIp3nP	převládat
radiální	radiální	k2eAgInPc1d1	radiální
přepravní	přepravní	k2eAgInPc1d1	přepravní
proudy	proud	k1gInPc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
zóny	zóna	k1gFnPc4	zóna
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
v	v	k7c6	v
území	území	k1gNnSc6	území
s	s	k7c7	s
více	hodně	k6eAd2	hodně
regionálními	regionální	k2eAgInPc7d1	regionální
centry	centr	k1gInPc7	centr
a	a	k8xC	a
větším	veliký	k2eAgInSc7d2	veliký
podílem	podíl	k1gInSc7	podíl
mezizónové	mezizónový	k2eAgFnSc2d1	mezizónový
přepravy	přeprava	k1gFnSc2	přeprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
integrovaný	integrovaný	k2eAgInSc4d1	integrovaný
dopravní	dopravní	k2eAgInSc4d1	dopravní
systém	systém	k1gInSc4	systém
podobného	podobný	k2eAgInSc2d1	podobný
typu	typ	k1gInSc2	typ
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
v	v	k7c6	v
Hamburgu	Hamburg	k1gInSc6	Hamburg
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
zprovoznění	zprovoznění	k1gNnSc2	zprovoznění
páteřní	páteřní	k2eAgFnSc2d1	páteřní
linky	linka	k1gFnSc2	linka
S-Bahnu	S-Bahnu	k1gFnSc2	S-Bahnu
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byly	být	k5eAaImAgFnP	být
organizátorské	organizátorský	k2eAgFnPc4d1	organizátorská
firmy	firma	k1gFnPc4	firma
společenstvím	společenství	k1gNnSc7	společenství
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
organizátorské	organizátorský	k2eAgFnSc2d1	organizátorská
firmy	firma	k1gFnSc2	firma
přešly	přejít	k5eAaPmAgInP	přejít
z	z	k7c2	z
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
dopravců	dopravce	k1gMnPc2	dopravce
do	do	k7c2	do
společného	společný	k2eAgNnSc2d1	společné
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
objednatelů	objednatel	k1gMnPc2	objednatel
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Integrované	integrovaný	k2eAgInPc1d1	integrovaný
dopravní	dopravní	k2eAgInPc1d1	dopravní
systémy	systém	k1gInPc1	systém
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
integrované	integrovaný	k2eAgInPc4d1	integrovaný
dopravní	dopravní	k2eAgInPc4d1	dopravní
systémy	systém	k1gInPc1	systém
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zejména	zejména	k9	zejména
tyto	tento	k3xDgInPc1	tento
integrované	integrovaný	k2eAgInPc1d1	integrovaný
dopravní	dopravní	k2eAgInPc1d1	dopravní
systémy	systém	k1gInPc1	systém
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
diskutabilní	diskutabilní	k2eAgMnSc1d1	diskutabilní
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
má	mít	k5eAaImIp3nS	mít
skutečně	skutečně	k6eAd1	skutečně
charakter	charakter	k1gInSc1	charakter
IDS	IDS	kA	IDS
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
integrace	integrace	k1gFnSc2	integrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
velkoplošných	velkoplošný	k2eAgInPc2d1	velkoplošný
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
systémů	systém	k1gInPc2	systém
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
mnohé	mnohý	k2eAgInPc1d1	mnohý
lokální	lokální	k2eAgInPc1d1	lokální
přesahy	přesah	k1gInPc1	přesah
městských	městský	k2eAgFnPc2d1	městská
hromadných	hromadný	k2eAgFnPc2d1	hromadná
doprav	doprava	k1gFnPc2	doprava
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
města	město	k1gNnSc2	město
či	či	k8xC	či
dohody	dohoda	k1gFnSc2	dohoda
dopravců	dopravce	k1gMnPc2	dopravce
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
uznávání	uznávání	k1gNnSc6	uznávání
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
PID	PID	kA	PID
</s>
</p>
<p>
<s>
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
SID	SID	kA	SID
</s>
</p>
<p>
<s>
Českobudějovická	českobudějovický	k2eAgFnSc1d1	českobudějovická
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
IDS	IDS	kA	IDS
ČB	ČB	kA	ČB
</s>
</p>
<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Táborska	Táborsk	k1gInSc2	Táborsk
IDS	IDS	kA	IDS
TA	ten	k3xDgFnSc1	ten
(	(	kIx(	(
<g/>
Tábor	tábor	k1gMnSc1	tábor
<g/>
,	,	kIx,	,
Sezimovo	Sezimův	k2eAgNnSc1d1	Sezimovo
Ústí	ústí	k1gNnSc1	ústí
<g/>
,	,	kIx,	,
Planá	Planá	k1gFnSc1	Planá
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
6	[number]	k4	6
obcí	obec	k1gFnPc2	obec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JIKORD	JIKORD	kA	JIKORD
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
(	(	kIx(	(
<g/>
Jihočeský	jihočeský	k2eAgMnSc1d1	jihočeský
koordinátor	koordinátor	k1gMnSc1	koordinátor
dopravy	doprava	k1gFnSc2	doprava
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
krajem	krajem	k6eAd1	krajem
jako	jako	k8xS	jako
jediným	jediný	k2eAgMnSc7d1	jediný
společníkem	společník	k1gMnSc7	společník
založena	založit	k5eAaPmNgFnS	založit
k	k	k7c3	k
18	[number]	k4	18
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Posouzení	posouzení	k1gNnSc1	posouzení
možnosti	možnost	k1gFnSc2	možnost
zavedení	zavedení	k1gNnSc2	zavedení
IDS	IDS	kA	IDS
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
funkčního	funkční	k2eAgInSc2d1	funkční
modelu	model	k1gInSc2	model
koordinace	koordinace	k1gFnSc1	koordinace
veřejné	veřejný	k2eAgFnSc2d1	veřejná
hromadné	hromadný	k2eAgFnSc2d1	hromadná
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úkolů	úkol	k1gInPc2	úkol
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
připravilo	připravit	k5eAaPmAgNnS	připravit
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něž	jenž	k3xRgMnPc4	jenž
by	by	kYmCp3nS	by
jízdenky	jízdenka	k1gFnPc4	jízdenka
vydával	vydávat	k5eAaImAgInS	vydávat
přímo	přímo	k6eAd1	přímo
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
příslib	příslib	k1gInSc4	příslib
na	na	k7c4	na
dotaci	dotace	k1gFnSc4	dotace
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
fondů	fond	k1gInPc2	fond
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
pracoviště	pracoviště	k1gNnSc4	pracoviště
krajského	krajský	k2eAgInSc2d1	krajský
IDS	IDS	kA	IDS
za	za	k7c4	za
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nové	nový	k2eAgNnSc1d1	nové
vedení	vedení	k1gNnSc1	vedení
kraje	kraj	k1gInSc2	kraj
od	od	k7c2	od
projektu	projekt	k1gInSc2	projekt
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
recese	recese	k1gFnSc2	recese
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
iDnes	iDnesa	k1gFnPc2	iDnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	se	k3xPyFc4	se
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2012	[number]	k4	2012
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
autobusové	autobusový	k2eAgFnSc2d1	autobusová
<g/>
,	,	kIx,	,
vlakové	vlakový	k2eAgFnSc2d1	vlaková
a	a	k8xC	a
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
znovu	znovu	k6eAd1	znovu
mluvilo	mluvit	k5eAaImAgNnS	mluvit
a	a	k8xC	a
větší	veliký	k2eAgNnSc4d2	veliký
jasno	jasno	k1gNnSc4	jasno
bylo	být	k5eAaImAgNnS	být
očekáváno	očekávat	k5eAaImNgNnS	očekávat
po	po	k7c6	po
říjnových	říjnový	k2eAgFnPc6d1	říjnová
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Náměstek	náměstek	k1gMnSc1	náměstek
českobudějovického	českobudějovický	k2eAgMnSc2d1	českobudějovický
primátora	primátor	k1gMnSc2	primátor
Miroslav	Miroslava	k1gFnPc2	Miroslava
Joch	Joch	k?	Joch
či	či	k8xC	či
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
ČSAD	ČSAD	kA	ČSAD
Autobusy	autobus	k1gInPc1	autobus
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
Vladimír	Vladimír	k1gMnSc1	Vladimír
Homola	Homola	k1gMnSc1	Homola
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
o	o	k7c6	o
zavádění	zavádění	k1gNnSc6	zavádění
IDS	IDS	kA	IDS
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Ivan	Ivan	k1gMnSc1	Ivan
Študlar	Študlar	k1gMnSc1	Študlar
z	z	k7c2	z
Jikordu	Jikord	k1gInSc2	Jikord
označil	označit	k5eAaPmAgMnS	označit
kvůli	kvůli	k7c3	kvůli
řídkosti	řídkost	k1gFnSc3	řídkost
osídlení	osídlení	k1gNnSc2	osídlení
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
dopady	dopad	k1gInPc4	dopad
IDS	IDS	kA	IDS
za	za	k7c4	za
sporné	sporný	k2eAgNnSc4d1	sporné
a	a	k8xC	a
Jikord	Jikord	k1gInSc4	Jikord
jej	on	k3xPp3gInSc4	on
připravoval	připravovat	k5eAaImAgInS	připravovat
jen	jen	k9	jen
pro	pro	k7c4	pro
Českobudějovicko	Českobudějovicko	k1gNnSc4	Českobudějovicko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Lataj	Lataj	k1gMnSc1	Lataj
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
dopravní	dopravní	k2eAgFnSc2d1	dopravní
komise	komise	k1gFnSc2	komise
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
a	a	k8xC	a
za	za	k7c4	za
podmínku	podmínka	k1gFnSc4	podmínka
úspěchu	úspěch	k1gInSc2	úspěch
zavedení	zavedení	k1gNnSc3	zavedení
jednotného	jednotný	k2eAgNnSc2d1	jednotné
tarifního	tarifní	k2eAgNnSc2d1	tarifní
jízdného	jízdné	k1gNnSc2	jízdné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
jízdenky	jízdenka	k1gFnPc1	jízdenka
vícedenní	vícedenní	k2eAgFnPc1d1	vícedenní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
základní	základní	k2eAgFnSc1d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
idnes	idnesa	k1gFnPc2	idnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
bude	být	k5eAaImBp3nS	být
pilotně	pilotně	k6eAd1	pilotně
zaveden	zaveden	k2eAgMnSc1d1	zaveden
IDS	IDS	kA	IDS
<g/>
,	,	kIx,	,
propojující	propojující	k2eAgFnSc4d1	propojující
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
a	a	k8xC	a
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
na	na	k7c4	na
Jindřichohradecku	Jindřichohradecka	k1gFnSc4	Jindřichohradecka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tomu	ten	k3xDgNnSc3	ten
předcházet	předcházet	k5eAaImF	předcházet
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
na	na	k7c4	na
provozovatele	provozovatel	k1gMnSc4	provozovatel
MHD	MHD	kA	MHD
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
ještě	ještě	k9	ještě
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
linkového	linkový	k2eAgNnSc2d1	linkové
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
krajský	krajský	k2eAgMnSc1d1	krajský
radní	radní	k1gMnSc1	radní
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
Antonín	Antonín	k1gMnSc1	Antonín
Krák	krák	k0	krák
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
vrátit	vrátit	k5eAaPmF	vrátit
a	a	k8xC	a
že	že	k8xS	že
pro	pro	k7c4	pro
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zavedení	zavedení	k1gNnSc1	zavedení
IDS	IDS	kA	IDS
napříč	napříč	k7c7	napříč
celým	celý	k2eAgInSc7d1	celý
krajem	kraj	k1gInSc7	kraj
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
kvůli	kvůli	k7c3	kvůli
řídkosti	řídkost	k1gFnSc3	řídkost
linek	linka	k1gFnPc2	linka
otázkou	otázka	k1gFnSc7	otázka
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
IDS	IDS	kA	IDS
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
člen	člen	k1gMnSc1	člen
nového	nový	k2eAgInSc2d1	nový
dopravního	dopravní	k2eAgInSc2d1	dopravní
výboru	výbor	k1gInSc2	výbor
kraje	kraj	k1gInSc2	kraj
David	David	k1gMnSc1	David
Lataj	Lataj	k1gMnSc1	Lataj
<g/>
.	.	kIx.	.
</s>
<s>
Hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
čipové	čipový	k2eAgFnSc2d1	čipová
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nahrávaly	nahrávat	k5eAaImAgInP	nahrávat
nejen	nejen	k6eAd1	nejen
jízdní	jízdní	k2eAgInPc1d1	jízdní
kupony	kupon	k1gInPc1	kupon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
Plzeňska	Plzeňsko	k1gNnSc2	Plzeňsko
IDP	IDP	kA	IDP
</s>
</p>
<p>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
záměr	záměr	k1gInSc1	záměr
založit	založit	k5eAaPmF	založit
organizaci	organizace	k1gFnSc4	organizace
POVED	povést	k5eAaPmDgInS	povést
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
(	(	kIx(	(
<g/>
Plzeňský	plzeňský	k2eAgMnSc1d1	plzeňský
organizátor	organizátor	k1gMnSc1	organizátor
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
)	)	kIx)	)
s	s	k7c7	s
třetinovým	třetinový	k2eAgInSc7d1	třetinový
podílem	podíl	k1gInSc7	podíl
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
IDOK	IDOK	kA	IDOK
</s>
</p>
<p>
<s>
EgroNet	EgroNet	k1gInSc1	EgroNet
<g/>
,	,	kIx,	,
přeshraniční	přeshraniční	k2eAgInSc1d1	přeshraniční
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
na	na	k7c6	na
území	území	k1gNnSc6	území
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
Saska	Sasko	k1gNnSc2	Sasko
a	a	k8xC	a
Duryňska	Duryňsko	k1gNnSc2	Duryňsko
</s>
</p>
<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
akceptaci	akceptace	k1gFnSc6	akceptace
čipové	čipový	k2eAgFnSc2d1	čipová
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
pilotního	pilotní	k2eAgInSc2d1	pilotní
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
od	od	k7c2	od
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
účastní	účastnit	k5eAaImIp3nS	účastnit
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
měst	město	k1gNnPc2	město
Mostu	most	k1gInSc2	most
a	a	k8xC	a
Litvínova	Litvínov	k1gInSc2	Litvínov
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
a	a	k8xC	a
ČSAD	ČSAD	kA	ČSAD
Slaný	Slaný	k1gInSc1	Slaný
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
10	[number]	k4	10
autobusových	autobusový	k2eAgFnPc6d1	autobusová
linkách	linka	k1gFnPc6	linka
BusLine	BusLin	k1gInSc5	BusLin
a.	a.	k?	a.
s.	s.	k?	s.
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
oblasti	oblast	k1gFnSc3	oblast
Lounsko-západ	Lounskoápad	k1gInSc1	Lounsko-západ
pilotně	pilotně	k6eAd1	pilotně
zaveden	zavést	k5eAaPmNgInS	zavést
IDS	IDS	kA	IDS
zahrnující	zahrnující	k2eAgMnSc1d1	zahrnující
i	i	k8xC	i
zónově-relační	zónověelační	k2eAgInSc1d1	zónově-relační
tarif	tarif	k1gInSc1	tarif
<g/>
,	,	kIx,	,
specifické	specifický	k2eAgNnSc1d1	specifické
číslování	číslování	k1gNnSc1	číslování
linek	linka	k1gFnPc2	linka
trojcifernými	trojciferný	k2eAgNnPc7d1	trojciferné
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc4d1	nový
standardy	standard	k1gInPc4	standard
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
na	na	k7c6	na
vozidlech	vozidlo	k1gNnPc6	vozidlo
i	i	k8xC	i
označnících	označník	k1gInPc6	označník
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
systém	systém	k1gInSc4	systém
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Doprava	doprava	k1gFnSc1	doprava
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
být	být	k5eAaImF	být
zahrnuta	zahrnout	k5eAaPmNgFnS	zahrnout
i	i	k9	i
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
IDOL	idol	k1gInSc1	idol
</s>
</p>
<p>
<s>
Jablonecký	jablonecký	k2eAgInSc1d1	jablonecký
regionální	regionální	k2eAgInSc1d1	regionální
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
JARIS	JARIS	kA	JARIS
</s>
</p>
<p>
<s>
Východočeský	východočeský	k2eAgInSc1d1	východočeský
dopravní	dopravní	k2eAgInSc1d1	dopravní
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
systém	systém	k1gInSc1	systém
VYDIS	VYDIS	kA	VYDIS
</s>
</p>
<p>
<s>
Integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
regionální	regionální	k2eAgFnSc1d1	regionální
doprava	doprava	k1gFnSc1	doprava
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
a	a	k8xC	a
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
IREDO	IREDO	kA	IREDO
</s>
</p>
<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
IDS	IDS	kA	IDS
Pk	Pk	k1gMnSc2	Pk
(	(	kIx(	(
<g/>
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
nahrazen	nahrazen	k2eAgMnSc1d1	nahrazen
IREDO	IREDO	kA	IREDO
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
zavedení	zavedení	k1gNnSc2	zavedení
IDS	IDS	kA	IDS
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
a	a	k8xC	a
sociálně-demokratické	sociálněemokratický	k2eAgNnSc1d1	sociálně-demokratické
vedení	vedení	k1gNnSc1	vedení
se	se	k3xPyFc4	se
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
koncepci	koncepce	k1gFnSc4	koncepce
dopravní	dopravní	k2eAgFnSc2d1	dopravní
obslužnosti	obslužnost	k1gFnSc2	obslužnost
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
zavést	zavést	k5eAaPmF	zavést
integrovaný	integrovaný	k2eAgInSc4d1	integrovaný
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
zastupitelstvi	zastupitelstev	k1gFnSc3	zastupitelstev
připravený	připravený	k2eAgInSc4d1	připravený
návrh	návrh	k1gInSc4	návrh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
jako	jako	k9	jako
příliš	příliš	k6eAd1	příliš
drahý	drahý	k2eAgInSc1d1	drahý
a	a	k8xC	a
problematický	problematický	k2eAgInSc1d1	problematický
a	a	k8xC	a
protože	protože	k8xS	protože
stávající	stávající	k2eAgFnSc1d1	stávající
dopravní	dopravní	k2eAgFnSc1d1	dopravní
obsluha	obsluha	k1gFnSc1	obsluha
kraje	kraj	k1gInSc2	kraj
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
analýze	analýza	k1gFnSc6	analýza
zhodnocena	zhodnocen	k2eAgFnSc1d1	zhodnocena
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
dobrá	dobrý	k2eAgFnSc1d1	dobrá
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vedení	vedení	k1gNnSc1	vedení
kraje	kraj	k1gInSc2	kraj
zdůvodňovalo	zdůvodňovat	k5eAaImAgNnS	zdůvodňovat
nepotřebnost	nepotřebnost	k1gFnSc4	nepotřebnost
IDS	IDS	kA	IDS
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
nejsou	být	k5eNaImIp3nP	být
velká	velký	k2eAgNnPc1d1	velké
městská	městský	k2eAgNnPc1d1	Městské
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
řídká	řídký	k2eAgFnSc1d1	řídká
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
IDS	IDS	kA	IDS
by	by	kYmCp3nS	by
nepřineslo	přinést	k5eNaPmAgNnS	přinést
výraznější	výrazný	k2eAgFnPc4d2	výraznější
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připustili	připustit	k5eAaPmAgMnP	připustit
možnost	možnost	k1gFnSc4	možnost
lokální	lokální	k2eAgFnSc2d1	lokální
integrace	integrace	k1gFnSc2	integrace
například	například	k6eAd1	například
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Jihlavy	Jihlava	k1gFnSc2	Jihlava
</s>
</p>
<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
</s>
</p>
<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
IDSOK	IDSOK	kA	IDSOK
</s>
</p>
<p>
<s>
Zlínská	zlínský	k2eAgFnSc1d1	zlínská
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
ZID	ZID	kA	ZID
</s>
</p>
<p>
<s>
KORIS	KORIS	kA	KORIS
<g/>
,	,	kIx,	,
Komplexní	komplexní	k2eAgFnSc1d1	komplexní
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgInSc1d1	řídící
a	a	k8xC	a
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
<g/>
,	,	kIx,	,
organizátor	organizátor	k1gMnSc1	organizátor
Koordinátor	koordinátor	k1gMnSc1	koordinátor
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
(	(	kIx(	(
<g/>
KOVED	KOVED	kA	KOVED
<g/>
)	)	kIx)	)
Plánované	plánovaný	k2eAgNnSc1d1	plánované
spuštění	spuštění	k1gNnSc1	spuštění
15.12	[number]	k4	15.12
<g/>
.2019	.2019	k4	.2019
</s>
</p>
<p>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
ODISVětšina	ODISVětšina	k1gFnSc1	ODISVětšina
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
dopravních	dopravní	k2eAgInPc2d1	dopravní
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společností	společnost	k1gFnPc2	společnost
či	či	k8xC	či
příspěvkových	příspěvkový	k2eAgFnPc2d1	příspěvková
organizací	organizace	k1gFnPc2	organizace
zřízených	zřízený	k2eAgFnPc2d1	zřízená
vesměs	vesměs	k6eAd1	vesměs
kraji	kraj	k1gInPc7	kraj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
sdružena	sdružit	k5eAaPmNgFnS	sdružit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
asociaci	asociace	k1gFnSc6	asociace
organizátorů	organizátor	k1gInPc2	organizátor
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
kraje	kraj	k1gInPc1	kraj
organizují	organizovat	k5eAaBmIp3nP	organizovat
dopravu	doprava	k1gFnSc4	doprava
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
asociaci	asociace	k1gFnSc6	asociace
zastoupení	zastoupení	k1gNnSc2	zastoupení
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
pro	pro	k7c4	pro
dopravní	dopravní	k2eAgFnSc4d1	dopravní
telematiku	telematika	k1gFnSc4	telematika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
zpracovaného	zpracovaný	k2eAgInSc2d1	zpracovaný
pro	pro	k7c4	pro
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
ČR	ČR	kA	ČR
stalo	stát	k5eAaPmAgNnS	stát
garantem	garant	k1gMnSc7	garant
tzv.	tzv.	kA	tzv.
národního	národní	k2eAgInSc2d1	národní
dopravního	dopravní	k2eAgInSc2d1	dopravní
standardu	standard	k1gInSc2	standard
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
a	a	k8xC	a
respektoval	respektovat	k5eAaImAgInS	respektovat
již	již	k6eAd1	již
existující	existující	k2eAgInPc4d1	existující
systémy	systém	k1gInPc4	systém
platebně	platebně	k6eAd1	platebně
identifikačních	identifikační	k2eAgInPc2d1	identifikační
systémů	systém	k1gInPc2	systém
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc6	vytvoření
jednotné	jednotný	k2eAgFnSc2d1	jednotná
platformy	platforma	k1gFnSc2	platforma
celoplošných	celoplošný	k2eAgInPc2d1	celoplošný
nebo	nebo	k8xC	nebo
nadregionálních	nadregionální	k2eAgInPc2d1	nadregionální
jízdenkových	jízdenkový	k2eAgInPc2d1	jízdenkový
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zpracovatele	zpracovatel	k1gMnSc2	zpracovatel
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
ČR	ČR	kA	ČR
poptávána	poptávat	k5eAaImNgFnS	poptávat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotný	jednotný	k2eAgInSc1d1	jednotný
standard	standard	k1gInSc1	standard
přináší	přinášet	k5eAaImIp3nS	přinášet
potřebu	potřeba	k1gFnSc4	potřeba
centrálního	centrální	k2eAgInSc2d1	centrální
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musí	muset	k5eAaImIp3nS	muset
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
evidenci	evidence	k1gFnSc4	evidence
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
akceptačních	akceptační	k2eAgNnPc2d1	akceptační
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
klíčové	klíčový	k2eAgNnSc4d1	klíčové
hospodářství	hospodářství	k1gNnSc4	hospodářství
a	a	k8xC	a
evidenci	evidence	k1gFnSc4	evidence
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
iniciativou	iniciativa	k1gFnSc7	iniciativa
zdola	zdola	k6eAd1	zdola
objevují	objevovat	k5eAaImIp3nP	objevovat
regionální	regionální	k2eAgNnSc4d1	regionální
řešení	řešení	k1gNnSc4	řešení
interoperability	interoperabilita	k1gFnSc2	interoperabilita
několika	několik	k4yIc2	několik
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
systémů	systém	k1gInPc2	systém
pro	pro	k7c4	pro
platbu	platba	k1gFnSc4	platba
a	a	k8xC	a
odbavení	odbavení	k1gNnSc4	odbavení
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
ČR	ČR	kA	ČR
stále	stále	k6eAd1	stále
neexistue	existuat	k5eNaPmIp3nS	existuat
celoplošný	celoplošný	k2eAgInSc1d1	celoplošný
standard	standard	k1gInSc1	standard
a	a	k8xC	a
strategická	strategický	k2eAgFnSc1d1	strategická
koncepce	koncepce	k1gFnSc1	koncepce
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
i	i	k9	i
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
Národní	národní	k2eAgFnSc6d1	národní
dopravní	dopravní	k2eAgFnSc6d1	dopravní
kartě	karta	k1gFnSc6	karta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
efektivní	efektivní	k2eAgFnSc4d1	efektivní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Pavel	Pavel	k1gMnSc1	Pavel
Adámek	Adámek	k1gMnSc1	Adámek
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Drábek	Drábek	k1gMnSc1	Drábek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
publikovalo	publikovat	k5eAaBmAgNnS	publikovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
vizi	vize	k1gFnSc4	vize
prioritních	prioritní	k2eAgInPc2d1	prioritní
směrů	směr	k1gInPc2	směr
dopravní	dopravní	k2eAgFnSc2d1	dopravní
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jako	jako	k8xS	jako
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
z	z	k7c2	z
8	[number]	k4	8
bodů	bod	k1gInPc2	bod
uvedlo	uvést	k5eAaPmAgNnS	uvést
vytvořit	vytvořit	k5eAaPmF	vytvořit
celostátní	celostátní	k2eAgInSc4d1	celostátní
integrovaný	integrovaný	k2eAgInSc4d1	integrovaný
systém	systém	k1gInSc4	systém
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
dopravní	dopravní	k2eAgFnSc6d1	dopravní
politice	politika	k1gFnSc6	politika
ani	ani	k8xC	ani
ve	v	k7c6	v
snahách	snaha	k1gFnPc6	snaha
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
krajů	kraj	k1gInPc2	kraj
se	se	k3xPyFc4	se
však	však	k9	však
podobná	podobný	k2eAgFnSc1d1	podobná
snaha	snaha	k1gFnSc1	snaha
výrazněji	výrazně	k6eAd2	výrazně
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Integrované	integrovaný	k2eAgInPc1d1	integrovaný
dopravní	dopravní	k2eAgInPc1d1	dopravní
systémy	systém	k1gInPc1	systém
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
==	==	k?	==
</s>
</p>
<p>
<s>
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
BID	BID	kA	BID
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žilinský	žilinský	k2eAgInSc1d1	žilinský
regionální	regionální	k2eAgInSc1d1	regionální
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
ŽRIDS	ŽRIDS	kA	ŽRIDS
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
dopravních	dopravní	k2eAgInPc2d1	dopravní
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
dopravní	dopravní	k2eAgInSc4d1	dopravní
svaz	svaz	k1gInSc4	svaz
(	(	kIx(	(
<g/>
Verkehrsverbund	Verkehrsverbund	k1gInSc4	Verkehrsverbund
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Tarifní	tarifní	k2eAgNnPc1d1	tarifní
pásma	pásmo	k1gNnPc1	pásmo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
aglomeracích	aglomerace	k1gFnPc6	aglomerace
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
zóny	zóna	k1gFnSc2	zóna
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pojem	pojem	k1gInSc1	pojem
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
okruh	okruh	k1gInSc1	okruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
někde	někde	k6eAd1	někde
je	být	k5eAaImIp3nS	být
dělení	dělení	k1gNnSc1	dělení
provedeno	provést	k5eAaPmNgNnS	provést
do	do	k7c2	do
šestiúhelníků	šestiúhelník	k1gInPc2	šestiúhelník
připomínajících	připomínající	k2eAgInPc2d1	připomínající
včelí	včelí	k2eAgNnSc4d1	včelí
plástve	plástev	k1gFnPc4	plástev
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Wabe	Wabe	k1gNnSc1	Wabe
(	(	kIx(	(
<g/>
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
Waben	Wabna	k1gFnPc2	Wabna
<g/>
)	)	kIx)	)
–	–	k?	–
např.	např.	kA	např.
Mannheim	Mannheim	k1gInSc1	Mannheim
nebo	nebo	k8xC	nebo
Würzburg	Würzburg	k1gInSc1	Würzburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Integrované	integrovaný	k2eAgInPc1d1	integrovaný
dopravní	dopravní	k2eAgInPc1d1	dopravní
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Drápal	Drápal	k1gMnSc1	Drápal
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Jareš	Jareš	k1gMnSc1	Jareš
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
asociace	asociace	k1gFnSc1	asociace
organizátorů	organizátor	k1gMnPc2	organizátor
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Drdla	Drdla	k1gMnSc1	Drdla
<g/>
:	:	kIx,	:
IDS	IDS	kA	IDS
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
–	–	k?	–
srovnání	srovnání	k1gNnPc4	srovnání
a	a	k8xC	a
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
<g/>
,	,	kIx,	,
Perner	Perner	k1gInSc1	Perner
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Contacts	Contacts	k1gInSc1	Contacts
<g/>
,	,	kIx,	,
ročník	ročník	k1gInSc1	ročník
3	[number]	k4	3
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
5	[number]	k4	5
<g/>
,	,	kIx,	,
prosinec	prosinec	k1gInSc1	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
69	[number]	k4	69
</s>
</p>
<p>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
V.	V.	kA	V.
Zapojení	zapojení	k1gNnSc2	zapojení
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
do	do	k7c2	do
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
dopravních	dopravní	k2eAgInPc2d1	dopravní
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
Vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
modul	modul	k1gInSc1	modul
CDV4	CDV4	k1gFnSc2	CDV4
–	–	k?	–
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
garant	garant	k1gMnSc1	garant
<g/>
:	:	kIx,	:
doc.	doc.	kA	doc.
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Modul	modul	k1gInSc1	modul
projektu	projekt	k1gInSc2	projekt
Zvýšení	zvýšení	k1gNnSc4	zvýšení
vědeckovýzkumného	vědeckovýzkumný	k2eAgInSc2d1	vědeckovýzkumný
potenciálu	potenciál	k1gInSc2	potenciál
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
studentů	student	k1gMnPc2	student
technických	technický	k2eAgFnPc2d1	technická
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
nových	nový	k2eAgFnPc2d1	nová
dopravních	dopravní	k2eAgFnPc2d1	dopravní
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
reg.	reg.	k?	reg.
č.	č.	k?	č.
CZ	CZ	kA	CZ
<g/>
.1	.1	k4	.1
<g/>
.07	.07	k4	.07
<g/>
/	/	kIx~	/
<g/>
2.3	[number]	k4	2.3
<g/>
.00	.00	k4	.00
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9.015	[number]	k4	9.015
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Žadatelem	žadatel	k1gMnSc7	žadatel
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
VŠB-Technická	VŠB-Technický	k2eAgFnSc1d1	VŠB-Technická
univerzita	univerzita	k1gFnSc1	univerzita
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
strojní	strojní	k2eAgFnSc1d1	strojní
<g/>
,	,	kIx,	,
Institut	institut	k1gInSc1	institut
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Fakultou	fakulta	k1gFnSc7	fakulta
stavební	stavební	k2eAgFnSc2d1	stavební
a	a	k8xC	a
Centrem	centr	k1gInSc7	centr
Nanotechnologií	nanotechnologie	k1gFnPc2	nanotechnologie
(	(	kIx(	(
<g/>
VŠB-Technická	VŠB-Technický	k2eAgFnSc1d1	VŠB-Technická
univerzita	univerzita	k1gFnSc1	univerzita
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
partnerem	partner	k1gMnSc7	partner
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgInSc1d1	národní
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
klastr	klastr	k1gInSc1	klastr
<g/>
,	,	kIx,	,
o.s.	o.s.	k?	o.s.
(	(	kIx(	(
<g/>
NSK	NSK	kA	NSK
<g/>
,	,	kIx,	,
o.s.	o.s.	k?	o.s.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
dopravního	dopravní	k2eAgInSc2d1	dopravní
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
v.v.i.	v.v.i.	k?	v.v.i.
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
CDV	CDV	kA	CDV
<g/>
,	,	kIx,	,
v.v.i.	v.v.i.	k?	v.v.i.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hercik	Hercika	k1gFnPc2	Hercika
<g/>
:	:	kIx,	:
Dopravní	dopravní	k2eAgInPc1d1	dopravní
systémy	systém	k1gInPc1	systém
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
geografie	geografie	k1gFnSc1	geografie
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
2008	[number]	k4	2008
</s>
</p>
