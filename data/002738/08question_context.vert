<s>
Odra	Odra	k1gFnSc1	Odra
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Oder	Odra	k1gFnPc2	Odra
<g/>
;	;	kIx,	;
slezsky	slezsky	k6eAd1	slezsky
Uodra	Uodr	k1gMnSc2	Uodr
<g/>
;	;	kIx,	;
polsky	polsky	k6eAd1	polsky
Odra	Odra	k1gFnSc1	Odra
<g/>
;	;	kIx,	;
dolnolužickosrbsky	dolnolužickosrbsky	k6eAd1	dolnolužickosrbsky
Wodra	Wodr	k1gMnSc2	Wodr
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
Viadua	Viaduum	k1gNnSc2	Viaduum
<g/>
,	,	kIx,	,
Viadrus	Viadrus	k1gInSc1	Viadrus
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
Odera	Odero	k1gNnSc2	Odero
<g/>
,	,	kIx,	,
Oddera	Oddero	k1gNnSc2	Oddero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
přes	přes	k7c4	přes
západní	západní	k2eAgNnSc4d1	západní
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
severní	severní	k2eAgFnSc1d1	severní
187	[number]	k4	187
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>

