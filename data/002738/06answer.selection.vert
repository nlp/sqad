<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
přes	přes	k7c4	přes
západní	západní	k2eAgNnSc4d1	západní
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
severní	severní	k2eAgFnSc1d1	severní
187	[number]	k4	187
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
