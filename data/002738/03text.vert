<s>
Odra	Odra	k1gFnSc1	Odra
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Oder	Odra	k1gFnPc2	Odra
<g/>
;	;	kIx,	;
slezsky	slezsky	k6eAd1	slezsky
Uodra	Uodr	k1gMnSc2	Uodr
<g/>
;	;	kIx,	;
polsky	polsky	k6eAd1	polsky
Odra	Odra	k1gFnSc1	Odra
<g/>
;	;	kIx,	;
dolnolužickosrbsky	dolnolužickosrbsky	k6eAd1	dolnolužickosrbsky
Wodra	Wodr	k1gMnSc2	Wodr
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
Viadua	Viaduum	k1gNnSc2	Viaduum
<g/>
,	,	kIx,	,
Viadrus	Viadrus	k1gInSc1	Viadrus
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
Odera	Odero	k1gNnSc2	Odero
<g/>
,	,	kIx,	,
Oddera	Oddero	k1gNnSc2	Oddero
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
přes	přes	k7c4	přes
západní	západní	k2eAgNnSc4d1	západní
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
severní	severní	k2eAgFnSc1d1	severní
187	[number]	k4	187
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
ramenech	rameno	k1gNnPc6	rameno
(	(	kIx(	(
<g/>
Dziwna	Dziwna	k1gFnSc1	Dziwna
<g/>
,	,	kIx,	,
Svina	Svina	k1gFnSc1	Svina
a	a	k8xC	a
Peene	Peen	k1gMnSc5	Peen
<g/>
)	)	kIx)	)
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
ve	v	k7c6	v
Štětínské	štětínský	k2eAgFnSc6d1	Štětínská
deltě	delta	k1gFnSc6	delta
severně	severně	k6eAd1	severně
od	od	k7c2	od
polského	polský	k2eAgInSc2d1	polský
Štětína	Štětín	k1gInSc2	Štětín
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
univerzální	univerzální	k2eAgFnSc2d1	univerzální
klasifikace	klasifikace	k1gFnSc2	klasifikace
se	se	k3xPyFc4	se
Štětínský	štětínský	k2eAgInSc1d1	štětínský
záliv	záliv	k1gInSc1	záliv
už	už	k6eAd1	už
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
řeky	řeka	k1gFnSc2	řeka
Odry	Odra	k1gFnSc2	Odra
nýbrž	nýbrž	k8xC	nýbrž
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Dziwna	Dziwno	k1gNnSc2	Dziwno
<g/>
,	,	kIx,	,
Svina	Svino	k1gNnSc2	Svino
a	a	k8xC	a
Peene	Peen	k1gInSc5	Peen
jsou	být	k5eAaImIp3nP	být
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
jako	jako	k8xS	jako
mořské	mořský	k2eAgInPc1d1	mořský
průlivy	průliv	k1gInPc1	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
za	za	k7c4	za
ústí	ústí	k1gNnSc4	ústí
řeky	řeka	k1gFnSc2	řeka
Odry	Odra	k1gFnSc2	Odra
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
místo	místo	k7c2	místo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Odra	Odra	k1gFnSc1	Odra
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
nejjižnější	jižní	k2eAgFnSc2d3	nejjižnější
části	část	k1gFnSc2	část
Štětínského	štětínský	k2eAgInSc2d1	štětínský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Odřanské	Odřanský	k2eAgFnSc2d1	Odřanský
roztoky	roztoka	k1gFnSc2	roztoka
(	(	kIx(	(
<g/>
Roztoka	roztoka	k1gFnSc1	roztoka
Odrzańska	Odrzańska	k1gFnSc1	Odrzańska
<g/>
)	)	kIx)	)
u	u	k7c2	u
města	město	k1gNnSc2	město
Police	police	k1gFnSc2	police
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
854	[number]	k4	854
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
112	[number]	k4	112
km	km	kA	km
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
742	[number]	k4	742
km	km	kA	km
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
187	[number]	k4	187
km	km	kA	km
jako	jako	k8xC	jako
hraniční	hraniční	k2eAgFnSc1d1	hraniční
řeka	řeka	k1gFnSc1	řeka
mezi	mezi	k7c7	mezi
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Odvádí	odvádět	k5eAaImIp3nS	odvádět
vodu	voda	k1gFnSc4	voda
ze	z	k7c2	z
118	[number]	k4	118
861	[number]	k4	861
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
106	[number]	k4	106
056	[number]	k4	056
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
89	[number]	k4	89
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7217	[number]	k4	7217
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
5587	[number]	k4	5587
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
pod	pod	k7c7	pod
Fidlovým	Fidlův	k2eAgInSc7d1	Fidlův
kopcem	kopec	k1gInSc7	kopec
v	v	k7c6	v
Oderských	oderský	k2eAgInPc6d1	oderský
vrších	vrch	k1gInPc6	vrch
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
protéká	protékat	k5eAaImIp3nS	protékat
českou	český	k2eAgFnSc7d1	Česká
částí	část	k1gFnSc7	část
Slezska	Slezsko	k1gNnSc2	Slezsko
(	(	kIx(	(
<g/>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c4	v
Polsku	Polska	k1gFnSc4	Polska
Slezským	slezský	k2eAgNnSc7d1	Slezské
vojvodstvím	vojvodství	k1gNnSc7	vojvodství
<g/>
,	,	kIx,	,
Opolským	opolský	k2eAgNnSc7d1	Opolské
vojvodstvím	vojvodství	k1gNnSc7	vojvodství
<g/>
,	,	kIx,	,
Dolnoslezským	dolnoslezský	k2eAgNnSc7d1	Dolnoslezské
vojvodstvím	vojvodství	k1gNnSc7	vojvodství
<g/>
,	,	kIx,	,
Lubušským	Lubušský	k2eAgNnSc7d1	Lubušský
vojvodstvím	vojvodství	k1gNnSc7	vojvodství
a	a	k8xC	a
Západopomořanským	Západopomořanský	k2eAgNnSc7d1	Západopomořanské
vojvodstvím	vojvodství	k1gNnSc7	vojvodství
<g/>
;	;	kIx,	;
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Braniborskem	Braniborsko	k1gNnSc7	Braniborsko
a	a	k8xC	a
Meklenburskem-Předním	Meklenburskem-Přední	k2eAgNnSc7d1	Meklenburskem-Přední
Pomořanskem	Pomořansko	k1gNnSc7	Pomořansko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
opustí	opustit	k5eAaPmIp3nP	opustit
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
terasovité	terasovitý	k2eAgFnSc6d1	terasovitá
dolině	dolina	k1gFnSc6	dolina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
z	z	k7c2	z
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
km	km	kA	km
na	na	k7c4	na
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dolina	dolina	k1gFnSc1	dolina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
díky	díky	k7c3	díky
odtoku	odtok	k1gInSc3	odtok
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
ledovců	ledovec	k1gInPc2	ledovec
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnPc1d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Lužické	lužický	k2eAgFnSc2d1	Lužická
Nisy	Nisa	k1gFnSc2	Nisa
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
šířky	šířka	k1gFnSc2	šířka
200	[number]	k4	200
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
mohutným	mohutný	k2eAgInSc7d1	mohutný
tokem	tok	k1gInSc7	tok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgInP	chránit
protipovodňovými	protipovodňový	k2eAgInPc7d1	protipovodňový
valy	val	k1gInPc7	val
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
84	[number]	k4	84
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
větve	větev	k1gFnPc1	větev
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
Štětínské	štětínský	k2eAgFnSc2d1	Štětínská
delty	delta	k1gFnSc2	delta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
,	,	kIx,	,
Štětínská	štětínský	k2eAgFnSc1d1	Štětínská
delta	delta	k1gFnSc1	delta
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrovy	ostrov	k1gInPc4	ostrov
Usedom	Usedom	k1gInSc1	Usedom
(	(	kIx(	(
<g/>
západně	západně	k6eAd1	západně
<g/>
)	)	kIx)	)
a	a	k8xC	a
Wolin	Wolin	k1gInSc4	Wolin
(	(	kIx(	(
<g/>
východně	východně	k6eAd1	východně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
ostrovy	ostrov	k1gInPc7	ostrov
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
úzký	úzký	k2eAgInSc1d1	úzký
kanál	kanál	k1gInSc1	kanál
(	(	kIx(	(
<g/>
Svina	Svina	k1gFnSc1	Svina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Pomořanského	pomořanský	k2eAgInSc2d1	pomořanský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
univerzální	univerzální	k2eAgFnSc2d1	univerzální
klasifikace	klasifikace	k1gFnSc2	klasifikace
se	se	k3xPyFc4	se
Štětínský	štětínský	k2eAgInSc1d1	štětínský
záliv	záliv	k1gInSc1	záliv
už	už	k6eAd1	už
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
řeky	řeka	k1gFnSc2	řeka
Odry	Odra	k1gFnSc2	Odra
nýbrž	nýbrž	k8xC	nýbrž
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Dziwna	Dziwno	k1gNnSc2	Dziwno
<g/>
,	,	kIx,	,
Svina	Svino	k1gNnSc2	Svino
a	a	k8xC	a
Peene	Peen	k1gInSc5	Peen
jsou	být	k5eAaImIp3nP	být
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
jako	jako	k8xS	jako
mořské	mořský	k2eAgInPc1d1	mořský
průlivy	průliv	k1gInPc1	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
za	za	k7c4	za
ústí	ústí	k1gNnSc4	ústí
řeky	řeka	k1gFnSc2	řeka
Odry	Odra	k1gFnSc2	Odra
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
místo	místo	k7c2	místo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Odra	Odra	k1gFnSc1	Odra
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
nejjižnější	jižní	k2eAgFnSc2d3	nejjižnější
části	část	k1gFnSc2	část
Štětínského	štětínský	k2eAgInSc2d1	štětínský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Odřanské	Odřanský	k2eAgFnSc2d1	Odřanský
roztoky	roztoka	k1gFnSc2	roztoka
(	(	kIx(	(
<g/>
Roztoka	roztoka	k1gFnSc1	roztoka
Odrzańska	Odrzańska	k1gFnSc1	Odrzańska
<g/>
)	)	kIx)	)
u	u	k7c2	u
města	město	k1gNnSc2	město
Police	police	k1gFnSc2	police
<g/>
.	.	kIx.	.
</s>
<s>
Odra	Odra	k1gFnSc1	Odra
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP	pramenit
v	v	k7c6	v
Oderských	oderský	k2eAgInPc6d1	oderský
vrších	vrch	k1gInPc6	vrch
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
přes	přes	k7c4	přes
západní	západní	k2eAgNnSc4d1	západní
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
187	[number]	k4	187
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
854	[number]	k4	854
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řeka	řeka	k1gFnSc1	řeka
(	(	kIx(	(
<g/>
před	před	k7c7	před
regulací	regulace	k1gFnSc7	regulace
toku	tok	k1gInSc2	tok
řeka	řeka	k1gFnSc1	řeka
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
přes	přes	k7c4	přes
1000	[number]	k4	1000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
112	[number]	k4	112
km	km	kA	km
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
742	[number]	k4	742
km	km	kA	km
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
je	být	k5eAaImIp3nS	být
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
-	-	kIx~	-
Odry	Odra	k1gFnSc2	Odra
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Bohumín	Bohumín	k1gInSc1	Bohumín
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
poslední	poslední	k2eAgNnSc1d1	poslední
město	město	k1gNnSc1	město
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ratiboř	Ratiboř	k1gFnSc1	Ratiboř
<g/>
,	,	kIx,	,
Kędzierzyn-Koźle	Kędzierzyn-Koźle	k1gFnSc5	Kędzierzyn-Koźle
<g/>
,	,	kIx,	,
Krapkowice	Krapkowice	k1gFnSc5	Krapkowice
<g/>
,	,	kIx,	,
Opole	Opole	k1gFnSc5	Opole
<g/>
,	,	kIx,	,
Brzeg	Brzeg	k1gMnSc1	Brzeg
<g/>
,	,	kIx,	,
Oława	Oława	k1gMnSc1	Oława
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Jelcz-Laskowice	Jelcz-Laskowice	k1gFnSc1	Jelcz-Laskowice
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
,	,	kIx,	,
Brzeg	Brzeg	k1gMnSc1	Brzeg
Dolny	Dolna	k1gFnSc2	Dolna
<g/>
,	,	kIx,	,
Ścinawa	Ścinawum	k1gNnSc2	Ścinawum
<g/>
,	,	kIx,	,
Szlichtyngowa	Szlichtyngowum	k1gNnSc2	Szlichtyngowum
<g/>
,	,	kIx,	,
Hlohov	Hlohov	k1gInSc1	Hlohov
<g/>
,	,	kIx,	,
Bytom	Bytom	k1gInSc1	Bytom
Odrzański	Odrzańsk	k1gFnSc2	Odrzańsk
<g/>
,	,	kIx,	,
Nowa	Now	k1gInSc2	Now
Sól	sólo	k1gNnPc2	sólo
<g/>
,	,	kIx,	,
Krosno	krosna	k1gFnSc5	krosna
Odrzańskie	Odrzańskie	k1gFnSc1	Odrzańskie
<g/>
,	,	kIx,	,
Eisenhüttenstadt	Eisenhüttenstadt	k1gInSc1	Eisenhüttenstadt
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
<g/>
,	,	kIx,	,
Słubice	Słubice	k1gFnSc1	Słubice
<g/>
,	,	kIx,	,
Kostrzyn	Kostrzyn	k1gInSc1	Kostrzyn
nad	nad	k7c4	nad
Odrą	Odrą	k1gFnSc4	Odrą
<g/>
,	,	kIx,	,
Cedynia	Cedynium	k1gNnPc4	Cedynium
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Oderberg	Oderberg	k1gMnSc1	Oderberg
<g/>
,	,	kIx,	,
Schwedt	Schwedt	k1gMnSc1	Schwedt
<g/>
,	,	kIx,	,
Vierraden	Vierraden	k2eAgMnSc1d1	Vierraden
<g/>
,	,	kIx,	,
Gartz	Gartz	k1gInSc1	Gartz
<g/>
,	,	kIx,	,
Gryfino	Gryfino	k1gNnSc1	Gryfino
<g/>
,	,	kIx,	,
Štětín	Štětín	k1gInSc1	Štětín
<g/>
,	,	kIx,	,
Police	police	k1gFnSc1	police
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
průlivu	průliv	k1gInSc2	průliv
Dziwna	Dziwna	k1gFnSc1	Dziwna
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
ostrovem	ostrov	k1gInSc7	ostrov
Wolin	Wolina	k1gFnPc2	Wolina
a	a	k8xC	a
polskou	polský	k2eAgFnSc7d1	polská
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
)	)	kIx)	)
-	-	kIx~	-
Wolin	Wolin	k1gMnSc1	Wolin
<g/>
,	,	kIx,	,
Kamień	Kamień	k1gMnSc1	Kamień
Pomorski	Pomorsk	k1gFnSc2	Pomorsk
<g/>
,	,	kIx,	,
Dziwnów	Dziwnów	k1gFnSc1	Dziwnów
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
průlivu	průliv	k1gInSc2	průliv
Svina	Svina	k1gFnSc1	Svina
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
ostrovy	ostrov	k1gInPc7	ostrov
<g />
.	.	kIx.	.
</s>
<s>
Wolin	Wolin	k1gInSc1	Wolin
a	a	k8xC	a
Usedom	Usedom	k1gInSc1	Usedom
<g/>
)	)	kIx)	)
-	-	kIx~	-
Svinoústí	Svinoúst	k1gFnPc2	Svinoúst
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
průlivu	průliv	k1gInSc2	průliv
Peena	Peena	k1gFnSc1	Peena
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
ostrovem	ostrov	k1gInSc7	ostrov
Usedom	Usedom	k1gInSc1	Usedom
a	a	k8xC	a
německou	německý	k2eAgFnSc7d1	německá
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
)	)	kIx)	)
-	-	kIx~	-
Usedom	Usedom	k1gInSc1	Usedom
<g/>
,	,	kIx,	,
Lassan	Lassan	k1gInSc1	Lassan
<g/>
,	,	kIx,	,
Wolgast	Wolgast	k1gFnSc1	Wolgast
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Štětínského	štětínský	k2eAgInSc2d1	štětínský
zálivu	záliv	k1gInSc2	záliv
-	-	kIx~	-
Nowe	Now	k1gInSc2	Now
Warpno	Warpen	k2eAgNnSc1d1	Warpen
<g/>
,	,	kIx,	,
Ueckermuende	Ueckermuend	k1gMnSc5	Ueckermuend
Celkově	celkově	k6eAd1	celkově
největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
Odry	Odra	k1gFnSc2	Odra
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Warta	Warta	k1gFnSc1	Warta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přitéká	přitékat	k5eAaImIp3nS	přitékat
zprava	zprava	k6eAd1	zprava
u	u	k7c2	u
Kostřína	Kostřín	k1gInSc2	Kostřín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
soutoku	soutok	k1gInSc2	soutok
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
Odra	Odra	k1gFnSc1	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Libavský	Libavský	k2eAgInSc1d1	Libavský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
121,7	[number]	k4	121,7
Plazský	Plazský	k2eAgInSc1d1	Plazský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
121,5	[number]	k4	121,5
Budišovka	Budišovka	k1gFnSc1	Budišovka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
104,0	[number]	k4	104,0
Něčínský	Něčínský	k2eAgInSc1d1	Něčínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
102,5	[number]	k4	102,5
Čermná	Čermný	k2eAgFnSc1d1	Čermná
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
97,0	[number]	k4	97,0
Vítovka	vítovka	k1gFnSc1	vítovka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
88,8	[number]	k4	88,8
Zlatý	zlatý	k2eAgInSc4d1	zlatý
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
87,8	[number]	k4	87,8
Stodolní	stodolní	k2eAgInSc1d1	stodolní
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
87,6	[number]	k4	87,6
Luha	Luhum	k1gNnSc2	Luhum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
78,7	[number]	k4	78,7
Jičínka	Jičínko	k1gNnSc2	Jičínko
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
63,3	[number]	k4	63,3
Husí	husí	k2eAgInSc1d1	husí
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
60,4	[number]	k4	60,4
Bartošovický	Bartošovický	k2eAgInSc1d1	Bartošovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
55,1	[number]	k4	55,1
Butovický	Butovický	k2eAgInSc1d1	Butovický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
51,6	[number]	k4	51,6
Sedlnice	Sedlnice	k1gFnSc2	Sedlnice
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
51,6	[number]	k4	51,6
Bílovka	Bílovka	k1gFnSc1	Bílovka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
40,7	[number]	k4	40,7
Lubina	Lubina	k1gMnSc1	Lubina
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
36,2	[number]	k4	36,2
Ondřejnice	Ondřejnice	k1gFnSc2	Ondřejnice
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
35,0	[number]	k4	35,0
Polančice	Polančice	k1gFnSc2	Polančice
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
30,8	[number]	k4	30,8
Porubka	Porubka	k1gFnSc1	Porubka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
23,6	[number]	k4	23,6
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
21,6	[number]	k4	21,6
Ostravice	Ostravice	k1gFnSc2	Ostravice
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
14,9	[number]	k4	14,9
Vrbická	Vrbická	k1gFnSc1	Vrbická
stružka	stružka	k1gFnSc1	stružka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
)	)	kIx)	)
10,4	[number]	k4	10,4
Bečva	Bečva	k1gFnSc1	Bečva
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
0,3	[number]	k4	0,3
(	(	kIx(	(
<g/>
ústí	ústit	k5eAaImIp3nS	ústit
již	již	k6eAd1	již
na	na	k7c6	na
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
Olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
0,0	[number]	k4	0,0
zprava	zprava	k6eAd1	zprava
-	-	kIx~	-
Ruda	Ruda	k1gMnSc1	Ruda
<g/>
,	,	kIx,	,
Bierawka	Bierawka	k1gMnSc1	Bierawka
<g/>
,	,	kIx,	,
Kłodnica	Kłodnica	k1gMnSc1	Kłodnica
<g/>
,	,	kIx,	,
Czarnka	Czarnka	k1gMnSc1	Czarnka
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Pěna	pěna	k1gFnSc1	pěna
(	(	kIx(	(
<g/>
Mała	Mał	k2eAgFnSc1d1	Mał
Panew	Panew	k1gFnSc1	Panew
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stobrawa	Stobrawa	k1gMnSc1	Stobrawa
<g/>
,	,	kIx,	,
Widawa	Widawa	k1gMnSc1	Widawa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Jezierzyca	Jezierzyca	k1gFnSc1	Jezierzyca
<g/>
,	,	kIx,	,
Barycz	Barycz	k1gInSc1	Barycz
<g/>
,	,	kIx,	,
Krzycki	Krzycki	k1gNnPc1	Krzycki
Rów	Rów	k1gFnSc1	Rów
<g/>
,	,	kIx,	,
Obrzyca	Obrzyca	k1gFnSc1	Obrzyca
<g/>
,	,	kIx,	,
Jabłonna	Jabłonna	k1gFnSc1	Jabłonna
<g/>
,	,	kIx,	,
Pliszka	Pliszka	k1gFnSc1	Pliszka
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
Ołobok	Ołobok	k1gInSc1	Ołobok
<g/>
,	,	kIx,	,
Gryzynka	Gryzynka	k1gFnSc1	Gryzynka
<g/>
,	,	kIx,	,
Warta	Warta	k1gFnSc1	Warta
(	(	kIx(	(
<g/>
s	s	k7c7	s
přítokem	přítok	k1gInSc7	přítok
Noteć	Noteć	k1gMnSc1	Noteć
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Myśla	Myśla	k1gMnSc1	Myśla
<g/>
,	,	kIx,	,
Kurzyca	Kurzyca	k1gMnSc1	Kurzyca
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
Stubia	Stubia	k1gFnSc1	Stubia
<g/>
,	,	kIx,	,
Rurzyca	Rurzyca	k1gFnSc1	Rurzyca
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Tywa	Tywum	k1gNnSc2	Tywum
<g/>
,	,	kIx,	,
Płonia	Płonium	k1gNnSc2	Płonium
<g/>
,	,	kIx,	,
Ina	Ina	k1gFnSc1	Ina
<g/>
,	,	kIx,	,
Gowienica	Gowienic	k2eAgFnSc1d1	Gowienic
zleva	zleva	k6eAd1	zleva
-	-	kIx~	-
Psina	psina	k1gFnSc1	psina
<g/>
,	,	kIx,	,
Cisek	Cisek	k1gInSc1	Cisek
<g/>
,	,	kIx,	,
Olszówka	Olszówko	k1gNnPc1	Olszówko
<g/>
,	,	kIx,	,
Stradunia	Stradunium	k1gNnPc1	Stradunium
<g/>
,	,	kIx,	,
Osoblaha	Osoblaha	k1gFnSc1	Osoblaha
<g/>
,	,	kIx,	,
Prószkowski	Prószkowski	k1gNnSc1	Prószkowski
Potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Kladská	kladský	k2eAgFnSc1d1	Kladská
Nisa	Nisa	k1gFnSc1	Nisa
<g/>
,	,	kIx,	,
Oława	Oława	k1gFnSc1	Oława
<g/>
,	,	kIx,	,
Ślęża	Ślęża	k1gFnSc1	Ślęża
<g/>
,	,	kIx,	,
Bystrzyca	Bystrzyca	k1gFnSc1	Bystrzyca
<g/>
,	,	kIx,	,
Średzka	Średzka	k1gFnSc1	Średzka
Woda	Woda	k1gFnSc1	Woda
<g/>
,	,	kIx,	,
Cicha	Cicha	k1gFnSc1	Cicha
<g />
.	.	kIx.	.
</s>
<s>
Woda	Woda	k1gMnSc1	Woda
<g/>
,	,	kIx,	,
Kaczawa	Kaczawa	k1gMnSc1	Kaczawa
<g/>
,	,	kIx,	,
Ślepca	Ślepca	k1gMnSc1	Ślepca
<g/>
,	,	kIx,	,
Zimnica	Zimnica	k1gMnSc1	Zimnica
<g/>
,	,	kIx,	,
Dębniak	Dębniak	k1gMnSc1	Dębniak
<g/>
,	,	kIx,	,
Biała	Biała	k1gMnSc1	Biała
Woda	Woda	k1gMnSc1	Woda
<g/>
,	,	kIx,	,
Czarna	Czarna	k1gFnSc1	Czarna
Struga	Struga	k1gFnSc1	Struga
<g/>
,	,	kIx,	,
Śląska	Śląska	k1gFnSc1	Śląska
Ochla	Ochla	k1gFnSc1	Ochla
<g/>
,	,	kIx,	,
Zimny	Zimny	k?	Zimny
Potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bobr	bobr	k1gMnSc1	bobr
<g/>
,	,	kIx,	,
Olcha	Olcha	k1gMnSc1	Olcha
<g/>
,	,	kIx,	,
Racza	Racza	k1gFnSc1	Racza
<g/>
,	,	kIx,	,
Lužická	lužický	k2eAgFnSc1d1	Lužická
Nisa	Nisa	k1gFnSc1	Nisa
Nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
průtoků	průtok	k1gInPc2	průtok
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
Odra	Odra	k1gFnSc1	Odra
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
především	především	k6eAd1	především
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
její	její	k3xOp3gFnSc2	její
hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokles	pokles	k1gInSc1	pokles
je	být	k5eAaImIp3nS	být
střídán	střídat	k5eAaImNgInS	střídat
občasnými	občasný	k2eAgInPc7d1	občasný
vzestupy	vzestup	k1gInPc7	vzestup
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
průtok	průtok	k1gInSc1	průtok
řeky	řeka	k1gFnSc2	řeka
začíná	začínat	k5eAaImIp3nS	začínat
souvisle	souvisle	k6eAd1	souvisle
stoupat	stoupat	k5eAaImF	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
zimách	zima	k1gFnPc6	zima
řeka	řeka	k1gFnSc1	řeka
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
Odry	Odra	k1gFnSc2	Odra
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Gozdowice	Gozdowice	k1gFnSc2	Gozdowice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
km	km	kA	km
pod	pod	k7c7	pod
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
Wartou	Warta	k1gFnSc7	Warta
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
547	[number]	k4	547
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Vzestupy	vzestup	k1gInPc1	vzestup
hladiny	hladina	k1gFnSc2	hladina
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
činí	činit	k5eAaImIp3nS	činit
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
m	m	kA	m
a	a	k8xC	a
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
m.	m.	k?	m.
K	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
povodním	povodeň	k1gFnPc3	povodeň
na	na	k7c6	na
Odře	Odra	k1gFnSc6	Odra
dle	dle	k7c2	dle
kronik	kronika	k1gFnPc2	kronika
města	město	k1gNnSc2	město
Vratislavi	Vratislav	k1gFnSc2	Vratislav
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1348	[number]	k4	1348
<g/>
,	,	kIx,	,
1464	[number]	k4	1464
<g/>
,	,	kIx,	,
1564	[number]	k4	1564
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgFnSc1d3	veliký
povodeň	povodeň	k1gFnSc1	povodeň
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
protékalo	protékat	k5eAaImAgNnS	protékat
Vratislaví	Vratislav	k1gFnSc7	Vratislav
3600	[number]	k4	3600
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Průměrné	průměrný	k2eAgInPc4d1	průměrný
měsíční	měsíční	k2eAgInPc4d1	měsíční
průtoky	průtok	k1gInPc4	průtok
Odry	Odra	k1gFnSc2	Odra
(	(	kIx(	(
<g/>
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Gozdowice	Gozdowice	k1gFnSc2	Gozdowice
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
období	období	k1gNnSc4	období
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Průměrný	průměrný	k2eAgInSc1d1	průměrný
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
průtok	průtok	k1gInSc1	průtok
Odry	Odra	k1gFnSc2	Odra
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opouští	opouštět	k5eAaImIp3nS	opouštět
území	území	k1gNnSc4	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
činí	činit	k5eAaImIp3nS	činit
49	[number]	k4	49
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Pod	pod	k7c7	pod
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Olší	olše	k1gFnSc7	olše
činí	činit	k5eAaImIp3nS	činit
průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
téměř	téměř	k6eAd1	téměř
63	[number]	k4	63
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Většina	většina	k1gFnSc1	většina
toků	tok	k1gInPc2	tok
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
povodí	povodí	k1gNnSc2	povodí
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
horské	horský	k2eAgFnSc2d1	horská
části	část	k1gFnSc2	část
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
Olše	olše	k1gFnSc2	olše
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
horské-sněhové	horskéněhový	k2eAgFnSc2d1	horské-sněhový
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Maxim	maxim	k1gInSc4	maxim
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInPc1d1	minimální
průtoky	průtok	k1gInPc1	průtok
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Horská	horský	k2eAgFnSc1d1	horská
část	část	k1gFnSc1	část
povodí	povodí	k1gNnSc2	povodí
Olše	olše	k1gFnSc2	olše
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
horské-sněhovodešťové	horskéněhovodešťový	k2eAgFnSc2d1	horské-sněhovodešťový
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
maximálními	maximální	k2eAgInPc7d1	maximální
průtoky	průtok	k1gInPc7	průtok
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgInPc1d1	průměrný
měsíční	měsíční	k2eAgInPc1d1	měsíční
průtoky	průtok	k1gInPc1	průtok
Odry	Odra	k1gFnSc2	Odra
(	(	kIx(	(
<g/>
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
Západní	západní	k2eAgNnSc1d1	západní
rameno	rameno	k1gNnSc1	rameno
delty	delta	k1gFnSc2	delta
je	být	k5eAaImIp3nS	být
využívané	využívaný	k2eAgNnSc1d1	využívané
pro	pro	k7c4	pro
vodní	vodní	k2eAgFnSc4d1	vodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
Opavy	Opava	k1gFnSc2	Opava
a	a	k8xC	a
pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
lodě	loď	k1gFnPc4	loď
k	k	k7c3	k
městu	město	k1gNnSc3	město
Koźle	Koźle	k1gFnSc2	Koźle
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
regulovaná	regulovaný	k2eAgFnSc1d1	regulovaná
jezy	jez	k1gInPc7	jez
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
plavebních	plavební	k2eAgInPc2d1	plavební
kanálů	kanál	k1gInPc2	kanál
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
řekami	řeka	k1gFnPc7	řeka
Havola	Havola	k1gFnSc1	Havola
<g/>
,	,	kIx,	,
Spréva	Spréva	k1gFnSc1	Spréva
<g/>
,	,	kIx,	,
Visla	Visla	k1gFnSc1	Visla
a	a	k8xC	a
Kłodnica	Kłodnica	k1gFnSc1	Kłodnica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Odry	Odra	k1gFnSc2	Odra
(	(	kIx(	(
<g/>
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Warty	Warta	k1gFnSc2	Warta
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
polský	polský	k2eAgInSc1d1	polský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osady	osada	k1gFnSc2	osada
Cedynia	Cedynium	k1gNnSc2	Cedynium
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Lubušské	Lubušský	k2eAgFnSc6d1	Lubušský
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Odry	Odra	k1gFnSc2	Odra
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
972	[number]	k4	972
k	k	k7c3	k
odražení	odražení	k1gNnSc3	odražení
první	první	k4xOgFnSc2	první
německé	německý	k2eAgFnSc2d1	německá
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
představovala	představovat	k5eAaImAgFnS	představovat
důležitou	důležitý	k2eAgFnSc7d1	důležitá
a	a	k8xC	a
silnou	silný	k2eAgFnSc7d1	silná
obranou	obrana	k1gFnSc7	obrana
linii	linie	k1gFnSc4	linie
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Viselsko-oderské	viselskoderský	k2eAgFnSc2d1	viselsko-oderský
operace	operace	k1gFnSc2	operace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
obsadila	obsadit	k5eAaPmAgFnS	obsadit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ukrajinským	ukrajinský	k2eAgInSc7d1	ukrajinský
frontem	front	k1gInSc7	front
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
1	[number]	k4	1
<g/>
.	.	kIx.	.
běloruský	běloruský	k2eAgInSc1d1	běloruský
front	front	k1gInSc1	front
obsadil	obsadit	k5eAaPmAgInS	obsadit
několik	několik	k4yIc4	několik
předmostí	předmostí	k1gNnPc2	předmostí
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kostřínské	Kostřínský	k2eAgNnSc1d1	Kostřínský
předmostí	předmostí	k1gNnSc1	předmostí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zahájení	zahájení	k1gNnSc4	zahájení
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Odře	Odra	k1gFnSc6	Odra
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
i	i	k9	i
pár	pár	k4xCyI	pár
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vlaků	vlak	k1gInPc2	vlak
kategorie	kategorie	k1gFnSc2	kategorie
expres	expres	k2eAgFnSc7d1	expres
společností	společnost	k1gFnSc7	společnost
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
ZSSK	ZSSK	kA	ZSSK
jezdící	jezdící	k2eAgMnSc1d1	jezdící
(	(	kIx(	(
<g/>
k	k	k7c3	k
r.	r.	kA	r.
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
-	-	kIx~	-
<g/>
)	)	kIx)	)
Bohumín	Bohumín	k1gInSc1	Bohumín
-	-	kIx~	-
Žilina	Žilina	k1gFnSc1	Žilina
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
