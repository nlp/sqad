<s>
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
švýcarské	švýcarský	k2eAgNnSc4d1	švýcarské
psí	psí	k2eAgNnSc4d1	psí
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc4d1	velký
až	až	k8xS	až
velký	velký	k2eAgInSc4d1	velký
<g/>
,	,	kIx,	,
dlouhosrstý	dlouhosrstý	k2eAgInSc4d1	dlouhosrstý
<g/>
,	,	kIx,	,
tříbarevný	tříbarevný	k2eAgInSc4d1	tříbarevný
<g/>
,	,	kIx,	,
harmonický	harmonický	k2eAgInSc4d1	harmonický
a	a	k8xC	a
vyrovnaný	vyrovnaný	k2eAgInSc4d1	vyrovnaný
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
hlídací	hlídací	k2eAgMnSc1d1	hlídací
<g/>
,	,	kIx,	,
honácký	honácký	k2eAgMnSc1d1	honácký
a	a	k8xC	a
tažný	tažný	k2eAgMnSc1d1	tažný
pes	pes	k1gMnSc1	pes
na	na	k7c6	na
selských	selský	k2eAgInPc6d1	selský
dvorech	dvůr	k1gInPc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Naháněl	nahánět	k5eAaImAgMnS	nahánět
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
hlídal	hlídat	k5eAaImAgMnS	hlídat
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
usedlosti	usedlost	k1gFnSc2	usedlost
a	a	k8xC	a
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
postroji	postroj	k1gInSc6	postroj
tahal	tahat	k5eAaImAgInS	tahat
malé	malý	k2eAgInPc4d1	malý
dvoukolové	dvoukolový	k2eAgInPc4d1	dvoukolový
káry	káry	k1gInPc4	káry
<g/>
.	.	kIx.	.
</s>
<s>
Bernský	bernský	k2eAgInSc1d1	bernský
salašnický	salašnický	k2eAgInSc1d1	salašnický
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
bernardýn	bernardýn	k1gMnSc1	bernardýn
umí	umět	k5eAaImIp3nS	umět
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
lidi	člověk	k1gMnPc4	člověk
ztracené	ztracený	k2eAgMnPc4d1	ztracený
ve	v	k7c6	v
sněhových	sněhový	k2eAgFnPc6d1	sněhová
závějích	závěj	k1gFnPc6	závěj
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
plemeno	plemeno	k1gNnSc1	plemeno
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgNnSc1d1	staré
a	a	k8xC	a
odjakživa	odjakživa	k6eAd1	odjakživa
využívané	využívaný	k2eAgFnPc1d1	využívaná
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
salaši	salaš	k1gFnSc6	salaš
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
k	k	k7c3	k
nahánění	nahánění	k1gNnSc3	nahánění
ovcí	ovce	k1gFnPc2	ovce
nebo	nebo	k8xC	nebo
tažení	tažení	k1gNnSc6	tažení
vozíčků	vozíček	k1gInPc2	vozíček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	on	k3xPp3gMnPc4	on
používali	používat	k5eAaImAgMnP	používat
třeba	třeba	k6eAd1	třeba
prodejci	prodejce	k1gMnPc1	prodejce
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dali	dát	k5eAaPmAgMnP	dát
mléko	mléko	k1gNnSc4	mléko
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
na	na	k7c4	na
vozíček	vozíček	k1gInSc4	vozíček
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vezl	vézt	k5eAaImAgMnS	vézt
pes	pes	k1gMnSc1	pes
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc4d1	podobný
bernskému	bernský	k2eAgMnSc3d1	bernský
salašnickému	salašnický	k2eAgMnSc3d1	salašnický
psu	pes	k1gMnSc3	pes
-	-	kIx~	-
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
kreseb	kresba	k1gFnPc2	kresba
neznámého	známý	k2eNgMnSc2d1	neznámý
člověka	člověk	k1gMnSc2	člověk
objevených	objevený	k2eAgFnPc2d1	objevená
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Oyonnax	Oyonnax	k1gInSc1	Oyonnax
v	v	k7c6	v
departmentu	department	k1gInSc6	department
Ain	Ain	k1gFnSc2	Ain
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
ale	ale	k9	ale
historie	historie	k1gFnSc1	historie
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
známá	známý	k2eAgFnSc1d1	známá
-	-	kIx~	-
ani	ani	k8xC	ani
přibližní	přibližný	k2eAgMnPc1d1	přibližný
předci	předek	k1gMnPc1	předek
nejsou	být	k5eNaImIp3nP	být
známí	známý	k2eAgMnPc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zemi	zem	k1gFnSc4	zem
jejich	jejich	k3xOp3gInSc2	jejich
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
jejich	jejich	k3xOp3gInSc1	jejich
první	první	k4xOgInSc1	první
standard	standard	k1gInSc1	standard
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byl	být	k5eAaImAgInS	být
uznán	uznán	k2eAgInSc1d1	uznán
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
-	-	kIx~	-
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
různé	různý	k2eAgInPc1d1	různý
názvy	název	k1gInPc1	název
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
ale	ale	k8xC	ale
i	i	k9	i
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
a	a	k8xC	a
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
založeno	založit	k5eAaPmNgNnS	založit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
chovatelských	chovatelský	k2eAgFnPc2d1	chovatelská
stanic	stanice	k1gFnPc2	stanice
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
výšky	výška	k1gFnSc2	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
podsadité	podsaditý	k2eAgNnSc1d1	podsadité
<g/>
,	,	kIx,	,
než	než	k8xS	než
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Plemeno	plemeno	k1gNnSc1	plemeno
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
rozeznatelné	rozeznatelný	k2eAgFnSc2d1	rozeznatelná
díky	díky	k7c3	díky
tříbarevným	tříbarevný	k2eAgInPc3d1	tříbarevný
znakům	znak	k1gInPc3	znak
(	(	kIx(	(
<g/>
černá	černá	k1gFnSc1	černá
<g/>
,	,	kIx,	,
hnědočervené	hnědočervený	k2eAgNnSc1d1	hnědočervené
pálení	pálení	k1gNnSc1	pálení
a	a	k8xC	a
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Líce	líc	k1gFnPc1	líc
<g/>
,	,	kIx,	,
punčochy	punčocha	k1gFnPc1	punčocha
a	a	k8xC	a
skvrna	skvrna	k1gFnSc1	skvrna
okolo	okolo	k7c2	okolo
oka	oko	k1gNnSc2	oko
má	mít	k5eAaImIp3nS	mít
hnědočerné	hnědočerný	k2eAgNnSc1d1	hnědočerné
pálení	pálení	k1gNnSc1	pálení
<g/>
.	.	kIx.	.
</s>
<s>
Hruď	hruď	k1gFnSc1	hruď
<g/>
,	,	kIx,	,
prsty	prst	k1gInPc1	prst
<g/>
,	,	kIx,	,
čumák	čumák	k1gInSc1	čumák
<g/>
,	,	kIx,	,
špička	špička	k1gFnSc1	špička
ocasu	ocas	k1gInSc2	ocas
a	a	k8xC	a
lysina	lysina	k1gFnSc1	lysina
mezi	mezi	k7c7	mezi
očima	oko	k1gNnPc7	oko
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pes	pes	k1gMnSc1	pes
sedí	sedit	k5eAaImIp3nS	sedit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejnápadnější	nápadní	k2eAgFnSc1d3	nápadní
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
bílé	bílý	k2eAgFnSc3d1	bílá
náprsence	náprsenka	k1gFnSc3	náprsenka
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
psů	pes	k1gMnPc2	pes
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jen	jen	k9	jen
nepatrně	patrně	k6eNd1	patrně
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgFnSc1d2	těžší
<g/>
,	,	kIx,	,
čumák	čumák	k1gInSc1	čumák
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
trojúhelníkové	trojúhelníkový	k2eAgFnSc2d1	trojúhelníková
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
zaoblené	zaoblený	k2eAgInPc1d1	zaoblený
<g/>
,	,	kIx,	,
vysoko	vysoko	k6eAd1	vysoko
nasazené	nasazený	k2eAgFnPc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
límec	límec	k1gInSc4	límec
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
také	také	k9	také
rovný	rovný	k2eAgMnSc1d1	rovný
a	a	k8xC	a
bohatě	bohatě	k6eAd1	bohatě
osrstěný	osrstěný	k2eAgInSc1d1	osrstěný
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
také	také	k9	také
bohatě	bohatě	k6eAd1	bohatě
osrstěný	osrstěný	k2eAgInSc1d1	osrstěný
<g/>
,	,	kIx,	,
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
svěšený	svěšený	k2eAgInSc1d1	svěšený
černý	černý	k2eAgInSc1d1	černý
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
vznášející	vznášející	k2eAgFnSc2d1	vznášející
se	se	k3xPyFc4	se
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nesený	nesený	k2eAgMnSc1d1	nesený
poněkud	poněkud	k6eAd1	poněkud
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
mají	mít	k5eAaImIp3nP	mít
výšku	výška	k1gFnSc4	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
64-70	[number]	k4	64-70
cm	cm	kA	cm
<g/>
,	,	kIx,	,
feny	fena	k1gFnSc2	fena
58-66	[number]	k4	58-66
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
váhu	váha	k1gFnSc4	váha
mezi	mezi	k7c7	mezi
32	[number]	k4	32
až	až	k9	až
50	[number]	k4	50
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
divoce	divoce	k6eAd1	divoce
vypadající	vypadající	k2eAgNnSc4d1	vypadající
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
krotké	krotký	k2eAgNnSc1d1	krotké
a	a	k8xC	a
miluje	milovat	k5eAaImIp3nS	milovat
své	svůj	k3xOyFgNnSc4	svůj
"	"	kIx"	"
<g/>
lidi	člověk	k1gMnPc4	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
přilne	přilnout	k5eAaPmIp3nS	přilnout
i	i	k9	i
k	k	k7c3	k
cizím	cizí	k2eAgNnPc3d1	cizí
a	a	k8xC	a
agresivita	agresivita	k1gFnSc1	agresivita
se	se	k3xPyFc4	se
u	u	k7c2	u
jedinců	jedinec	k1gMnPc2	jedinec
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
skoro	skoro	k6eAd1	skoro
vůbec	vůbec	k9	vůbec
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgMnSc1d1	dobrý
pes	pes	k1gMnSc1	pes
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
-	-	kIx~	-
miluje	milovat	k5eAaImIp3nS	milovat
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
hrátky	hrátky	k1gFnPc4	hrátky
mu	on	k3xPp3gMnSc3	on
nevadí	vadit	k5eNaImIp3nP	vadit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc1d1	vysoký
práh	práh	k1gInSc1	práh
bolesti	bolest	k1gFnPc1	bolest
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
něco	něco	k3yInSc1	něco
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
,	,	kIx,	,
prostě	prostě	k6eAd1	prostě
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
velmi	velmi	k6eAd1	velmi
rád	rád	k6eAd1	rád
i	i	k8xC	i
ostatní	ostatní	k2eAgNnPc1d1	ostatní
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
většinou	většinou	k6eAd1	většinou
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
a	a	k8xC	a
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
nevyhledává	vyhledávat	k5eNaImIp3nS	vyhledávat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nevyhledává	vyhledávat	k5eNaImIp3nS	vyhledávat
ani	ani	k9	ani
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
cizím	cizí	k2eAgMnPc3d1	cizí
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
nadmíru	nadmíru	k6eAd1	nadmíru
přátelsky	přátelsky	k6eAd1	přátelsky
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
dobrý	dobrý	k2eAgInSc1d1	dobrý
hlídač	hlídač	k1gInSc1	hlídač
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
rád	rád	k6eAd1	rád
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
koupání	koupání	k1gNnSc4	koupání
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
milý	milý	k2eAgInSc1d1	milý
<g/>
,	,	kIx,	,
laskavý	laskavý	k2eAgInSc1d1	laskavý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
přátelský	přátelský	k2eAgInSc1d1	přátelský
<g/>
.	.	kIx.	.
</s>
<s>
Samotu	samota	k1gFnSc4	samota
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
a	a	k8xC	a
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
nejraději	rád	k6eAd3	rád
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pánem	pán	k1gMnSc7	pán
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
a	a	k8xC	a
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nemá	mít	k5eNaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obzvláště	obzvláště	k6eAd1	obzvláště
náchylný	náchylný	k2eAgMnSc1d1	náchylný
k	k	k7c3	k
dysplazii	dysplazie	k1gFnSc3	dysplazie
kyčelních	kyčelní	k2eAgInPc2d1	kyčelní
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nP	trpět
také	také	k9	také
dědičnými	dědičný	k2eAgFnPc7d1	dědičná
očními	oční	k2eAgFnPc7d1	oční
chorobami	choroba	k1gFnPc7	choroba
a	a	k8xC	a
rakovinou	rakovina	k1gFnSc7	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
rodokmen	rodokmen	k1gInSc4	rodokmen
a	a	k8xC	a
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
předky	předek	k1gMnPc4	předek
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
netrpěli	trpět	k5eNaImAgMnP	trpět
právě	právě	k6eAd1	právě
potížemi	potíž	k1gFnPc7	potíž
s	s	k7c7	s
klouby	kloub	k1gInPc7	kloub
nebo	nebo	k8xC	nebo
potížemi	potíž	k1gFnPc7	potíž
s	s	k7c7	s
kardiovaskulárním	kardiovaskulární	k2eAgInSc7d1	kardiovaskulární
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
často	často	k6eAd1	často
zaviní	zavinit	k5eAaPmIp3nS	zavinit
nečekanou	čekaný	k2eNgFnSc4d1	nečekaná
smrt	smrt	k1gFnSc4	smrt
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
jen	jen	k9	jen
málo	málo	k4c1	málo
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
deseti	deset	k4xCc2	deset
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
h.	h.	k?	h.
c.	c.	k?	c.
Hans	Hans	k1gMnSc1	Hans
Räber	Räber	k1gMnSc1	Räber
<g/>
:	:	kIx,	:
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
doplnil	doplnit	k5eAaPmAgMnS	doplnit
MVDr.	MVDr.	kA	MVDr.
Jan	Jan	k1gMnSc1	Jan
Nesvadba	Nesvadba	k1gMnSc1	Nesvadba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dona	Don	k1gMnSc4	Don
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Prof.	prof.	kA	prof.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Bernd	Bernd	k1gMnSc1	Bernd
Günter	Günter	k1gMnSc1	Günter
<g/>
:	:	kIx,	:
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Timy	Timy	k1gInPc1	Timy
<g/>
,	,	kIx,	,
125	[number]	k4	125
stran	strana	k1gFnPc2	strana
Esther	Esthra	k1gFnPc2	Esthra
Verhoef-Verhallen	Verhoef-Verhallen	k2eAgInSc1d1	Verhoef-Verhallen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Rebo	Rebo	k1gMnSc1	Rebo
<g/>
,	,	kIx,	,
128	[number]	k4	128
stran	strana	k1gFnPc2	strana
Harper	Harpra	k1gFnPc2	Harpra
Louise	Louis	k1gMnSc2	Louis
<g/>
:	:	kIx,	:
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
,	,	kIx,	,
158	[number]	k4	158
stran	strana	k1gFnPc2	strana
Ludwig	Ludwiga	k1gFnPc2	Ludwiga
J.	J.	kA	J.
<g/>
,	,	kIx,	,
<g/>
Steimerová	Steimerový	k2eAgFnSc1d1	Steimerový
Chr	chr	k0	chr
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
<g/>
Švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
salašničtí	salašnický	k2eAgMnPc1d1	salašnický
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
Vašut	Vašut	k1gMnSc1	Vašut
<g/>
,	,	kIx,	,
64	[number]	k4	64
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Klub	klub	k1gInSc1	klub
švýcarských	švýcarský	k2eAgMnPc2d1	švýcarský
salašnických	salašnický	k2eAgMnPc2d1	salašnický
psů	pes	k1gMnPc2	pes
The	The	k1gMnSc1	The
Bernese	Bernese	k1gFnSc2	Bernese
Mountain	Mountain	k1gMnSc1	Mountain
dog	doga	k1gFnPc2	doga
Club	club	k1gInSc4	club
of	of	k?	of
America	Americ	k1gInSc2	Americ
</s>
