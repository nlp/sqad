<s>
Týden	týden	k1gInSc1	týden
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
den	den	k1gInSc4	den
a	a	k8xC	a
kratší	krátký	k2eAgNnSc4d2	kratší
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
juliánského	juliánský	k2eAgInSc2d1	juliánský
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc1	sedm
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
počítaných	počítaný	k2eAgMnPc2d1	počítaný
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
neděle	neděle	k1gFnSc2	neděle
<g/>
,	,	kIx,	,
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
,	,	kIx,	,
úterý	úterý	k1gNnSc4	úterý
<g/>
,	,	kIx,	,
středa	středa	k1gFnSc1	středa
<g/>
,	,	kIx,	,
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
sobota	sobota	k1gFnSc1	sobota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
vžilo	vžít	k5eAaPmAgNnS	vžít
počítání	počítání	k1gNnSc1	počítání
dnů	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
od	od	k7c2	od
pondělí	pondělí	k1gNnSc2	pondělí
jakožto	jakožto	k8xS	jakožto
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
pracovního	pracovní	k2eAgInSc2d1	pracovní
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Týdny	týden	k1gInPc1	týden
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
průběžné	průběžný	k2eAgFnPc1d1	průběžná
stále	stále	k6eAd1	stále
dál	daleko	k6eAd2	daleko
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
s	s	k7c7	s
roky	rok	k1gInPc7	rok
sladěné	sladěný	k2eAgFnSc2d1	sladěná
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
roky	rok	k1gInPc4	rok
nemají	mít	k5eNaImIp3nP	mít
stále	stále	k6eAd1	stále
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
také	také	k9	také
začátek	začátek	k1gInSc4	začátek
počítání	počítání	k1gNnSc2	počítání
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
sjednotilo	sjednotit	k5eAaPmAgNnS	sjednotit
číslování	číslování	k1gNnSc4	číslování
týdnů	týden	k1gInPc2	týden
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc1	první
týden	týden	k1gInSc1	týden
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
první	první	k4xOgInSc4	první
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
týdne	týden	k1gInSc2	týden
(	(	kIx(	(
<g/>
až	až	k9	až
3	[number]	k4	3
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
zpravidla	zpravidla	k6eAd1	zpravidla
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
má	mít	k5eAaImIp3nS	mít
číslo	číslo	k1gNnSc1	číslo
-	-	kIx~	-
buď	buď	k8xC	buď
konec	konec	k1gInSc4	konec
posledního	poslední	k2eAgInSc2d1	poslední
týdne	týden	k1gInSc2	týden
starého	starý	k2eAgInSc2d1	starý
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
novém	nový	k2eAgMnSc6d1	nový
<g/>
,	,	kIx,	,
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
nebo	nebo	k8xC	nebo
začátek	začátek	k1gInSc1	začátek
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
<g/>
,	,	kIx,	,
předchozím	předchozí	k2eAgNnSc6d1	předchozí
<g/>
,	,	kIx,	,
loňském	loňský	k2eAgNnSc6d1	loňské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
každý	každý	k3xTgInSc1	každý
sedmý	sedmý	k4xOgInSc1	sedmý
zlom	zlom	k1gInSc1	zlom
roků	rok	k1gInPc2	rok
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
společný	společný	k2eAgInSc4d1	společný
se	s	k7c7	s
zlomem	zlom	k1gInSc7	zlom
týdnů	týden	k1gInPc2	týden
<g/>
:	:	kIx,	:
pondělí	pondělí	k1gNnSc1	pondělí
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
1	[number]	k4	1
týden	týden	k1gInSc1	týden
=	=	kIx~	=
7	[number]	k4	7
dní	den	k1gInPc2	den
1	[number]	k4	1
týden	týden	k1gInSc4	týden
=	=	kIx~	=
168	[number]	k4	168
hodin	hodina	k1gFnPc2	hodina
=	=	kIx~	=
10080	[number]	k4	10080
minut	minuta	k1gFnPc2	minuta
=	=	kIx~	=
604800	[number]	k4	604800
sekund	sekunda	k1gFnPc2	sekunda
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
přechodu	přechod	k1gInSc2	přechod
na	na	k7c4	na
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
rok	rok	k1gInSc1	rok
=	=	kIx~	=
52	[number]	k4	52
týdnů	týden	k1gInPc2	týden
+	+	kIx~	+
1	[number]	k4	1
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
2	[number]	k4	2
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
rok	rok	k1gInSc4	rok
přestupný	přestupný	k2eAgInSc4d1	přestupný
<g/>
)	)	kIx)	)
1	[number]	k4	1
týden	týden	k1gInSc1	týden
=	=	kIx~	=
23	[number]	k4	23
%	%	kIx~	%
průměrného	průměrný	k2eAgInSc2d1	průměrný
měsíce	měsíc	k1gInSc2	měsíc
V	v	k7c6	v
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
kalendáři	kalendář	k1gInSc6	kalendář
má	mít	k5eAaImIp3nS	mít
rok	rok	k1gInSc1	rok
přesně	přesně	k6eAd1	přesně
365,242	[number]	k4	365,242
<g/>
5	[number]	k4	5
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
přesně	přesně	k6eAd1	přesně
52,177	[number]	k4	52,177
<g/>
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
juliánského	juliánský	k2eAgInSc2d1	juliánský
roku	rok	k1gInSc2	rok
s	s	k7c7	s
365,25	[number]	k4	365,25
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
množství	množství	k1gNnSc4	množství
týdnů	týden	k1gInPc2	týden
představované	představovaný	k2eAgInPc1d1	představovaný
konečnou	konečný	k2eAgFnSc7d1	konečná
desetinnou	desetinný	k2eAgFnSc7d1	desetinná
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
400	[number]	k4	400
gregoriánských	gregoriánský	k2eAgNnPc2d1	gregoriánské
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vejde	vejít	k5eAaPmIp3nS	vejít
20871	[number]	k4	20871
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
že	že	k8xS	že
např.	např.	kA	např.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1605	[number]	k4	1605
byla	být	k5eAaImAgFnS	být
neděle	neděle	k1gFnPc4	neděle
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
není	být	k5eNaImIp3nS	být
přímou	přímý	k2eAgFnSc7d1	přímá
součástí	součást	k1gFnSc7	součást
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
dny	den	k1gInPc1	den
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
stále	stále	k6eAd1	stále
dál	daleko	k6eAd2	daleko
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starého	starý	k2eAgInSc2d1	starý
judaismu	judaismus	k1gInSc2	judaismus
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
kulturách	kultura	k1gFnPc6	kultura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
sedmidenní	sedmidenní	k2eAgInSc4d1	sedmidenní
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
např.	např.	kA	např.
při	při	k7c6	při
gregoriánské	gregoriánský	k2eAgFnSc6d1	gregoriánská
reformě	reforma	k1gFnSc6	reforma
kalendáře	kalendář	k1gInSc2	kalendář
byl	být	k5eAaImAgInS	být
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1582	[number]	k4	1582
následován	následován	k2eAgInSc1d1	následován
pátkem	pátek	k1gInSc7	pátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1582	[number]	k4	1582
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
nedělních	nedělní	k2eAgNnPc2d1	nedělní
písmen	písmeno	k1gNnPc2	písmeno
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
dne	den	k1gInSc2	den
týdne	týden	k1gInSc2	týden
v	v	k7c6	v
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
nebo	nebo	k8xC	nebo
juliánském	juliánský	k2eAgInSc6d1	juliánský
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
o	o	k7c6	o
7	[number]	k4	7
dnech	den	k1gInPc6	den
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
užíván	užívat	k5eAaImNgMnS	užívat
v	v	k7c6	v
Babylóně	Babylón	k1gInSc6	Babylón
již	již	k6eAd1	již
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
římskou	římský	k2eAgFnSc7d1	římská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
nynější	nynější	k2eAgFnSc6d1	nynější
moderní	moderní	k2eAgFnSc6d1	moderní
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc4d1	běžný
sedmidenní	sedmidenní	k2eAgInSc4d1	sedmidenní
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
antropologové	antropolog	k1gMnPc1	antropolog
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
prehistorických	prehistorický	k2eAgFnPc6d1	prehistorická
společnostech	společnost	k1gFnPc6	společnost
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
týdny	týden	k1gInPc4	týden
různé	různý	k2eAgFnSc2d1	různá
délky	délka	k1gFnSc2	délka
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
deseti	deset	k4xCc2	deset
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpozorovali	zpozorovat	k5eAaPmAgMnP	zpozorovat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
pro	pro	k7c4	pro
týden	týden	k1gInSc4	týden
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
shodný	shodný	k2eAgMnSc1d1	shodný
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
"	"	kIx"	"
<g/>
tržního	tržní	k2eAgInSc2d1	tržní
dne	den	k1gInSc2	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
koncept	koncept	k1gInSc1	koncept
týdne	týden	k1gInSc2	týden
zřejmě	zřejmě	k6eAd1	zřejmě
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
nebo	nebo	k8xC	nebo
předzemědělských	předzemědělský	k2eAgFnPc2d1	předzemědělský
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
lidé	člověk	k1gMnPc1	člověk
měli	mít	k5eAaImAgMnP	mít
tržiště	tržiště	k1gNnSc4	tržiště
či	či	k8xC	či
tržní	tržní	k2eAgInPc4d1	tržní
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
rozptýleným	rozptýlený	k2eAgNnSc7d1	rozptýlené
osídlením	osídlení	k1gNnSc7	osídlení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obchodní	obchodní	k2eAgInSc1d1	obchodní
styk	styk	k1gInSc1	styk
nebyl	být	k5eNaImAgInS	být
veden	vést	k5eAaImNgInS	vést
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
zemědělci	zemědělec	k1gMnPc7	zemědělec
a	a	k8xC	a
spotřebiteli	spotřebitel	k1gMnPc7	spotřebitel
ohledně	ohledně	k7c2	ohledně
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
setkávat	setkávat	k5eAaImF	setkávat
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
trh	trh	k1gInSc4	trh
trvala	trvat	k5eAaImAgFnS	trvat
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
nebo	nebo	k8xC	nebo
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
(	(	kIx(	(
<g/>
míněno	míněn	k2eAgNnSc4d1	míněno
fixovaný	fixovaný	k2eAgInSc4d1	fixovaný
počet	počet	k1gInSc4	počet
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgInSc1d2	jednodušší
a	a	k8xC	a
přesnější	přesný	k2eAgInSc1d2	přesnější
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tyto	tento	k3xDgInPc4	tento
dny	den	k1gInPc4	den
určovat	určovat	k5eAaImF	určovat
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c6	na
lunárním	lunární	k2eAgInSc6d1	lunární
kalendáři	kalendář	k1gInSc6	kalendář
nebo	nebo	k8xC	nebo
na	na	k7c6	na
rotaci	rotace	k1gFnSc6	rotace
nebeské	nebeský	k2eAgFnSc2d1	nebeská
sféry	sféra	k1gFnSc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
tak	tak	k6eAd1	tak
raději	rád	k6eAd2	rád
používali	používat	k5eAaImAgMnP	používat
týden	týden	k1gInSc4	týden
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
prostém	prostý	k2eAgNnSc6d1	prosté
počítání	počítání	k1gNnSc6	počítání
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
pohybu	pohyb	k1gInSc6	pohyb
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
týden	týden	k1gInSc4	týden
tak	tak	k6eAd1	tak
nebyl	být	k5eNaImAgMnS	být
"	"	kIx"	"
<g/>
nebeský	nebeský	k2eAgMnSc1d1	nebeský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
sedmi	sedm	k4xCc2	sedm
dnům	den	k1gInPc3	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
pět	pět	k4xCc4	pět
známých	známý	k2eAgFnPc2d1	známá
planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Sedmidenní	sedmidenní	k2eAgInSc1d1	sedmidenní
týden	týden	k1gInSc1	týden
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
a	a	k8xC	a
Východě	východ	k1gInSc6	východ
různými	různý	k2eAgFnPc7d1	různá
cestami	cesta	k1gFnPc7	cesta
<g/>
:	:	kIx,	:
Teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pevný	pevný	k2eAgInSc1d1	pevný
sedmidenní	sedmidenní	k2eAgInSc1d1	sedmidenní
týden	týden	k1gInSc1	týden
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
díky	díky	k7c3	díky
rovnoměrnému	rovnoměrný	k2eAgNnSc3d1	rovnoměrné
rozdělení	rozdělení	k1gNnSc3	rozdělení
lunárního	lunární	k2eAgInSc2d1	lunární
měsíce	měsíc	k1gInSc2	měsíc
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
dobře	dobře	k6eAd1	dobře
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neexistuje	existovat	k5eNaImIp3nS	existovat
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
přímý	přímý	k2eAgInSc4d1	přímý
důkaz	důkaz	k1gInSc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
dodržovali	dodržovat	k5eAaImAgMnP	dodržovat
sedmidenní	sedmidenní	k2eAgInSc4d1	sedmidenní
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
astronomického	astronomický	k2eAgNnSc2d1	astronomické
pozorování	pozorování	k1gNnSc2	pozorování
a	a	k8xC	a
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
a	a	k8xC	a
božstva	božstvo	k1gNnPc1	božstvo
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
sedmi	sedm	k4xCc6	sedm
světelných	světelný	k2eAgInPc6d1	světelný
tělesech	těleso	k1gNnPc6	těleso
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
světlech	světlo	k1gNnPc6	světlo
<g/>
"	"	kIx"	"
viditelných	viditelný	k2eAgInPc2d1	viditelný
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
(	(	kIx(	(
<g/>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
pět	pět	k4xCc1	pět
viditelných	viditelný	k2eAgFnPc2d1	viditelná
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zoroastrismus	Zoroastrismus	k1gInSc1	Zoroastrismus
používal	používat	k5eAaImAgInS	používat
kombinovaně	kombinovaně	k6eAd1	kombinovaně
7	[number]	k4	7
a	a	k8xC	a
8	[number]	k4	8
denní	denní	k2eAgInPc4d1	denní
týdny	týden	k1gInPc4	týden
(	(	kIx(	(
<g/>
dělil	dělit	k5eAaImAgMnS	dělit
tak	tak	k6eAd1	tak
30	[number]	k4	30
dní	den	k1gInPc2	den
měsíce	měsíc	k1gInSc2	měsíc
na	na	k7c4	na
4	[number]	k4	4
"	"	kIx"	"
<g/>
týdny	týden	k1gInPc4	týden
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
časové	časový	k2eAgFnSc3d1	časová
posloupnosti	posloupnost	k1gFnSc3	posloupnost
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
hindský	hindský	k2eAgInSc4d1	hindský
i	i	k8xC	i
hebrejsko-křesťansko-muslimský	hebrejskořesťanskouslimský	k2eAgInSc4d1	hebrejsko-křesťansko-muslimský
týden	týden	k1gInSc4	týden
rovněž	rovněž	k9	rovněž
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
právě	právě	k6eAd1	právě
tento	tento	k3xDgInSc1	tento
babylonský	babylonský	k2eAgInSc1d1	babylonský
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zdůvodnění	zdůvodněný	k2eAgMnPc1d1	zdůvodněný
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
vlastní	vlastní	k2eAgFnPc1d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejský	hebrejský	k2eAgMnSc1d1	hebrejský
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
a	a	k8xC	a
muslimský	muslimský	k2eAgMnSc1d1	muslimský
<g/>
)	)	kIx)	)
sedmidenní	sedmidenní	k2eAgInSc1d1	sedmidenní
týden	týden	k1gInSc1	týden
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
biblickému	biblický	k2eAgInSc3d1	biblický
příběhu	příběh	k1gInSc3	příběh
o	o	k7c6	o
stvoření	stvoření	k1gNnSc6	stvoření
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Bůh	bůh	k1gMnSc1	bůh
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svět	svět	k1gInSc4	svět
během	během	k7c2	během
šesti	šest	k4xCc2	šest
dnů	den	k1gInPc2	den
a	a	k8xC	a
poté	poté	k6eAd1	poté
sedmý	sedmý	k4xOgInSc4	sedmý
den	den	k1gInSc4	den
odpočíval	odpočívat	k5eAaImAgMnS	odpočívat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
babylonské	babylonský	k2eAgNnSc1d1	babylonské
zajetí	zajetí	k1gNnSc1	zajetí
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Jóšijáš	Jóšijáš	k1gMnSc1	Jóšijáš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
teprve	teprve	k6eAd1	teprve
prosadil	prosadit	k5eAaPmAgMnS	prosadit
s	s	k7c7	s
reformami	reforma	k1gFnPc7	reforma
monoteismus	monoteismus	k1gInSc4	monoteismus
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
Tóry	tóra	k1gFnSc2	tóra
a	a	k8xC	a
Bible	bible	k1gFnSc2	bible
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
zdroje	zdroj	k1gInSc2	zdroj
není	být	k5eNaImIp3nS	být
vědecky	vědecky	k6eAd1	vědecky
přijímána	přijímán	k2eAgFnSc1d1	přijímána
a	a	k8xC	a
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kompilaci	kompilace	k1gFnSc4	kompilace
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
sedm	sedm	k4xCc1	sedm
dnů	den	k1gInPc2	den
mohlo	moct	k5eAaImAgNnS	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
textu	text	k1gInSc2	text
babylónským	babylónský	k2eAgInSc7d1	babylónský
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
například	například	k6eAd1	například
pro	pro	k7c4	pro
název	název	k1gInSc4	název
šabat	šabat	k1gInSc4	šabat
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc4d1	významný
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
babylónské	babylónský	k2eAgFnSc2d1	Babylónská
oslavy	oslava	k1gFnSc2	oslava
úplňku	úplněk	k1gInSc2	úplněk
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
Shabbât	Shabbât	k1gInSc1	Shabbât
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hindská	hindský	k2eAgFnSc1d1	hindská
civilizace	civilizace	k1gFnSc1	civilizace
má	mít	k5eAaImIp3nS	mít
sedmidenní	sedmidenní	k2eAgInSc4d1	sedmidenní
pracovní	pracovní	k2eAgInSc4d1	pracovní
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
v	v	k7c6	v
Rámájaně	Rámájana	k1gFnSc6	Rámájana
<g/>
,	,	kIx,	,
posvátného	posvátný	k2eAgInSc2d1	posvátný
eposu	epos	k1gInSc2	epos
napsaného	napsaný	k2eAgInSc2d1	napsaný
sanskrtem	sanskrt	k1gInSc7	sanskrt
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bhanu-vaar	Bhanuaar	k1gInSc1	Bhanu-vaar
znamená	znamenat	k5eAaImIp3nS	znamenat
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
Soma-vaar	Somaaar	k1gInSc4	Soma-vaar
pak	pak	k6eAd1	pak
měsíční	měsíční	k2eAgInSc4d1	měsíční
den	den	k1gInSc4	den
atd.	atd.	kA	atd.
Ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
týden	týden	k1gInSc1	týden
o	o	k7c4	o
5	[number]	k4	5
či	či	k8xC	či
10	[number]	k4	10
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
čínského	čínský	k2eAgInSc2d1	čínský
sedmidenního	sedmidenní	k2eAgInSc2d1	sedmidenní
týdne	týden	k1gInSc2	týden
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
u	u	k7c2	u
korejského	korejský	k2eAgNnSc2d1	korejské
<g/>
,	,	kIx,	,
japonského	japonský	k2eAgMnSc2d1	japonský
<g/>
,	,	kIx,	,
tibetského	tibetský	k2eAgNnSc2d1	tibetské
a	a	k8xC	a
vietnamského	vietnamský	k2eAgNnSc2d1	vietnamské
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
babylonský	babylonský	k2eAgInSc1d1	babylonský
pojem	pojem	k1gInSc1	pojem
sedmi	sedm	k4xCc2	sedm
"	"	kIx"	"
<g/>
světel	světlo	k1gNnPc2	světlo
<g/>
"	"	kIx"	"
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Dny	dna	k1gFnPc1	dna
byly	být	k5eAaImAgFnP	být
přiděleny	přidělit	k5eAaPmNgFnP	přidělit
každému	každý	k3xTgInSc3	každý
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
světel	světlo	k1gNnPc2	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
týden	týden	k1gInSc1	týden
neovlivňoval	ovlivňovat	k5eNaImAgInS	ovlivňovat
společenský	společenský	k2eAgInSc4d1	společenský
život	život	k1gInSc4	život
nebo	nebo	k8xC	nebo
úřední	úřední	k2eAgInSc4d1	úřední
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
většinou	většinou	k6eAd1	většinou
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
astrologické	astrologický	k2eAgInPc4d1	astrologický
účely	účel	k1gInPc4	účel
a	a	k8xC	a
citovaný	citovaný	k2eAgInSc1d1	citovaný
v	v	k7c6	v
několika	několik	k4yIc6	několik
buddhistických	buddhistický	k2eAgInPc6d1	buddhistický
textech	text	k1gInPc6	text
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gMnSc4	on
Jezuité	jezuita	k1gMnPc1	jezuita
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
znovu	znovu	k6eAd1	znovu
zavedli	zavést	k5eAaPmAgMnP	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Japonci	Japonec	k1gMnPc1	Japonec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
převzali	převzít	k5eAaPmAgMnP	převzít
sedmidenní	sedmidenní	k2eAgInSc4d1	sedmidenní
západní	západní	k2eAgInSc4d1	západní
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
převzali	převzít	k5eAaPmAgMnP	převzít
vlastní	vlastní	k2eAgInSc4d1	vlastní
astrologický	astrologický	k2eAgInSc4d1	astrologický
týden	týden	k1gInSc4	týden
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
pro	pro	k7c4	pro
dny	den	k1gInPc4	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
anglickým	anglický	k2eAgInPc3d1	anglický
názvům	název	k1gInPc3	název
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
záchrana	záchrana	k1gFnSc1	záchrana
babylonských	babylonský	k2eAgInPc2d1	babylonský
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
anglická	anglický	k2eAgNnPc1d1	anglické
jména	jméno	k1gNnPc1	jméno
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
názvu	název	k1gInSc2	název
bohů	bůh	k1gMnPc2	bůh
germánské	germánský	k2eAgFnSc2d1	germánská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
japonské	japonský	k2eAgInPc1d1	japonský
názvy	název	k1gInPc1	název
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
čínský	čínský	k2eAgInSc4d1	čínský
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
pět	pět	k4xCc1	pět
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
planety	planeta	k1gFnPc1	planeta
japonského	japonský	k2eAgInSc2d1	japonský
týdne	týden	k1gInSc2	týden
mají	mít	k5eAaImIp3nP	mít
čínská	čínský	k2eAgNnPc1d1	čínské
jména	jméno	k1gNnPc1	jméno
založená	založený	k2eAgNnPc1d1	založené
na	na	k7c6	na
pěti	pět	k4xCc6	pět
elementech	element	k1gInPc6	element
než	než	k8xS	než
na	na	k7c6	na
pohanském	pohanský	k2eAgNnSc6d1	pohanské
božstvu	božstvo	k1gNnSc6	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Egypťané	Egypťan	k1gMnPc1	Egypťan
používali	používat	k5eAaImAgMnP	používat
týden	týden	k1gInSc4	týden
o	o	k7c6	o
10	[number]	k4	10
dnech	den	k1gInPc6	den
(	(	kIx(	(
<g/>
dekan	dekan	k1gInSc1	dekan
<g/>
)	)	kIx)	)
a	a	k8xC	a
dělili	dělit	k5eAaImAgMnP	dělit
tak	tak	k6eAd1	tak
měsíc	měsíc	k1gInSc4	měsíc
s	s	k7c7	s
třiceti	třicet	k4xCc7	třicet
dny	den	k1gInPc7	den
na	na	k7c4	na
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Řekové	Řek	k1gMnPc1	Řek
ale	ale	k8xC	ale
přejali	přejmout	k5eAaPmAgMnP	přejmout
týden	týden	k1gInSc4	týden
od	od	k7c2	od
Babyloňanů	Babyloňan	k1gMnPc2	Babyloňan
(	(	kIx(	(
<g/>
Eudoxos	Eudoxos	k1gMnSc1	Eudoxos
z	z	k7c2	z
Knidu	Knid	k1gInSc2	Knid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
skupiny	skupina	k1gFnPc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
postupně	postupně	k6eAd1	postupně
přebíraly	přebírat	k5eAaImAgFnP	přebírat
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
trávily	trávit	k5eAaImAgFnP	trávit
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sedmidenní	sedmidenní	k2eAgInSc4d1	sedmidenní
týden	týden	k1gInSc4	týden
používal	používat	k5eAaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
životě	život	k1gInSc6	život
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
od	od	k7c2	od
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
)	)	kIx)	)
totiž	totiž	k9	totiž
obvyklá	obvyklý	k2eAgNnPc1d1	obvyklé
osmidenní	osmidenní	k2eAgNnPc1d1	osmidenní
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
každodenní	každodenní	k2eAgFnSc2d1	každodenní
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
osmý	osmý	k4xOgInSc4	osmý
den	den	k1gInSc4	den
přicházeli	přicházet	k5eAaImAgMnP	přicházet
totiž	totiž	k9	totiž
rolníci	rolník	k1gMnPc1	rolník
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Říma	Řím	k1gInSc2	Řím
do	do	k7c2	do
města	město	k1gNnSc2	město
na	na	k7c4	na
trh	trh	k1gInSc4	trh
a	a	k8xC	a
vyřídit	vyřídit	k5eAaPmF	vyřídit
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnPc4	svůj
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
osmý	osmý	k4xOgInSc4	osmý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
nundinae	nundinae	k1gNnPc4	nundinae
a	a	k8xC	a
dny	den	k1gInPc4	den
celého	celý	k2eAgInSc2d1	celý
osmidenního	osmidenní	k2eAgInSc2d1	osmidenní
týdne	týden	k1gInSc2	týden
se	se	k3xPyFc4	se
označovaly	označovat	k5eAaImAgInP	označovat
písmeny	písmeno	k1gNnPc7	písmeno
A	A	kA	A
až	až	k8xS	až
H.	H.	kA	H.
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
i	i	k8xC	i
oktáv	oktáv	k1gInSc4	oktáv
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
321	[number]	k4	321
n.	n.	k?	n.
l.	l.	k?	l.
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
reguloval	regulovat	k5eAaImAgMnS	regulovat
používání	používání	k1gNnSc4	používání
týdne	týden	k1gInSc2	týden
kvůli	kvůli	k7c3	kvůli
problému	problém	k1gInSc3	problém
používání	používání	k1gNnSc2	používání
různých	různý	k2eAgInPc2d1	různý
dnů	den	k1gInPc2	den
pro	pro	k7c4	pro
náboženské	náboženský	k2eAgFnPc4d1	náboženská
slavnosti	slavnost	k1gFnPc4	slavnost
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
neděli	neděle	k1gFnSc4	neděle
jako	jako	k8xS	jako
den	den	k1gInSc4	den
náboženského	náboženský	k2eAgInSc2d1	náboženský
svátku	svátek	k1gInSc2	svátek
a	a	k8xC	a
odpočinku	odpočinek	k1gInSc2	odpočinek
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
již	jenž	k3xRgFnSc4	jenž
neděli	neděle	k1gFnSc4	neděle
slavily	slavit	k5eAaImAgInP	slavit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
převzali	převzít	k5eAaPmAgMnP	převzít
sedmidenní	sedmidenní	k2eAgInSc4d1	sedmidenní
týden	týden	k1gInSc4	týden
i	i	k9	i
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
společenství	společenství	k1gNnSc1	společenství
raných	raný	k2eAgMnPc2d1	raný
křesťanů	křesťan	k1gMnPc2	křesťan
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
odlišovat	odlišovat	k5eAaImF	odlišovat
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
židovské	židovský	k2eAgFnSc2d1	židovská
společnosti	společnost	k1gFnSc2	společnost
např.	např.	kA	např.
i	i	k8xC	i
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
Židů	Žid	k1gMnPc2	Žid
připadl	připadnout	k5eAaPmAgInS	připadnout
hlavní	hlavní	k2eAgInSc1d1	hlavní
svátek	svátek	k1gInSc1	svátek
na	na	k7c4	na
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
u	u	k7c2	u
křesťanů	křesťan	k1gMnPc2	křesťan
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
uchovali	uchovat	k5eAaPmAgMnP	uchovat
jako	jako	k8xS	jako
tradiční	tradiční	k2eAgInSc4d1	tradiční
svátek	svátek	k1gInSc4	svátek
sobotu	sobota	k1gFnSc4	sobota
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
uctívání	uctívání	k1gNnSc6	uctívání
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
vzestupu	vzestup	k1gInSc6	vzestup
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
svátečním	sváteční	k2eAgInSc7d1	sváteční
dnem	den	k1gInSc7	den
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
náboženství	náboženství	k1gNnSc4	náboženství
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
<s>
Sedmidenní	sedmidenní	k2eAgInSc1d1	sedmidenní
týden	týden	k1gInSc1	týden
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgInS	stát
zvykem	zvyk	k1gInSc7	zvyk
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
,	,	kIx,	,
židy	žid	k1gMnPc4	žid
i	i	k8xC	i
muslimy	muslim	k1gMnPc4	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
evropská	evropský	k2eAgFnSc1d1	Evropská
kolonizace	kolonizace	k1gFnSc1	kolonizace
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývající	vyplývající	k2eAgInSc4d1	vyplývající
rozvoj	rozvoj	k1gInSc4	rozvoj
globálního	globální	k2eAgInSc2d1	globální
obchodu	obchod	k1gInSc2	obchod
zavedl	zavést	k5eAaPmAgInS	zavést
sedmidenní	sedmidenní	k2eAgInSc1d1	sedmidenní
týden	týden	k1gInSc1	týden
jako	jako	k8xS	jako
univerzální	univerzální	k2eAgInSc4d1	univerzální
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
i	i	k9	i
v	v	k7c6	v
kulturách	kultura	k1gFnPc6	kultura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepraktikoval	praktikovat	k5eNaImAgMnS	praktikovat
<g/>
.	.	kIx.	.
</s>
<s>
Dvoudenní	dvoudenní	k2eAgNnSc1d1	dvoudenní
víkendové	víkendový	k2eAgNnSc1d1	víkendové
volno	volno	k1gNnSc1	volno
utvrdilo	utvrdit	k5eAaPmAgNnS	utvrdit
pondělní	pondělní	k2eAgInSc4d1	pondělní
začátek	začátek	k1gInSc4	začátek
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
normy	norma	k1gFnSc2	norma
ISO	ISO	kA	ISO
8601	[number]	k4	8601
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
datování	datování	k1gNnSc4	datování
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
definuje	definovat	k5eAaBmIp3nS	definovat
také	také	k9	také
pondělí	pondělí	k1gNnSc4	pondělí
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
standardu	standard	k1gInSc6	standard
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
prvním	první	k4xOgInSc7	první
týdnem	týden	k1gInSc7	týden
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
takový	takový	k3xDgInSc4	takový
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
první	první	k4xOgInSc4	první
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
je	být	k5eAaImIp3nS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
nezačíná	začínat	k5eNaImIp3nS	začínat
tzv.	tzv.	kA	tzv.
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
víkendem	víkend	k1gInSc7	víkend
(	(	kIx(	(
<g/>
od	od	k7c2	od
pátku	pátek	k1gInSc2	pátek
do	do	k7c2	do
neděle	neděle	k1gFnSc2	neděle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bez	bez	k7c2	bez
přímého	přímý	k2eAgInSc2d1	přímý
astronomického	astronomický	k2eAgInSc2d1	astronomický
základu	základ	k1gInSc2	základ
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc1	sedm
dní	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
než	než	k8xS	než
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
lunárního	lunární	k2eAgInSc2d1	lunární
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
týden	týden	k1gInSc4	týden
široce	široko	k6eAd1	široko
užívanou	užívaný	k2eAgFnSc7d1	užívaná
jednotkou	jednotka	k1gFnSc7	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
sociálním	sociální	k2eAgMnSc6d1	sociální
a	a	k8xC	a
komerčním	komerční	k2eAgInSc6d1	komerční
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Týdny	týden	k1gInPc1	týden
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
formu	forma	k1gFnSc4	forma
nezávislého	závislý	k2eNgInSc2d1	nezávislý
souvislého	souvislý	k2eAgInSc2d1	souvislý
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
běží	běžet	k5eAaImIp3nS	běžet
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
dalšími	další	k2eAgInPc7d1	další
kalendáři	kalendář	k1gInPc7	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
některé	některý	k3yIgInPc1	některý
originální	originální	k2eAgInPc1d1	originální
kalendáře	kalendář	k1gInPc1	kalendář
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
týdny	týden	k1gInPc1	týden
a	a	k8xC	a
roky	rok	k1gInPc1	rok
byly	být	k5eAaImAgInP	být
synchronizovány	synchronizován	k2eAgInPc1d1	synchronizován
přidáváním	přidávání	k1gNnSc7	přidávání
skokových	skokový	k2eAgInPc2d1	skokový
<g/>
,	,	kIx,	,
přestupných	přestupný	k2eAgInPc2d1	přestupný
týdnů	týden	k1gInPc2	týden
nebo	nebo	k8xC	nebo
dní	den	k1gInPc2	den
do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
takovýchto	takovýto	k3xDgInPc2	takovýto
kalendářů	kalendář	k1gInPc2	kalendář
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dané	daný	k2eAgNnSc1d1	dané
datum	datum	k1gNnSc1	datum
připadá	připadat	k5eAaImIp3nS	připadat
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
navrhovaný	navrhovaný	k2eAgInSc1d1	navrhovaný
světový	světový	k2eAgInSc1d1	světový
kalendář	kalendář	k1gInSc1	kalendář
má	mít	k5eAaImIp3nS	mít
52	[number]	k4	52
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
dodatečné	dodatečný	k2eAgInPc4d1	dodatečný
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
francouzský	francouzský	k2eAgInSc4d1	francouzský
revoluční	revoluční	k2eAgInSc4d1	revoluční
kalendář	kalendář	k1gInSc4	kalendář
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měl	mít	k5eAaImAgMnS	mít
36	[number]	k4	36
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
dekád	dekáda	k1gFnPc2	dekáda
<g/>
)	)	kIx)	)
po	po	k7c6	po
10	[number]	k4	10
dnech	den	k1gInPc6	den
a	a	k8xC	a
pět	pět	k4xCc4	pět
nebo	nebo	k8xC	nebo
šest	šest	k4xCc4	šest
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
přidávání	přidávání	k1gNnSc2	přidávání
dalších	další	k2eAgInPc2d1	další
dnů	den	k1gInPc2	den
mimo	mimo	k7c4	mimo
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přidat	přidat	k5eAaPmF	přidat
celý	celý	k2eAgInSc4d1	celý
týden	týden	k1gInSc4	týden
do	do	k7c2	do
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
roky	rok	k1gInPc1	rok
mají	mít	k5eAaImIp3nP	mít
kolísavou	kolísavý	k2eAgFnSc4d1	kolísavá
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
bývalý	bývalý	k2eAgInSc1d1	bývalý
islandský	islandský	k2eAgInSc1d1	islandský
kalendář	kalendář	k1gInSc1	kalendář
měl	mít	k5eAaImAgInS	mít
roky	rok	k1gInPc4	rok
o	o	k7c4	o
52	[number]	k4	52
nebo	nebo	k8xC	nebo
53	[number]	k4	53
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Nejranější	raný	k2eAgInSc1d3	nejranější
severský	severský	k2eAgInSc1d1	severský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
,	,	kIx,	,
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
vikinských	vikinský	k2eAgFnPc2d1	vikinská
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
pětidenní	pětidenní	k2eAgInPc4d1	pětidenní
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
fimmty	fimmt	k1gInPc4	fimmt
<g/>
,	,	kIx,	,
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
do	do	k7c2	do
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
šesti	šest	k4xCc6	šest
fimmtech	fimmt	k1gInPc6	fimmt
<g/>
,	,	kIx,	,
s	s	k7c7	s
pěti	pět	k4xCc7	pět
obřadními	obřadní	k2eAgInPc7d1	obřadní
dny	den	k1gInPc7	den
nepříslušející	příslušející	k2eNgFnSc2d1	nepříslušející
žádnému	žádný	k3yNgInSc3	žádný
měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Hermetický	hermetický	k2eAgInSc1d1	hermetický
lunární	lunární	k2eAgInSc1d1	lunární
týdenní	týdenní	k2eAgInSc1d1	týdenní
kalendář	kalendář	k1gInSc1	kalendář
používá	používat	k5eAaImIp3nS	používat
týden	týden	k1gInSc4	týden
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
lunace	lunace	k1gFnSc2	lunace
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
nebo	nebo	k8xC	nebo
9	[number]	k4	9
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
7.382	[number]	k4	7.382
<g/>
647	[number]	k4	647
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dny	dna	k1gFnSc2	dna
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dnů	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
daných	daný	k2eAgInPc2d1	daný
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jména	jméno	k1gNnPc4	jméno
dní	den	k1gInPc2	den
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
řadových	řadový	k2eAgFnPc2d1	řadová
číslovek	číslovka	k1gFnPc2	číslovka
nebo	nebo	k8xC	nebo
vymezením	vymezení	k1gNnSc7	vymezení
pozice	pozice	k1gFnSc2	pozice
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dní	den	k1gInPc2	den
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
dnům	den	k1gInPc3	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
jsou	být	k5eAaImIp3nP	být
názvy	název	k1gInPc1	název
dnů	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
pondělí	pondělí	k1gNnSc2	pondělí
úterý	úterý	k1gNnSc2	úterý
středa	středa	k1gFnSc1	středa
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
pátek	pátek	k1gInSc1	pátek
sobota	sobota	k1gFnSc1	sobota
neděle	neděle	k1gFnSc1	neděle
Sobota	sobota	k1gFnSc1	sobota
a	a	k8xC	a
neděle	neděle	k1gFnSc1	neděle
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývají	nazývat	k5eAaImIp3nP	nazývat
dohromady	dohromady	k6eAd1	dohromady
jako	jako	k8xS	jako
víkend	víkend	k1gInSc4	víkend
a	a	k8xC	a
většině	většina	k1gFnSc6	většina
západních	západní	k2eAgFnPc6d1	západní
kulturách	kultura	k1gFnPc6	kultura
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
dny	den	k1gInPc7	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
pět	pět	k4xCc4	pět
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
termín	termín	k1gInSc1	termín
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
osvobozením	osvobození	k1gNnSc7	osvobození
soboty	sobota	k1gFnSc2	sobota
od	od	k7c2	od
práce	práce	k1gFnSc2	práce
týkal	týkat	k5eAaImAgInS	týkat
všech	všecek	k3xTgMnPc2	všecek
dní	den	k1gInPc2	den
kromě	kromě	k7c2	kromě
neděle	neděle	k1gFnSc2	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Pátek	pátek	k1gInSc1	pátek
a	a	k8xC	a
sobota	sobota	k1gFnSc1	sobota
jsou	být	k5eAaImIp3nP	být
dny	den	k1gInPc4	den
odpočinku	odpočinek	k1gInSc2	odpočinek
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
muslimských	muslimský	k2eAgFnPc6d1	muslimská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Biblický	biblický	k2eAgInSc1d1	biblický
sabat	sabat	k1gInSc1	sabat
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
pátečního	páteční	k2eAgInSc2d1	páteční
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
do	do	k7c2	do
sobotního	sobotní	k2eAgInSc2d1	sobotní
slunečního	sluneční	k2eAgInSc2d1	sluneční
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Írán	Írán	k1gInSc1	Írán
je	být	k5eAaImIp3nS	být
víkendem	víkend	k1gInSc7	víkend
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
pátek	pátek	k1gInSc4	pátek
<g/>
)	)	kIx)	)
a	a	k8xC	a
týden	týden	k1gInSc4	týden
začíná	začínat	k5eAaImIp3nS	začínat
sobotou	sobota	k1gFnSc7	sobota
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
muslimské	muslimský	k2eAgFnPc1d1	muslimská
země	zem	k1gFnPc1	zem
mají	mít	k5eAaImIp3nP	mít
víkend	víkend	k1gInSc4	víkend
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
a	a	k8xC	a
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
víkendové	víkendový	k2eAgInPc1d1	víkendový
dny	den	k1gInPc1	den
začaly	začít	k5eAaPmAgInP	začít
převládat	převládat	k5eAaImF	převládat
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
kalendářů	kalendář	k1gInPc2	kalendář
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
neděle	neděle	k1gFnSc2	neděle
na	na	k7c4	na
konec	konec	k1gInSc4	konec
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pětidenním	pětidenní	k2eAgInSc6d1	pětidenní
pracovním	pracovní	k2eAgInSc6d1	pracovní
týdnu	týden	k1gInSc6	týden
a	a	k8xC	a
zaměňování	zaměňování	k1gNnSc6	zaměňování
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
uctívání	uctívání	k1gNnSc2	uctívání
neděle	neděle	k1gFnSc2	neděle
některými	některý	k3yIgFnPc7	některý
lidmi	člověk	k1gMnPc7	člověk
za	za	k7c4	za
den	den	k1gInSc4	den
sabatu	sabat	k1gInSc2	sabat
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
začali	začít	k5eAaPmAgMnP	začít
považovat	považovat	k5eAaImF	považovat
pondělí	pondělí	k1gNnSc4	pondělí
za	za	k7c4	za
počáteční	počáteční	k2eAgInSc4d1	počáteční
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
ISO	ISO	kA	ISO
a	a	k8xC	a
evropské	evropský	k2eAgFnPc1d1	Evropská
normy	norma	k1gFnPc1	norma
předepisují	předepisovat	k5eAaImIp3nP	předepisovat
pondělí	pondělí	k1gNnSc4	pondělí
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ISO	ISO	kA	ISO
8601	[number]	k4	8601
nebyla	být	k5eNaImAgFnS	být
běžně	běžně	k6eAd1	běžně
přijata	přijmout	k5eAaPmNgFnS	přijmout
nebo	nebo	k8xC	nebo
implementována	implementovat	k5eAaImNgFnS	implementovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
důležité	důležitý	k2eAgInPc4d1	důležitý
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Týdny	týden	k1gInPc1	týden
v	v	k7c6	v
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
číslovány	číslován	k2eAgFnPc1d1	číslována
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
číslování	číslování	k1gNnSc2	číslování
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
používán	používán	k2eAgMnSc1d1	používán
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
<g/>
)	)	kIx)	)
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
evropských	evropský	k2eAgFnPc6d1	Evropská
a	a	k8xC	a
asijských	asijský	k2eAgFnPc6d1	asijská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinde	jinde	k6eAd1	jinde
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
ISO	ISO	kA	ISO
8601	[number]	k4	8601
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ISO	ISO	kA	ISO
datovací	datovací	k2eAgInSc1d1	datovací
systém	systém	k1gInSc1	systém
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
číselný	číselný	k2eAgInSc4d1	číselný
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
týdny	týden	k1gInPc4	týden
<g/>
;	;	kIx,	;
každý	každý	k3xTgInSc1	každý
týden	týden	k1gInSc1	týden
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jeho	jeho	k3xOp3gInSc4	jeho
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
samotný	samotný	k2eAgInSc1d1	samotný
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
víkend	víkend	k1gInSc1	víkend
pátek-sobota	pátekobota	k1gFnSc1	pátek-sobota
se	se	k3xPyFc4	se
nestal	stát	k5eNaPmAgMnS	stát
týdnem	týden	k1gInSc7	týden
jednoho	jeden	k4xCgMnSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
1	[number]	k4	1
<g/>
.	.	kIx.	.
týden	týden	k1gInSc4	týden
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
W	W	kA	W
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
a	a	k8xC	a
končil	končit	k5eAaImAgInS	končit
nedělí	neděle	k1gFnSc7	neděle
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
číslo	číslo	k1gNnSc1	číslo
týdne	týden	k1gInSc2	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hodnotu	hodnota	k1gFnSc4	hodnota
52	[number]	k4	52
nebo	nebo	k8xC	nebo
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
číslování	číslování	k1gNnSc2	číslování
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
odchylovat	odchylovat	k5eAaImF	odchylovat
od	od	k7c2	od
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
ISO	ISO	kA	ISO
standardu	standard	k1gInSc2	standard
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
přinejmenším	přinejmenším	k6eAd1	přinejmenším
šest	šest	k4xCc4	šest
možností	možnost	k1gFnPc2	možnost
<g/>
:	:	kIx,	:
Systém	systém	k1gInSc1	systém
sedmidenních	sedmidenní	k2eAgInPc2d1	sedmidenní
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
velkých	velký	k2eAgFnPc2d1	velká
lidských	lidský	k2eAgFnPc2d1	lidská
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
počítají	počítat	k5eAaImIp3nP	počítat
s	s	k7c7	s
deseti	deset	k4xCc7	deset
číslicemi	číslice	k1gFnPc7	číslice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
kulturách	kultura	k1gFnPc6	kultura
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
i	i	k8xC	i
týdny	týden	k1gInPc1	týden
jiné	jiný	k2eAgFnSc2d1	jiná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
pracovních	pracovní	k2eAgInPc2d1	pracovní
lidských	lidský	k2eAgInPc2d1	lidský
rytmů	rytmus	k1gInPc2	rytmus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nebyl	být	k5eNaImAgInS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
podle	podle	k7c2	podle
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
starých	starý	k2eAgFnPc6d1	stará
kulturách	kultura	k1gFnPc6	kultura
pozorovány	pozorován	k2eAgInPc1d1	pozorován
dílčí	dílčí	k2eAgInPc1d1	dílčí
rytmy	rytmus	k1gInPc1	rytmus
složené	složený	k2eAgInPc1d1	složený
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
se	se	k3xPyFc4	se
do	do	k7c2	do
císařských	císařský	k2eAgFnPc2d1	císařská
dob	doba	k1gFnPc2	doba
nepoužíval	používat	k5eNaImAgMnS	používat
žádný	žádný	k3yNgInSc4	žádný
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kalendy	kalendy	k1gFnPc1	kalendy
<g/>
,	,	kIx,	,
nony	nony	k1gInPc1	nony
<g/>
,	,	kIx,	,
idy	idy	k1gFnPc1	idy
a	a	k8xC	a
terminalie	terminalie	k1gFnPc1	terminalie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
321	[number]	k4	321
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgFnP	být
vystřídány	vystřídán	k2eAgMnPc4d1	vystřídán
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
týdnem	týden	k1gInSc7	týden
s	s	k7c7	s
nedělí	neděle	k1gFnSc7	neděle
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
oficiálním	oficiální	k2eAgInSc7d1	oficiální
dnem	den	k1gInSc7	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Pětidenní	pětidenní	k2eAgInSc4d1	pětidenní
a	a	k8xC	a
třináctidenní	třináctidenní	k2eAgInSc4d1	třináctidenní
týden	týden	k1gInSc4	týden
měli	mít	k5eAaImAgMnP	mít
Aztékové	Azték	k1gMnPc1	Azték
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgInSc1d1	normální
aztécký	aztécký	k2eAgInSc1d1	aztécký
kalendář	kalendář	k1gInSc1	kalendář
se	se	k3xPyFc4	se
řídil	řídit	k5eAaImAgInS	řídit
slunečním	sluneční	k2eAgInSc7d1	sluneční
rokem	rok	k1gInSc7	rok
a	a	k8xC	a
nazýval	nazývat	k5eAaImAgMnS	nazývat
se	se	k3xPyFc4	se
xihuitl	xihuitnout	k5eAaPmAgMnS	xihuitnout
(	(	kIx(	(
<g/>
u	u	k7c2	u
Mayů	May	k1gMnPc2	May
haab	haaba	k1gFnPc2	haaba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
20	[number]	k4	20
dnech	den	k1gInPc6	den
a	a	k8xC	a
5	[number]	k4	5
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
měl	mít	k5eAaImAgMnS	mít
4	[number]	k4	4
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
5	[number]	k4	5
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
byl	být	k5eAaImAgMnS	být
dnem	den	k1gInSc7	den
veřejného	veřejný	k2eAgInSc2d1	veřejný
trhu	trh	k1gInSc2	trh
<g/>
(	(	kIx(	(
<g/>
tianquiztli	tianquiztle	k1gFnSc4	tianquiztle
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
svátečním	sváteční	k2eAgNnSc7d1	sváteční
dnem	dno	k1gNnSc7	dno
a	a	k8xC	a
dnem	den	k1gInSc7	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Týdny	týden	k1gInPc1	týden
tak	tak	k9	tak
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
dohromady	dohromady	k6eAd1	dohromady
za	za	k7c4	za
rok	rok	k1gInSc4	rok
288	[number]	k4	288
pracovních	pracovní	k2eAgInPc2d1	pracovní
dnů	den	k1gInPc2	den
a	a	k8xC	a
72	[number]	k4	72
tianquiztli	tianquiztle	k1gFnSc4	tianquiztle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
nešťastných	šťastný	k2eNgInPc6d1	nešťastný
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
nesmělo	smět	k5eNaImAgNnS	smět
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dalo	dát	k5eAaPmAgNnS	dát
společně	společně	k6eAd1	společně
365	[number]	k4	365
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
sluneční	sluneční	k2eAgInSc1d1	sluneční
rok	rok	k1gInSc1	rok
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
delší	dlouhý	k2eAgFnSc4d2	delší
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
posunuje	posunovat	k5eAaImIp3nS	posunovat
kalendář	kalendář	k1gInSc1	kalendář
vůči	vůči	k7c3	vůči
skutečnému	skutečný	k2eAgInSc3d1	skutečný
slunečnímu	sluneční	k2eAgInSc3d1	sluneční
roku	rok	k1gInSc3	rok
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aztékové	Azték	k1gMnPc1	Azték
tento	tento	k3xDgInSc4	tento
deficit	deficit	k1gInSc4	deficit
vyrovnávali	vyrovnávat	k5eAaImAgMnP	vyrovnávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
prováděno	provádět	k5eAaImNgNnS	provádět
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
pouze	pouze	k6eAd1	pouze
různé	různý	k2eAgFnPc1d1	různá
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
svatý	svatý	k2eAgInSc1d1	svatý
kalendář	kalendář	k1gInSc1	kalendář
o	o	k7c6	o
260	[number]	k4	260
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
tonalpohualli	tonalpohualle	k1gFnSc3	tonalpohualle
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
ke	k	k7c3	k
věštění	věštění	k1gNnSc3	věštění
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
délku	délka	k1gFnSc4	délka
260	[number]	k4	260
dnů	den	k1gInPc2	den
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
20	[number]	k4	20
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
13	[number]	k4	13
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
jednomu	jeden	k4xCgNnSc3	jeden
bohu	bůh	k1gMnSc6	bůh
nebo	nebo	k8xC	nebo
bohyni	bohyně	k1gFnSc6	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
člověka	člověk	k1gMnSc4	člověk
závisel	záviset	k5eAaImAgInS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
dni	den	k1gInPc1	den
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
připisovaly	připisovat	k5eAaImAgFnP	připisovat
dobré	dobrý	k2eAgFnPc1d1	dobrá
nebo	nebo	k8xC	nebo
špatné	špatný	k2eAgFnPc1d1	špatná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
den	den	k1gInSc1	den
"	"	kIx"	"
<g/>
Sedm	sedm	k4xCc4	sedm
dešťů	dešť	k1gInPc2	dešť
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
dnem	den	k1gInSc7	den
výhodným	výhodný	k2eAgInSc7d1	výhodný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
den	den	k1gInSc1	den
"	"	kIx"	"
<g/>
Dva	dva	k4xCgMnPc1	dva
králíci	králík	k1gMnPc1	králík
<g/>
"	"	kIx"	"
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
dnem	den	k1gInSc7	den
špatným	špatný	k2eAgInSc7d1	špatný
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
u	u	k7c2	u
francouzského	francouzský	k2eAgInSc2d1	francouzský
revolučního	revoluční	k2eAgInSc2d1	revoluční
kalendáře	kalendář	k1gInSc2	kalendář
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
desetidenní	desetidenní	k2eAgInSc1d1	desetidenní
týden	týden	k1gInSc1	týden
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
měsíc	měsíc	k1gInSc1	měsíc
tak	tak	k6eAd1	tak
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
tři	tři	k4xCgInPc4	tři
tyto	tento	k3xDgInPc4	tento
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
měsíc	měsíc	k1gInSc1	měsíc
tak	tak	k9	tak
měl	mít	k5eAaImAgInS	mít
třicet	třicet	k4xCc4	třicet
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
5	[number]	k4	5
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
Napoleon	Napoleon	k1gMnSc1	Napoleon
tento	tento	k3xDgInSc4	tento
kalendář	kalendář	k1gInSc4	kalendář
opět	opět	k6eAd1	opět
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
revoluční	revoluční	k2eAgInSc1d1	revoluční
říjnový	říjnový	k2eAgInSc1d1	říjnový
kalendář	kalendář	k1gInSc1	kalendář
měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
pětidenní	pětidenní	k2eAgInSc1d1	pětidenní
týden	týden	k1gInSc1	týden
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
pracovní	pracovní	k2eAgFnPc1d1	pracovní
dny	dna	k1gFnPc1	dna
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
volný	volný	k2eAgMnSc1d1	volný
<g/>
)	)	kIx)	)
s	s	k7c7	s
šestitýdenními	šestitýdenní	k2eAgInPc7d1	šestitýdenní
měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Týdny	týden	k1gInPc1	týden
se	se	k3xPyFc4	se
točily	točit	k5eAaImAgInP	točit
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
provoz	provoz	k1gInSc4	provoz
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Bali	Bal	k1gInSc6	Bal
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
dnes	dnes	k6eAd1	dnes
používáno	používat	k5eAaImNgNnS	používat
současně	současně	k6eAd1	současně
několik	několik	k4yIc1	několik
různých	různý	k2eAgInPc2d1	různý
hinduistických	hinduistický	k2eAgInPc2d1	hinduistický
týdenních	týdenní	k2eAgInPc2d1	týdenní
systému	systém	k1gInSc2	systém
pro	pro	k7c4	pro
rituální	rituální	k2eAgInPc4d1	rituální
účely	účel	k1gInPc4	účel
(	(	kIx(	(
<g/>
Pakuwon	Pakuwon	k1gInSc4	Pakuwon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kombinace	kombinace	k1gFnSc2	kombinace
těchto	tento	k3xDgInPc2	tento
týdnů	týden	k1gInPc2	týden
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
dostat	dostat	k5eAaPmF	dostat
55	[number]	k4	55
různých	různý	k2eAgInPc2d1	různý
dnů	den	k1gInPc2	den
((	((	k?	((
<g/>
=	=	kIx~	=
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
+	+	kIx~	+
<g/>
8	[number]	k4	8
<g/>
+	+	kIx~	+
<g/>
9	[number]	k4	9
<g/>
+	+	kIx~	+
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
tato	tento	k3xDgFnSc1	tento
kombinace	kombinace	k1gFnSc1	kombinace
:	:	kIx,	:
Triwara	Triwara	k1gFnSc1	Triwara
=	=	kIx~	=
Třídenní	třídenní	k2eAgInSc1d1	třídenní
týden	týden	k1gInSc1	týden
Pancawara	Pancawara	k1gFnSc1	Pancawara
=	=	kIx~	=
Pětidenní	pětidenní	k2eAgInSc1d1	pětidenní
týden	týden	k1gInSc1	týden
Saptawara	Saptawara	k1gFnSc1	Saptawara
=	=	kIx~	=
Sedmidenní	sedmidenní	k2eAgInSc1d1	sedmidenní
týden	týden	k1gInSc1	týden
Třídenní	třídenní	k2eAgInSc4d1	třídenní
týden	týden	k1gInSc4	týden
(	(	kIx(	(
<g/>
Triwara	Triwara	k1gFnSc1	Triwara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Bali	Bal	k1gFnSc6	Bal
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
cyklem	cyklus	k1gInSc7	cyklus
tržních	tržní	k2eAgInPc2d1	tržní
dnů	den	k1gInPc2	den
<g/>
:	:	kIx,	:
každým	každý	k3xTgMnSc7	každý
třetím	třetí	k4xOgInSc7	třetí
dnem	den	k1gInSc7	den
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
trh	trh	k1gInSc1	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
liturgii	liturgie	k1gFnSc6	liturgie
je	být	k5eAaImIp3nS	být
týden	týden	k1gInSc4	týden
hlavně	hlavně	k9	hlavně
dominantní	dominantní	k2eAgNnSc1d1	dominantní
speciálním	speciální	k2eAgNnSc7d1	speciální
postavením	postavení	k1gNnSc7	postavení
neděle	neděle	k1gFnSc2	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
byl	být	k5eAaImAgInS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
mezi	mezi	k7c4	mezi
Židy	Žid	k1gMnPc4	Žid
za	za	k7c4	za
posvátný	posvátný	k2eAgInSc4d1	posvátný
pojem	pojem	k1gInSc4	pojem
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zákonu	zákon	k1gInSc3	zákon
ohledně	ohledně	k7c2	ohledně
odpočinku	odpočinek	k1gInSc2	odpočinek
o	o	k7c6	o
sabatu	sabat	k1gInSc6	sabat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
asociací	asociace	k1gFnPc2	asociace
s	s	k7c7	s
první	první	k4xOgFnSc7	první
kapitolou	kapitola	k1gFnSc7	kapitola
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
.	.	kIx.	.
</s>
<s>
Raní	raný	k2eAgMnPc1d1	raný
křesťané	křesťan	k1gMnPc1	křesťan
převzali	převzít	k5eAaPmAgMnP	převzít
jeho	jeho	k3xOp3gNnSc4	jeho
používání	používání	k1gNnSc4	používání
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
podobě	podoba	k1gFnSc6	podoba
s	s	k7c7	s
číslovanými	číslovaný	k2eAgInPc7d1	číslovaný
dny	den	k1gInPc7	den
beze	beze	k7c2	beze
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sabatu	sabat	k1gInSc2	sabat
<g/>
.	.	kIx.	.
</s>
<s>
Neděle	neděle	k1gFnSc1	neděle
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
týdne	týden	k1gInSc2	týden
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Skutky	skutek	k1gInPc7	skutek
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Korintským	korintský	k2eAgInSc7d1	korintský
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Zjevení	zjevení	k1gNnSc1	zjevení
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
nahradila	nahradit	k5eAaPmAgFnS	nahradit
sabat	sabat	k1gInSc4	sabat
jako	jako	k8xC	jako
den	den	k1gInSc4	den
náboženského	náboženský	k2eAgInSc2d1	náboženský
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotný	samotný	k2eAgInSc1d1	samotný
týden	týden	k1gInSc1	týden
zůstal	zůstat	k5eAaPmAgInS	zůstat
samostatný	samostatný	k2eAgInSc1d1	samostatný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
vhodné	vhodný	k2eAgNnSc1d1	vhodné
si	se	k3xPyFc3	se
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvá	prvý	k4xOgNnPc1	prvý
dvě	dva	k4xCgNnPc1	dva
století	století	k1gNnPc1	století
oslavy	oslava	k1gFnSc2	oslava
velkých	velký	k2eAgNnPc2d1	velké
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
mystérií	mystérium	k1gNnPc2	mystérium
tvořily	tvořit	k5eAaImAgInP	tvořit
týdenní	týdenní	k2eAgInPc1d1	týdenní
a	a	k8xC	a
ne	ne	k9	ne
celoroční	celoroční	k2eAgInSc4d1	celoroční
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Neděle	neděle	k1gFnSc1	neděle
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
epištoly	epištola	k1gFnSc2	epištola
Barnabáše	Barnabáš	k1gMnSc2	Barnabáš
(	(	kIx(	(
<g/>
xv	xv	k?	xv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
začátek	začátek	k1gInSc4	začátek
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
pisatel	pisatel	k1gMnSc1	pisatel
dále	daleko	k6eAd2	daleko
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pročež	pročež	k6eAd1	pročež
také	také	k9	také
my	my	k3xPp1nPc1	my
držíme	držet	k5eAaImIp1nP	držet
osmý	osmý	k4xOgInSc4	osmý
den	den	k1gInSc4	den
pro	pro	k7c4	pro
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
vstal	vstát	k5eAaPmAgMnS	vstát
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Didaché	Didachý	k2eAgFnPc1d1	Didachý
(	(	kIx(	(
<g/>
viii	vii	k1gFnPc1	vii
<g/>
)	)	kIx)	)
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neslavte	slavit	k5eNaImRp2nP	slavit
svůj	svůj	k3xOyFgInSc4	svůj
svátek	svátek	k1gInSc4	svátek
s	s	k7c7	s
pokrytci	pokrytec	k1gMnPc7	pokrytec
<g/>
;	;	kIx,	;
oni	onen	k3xDgMnPc1	onen
slaví	slavit	k5eAaImIp3nP	slavit
druhý	druhý	k4xOgInSc1	druhý
[	[	kIx(	[
<g/>
Pondělí	pondělí	k1gNnSc1	pondělí
<g/>
]	]	kIx)	]
a	a	k8xC	a
pátý	pátý	k4xOgInSc4	pátý
[	[	kIx(	[
<g/>
Čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
]	]	kIx)	]
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vy	vy	k3xPp2nPc1	vy
slavte	slavit	k5eAaImRp2nP	slavit
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
den	den	k1gInSc1	den
[	[	kIx(	[
<g/>
Středa	středa	k1gFnSc1	středa
<g/>
]	]	kIx)	]
a	a	k8xC	a
den	den	k1gInSc4	den
přípravy	příprava	k1gFnSc2	příprava
[	[	kIx(	[
<g/>
Pátek	Pátek	k1gMnSc1	Pátek
<g/>
]	]	kIx)	]
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
c.	c.	k?	c.
<g/>
xiv	xiv	k?	xiv
jsme	být	k5eAaImIp1nP	být
seznámeni	seznámen	k2eAgMnPc1d1	seznámen
"	"	kIx"	"
<g/>
A	a	k9	a
v	v	k7c4	v
den	den	k1gInSc4	den
Páně	páně	k2eAgInSc4d1	páně
se	se	k3xPyFc4	se
shromažďujte	shromažďovat	k5eAaImRp2nP	shromažďovat
<g/>
,	,	kIx,	,
lámejte	lámat	k5eAaImRp2nP	lámat
chléb	chléb	k1gInSc4	chléb
a	a	k8xC	a
vzdávejte	vzdávat	k5eAaImRp2nP	vzdávat
dík	dík	k1gInSc4	dík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
jasně	jasně	k6eAd1	jasně
to	ten	k3xDgNnSc1	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
řeči	řeč	k1gFnSc2	řeč
Tertulliana	Tertulliana	k1gFnSc1	Tertulliana
<g/>
,	,	kIx,	,
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
konstituce	konstituce	k1gFnSc2	konstituce
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
raných	raný	k2eAgMnPc2d1	raný
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
že	že	k8xS	že
sobota	sobota	k1gFnSc1	sobota
každého	každý	k3xTgInSc2	každý
týdne	týden	k1gInSc2	týden
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
připomínku	připomínka	k1gFnSc4	připomínka
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
Krista	Krista	k1gFnSc1	Krista
<g/>
,	,	kIx,	,
středa	středa	k1gFnSc1	středa
a	a	k8xC	a
pátek	pátek	k1gInSc4	pátek
pak	pak	k6eAd1	pak
za	za	k7c4	za
dny	den	k1gInPc4	den
zrady	zrada	k1gFnSc2	zrada
a	a	k8xC	a
ukřižování	ukřižování	k1gNnSc2	ukřižování
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
tato	tento	k3xDgFnSc1	tento
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
prvotní	prvotní	k2eAgFnSc1d1	prvotní
koncepce	koncepce	k1gFnSc1	koncepce
zaplňovala	zaplňovat	k5eAaImAgFnS	zaplňovat
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byly	být	k5eAaImAgInP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
svátky	svátek	k1gInPc1	svátek
vkládány	vkládán	k2eAgInPc1d1	vkládán
a	a	k8xC	a
násobeny	násoben	k2eAgInPc1d1	násoben
<g/>
,	,	kIx,	,
v	v	k7c6	v
každoročním	každoroční	k2eAgInSc6d1	každoroční
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
,	,	kIx,	,
týden	týden	k1gInSc4	týden
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
udržoval	udržovat	k5eAaImAgMnS	udržovat
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
zvláště	zvláště	k6eAd1	zvláště
poznat	poznat	k5eAaPmF	poznat
u	u	k7c2	u
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
v	v	k7c6	v
týdenním	týdenní	k2eAgNnSc6d1	týdenní
dělení	dělení	k1gNnSc6	dělení
žaltáře	žaltář	k1gInSc2	žaltář
<g/>
.	.	kIx.	.
</s>
<s>
Amalarius	Amalarius	k1gInSc1	Amalarius
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
zachoval	zachovat	k5eAaPmAgMnS	zachovat
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c6	o
uspořádání	uspořádání	k1gNnSc6	uspořádání
přijatém	přijatý	k2eAgNnSc6d1	přijaté
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
kapli	kaple	k1gFnSc6	kaple
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
roku	rok	k1gInSc2	rok
802	[number]	k4	802
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
byl	být	k5eAaImAgMnS	být
týdně	týdně	k6eAd1	týdně
přednesen	přednesen	k2eAgInSc4d1	přednesen
celý	celý	k2eAgInSc4d1	celý
žaltář	žaltář	k1gInSc4	žaltář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
širších	široký	k2eAgInPc6d2	širší
rysech	rys	k1gInPc6	rys
dělení	dělení	k1gNnSc2	dělení
byl	být	k5eAaImAgInS	být
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
teoretickým	teoretický	k2eAgInSc7d1	teoretický
vyhlášeným	vyhlášený	k2eAgInSc7d1	vyhlášený
římským	římský	k2eAgInSc7d1	římský
breviářem	breviář	k1gInSc7	breviář
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1911	[number]	k4	1911
vydána	vydán	k2eAgFnSc1d1	vydána
publikace	publikace	k1gFnSc1	publikace
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
konstituce	konstituce	k1gFnSc2	konstituce
"	"	kIx"	"
<g/>
Divine	Divin	k1gInSc5	Divin
afflatu	afflat	k1gInSc6	afflat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
Amalariovi	Amalarius	k1gMnSc6	Amalarius
<g/>
,	,	kIx,	,
že	že	k8xS	že
karolínské	karolínský	k2eAgNnSc1d1	karolínské
uspořádání	uspořádání	k1gNnSc1	uspořádání
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k8xS	jako
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
přijaté	přijatý	k2eAgNnSc1d1	přijaté
římskou	římska	k1gFnSc7	římska
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sv.	sv.	kA	sv.
Benedikt	Benedikt	k1gMnSc1	Benedikt
stanovil	stanovit	k5eAaPmAgInS	stanovit
jasný	jasný	k2eAgInSc4d1	jasný
princip	princip	k1gInSc4	princip
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
žaltář	žaltář	k1gInSc1	žaltář
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
přednesen	přednést	k5eAaPmNgInS	přednést
nejméně	málo	k6eAd3	málo
jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
;	;	kIx,	;
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
uspořádání	uspořádání	k1gNnSc1	uspořádání
přisuzováno	přisuzován	k2eAgNnSc1d1	přisuzováno
papeži	papež	k1gMnSc3	papež
sv.	sv.	kA	sv.
Damasusovi	Damasus	k1gMnSc3	Damasus
<g/>
.	.	kIx.	.
</s>
<s>
Požehnání	požehnání	k1gNnSc1	požehnání
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
dnů	den	k1gInPc2	den
týdne	týden	k1gInSc2	týden
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
subjekty	subjekt	k1gInPc7	subjekt
zbožnosti	zbožnost	k1gFnSc2	zbožnost
je	být	k5eAaImIp3nS	být
také	také	k9	také
oficiálně	oficiálně	k6eAd1	oficiálně
uznaná	uznaný	k2eAgFnSc1d1	uznaná
speciální	speciální	k2eAgFnSc1d1	speciální
mše	mše	k1gFnSc1	mše
požehnané	požehnaný	k2eAgFnSc2d1	požehnaná
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
pašijové	pašijový	k2eAgFnSc2d1	pašijová
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
během	během	k7c2	během
půstu	půst	k1gInSc2	půst
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc2	uspořádání
děkovných	děkovný	k2eAgFnPc2d1	děkovná
mší	mše	k1gFnPc2	mše
pro	pro	k7c4	pro
speciální	speciální	k2eAgInPc4d1	speciální
dny	den	k1gInPc4	den
týdne	týden	k1gInSc2	týden
schválené	schválený	k2eAgFnSc2d1	schválená
papežem	papež	k1gMnSc7	papež
Lvem	Lev	k1gMnSc7	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
druh	druh	k1gInSc4	druh
menšího	malý	k2eAgInSc2d2	menší
svátku	svátek	k1gInSc2	svátek
nebo	nebo	k8xC	nebo
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
den	den	k1gInSc1	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
týden	týden	k1gInSc1	týden
přesunul	přesunout	k5eAaPmAgInS	přesunout
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
Bede	Bed	k1gFnSc2	Bed
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hist.	Hist.	k1gMnSc1	Hist.
Eccl	Eccl	k1gMnSc1	Eccl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
breviář	breviář	k1gInSc1	breviář
odsouhlasený	odsouhlasený	k2eAgInSc1d1	odsouhlasený
po	po	k7c6	po
koncilu	koncil	k1gInSc6	koncil
v	v	k7c6	v
Tridentu	Trident	k1gMnSc6	Trident
povolil	povolit	k5eAaPmAgInS	povolit
jistý	jistý	k2eAgInSc4d1	jistý
bohoslužebný	bohoslužebný	k2eAgInSc4d1	bohoslužebný
přírůstek	přírůstek	k1gInSc4	přírůstek
ke	k	k7c3	k
mši	mše	k1gFnSc3	mše
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zádušní	zádušní	k2eAgFnSc4d1	zádušní
mši	mše	k1gFnSc4	mše
<g/>
,	,	kIx,	,
graduální	graduální	k2eAgInPc4d1	graduální
žalmy	žalm	k1gInPc4	žalm
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
přednášené	přednášený	k2eAgFnSc2d1	přednášená
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c4	v
adventní	adventní	k2eAgNnSc4d1	adventní
a	a	k8xC	a
postní	postní	k2eAgNnSc4d1	postní
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
.	.	kIx.	.
</s>
