<s>
Box	box	k1gInSc1
<g/>
,	,	kIx,
boxing	boxing	k1gInSc1
<g/>
,	,	kIx,
zastarale	zastarale	k6eAd1
rohování	rohování	k1gNnSc1
<g/>
,	,	kIx,
úpolový	úpolový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
,	,	kIx,
pěstní	pěstní	k2eAgInSc1d1
boj	boj	k1gInSc1
dvou	dva	k4xCgMnPc2
soupeřů	soupeř	k1gMnPc2
podobné	podobný	k2eAgFnSc2d1
hmotnostní	hmotnostní	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
<g/>
,	,	kIx,
snažících	snažící	k2eAgMnPc2d1
se	se	k3xPyFc4
pomocí	pomocí	k7c2
úderů	úder	k1gInPc2
pěstmi	pěst	k1gFnPc7
na	na	k7c4
pravidly	pravidlo	k1gNnPc7
povolenou	povolený	k2eAgFnSc4d1
část	část	k1gFnSc4
hlavy	hlava	k1gFnSc2
a	a	k8xC
těla	tělo	k1gNnSc2
zvítězit	zvítězit	k5eAaPmF
buď	buď	k8xC
získáním	získání	k1gNnSc7
bodové	bodový	k2eAgFnSc2d1
převahy	převaha	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
vítězství	vítězství	k1gNnSc4
k.o.	k.o.	k?
Boj	boj	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
ringu	ring	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
výhradně	výhradně	k6eAd1
ve	v	k7c4
stoje	stoj	k1gInPc4
<g/>
.	.	kIx.
</s>