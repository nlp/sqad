<s>
Box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
boxing	boxing	k1gInSc1	boxing
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
rohování	rohování	k1gNnSc1	rohování
<g/>
,	,	kIx,	,
úpolový	úpolový	k2eAgInSc1d1	úpolový
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
pěstní	pěstní	k2eAgInSc1d1	pěstní
boj	boj	k1gInSc1	boj
dvou	dva	k4xCgMnPc2	dva
soupeřů	soupeř	k1gMnPc2	soupeř
podobné	podobný	k2eAgFnSc2d1	podobná
hmotnostní	hmotnostní	k2eAgFnSc2d1	hmotnostní
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
snažících	snažící	k2eAgMnPc2d1	snažící
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
úderů	úder	k1gInPc2	úder
pěstmi	pěst	k1gFnPc7	pěst
na	na	k7c4	na
pravidly	pravidlo	k1gNnPc7	pravidlo
povolenou	povolený	k2eAgFnSc4d1	povolená
část	část	k1gFnSc4	část
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
zvítězit	zvítězit	k5eAaPmF	zvítězit
buď	buď	k8xC	buď
získáním	získání	k1gNnSc7	získání
bodové	bodový	k2eAgFnSc2d1	bodová
převahy	převaha	k1gFnSc2	převaha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vítězství	vítězství	k1gNnSc4	vítězství
k.o.	k.o.	k?	k.o.
Boj	boj	k1gInSc1	boj
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
borec	borec	k1gMnSc1	borec
po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
úderu	úder	k1gInSc6	úder
dotkne	dotknout	k5eAaPmIp3nS	dotknout
podlahy	podlaha	k1gFnSc2	podlaha
jinou	jiný	k2eAgFnSc7d1	jiná
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
než	než	k8xS	než
chodily	chodit	k5eAaImAgFnP	chodit
nebo	nebo	k8xC	nebo
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
provazech	provaz	k1gInPc6	provaz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
rozhodčím	rozhodčí	k1gMnSc7	rozhodčí
odpočítáván	odpočítáván	k2eAgMnSc1d1	odpočítáván
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
pouze	pouze	k6eAd1	pouze
údery	úder	k1gInPc4	úder
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
blokovány	blokován	k2eAgInPc1d1	blokován
nebo	nebo	k8xC	nebo
zachyceny	zachycen	k2eAgInPc1d1	zachycen
soupeřem	soupeř	k1gMnSc7	soupeř
a	a	k8xC	a
dopadnou	dopadnout	k5eAaPmIp3nP	dopadnout
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
švihem	švih	k1gInSc7	švih
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kloubovou	kloubový	k2eAgFnSc7d1	kloubová
částí	část	k1gFnSc7	část
sevřené	sevřený	k2eAgFnSc2d1	sevřená
rukavice	rukavice	k1gFnSc2	rukavice
<g/>
,	,	kIx,	,
na	na	k7c4	na
přední	přední	k2eAgFnSc4d1	přední
nebo	nebo	k8xC	nebo
postranní	postranní	k2eAgFnSc4d1	postranní
část	část	k1gFnSc4	část
hlavy	hlava	k1gFnSc2	hlava
nebo	nebo	k8xC	nebo
těla	tělo	k1gNnSc2	tělo
od	od	k7c2	od
pasu	pas	k1gInSc2	pas
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
osvícenství	osvícenství	k1gNnSc2	osvícenství
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
počátkem	počátkem	k7c2	počátkem
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
přichází	přicházet	k5eAaImIp3nS	přicházet
mistr	mistr	k1gMnSc1	mistr
šermu	šerm	k1gInSc2	šerm
Angličan	Angličan	k1gMnSc1	Angličan
James	James	k1gMnSc1	James
Figg	Figg	k1gMnSc1	Figg
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
šermovat	šermovat	k5eAaImF	šermovat
holýma	holý	k2eAgFnPc7d1	holá
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1719	[number]	k4	1719
zakládá	zakládat	k5eAaImIp3nS	zakládat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
první	první	k4xOgFnSc4	první
školu	škola	k1gFnSc4	škola
ušlechtilého	ušlechtilý	k2eAgInSc2d1	ušlechtilý
způsobu	způsob	k1gInSc2	způsob
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
záhy	záhy	k6eAd1	záhy
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
zájem	zájem	k1gInSc1	zájem
obyvatel	obyvatel	k1gMnPc2	obyvatel
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Figgův	Figgův	k2eAgInSc1d1	Figgův
způsob	způsob	k1gInSc1	způsob
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
pugilism	pugilism	k1gInSc1	pugilism
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
současných	současný	k2eAgNnPc2d1	současné
pravidel	pravidlo	k1gNnPc2	pravidlo
boxu	box	k1gInSc2	box
<g/>
.	.	kIx.	.
</s>
<s>
Obsahoval	obsahovat	k5eAaImAgInS	obsahovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
zápasnických	zápasnický	k2eAgInPc2d1	zápasnický
chvatů	chvat	k1gInPc2	chvat
a	a	k8xC	a
kopů	kop	k1gInPc2	kop
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prvním	první	k4xOgInSc7	první
výrazným	výrazný	k2eAgInSc7d1	výrazný
posunem	posun	k1gInSc7	posun
k	k	k7c3	k
novodobému	novodobý	k2eAgInSc3d1	novodobý
boxu	box	k1gInSc3	box
přišel	přijít	k5eAaPmAgMnS	přijít
Figgův	Figgův	k2eAgMnSc1d1	Figgův
žák	žák	k1gMnSc1	žák
Jack	Jack	k1gMnSc1	Jack
Broughton	Broughton	k1gInSc1	Broughton
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc1	jeho
pravidla	pravidlo	k1gNnPc1	pravidlo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1743	[number]	k4	1743
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
nových	nový	k2eAgNnPc2d1	nové
pravidel	pravidlo	k1gNnPc2	pravidlo
bylo	být	k5eAaImAgNnS	být
snížit	snížit	k5eAaPmF	snížit
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc4d1	vysoký
počet	počet	k1gInSc4	počet
zmrzačení	zmrzačení	k1gNnPc2	zmrzačení
či	či	k8xC	či
úmrtí	úmrtí	k1gNnPc2	úmrtí
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Broughton	Broughton	k1gInSc4	Broughton
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
pravidlech	pravidlo	k1gNnPc6	pravidlo
poprvé	poprvé	k6eAd1	poprvé
vymezil	vymezit	k5eAaPmAgInS	vymezit
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
souboje	souboj	k1gInPc4	souboj
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc4d1	dnešní
ring	ring	k1gInSc4	ring
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
jen	jen	k6eAd1	jen
křídou	křída	k1gFnSc7	křída
namalovaná	namalovaný	k2eAgFnSc1d1	namalovaná
čára	čára	k1gFnSc1	čára
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
začal	začít	k5eAaPmAgInS	začít
řídit	řídit	k5eAaImF	řídit
nestranný	nestranný	k2eAgMnSc1d1	nestranný
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Boxer	boxer	k1gMnSc1	boxer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
upadl	upadnout	k5eAaPmAgMnS	upadnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
třicet	třicet	k4xCc4	třicet
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
i	i	k9	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
(	(	kIx(	(
<g/>
pomocníků	pomocník	k1gMnPc2	pomocník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přátelé	přítel	k1gMnPc1	přítel
boxera	boxer	k1gMnSc4	boxer
měli	mít	k5eAaImAgMnP	mít
právo	právo	k1gNnSc4	právo
ukončit	ukončit	k5eAaPmF	ukončit
zápas	zápas	k1gInSc4	zápas
hozením	hození	k1gNnSc7	hození
houby	houba	k1gFnSc2	houba
(	(	kIx(	(
<g/>
ručníku	ručník	k1gInSc2	ručník
<g/>
)	)	kIx)	)
do	do	k7c2	do
ringu	ring	k1gInSc2	ring
<g/>
.	.	kIx.	.
</s>
<s>
Zakázalo	zakázat	k5eAaPmAgNnS	zakázat
se	se	k3xPyFc4	se
útočit	útočit	k5eAaImF	útočit
od	od	k7c2	od
pasu	pas	k1gInSc2	pas
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Bojovalo	bojovat	k5eAaImAgNnS	bojovat
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
pěstmi	pěst	k1gFnPc7	pěst
bez	bez	k7c2	bez
pomocí	pomoc	k1gFnPc2	pomoc
rukavic	rukavice	k1gFnPc2	rukavice
tzv.	tzv.	kA	tzv.
bare-knuckle	barenuckle	k6eAd1	bare-knuckle
pugilism	pugilism	k6eAd1	pugilism
<g/>
.	.	kIx.	.
</s>
<s>
Rukavice	rukavice	k1gFnSc1	rukavice
"	"	kIx"	"
<g/>
mufflers	mufflers	k1gInSc1	mufflers
<g/>
"	"	kIx"	"
Broughton	Broughton	k1gInSc1	Broughton
znal	znát	k5eAaImAgInS	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používaly	používat	k5eAaImAgFnP	používat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
k	k	k7c3	k
výuce	výuka	k1gFnSc3	výuka
žáků	žák	k1gMnPc2	žák
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Rukavice	rukavice	k1gFnPc1	rukavice
přišly	přijít	k5eAaPmAgFnP	přijít
do	do	k7c2	do
módy	móda	k1gFnSc2	móda
až	až	k9	až
po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
viktoriánské	viktoriánský	k2eAgFnSc6d1	viktoriánská
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Broughtonova	Broughtonův	k2eAgNnPc1d1	Broughtonův
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
s	s	k7c7	s
mírnými	mírný	k2eAgFnPc7d1	mírná
odchylkami	odchylka	k1gFnPc7	odchylka
bezmála	bezmála	k6eAd1	bezmála
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
éra	éra	k1gFnSc1	éra
pugilismu	pugilismus	k1gInSc2	pugilismus
šla	jít	k5eAaImAgFnS	jít
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
revolucí	revoluce	k1gFnSc7	revoluce
a	a	k8xC	a
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
regentství	regentství	k1gNnSc2	regentství
Jiřího	Jiří	k1gMnSc2	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
počátkem	počátkem	k7c2	počátkem
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
pugilismu	pugilismus	k1gInSc2	pugilismus
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
turnaje	turnaj	k1gInPc4	turnaj
zvané	zvaný	k2eAgInPc4d1	zvaný
prizefighting	prizefighting	k1gInSc4	prizefighting
<g/>
.	.	kIx.	.
</s>
<s>
Boxeři	boxer	k1gMnPc1	boxer
(	(	kIx(	(
<g/>
pugilisti	pugilist	k1gMnPc1	pugilist
<g/>
)	)	kIx)	)
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
těšili	těšit	k5eAaImAgMnP	těšit
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
popularity	popularita	k1gFnPc4	popularita
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třídách	třída	k1gFnPc6	třída
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc4	jméno
Daniel	Daniela	k1gFnPc2	Daniela
Mendoza	Mendoza	k1gFnSc1	Mendoza
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Humphries	Humphries	k1gMnSc1	Humphries
(	(	kIx(	(
<g/>
zvěčněn	zvěčněn	k2eAgMnSc1d1	zvěčněn
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Johna	John	k1gMnSc2	John
Hoppnera	Hoppner	k1gMnSc2	Hoppner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Gully	Gulla	k1gFnSc2	Gulla
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Cribb	Cribb	k1gMnSc1	Cribb
nebo	nebo	k8xC	nebo
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
boxerů	boxer	k1gMnPc2	boxer
tmavé	tmavý	k2eAgFnSc2d1	tmavá
pleti	pleť	k1gFnSc2	pleť
Tom	Tom	k1gMnSc1	Tom
Molyneaux	Molyneaux	k1gInSc1	Molyneaux
vyprodávala	vyprodávat	k5eAaImAgFnS	vyprodávat
ochozy	ochoz	k1gInPc4	ochoz
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vešlo	vejít	k5eAaPmAgNnS	vejít
i	i	k9	i
dvacet	dvacet	k4xCc1	dvacet
tisíc	tisíc	k4xCgInSc1	tisíc
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
zůstával	zůstávat	k5eAaImAgInS	zůstávat
pouze	pouze	k6eAd1	pouze
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnPc2	úmrtí
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
dále	daleko	k6eAd2	daleko
snížit	snížit	k5eAaPmF	snížit
počet	počet	k1gInSc4	počet
úmrtí	úmrtí	k1gNnPc2	úmrtí
a	a	k8xC	a
vážných	vážný	k2eAgNnPc2d1	vážné
zranění	zranění	k1gNnPc2	zranění
sepsáním	sepsání	k1gNnSc7	sepsání
prvních	první	k4xOgNnPc2	první
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
pravidel	pravidlo	k1gNnPc2	pravidlo
"	"	kIx"	"
<g/>
London	London	k1gMnSc1	London
Prize	Prize	k1gFnSc2	Prize
Ring	ring	k1gInSc1	ring
Rules	Rules	k1gInSc1	Rules
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nová	Nová	k1gFnSc1	Nová
pravidla	pravidlo	k1gNnSc2	pravidlo
stanovila	stanovit	k5eAaPmAgFnS	stanovit
velikost	velikost	k1gFnSc1	velikost
ringu	ring	k1gInSc2	ring
na	na	k7c4	na
730	[number]	k4	730
<g/>
cm	cm	kA	cm
(	(	kIx(	(
<g/>
24	[number]	k4	24
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
veliký	veliký	k2eAgInSc1d1	veliký
čtverec	čtverec	k1gInSc1	čtverec
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
provazů	provaz	k1gInPc2	provaz
91	[number]	k4	91
<g/>
cm	cm	kA	cm
(	(	kIx(	(
<g/>
3	[number]	k4	3
stopy	stopa	k1gFnPc1	stopa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provazy	provaz	k1gInPc1	provaz
měly	mít	k5eAaImAgInP	mít
bránit	bránit	k5eAaImF	bránit
taktickým	taktický	k2eAgInPc3d1	taktický
únikům	únik	k1gInPc3	únik
mimo	mimo	k7c4	mimo
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgNnP	být
zpřísněna	zpřísnit	k5eAaPmNgNnP	zpřísnit
pravidla	pravidlo	k1gNnPc1	pravidlo
o	o	k7c6	o
nedovolených	dovolený	k2eNgInPc6d1	nedovolený
úderech	úder	k1gInPc6	úder
a	a	k8xC	a
faulech	faul	k1gInPc6	faul
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pugilismu	pugilismus	k1gInSc2	pugilismus
tak	tak	k6eAd1	tak
prakticky	prakticky	k6eAd1	prakticky
vymizely	vymizet	k5eAaPmAgInP	vymizet
zápasnické	zápasnický	k2eAgInPc1d1	zápasnický
chvaty	chvat	k1gInPc1	chvat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
kousání	kousání	k1gNnSc1	kousání
<g/>
,	,	kIx,	,
škrábání	škrábání	k1gNnSc1	škrábání
<g/>
,	,	kIx,	,
zatlačování	zatlačování	k1gNnSc1	zatlačování
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
kopání	kopání	k1gNnSc1	kopání
či	či	k8xC	či
boxování	boxování	k1gNnSc1	boxování
s	s	k7c7	s
kameny	kámen	k1gInPc7	kámen
v	v	k7c6	v
dlaních	dlaň	k1gFnPc6	dlaň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poslání	poslání	k1gNnSc6	poslání
soupeře	soupeř	k1gMnSc2	soupeř
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
bylo	být	k5eAaImAgNnS	být
kolo	kolo	k1gNnSc1	kolo
ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
a	a	k8xC	a
po	po	k7c6	po
třicetisekundové	třicetisekundový	k2eAgFnSc6d1	třicetisekundová
přestávce	přestávka	k1gFnSc6	přestávka
měl	mít	k5eAaImAgInS	mít
knockoutovaný	knockoutovaný	k2eAgInSc1d1	knockoutovaný
boxer	boxer	k1gInSc1	boxer
dalších	další	k2eAgFnPc2d1	další
osm	osm	k4xCc1	osm
sekund	sekunda	k1gFnPc2	sekunda
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
cizí	cizí	k2eAgFnSc2d1	cizí
pomoci	pomoc	k1gFnSc2	pomoc
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
migraci	migrace	k1gFnSc3	migrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
měst	město	k1gNnPc2	město
a	a	k8xC	a
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
chudinských	chudinský	k2eAgFnPc2d1	chudinská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
radikální	radikální	k2eAgFnPc4d1	radikální
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
úpadku	úpadek	k1gInSc3	úpadek
pugilismu	pugilismus	k1gInSc2	pugilismus
<g/>
.	.	kIx.	.
</s>
<s>
Prizefighting	Prizefighting	k1gInSc1	Prizefighting
klesl	klesnout	k5eAaPmAgInS	klesnout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
zápasu	zápas	k1gInSc2	zápas
s	s	k7c7	s
bojovými	bojový	k2eAgNnPc7d1	bojové
plemeny	plemeno	k1gNnPc7	plemeno
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
kohoutích	kohoutí	k2eAgMnPc2d1	kohoutí
zápasů	zápas	k1gInPc2	zápas
apod.	apod.	kA	apod.
Kolem	kolem	k7c2	kolem
zápasů	zápas	k1gInPc2	zápas
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
spodina	spodina	k1gFnSc1	spodina
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zkorumpovaní	zkorumpovaný	k2eAgMnPc1d1	zkorumpovaný
boxeři	boxer	k1gMnPc1	boxer
prodávaly	prodávat	k5eAaImAgInP	prodávat
své	svůj	k3xOyFgNnSc1	svůj
zápasy	zápas	k1gInPc1	zápas
gamblerům	gambler	k1gMnPc3	gambler
<g/>
.	.	kIx.	.
</s>
<s>
Kapitolou	kapitola	k1gFnSc7	kapitola
samu	sám	k3xTgFnSc4	sám
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
byli	být	k5eAaImAgMnP	být
fanoušci	fanoušek	k1gMnPc1	fanoušek
boxerů	boxer	k1gMnPc2	boxer
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nezadali	zadat	k5eNaPmAgMnP	zadat
se	s	k7c7	s
současnými	současný	k2eAgMnPc7d1	současný
rowdies	rowdies	k1gMnSc1	rowdies
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
je	být	k5eAaImIp3nS	být
odmítala	odmítat	k5eAaImAgFnS	odmítat
přepravovat	přepravovat	k5eAaImF	přepravovat
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
bez	bez	k7c2	bez
policejního	policejní	k2eAgInSc2d1	policejní
dohledu	dohled	k1gInSc2	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prizefighting	prizefighting	k1gInSc4	prizefighting
přestávali	přestávat	k5eAaImAgMnP	přestávat
chodit	chodit	k5eAaImF	chodit
lidé	člověk	k1gMnPc1	člověk
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Pěstní	pěstní	k2eAgInPc1d1	pěstní
souboje	souboj	k1gInPc1	souboj
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
zakazovány	zakazovat	k5eAaImNgInP	zakazovat
kvůli	kvůli	k7c3	kvůli
vysokému	vysoký	k2eAgInSc3d1	vysoký
nárůstu	nárůst	k1gInSc3	nárůst
kriminálních	kriminální	k2eAgFnPc2d1	kriminální
činností	činnost	k1gFnPc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dostává	dostávat	k5eAaImIp3nS	dostávat
boxování	boxování	k1gNnSc4	boxování
v	v	k7c6	v
rukavicích	rukavice	k1gFnPc6	rukavice
<g/>
.	.	kIx.	.
</s>
<s>
Boxování	boxování	k1gNnSc1	boxování
v	v	k7c6	v
rukavicích	rukavice	k1gFnPc6	rukavice
získalo	získat	k5eAaPmAgNnS	získat
živnou	živný	k2eAgFnSc4d1	živná
půdu	půda	k1gFnSc4	půda
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
stanech	stan	k1gInPc6	stan
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
booths	booths	k6eAd1	booths
<g/>
)	)	kIx)	)
na	na	k7c6	na
poutích	pouť	k1gFnPc6	pouť
v	v	k7c6	v
době	doba	k1gFnSc6	doba
úpadku	úpadek	k1gInSc2	úpadek
boxování	boxování	k1gNnSc2	boxování
pěstmi	pěst	k1gFnPc7	pěst
<g/>
.	.	kIx.	.
</s>
<s>
Boxerům	boxer	k1gMnPc3	boxer
v	v	k7c6	v
rukavicích	rukavice	k1gFnPc6	rukavice
se	se	k3xPyFc4	se
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
"	"	kIx"	"
<g/>
booth	booth	k1gInSc1	booth
fighters	fighters	k1gInSc1	fighters
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
Queensberryho	Queensberry	k1gMnSc2	Queensberry
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rukavice	rukavice	k1gFnPc1	rukavice
stávají	stávat	k5eAaImIp3nP	stávat
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
výbavy	výbava	k1gFnSc2	výbava
boxera	boxer	k1gMnSc2	boxer
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
novinek	novinka	k1gFnPc2	novinka
bylo	být	k5eAaImAgNnS	být
pravidlo	pravidlo	k1gNnSc1	pravidlo
o	o	k7c4	o
stanovení	stanovení	k1gNnSc4	stanovení
délky	délka	k1gFnSc2	délka
hrací	hrací	k2eAgFnSc2d1	hrací
doby	doba	k1gFnSc2	doba
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
a	a	k8xC	a
minutovou	minutový	k2eAgFnSc7d1	minutová
přestávkou	přestávka	k1gFnSc7	přestávka
mezi	mezi	k7c7	mezi
koly	kolo	k1gNnPc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
však	však	k9	však
bojovalo	bojovat	k5eAaImAgNnS	bojovat
do	do	k7c2	do
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
na	na	k7c4	na
neomezený	omezený	k2eNgInSc4d1	neomezený
počet	počet	k1gInSc4	počet
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
známý	známý	k2eAgInSc1d1	známý
souboj	souboj	k1gInSc1	souboj
trval	trvat	k5eAaImAgInS	trvat
106	[number]	k4	106
kol	kolo	k1gNnPc2	kolo
(	(	kIx(	(
<g/>
cca	cca	kA	cca
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
významného	významný	k2eAgMnSc2d1	významný
propagátora	propagátor	k1gMnSc2	propagátor
boxování	boxování	k1gNnSc2	boxování
v	v	k7c6	v
rukavicích	rukavice	k1gFnPc6	rukavice
byl	být	k5eAaImAgInS	být
celosvětově	celosvětově	k6eAd1	celosvětově
proslulý	proslulý	k2eAgInSc1d1	proslulý
Jem	Jem	k?	Jem
Mace	Mace	k1gInSc1	Mace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
společnosti	společnost	k1gFnSc2	společnost
zůstával	zůstávat	k5eAaImAgInS	zůstávat
box	box	k1gInSc1	box
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
devatenástého	devatenástý	k2eAgNnSc2d1	devatenástý
století	století	k1gNnSc2	století
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
boxu	box	k1gInSc2	box
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
studentů	student	k1gMnPc2	student
středních	střední	k2eAgFnPc2d1	střední
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
nočním	noční	k2eAgInSc6d1	noční
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
sázení	sázení	k1gNnSc6	sázení
<g/>
,	,	kIx,	,
alkoholismu	alkoholismus	k1gInSc6	alkoholismus
šla	jít	k5eAaImAgFnS	jít
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
boxerskými	boxerský	k2eAgInPc7d1	boxerský
turnaji	turnaj	k1gInPc7	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Slavní	slavný	k2eAgMnPc1d1	slavný
boxeři	boxer	k1gMnPc1	boxer
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c4	na
propagaci	propagace	k1gFnSc4	propagace
tabákového	tabákový	k2eAgNnSc2d1	tabákové
a	a	k8xC	a
alkoholického	alkoholický	k2eAgNnSc2d1	alkoholické
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
marketingové	marketingový	k2eAgFnPc1d1	marketingová
aktivity	aktivita	k1gFnPc1	aktivita
alkoholických	alkoholický	k2eAgFnPc2d1	alkoholická
<g/>
/	/	kIx~	/
<g/>
tabákových	tabákový	k2eAgFnPc2d1	tabáková
společností	společnost	k1gFnPc2	společnost
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
narážely	narážet	k5eAaImAgFnP	narážet
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
(	(	kIx(	(
<g/>
prohibice	prohibice	k1gFnSc1	prohibice
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
až	až	k9	až
k	k	k7c3	k
úplnému	úplný	k2eAgInSc3d1	úplný
zákazu	zákaz	k1gInSc3	zákaz
prodeje	prodej	k1gInSc2	prodej
alkoholických	alkoholický	k2eAgMnPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
reklam	reklama	k1gFnPc2	reklama
na	na	k7c4	na
alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
a	a	k8xC	a
tabákové	tabákový	k2eAgInPc4d1	tabákový
výrobky	výrobek	k1gInPc4	výrobek
poklesly	poklesnout	k5eAaPmAgInP	poklesnout
peníze	peníz	k1gInPc1	peníz
tekoucí	tekoucí	k2eAgInPc1d1	tekoucí
do	do	k7c2	do
boxu	box	k1gInSc2	box
a	a	k8xC	a
box	box	k1gInSc4	box
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
radikální	radikální	k2eAgFnPc4d1	radikální
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Změn	změna	k1gFnPc2	změna
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
vytvořením	vytvoření	k1gNnSc7	vytvoření
regulačních	regulační	k2eAgInPc2d1	regulační
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
"	"	kIx"	"
<g/>
National	National	k1gFnSc1	National
Sporting	Sporting	k1gInSc1	Sporting
Club	club	k1gInSc1	club
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
se	se	k3xPyFc4	se
bodování	bodování	k1gNnSc4	bodování
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Omezuje	omezovat	k5eAaImIp3nS	omezovat
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
kol	kola	k1gFnPc2	kola
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zavedeny	zaveden	k2eAgFnPc4d1	zavedena
váhové	váhový	k2eAgFnPc4d1	váhová
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
váhových	váhový	k2eAgFnPc2d1	váhová
kategorií	kategorie	k1gFnPc2	kategorie
znamená	znamenat	k5eAaImIp3nS	znamenat
více	hodně	k6eAd2	hodně
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
více	hodně	k6eAd2	hodně
prodaných	prodaný	k2eAgInPc2d1	prodaný
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
regulačního	regulační	k2eAgInSc2d1	regulační
orgánu	orgán	k1gInSc2	orgán
do	do	k7c2	do
boxu	box	k1gInSc2	box
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c4	na
amatérskou	amatérský	k2eAgFnSc4d1	amatérská
a	a	k8xC	a
profesionální	profesionální	k2eAgFnSc4d1	profesionální
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Amatérský	amatérský	k2eAgInSc1d1	amatérský
box	box	k1gInSc1	box
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
profesionálního	profesionální	k2eAgInSc2d1	profesionální
boxu	box	k1gInSc2	box
ohrazoval	ohrazovat	k5eAaImAgMnS	ohrazovat
vůči	vůči	k7c3	vůči
sázení	sázení	k1gNnSc3	sázení
<g/>
.	.	kIx.	.
</s>
<s>
Promotéři	promotér	k1gMnPc1	promotér
turnajů	turnaj	k1gInPc2	turnaj
dělali	dělat	k5eAaImAgMnP	dělat
maximum	maximum	k1gNnSc4	maximum
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
boxerů	boxer	k1gMnPc2	boxer
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
amatérským	amatérský	k2eAgInSc7d1	amatérský
a	a	k8xC	a
profesionálním	profesionální	k2eAgInSc7d1	profesionální
boxem	box	k1gInSc7	box
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
amatérském	amatérský	k2eAgInSc6d1	amatérský
boxu	box	k1gInSc6	box
není	být	k5eNaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
vyhrát	vyhrát	k5eAaPmF	vyhrát
před	před	k7c7	před
časovým	časový	k2eAgInSc7d1	časový
limitem	limit	k1gInSc7	limit
na	na	k7c6	na
k.o.	k.o.	k?	k.o.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cílem	cíl	k1gInSc7	cíl
úderu	úder	k1gInSc2	úder
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
správně	správně	k6eAd1	správně
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
soupeře	soupeř	k1gMnPc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
amatérského	amatérský	k2eAgInSc2d1	amatérský
boxu	box	k1gInSc2	box
je	být	k5eAaImIp3nS	být
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c4	na
body	bod	k1gInPc4	bod
(	(	kIx(	(
<g/>
za	za	k7c4	za
technickou	technický	k2eAgFnSc4d1	technická
převahu	převaha	k1gFnSc4	převaha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
předvedení	předvedení	k1gNnSc1	předvedení
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
taktiky	taktika	k1gFnSc2	taktika
<g/>
,	,	kIx,	,
čistoty	čistota	k1gFnSc2	čistota
boje	boj	k1gInSc2	boj
a	a	k8xC	a
správného	správný	k2eAgNnSc2d1	správné
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Amatérský	amatérský	k2eAgInSc4d1	amatérský
box	box	k1gInSc4	box
měl	mít	k5eAaImAgMnS	mít
svá	svůj	k3xOyFgNnPc4	svůj
specifika	specifikon	k1gNnPc4	specifikon
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
v	v	k7c6	v
členských	členský	k2eAgInPc6d1	členský
klubech	klub	k1gInPc6	klub
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
propagátorům	propagátor	k1gMnPc3	propagátor
amatérského	amatérský	k2eAgInSc2d1	amatérský
boxu	box	k1gInSc2	box
patřili	patřit	k5eAaImAgMnP	patřit
náboženské	náboženský	k2eAgInPc4d1	náboženský
spolky	spolek	k1gInPc4	spolek
-	-	kIx~	-
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
(	(	kIx(	(
<g/>
YMCA	YMCA	kA	YMCA
<g/>
,	,	kIx,	,
CYO	CYO	kA	CYO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židovské	židovský	k2eAgNnSc4d1	Židovské
(	(	kIx(	(
<g/>
Maccabi	Maccabi	k1gNnSc4	Maccabi
<g/>
)	)	kIx)	)
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
propagátory	propagátor	k1gMnPc7	propagátor
amatérského	amatérský	k2eAgInSc2d1	amatérský
boxu	box	k1gInSc2	box
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
sklepení	sklepení	k1gNnSc6	sklepení
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
postavený	postavený	k2eAgInSc4d1	postavený
boxerský	boxerský	k2eAgInSc4d1	boxerský
ring	ring	k1gInSc4	ring
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
zásluhou	zásluha	k1gFnSc7	zásluha
se	se	k3xPyFc4	se
amatérský	amatérský	k2eAgInSc1d1	amatérský
box	box	k1gInSc1	box
objevil	objevit	k5eAaPmAgInS	objevit
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
programu	program	k1gInSc6	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
Atverpách	Atverpa	k1gFnPc6	Atverpa
během	během	k7c2	během
olympijkých	olympijký	k2eAgFnPc2d1	olympijký
her	hra	k1gFnPc2	hra
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
boxerská	boxerský	k2eAgFnSc1d1	boxerská
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FIBA	FIBA	kA	FIBA
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
AIBA	AIBA	kA	AIBA
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakládajícími	zakládající	k2eAgMnPc7d1	zakládající
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Kanada	Kanada	k1gFnSc1	Kanada
však	však	k9	však
záhy	záhy	k6eAd1	záhy
zakládají	zakládat	k5eAaImIp3nP	zakládat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
amatérskou	amatérský	k2eAgFnSc4d1	amatérská
sportovní	sportovní	k2eAgFnSc4d1	sportovní
organizaci	organizace	k1gFnSc4	organizace
při	při	k7c6	při
NCAA	NCAA	kA	NCAA
(	(	kIx(	(
<g/>
National	National	k1gMnSc5	National
Collegiate	Collegiat	k1gMnSc5	Collegiat
Athletic	Athletice	k1gFnPc2	Athletice
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
s	s	k7c7	s
odlišnými	odlišný	k2eAgNnPc7d1	odlišné
pravidly	pravidlo	k1gNnPc7	pravidlo
než	než	k8xS	než
FIBA	FIBA	kA	FIBA
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
soutěží	soutěž	k1gFnSc7	soutěž
NCAA	NCAA	kA	NCAA
je	být	k5eAaImIp3nS	být
turnaj	turnaj	k1gInSc1	turnaj
Golden	Goldna	k1gFnPc2	Goldna
Glove	Gloev	k1gFnSc2	Gloev
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
FIBA	FIBA	kA	FIBA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mecenášům	mecenáš	k1gMnPc3	mecenáš
amatérského	amatérský	k2eAgInSc2d1	amatérský
boxu	box	k1gInSc2	box
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
patřily	patřit	k5eAaImAgFnP	patřit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
náboženské	náboženský	k2eAgInPc4d1	náboženský
spolky	spolek	k1gInPc4	spolek
a	a	k8xC	a
především	především	k9	především
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
držely	držet	k5eAaImAgFnP	držet
amatérský	amatérský	k2eAgInSc4d1	amatérský
box	box	k1gInSc4	box
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
sledovanosti	sledovanost	k1gFnSc2	sledovanost
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
nastaly	nastat	k5eAaPmAgFnP	nastat
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Amerika	Amerika	k1gFnSc1	Amerika
objevuje	objevovat	k5eAaImIp3nS	objevovat
divácký	divácký	k2eAgInSc4d1	divácký
potenciál	potenciál	k1gInSc4	potenciál
sportů	sport	k1gInPc2	sport
jako	jako	k8xS	jako
basketbal	basketbal	k1gInSc4	basketbal
a	a	k8xC	a
volejbal	volejbal	k1gInSc4	volejbal
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ranou	rána	k1gFnSc7	rána
pro	pro	k7c4	pro
amatérský	amatérský	k2eAgInSc4d1	amatérský
box	box	k1gInSc4	box
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
popularizace	popularizace	k1gFnSc1	popularizace
asijských	asijský	k2eAgNnPc2d1	asijské
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
boxerský	boxerský	k2eAgInSc4d1	boxerský
výcvik	výcvik	k1gInSc4	výcvik
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
začínají	začínat	k5eAaImIp3nP	začínat
amatérskou	amatérský	k2eAgFnSc4d1	amatérská
boxerskou	boxerský	k2eAgFnSc4d1	boxerská
scénu	scéna	k1gFnSc4	scéna
ovládat	ovládat	k5eAaImF	ovládat
boxeři	boxer	k1gMnPc1	boxer
ze	z	k7c2	z
socialistických	socialistický	k2eAgFnPc2d1	socialistická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
úspěch	úspěch	k1gInSc4	úspěch
ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
nad	nad	k7c7	nad
rivaly	rival	k1gMnPc7	rival
z	z	k7c2	z
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
zemí	zem	k1gFnPc2	zem
znamená	znamenat	k5eAaImIp3nS	znamenat
prestiž	prestiž	k1gFnSc1	prestiž
(	(	kIx(	(
<g/>
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
váhových	váhový	k2eAgFnPc2d1	váhová
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
medailí	medaile	k1gFnPc2	medaile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dohánějí	dohánět	k5eAaImIp3nP	dohánět
slabší	slabý	k2eAgInSc4d2	slabší
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Výcvik	výcvik	k1gInSc1	výcvik
boxera	boxer	k1gMnSc2	boxer
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
fyzicky	fyzicky	k6eAd1	fyzicky
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mimo	mimo	k7c4	mimo
schopného	schopný	k2eAgMnSc4d1	schopný
trenéra	trenér	k1gMnSc4	trenér
a	a	k8xC	a
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
sparingu	sparing	k1gInSc2	sparing
je	být	k5eAaImIp3nS	být
výbava	výbava	k1gFnSc1	výbava
boxera	boxer	k1gMnSc2	boxer
finančně	finančně	k6eAd1	finančně
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc4d1	finanční
nenáročnost	nenáročnost	k1gFnSc4	nenáročnost
boxu	box	k1gInSc2	box
znamenal	znamenat	k5eAaImAgInS	znamenat
vysoký	vysoký	k2eAgInSc1d1	vysoký
nárůst	nárůst	k1gInSc1	nárůst
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
první	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
boxu	box	k1gInSc6	box
v	v	k7c6	v
kubánské	kubánský	k2eAgFnSc6d1	kubánská
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
amatérského	amatérský	k2eAgInSc2d1	amatérský
boxu	box	k1gInSc2	box
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
celosvětově	celosvětově	k6eAd1	celosvětově
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
migrací	migrace	k1gFnSc7	migrace
trenérů	trenér	k1gMnPc2	trenér
z	z	k7c2	z
postsovětských	postsovětský	k2eAgFnPc2d1	postsovětská
republik	republika	k1gFnPc2	republika
a	a	k8xC	a
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
prestiže	prestiž	k1gFnSc2	prestiž
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sportovních	sportovní	k2eAgInPc2d1	sportovní
úspěchů	úspěch	k1gInPc2	úspěch
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
zemí	zem	k1gFnPc2	zem
prioritou	priorita	k1gFnSc7	priorita
a	a	k8xC	a
tradiční	tradiční	k2eAgFnSc1d1	tradiční
velmoci	velmoc	k1gFnPc1	velmoc
jako	jako	k8xS	jako
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
mají	mít	k5eAaImIp3nP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
boxu	box	k1gInSc2	box
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
představili	představit	k5eAaPmAgMnP	představit
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počátky	počátek	k1gInPc4	počátek
orgánem	orgán	k1gInSc7	orgán
řízeného	řízený	k2eAgInSc2d1	řízený
profesionálního	profesionální	k2eAgInSc2d1	profesionální
boxu	box	k1gInSc2	box
je	být	k5eAaImIp3nS	být
založení	založení	k1gNnSc4	založení
privátního	privátní	k2eAgInSc2d1	privátní
klubu	klub	k1gInSc2	klub
"	"	kIx"	"
<g/>
National	National	k1gFnSc1	National
sportong	sportong	k1gMnSc1	sportong
club	club	k1gInSc1	club
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
NSC	NSC	kA	NSC
<g/>
)	)	kIx)	)
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
model	model	k1gInSc1	model
NSC	NSC	kA	NSC
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
standardy	standard	k1gInPc1	standard
byly	být	k5eAaImAgInP	být
přebíraný	přebíraný	k2eAgInSc1d1	přebíraný
dalšími	další	k2eAgInPc7d1	další
kluby	klub	k1gInPc7	klub
a	a	k8xC	a
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
britské	britský	k2eAgInPc1d1	britský
kluby	klub	k1gInPc1	klub
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
slučují	slučovat	k5eAaImIp3nP	slučovat
v	v	k7c6	v
"	"	kIx"	"
<g/>
British	Britisha	k1gFnPc2	Britisha
Boxing	boxing	k1gInSc1	boxing
Board	Board	k1gMnSc1	Board
of	of	k?	of
Control	Control	k1gInSc1	Control
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
BBBC	BBBC	kA	BBBC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
je	být	k5eAaImIp3nS	být
mekkou	mekká	k1gFnSc4	mekká
boxu	box	k1gInSc2	box
město	město	k1gNnSc1	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Yorská	yorský	k2eAgNnPc1d1	Yorské
pravidla	pravidlo	k1gNnPc1	pravidlo
boxu	box	k1gInSc2	box
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
britských	britský	k2eAgInPc2d1	britský
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
standardem	standard	k1gInSc7	standard
pro	pro	k7c4	pro
bezpočet	bezpočet	k1gInSc4	bezpočet
amerických	americký	k2eAgFnPc2d1	americká
boxerských	boxerský	k2eAgFnPc2d1	boxerská
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
významnou	významný	k2eAgFnSc7d1	významná
americkou	americký	k2eAgFnSc7d1	americká
boxerskou	boxerský	k2eAgFnSc7d1	boxerská
organizací	organizace	k1gFnSc7	organizace
(	(	kIx(	(
<g/>
komisí	komise	k1gFnSc7	komise
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
State	status	k1gInSc5	status
Athletic	Athletice	k1gFnPc2	Athletice
Commision	Commision	k1gInSc1	Commision
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
box	box	k1gInSc1	box
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Britského	britský	k2eAgMnSc2d1	britský
povoloval	povolovat	k5eAaImAgMnS	povolovat
dva	dva	k4xCgMnPc4	dva
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
<g/>
,	,	kIx,	,
vatou	vata	k1gFnSc7	vata
obalené	obalený	k2eAgInPc1d1	obalený
provazy	provaz	k1gInPc1	provaz
<g/>
,	,	kIx,	,
protiskluzová	protiskluzový	k2eAgFnSc1d1	protiskluzová
podlaha	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
rychlou	rychlý	k2eAgFnSc4d1	rychlá
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
pomoc	pomoc	k1gFnSc4	pomoc
atd.	atd.	kA	atd.
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgFnPc1	čtyři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
boxerské	boxerský	k2eAgFnPc1d1	boxerská
organizace	organizace	k1gFnPc1	organizace
-	-	kIx~	-
International	International	k1gFnSc1	International
Boxing	boxing	k1gInSc1	boxing
Federation	Federation	k1gInSc1	Federation
(	(	kIx(	(
<g/>
IBF	IBF	kA	IBF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
World	World	k1gInSc1	World
Boxing	boxing	k1gInSc1	boxing
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
WBA	WBA	kA	WBA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
World	World	k1gInSc1	World
Boxing	boxing	k1gInSc1	boxing
Council	Council	k1gInSc1	Council
(	(	kIx(	(
<g/>
WBC	WBC	kA	WBC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
World	World	k1gInSc1	World
Boxing	boxing	k1gInSc1	boxing
Organization	Organization	k1gInSc1	Organization
(	(	kIx(	(
<g/>
WBO	WBO	kA	WBO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
země	zem	k1gFnPc4	zem
mimo	mimo	k7c4	mimo
britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
první	první	k4xOgFnSc2	první
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
unie	unie	k1gFnSc2	unie
profesionálního	profesionální	k2eAgInSc2d1	profesionální
boxu	box	k1gInSc2	box
"	"	kIx"	"
<g/>
International	International	k1gFnSc1	International
Boxing	boxing	k1gInSc1	boxing
Union	union	k1gInSc1	union
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
IBU	IBU	kA	IBU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
IBU	IBU	kA	IBU
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
kontinent	kontinent	k1gInSc4	kontinent
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
transformuje	transformovat	k5eAaBmIp3nS	transformovat
na	na	k7c4	na
Evropskou	evropský	k2eAgFnSc4d1	Evropská
boxerskou	boxerský	k2eAgFnSc4d1	boxerská
unii	unie	k1gFnSc4	unie
(	(	kIx(	(
<g/>
EBU	EBU	kA	EBU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
řídícím	řídící	k2eAgInSc7d1	řídící
orgánem	orgán	k1gInSc7	orgán
evropského	evropský	k2eAgInSc2d1	evropský
profesionálního	profesionální	k2eAgInSc2d1	profesionální
boxu	box	k1gInSc2	box
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
EBU	EBU	kA	EBU
<g/>
,	,	kIx,	,
BBBC	BBBC	kA	BBBC
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
organizace	organizace	k1gFnPc1	organizace
utvářejí	utvářet	k5eAaImIp3nP	utvářet
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
World	Worlda	k1gFnPc2	Worlda
Boxing	boxing	k1gInSc1	boxing
Council	Council	k1gMnSc1	Council
(	(	kIx(	(
<g/>
WBC	WBC	kA	WBC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
boxu	box	k1gInSc2	box
rychle	rychle	k6eAd1	rychle
stoupala	stoupat	k5eAaImAgFnS	stoupat
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
rádia	rádio	k1gNnSc2	rádio
a	a	k8xC	a
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
časopisů	časopis	k1gInPc2	časopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boxerské	boxerský	k2eAgInPc1d1	boxerský
zápasy	zápas	k1gInPc1	zápas
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
prvními	první	k4xOgInPc7	první
akčními	akční	k2eAgInPc7d1	akční
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
boxerům	boxer	k1gMnPc3	boxer
kult	kult	k1gInSc4	kult
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
profesionální	profesionální	k2eAgInSc1d1	profesionální
boxer	boxer	k1gInSc1	boxer
byl	být	k5eAaImAgInS	být
bezesporu	bezesporu	k9	bezesporu
vynikající	vynikající	k2eAgMnSc1d1	vynikající
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
musel	muset	k5eAaImAgMnS	muset
umět	umět	k5eAaImF	umět
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
médii	médium	k1gNnPc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrazným	výrazný	k2eAgFnPc3d1	výrazná
postavám	postava	k1gFnPc3	postava
profesionální	profesionální	k2eAgFnSc2d1	profesionální
ringu	ring	k1gInSc2	ring
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
uměli	umět	k5eAaImAgMnP	umět
vytvořit	vytvořit	k5eAaPmF	vytvořit
jak	jak	k6eAd1	jak
sportovní	sportovní	k2eAgFnSc4d1	sportovní
tak	tak	k8xS	tak
mediální	mediální	k2eAgFnSc4d1	mediální
tvář	tvář	k1gFnSc4	tvář
patřili	patřit	k5eAaImAgMnP	patřit
Jack	Jack	k1gMnSc1	Jack
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Dempsey	Dempsea	k1gFnSc2	Dempsea
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Schmeling	Schmeling	k1gInSc1	Schmeling
<g/>
,	,	kIx,	,
Sonny	Sonna	k1gFnPc1	Sonna
Liston	Liston	k1gInSc1	Liston
<g/>
,	,	kIx,	,
Muhammad	Muhammad	k1gInSc1	Muhammad
Ali	Ali	k1gFnSc1	Ali
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnSc1	Mike
Tyson	Tyson	k1gMnSc1	Tyson
nebo	nebo	k8xC	nebo
Oscar	Oscar	k1gMnSc1	Oscar
de	de	k?	de
la	la	k1gNnSc4	la
Hoya	Hoy	k1gInSc2	Hoy
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
boxu	box	k1gInSc2	box
zásadně	zásadně	k6eAd1	zásadně
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
filmy	film	k1gInPc1	film
-	-	kIx~	-
The	The	k1gFnSc1	The
Champ	Champ	k1gMnSc1	Champ
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Champion	Champion	k1gInSc1	Champion
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Někdo	někdo	k3yInSc1	někdo
tam	tam	k6eAd1	tam
nahoře	nahoře	k6eAd1	nahoře
mě	já	k3xPp1nSc4	já
má	mít	k5eAaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nadmuté	nadmutý	k2eAgNnSc1d1	nadmuté
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rocky	rock	k1gInPc1	rock
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
box	box	k1gInSc4	box
především	především	k9	především
Million	Million	k1gInSc4	Million
Dollar	dollar	k1gInSc1	dollar
Baby	baby	k1gNnSc1	baby
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
filmů	film	k1gInPc2	film
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
Pěsti	pěst	k1gFnSc2	pěst
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
boxovaly	boxovat	k5eAaImAgFnP	boxovat
vedle	vedle	k7c2	vedle
mužů	muž	k1gMnPc2	muž
prakticky	prakticky	k6eAd1	prakticky
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
vzniku	vznik	k1gInSc2	vznik
moderního	moderní	k2eAgInSc2d1	moderní
boxu	box	k1gInSc2	box
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
zápasy	zápas	k1gInPc1	zápas
měly	mít	k5eAaImAgInP	mít
statut	statut	k1gInSc4	statut
exhibice	exhibice	k1gFnSc2	exhibice
k	k	k7c3	k
pobavení	pobavení	k1gNnSc3	pobavení
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgFnSc4d2	významnější
roli	role	k1gFnSc4	role
v	v	k7c6	v
boxu	box	k1gInSc6	box
začaly	začít	k5eAaPmAgFnP	začít
mít	mít	k5eAaImF	mít
ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
popularizací	popularizace	k1gFnSc7	popularizace
poutí	pouť	k1gFnPc2	pouť
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
devatenáctiletého	devatenáctiletý	k2eAgNnSc2d1	devatenáctileté
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
žen	žena	k1gFnPc2	žena
věnovalo	věnovat	k5eAaImAgNnS	věnovat
exhibičnímu	exhibiční	k2eAgInSc3d1	exhibiční
boxu	box	k1gInSc3	box
dokonce	dokonce	k9	dokonce
profesionálně	profesionálně	k6eAd1	profesionálně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
profesionálním	profesionální	k2eAgInSc6d1	profesionální
boxu	box	k1gInSc6	box
se	se	k3xPyFc4	se
ženy	žena	k1gFnPc1	žena
začaly	začít	k5eAaPmAgFnP	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
promoterky	promoterka	k1gFnSc2	promoterka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patřily	patřit	k5eAaImAgInP	patřit
Aileen	Aileno	k1gNnPc2	Aileno
Eatonová	Eatonový	k2eAgFnSc1d1	Eatonový
nebo	nebo	k8xC	nebo
Bella	Bella	k1gFnSc1	Bella
Burgeová	Burgeová	k1gFnSc1	Burgeová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
seriózní	seriózní	k2eAgInPc1d1	seriózní
zápasy	zápas	k1gInPc1	zápas
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
boxu	box	k1gInSc6	box
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
profesionálním	profesionální	k2eAgInSc6d1	profesionální
boxu	box	k1gInSc6	box
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
osobností	osobnost	k1gFnSc7	osobnost
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
Graciela	Graciela	k1gFnSc1	Graciela
Casillasová	Casillasová	k1gFnSc1	Casillasová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
předlohou	předloha	k1gFnSc7	předloha
k	k	k7c3	k
filmu	film	k1gInSc3	film
Million	Million	k1gInSc1	Million
Dollar	dollar	k1gInSc4	dollar
Baby	baba	k1gFnSc2	baba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
popularizaci	popularizace	k1gFnSc6	popularizace
ženského	ženský	k2eAgInSc2d1	ženský
boxu	box	k1gInSc2	box
bránily	bránit	k5eAaImAgInP	bránit
předsudky	předsudek	k1gInPc1	předsudek
a	a	k8xC	a
především	především	k9	především
obava	obava	k1gFnSc1	obava
promotérů	promotér	k1gMnPc2	promotér
o	o	k7c4	o
zdraví	zdraví	k1gNnSc4	zdraví
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
promotéru	promotér	k1gMnSc3	promotér
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
odvahu	odvaha	k1gFnSc4	odvaha
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
případnou	případný	k2eAgFnSc4d1	případná
smrt	smrt	k1gFnSc4	smrt
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc7d1	samotná
kapitolou	kapitola	k1gFnSc7	kapitola
byli	být	k5eAaImAgMnP	být
ringoví	ringový	k2eAgMnPc1d1	ringový
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
(	(	kIx(	(
<g/>
z	z	k7c2	z
99	[number]	k4	99
<g/>
%	%	kIx~	%
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nechtěli	chtít	k5eNaImAgMnP	chtít
zápasy	zápas	k1gInPc4	zápas
žen	žena	k1gFnPc2	žena
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Ženské	ženský	k2eAgInPc1d1	ženský
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
častěji	často	k6eAd2	často
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
až	až	k9	až
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
univerzitách	univerzita	k1gFnPc6	univerzita
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k8xS	jako
mezi	mezi	k7c7	mezi
profesionály	profesionál	k1gMnPc7	profesionál
<g/>
.	.	kIx.	.
</s>
<s>
Školy	škola	k1gFnPc1	škola
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
preferovaly	preferovat	k5eAaImAgFnP	preferovat
sportovní	sportovní	k2eAgInPc4d1	sportovní
kroužky	kroužek	k1gInPc4	kroužek
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
juda	judo	k1gNnSc2	judo
nebo	nebo	k8xC	nebo
karate	karate	k1gNnSc2	karate
<g/>
.	.	kIx.	.
</s>
<s>
Obava	obava	k1gFnSc1	obava
o	o	k7c4	o
zdraví	zdraví	k1gNnSc4	zdraví
žen	žena	k1gFnPc2	žena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
zásadní	zásadní	k2eAgFnSc4d1	zásadní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
posunu	posun	k1gInSc3	posun
směrem	směr	k1gInSc7	směr
dopředu	dopředu	k6eAd1	dopředu
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
sérii	série	k1gFnSc6	série
stížností	stížnost	k1gFnPc2	stížnost
na	na	k7c4	na
diskriminaci	diskriminace	k1gFnSc4	diskriminace
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
především	především	k9	především
kauza	kauza	k1gFnSc1	kauza
Gail	Gaila	k1gFnPc2	Gaila
Grandchampové	Grandchampové	k2eAgFnPc2d1	Grandchampové
s	s	k7c7	s
Americkou	americký	k2eAgFnSc7d1	americká
amatérskou	amatérský	k2eAgFnSc7d1	amatérská
boxerskou	boxerský	k2eAgFnSc7d1	boxerská
asociací	asociace	k1gFnSc7	asociace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
první	první	k4xOgNnSc1	první
Americké	americký	k2eAgNnSc1d1	americké
mistrovství	mistrovství	k1gNnSc1	mistrovství
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
amatérském	amatérský	k2eAgInSc6d1	amatérský
boxu	box	k1gInSc6	box
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1	úmrtí
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ringu	ring	k1gInSc6	ring
nebyla	být	k5eNaImAgFnS	být
častá	častý	k2eAgFnSc1d1	častá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
při	při	k7c6	při
zápasu	zápas	k1gInSc6	zápas
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Patricia	Patricius	k1gMnSc4	Patricius
Quinnová	Quinnová	k1gFnSc1	Quinnová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
Katie	Katie	k1gFnSc1	Katie
Dallamová	Dallamový	k2eAgFnSc1d1	Dallamový
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Patricia	Patricius	k1gMnSc4	Patricius
Devellerezová	Devellerezová	k1gFnSc1	Devellerezová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Stacey	Stacea	k1gFnSc2	Stacea
Youngová	Youngový	k2eAgFnSc1d1	Youngová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Becky	Becka	k1gFnSc2	Becka
Zerlentesová	Zerlentesový	k2eAgFnSc1d1	Zerlentesový
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
k	k	k7c3	k
žádnému	žádný	k3yNgNnSc3	žádný
úmrtí	úmrtí	k1gNnSc3	úmrtí
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
ženský	ženský	k2eAgInSc1d1	ženský
box	box	k1gInSc1	box
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
programu	program	k1gInSc2	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Ženský	ženský	k2eAgInSc1d1	ženský
box	box	k1gInSc1	box
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
jako	jako	k9	jako
Egypt	Egypt	k1gInSc4	Egypt
nebo	nebo	k8xC	nebo
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
lehká	lehký	k2eAgFnSc1d1	lehká
muší	muší	k2eAgFnSc1d1	muší
-	-	kIx~	-
nad	nad	k7c7	nad
46	[number]	k4	46
kg	kg	kA	kg
do	do	k7c2	do
49	[number]	k4	49
kg	kg	kA	kg
muší	muší	k2eAgMnSc1d1	muší
-	-	kIx~	-
nad	nad	k7c7	nad
49	[number]	k4	49
kg	kg	kA	kg
do	do	k7c2	do
52	[number]	k4	52
kg	kg	kA	kg
bantamová	bantamový	k2eAgFnSc1d1	bantamová
-	-	kIx~	-
nad	nad	k7c7	nad
52	[number]	k4	52
kg	kg	kA	kg
do	do	k7c2	do
56	[number]	k4	56
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
-	-	kIx~	-
nad	nad	k7c7	nad
56	[number]	k4	56
kg	kg	kA	kg
do	do	k7c2	do
60	[number]	k4	60
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
velterová	velterový	k2eAgFnSc1d1	velterová
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
nad	nad	k7c7	nad
60	[number]	k4	60
kg	kg	kA	kg
do	do	k7c2	do
64	[number]	k4	64
kg	kg	kA	kg
velterová	velterový	k2eAgFnSc1d1	velterová
-	-	kIx~	-
nad	nad	k7c7	nad
64	[number]	k4	64
kg	kg	kA	kg
do	do	k7c2	do
69	[number]	k4	69
kg	kg	kA	kg
střední	střední	k2eAgFnSc1d1	střední
-	-	kIx~	-
nad	nad	k7c7	nad
69	[number]	k4	69
kg	kg	kA	kg
do	do	k7c2	do
75	[number]	k4	75
kg	kg	kA	kg
polotěžká	polotěžký	k2eAgFnSc1d1	polotěžká
-	-	kIx~	-
nad	nad	k7c7	nad
75	[number]	k4	75
kg	kg	kA	kg
do	do	k7c2	do
81	[number]	k4	81
kg	kg	kA	kg
těžká	těžký	k2eAgFnSc1d1	těžká
-	-	kIx~	-
nad	nad	k7c7	nad
81	[number]	k4	81
kg	kg	kA	kg
do	do	k7c2	do
91	[number]	k4	91
kg	kg	kA	kg
supertěžká	supertěžký	k2eAgFnSc1d1	supertěžká
-	-	kIx~	-
nad	nad	k7c7	nad
91	[number]	k4	91
kg	kg	kA	kg
Tyto	tento	k3xDgFnPc1	tento
váhové	váhový	k2eAgFnPc1d1	váhová
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc4d1	platný
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
lehká	lehký	k2eAgFnSc1d1	lehká
muší	muší	k2eAgFnSc1d1	muší
-	-	kIx~	-
nad	nad	k7c7	nad
45	[number]	k4	45
do	do	k7c2	do
48	[number]	k4	48
kg	kg	kA	kg
muší	muší	k2eAgMnSc1d1	muší
-	-	kIx~	-
nad	nad	k7c7	nad
48	[number]	k4	48
kg	kg	kA	kg
do	do	k7c2	do
51	[number]	k4	51
kg	kg	kA	kg
bantamová	bantamový	k2eAgFnSc1d1	bantamová
-	-	kIx~	-
nad	nad	k7c7	nad
51	[number]	k4	51
kg	kg	kA	kg
do	do	k7c2	do
54	[number]	k4	54
kg	kg	kA	kg
pérová	pérový	k2eAgFnSc1d1	pérová
-	-	kIx~	-
nad	nad	k7c7	nad
54	[number]	k4	54
kg	kg	kA	kg
do	do	k7c2	do
57	[number]	k4	57
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
-	-	kIx~	-
nad	nad	k7c7	nad
57	[number]	k4	57
kg	kg	kA	kg
do	do	k7c2	do
60	[number]	k4	60
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
velterová	velterový	k2eAgFnSc1d1	velterová
-	-	kIx~	-
nad	nad	k7c7	nad
60	[number]	k4	60
<g />
.	.	kIx.	.
</s>
<s>
kg	kg	kA	kg
do	do	k7c2	do
64	[number]	k4	64
kg	kg	kA	kg
velterová	velterový	k2eAgFnSc1d1	velterová
-	-	kIx~	-
nad	nad	k7c7	nad
64	[number]	k4	64
kg	kg	kA	kg
do	do	k7c2	do
69	[number]	k4	69
kg	kg	kA	kg
střední	střední	k2eAgFnSc1d1	střední
-	-	kIx~	-
nad	nad	k7c7	nad
69	[number]	k4	69
kg	kg	kA	kg
do	do	k7c2	do
75	[number]	k4	75
kg	kg	kA	kg
polotěžká	polotěžký	k2eAgFnSc1d1	polotěžká
-	-	kIx~	-
nad	nad	k7c7	nad
75	[number]	k4	75
kg	kg	kA	kg
do	do	k7c2	do
81	[number]	k4	81
kg	kg	kA	kg
těžká	těžký	k2eAgFnSc1d1	těžká
-	-	kIx~	-
nad	nad	k7c7	nad
81	[number]	k4	81
kg	kg	kA	kg
Tyto	tento	k3xDgFnPc1	tento
váhové	váhový	k2eAgFnPc1d1	váhová
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc4d1	platný
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
Pinová	Pinová	k1gFnSc1	Pinová
-	-	kIx~	-
do	do	k7c2	do
46	[number]	k4	46
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
<g />
.	.	kIx.	.
</s>
<s>
muší	muší	k2eAgFnSc1d1	muší
-	-	kIx~	-
nad	nad	k7c7	nad
46	[number]	k4	46
kg	kg	kA	kg
do	do	k7c2	do
48	[number]	k4	48
kg	kg	kA	kg
Muší	muší	k2eAgMnSc1d1	muší
-	-	kIx~	-
nad	nad	k7c7	nad
48	[number]	k4	48
kg	kg	kA	kg
do	do	k7c2	do
50	[number]	k4	50
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
bantamová	bantamový	k2eAgFnSc1d1	bantamová
-	-	kIx~	-
nad	nad	k7c7	nad
50	[number]	k4	50
kg	kg	kA	kg
do	do	k7c2	do
52	[number]	k4	52
kg	kg	kA	kg
Bantamová	bantamový	k2eAgFnSc1d1	bantamová
-	-	kIx~	-
nad	nad	k7c7	nad
52	[number]	k4	52
kg	kg	kA	kg
do	do	k7c2	do
54	[number]	k4	54
kg	kg	kA	kg
Pérová	pérový	k2eAgFnSc1d1	pérová
-	-	kIx~	-
nad	nad	k7c7	nad
54	[number]	k4	54
kg	kg	kA	kg
do	do	k7c2	do
57	[number]	k4	57
kg	kg	kA	kg
Lehká	Lehká	k1gFnSc1	Lehká
-	-	kIx~	-
nad	nad	k7c4	nad
57	[number]	k4	57
kg	kg	kA	kg
do	do	k7c2	do
60	[number]	k4	60
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
<g />
.	.	kIx.	.
</s>
<s>
velterová	velterový	k2eAgFnSc1d1	velterová
-	-	kIx~	-
nad	nad	k7c7	nad
60	[number]	k4	60
kg	kg	kA	kg
do	do	k7c2	do
63	[number]	k4	63
kg	kg	kA	kg
Velterová	velterový	k2eAgFnSc1d1	velterová
-	-	kIx~	-
nad	nad	k7c7	nad
63	[number]	k4	63
kg	kg	kA	kg
do	do	k7c2	do
66	[number]	k4	66
kg	kg	kA	kg
lehká	lehký	k2eAgFnSc1d1	lehká
střední	střední	k2eAgFnSc1d1	střední
-	-	kIx~	-
nad	nad	k7c7	nad
66	[number]	k4	66
kg	kg	kA	kg
do	do	k7c2	do
70	[number]	k4	70
kg	kg	kA	kg
Střední	střední	k2eAgFnSc1d1	střední
nad	nad	k7c4	nad
70	[number]	k4	70
kg	kg	kA	kg
do	do	k7c2	do
75	[number]	k4	75
kg	kg	kA	kg
Polotěžká	polotěžký	k2eAgFnSc1d1	polotěžká
-	-	kIx~	-
nad	nad	k7c7	nad
75	[number]	k4	75
kg	kg	kA	kg
do	do	k7c2	do
80	[number]	k4	80
kg	kg	kA	kg
Těžká	těžký	k2eAgFnSc1d1	těžká
-	-	kIx~	-
nad	nad	k7c7	nad
80	[number]	k4	80
kg	kg	kA	kg
Tyto	tento	k3xDgFnPc1	tento
váhové	váhový	k2eAgFnPc1d1	váhová
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
papírová	papírový	k2eAgFnSc1d1	papírová
-	-	kIx~	-
do	do	k7c2	do
48	[number]	k4	48
kg	kg	kA	kg
<g/>
,	,	kIx,	,
muší	muší	k2eAgFnSc1d1	muší
-	-	kIx~	-
nad	nad	k7c7	nad
48	[number]	k4	48
kg	kg	kA	kg
do	do	k7c2	do
51	[number]	k4	51
kg	kg	kA	kg
<g/>
,	,	kIx,	,
bantamová	bantamový	k2eAgFnSc1d1	bantamová
-	-	kIx~	-
nad	nad	k7c7	nad
51	[number]	k4	51
kg	kg	kA	kg
do	do	k7c2	do
54	[number]	k4	54
kg	kg	kA	kg
<g/>
,	,	kIx,	,
pérová	pérový	k2eAgFnSc1d1	pérová
-	-	kIx~	-
nad	nad	k7c7	nad
54	[number]	k4	54
kg	kg	kA	kg
do	do	k7c2	do
57	[number]	k4	57
kg	kg	kA	kg
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
-	-	kIx~	-
nad	nad	k7c7	nad
57	[number]	k4	57
kg	kg	kA	kg
do	do	k7c2	do
60	[number]	k4	60
<g />
.	.	kIx.	.
</s>
<s>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
velterová	velterový	k2eAgFnSc1d1	velterová
-	-	kIx~	-
nad	nad	k7c7	nad
60	[number]	k4	60
kg	kg	kA	kg
do	do	k7c2	do
64	[number]	k4	64
kg	kg	kA	kg
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
střední	střední	k2eAgFnSc1d1	střední
-	-	kIx~	-
nad	nad	k7c7	nad
64	[number]	k4	64
kg	kg	kA	kg
do	do	k7c2	do
69	[number]	k4	69
kg	kg	kA	kg
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
-	-	kIx~	-
nad	nad	k7c7	nad
69	[number]	k4	69
kg	kg	kA	kg
do	do	k7c2	do
75	[number]	k4	75
kg	kg	kA	kg
<g/>
,	,	kIx,	,
polotěžká	polotěžký	k2eAgFnSc1d1	polotěžká
-	-	kIx~	-
nad	nad	k7c7	nad
75	[number]	k4	75
kg	kg	kA	kg
do	do	k7c2	do
81	[number]	k4	81
kg	kg	kA	kg
<g/>
,	,	kIx,	,
těžká	těžký	k2eAgFnSc1d1	těžká
-	-	kIx~	-
nad	nad	k7c7	nad
81	[number]	k4	81
kg	kg	kA	kg
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
91	[number]	k4	91
kg	kg	kA	kg
<g/>
,	,	kIx,	,
supertěžká	supertěžký	k2eAgFnSc1d1	supertěžká
-	-	kIx~	-
nad	nad	k7c7	nad
91	[number]	k4	91
kg	kg	kA	kg
Tyto	tento	k3xDgFnPc1	tento
váhové	váhový	k2eAgFnPc1d1	váhová
kategorie	kategorie	k1gFnPc1	kategorie
byly	být	k5eAaImAgFnP	být
platné	platný	k2eAgNnSc1d1	platné
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Legality	legalita	k1gFnSc2	legalita
of	of	k?	of
Boxing	boxing	k1gInSc1	boxing
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
2007	[number]	k4	2007
Brailsford	Brailsfordo	k1gNnPc2	Brailsfordo
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
:	:	kIx,	:
Bareknuckles	Bareknuckles	k1gMnSc1	Bareknuckles
<g/>
:	:	kIx,	:
A	a	k9	a
Social	Social	k1gInSc1	Social
History	Histor	k1gInPc1	Histor
of	of	k?	of
Prizefighting	Prizefighting	k1gInSc1	Prizefighting
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
1988	[number]	k4	1988
Gradopolov	Gradopolovo	k1gNnPc2	Gradopolovo
<g/>
,	,	kIx,	,
K.	K.	kA	K.
V	V	kA	V
<g/>
:	:	kIx,	:
Rohování	rohování	k1gNnSc1	rohování
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
Hargreaves	Hargreavesa	k1gFnPc2	Hargreavesa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Women	Women	k1gInSc1	Women
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Boxing	boxing	k1gInSc1	boxing
and	and	k?	and
Related	Related	k1gInSc1	Related
Activities	Activities	k1gInSc1	Activities
<g/>
,	,	kIx,	,
Westport	Westport	k1gInSc1	Westport
2003	[number]	k4	2003
Heinz	Heinza	k1gFnPc2	Heinza
<g/>
,	,	kIx,	,
W.	W.	kA	W.
C.	C.	kA	C.
<g/>
:	:	kIx,	:
Fireside	Firesid	k1gInSc5	Firesid
Book	Book	k1gInSc4	Book
of	of	k?	of
Boxing	boxing	k1gInSc1	boxing
Sammons	Sammons	k1gInSc1	Sammons
<g/>
,	,	kIx,	,
J.	J.	kA	J.
T.	T.	kA	T.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Beyonf	Beyonf	k1gInSc1	Beyonf
the	the	k?	the
Ring	ring	k1gInSc1	ring
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Role	role	k1gFnSc1	role
of	of	k?	of
Boxing	boxing	k1gInSc1	boxing
in	in	k?	in
American	American	k1gInSc1	American
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gInSc4	Illinois
Press	Pressa	k1gFnPc2	Pressa
1990	[number]	k4	1990
Sheard	Shearda	k1gFnPc2	Shearda
<g/>
,	,	kIx,	,
K.	K.	kA	K.
C.	C.	kA	C.
<g/>
:	:	kIx,	:
Aspect	Aspect	k1gMnSc1	Aspect
of	of	k?	of
boxing	boxing	k1gInSc1	boxing
in	in	k?	in
the	the	k?	the
Western	Western	kA	Western
Civilizing	Civilizing	k1gInSc4	Civilizing
Process	Processa	k1gFnPc2	Processa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Wiganll	Wiganlla	k1gFnPc2	Wiganlla
<g/>
,	,	kIx,	,
T.	T.	kA	T.
C.	C.	kA	C.
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
Boxing	boxing	k1gInSc1	boxing
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g />
.	.	kIx.	.
</s>
<s>
1923	[number]	k4	1923
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
box	box	k1gInSc4	box
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
box	box	k1gInSc1	box
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Box	box	k1gInSc1	box
<g/>
.	.	kIx.	.
<g/>
clanweb	clanwba	k1gFnPc2	clanwba
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
-	-	kIx~	-
web	web	k1gInSc1	web
o	o	k7c6	o
technice	technika	k1gFnSc6	technika
boxu	box	k1gInSc2	box
Profiboxing	Profiboxing	k1gInSc1	Profiboxing
-	-	kIx~	-
stránky	stránka	k1gFnPc1	stránka
našeho	náš	k3xOp1gMnSc2	náš
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
profesionála	profesionál	k1gMnSc2	profesionál
Lukáše	Lukáš	k1gMnSc2	Lukáš
Konečného	Konečného	k2eAgInSc4d1	Konečného
Box	box	k1gInSc4	box
<g/>
.	.	kIx.	.
<g/>
mefistofeles	mefistofeles	k1gMnSc1	mefistofeles
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
magazín	magazín	k1gInSc1	magazín
o	o	k7c6	o
boxu	box	k1gInSc6	box
Box	box	k1gInSc1	box
-	-	kIx~	-
těžká	těžký	k2eAgFnSc1d1	těžká
váha	váha	k1gFnSc1	váha
-	-	kIx~	-
Novinky	novinka	k1gFnPc1	novinka
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
boxu	box	k1gInSc2	box
</s>
<s>
Příčinu	příčina	k1gFnSc4	příčina
mnohosti	mnohost	k1gFnSc2	mnohost
asociací	asociace	k1gFnSc7	asociace
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
spatřovat	spatřovat	k5eAaImF	spatřovat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
box	box	k1gInSc1	box
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
nejvíce	nejvíce	k6eAd1	nejvíce
provozovaným	provozovaný	k2eAgInSc7d1	provozovaný
individuálním	individuální	k2eAgInSc7d1	individuální
sportem	sport	k1gInSc7	sport
<g/>
.	.	kIx.	.
</s>
