<s>
Tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zřízeny	zřízen	k2eAgInPc1d1	zřízen
dva	dva	k4xCgInPc1	dva
vrchní	vrchní	k2eAgInPc1d1	vrchní
soudy	soud	k1gInPc1	soud
<g/>
:	:	kIx,	:
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
vůči	vůči	k7c3	vůči
krajským	krajský	k2eAgInPc3d1	krajský
soudům	soud	k1gInPc3	soud
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jenž	k3xRgInSc2	jenž
obvodu	obvod	k1gInSc2	obvod
spadají	spadat	k5eAaPmIp3nP	spadat
krajské	krajský	k2eAgInPc1d1	krajský
soudy	soud	k1gInPc1	soud
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
