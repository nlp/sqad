<p>
<s>
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vyšších	vysoký	k2eAgInPc2d2	vyšší
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středoevropské	středoevropský	k2eAgFnSc6d1	středoevropská
tradici	tradice	k1gFnSc6	tradice
jeho	jeho	k3xOp3gFnSc1	jeho
působnost	působnost	k1gFnSc1	působnost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
existujícím	existující	k2eAgFnPc3d1	existující
nebo	nebo	k8xC	nebo
historickým	historický	k2eAgFnPc3d1	historická
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zřízeny	zřízen	k2eAgInPc1d1	zřízen
dva	dva	k4xCgInPc1	dva
vrchní	vrchní	k2eAgInPc1d1	vrchní
soudy	soud	k1gInPc1	soud
<g/>
:	:	kIx,	:
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
vůči	vůči	k7c3	vůči
krajským	krajský	k2eAgInPc3d1	krajský
soudům	soud	k1gInPc3	soud
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jenž	k3xRgInSc2	jenž
obvodu	obvod	k1gInSc2	obvod
spadají	spadat	k5eAaImIp3nP	spadat
krajské	krajský	k2eAgInPc1d1	krajský
soudy	soud	k1gInPc1	soud
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
třetím	třetí	k4xOgInSc7	třetí
článkem	článek	k1gInSc7	článek
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
českých	český	k2eAgInPc2d1	český
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
stojí	stát	k5eAaImIp3nS	stát
už	už	k6eAd1	už
jen	jen	k9	jen
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
existuje	existovat	k5eAaImIp3nS	existovat
instituce	instituce	k1gFnSc1	instituce
vrchního	vrchní	k2eAgInSc2d1	vrchní
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
Oberlandesgericht	Oberlandesgericht	k1gInSc1	Oberlandesgericht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
mezičlánkem	mezičlánek	k1gInSc7	mezičlánek
mezi	mezi	k7c7	mezi
soudem	soud	k1gInSc7	soud
zemským	zemský	k2eAgInSc7d1	zemský
(	(	kIx(	(
<g/>
Landesgericht	Landesgericht	k1gInSc1	Landesgericht
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
(	(	kIx(	(
<g/>
Oberster	Oberster	k1gMnSc1	Oberster
Gerichtshof	Gerichtshof	k1gMnSc1	Gerichtshof
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Bundesgerichtshof	Bundesgerichtshof	k1gInSc1	Bundesgerichtshof
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
pak	pak	k6eAd1	pak
působí	působit	k5eAaImIp3nP	působit
High	High	k1gInSc4	High
Court	Court	k1gInSc1	Court
of	of	k?	of
Justice	justice	k1gFnSc2	justice
(	(	kIx(	(
<g/>
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
Queen	Quena	k1gFnPc2	Quena
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bench	Bench	k1gInSc1	Bench
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
Chancery	Chancer	k1gInPc1	Chancer
Division	Division	k1gInSc1	Division
a	a	k8xC	a
Family	Famila	k1gFnPc1	Famila
Division	Division	k1gInSc1	Division
<g/>
)	)	kIx)	)
a	a	k8xC	a
skotský	skotský	k2eAgInSc1d1	skotský
High	High	k1gInSc1	High
Court	Court	k1gInSc1	Court
of	of	k?	of
Justiciary	Justiciara	k1gFnSc2	Justiciara
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgInPc1d1	vrchní
soudy	soud	k1gInPc1	soud
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnPc2	součást
i	i	k8xC	i
soustavy	soustava	k1gFnSc2	soustava
slovinských	slovinský	k2eAgInPc2d1	slovinský
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
Višja	Višja	k1gMnSc1	Višja
sodišča	sodišča	k1gMnSc1	sodišča
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc4d1	malý
stát	stát	k1gInSc4	stát
existují	existovat	k5eAaImIp3nP	existovat
hned	hned	k6eAd1	hned
čtyři	čtyři	k4xCgFnPc4	čtyři
(	(	kIx(	(
<g/>
Celje	Celj	k1gFnSc2	Celj
<g/>
,	,	kIx,	,
Koper	kopra	k1gFnPc2	kopra
<g/>
,	,	kIx,	,
Ljubljana	Ljubljana	k1gFnSc1	Ljubljana
<g/>
,	,	kIx,	,
Maribor	Maribor	k1gInSc1	Maribor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česko	Česko	k1gNnSc1	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byly	být	k5eAaImAgInP	být
vrchní	vrchní	k2eAgInPc1d1	vrchní
zemské	zemský	k2eAgInPc1d1	zemský
soudy	soud	k1gInPc1	soud
zavedeny	zaveden	k2eAgInPc1d1	zaveden
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
působil	působit	k5eAaImAgInS	působit
Vrchní	vrchní	k2eAgInSc1d1	vrchní
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
i	i	k8xC	i
rakouské	rakouský	k2eAgNnSc4d1	rakouské
Slezsko	Slezsko	k1gNnSc4	Slezsko
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
Vrchní	vrchní	k2eAgInSc1d1	vrchní
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
zemským	zemský	k2eAgInSc7d1	zemský
soudem	soud	k1gInSc7	soud
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
soud	soud	k1gInSc1	soud
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
země	zem	k1gFnSc2	zem
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
krajským	krajský	k2eAgInPc3d1	krajský
soudům	soud	k1gInPc3	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působily	působit	k5eAaImAgFnP	působit
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
jako	jako	k8xC	jako
soudy	soud	k1gInPc1	soud
odvolací	odvolací	k2eAgInPc1d1	odvolací
v	v	k7c6	v
civilních	civilní	k2eAgFnPc6d1	civilní
i	i	k8xC	i
trestních	trestní	k2eAgFnPc6d1	trestní
věcech	věc	k1gFnPc6	věc
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
sborové	sborový	k2eAgInPc4d1	sborový
soudy	soud	k1gInPc4	soud
druhé	druhý	k4xOgFnSc2	druhý
stolice	stolice	k1gFnSc2	stolice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
instanci	instance	k1gFnSc6	instance
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
soudy	soud	k1gInPc1	soud
krajské	krajský	k2eAgFnSc2d1	krajská
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
vyřizovaly	vyřizovat	k5eAaImAgFnP	vyřizovat
také	také	k9	také
civilní	civilní	k2eAgNnSc4d1	civilní
odvolání	odvolání	k1gNnSc4	odvolání
od	od	k7c2	od
okresních	okresní	k2eAgInPc2d1	okresní
soudů	soud	k1gInPc2	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byly	být	k5eAaImAgFnP	být
přejmenovány	přejmenovat	k5eAaPmNgFnP	přejmenovat
na	na	k7c4	na
vrchní	vrchní	k2eAgInPc4d1	vrchní
soudy	soud	k1gInPc4	soud
a	a	k8xC	a
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byly	být	k5eAaImAgInP	být
čtyři	čtyři	k4xCgInPc4	čtyři
<g/>
:	:	kIx,	:
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
zlidovění	zlidovění	k1gNnSc6	zlidovění
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
zrušeny	zrušen	k2eAgInPc4d1	zrušen
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
instituce	instituce	k1gFnSc2	instituce
vrchního	vrchní	k2eAgInSc2d1	vrchní
soudu	soud	k1gInSc2	soud
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
československé	československý	k2eAgFnSc2d1	Československá
federace	federace	k1gFnSc2	federace
díky	díky	k7c3	díky
kompromisnímu	kompromisní	k2eAgNnSc3d1	kompromisní
řešení	řešení	k1gNnSc3	řešení
konkurence	konkurence	k1gFnSc2	konkurence
dvou	dva	k4xCgInPc2	dva
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
federálního	federální	k2eAgNnSc2d1	federální
a	a	k8xC	a
republikového	republikový	k2eAgNnSc2d1	republikové
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
federálního	federální	k2eAgMnSc2d1	federální
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
stal	stát	k5eAaPmAgInS	stát
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
z	z	k7c2	z
republikového	republikový	k2eAgInSc2d1	republikový
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
zahájil	zahájit	k5eAaPmAgInS	zahájit
činnost	činnost	k1gFnSc4	činnost
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
někdy	někdy	k6eAd1	někdy
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
jako	jako	k8xC	jako
nadbytečná	nadbytečný	k2eAgFnSc1d1	nadbytečná
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
vážně	vážně	k6eAd1	vážně
zvažováno	zvažován	k2eAgNnSc1d1	zvažováno
jejich	jejich	k3xOp3gNnSc4	jejich
zrušení	zrušení	k1gNnSc4	zrušení
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
soustava	soustava	k1gFnSc1	soustava
českých	český	k2eAgMnPc2d1	český
soudů	soud	k1gInPc2	soud
změnila	změnit	k5eAaPmAgFnS	změnit
z	z	k7c2	z
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
čtyřčlánkové	čtyřčlánkový	k2eAgFnSc2d1	čtyřčlánková
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgFnSc4d2	jednodušší
trojčlánkovou	trojčlánkový	k2eAgFnSc4d1	trojčlánkový
(	(	kIx(	(
<g/>
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
–	–	k?	–
krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
–	–	k?	–
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Agenda	agenda	k1gFnSc1	agenda
a	a	k8xC	a
působnost	působnost	k1gFnSc1	působnost
===	===	k?	===
</s>
</p>
<p>
<s>
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
v	v	k7c6	v
senátech	senát	k1gInPc6	senát
složených	složený	k2eAgInPc6d1	složený
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
po	po	k7c6	po
přenechání	přenechání	k1gNnSc6	přenechání
správního	správní	k2eAgNnSc2d1	správní
soudnictví	soudnictví	k1gNnSc2	soudnictví
krajským	krajský	k2eAgInPc3d1	krajský
soudům	soud	k1gInPc3	soud
a	a	k8xC	a
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
správnímu	správní	k2eAgInSc3d1	správní
soudu	soud	k1gInSc3	soud
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
dříve	dříve	k6eAd2	dříve
jeho	jeho	k3xOp3gFnSc7	jeho
jedinou	jediný	k2eAgFnSc7d1	jediná
agendou	agenda	k1gFnSc7	agenda
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c6	o
řádných	řádný	k2eAgInPc6d1	řádný
opravných	opravný	k2eAgInPc6d1	opravný
prostředcích	prostředek	k1gInPc6	prostředek
proti	proti	k7c3	proti
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
krajských	krajský	k2eAgMnPc2d1	krajský
soudů	soud	k1gInPc2	soud
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tyto	tento	k3xDgInPc1	tento
soudy	soud	k1gInPc1	soud
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
nejzávažnější	závažný	k2eAgInPc4d3	nejzávažnější
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
(	(	kIx(	(
<g/>
především	především	k9	především
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
dolní	dolní	k2eAgFnSc1d1	dolní
hranice	hranice	k1gFnSc1	hranice
trestní	trestní	k2eAgFnSc2d1	trestní
sazby	sazba	k1gFnSc2	sazba
činí	činit	k5eAaImIp3nS	činit
nejméně	málo	k6eAd3	málo
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
insolvenční	insolvenční	k2eAgNnSc4d1	insolvenční
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
o	o	k7c4	o
spory	spor	k1gInPc4	spor
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
obchodních	obchodní	k2eAgFnPc2d1	obchodní
korporací	korporace	k1gFnPc2	korporace
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
nebo	nebo	k8xC	nebo
směnek	směnka	k1gFnPc2	směnka
a	a	k8xC	a
šeků	šek	k1gInPc2	šek
<g/>
.	.	kIx.	.
<g/>
Pražský	pražský	k2eAgInSc1d1	pražský
vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
má	mít	k5eAaImIp3nS	mít
působnost	působnost	k1gFnSc4	působnost
pro	pro	k7c4	pro
pražský	pražský	k2eAgInSc4d1	pražský
městský	městský	k2eAgInSc4d1	městský
soud	soud	k1gInSc4	soud
a	a	k8xC	a
krajské	krajský	k2eAgInPc4d1	krajský
soudy	soud	k1gInPc4	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
krajské	krajský	k2eAgInPc4d1	krajský
soudy	soud	k1gInPc4	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
německých	německý	k2eAgFnPc2d1	německá
a	a	k8xC	a
rakouských	rakouský	k2eAgFnPc2d1	rakouská
vrchních	vrchní	k1gFnPc2	vrchní
zemských	zemský	k2eAgInPc2d1	zemský
soudů	soud	k1gInPc2	soud
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
