<s>
Jako	jako	k8xS	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
debutoval	debutovat	k5eAaBmAgMnS	debutovat
Galsworthy	Galswortha	k1gFnPc4	Galswortha
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
povídkovým	povídkový	k2eAgInSc7d1	povídkový
souborem	soubor	k1gInSc7	soubor
Ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
světa	svět	k1gInSc2	svět
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
nepříliš	příliš	k6eNd1	příliš
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
romány	román	k1gInPc4	román
<g/>
.	.	kIx.	.
</s>
