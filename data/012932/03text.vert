<p>
<s>
Schirn	Schirn	k1gInSc1	Schirn
Kunsthalle	Kunsthalle	k1gFnSc2	Kunsthalle
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
Schirn	Schirn	k1gMnSc1	Schirn
je	být	k5eAaImIp3nS	být
výstavní	výstavní	k2eAgFnSc4d1	výstavní
síň	síň	k1gFnSc4	síň
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výstavba	výstavba	k1gFnSc1	výstavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
vlastní	vlastní	k2eAgFnSc4d1	vlastní
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pořádá	pořádat	k5eAaImIp3nS	pořádat
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
tematické	tematický	k2eAgFnPc4d1	tematická
výstavy	výstava	k1gFnPc4	výstava
nebo	nebo	k8xC	nebo
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
díla	dílo	k1gNnPc4	dílo
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Schirn	Schirn	k1gInSc4	Schirn
Kunsthalle	Kunsthalle	k1gInSc1	Kunsthalle
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Schirn	Schirna	k1gFnPc2	Schirna
Kunsthalle	Kunsthalle	k1gFnSc2	Kunsthalle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Schirn	Schirn	k1gInSc1	Schirn
Kunsthalle	Kunsthalle	k1gNnSc1	Kunsthalle
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
</s>
</p>
