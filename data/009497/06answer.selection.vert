<s>
Big	Big	k?	Big
Ben	Ben	k1gInSc1	Ben
je	být	k5eAaImIp3nS	být
hovorové	hovorový	k2eAgNnSc4d1	hovorové
označení	označení	k1gNnSc4	označení
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
budovy	budova	k1gFnSc2	budova
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Temže	Temže	k1gFnSc2	Temže
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
neformální	formální	k2eNgNnSc1d1	neformální
pojmenování	pojmenování	k1gNnSc1	pojmenování
Velkého	velký	k2eAgInSc2d1	velký
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
