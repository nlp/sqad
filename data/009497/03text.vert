<p>
<s>
Big	Big	k?	Big
Ben	Ben	k1gInSc1	Ben
je	být	k5eAaImIp3nS	být
hovorové	hovorový	k2eAgNnSc4d1	hovorové
označení	označení	k1gNnSc4	označení
používané	používaný	k2eAgNnSc4d1	používané
pro	pro	k7c4	pro
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
budovy	budova	k1gFnSc2	budova
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Temže	Temže	k1gFnSc2	Temže
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
neformální	formální	k2eNgNnSc1d1	neformální
pojmenování	pojmenování	k1gNnSc1	pojmenování
Velkého	velký	k2eAgInSc2d1	velký
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
nad	nad	k7c7	nad
severovýchodní	severovýchodní	k2eAgFnSc7d1	severovýchodní
částí	část	k1gFnSc7	část
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
zasedá	zasedat	k5eAaImIp3nS	zasedat
britský	britský	k2eAgInSc1d1	britský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Big	Big	k1gFnSc1	Big
Ben	Ben	k1gInSc1	Ben
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
všeobecně	všeobecně	k6eAd1	všeobecně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
Big	Big	k1gFnSc1	Big
Ben	Ben	k1gInSc4	Ben
pouze	pouze	k6eAd1	pouze
přezdívka	přezdívka	k1gFnSc1	přezdívka
hlavního	hlavní	k2eAgInSc2d1	hlavní
a	a	k8xC	a
největšího	veliký	k2eAgInSc2d3	veliký
zvonu	zvon	k1gInSc2	zvon
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odbíjí	odbíjet	k5eAaImIp3nS	odbíjet
celou	celý	k2eAgFnSc4d1	celá
hodinu	hodina	k1gFnSc4	hodina
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgNnSc4d1	oficiální
jméno	jméno	k1gNnSc4	jméno
tohoto	tento	k3xDgInSc2	tento
zvonu	zvon	k1gInSc2	zvon
je	být	k5eAaImIp3nS	být
Great	Great	k2eAgInSc1d1	Great
Bell	bell	k1gInSc1	bell
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
několika	několik	k4yIc2	několik
výkladů	výklad	k1gInPc2	výklad
je	být	k5eAaImIp3nS	být
zvon	zvon	k1gInSc4	zvon
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
po	po	k7c6	po
hlavním	hlavní	k2eAgMnSc6d1	hlavní
staviteli	stavitel	k1gMnSc6	stavitel
siru	sir	k1gMnSc6	sir
Benjaminu	Benjamin	k1gMnSc6	Benjamin
Hallovi	Hall	k1gMnSc6	Hall
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
verze	verze	k1gFnSc2	verze
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
nejtěžším	těžký	k2eAgInSc6d3	nejtěžší
zápasníku	zápasník	k1gMnSc3	zápasník
své	svůj	k3xOyFgMnPc4	svůj
doby	doba	k1gFnSc2	doba
Benjaminu	Benjamin	k1gMnSc3	Benjamin
Cauntovi	Caunt	k1gMnSc3	Caunt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
věž	věž	k1gFnSc4	věž
je	být	k5eAaImIp3nS	být
také	také	k9	také
občas	občas	k6eAd1	občas
používán	používat	k5eAaImNgInS	používat
název	název	k1gInSc1	název
St	St	kA	St
Stephen	Stephen	k1gInSc1	Stephen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tower	Tower	k1gInSc1	Tower
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
nepoužívají	používat	k5eNaImIp3nP	používat
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
zřejmě	zřejmě	k6eAd1	zřejmě
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
názvu	název	k1gInSc2	název
západního	západní	k2eAgNnSc2d1	západní
křídla	křídlo	k1gNnSc2	křídlo
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
–	–	k?	–
St	St	kA	St
Stephen	Stephen	k1gInSc1	Stephen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k8xS	jako
vstup	vstup	k1gInSc1	vstup
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
a	a	k8xC	a
lobbisty	lobbista	k1gMnPc4	lobbista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
šedesátým	šedesátý	k4xOgNnSc7	šedesátý
výročím	výročí	k1gNnSc7	výročí
vlády	vláda	k1gFnSc2	vláda
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
oficiálně	oficiálně	k6eAd1	oficiálně
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Elizabeth	Elizabeth	k1gFnSc4	Elizabeth
Tower	Towra	k1gFnPc2	Towra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hodinová	hodinový	k2eAgFnSc1d1	hodinová
věž	věž	k1gFnSc1	věž
===	===	k?	===
</s>
</p>
<p>
<s>
Věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Charlese	Charles	k1gMnSc2	Charles
Barryho	Barry	k1gMnSc2	Barry
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
původního	původní	k2eAgInSc2d1	původní
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
ve	v	k7c6	v
viktoriánsko-gotickém	viktoriánskootický	k2eAgInSc6d1	viktoriánsko-gotický
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
96,3	[number]	k4	96,3
m	m	kA	m
(	(	kIx(	(
<g/>
315	[number]	k4	315
ft	ft	k?	ft
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věž	věž	k1gFnSc1	věž
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
61	[number]	k4	61
m	m	kA	m
je	být	k5eAaImIp3nS	být
postavená	postavený	k2eAgFnSc1d1	postavená
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
zpevněných	zpevněný	k2eAgMnPc2d1	zpevněný
kamenem	kámen	k1gInSc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
zpevněna	zpevnit	k5eAaPmNgFnS	zpevnit
ocelovou	ocelový	k2eAgFnSc7d1	ocelová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
věže	věž	k1gFnSc2	věž
tvoří	tvořit	k5eAaImIp3nP	tvořit
betonové	betonový	k2eAgNnSc4d1	betonové
těleso	těleso	k1gNnSc4	těleso
tloušťky	tloušťka	k1gFnSc2	tloušťka
3	[number]	k4	3
m	m	kA	m
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
15	[number]	k4	15
*	*	kIx~	*
15	[number]	k4	15
m	m	kA	m
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hloubky	hloubka	k1gFnPc1	hloubka
7	[number]	k4	7
m	m	kA	m
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
8	[number]	k4	8
667	[number]	k4	667
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
hodinové	hodinový	k2eAgInPc1d1	hodinový
ciferníky	ciferník	k1gInPc1	ciferník
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
55	[number]	k4	55
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
půdních	půdní	k2eAgFnPc2d1	půdní
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stavby	stavba	k1gFnSc2	stavba
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
naklání	naklánět	k5eAaImIp3nP	naklánět
asi	asi	k9	asi
o	o	k7c4	o
220	[number]	k4	220
mm	mm	kA	mm
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
termálního	termální	k2eAgInSc2d1	termální
efektu	efekt	k1gInSc2	efekt
také	také	k9	také
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
o	o	k7c4	o
několik	několik	k4yIc4	několik
milimetrů	milimetr	k1gInPc2	milimetr
ročně	ročně	k6eAd1	ročně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
východ	východ	k1gInSc4	východ
–	–	k?	–
západ	západ	k1gInSc1	západ
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hodiny	hodina	k1gFnPc1	hodina
===	===	k?	===
</s>
</p>
<p>
<s>
Hodiny	hodina	k1gFnPc1	hodina
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
největšími	veliký	k2eAgFnPc7d3	veliký
hodinami	hodina	k1gFnPc7	hodina
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
odbít	odbít	k5eAaPmF	odbít
první	první	k4xOgInSc1	první
úder	úder	k1gInSc1	úder
každou	každý	k3xTgFnSc4	každý
celou	celý	k2eAgFnSc4d1	celá
hodinu	hodina	k1gFnSc4	hodina
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
jedné	jeden	k4xCgFnSc2	jeden
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
hodin	hodina	k1gFnPc2	hodina
byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlastní	vlastní	k2eAgFnSc1d1	vlastní
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
hodinových	hodinový	k2eAgInPc2d1	hodinový
ciferníků	ciferník	k1gInPc2	ciferník
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
Augusta	August	k1gMnSc2	August
Pugina	Pugin	k1gMnSc2	Pugin
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
kovovou	kovový	k2eAgFnSc7d1	kovová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
312	[number]	k4	312
kusů	kus	k1gInPc2	kus
opálového	opálový	k2eAgNnSc2d1	opálové
skla	sklo	k1gNnSc2	sklo
podobného	podobný	k2eAgNnSc2d1	podobné
obarvenému	obarvený	k2eAgNnSc3d1	obarvené
oknu	okno	k1gNnSc3	okno
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
skleněné	skleněný	k2eAgInPc1d1	skleněný
kusy	kus	k1gInPc1	kus
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
odejmuty	odejmout	k5eAaPmNgInP	odejmout
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
opravy	oprava	k1gFnSc2	oprava
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
ciferníku	ciferník	k1gInSc2	ciferník
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
DOMINE	DOMINE	kA	DOMINE
SALVAM	SALVAM	kA	SALVAM
FAC	FAC	kA	FAC
REGINAM	REGINAM	kA	REGINAM
NOSTRAM	NOSTRAM	kA	NOSTRAM
VICTORIAM	VICTORIAM	kA	VICTORIAM
PRIMAM	PRIMAM	kA	PRIMAM
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
ochraňuj	ochraňovat	k5eAaImRp2nS	ochraňovat
naši	náš	k3xOp1gFnSc4	náš
královnu	královna	k1gFnSc4	královna
Viktorii	Viktoria	k1gFnSc4	Viktoria
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Jménem	jméno	k1gNnSc7	jméno
Big	Big	k1gFnPc2	Big
Ben	Ben	k1gInSc4	Ben
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc1d1	nazván
šestnáctitunový	šestnáctitunový	k2eAgInSc1d1	šestnáctitunový
zvon	zvon	k1gInSc1	zvon
<g/>
,	,	kIx,	,
odlitý	odlitý	k2eAgInSc1d1	odlitý
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
věž	věž	k1gFnSc1	věž
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
New	New	k1gFnSc6	New
Palace	Palace	k1gFnSc2	Palace
Yard	yard	k1gInSc1	yard
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
úderů	úder	k1gInPc2	úder
srdce	srdce	k1gNnSc2	srdce
zvonu	zvon	k1gInSc2	zvon
popraskal	popraskat	k5eAaPmAgMnS	popraskat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
odlit	odlit	k2eAgInSc1d1	odlit
zvon	zvon	k1gInSc1	zvon
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
13,8	[number]	k4	13,8
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zvon	zvon	k1gInSc1	zvon
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
na	na	k7c4	na
věž	věž	k1gFnSc4	věž
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
menšími	malý	k2eAgInPc7d2	menší
zvony	zvon	k1gInPc7	zvon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1859	[number]	k4	1859
byly	být	k5eAaImAgFnP	být
hodiny	hodina	k1gFnPc1	hodina
plně	plně	k6eAd1	plně
zprovozněny	zprovozněn	k2eAgFnPc1d1	zprovozněna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
zvon	zvon	k1gInSc1	zvon
znovu	znovu	k6eAd1	znovu
popraskal	popraskat	k5eAaPmAgInS	popraskat
vlivem	vliv	k1gInSc7	vliv
opakovaných	opakovaný	k2eAgInPc2d1	opakovaný
úderů	úder	k1gInPc2	úder
srdce	srdce	k1gNnSc2	srdce
zvonu	zvon	k1gInSc2	zvon
(	(	kIx(	(
<g/>
stejného	stejné	k1gNnSc2	stejné
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
původní	původní	k2eAgInSc4d1	původní
zvon	zvon	k1gInSc4	zvon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
plnil	plnit	k5eAaImAgInS	plnit
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc4	funkce
největší	veliký	k2eAgFnSc4d3	veliký
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
zvonů	zvon	k1gInPc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
Big	Big	k1gFnSc2	Big
Ben	Ben	k1gInSc1	Ben
otočen	otočen	k2eAgInSc1d1	otočen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
srdce	srdce	k1gNnSc1	srdce
nenaráželo	narážet	k5eNaImAgNnS	narážet
na	na	k7c4	na
poškozenou	poškozený	k2eAgFnSc4d1	poškozená
část	část	k1gFnSc4	část
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velký	velký	k2eAgInSc1d1	velký
westminsterský	westminsterský	k2eAgInSc1d1	westminsterský
zvon	zvon	k1gInSc1	zvon
===	===	k?	===
</s>
</p>
<p>
<s>
Zvon	zvon	k1gInSc1	zvon
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
13,762	[number]	k4	13,762
t	t	k?	t
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
naladěn	naladit	k5eAaPmNgInS	naladit
na	na	k7c4	na
tón	tón	k1gInSc4	tón
E	E	kA	E
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
zvonu	zvon	k1gInSc2	zvon
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
203	[number]	k4	203
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
údery	úder	k1gInPc7	úder
zvonu	zvon	k1gInSc2	zvon
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
běžným	běžný	k2eAgInSc7d1	běžný
omylem	omyl	k1gInSc7	omyl
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
zvon	zvon	k1gInSc4	zvon
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgMnSc1	třetí
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
po	po	k7c4	po
Great	Great	k1gInSc4	Great
Paul	Paula	k1gFnPc2	Paula
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
17,002	[number]	k4	17,002
t	t	k?	t
a	a	k8xC	a
Great	Great	k2eAgInSc1d1	Great
George	George	k1gInSc1	George
v	v	k7c6	v
Liverpoolské	liverpoolský	k2eAgFnSc6d1	liverpoolská
katedrále	katedrála	k1gFnSc6	katedrála
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
15,013	[number]	k4	15,013
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
zvon	zvon	k1gInSc1	zvon
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
14	[number]	k4	14
t	t	k?	t
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
zvonu	zvon	k1gInSc2	zvon
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
300	[number]	k4	300
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Zvon	zvon	k1gInSc4	zvon
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
společnost	společnost	k1gFnSc1	společnost
John	John	k1gMnSc1	John
Warner	Warner	k1gMnSc1	Warner
and	and	k?	and
Sons	Sons	k1gInSc1	Sons
v	v	k7c4	v
Stockton-on-Tees	Stocktonn-Tees	k1gInSc4	Stockton-on-Tees
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
16	[number]	k4	16
tun	tuna	k1gFnPc2	tuna
Ten	ten	k3xDgMnSc1	ten
popraskal	popraskat	k5eAaPmAgInS	popraskat
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
zkoušen	zkoušet	k5eAaImNgInS	zkoušet
na	na	k7c4	na
Palace	Palace	k1gFnPc4	Palace
Yard	yard	k1gInSc1	yard
<g/>
.	.	kIx.	.
</s>
<s>
Zakázka	zakázka	k1gFnSc1	zakázka
na	na	k7c4	na
odlití	odlití	k1gNnSc4	odlití
nového	nový	k2eAgInSc2d1	nový
zvonu	zvon	k1gInSc2	zvon
byla	být	k5eAaImAgFnS	být
zadána	zadán	k2eAgFnSc1d1	zadána
společnosti	společnost	k1gFnSc2	společnost
Whitechapel	Whitechapel	k1gInSc1	Whitechapel
Bell	bell	k1gInSc1	bell
Foundry	Foundra	k1gFnSc2	Foundra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
znovu	znovu	k6eAd1	znovu
odlila	odlít	k5eAaPmAgFnS	odlít
zvon	zvon	k1gInSc4	zvon
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
13.8	[number]	k4	13.8
t.	t.	k?	t.
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
používán	používán	k2eAgInSc4d1	používán
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
popraskal	popraskat	k5eAaPmAgMnS	popraskat
ale	ale	k8xC	ale
pootočením	pootočení	k1gNnSc7	pootočení
vůči	vůči	k7c3	vůči
srdci	srdce	k1gNnSc3	srdce
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
a	a	k8xC	a
úpravou	úprava	k1gFnSc7	úprava
srdce	srdce	k1gNnSc2	srdce
na	na	k7c4	na
hmotnost	hmotnost	k1gFnSc4	hmotnost
200	[number]	k4	200
kg	kg	kA	kg
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
možnosti	možnost	k1gFnPc4	možnost
dalšího	další	k2eAgNnSc2d1	další
používání	používání	k1gNnSc2	používání
<g/>
.	.	kIx.	.
</s>
<s>
Prasklina	prasklina	k1gFnSc1	prasklina
byla	být	k5eAaImAgFnS	být
zaplněna	zaplnit	k5eAaPmNgFnS	zaplnit
a	a	k8xC	a
otočení	otočení	k1gNnSc1	otočení
zvonu	zvon	k1gInSc2	zvon
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvon	zvon	k1gInSc1	zvon
původně	původně	k6eAd1	původně
laděný	laděný	k2eAgInSc1d1	laděný
na	na	k7c4	na
tón	tón	k1gInSc4	tón
E	E	kA	E
nezní	znět	k5eNaImIp3nS	znět
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
tónu	tón	k1gInSc6	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
zvonem	zvon	k1gInSc7	zvon
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zvonice	zvonice	k1gFnSc1	zvonice
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
zvony	zvon	k1gInPc4	zvon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
ohlašují	ohlašovat	k5eAaImIp3nP	ohlašovat
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
===	===	k?	===
</s>
</p>
<p>
<s>
Hodiny	hodina	k1gFnPc1	hodina
jsou	být	k5eAaImIp3nP	být
pověstné	pověstný	k2eAgInPc1d1	pověstný
svou	svůj	k3xOyFgFnSc7	svůj
spolehlivostí	spolehlivost	k1gFnSc7	spolehlivost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
schopnostmi	schopnost	k1gFnPc7	schopnost
jejich	jejich	k3xOp3gMnPc4	jejich
tvůrce	tvůrce	k1gMnSc4	tvůrce
<g/>
,	,	kIx,	,
amatérského	amatérský	k2eAgMnSc4d1	amatérský
hodináře	hodinář	k1gMnSc4	hodinář
Edmund	Edmund	k1gMnSc1	Edmund
Becketta	Beckett	k1gMnSc4	Beckett
Denisona	Denison	k1gMnSc4	Denison
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
návrhu	návrh	k1gInSc2	návrh
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
hodinářem	hodinář	k1gMnSc7	hodinář
Edwardem	Edward	k1gMnSc7	Edward
Johnem	John	k1gMnSc7	John
Dentem	Dent	k1gMnSc7	Dent
ještě	ještě	k6eAd1	ještě
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
věž	věž	k1gFnSc1	věž
samotná	samotný	k2eAgFnSc1d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Denison	Denison	k1gInSc1	Denison
tak	tak	k9	tak
měl	mít	k5eAaImAgInS	mít
dostatek	dostatek	k1gInSc1	dostatek
času	čas	k1gInSc2	čas
na	na	k7c6	na
experimentování	experimentování	k1gNnSc6	experimentování
a	a	k8xC	a
seřizování	seřizování	k1gNnSc6	seřizování
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Krokové	krokový	k2eAgNnSc1d1	krokové
ústrojí	ústrojí	k1gNnSc1	ústrojí
hodin	hodina	k1gFnPc2	hodina
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
dokonalé	dokonalý	k2eAgNnSc1d1	dokonalé
oddělení	oddělení	k1gNnSc1	oddělení
mezi	mezi	k7c7	mezi
kyvadlem	kyvadlo	k1gNnSc7	kyvadlo
a	a	k8xC	a
hodinovým	hodinový	k2eAgInSc7d1	hodinový
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
uzavřením	uzavření	k1gNnSc7	uzavření
ve	v	k7c6	v
schránce	schránka	k1gFnSc6	schránka
odolné	odolný	k2eAgFnSc6d1	odolná
proti	proti	k7c3	proti
působení	působení	k1gNnSc3	působení
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
podmínek	podmínka	k1gFnPc2	podmínka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
trvalou	trvalý	k2eAgFnSc4d1	trvalá
přesnost	přesnost	k1gFnSc4	přesnost
hodinového	hodinový	k2eAgInSc2d1	hodinový
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodiny	hodina	k1gFnPc1	hodina
si	se	k3xPyFc3	se
uchovaly	uchovat	k5eAaPmAgFnP	uchovat
svou	svůj	k3xOyFgFnSc4	svůj
přesnost	přesnost	k1gFnSc4	přesnost
navzdory	navzdory	k7c3	navzdory
bombardování	bombardování	k1gNnSc3	bombardování
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hustého	hustý	k2eAgNnSc2d1	husté
sněžení	sněžení	k1gNnSc2	sněžení
zpomalily	zpomalit	k5eAaPmAgFnP	zpomalit
a	a	k8xC	a
odbily	odbít	k5eAaPmAgFnP	odbít
půlnoc	půlnoc	k1gFnSc4	půlnoc
o	o	k7c4	o
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
velký	velký	k2eAgInSc1d1	velký
výpadek	výpadek	k1gInSc1	výpadek
postihl	postihnout	k5eAaPmAgInS	postihnout
hodiny	hodina	k1gFnPc4	hodina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Bicí	bicí	k2eAgInSc1d1	bicí
mechanismus	mechanismus	k1gInSc1	mechanismus
se	se	k3xPyFc4	se
poškodil	poškodit	k5eAaPmAgInS	poškodit
vlivem	vliv	k1gInSc7	vliv
kazu	kaz	k1gInSc2	kaz
kovového	kovový	k2eAgInSc2d1	kovový
materiálu	materiál	k1gInSc2	materiál
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1976	[number]	k4	1976
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodiny	hodina	k1gFnPc1	hodina
se	se	k3xPyFc4	se
zastavily	zastavit	k5eAaPmAgFnP	zastavit
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1977	[number]	k4	1977
den	den	k1gInSc1	den
před	před	k7c7	před
všeobecnými	všeobecný	k2eAgFnPc7d1	všeobecná
volbami	volba	k1gFnPc7	volba
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
se	se	k3xPyFc4	se
hodiny	hodina	k1gFnPc1	hodina
na	na	k7c4	na
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
zastavily	zastavit	k5eAaPmAgFnP	zastavit
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
vlivem	vliv	k1gInSc7	vliv
velkého	velký	k2eAgNnSc2d1	velké
horka	horko	k1gNnSc2	horko
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
31.8	[number]	k4	31.8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
činnost	činnost	k1gFnSc1	činnost
hodin	hodina	k1gFnPc2	hodina
asi	asi	k9	asi
na	na	k7c4	na
33	[number]	k4	33
hodin	hodina	k1gFnPc2	hodina
zastavena	zastaven	k2eAgFnSc1d1	zastavena
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
opravy	oprava	k1gFnSc2	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
interval	interval	k1gInSc1	interval
nefunkčnosti	nefunkčnost	k1gFnSc2	nefunkčnost
hodin	hodina	k1gFnPc2	hodina
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
BBC	BBC	kA	BBC
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Big	Big	k1gFnSc2	Big
Ben	Ben	k1gInSc1	Ben
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Big	Big	k1gFnSc1	Big
Ben	Ben	k1gInSc4	Ben
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.earthcam.com/uk/england/london/bigben.php	[url]	k1gInSc1	http://www.earthcam.com/uk/england/london/bigben.php
–	–	k?	–
Web	web	k1gInSc1	web
kamera	kamera	k1gFnSc1	kamera
na	na	k7c4	na
Earthcam	Earthcam	k1gInSc4	Earthcam
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
http://www.bigben.freeservers.com/index2.html	[url]	k1gInSc1	http://www.bigben.freeservers.com/index2.html
–	–	k?	–
WWW	WWW	kA	WWW
stránky	stránka	k1gFnSc2	stránka
Big	Big	k1gFnSc2	Big
Ben	Ben	k1gInSc1	Ben
</s>
</p>
<p>
<s>
The	The	k?	The
chimes	chimes	k1gInSc1	chimes
of	of	k?	of
Big	Big	k1gFnSc1	Big
Ben	Ben	k1gInSc1	Ben
–	–	k?	–
video	video	k1gNnSc1	video
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
<g/>
,	,	kIx,	,
ukazující	ukazující	k2eAgInSc1d1	ukazující
největší	veliký	k2eAgInSc1d3	veliký
zvon	zvon	k1gInSc1	zvon
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
</s>
</p>
