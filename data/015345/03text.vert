<s>
Roman	Roman	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Roman	Roman	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
938	#num#	k4
</s>
<s>
Konstantinopol	Konstantinopol	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
963	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
24	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Konstantinopol	Konstantinopol	k1gInSc1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Anna	Anna	k1gFnSc1
Porfyrogennéta	Porfyrogennéta	k1gFnSc1
<g/>
,	,	kIx,
Basileios	Basileios	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulgaroktonos	Bulgaroktonosa	k1gFnPc2
a	a	k8xC
Konstantin	Konstantin	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porfyrogennetos	Porfyrogennetos	k1gInSc1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Helena	Helena	k1gFnSc1
Lekapene	Lekapen	k1gMnSc5
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnSc1
s	s	k7c7
podobou	podoba	k1gFnSc7
Romana	Roman	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Romanos	Romanos	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
938	#num#	k4
<g/>
–	–	k?
<g/>
963	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
959	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
dvaceti	dvacet	k4xCc2
jedna	jeden	k4xCgFnSc1
let	léto	k1gNnPc2
byzantským	byzantský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
963	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
otráven	otráven	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
Theofano	Theofana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Původ	původ	k1gInSc1
<g/>
,	,	kIx,
mládí	mládí	k1gNnSc1
</s>
<s>
Roman	Roman	k1gMnSc1
byl	být	k5eAaImAgMnS
synem	syn	k1gMnSc7
císaře	císař	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
Heleny	Helena	k1gFnPc1
Lakapeny	Lakapen	k2eAgFnPc1d1
<g/>
,	,	kIx,
dcery	dcera	k1gFnPc1
císaře	císař	k1gMnSc2
Romana	Roman	k1gMnSc2
I.	I.	kA
Pojmenován	pojmenovat	k5eAaPmNgInS
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
dědovi	děd	k1gMnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Roman	Roman	k1gMnSc1
ještě	ještě	k9
jako	jako	k8xC,k8xS
dítě	dítě	k1gNnSc1
oženěn	oženěn	k2eAgMnSc1d1
s	s	k7c7
Berthou	Bertha	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
italského	italský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Huga	Hugo	k1gMnSc2
z	z	k7c2
Arles	Arlesa	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
přijala	přijmout	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Eudoxie	Eudoxie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pádu	pád	k1gInSc6
Lakapenů	Lakapen	k1gMnPc2
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
Konstantin	Konstantin	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
Romana	Roman	k1gMnSc4
spolucísařem	spolucísař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
potomci	potomek	k1gMnPc1
</s>
<s>
Když	když	k8xS
Bertha	Bertha	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
949	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gNnSc1
manželství	manželství	k1gNnSc1
patrně	patrně	k6eAd1
nebylo	být	k5eNaImAgNnS
konzumováno	konzumovat	k5eAaBmNgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
Roman	Roman	k1gMnSc1
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
slib	slib	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
vybrat	vybrat	k5eAaPmF
nevěstu	nevěsta	k1gFnSc4
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romanova	Romanův	k2eAgFnSc1d1
volba	volba	k1gFnSc1
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
na	na	k7c4
dceru	dcera	k1gFnSc4
hostinského	hostinský	k1gMnSc4
jménem	jméno	k1gNnSc7
Anastaso	Anastasa	k1gFnSc5
(	(	kIx(
<g/>
později	pozdě	k6eAd2
přijala	přijmout	k5eAaPmAgFnS
nové	nový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Theofano	Theofana	k1gFnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
956	#num#	k4
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
manželství	manželství	k1gNnPc2
Romana	Roman	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Theofano	Theofana	k1gFnSc5
vzešli	vzejít	k5eAaPmAgMnP
tři	tři	k4xCgMnPc1
potomci	potomek	k1gMnPc1
<g/>
,	,	kIx,
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
a	a	k8xC
nejmladší	mladý	k2eAgFnSc1d3
dcera	dcera	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Basileios	Basileios	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulgaroktonos	Bulgaroktonos	k1gInSc1
(	(	kIx(
<g/>
asi	asi	k9
958	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1025	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
976	#num#	k4
<g/>
–	–	k?
<g/>
1025	#num#	k4
byzantský	byzantský	k2eAgInSc4d1
císař	císař	k1gMnSc1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
kolem	kolem	k7c2
960	#num#	k4
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1028	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1025	#num#	k4
až	až	k9
1028	#num#	k4
byzantský	byzantský	k2eAgInSc4d1
císař	císař	k1gMnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Porfyrogennéta	Porfyrogennéta	k1gFnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
963	#num#	k4
<g/>
–	–	k?
<g/>
1011	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
ruského	ruský	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
Vladimíra	Vladimír	k1gMnSc2
I.	I.	kA
</s>
<s>
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
(	(	kIx(
<g/>
ne	ne	k9
však	však	k9
jisté	jistý	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc7
další	další	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
byla	být	k5eAaImAgFnS
</s>
<s>
Theofano	Theofana	k1gFnSc5
(	(	kIx(
<g/>
955	#num#	k4
<g/>
/	/	kIx~
<g/>
960	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejznámějších	známý	k2eAgFnPc2d3
středověkých	středověký	k2eAgFnPc2d1
královen	královna	k1gFnPc2
a	a	k8xC
císařoven	císařovna	k1gFnPc2
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
římsko-německého	římsko-německý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Oty	Ota	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
matka	matka	k1gFnSc1
Oty	Ota	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
959	#num#	k4
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
Roman	Roman	k1gMnSc1
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prý	prý	k9
tehdy	tehdy	k6eAd1
kolovaly	kolovat	k5eAaImAgFnP
pověsti	pověst	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Roman	Roman	k1gMnSc1
nebo	nebo	k8xC
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
uspíšili	uspíšit	k5eAaPmAgMnP
Konstantinovu	Konstantinův	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
pomocí	pomocí	k7c2
jedu	jed	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roman	Roman	k1gMnSc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
zbavil	zbavit	k5eAaPmAgInS
všech	všecek	k3xTgMnPc2
někdejších	někdejší	k2eAgMnPc2d1
dvořanů	dvořan	k1gMnPc2
a	a	k8xC
rádců	rádce	k1gMnPc2
svého	svůj	k1gMnSc2
otce	otec	k1gMnSc2
a	a	k8xC
nahradil	nahradit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
svými	svůj	k3xOyFgMnPc7
přáteli	přítel	k1gMnPc7
nebo	nebo	k8xC
přáteli	přítel	k1gMnPc7
své	svůj	k3xOyFgFnPc4
ženy	žena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
osobami	osoba	k1gFnPc7
odsunutými	odsunutý	k2eAgFnPc7d1
z	z	k7c2
císařského	císařský	k2eAgInSc2d1
paláce	palác	k1gInSc2
byla	být	k5eAaImAgFnS
také	také	k9
císařova	císařův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
Helena	Helena	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc2
dcery	dcera	k1gFnSc2
–	–	k?
všechny	všechen	k3xTgFnPc1
byly	být	k5eAaImAgFnP
vykázány	vykázat	k5eAaPmNgFnP
do	do	k7c2
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravda	pravda	k1gFnSc1
ovšem	ovšem	k9
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
Romanem	Roman	k1gMnSc7
povolaných	povolaný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
byli	být	k5eAaImAgMnP
schopní	schopný	k2eAgMnPc1d1
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
císařova	císařův	k2eAgMnSc2d1
komorníka	komorník	k1gMnSc2
(	(	kIx(
<g/>
parakoimomenos	parakoimomenos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
eunucha	eunuch	k1gMnSc4
Iosefa	Iosef	k1gMnSc4
Bringase	Bringasa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Požitky	požitek	k1gInPc1
milující	milující	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
přenechal	přenechat	k5eAaPmAgMnS
vojenské	vojenský	k2eAgFnSc2d1
záležitosti	záležitost	k1gFnSc2
svým	svůj	k3xOyFgMnPc3
zkušeným	zkušený	k2eAgMnPc3d1
vojevůdcům	vojevůdce	k1gMnPc3
<g/>
,	,	kIx,
především	především	k9
bratrům	bratr	k1gMnPc3
Leonovi	Leonův	k2eAgMnPc1d1
a	a	k8xC
Nikeforovi	Nikeforův	k2eAgMnPc1d1
Fokům	Fokum	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
započala	započnout	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
proti	proti	k7c3
islámu	islám	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
960	#num#	k4
byl	být	k5eAaImAgInS
Nikeforos	Nikeforosa	k1gFnPc2
Fokas	Fokasa	k1gFnPc2
vyslán	vyslán	k2eAgInSc4d1
ke	k	k7c3
znovudobytí	znovudobytí	k1gNnSc3
Kréty	Kréta	k1gFnSc2
na	na	k7c6
jejích	její	k3xOp3gInPc6
tehdejších	tehdejší	k2eAgInPc6d1
muslimských	muslimský	k2eAgInPc6d1
pánech	pan	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
namáhavém	namáhavý	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
a	a	k8xC
devět	devět	k4xCc4
měsíců	měsíc	k1gInPc2
trvajícím	trvající	k2eAgMnSc6d1
obléhání	obléhání	k1gNnSc1
Candie	Candie	k1gFnSc2
Nikeforos	Nikeforosa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
961	#num#	k4
úspěšně	úspěšně	k6eAd1
završil	završit	k5eAaPmAgInS
obnovení	obnovení	k1gNnSc4
byzantské	byzantský	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
celým	celý	k2eAgInSc7d1
ostrovem	ostrov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
oslavil	oslavit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
triumf	triumf	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Nikeforos	Nikeforosa	k1gFnPc2
poslán	poslán	k2eAgMnSc1d1
na	na	k7c6
východní	východní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
dobyl	dobýt	k5eAaPmAgInS
Kilíkii	Kilíkie	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
962	#num#	k4
dokonce	dokonce	k9
Aleppo	Aleppa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
Leon	Leona	k1gFnPc2
Fokas	Fokas	k1gInSc1
a	a	k8xC
Marianos	Marianos	k1gInSc1
Argyros	Argyrosa	k1gFnPc2
odrazili	odrazit	k5eAaPmAgMnP
nájezd	nájezd	k1gInSc4
Maďarů	Maďar	k1gMnPc2
do	do	k7c2
byzantských	byzantský	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
smrt	smrt	k1gFnSc4
Romana	Roman	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
963	#num#	k4
po	po	k7c6
jedné	jeden	k4xCgFnSc6
dlouhé	dlouhý	k2eAgFnSc6d1
lovecké	lovecký	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
Roman	Roman	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
onemocněl	onemocnět	k5eAaPmAgMnS
a	a	k8xC
krátce	krátce	k6eAd1
nato	nato	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověsti	pověst	k1gFnPc1
připisují	připisovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnSc4
smrt	smrt	k1gFnSc4
jedu	jed	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
mu	on	k3xPp3gNnSc3
měla	mít	k5eAaImAgFnS
podat	podat	k5eAaPmF
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Theofano	Theofana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roman	Roman	k1gMnSc1
se	se	k3xPyFc4
během	během	k7c2
své	svůj	k3xOyFgFnSc2
vlády	vláda	k1gFnSc2
spoléhal	spoléhat	k5eAaImAgMnS
na	na	k7c4
rady	rada	k1gFnPc4
své	svůj	k3xOyFgFnSc2
ženy	žena	k1gFnSc2
a	a	k8xC
úředníků	úředník	k1gMnPc2
jako	jako	k9
byl	být	k5eAaImAgMnS
Iosef	Iosef	k1gMnSc1
Bringas	Bringas	k1gMnSc1
a	a	k8xC
disponoval	disponovat	k5eAaBmAgInS
tak	tak	k9
relativně	relativně	k6eAd1
zdatnou	zdatný	k2eAgFnSc7d1
administrativou	administrativa	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ale	ale	k9
vyvolalo	vyvolat	k5eAaPmAgNnS
nelibost	nelibost	k1gFnSc4
mezi	mezi	k7c7
magnáty	magnát	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
měli	mít	k5eAaImAgMnP
značný	značný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Romanově	Romanův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
vládla	vládnout	k5eAaImAgFnS
jménem	jméno	k1gNnSc7
jeho	jeho	k3xOp3gMnPc2
dvou	dva	k4xCgMnPc2
nezletilých	nezletilých	k2eAgMnPc2d1,k2eNgMnPc2d1
synů	syn	k1gMnPc2
Theofano	Theofana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
se	se	k3xPyFc4
však	však	k9
znovu	znovu	k6eAd1
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
generála	generál	k1gMnSc4
Nikefora	Nikefor	k1gMnSc4
Foka	Fokus	k1gMnSc4
a	a	k8xC
ten	ten	k3xDgMnSc1
uzurpoval	uzurpovat	k5eAaBmAgMnS
veškerou	veškerý	k3xTgFnSc4
moc	moc	k1gFnSc4
na	na	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgMnS
Nikeforos	Nikeforosa	k1gFnPc2
za	za	k7c2
pomoci	pomoc	k1gFnSc2
Theofano	Theofana	k1gFnSc5
roku	rok	k1gInSc2
969	#num#	k4
zavražděn	zavražděn	k2eAgInSc1d1
<g/>
,	,	kIx,
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
zmocnil	zmocnit	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
I.	I.	kA
Tzimiskes	Tzimiskes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legitimní	legitimní	k2eAgMnPc1d1
následníci	následník	k1gMnPc1
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
Romanovi	Romanův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
zatlačeni	zatlačit	k5eAaPmNgMnP
do	do	k7c2
pozadí	pozadí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
roku	rok	k1gInSc2
976	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
osmnácti	osmnáct	k4xCc6
letech	léto	k1gNnPc6
vlády	vláda	k1gFnSc2
chopil	chopit	k5eAaPmAgMnS
prvorozený	prvorozený	k2eAgMnSc1d1
Basileios	Basileios	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
bratr	bratr	k1gMnSc1
byl	být	k5eAaImAgMnS
formálně	formálně	k6eAd1
spolucísařem	spolucísař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
ZÁSTĚROVÁ	Zástěrová	k1gFnSc1
<g/>
,	,	kIx,
Bohumila	Bohumila	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
Byzance	Byzanc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Makedonská	makedonský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Roman	Romana	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Biografie	biografie	k1gFnPc1
Romana	Roman	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Konstantin	Konstantin	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porfyrogennetos	Porfyrogennetos	k1gInSc1
</s>
<s>
Byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
959	#num#	k4
<g/>
–	–	k?
<g/>
963	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Nikeforos	Nikeforosa	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fokas	Fokas	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1216731683	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
6521159880871718540002	#num#	k4
</s>
